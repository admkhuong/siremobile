<cfcomponent>
	<cfinclude template="/public/paths.cfm">
    <cfinclude template="/session/sire/configs/paths.cfm">
    <cfinclude template="/session/sire/configs/env_paths.cfm">
    <cfinclude template="/session/sire/configs/userConstants.cfm">
	<!--- Used IF locking down by session - User has to be logged in --->
	<cfparam name="Session.DBSourceEBM" default="Bishop"/>
    <cfparam name="Session.USERID" default="0"/>

	<cffunction name="GetGroupDetailsForDatatable" access="remote" output="true" returnFormat="jSon" hint="get group details for datatable">
		<cfargument name="sColumns" default="" />
		<cfargument name="iDisplayStart" TYPE="numeric" required="false" default="1">
		<cfargument name="iDisplayLength" TYPE="numeric" required="false" default="20">
		<cfargument name="iSortCol_0" type="numeric" required="false" default="0">
		<cfargument name="sSortDir_0" type="string" required="false" default="asc">
		<cfargument name="iSortCol_1" type="numeric" required="false" default="0">
		<cfargument name="sSortDir_1" type="string" required="false" default="asc">
		<cfargument name="iSortCol_2" type="numeric" required="false" default="0">
		<cfargument name="sSortDir_2" type="string" required="false" default="asc">
		<cfargument name="iSortCol_3" type="numeric" required="false" default="0">
		<cfargument name="sSortDir_3" type="string" required="false" default="asc">
		<cfargument name="iSortCol_4" type="numeric" required="false" default="0">
		<cfargument name="sSortDir_4" type="string" required="false" default="asc">
		<cfargument name="iSortingCols" type="numeric" required="false" default="1">
		<cfargument name="bSortable_0" type="boolean" required="false" default=false>
		<cfargument name="bSortable_1" type="boolean" required="false" default=false>
		<cfargument name="bSortable_2" type="boolean" required="false" default=true>
		<cfargument name="bSortable_3" type="boolean" required="false" default=true>
		<cfargument name="bSortable_4" type="boolean" required="false" default=false>
		<cfargument name="customFilter" default="">
		<cfargument name="INPGROUPID" default="0">
		<cfargument name="sendType" default="0">
		<cfargument name="toSend" type="boolean" default="true">
		<cfparam name="URL.INPGROUPID" type="numeric" default="#arguments.INPGROUPID#">

		<cfset var filterItem	= '' />
		<cfset var contactList	= '' />
		<cfset var INPGROUPID	= URL.INPGROUPID />

		<cfset var dataOut = {}>
		<cfset dataout.RXRESULTCODE = -1>
	    <cfset dataout.MESSAGE = "">
		<cfset dataout.ERRMESSAGE = "">
		<cfset dataout["aaData"] = ArrayNew(1)>
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
		<cfset var filterFields = {
			"CONTACTSTRING_VCH" = "CS.ContactString_vch",
			"OPTINOUT_INT" = "OptInOut_int"
		}>
		<cftry>
			<!---get data here --->
			<cfset var contactCount = "">
			<cfquery name="contactCount" datasource="#Session.DBSourceREAD#">
				SELECT COUNT(DISTINCT GC.ContactAddressId_bi) AS TOTALCOUNT
				FROM simplelists.groupcontactlist AS GC
				INNER JOIN simplelists.grouplist GL
					ON GL.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
	                <cfif INPGROUPID GT 0>
						AND GC.GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">
		            </cfif>
					AND GC.GroupId_bi = GL.GroupId_bi
				INNER JOIN simplelists.contactstring CS
					<cfif toSend>
						ON CS.ContactType_int = <cfif sendType EQ 0>3<cfelse>4</cfif> AND GC.ContactAddressId_bi = CS.ContactAddressId_bi
					<cfelse>
						ON (CS.ContactType_int = 3 OR CS.ContactType_int = 4) AND GC.ContactAddressId_bi = CS.ContactAddressId_bi
					</cfif>
		            <cfif customFilter NEQ "">
		            	<cfoutput>
			            	<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
			            		<cfif filterItem.NAME EQ "CONTACTSTRING_VCH" AND Trim(filterItem.VALUE) NEQ "">
			            			<cfswitch expression="#filterItem.OPERATOR#">
			            				<cfcase value="LIKE">
			            					AND #filterFields[filterItem.NAME]# LIKE <CFQUERYPARAM CFSQLTYPE="#filterItem.TYPE#" VALUE="%#filterItem.VALUE#%">
			            				</cfcase>
			            			</cfswitch>
		            			</cfif>
			            	</cfloop>
		            	</cfoutput>
	            	</cfif>
				LEFT JOIN simplelists.optinout IO
					ON (IO.OptOut_dt IS NOT NULL
					OR IO.BatchId_bi IN (SELECT BatchId_bi FROM simpleobjects.batch WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">)
					OR IO.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">)
					AND CS.ContactString_vch = IO.ContactString_vch
					AND LENGTH(CS.ContactString_vch) < 14
				GROUP BY CS.ContactAddressId_bi
	            <cfif customFilter NEQ "">
	            	<cfoutput>
		            	<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
	            			<cfif filterItem.NAME EQ "OPTINOUT_INT" AND filterItem.VALUE GE 0>
		            			<cfswitch expression="#filterItem.OPERATOR#">
		            				<cfcase value="=">
		            					HAVING BIT_OR(IF(IO.OptOut_dt IS NOT NULL, 0, 1)) = <CFQUERYPARAM CFSQLTYPE="#filterItem.TYPE#" VALUE="#filterItem.VALUE#">
		            				</cfcase>
		            			</cfswitch>
		            		</cfif>
		            	</cfloop>
	            	</cfoutput>
            	</cfif>
	        </cfquery>

			<cfset dataout["iTotalRecords"] = (contactCount.RecordCount GT 1 ? contactCount.RecordCount : contactCount.TOTALCOUNT)>
			<cfif !isNumeric(dataout["iTotalRecords"])>
				<cfset dataout["iTotalRecords"] = 0>
			</cfif>
			<cfset dataout["iTotalDisplayRecords"] = dataout["iTotalRecords"]>

			<cfset var GetNumbers = "">
			<cfquery name="contactList" datasource="#Session.DBSourceREAD#">
				SELECT GC.ContactAddressId_bi, GC.GroupId_bi, CS.ContactString_vch, CS.ContactId_bi, CS.ContactType_int,
					BIT_OR(IF(IO.OptOut_dt IS NOT NULL, 0, 1)) AS OptInOut_int,
					MAX(IO.OptIn_dt) AS OptIn_dt, MAX(IO.OptOut_dt) AS OptOut_dt,
					CS.Created_dt,CS.LastUpdated_dt
				FROM simplelists.groupcontactlist AS GC
				INNER JOIN simplelists.grouplist GL
					ON GL.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
	                <cfif INPGROUPID GT 0>
						AND GC.GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">
		            </cfif>
					AND GC.GroupId_bi = GL.GroupId_bi
				INNER JOIN simplelists.contactstring CS
					<cfif toSend>
						ON CS.ContactType_int = <cfif sendType EQ 0>3<cfelse>4</cfif> AND GC.ContactAddressId_bi = CS.ContactAddressId_bi
					<cfelse>
						ON (CS.ContactType_int = 3 OR CS.ContactType_int = 4) AND GC.ContactAddressId_bi = CS.ContactAddressId_bi
					</cfif>
		            <cfif customFilter NEQ "">
		            	<cfoutput>
			            	<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
			            		<cfif filterItem.NAME EQ "CONTACTSTRING_VCH" AND Trim(filterItem.VALUE) NEQ "">
			            			<cfswitch expression="#filterItem.OPERATOR#">
			            				<cfcase value="LIKE">
			            					AND #filterFields[filterItem.NAME]# LIKE <CFQUERYPARAM CFSQLTYPE="#filterItem.TYPE#" VALUE="%#filterItem.VALUE#%">
			            				</cfcase>
			            			</cfswitch>
			            		</cfif>
			            	</cfloop>
		            	</cfoutput>
	            	</cfif>
				LEFT JOIN simplelists.optinout IO
					ON (IO.OptOut_dt IS NOT NULL
					OR IO.BatchId_bi IN (SELECT BatchId_bi FROM simpleobjects.batch WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">)
					OR IO.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">)
					AND CS.ContactString_vch = IO.ContactString_vch
					AND LENGTH(CS.ContactString_vch) < 14
				GROUP BY CS.ContactAddressId_bi
				<cfif customFilter NEQ "">
	            	<cfoutput>
		            	<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
	            			<cfif filterItem.NAME EQ "OPTINOUT_INT" AND filterItem.VALUE GE 0>
		            			<cfswitch expression="#filterItem.OPERATOR#">
		            				<cfcase value="=">
		            					HAVING BIT_OR(IF(IO.OptOut_dt IS NOT NULL, 0, 1)) = <CFQUERYPARAM CFSQLTYPE="#filterItem.TYPE#" VALUE="#filterItem.VALUE#">
		            				</cfcase>
		            			</cfswitch>
		            		</cfif>
		            	</cfloop>
	            	</cfoutput>
            	</cfif>
				<cfif iSortingCols GT 0>
					<cfoutput>
					<cfswitch expression="#iSortCol_0#">
						<cfcase value="2">
							ORDER BY OptIn_dt #sSortDir_0#
						</cfcase>
						<cfcase value="3">
							ORDER BY OptOut_dt #sSortDir_0#
						</cfcase>
					</cfswitch>
					</cfoutput>
				</cfif>
				LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
			</cfquery>

			<cfset var stt = iDisplayStart>
			<cfloop query="contactList">
 				<cfset var data = [
					++stt,
					ContactString_vch,
					(OptIn_dt NEQ '' ? DateFormat(OptIn_dt, 'mm/dd/yyyy ') & TimeFormat(OptIn_dt, 'hh:mm tt') : DateFormat(Created_dt, 'mm/dd/yyyy ') & TimeFormat(Created_dt, 'hh:mm tt')),
					(OptInOut_int EQ 0 ? DateFormat(OptOut_dt, 'mm/dd/yyyy ') & TimeFormat(OptOut_dt, 'hh:mm tt') : ''),
					'<a href="/session/sire/pages/subscriber-edit?id=' & ContactId_bi & '">
						<img width="19" src="/session/sire/images/icon/icon_search_small.png" class="subscriber-detail-icon" /></a>' &
						' <a href="##" class="glyphicon glyphicon-remove subscriber-delete-icon" data-contact-type="' & ContactType_int &'" aria-hidden="true" data-contact-id="' &
						ContactId_bi & '" data-contact-group-id="' & GroupId_bi & '"></a>'
				]>
				<cfset arrayappend(dataout["aaData"],data)>
			</cfloop>
			<cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
			<cfset dataout["aaData"] = ArrayNew(1)>
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>
		<cfreturn dataOut>
	</cffunction>

	<cffunction name="GetGroupDetailsForDatatableNew" access="remote" output="true" returnFormat="jSon" hint="Get group details for datatable new">
		<cfargument name="sColumns" default="" />
		<cfargument name="iDisplayStart" TYPE="numeric" required="false" default="1">
		<cfargument name="iDisplayLength" TYPE="numeric" required="false" default="20">
		<cfargument name="iSortCol_0" type="numeric" required="false" default="0">
		<cfargument name="sSortDir_0" type="string" required="false" default="asc">
		<cfargument name="iSortCol_1" type="numeric" required="false" default="0">
		<cfargument name="sSortDir_1" type="string" required="false" default="asc">
		<cfargument name="iSortCol_2" type="numeric" required="false" default="0">
		<cfargument name="sSortDir_2" type="string" required="false" default="asc">
		<cfargument name="iSortCol_3" type="numeric" required="false" default="0">
		<cfargument name="sSortDir_3" type="string" required="false" default="asc">
		<cfargument name="iSortCol_4" type="numeric" required="false" default="0">
		<cfargument name="sSortDir_4" type="string" required="false" default="asc">
		<cfargument name="iSortingCols" type="numeric" required="false" default="1">
		<cfargument name="bSortable_0" type="boolean" required="false" default=false>
		<cfargument name="bSortable_1" type="boolean" required="false" default=false>
		<cfargument name="bSortable_2" type="boolean" required="false" default=true>
		<cfargument name="bSortable_3" type="boolean" required="false" default=true>
		<cfargument name="bSortable_4" type="boolean" required="false" default=false>
		<cfargument name="customFilter" default="">
		<cfargument name="INPGROUPID" default="0">
		<cfargument name="inpKeyword" default="" type="string" />
		<cfargument name="sendType" default="0">
		<cfargument name="toSend" type="boolean" default="true">
		<cfargument name="shortCode" required="true" type="string" default="">

		<cfset var filterItem	= '' />
		<cfset var contactList	= '' />
		<cfset var INPGROUPID	= arguments.INPGROUPID />
		<cfset var selectGroupList = '' />
		<cfset var rxContactList = ''/>

		<cfset var dataOut = {}>
		<cfset dataout.RXRESULTCODE = -1>
	    <cfset dataout.MESSAGE = "">
		<cfset dataout.ERRMESSAGE = "">
		<cfset dataout["DATALIST"] = []>
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
		<cfset var filterFields = {
			"CONTACTSTRING_VCH" = "simplelists.contactstring.ContactString_vch",
			"OPTINOUT_INT" = "OptInOut_int"
		}>

		<cfif INPGROUPID EQ 0 ||  INPGROUPID EQ "" >
			<cfreturn dataOut/>
		</cfif>

		<cftry>
			<!---get data here --->
			<cfset var contactCount = "">

			<cfquery name="contactCount" datasource="#Session.DBSourceREAD#">
				SELECT
					count(Distinct(simplelists.contactstring.ContactString_vch)) as TotalRecords
				FROM
					simplelists.groupcontactlist
					INNER JOIN simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
					INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi
				WHERE
					simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
				<cfif INPGROUPID GT 0>
				AND
					simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">
				</cfif>
				AND
					simplelists.contactstring.ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.contactstring INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi WHERE UserId_int = 50)
				AND
					simplelists.contactstring.ContactType_int =3
	            <cfif customFilter NEQ "">
	            	<cfoutput>
		            	<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
		            		<!---
	            			<cfif filterItem.NAME EQ "OPTINOUT_INT" AND filterItem.VALUE GE 0>
		            			<cfswitch expression="#filterItem.OPERATOR#">
		            				<cfcase value="=">
		            					<cfif filterItem.VALUE EQ 0>
		            						AND IO.OptOut_dt IS NOT NULL
		            					<cfelseif filterItem.VALUE EQ 1>
		            						AND OptIn_dt IS NOT NULL AND OptOut_dt IS NULL
		            					</cfif>
		            				</cfcase>
		            			</cfswitch>
		            		</cfif>
		            		--->
		            		<cfif filterItem.NAME EQ "CONTACTSTRING_VCH" AND Trim(filterItem.VALUE) NEQ "">
		            			<cfswitch expression="#filterItem.OPERATOR#">
		            				<cfcase value="LIKE">
		            					AND #filterFields[filterItem.NAME]# LIKE <CFQUERYPARAM CFSQLTYPE="#filterItem.TYPE#" VALUE="%#filterItem.VALUE#%">
		            				</cfcase>
		            			</cfswitch>
			            	</cfif>
		            	</cfloop>
	            	</cfoutput>
            	</cfif>
            	<!--- GROUP BY ContactString_vch --->
            	<!--- GROUP BY simplelists.contactstring.ContactString_vch DESC --->
            	ORDER BY simplelists.contactstring.OptIn_dt DESC
	        </cfquery>


			<cfset dataout["iTotalRecords"] = (contactCount.TotalRecords GTE 1 ? contactCount.TotalRecords : 0)>

			<cfif !isNumeric(dataout["iTotalRecords"])>
				<cfset dataout["iTotalRecords"] = 0>
			</cfif>
			<cfset dataout["iTotalDisplayRecords"] = dataout["iTotalRecords"]>

			<cfset var GetNumbers = "">
			<cfquery name="contactList" datasource="#Session.DBSourceREAD#" result="rxContactList">
				SELECT
					simplelists.contactstring.ContactString_vch, simplelists.contactstring.ContactId_bi, simplelists.contactstring.OptIn_dt, simplelists.contactstring.OptOut_dt,
					simplelists.contactstring.ContactType_int, simplelists.contactstring.Created_dt, simplelists.groupcontactlist.groupid_bi
				FROM
					simplelists.groupcontactlist
					INNER JOIN simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
					INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi
				WHERE
					simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
				<cfif INPGROUPID GT 0>
				AND
					simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">
				</cfif>
				AND
					simplelists.contactstring.ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.contactstring INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi WHERE UserId_int = 50)
				AND
					simplelists.contactstring.ContactType_int =3

	            <cfif customFilter NEQ "">
	            	<cfoutput>
		            	<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
		            		<cfif filterItem.NAME EQ "CONTACTSTRING_VCH" AND Trim(filterItem.VALUE) NEQ "">
		            			<cfswitch expression="#filterItem.OPERATOR#">
		            				<cfcase value="LIKE">
		            					AND #filterFields[filterItem.NAME]# LIKE <CFQUERYPARAM CFSQLTYPE="#filterItem.TYPE#" VALUE="%#filterItem.VALUE#%">
		            				</cfcase>
		            			</cfswitch>
			            	</cfif>
		            	</cfloop>
	            	</cfoutput>
            	</cfif>
            	<cfif arguments.inpKeyword NEQ "">
	            	AND simplelists.contactstring.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpKeyword#%">
            	</cfif>
				GROUP BY simplelists.contactstring.ContactString_vch
				<cfif iSortingCols GT 0>
					<cfoutput>
					<cfswitch expression="#iSortCol_0#">
						<cfcase value="2">
							ORDER BY simplelists.contactstring.OptIn_dt #sSortDir_0#
						</cfcase>
						<cfcase value="3">
							ORDER BY simplelists.contactstring.OptOut_dt #sSortDir_0#
						</cfcase>
					</cfswitch>
					</cfoutput>
				</cfif>
				LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
			</cfquery>

			<cfset var stt = iDisplayStart>
			<cfloop query="contactList">
 				<cfset var data = {
					stt = ++stt,
					Id = ContactId_bi,
					ContactType = ContactType_int,
					Contact = ContactString_vch,
					OptIn = (OptIn_dt NEQ '' ? DateFormat(OptIn_dt, 'mm/dd/yyyy ') & TimeFormat(OptIn_dt, 'hh:mm tt') : DateFormat(Created_dt, 'mm/dd/yyyy ') & TimeFormat(Created_dt, 'hh:mm tt')),
					Optout = (OptOut_dt NEQ '' ? DateFormat(OptOut_dt, 'mm/dd/yyyy ') & TimeFormat(OptOut_dt, 'hh:mm tt') : ''),
					GroupId = GroupId_bi
				} />
				<cfset dataout["DATALIST"].append(data) />
			</cfloop>
			<cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
			<cfset dataout["aaData"] = ArrayNew(1)>
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>

		<cfreturn dataOut>
	</cffunction>

	<cffunction name="GetSubscriber" output="false" hint="Get subscriber by subscriberid">
	 	<cfargument name="subscriberId" type="numeric" required="true">
	 	<cfargument name="userID" type="numeric" required="false" default="#Session.UserId#">

	 	<cfset var subscriber	= '' />

	 	<cfquery name="subscriber" datasource="#Session.DBSourceREAD#">
			SELECT CS.ContactAddressId_bi, CS.ContactId_bi, CS.ContactString_vch,
				BIT_OR(IF(IO.OptOut_dt IS NOT NULL, 0, 1)) AS OptInOut_int,
				MAX(IO.OptIn_dt) AS OptIn_dt, MAX(IO.OptOut_dt) AS OptOut_dt,
				IO.BatchId_bi, OperatorId_int,CS.OptIn_dt, CS.LastUpdated_dt, CS.ContactType_int
			FROM simplelists.contactstring AS CS
			<cfif arguments.userID GT 0>
			INNER JOIN simplelists.groupcontactlist GC
				ON CS.ContactAddressId_bi = GC.ContactAddressId_bi
			INNER JOIN simplelists.grouplist GL
				ON GL.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.userID#">
				AND GC.GroupId_bi = GL.GroupId_bi
			</cfif>
			LEFT JOIN simplelists.optinout AS IO
				ON (IO.OptOut_dt IS NOT NULL
				OR IO.BatchId_bi IN (SELECT BatchId_bi FROM simpleobjects.batch WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">)
				OR IO.ContactString_vch IN (SELECT ContactString_vch FROM simplexresults.cppx_tracking WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">))
				AND CS.ContactType_int IN (3,4)
				AND CS.ContactString_vch = IO.ContactString_vch
			WHERE CS.ContactId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.subscriberId#">
			GROUP BY CS.ContactAddressId_bi
	 	</cfquery>

	 	<cfreturn subscriber>
	</cffunction>

	<cffunction name="getSubscriber4Edit" output="false" hint="get contact data for edit form">
		<cfargument name="inpContactId" type="numeric" required="true">
		<cfargument name="inpUserID" type="numeric" required="false" default="#Session.UserId#">

		<cfset var GetContactStringData = '' />
		<cfset var ListContactStrings = '' />
		<cfset var GetCustomFieldsData = '' />
		<cfset var GetCustomFields = '' />
		<cfset var GetListDefaultValuesCDFOfUser = '' />

		<cfset var optinLastest = '' />
		<cfset var optoutLastest = '' />
		<cfset var getKeyword = {'Keyword_vch': ''} />
		<cfset var carrierQuery = '' />
		<cfset var subscriber = '' />

		<cfquery name="GetContactStringData" datasource="#Session.DBSourceREAD#">
			SELECT
				CL.ContactId_bi,
				CL.UserId_int,
				CL.Company_vch,
				CL.FirstName_vch,
				CL.LastName_vch,
				CL.Address_vch,
				CL.Address1_vch,
				CL.City_vch,
				CL.State_vch,
				CL.ZipCode_vch,
				CL.Country_vch,
				CL.UserName_vch,
				CL.Password_vch,
				CL.UserDefinedKey_vch,
				CL.Created_dt,
				-- CL.LastUpdated_dt,
				CL.DataTrack_vch,
				CL.CPPID_vch,
				CL.CPP_UUID_vch,

				CS.ContactAddressId_bi,
				CS.ContactString_vch,
				CS.OperatorId_int,
				CS.OptIn_dt,
				CS.LastUpdated_dt,
				CS.ContactType_int,

				BIT_OR(IF(IO.OptOut_dt IS NOT NULL, 0, 1)) AS OptInOut_int,
				MAX(IO.OptIn_dt) AS OptIn_dt,
				MAX(IO.OptOut_dt) AS OptOut_dt,
				IO.BatchId_bi

			FROM
				simplelists.contactlist AS CL

			INNER JOIN
				simplelists.contactstring AS CS
			ON
				CL.contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpContactId#">
			AND
				CL.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">
			AND
				CS.contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpContactId#">
			AND
				CL.contactid_bi = CS.contactid_bi

			LEFT JOIN
				simplelists.optinout AS IO
			ON
				(
					IO.OptOut_dt IS NOT NULL
				OR
					IO.BatchId_bi IN (SELECT BatchId_bi FROM simpleobjects.batch WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">)
				OR
					IO.ContactString_vch IN (SELECT ContactString_vch FROM simplexresults.cppx_tracking WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">)
				)
			AND
				CS.ContactType_int IN (3,4)
			AND
				CS.ContactString_vch = IO.ContactString_vch

			GROUP BY CS.ContactAddressId_bi

			LIMIT 1
		</cfquery>

		<cfquery name="ListContactStrings" datasource="#Session.DBSourceREAD#">
			SELECT
				ContactAddressId_bi,
				ContactString_vch,
				ContactType_int,
				TimeZone_int

			FROM
				simplelists.contactstring

			WHERE
				contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpContactId#">
		</cfquery>

		<cfquery name="GetCustomFieldsData" datasource="#Session.DBSourceREAD#">
			SELECT
				VariableName_vch,
				VariableValue_vch,
				CdfId_int
			FROM
				simplelists.contactvariable
			WHERE
				contactid_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#GetContactStringData.ContactId_bi#">
			AND
				UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserID#">
		</cfquery>

		<cfquery name="GetCustomFields" datasource="#Session.DBSourceREAD#">
			SELECT
				CdfId_int,
				CdfName_vch as VariableName_vch,
				CdfDefaultValue_vch,
				type_field
			FROM
				simplelists.customdefinedfields
			WHERE
				userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserID#">
			ORDER BY
				Created_dt ASC,
				CdfId_int ASC
		</cfquery>

		<cfquery name="GetListDefaultValuesCDFOfUser" datasource="#Session.DBSourceREAD#">
			SELECT
				df.id,
				cdf.CdfId_int,
				df.default_value
			FROM
				simplelists.customdefinedfields as cdf
			LEFT JOIN
				simplelists.default_value_custom_defined_fields as df
			ON
				cdf.CdfId_int = df.CdfId_int
			WHERE
				cdf.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserID#">
			AND
				cdf.type_field = 1
		</cfquery>

		<!--- <cfquery name="optinLastest" datasource="#Session.DBSourceREAD#">
			SELECT
				BatchId_bi,
				OptIn_dt

			FROM
				(
					(
						SELECT
							OT.BatchId_bi, OptIn_dt
						FROM
							simplelists.optinout OT
						INNER JOIN
							simpleobjects.batch BA
						ON
							OT.BatchId_bi = BA.BatchId_bi
						WHERE
							OT.ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetContactStringData.ContactString_vch#">
						AND
							OT.OptInSource_vch = 'MO Confirm Opt In'
						AND
							BA.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">
						ORDER BY
							OptId_int DESC
					)
					UNION
					(
						SELECT
							0 AS BatchId_bi, Created_dt AS OptIn_dt
						FROM
							simplexresults.cppx_tracking CPPX
						WHERE
							CPPX.ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetContactStringData.ContactString_vch#">
						AND
							CPPX.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">
						ORDER BY
							CPPX_trackingId_int DESC LIMIT 1
					)
				) AS optinLastest

			ORDER BY
				OptIn_dt DESC

			LIMIT 1
		</cfquery>

		<cfquery name="optoutLastest" datasource="#Session.DBSourceREAD#">
			SELECT
				OT.BatchId_bi,
				OT.OptOut_dt

			FROM
				simplelists.optinout AS OT

			WHERE
				OT.ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetContactStringData.ContactString_vch#">
			AND
				OT.OptOutSource_vch = 'MO Stop Request'
			<cfif optinLastest.OptIn_dt NEQ "" >
				AND
					OT.OptOut_dt > #optinLastest.OptIn_dt#
			</cfif>

			LIMIT 1
		</cfquery>

		<cfif optinLastest.BatchId_bi GT 0>
			<cfquery name="getKeyword" datasource="#Session.DBSourceREAD#">
				SELECT
					Keyword_vch
				FROM
					sms.keyword
				WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#optinLastest.BatchId_bi#">
				AND
					Active_int = 1
				LIMIT 1
			</cfquery>
		</cfif> --->

		<cfquery name="carrierQuery" datasource="#Session.DBSourceREAD#">
			SELECT
				Operator_vch
			FROM
				sms.carrierlist
			WHERE
				OperatorId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetContactStringData.OperatorId_int#">
			LIMIT 1
		</cfquery>


		<cfreturn {
			"GetContactStringData": GetContactStringData,
			"ListContactStrings": ListContactStrings,
			"GetCustomFieldsData": GetCustomFieldsData,
			"GetCustomFields": GetCustomFields,
			"GetListDefaultValuesCDFOfUser": GetListDefaultValuesCDFOfUser,
			"optinLastest": optinLastest,
			"optoutLastest": optoutLastest,
			"Keyword": getKeyword.Keyword_vch,
			"carrierQuery": carrierQuery

		} />


		<!---
		<cfquery name="subscriber" datasource="#Session.DBSourceREAD#">
			SELECT CS.ContactAddressId_bi, CS.ContactId_bi, CS.ContactString_vch,
			BIT_OR(IF(IO.OptOut_dt IS NOT NULL, 0, 1)) AS OptInOut_int,
			MAX(IO.OptIn_dt) AS OptIn_dt, MAX(IO.OptOut_dt) AS OptOut_dt,
			IO.BatchId_bi, OperatorId_int,CS.OptIn_dt, CS.LastUpdated_dt, CS.ContactType_int
			FROM simplelists.contactstring AS CS
			<cfif arguments.userID GT 0>
				INNER JOIN simplelists.groupcontactlist GC
				ON CS.ContactAddressId_bi = GC.ContactAddressId_bi
				INNER JOIN simplelists.grouplist GL
				ON GL.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">
				AND GC.GroupId_bi = GL.GroupId_bi
			</cfif>
			LEFT JOIN simplelists.optinout AS IO
			ON (IO.OptOut_dt IS NOT NULL
				OR IO.BatchId_bi IN (SELECT BatchId_bi FROM simpleobjects.batch WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">)
				OR IO.ContactString_vch IN (SELECT ContactString_vch FROM simplexresults.cppx_tracking WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">))
			AND CS.ContactType_int IN (3,4)
			AND CS.ContactString_vch = IO.ContactString_vch
			WHERE CS.ContactId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpContactId#">
			GROUP BY CS.ContactAddressId_bi
		</cfquery>

		<cfreturn subscriber>
		 --->
	</cffunction>

	<cffunction name="getCustomFieldData" hint="Get custom field data">
		<cfargument name="customFieldsData" type="query" required="true">
		<cfargument name="fieldName" type="string" required="true">
		<cfif customFieldsData.RecordCount GT 0>
			<cfloop query="customFieldsData">
				<cfif CdfName_vch EQ fieldName>
					<cfreturn {'id' = (isNumeric(ContactVariableId_bi) ? ContactVariableId_bi : 0), 'value' = VariableValue_vch}>
				</cfif>
			</cfloop>
		</cfif>
		<cfreturn {'id' = 0, 'value' = ''}>
	</cffunction>

	<cffunction name="updateCustomField" access="remote" output="false" returnFormat="jSon" hint="Update custom field">
		<cfargument name="contactId" type="numeric" required="true">
		<cfargument name="fieldId" type="numeric" required="false" default="0">
		<cfargument name="fieldIndex" type="numeric" required="true">
		<cfargument name="fieldValue" type="string" required="true">

		<cfset var data = {'status' = 0, 'msg' = "We're Sorry! Unable to complete save #_UserCustomFields[fieldIndex]# at this time."}>
		<cfset var customFieldValue	= '' />
		<cfset var customDefinedField	= '' />
		<!--- Add try --->

		<cfif fieldId GT 0>
			<cfquery name="customFieldValue" datasource="#Session.DBSourceREAD#">
				select CV.ContactVariableId_bi
				from simplelists.contactvariable as CV
				where CV.ContactVariableId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#fieldId#">
					and CV.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">
			</cfquery>

			<cfif customFieldValue.RecordCount GT 0>
				<cfquery name="customFieldValue" datasource="#Session.DBSourceEBM#">
					update simplelists.contactvariable as CV
					set CV.VariableValue_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(fieldValue,5000)#">
					where CV.ContactVariableId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#fieldId#">
						and CV.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">
				</cfquery>
				<cfset data = {'status' = 1, 'cfvId' = fieldId}>
				<cfreturn data>
			</cfif>
		</cfif>

		<cfif arrayIsDefined(_UserCustomFields, fieldIndex)>
			<cfquery name="customDefinedField" datasource="#Session.DBSourceREAD#">
				select CF.CdfId_int, CF.UserId_int, CF.CdfName_vch
				from simplelists.customdefinedfields as CF
		        where CF.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">
		        	and CF.CdfName_vch = <CFQUERYPARAM cfsqltype="cf_sql_varchar" value="#_UserCustomFields[fieldIndex]#">
			</cfquery>

			<cfif customDefinedField.RecordCount GT 0>
				<cfset var cfId = customDefinedField.CdfId_int>
				<cfset var cfUserId = customDefinedField.UserId_int>
				<cfset var cfName = customDefinedField.CdfName_vch>
			<cfelse>
				<cfquery name="customDefinedField" result="customDefinedField" datasource="#Session.DBSourceEBM#">
					insert into simplelists.customdefinedfields (
						CdfName_vch,
						UserId_int,
						CdfDefaultValue_vch,
						Created_dt,
						type_field
					) values (
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#_UserCustomFields[fieldIndex]#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userId#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="">,
						now(),
						0
					)
				</cfquery>
				<cfset var cfId = customDefinedField["GENERATEDKEY"]>
				<cfset var cfUserId = session.userId>
				<cfset var cfName = _UserCustomFields[fieldIndex]>
			</cfif>

			<cfquery name="customFieldValue" result="customFieldValue" datasource="#Session.DBSourceEBM#">
				insert into simplelists.contactvariable (
					UserId_int,
					ContactId_bi,
					VariableName_vch,
					VariableValue_vch,
					Created_dt,
					CdfId_int
				) values (
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#cfUserId#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#contactId#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#cfName#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(fieldValue,5000)#">,
					now(),
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#cfId#">
				)
			</cfquery>
			<cfset data = {'status' = 1, 'cfvId' = customFieldValue["GENERATEDKEY"]}>
		</cfif>

		<cfreturn data>
	</cffunction>

	<cffunction name="updateCFV" access="remote" output="false" hint="Update custom data field for current user's customer">
		<cfargument name="contactId" type="numeric" required="true">
		<cfargument name="fieldId" type="numeric" required="true">
		<cfargument name="fieldValue" type="string" required="true">

		<cfset var data = {'status' = 0, 'msg' = "We're Sorry! Unable to complete save at this time."}/>
		<cfset var contactVariable = ''/>
		<cfset var getCustomField = ''/>

		<cftry>

			<cfquery name="contactVariable" datasource="#Session.DBSourceREAD#">
				SELECT
					COUNT(CdfId_int) AS TotalCount
				FROM
					simplelists.contactvariable
				WHERE
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
				AND
					ContactId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.contactId#">
				AND
					CdfId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.fieldId#">
			</cfquery>

			<cfif contactVariable.TotalCount GT 0>
				<cfquery name="contactVariable" datasource="#Session.DBSourceEBM#">
					UPDATE
						simplelists.contactvariable
					SET
						VariableValue_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(arguments.fieldValue, 5000)#" null="#IIF(TRIM(arguments.fieldValue) EQ "", true, false)#">,
						Created_dt = NOW()
					WHERE
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
					AND
						ContactId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.contactId#">
					AND
						CdfId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.fieldId#">
				</cfquery>

			<cfelse>
				<cfquery name="getCustomField" datasource="#Session.DBSourceEBM#">
					SELECT
						CdfId_int,
						CdfName_vch as VariableName_vch,
						CdfDefaultValue_vch,
						type_field
					FROM
						simplelists.customdefinedfields
					WHERE
						CdfId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.fieldId#">
					AND
						UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
				</cfquery>

				<cfif getCustomField.RecordCount GT 0>
					<cfquery name="contactVariable" datasource="#Session.DBSourceEBM#">
						INSERT INTO
							simplelists.contactvariable
							(
								ContactId_bi,
								VariableName_vch,
								VariableValue_vch,
								UserId_int,
								Created_dt,
								CdfId_int
							)
						VALUES
							(
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.contactId#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getCustomField.VariableName_vch#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(arguments.fieldValue, 5000)#" null="#IIF(TRIM(arguments.fieldValue) EQ "", true, false)#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
								NOW(),
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.fieldId#">
							)
					</cfquery>

				</cfif>

			</cfif>

			<cfset data = {'status' = 1}>

			<cfcatch type="Any">
				<cfset data.status = -1 />
				<cfset data.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset data.ERRMESSAGE = "#cfcatch.detail#"/>
			</cfcatch>

        </cftry>

		<cfreturn data>
	</cffunction>

	<cffunction name="unsubscribe" access="remote" output="true" returnFormat="jSon" hint="unsubscribe">
		<cfargument name="contactString" type="string" required="true">

		<cfset var shortCode	= '' />
		<cfset var OptOut	= '' />
		<cfset var defaultShortCode	= '' />

		<!--- remove old method:
		<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="defaultShortCode"/>
		--->
		<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="defaultShortCode"></cfinvoke>
		<!---<cfinvoke component="session.cfc.csc.csc" method="GetSMSResponse" returnvariable="OptOut">
			<cfinvokeargument name="inpKeyword" value="STOP">
			<cfinvokeargument name="inpShortCode" value="#shortCode.SHORTCODE#">
			<cfinvokeargument name="inpContactString" value="#arguments.contactString#">
		</cfinvoke>--->

		<cfinvoke component="session.cfc.csc.csc" method="ProcessStopRequest" returnvariable="OptOut">
			<cfinvokeargument name="inpShortCode" value="#defaultShortCode.SHORTCODE#">
			<cfinvokeargument name="inpContactString" value="#arguments.contactString#">
		</cfinvoke>

		<cfset var data = {'status' = OptOut.RXRESULTCODE, 'msg' = OptOut.MESSAGE, 'optOutDate' = dateFormat(now(), "medium")}>
		<cfreturn data>
	</cffunction>

	<cffunction name="GetSubscriberHistoryForDatatable" access="remote" output="true" returnFormat="JSON" hint="Get subscriber history for datatable">
		<cfargument name="sColumns" default="" />
		<cfargument name="iDisplayStart" TYPE="numeric" required="false" default="1">
		<cfargument name="iDisplayLength" TYPE="numeric" required="false" default="20">
		<cfargument name="iSortCol_0" type="numeric" required="false" default="0">
		<cfargument name="sSortDir_0" type="string" required="false" default="asc">
		<cfargument name="iSortingCols" type="numeric" required="false" default="1">
		<cfargument name="bSortable_0" type="boolean" required="false" default=false>
		<cfargument name="bSortable_1" type="boolean" required="false" default=false>
		<cfargument name="bSortable_2" type="boolean" required="false" default=true>
		<cfargument name="bSortable_3" type="boolean" required="false" default=true>
		<cfargument name="bSortable_4" type="boolean" required="false" default=false>
		<cfargument name="customFilter" default="">
		<cfargument name="ContactString" default="">
		<cfargument name="ContactType" default="3" required="false">

		<cfparam name="URL.ContactString" type="string" default="#arguments.ContactString#">

		<cfset var batchIds	= '' />
		<cfset var filterItem	= '' />
		<cfset var campaignBatchIds	= '' />
		<cfset var historyList	= '' />
		<cfset var ContactString = URL.ContactString />

		<cfset var dataOut = {}>
		<cfset dataout.RXRESULTCODE = -1>
	    <cfset dataout.MESSAGE = "">
		<cfset dataout.ERRMESSAGE = "">
		<cfset dataout["aaData"] = ArrayNew(1)>
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
		<cfset dataout.ContactString = "#ContactString#">
		<cfset var customFilterStruct = '' />
		<cfset var cppDisplay = '' />
		<cfset var direction = '' />

		<cftry>
			<cfquery name="campaignBatchIds" datasource="#Session.DBSourceREAD#">
				SELECT BatchId_bi FROM simpleobjects.batch WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">;
			</cfquery>

			<cfif campaignBatchIds.RecordCount GT 0>
				<cfset batchIds = valueList(campaignBatchIds.BatchId_bi,',')>
			<cfelse>
				<cfset batchIds = '-1'>
			</cfif>

			<cfset customFilterStruct = deserializeJSON(customFilter)/>

			<cfset cppDisplay = 0/>

			<cfif customFilter EQ '' OR (customFilter NEQ '' && customFilterStruct[1].VALUE EQ 0)>
				<cfset cppDisplay = 1/>
			</cfif>
			<cfif (customFilter NEQ '' && customFilterStruct[1].VALUE EQ -999)>
				<cfset cppDisplay = 2/>
			</cfif>

			<cfset var historyCount = "">
			<cfquery name="historyCount" datasource="#Session.DBSourceREAD#">
				SELECT COUNT(id) AS TOTALCOUNT
				FROM (

					<cfif cppDisplay NEQ 2>
						SELECT IREResultsId_bi AS Id
						FROM simplexresults.ireresults
						WHERE (UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
								<cfif customFilter EQ '' OR (customFilter NEQ '' && customFilterStruct[1].VALUE EQ 0)>
									OR (UserId_int IS NULL AND LOWER(Response_vch) = 'stop')
								</cfif>
							 )
							AND IREType_int IN (1,2)
							AND (
							<cfif customFilter NEQ "" AND isJSON(customFilter)>
				            	<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
				            		<cfswitch expression="#filterItem.NAME#" >
				            			<cfcase value="BatchId">
				            				<cfif isNumeric(filterItem.VALUE) AND filterItem.VALUE GT 0>
				            					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#filterItem.VALUE#">
			            					<cfelse>
			            						BatchId_bi IN (#batchIds#)
				            				</cfif>
				            			</cfcase>
				            		</cfswitch>
				            	</cfloop>
			            	<cfelse>
			            		<cfoutput>
			            			BatchId_bi IN (#batchIds#)
			            		</cfoutput>
			            	</cfif>
			            	<cfif customFilter EQ '' OR (customFilter NEQ '' && customFilterStruct[1].VALUE EQ 0)>
			            	OR (UserId_int IS NULL AND LOWER(Response_vch) = 'stop')
			            	</cfif>
			            	)
							AND ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ContactString#">
							<cfif ContactType EQ 3 >
								AND LENGTH(ContactString_vch) < 14
							</cfif>
			        </cfif>

					<cfif cppDisplay EQ 1>
						UNION ALL
					</cfif>
					<cfif cppDisplay EQ 1 OR cppDisplay EQ 2>
						SELECT CPPX_trackingId_int AS Id
						FROM simplexresults.cppx_tracking
						WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
							AND ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ContactString#">
							<cfif ContactType EQ 3 >
								AND LENGTH(ContactString_vch) < 14
							</cfif>
					</cfif>
				) history

			</cfquery>

			<cfset dataout["iTotalRecords"] = (historyCount.RecordCount GT 1 ? historyCount.RecordCount : historyCount.TOTALCOUNT)>
			<cfset dataout["iTotalDisplayRecords"] = dataout["iTotalRecords"]>

			<cfif dataout["iTotalRecords"] GT 0>
				<cfquery name="historyList" datasource="#Session.DBSourceREAD#">
					SELECT * FROM (

					<cfif cppDisplay NEQ 2>
						SELECT Created_dt, IREType_int, MasterRXCallDetailId_bi, Response_vch
						FROM simplexresults.ireresults
						WHERE (UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
						<cfif customFilter EQ '' OR (customFilter NEQ '' && customFilterStruct[1].VALUE EQ 0)>
						 OR (UserId_int IS NULL AND LOWER(Response_vch) = 'stop')
						</cfif>
						)
							AND IREType_int IN (1,2)
							AND (
							<cfif customFilter NEQ "" AND isJSON(customFilter)>
				            	<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
				            		<cfswitch expression="#filterItem.NAME#" >
				            			<cfcase value="BatchId">
				            				<cfif isNumeric(filterItem.VALUE) AND filterItem.VALUE GT 0>
				            					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#filterItem.VALUE#">
			            					<cfelse>
			            						BatchId_bi IN (#batchIds#)
				            				</cfif>
				            			</cfcase>
				            		</cfswitch>
				            	</cfloop>
			            	<cfelse>
			            		<cfoutput>
			            			BatchId_bi IN (#batchIds#)
			            		</cfoutput>
			            	</cfif>
			            	<cfif customFilter EQ '' OR (customFilter NEQ '' && customFilterStruct[1].VALUE EQ 0)>
			            	OR (UserId_int IS NULL AND LOWER(Response_vch) = 'stop')
			            	</cfif>
			            	)
							AND ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ContactString#">
							<cfif ContactType EQ 3 >
								AND LENGTH(ContactString_vch) < 14
							</cfif>
			       	</cfif>

					<cfif cppDisplay EQ 1>
						UNION ALL
					</cfif>
					<cfif cppDisplay EQ 1 OR cppDisplay EQ 2>
						SELECT Created_dt, 999 AS IREType_int, '' AS MasterRXCallDetailId_bi, Subject_vch AS Response_vch
						FROM simplexresults.cppx_tracking
						WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
							 AND ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ContactString#">
							 <cfif ContactType EQ 3 >
								AND LENGTH(ContactString_vch) < 14
							</cfif>
					</cfif>
					) AS history
					ORDER BY Created_dt DESC
					LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
				</cfquery>


				<cfset var stt = iDisplayStart>
				<cfloop query="historyList">
					<cfset direction = ''>
					<cfif IREType_int EQ 1>
						<cfset direction = 'OutBound<span class="HistoryOutBound"></span>'/>
					<cfelseif  IREType_int EQ 2>
						<cfset direction = 'InBound<span class="HistoryInBound"></span>'/>
					<cfelseif IREType_int EQ 999>
						<cfset direction = 'InBound (CPP)<span class="HistoryInBound HistoryCPP"></span>'/>
					</cfif>
	 				<cfset var data = [
						++stt,
						DateFormat(Created_dt, 'mm/dd/yyyy ') & TimeFormat(Created_dt, 'hh:mm tt'),
						direction,
						Response_vch,
						'Success'
					]>
					<cfset arrayappend(dataout["aaData"],data)>
				</cfloop>

			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
			<cfset dataout["aaData"] = ArrayNew(1)>
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>
		<cfreturn dataOut>
	</cffunction>

	<cffunction name="BindCountQueryForDatatable" output="true" hint="bind count query for datatable">
		<cfset var condition = 'SELECT simplelists.groupcontactlist.groupid_bi,
			sum( case when simplelists.contactstring.ContactType_int = 3 then 1 else 0 end) as smsNumber
			FROM simplelists.groupcontactlist
	        INNER JOIN simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
	        INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi '
			>

			<!--- <cfset condition = condition & "JOIN simpleobjects.useraccount AS u ">
			<cfset condition = condition & "ON simplelists.contactlist.userid_int = u.UserId_int "> --->

			<cfset condition = condition & "WHERE ">
			<cfset condition = condition & "simplelists.contactlist.userid_int = #Session.USERID# ">

		<cfset condition = condition & " group by simplelists.groupcontactlist.groupid_bi">
		<cfreturn condition>
	</cffunction>

  	<cffunction name="GetSubcriberList" access="remote" output="true" returnFormat="jSon" hint="get list subscriber">
    	<cfargument name="inpShortCode" type="string" required="true" default="QA2Nowhere">
    	<cfargument name="inpContactTypeId" type="numeric" required="false" default="3">
    	<cfargument name="inpShortCodeId" required="false" default="0"/>

		<cfset var getList	= '' />
		<cfset var DebugStr	= '' />
		<cfset var data	= '' />
		<cfset var filterItem	= '' />
		<cfset var inpSkipDNCCheck = 0/>

    	<cfset var permissionObject = CreateObject("component", "session.cfc.administrator.permission")>
    	<cfset var contactGroupPermission = permissionObject.havePermission(Contact_Group_Title)>
    	<!---
		<cfset var contactPermission = permissionObject.havePermission(Contact_Title)>
		<cfset var contactViewGroupPermission = permissionObject.havePermission(View_Group_Details_Title)>
		<cfset var contactEditGroupPermission = permissionObject.havePermission(edit_Group_Details_Title)>
		<cfset var contactDeleteGroupPermission = permissionObject.havePermission(delete_Group_Title)>
		<cfset var contactAddContactToGroupPermission = permissionObject.havePermission(Add_Contacts_To_Group_Title)>
		<cfset var contactDeleteContactFromGroupPermission = permissionObject.havePermission(delete_Contacts_From_Group_Title)>
		--->

		<cfif NOT contactGroupPermission.havePermission>
			<cfset session.permissionError = contactGroupPermission.message>
			<cflocation url="/session/sire/pages/" addtoken="no">
		</cfif>

		<cfset var dataOut = {}>
		<cfset dataout["listData"] = ArrayNew(1)>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
	    <cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
	    <cfset dataout.TYPE = '' />
	    <cfset var GetGroups = ''/>

	    <cftry>

            <cfquery name="GetGroups" datasource="#Session.DBSourceEBM#">
	            SELECT
					count(simplelists.groupcontactlist.ContactAddressId_bi) as totalSMS, simplelists.grouplist.GroupId_bi, simplelists.grouplist.GroupName_vch
				FROM
					simplelists.grouplist
				LEFT JOIN
					simplelists.groupcontactlist ON simplelists.grouplist.GroupId_bi = simplelists.groupcontactlist.GroupId_bi
				WHERE
					simplelists.grouplist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				<cfif arguments.inpShortCodeId GT 0>
					AND
						simplelists.grouplist.ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpShortCodeId#"/>
				</cfif>
				GROUP BY
					simplelists.grouplist.GroupId_bi
            </cfquery>

            <cfloop query="GetGroups">
				<cfset data = [
					GetGroups.GroupId_bi,
					htmleditformat(GetGroups.GroupName_vch),
					GetGroups.TotalSMS
				]>
				<cfset arrayappend(dataout["listData"],data)>
			</cfloop>

			<cfset dataout["iTotalRecords"] = GetGroups.RECORDCOUNT>
			<cfset dataout["iTotalDisplayRecords"] = GetGroups.RECORDCOUNT>
			<cfset dataout.RXRESULTCODE = 1 />

	        <cfcatch type="Any" >
			    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
	        </cfcatch>
	    </cftry>
	    <cfreturn dataOut>
    </cffunction>
	<cffunction name="GetSubcriberListBringContactTimeZoneNot31" access="remote" output="true" hint="Check Subsciber list have contact timezone not 31">
    	<cfargument name="inpStartTime" required="false" default="9"/>
		<cfargument name="inpEndTime" required="false" default="21"/>
		<cfargument name="inpBlastNow" required="false" default="0"/>
		<cfargument name="inpListGroupID" required="false" type="string" default=""/>

		<cfset var getList	= '' />
		<cfset var DebugStr	= '' />
		<cfset var data	= '' />
		<cfset var filterItem	= '' />
		<cfset var inpSkipDNCCheck = 0/>

    	<cfset var permissionObject = CreateObject("component", "session.cfc.administrator.permission")>
    	<cfset var contactGroupPermission = permissionObject.havePermission(Contact_Group_Title)>

		<cfif NOT contactGroupPermission.havePermission>
			<cfset session.permissionError = contactGroupPermission.message>
			<cflocation url="/session/sire/pages/" addtoken="no">
		</cfif>

		<cfset var dataOut = {}>
		<cfset dataout.RXRESULTCODE = -1 />
	    <cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
	    <cfset dataout.TYPE = '' />
		<cfset dataout.TOTALRECORD = '' />
	    <cfset var contactList = ''/>
		<cfset var GroupContactId = ''/>
		<cfset var IndexLoop = 0/>
		<cfset var getTimewDBNow = 0/>


		<cfset var SchduleStartHour=arguments.inpStartTime>
		<cfset var SchduleEndHour=arguments.inpEndTime>
		<!--- for blast sendnow , we check if 'db time NOW' in range 9am-10pm , set SchduleStartHour= DB time now--->
		<cfif inpBlastNow EQ "1">
			<cfquery name="getTimewDBNow" datasource="#Session.DBSourceREAD#" >
				SELECT EXTRACT(HOUR FROM NOW()) as TimeNow
			</cfquery>
			<cfif getTimewDBNow.TimeNow GT 9 and getTimewDBNow.TimeNow LT 22>
				<cfset var SchduleStartHour=getTimewDBNow.TimeNow>
			</cfif>
		</cfif>

	    <cftry>
			<cfquery name="contactList" datasource="#Session.DBSourceREAD#" >
				SELECT
					simplelists.contactstring.TimeZone_int
				FROM
					simplelists.groupcontactlist
					INNER JOIN simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
					INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi
				WHERE
					simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
				<cfif LISTLEN(inpListGroupID) GT 0>
					AND(
						<cfset var IndexLoop = 0/>
						<cfloop list="#inpListGroupID#" index="GroupContactId">
						<cfset var IndexLoop ++/>
						<cfif IndexLoop	GT 1>
							OR
						</cfif>
								simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GroupContactId#">
						</cfloop>
					)
				</cfif>
				AND
					simplelists.contactstring.ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.contactstring INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi WHERE UserId_int = 50)
				AND
					simplelists.contactstring.ContactType_int =3
				AND
					simplelists.contactstring.TimeZone_int <> 31
				AND
					#SchduleEndHour# < (#SchduleStartHour# - simplelists.contactstring.TimeZone_int + 31)


				GROUP BY simplelists.contactstring.ContactString_vch
			</cfquery>

			<cfset dataout.TOTALRECORD = contactList.RECORDCOUNT />
           	<cfif contactList.RECORDCOUNT gt 0>
				<cfset dataout.RXRESULTCODE = 1 />
			</cfif>

	        <cfcatch type="Any" >
			    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
	        </cfcatch>
	    </cftry>
	    <cfreturn dataOut>
    </cffunction>

	<cffunction name="CheckTimeBlastDbNotbetween9amto8pm" access="remote" output="true" hint="Check time blast db">

		<cfset var getList	= '' />
		<cfset var DebugStr	= '' />
		<cfset var data	= '' />
		<cfset var filterItem	= '' />
		<cfset var inpSkipDNCCheck = 0/>

    	<cfset var permissionObject = CreateObject("component", "session.cfc.administrator.permission")>
    	<cfset var contactGroupPermission = permissionObject.havePermission(Contact_Group_Title)>

		<cfif NOT contactGroupPermission.havePermission>
			<cfset session.permissionError = contactGroupPermission.message>
			<cflocation url="/session/sire/pages/" addtoken="no">
		</cfif>

		<cfset var dataOut = {}>
		<cfset dataout.RXRESULTCODE = -1 />
	    <cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
	    <cfset dataout.HOURDATA = '' />
		<cfset dataout.TOTALRECORD = '' />
	    <cfset var Checktimeblast = ''/>

	    <cftry>
			<cfquery name="Checktimeblast" datasource="#Session.DBSourceREAD#" >
				SELECT HOUR(NOW()) AS DATEDB
			</cfquery>
			<cfset dataout.TOTALRECORD = Checktimeblast.RECORDCOUNT />
			<cfif dataout.TOTALRECORD gt 0>
				<cfset dataout.HOURDATA = Checktimeblast.DATEDB />
				<cfif dataout.HOURDATA lt 9 or dataout.HOURDATA gt 20>
					<cfset dataout.RXRESULTCODE = -1 />
				<cfelse>
					<cfset dataout.RXRESULTCODE =  1 />
				</cfif>
			<cfelse>
				<cfset dataout.RXRESULTCODE =  1 />
			</cfif>

	        <cfcatch type="Any" >
			    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
	        </cfcatch>
	    </cftry>
	    <cfreturn dataOut>
    </cffunction>

	<cffunction name='GetLastestOPTBatch' hint="get Lastest OPT BatchId for a contratstring">
		<cfargument name="ContactString" required="true">
		<cfset var dataout	= {} />
		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.DATA = ''>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset var getLastestBatch = '' />

		<cftry>
			<cfquery name="getLastestBatch" datasource="#Session.DBSourceREAD#">
				SELECT
					BA.BatchId_bi
				FROM
					simpleobjects.batch BA
				JOIN
				  simplelists.optinout AS IO
				ON
					BA.BatchId_bi = IO.BatchId_bi
				WHERE
					BA.UserId_int = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#Session.UserId#">
				AND
					BA.Active_int = 1
				AND
					IO.ContactString_vch = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.ContactString#">
				ORDER BY
					IO.OptId_int DESC
				LIMIT 1
			</cfquery>

			<cfif getLastestBatch.RecordCount GT 0>
				<cfset dataout.RXRESULTCODE = 1>
				<cfset dataout.DATA = getLastestBatch['BatchId_bi'][1]>
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0>
				<cfset dataout.MESSAGE = "Not found" />
			</cfif>
			<cfcatch>
				<cfset dataout = {}>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
				<cfreturn dataout />
			</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction>

	<!---get list group to select in emergency --->
	<cffunction name="GetGroupListForDatatable" access="remote" output="true" returnFormat="jSon" hint="Get group list for datatable">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="customFilter" default="">
		<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_3" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_3" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_4" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_4" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="isCreateEms" default="1" /><!--- this is check request from create ems form or group list page  --->

		<!---check permission--->

		<cfset var permissionObject = CreateObject("component","session.cfc.administrator.permission")>
		<cfset var contactPermission = permissionObject.havePermission(Contact_Title)>
		<cfset var contactGroupPermission = permissionObject.havePermission(Contact_Group_Title)>

		<cfset var order_0 = '' />
		<cfset var order_1 = '' />
		<cfset var order_2 = '' />
		<cfset var order_3 = '' />
		<cfset var order_4 = '' />
		<cfset var data = '' />
		<cfset var filterItem = '' />
		<cfset var addHistory = '' />
		<cfset var groupList = ''/>

		<cfif NOT contactGroupPermission.havePermission>
			<cfset session.permissionError = contactGroupPermission.message>
			<cflocation url="#rootUrl#/#sessionPath#/account/home" addtoken="no">
		</cfif>

		<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
			<cfinvokeargument name="pageTitle" value="#Contact_Group_Text#">
		</cfinvoke>

        <cfset var DebugStr = "" />

		<cfset var dataOut = {}>
		<cfset dataout["aaData"] = ArrayNew(1)>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
	    <cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
	    <cfset dataout.TYPE = '' />
		<cftry>

		   	<!---set order param for query--->
			<cfif sSortDir_1 EQ "asc" OR sSortDir_1 EQ "desc">
				<cfset order_1 = sSortDir_1>
			<cfelse>
				<cfset order_1 = "">
			</cfif>
			<cfif sSortDir_2 EQ "asc" OR sSortDir_2 EQ "desc">
				<cfset order_2 = sSortDir_2>
			<cfelse>
				<cfset order_2 = "">
			</cfif>
			<cfif sSortDir_3 EQ "asc" OR sSortDir_3 EQ "desc">
				<cfset order_3 = sSortDir_3>
			<cfelse>
				<cfset order_3 = "">
			</cfif>
			<cfif sSortDir_4 EQ "asc" OR sSortDir_4 EQ "desc">
				<cfset order_4 = sSortDir_4>
			<cfelse>
				<cfset order_4 = "">
			</cfif>
			<cfif iSortCol_0 EQ 0>
				<cfset order_0 = "desc">
			<cfelse>
				<cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
					<cfset order_0 = sSortDir_0>
				<cfelse>
					<cfset order_0 = "">
				</cfif>
			</cfif>



			<!---get data here --->
			<cfset var GetTotalGroups = "">
			<cfquery name="GetTotalGroups" datasource="#Session.DBSourceREAD#">
	            SELECT
					count(*) AS totalGroups
	            FROM
	            	simplelists.grouplist AS g
				LEFT JOIN
					simplelists.groupcontactlist ON g.GroupId_bi = simplelists.groupcontactlist.GroupId_bi
				WHERE
					g.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				GROUP BY
					g.GroupId_bi
			</cfquery>


            <cfset DebugStr = DebugStr & " GetTotalGroups.totalGroups = #GetTotalGroups.totalGroups#" />

			<cfset var GetGroups = "">
			<cfquery name="GetGroups" datasource="#Session.DBSourceREAD#">
	            SELECT
					g.UserId_int,
	                g.GroupId_bi,
	                g.GroupName_vch,
	                <!--- since request to this function has not came from grouplist (with isCreateEms = 0), fields of contactCount like phoneNumber, mailNumber, smsNumber could be null, so we need to change null string to 0 number  --->
					(case when contactCount.smsNumber is not null then contactCount.smsNumber else 0 end)  as TotalSMS
	            FROM
	            	simplelists.grouplist AS g
	            LEFT JOIN
					(
						<cfoutput>#BindCountQueryFordatatable()#</cfoutput>
					) AS contactCount
				ON contactCount.groupid_bi = g.GroupId_bi
	            WHERE
				g.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				<cfif iSortCol_0 neq -1 and order_0 NEQ "">
					order by
					<cfif iSortCol_0 EQ 0>
						g.GroupId_bi
						<cfelseif iSortCol_0 EQ 1> g.GroupName_vch
						<cfelseif iSortCol_0 EQ 4> TotalSMS
					</cfif> #order_0#
				</cfif>
				<cfif iSortCol_1 neq -1 and order_1 NEQ "">,
					<cfif iSortCol_1 EQ 0>
						g.GroupId_bi
						<cfelseif iSortCol_1 EQ 1> g.GroupName_vch
						<cfelseif iSortCol_1 EQ 4> TotalSMS
					</cfif> #order_1#
				</cfif>
				<cfif iSortCol_2 neq -1 and order_2 NEQ "">,
					<cfif iSortCol_2 EQ 0>
						g.GroupId_bi
						<cfelseif iSortCol_2 EQ 1> g.GroupName_vch
						<cfelseif iSortCol_2 EQ 4> TotalSMS
					</cfif> #order_2#
				</cfif>
				<cfif iSortCol_3 neq -1 and order_3 NEQ "">,
					<cfif iSortCol_3 EQ 0>
						g.GroupId_bi
						<cfelseif iSortCol_3 EQ 1> g.GroupName_vch
						<cfelseif iSortCol_3 EQ 4> TotalSMS
					</cfif> #order_3#
				</cfif>
				<cfif iSortCol_4 neq -1 and order_4 NEQ "">,
					<cfif iSortCol_4 EQ 0>
						g.GroupId_bi
						<cfelseif iSortCol_4 EQ 1> g.GroupName_vch
						<cfelseif iSortCol_4 EQ 4> TotalSMS
					</cfif> #order_4#
				</cfif>
				LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
			</cfquery>

			<cfset dataout["sEcho"] = #sEcho#>
			<cfset dataout["iTotalRecords"] = GetTotalGroups.RECORDCOUNT>
			<cfset dataout["iTotalDisplayRecords"] = GetTotalGroups.RECORDCOUNT>

			<cfloop query="GetGroups">
				<cfset var htmlOptionRow = "">
                <cfset var HTMLGroupName = Replace(GetGroups.GroupName_vch, "'", "`", "ALL") />
				<cfset data = [
					GetGroups.GroupId_bi & '&nbsp;',
					GetGroups.GroupId_bi & '&nbsp;',
					htmleditformat(GetGroups.GroupName_vch) & '&nbsp;',
					0 & '&nbsp;',
					GetGroups.TotalSMS  & '&nbsp;',
					0 & '&nbsp;',
					htmlOptionRow

				]>
				<cfset arrayappend(dataout["aaData"],data)>
			</cfloop>
			<cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >

        	<!-------- #BindCountQueryFordatatable()#--->

			<cfset dataout["aaData"] = ArrayNew(1)>
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail# #DebugStr# #BindCountQueryFordatatable()#"/>
        </cfcatch>
        </cftry>
		<cfreturn dataOut>
	</cffunction>

	<!---get list group to select in emergency --->
	<cffunction name="GetGroupListForDatatableNew" access="remote" output="true" returnFormat="jSon" hint="Get group list for datatable">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="inpGroupId" TYPE="numeric" default="0">
		<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_3" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_3" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_4" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_4" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
		<cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="isCreateEms" default="1" /><!--- this is check request from create ems form or group list page  --->
		<cfargument name="inpUserId" required="false" default="#Session.USERID#"/>

		<!---check permission--->

		<cfset var permissionObject = CreateObject("component","session.cfc.administrator.permission")>
		<cfset var contactPermission = permissionObject.havePermission(Contact_Title)>
		<cfset var contactGroupPermission = permissionObject.havePermission(Contact_Group_Title)>

		<cfset var order_0 = '' />
		<cfset var order_1 = '' />
		<cfset var order_2 = '' />
		<cfset var order_3 = '' />
		<cfset var order_4 = '' />
		<cfset var data = '' />
		<cfset var filterItem = '' />
		<cfset var addHistory = '' />
		<cfset var selectGroupList = ''/>
		<cfset var GetGroupsNew = "">
		<cfset var GetTotalGroups = '' />
		<cfset var shortCode = '' />

		<cfif NOT contactGroupPermission.havePermission>
			<cfset session.permissionError = contactGroupPermission.message>
			<cflocation url="#rootUrl#/#sessionPath#/account/home" addtoken="no">
		</cfif>

		<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
			<cfinvokeargument name="pageTitle" value="#Contact_Group_Text#">
		</cfinvoke>

        <cfset var DebugStr = "" />


		<cfset var dataOut = {}>
		<cfset dataout["DATALIST"] = []>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
	    <cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
	    <cfset dataout.TYPE = '' />

		   	<!---set order param for query--->
			<cfif sSortDir_1 EQ "asc" OR sSortDir_1 EQ "desc">
				<cfset order_1 = sSortDir_1>
			<cfelse>
				<cfset order_1 = "">
			</cfif>
			<cfif sSortDir_2 EQ "asc" OR sSortDir_2 EQ "desc">
				<cfset order_2 = sSortDir_2>
			<cfelse>
				<cfset order_2 = "">
			</cfif>
			<cfif sSortDir_3 EQ "asc" OR sSortDir_3 EQ "desc">
				<cfset order_3 = sSortDir_3>
			<cfelse>
				<cfset order_3 = "">
			</cfif>
			<cfif sSortDir_4 EQ "asc" OR sSortDir_4 EQ "desc">
				<cfset order_4 = sSortDir_4>
			<cfelse>
				<cfset order_4 = "">
			</cfif>
			<cfif iSortCol_0 EQ 0>
				<cfset order_0 = "desc">
			<cfelse>
				<cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
					<cfset order_0 = sSortDir_0>
				<cfelse>
					<cfset order_0 = "">
				</cfif>
			</cfif>


			<!--- remove old method:
			<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"/>
			--->
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>

			<!---get data here --->
			<cfquery name="GetGroupsNew" datasource="#Session.DBSourceREAD#">
				SELECT SQL_CALC_FOUND_ROWS
					simplelists.grouplist.GroupId_bi,
					simplelists.grouplist.GroupName_vch,
					IF(optoutGroup.optout IS NULL, 0, optoutGroup.optout) AS optout,
					COUNT(simplelists.groupcontactlist.GroupId_bi) AS optin
				FROM
					simplelists.grouplist
				LEFT JOIN
					simplelists.groupcontactlist ON simplelists.grouplist.GroupId_bi = simplelists.groupcontactlist.GroupId_bi
				LEFT JOIN (
					SELECT
						COUNT(IF(tbl2.OptOut_dt IS NOT NULL, 1, NUll)) AS optout, tbl2.GroupID_bi
						FROM (
							SELECT * FROM (
								SELECT * FROM simplelists.optinout WHERE GroupId_bi IS NOT NULL
								ORDER BY OptId_int DESC
							) tbl

							GROUP BY tbl.GroupId_bi, tbl.ContactString_vch
						) tbl2
					GROUP BY
					tbl2.GroupID_bi

				) optoutGroup ON simplelists.grouplist.GroupId_bi = optoutGroup.GroupID_bi
				WHERE
					simplelists.grouplist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
				AND
					simplelists.grouplist.ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#shortCode.SHORTCODEID#"/>
					<cfif arguments.inpGroupId GT 0>
						AND simplelists.grouplist.GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpGroupId#">
					</cfif>
            	GROUP BY
				simplelists.grouplist.GroupId_bi
			ORDER BY
				<cfif iSortCol_0 EQ 1>
					simplelists.grouplist.GroupName_vch <cfif sSortDir_0 EQ "asc">ASC<cfelse>DESC</cfif>
				<cfelse>
					simplelists.grouplist.GroupId_bi <cfif sSortDir_0 EQ "asc">ASC<cfelse>DESC</cfif>
				</cfif>

            	LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
	        </cfquery>

	        <cfquery datasource="#Session.DBSourceREAD#"  name="GetTotalGroups">
				SELECT FOUND_ROWS() AS totalGroups
			</cfquery>

			<cfset dataout["sEcho"] = #sEcho#>
			<cfset dataout["iTotalRecords"] = (GetTotalGroups.RECORDCOUNT GTE 1 ? GetTotalGroups.totalGroups : 0) >
			<cfset dataout["iTotalDisplayRecords"] = (GetTotalGroups.RECORDCOUNT GTE 1 ? GetTotalGroups.totalGroups : 0) >

			<cfloop query="GetGroupsNew">
				<cfset var HTMLGroupName = Replace(GetGroupsNew.GroupName_vch, "'", "`", "ALL") />
				<cfset var dataItem = {}/>
				<cfset dataItem = {
					ID = "#GetGroupsNew.GroupId_bi#",
					NAME = "#HTMLGroupName#",
					optin  = "#GetGroupsNew.optin#",
					optout = "#GetGroupsNew.optout#"
				}>
				<cfset dataout["DATALIST"].append(dataItem) />
			</cfloop>
			<cfset dataout.RXRESULTCODE = 1 />

		<cfreturn dataOut>
	</cffunction>

	<cffunction name="GetTotalOptOutByUserId" access="remote" output="true" hint="Get total opt out by userid">
		<cfargument name="inpUserId" required="false" default="#Session.USERID#"/>

		<cfset var data = '' />

		<cfset var selectGroupList = ''/>
		<cfset var GetGroupsNew = "">
		<cfset var GetTotalGroups = '' />
		<cfset var shortCode = '' />



		<cfset var dataOut = {}>
		<cfset dataout.RXRESULTCODE = -1 />
	    <cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
	    <cfset dataout.TYPE = '' />
		<cfset dataout.TOTALOPTOUT = 0/>



			<!--- remove old method:
			<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"/>
			--->
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>

			<!---get data here --->
			<cfquery name="GetGroupsNew" datasource="#Session.DBSourceREAD#">
				SELECT
					COUNT(DISTINCT(ContactString_vch)) AS Total
				FROM
					simplelists.optinout
				WHERE
					OptOut_dt IS NOT NULL
				AND
					OptIn_dt IS NULL
				AND
					BatchId_bi IN(
						SELECT 	BatchId_bi
						FROM	simpleobjects.batch
						WHERE	UserId_int=	<cfqueryparam cfsqltype="cf_sql_integer" value="#inpUserId#"/>
						AND		ShortCodeId_int =<cfqueryparam cfsqltype="cf_sql_integer" value="#shortCode.SHORTCODEID#"/>
					)
	        </cfquery>


			<cfset dataout.TOTALOPTOUT += GetGroupsNew.Total>
			<cfset dataout.RXRESULTCODE = 1 />

		<cfreturn dataOut>
	</cffunction>

<!--- ************************************************************************************************************************* --->
<!--- Remove Contacts from Group --->
<!--- ************************************************************************************************************************* --->

<cffunction name="RemoveContactFromGroup" output="true" access="remote" hint="Delete contact string from group">
    <cfargument name="INPGROUPID" required="no" default="-1">
    <cfargument name="INPCONTACTID" required="no" default="0" hint="Used to specify a contact in case of duplicates contact strings with different contact Ids in group">
    <cfargument name="INPCONTACTSTRING" required="yes" default="-1">
    <cfargument name="INPCONTACTTYPE" required="no" default="3">

    <cfset var dataOut = {}>
    <cfset var UpdateListData = ''/>
    <cfset var VerifyUserGroup = ''/>


	<cfset var shortCode = ''/>
	<cfset var rs1 = ''/>

     <!---
    Positive is success
    1 = OK
    2 =
    3 =
    4 =

    Negative is failure
    -1 = general failure
    -2 = Session Expired
    -3 =

     --->
	<cfset dataout.RXRESULTCODE = -1 />
	<cfset dataout.INPCONTACTSTRING = '#arguments.INPCONTACTSTRING#'>
	<cfset dataout.INPGROUPID = '#INPGROUPID#'>
    <cfset dataout.MESSAGE = ""/>
	<cfset dataout.ERRMESSAGE = ""/>
    <cfset dataout.TYPE = '' />

    <cftry>
        <!--- Validate session still in play - handle gracefully if not --->
        <cfif Session.USERID GT 0>


            <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
                <cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
            </cfif>

            <cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>

            <!--- Verify user is group id owner--->
            <cfquery name="VerifyUserGroup" datasource="#Session.DBSourceEBM#">
             	SELECT
                 	COUNT(GroupName_vch) AS TOTALCOUNT
             	FROM
                	simplelists.grouplist
             	WHERE
                 UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
             	AND
                	GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPGROUPID#">
                AND
                 	ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#shortcode.SHORTCODEID#"/>
            </cfquery>

            <cfif VerifyUserGroup.TOTALCOUNT EQ 0>
                  <cfthrow MESSAGE="Group ID does not exists for this user account!" TYPE="Any" detail="" errorcode="-6">
            </cfif>

            <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#" result="rs1">
                DELETE FROM
                        simplelists.groupcontactlist
                WHERE
                    GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">
                AND
                    ContactAddressId_bi IN
                  (
                      SELECT
                            simplelists.contactstring.contactaddressid_bi
                      FROM
                            simplelists.contactstring
                      WHERE
                            simplelists.contactstring.contactstring_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.INPCONTACTSTRING#">

                      <cfif INPCONTACTTYPE GT 0>
                          AND
                                simplelists.contactstring.contacttype_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTTYPE#">
                      </cfif>

                      <cfif INPCONTACTID GT 0>
                          AND
                                simplelists.contactstring.contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTID#">
                      </cfif>

                  )
            </cfquery>

            <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                <cfinvokeargument name="userId" value="#session.userid#">
                <cfinvokeargument name="moduleName" value="Contact #arguments.INPCONTACTSTRING#">
                <cfinvokeargument name="operator" value="Remove contact: #arguments.INPCONTACTSTRING# in Group: #INPGROUPID#">
            </cfinvoke>

            <cfset dataout.RXRESULTCODE = 1 />

          <cfelse>

            <cfset dataout.RXRESULTCODE = -2 />
            <cfset dataout.MESSAGE = "Session Expired! Refresh page after logging back in."/>

          </cfif>

    <cfcatch TYPE="any">
        <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
        <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
    </cfcatch>

    </cftry>
    <cfreturn dataout />
</cffunction>

<!--- ************************************************************************************************************************* --->
<!--- Remove Contacts from All Groups User owns --->
<!--- ************************************************************************************************************************* --->

<cffunction name="RemoveContactFromAllGroups" output="true" access="remote" hint="Delete contact string from all groups user owns">
    <cfargument name="INPCONTACTSTRING" required="yes" default="-1">
    <cfargument name="INPCONTACTID" required="no" default="0" hint="Used to specify a contact in case of duplicates contact strings with different contact Ids in group">
    <cfargument name="INPCONTACTTYPE" required="no" default="3">

    <cfset var dataout = {} />
    <cfset var shortCode = ''/>
    <cfset var UpdateListData = ''/>


	<cfset var rs1 = ''/>

     <!---
    Positive is success
    1 = OK
    2 =
    3 =
    4 =

    Negative is failure
    -1 = general failure
    -2 = Session Expired
    -3 =

     --->
   	<cfset dataout.RXRESULTCODE = -1 />
	<cfset dataout.INPCONTACTSTRING = '#arguments.INPCONTACTSTRING#'>
	<cfset dataout.INPGROUPID = '#INPGROUPID#'>
    <cfset dataout.MESSAGE = ""/>
	<cfset dataout.ERRMESSAGE = ""/>
    <cfset dataout.TYPE = '' />

    <cftry>
        <!--- Validate session still in play - handle gracefully if not --->
        <cfif Session.USERID GT 0>



            <cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>

            <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#" result="rs1">
                DELETE FROM
                        simplelists.groupcontactlist
                WHERE
                    GroupId_bi IN
                    (
                    	SELECT
                    		GroupId_bi
                    	FROM
                    		simplelists.grouplist
                    	WHERE
                    		UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    	AND
                 			ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#shortcode.SHORTCODEID#"/>
                 	)
                AND
                        ContactAddressId_bi IN
                  (
                      SELECT
                            simplelists.contactstring.contactaddressid_bi
                      FROM
                            simplelists.contactstring
                      WHERE
                            simplelists.contactstring.contactstring_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.INPCONTACTSTRING#">

                      <cfif INPCONTACTTYPE GT 0>
                          AND
                                simplelists.contactstring.contacttype_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTTYPE#">
                      </cfif>

                      <cfif INPCONTACTID GT 0>
                          AND
                                simplelists.contactstring.contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTID#">
                      </cfif>
                  )
            </cfquery>

            <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                <cfinvokeargument name="userId" value="#session.userid#">
                <cfinvokeargument name="moduleName" value="Contact #arguments.INPCONTACTSTRING# Removed from all groups">
                <cfinvokeargument name="operator" value="Edit Group">
            </cfinvoke>

         	<cfset dataout.RXRESULTCODE = 1 />

        <cfelse>

            <cfset dataout.RXRESULTCODE = -2 />
        	<cfset dataout.MESSAGE = "Session Expired! Refresh page after logging back in."/>

        </cfif>

    <cfcatch TYPE="any">
         <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
    	<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
    </cfcatch>

    </cftry>
    <cfreturn dataout />
</cffunction>



    <cffunction name="GetSubscriberListByGroupId" access="remote" output="true" hint="Get subscriber list by group id">
    	<cfargument name="inpGroupId" required="true">
    	<cfset var getSubscriberList = '' />
    	<cfset var dataout = {} />
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset getSubscriberList = '' />

    	<cftry>
    		<cfquery name="getSubscriberList" datasource="#Session.DBSourceREAD#">
    			SELECT
    				GroupName_vch
    			FROM
    				simplelists.grouplist
    			WHERE
    				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
    			AND
    				GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpGroupId#">
    			LIMIT
    				1
    		</cfquery>
    		<cfif getSubscriberList.RecordCount GT 0>
    			<cfset dataout.RXRESULTCODE = 1 />
    			<cfset dataout.SUBSCRIBERLISTNAME = "#getSubscriberList.GroupName_vch#"/>
    			<cfset dataout.MESSAGE = "Get subscriber list successfully" />
    		<cfelse>
    			<cfset dataout.RXRESULTCODE = 0 />
    			<cfset dataout.MESSAGE = "Invalid groupid or userid" />
    		</cfif>
    		<cfcatch type="Any" >
    			<cfset dataout.RXRESULTCODE = -1 />
			    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
	        </cfcatch>
    	</cftry>

    	<cfreturn dataout />
    </cffunction>

    <cffunction name="UpdateSubscriberListNameByGroupId" access="remote" output="true" hint="Update subscriber list name by group id">
    	<cfargument name="inpGroupId" required="true">
    	<cfargument name="inpNewGroupName" required="true"/>
    	<cfset var updateSubscriberList = '' />
    	<cfset var updateResult = '' />
    	<cfset var dataout = {} />
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset var checkDuplicatedGroupName = '' />
    	<cfset var validationResult = '' />

    	<cftry>
    		<cfif arguments.inpNewGroupName EQ ''>
				<cfset dataout.RXRESULTCODE = -2>
				<cfset dataout.MESSAGE = 'Please enter new Subscriber List name!'>
				<cfreturn dataout/>
			<cfelse>
				<cfinvoke method="VldContent" component="public.cfc.validation" returnvariable="validationResult">
				    <cfinvokeargument name="Input" value="#arguments.inpNewGroupName#"/>
				</cfinvoke>

				<cfif validationResult NEQ true>
				    <cfset dataout.RXRESULTCODE = -3>
				    <cfset dataout.MESSAGE = 'Please do not input special character.'>
				    <cfreturn dataout>
				</cfif>

				<cfquery name="checkDuplicatedGroupName" datasource="#Session.DBSourceEBM#">
					SELECT
						GroupName_vch
					FROM
						simplelists.grouplist
					WHERE
						GroupName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpNewGroupName#">
					AND
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					AND
						GroupId_bi != <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpGroupId#">
				</cfquery>

				<cfif checkDuplicatedGroupName.RecordCount GT 0>
					<cfset dataout.RXRESULTCODE = -4>
				    <cfset dataout.MESSAGE = 'Oops! Duplicated Subscriber List name, please input another name.'>
				    <cfreturn dataout>
				</cfif>
			</cfif>

    		<cfquery name="updateSubscriberList" datasource="#Session.DBSourceEBM#" result="updateResult">
    			UPDATE
    				simplelists.grouplist
    			SET
    				GroupName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpNewGroupName#">
    			WHERE
    				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
    			AND
    				GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpGroupId#">
    		</cfquery>

    		<cfif updateResult.RecordCount GT 0>
    			<cfset dataout.RXRESULTCODE = 1 />
    			<cfset dataout.MESSAGE = "Update subscriber list successfully!" />

                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="GroupId = #arguments.inpGroupId#">
                    <cfinvokeargument name="operator" value="Subscriber updated, subscriberName = #arguments.inpNewGroupName#">
                </cfinvoke>
    		<cfelse>
    			<cfset dataout.RXRESULTCODE = 0 />
    			<cfset dataout.MESSAGE = "Invalid groupid or userid!" />
    		</cfif>
    		<cfcatch type="Any" >
    			<cfset dataout.RXRESULTCODE = -1 />
			    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
	        </cfcatch>
    	</cftry>

    	<cfreturn dataout />
    </cffunction>

    <cffunction name="GetCampaignByGroupId" access="remote" output="true" hint="Get all campaign use a subcriberlist">
    	<cfargument name="inpGroupId" required="true">

    	<cfset var getCampaign = '' />
    	<cfset var getCampaignName = '' />
    	<cfset var batchItem = {} />
    	<cfset var dataout = {} />
    	<cfset dataout.MESSAGE = '' />
    	<cfset batchItem = '' />
    	<cfset dataout['batchDetail'] = arrayNew(1)>
    	<cfset dataout.ERRMESSAGE = '' />

    	<cftry>
    		<cfquery name="getCampaign" datasource="#Session.DBSourceREAD#">
	    		SELECT
	    			BatchId_bi
	    		FROM
	    			simplelists.subscriberbatch
	    		WHERE
	    			GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpGroupId#">
	    		AND
	    			UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	    	</cfquery>
	    	<cfif getCampaign.RecordCount GT 0>
	    		<cfloop query="getCampaign">
	    			<cfquery name="getCampaignName" datasource="#Session.DBSourceREAD#">
	    				SELECT
	    					Desc_vch, BatchId_bi
	    				FROM
	    					simpleobjects.batch
	    				WHERE
	    					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getCampaign.BatchId_bi#">
	    				AND
	    					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	    				LIMIT
	    					1
	    			</cfquery>
	    			<cfif getCampaignName.RecordCount GT 0>
	    				<cfset batchItem = {
	    					id="#getCampaignName.BatchId_bi#",
	    					name="#getCampaignName.Desc_vch#"
	    				}>
	    				<cfset arrayAppend(dataout['batchDetail'], batchItem)>
	    			</cfif>
	    		</cfloop>
	    		<cfif arrayLen(dataout['batchDetail']) GT 0>
		    		<cfset dataout.RXRESULTCODE = 1 />
		    		<cfset dataout.MESSAGE = 'Get Campaign by subscriber list successfully' />
		    	<cfelse>
		    		<cfset dataout.RXRESULTCODE = -2 />
		    		<cfset dataout.MESSAGE = 'No record found' />
		    	</cfif>
	    	<cfelse>
	    		<cfset dataout.RXRESULTCODE = 0 />
	    		<cfset dataout.MESSAGE = 'No record found' />
	    	</cfif>
    		<cfcatch type="Any" >
    			<cfset dataout.RXRESULTCODE = -1 />
			    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
	        </cfcatch>
    	</cftry>
    	<cfreturn dataout />
	</cffunction>

	<cffunction name="DeleteSubscriberListByGroupId" access="remote" output="true" hint="Delete subscriber list by group id">
		<cfargument name="inpGroupId" required="true">
		<cfset var dataout = {} />
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout['batchname'] = arrayNew(1)>
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset var deleteGroupList = '' />
    	<cfset var deleteGroupContactList = '' />
    	<cfset var selectGroupContactList = '' />
    	<cfset var updateSubscriberBatchRecord = '' />
    	<cfset var selectGroupList = '' />
    	<cfset var deleteGroupListResult = '' />
    	<cfset var deleteGroupContactListResult = '' />

    	<cftry>
    		<cfquery name="deleteGroupList" datasource="#Session.DBSourceEBM#" result="deleteGroupListResult">
    			DELETE FROM simplelists.grouplist
    			WHERE
    				GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpGroupId#">
    			AND
    				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
    		</cfquery>

    		<cfquery name="updateSubscriberBatchRecord" datasource="#Session.DBSourceEBM#">
    			UPDATE
    				simplelists.subscriberbatch
    			SET
    				GroupId_bi = 0
    			WHERE
    				GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpGroupId#">
    			AND
    				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
    		</cfquery>

    		<cfquery name="selectGroupList" datasource="#Session.DBSourceREAD#">
    			SELECT
    				GroupId_bi
    			FROM
    				simplelists.groupcontactlist
    			WHERE
    				GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpGroupId#">
    		</cfquery>

    		<cfif selectGroupList.RecordCount GT 0>
	    		<cfquery name="deleteGroupContactList" datasource="#Session.DBSourceEBM#" result="deleteGroupContactListResult">
	    			DELETE FROM simplelists.groupcontactlist
	    			WHERE
	    				GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpGroupId#">
	    		</cfquery>
	    		<cfif deleteGroupListResult.RecordCount GT 0 && deleteGroupContactListResult.RecordCount GT 0>
	    			<cfset dataout.RXRESULTCODE = 1>
	    			<cfset dataout.MESSAGE = "Delete subscriber list successfully" />
	    		<cfelseif deleteGroupListResult.RecordCount GT 0 && deleteGroupContactListResult.RecordCount EQ 0>
	    			<cfset dataout.RXRESULTCODE = 0>
	    			<cfset dataout.MESSAGE = "Delete subscriber list successfully. Delete group contact list failed" />
	    		<cfelse>
	    			<cfset dataout.RXRESULTCODE = -2>
	    			<cfset dataout.MESSAGE = "Delete subscriber list failed" />
	    		</cfif>
	    	<cfelse>
	    		<cfif deleteGroupListResult.RecordCount GT 0>
	    			<cfset dataout.RXRESULTCODE = 1>
	    			<cfset dataout.MESSAGE = "Delete subscriber list successfully" />
	    		<cfelse>
	    			<cfset dataout.RXRESULTCODE = 0>
	    			<cfset dataout.MESSAGE = "Delete subscriber list failed" />
	    		</cfif>
	    	</cfif>

	    	<cfif deleteGroupListResult.RecordCount GT 0>
                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="GroupId = #arguments.inpGroupId#">
                    <cfinvokeargument name="operator" value="Subscriber deleted">
                </cfinvoke>
            </cfif>
    		<cfcatch type="Any" >
    			<cfset dataout.RXRESULTCODE = -1 />
			    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
	        </cfcatch>
    	</cftry>

    	<cfreturn dataout />
	</cffunction>



	<cffunction name="GetSubcriberListByUserID" access="remote" hint="Get all subcriber of specified user">
    	<cfargument name="inpUserID" required="true" default="#Session.UserID#">
		<cfset var dataout = {} />
		<cfset var getGroups = getTotal ={} />
		<cfset var shortcode = '' />

    	<cfset dataout.RXRESULTCODE = -1 />
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset dataout.DATALIST = [] />

    	<cftry>

			<!--- remove old method:
			<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"/>
			--->
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>
    		<cfquery name="getGroups" datasource="#Session.DBSourceREAD#">
	    	 	SELECT
					simplelists.grouplist.GroupId_bi,
					simplelists.grouplist.GroupName_vch
				FROM
					simplelists.grouplist
				WHERE
					simplelists.grouplist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">
				AND
					simplelists.grouplist.ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#shortcode.SHORTCODEID#"/>
	    	</cfquery>

	    	<cfloop query="getGroups">
		    	<cfset var item = {"ID" = "#getGroups.GroupId_bi#", "NAME" = "#getGroups.GroupName_vch#" } />
		    	<cfset ArrayAppend(dataout.DATALIST,  item) />
	    	</cfloop>

	    	<cfset dataout.RXRESULTCODE = 1 >
			<cfset dataout.MESSAGE = "Get subscribers successfully!">
    		<cfcatch type="any" >
    			<cfset dataout.RXRESULTCODE = -1 />
			    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
	        </cfcatch>
    	</cftry>
    	<cfreturn dataout />
	</cffunction>

	<cffunction name="GetSubcriberListByBatchID" access="remote" hint="Get all subcriber of specified user">
    	<cfargument name="inpBatchId" required="true">
    	<cfargument name="inpType" required="false" default="-1"/>
		<cfset var dataout = {} />
		<cfset var getGroups = getTotal ={} />

    	<cfset dataout.RXRESULTCODE = -1 />
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset dataout.DATALIST = [] />
    	<cfset var RetVarGetBatchDetails = '' />

    	<cftry>
    		<cfif arguments.inpType EQ -1>
				<cfinvoke method="GetBatchDetails" component="session.sire.models.cfc.control-point" returnvariable="RetVarGetBatchDetails">
				    <cfinvokeargument name="inpBatchID" value="#arguments.inpBatchId#">
				</cfinvoke>

				<cfif RetVarGetBatchDetails.RXRESULTCODE LT 1>
					<cfthrow type="Batch" message="Campaign id not valid" detail="#RetVarGetBatchDetails.MESSAGE#"/>
				</cfif>

				<cfset arguments.inpType = RetVarGetBatchDetails.EMSFLAG/>
    		</cfif>

	    	<cfif arguments.inpType EQ 1>
		        <cfquery name="getGroups" datasource="#session.DBSourceREAD#">
                    SELECT
	                    sg.GroupName_vch,
	                    sg.GroupId_bi
                    FROM
                    	simpleobjects.batch sb
                    INNER JOIN
                    	simpleobjects.batch_blast_log sbbl
                    ON
                    	sb.BatchId_bi = sbbl.BatchId_bi
                    INNER JOIN
                    	simplelists.grouplist sg
                    ON
                    	sbbl.GroupId_int = sg.GroupId_bi
                    WHERE
                    	sb.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
                    AND
                    	sb.BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>
		        </cfquery>
		    <cfelse>
		        <cfquery name="getGroups" datasource="#session.DBSourceREAD#">
	                SELECT
	                    sg.GroupName_vch,
	                    sg.GroupId_bi
	                FROM
	                    simpleobjects.batch sb
	                INNER JOIN
	                    simplelists.subscriberbatch ss
	                ON
	                    sb.BatchId_bi = ss.BatchId_bi
	                INNER JOIN
	                    simplelists.grouplist sg
	                ON
	                    ss.GroupId_bi = sg.GroupId_bi
	                WHERE
	                    sb.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
	                AND
	                	sb.BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>
		        </cfquery>
	    	</cfif>

	    	<cfloop query="getGroups">
		    	<cfset var item = {"ID" = "#getGroups.GroupId_bi#", "NAME" = "#getGroups.GroupName_vch#" } />
		    	<cfset ArrayAppend(dataout.DATALIST,  item) />
	    	</cfloop>

	    	<cfset dataout.RXRESULTCODE = 1 >
			<cfset dataout.MESSAGE = "Get subscribers successfully!">
    		<cfcatch type="any" >
    			<cfset dataout.RXRESULTCODE = -1 />
			    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
	        </cfcatch>
    	</cftry>
    	<cfreturn dataout />
	</cffunction>

	<cffunction name="addgroup" hint="Get simple list statistics" access="remote" output="true">
       	<cfargument name="INPGROUPDESC" required="yes" default="">
  		<cfargument name="INPUSERID" required="0" default="#Session.USERID#">
        <cfset var dataout = '0' />
        <cfset var NextGroupId = -1>
        <cfset var VerifyUnique = "">
       	<cfset var addgroup = "" />
        <cfset var addgroupResult = "" />
		<cfset var shortCode = "" />






         <!---
        Positive is success
        1 = OK
		2 =
		3 =
		4 =

        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 =

	     --->


       	<cfoutput>





        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPDESC, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPGROUPDESC", "#INPGROUPDESC#") />
            <cfset QuerySetCell(dataout, "INPGROUPID", "#NextGroupId#") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />

            <cftry>
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>

					<!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->

				  	<!--- Set default to -1 --->
				   	<cfset NextGroupId = -1>

                    <!--- Verify does not already exist--->
                    <!--- Get next Lib ID for current user --->
                    <cfquery name="VerifyUnique" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(GroupName_vch) AS TOTALCOUNT
                        FROM
                            simplelists.grouplist
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.INPUSERID#">
                            AND
                            GroupName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPGROUPDESC#">
                    </cfquery>

                    <cfif VerifyUnique.TOTALCOUNT GT 0>
	                    <cfthrow MESSAGE="Group already exists! Try a different group name." TYPE="Any" detail="" errorcode="-6">
                    </cfif>

					<!---Pre-reserve the first five group ids for DNC, and other defaults--->

                    <cfif NextGroupId LT 5><cfset NextGroupId = 5></cfif>
                    <cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode">
						<cfinvokeargument name="inpUserIDRequest" value="#arguments.INPUSERID#"/>
					</cfinvoke>

                    <cfquery name="addgroup" datasource="#Session.DBSourceEBM#" result="addgroupResult">
                        INSERT INTO simplelists.grouplist
                            (UserId_int, GroupName_vch, Created_dt,ShortCodeId_int )
                        VALUES
                            (
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.INPUSERID#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPGROUPDESC#">,
                            NOW(),
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#shortCode.SHORTCODEID#">
                            )
                    </cfquery>

                    <cfset NextGroupId = #addgroupResult.GENERATEDKEY#>

                    <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPDESC, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPGROUPDESC", "#INPGROUPDESC#") />
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#NextGroupId#") />
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />

                <cfelse>

                    <cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPDESC, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPGROUPDESC", "#INPGROUPDESC#") />
            		<cfset QuerySetCell(dataout, "INPGROUPID", "#NextGroupId#") />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />

                </cfif>

            <cfcatch TYPE="any">

				<cfset dataout =  QueryNew("RXRESULTCODE, INPGROUPDESC, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPGROUPDESC", "#INPGROUPDESC#") />
	            <cfset QuerySetCell(dataout, "INPGROUPID", "#NextGroupId#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />

            </cfcatch>

            </cftry>

		</cfoutput>

        <cfreturn dataout />
    </cffunction>




    <cffunction name="getGroupString" access="private" hint="Get group string by off set date">
	    <cfargument name="inpOffsetDate" type="numeric" required="true">

	    <cfset var type = "day">

	   	<cfif arguments.inpOffsetDate GTE 14>
		   	<cfset type = "week">
		</cfif>

		<cfif arguments.inpOffsetDate GTE 89>
			<cfset type = "month">
		</cfif>

	   	<cfreturn type />
    </cffunction>



    <cffunction name="countDataByDate" access="private" hint="Count subscriber by date">
	    <cfargument name="inpDate" type="string" required="true">
	    <cfargument name="inpGroupId" type="numeric" default="0">

	    <cfset var value = 0 />
	    <cfset var query =  {} />

	    <cfset var shortcode = '' />


		<!--- remove old method:
		<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"/>
		--->
		<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>

    	<cfquery name="query" datasource="#Session.DBSourceEBM#">
    		SELECT
    			COUNT( CS.ContactAddressId_bi ) AS Subcribers
			FROM simplelists.grouplist GL
			INNER JOIN simplelists.groupcontactlist GCL
				ON GL.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				AND GCL.GroupId_bi = GL.GroupId_bi
			INNER JOIN simplelists.contactstring AS CS
				ON GCL.ContactAddressId_bi =  CS.ContactAddressId_bi
			WHERE
				GL.Active_int = 1
				AND
				CS.OptIn_dt <= DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#DATEFORMAT(arguments.inpDate, 'yyyy-mm-dd')#">, INTERVAL 86399 SECOND)
				AND (CS.OptOut_dt IS NULL OR CS.OptOut_dt >= DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#DATEFORMAT(arguments.inpDate, 'yyyy-mm-dd')#">, INTERVAL 1 DAY))
				<cfif arguments.inpGroupId GT 0>
					AND GCL.GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpGroupId#">
				</cfif>
				AND
					GL.ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#shortCode.SHORTCODEID#"/>
    	</cfquery>

    	<cfset value = query.Subcribers>

    	<cfreturn value />
    </cffunction>


    <cffunction name="GetStatistic" access="remote" hint="Get Statistic">
    	<cfargument name="inpGroupId" type="numeric" required="true" default="0">
	    <cfargument name="inpStartDate" type="string" required="true" default="">
	    <cfargument name="inpEndDate" type="string" required="true" default="">


	    <cfset var dataout = {} />
	    <cfset var dataout["DataList"] = [] />

	    <cfset var getStatisticData = [] />
	    <cfset var strGroupType = "" />

	    <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
	    <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />


	    <cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
	    <cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
	    <cfset var dateOffset  = DateDiff("d",startDate, endDate) />

	    <cfset var day = ''/>
	    <cfset var eachDay = '' />

	    <cftry>

	    	<cfinvoke method="getGroupString" returnvariable="strGroupType">
	    		<cfinvokeargument name="inpOffsetDate" value="#dateOffset#">
	    	</cfinvoke>



	    	<cfswitch expression="#strGroupType#">
		    	<cfcase value="day">

					<cfset var currDateOut = startDate >
					<cfloop from="#startDate#" to="#endDate#" index="day" step="#CreateTimeSpan(1,0,0,0)#">
						<cfset var valueItem = {
							"Time" 	= "#DateFormat(currDateOut, "yyyy-mm-dd")#",
							"Value" = 0,
							"format" = "#strGroupType#"
						}/>

						<cfinvoke method="countDataByDate" returnvariable="valueItem.Value">
							<cfinvokeargument name="inpDate" value="#currDateOut#">
							<cfinvokeargument name="inpGroupId" value="#arguments.inpGroupId#">
						</cfinvoke>
						<cfset dataout["DataList"].append(valueItem) />
						<cfset currDateOut = currDateOut.add('d', 1) >
					</cfloop>

				</cfcase>
				<cfcase value="week">

					<!--- Create key list for data --->
					<cfset var dateList = [] />
					<cfset var currDate = startDate >
					<cfloop from="#startDate#" to="#endDate#" index="day" step="#CreateTimeSpan(7,0,0,0)#">
						<cfif DateDiff("d",currDate, endDate) GT 0 AND currDate LT endDate>
							<cfset  dateList.append(currDate) />
							<cfset currDate = currDate.add('d', 7) >
						</cfif>
					</cfloop>
					<cfset  dateList.append(endDate) />

					<cfloop array="#dateList#" index="eachDay">
						<cfset var valueItem = { "Time" = DateFormat(eachDay, "yyyy-mm-dd"), "Value" = 0, "format" = "#strGroupType#"} />

						<cfinvoke method="countDataByDate" returnvariable="valueItem.Value">
							<cfinvokeargument name="inpDate" value="#eachDay#">
							<cfinvokeargument name="inpGroupId" value="#arguments.inpGroupId#">
						</cfinvoke>

						<cfset dataout["DataList"].append(valueItem) />
					</cfloop>

				</cfcase>
				<cfcase value="month">
					<cfset var dateList = [] />
					<cfset var currDate = startDate />
					<cfset dateList.append(currDate) />

					<cfloop condition="DateCompare(currDate, endDate) LT 0)">
						<cfset currDate = currDate.add('m', 1) />
						<cfif DateCompare(currDate, endDate) LT 0>
							<cfset dateList.append(CreateDate(currDate.year(), currDate.month(), 1)) />
						</cfif>
					</cfloop>
					<cfset dateList.append(endDate) />


					<cfloop array="#dateList#" index="eachDay">
						<cfset var valueItem = { "Time" = DateFormat(eachDay, "yyyy-mm-dd"), "Value" = 0, "format" = "#strGroupType#"} />
						<cfinvoke method="countDataByDate" returnvariable="valueItem.Value">
							<cfinvokeargument name="inpDate" value="#eachDay#">
							<cfinvokeargument name="inpGroupId" value="#arguments.inpGroupId#">
						</cfinvoke>
						<cfset dataout["DataList"].append(valueItem) />
					</cfloop>
				</cfcase>

	    	</cfswitch>
		    <cfcatch type="any">
		    	<cfset dataout.RXRESULTCODE = -1 />
			    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
		    </cfcatch>
	    </cftry>

	    <cfreturn dataout />
    </cffunction>





    <cffunction name="exportContactByGroupID" access="remote" hint="Export contact by groupid">
	    <cfargument name="inpGroupId" type="numeric" required="true" default="0">


    </cffunction>

    <cffunction name="GetSubscriberInfo" access="remote" hint="Get subscriber info">
    	<cfargument name="inpSubscriberIds" type="string" required="true" hint="list of subscriber id" />
    	<cfargument name="inpStartDate" type="string" required="no" default="">
		<cfargument name="inpEndDate" type="string" required="no" default="">
		<cfargument name="inpBatchId" type="string" required="no" default="">
		<cfargument name="inpUserId" required="no" default="#Session.USERID#">

    	<cfset var dataout = {}/>
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset dataout.TYPE = '' />
    	<cfset dataout.RXRESULTCODE = -1 />

    	<cfset dataout.RESULT = []/>

    	<cfset var GetGroupsNew = '' />

    	<cftry>
			<cfquery name="GetGroupsNew" datasource="#Session.DBSourceREAD#">
				SELECT
					count(Distinct(simplelists.contactstring.ContactString_vch)) as totalSMS,
					simplelists.grouplist.GroupId_bi,
					simplelists.grouplist.GroupName_vch,
					IF(optoutGroup.optout IS NULL, 0, optoutGroup.optout) AS optout,
					IF(optoutGroup.optin IS NULL, 0, optoutGroup.optin) AS optinByBatch,
					COUNT(simplelists.groupcontactlist.GroupId_bi) AS optin
				FROM
					simplelists.groupcontactlist
				INNER JOIN
					simplelists.contactstring ON simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
					<cfif arguments.inpStartDate NEQ "" AND arguments.inpEndDate>
						AND simplelists.contactstring.optin_dt BETWEEN
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpStartDate#"> AND
						DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpEndDate#">, INTERVAL 86399 SECOND)
					</cfif>
				RIGHT JOIN
					simplelists.grouplist ON simplelists.grouplist.GroupId_bi = simplelists.groupcontactlist.GroupId_bi
				LEFT JOIN (

					SELECT
						COUNT(IF(tbl2.OptOut_dt IS NOT NULL, 1, NUll)) AS optout, tbl2.GroupID_bi, COUNT(IF(tbl2.OptOut_dt IS NULL, 1, NUll)) AS optin
						FROM (
								SELECT
									OptOut_dt,GroupId_bi,ContactString_vch
								FROM
									(
										SELECT
											OptOut_dt,GroupId_bi,ContactString_vch
										FROM
											simplelists.optinout
										WHERE
											GroupId_bi IS NOT NULL
										<cfif inpBatchId GT 0>
											AND Batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#">
										</cfif>
										GROUP BY ContactString_vch
										ORDER BY
											OptId_int DESC
									) tbl
							GROUP BY tbl.GroupId_bi, tbl.ContactString_vch
						) tbl2
					GROUP BY
					tbl2.GroupID_bi

				) optoutGroup ON simplelists.grouplist.GroupId_bi = optoutGroup.GroupID_bi
				WHERE
					simplelists.grouplist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
				AND
					simplelists.grouplist.GroupId_bi IN ( #inpSubscriberIds# )
            	GROUP BY
            		simplelists.grouplist.GroupId_bi
	        </cfquery>

	        <cfif GetGroupsNew.RECORDCOUNT GT 0>
	        	<cfloop query="GetGroupsNew">
	        		<cfset var item = {} />
        			<cfset item.totalSMS = GetGroupsNew.totalSMS/>
        			<cfset item.GroupId_bi = GetGroupsNew.GroupId_bi/>
        			<cfset item.GroupName_vch = GetGroupsNew.GroupName_vch/>
        			<cfset item.optout = GetGroupsNew.optout/>
        			<cfset item.optin = GetGroupsNew.optinByBatch/>
	        		<cfset arrayAppend(dataout.RESULT, item)/>
	        	</cfloop>
	        </cfif>

	        <cfset dataout.RXRESULTCODE = 1/>

    		<cfcatch type="any">
    			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
    			<cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
    			<cfset dataout.TYPE = "#cfcatch.TYPE#"/>
    		</cfcatch>
    	</cftry>

    	<cfreturn dataout/>
    </cffunction>

    <cffunction name="CountSMSOfSubscriber" access="remote" output="false" hint="Count sms sent and receive for subscriber">
    	<cfargument name="inpGroupId" required="true" type="numeric">
    	<cfargument name="inpBatchId" required="true" type="numeric">

    	<cfset var dataout = {} />
    	<cfset dataout.RXRESULTCODE = -1/>
    	<cfset dataout.MESSAGE = ""/>
    	<cfset dataout.ERRMESSAGE = "" />
    	<cfset dataout.TYPE = "" />

    	<cfset dataout.SENT = 0/>
    	<cfset dataout.RECEIVED = 0/>

    	<cfset var GetSMSData = '' />

    	<cftry>

    		<cfquery name="GetSMSData" datasource="#session.DBSourceREAD#">
				SELECT
					sum(if(si.IREType_int = 1, 1, 0)) AS SENT,
					sum(if(si.IREType_int = 2, 1, 0)) AS RECEIVED
				FROM
					simplexresults.ireresults si
				WHERE
					si.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.userID#"/>
				AND
					si.IREType_int IN (1,2)
				AND
					si.BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>
				AND
					si.ContactString_vch IN (
						SELECT
							simplelists.contactstring.ContactString_vch
						FROM
							simplelists.groupcontactlist
							INNER JOIN simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
							INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi
						WHERE
							simplelists.contactlist.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.userID#"/>
							<cfif arguments.inpStartDate NEQ "" AND arguments.inpEndDate>
								AND simplelists.contactstring.optin_dt BETWEEN
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpStartDate#"> AND
								DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpEndDate#">, INTERVAL 86399 SECOND)
							</cfif>
						AND
							simplelists.groupcontactlist.groupid_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpGroupId#"/>
						AND
							simplelists.contactstring.ContactString_vch NOT IN (
								SELECT
									ContactString_vch
								FROM
									simplelists.contactstring
								INNER JOIN
									simplelists.contactlist
								ON
									simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi
								WHERE UserId_int = 50
							)
						AND
							simplelists.contactstring.ContactType_int = 3
				)
    		</cfquery>

    		<cfif GetSMSData.RECORDCOUNT GT 0>
    			<cfset dataout.SENT = (GetSMSData.SENT NEQ "" ? GetSMSData.SENT : 0)/>
    			<cfset dataout.RECEIVED = (GetSMSData.RECEIVED NEQ "" ? GetSMSData.RECEIVED : 0)/>
    		</cfif>

    		<cfset dataout.RXRESULTCODE = 1/>

    		<cfcatch type="any">
    			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
    			<cfset dataout.ERRMESSAGE = "#cfcatch.ERRMESSAGE#"/>
    			<cfset dataout.TYPE = "#cfcatch.TYPE#"/>
    		</cfcatch>
    	</cftry>

    	<cfreturn dataout/>
    </cffunction>

    <cffunction name="CountAllSMSOfSubscriber" access="remote" output="false" hint="Count sms sent and receive for subscriber (included optout)">
    	<cfargument name="inpGroupId" required="true" type="numeric">
    	<cfargument name="inpBatchId" required="true" type="numeric">
    	<cfargument name="inpStartDate" required="false" default=""/>
    	<cfargument name="inpEndDate" required="false" default=""/>

    	<cfset var dataout = {} />
    	<cfset dataout.RXRESULTCODE = -1/>
    	<cfset dataout.MESSAGE = ""/>
    	<cfset dataout.ERRMESSAGE = "" />
    	<cfset dataout.TYPE = "" />

    	<cfset dataout.SENT = 0/>
    	<cfset dataout.RECEIVED = 0/>

    	<cfset var GetSMSData = '' />

    	<cftry>

    		<cfquery name="GetSMSData" datasource="#session.DBSourceREAD#">
				SELECT
					sum(if(sc.DTSStatusType_ti = 5, 1, 0)) AS SENT,
					-- sum(if(si.DTSStatusType_ti = 1, 1, 0)) AS RECEIVED
					sum(if(sc.DTSStatusType_ti = 1, 1, 0)) AS QUEUED
				FROM
					simplequeue.contactqueue sc
				INNER JOIN
					simplelists.contactstring cs
				ON
					sc.ContactString_vch = cs.ContactString_vch
				INNER JOIN
					simplelists.contactlist cl
				ON
					cs.ContactId_bi = cl.ContactId_bi
				AND
					cl.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.userID#"/>
				-- INNER JOIN
				-- 	simplelists.groupcontactlist gcl
				-- ON
				-- 	gcl.ContactAddressId_bi = cs.ContactAddressId_bi
				WHERE
					sc.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.userID#"/>
				-- AND
				-- 	si.IREType_int IN (1,2)
				AND
					sc.BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>
				AND
					sc.GroupId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpGroupId#"/>
				-- AND
				-- 	LENGTH(si.ContactString_vch) < 14
    		</cfquery>

    		<cfif GetSMSData.RECORDCOUNT GT 0>
    			<cfset dataout.SENT = (GetSMSData.SENT NEQ "" ? GetSMSData.SENT : 0)/>
    			<cfset dataout.QUEUED = (GetSMSData.QUEUED NEQ "" ? GetSMSData.QUEUED : 0)/>
    			<!--- <cfset dataout.RECEIVED = (GetSMSData.RECEIVED NEQ "" ? GetSMSData.RECEIVED : 0)/> --->
    		</cfif>

    		<cfset dataout.RXRESULTCODE = 1/>

    		<cfcatch type="any">
    			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
    			<cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
    			<cfset dataout.TYPE = "#cfcatch.TYPE#"/>
    		</cfcatch>
    	</cftry>

    	<cfreturn dataout/>
    </cffunction>

    <cffunction name="GetGroupDetailsForDatatableReport" access="remote" hint="get subcriberlist detail for campaign report">
   		<cfargument name="sColumns" default="" />
		<cfargument name="iDisplayStart" TYPE="numeric" required="false" default="1">
		<cfargument name="iDisplayLength" TYPE="numeric" required="false" default="20">
		<cfargument name="iSortCol_0" type="numeric" required="false" default="0">
		<cfargument name="sSortDir_0" type="string" required="false" default="asc">
		<cfargument name="iSortCol_1" type="numeric" required="false" default="0">
		<cfargument name="sSortDir_1" type="string" required="false" default="asc">
		<cfargument name="iSortCol_2" type="numeric" required="false" default="0">
		<cfargument name="sSortDir_2" type="string" required="false" default="asc">
		<cfargument name="iSortCol_3" type="numeric" required="false" default="0">
		<cfargument name="sSortDir_3" type="string" required="false" default="asc">
		<cfargument name="iSortCol_4" type="numeric" required="false" default="0">
		<cfargument name="sSortDir_4" type="string" required="false" default="asc">
		<cfargument name="iSortingCols" type="numeric" required="false" default="1">
		<cfargument name="bSortable_0" type="boolean" required="false" default=false>
		<cfargument name="bSortable_1" type="boolean" required="false" default=false>
		<cfargument name="bSortable_2" type="boolean" required="false" default=true>
		<cfargument name="bSortable_3" type="boolean" required="false" default=true>
		<cfargument name="bSortable_4" type="boolean" required="false" default=false>
		<cfargument name="customFilter" default="">
		<cfargument name="INPGROUPID" default="0">
		<cfargument name="inpKeyword" default="" type="string" />
		<cfargument name="inpBatchId" default="0">

		<cfset var filterItem	= '' />
		<cfset var contactList	= '' />
		<cfset var INPGROUPID	= arguments.INPGROUPID />
		<cfset var selectGroupList = '' />
		<cfset var rxContactList = ''/>

		<cfset var dataOut = {}>
		<cfset dataout.RXRESULTCODE = -1>
	    <cfset dataout.MESSAGE = "">
		<cfset dataout.ERRMESSAGE = "">
		<cfset dataout["DATALIST"] = []>
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
		<cfset var filterFields = {
			"CONTACTSTRING_VCH" = "simplelists.contactstring.ContactString_vch",
			"OPTINOUT_INT" = "OptInOut_int"
		}>

		<cfif INPGROUPID EQ 0 ||  INPGROUPID EQ "" >
			<cfreturn dataOut/>
		</cfif>

		<cftry>
			<!---get data here --->
			<cfset var contactCount = "">

			<cfquery name="contactCount" datasource="#Session.DBSourceREAD#">
				SELECT
					OptOut_dt,GroupId_bi,ContactString_vch, OptIn_dt
				FROM
				(
					SELECT
						OptOut_dt,GroupId_bi,ContactString_vch, OptIn_dt
					FROM
						simplelists.optinout
					WHERE
						GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">
					AND
						Batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#">
					AND
						OptOut_dt is null
					ORDER BY
						OptId_int DESC
				) tbl
				GROUP BY tbl.GroupId_bi, tbl.ContactString_vch
	        </cfquery>

			<cfset dataout["iTotalRecords"] = (contactCount.RECORDCOUNT GTE 1 ? contactCount.RECORDCOUNT : 0)>

			<cfif !isNumeric(dataout["iTotalRecords"])>
				<cfset dataout["iTotalRecords"] = 0>
			</cfif>
			<cfset dataout["iTotalDisplayRecords"] = dataout["iTotalRecords"]>

			<cfset var GetNumbers = "">

			<cfquery name="contactList" datasource="#Session.DBSourceREAD#" result="rxContactList">
				SELECT
					OptOut_dt,GroupId_bi,ContactString_vch, OptIn_dt , ContactId_bi
				FROM
				(
					SELECT
						opt.OptOut_dt,	opt.GroupId_bi,	opt.ContactString_vch, 	opt.OptIn_dt ,
						ctl.ContactId_bi
					FROM
						simplelists.optinout opt
					INNER JOIN
						simplelists.groupcontactlist gcl
						ON
						gcl.groupid_bi=opt.GroupId_bi
						<cfif arguments.inpBatchId GT 0>
							AND  opt.BatchId_bi =	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#">
						</cfif>
					INNER JOIN simplelists.contactstring cts
						ON
						gcl.contactaddressid_bi = cts.contactaddressid_bi
					INNER JOIN simplelists.contactlist ctl
						ON
						cts.contactid_bi = ctl.contactid_bi
						AND
						(
							cts.OptInSourceBatchId_bi =<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#">
							OR
							cts.OptOutSourceBatchId_bi =<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#">
						)
						 and cts.ContactString_vch = opt.ContactString_vch
					WHERE
						opt.GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">
					AND
						opt.Batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBatchId#">
					<cfif arguments.inpKeyword NEQ "">
	            		AND opt.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpKeyword#%">
            		</cfif>
            		AND
						opt.OptOut_dt is null

					ORDER BY
						opt.OptId_int DESC
				) tbl
				GROUP BY tbl.GroupId_bi, tbl.ContactString_vch,tbl.ContactId_bi

				LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
			</cfquery>
			<cfset var stt = iDisplayStart>
			<cfloop query="contactList">
 				<cfset var data = {
					stt = ++stt,
					Id = ContactId_bi,
					Contact = ContactString_vch,
					OptIn = (OptIn_dt NEQ '' ? DateFormat(OptIn_dt, 'mm/dd/yyyy ') & TimeFormat(OptIn_dt, 'hh:mm tt') : DateFormat(Created_dt, 'mm/dd/yyyy ') & TimeFormat(Created_dt, 'hh:mm tt')),
					Optout = (OptOut_dt NEQ '' ? DateFormat(OptOut_dt, 'mm/dd/yyyy ') & TimeFormat(OptOut_dt, 'hh:mm tt') : ''),
					GroupId = GroupId_bi
				} />
				<cfset dataout["DATALIST"].append(data) />
			</cfloop>
			<cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
			<cfset dataout["aaData"] = ArrayNew(1)>
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>

		<cfreturn dataOut>
    </cffunction>

    <cffunction name="getContactStringData" access="remote" output="false" hint="Get contactstring data and custom fields for edit">
    	<cfargument name="inpContactId" required="true" />

		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = 0/>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.TYPE = "" />
		<cfset dataout.FORM = "" />

    	<cfset var GetContactStringData = "" />
    	<cfset var ListContactStrings = "" />
    	<cfset var GetCustomFieldsData = "" />
    	<cfset var GetCustomFields = "" />
    	<cfset var GetListDefaultValuesCDFOfUser = "" />

    	<cfset var _PageContext = "" />
    	<cfset var _Response = "" />

    	<cftry>
	    	<cfquery name="GetContactStringData" datasource="#Session.DBSourceREAD#">
	    		SELECT
		    		ContactId_bi,
		    		UserId_int,
		    		Company_vch,
		    		FirstName_vch,
		    		LastName_vch,
		    		Address_vch,
		    		Address1_vch,
		    		City_vch,
		    		State_vch,
		    		ZipCode_vch,
		    		Country_vch,
		    		UserName_vch,
		    		Password_vch,
		    		UserDefinedKey_vch,
		    		Created_dt,
		    		LastUpdated_dt,
		    		DataTrack_vch,
		    		CPPID_vch,
		    		CPP_UUID_vch

	    		FROM
	    			simplelists.contactlist
	    		WHERE
	    			userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	    		AND
	    			contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpContactId#">
	    	</cfquery>

	    	<cfquery name="ListContactStrings" datasource="#Session.DBSourceREAD#">
	    		SELECT
		    		ContactAddressId_bi,
		    		ContactString_vch,
		    		ContactType_int,
		    		TimeZone_int
	    		FROM
	    			simplelists.contactstring
	    		WHERE
	    			contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpContactId#">
	    	</cfquery>

	    	<cfquery name="GetCustomFieldsData" datasource="#Session.DBSourceREAD#">
	    		SELECT
		    		VariableName_vch,
		    		VariableValue_vch,
		    		CdfId_int
	    		FROM
	    			simplelists.contactvariable
	    		WHERE
	    			contactid_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#GetContactStringData.ContactId_bi#">
	    		AND
	    			UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
	    	</cfquery>

	    	<cfquery name="GetCustomFields" datasource="#Session.DBSourceREAD#">
	    		SELECT
		    		CdfId_int,
		    		CdfName_vch as VariableName_vch,
		    		CdfDefaultValue_vch,
		    		type_field
	    		FROM
	    			simplelists.customdefinedfields
	    		WHERE
	    			userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
	    		ORDER BY
	    			CdfDefaultValue_vch ASC
	    	</cfquery>

	    	<cfquery name="GetListDefaultValuesCDFOfUser" datasource="#Session.DBSourceREAD#">
	    		SELECT
		    		df.id,
		    		cdf.CdfId_int,
		    		df.default_value
	    		FROM
	    			simplelists.customdefinedfields as cdf
	    		LEFT JOIN
	    			simplelists.default_value_custom_defined_fields as df
	    		ON
	    			cdf.CdfId_int = df.CdfId_int
	    		WHERE
	    			cdf.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
	    		AND
	    			cdf.type_field = 1
	    	</cfquery>

	    	<cfinclude template="subscribers.contact.fom" />

	    	<cfset _PageContext = GetPageContext() />
	    	<cfset _Response = _PageContext.getOut() />
	    	<cfset dataout.FORM = _Response.getString() />

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

    	<cfreturn dataout />
    </cffunction>

	<cffunction name="saveContactStringData" access="remote" output="false" hint="Set contactstring data and custom fields">
		<cfargument name="inpContactId" default="0"/>
		<cfargument name="inpContactString" default=""/>
		<cfargument name="inpContactType" default="0"/>

		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = 0/>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.TYPE = "" />

		<cfset var permissionStr = "" />
		<cfset var UpdateContactData = "" />
		<cfset var resultUpdateContact = "" />

		<cftry>
			<cfinvoke component="session.cfc.administrator.permission" method="checkContactPermission" returnvariable="permissionStr">
				<cfinvokeargument name="inpContactString" value="#inpContactString#"/>
				<cfinvokeargument name="inpContactType" value="#inpContactType#"/>
				<cfinvokeargument name="userId" value="#Session.UserId#"/>
				<cfinvokeargument name="operator" value="Edit Contact Details"/>
			</cfinvoke>

			<cfif NOT permissionStr.havePermission >
				<cfset dataout.MESSAGE = permissionStr.message />
				<cfreturn dataout/>
			</cfif>

			<cfinvoke method="UpdateContactData" component="session.sire.models.cfc.subscribers" returnvariable="resultUpdateContact">
				<cfinvokeargument name="theForm" value="#FORM#"/>
			</cfinvoke>

			<cfif resultUpdateContact.RXRESULTCODE GT 0>
				<cfset dataout.MESSAGE = "Edit Contact Successfully." />
				<cfset dataout.RXRESULTCODE = 1 />
			<cfelse>
				<cfset dataout.MESSAGE = "Edit Contacts failed: " & resultUpdateContact.MESSAGE />
			</cfif>

			<cfcatch>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>







    <!--- ************************************************************************************************************************* --->
    <!--- Update Contact Data --->
    <!--- ************************************************************************************************************************* --->

    <cffunction name="UpdateContactData" access="remote" output="false" hint="Update user specified data for the current contact">
        <cfargument name="theFORM" default="" type="struct">

        <cfset var dataout = '0' />
        <cfset var inpDesc = '' />
		<cfset var CurrentValue = '' />
		<cfset var UpdateContactStringData = '' />
		<cfset var GetCustomFields = '' />
		<cfset var GetContactStringData = '' />
		<cfset var InsertContactStringData = '' />

         <!---
        Positive is success
        1 = OK
        2 =
        3 =
        4 =

        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 =

         --->


        <cfoutput>

            <!--- move to just Lib number
            <cfif inpDesc EQ "">
                <cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
             --->

            <!--- Set default to error in case later processing goes bad --->
            <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
            <cfset QuerySetCell(dataout, "MESSAGE", "") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />

            <cftry>


                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>

                    <!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->

                    <cfif !Isdefined("theFORM.ContactId_bi")>
                        <cfthrow MESSAGE="Invalid Contact Id Specified #theFORM.ContactId_bi#" TYPE="Any" detail="" errorcode="-2">
                    </cfif>

                        <cfquery name="UpdateContactStringData" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                 simplelists.contactlist
                            SET
                                Company_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.Company_vch, 500)#" null="#IIF(TRIM(theFORM.Company_vch) EQ "", true, false)#">,
                                FirstName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.FirstName_vch, 90)#" null="#IIF(TRIM(theFORM.FirstName_vch) EQ "", true, false)#">,
                                LastName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.LastName_vch, 90)#" null="#IIF(TRIM(theFORM.LastName_vch) EQ "", true, false)#">,
                                Address_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.Address_vch, 250)#" null="#IIF(TRIM(theFORM.Address_vch) EQ "", true, false)#">,
                                Address1_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.Address1_vch, 100)#" null="#IIF(TRIM(theFORM.Address1_vch) EQ "", true, false)#">,
                                City_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.City_vch, 100)#" null="#IIF(TRIM(theFORM.City_vch) EQ "", true, false)#">,
                                State_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.State_vch, 50)#" null="#IIF(TRIM(theFORM.State_vch) EQ "", true, false)#">,
                                ZipCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.ZipCode_vch, 20)#" null="#IIF(TRIM(theFORM.ZipCode_vch) EQ "", true, false)#">,
                                Country_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.Country_vch, 100)#" null="#IIF(TRIM(theFORM.Country_vch) EQ "", true, false)#">,
                                UserDefinedKey_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(theFORM.UserDefinedKey_vch, 2048)#" null="#IIF(TRIM(theFORM.UserDefinedKey_vch) EQ "", true, false)#">,
                                LastUpdated_dt = NOW()
                            WHERE
                                simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                            AND
                                simplelists.contactlist.contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#theFORM.ContactId_bi#">
                        </cfquery>

                        <!--- <cfquery name="GetCustomFields" datasource="#Session.DBSourceEBM#">

                            SELECT DISTINCT
                                VariableName_vch,
                                CDFId_int
                            FROM
                                simplelists.contactvariable
                                INNER JOIN simplelists.contactlist on simplelists.contactlist.contactid_bi = simplelists.contactvariable.contactid_bi
                            WHERE
                                simplelists.contactlist.userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                            ORDER BY
                                VariableName_vch DESC

                        </cfquery> --->
                        <cfquery name="GetCustomFields" datasource="#Session.DBSourceEBM#">
                            SELECT
                                CdfId_int,
                                CdfName_vch as VariableName_vch,
                                CdfDefaultValue_vch,
                                type_field
                            FROM
                                simplelists.customdefinedfields
                            WHERE
                                userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                            ORDER BY
                                CdfDefaultValue_vch ASC
                        </cfquery>

                        <cfloop query="GetCustomFields">


                            <!--- What about CDFs with no CDFID ?--->

                            <cfif GetCustomFields.CdfId_int NEQ "">

                                <cfset CurrentValue = Evaluate("theFORM.#GetCustomFields.CdfId_int#")>
                           <cfelse>
                                 <!--- What about older style CDFs with no CDFID ?--->
                                <cfset CurrentValue = "" />
                            </cfif>

                            <!--- Self repair if values get delete or not created somehow--->
                             <cfquery name="GetContactStringData" datasource="#Session.DBSourceEBM#">
                                SELECT
                                    COUNT(CdfId_int)  AS TotalCount
                                FROM
                                    simplelists.contactvariable
                                WHERE
                                    CdfId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCustomFields.CdfId_int#">
                                AND
                                    simplelists.contactvariable.contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#theFORM.ContactId_bi#">
                            </cfquery>

                            <cfif GetContactStringData.TotalCount GT 0>

                                <cfquery name="UpdateContactStringData" datasource="#Session.DBSourceEBM#">
                                    UPDATE
                                        simplelists.contactvariable
                                    SET
                                        VariableValue_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CurrentValue, 5000)#" null="#IIF(TRIM(CurrentValue) EQ "", true, false)#">                                    ,
                                        Created_dt = NOW()
                                    WHERE
                                        CdfId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCustomFields.CdfId_int#">
                                    AND
                                        simplelists.contactvariable.contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#theFORM.ContactId_bi#">
                                </cfquery>

                            <cfelse>

                                <!--- Dont insert empty records on edit --->
                                <cfif TRIM(CurrentValue) NEQ "">
                                    <cfquery name="InsertContactStringData" datasource="#Session.DBSourceEBM#">
                                        INSERT INTO
                                            simplelists.contactvariable
                                        (
                                            ContactId_bi,
                                            VariableName_vch,
                                            VariableValue_vch,
                                            UserId_int,
                                            Created_dt,
                                            CdfId_int
                                        )
                                        VALUES
                                        (
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#theFORM.ContactId_bi#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCustomFields.VariableName_vch#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CurrentValue, 5000)#" null="#IIF(TRIM(CurrentValue) EQ "", true, false)#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                                            NOW(),
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCustomFields.CdfId_int#">
                                        )

                                    </cfquery>

                                 </cfif>

                            </cfif>



                        </cfloop>


                        <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />

                  <cfelse>

                    <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />

                  </cfif>

            <cfcatch TYPE="any">

                <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />

            </cfcatch>

            </cftry>


        </cfoutput>

        <cfreturn dataout />
    </cffunction>

    	<cffunction name="getSubscriber4Export" output="false" hint="get contact data for export to csv">
		<cfargument name="inpContactId" type="numeric" required="true">
		<cfargument name="inpUserID" type="numeric" required="false" default="#Session.UserId#">

		<cfset var GetContactStringData = '' />
		<cfset var GetCustomFieldsData = '' />
		<cfset var GetCustomFields = '' />
		<cfset var carrierQuery = '' />
		<cfset var subscriber = '' />
		<cfset var operatorName = ''>
		<cfset var lastUpdateDate = ''>

		<cfquery name="GetContactStringData" datasource="#Session.DBSourceREAD#">
			SELECT
				CS.LastUpdated_dt,
				CS.contactid_bi,
				CS.OperatorId_int,
				BIT_OR(IF(IO.OptOut_dt IS NOT NULL, 0, 1)) AS OptInOut_int,
				MAX(IO.OptIn_dt) AS OptIn_dt,
				IO.BatchId_bi
			FROM
				simplelists.contactstring AS CS

			LEFT JOIN
				simplelists.optinout AS IO
			ON
				(
					IO.OptOut_dt IS NOT NULL
				OR
					IO.BatchId_bi IN (SELECT BatchId_bi FROM simpleobjects.batch WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">)
				)
			AND
				CS.ContactType_int = 3
			AND
				CS.ContactString_vch = IO.ContactString_vch
			WHERE
				CS.contactid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpContactId#">
			GROUP BY CS.ContactAddressId_bi
			LIMIT 1
		</cfquery>

		<cfquery name="GetCustomFieldsData" datasource="#Session.DBSourceREAD#">
			SELECT

				case
					when cdf.CdfName_vch is not null
					then cdf.CdfName_vch
					else c.VariableName_vch
				end
				as VariableName_vch,
				c.VariableValue_vch,
				c.CdfId_int
			FROM
				simplelists.contactvariable c
			INNER JOIN
				simplelists.customdefinedfields cdf
			ON
				c.UserId_int=cdf.UserId_int
			AND
				c.CdfId_int=cdf.CdfId_int
			WHERE
				c.contactid_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#GetContactStringData.ContactId_bi#">
			AND
				c.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserID#">
		</cfquery>

		<cfquery name="carrierQuery" datasource="#Session.DBSourceREAD#">
			SELECT
				Operator_vch
			FROM
				sms.carrierlist
			WHERE
				OperatorId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetContactStringData.OperatorId_int#">
			LIMIT 1
		</cfquery>

		<cfif carrierQuery.RecordCount GT 0>
			<cfloop query="#carrierQuery#">
				<cfset operatorName = Operator_vch />
			</cfloop>
		</cfif>

		<cfif GetContactStringData.RecordCount GT 0>
			<cfloop query="#GetContactStringData#">
				<cfif GetContactStringData.OptIn_dt GT ''>
					<cfset lastUpdateDate = dateFormat(GetContactStringData.OptIn_dt, "mm/dd/yyyy")>
				<cfelse>
					<cfset lastUpdateDate = dateFormat(GetContactStringData.LastUpdated_dt, "mm/dd/yyyy") />
				</cfif>
			</cfloop>
		</cfif>

		<cfreturn {
			"GetContactStringData": GetContactStringData,
			"GetCustomFieldsData": GetCustomFieldsData,
			"carrierQuery" : carrierQuery,
			"operatorName": operatorName,
			"lastUpdateDate" : lastUpdateDate
		} />


	</cffunction>

</cfcomponent>
