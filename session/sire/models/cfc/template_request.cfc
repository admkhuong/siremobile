<cfcomponent>

    <cfinclude template="/public/sire/configs/paths.cfm">


<!---     <cffunction name="SendTemplateRequestEmail" access="remote" output="true" hint="Users send their template requests email">
        <cfargument name="inpUserId" required="true" type="string" default="">
        <cfargument name="inpUserName" required="true" type="string" default="">
        <cfargument name="inpUserEmail" required="true" type="string" default="">
        <cfargument name="inpTemplateName" required="true" type="string" default="">
        <cfargument name="inpTemplateContent" required="true" type="string" default="">
        <cfargument name="inpTotalQuestion" required="true" type="numeric" default="">
        <cfargument name="inpBranch" required="true" type="string" default="">

        <cfset var GetEMSAdmins = '' />
        <cfset var EBMAdminEMSList = '' />

        <cfinclude template="../../../../public/paths.cfm">
        <cfinclude template="../../../../public/sire/configs/paths.cfm">

        <cfset var dataout = {}>
        <cfset dataout.RXRESULTCODE = 0 />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />

        <cfif TRIM(arguments.inpUserId) NEQ "" AND TRIM(arguments.inpUserEmail) NEQ "" AND TRIM(arguments.inpTemplateContent) NEQ "" AND TRIM(arguments.inpTemplateName) NEQ "">

            <cfquery name="GetEMSAdmins" datasource="#Session.DBSourceREAD#">
                SELECT
                    ContactAddress_vch                              
                FROM 
                    simpleobjects.systemalertscontacts
                WHERE
                    ContactType_int = 2
                AND
                    EMSNotice_int = 1    
            </cfquery>
           
            <cfif GetEMSAdmins.RecordCount GT 0>
                <cfset EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>
            <cfelse>
                <cfset EBMAdminEMSList = SupportEMailFrom>                            
            </cfif>

            <cftry>

                <cfmail to="#EBMAdminEMSList#" subject="SIRE Template Request" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                    <cfoutput>
                        SIRE user template request detail:<br><br>
                        <strong>User Information:</strong>
                        <ul>
                            <li>User ID: #arguments.inpUserId#</li>
                            <li>User Name: #arguments.inpUserName#</li>
                            <li>User email: #arguments.inpUserEmail#</li>
                        </ul>
                        <strong>Template Content:</strong>
                        <ul>
                            <li>Template Name: #arguments.inpTemplateName#</li>
                            <li>Total question: #arguments.inpTotalQuestion#</li>
                            #arguments.inpTemplateContent#
                            <li>Template Branch: #arguments.inpBranch#</li>
                        </ul>
                    </cfoutput>
                </cfmail>
                <cfset dataout.RXRESULTCODE = 1>
                <cfset dataout.MESSAGE = "Send request successfully!" />
                <cfcatch type="any">
                    <cfset dataout.RXRESULTCODE = -1>
                    <cfset dataout.MESSAGE = "Send request failed!" />
                    <cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail />
                </cfcatch>
            </cftry>
        <cfelse>
                <cfset dataout.RXRESULTCODE = -2>
                <cfset dataout.MESSAGE = "Send request failed because of wrong infomation!" />
                <cfreturn dataout>
        </cfif>

        <cfreturn dataout>
    </cffunction> --->

    <cffunction name="TemplateEmailReceived" access="remote" output="false" hint="SIRE send emails to users when support team received template request">
        <cfargument name="inpUserFirstName" required="true" type="string" default="">
        <cfargument name="inpUserEmail" required="true" type="string" default="">
        <cfargument name="ticket" required="true">

        <cfset var data = {} />

        <cfset data = {
            UserFirstName: inpUserFirstName,
            Ticket: ticket
        }/>

        <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
            <cfinvokeargument name="to" value="#arguments.inpUserEmail#">
            <cfinvokeargument name="type" value="html">
            <cfinvokeargument name="subject" value="Sire received your template request!">
            <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_template_request.cfm">
            <cfinvokeargument name="data" value="#TRIM(serializeJSON(data))#">
        </cfinvoke>

        
    </cffunction>

    <!--- function call API not work --->
   <!---  <cffunction name="SendTemplateRequestEmail" access="remote" output="true" hint="Users send their template requests email">
        <cfargument name="inputBatchId" required="no" default="0" type="numeric">
        <cfargument name="inputUserId" required="yes" type="numeric">
        <cfargument name="inputContactString" required="yes" default="" type="string">
        <cfargument name="inputListOfMails" required="no" default="">
        <cfargument name="inputMessageText" required="yes" type="string">

        <cfset var dataout = {} />
        <cfset var variables = {} />
        <cfset var GetCredentials = '' />
        <cfset var returnStruct = {} />

        <cfquery name="GetCredentials" datasource="#Session.DBSourceEBM#">
            SELECT 
                AccessKey_vch,
                SecretKey_vch,
                Active_ti
            FROM 
                simpleobjects.securitycredentials
            WHERE
                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#arguments.inputUserId#">
            AND 
                Active_ti = 1
            LIMIT 1
        </cfquery>

        <cfset variables.accessKey = "D40C950F2536AF0D7EC6" />
        <cfset variables.secretKey = "08AA/b9Ec31/285B26aD54A7Bd7Eafed/98E3D95" />

        <cfset variables.sign = generateSignature("POST", variables.secretKey) />
        <cfset var datetime = variables.sign.DATE>
        <cfset var signature = variables.sign.SIGNATURE>
        <cfset var authorization = variables.accessKey & ":" & signature>

        <cfhttp url="https://apiqa.siremobile.com/webservice/ebm/support/sireRequest" method="POST" result="returnStruct" >
           
           <!--- Required components --->
        
           <!--- By default EBM API will return json or XML --->
           <cfhttpparam name="Accept" type="header" value="application/json" />     
           <cfhttpparam type="header" name="datetime" value="#datetime#" />
           <cfhttpparam type="header" name="authorization" value="#authorization#" /> 
                         
           <!--- Batch Id controls which pre-defined campaign to run --->
           <cfhttpparam type="formfield" name="inpBatchId" value="0" />
           
           <!--- Contact string--->
           <cfhttpparam type="formfield" name="inpContactString" value="#arguments.inputContactString#" />
           
           
           <!--- ***TODO: Remove this after adding parse for DNC STOP Requests --->
           <cfhttpparam type="formfield" name="inpListOfMails" value="#arguments.inputListOfMails#" />
           
           <!--- Optional Components --->
           
           <!--- Custom data element for PDC Batch Id 1135 --->
           <cfhttpparam type="formfield" name="inpMessageText" value="#arguments.inputMessageText#" />

           <cfhttpparam type="formfield" name="inpUserId" value="#arguments.inputUserId#" />
          
        </cfhttp>

        <!--- <cfdump var="#returnStruct#" abort="true" > --->

        <cfset dataout = returnStruct.Filecontent />

        <cfreturn dataout />

    </cffunction> --->

    <cffunction name="SendTemplateRequestEmail" access="remote" output="true" hint="Users send their template requests email">
        <cfargument name="inpBatchId" required="no" default="0" type="numeric">
        <cfargument name="inpUserId" required="yes" type="numeric">
        <cfargument name="inpContactString" required="yes" default="" type="string">
        <cfargument name="inpListOfMails" required="no" default="">
        <cfargument name="inpMessageText" required="yes" type="string">
        <cfargument name="inpUserEmail" required="yes" type="string">
        <cfargument name="inpUserFirstname" required="yes" type="string">
        <cfargument name="inpUserFullname" required="yes" type="string">

        <cfset var dataout = {} />
        <cfset var ENA_Message = '' />
        <cfset var SubjectLine = '' />
        <cfset var TroubleShootingTips = '' />
        <cfset var ErrorNumber = '' />
        <cfset var AlertType = '' />
        <cfset var InsertToEventStatus = '' />
        <cfset var InsertToErrorLog = '' />
        <cfset var insertResult = '' />
        <cfset var validationResult = '' />
        <cfset var DBSourceEBM = "Bishop"/> 
                    
        <!--- Formatted Date Time --->
        <cftry>
           
        
            <!--- Replace any existing status for today with current status (use update with insert? ) ---> 
        
            <!---
                
                
                {inpBatchId:"{%SRBATCHID%}",inpContactString:"{%SRCONTACTSTRING%}",inpListOfEmails:"lee@siremobile.com",inpMessageText:"{%INPPA=2%}"}

{
    "inpBatchId": "{%SRBATCHID%}",
    "inpContactString": "{%SRCONTACTSTRING%}",
    "inpListOfEmails": "lee@siremobile.com",
    "inpMessageText": "{%INPPA=2%}"
}

                
                simplequeue.`supportapp` (
  `PKId_bi` bigint(20) NOT NULL AUTO_INCREMENT,
  `BatchId_bi` bigint(20) NOT NULL,
  `Status_int` int(11) NOT NULL,
  `Event_date` date NOT NULL,
  `Created_dt` datetime NOT NULL,
  `ContactString_vch` varchar(255) DEFAULT NULL,
  `CustomerMessage_vch` text,
  
            --->
            <cfinvoke method="VldContent" component="public.cfc.validation" returnvariable="validationResult">
                <cfinvokeargument name="Input" value="#arguments.inpMessageText#"/>
            </cfinvoke>

            <cfif validationResult NEQ true>
                <cfset dataout.RXRESULTCODE = -4>
                <cfset dataout.MESSAGE = 'Please do not input special character.'>
                <cfreturn dataout>
            </cfif>

            <!--- Add a status update for today --->                          
            <cfquery name="InsertToEventStatus" datasource="#DBSourceEBM#" result="insertResult">              
              
              INSERT INTO 
                simplequeue.supportapp 
                (                        
                    BatchId_bi, 
                    Status_int, 
                    Event_date, 
                    Created_dt,
                    LastUpdated_dt,
                    ContactString_vch,
                    CustomerMessage_vch,
                    UserId_int
                )
                VALUES
                (                   
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpBatchId#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                    NOW(),
                    NOW(),
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(arguments.inpContactString)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpMessageText#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                     
                 )             
            </cfquery>
            
            <!--- Send back new trouble ticket number - if this number is 0 then branch to no number available logic --->
            <cfif insertResult.GENERATED_KEY GT 0>
                <cfset dataout.GENERATEDKEY = insertResult.GENERATED_KEY />
            <cfelse>
                <cfset dataout.GENERATEDKEY = 0> 
            </cfif>
            
        
            <cfinvoke method="TemplateEmailReceived">
                <cfinvokeargument name="inpUserFirstName" value="#arguments.inpUserFirstname#">
                <cfinvokeargument name="inpUserEmail" value="#arguments.inpUserEmail#">
                <cfinvokeargument name="ticket" value="#dataout.GENERATEDKEY#">
            </cfinvoke>


            <cfmail to="#SupportEMailFrom#" subject="Request Custom Template from #arguments.inpUserFullname#" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                <html>
                    <head>
                        <meta charset="utf-8">
                        <title>Support Request</title>
                    </head>
                
                    <body style="font-family: Arial; font-size: 12px;">

                        <table style="width: 600px; margin: 0 auto;" cellpadding="0" cellspacing="0">
                        
                            <tbody style="margin: 0; padding: 0;">
                                <tr>
                                    <td colspan="2" style="padding: 0 30px;">
                
                                        <p style="font-size: 16px;color: ##5c5c5c;">Dear Support,</p>
                                        
                                        <br>
                
                                        <p style="font-size: 18px;color: ##74c37f; text-align:center; font-weight:bold">Request Custom Template</p>
                                        <hr style="border: 0;border-bottom: 1px solid ##74c37f;">
                
                                        <table style="width:100%">
                                            <!--- <tr>
                                                <td style="width:40%">
                                                    <p style="font-size: 16px;color: ##5c5c5c;">Batch Id:</p>
                                                </td>
                                                <td style="width:60%">
                                                    <p style="font-size: 16px;color: ##5c5c5c;"><cfoutput>#inpBatchId#</cfoutput></p>               
                                                </td>
                                            </tr> --->

                                            <tr>
                                                <td style="width:40%">
                                                    <p style="font-size: 16px;color: ##5c5c5c;">Contact From: </p>
                                                </td>
                                                <td style="width:60%">
                                                    <p style="font-size: 16px;color: ##5c5c5c;"><cfoutput>#arguments.inpContactString#</cfoutput></p>               
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="width:40%">
                                                    <p style="font-size: 16px;color: ##5c5c5c;">E-mail Address: </p>
                                                </td>
                                                <td style="width:60%">
                                                    <p style="font-size: 16px;color: ##5c5c5c;"><cfoutput>#arguments.inpUserEmail#</cfoutput></p>               
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="width:40%">
                                                    <p style="font-size: 16px;color: ##5c5c5c;">Message From Customer:</p>
                                                </td>
                                                <td style="width:60%">
                                                    <p style="font-size: 16px;color: ##5c5c5c;"><cfoutput>#reReplace(arguments.inpMessageText, '\n', '<br />', 'ALL')#</cfoutput></p>              
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="width:40%">
                                                    <p style="font-size: 16px;color: ##5c5c5c;">Ticket Number:</p>
                                                </td>
                                                <td style="width:60%">
                                                    <p style="font-size: 16px;color: ##5c5c5c;"><cfoutput>#dataout.GENERATEDKEY#</cfoutput></p>                  
                                                </td>
                                            </tr>
                                            
                                        </table>
                                                                                    
                                    </td>
                                </tr>   
                            </tbody>
                                       
                        </table>

                    </body>
                
                </html>     
            </cfmail>

            <cfset dataout.RXRESULTCODE = 1>
            <cfset dataout.MESSAGE = 'Your template request has been sent to Sire! We will be in touch with you shortly.'>
            <cfreturn dataout />
    
                                    
                                                           
            <cfcatch>           
                                
                            <!--- For debbugging only! --->    
                            <!---  <cfset dataout = SerializeJSON(cfcatch) />    --->             
                <cftry>        
        
                    <cfset ENA_Message = "">
                    <cfset SubjectLine = "SimpleX Support/Sire API Notification">
                    <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                    <cfset ErrorNumber="1110">
                    <cfset AlertType="1">
                                   
                    <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                        INSERT INTO simplequeue.errorlogs
                        (
                            ErrorNumber_int,
                            Created_dt,
                            Subject_vch,
                            Message_vch,
                            TroubleShootingTips_vch,
                            CatchType_vch,
                            CatchMessage_vch,
                            CatchDetail_vch,
                            Host_vch,
                            Referer_vch,
                            UserAgent_vch,
                            Path_vch,
                            QueryString_vch
                        )
                        VALUES
                        (
                            #ErrorNumber#,
                            NOW(),
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,     
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                        )
                    </cfquery>
                
                <cfcatch type="any">
                    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
                </cfcatch>
                    
                </cftry>    
                
                <cfreturn dataout />
            </cfcatch>         
                
        </cftry>
        
        <cfreturn dataout />
    </cffunction>



    <cffunction name="generateSignature" returnformat="json" access="private" output="false" hint="generate signature by secretkey">
        <cfargument name="method" required="true" type="string" default="GET" hint="Method" />
        <cfargument name="secretKey" required="true" type="string" default="gsdagadfgadsfgsdfgdsfgsdfg" hint="Secret Key" />
                 
        <cfset var retval = {} />                 
        <cfset var dateTimeString = GetHTTPTimeString(Now())>
        
        <!--- Create a canonical string to send --->
        <cfset var cs = "#arguments.method#\n\n\n#dateTimeString#\n\n\n">
        <cfset var signature = createSignature(cs,secretKey)>
        
        <cfset retval.SIGNATURE = signature>
        <cfset retval.DATE = dateTimeString>
        
        <cfreturn retval>
    </cffunction>

    <cffunction name="createSignature" returntype="string" access="private" output="false" hint="Create signature">
        <cfargument name="stringIn" type="string" required="true" />
        <cfargument name="scretKey" type="string" required="true" />
        <!--- Replace "\n" with "chr(10) to get a correct digest --->
        <cfset var fixedData = replace(arguments.stringIn,"\n","#chr(10)#","all")>
        <!--- Calculate the hash of the information --->
        <cfset var digest = HMAC_SHA1(scretKey,fixedData)>
        <!--- fix the returned data to be a proper signature --->
        <cfset var signature = ToBase64("#digest#")>
        
        <cfreturn signature>
    </cffunction>

    <cffunction name="HMAC_SHA1" returntype="binary" access="private" output="false" hint="NSA SHA-1 Algorithm">
       <cfargument name="signKey" type="string" required="true" />
       <cfargument name="signMessage" type="string" required="true" />
    
       <cfset var jMsg = JavaCast("string",arguments.signMessage).getBytes("iso-8859-1") />
       <cfset var jKey = JavaCast("string",arguments.signKey).getBytes("iso-8859-1") />
       <cfset var key = createObject("java","javax.crypto.spec.SecretKeySpec") />
       <cfset var mac = createObject("java","javax.crypto.Mac") />
    
       <cfset key = key.init(jKey,"HmacSHA1") />
       <cfset mac = mac.getInstance(key.getAlgorithm()) />
       <cfset mac.init(key) />
       <cfset mac.update(jMsg) />
    
       <cfreturn mac.doFinal() />
    </cffunction>

</cfcomponent>
