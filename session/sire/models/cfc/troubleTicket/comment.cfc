<cfcomponent hint="Trouble ticket comment" output="false">
    <cfinclude template="/public/sire/configs/paths.cfm" >

    <cffunction name="GetListComments" access="remote" output="false" hint="Get list Ticket comment">
		<cfargument name="inpTicketId" TYPE="string" required="yes" default=""  hint="Trouble ticket Id"/>
		<cfargument name="inpSkipPaging" TYPE="string" required="no" default="0" />
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />

		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default="DESC"/>	

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout["ListTroubleTicketComment"] = ArrayNew(1)/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />

		<cfset var item = '' />
		<cfset var GetListComments = '' />
		<cfset var rsCount = '' />

		<cfset var orderField = '' />
		<cfswitch expression="#arguments.iSortCol_0#">		    
		    <cfcase value="0">
		        <cfset orderField = 'cm.PKId_bi' />
		    </cfcase>	
            <cfdefaultcase>
                <cfset orderField = 'cm.PKId_bi' />
            </cfdefaultcase>	    	    
		</cfswitch>

		<cftry>								
			<cfquery name="GetListComments" datasource="#Session.DBSourceREAD#">
                SELECT 
                    SQL_CALC_FOUND_ROWS
                    cm.PKId_bi,
                    cm.UserId_int,
                    cm.TicketId_bi,
                    cm.Content_txt,
                    cm.Created_dt,
                    u.FirstName_vch,
                    u.LastName_vch
                FROM 
                    troubletickets.comments	cm
                LEFT JOIN
                    simpleobjects.useraccount u
                ON
                    cm.UserId_int= u.UserId_int				
				WHERE
					cm.TicketId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpTicketId#"/>                
				<cfif orderField NEQ "">
					ORDER BY
						#orderField# DESC
				<cfelse>
					ORDER BY 
						cm.PKId_bi DESC
				</cfif>
				LIMIT 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#"/>, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"/>
			</cfquery>	
			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>		
			<cfif GetListComments.RecordCount GT 0>
				<cfloop query="GetListComments">
					<cfset item = {
						ID: PKId_bi, 
                        USERID:UserId_int,
                        USERFULLNAME: FirstName_vch & " " & LastName_vch,
                        CREATEDATE: LSDateFormat(Created_dt, "yyyy-MM-dd") & " " & LSTimeFormat(Created_dt, "HH:mm:ss"),
                        CONTENT: REReplaceNoCase(Content_txt,"#chr(10)#","<br>","ALL")
					} />
					<cfset arrayAppend(dataout["ListTroubleTicketComment"],item) />
				</cfloop>
				
				<cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
				<cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
    <cffunction name="InsertUpdateComment" access="remote" output="false" hint="insert new comment or update comment">
		<cfargument name="inpId" TYPE="numeric" required="no" default="0" hint = "Comment id (0: create new comment, else update comment)"/>
        <cfargument name="inpTicketId" TYPE="numeric" required="yes" default="0" hint ="Trouble ticket Id"/>        
		<cfargument name="inpContent" TYPE="string" required="yes" default="" hint="User's comment" />      
		<cfargument name="inpUserId" TYPE="string" required="no" default="#session.UserId#" />
			

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />

		<cfset var InsertUpdateComment = ""/>
        <cfset var logcontent = ""/>
        
		<cftry>		    
            
			<cfif arguments.inpId is 0>
				<cfquery name="InsertUpdateComment" datasource="#Session.DBSourceEBM#">
					INSERT INTO troubletickets.comments
					(			
                        TicketId_bi,                        			
                        UserId_int,
                        Content_txt,
                        Created_dt			  	
					)
					VALUES
					(			
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpTicketId#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpUserId#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpContent)#">,			
						NOW()		
					)
				</cfquery>					
                <cfset logcontent = "Add comment"/>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = "Add comment successfully."/>
			<cfelse>
				<cfquery name="InsertUpdateComment" datasource="#Session.DBSourceEBM#">
					UPDATE troubletickets.comments
					SET						                        	                        
                        Content_txt= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpContent)#">		
					WHERE
						PKId_bi=<CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpId#">
                    AND 
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#"/>
				</cfquery>		
                <cfset logcontent = "Edit comment"/>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = "Edit comment successfully."/>
			</cfif>

            <!--- Create log user --->
			<cfinvoke method="CreateUserLog" component="session.sire.models.cfc.troubleTicket.trouble-ticket">
				<cfinvokeargument name="inpTicketId" value="#arguments.inpTicketId#">
				<cfinvokeargument name="inpNote" value="#logcontent#">
				<cfinvokeargument name="inpActionName" value="Trouble Ticket - #logcontent#">
			</cfinvoke>

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
    <cffunction name="DeleteCommentByID" access="remote" output="false" hint="delete ticket comment by id">
		<cfargument name="inpId" type="numeric" required="true" hint="id of comment"/>
		<cfargument name="inpTicketId" type="numeric" required="true" hint="id of ticket"/>

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = ""/>
		
		<cfset var DeleteComment = '' />

		<cftry>
            <cfquery result="DeleteComment" datasource="#Session.DBSourceEBM#">
                DELETE FROM 
                    troubletickets.comments                
                WHERE
                    PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpId#"/>
                AND
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#"/>
            </cfquery>

            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.MESSAGE = "Delete this comment successfully." />

            <!--- Create log user --->
           	<cfinvoke method="CreateUserLog" component="session.sire.models.cfc.troubleTicket.trouble-ticket">
				<cfinvokeargument name="inpTicketId" value="#arguments.inpTicketId#">
				<cfinvokeargument name="inpNote" value="Delete comment">
				<cfinvokeargument name="inpActionName" value="Trouble Ticket - Delete comment">
			</cfinvoke>

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>
	</cffunction>
	
    
</cfcomponent>	