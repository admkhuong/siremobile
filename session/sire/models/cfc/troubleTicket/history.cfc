<cfcomponent hint="Trouble ticket history" output="false">
    <cfparam name="Session.DBSourceEBM" default="BISHOP"/>	
    <cfinclude template="/session/sire/configs/paths.cfm">
    <cffunction name="GetListHistory" access="remote" output="false" hint="Get list Ticket history log">
		<cfargument name="inpTicketId" TYPE="string" required="yes" default="" />
		<cfargument name="inpSkipPaging" TYPE="string" required="no" default="0" />
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="100" />

		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default=""/>		
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout["aaData"] = ArrayNew(1)/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />
        <cfset dataout.TOTALTIME = '0' />

		<cfset var item = '' />
		<cfset var GetListHistory = '' />
		<cfset var rsCount = '' />
		<cfset var noteFormat = '' />

		<cfset var orderField = '' />
		<cfswitch expression="#arguments.iSortCol_0#">		    
		    <cfcase value="0">
		        <cfset orderField = 'LogId_int' />
		    </cfcase>	
            <cfdefaultcase>
                <cfset orderField = 'LogId_int' />
            </cfdefaultcase>	    	    
		</cfswitch>

		<cftry>								
			<cfquery name="GetListHistory" datasource="#Session.DBSourceREAD#">
                SELECT 
                    SQL_CALC_FOUND_ROWS
					h.TicketId_bi,
                    h.Note_vch,
					h.ActionName_vch,
					h.Created_dt,
					u.UserId_int,
                    u.FirstName_vch,
                    u.LastName_vch
                FROM 
                    troubletickets.history	h
                LEFT JOIN
                    simpleobjects.useraccount u
                ON
                    h.UserId_int= u.UserId_int				
				WHERE
					h.ActionName_vch LIKE "Trouble Ticket%"
				AND
					h.TicketId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpTicketId#"/>                
				ORDER BY 
					h.PKId_bi DESC
                <cfif arguments.inpSkipPaging EQ 0>
				LIMIT 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#"/>, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"/>
                </cfif>
			</cfquery>	
			
			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>		
			<cfif GetListHistory.RecordCount GT 0>
				<cfloop query="GetListHistory">                    
					<cfset item = {
												
                        USERID:UserId_int,
                        USER_FULL_NAME: FirstName_vch & " " & LastName_vch,
                        DATE: LSDateFormat(Created_dt, "yyyy-mm-dd") & " " & LSTimeFormat(Created_dt, "HH:mm"),                                                
						NOTES: Note_vch
					} />
					<cfset arrayAppend(dataout["aaData"],item) />
				</cfloop>
				
				<cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
				<cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>
			</cfif>
			

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
	    
</cfcomponent>	