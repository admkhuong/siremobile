<cfcomponent hint="Trouble ticket type" output="false">
    <cfparam name="Session.DBSourceEBM" default="BISHOP"/>	
    <cfinclude template="/session/sire/configs/paths.cfm">
    <cffunction name="GetListTicketType" access="remote" output="false" hint="Get list Ticket Type">
		<cfargument name="inpUserId" TYPE="string" required="no" default="#session.UserId#" />
		<cfargument name="inpType" TYPE="string" required="no" default=""  />
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />

		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default=""/>		
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout["aaData"] = ArrayNew(1)/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />

		<cfset var item = '' />
		<cfset var GetListType = '' />
		<cfset var rsCount = '' />

		<cfset var orderField = '' />
		<cfswitch expression="#arguments.iSortCol_0#">		    
		    <cfcase value="0">
		        <cfset orderField = 'TypeName_vch' />
		    </cfcase>	
            <cfdefaultcase>
                <cfset orderField = 'TypeName_vch' />
            </cfdefaultcase>	    	    
		</cfswitch>

		<cftry>								
			<cfquery name="GetListType" datasource="#Session.DBSourceREAD#">
                SELECT 
                    SQL_CALC_FOUND_ROWS
                    PKId_bi,
                    UserId_int,
                    TypeName_vch,
                    TypeStatus_ti
                FROM 
                    troubletickets.type	t				
				WHERE
					t.TypeStatus_ti = 1
                AND
                    UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/>
				<cfif TRIM(arguments.inpType) neq "">
					AND
					t.TypeName_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpType#%">
				</cfif>					
				<cfif orderField NEQ "">
					ORDER BY
						#orderField# #arguments.sSortDir_0#
				<cfelse>
					ORDER BY 
						t.PKId_bi DESC
				</cfif>

				LIMIT 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#"/>, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"/>
			</cfquery>	
			
			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>		
			<cfif GetListType.RecordCount GT 0>
				<cfloop query="GetListType">
					<cfset item = {
						ID: PKId_bi, 
						TYPE: TypeName_vch,
                        USERID:UserId_int
					} />
					<cfset arrayAppend(dataout["aaData"],item) />
				</cfloop>
				
				<cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
				<cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>
			</cfif>
			

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
    <cffunction name="InsertUpdateTicketType" access="remote" output="false" hint="insert new type">
		<cfargument name="inpId" TYPE="numeric" required="no" default="0"/>
		<cfargument name="inpType" TYPE="string" required="yes" default="" />
		<cfargument name="inpUserId" TYPE="string" required="no" default="#session.UserId#" />
			

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />		

		<cfset var InsertUpdateTicketType = ""/>
        <cfset var CheckTypeExist = ""/>        

		<cfif arguments.inpType is "">
			<cfset dataout.RXRESULTCODE = -1 />
   		    <cfset dataout.TYPE = "" />
   		    <cfset dataout.MESSAGE = "Error Blank Type" />
   		    <cfset dataout.ERRMESSAGE = "Error Blank Type" />
			<cfreturn dataout>
		</cfif>
		<cftry>		
            <cfquery name="CheckTypeExist" datasource="#Session.DBSourceREAD#">
                SELECT                     
                    PKId_bi                    
                FROM 
                    troubletickets.type				
				WHERE
					TypeName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpType)#">                
                AND
                    PKId_bi <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpId)#">  
                AND 
					UserId_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Trim(arguments.inpUserId)#"> 
				AND 
					TypeStatus_ti = 1
            </cfquery>	
            <cfif CheckTypeExist.RECORDCOUNT GT 0>            	
                <cfthrow MESSAGE='Type name "#arguments.inpType#"  is existed. Please create a new one.' TYPE="Any" detail='Type name "#arguments.inpType#"  is existed. Please create a new one.' >                   
            </cfif>         

			<cfif arguments.inpId is 0>
				<cfquery name="InsertUpdateTicketType" datasource="#Session.DBSourceEBM#">
					INSERT INTO troubletickets.type
					(						
                        UserId_int,
                        TypeName_vch,
                        TypeStatus_ti			  	
					)
					VALUES
					(						
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpUserId)#">, 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpType)#">, 						
						1						
					)
				</cfquery>					
				<!--- Create log user --->
				<cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
					<cfinvokeargument name="moduleName" value="Trouble Ticket">
					<cfinvokeargument name="operator" value="Trouble Ticket - Insert Ticket Type">
				</cfinvoke>					
			<cfelse>
				<cfquery name="InsertUpdateTicketType" datasource="#Session.DBSourceEBM#">
					UPDATE troubletickets.type
					SET
						TypeName_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpType)#">					  	
					WHERE
						PKId_bi=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpId#">
				</cfquery>	
				<!--- Create log user --->
				<cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
					<cfinvokeargument name="moduleName" value="Trouble Ticket">
					<cfinvokeargument name="operator" value="Trouble Ticket - Update Ticket Type">
				</cfinvoke>		
				
			</cfif>
			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "Update Successfully"/>

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
    <cffunction name="DeleteTicketTypeByID" access="remote" output="false" hint="delete ticket type by id">
		<cfargument name="inpId" type="numeric" required="true" hint="id of message"/>

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = ""/>

		<cfset var CheckUsed = '' />
		<cfset var DeleteTicketType = '' />

		<cftry>

			<cfquery name="CheckUsed" datasource="#Session.DBSourceREAD#">
				SELECT
					COUNT(*) AS Total
				FROM
					troubletickets.tickets 
				WHERE
					TicketTypeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpId#"/>				
			</cfquery>

			<cfif CheckUsed.Total GT 0>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = "Can not delete this ticket type. This ticket type is in used" />
			<cfelse>
				<cfquery result="DeleteTicketType" datasource="#Session.DBSourceEBM#">
					UPDATE
						troubletickets.type
					SET
					    TypeStatus_ti = -1
					WHERE
						PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpId#"/>
				</cfquery>
				<!--- Create log user --->
				<cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
					<cfinvokeargument name="moduleName" value="Trouble Ticket">
					<cfinvokeargument name="operator" value="Trouble Ticket - Delete Ticket Type">
				</cfinvoke>	

				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = "Delete this ticket type successfully." />
			</cfif>

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>
	</cffunction>
	<cffunction name="GetListTicketTypeByUserId" access="remote" output="false" hint="Get list Ticket Type by user id">
		<cfargument name="inpUserId" TYPE="string" required="no" default="#session.UserId#" />
		<cfargument name="inpTypeName" TYPE="string" required="yes" default="#session.UserId#" />
			
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout["OutData"] = ArrayNew(1)/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />

		<cfset var item = '' />
		<cfset var GetListTypeByUserId = '' />			

		<cftry>										
			<cfquery name="GetListTypeByUserId" datasource="#Session.DBSourceREAD#">
                SELECT 
                    SQL_CALC_FOUND_ROWS
                    PKId_bi,
                    UserId_int,
                    TypeName_vch,
                    TypeStatus_ti
                FROM 
                    troubletickets.type	t				
				WHERE
					t.TypeStatus_ti = 1
                AND
                    UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/>	
                AND TypeName_vch LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.inpTypeName#%"> 			
				ORDER BY 
					t.PKId_bi DESC				
			</cfquery>					
			<cfif GetListTypeByUserId.RecordCount GT 0>
				<cfloop query="GetListTypeByUserId">					
					<cfset item = {
						ID: PKId_bi, 
						TYPE: TypeName_vch,
                        USERID:UserId_int                        
					} />
					<cfset arrayAppend(dataout["OutData"],item) />
				</cfloop>			
			</cfif>		
			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>
    </cffunction>

	<cffunction name="GetTicketTypeById" access="public" output="false" hint="Get list Ticket Type by id">
		<cfargument name="inpId" TYPE="string" required="true" default="Id of trouble ticket type" />
			
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPEID = "" />
		<cfset dataout.TYPEDES = "" />

		<cfset var GetListTypeById = '' />			

		<cftry>										
			<cfquery name="GetListTypeById" datasource="#Session.DBSourceREAD#">
                SELECT 
                    PKId_bi,
                    UserId_int,
                    TypeName_vch,
                    TypeStatus_ti
                FROM 
                    troubletickets.type			
				WHERE
					TypeStatus_ti = 1
                AND
                    PKId_bi= <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpId#"/>								
			</cfquery>

			<cfif GetListTypeById.RecordCount GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = "Get trouble ticket type successfully"/>
				<cfset dataout.TYPEID = GetListTypeById.PKId_bi />
				<cfset dataout.TYPEDES = GetListTypeById.TypeName_vch />
			</cfif>

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>
    </cffunction>
</cfcomponent>	