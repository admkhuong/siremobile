<cfcomponent hint="Trouble ticket app" output="false">
    <cfinclude template="/session/cfc/csc/constants.cfm">
	<cfinclude template="/session/sire/configs/paths.cfm">
    <!--- Send SMS --->
    <cfinclude template="/session/cfc/csc/inc_smsdelivery.cfm">
    <cfinclude template="/session/cfc/csc/inc_ext_csc.cfm">
    <cfinclude template="/session/cfc/csc/inc-billing-sire.cfm">

    <cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.loggedIn" default="0"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
    <cfparam name="Session.DBSourceREAD" default="bishop_read"/>
    <!--- Create default Trouble ticket batch --->
	<cffunction name="CreateDefaultBatch" access="public" hint="function to create new troubleticket batch from template for current user">
        <cfargument name="inpTroubleTicketConfig" TYPE="numeric" required="false" default="0" hint ="Create new trouble ticket config (0: don't create; 1: create)" />

		<cfset var dataout	= {} />
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.BATCHID = 0 />
        <cfset dataout.KEYWORDVALID = 0/><!---0: Keyword invalid; 1: keyword valid ; If Keyword is invalid, user must to update keyword before working on Trouble ticket App--->
		<cfset var RetVarshortCode = {} />
		<cfset var GetTroubleTicketTemplate = {} />
        <cfset var GetAssignedShortCode = {}/>
        <cfset var GetTroubleTicketConfiguation = {}/>
		<cfset var RetVarCreateTroubleTicketBatchResult = {} />
        <cfset var RetVarCreateTroubleTicketConfiguration = {}/>
        <cfset var InsertTroubleConfiguration = {}/>
        <cfset var GetKeywordValid = {}/>
		<cfset var InsertBatch = {} />
        <cfset var DeleteTroubleTicketConfig = {}/>
        <cfset var xmlControlString = ''/>

		<cftry>
            <!--- Delete current config when need create new config ---->
            <cfif arguments.inpTroubleTicketConfig EQ 1>
                <cfquery name="DeleteTroubleTicketConfig" datasource="#Session.DBSourceEBM#">
                    DELETE FROM 
                        troubletickets.configuration                
                    WHERE
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#"/>
                </cfquery>
            </cfif>
            <!--- Check Trouble ticket batch exist --->
            <cfquery name="GetTroubleTicketConfiguation" datasource="#Session.DBSourceREAD#">
				SELECT 
					BatchId_bi
				FROM 
					troubletickets.configuration
				WHERE 
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
			</cfquery>
            <!--- get user curent short code --->
            <cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="RetVarshortCode"></cfinvoke>

            <cfif GetTroubleTicketConfiguation.RecordCount GT 0>
                <cfset dataout.BATCHID = GetTroubleTicketConfiguation.BatchId_bi />
                <!--- check keyword valid --->
                <cfquery name="GetKeywordValid" datasource="#Session.DBSourceREAD#">
                    SELECT 
                       k.Keyword_vch
                    FROM 
                        sms.keyword AS k
                    JOIN
                        sms.shortcoderequest AS scr
                    ON
                        k.ShortCodeRequestId_int = scr.ShortCodeRequestId_int
                    JOIN 
                        sms.shortcode AS sc
                    ON 
                        sc.ShortCodeId_int = scr.ShortCodeId_int
                    WHERE 
                        k.Active_int = 1
                    AND 
                        sc.ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#RetVarshortCode.SHORTCODE#">        
                    AND
                        k.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#dataout.BATCHID#">                    
                </cfquery>
                <cfif GetKeywordValid.RecordCount GT 0>
                    <cfset dataout.KEYWORDVALID = 1/>
                </cfif>
            <cfelse>
            <!--- Create new Batch and trouble ticket configuration for current user --->
                <!--- get default template campaign --->
                <cfquery name="GetTroubleTicketTemplate" datasource="#Session.DBSourceREAD#">
                    SELECT 
                        XMLControlString_vch
                    FROM 
                        simpleobjects.templatecampaign
                    WHERE 
                        Name_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TROUBLETICKET_NAME#">
                </cfquery>

                <cfif GetTroubleTicketTemplate.RecordCount GT 0>
                    <cfset xmlControlString = GetTroubleTicketTemplate.XMLControlString_vch/>
                </cfif>

                <cfquery name="InsertBatch" datasource="#Session.DBSourceEBM#" result="RetVarCreateTroubleTicketBatchResult">
                    INSERT INTO simpleobjects.batch 
                    (
                        UserId_int,
                        rxdsLibrary_int,
                        rxdsElement_int, 
                        rxdsScript_int, 
                        UserBatchNumber_int, 
                        Created_dt,
                        Desc_vch,
                        LastUpdated_dt,
                        GroupId_int,
                        XMLControlString_vch,
                        Active_int,
                        ContactGroupId_int,
                        ContactTypes_vch,
                        TemplateType_ti,
                        EMS_Flag_int,
                        RealTimeFlag_int,
                        TemplateId_int,
                        ShortCodeId_int
                    ) 
                    VALUES 
                    (
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                        0,
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TROUBLETICKET_NAME#">,
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#xmlControlString#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="3">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="3">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RetVarshortCode.SHORTCODEID#">
                    )
                </cfquery>
                <cfif RetVarCreateTroubleTicketBatchResult.RecordCount GT 0>
                    <cfset dataout.BATCHID = RetVarCreateTroubleTicketBatchResult.GENERATED_KEY />
                     <!--- create configuration --->
                    <cfquery name="InsertTroubleConfiguration" datasource="#Session.DBSourceEBM#" result="RetVarCreateTroubleTicketConfiguration">
                        INSERT INTO troubletickets.configuration 
                        (
                            UserId_int,
                            BatchId_bi,
                            Created_dt
                        ) 
                        VALUES 
                        (
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#dataout.BATCHID#">,
                            NOW()
                        )
                    </cfquery>
                </cfif>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1 />

            <cfcatch TYPE="any">
                <cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
            </cfcatch>
        </cftry>
        <cfreturn dataout/>
    </cffunction>

	<!--- Get list trouble ticket ---> 
    <cffunction name="ListAllTroubleTickets" access="remote" output="true" hint="List All Trouble Tickets"><!--- returnformat="JSON"--->     
    	<cfargument name="inpStartDate" type="string" required="false" default="" hint="Search by start date"/>
        <cfargument name="inpEndDate" type="string" required="false" default="" hint="Search by end date"/>
        <cfargument name="inpUserId" required="no" default="#Session.USERID#" hint="User logon or param from client">

        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />                     
        <cfargument name="iSortCol_0" default="-1"> <!--- sort default is Created_dt --->

        <cfargument name="sSortDir_0" default="DESC"><!---this is the direction of sorting, asc or desc --->
        <cfargument name="customFilter" default=""><!---this is the custom filter data --->
        
        <cfset var startDate 	= "" />
	    <cfset var endDate   	= "" />    
        <cfif arguments.inpStartDate NEQ "" && arguments.inpEndDate NEQ "">
            <cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
	        <cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
	        <cfset var startDate 	= CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
	        <cfset var endDate   	= CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />                            
        </cfif>          

        <cfset var dataout 		= {}>
        <cfset dataout.DATA 	= ArrayNew(1)>
        <cfset var GetTroubleTicketQuery = ''>
        <cfset var GetTroubleTicketSummaryQuery = ''>
        <cfset var sSortDir_0 	= 'DESC'/>
        <cfset var orderBy    	= "">
        <cfset var tempItem 	= '' />
        <cfset var filterItem   = '' />                            
        <cfset var status 		= ''/>
        <cfset var currentDue 	= ''/>
        <cfset var overDue 		= '0'/>
        <cfset var priority 	= ''/>
        <cfset var rsCount 		= '0'/>   

         <!--- Variable for summary box --->   
        <cfset var countTotal 			= 0/>   
        <cfset var countOpen 			= 0/>   
        <cfset var countInProgress 		= 0/>   
        <cfset var countOnHold 			= 0/>   
        <cfset var countOverDue 		= 0/>   
        <cfset var countClosed 			= 0/>     
		

        <cftry>
                        
            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            
            <!--- 1 ListTroubleTicketData is key of DataTable control--->
            <cfset dataout["ListTroubleTicketData"] = ArrayNew(1)>
        
            <!--- Order by --->
            <cfif iSortCol_0 GTE 0>
                <cfswitch expression="#Trim(iSortCol_0)#">                          
                    <cfdefaultcase> 
                        <cfset orderBy = "tt.Created_dt"/>   
                        <cfset sSortDir_0 = 'DESC'/>    
                    </cfdefaultcase>    
                </cfswitch>
            <cfelse>
                <cfset orderBy = "tt.Created_dt"/>
                <cfset sSortDir_0 = 'DESC'/>
            </cfif> 		
			<!--- Get list trouble ticket --->
            <cfquery name="GetTroubleTicketQuery" datasource="#Session.DBSourceREAD#">
                SELECT
                    SQL_CALC_FOUND_ROWS 
                    tt.PKId_bi,
                    tt.UserId_int,

                    tt.TicketName_vch,
                    tt.TicketStatus_ti,                   
                    tt.TicketTypeId_int,

                    -- ttt.TypeName_vch,
                    tt.TicketPriority_ti,
                    tt.TicketEAT_dec,

                    tt.Created_dt,
                    tt.Updated_dt,                   
                    tt.ContactString_vch

                FROM
                    troubletickets.tickets tt                 
                WHERE                
                tt.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpUserId#"> 

                <cfif customFilter NEQ "">

                    <cfoutput>
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                        	<cfif filterItem.VALUE NEQ '0'>
                                <cfif filterItem.NAME EQ ' tt.ContactString_vch '>
                                    <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                                        AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#REReplaceNoCase(filterItem.VALUE, "[^\d^*^P^X^##]", "", "ALL")#'>                                   <cfelse>                                    
                                        AND ( tt.ContactString_vch #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#REReplaceNoCase(filterItem.VALUE, "[^\d^*^P^X^##]", "", "ALL")#%'>
                                        OR #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#REReplaceNoCase(filterItem.VALUE, "[^\d^*^P^X^##]", "", "ALL")#%'>)                                   
                                    </cfif>
                                    
                                <cfelse>
                                    <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                                        AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>                                                                                                  
                                    <cfelse>                                    
                                        AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>                                
                                    </cfif>
                                </cfif>                        		
                        	</cfif>                            
                        </cfloop>
                    </cfoutput>
                </cfif>	
                <cfif arguments.inpStartDate NEQ "" && arguments.inpEndDate NEQ "">
	                AND 
	                	DATE(tt.Created_dt) >= #startDate#
	                AND
	                	DATE(tt.Created_dt) <= #endDate#
                </cfif>
                
                ORDER BY 
                        #orderBy# #sSortDir_0#
                LIMIT 
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
                OFFSET 
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
            </cfquery>
            <!--- Get data for summary box --->
            <cfquery name="GetTroubleTicketSummaryQuery" datasource="#Session.DBSourceREAD#">
                SELECT                    
                    tt.PKId_bi,
                    tt.UserId_int,

                    tt.TicketName_vch,
                    tt.TicketStatus_ti,                   
                    tt.TicketTypeId_int,

                    -- ttt.TypeName_vch,
                    tt.TicketPriority_ti,
                    tt.TicketEAT_dec,

                    tt.Created_dt,
                    tt.Updated_dt,                   
                    tt.ContactString_vch

                FROM
                    troubletickets.tickets tt                 
                WHERE                
                tt.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpUserId#"> 

                <cfif customFilter NEQ "">

                    <cfoutput>
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.VALUE NEQ '0'>
                                <cfif filterItem.NAME EQ ' tt.ContactString_vch '>
                                    <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                                        AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#REReplaceNoCase(filterItem.VALUE, "[^\d^*^P^X^##]", "", "ALL")#'>                                   <cfelse>                                    
                                        AND ( tt.ContactString_vch #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#REReplaceNoCase(filterItem.VALUE, "[^\d^*^P^X^##]", "", "ALL")#%'>
                                        OR #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#REReplaceNoCase(filterItem.VALUE, "[^\d^*^P^X^##]", "", "ALL")#%'>)                                   
                                    </cfif>
                                    
                                <cfelse>
                                    <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                                        AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>                                                                                                  
                                    <cfelse>                                    
                                        AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>                                
                                    </cfif>
                                </cfif>                             
                            </cfif>                            
                        </cfloop>
                    </cfoutput>
                </cfif> 
                <cfif arguments.inpStartDate NEQ "" && arguments.inpEndDate NEQ "">
                    AND 
                        DATE(tt.Created_dt) >= #startDate#
                    AND
                        DATE(tt.Created_dt) <= #endDate#
                </cfif>
                
                ORDER BY 
                        #orderBy# #sSortDir_0#               
            </cfquery>
            <!--- Get total records --->
            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfloop query="rsCount">
                <cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
                <cfset dataout["iTotalRecords"] = iTotalRecords>
            </cfloop>

            <!--- loop and result data of Data list --->
            <cfloop query="GetTroubleTicketQuery">                             
                <cfif GetTroubleTicketQuery.TicketStatus_ti EQ _TICKET_OPEN>
                    <cfset status = _TICKET_OPEN_TEXT>
                <cfelseif GetTroubleTicketQuery.TicketStatus_ti EQ _TICKET_INPROGRESS>
                    <cfset status = _TICKET_INPROGRESS_TEXT>
                <cfelseif GetTroubleTicketQuery.TicketStatus_ti EQ _TICKET_ONHOLD>
                    <cfset status = _TICKET_ONHOLD_TEXT>                
				<cfelseif GetTroubleTicketQuery.TicketStatus_ti EQ _TICKET_CLOSED>
                    <cfset status = _TICKET_CLOSED_TEXT>
                </cfif>                 

                <cfif GetTroubleTicketQuery.TicketPriority_ti EQ _TICKET_MAJOR>
                    <cfset priority = _TICKET_MAJOR_TEXT>
                <cfelseif GetTroubleTicketQuery.TicketPriority_ti EQ _TICKET_MINOR>
                    <cfset priority = _TICKET_MINOR_TEXT>                
                </cfif>  
                <!--- Calculate Over Due --->
                                
                <cfif GetTroubleTicketQuery.TicketEAT_dec NEQ 0 AND GetTroubleTicketQuery.TicketStatus_ti NEQ _TICKET_CLOSED>
                    <cfset currentDue = DateAdd('n', GetTroubleTicketQuery.TicketEAT_dec * 60, GetTroubleTicketQuery.Created_dt) >
                    <cfset overDue = dateDiff("n", currentDue, NOW())>              
                <cfelse>
                    <cfset overDue = -1>        
                </cfif>                

                <cfif overDue LT 0 OR overDue EQ 0 >
                    <cfset overDue = 0>                             
                <cfelse>
                	<!--- <cfset countOverDue = countOverDue + 1>  --->
                    <cfset overDue = overDue / 60>   <!--- Convert from minute to hour --->
                </cfif>                
                
                <!--- Calculate for summary box --->   
              <!---   <cfif GetTroubleTicketQuery.TicketStatus_ti EQ _TICKET_OPEN>
                	<cfset countOpen = countOpen + 1>
                <cfelseif GetTroubleTicketQuery.TicketStatus_ti EQ _TICKET_INPROGRESS>
                    <cfset countInProgress = countInProgress + 1>
                <cfelseif GetTroubleTicketQuery.TicketStatus_ti EQ _TICKET_ONHOLD>
                    <cfset countOnHold = countOnHold + 1>                
				<cfelseif GetTroubleTicketQuery.TicketStatus_ti EQ _TICKET_CLOSED>
                    <cfset countClosed = countClosed + 1>
                </cfif>     --->

                <cfset tempItem = {
                    ID = '#PKId_bi#',
                    USERID = '#UserId_int#',
                    TICKETNAME = '#TicketName_vch#',

                    STATUS = "#status#",
                    STATUSID = "#TicketStatus_ti#",                    
                    // TICKETTYPE = "#TypeName_vch#",                   

                    PRIORITY = '#priority#',   
					TICKETETA = '#TicketEAT_dec#',   
                    PHONE = "#ContactString_vch#",                                  

                    CREATEDDATE = '#DateTimeFormat(Created_dt,'yyyy-MM-dd HH:nn:ss')#',                                   
                    UPDATEDDATE = '#DateTimeFormat(Updated_dt,'yyyy-MM-dd HH:nn:ss')#',
                    OVERDUE = "#overDue#"                                 
                }>      
                <cfset ArrayAppend(dataout["ListTroubleTicketData"],tempItem)>
            </cfloop>  

            <!--- Summary box  --->
            <cfloop query="GetTroubleTicketSummaryQuery">                                                                         
                <cfif GetTroubleTicketSummaryQuery.TicketEAT_dec NEQ 0 AND GetTroubleTicketSummaryQuery.TicketStatus_ti NEQ _TICKET_CLOSED>
                    <cfset currentDue = DateAdd('n', GetTroubleTicketSummaryQuery.TicketEAT_dec * 60, GetTroubleTicketSummaryQuery.Created_dt) >
                    <cfset overDue = dateDiff("n", currentDue, NOW())>              
                <cfelse>
                    <cfset overDue = -1>        
                </cfif>                

                <cfif overDue LT 0 OR overDue EQ 0 >
                    <cfset overDue = 0>                             
                <cfelse>
                    <cfset countOverDue = countOverDue + 1>                    
                </cfif>                
                
                <!--- Calculate for summary box --->   
                <cfif GetTroubleTicketSummaryQuery.TicketStatus_ti EQ _TICKET_OPEN>
                    <cfset countOpen = countOpen + 1>
                <cfelseif GetTroubleTicketSummaryQuery.TicketStatus_ti EQ _TICKET_INPROGRESS>
                    <cfset countInProgress = countInProgress + 1>
                <cfelseif GetTroubleTicketSummaryQuery.TicketStatus_ti EQ _TICKET_ONHOLD>
                    <cfset countOnHold = countOnHold + 1>                
                <cfelseif GetTroubleTicketSummaryQuery.TicketStatus_ti EQ _TICKET_CLOSED>
                    <cfset countClosed = countClosed + 1>
                </cfif>                   
            </cfloop>    
             <!--- <cfdump var="#CountOpen#" abort="true"/> --->
            <cfset dataout["CountTotal"] = dataout["iTotalRecords"] />            
            <cfset dataout["CountOpen"] = #countOpen# />  
            <cfset dataout["CountInProgress"] = #countInProgress# />  
            <cfset dataout["CountOnHold"] = #countOnHold# />  
            <cfset dataout["CountClosed"] = #countClosed# />
            <cfset dataout["CountOverDue"] = #countOverDue# />


        <cfcatch type="Any" >
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout["ListTroubleTicketData"] = ArrayNew(1)>
            <cfset dataout["CountTotal"] = 0 />            
            <cfset dataout["CountOpen"] = 0 />  
            <cfset dataout["CountInProgress"] = 0 />  
            <cfset dataout["CountOnHold"] = 0 />  
            <cfset dataout["CountClosed"] = 0 />
            <cfset dataout["CountOverDue"] = 0 />
        </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>

    <!--- Save trouble ticket --->
    <cffunction name="SaveTroubleTicket" access="remote" output="true" hint="Save trouble ticket">
    	<cfargument name="inpTroubleTicketObject" required="true" hint="Trouble ticket information in json object">        
        <cfargument name="inpUserId" required="false" default="#Session.USERID#" hint="user id">			

		<cfset var dataout = {}/>
		<cfset var SaveTroubleTicketQuery = ''/>
        <cfset var RetVarSaveTroubleTicketQuery ={}/>
        <cfset var RetVarTroubleTicketNotify = {}/>
        <cfset var RetVarNotifyResult = {}/>
        <cfset var RetVarTypeById = {}/>
        <cfset var RetvarUserInfo = {}/>
		<cfset var contactString = ''/>		
        <cfset var ticketId = 0/>    
        <cfset var troubleStatus = ""/>   
        <cfset var historydetail = ""/>
        <cfset var troublePriority = ""/>

		<cfset dataout.RXRESULTCODE = 0 /> 
		<cfset dataout.RESULT = ''/>                    
		<cfset dataout.MESSAGE = ''/>	
        <cfset dataout.TROUBLETICKETID = 0/>	

		<cfset var ticketInfo = DESERIALIZEJSON(arguments.inpTroubleTicketObject)/>	

        <cfset dataout.TROUBLETICKETID = ticketinfo.inpTicketNo/>

        <!--- Get trouble ticket status des --->
        <cfswitch expression="#Trim(ticketInfo.inpTicketStatusId)#">
            <cfcase value="#_TICKET_OPEN#"> 
                <cfset troubleStatus = "#_TICKET_OPEN_TEXT#" />
            </cfcase> 
            <cfcase value="#_TICKET_INPROGRESS#"> 
                <cfset troubleStatus = "#_TICKET_INPROGRESS_TEXT#" />
            </cfcase>
             <cfcase value="#_TICKET_ONHOLD#"> 
                <cfset troubleStatus = "#_TICKET_ONHOLD_TEXT#" />
            </cfcase>
             <cfcase value="#_TICKET_CLOSED#"> 
                <cfset troubleStatus = "#_TICKET_CLOSED_TEXT#" />
            </cfcase> 
            <cfdefaultcase> 
                <cfset troubleStatus = "UNKNOWN"/>   	
            </cfdefaultcase> 	
        </cfswitch>

        <!--- Get trouble ticket priority --->
        <cfswitch expression="#Trim(ticketInfo.inpTicketPriorityId)#">
            <cfcase value="#_TICKET_MAJOR#"> 
                <cfset troublePriority = "#_TICKET_MAJOR_TEXT#" />
            </cfcase> 
            <cfcase value="#_TICKET_MINOR#"> 
                <cfset troublePriority = "#_TICKET_MINOR_TEXT#" />
            </cfcase>
            <cfdefaultcase> 
                <cfset troublePriority = "UNKNOWN"/>   	
            </cfdefaultcase> 	
        </cfswitch>

        <cftry>
        	<!--- check Create/Edit mode --->
	        <cfif ticketinfo.inpTicketNo EQ ''>	    
	    	    <cfset contactString = REReplaceNoCase(ticketInfo.inpTicketContactString, "[^\d^*^P^X^##]", "", "ALL")>   
	    	    <!--- Check phone number US --->
				<cfif NOT isvalid('telephone', ticketinfo.inpTicketContactString)>
					<cfset dataout.ERRMESSAGE = 'SMS Number is not a valid US telephone number!'> 
					<cfset dataout.MESSAGE = 'SMS Number is not a valid US telephone number!'>
					<cfreturn dataout>
				</cfif>     	
	        	<!--- 1. Case Create new ticket --->
	        	<cfquery name="SaveTroubleTicketQuery" datasource="#Session.DBSourceEBM#" result="RetVarSaveTroubleTicketQuery">
					INSERT INTO troubletickets.tickets(
						TicketName_vch,
						UserId_int,
						ContactString_vch,					
						TicketStatus_ti,
						TicketTypeId_int,	
						TicketPriority_ti,
						TicketEAT_dec,
						Description_txt,
						Created_dt
					) 
					VALUES(					
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ticketInfo.inpTicketName#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpUserId#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#contactString#">,					
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ticketInfo.inpTicketStatusId#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ticketInfo.inpTicketTypeId#">,	
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ticketInfo.inpTicketPriorityId#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#ticketInfo.inpTicketETA#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#Trim(ticketInfo.inpDescription)#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#DateTimeFormat(NOW(), "yyyy-MM-dd HH:nn:ss")#">
					)					
				</cfquery>

                <cfset ticketId = RetVarSaveTroubleTicketQuery.generatedkey>
            
                <!--- Get user info --->
                
                <cfinvoke method="GetUserInfor" returnvariable="RetvarUserInfo">
                    <cfinvokeargument name="inpIdUser" value="#arguments.inpUserId#">                  
                </cfinvoke>         

                <!--- Add current UserId to watcher list --->
                <cfinvoke method="InsertUpdateWatcher" component="session.sire.models.cfc.troubleTicket.watcher">                    
                    <cfinvokeargument name="inpTicketId" value="#ticketId#">      
                    <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">      
                    <cfinvokeargument name="inpWatcherType" value="1">   <!--- 1 is add email watcher --->   
                    <cfinvokeargument name="inpWatcherName" value="#RetvarUserInfo.EMAIL#"> 
                </cfinvoke>     
                <cfinvoke method="InsertUpdateWatcher" component="session.sire.models.cfc.troubleTicket.watcher">                    
                    <cfinvokeargument name="inpTicketId" value="#ticketId#">      
                    <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">      
                    <cfinvokeargument name="inpWatcherType" value="2">   <!--- 2 is add phone number watcher --->   
                    <cfinvokeargument name="inpWatcherName" value="#RetvarUserInfo.PHONENUMBER#"> 
                </cfinvoke>   
                <cfinvoke method="InsertUpdateWatcher" component="session.sire.models.cfc.troubleTicket.watcher">                    
                    <cfinvokeargument name="inpTicketId" value="#ticketId#">      
                    <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">      
                    <cfinvokeargument name="inpWatcherType" value="2">   <!--- 2 is add phone number of user send watcher --->   
                    <cfinvokeargument name="inpWatcherName" value="#contactString#"> 
                </cfinvoke>                
            
                 <!--- Create log user --->
                <cfinvoke method="CreateUserLog">
                    <cfinvokeargument name="inpTicketId" value="#ticketId#">
                    <cfinvokeargument name="inpNote" value="Insert new trouble ticket">
                    <cfinvokeargument name="inpActionName" value="Trouble Ticket - Insert new trouble ticket">
                </cfinvoke>

                <!--- Return New Trouble ticket ID ---->
                <cfif RetVarSaveTroubleTicketQuery.RecordCount GT 0>
                    <cfset dataout.RXRESULTCODE = 1 />
                    <cfset dataout.TROUBLETICKETID = RetVarSaveTroubleTicketQuery.GENERATED_KEY/>
                    <cfset dataout.RESULT = 'SUCCESS'>                    
	            <cfset dataout.MESSAGE = "Trouble ticket has been created successfully.">
                <cfelse>
                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.TROUBLETICKETID = 0/>
                    <cfset dataout.RESULT = 'FAIL'>                    
	                <cfset dataout.MESSAGE = "Add new trouble ticket fail."> 
                </cfif>
				
	        <cfelse>
	        	<!--- 2. Case Edit ticket --->	   
                <cfset contactString = REReplaceNoCase(ticketInfo.inpTicketContactString, "[^\d^*^P^X^##]", "", "ALL")>   
                
                <!--- Check phone number US --->
                <cfif NOT isvalid('telephone', ticketinfo.inpTicketContactString)>
                    <cfset dataout.ERRMESSAGE = 'SMS Number is not a valid US telephone number!'> 
                    <cfset dataout.MESSAGE = 'SMS Number is not a valid US telephone number!'>
                    <cfreturn dataout>
                </cfif>   

                <!--- Get trouble ticket info before edit --->
                <cfinvoke method="GetTroubleTicketNotifyDetailById" returnvariable ="RetVarTroubleTicketNotify">
                    <cfinvokeargument name="inpTicketId" value="#ticketInfo.inpTicketNo#">
                </cfinvoke>

	        	<cfquery name="SaveTroubleTicketQuery" datasource="#Session.DBSourceEBM#">
					UPDATE troubletickets.tickets
					SET 
						TicketName_vch 			= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ticketInfo.inpTicketName#">,
						ContactString_vch		= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#contactString#">,					
						TicketStatus_ti			= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ticketInfo.inpTicketStatusId#">,
						TicketTypeId_int		= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ticketInfo.inpTicketTypeId#">,	

						TicketPriority_ti		= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ticketInfo.inpTicketPriorityId#">,
						TicketEAT_dec			= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#ticketInfo.inpTicketETA#">,

						Description_txt			= <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#Trim(ticketInfo.inpDescription)#">,
						Updated_dt				= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#DateTimeFormat(NOW(), "yyyy-MM-dd HH:nn:ss")#">
					WHERE 
						PKId_bi 				=  <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#ticketInfo.inpTicketNo#">			
				</cfquery>

                
                <cfif RetVarTroubleTicketNotify.RXRESULTCODE EQ 1>

                    <!--- Compare Ticket Name --->
                    <cfif RetVarTroubleTicketNotify.NAME NEQ ticketInfo.inpTicketName>
                        <!--- Update history detail --->
                        <cfset historydetail = historydetail & "<br><b>Name:</b> #ticketInfo.inpTicketName# "/>
                    </cfif>

                    <!--- Compare Ticket Phone --->
                    <cfif RetVarTroubleTicketNotify.PHONE NEQ contactString>
                        <!--- Update history detail --->
                        <cfset historydetail = historydetail & "<br><b>Phone:</b> #contactString# "/>
                    </cfif>

                    <!--- Compare Ticket Priority --->
                    <cfif RetVarTroubleTicketNotify.PRIORITY NEQ ticketInfo.inpTicketPriorityId>
                        <!--- Update history detail --->
                        <cfset historydetail = historydetail & "<br><b>Priority:</b> #troublePriority# "/>
                    </cfif>

                    <!--- Compare status before and status after edit --->
                    <cfif RetVarTroubleTicketNotify.STATUS NEQ ticketInfo.inpTicketStatusId>
                        <!--- Notify to watcher list when trouble ticket change status --->
                        <cfinvoke method="NotifyWatcherList" returnvariable ="RetVarNotifyResult">
                            <cfinvokeargument name="inpTicketId" value="#ticketInfo.inpTicketNo#">
                            <cfinvokeargument name="inpTroubleStatus" value="#troubleStatus#">
                        </cfinvoke>
                        <!--- Update history detail --->
                        <cfset historydetail = historydetail & "<br><b>Status:</b> #troubleStatus# "/>
                    </cfif>

                    <!--- Compare Ticket Type --->
                    <cfif RetVarTroubleTicketNotify.TYPE NEQ ticketInfo.inpTicketTypeId>
                        <!--- Get trouble ticket type des --->
                        <cfinvoke method="GetTicketTypeById" component="session.sire.models.cfc.troubleTicket.ticket-type" returnvariable ="RetVarTypeById">
                            <cfinvokeargument name="inpId" value="#ticketInfo.inpTicketTypeId#">
                        </cfinvoke>
                         <!--- Update history detail --->
                        <cfif RetVarTypeById.RXRESULTCODE EQ 1>
                            <cfset historydetail = historydetail & "<br><b>Type:</b> #RetVarTypeById.TYPEDES# "/>
                        <cfelse>
                            <cfset historydetail = historydetail & "<br><b>Type:</b> #ticketInfo.inpTicketTypeId# "/>
                        </cfif>
                    </cfif>

                    <!--- Compare Ticket ETA --->
                    <cfif RetVarTroubleTicketNotify.ETA NEQ ticketInfo.inpTicketETA>
                        <!--- Update history detail --->
                        <cfset historydetail = historydetail & "<br><b>ETA:</b> #ticketInfo.inpTicketETA# "/>
                    </cfif>

                     <!--- Compare Ticket Desc --->
                    <cfif RetVarTroubleTicketNotify.DESC NEQ ticketInfo.inpDescription>
                        <!--- Update history detail --->
                        <cfset ticketInfo.inpDescription  = REReplaceNoCase(ticketInfo.inpDescription,"#chr(10)#","<br>","ALL")/>
                        <cfset historydetail = historydetail & "<br><b>Desc:</b><br> #ticketInfo.inpDescription# "/>
                    </cfif>
                </cfif>

                <cfif historydetail NEQ "">
                    <!--- Create log user --->
                    <cfinvoke method="CreateUserLog" returnvariable ="RetVarCreateUserLog">
                        <cfinvokeargument name="inpTicketId" value="#ticketInfo.inpTicketNo#">
                        <cfinvokeargument name="inpNote" value="Updated trouble ticket with new data: #historydetail#">
                        <cfinvokeargument name="inpActionName" value="Trouble Ticket - Updated trouble ticket">
                    </cfinvoke>
                </cfif>

                                
	        	<cfset dataout.RXRESULTCODE = 1 /> 
				<cfset dataout.RESULT = 'SUCCESS'>                    
	            <cfset dataout.MESSAGE = "Trouble ticket has been updated successfully.">
	        </cfif>						
			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 /> 
				<cfset dataout.RESULT = 'ERROR'>                    
				<cfset dataout.MESSAGE = cfcatch.MESSAGE>
			</cfcatch> 			
        </cftry>
		<cfreturn dataout />
    </cffunction>

    <!--- Get info trouble ticket notify by ticket id --->
    <cffunction name="GetTroubleTicketNotifyDetailById" access="remote" output="false" hint="Get trouble ticket notify detail by Ticket Id">
		<cfargument name="inpTicketId" required="true" hint ="Trouble ticket Id">
        <cfargument name="inpUserId" required="false" default ="#session.USERID#" hint ="User Id">

		<cfset var dataout = {} />
		<cfset var GetTroubleTicketDetail = {} />
		<cfset var RetvarUserInfo = {} />

        <cfset dataout.RXRESULTCODE = 0 />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.PHONE = "" />
        <cfset dataout.DESC = "" />
        <cfset dataout.STATUS = "" />
        <cfset dataout.NAME = "" />
        <cfset dataout.EMAIL = "" />
        <cfset dataout.USERID = "" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.PRIORITY =""/>
        <cfset dataout.ETA =""/>

		<cftry>
			<cfquery name="GetTroubleTicketDetail" datasource="#Session.DBSourceREAD#">
				SELECT
					UserId_int,
                    TicketName_vch,
                    Description_txt,
                    ContactString_vch,
                    TicketStatus_ti,
                    TicketEAT_dec,
                    Created_dt,
                    TicketPriority_ti,
                    TicketTypeId_int
                    
				FROM
					troubletickets.tickets
				WHERE
					PKId_bi = <CFQUERYPARAM CFSQLTYPE='CF_SQL_BIGINT' VALUE='#arguments.inpTicketId#'>
			</cfquery>
			<cfif GetTroubleTicketDetail.Recordcount GT 0>
				<cfinvoke method="GetUserInfoById" component="public.sire.models.cfc.userstools" returnvariable="RetvarUserInfo">
					<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
				</cfinvoke>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = "Get trouble ticket detail successfully!" />
				<cfset dataout.PHONE = "#GetTroubleTicketDetail.ContactString_vch#" />
				<cfset dataout.DESC = "#GetTroubleTicketDetail.Description_txt#" />
				<cfset dataout.STATUS = "#GetTroubleTicketDetail.TicketStatus_ti#" />
				<cfset dataout.NAME = "#GetTroubleTicketDetail.TicketName_vch#" />
				<cfset dataout.EMAIL = "#RetvarUserInfo.FULLEMAIL#" />
				<cfset dataout.USERID = "#GetTroubleTicketDetail.UserId_int#" />
                <cfset dataout.TYPE = "#GetTroubleTicketDetail.TicketTypeId_int#" />
                <cfset dataout.PRIORITY ="#GetTroubleTicketDetail.TicketPriority_ti#"/>
                <cfset dataout.ETA = "#GetTroubleTicketDetail.TicketEAT_dec#"/>
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = "No record found." />
			</cfif>
			<cfcatch type="any">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>
    <!--- Trouble ticket notify --->
    <cffunction name="SendNotifyEmail" access="remote" output="false" hint="Send notify email to Sire's users for their trouble ticket">
		<cfargument name="inpEmail" required="false" hint ="Send notify to this email">
		<cfargument name="inpMessage" required="false" hint ="Content of email">
		<cfargument name="inpSubject" required="false" hint ="Subject of email">
		<cfargument name="inpTicketId" required="true" hint ="Trouble ticket id">
		<cfset var dataout = {} />
		<cfset var dataMail = '' />

		<cftry>
			<cfif arguments.inpEmail EQ ''>
				<cfset dataout.RXRESULTCODE = -2 />
				<cfset dataout.MESSAGE = 'Please insert Email Address!' />
				<cfreturn dataout />
			<cfelseif arguments.inpMessage EQ ''>
				<cfset dataout.RXRESULTCODE = -2 />
				<cfset dataout.MESSAGE = 'Please insert Email Message!' />
				<cfreturn dataout />
			<cfelseif arguments.inpSubject EQ ''>
				<cfset dataout.RXRESULTCODE = -2 />
				<cfset dataout.MESSAGE = 'Please insert Email Subject!' />
				<cfreturn dataout />
			<cfelseif arguments.inpTicketId EQ '' || arguments.inpTicketId LTE 0>
				<cfset dataout.RXRESULTCODE = -2>
				<cfset dataout.MESSAGE = 'Invalid ticket Id!' />
				<cfreturn dataout />
			<cfelse>
				<cfset dataMail = {
					message: arguments.inpMessage
				} />

				<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
	                <cfinvokeargument name="to" value="#arguments.inpEmail#">
	                <cfinvokeargument name="type" value="html">
	                <cfinvokeargument name="subject" value="#arguments.inpSubject#">
	                <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_notify_trouble_ticket.cfm">
	                <cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#">
	            </cfinvoke>

	            <cfset dataout.RXRESULTCODE = 1>
	            <cfset dataout.MESSAGE = 'Send Notify Email successfully!'>
	        </cfif>
			<cfcatch type="any">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="SendNotifySMS" access="remote" output="false" hint="Send notify SMS to Sire's users for their trouble ticket">
		<cfargument name="inpMessage" required="true">
		<cfargument name="inpTicketId" required="true">
		<cfargument name="inpPhone" required="true">

		<cfset var dataout = {} />
        <cfset var RetVarshortCode = {}/>
        <cfset var RetVarSendSingleMT ={}/>

		<cftry>
			<cfif arguments.inpPhone EQ ''>
				<cfset dataout.RXRESULTCODE = -2>
				<cfset dataout.MESSAGE = 'Please insert Phone number!' />
				<cfreturn dataout />
			<cfelseif arguments.inpMessage EQ '' >
				<cfset dataout.RXRESULTCODE = -2>
				<cfset dataout.MESSAGE = 'Please insert SMS message!' />
				<cfreturn dataout />
			<cfelseif arguments.inpTicketId EQ '' || arguments.inpTicketId LTE 0>
				<cfset dataout.RXRESULTCODE = -2>
				<cfset dataout.MESSAGE = 'Invalid ticket Id!' />
				<cfreturn dataout />
			</cfif>

             <!--- Check phone number US --->
			<cfif NOT isvalid('telephone', arguments.inpPhone)>
				<cfset dataout.ERRMESSAGE = 'SMS Number is not a valid US telephone number!'> 
				<cfset dataout.MESSAGE = 'SMS Number is not a valid US telephone number!'>
				<cfreturn dataout>
			</cfif>

            <!---Find and replace all non numerics except P X * #--->
			<cfset arguments.inpPhone = REReplaceNoCase(arguments.inpPhone, "[^\d^\*^P^X^##]", "", "ALL")>
            <!--- get user curent short code --->
            <cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="RetVarshortCode"></cfinvoke>

			<cfinvoke method="SendSingleMT" returnvariable="RetVarSendSingleMT">
                <cfinvokeargument name="inpContactString" value="#arguments.inpPhone#"/>
                <cfinvokeargument name="inpShortCode" value="#RetVarshortCode.SHORTCODE#"/>
                <cfinvokeargument name="inpTextToSend" value="#arguments.inpMessage#"/>
                <cfinvokeargument name="inpSkipLocalUserDNCCheck" value="1"/> <!--- option to send to phone number has opt-out --->
            </cfinvoke>

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = 'Send Notify SMS successfully!' /> 
			<cfcatch type="any">
				<cfset dataout.TYPE = cfcatch.type />
				<cfset dataout.MESSAGE = cfcatch.message />
				<cfset dataout.ERRMESSAGE = cfcatch.detail />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
 
     <!--- Get info trouble ticket details by ticket id --->
    <cffunction name="GetTroubleTicketDetailById" access="remote" output="true" hint="Get trouble ticket detail by Ticket Id">
        <cfargument name="inpTicketId" required="true" hint ="Trouble ticket Id">
        <cfargument name="inpUserId" required="false" default ="#session.USERID#" hint ="User Id">

        <cfset var dataout = {} />
        <cfset var GetTroubleTicketDetail = {} /> 
        <cfset var RetValGetTroubleTicketDetail = {} />       

        <cftry>
            <cfquery name="GetTroubleTicketDetail" datasource="#Session.DBSourceREAD#" result="RetValGetTroubleTicketDetail">
                SELECT      
                    t.PKId_bi,              
                    t.UserId_int,
                    t.TicketName_vch,

                    t.Description_txt,
                    t.ContactString_vch,
                    t.TicketStatus_ti,

                    t.TicketTypeId_int,
                    tt.TypeName_vch AS TypeName,
                    t.TicketPriority_ti,
                    t.TicketEAT_dec,

                    t.Created_dt,
                    t.Updated_dt
                FROM
                    troubletickets.tickets t
                LEFT JOIN troubletickets.type tt
                    ON  t.TicketTypeId_int    = tt.PKId_bi
                WHERE
                    t.PKId_bi = <CFQUERYPARAM CFSQLTYPE='CF_SQL_BIGINT' VALUE='#arguments.inpTicketId#'>
            </cfquery>

            <cfif GetTroubleTicketDetail.Recordcount GT 0>                
                <cfset dataout.RXRESULTCODE = 1 />
                <cfset dataout.MESSAGE = "Get trouble ticket detail successfully!" />                
                
                <cfset dataout.ID = "#GetTroubleTicketDetail.PKId_bi#" />
                <cfset dataout.USERID = "#GetTroubleTicketDetail.UserId_int#" />
                <cfset dataout.TICKETNAME = "#GetTroubleTicketDetail.TicketName_vch#" />

                <cfset dataout.DESCRIPTION = "#GetTroubleTicketDetail.Description_txt#" />
                <cfset dataout.PHONE = "#GetTroubleTicketDetail.ContactString_vch#" />                                
                <cfset dataout.STATUSID = "#GetTroubleTicketDetail.TicketStatus_ti#" />

                <cfset dataout.TICKETTYPEID = "#GetTroubleTicketDetail.TicketTypeId_int#" />                
                <cfset dataout.TICKETTYPENAME = "#GetTroubleTicketDetail.TypeName#" />   

                <cfset dataout.TICKETPRIORITYID = "#GetTroubleTicketDetail.TicketPriority_ti#" />  
                <cfset dataout.TICKETETA = "#GetTroubleTicketDetail.TicketEAT_dec#" />                                

                <cfset dataout.CREATEDDATE = "#DateTimeFormat(GetTroubleTicketDetail.Created_dt,'yyyy-MM-dd HH:nn:ss')#" />
                <cfset dataout.UPDATEDDATE = "#DateTimeFormat(GetTroubleTicketDetail.Updated_dt,'yyyy-MM-dd HH:nn:ss')#" />
            <cfelse>
                <cfset dataout.RXRESULTCODE = 0 />
                <cfset dataout.MESSAGE = "No record found." />
            </cfif>            

            <cfcatch type="any">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataout />
    </cffunction>  

    <!--- notify to watcher --->
    <cffunction name="NotifyWatcherList" access="public" output="true" hint="Notify to watcher list">
        <cfargument name="inpTicketId" required="true" hint ="Trouble ticket Id">
        <cfargument name="inpUserId" required="false" default ="#session.USERID#" hint ="User Id">
        <cfargument name="inpTroubleStatus" required="false" default ="In Progress" hint ="Trouble ticket status">

        <cfset var dataout = {} />
        <cfset var data = {}/>
        <cfset var GetListWatcher = {} /> 
        <cfset var RetVarshortCode = {}/>
        <cfset var dataMail ={}/>

        <cfset dataout.RXRESULTCODE = 0 />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset listEmail = ""/>
        <cfset phone = ""/>

        <cftry>
            <cfquery name="GetListWatcher" datasource="#Session.DBSourceREAD#">
                SELECT 
                    w.PKId_bi,
                    w.UserId_int,
                    w.WatcherType_ti,
                    w.WatcherName_vch,
                    twl.TicketId_bi,
                    u.FirstName_vch,
                    u.LastName_vch
                FROM 
                    troubletickets.watcher_list	w
                INNER JOIN
                    troubletickets.ticket_watcher_list twl
                ON
                    w.PKId_bi=twl.WatcherId_bi	
                LEFT JOIN
                    simpleobjects.useraccount u
                ON
                    w.UserId_int= u.UserId_int			
                WHERE
                    twl.TicketId_bi =  <CFQUERYPARAM CFSQLTYPE='CF_SQL_BIGINT' VALUE='#arguments.inpTicketId#'>            
                AND
                    w.UserId_int= <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#arguments.inpUserId#'> 
            </cfquery>

            <cfif GetListWatcher.RecordCount GT 0>
                <cfset dataout.RXRESULTCODE = 1 />
                <cfset dataout.MESSAGE = "Get watcher list successfully." />
                <cfloop query="GetListWatcher">

                    <cfif GetListWatcher.WatcherType_ti EQ 1>
                        <!--- get list email --->
                        <cfset listEmail = ListAppend(listEmail,GetListWatcher.WatcherName_vch,";")/>
                    <cfelseif GetListWatcher.WatcherType_ti EQ 2>
                        <!--- notify sms --->
                        <cftry>
                            <!--- get user curent short code --->
                            <cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="RetVarshortCode"></cfinvoke>

                            <cfset phone = ReReplaceNoCase(GetListWatcher.WatcherName_vch,"[^0-9,]","","ALL")/>
                            
                            <cfinvoke method="SendSingleMT" returnvariable="RetVarSendSingleMT">
                                <cfinvokeargument name="inpContactString" value="#phone#"/>
                                <cfinvokeargument name="inpShortCode" value="#RetVarshortCode.SHORTCODE#"/>
                                <cfinvokeargument name="inpTextToSend" value="Trouble ticket Id is #arguments.inpTicketId# - Assign to #GetListWatcher.FirstName_vch# #GetListWatcher.LastName_vch# just have changed status to #arguments.inpTroubleStatus#."/>
                                <cfinvokeargument name="inpSkipLocalUserDNCCheck" value="1"/> <!--- option to send to phone number has opt-out --->
                            </cfinvoke>
                            
                        <cfcatch>
                        </cfcatch>
                        </cftry>
                    </cfif>
                </cfloop>

                <cfif listEmail NEQ "">
                    <!--- notify email --->
                    <cfset dataMail.FULLNAME="#GetListWatcher.FirstName_vch# #GetListWatcher.LastName_vch#"/>
                    <cfset dataMail.TROUBLETICKETID = arguments.inpTicketId/>
                    <cfset dataMail.STATUS = arguments.inpTroubleStatus/>

                    <cfset arguments.data = serializeJSON(dataMail)/>
                    <cfmail to="#listEmail#" subject="Notify Trouble Ticket Change Status" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#"  port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfinclude template="/session/sire/views/emailtemplates/mail_template_notify_trouble_ticket_change_status.cfm" >
                    </cfmail>
                </cfif>
            </cfif>

        <cfcatch type="any">
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>
        <cfreturn dataout/>
    </cffunction>

    <!--- add history trouble ticket --->
    <cffunction name="CreateUserLog" access="public" output="true" hint="Store history trouble ticket">
		<cfargument name="inpTicketId" required="true" hint ="Id of ticket">
		<cfargument name="inpUserId" required="false" default ="#session.USERID#" hint ="User id">
        <cfargument name="inpNote" required="true" hint="Action detail">
        <cfargument name="inpActionName" required="true" hint ="Action Name">
				
		<!--- Default Return Structure --->		
		<cfset var dataout	= {} />
		
		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />		
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		
		<cfset var insertUserLog = '' />
					
		<cftry>                  
			<cfquery name="insertUserLog" datasource="#Session.DBSourceEBM#">
				INSERT INTO
					troubletickets.history
				(
					TicketId_bi,
                    UserId_int,
                    Created_dt,
                    Note_vch,
                    ActionName_vch
				)
				VALUES
				(
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpTicketId#"/>,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/>, 
                    NOW(),
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpNote#" />,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpActionName#" />
					
				)
			</cfquery>
						
			<cfset dataOut.RXRESULTCODE = 1>  
            <cfset dataout.TYPE = "" />		
            <cfset dataout.MESSAGE = "Successfull" />
            <cfset dataout.ERRMESSAGE = "" />        
			
			<cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1 />
				</cfif>
				
				<cfset dataOut.RXRESULTCODE = cfcatch.errorcode>
				<cfset dataOut.TYPE = "#cfcatch.TYPE#">
				<cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataOut.ERRMESSAGE = "#cfcatch.detail#">
			</cfcatch>
		</cftry>
		
		<cfreturn dataout />
	</cffunction>

    <!--- Get user Infor --->
    <cffunction name="GetUserInfor" access="public" output="false" hint="get user infor">
		<cfargument name="inpIdUser" TYPE="string" hint="User ID" default="#session.USERID#" >
				
		<cfset var dataout = {}>
		<cfset var GetUserInforName = '' />
		
		<!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 /> 		
		<cfset dataout.FIRSTNAME = "" />
		<cfset dataout.LASTNAME = "" />
		<cfset dataout.PHONENUMBER = "" />
		<cfset dataout.EMAIL = "" />
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.SECURITYCREDENTIAL = 0 />
		<cfset dataout.AMOUNTLIMITED = 0 />
		<cfset dataout.ALLEDITXMLCS ='0'/>
		<cfset dataout.FULLNAME = '' />
		<cfset dataout.PAYMENTGATEWAY = '' />
		
		<cfset dataout.FULLNAME = '' />
		<cfset dataout.CAPTURECC = '' />
		<cfset var RetVarGetAdminPermission	= '' />

		<cfif arguments.inpIdUser GT 0>
			<cftry>
	            <cfquery name="GetUserInforName" datasource="#Session.DBSourceREAD#">              
					SELECT
						EmailAddress_vch,MFAContactString_vch,FirstName_vch,LastName_vch,MFAContactType_ti,MFAEXT_vch,SecurityQuestionEnabled_ti,Address1_vch,PostalCode_vch,MFAEnabled_ti,SecurityCredential_ti,AllowNotRealPhone_ti,
					LimitedPurchaseAmount_int, AllowEditXMLControlString_ti,PaymentGateway_ti,CaptureCCInfo_ti
					FROM
						simpleobjects.useraccount
					WHERE                
						userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpIdUser#">
					AND
						Active_int > 0       
	            </cfquery>  
	            
				<cfif GetUserInforName.RecordCount gt 0>
					<cfset dataout.RXRESULTCODE = 1 />
					<cfset dataout.FIRSTNAME = "#GetUserInforName.FirstName_vch#" />
					<cfset dataout.LASTNAME = "#GetUserInforName.LastName_vch#" />
					<cfset dataout.PHONENUMBER = "#GetUserInforName.MFAContactString_vch#"/>
					<cfset dataout.EMAIL = "#GetUserInforName.EmailAddress_vch#" />
					<cfset dataout.MFATYPE = "#GetUserInforName.MFAEnabled_ti#" />
					<cfset dataout.SECURITYCREDENTIAL = "#GetUserInforName.SecurityCredential_ti#" />
					<cfset dataout.ALLOWNOTPHONE = "#GetUserInforName.AllowNotRealPhone_ti#" />
					<cfset dataout.AMOUNTLIMITED = "#GetUserInforName.LimitedPurchaseAmount_int#" />
					<cfset dataout.ALLEDITXMLCS = "#GetUserInforName.AllowEditXMLControlString_ti#" />
				<cfset dataout.FULLNAME =  dataout.FIRSTNAME&" "&dataout.LASTNAME />				
				<cfset dataout.PAYMENTGATEWAY = "#GetUserInforName.PaymentGateway_ti#" />
				<cfset dataout.CAPTURECC = "#GetUserInforName.CaptureCCInfo_ti#" />

				<cfelse>
					<cfset dataout.RXRESULTCODE = -1 />
				</cfif>
										
				<cfcatch TYPE="any">
						<cfset dataout.RXRESULTCODE = -1 /> 
						<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
						<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
					<cfreturn dataout>
				</cfcatch>
	        </cftry>
        </cfif>
		<cfreturn dataout />	
	</cffunction>
</cfcomponent>	