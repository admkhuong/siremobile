<cfcomponent hint="Trouble ticket watcher" output="false">
    <cfparam name="Session.DBSourceEBM" default="BISHOP"/>
    <cfinclude template="/session/cfc/csc/constants.cfm">	
    <cfinclude template="/session/sire/configs/paths.cfm">
    <cffunction name="GetListWatcher" access="remote" output="false" hint="Get list watcher">
		<cfargument name="inpTicketId" TYPE="string" required="yes" default="" />
		<cfargument name="inpSkipPaging" TYPE="string" required="no" default="0" />
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="100" />
        <cfargument name="inpUserId" TYPE="string" required="no" default="#session.UserId#" />
        <cfargument name="inpName" TYPE="string" required="no" default="" />
		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default=""/>		
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout["aaData"] = ArrayNew(1)/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>		
        

		<cfset var item = '' />
		<cfset var GetListWatcher = '' />
		<cfset var rsCount = '' />
        <cfset var phonenumberformat = ''/>

		<cfset var orderField = '' />
		<cfswitch expression="#arguments.iSortCol_0#">		    
		    <cfcase value="0">
		        <cfset orderField = 'w.PKId_bi' />
		    </cfcase>	
            <cfdefaultcase>
                <cfset orderField = 'w.PKId_bi' />
            </cfdefaultcase>	    	    
		</cfswitch>

		<cftry>			
            
			<cfquery name="GetListWatcher" datasource="#Session.DBSourceREAD#">
                SELECT 
                    SQL_CALC_FOUND_ROWS
                    w.PKId_bi,
                    w.UserId_int,
                    w.WatcherType_ti,
                    w.WatcherName_vch,
                    twl.TicketId_bi,
                    w.Created_dt,
                    w.Updated_dt,
                    u.FirstName_vch,
                    u.LastName_vch
                FROM 
                    troubletickets.watcher_list	w
                INNER JOIN
                    troubletickets.ticket_watcher_list twl
                ON
                    w.PKId_bi=twl.WatcherId_bi
                LEFT JOIN
                    simpleobjects.useraccount u
                ON
                    w.UserId_int= u.UserId_int				
				WHERE
					twl.TicketId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpTicketId#"/>                
                AND
                    w.UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/>                
                <cfif arguments.inpName NEQ "">
                AND

                (  w.WatcherName_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpName#%"/> 
                    OR   
                   w.WatcherName_vch LIKE <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#REReplaceNoCase(arguments.inpName, "[^\d^*^P^X^##]", "", "ALL")#'> 
                )      
                </cfif>
				<cfif orderField NEQ "">
					ORDER BY
						#orderField# #arguments.sSortDir_0#
				<cfelse>
					ORDER BY 
						w.PKId_bi DESC
				</cfif>
                <cfif arguments.inpSkipPaging EQ 0>
				LIMIT 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#"/>, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"/>
                </cfif>
			</cfquery>	
			
			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>		
			<cfif GetListWatcher.RecordCount GT 0>
				<cfloop query="GetListWatcher"> 
                    <cfset phonenumberformat = WatcherName_vch/>

                    <cfif WatcherType_ti EQ 2>
                        <!--- format phone number (XXX) XXX-XXX --->
                        <cfif len(WatcherName_vch) EQ 10>
                            <!--- This only works with 10-digit US/Canada phone numbers --->
                            <cfset phonenumberformat =  "(#left(WatcherName_vch,3)#) #mid(WatcherName_vch,4,3)#-#right(WatcherName_vch,4)#" />
                        </cfif>
                    </cfif>

					<cfset item = {
						ID: PKId_bi, 
						TYPE: WatcherType_ti,
                        NAME: phonenumberformat,
                        USERID: UserId_int,
                        USER_FULL_NAME: FirstName_vch & " " & LastName_vch,                       
                        SHOWACTION: UserId_int EQ Session.UserID ? "Y":"N"
					} />
					<cfset arrayAppend(dataout["aaData"],item) />
				</cfloop>
				
				<cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
				<cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>
			</cfif>
			

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
    <cffunction name="InsertUpdateWatcher" access="remote" output="false" hint="insert new watcher">
		<cfargument name="inpId" TYPE="numeric" required="no" default="0"/>
        <cfargument name="inpTicketId" TYPE="numeric" required="yes" default="0"/>        		
		<cfargument name="inpUserId" TYPE="string" required="no" default="#session.UserId#" />
        <cfargument name="inpWatcherType" TYPE="string" required="true" default="" />
        <cfargument name="inpWatcherName" TYPE="string" required="true" default=""/>
			

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />		

		<cfset var InsertUpdateWorkLog = ""/>
        <cfset var InsertTicketWatcherList= ''>
        <cfset var rsInsertUpdateWorkLog=''>
        <cfset var CheckWatcherExist= ''>
        <cfset var watcherId=0>
        <cfset var CheckWatcherTicketExist= ''>
		<cftry>		                    
            <cfif arguments.inpWatcherType EQ 2>        
                <cfset arguments.inpWatcherName = REReplaceNoCase(arguments.inpWatcherName, "[^\d^\*^P^X^##]", "", "ALL")>                          
                <!--- Check phone number US --->
                <cfif NOT isvalid('telephone', arguments.inpWatcherName)>
                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.ERRMESSAGE = 'SMS Number is not a valid US telephone number !'> 
                    <cfset dataout.MESSAGE = 'SMS Number is not a valid US telephone number !'>
                    <cfreturn dataout>
                </cfif>    
            </cfif>
            <!--- check watcher exiting--->
            <cfquery name="CheckWatcherExist" datasource="#Session.DBSourceREAD#" >
                SELECT 
                    UserId_int,
                    PKId_bi
                FROM
                    troubletickets.watcher_list
                WHERE
                    WatcherType_ti=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpWatcherType#">                     
                AND
                    WatcherName_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpWatcherName#">                      
                AND
                    UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/>                
                AND
                    PKId_bi <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpId#">                     
            </cfquery>
            <cfif CheckWatcherExist.recordcount GT 0>
                <cfset watcherId=CheckWatcherExist.PKId_bi>                
            </cfif>

			<cfif arguments.inpId is 0> <!--- insert cases--->
                <cfif watcherId EQ 0> <!--- only insert watcher list when watcher id not exitsting  --->
                    <cfquery name="InsertUpdateWorkLog" datasource="#Session.DBSourceEBM#" result="rsInsertUpdateWorkLog">
                        INSERT INTO troubletickets.watcher_list
                        (                        
                            UserId_int,                        
                            WatcherType_ti,
                            WatcherValue_vch,
                            WatcherName_vch,
                            Status_ti,
                            Created_dt,
                            Updated_dt
                        )
                        VALUES
                        (			                        
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpWatcherType#">,			
                            '',
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpWatcherName#">,			
                            1,                        
                            NOW(),
                            NOW()
                        )
                    </cfquery>	
                    <cfset watcherId=rsInsertUpdateWorkLog.generatedkey>
                </cfif> 
                <!--- check watcher ticket exiting--->
                <cfquery name="CheckWatcherTicketExist" datasource="#Session.DBSourceREAD#" >
                    SELECT                         
                        PKId_bi
                    FROM
                        troubletickets.ticket_watcher_list
                    WHERE
                        WatcherId_bi=<CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#watcherId#">                     
                    AND
                        TicketId_bi=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpTicketId#">                                          
                </cfquery>    
                <cfif CheckWatcherTicketExist.RECORDCOUNT GT 0>
                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.ERRMESSAGE = 'Watcher #arguments.inpWatcherName# is existing on this ticket already, please try anothers!'> 
                    <cfset dataout.MESSAGE = 'Watcher #arguments.inpWatcherName# is existing on this ticket already, please try anothers!'>
                    <cfreturn dataout>
                <cfelse>           
                    <cfquery name="InsertTicketWatcherList" datasource="#Session.DBSourceEBM#">				
                        INSERT INTO troubletickets.ticket_watcher_list
                        (
                            WatcherId_bi,
                            TicketId_bi
                        )
                        VALUES
                        (
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#watcherId#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpTicketId#">
                        )
                    </cfquery>	                    
                </cfif>
				<!--- Create log user --->
                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="Trouble Ticket">
                    <cfinvokeargument name="operator" value="Trouble Ticket - TicketID:#arguments.inpTicketId# - Insert Watcher">
                </cfinvoke>
			<cfelse><!--- update cases--->
				<cfquery name="InsertUpdateWorkLog" datasource="#Session.DBSourceEBM#">
					UPDATE troubletickets.watcher_list
					SET						                        	                                                
                        WatcherType_ti=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpWatcherType#">,                        
                        WatcherName_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpWatcherName#">,                      
                        Updated_dt=NOW()
					WHERE
						PKId_bi=<CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpId#">
				</cfquery>	
                <!--- Create log user --->
                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="Trouble Ticket">
                    <cfinvokeargument name="operator" value="Trouble Ticket - TicketID:#arguments.inpTicketId# - Update Watcher">
                </cfinvoke>				
			</cfif>
            <cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "Insert Successfully"/>
			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
    <cffunction name="DeleteWatcherByID" access="remote" output="false" hint="delete watcher by id">
		<cfargument name="inpTicketID" type="numeric" required="true" hint=""/>
        <cfargument name="inpWatcherID" type="numeric" required="true" hint=""/>

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = ""/>
		
		<cfset var DeleteWatcherByID = '' />

		<cftry>
            <cfquery result="DeleteWatcherByID" datasource="#Session.DBSourceEBM#">
                DELETE FROM 
                    troubletickets.ticket_watcher_list                
                WHERE
                    WatcherId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpWatcherID#">
                AND
                    TicketId_bi= <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpTicketID#">
            </cfquery>
            <!--- Create log user --->
            <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                <cfinvokeargument name="moduleName" value="Trouble Ticket">
                <cfinvokeargument name="operator" value="Trouble Ticket - TicketID:#arguments.inpTicketId# - Delete Watcher">
            </cfinvoke>		
            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.MESSAGE = "Delete this watcher successfully." />

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>
	</cffunction>
	
    
</cfcomponent>	