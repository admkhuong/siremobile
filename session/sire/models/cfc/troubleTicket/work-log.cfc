<cfcomponent hint="Trouble ticket work log" output="false">
    <cfparam name="Session.DBSourceEBM" default="BISHOP"/>	
    <cfinclude template="/session/sire/configs/paths.cfm">
    <cffunction name="GetListWorkLog" access="remote" output="false" hint="Get list Ticket work log">
		<cfargument name="inpTicketId" TYPE="string" required="yes" default="" />
		<cfargument name="inpSkipPaging" TYPE="string" required="no" default="0" />
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="100" />

		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default=""/>		
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout["aaData"] = ArrayNew(1)/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />
        <cfset dataout.TOTALTIME = '0' />

		<cfset var item = '' />
		<cfset var GetListWorkLog = '' />
		<cfset var rsCount = '' />

		<cfset var orderField = '' />
		<cfswitch expression="#arguments.iSortCol_0#">		    
		    <cfcase value="0">
		        <cfset orderField = 'w.PKId_bi' />
		    </cfcase>	
            <cfdefaultcase>
                <cfset orderField = 'w.PKId_bi' />
            </cfdefaultcase>	    	    
		</cfswitch>

		<cftry>								
			<cfquery name="GetListWorkLog" datasource="#Session.DBSourceREAD#">
                SELECT 
                    SQL_CALC_FOUND_ROWS
                    w.PKId_bi,
                    w.UserId_int,
                    w.StartDate_dt,
                    w.Created_dt,
                    w.TotalTime_dec,
                    w.Note_vch,
                    u.FirstName_vch,
                    u.LastName_vch
                FROM 
                    troubletickets.worklog	w
                LEFT JOIN
                    simpleobjects.useraccount u
                ON
                    w.UserId_int= u.UserId_int				
				WHERE
					w.TicketId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpTicketId#"/>                
				<cfif orderField NEQ "">
					ORDER BY
						#orderField# #arguments.sSortDir_0#
				<cfelse>
					ORDER BY 
						w.PKId_bi DESC
				</cfif>
                <cfif arguments.inpSkipPaging EQ 0>
				LIMIT 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#"/>, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"/>
                </cfif>
			</cfquery>	
			
			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>		
			<cfif GetListWorkLog.RecordCount GT 0>
				<cfloop query="GetListWorkLog">
                    <cfset dataout.TOTALTIME += TotalTime_dec />
					<cfset item = {
						ID: PKId_bi, 
						TOTALTIME: TotalTime_dec,
                        USERID:UserId_int,
                        USER_FULL_NAME: FirstName_vch & " " & LastName_vch,
                        START_DATE: LSDateFormat(StartDate_dt, "yyyy-mm-dd") & " " & LSTimeFormat(StartDate_dt, "HH:mm"),
                        PICKER_DATE: LSDateFormat(StartDate_dt, "MM/DD/YYYY") & " " & LSTimeFormat(StartDate_dt, "HH:mm"),
                        NOTES:Note_vch,
                        SHOWACTION: UserId_int EQ Session.UserID ? "Y":"N"
					} />
					<cfset arrayAppend(dataout["aaData"],item) />
				</cfloop>
				
				<cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
				<cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>
			</cfif>
			

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
    <cffunction name="InsertUpdateWorkLog" access="remote" output="false" hint="insert new works log">
		<cfargument name="inpId" TYPE="numeric" required="no" default="0"/>
        <cfargument name="inpTicketId" TYPE="numeric" required="yes" default="0"/>        
		<cfargument name="inpStartDate" TYPE="string" required="yes" default="" hint="date format should be mm/dd/yyyy" />
        <cfargument name="inpSpendTime" TYPE="string" required="yes" default="" hint="time format should be HH:mm (24h format)" />        
		<cfargument name="inpUserId" TYPE="string" required="no" default="#session.UserId#" />
			

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />		

		<cfset var InsertUpdateWorkLog = ""/>
        <cfset var startDate= NOW()>
        
        <cfset var datePart="">
        <cfset var timePart="">
        <cfset var d="">
        <cfset var m="">
        <cfset var y="">

        <cfset var h="">
        <cfset var s="">
        
		<cftry>		    
            
            <cfset datePart=arguments.inpStartDate.split(" ")[1]>
            <cfset timePart=arguments.inpStartDate.split(" ")[2]> 
            <cfset d=datePart.split("/")[2]>
            <cfset m=datePart.split("/")[1]>
            <cfset y=datePart.split("/")[3]>

            <cfset h= timePart.split(":")[1]>
            <cfset s= timePart.split(":")[2]>
            <cfset startDate= CreateDateTime(y,m,d,h,s,0)> 
            
			<cfif arguments.inpId is 0>
				<cfquery name="InsertUpdateWorkLog" datasource="#Session.DBSourceEBM#">
					INSERT INTO troubletickets.worklog
					(			
                        TicketId_bi,                        			
                        UserId_int,
                        TotalTime_dec,
                        Note_vch,
                        Created_dt,
                        StartDate_dt			  	
					)
					VALUES
					(			
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpTicketId#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpUserId#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#arguments.inpSpendTime#">,			
                        '',
						NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_TIMESTAMP" VALUE="#startDate#">			
					)
				</cfquery>					
				<!--- Create log user --->
				<cfinvoke method="CreateUserLog" component="session.sire.models.cfc.troubleTicket.trouble-ticket">
					<cfinvokeargument name="inpTicketId" value="#arguments.inpTicketId#">
					<cfinvokeargument name="inpNote" value="Insert Work Log">
					<cfinvokeargument name="inpActionName" value="Trouble Ticket - Insert Work Log">
				</cfinvoke>	
				
			<cfelse>
				<cfquery name="InsertUpdateWorkLog" datasource="#Session.DBSourceEBM#">
					UPDATE troubletickets.worklog
					SET						                        	                        
                        TotalTime_dec= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#arguments.inpSpendTime#">,
                        Note_vch ='',                        
                        StartDate_dt= 	<CFQUERYPARAM CFSQLTYPE="CF_SQL_TIMESTAMP" VALUE="#startDate#">			
					WHERE
						PKId_bi=<CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpId#">
				</cfquery>		
				<!--- Create log user --->
				<cfinvoke method="CreateUserLog" component="session.sire.models.cfc.troubleTicket.trouble-ticket">
					<cfinvokeargument name="inpTicketId" value="#arguments.inpTicketId#">
					<cfinvokeargument name="inpNote" value="Update Work Log">
					<cfinvokeargument name="inpActionName" value="Trouble Ticket - Update Work Log">
				</cfinvoke>
				
			</cfif>
			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "Update Successfully"/>

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
    <cffunction name="DeleteWorkLogByID" access="remote" output="false" hint="delete ticket work log by id">
		<cfargument name="inpId" type="numeric" required="true" hint="id of message"/>
		<cfargument name="inpTicketId" TYPE="numeric" required="yes" default="0"/>        

		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = ""/>
		
		<cfset var DeleteWorkLog = '' />

		<cftry>
            <cfquery result="DeleteWorkLog" datasource="#Session.DBSourceEBM#">
                DELETE FROM 
                    troubletickets.worklog                
                WHERE
                    PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpId#"/>
            </cfquery>
			<!--- Create log user --->
			<cfinvoke method="CreateUserLog" component="session.sire.models.cfc.troubleTicket.trouble-ticket">
				<cfinvokeargument name="inpTicketId" value="#arguments.inpTicketId#">
				<cfinvokeargument name="inpNote" value="Delete Work Log">
				<cfinvokeargument name="inpActionName" value="Trouble Ticket - Delete Work Log">
			</cfinvoke>

            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.MESSAGE = "Delete this work log successfully." />

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>
	</cffunction>
	
    
</cfcomponent>	