<cfcomponent>

	<cfinclude template="/public/sire/configs/paths.cfm" >
	<cfinclude template="../../configs/paths.cfm" >
	<cffunction name="GetTroubleTicket" access="remote" output="true" hint="Get list trouble ticket">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="customFilter" default=""><!---this is the custom filter data --->

		<cfset var dataout = {} />
		<cfset dataout.DATA = ArrayNew(1)>
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRORMESSAGE = '' />
		<cfset var total_pages = 1>
		<cfset var order 	= "">
		<cfset var ticketDetail = "" />
		<cfset var actionHtml = "" />
		<cfset var status = "" />
		<cfset var GetNumbersCount = "" />
		<cfset var GetTroubleTicket = "" />

		<cfset var tempItem = '' />
		<cfset var filterItem = '' />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>
		
		<!---ListEMSData is key of DataTable control--->
		<cfset dataout["ListEMSData"] = ArrayNew(1)>
		<cfset order = #BuildSortingParamsForDatatable(iSortCol_0,sSortDir_0)#>

		<!---Get total EMS for paginate --->
		<cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
			SELECT
				COUNT(*) AS TOTALCOUNT
			FROM 
				simplequeue.supportapp sa	
			
		    <cfif customFilter NEQ "">
		   		WHERE 1 = 1
				<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
					<cfif filterItem.NAME EQ ' sa.Status_int ' AND  filterItem.VALUE LT 0>
					<!--- DO nothing --->
					<cfelseif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
					<cfelse>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
					</cfif>
				</cfloop>
			</cfif>
		</cfquery>
		<cfif GetNumbersCount.TOTALCOUNT GT 0>
			<cfset dataout["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
			<cfset dataout["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>
        </cfif>

        <!--- Get trouble ticket --->		
		<cfquery name="GetTroubleTicket" datasource="#Session.DBSourceREAD#">
			SELECT
				sa.PKId_bi,
				sa.BatchId_bi, 
				sa.CustomerMessage_vch, 
				sa.ContactString_vch, 
				sa.UserId_int,
				sa.Created_dt,
				sa.Status_int,
				sa.Note_vch,
				sa.Notified_int
			FROM 
				simplequeue.supportapp sa	
		   <cfif customFilter NEQ "">
		   	WHERE 1 = 1
				<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
					<cfif filterItem.NAME EQ ' sa.Status_int ' AND  filterItem.VALUE LT 0>
					<!--- DO nothing --->
					<cfelseif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
					<cfelse>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
					</cfif>
				</cfloop>
			</cfif>
			#order#
			LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
        </cfquery>

        <cfloop query="GetTroubleTicket">
            <cfset actionHtml = ''>
            <cfset status = ''>

            <cfswitch expression = #GetTroubleTicket.Status_int#>
            	<cfcase value="0"> <cfset status = 'Open'> </cfcase> 
            	<cfcase value="1"> <cfset status = 'Assigned'> </cfcase> 
            	<cfcase value="2"> <cfset status = 'In Progress'> </cfcase> 
            	<cfcase value="3"> <cfset status = 'On Hold'> </cfcase> 
            	<cfcase value="4"> <cfset status = 'Closed'> </cfcase> 
            </cfswitch>
            <cfif GetTroubleTicket.Notified_int EQ 0 && GetTroubleTicket.Status_int EQ 4>

	            <cfset actionHtml = '<a data-id="#GetTroubleTicket.PKId_bi#" class="edit-trouble-ticket" data-target="##AdminTroubleTicketModal">
							<img width="19" src="/session/sire/images/icon/icon_edit_active.png" class="subscriber-detail-icon" /></a> 
							<a data-id="#GetTroubleTicket.PKId_bi#" class="notify-trouble-ticket" data-target="##AdminTroubleTicketNotifyModal">
							<img width="19" src="/session/sire/images/icon/icon_message_active.png" class="subscriber-detail-icon" /></a>'>
			<cfelse>
				<cfset actionHtml = '<a data-id="#GetTroubleTicket.PKId_bi#" class="edit-trouble-ticket">
							<img width="19" data-target="##AdminTroubleTicketModal" src="/session/sire/images/icon/icon_edit_active.png" class="subscriber-detail-icon" /></a> 
							<a data-id="#GetTroubleTicket.PKId_bi#" class="disabled-notify-trouble-ticket">
							<img width="19" src="/session/sire/images/icon/icon_message.png" class="subscriber-detail-icon" /></a>'>
			</cfif>
			<cfif Len(GetTroubleTicket.CustomerMessage_vch) LTE 100>
				<cfset ticketDetail = GetTroubleTicket.CustomerMessage_vch>
			<cfelse>
				<cfset ticketDetail = Left(GetTroubleTicket.CustomerMessage_vch,100) & "...">
			</cfif>
			<cfset tempItem = [
				'#GetTroubleTicket.PKId_bi#',
				'#GetTroubleTicket.BatchId_bi#',
				'#GetTroubleTicket.UserId_int#',
				'#GetTroubleTicket.ContactString_vch#',
				'#LSDateFormat(GetTroubleTicket.Created_dt, 'mm/dd/yyyy')#',
				'#status#',
				'#ticketDetail#',
				'#GetTroubleTicket.Note_vch#',
				'#actionHtml#'
			]>	
			<cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
	    </cfloop>

	    <cfreturn serializeJSON(dataOut)>

	</cffunction>

	<cffunction name="BuildSortingParamsForDatatable" access="public" hint="build sorting params for datatable">
		<cfargument name="iSortCol_0" default="-1">
		<cfargument name="sSortDir_0" default="">
			<cfset var dataout= "">
			<cftry>
            	<!---set order param for query--->
				<cfif iSortCol_0 EQ 0>	
					<cfset order_0 = "desc">	
				<cfelse>
					<cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
						<cfset var order_0 = sSortDir_0>
					<cfelse>
						<cfset order_0 = "">	
					</cfif>
				</cfif>

				<cfif iSortCol_0 neq -1 and order_0 NEQ "">
					<cfset dataout = dataout &" order by ">
					<cfif iSortCol_0 EQ 1> 
						<cfset dataout = dataout &" sa.BatchId_bi ">
					<cfelseif iSortCol_0 EQ 2>
							<cfset dataout = dataout &" sa.UserId_int ">
					<cfelseif iSortCol_0 EQ 4>
							<cfset dataout = dataout &" sa.Created_dt ">
					<cfelse>
							<cfset dataout = dataout &" sa.PKId_bi ">	
					</cfif>
					<cfset dataout = dataout &"#order_0#">
				</cfif>      
            <cfcatch type="Any" >
				<cfset dataout = "">
            </cfcatch>
            </cftry>
			<cfreturn dataout>
	</cffunction>

	<cffunction name="GetTroubleTicketDetailById" access="remote" output="false" hint="Get trouble ticket detail by Ticket Id">
		<cfargument name="inpTicketId" required="true">

		<cfset var dataout = {} />
		<cfset var getTroubleTicketDetail = '' />
		<cfset var userInfo = '' />

		<cftry>
			<cfquery name="getTroubleTicketDetail" datasource="#Session.DBSourceREAD#">
				SELECT
					ContactString_vch,
					CustomerMessage_vch,
					Status_int,
					Note_vch,
					UserId_int,
					BatchId_bi
				FROM
					simplequeue.supportapp
				WHERE
					PKId_bi = <CFQUERYPARAM CFSQLTYPE='CF_SQL_Integer' VALUE='#arguments.inpTicketId#'>
			</cfquery>
			<cfif getTroubleTicketDetail.Recordcount GT 0>
				<cfinvoke method="GetUserInfoById" component="public.sire.models.cfc.userstools" returnvariable="userInfo">
					<cfinvokeargument name="inpUserId" value="#getTroubleTicketDetail.UserId_int#">
				</cfinvoke>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = "Get trouble ticket detail successfully!" />
				<cfset dataout.PHONE = "#getTroubleTicketDetail.ContactString_vch#" />
				<cfset dataout.DETAIL = "#getTroubleTicketDetail.CustomerMessage_vch#" />
				<cfset dataout.STATUS = "#getTroubleTicketDetail.Status_int#" />
				<cfset dataout.NOTE = "#getTroubleTicketDetail.Note_vch#" />
				<cfset dataout.EMAIL = "#userInfo.FULLEMAIL#" />
				<cfset dataout.USERID = "#getTroubleTicketDetail.UserId_int#" />
				<cfset dataout.BATCHID = "#getTroubleTicketDetail.BatchId_bi#" />
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = "No record found" />
			</cfif>
			<cfcatch type="any">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="UpdateTroubleTicketDetailById" access="remote" output="false" hint="Update trouble ticket detail by Ticket Id">
		<cfargument name="inpTicketId" required="true">
		<cfargument name="inpStatus" required="true">
		<cfargument name="inpDetail" required="true">
		<cfargument name="inpNote" required="false" default="">
		<cfargument name="inpPhone" required="true">
		<cfargument name="inpUserId" required="true">
		<cfargument name="inpBatchId" required="true">

		<cfset var dataout = {} />
		<cfset var updateTroubleTicketDetail = '' />
		<cfset var updateResult = '' />

		<cftry>
			<cfif NOT isvalid('telephone', arguments.inpPhone)>
                <cfset dataout.RXRESULTCODE = -2>
                <cfset dataout.MESSAGE = 'Phone Number is not a valid US telephone number !'>
                <cfreturn dataout>
            </cfif>
			<cfquery name="updateTroubleTicketDetail" datasource="#Session.DBSourceEBM#" result="updateResult">
				UPDATE
					simplequeue.supportapp
				SET
					ContactString_vch = <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#arguments.inpPhone#'>,
					CustomerMessage_vch = <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#arguments.inpDetail#'>,
					Note_vch = <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#arguments.inpNote#'>,
					Status_int = <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#arguments.inpStatus#'>,
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#arguments.inpBatchId#'>,
					UserId_int = <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#arguments.inpUserId#'>
				WHERE
					PKId_bi = <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#arguments.inpTicketId#'>
			</cfquery>

			<cfif updateResult.Recordcount GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = "Update trouble ticket successfully!" />
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = "Update failed" />
			</cfif>
			<cfcatch type="any">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="SendNotifyEmail" access="remote" output="false" hint="Send notify email to Sire's users for their trouble ticket">
		<cfargument name="inpEmail" required="false">
		<cfargument name="inpMessage" required="false">
		<cfargument name="inpSubject" required="false">
		<cfargument name="inpTicketId" required="true">
		<cfset var dataout = {} />
		<cfset var updateTroubleTicketDetail = '' />
		<cfset var dataMail = '' />
		<cfset var updateResult = '' />

		<cftry>
			<cfif arguments.inpEmail EQ ''>
				<cfset dataout.RXRESULTCODE = -2 />
				<cfset dataout.MESSAGE = 'Please insert Email Address!' />
				<cfreturn dataout />
			<cfelseif arguments.inpMessage EQ ''>
				<cfset dataout.RXRESULTCODE = -2 />
				<cfset dataout.MESSAGE = 'Please insert Email Message!' />
				<cfreturn dataout />
			<cfelseif arguments.inpSubject EQ ''>
				<cfset dataout.RXRESULTCODE = -2 />
				<cfset dataout.MESSAGE = 'Please insert Email Subject!' />
				<cfreturn dataout />
			<cfelseif arguments.inpTicketId EQ '' || arguments.inpTicketId LTE 0>
				<cfset dataout.RXRESULTCODE = -2>
				<cfset dataout.MESSAGE = 'Invalid ticket Id!' />
				<cfreturn dataout />
			<cfelse>
				<cfset dataMail = {
					message: arguments.inpMessage
				} />

				<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
	                <cfinvokeargument name="to" value="#arguments.inpEmail#">
	                <cfinvokeargument name="type" value="html">
	                <cfinvokeargument name="subject" value="#arguments.inpSubject#">
	                <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_notify_trouble_ticket.cfm">
	                <cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#">
	            </cfinvoke>

	            <cfquery name="updateTroubleTicketDetail" datasource="#Session.DBSourceEBM#" result="updateResult">
					UPDATE
						simplequeue.supportapp
					SET
						Notified_int = Notified_int + 1
					WHERE
						PKId_bi = <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#arguments.inpTicketId#'>
				</cfquery>

	            <cfset dataout.RXRESULTCODE = 1>
	            <cfset dataout.MESSAGE = 'Send Notify Email successfully!'>
	        </cfif>
			<cfcatch type="any">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="SendNotifySMS" access="remote" output="false" hint="Send notify SMS to Sire's users for their trouble ticket">
		<cfargument name="inpMessage" required="true">
		<cfargument name="inpTicketId" required="true">
		<cfargument name="inpPhone" required="true">

		<cfset var dataout = {} />
		<cfset var variables = {} />
		<cfset var datetime = '' />
		<cfset var signature = '' />
		<cfset var authorization = '' />
		<cfset var apiUrl = '' />
		<cfset var updateTroubleTicketDetail = '' />
		<cfset var InsertToAPILog = '' />
		<cfset var returnStruct = '' />
		<cfset var updateResult = '' />

		<cftry>
			<cfif arguments.inpPhone EQ ''>
				<cfset dataout.RXRESULTCODE = -2>
				<cfset dataout.MESSAGE = 'Please insert Phone number!' />
				<cfreturn dataout />
			<cfelseif arguments.inpMessage EQ '' >
				<cfset dataout.RXRESULTCODE = -2>
				<cfset dataout.MESSAGE = 'Please insert SMS message!' />
				<cfreturn dataout />
			<cfelseif arguments.inpTicketId EQ '' || arguments.inpTicketId LTE 0>
				<cfset dataout.RXRESULTCODE = -2>
				<cfset dataout.MESSAGE = 'Invalid ticket Id!' />
				<cfreturn dataout />
			</cfif>
			<cfset variables.accessKey = '8A8C2A51BC345F301417' />
			<cfset variables.secretKey = '1091c2F+b59dD076Ea52EF531a245f0Cb9E46d11' />

			<cfset variables.sign = generateSignature("POST", variables.secretKey) />
			<cfset datetime = variables.sign.DATE>
			<cfset signature = variables.sign.SIGNATURE>
			<cfset authorization = variables.accessKey & ":" & signature>

			<cfif FINDNOCASE(CGI.SERVER_NAME,'siremobile.com') GT 0 OR FINDNOCASE(CGI.SERVER_NAME,'www.siremobile.com') GT 0 OR FINDNOCASE(CGI.SERVER_NAME,'cron.siremobile.com') GT 0 >
			  	<cfset apiUrl = "https://api.siremobile.com/ire/secure/triggerSMS" />
			<cfelse>
				<cfset apiUrl = "http://apiqa.siremobile.com/ire/secure/triggerSMS" />
			</cfif>
			<cfhttp url="#apiUrl#" method="POST" result="returnStruct" >

				<!--- Required components --->

				<!--- By default EBM API will return json or XML --->
				<cfhttpparam name="Accept" type="header" value="application/json" />     
				<cfhttpparam type="header" name="datetime" value="#datetime#" />
				<cfhttpparam type="header" name="authorization" value="#authorization#" /> 

				<!--- Batch Id controls which pre-defined campaign to run --->
				<cfhttpparam type="formfield" name="inpBatchId" value="#_TroubleTicketBatch#" />

				<!--- Contact string--->
				<cfhttpparam type="formfield" name="inpContactString" value="#arguments.inpPhone#" />

				<!--- 1=voice 2=email 3=SMS--->
				<cfhttpparam type="formfield" name="inpContactTypeId" value="3" />


				<!--- ***TODO: Remove this after adding parse for DNC STOP Requests --->
				<cfhttpparam type="formfield" name="inpSkipLocalUserDNCCheck" value="1" />

				<!--- Optional Components --->

				<!--- Custom data element for Batch --->
				<cfhttpparam type="formfield" name="inpMessage" value="#arguments.inpMessage#" />

			</cfhttp>

			<cfset dataout.RESULT = returnStruct />

			<cfset dataout.RXRESULTCODE = 1 />

			<cfquery name="updateTroubleTicketDetail" datasource="#Session.DBSourceEBM#" result="updateResult">
				UPDATE
					simplequeue.supportapp
				SET
					Notified_int = Notified_int + 1
				WHERE
					PKId_bi = <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#arguments.inpTicketId#'>
			</cfquery>

			<!--- Insert into Log --->   
			<cfquery name="InsertToAPILog" datasource="#Session.DBSourceEBM#">
				INSERT INTO simplexresults.apitracking
				(
					EventId_int,
					Created_dt,
					UserId_int,
					Subject_vch,
					Message_vch,
					TroubleShootingTips_vch,
					Output_vch,
					DebugStr_vch,
					DataSource_vch,
					Host_vch,
					Referer_vch,
					UserAgent_vch,
					Path_vch,
					QueryString_vch
				)
				VALUES
				(
					3000,
					NOW(),
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="SendWaitListSMS">, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="SendWaitListSMS Debugging Info">, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="See Query String and Result">,     
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="BatchId={#_TroubleTicketBatch#} #arguments.inpPhone#, 3, #arguments.inpMessage#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="BatchId={#_TroubleTicketBatch#}">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.DBSourceEBM#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_HOST),2024)#">, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.REMOTE_ADDR),2024)#">, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_USER_AGENT),2024)#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.PATH_TRANSLATED),2024)#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
				)
			</cfquery>
			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = 'Send Notify SMS successfully!' /> 
			<cfcatch type="any">
				<cfset dataout.TYPE = cfcatch.type />
				<cfset dataout.MESSAGE = cfcatch.message />
				<cfset dataout.ERRMESSAGE = cfcatch.detail />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="generateSignature" returnformat="json" access="private" output="false" hint="generate Signature by secret key">
        <cfargument name="method" required="true" type="string" default="GET" hint="Method" />
        <cfargument name="secretKey" required="true" type="string" default="gsdagadfgadsfgsdfgdsfgsdfg" hint="Secret Key" />
                 
        <cfset var retval = {} />                 
        <cfset var dateTimeString = GetHTTPTimeString(Now())>
        
        <!--- Create a canonical string to send --->
        <cfset var cs = "#arguments.method#\n\n\n#dateTimeString#\n\n\n">
        <cfset var signature = createSignature(cs,secretKey)>
        
        <cfset retval.SIGNATURE = signature>
        <cfset retval.DATE = dateTimeString>
        
        <cfreturn retval>
    </cffunction>

    <cffunction name="createSignature" returntype="string" access="private" output="false" hint="Create signature">
        <cfargument name="stringIn" type="string" required="true" />
        <cfargument name="scretKey" type="string" required="true" />
        <!--- Replace "\n" with "chr(10) to get a correct digest --->
        <cfset var fixedData = replace(arguments.stringIn,"\n","#chr(10)#","all")>
        <!--- Calculate the hash of the information --->
        <cfset var digest = HMAC_SHA1(scretKey,fixedData)>
        <!--- fix the returned data to be a proper signature --->
        <cfset var signature = ToBase64("#digest#")>
        
        <cfreturn signature>
    </cffunction>

    <cffunction name="HMAC_SHA1" returntype="binary" access="private" output="false" hint="NSA SHA-1 Algorithm">
       <cfargument name="signKey" type="string" required="true" />
       <cfargument name="signMessage" type="string" required="true" />
    
       <cfset var jMsg = JavaCast("string",arguments.signMessage).getBytes("iso-8859-1") />
       <cfset var jKey = JavaCast("string",arguments.signKey).getBytes("iso-8859-1") />
       <cfset var key = createObject("java","javax.crypto.spec.SecretKeySpec") />
       <cfset var mac = createObject("java","javax.crypto.Mac") />
    
       <cfset key = key.init(jKey,"HmacSHA1") />
       <cfset mac = mac.getInstance(key.getAlgorithm()) />
       <cfset mac.init(key) />
       <cfset mac.update(jMsg) />
    
       <cfreturn mac.doFinal() />
    </cffunction>

    <cffunction name="FindUser" access="remote" output="false" hint="Find user by keyword">
    	<cfargument name="inpKeyword" type="string" required="true">
    	<cfset var dataout = {} />	
    	<cfset dataout["userlist"] = [] />
    	<cfset var getUsers = '' />
    	<cftry>
    		<cfquery datasource="#Session.DBSourceREAD#" name="getUsers">
	    		SELECT 
	    			UserID_int,
	    			emailAddress_vch 
	    		FROM 
	    			simpleobjects.useraccount
	    		WHERE 
	    			UserID_int LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpKeyword#%">
	    			OR	emailAddress_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpKeyword#%">
	    		ORDER BY UserID_int, emailAddress_vch
	    		LIMIT 100
	    		
	    	</cfquery>

	    	<cfloop query="getUsers" >
	    		<cfset var userItem = {
	    			id = "#UserID_int#",
	    			name = "#emailAddress_vch#"
	    		} />
	    		<cfset arrayAppend(dataout["userlist"], userItem) />
	    	</cfloop>
	    	<cfset dataout.RXRESULTCODE = 1 />
	    	<cfset dataout.MESSAGE = 'Find user successfully' />
    		<cfcatch type="any">
				<cfset dataout.TYPE = cfcatch.type />
				<cfset dataout.MESSAGE = cfcatch.message />
				<cfset dataout.ERRMESSAGE = cfcatch.detail />
			</cfcatch>
    	</cftry>

    	<cfreturn dataout />
	</cffunction>

	<cffunction name="FindBatch" access="remote" output="false" hint="Find batch id by keyword">
		<cfargument name="inpKeyword" type="string" required="true">
    	<cfset var dataout = {} />	
    	<cfset dataout["batchlist"] = [] />
    	<cfset var getBatch = '' />
    	<cftry>
    		<cfquery datasource="#Session.DBSourceREAD#" name="getBatch">
	    		SELECT 
	    			BatchId_bi,
	    			Desc_vch
	    		FROM 
	    			simpleobjects.batch
	    		WHERE 
	    			BatchId_bi LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpKeyword#%">
	    			OR	Desc_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#arguments.inpKeyword#%">
	    		ORDER BY BatchId_bi, Desc_vch
	    		LIMIT 100
	    		
	    	</cfquery>

	    	<cfloop query="getBatch" >
	    		<cfset var Item = {
	    			id = "#BatchId_bi#",
	    			name = "#Desc_vch#"
	    		} />
	    		<cfset arrayAppend(dataout["batchlist"], Item) />
	    	</cfloop>
	    	<cfset dataout.RXRESULTCODE = 1 />
	    	<cfset dataout.MESSAGE = 'Find batch successfully' />
    		<cfcatch type="any">
				<cfset dataout.TYPE = cfcatch.type />
				<cfset dataout.MESSAGE = cfcatch.message />
				<cfset dataout.ERRMESSAGE = cfcatch.detail />
			</cfcatch>
    	</cftry>

    	<cfreturn dataout />
	</cffunction>
</cfcomponent>