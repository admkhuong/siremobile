<cfcomponent>
    <cfsetting showdebugoutput="true">
    <cfinclude template="/session/sire/configs/paths.cfm">
    <cfinclude template="/session/sire/configs/env_paths.cfm">
    <cfinclude template="/session/sire/configs/userConstants.cfm">

    <!--- Used IF locking down by session - User has to be logged in --->
    <cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.loggedIn" default="0"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
    
    <cfset LOCAL_LOCALE = "English (US)">    

    <cffunction name="uploadImg" access="remote" output="true" hint="upload Image" returnformat="plain">
        <cfset var dataout  = '' />
        <cfset var uploadPath   = '' />
        <cfset var fileInfo = '' />
        <cfset var info = '' />
        <cfset var logoImage    = '' />
        <cfset var resizeImage    = '' />
        <cfset dataout = structNew()>
        <cfset dataout.RESULT = 'FAIL'>
        <cfset dataout.IMAGE = ''>
        <cfset dataout.IMAGE_URL = ''>
        <cfset dataout.MESSAGE = ''>
        <cfset uploadPath = ExpandPath(UPLOAD_ORG_LOGO_PATH)/>

        <cfset var activation_random_code = '' />
        <cfset var activation_trim_space = '' />

        <cfset fileInfo = GetFileInfo(image)>

        <cfif(fileInfo.size GT MAXUPLOADIMAGE)>
            <cfset dataout.RESULT = 'FAIL'>
            <cfset dataout.MESSAGE = 'The maximum size of avarta image is 4MB'>
            <cfreturn serializeJSON(dataout)>
        </cfif>

        <cftry>

            <cfif NOT DirectoryExists(uploadPath)>
                <cfdirectory action = "create" directory="#uploadPath#" />
            </cfif>

            <cffile action="upload" filefield="image" destination="#uploadPath#" nameconflict="makeunique" result="uploadResult" accept="image/jpg,image/jpeg,image/gif,image/png,image/pjpeg">
            <!--- Quick and dirty response dump for DEV/Debugging only --->

            <!--- RENAME UPLOAD FILE --->
            <!--- <cfset var newImageName = GetTickCount()&'_'&uploadResult.serverfilename&"."&uploadResult.serverFileExt> --->

            <cfset activation_random_code="#RandRange(10000,99999)#">
            <!--- <cfset activation_trim_space = reReplace(uploadResult.serverfilename, "[[:space:]]", "", "ALL") /> --->
            <cfset var newImageName = GetTickCount()&'_'&activation_random_code&"."&uploadResult.serverFileExt>

            <cffile action="rename" source="#uploadResult.serverdirectory#/#uploadResult.serverfile#" destination="#uploadResult.serverdirectory#/#newImageName#"/>

            <cfimage source="#uploadResult.serverdirectory#/#newImageName#" name="logoImage">

            <!--- Turn on antialiasing to improve image quality. ---> 
            <cfset ImageSetAntialiasing(logoImage,"on")> 
            <cfset info =ImageInfo(logoImage)>

            <cfif info.width GT info.height>
                <cfset ImageScaleToFit(logoImage,150,"","highestQuality")> 
            <cfelse>
                <cfset ImageScaleToFit(logoImage,"",150,"highestQuality")>                 
            </cfif> 
            
            <cfimage source="#logoImage#" action="write" destination="#uploadResult.serverdirectory#/#newImageName#" overwrite="yes" name="resizeImage"> 
           
            <cfset dataout.RESULT = 'SUCCESS'>
            <cfset dataout.IMAGE = newImageName>
            <cfset dataout.IMAGE_URL = UPLOAD_ORG_LOGO_PATH&dataout.IMAGE>
            <cfreturn serializeJSON(dataout)>   
            <cfcatch>
                <cfset dataout.RESULT = 'FAIL'>
                <cfset dataout.MESSAGE = 'Unable to upload image at this time. An error occurred.'>                    
                <cfreturn serializeJSON(dataout)>                           
            </cfcatch>  
        </cftry>

        <cfreturn serializeJSON(dataout) >   
    </cffunction>

    <cffunction name="uploadS3" returntype="any" output="true" hint="upload file to S3">

        <cfargument name="name" type="string" required="true" />
        <cfargument name="local_path" type="string" required="true" />

        <cfset var result = {}>
        <cfset var result.STATUS = 1 >
        <cfset var result.AMAZONEID = 0 >
        
        <cfif FileExists(local_path&name)>

            <cfset var accessKeyId = "#_S3ACCESSKEYID#"> 
            <cfset var secretAccessKey = "#_S3SECRETACCESSKEY#">
            <cfset var bucketName = '#_S3IMAGEFOLDERPATCH#'>

            <cfset var amazoneS3 = CreateObject("component","session.sire.models.cfc.s3_v1_9").init(accessKeyId,secretAccessKey)>

            <!--- <cfset var fullpath = getDirectoryFromPath(getCurrentTemplatePath())> --->
            
            <cfset var serverFile = '#arguments.name#'>
            <cfset var contentType = 'image'>
            <cfset var cacheControl = true>
            <cfset var acl = 'public-read'>
            <cfset var storageClass = 'STANDARD'>
            <cfset var timeout= 300 >
            <cfset var keyname = ''>
            <cfset var cacheDays = 30>

			<!--- https://stackoverflow.com/questions/18009493/content-type-to-be-used-for-uploading-svg-images-to-aws-s3 --->
			<!--- https://github.com/aws/aws-sdk-js/issues/1134 --->
			<cfif FindNoCase('.svg', arguments.name )	GT 0 OR FindNoCase('.svg', arguments.local_path )	GT 0>
				<cfset contentType = 'image/svg+xml'>
			</cfif>


            <cfset result = amazoneS3.putObject(_S3IMAGEROOTURL,bucketName,serverFile,contentType,timeout,cacheControl,cacheDays,acl,storageClass,keyname,arguments.local_path)>

            <!--- IF PUT SUCCESS,DELETE LOCAL FILE
            <cfif result NEQ ''>
                <cfif FileExists(local_path&name)>
                    <cfscript>
                        FileDelete(local_path&name) 
                    </cfscript>
                </cfif>
            </cfif>
            --->
        <cfelse>
            <cfset var result.STATUS = -1 >
        </cfif>
        
        <cfreturn result />
    </cffunction>

    <cffunction name="DeleteS3" returntype="any" output="false" hint="Delete s3 object">

        <cfargument name="inpBucketName" type="string" required="true" />
        <cfargument name="inpFileName" type="string" required="true" />

        <cfset var result = {}>
        <cfset var result.STATUS = 1 >
        <cfset var result.AMAZONEID = 0 >

        <cfset var accessKeyId = "#_S3ACCESSKEYID#">
        <cfset var secretAccessKey = "#_S3SECRETACCESSKEY#">
        <cfset var bucketName = '#arguments.inpBucketName#'>
        <cfset var serverFile = '#arguments.inpFileName#'>

        <cftry>
            <cfset var amazoneS3 = CreateObject("component","session.sire.models.cfc.s3_v1_9").init(accessKeyId,secretAccessKey)>

            <cfset result = amazoneS3.deleteObject(_S3IMAGEROOTURL,bucketName,serverFile)>

            <cfcatch type="any">
                <cfset result.STATUS = -1>
            </cfcatch>
        </cftry>

        <cfreturn result />
    </cffunction>

    <cffunction name="fileBrowser" access= "public" returntype="any" output="true" hint="file Browser">
        <cfset var result = {} >
        <cfset result[1] = 'https://s3-us-west-1.amazonaws.com/siremobile/org_logo/1454487796782_A_Size_Too_Small_Cute_Dog.jpg'>
        <cfreturn result>
    </cffunction>

    
    <cffunction name="fileUpload" access= "public" returntype="any" output="true" hint="file Upload">
        <cfset var result = {}>
        <cfreturn result>
    </cffunction>
    
    <cffunction name="uploadFileS3" returntype="any" output="true" hint="upload file to S3">

        <cfargument name="name" type="string" required="true" />
        <cfargument name="local_path" type="string" required="true" />
        <cfargument name="bucketName" type="string" required="true" />
        <cfargument name="contentType" type="string" required="true" />

        <cfset var result = {}>
        <cfset var result.STATUS = 1 >
        <cfset var result.AMAZONEID = 0 >
  
        <cfif FileExists(local_path&name)>


            <cfset var accessKeyId = "#_S3ACCESSKEYID#"> 
            <cfset var secretAccessKey = "#_S3SECRETACCESSKEY#">
            <cfset var bucketName = "#bucketName#">

            <cfset var amazoneS3 = CreateObject("component","session.sire.models.cfc.s3_v1_9").init(accessKeyId,secretAccessKey)>

            <!--- <cfset var fullpath = getDirectoryFromPath(getCurrentTemplatePath())> --->
            
            <cfset var serverFile = '#arguments.name#'>
            <cfset var contentType = '#arguments.contentType#'>
            <cfset var cacheControl = true>
            <cfset var acl = 'public-read'>
            <cfset var storageClass = 'STANDARD'>
            <cfset var timeout= 300 >
            <cfset var keyname = ''>
            <cfset var cacheDays = 30>

            <cfset result = amazoneS3.putObject(_S3IMAGEROOTURL,bucketName,serverFile,contentType,timeout,cacheControl,cacheDays,acl,storageClass,keyname,arguments.local_path)>

            <!--- IF PUT SUCCESS,DELETE LOCAL FILE
            <cfif result NEQ ''>
                <cfif FileExists(local_path&name)>
                    <cfscript>
                        FileDelete(local_path&name) 
                    </cfscript>
                </cfif>
            </cfif>
            --->
        <cfelse>
            <cfset var result.STATUS = -1 >
        </cfif>
        
        <cfreturn result />
    </cffunction>


<cffunction name="uploadFile2S3" returntype="any" output="true" hint="upload file to S3">

        <cfargument name="name" type="string" required="true" />
        <cfargument name="local_path" type="string" required="true" />
        <cfargument name="bucketName" type="string" required="true" />
        <cfargument name="contentType" type="string" required="true" />

        <cfset var result = {}>
        <cfset var result.STATUS = 1 >
        <cfset var result.AMAZONEID = 0 >
  
        <cfif FileExists(local_path&name)>


            <cfset var accessKeyId = "#_S3ACCESSKEYID#"> 
            <cfset var secretAccessKey = "#_S3SECRETACCESSKEY#">
            <cfset var bucketName = "#bucketName#">

            <cfset var amazoneS3 = CreateObject("component","session.sire.models.cfc.s3_v1_9").init(accessKeyId,secretAccessKey)>

            <!--- <cfset var fullpath = getDirectoryFromPath(getCurrentTemplatePath())> --->
            
            <cfset var serverFile = '#arguments.name#'>
            <cfset var contentType = arguments.contentType>
            <cfset var cacheControl = true>
            <cfset var acl = 'public-read'>
            <cfset var storageClass = 'STANDARD'>
            <cfset var timeout= 300 >
            <cfset var keyname = ''>
            <cfset var cacheDays = 30>

            <cfset result = amazoneS3.putObject(_S3IMAGEROOTURL,bucketName,serverFile,contentType,timeout,cacheControl,cacheDays,acl,storageClass,keyname,arguments.local_path)>

            <!--- IF PUT SUCCESS,DELETE LOCAL FILE
            <cfif result NEQ ''>
                <cfif FileExists(local_path&name)>
                    <cfscript>
                        FileDelete(local_path&name) 
                    </cfscript>
                </cfif>
            </cfif>
            --->
        <cfelse>
            <cfset var result.STATUS = -1 >
        </cfif>
        
        <cfreturn result />
    </cffunction>
	
	 <cffunction name="uploadCppCSS" access="remote" output="true" hint="upload css cpp" returnformat="plain">
	 	<cfset var dataout  = '' />
        <cfset var uploadPath   = '' />
        <cfset var fileInfo = '' />
        <cfset var info = '' />
        <cfset var logoImage    = '' />
        <cfset var resizeImage    = '' />
        <cfset dataout = structNew()>
        <cfset dataout.RESULT = 'FAIL'>
        <cfset dataout.FILE = ''>
        <cfset dataout.URL = ''>
        <cfset dataout.MESSAGE = ''>
        <cfset uploadPath = ExpandPath(CSS_UPLOAD_PATH)/>

        <cfset fileInfo = GetFileInfo(image)>

        <cfset var ResUploadS3 = '' />

        <cfif(fileInfo.size GT MAXUPLOADIMAGE)>
            <cfset dataout.RESULT = 'FAIL'>
            <cfset dataout.MESSAGE = 'The maximum size of avarta image is 4MB'>
            <cfreturn serializeJSON(dataout)>
        </cfif>

        <cftry>

            <cfif NOT DirectoryExists(uploadPath)>
                <cfdirectory action = "create" directory="#uploadPath#" />
            </cfif>

            <cffile action="upload" filefield="image" destination="#uploadPath#" nameconflict="makeunique" result="uploadResult" accept="text/css">
            <!--- Quick and dirty response dump for DEV/Debugging only --->

            <!--- RENAME UPLOAD FILE --->
            <cfset var newFileName = "css_custom_"&GetTickCount()&"."&uploadResult.serverFileExt>

            <cffile action="rename" source="#uploadResult.serverdirectory#/#uploadResult.serverfile#" destination="#uploadResult.serverdirectory#/#newFileName#"/>

			<cfinvoke method="uploadFile2S3" returnvariable="ResUploadS3">
			    <cfinvokeargument name="name" value="#newFileName#"/>
			    <cfinvokeargument name="local_path" value="#uploadResult.serverdirectory#/"/>
			    <cfinvokeargument name="bucketName" value="siremobile/cpp_css"/>
			    <cfinvokeargument name="contentType" value="text/css"/>
		    </cfinvoke>
		    
		    <cfif ResUploadS3.STATUS EQ 1>
		    	<cfset dataout.RESULT = 'SUCCESS'>
            	<cfset dataout.FILE = newFileName>
            	<cfset dataout.URL = _S3URL & "/siremobile/cpp_css/" & newFileName />
		    </cfif>
            
            <cfreturn serializeJSON(dataout)>   
            <cfcatch>
                <cfset dataout.RESULT = 'FAIL'>
                <cfset dataout.MESSAGE = 'Unable to upload image at this time. An error occurred.'>                    
                <cfreturn serializeJSON(dataout)>                           
            </cfcatch>  
        	</cftry>

        <cfreturn serializeJSON(dataout) >  
	 	
	 </cffunction>

</cfcomponent>    
