<cfcomponent>
    <cfinclude template="../../../../public/paths.cfm" >
    
    <!--- 
        CREATE TABLE `url_shortner` (
  `PKId_bi` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserId_int` int(11) DEFAULT NULL,
  `UniqueTargetId_int` int(11) DEFAULT '0',
  `RedirectCode_int` int(11) DEFAULT '301',
  `ClickCount_int` int(11) DEFAULT '0',
  `Active_int` int(11) DEFAULT '1',
  `Created_dt` datetime DEFAULT NULL,
  `LastClicked_dt` datetime DEFAULT NULL,
  `ShortURL_vch` varchar(45) DEFAULT NULL,
  `Desc_vch` varchar(255) DEFAULT NULL,
  `TargetURL_vch` varchar(2048) DEFAULT NULL,
  `DynamicData_vch` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`PKId_bi`),
  KEY `IDX_Combo_ShortURL_User` (`ShortURL_vch`,`UserId_int`,`Active_int`),
  KEY `IDX_COMOB_Target_User` (`TargetURL_vch`(767),`UserId_int`,`Active_int`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
--->

<cffunction name="AddShortURL" access="remote" output="false" hint="Generate a short URL - if one for the currently logged in user account">
    <cfargument name="inpDesc" TYPE="string" required="no" default="" hint="Freindly name for organizing list of shortened URLs"/>
    <cfargument name="inpTargetURL" TYPE="string" required="yes" hint="Where is this short URL going to re-driect to?"/>
    <cfargument name="inpShortURLKey" TYPE="string" required="no" default="" hint="Allow optional user specified short URL key?"/>
    <cfargument name="inpDynamicData" TYPE="string" required="no" default="" hint="Optional Query string added to target URL"/>
    <cfargument name="inpLength" type="Numeric" required="no" default="6" hint="The length of the short URL key - 6 by default" />
    <cfargument name="inpUniqueTarget" type="Numeric" required="no" default="0" hint="Allow duplicate targets on request. inpUniqueTarget=1 to create duplicate shortened URL for target. inpUniqueTarget=0 will just return existing short key if found otherwise will create a new key" /> 
    <cfargument name="inpRedirectCode" TYPE="Numeric" required="no" default="301" hint="Allow user to specify alternate types of Re-Directs - Default is HTTP status 301 (permanent redirect), 302, or 307 (temporary redirect)."/>
    
    <cfset var dataout = {} />    
    <cfset var AddShortURLToDB = '' />
    <cfset var RetVarGenerateLinkCode = '' />
    <cfset var currentIndex = '' />
    <cfset var CheckForCollisions = '' />
    <cfset var GetUniqueTargetId = '' />
    <cfset var UniqueTargetId = '0' />
    <cfset var allowedAddUrl = false />
    <cfset var CheckDuplicatedShortKey = ''/>
    <cfset var RetValInsertShortUrl = '' />
    
    <cftry>      
        
        <!--- Set default return values --->       
        <cfset dataout.RXRESULTCODE = "-1" />
        <cfset dataout.SHORTURLKEY = ""> 
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        
        <!--- Check user add  new short url permission --->
        <cfinvoke method="AllowAddUrl" returnvariable="allowedAddUrl"></cfinvoke>
        <cfif !allowedAddUrl >
            <cfset dataout.RXRESULTCODE = "0" />
            <cfset dataout.MESSAGE = "Can't add a new short url" />
            <cfset dataout.ERRMESSAGE = "Can't add a new short url" />
            <cfreturn dataout />
        </cfif>            
        
        <!--- Verify this target Id is unique --->
        <cfquery name="CheckForCollisions" datasource="#Session.DBSourceEBM#">
            SELECT 
            ShortURL_vch
            FROM
            simplelists.url_shortner
            WHERE
            TargetURL_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpTargetURL#"> 
            AND
            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#"> 
            AND
            Active_int = 1  
                -- ORDER BY
                --  UniqueTargetId_int DESC 
            </cfquery>

            <!--- Verify this short key is unique --->
            <!--- <cfquery name="CheckDuplicatedShortKey" datasource="#Session.DBSourceEBM#">
                SELECT
                    ShortURL_vch
                FROM 
                    simplelists.url_shortner
                WHERE
                    ShortURL_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpShortURLKey#">
                AND 
                    Active_int = 1 
            </cfquery>

            <cfif CheckDuplicatedShortKey.RECORDCOUNT GT 0>
                <cfset dataout.RXRESULTCODE = "-1" />
                <cfset dataout.MESSAGE = "Short URL key already exists!" />
                <cfset dataout.ERRMESSAGE = "Short URL key already exists!" />
                <cfreturn dataout />
            </cfif> --->
            
            <!--- Create a duplicate key --->
            <!--- <cfif inpUniqueTarget EQ 1 AND CheckForCollisions.RECORDCOUNT GT 0> 
            
                <!--- Get max key plus 1 --->
                <cfquery name="GetUniqueTargetId" datasource="#Session.DBSourceEBM#">
                    SELECT 
                        MAX(UniqueTargetId_int) + 1  AS UNIQUETARGETID
                    FROM
                        simplelists.url_shortner
                    WHERE
                        TargetURL_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpTargetURL#"> 
                    AND
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#"> 
                    AND
                        Active_int = 1                     
                </cfquery>  
            
                <cfif GetUniqueTargetId.RECORDCOUNT GT 0 >
                    
                    <cfset UniqueTargetId = '#GetUniqueTargetId.UNIQUETARGETID#' />
                    
                </cfif>
            
            </cfif> --->
            <!--- Use existing key --->
           <!---  <cfif inpUniqueTarget EQ 0 AND CheckForCollisions.RECORDCOUNT GT 0>
                <cfset dataout.RXRESULTCODE = "-3" />            
                <cfset dataout.SHORTURLKEY = "#CheckForCollisions.ShortURL_vch#">
                <cfset dataout.MESSAGE = "Target URL already exists!" />
                <cfset dataout.ERRMESSAGE = "Target URL key already exists!" />
                <cfreturn dataout>
            <cfelse>  --->
                <!--- Create a new key --->
                <cfif LEN(TRIM(inpShortURLKey)) EQ 0> 
                    <cfloop index = "currentIndex" from = "1" to = "100" step = "1">
                        
                        <!--- Generate a randon key --->
                        <cfinvoke method="GenerateLinkCode" returnvariable="RetVarGenerateLinkCode">
                            <cfinvokeargument name="inpLength" value="#inpLength#">                                   
                            </cfinvoke>     
                            
                            <!--- Lookup in DB for Collisions - key must be unique - dont rely on error handling do an actual lookup --->
                            <!--- always get latest entry for operator Id regardless of whose list it is on --->
                            <cfquery name="CheckForCollisions" datasource="#Session.DBSourceEBM#">
                                SELECT 
                                COUNT(*) AS TOTALCOUNT
                                FROM
                                simplelists.url_shortner
                                WHERE
                                ShortURL_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#RetVarGenerateLinkCode.SHORTURLKEY#"> 
                                AND
                                Active_int = 1  
                            </cfquery>  
                            
                            <cfif CheckForCollisions.TOTALCOUNT EQ 0> 
                                <!--- Exit loop if no collision found--->                   
                                <cfbreak />
                            </cfif>             
                            
                        </cfloop>   
                        
                    <cfelse>
                        
                        <cfset RetVarGenerateLinkCode = structNew() />
                        
                        <cfset RetVarGenerateLinkCode.SHORTURLKEY = TRIM(inpShortURLKey)/>
                        
                        
                    </cfif>        
                    
                    <!--- Insert entry into DB --->
                    <cfquery name="AddShortURLToDB" datasource="#Session.DBSourceEBM#" result="RetValInsertShortUrl">
                     INSERT INTO 
                     simplelists.url_shortner
                     (
                        UserId_int, 
                        UniqueTargetId_int,
                        RedirectCode_int,
                        Created_dt, 
                        LastClicked_dt,
                        Desc_vch,
                        ShortURL_vch,
                        TargetURL_vch,
                        DynamicData_vch,
                        ClickCount_int                                                                          
                        )
                     VALUES
                     (
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UniqueTargetId#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpRedirectCode#">,
                        NOW(), 
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpDesc,255)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#RetVarGenerateLinkCode.SHORTURLKEY#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpTargetURL,2048)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpDynamicData,2048)#">,
                        0                           
                        )                                        
                 </cfquery>         
                 
                 <!--- </cfif>     --->
                 
                 <cfset dataout.RXRESULTCODE = "1" />
                 <cfset dataout.TYPE = "" />
                 <cfset dataout.MESSAGE = "" />
                 <cfset dataout.ERRMESSAGE = "" />

                 <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="ShortURLId = #RetValInsertShortUrl.GENERATED_KEY#">
                        <cfinvokeargument name="operator" value="ShortURL created">
                        </cfinvoke>
                        
                        <cfcatch TYPE="any">
                            <cfset dataout.RXRESULTCODE = "-2" />
                            <cfset dataout.SHORTURLKEY = ""> 
                            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
                        </cfcatch>
                        
                    </cftry> 

                    <cfreturn dataout />
                </cffunction>
                
                <cffunction name="GetShortURLData" access="public" output="false" hint="Get full short URL data - Returns object of data  ">
                    <cfargument name="inpPK" type="string" required="true" hint="This is the Primary Key for the Short URL"> 
                    
                    <cfset var dataout = {} />    
                    <cfset var LookupFullURL = '' />
                    
                    <cftry>      
                        
                        <!--- Set default return values --->       
                        <cfset dataout.RXRESULTCODE = "-1" />
                        <cfset dataout.TARGETURL = "" />                
                        <cfset dataout.TYPE = "" />
                        <cfset dataout.MESSAGE = "" />
                        <cfset dataout.ERRMESSAGE = "" />
                        
                        <cfset dataout.USERID = "" />
                        <cfset dataout.UNIQUETARGETID = "" />
                        <cfset dataout.REDIRECTCODE = "" />
                        <cfset dataout.CREATED = "" />
                        <cfset dataout.LASTCLICKED = "" />
                        <cfset dataout.DESC = "" />
                        <cfset dataout.SHORTURL = "" />
                        <cfset dataout.TARGETURL = "" />
                        <cfset dataout.DYNAMICDATA = "" />
                        <cfset dataout.CLICKCOUNT = "" /> 
                        
                        <!--- always get latest entry for operator Id regardless of whose list it is on --->
                        <cfquery name="LookupFullURL" datasource="#Session.DBSourceEBM#">
                            SELECT 
                            UserId_int, 
                            UniqueTargetId_int,
                            RedirectCode_int,
                            Created_dt, 
                            LastClicked_dt,
                            Desc_vch,
                            ShortURL_vch,
                            TargetURL_vch,
                            DynamicData_vch,
                            ClickCount_int     
                            FROM
                            simplelists.url_shortner
                            WHERE
                            PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpPK#">  
                            AND
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#"> 
                            AND
                            Active_int = 1                
                        </cfquery>         
                        
                        <cfif LookupFullURL.RecordCount GT 0>       
                            <cfset dataout.USERID = LookupFullURL.UserId_int />
                            <cfset dataout.UNIQUETARGETID = LookupFullURL.UniqueTargetId_int />
                            <cfset dataout.REDIRECTCODE = LookupFullURL.RedirectCode_int />
                            <cfset dataout.CREATED = LookupFullURL.Created_dt />
                            <cfset dataout.LASTCLICKED = LookupFullURL.LastClicked_dt />
                            <cfset dataout.DESC = LookupFullURL.Desc_vch />
                            <cfset dataout.SHORTURL = LookupFullURL.ShortURL_vch />
                            <cfset dataout.TARGETURL = LookupFullURL.TargetURL_vch />
                            <cfset dataout.DYNAMICDATA = LookupFullURL.DynamicData_vch />
                            <cfset dataout.CLICKCOUNT = LookupFullURL.ClickCount_int />                 
                        </cfif>      
                        
                        <cfset dataout.RXRESULTCODE = "1" />
                        <cfset dataout.TYPE = "" />
                        <cfset dataout.MESSAGE = "" />
                        <cfset dataout.ERRMESSAGE = "" />
                        
                        <cfcatch TYPE="any">
                            <cfset dataout.RXRESULTCODE = "-2" />
                            <cfset dataout.TARGETURL = "" />       
                            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />  
                            
                            <cfset dataout.USERID = "" />
                            <cfset dataout.UNIQUETARGETID = "" />
                            <cfset dataout.REDIRECTCODE = "" />
                            <cfset dataout.CREATED = "" />
                            <cfset dataout.LASTCLICKED = "" />
                            <cfset dataout.DESC = "" />
                            <cfset dataout.SHORTURL = "" />
                            <cfset dataout.TARGETURL = "" />
                            <cfset dataout.DYNAMICDATA = "" />
                            <cfset dataout.CLICKCOUNT = "" /> 
                            
                        </cfcatch>
                    </cftry> 

                    <cfreturn dataout />
                </cffunction>

                <cffunction name="UpdateShortURLData" access="remote" output="false" hint="Update existing Short URL Data">
                    <cfargument name="inpPK" type="string" required="true" hint="This is the key for the Short URL to update"> 
                    <cfargument name="inpDesc" TYPE="string" required="no" default="" hint="Freindly name for organizing list of shortened URLs"/>
                    <cfargument name="inpTargetURL" TYPE="string" required="yes" hint="Where is this short URL going to re-driect to?"/>
                    <cfargument name="inpDynamicData" TYPE="string" required="no" default="" hint="Optional Query string added to target URL"/>
                    
                    <cfset var dataout = {} />    
                    <cfset var UpdateShortURLData = '' />
                    
                    <cftry>      
                        
                        <!--- Set default return values --->       
                        <cfset dataout.RXRESULTCODE = "-1" />
                        <cfset dataout.SHORTURLKEY = ""> 
                        <cfset dataout.TYPE = "" />
                        <cfset dataout.MESSAGE = "" />
                        <cfset dataout.ERRMESSAGE = "" />
                        
                        <!--- Update entry into DB --->
                        <cfquery name="UpdateShortURLData" datasource="#Session.DBSourceEBM#">
                            UPDATE  
                            simplelists.url_shortner
                            SET     
                            Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpDesc,255)#">,
                            TargetURL_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpTargetURL,2048)#">,
                            DynamicData_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpDynamicData,2048)#">
                            WHERE
                            PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpPK#">  
                            AND
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#"> 
                            AND
                            Active_int = 1                
                        </cfquery>         
                        
                        <cfset dataout.RXRESULTCODE = "1" />
                        <cfset dataout.TYPE = "" />
                        <cfset dataout.MESSAGE = "" />
                        <cfset dataout.ERRMESSAGE = "" />

                        <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                            <cfinvokeargument name="moduleName" value="ShortURLId = #arguments.inpPK#">
                                <cfinvokeargument name="operator" value="ShortURL updated">
                                </cfinvoke>
                                
                                <cfcatch TYPE="any">
                                    <cfset dataout.RXRESULTCODE = "-2" />
                                    <cfset dataout.SHORTURLKEY = ""> 
                                    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                                    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                                    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
                                </cfcatch>
                                
                            </cftry> 

                            <cfreturn dataout />
                        </cffunction>
                        
                        <cffunction name="GetFullURL" access="public" output="false" hint="Get full link from shortened URL - Returns obj.TARGETURL ">
                            <cfargument name="ShortURL" type="string" required="true" hint="This is the Short version of the URL"> 
                            
                            <cfset var dataout = {} />    
                            <cfset var LookupFullURL = '' />
                            
                            <cftry>      
                                
                                <!--- Set default return values --->       
                                <cfset dataout.RXRESULTCODE = "-1" />
                                <cfset dataout.TARGETURL = "" />                
                                <cfset dataout.TYPE = "" />
                                <cfset dataout.MESSAGE = "" />
                                <cfset dataout.ERRMESSAGE = "" />
                                
                                <!---  --->
                                <cfquery name="LookupFullURL" datasource="#Session.DBSourceEBM#">
                                    SELECT 
                                    TargetURL_vch
                                    FROM
                                    simplelists.url_shortner
                                    WHERE
                                    ShortURL_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ShortURL#">  
                                    AND
                                    Active_int = 1                
                                </cfquery>         
                                
                                <cfif LookupFullURL.RecordCount GT 0>       
                                    <cfset dataout.TARGETURL = LookupFullURL.Redirect_vch />                 
                                <cfelse>
                                    <cfset dataout.TARGETURL = "" />                     
                                </cfif>      
                                
                                <cfset dataout.RXRESULTCODE = "1" />
                                <cfset dataout.TYPE = "" />
                                <cfset dataout.MESSAGE = "" />
                                <cfset dataout.ERRMESSAGE = "" />
                                
                                <cfcatch TYPE="any">
                                    <cfset dataout.RXRESULTCODE = "-2" />
                                    <cfset dataout.TARGETURL = "" />       
                                    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                                    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                                    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
                                </cfcatch>
                            </cftry> 

                            <cfreturn dataout />
                        </cffunction>


                        <cffunction name="DeleteShortURL" access="remote" output="false" hint="Logically delete a short URL">
                            <cfargument name="inpPKId" type="string" required="true" hint="This is the key for the Short URL to delete"> 
                            
                            <cfset var dataout = {} />    
                            <cfset var SystemUpdate = '' />
                            
                            <cftry>      
                                
                                <!--- Set default return values --->       
                                <cfset dataout.RXRESULTCODE = "-1" />
                                <cfset dataout.TYPE = "" />
                                <cfset dataout.MESSAGE = "" />
                                <cfset dataout.ERRMESSAGE = "" />

                                <!--- Only allow logged in user to mnake changes to thier own data --->            
                                <cfquery name="SystemUpdate" datasource="#Session.DBSourceEBM#" >
                                    UPDATE
                                    simplelists.url_shortner
                                    SET
                                    Active_int = 0       
                                    WHERE
                                    PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpPKId#">
                                    AND
                                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#"> 
                                </cfquery>    
                                
                                <cfset dataout.RXRESULTCODE = "1" />
                                <cfset dataout.TYPE = "" />
                                <cfset dataout.MESSAGE = "Delete Short URL successfully." />
                                <cfset dataout.ERRMESSAGE = "" />

                                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                                    <cfinvokeargument name="moduleName" value="ShortURLId = #arguments.inpPKId#">
                                        <cfinvokeargument name="operator" value="ShortURL deleted">
                                        </cfinvoke>
                                        
                                        <cfcatch TYPE="any">
                                            <cfset dataout.RXRESULTCODE = "-2" />
                                            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                                            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                                            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
                                        </cfcatch>
                                    </cftry> 

                                    <cfreturn dataout />
                                </cffunction>


                                <cffunction name="GenerateLinkCode" access="public" output="false" hint="Generate a alphanumberic key - non-case sensitive">   
                                    <cfargument name="inpLength" type="Numeric" required="true" default="6">   

                                    <cfset var i = '' />

                                    <!--- create a list of all allowable characters for our short URL link - not case sensitive --->   
                                    <cfset var chars="a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9,-">   
                                    
                                    <!--- our radix is the total number of possible characters --->   
                                    <cfset var radix=listlen(local.chars)>
                                    
                                    <cfset var randnum = 1>  
                                    
                                    <!---   Then, after setting our return variable to an empty string, we're going to add random characters from that list in a loop: --->
                                    <cfset var dataout=StructNew()>
                                    
                                    <!--- initialise our return variable --->   
                                    <cfset dataout.SHORTURLKEY="">
                                    <cfset var i = '' /> 
                                    
        <!--- loop from 1 until the number of characters our URL should be,   
          adding a random character from our master list on each iteration  --->   
          <cfloop from="1" to="#arguments.inpLength#" index="i">   
             
              <!--- generate a random number in the range of 1 to the total number of possible characters we have defined --->   
              <cfset randnum = RandRange(1, radix)>   
              
              <!--- add the character from a random position in our list to our short link --->   
              <cfset dataout.SHORTURLKEY=dataout.SHORTURLKEY & listGetAt(chars, randnum)>   
              
          </cfloop>
          
          <!--- Finally, return the newly generated short link to the calling code: --->  
          <!--- return the generated random short link --->   
          <cfreturn dataout>   
          
      </cffunction> 
      
      
      <cffunction name="GetShortURLList" access="remote" output="false" hint="get list cpp">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="iSortCol_1" default="-1">
        <cfargument name="sSortDir_1" default="">
        <cfargument name="iSortCol_2" default="-1">
        <cfargument name="sSortDir_2" default="">
        <cfargument name="iSortCol_0" default="-1">
        <cfargument name="sSortDir_0" default="">
        <cfargument name="customFilter" default="">

        <cfset var dataOut = {} />
        <cfset dataOut.DATA = ArrayNew(1) />
        <cfset var GetNumbersCount = '' />
        <cfset var order = '' />
        <cfset var GetShortURLList = '' />
        <cfset var actionHtml = '' />
        <cfset var DisplaySection = '' />
        <cfset var cppStatusTxt = '' />
        <cfset var allowedAddUrl = false />
        <cfset var tempItem = '' />
        <cfset var filterItem   = '' />
        
        <cfset var ShortURLSite = "http://mlp-x.com" />

        <cfset var cppStatusTxt = '' />
        <cfset var tempItem = '' />
        <cfset var filterItem = '' />
        
        <cftry>
            <cfinvoke method="BuildSortingParamsForDatatable" returnvariable="order">
                <cfinvokeargument name="iSortCol_0" value="#iSortCol_0#"/>
                <cfinvokeargument name="iSortCol_1" value="#iSortCol_1#"/>
                <cfinvokeargument name="sSortDir_0" value="#sSortDir_0#"/>
                <cfinvokeargument name="sSortDir_1" value="#sSortDir_1#"/>
            </cfinvoke>
            
            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            <cfset dataout.LOCKADDBUTTON = false />
            
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            
            <!---ListShortURLData is key of DataTable control--->
            <cfset dataout["ListShortURLData"] = []>
            
            <!---Get total EMS for paginate --->
            <cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
                SELECT
                COUNT(*) AS TOTALCOUNT
                FROM
                simplelists.url_shortner AS c
                WHERE        
                c.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                AND 
                c.Active_int = 1    
                AND
                    c.Type_ti = 1 -- 1 is User Created type
                    <cfif customFilter NEQ "">
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                                AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                            <cfelse>
                                AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfif>
                </cfquery>
                
                <cfif GetNumbersCount.TOTALCOUNT GT 0>
                    <cfset dataout["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
                    <cfset dataout["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>
                    
                    <!--- Get ems data --->     
                    <cfquery name="GetShortURLList" datasource="#Session.DBSourceREAD#">
                        SELECT 
                        c.PKId_bi,
                        c.UniqueTargetId_int,
                        c.RedirectCode_int,
                        c.Created_dt,
                        c.LastClicked_dt,
                        c.Desc_vch,
                        c.ShortURL_vch,
                        c.TargetURL_vch,
                        c.DynamicData_vch,
                        c.ClickCount_int,
                        c.Active_int
                        FROM
                        simplelists.url_shortner c
                        WHERE 
                        c.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
                        AND 
                        c.Active_int = 1
                        AND
                        c.Type_ti = 1 -- 1 is User Created type
                        <cfif customFilter NEQ "">
                            <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                                <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                                    AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                                <cfelse>
                                    AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                                </cfif>
                            </cfloop>
                        </cfif>
                        #order#
                        LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#">
                    </cfquery>
                    
                    
                    <cfloop query="GetShortURLList">
                        
                        <cfset tempItem = {
                        Id = "#PKId_bi#",
                        UniqueTargetId = "#UniqueTargetId_int#",
                        RedirectCode = "#RedirectCode_int#",
                        Created = "#Created_dt#",
                        LastClicked = "#LastClicked_dt#",
                        Desc = "#Desc_vch#",
                        ShortURL = ShortURLSite&"/#ShortURL_vch#",
                        TargetURL = "#TargetURL_vch#",
                        DynamicData = "#DynamicData_vch#",
                        ClickCount = "#ClickCount_int#",
                        Status = "#Active_int#"
                    }/>
                    <cfset dataout["ListShortURLData"].append(tempItem) />
                </cfloop>
            </cfif>
            <cfinvoke method="AllowAddUrl" returnvariable="allowedAddUrl"></cfinvoke>
            <cfif !allowedAddUrl >
                <cfset dataout.LOCKADDBUTTON = true />
            </cfif>
            
            <cfcatch type="Any">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
                <cfset dataout["iTotalRecords"] = 0>
                <cfset dataout["iTotalDisplayRecords"] = 0>
                <cfset dataout["ListShortURLData"] = ArrayNew(1)>
            </cfcatch>
        </cftry>
        
        <cfreturn dataout>
    </cffunction>

    <cffunction name="BuildSortingParamsForDatatable" access="public" hint="build sorting params for datatable">
        <cfargument name="iSortCol_0" default="-1">
        <cfargument name="iSortCol_1" default="-1">
        <!---these are the index of colums that need to be sorted, their value could be  
            1 is Desc, 
            2 is ClickCount_int, 
        --->
        <cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
        <cfset var dataout= "">
        <cftry>
            <!---set order param for query--->
            
            <cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
                <cfset var order_0 = sSortDir_0>
            <cfelse>
                <cfset order_0 = "">    
            </cfif>
            
            <cfif sSortDir_1 EQ "asc" OR sSortDir_1 EQ "desc">
                <cfset var order_1 = sSortDir_1>
            <cfelse>
                <cfset order_1 = "">    
            </cfif>
            <cfif iSortCol_0 neq -1 and order_0 NEQ "">
                <cfset dataout = dataout &" order by ">
                <cfif iSortCol_0 EQ 0> 
                    <cfset dataout = dataout &" c.Desc_vch ">
                <cfelseif iSortCol_0 EQ 1>
                    <cfset dataout = dataout &" c.ClickCount_int ">
                <cfelse>
                    <cfset dataout = dataout &" c.PKId_bi ">    
                </cfif>
                <cfset dataout = dataout &"#order_0#">
            </cfif>
            <cfif iSortCol_1 neq -1 and order_1 NEQ "">
                <cfset dataout = dataout &" , ">
                <cfif iSortCol_1 EQ 0> 
                    <cfset dataout = dataout &" c.Desc_vch ">   
                    <cfset dataout = dataout &"desc">
                <cfelseif iSortCol_1 EQ 1>
                    <cfset dataout = dataout &" c.ClickCount_int ">
                <cfelse>
                    <cfset dataout = dataout & " c.PKId_bi ">
                    <cfset dataout = dataout & "#order_0#">                     
                </cfif>
            </cfif>      
            <cfcatch type="Any" >
                <cfset dataout = "">
            </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>


    <cffunction name="AllowAddUrl" access="public" hint="Determines if current user is allowed add short url" >
        <cfset var result = false />
        <cfset var curUserPlan = {} />
        <cfset var shorUrlLimited = 2 />
        <cfset var totalCurrentUserUrl = 0 /> 

        <cftry>
            <cfinvoke method="CountUrl" returnvariable="totalCurrentUserUrl">
            </cfinvoke>
            
            <!--- Check condition of current user for Add new short url action --->
            <cfinvoke component="session.sire.models.cfc.order_plan" method="GetUserPlan" returnvariable="curUserPlan" >
                <cfinvokeargument name="InpUserId" value="#Session.userID#">
                </cfinvoke>
                <cfset shorUrlLimited = curUserPlan.shortUrlsLimitNumber />
                <cfif shorUrlLimited EQ 0 OR (shorUrlLimited GT 0 AND totalCurrentUserUrl LT shorUrlLimited) >
                    <cfset result = true />
                </cfif>
                <cfcatch>
                    <cfset result = -1 />
                </cfcatch>
            </cftry>
            <cfreturn result />
        </cffunction>


        <cffunction name="CountUrl" access="private" hint="Get number of current user's short url" >
            <cfset var count = 0 />
            <cfset var rsCount = {} />
            
            <cftry>
                <!--- count total short url of current user --->
                <cfquery name="rsCount" datasource="#Session.DBSourceEBM#">
                    SELECT 
                    count(1) AS totalUrl
                    FROM 
                    simplelists.url_shortner
                    WHERE 
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.userID#">
                    AND
                    Active_int = 1
                    AND
                    Type_ti = 1
                </cfquery>
                <cfset count = rsCount.totalUrl />
                <cfcatch type="any">
                    <cfset count = -1 />
                </cfcatch>    
            </cftry>
            <cfreturn count />
        </cffunction>

        <cffunction name="GetShortListForDowngrade" access="remote" output="true" hint="Get mlp by UserId ">
            <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
            <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
            <cfargument name="sColumns" default="" />
            <!--- <cfargument name="sEcho"> --->
            <cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
            <cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
            <cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
            <cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
            <cfargument name="iSortCol_3" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
            <cfargument name="sSortDir_3" default=""><!---this is the direction of sorting, asc or desc --->
            <cfargument name="iSortCol_4" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
            <cfargument name="sSortDir_4" default=""><!---this is the direction of sorting, asc or desc --->
            <cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
            <cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
            <cfargument name="inpAction" required="false" default="downgrade">

            <cfset var dataout = {} />
            <cfset dataout.RXRESULTCODE = '' />
            <cfset dataout.MESSAGE = '' />
            <cfset dataout.ERRMESSAGE = '' />
            <cfset dataout["ListShortURLData"] = ArrayNew(1) />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset var tempItem = {} />
            <cfset var rsCount = ''/>

            <cfset var getShortUrl = '' />
            <cfset var active = 1 />
            <cfif arguments.inpAction EQ 'upgrade'>
                <cfset var active = 2 />
            </cfif>

            <cftry>
                <cfif arguments.iDisplayStart < 0>
                    <cfset arguments.iDisplayStart = 0>
                </cfif>

                <cfquery name="getShortUrl" datasource="#Session.DBSourceREAD#">
                    SELECT 
                    SQL_CALC_FOUND_ROWS
                    c.PKId_bi,
                    c.UniqueTargetId_int,
                    c.RedirectCode_int,
                    c.Created_dt,
                    c.LastClicked_dt,
                    c.Desc_vch,
                    c.ShortURL_vch,
                    c.TargetURL_vch,
                    c.DynamicData_vch,
                    c.ClickCount_int,
                    c.Active_int
                    FROM
                    simplelists.url_shortner c
                    WHERE 
                    c.Active_int = #active#       
                    AND 
                    c.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    AND
                    c.Type_ti = 1
                    ORDER BY 
                    c.PKId_bi desc
                    LIMIT 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#">
                    
                </cfquery>
                
                <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                    SELECT FOUND_ROWS() AS iTotalRecords
                </cfquery>
                <cfloop query="rsCount">
                    <cfset dataout["iTotalDisplayRecords"] =  (iTotalRecords GTE 1 ? iTotalRecords : 0)>
                    <cfset dataout["iTotalRecords"] = (iTotalRecords GTE 1 ? iTotalRecords : 0)>
                </cfloop>

            <!--- <cfset dataout["iTotalRecords"] = (getKeyword.RECORDCOUNT GTE 1 ? getKeyword.RECORDCOUNT : 0) >
                <cfset dataout["iTotalDisplayRecords"] = (getKeyword.RECORDCOUNT GTE 1 ? getKeyword.RECORDCOUNT : 0) > --->

                <cfif getShortUrl.RECORDCOUNT GT 0>
                    <cfloop query="getShortUrl">
                        <cfset tempItem = 
                        {
                            NAME = '#getShortUrl.Desc_vch#',
                            URL = '#getShortUrl.ShortURL_vch#', 
                            CREATED = '#DateFormat(getShortUrl.Created_dt, 'mm/dd/yyyy ')#',
                            ID = '#getShortUrl.PKId_bi#'
                        }
                        >
                        <cfset ArrayAppend(dataout["ListShortURLData"],tempItem)>
                    </cfloop>
                    
                    <cfset dataout.RXRESULTCODE = 1 />
                    <cfset dataout.MESSAGE = 'Get mlp successfully!' />
                <cfelse>
                    <cfset dataout.RXRESULTCODE = 0 />
                    <cfset dataout.MESSAGE = 'No mlp found' />
                </cfif>
                <cfcatch type="any">
                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
                </cfcatch>
            </cftry>

            <cfreturn dataout />
        </cffunction>


        <cffunction name="CheckShortURL" access="remote" output="true" hint="Function check Short Url">

            <cfargument name="inpShortURL" TYPE="string" hint="Short URL to lookup if it is in use yet." />

            <cfset var dataOut = {} />
            <cfset var ShortURLCount = {}/>

            <cfset dataOut.RXRESULTCODE = 0 /> 
            <cfset dataOut.MESSAGE = ""/>
            <cfset dataOut.ERRMESSAGE = ""/>
            <cfset dataOut.EXISTSFLAG = 1 />   

            <cftry>    
                <cfquery name="ShortURLCount" datasource="#Session.DBSourceEBM#">
                    SELECT 
                    COUNT(u.ShortURL_vch) AS TOTALCOUNT
                    FROM 
                    simplelists.url_shortner AS u
                    WHERE 
                    u.Active_int = 1
                    AND                     
                    u.ShortURL_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpShortURL#">                 
                </cfquery>

                <cfif ShortURLCount.TOTALCOUNT GT 0>
                    <cfset dataOut.EXISTSFLAG = 1 />
                <cfelse>
                    <cfset dataOut.EXISTSFLAG = 0 />
                </cfif>           

                <cfif dataOut.EXISTSFLAG EQ 1>
                 <cfset dataOut.RXRESULTCODE = 0 /> 
                 <cfset dataOut.MESSAGE = "This Short URL already is already exists. Please try another."/>
                 <cfset dataOut.ERRMESSAGE = ""/>
             <cfelse >
              <cfset dataOut.RXRESULTCODE = 1 /> 
              <cfset dataOut.MESSAGE = "This Short URL is available."/>
              <cfset dataOut.ERRMESSAGE = ""/>
          </cfif>   

          <cfcatch TYPE="any">
            <cfset dataOut.RXRESULTCODE = -1 /> 
            <cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataOut.ERRMESSAGE = "#cfcatch.detail#"/>
            <cfreturn dataOut>
        </cfcatch>   

    </cftry>
    <cfreturn dataOut/>
</cffunction>


<cffunction name="CheckTargetURL" access="remote" output="true" hint="Function check Target URL">

    <cfargument name="inpTargetURLValue" TYPE="string" hint="Target URL to lookup if it is in use yet." />

    <cfset var dataOut = {} />
    <cfset var TargetURLCount = {} />

    <cfset dataOut.RXRESULTCODE = 0 /> 
    <cfset dataOut.MESSAGE = ""/>
    <cfset dataOut.ERRMESSAGE = ""/>
    <cfset dataOut.EXISTSFLAG = 1 />   

    <cftry>    
        <cfquery name="TargetURLCount" datasource="#Session.DBSourceEBM#">
            SELECT 
                COUNT(u.TargetURL_vch) AS TOTALCOUNT
            FROM 
                simplelists.url_shortner AS u
            WHERE 
                u.Active_int = 1
            AND                     
                u.TargetURL_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpTargetURLValue#">     
            AND
                u.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
        </cfquery>
        <!---old version --->
        <!---
            <cfif TargetURLCount.TOTALCOUNT GT 0>
                <cfset dataOut.EXISTSFLAG = 1 />
            <cfelse>
                <cfset dataOut.EXISTSFLAG = 0 />
            </cfif>           

            <cfif dataOut.EXISTSFLAG EQ 1>
                <cfset dataOut.RXRESULTCODE = 0 /> 
                <cfset dataOut.MESSAGE = "This Target URL already exists. Please try another."/>
                <cfset dataOut.ERRMESSAGE = ""/>
            <cfelse >
                <cfset dataOut.RXRESULTCODE = 1 /> 
                <cfset dataOut.MESSAGE = "This Target URL is available."/>
                <cfset dataOut.ERRMESSAGE = ""/>
            </cfif>   
        --->
        <cfset dataOut.EXISTSFLAG = 0 />
        <cfset dataOut.RXRESULTCODE = 1 /> 
        <cfif TargetURLCount.TOTALCOUNT GT 0>
            <cfset dataOut.MESSAGE = "This Target URL already exists."/>
            <cfset dataOut.ERRMESSAGE = ""/>
        <cfelse>
            <cfset dataOut.MESSAGE = "This Target URL is available."/>
            <cfset dataOut.ERRMESSAGE = ""/>
        </cfif>

        <cfcatch TYPE="any">
            <cfset dataOut.RXRESULTCODE = -1 /> 
            <cfset dataOut.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataOut.ERRMESSAGE = "#cfcatch.detail#"/>
            <cfreturn dataOut>
        </cfcatch>   

    </cftry>
    <cfreturn dataOut/>
</cffunction>

<cffunction name="GetShortUrlByListId" access="remote" output="true" hint="get keyword info by list ID">
    <cfargument name="inpListShortUrlId" type="array" required="true" default="">
    <cfset var dataout = {} />
    <cfset dataout.RXRESULTCODE = -1 />
    <cfset dataout.MESSAGE = ""/>
    <cfset dataout.ERRMESSAGE = ""/>
    <cfset dataout.LIST = {}/>
    <cfset dataout.NAMELIST = []/>
    <cfset var listShortUrl = {} />

    <cfset var defaultShortCode = '' />

    <cfif arrayLen(inpListShortUrlId) GT 0>
        <cftry>

            <cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="defaultShortCode"></cfinvoke>

            <cfquery name="listShortUrl" datasource="#Session.DBSourceEBM#">
               SELECT 
                    PKId_bi,Desc_vch
                FROM 
                    simplelists.url_shortner AS u
                WHERE 
                    u.Active_int = 1
                AND             
                    u.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.USERID#"> 
                AND
                    PKId_bi IN (#ArrayToList(inpListShortUrlId)#)
            </cfquery>  

            <cfif listShortUrl.RECORDCOUNT GT 0>
                <cfloop query="#listShortUrl#">
                    <cfset dataout.LIST["#PKId_bi#"] = #Desc_vch#/>
                    <cfset arrayAppend(dataout.NAMELIST, Desc_vch)/> 
                </cfloop>
                <cfset dataout.RXRESULTCODE = 1 />
            </cfif>
            <cfcatch type="any">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
            </cfcatch>
        </cftry>
    <cfelse>
         <cfset dataout.MESSAGE = "Invalid input."/>    
    </cfif>
    <cfreturn dataout />
</cffunction>



</cfcomponent>    