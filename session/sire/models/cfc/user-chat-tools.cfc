<cfcomponent hint="Admin Tool Feature" output="false">

	<cfinclude template="/public/sire/configs/paths.cfm"/>

	<cfinclude template="/session/sire/configs/alert_constants.cfm"/>


	<cffunction name="GetListChatCampaign" access="remote" output="false" hint="Get list chat campaign">
		<cfargument name="inpUserId" TYPE="string" required="no" default="#session.UserId#" />
		
		<cfargument name="customFilter" TYPE="string" required="no" default=""  />
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />

		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default=""/>		
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout["aaData"] = ArrayNew(1)/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />		
		
		<cfset var shortCode=''>
		<cfset var GetListCampaign="">
		<cfset var GetSessionActive="">
		
		<cfset var filterItem="">
		<cfset var item="">
		<cfset var rsCount="">

		<cfset var orderField = '' />
		<cfswitch expression="#arguments.iSortCol_0#">		    
		    <cfcase value="0">
		        <cfset orderField = 'b.DESC_VCH' />
		    </cfcase>
		    <cfcase value="1">
		        <cfset orderField = 'k.Keyword_vch' />
		    </cfcase>		    
		    <cfcase value="2">
		        <cfset orderField = 'b.Created_dt' />
		    </cfcase>	
		</cfswitch>

		<cftry>					

			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>
			<cfquery name="GetListCampaign" datasource="#Session.DBSourceREAD#">
				SELECT
					SQL_CALC_FOUND_ROWS
					b.BatchId_bi,
					b.RXDSLibrary_int,
					b.RXDSElement_int,
					b.RXDSScript_int,
					b.DESC_VCH,
					b.Created_dt,
					b.ContactGroupId_int,
					b.ContactTypes_vch,
					b.ContactNote_vch, 
					b.ContactIsApplyFilter,
					b.ContactFilter_vch,
					b.EMS_Flag_int,
					b.TemplateId_int,
					b.TemplateType_ti,
					k.KeywordId_int,
					k.Keyword_vch					
				FROM
					simpleobjects.batch AS b					
					LEFT JOIN sms.keyword AS k ON (k.BatchId_bi = b.BatchId_bi AND k.Active_int = 1) 					
				WHERE        
					b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				AND
					b.Active_int > 0
				AND
					b.EMS_Flag_int IN (0,1,2)
				AND
					b.TemplateId_int=9
				AND
					b.ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#shortCode.SHORTCODEID#"/>
				<cfif customFilter NEQ "">
					<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
						<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
						<cfelse>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
						</cfif>
					</cfloop>
				</cfif>				
				<cfif orderField NEQ "">
					ORDER BY
						#orderField# #arguments.sSortDir_0#
				<cfelse>
					ORDER BY 
						b.BatchId_bi DESC
				</cfif>

				LIMIT 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#"/>, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"/>
			</cfquery>	
			
			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>		
			<cfif GetListCampaign.RecordCount GT 0>
				<cfloop query="GetListCampaign">
					<!--- get session status of batch id--->
					<cfquery name="GetSessionActive" datasource="#Session.DBSourceREAD#">
						SELECT							
							ire.BatchId_bi,
							ss.SessionId_bi,
							ss.SessionState_int,
							ss.ContactString_vch
						FROM
							simplexresults.ireresults ire
							JOIN
							simplequeue.sessionire ss
							ON
								ire.IRESessionId_bi = ss.SessionId_bi 
						WHERE
								ire.BatchId_bi = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#BatchId_bi#">
							AND	
								ire.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.UserId#">
							AND
								ire.IREType_int = 2 
							AND	
								LENGTH(ire.ContactString_vch) < 14
							AND 
								ire.ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SESSION.SHORTCODE#">		
							AND
								ss.SessionState_int =1		
						GROUP BY 
							ire.BatchId_bi,
							ss.SessionId_bi,
							ss.SessionState_int,
							ss.ContactString_vch									
					</cfquery>	
					<cfset item = {
						"ID": BatchId_bi, 	
						"Name":DESC_VCH,
						"CreateDate": DateFormat(Created_dt,"mm-dd-yyyy") & " "& TimeFormat(Created_dt, "HH:mm:ss"), 
						"TOTALCHAT":GetSessionActive.RECORDCOUNT,	
						"Keyword": Keyword_vch												
					} />
					<cfset arrayAppend(dataout["aaData"],item) />
				</cfloop>
				
				<cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
				<cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>
			</cfif>			

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
		
</cfcomponent>