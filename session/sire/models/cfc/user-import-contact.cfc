<cfcomponent>


    <cfinclude template="/public/paths.cfm">
    <cfinclude template="/public/sire/configs/paths.cfm">
    <cfinclude template="/public/sire/configs/bulk_insert_constants.cfm">
    <cfinclude template="/session/sire/configs/paths.cfm">
    <cfinclude template="/public/sire/configs/paths.cfm">
    <cfinclude template="/session/sire/configs/env_paths.cfm">
    <cfinclude template="/session/sire/configs/userConstants.cfm">

    <cfinclude template="/session/cfc/csc/constants.cfm">
    <cfinclude template="/session/cfc/csc/inc_smsdelivery.cfm">
    <cfinclude template="/session/cfc/csc/inc_ext_csc.cfm">
    <cfinclude template="/session/cfc/csc/inc-billing-sire.cfm">

    <cfinclude template="/public/sire/configs/bulk_insert_constants.cfm"/>

    <!--- Used IF locking down by session - User has to be logged in --->
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>
    <cfparam name="Session.USERID" default="0"/>

    <cfparam name="DeliveryServiceComponentPath" default="" >
    <cfparam name="DeliveryServiceMCIDDMsComponentPath" default="">

    <cfparam name="imp_skipline" default="0">
    <cfparam name="imp_ErrorsIgnore" default="0">

    <cffunction name="ImportDataFromCSV" access="public" output="true" hint="read contact string from csv then import to db">
        <cfargument name="inpCSVFileName" TYPE="string" required="true" default="" hint="path to csv file">
        <cfargument name="inpStageTable" TYPE="string" required="true" default="" hint="table Name to import data">

        <cfargument name="inpColumnNumber" type="numeric" default="1"/>
        <cfargument name="inpColumnNames" type="string" default="ContactString_vch"/>
        <cfargument name="inpContactStringCol" type="numeric" default="1"/>
        <cfargument name="inpSeparator" type="string" default=","/>
        <cfargument name="inpSkipLine" type="numeric" default="1"/>
        <cfargument name="inpUploadIdPK" type="numeric" default="0" required="true"/>

        <cfset var uploadPath = ExpandPath(UPLOAD_CSV_PATH) />


        <cfset var dataout = {}>
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "invalid Input">
        <cfset dataout.ERRMESSAGE = "">
        <cfset var filePath = '' />
        <cfset var dataFile = '' />
        <cfset var insertContactString = '' />
        <cfset var checkFileContent = -1 />
        <cfset var line = '' />
        <cfset var getContactStringFromStageTable = ''/>
        <cfset var stageTableName = ''/>
        <cfset var rxInsertContactString = ''/>

        <cfset var fieldsArray = '' />
        <cfset var i = ''/>

        <cfset dataout["duplicate"] = 0/>
        <cfset var getDuplicateData = ''/>
        <cfset var arrDuplicateData = []/>
        <cfset var arrData = '' />
        <cfset var getData = '' />
        <cfset var updateDuplicateData = '' />
        <cfset var updateUploadData = '' />
        <cfset var numLine = 0 />
        <cfset var rxGetUserMaxImportNumber = '' />
        <cfset var fristLine =''/>
        <cfset var additionField = []/>

        <!---
        RXRESULTCODE :
        1  - success
        -1 - errors
        -2 - can not find file or file errors
        -3 - stage table already have data
        -4 - over limit import number
        --->

        <cfif arguments.inpCSVFileName NEQ '' AND arguments.inpStageTable NEQ ''>

        <cftry>
                <cfset stageTableName = '#arguments.inpStageTable#'/>

                <!--- Check if table is empty or not --->
                <cfquery name="getContactStringFromStageTable" datasource="#Session.DBSourceEBM#">
                    SELECT count(ContactString_vch) as TotalContactString
                    FROM #stageTableName#;
                </cfquery>

                <!--- If already have data, ignore process..
                Todo: should ask user to replace --->
                <cfif getContactStringFromStageTable.TotalContactString GT 0>
                    <cfset dataout.RXRESULTCODE = -3>
                    <cfset dataout.MESSAGE = 'Stage table already have data'>
                    <cfreturn dataout />
                </cfif>

                <cfset filePath = expandPath(UPLOAD_CSV_PATH&'/'&arguments.inpCSVFileName) />

               <!--- convert LFs to CRLFs --->
                <cfexecute name="unix2dos"
                    arguments="#filePath#"
                    timeout="10">
                </cfexecute>

<!---
                <cfexecute name="unix2dos"
                    arguments="#filePath#"
                    timeout="10">
                </cfexecute>
                --->

                <!--- CHECK IF File exits on local. If not, download from S3 --->
                <!--- <cfset var s3FileUrl = _UPLOAD_S3_CSV_URL&'/'&'#arguments.inpCSVFileName#'/>

                <cfif !fileExists( filePath )>
                    <cfhttp url="#s3FileUrl#" method="get" getAsBinary="yes" path="#expandPath(UPLOAD_CSV_PATH)#" file="#arguments.inpCSVFileName#" />
                </cfif> --->

                <!--- check if file is valid --->
                <cfset dataFile = fileOpen( filePath, "read" ) />

                 <cfloop condition="!fileIsEOF( dataFile )">
                    <cfset line = fileReadLine( dataFile ) />
                    <cfif line NEQ ''>
                        <cfset checkFileContent = 1 />
                    </cfif>
                    <cfset numLine ++ />
                </cfloop>


                <!--- Close the file stream to prevent locking. --->
                <cfset fileClose( dataFile ) />

                 <!--- CHECK IF FILE CONTENT IS OK --->
                <cfif checkFileContent EQ -1>
                    <cfset dataout.RXRESULTCODE = -2 />
                    <cfset dataout.MESSAGE = "File content errors"/>
                    <cfreturn dataout />
                </cfif>

                <cfinvoke method="GetUserMaxImportNumber" returnvariable="rxGetUserMaxImportNumber"></cfinvoke>

                <cfif rxGetUserMaxImportNumber.RXRESULTCODE EQ 1>
                    <cfif (numLine-1) GT rxGetUserMaxImportNumber.MAXLIMIT>
                        <cfset dataout.RXRESULTCODE = -4 />
                        <cfset dataout.MESSAGE = "Imports must not exceed #rxGetUserMaxImportNumber.MAXLIMIT# contact records.  Please reduce your list size and try again."/>
                        <cfreturn dataout />
                    </cfif>
                </cfif>

                <cfquery name="insertContactString" datasource="#Session.DBSourceEBM#" result="rxInsertContactString">
                    LOAD DATA LOCAL INFILE '#filePath#'
                    INTO TABLE #stageTableName#
                    CHARACTER SET UTF8
                    <!--- FIELDS TERMINATED BY ';' ENCLOSED BY '' --->
                    FIELDS TERMINATED BY <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpSeparator#"/>
                    OPTIONALLY ENCLOSED BY '"'
                    LINES TERMINATED BY '\r\n'
                    IGNORE <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpSkipLine#"/> LINES
                    (<cfoutput>#arguments.inpColumnNames#</cfoutput>);
                </cfquery>

                <cfquery name="getData" datasource="#SESSION.DBSourceEBM#">
                    SELECT
                        COUNT(1) AS COUNT
                    FROM
                        #stageTableName#
                </cfquery>

                <cfset dataout["total"] = getData.COUNT />

                <!--- REMOVE ALL NON-NUMBERIC CHARACTER --->
                <!--- REMOVE 1 in contactstring --->
                <cfstoredproc procedure="simplexuploadstage.RemoveNonNumberic" datasource="#SESSION.DBSourceEBM#">
                    <cfprocparam cfsqltype="cf_sql_varchar" value="#stageTableName#">
                </cfstoredproc>

                <!--- GET DUPLICATE DATA --->
                <cfquery name="getDuplicateData" datasource="#SESSION.DBSourceEBM#">
                    SELECT PKId_bi, ContactStringCorrect_vch
                    FROM #stageTableName#
                    WHERE ContactStringCorrect_vch IN (
                            SELECT ContactStringCorrect_vch
                            FROM #stageTableName#
                            Where Status_ti = 1
                            GROUP BY ContactStringCorrect_vch
                            HAVING COUNT(PKId_bi) > 1
                        )
                    ORDER BY ContactStringCorrect_vch desc
                </cfquery>

                <!--- KEEP THE 1st duplicate item, update from 2nd --->
                <cfif getDuplicateData.RECORDCOUNT GT 0>
                    <cfset arrData = []/>
                    <cfloop query="#getDuplicateData#">
                        <cfif ArrayFind(arrData,getDuplicateData.ContactStringCorrect_vch) GT 0>
                            <cfset arrayAppend(arrDuplicateData, getDuplicateData.PKId_bi)>
                        <cfelse>
                            <cfset arrayAppend(arrData, getDuplicateData.ContactStringCorrect_vch)>
                        </cfif>
                    </cfloop>
                </cfif>

                <!--- Update Duplicate Data STATUS --->
                <cfif !arrayIsEmpty(arrDuplicateData)>
                    <cfset dataout["duplicate"] = arrayLen(arrDuplicateData) />
                    <cfquery name="updateDuplicateData" datasource="#Session.DBSourceEBM#">
                        UPDATE #stageTableName#
                        SET Status_ti =  10
                        WHERE PKId_bi IN (#ArrayToList(arrDuplicateData)#)
                    </cfquery>
                </cfif>

                <!--- UPDATE TOTAL RECORD --->
                <cfquery name="updateUploadData" datasource="#SESSION.DBSourceEBM#">
                    UPDATE
                        `simplexuploadstage`.`uploads`
                    SET
                        Total_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#dataout["total"]#"/>,
                        Duplicates_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#dataout["duplicate"]#"/>
                    WHERE
                        PKId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUploadIdPK#"/>
                </cfquery>

                <cfif rxInsertContactString.RECORDCOUNT GT 0>
                    <cfset dataout.RXRESULTCODE = 1 />
                    <cfset dataout.MESSAGE = 'Load data success'/>
                </cfif>

                <!--- delete file --->
                <cffile action="delete" file="#filePath#" />

            <cfcatch>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
            </cfcatch>
            </cftry>

        </cfif>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="CreateContactlistStageTable" access="public" output="true" hint="Create Stage File in DB">
        <cfargument name="inpUploadIdPK" TYPE="numeric" required="true" default="" hint="PKID from 'simplexuploadstage'.'uploads' ">

        <cfargument name="inpColumnNumber" default="1"/>
        <cfargument name="inpColumnNames" default="ContactString_vch"/>
        <cfargument name="inpContactStringCol" default="1"/>
        <cfargument name="inpSeparator" default=","/>

        <cfargument name="inpSendToQueue" default="0"/>

        <cfset var dataout = {}>
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">
        <cfset dataout.ERRMESSAGE = "">
        <cfset dataout.TABLENAME = "">
        <cfset var createStageTable = ''/>
        <cfset var dropStageTable = ''/>
        <cfset var tableName = "simplexuploadstage.Contactlist_stage_"&#arguments.inpUploadIdPK#>
        <cfset var createTable = '' />

        <cfset var fieldsArray = '' />
        <cfset var i = ''/>

        <cftry>

            <!---
            <cfquery name="dropStageTable" datasource="#Session.DBSourceEBM#">
                DROP TABLE IF EXISTS #tableName#
            </cfquery>
            --->

            <cfif arguments.inpColumnNumber GT 0>
                <cfset fieldsArray = listToArray(arguments.inpColumnNames, ',')/>
            </cfif>

            <cfquery name="createStageTable" datasource="#Session.DBSourceEBM#" result="createTable">
                CREATE TABLE #tableName#
                (
                    PKId_bi INT NOT NULL AUTO_INCREMENT,
                    Status_ti TINYINT(4) NULL DEFAULT 1,
                    <!---ContactString_vch VARCHAR(200) NULL,--->
                    ContactStringCorrect_vch VARCHAR(200) NULL,
                    Note_vch VARCHAR(255) NULL,
                    TimeZone_int INT(11) NULL DEFAULT 31,
                    City_vch VARCHAR(50) NULL DEFAULT "N/A",
                    State_vch VARCHAR(50) NULL DEFAULT "N/A",
                    Zip1_vch VARCHAR(20) NULL DEFAULT "N/A",
                    Zip2_vch VARCHAR(20) NULL DEFAULT "N/A",
                    Zip3_vch VARCHAR(20) NULL DEFAULT "N/A",
                    BatchId_bi BIGINT(11) NULL DEFAULT 0,
                    SendToQueue_ti TINYINT(4) NULL DEFAULT <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpSendToQueue#"/>,
                    <!--- NPA CHAR(3) NULL DEFAULT NULL,
                    NXX CHAR(3) NOT NULL DEFAULT '',--->
                    <cfif arguments.inpColumnNumber GT 0>
                        <cfloop from="1" to="#arrayLen(fieldsArray)#" index="i">
                            <!---<cfif i NEQ arguments.inpContactStringCol>--->
                            <cfif fieldsArray[i] NEQ "@dummy">
                                <cfoutput>
                                    #fieldsArray[i]# VARCHAR(255) NULL,
                                </cfoutput>
                            </cfif>
                            <!---</cfif>--->
                        </cfloop>
                    </cfif>
                    PRIMARY KEY (PKId_bi)
                )
            </cfquery>

            <cfset dataout.RXRESULTCODE = 1>
            <cfset dataout.TABLENAME = #tableName#>
            <cfset dataout.MESSAGE = 'Created table #tableName#'>

        <cfcatch type="Any" >
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>

        <cfreturn dataout/>

    </cffunction>

    <cffunction name="AddToUploadTable" access="public" output="true" hint="Add new row to upload table">
        <cfargument name="inpUserId" required="false" default="#session.UserID#">
        <cfargument name="inpSourceFileName" TYPE="string" required="true" default="" hint="">
        <cfargument name="inpProcessType" TYPE="string" required="true" default="" hint="">
        <cfargument name="inpCampaignID" TYPE="numeric" required="true" default="0" hint="">
        <cfargument name="inpSubcriberListID" TYPE="numeric" required="true" default="0" hint="">
        <cfargument name="inpImportID" TYPE="string" required="true" default="" hint="">

        <cfargument name="inpColumnNames" default="ContactString_vch"/>
        <cfargument name="inpColumnNumber" default="1"/>
        <cfargument name="inpErrorsIgnore" type="numeric" default="0"/>

        <cfargument name="inpSendToQueue" type="numeric" default="0"/>

        <cfset var dataout = {}>
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">
        <cfset dataout.ERRMESSAGE = "">
        <cfset dataout.PKID = 0>
        <cfset var insertToUploadTable = ''/>
        <cfset var rxInsertToUploadTable = ''/>

        <cftry>

            <cfquery name="insertToUploadTable" datasource="#Session.DBSourceEBM#" result="rxInsertToUploadTable" >
                INSERT INTO `simplexuploadstage`.`uploads`
                (
                    UserId_int,
                    SourceFileName_vch,
                    ProcessType_ti,
                    Created_dt,
                    BatchId_bi,
                    SubcriberList_bi,
                    ProcessId_vch,
                    AdminUserID_int,
                    ColumnNumber_int,
                    ColumnNames_vch,
                    ErrorsIgnore_int,
                    SendToQueue_ti,
                    UserIP_vch,
                    UploadType_ti
                )
                VALUES
                (
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpSourceFileName#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpProcessType#">,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCampaignID#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpSubcriberListID#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpImportID#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.UserId#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpColumnNumber#"/>,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpColumnNames#"/>,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpErrorsIgnore#"/>,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpSendToQueue#"/>,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.REMOTE_ADDR),45)#">,
                    2
                )
            </cfquery>

            <cfif rxInsertToUploadTable.GENERATED_KEY GT 0>
                <cfset dataout.RXRESULTCODE = 1>
                <cfset dataout.MESSAGE = 'Created table success'>
                <cfset dataout.PKID = rxInsertToUploadTable.GENERATED_KEY >
            </cfif>

        <cfcatch type="Any" >
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>

        <cfreturn dataout/>

    </cffunction>

    <cffunction name="ProcessUpload" access="public" output="true" hint="Process Upload file">
        <cfargument name="inpUserId" required="false" default="#session.UserID#">
        <cfargument name="inpSourceFileName" TYPE="string" required="true" default="" hint="">
        <cfargument name="inpProcessType" TYPE="string" required="true" default="" hint="">
        <cfargument name="inpCampaignID" TYPE="numeric" required="true" default="0" hint="">
        <cfargument name="inpSubcriberListID" TYPE="numeric" required="true" default="0" hint="">
        <cfargument name="inpImportID" TYPE="string" required="true" default="" hint="">

        <cfargument name="inpColumnNumber" type="numeric" default="1"/>
        <cfargument name="inpColumnNames" type="string" default="ContactString_vch"/>
        <cfargument name="inpContactStringCol" type="numeric" default="1"/>
        <cfargument name="inpSeparator" type="string" default=","/>
        <cfargument name="inpSkipLine" type="numeric" default="0"/>
        <cfargument name="inpErrorsIgnore" type="numeric" default="0"/>

        <cfargument name="inpSendToQueue" type="numeric" default="0"/>

        <cfset var dataout = {}>
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">
        <cfset dataout.ERRMESSAGE = "">
        <cfset dataout.UPLOADPKID = "0">
        <cfset var pKID = 0/>
        <cfset var contactlistStageTable = ''/>
        <cfset var AddToUploadTable = ''/>
        <cfset var rxAddToUploadTable = ''/>
        <cfset var CreateContactlistStageTable = ''/>
        <cfset var rxCreateContactlistStageTable = ''/>
        <cfset var rxImportDataFromCSV = ''/>
        <cfset var ImportDataFromCSV = ''/>
        <cfset var dropStageTable = ''/>
        <cfset var deleteUploadRow = ''/>

        <cftry>
            <!--- add new row to upload table --->
            <cfinvoke method="AddToUploadTable" component="session.sire.models.cfc.user-import-contact" returnvariable="rxAddToUploadTable">
                <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
                <cfinvokeargument name="inpSourceFileName" value="#arguments.inpSourceFileName#"/>
                <cfinvokeargument name="inpProcessType" value="#arguments.inpProcessType#"/>
                <cfinvokeargument name="inpCampaignID" value="#arguments.inpCampaignID#"/>
                <cfinvokeargument name="inpSubcriberListID" value="#arguments.inpSubcriberListID#"/>
                <cfinvokeargument name="inpImportID" value="#arguments.inpImportID#"/>

                <cfinvokeargument name="inpColumnNumber" value="#arguments.inpColumnNumber#"/>
                <cfinvokeargument name="inpColumnNames" value="#arguments.inpColumnNames#"/>
                <cfinvokeargument name="inpErrorsIgnore" value="#arguments.inpErrorsIgnore#"/>

                <cfinvokeargument name="inpSendToQueue" value="#arguments.inpSendToQueue#"/>
            </cfinvoke>

            <!--- If Add success --->
            <cfif rxAddToUploadTable.RXRESULTCODE EQ 1>
                <cfset pKID = rxAddToUploadTable.PKID>
                <cfset dataout.UPLOADPKID = pKID>
                <!--- create stage table --->
                <cfinvoke method="CreateContactlistStageTable" component="session.sire.models.cfc.user-import-contact" returnvariable="rxCreateContactlistStageTable">
                    <cfinvokeargument name="inpUploadIdPK" value="#pKID#"/>

                    <cfinvokeargument name="inpColumnNumber" value="#arguments.inpColumnNumber#"/>
                    <cfinvokeargument name="inpColumnNames" value="#arguments.inpColumnNames#"/>
                    <cfinvokeargument name="inpContactStringCol" value="#arguments.inpContactStringCol#"/>
                    <cfinvokeargument name="inpSeparator" value="#arguments.inpSeparator#"/>

                    <cfinvokeargument name="inpSendToQueue" value="#arguments.inpSendToQueue#"/>
                </cfinvoke>

                <cfif rxCreateContactlistStageTable.RXRESULTCODE GT 0>
                    <!--- Import data from csv --->
                    <cfinvoke method="ImportDataFromCSV" component="session.sire.models.cfc.user-import-contact" returnvariable="rxImportDataFromCSV">
                        <cfinvokeargument name="inpCSVFileName" value="#arguments.inpSourceFileName#"/>
                        <cfinvokeargument name="inpStageTable" value="#rxCreateContactlistStageTable.TABLENAME#"/>
                        <cfinvokeargument name="inpColumnNumber" value="#arguments.inpColumnNumber#"/>
                        <cfinvokeargument name="inpColumnNames" value="#arguments.inpColumnNames#"/>
                        <cfinvokeargument name="inpContactStringCol" value="#arguments.inpContactStringCol#"/>
                        <cfinvokeargument name="inpSeparator" value="#arguments.inpSeparator#"/>
                        <cfinvokeargument name="inpSkipLine" value="#arguments.inpSkipLine#"/>
                        <cfinvokeargument name="inpUploadIdPK" value="#pKID#"/>
                    </cfinvoke>

                    <cfif rxImportDataFromCSV.RXRESULTCODE EQ 1>
                        <cfset dataout.RXRESULTCODE = 1 />
                    <cfelse>
                        <cfset dataout.MESSAGE = rxImportDataFromCSV.MESSAGE />
                        <cfset dataout.ERRMESSAGE = rxImportDataFromCSV.ERRMESSAGE />
                    </cfif>
                <cfelse>
                    <cfset dataout.MESSAGE = 'Can not create stage table (#pKID#)'/>
                </cfif>
            <cfelse>
                <cfset dataout.MESSAGE = 'Can not insert to upload table'/>
            </cfif>

            <!--- IF NOT IMPORT SUCCESS -> REMOVE ALL DATA --->
            <cfif dataout.RXRESULTCODE NEQ 1 >
                <cfif contactlistStageTable NEQ ''>
                    <cfquery name="dropStageTable" datasource="#Session.DBSourceEBM#">
                        DROP TABLE IF EXISTS #rxCreateContactlistStageTable.TABLENAME#
                    </cfquery>
                </cfif>

                <cfif pKID GT 0 >
                    <cfquery name="deleteUploadRow" datasource="#Session.DBSourceEBM#">
                        DELETE FROM `simplexuploadstage`.`uploads`
                        WHERE
                            PkID_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#pKID#">
                        AND
                            AdminUserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.UserID#">
                    </cfquery>
                </cfif>
            </cfif>

        <cfcatch type="Any" >
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>

        <cfreturn dataout/>

    </cffunction>
    <cffunction name="GetIndiDataByUploadId" access="remote" output="false" hint="Get Upload Data">
		<cfargument name="inpUploadId" TYPE="string" required="yes" default="" />
        <cfargument name="inpType" TYPE="string" required="no" default="0" />


		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default=""/>
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout["aaData"] = ArrayNew(1)/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />

		<cfset var GetIndiDataByUploadId = '' />
        <cfset var ListContact=''>
        <cfset var contact=''>
        <cfset var rsCount=''>
        <cfset var running=0>
        <cfset var item=''>




		<cfset var orderField = '' />
		<cfswitch expression="#arguments.iSortCol_0#">
		    <cfcase value="0">
		        <cfset orderField = '' />
		    </cfcase>
		</cfswitch>

		<cftry>

			<cfif arguments.inpType EQ 0>
                <cfquery name="GetIndiDataByUploadId" datasource="#Session.DBSourceREAD#">
                    SELECT
                        SQL_CALC_FOUND_ROWS
                        data.PKId_bi,
                        data.ListContactString_vch
                    FROM
                        simplequeue.import_individual_quee data
                    WHERE
                        PKId_bi	=  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUploadId#"/>
                </cfquery>
                <cfset ListContact=GetIndiDataByUploadId.ListContactString_vch>

                <cfif listlen(ListContact) GT 0>
                    <cfset running=0>
                    <cfloop list="#ListContact#" item="contact">
                        <cfset running++>
                        <cfif running LTE (arguments.iDisplayStart + arguments.iDisplayLength) AND running GT arguments.iDisplayStart>
                            <cfset item = {
                                PhoneNumber = "#contact#"
                            } />
                            <cfset arrayAppend(dataout["aaData"],item) />
                        </cfif>
                    </cfloop>

                    <cfset dataout["iTotalDisplayRecords"] = listlen(ListContact)>
                    <cfset dataout["iTotalRecords"] = listlen(ListContact)>
                </cfif>
            <cfelse>
                <cfquery name="GetIndiDataByUploadId" datasource="#Session.DBSourceREAD#">
                    SELECT
                        SQL_CALC_FOUND_ROWS
                        ContactString_vch
                    FROM
                        simplelists.import_individual_detail
                    WHERE
                        ImportId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUploadId#"/>
                    AND
                        Status_ti =<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpType#"/>
                    GROUP BY ContactString_vch
                    LIMIT
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#"/>,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"/>
                </cfquery>
                <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                    SELECT FOUND_ROWS() AS iTotalRecords
                </cfquery>
                <cfloop query="GetIndiDataByUploadId">
                    <cfset item = {
                        PhoneNumber = ContactString_vch
                    } />
                    <cfset arrayAppend(dataout["aaData"],item) />
                </cfloop>
                <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
                <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>
            </cfif>


			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
    <cffunction name="GetDataByUploadId" access="remote" output="false" hint="Get Upload Data">
		<cfargument name="inpUploadId" TYPE="string" required="yes" default="" />

		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default=""/>
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout["aaData"] = ArrayNew(1)/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />

		<cfset var GetDataByUploadId = '' />
        <cfset var item = '' />
        <cfset var rsCount = '' />
        <cfset var tableName = "simplexuploadstage.Contactlist_stage_"&arguments.inpUploadId />


		<cfset var orderField = '' />
		<cfswitch expression="#arguments.iSortCol_0#">
		    <cfcase value="0">
		        <cfset orderField = 'data.ContactString_vch' />
		    </cfcase>
		</cfswitch>

		<cftry>


			<cfquery name="GetDataByUploadId" datasource="#Session.DBSourceREAD#">
				SELECT
					SQL_CALC_FOUND_ROWS
					data.PKId_bi,
                    data.ContactString_vch
				FROM
					#tableName# data

				<cfif orderField NEQ "">
					ORDER BY
						#orderField# #arguments.sSortDir_0#
				<cfelse>
					ORDER BY
						data.PKId_bi DESC
				</cfif>

				LIMIT
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#"/>,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"/>
			</cfquery>

			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			<cfif GetDataByUploadId.RecordCount GT 0>
				<cfloop query="GetDataByUploadId">
					<cfset item = {
						id = "#PKId_bi#",
                        PhoneNumber = "#ContactString_vch#"
					} />
					<cfset arrayAppend(dataout["aaData"],item) />
				</cfloop>

				<cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
				<cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>
			</cfif>


			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
    <cffunction name="GetAllUploadList" access="remote" output="false" hint="Get all upload list">
		<cfargument name="inpUserId" TYPE="string" required="no" default="#session.UserId#" />
		<cfargument name="customFilter" TYPE="string" required="no" default=""  />
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="0" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="10" />

		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default=""/>
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout["aaData"] = ArrayNew(1)/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />

		<cfset var item = '' />
        <cfset var filterItem = '' />
		<cfset var GetAllUploadList = '' />
		<cfset var rsCount = '' />

		<cfset var orderField = '' />
		<cfswitch expression="#arguments.iSortCol_0#">
		    <cfcase value="0">
                <cfset orderField = 'u.UserId_int' />
		    </cfcase>
		    <cfcase value="1">
		        <cfset orderField = 'u.OriginFileName_vch' />
		    </cfcase>
            <cfcase value="2">
		        <cfset orderField = 'u.Created_dt' />
		    </cfcase>
		</cfswitch>

		<cftry>


			<cfquery name="GetAllUploadList" datasource="#Session.DBSourceREAD#">
				SELECT
					SQL_CALC_FOUND_ROWS
					u.PKId_bi,
                    u.OriginFileName_vch,

					IF(u.Total_int > 0, u.Total_int, 0) AS TotalContactString_int,
                    IF(u.CountErrors_int > 0, u.CountErrors_int, 0) AS CountErrors_int,
                    IF(u.Duplicates_int > 0, u.Duplicates_int, 0) AS Duplicates_int,
                    IF(u.Valid_int > 0, u.Valid_int, 0) AS Valid_int,
                    IF(u.Dnc_int > 0, u.Dnc_int, 0) AS Dnc_int,

                    IF(u.Deleted_int > 0, u.Deleted_int, 0) AS Deleted_int,
                    CONCAT(SUBSTRING(ua.FirstName_vch, 1, 1), ".", ua.LastName_vch) AS UserName,

                    u.Status_ti,
                    u.ProcessType_ti,
                    u.UserId_int AS UserID,
                    DATE_FORMAT(u.Updated_dt, '%m-%d-%Y %r') AS UpdatedAt,
                    DATE_FORMAT(u.Created_dt, '%m-%d-%Y %r') AS CreatedAt,
                    ua.EmailAddress_vch,
                    DATE_FORMAT(u.AdminActionDate_dt, '%m-%d-%Y %r') AS AdminActionAt,
                    AdminActionId_int as AdminActionBy,
                    AdminActionType_int as AdminActionType,
                    CONCAT(SUBSTRING(ua.FirstName_vch, 1, 1), ".", ua.LastName_vch) AS NameAdminActionBy
				FROM
					simplexuploadstage.uploads u
                LEFT JOIN
                    simpleobjects.useraccount AS ua ON u.AdminActionId_int = ua.UserId_int
                LEFT JOIN
                    simpleobjects.useraccount AS uimport ON u.UserId_int = uimport.UserId_int
				WHERE
                UploadType_ti=2
				<cfif customFilter NEQ "">
					<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
						<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
						<cfelse>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
						</cfif>
					</cfloop>
				</cfif>
				<cfif orderField NEQ "">
					ORDER BY
						#orderField# #arguments.sSortDir_0#
				<cfelse>
					ORDER BY
						b.BatchId_bi DESC
				</cfif>

				LIMIT
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#"/>,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"/>
			</cfquery>
			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			<cfif GetAllUploadList.RecordCount GT 0>
				<cfloop query="GetAllUploadList">
					<cfset item = {
						id = "#PKId_bi#",
                        filename = "#OriginFileName_vch#",
                        Loaded = "#TotalContactString_int#",
                        Invalid = "#CountErrors_int#",
                        Duplicate = "#Duplicates_int#",
                        DNC="#Dnc_int#",
                        Valid = "#Valid_int#",
                        Deleted = "#Deleted_int#",
                        UserID = "#UserID#",
                        Status = "#Status_ti#",

                        AdminActionAt = "#AdminActionAt#",
                        AdminActionBy = "#AdminActionBy#",
                        NameAdminActionBy = "#NameAdminActionBy#",
                        AdminActionType = "#AdminActionType#",
                        CreatedAt = "#CreatedAt#"
					} />
					<cfset arrayAppend(dataout["aaData"],item) />
				</cfloop>

				<cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
				<cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>
			</cfif>


			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
    <cffunction name="getReport" access="remote" hint="get upload Report">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="customFilter" default="">

        <cfset var dataout = {} />
        <cfset var getData = {} />
        <cfset var getSummary = {} />

        <cfset dataout["summary"]["duplicate"] = 0 />
        <cfset dataout["summary"]["unique"] = 0 />
        <cfset dataout["summary"]["invalid"] = 0 />
        <cfset dataout["summary"]["loaded"] = 0 />
        <cfset dataout["datalist"] = [] />

        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>

        <cfset var rsCount  = {} />
        <cfset var customFilterStr = DeserializeJson(arguments.customFilter) />

        <!--- <cfdump var="#customFilterStr#" abort="true" /> --->

        <cftry>
            <cfquery name="getSummary" datasource="#SESSION.DBSourceEBM#">
                SELECT
                    IF(SUM(Total_int) > 0, SUM(Total_int),0)    AS Loaded,
                    IF(SUM(Duplicates_int) > 0, SUM(Duplicates_int),0)  AS duplicated,
                    IF(SUM(CountErrors_int) > 0, SUM(CountErrors_int),0)    AS invalided,
                    IF(SUM(Valid_int) > 0, SUM(Valid_int),0)  AS uniqued
                FROM
                    simplexuploadstage.uploads up
                WHERE
                    1
                AND
                    up.UserId_int= <cfqueryparam cfsqltype="cf_sql_integer" value="#session.userID#">
                AND
                    up.UploadType_ti = 2
                    <cfif IsStruct(customFilterStr) AND structKeyExists(customFilterStr, "USERID")>
                        <cfif customFilterStr.userID GT 0 >
                            AND up.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#customFilterStr.userID#">
                        </cfif>
                        <cfif customFilterStr.startDate NEQ "" >
                            AND up.Created_dt
                                BETWEEN <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#customFilterStr.startDate#">
                                AND DATE_ADD(<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#customFilterStr.endDate#">,INTERVAL 1 DAY)
                        </cfif>

                    </cfif>
                -- AND
                    -- Status_ti != #UPLOADS_DELETED#
            </cfquery>

            <cfset dataout["summary"]["duplicate"] = getSummary.duplicated />
            <cfset dataout["summary"]["unique"] = getSummary.uniqued />
            <cfset dataout["summary"]["invalid"] = getSummary.invalided />
            <cfset dataout["summary"]["loaded"] = getSummary.Loaded />


            <cfquery name="getData" datasource="#SESSION.DBSourceEBM#">

                SELECT SQL_CALC_FOUND_ROWS
                    PKID_BI,
                    OriginFileName_vch,
                    IF(Total_int > 0, Total_int, 0) AS TotalContactString_int,
                    IF(CountErrors_int > 0, CountErrors_int, 0) AS CountErrors_int,
                    IF(Duplicates_int > 0, Duplicates_int, 0) AS Duplicates_int,
                    IF(Valid_int > 0, Valid_int, 0) AS Valid_int,
                    IF(Dnc_int > 0, Dnc_int, 0) AS Dnc_int,

                    IF(ProcessingTime_int > 0, ProcessingTime_int, 0) AS ProcessingTime_int,
                    IF(Deleted_int > 0, Deleted_int, 0) AS Deleted_int,
                    CONCAT(SUBSTRING(ua.FirstName_vch, 1, 1), ".", ua.LastName_vch) AS UserName,
                    CONCAT(SUBSTRING(ua1.FirstName_vch, 1, 1), ".", ua1.LastName_vch) AS ImportedBy,
                    CONCAT(SUBSTRING(ua2.FirstName_vch, 1, 1), ".", ua2.LastName_vch) AS UpdatedBy,
                    Status_ti,
                    ProcessType_ti,
                    up.UserId_int AS UserID,
                    DATE_FORMAT(Updated_dt, '%Y-%m-%d %r') AS UpdatedAt,
                    DATE_FORMAT(up.Created_dt, '%m-%d-%Y %r') AS CreatedAt,
                    up.SendToQueue_ti,
                    subsc.GroupName_vch as SubscriberList
                FROM
                    simplexuploadstage.uploads AS up
                    LEFT JOIN simpleobjects.useraccount AS ua ON up.UserId_int = ua.UserId_int
                    LEFT JOIN simpleobjects.useraccount AS ua1 ON up.AdminUserID_int = ua1.UserId_int
                    LEFT JOIN simpleobjects.useraccount AS ua2 ON up.LastUpdatedAUID_int = ua2.UserId_int
                    LEFT JOIN  simplelists.grouplist subsc ON up.SubcriberList_bi=subsc.GroupId_bi
                WHERE
                    1
                AND
                    up.UserId_int= <cfqueryparam cfsqltype="cf_sql_integer" value="#session.userID#">
                AND
                    up.UploadType_ti = 2
                    <cfif IsStruct(customFilterStr) AND structKeyExists(customFilterStr, "USERID")>
                        <cfif customFilterStr.userID GT 0 >
                            AND up.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#customFilterStr.userID#">
                        </cfif>
                        <cfif customFilterStr.startDate NEQ "" >
                            AND up.Created_dt
                                BETWEEN <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#customFilterStr.startDate#">
                                AND DATE_ADD(<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#customFilterStr.endDate#">,INTERVAL 1 DAY)
                        </cfif>

                    </cfif>
                -- AND
                    -- Status_ti != #UPLOADS_DELETED#
                ORDER BY
                    up.Created_dt DESC
                LIMIT
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
                OFFSET
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
            </cfquery>
            <!--- <cfdump var="#getData#" abort="true"/> --->

            <cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>
            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>

            <cfloop query="getData" >
                <cfset var tmpItem = {
                    id = "#getData.PKId_bi#",
                    filename = "#getData.OriginFileName_vch#",
                    Loaded = "#getData.TotalContactString_int#",
                    Invalid = "#getData.CountErrors_int#",
                    Duplicate = "#getData.Duplicates_int#",
                    DNC="#getData.Dnc_int#",
                    Unique = "#getData.Valid_int#",
                    Deleted = "#getData.Deleted_int#",
                    ProcessingTime = "#getData.ProcessingTime_int#",
                    UserName = "#getData.UserName#",
                    UserID = "#getData.UserID#",
                    Status = "#getData.Status_ti#",
                    ImportedBy = "#ImportedBy#",
                    UpdatedAt = "#UpdatedAt#",
                    UpdatedBy = "#UpdatedBy#",
                    ListType  = "#ProcessType_ti#",
                    CreatedAt = "#CreatedAt#",
                    SendToQueue = "#SendToQueue_ti#",
                    SubscriberList=SubscriberList
                }/>

                <cfset ArrayAppend(dataout["datalist"], tmpItem) />
            </cfloop>


            <cfset dataout.MESSAGE = "Get data successfully!" />
            <cfset dataout.RXRESULTCODE = 1 />

            <cfcatch type="any">
                <cfset dataout.MESSAGE = cfcatch.DETAIL />
                <cfset dataout.ERRMESSAGE = cfcatch.MESSAGE />
                <cfset dataout.TYPE = cfcatch.TYPE />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>
    <cffunction name="UploadCSVContact" access="remote" hint="Save CSV contact">
        <cfset var dataout = {} />
        <cfset var retValUploadCSV = '' />
        <cfset var uploadResult = '' />
        <cfset var userStorageInfo = '' />
        <cfset var checkFilename = '' />
        <cfset var uploadPath = ExpandPath(UPLOAD_CSV_PATH) />

        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.FILENAME_ORG = ''/>
        <cfset dataout.FILENAME = ''/>
        <cfset var processType = 1>
        <cfset var RXProcessUpload = ''/>
        <cfset var uploadPKID = ''/>
        <cfset var RXCleanData = ''/>
        <cfset var RXImportContactToSubcriberList = ''/>
        <cfset var inpUserId = ''/>
        <cfset inpUserId = Session.UserID/>

        <cfset var numLine = 0/>
        <cfset var errornum = 0/>

        <cfset var filePath = ''/>
        <cfset var dataFile = ''/>
        <cfset var line = ''/>
        <cfset var checkFileContent = ''/>
        <cfset var rxGetUserMaxImportNumber = ''/>

        <cfset var firstLineHeader = arrayNew(1)/>
        <cfset var nextLineHeader = arrayNew(1)/>
        <cfset var checkname = arrayNew(1)/>
        <cfset var checkFileCSVType = 1 />

        <cftry>
        <cfif NOT DirectoryExists(uploadPath)>
            <cfdirectory action = "create" directory="#uploadPath#" mode="770" />
        </cfif>

        <!--- Upload file to server --->
        <cffile action="upload" fileField="file" destination="#uploadPath#" accept="text/csv,application/vnd.ms-excel,application/csv,text/plain" nameconflict="MakeUnique" result="uploadResult" mode="770">
        <cfif !arrayContains(_ACCEPTEDCSVTYPE, lCase(uploadResult.clientfileext))>
            <!--- <cffile action="delete" file="#uploadResult.serverdirectory#/#uploadResult.serverfile#" /> --->
            <cfthrow message="File type not accepted!" detail="File type not accepted!" />
        </cfif>
        <cfset dataout.FILENAME_ORG = uploadResult.serverfile/>
         <!--- Check filesize, client already handle this but check again for security --->
            <cfif uploadResult.filesize GT _CSVMAXSIZE>
                <cffile action="delete" file="#uploadResult.serverdirectory#/#uploadResult.serverfile#" />
                <cfthrow message="This file is too large! Max file size is 300MB" />
            </cfif>

            <!--- <cfdump var="#uploadResult.serverdirectory#/contact-string-rwerwerwer.#lCase(uploadResult.clientfileext)#" abort="true"> --->
            <cfset var newName = "contact-string-#inpUserId#-#DateFormat(NOW(), "mmm-dd-yyyy")#-#TimeFormat(now(), "HH-mm-ss")#.#lCase(uploadResult.clientfileext)#" />
            <cffile action="rename" source="#uploadResult.serverdirectory#/#uploadResult.serverfile#" destination="#newName#" attributes="normal" >

            <cfexecute name="mac2unix"
                arguments="#uploadPath#/#newName#"
                timeout="10">
            </cfexecute>

             <cfset filePath = expandPath(UPLOAD_CSV_PATH&'/'&newName) />

            <!--- check if file is valid --->
            <cfset dataFile = fileOpen( filePath, "read" ) />

            <cfloop condition="!fileIsEOF( dataFile )">
                <cfset line = fileReadLine( dataFile ) />

                <cfif line NEQ ''>
                    <cfset checkFileContent = 1 />
                <cfelse>
                    <cfbreak>
                </cfif>

                <!--- check valid csv type --->
                <cfif numLine EQ 0>
                   <cfset firstLineHeader = listToArray(line,",",true,true)>
                <cfelse>
                    <cfset nextLineHeader = listToArray(line,",",true,true)>
                    <cfset checkname = listToArray(line,'"',true,true)>
                    <cfif arrayLen(checkname) LT 2>
                        <cfif arrayLen(firstLineHeader) NEQ arrayLen(nextLineHeader)>
                                <cfset errornum ++/>
                                <cfif errornum GT LSParseNumber(imp_ErrorsIgnore)>
                                    <cfset var checkFileCSVType = 0 />
                                    <cfbreak>
                                </cfif>
                        </cfif>
                    </cfif>
                </cfif>
                <cfset numLine ++ />
            </cfloop>
            <!--- Close the file stream to prevent locking. --->
            <cfset fileClose( dataFile ) />

             <!--- CHECK IF FILE CONTENT IS OK --->
            <cfif checkFileContent EQ -1>
                 <cffile action="delete" file="#uploadResult.serverdirectory#/#newName#" />
                <cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.MESSAGE = "File content errors"/>
                <cfreturn dataout />
            </cfif>

             <!--- CHECK IF FILE TYPE IS OK --->
            <cfif checkFileCSVType EQ 0>
                <cffile action="delete" file="#uploadResult.serverdirectory#/#newName#" />
                <cfset dataout.RXRESULTCODE = -3 />
                <cfset dataout.MESSAGE = "Invalid CSV TYPE - at line(#numLine+1#)"/>
                <cfreturn dataout />
            </cfif>
            <!---  CHECK skip line   --->
            <cfif LSParseNumber(imp_skipline) GTE numLine>
                <cffile action="delete" file="#uploadResult.serverdirectory#/#newName#" />
                <cfset dataout.RXRESULTCODE = -5 />
                <cfset dataout.MESSAGE = "Phone number should be required field"/>
                <cfreturn dataout />
            </cfif>

            <cfinvoke method="GetUserMaxImportNumber" returnvariable="rxGetUserMaxImportNumber"></cfinvoke>

            <cfif rxGetUserMaxImportNumber.RXRESULTCODE EQ 1>
                <cfif (numLine-1) GT rxGetUserMaxImportNumber.MAXLIMIT>
                    <cffile action="delete" file="#uploadResult.serverdirectory#/#newName#" />
                    <cfset dataout.RXRESULTCODE = -4 />
                    <cfset dataout.MESSAGE = "Imports must not exceed #rxGetUserMaxImportNumber.MAXLIMIT# contact records.  Please reduce your list size and try again."/>
                    <cfreturn dataout />
                </cfif>
            </cfif>


            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.FILENAME = newName/>
        <cfcatch type="Any" >
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
        </cfcatch>
        </cftry>
         <cfreturn dataout/>

    </cffunction>

    <cffunction name="SaveCSVContact" access="remote" hint="Save CSV contact">
        <cfargument name="inpUserId" required="false" default="#session.UserID#">
        <cfargument name="inpSubcriberListID" default="0">
        <cfargument name="inpCampaignID" default="0">
        <cfargument name="inptProcessType" default="1">
        <cfargument name="inpImportID" default="">

        <cfargument name="inpNewName" default="" required="true">
        <cfargument name="inpOrgFileName" default="" required="true">

        <cfargument name="inpColumnNumber" type="numeric" default="1"/>
        <cfargument name="inpColumnNames" type="string" default="ContactString_vch"/>
        <cfargument name="inpContactStringCol" type="numeric" default="1"/>
        <cfargument name="inpSeparator" type="string" default=","/>
        <cfargument name="inpSkipLine" type="numeric" default="0"/>
        <cfargument name="inpErrorsIgnore" type="numeric" default="0"/>

        <cfargument name="inpSendToQueue" type="numeric" default="0"/>



        <cfset var dataout = {} />
        <cfset var retValUploadCSV = '' />
        <cfset var uploadResult = '' />
        <cfset var userStorageInfo = '' />
        <cfset var checkFilename = '' />
        <cfset var uploadPath = ExpandPath(UPLOAD_CSV_PATH) />

        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.FILENAME = ''/>
        <cfset var processType = 1>
        <cfset var RXProcessUpload = ''/>
        <cfset var uploadPKID = ''/>
        <cfset var RXCleanData = ''/>
        <cfset var RXImportContactToSubcriberList = ''/>

        <cfset dataout.DUPLICATE =  0/>
        <cfset dataout.INVALID =  0/>
        <cfset dataout.ERRORS =  0/>
        <cfset dataout.TOTAL =  0/>
        <cfset dataout.UNIQUE =  0/>
        <cfset var updateUploadStatus = ''/>
        <cfset var tickBegin = 0 />
        <cfset var tickEnd = 0 />
        <cfset var loopTime = 0 />
        <cfset var retValUploadFile = '' />
        <cfset var rxUpdateUploadStatus = '' />

        <cfif arguments.inptProcessType EQ PROCESSTYPE_SUBSCRIBER_LIST>
            <cfset arguments.inpSendToQueue = 1/>
        </cfif>

        <cfset var newName =  arguments.inpNewName/>
        <cfset var orgName =  arguments.inpOrgFileName/>
        <cftry>

            <!---
            <cfif NOT DirectoryExists(uploadPath)>
                <cfdirectory action = "create" directory="#uploadPath#" mode="770" />
            </cfif>

            <!--- Upload file to server --->
            <cffile action="upload" fileField="file" destination="#uploadPath#" accept="text/csv,application/vnd.ms-excel,application/csv,text/plain" nameconflict="MakeUnique" result="uploadResult" mode="770">
            <cfif !arrayContains(_ACCEPTEDCSVTYPE, lCase(uploadResult.clientfileext))>
                <!--- <cffile action="delete" file="#uploadResult.serverdirectory#/#uploadResult.serverfile#" /> --->
                <cfthrow message="File type not accepted!" detail="File type not accepted!" />
            </cfif>
            <cfdump var="#uploadResult#"/>
            <!--- Check filesize, client already handle this but check again for security --->
            <cfif uploadResult.filesize GT _CSVMAXSIZE>
                <cffile action="delete" file="#uploadResult.serverdirectory#/#uploadResult.serverfile#" />
                <cfthrow message="This file is too large! Max file size is 300MB" />
            </cfif>

            <!--- <cfdump var="#uploadResult.serverdirectory#/contact-string-rwerwerwer.#lCase(uploadResult.clientfileext)#" abort="true"> --->
            <cfset var newName = "contact-string#arguments.inpUserId#-#DateFormat(NOW(), "mmm-dd-yyyy")#-#TimeFormat(now(), "HH-mm-ss")#.#lCase(uploadResult.clientfileext)#" />
            <cffile action="rename" source="#uploadResult.serverdirectory#/#uploadResult.serverfile#" destination="#newName#" attributes="normal" >

            <cfexecute name="mac2unix"
                arguments="#uploadPath#/#newName#"
                timeout="10">
            </cfexecute>
            --->

            <cfset tickBegin = GetTickCount()>

            <cfset processType = arguments.inptProcessType>

            <cfif processType EQ 1>
            <cfelseif arguments.inpCampaignID GT 2>
                <cfset arguments.inpSubcriberListID = 0/>
            </cfif>

            <cfif NOT findNoCase('ContactString_vch', arguments.inpColumnNames)>
                <cfthrow message="Column names much contain Phone Number" detail="Column names much contain Phone Number" />
            </cfif>

            <!--- <cfif arrayLen(REMatchNoCase('[^,a-zA-Z0-9_]|[,_]$', arguments.inpColumnNames)) GT 0>
                <cfthrow message="Column names much not contain spaces or any special characters except underscore(_)." detail="Column name much not contain spaces or any special characters except underscore (_)." />
            </cfif> --->


            <!--- LOAD DATA TO TABLE --->
            <cfinvoke component="session.sire.models.cfc.user-import-contact" method="ProcessUpload" returnvariable="RXProcessUpload">
                <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                <cfinvokeargument name="inpSourceFileName" value="#newName#">
                <cfinvokeargument name="inpProcessType" value="#processType#">
                <cfinvokeargument name="inpCampaignID" value="#arguments.inpCampaignID#">
                <cfinvokeargument name="inpSubcriberListID" value="#arguments.inpSubcriberListID#">
                <cfinvokeargument name="inpImportID" value="#arguments.inpImportID#">

                <cfinvokeargument name="inpColumnNumber" value="#arguments.inpColumnNumber#"/>
                <cfinvokeargument name="inpColumnNames" value="#arguments.inpColumnNames#"/>
                <cfinvokeargument name="inpContactStringCol" value="#arguments.inpContactStringCol#"/>
                <cfinvokeargument name="inpSeparator" value="#arguments.inpSeparator#"/>
                <cfinvokeargument name="inpSkipLine" value="#arguments.inpSkipLine#"/>
                <cfinvokeargument name="inpErrorsIgnore" value="#arguments.inpErrorsIgnore#"/>

                <cfinvokeargument name="inpSendToQueue" value="#arguments.inpSendToQueue#"/>
            </cfinvoke>

            <cfset uploadPKID = RXProcessUpload.UPLOADPKID/>
            <cfif RXProcessUpload.RXRESULTCODE NEQ 1>

                <cfquery name="updateUploadStatus" datasource="#SESSION.DBSourceEBM#" result="rxUpdateUploadStatus">
                    UPDATE
                        simplexuploadstage.uploads
                    SET
                        Status_ti = -99,
                        ErrorDetails_vch = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#RXProcessUpload.MESSAGE#"/>
                    WHERE
                        PKId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#uploadPKID#"/>
                </cfquery>

                <cfthrow MESSAGE="#RXProcessUpload.MESSAGE#" TYPE="Any" detail="#RXProcessUpload.ERRMESSAGE#" errorcode="-2">
            </cfif>

            <cfquery name="updateUploadStatus" datasource="#SESSION.DBSourceEBM#">
                UPDATE simplexuploadstage.uploads
                SET Status_ti = 1,
                OriginFileName_vch =  <cfqueryparam cfsqltype="cf_sql_varchar" value="#orgName#"/>
                WHERE PKId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#uploadPKID#"/>
            </cfquery>

            <cfset dataout.RXRESULTCODE = 1 />

            <cfcatch type="any">
                <cfset dataout.MESSAGE = cfcatch.DETAIL />
                <cfset dataout.ERRMESSAGE = cfcatch.MESSAGE />
                <cfset dataout.TYPE = cfcatch.TYPE />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="IndividualImprtSummary" access="remote" output="false" hint="Get Upload Data">
		<cfargument name="inpUserID" TYPE="string" required="no" default="#Session.UserId#" />
        <cfargument name="inpIsGetAll" TYPE="string" required="no" default="0" />
        <cfargument name="customFilter" TYPE="string" required="no" default=""  />

		<cfargument name="iSortCol_0" default="-1"/>
		<cfargument name="sSortDir_0" default=""/>
		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout["aaData"] = ArrayNew(1)/>
		<cfset dataout["iTotalRecords"] = 0/>
		<cfset dataout["iTotalDisplayRecords"] = 0/>
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />

		<cfset var IndividualImprtSummary = '' />
        <cfset var IndividualImprtDetail = '' />

        <cfset var item = '' />
        <cfset var filterItem = '' />
        <cfset var rsCount = '' />

        <cfset var IMPORTED=0>
        <cfset var FAILED=0>
        <cfset var DNC=0>


		<cfset var orderField = '' />
		<cfswitch expression="#arguments.iSortCol_0#">
		    <cfcase value="0">
		        <cfset orderField = 'data.PKId_bi' />
		    </cfcase>
		</cfswitch>

		<cftry>

			<cfquery name="IndividualImprtSummary" datasource="#Session.DBSourceREAD#">
				SELECT
					SQL_CALC_FOUND_ROWS
					data.PKId_bi,
                    data.UserId_int,
                    data.SubscriberListId_int,
                    data.Status_ti,
                    data.ListContactString_vch,
                    DATE_FORMAT(data.ImportDate_dt, '%m-%d-%Y %r') AS ImportDate_dt,
                    data.AdminActionId_int,
                    DATE_FORMAT(data.AdminActionDate_dt, '%m-%d-%Y %r') AS AdminActionDate_dt,
                    CONCAT(SUBSTRING(ua.FirstName_vch, 1, 1), ".", ua.LastName_vch) AS NameAdminActionBy,
                    subsc.GroupName_vch as SubscriberList

				FROM
					simplequeue.import_individual_quee data
                LEFT JOIN
                    simplelists.grouplist subsc ON data.SubscriberListId_int=subsc.GroupId_bi
                LEFT JOIN
                    simpleobjects.useraccount AS ua ON data.AdminActionId_int = ua.UserId_int
                 LEFT JOIN
                    simpleobjects.useraccount AS uimport ON data.UserId_int = uimport.UserId_int
                WHERE
                1
                <cfif customFilter NEQ "">
					<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
						<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
						<cfelse>
						AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
						</cfif>
					</cfloop>
				</cfif>
                <cfif arguments.inpIsGetAll EQ 0>
                    AND
                    data.UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#"/>
                </cfif>

				<cfif orderField NEQ "">
					ORDER BY
						#orderField# #arguments.sSortDir_0#
				<cfelse>
					ORDER BY
						data.PKId_bi DESC
				</cfif>

				LIMIT
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#"/>,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"/>
			</cfquery>

			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			<cfif IndividualImprtSummary.RecordCount GT 0>
				<cfloop query="IndividualImprtSummary">

                    <cfquery name="IndividualImprtDetail" datasource="#Session.DBSourceREAD#">
                        SELECT
                            Status_ti, count(Status_ti) as Total
                        FROM
                            simplelists.import_individual_detail
                        WHERE
                            ImportId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#PKId_bi#"/>
                        GROUP BY Status_ti,ContactString_vch
                    </cfquery>
                    <cfset IMPORTED=0>
                    <cfset FAILED=0>
                    <cfset DNC=0>
                    <cfloop query="IndividualImprtDetail">
                        <cfif Status_ti EQ 1>
                            <cfset IMPORTED ++>
                        <cfelseif Status_ti EQ 2>
                            <cfset FAILED ++>
                        <cfelseif Status_ti EQ 3>
                            <cfset DNC++>
                        <cfelse>

                        </cfif>
                    </cfloop>

					<cfset item = {
						ID = PKId_bi,
                        UserID= UserId_int,
                        SubscriberListId=SubscriberListId_int,
                        SubscriberList=SubscriberList,
                        Status=Status_ti,
                        ListContactString=ListContactString_vch,
                        TOTALCONTACT=listlen(ListContactString_vch),
                        ImportDate=ImportDate_dt,
                        AdminActionId=AdminActionId_int,
                        NameAdminActionBy=NameAdminActionBy,
                        AdminActionDate=AdminActionDate_dt ,
                        IMPORTED=IMPORTED,
                        FAILED=FAILED,
                        DNC=DNC
					} />
					<cfset arrayAppend(dataout["aaData"],item) />
				</cfloop>

				<cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords>
				<cfset dataout["iTotalRecords"] = rsCount.iTotalRecords>
			</cfif>


			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
    <cffunction name="IndividualImprtToQuee" access="remote" hint="Read and Import data to subcriber or contact queue">
        <cfargument name="inpListNumber" type="string" required="true" >
        <cfargument name="inpSubcriberListID" type="numeric" required="true">
        <cfargument name="inpUserId" required="false" default="#session.UserID#">

        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.IMPORTSTATUS = '' />
        <cfset var IndividualImprtToQuee=''>
        <cfset var CheckUserPermission=''>
        <cfset var statusSet=''>
        <cftry>
            <cfset statusSet=0> <!--- default is 0 : wait for approval--->
            <cfinvoke component="session.sire.models.cfc.user-import-contact" method="GetUserMaxImportNumber" returnvariable="CheckUserPermission">
                <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
            </cfinvoke>
            <cfif listfind("3",CheckUserPermission.UPLOADPERMISSION) GT 0>
                <cfset statusSet=INDIVIDUAL_IMPORT_READY>
            </cfif>
            <cfquery name="IndividualImprtToQuee" datasource="#SESSION.DBSourceEBM#">
                INSERT INTO  simplequeue.import_individual_quee
                (
                    PKId_bi,
                    UserId_int,
                    SubscriberListId_int,
                    Status_ti,
                    ListContactString_vch,
                    ImportDate_dt
                )
                VALUES
                (
                    NULL,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Trim(arguments.inpUserId)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpSubcriberListID#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#statusSet#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpListNumber#"> ,
                    NOW()
                )
            </cfquery>
            <cfset dataout.IMPORTSTATUS = statusSet/>
            <cfset dataout.RXRESULTCODE = 1 />
            <cfcatch type="any">
                <cfset dataout.MESSAGE = cfcatch.DETAIL />
                <cfset dataout.ERRMESSAGE = cfcatch.MESSAGE />
                <cfset dataout.TYPE = cfcatch.TYPE />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>


    <cffunction name="ImportListContactToSubcriberList" access="remote" hint="Read and Import data to subcriber or contact queue">
        <cfargument name="inpListNumber" type="string" required="true" >
        <cfargument name="inpSubcriberListID" type="numeric">
        <cfargument name="inpUserId" required="false" default="#session.UserID#">
        <cfargument name="inpImportId" type="numeric">

        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.LIST = arrayNew(1) />
        <cfset var dataRx = {}/>
        <cfset dataRx.NUM =''/>
        <cfset dataRx.MSG =''/>
        <cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>

        <cfset var getData = {} />
        <cfset var txtLog = ''/>
        <cfset var contactString = ''/>
        <cfset var shortCode = ''/>

        <cfset var InsertLogDetail = ''/>
        <cfset var StatusForImport_individual_detail = ''/> <!--- 1 = complete,	2 = fail,	3=  block dnc--->



        <cfset var number = ''/>
        <cfset var checkUsPhone = ''/>
        <cfset var rxAddContactStringToList = ''/>
        <cfset arguments.inpListNumber =ListToArray(arguments.inpListNumber)>

        <cftry>
            <!--- <cfif arguments.inpListNumber NEQ ''>
                <cfset arguments.inpListNumber = listToArray(arguments.inpListNumber)>
            </cfif> --->
            <cfif arrayLen(arguments.inpListNumber) GT 0>

                <cfloop array="#arguments.inpListNumber#" index="number">
                    <cfset dataRx = {}/>
                    <cfset number = REReplace(number,"[^0-9]","","all")/>
                    <cfset contactString = number />
                    <cfset dataRx.NUM = contactString/>

                    <cfinvoke method="validUsPhoneNumber" returnvariable="checkUsPhone" component="session.sire.models.cfc.import-contact">
                        <cfinvokeargument name="inpContactString" value="#contactString#">
                    </cfinvoke>

                    <cfif checkUsPhone>

                        <cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode">
							<cfinvokeargument name="inpUserIDRequest" value="#arguments.inpUserId#"/>
						</cfinvoke>
                        <cfinvoke component="session.sire.models.cfc.import-contact" method="AddContactStringToList" returnvariable="rxAddContactStringToList">
                            <cfinvokeargument name="INPCONTACTSTRING" value="#contactString#" />
                            <cfinvokeargument name="INPCONTACTTYPEID" value="3" />
                            <cfinvokeargument name="INPGROUPID" value="#arguments.inpSubcriberListID#" />
                            <cfinvokeargument name="INPUSERID" value="#arguments.inpUserId#" />
                            <!--- <cfinvokeargument name="INP_SOURCEKEY" value="UserManualImport" /> --->
                            <cfinvokeargument name="inpShortCode" value="#shortCode.SHORTCODE#"/>
                        </cfinvoke>

                        <cfif rxAddContactStringToList.RXRESULTCODE EQ 1>
                            <cfset dataRx.MSG ='Import Success'/>
                            <cfset  StatusForImport_individual_detail = 1/>
                        <cfelse>
                            <cfset dataRx.MSG = rxAddContactStringToList.MESSAGE&'.'&rxAddContactStringToList.ERRMESSAGE/>
                            <cfset  StatusForImport_individual_detail = 3/>
                        </cfif>
                    <cfelse>
                        <cfset dataRx.MSG = " is not a US phone!"/>
                        <cfset  StatusForImport_individual_detail = 2/>
                    </cfif>
                    <cfset arrayAppend(dataout.LIST, dataRx) />
                    <!--- insert log detail--->
                    <cfquery name="InsertLogDetail" datasource="#SESSION.DBSourceEBM#">
                        INSERT INTO  simplelists.import_individual_detail
                        (
                            PKId_bi,
                            ImportId_int,
                            UserId_int,
                            SubscriberListId_int,
                            Status_ti,
                            ContactString_vch,
                            ImportDate_dt
                        )
                        VALUES
                        (
                            NULL,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpImportId#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Trim(arguments.inpUserId)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpSubcriberListID#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#StatusForImport_individual_detail#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#contactString#"> ,
                            NOW()
                        )
                    </cfquery>
                </cfloop>
                <cfset dataout["iTotalRecords"] = arrayLen(arguments.inpListNumber)>
		        <cfset dataout["iTotalDisplayRecords"] = arrayLen(arguments.inpListNumber)>
                <cfset txtLog = "Sub:"&inpSubcriberListID&" List:"&serializeJSON(inpListNumber)/>
                <cfinvoke method="createUserLog" component="#LocalSessionDotPath#.cfc.administrator.usersLogs">
                    <cfinvokeargument name="userId" value="#session.UserID#">
                    <cfinvokeargument name="moduleName" value="Manual Import ContactString">
                    <cfinvokeargument name="operator" value="#TRIM(LEFT(txtLog,255))#">
                </cfinvoke>

                <cfset dataout.RXRESULTCODE = 1 />

            </cfif>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = cfcatch.DETAIL />
                <cfset dataout.ERRMESSAGE = cfcatch.MESSAGE />
                <cfset dataout.TYPE = cfcatch.TYPE />
            </cfcatch>
        </cftry>
        <cfreturn dataout/>
    </cffunction>


    <cffunction name="GetContactListByCDF" access="remote" output="true" hint="get list contact by CDF">
        <cfargument name="CDFData" required="no">
        <cfargument name="INPGROUPID" required="yes" >
        <cftry>
            <cfset var dataOut = {}>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

            <cfset var cdfLoopIndex = 1>
            <cfset var GetContactListByCDF = "">
            <cfset var cdfArray = CDFData>

            <cfset var cdfItem = ''/>
            <cfset var cdfRowItem = ''/>

            <!---get contact id by cdf --->
            <!---CDF is an array of structs which looks like [
                                                                {
                                                                    "CDFID":"12",
                                                                    "ROWS":[{"OPERATOR":"=","VALUE":"abc"},{"OPERATOR":"<>","VALUE":"abc"}]
                                                                },
                                                                {
                                                                    "CDFID":"13",
                                                                    "ROWS":[{"OPERATOR":"<>","VALUE":"def"},{"OPERATOR":"=","VALUE":"345"}]
                                                                }
                                                            ] --->
            <!---CDFID is used to query cdfId_int field in simplelists.contactvariable--->
            <!---ROWS is array of OPERATOR and VALUE --->
            <!---OPERATOR is operator used in query, it could be '=' (is), '<>'(is not), 'like'(is similar to) --->
            <!---VALUE is value of VariableValue_vch in simplelists.contactvariable--->
            <!---presume we have an array CDF with 2 elements --->
            <!---so we need to query contacts which match all condition element --->
            <!---that means the desired contacts must have exactly 2(2 is length of cdf array) records in simplelists.contactvariable,
                one has cdfid_int = 12 (12 comes from CDFID) and   VariableValue_vch    '='(equal sign comes from OPERATOR)    '123' (123 comes from VALUE) AND  VariableValue_vch    '<>'('<>' comes from OPERATOR)    'abc' (abc comes from VALUE)
                and the left record has cdfid_int = 13 (13 comes from CDFID) and  VariableValue_vch    '<>'('<>' comes from OPERATOR)    'def' (def comes from VALUE) AND   VariableValue_vch    '='(equal sign comes from OPERATOR)    '345' (345 comes from VALUE)
             --->
            <!---below query should be used when we need to make a complex query --->
            <!---we can use return list contact ids to pass into other query as a list param--->
            <cfquery name="GetContactListByCDF" datasource="#Session.DBSourceEBM#">
                SELECT
                    CDF1.ContactId_bi,CDF1.ContactVariableId_bi,CDF1.CdfId_int,VariableValue_vch
                FROM
                    (
                        SELECT cs.ContactId_bi,cv.ContactVariableId_bi,cv.CdfId_int,cv.VariableValue_vch
                        FROM
                            simplelists.grouplist gl
                        inner join
                            simplelists.groupcontactlist gcl
                        on
                            gl.GroupId_bi = gcl.GroupId_bi

                        <cfif INPGROUPID NEQ "" AND INPGROUPID NEQ "0" >
                            AND
                                gl.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">
                        </cfif>
                        and gl.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                        inner join simplelists.contactstring cs
                        on
                            gcl.ContactAddressId_bi = cs.ContactAddressId_bi

                        left join
                        simplelists.contactvariable cv
                            on cs.ContactId_bi = cv.ContactId_bi

                        GROUP BY ContactId_bi, CdfId_int

                        <cfif arraylen(cdfArray) GT 0 >
                        HAVING
                        (
                            <cfloop array="#cdfArray#" index="cdfItem">
                                <cfif cdfLoopIndex  GT 1>
                                     Or
                                </cfif>
                                 (cv.CdfId_int =  <CFQUERYPARAM CFSQLTYPE="cf_sql_integer"  VALUE="#cdfItem.CDFID#">
                                  <cfloop array = "#cdfItem.ROWS#" index="cdfRowItem">
                                      And cv.VariableValue_vch
                                    <cfif lcase(cdfRowItem.OPERATOR) eq 'like'>
                                         #cdfRowItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="%#cdfRowItem.VALUE#%">
                                    <cfelse>
                                         #cdfRowItem.OPERATOR# <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#cdfRowItem.VALUE#">
                                    </cfif>
                                  </cfloop>
                                 )
                                 <cfset cdfLoopIndex++>
                            </cfloop>
                        )
                        </cfif>
                    ) AS CDF1
                 GROUP BY CDF1.ContactId_bi
                 <cfif  arraylen(cdfArray) GT 0>
                 having count(CDF1.ContactVariableId_bi) = <cfqueryparam cfsqltype="cf_sql_integer" value="#arraylen(cdfArray)#">
                 </cfif>
            </cfquery>

            <cfset var contactListByCdf = valuelist(GetContactListByCDF.ContactId_bi)>
            <cfset dataout.CONTACTLISTBYCDF = contactListByCdf NEQ ""? contactListByCdf : "-1"/>
            <cfset dataout.RXRESULTCODE = 1 />
        <cfcatch type="Any" >
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>

    <cffunction name="UpdateAdminIndividualAction" access="remote" output="true" hint="update admin action">
        <cfargument name="inpUploadId" TYPE="string" required="yes" default="" />
        <cfargument name="inpActionId" TYPE="string" required="yes" default="" hint="10: approval, 20 reject" />

        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset var UpdateAdminIndividualAction=''>
        <cfset var StatusUpdate=''>
        <cftry>
            <cfif arguments.inpActionId EQ IMPORT_ADM_ACTION_TYPE_APPROVED>
                <cfset StatusUpdate=INDIVIDUAL_IMPORT_READY>
            <cfelseif arguments.inpActionId EQ IMPORT_ADM_ACTION_TYPE_REJECTED>
                <cfset StatusUpdate=INDIVIDUAL_IMPORT_REJECTED>
            </cfif>
             <cfquery name="UpdateAdminIndividualAction" datasource="#Session.DBSourceEBM#">
                UPDATE
                    simplequeue.import_individual_quee
                SET
                    AdminActionDate_dt= NOW(),
                    AdminActionType_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpActionId#">,
                    AdminActionId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.userID#">,
                    Status_ti=#StatusUpdate#
                WHERE
                    PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpUploadId#">
            </cfquery>

            <cfset dataout.RXRESULTCODE=1>

            <cfcatch type="any">
                <cfset dataout.RXRESULTCODE = "#cfcatch.ERRORCODE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout>
    </cffunction>
    <cffunction name="UpdateAdminAction" access="remote" output="true" hint="update admin action">
        <cfargument name="inpUploadId" TYPE="string" required="yes" default="" />
        <cfargument name="inpActionId" TYPE="string" required="yes" default="" hint="10: approval, 20 reject" />

        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset var UpdateAdminAction=''>
        <cfset var StatusUpdate=''>
        <cftry>
            <cfif arguments.inpActionId EQ UPLOADS_ADM_ACTION_APPROVED>
                <cfset StatusUpdate=UPLOADS_READY>
            <cfelseif arguments.inpActionId EQ UPLOADS_ADM_ACTION_REJECTED>
                <cfset StatusUpdate=UPLOADS_REJECT_BY_ADM>
            </cfif>
             <cfquery name="UpdateAdminAction" datasource="#Session.DBSourceEBM#">
                UPDATE
                    simplexuploadstage.uploads
                SET
                    AdminActionDate_dt= NOW(),
                    AdminActionType_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpActionId#">,
                    AdminActionId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.userID#">,
                    Status_ti=#StatusUpdate#
                WHERE
                    PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpUploadId#">
            </cfquery>

            <cfset dataout.RXRESULTCODE=1>

            <cfcatch type="any">
                <cfset dataout.RXRESULTCODE = "#cfcatch.ERRORCODE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout>
    </cffunction>
    <cffunction name="UpdateUserMaxImportNumber" access="remote" output="true" hint="update user max import number">
        <cfargument name="inpUserId" TYPE="string" required="yes" default="" />
        <cfargument name="inpLimitUploadContact" TYPE="string" required="yes" default="0" />
        <cfargument name="inpPermissionUpload" TYPE="string" required="no" default="1" hint="1: not allow upload, 2: allow but need approval, 3: allow without approval" />
        <!--- this function always return success, if can not find user max import number -> return default 5000 --->
        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset var UpdateUserMaxImportNumber=''>

        <cftry>
             <cfquery name="UpdateUserMaxImportNumber" datasource="#Session.DBSourceEBM#">
                UPDATE
                    simpleobjects.useraccount
                SET
                    MaxImportNumber_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpLimitUploadContact#">,
                    UploadPermission_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPermissionUpload#">
                WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpUserId#">
            </cfquery>

            <cfset dataout.RXRESULTCODE=1>

            <cfcatch type="any">
                <cfset dataout.RXRESULTCODE = "#cfcatch.ERRORCODE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout>
    </cffunction>
    <cffunction name="GetUserMaxImportNumber" access="remote" output="true" hint="get user max import number">
        <cfargument name="inpUserId" TYPE="string" required="no" default="#session.UserId#" />
        <!--- this function always return success, if can not find user max import number -> return default 5000 --->
        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = 1 />
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.MAXLIMIT = 5000/>
        <cfset dataout.UPLOADPERMISSION = 0/>
        <cfset var getUserMaxImportNumber=''>
        <cftry>
             <cfquery name="getUserMaxImportNumber" datasource="#Session.DBSourceEBM#">
                SELECT
                    MaxImportNumber_int,
                    UploadPermission_int
                FROM
                    simpleobjects.useraccount
                WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpUserId#">
            </cfquery>

            <cfif getUserMaxImportNumber.RecordCount GT 0>
                <cfset dataout.MAXLIMIT = getUserMaxImportNumber.MaxImportNumber_int>
                <cfset dataout.UPLOADPERMISSION = getUserMaxImportNumber.UploadPermission_int/>
            </cfif>

            <cfcatch type="any">
                <cfset dataout.RXRESULTCODE = "#cfcatch.ERRORCODE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
            </cfcatch>
        </cftry>

        <cfreturn dataout>
    </cffunction>

    <cffunction name="GetUserCDF" access="remote" output="false" hint="Get CDF User">
        <cfargument name="inpUserId" TYPE="string" required="no" default="#session.UserId#" />

        <cfset var dataout = StructNew()/>
        <cfset dataout["listData"] = ArrayNew(1)>
        <cfset dataout.RXRESULTCODE = 0 />
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.TYPE = '' />

        <cfset var GetListUserCDF="">
        <cfset var data="">



        <cftry>
            <cfquery name="GetListUserCDF" datasource="#Session.DBSourceEBM#">
                SELECT
                    CdfId_int,
                    CdfName_vch,
                    Created_dt,
                    type_field
                FROM
                    simplelists.customdefinedfields
                WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                ORDER BY
                    CdfName_vch
            </cfquery>

            <cfset data = [
                'ContactString_vch',
                'PhoneNumber',
                NOw(),
                ''
            ]>
            <cfset arrayappend(dataout["listData"],data)>
            <cfloop query="GetListUserCDF">
                <cfset data = [
                    CdfId_int,
                    CdfName_vch,
                    Created_dt,
                    type_field
                ]>
                <cfset arrayappend(dataout["listData"],data)>
            </cfloop>

            <cfset dataout.RXRESULTCODE = 1 />
            <cfcatch>
                <cfset dataout = {}>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout>

    </cffunction>
    <cffunction name="DeleteCDF" access="remote" output="false" hint="delete CDF">
		<cfargument name="inpCDFId" TYPE="numeric" required="yes" default="0"/>


		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />

		<cfset var DeleteCDF = ""/>
		<cftry>

            <cfquery name="DeleteCDF" datasource="#Session.DBSourceEBM#">
                DELETE FROM simplelists.customdefinedfields
                WHERE   CdfId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCDFId#">
            </cfquery>


            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.MESSAGE = "Delete Successfully"/>

			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
    <cffunction name="InsertUpdateCDF" access="remote" output="false" hint="insert new CDF">
		<cfargument name="inpFieldType" TYPE="numeric" required="yes" default="0"/>
		<cfargument name="inpFieldName" TYPE="string" required="yes" default="" />


		<cfset var dataout = StructNew()/>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.TYPE = '' />

        <cfset dataout.ID=''>

        <cfset var CheckExistCDF = ""/>
		<cfset var InsertNewCDF = ""/>
        <cfset var resultInsertNewCDF="">
		<cftry>

            <cfquery name="CheckExistCDF" datasource="#Session.DBSourceEBM#">
                SELECT
                    CdfId_int,
                    CdfName_vch
                FROM
                    simplelists.customdefinedfields
                WHERE
                    UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
                AND
                    CdfName_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpFieldName)#">
            </cfquery>
            <cfif CheckExistCDF.RECORDCOUNT GT 0>
                <cfset dataout.ID=CheckExistCDF.CdfId_int >
                <cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.MESSAGE = "This custom field already exists, please enter another name"/>
                <cfreturn dataout>
            </cfif>
            <cfquery name="InsertNewCDF" datasource="#Session.DBSourceEBM#" result="resultInsertNewCDF">
                INSERT INTO simplelists.customdefinedfields
                (
                    CdfId_int,
                    CdfName_vch,
                    UserId_int,
                    CdfDefaultValue_vch,
                    Created_dt,
                    type_field
                )
                VALUES
                (
                    NULL,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpFieldName)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">,

                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Trim(arguments.inpFieldName)#">,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpFieldType#">
                )
            </cfquery>

            <cfset dataout.ID=resultInsertNewCDF.GENERATED_KEY >
            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.MESSAGE = "Insert Successfully"/>


			<cfcatch>
				<cfset dataout = {}>
			    <cfset dataout.RXRESULTCODE = -1 />
	   		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
	   		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	   		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>

	</cffunction>
</cfcomponent>
