<cfcomponent>

	<cfinclude template="/public/paths.cfm" >
    <cfinclude template="/public/sire/configs/paths.cfm" >    
    <cfinclude template="../../configs/credits.cfm">

    <cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.loggedIn" default="0"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
            
    <cfset LOCAL_LOCALE = "English (US)">

    <cffunction name="GetUserList" access="remote" output="true" hint="get user list with filter">
        <cfargument name="inpEmailAddress" TYPE="string" required="yes" default="" />
        

        <cfset var dataout = {} />
        <cfset var GetUserList = '' />
        <cfset var rsGetUserList = '' />
        <cfset var displayInfo = '' />
            
        <cfset dataout.RXRESULTCODE = 1 />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        
        <cfset dataout.UserList = arrayNew() />

        <cftry>
            <cfquery datasource="#Session.DBSourceEBM#" name="GetUserList" result="rsGetUserList">
                    SELECT 
                        u.UserId_int,
                        CONCAT(u.FirstName_vch ," ", u.LastName_vch) AS Full_Name,
                        u.EmailAddress_vch,
                        u.MFAContactString_vch,
                        o.OrganizationName_vch
                    FROM 
                        simpleobjects.useraccount u
                    LEFT JOIN
                        simpleobjects.organization o
                    ON 
                        o.UserId_int = u.UserId_int   
                    WHERE
                        (
                            u.EmailAddress_vch LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.inpEmailAddress#%"> 
                            OR 
                            u.UserId_int = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpEmailAddress#">
                        )
                    AND u.Active_int = 1    
            </cfquery>
         
            <cfif GetUserList.RECORDCOUNT GT 0>
               
                <cfloop query="GetUserList" >
                    <cfset displayInfo = '#UserId_int# - #EmailAddress_vch#' > 
                    <!--- <cfif OrganizationName_vch NEQ ''>
                        <cfset displayInfo&=' - #OrganizationName_vch#'>
                    </cfif> --->
                    <cfset var tmpUser = {
                        'ID':'#UserId_int#',
                        'NAME':'#Full_Name#',
                        'EMAIL':'#displayInfo#',
                        'ORG_NAME' : "#OrganizationName_vch#"
                    } />
                    <cfset ArrayAppend(dataout.UserList, tmpUser) />
                </cfloop>
            </cfif>

            <cfcatch>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>    

        <cfreturn dataout.UserList>
    </cffunction>

    <cffunction name="GetUserBalance" access="remote" hint="Get user billing info">
        <cfargument name="inpUserId" required="true" type="numeric"/>

        <cfset var dataout = {} />
        <cfset var GetUserBalance = '' />
        <cfset var GetUserPlan = '' />

        <cfset dataout.RXRESULTCODE = -1/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.USERBALANCE = {} />
        <cfset dataout.USERBALANCE.PLANCREDIT = 0/>
        <cfset dataout.USERBALANCE.BUYCREDIT = 0/>
        <cfset dataout.USERBALANCE.PROMOTIONCREDIT = 0/>
        <cfset dataout.PRICE = 0/>
        <cfset dataout.ERRMESSAGE = '' />

        <cftry>
            <cfinvoke method="GetBalance" component="session.sire.models.cfc.billing" returnvariable="GetUserBalance">
                <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
            </cfinvoke>

            <cfinvoke method="GetUserPlan" component="session.sire.models.cfc.order_plan" returnvariable="GetUserPlan">
                <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
            </cfinvoke>

            <cfif GetUserBalance.RXRESULTCODE GT 0 AND GetUserPlan.RXRESULTCODE GT 0>
                <cfset dataout.RXRESULTCODE = 1/>
                <cfset dataout.MESSAGE = "Get user balance info successfully"/>
                <cfset dataout.PLANCREDIT = GetUserBalance.PLANCREDITBALANCE/>
                <cfset dataout.BUYCREDIT = GetUserBalance.BUYCREDITBALANCE/>
                <cfset dataout.PROMOTIONCREDIT = GetUserBalance.PROMOTIONCREDITBALANCE/>
                <cfset dataout.PRICE = (GetUserPlan.PRICEMSGAFTER/100)/>
            <cfelse>
                <cfset dataout.MESSAGE = "Get user balance info failed"/>
            </cfif>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn serializeJSON(dataout)/>
    </cffunction>

    <cffunction name="MarkTestAccount" access="remote" output="false" hint="Mark account as test account, test account will not show up at admin dashboard">
        <cfargument name="inpUserId" required="true" type="numeric" hint="User id"/>

        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = ''/>
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />

        <cfset var UpdateUser = '' />

        <cftry>
            <cfquery datasource="#Session.DBSourceEBM#" result="UpdateUser">
                UPDATE
                    simpleobjects.useraccount
                SET
                    IsTestAccount_ti = 1
                WHERE
                    UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#"/>
            </cfquery>

            <cfif UpdateUser.RECORDCOUNT GT 0>
                <cfset dataout.MESSAGE = "Update user successfully!"/>
                <cfset dataout.RXRESULTCODE = 1/>
            <cfelse>
                <cfset dataout.MESSAGE = "Update user failed!"/>
            </cfif>
            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn serializeJSON(dataout)/>
    </cffunction>

    <cffunction name="UnMarkTestAccount" access="remote" output="false" hint="Mark account as test account, test account will not show up at admin dashboard">
        <cfargument name="inpUserId" required="true" type="numeric" hint="User id"/>

        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = ''/>
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />

        <cfset var UpdateUser = '' />

        <cftry>
            <cfquery datasource="#Session.DBSourceEBM#" result="UpdateUser">
                UPDATE
                    simpleobjects.useraccount
                SET
                    IsTestAccount_ti = 0
                WHERE
                    UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#"/>
            </cfquery>

            <cfif UpdateUser.RECORDCOUNT GT 0>
                <cfset dataout.MESSAGE = "Update user successfully!"/>
                <cfset dataout.RXRESULTCODE = 1/>
            <cfelse>
                <cfset dataout.MESSAGE = "Update user failed!"/>
            </cfif>
            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>

        <cfreturn serializeJSON(dataout)/>
    </cffunction>

    <cffunction name="getListShortCodes" access="remote" output="true" hint="Get List Of All Available Shortcodes">
        <cfset var dataout = {} />
        <cfset var GetListShortCodes = '' />
        <cfset var resultGetListShortCodes = '' />
        <cfset var displayInfo = '' />

        <cfset dataout.RXRESULTCODE = 1 />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />

        <cfset dataout["LIST_SHORT_CODES"] = ArrayNew(1) />

        <cfif structKeyExists(session, "USERID") && session.USERID GT 0>
            <cftry>
                <cfquery datasource="#Session.DBSourceEBM#" name="GetListShortCodes" result="resultGetListShortCodes">
                SELECT
                    sr.ShortCodeId_int,
                    s.ShortCode_vch
                FROM
                    sms.shortcoderequest sr
                INNER JOIN
                    sms.shortcode s
                ON
                    s.ShortCodeId_int = sr.ShortCodeId_int
                WHERE
                    RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
                    ORDER BY
                    sr.IsDefault_ti DESC
                </cfquery>
                <cfif GetListShortCodes.RECORDCOUNT GT 0>
                    <cfloop query="GetListShortCodes" >
                        <cfset displayInfo = '#ShortCodeId_int# - #ShortCode_vch#' >
                        <cfset var tmpShortCode = {
                            'ID':'#ShortCodeId_int#',
                            'SHORT_CODE':'#ShortCode_vch#'
                        } />
                        <cfset ArrayAppend(dataout["LIST_SHORT_CODES"],tmpShortCode)>
                    </cfloop>
                    <cfset dataout.RXRESULTCODE = 1 />
                    <cfset dataout.MESSAGE = 'Get Keyword successfully!' />
                </cfif>

                <cfcatch>
                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
                </cfcatch>
            </cftry>
        <cfelse>
            <cfset dataout.RXRESULTCODE = -1 >
            <cfset dataout.MESSAGE = 'Your session has timed out. Please sign in again.'>
            <cfset dataout.ERRMESSAGE = 'Your session has timed out. Please sign in again.'>
            <cfreturn dataout>
        </cfif>
        <cfreturn dataout/>
    </cffunction>

    <cffunction name="getDefaultShortCode" access="remote" output="true" hint="Display the output of the shortcode default">
        <cfset var getShortCode = '' />
        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = ''>
        <cfset dataout.ERRMESSAGE = ''>
        <cfset dataout.SHORTCODE = ''>

        <cfif structKeyExists(session, "USERID") && session.USERID GT 0>
            <cftry>
                <cfquery name="getShortCode" datasource="#Session.DBSourceREAD#">
                SELECT
                    sr.ShortCodeId_int,
                    s.ShortCode_vch
                FROM
                    sms.shortcoderequest sr
                INNER JOIN
                    sms.shortcode s
                ON
                    s.ShortCodeId_int = sr.ShortCodeId_int
                WHERE
                    RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
                    AND
                    sr.IsDefault_ti = 1
                    LIMIT 1
                </cfquery>
                <cfif getShortCode.RecordCount GT 0>
                    <cfset dataout.RXRESULTCODE = 1 />
                    <cfset dataout.SHORTCODE = getShortCode.ShortCode_vch>
                    <cfset dataout.SHORTCODEID = getShortCode.ShortCodeId_int>
                <cfelse>
                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.MESSAGE = "No shortcode found." />
                    <cfset dataout.ERRMESSAGE = "" />
                </cfif>
                <cfcatch type="any">
                    <cfset dataout = {} />
                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
                </cfcatch>
            </cftry>
        <cfelse>
            <cfset dataout.RXRESULTCODE = -1 >
            <cfset dataout.MESSAGE = 'Your session has timed out. Please sign in again.'>
            <cfset dataout.ERRMESSAGE = 'Your session has timed out. Please sign in again.'>
            <cfreturn dataout>
        </cfif>
        <cfreturn dataout >
    </cffunction>

    <cffunction name="markShortCodeDefault" access="remote" output="false" hint="which allows him to select default short code.">
        <cfargument name="shortCodeId" required="true" type="numeric" hint="short code id"/>

        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = ''/>
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />

        <cfset var UpdateShortCode = '' />

        <cfif structKeyExists(session, "USERID") && session.USERID GT 0>

        <cfelse>
            <cfset dataout.RXRESULTCODE = -1 >
            <cfset dataout.MESSAGE = 'Your session has timed out. Please sign in again.'>
            <cfset dataout.ERRMESSAGE = 'Your session has timed out. Please sign in again.'>
            <cfreturn dataout>
        </cfif>

        <cftry>
            <cfquery datasource="#Session.DBSourceEBM#" result="UpdateShortCode">
                UPDATE
                    sms.shortcoderequest AS sr
                SET
                    sr.IsDefault_ti =
                    CASE
                        sr.ShortCodeId_int
                    WHEN
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.shortCodeId#"/>
                    THEN
                            1
                    ELSE
                            0
                    END
                WHERE sr.ShortCodeId_int IN
                (
                    SELECT ShortCodeId_int from
                    (

                        SELECT
                            t_sr.ShortCodeId_int
                        FROM
                            sms.shortcoderequest t_sr
                        WHERE
                            t_sr.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">)
                        AS
                            ShortCodeId_int

                    )
                AND sr.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">;
            </cfquery>

            <cfif UpdateShortCode.RECORDCOUNT GT 0>
                <cfset dataout.MESSAGE = "Update ShortCode successfully!"/>
                <cfset dataout.RXRESULTCODE = 1/>
            <cfelse>
                <cfset dataout.MESSAGE = "Update ShortCode failed!"/>
                <cfset dataout.RXRESULTCODE = -1/>
            </cfif>
            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>
        <cfreturn dataout/>
    </cffunction>
    <cffunction name="DoProcessPayment" access="public" hint="Do Payment">
        <cfargument name="inpUserID" type="numeric" required="true"/>   
        <cfargument name="inpNumberSMSToSend" type="numeric" default="0"/>   
        <cfargument name="inpNumberKeywordToSend" type="numeric" default="0"/>   
        <cfargument name="inpAmount" type="numeric" required="true" default="0"/>   


        <cfparam name="form.monthYearSwitch" default="1"/>
        <cfparam name="form.payopt" default=""/>
        <cfparam name="form.inpCustomAmount" default="0"/>
        <cfparam name="form.inpPromotionCode" default=""/>
        <cfparam name="form.select_used_card" default="1"/>
        <cfparam name="form.payment_method" default="0"/>
        <cfparam name="form.number" default=""/>
        <cfparam name="form.cvv" default=""/>
        <cfparam name="form.expirationDate" default=""/>
        <cfparam name="form.firstName" default=""/>
        <cfparam name="form.lastName" default=""/>
        <cfparam name="form.line1" default=""/>
        <cfparam name="form.city" default=""/>
        <cfparam name="form.state" default=""/>
        <cfparam name="form.zip" default=""/>
        <cfparam name="form.country" default="US"/>
        <cfparam name="form.phone" default=""/>
        <cfparam name="form.email" default=""/>
        <cfparam name="form.save_cc_info" default="0"/>                
                
        
        <cfset var dataout = {} />
        
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "Payment Fail!" />
        <cfset dataout.ERRMESSAGE = "Payment Fail!" />
        <cfset dataout.ID = "0">

        <cfset var amount=arguments.inpAmount>
        <cfset var customerId = '' />
        <cfset var paymentMethodId = '' />
        <cfset var paymentTypeShortName = '' />
        <cfset var paymentType = '' />
        <cfset var primaryPaymentMethodId = '' />
        <cfset var txtTotalAddon=''/>
        <cfset var paymentData=''/>
        <cfset var closeBatchesData=''/>
        <cfset var paymentResposeContent=''/>
        <cfset var paymentRespose=''/>
        <cfset var userLogOperator=''/>
        <cfset var getUserPlan=''/>
        <cfset var GetUserAuthen=''/>
        <cfset var RetCustomerInfo=''/>
        <cfset var closeBatchesRequestResult=''/>


        <cfset var numberSMSToSend = arguments.inpNumberSMSToSend />
        <cfset var numberKeywordToSend = arguments.inpNumberKeywordToSend />
        
        
        
        <cftry>
            <cfinvoke method="GetUserPlan" component="session/sire/models/cfc/order_plan" returnvariable="getUserPlan">                
                <cfinvokeargument name="InpUserId" value="#arguments.inpUserID#">
            </cfinvoke>            
            <cfif numberSMSToSend EQ 0 AND numberKeywordToSend GT 0>
                <cfset txtTotalAddon = "Admin (#Session.USERID#) Buy " & numberKeywordToSend & ' Keyword(s) for user (#arguments.inpUserID#) '>
            <cfelseif numberSMSToSend GT 0 AND numberKeywordToSend EQ 0>                  
                <cfset txtTotalAddon = "Admin (#Session.USERID#) Buy " & numberKeywordToSend & ' Credit(s) for user (#arguments.inpUserID#) '>
            <cfelseif numberSMSToSend GT 0 AND numberKeywordToSend GT 0>
                <cfset txtTotalAddon = "Admin (#Session.USERID#) Buy " & numberSMSToSend & ' Credit(s) and ' & numberKeywordToSend & ' Keyword(s) for user (#arguments.inpUserID#) '>
            </cfif>
            <cfif form.select_used_card EQ 1> <!--- USE SAVED TOKEN TO PAYMENT --->
                <cfinvoke component="session.sire.models.cfc.billing" method="getUserAuthenInfo" returnvariable="GetUserAuthen">                    
                    <cfinvokeargument name="userId" value="#arguments.inpUserID#">
                </cfinvoke> 

                <cfif GetUserAuthen.RXRESULTCODE EQ 1 AND structKeyExists(GetUserAuthen.DATA, "PaymentMethodID_vch") AND GetUserAuthen.DATA.PaymentMethodID_vch NEQ ''  >
                    <cfset customerId = #arguments.inpUserID#>
                    <cfset paymentMethodId = #GetUserAuthen.DATA.PaymentMethodID_vch#>
                    <cfset paymentTypeShortName = #GetUserAuthen.DATA.PaymentType_vch#>

                    <cftry>
                        <cfset paymentType = StructFind(worldpayPaymentTypes, paymentTypeShortName)>
                    <cfcatch>
                        <cfset paymentType = worldpayPaymentTypes.UNKNOWN>
                    </cfcatch>  
                    </cftry>

                <cfelse>
                        <cfset dataout.ID="-4">
                        <cfset dataout.MESSAGE="We're Sorry! Unable to get your CreditCard info.Please update.">
                </cfif>

                <cfset paymentData = {
                    amount = amount,
                    transactionDuplicateCheckIndicator = 1,
                    extendedInformation = {
                        notes = txtTotalAddon,
                    },
                    paymentVaultToken = {  
                       customerId = customerId,
                       paymentMethodId = paymentMethodId,
                       paymentType = paymentType
                    },
                    developerApplication = worldpayApplication
                }>
            <cfelse>

                <cfset paymentData = {
                    amount = amount,
                    transactionDuplicateCheckIndicator = 1,
                    extendedInformation = {
                        notes = txtTotalAddon,
                    },
                    developerApplication = {
                        developerId: worldpayApplication.developerId,
                        version: worldpayApplication.version
                  }
                }>

                <cfif form.payment_method EQ 1>
                    <cfset paymentData.check = {
                            routingNumber   = form.number,
                            accountNumber   = form.cvv,
                            firstName       = form.firstName,
                            lastName        = form.lastName,
                            address         = {
                                line1   = form.line1,
                                city    = form.city,
                                state   = form.state,
                                zip     = form.zip,
                                country = form.country,
                                phone   = form.phone
                            },
                            email       = form.email
                        }>
                <cfelse>
                    <cfset paymentData.card = {
                            number          = form.number,
                            cvv             = form.cvv,
                            expirationDate  = form.expirationDate,
                            firstName       = form.firstName,
                            lastName        = form.lastName,
                            address         = {
                                line1   = form.line1,
                                city    = form.city,
                                state   = form.state,
                                zip     = form.zip,
                                country = form.country,
                                phone   = form.phone
                            },
                            email       = form.email
                        }>
                </cfif>
            </cfif>

            <cfset closeBatchesData = {
                            developerApplication = {
                                developerId: worldpayApplication.developerId,
                                version: worldpayApplication.version
                          }
                        }>
                

            <cfhttp url="#payment_url#" method="post" result="paymentRespose" port="443">
                <cfhttpparam type="header" name="content-type" value="application/json">
                <cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
                <cfhttpparam type="body" value='#TRIM( serializeJSON(paymentData) )#'>
            </cfhttp>

            <cfset dataout = {} />
            <cfset dataout.ID = "-10">
            <cfset dataout.MESSAGE = "We're Sorry! Unable to complete your transaction at this time. Transaction Failed.">

            <!--- --->
            <cfif paymentRespose.status_code EQ 200>
                <cfif trim(paymentRespose.filecontent) NEQ "" AND isJson(paymentRespose.filecontent)>

                    <cfset paymentResposeContent = deserializeJSON(paymentRespose.filecontent)>
                        
                        <cfif paymentResposeContent.success>

                            <cfset dataout.ID = "1">
                            <cfset dataout.MESSAGE = "Transaction Complete. Thank you!">
                            
                            <!--- close session--->
                            <cfhttp url="#close_batches_url#" method="post" result="closeBatchesRequestResult" port="443">
                            <cfhttpparam type="header" name="content-type" value="application/json">
                            <cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
                            <cfhttpparam type="body" value='#TRIM( serializeJSON(closeBatchesData) )#'>
                            </cfhttp>
                           <!--- Sendmail payment --->
                           
                            <cftry>
                                
                                <cfset paymentRespose.filecontent = deserializeJSON(paymentRespose.filecontent)/>
                                <cfset paymentRespose.filecontent.numberSMS = numberSMSToSend />
                                <cfset paymentRespose.filecontent.numberKeyword = numberKeywordToSend />
                                <cfset paymentRespose.filecontent = serializeJSON(paymentRespose.filecontent) />

                                <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                                    <cfinvokeargument name="to" value="#form.inpUserEmail#">
                                    <cfinvokeargument name="type" value="html">
                                    <cfinvokeargument name="subject" value="[SIRE][Buy Credits - Keywords] Payment Info">
                                    <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_buy_credit.cfm">
                                    <cfinvokeargument name="data" value="#paymentRespose.filecontent#">
                                </cfinvoke>
                                <cfcatch type="any">
                                    <cfset dataout.ID = cfcatch.TYPE />
                                    <cfset dataout.MSG = cfcatch.MESSAGE />
                                    <cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
                                </cfcatch>
                            </cftry>
                            
                            <!--- Add payment --->
                            <cfinvoke method="updatePaymentWorlDay" component="session.sire.models.cfc.order_plan">
                                <cfinvokeargument name="planId" value="#getUserPlan.PLANID#">
                                <cfinvokeargument name="numberKeyword" value="#numberKeywordToSend#">
                                <cfinvokeargument name="numberSMS" value="#numberSMSToSend#">
                                <cfinvokeargument name="moduleName" value="Admin Buy Credits and Keywords For User">
                                <cfinvokeargument name="paymentRespose" value="#paymentRespose.filecontent#">
                            </cfinvoke>

                            <cfif numberKeywordToSend EQ 0 AND numberSMSToSend GT 0>
                                <cfset userLogOperator = "Securenet Payment Executed OK! Credit added = #numberSMSToSend# for $#amount#" />
                            <cfelseif numberKeywordToSend GT 0 AND numberSMSToSend EQ 0>
                                <cfset userLogOperator = "Securenet Payment Executed OK! Keyword Added = #numberKeywordToSend# for $#amount#" />
                            <cfelseif numberKeywordToSend GT 0 AND numberSMSToSend GT 0>
                                <cfset userLogOperator = "Securenet Payment Executed OK! Credit added = #numberSMSToSend# and Keyword Added = #numberKeywordToSend# for $#amount#" />
                            </cfif>
                            <!--- Add Userlog --->
                            <cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
                                <cfinvokeargument name="userId" value="#arguments.inpUserID#">
                                <cfinvokeargument name="moduleName" value="Payment Processing">
                                <cfinvokeargument name="operator" value="#userLogOperator#">
                            </cfinvoke>
                            <cftry>
                                <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
                                    <cfinvokeargument name="moduleName" value="Buy Credits and Keywords">
                                    <cfinvokeargument name="status_code" value="#paymentRespose.status_code#">
                                    <cfinvokeargument name="status_text" value="#paymentRespose.status_text#">
                                    <cfinvokeargument name="errordetail" value="#paymentRespose.errordetail#">
                                    <cfinvokeargument name="filecontent" value="#paymentRespose.filecontent#">
                                    <cfinvokeargument name="paymentdata" value="#paymentData#">
                                    <cfinvokeargument name="inpUserId" value="#arguments.inpUserID#">
                                </cfinvoke>
                                <cfcatch type="any">
                                </cfcatch>
                            </cftry>
                            
                            <!--- CREATE ACCOUNT ON SERCUNET --->
                            <cfif form.select_used_card EQ 0 OR form.save_cc_info EQ 1 > <!--- If do not have account or choice save then create --->
                                <cfinvoke component="session.sire.models.cfc.billing" method="RetrieveCustomer" returnvariable="RetCustomerInfo">                                    
                                    <cfinvokeargument name="userId" value="#arguments.inpUserID#">
                                </cfinvoke>    
                                <cfif RetCustomerInfo.RXRESULTCODE EQ 1>
                                    <cfset primaryPaymentMethodId = 0>
                                <cfelse>    
                                    <cfset primaryPaymentMethodId = 1>
                                </cfif>
                                <cfinvoke method="UpdateCustomer" component="session.sire.models.cfc.billing">
                                    <cfinvokeargument name="payment_method" value="#form.payment_method#">
                                    <cfinvokeargument name="line1" value="#form.line1#">
                                    <cfinvokeargument name="city" value="#form.city#">
                                    <cfinvokeargument name="country" value="#form.country#">
                                    <cfinvokeargument name="phone" value="#form.phone#">
                                    <cfinvokeargument name="state" value="#form.state#">
                                    <cfinvokeargument name="zip" value="#form.zip#">
                                    <cfinvokeargument name="email" value="#form.email#">
                                    <cfinvokeargument name="cvv" value="#form.cvv#">
                                    <cfinvokeargument name="expirationDate" value="#form.expirationDate#">
                                    <cfinvokeargument name="firstName" value="#form.firstName#">
                                    <cfinvokeargument name="lastName" value="#form.lastName#">
                                    <cfinvokeargument name="number" value="#form.number#">
                                    <cfinvokeargument name="primaryPaymentMethodId" value="#primaryPaymentMethodId#">
                                    <cfinvokeargument name="userId" value="#arguments.inpUserID#">
                                    
                                </cfinvoke>
                            </cfif>
                            
                        </cfif>
                    
                </cfif>
            </cfif>
            <!--- --->

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>
        <cfreturn dataout/>
    </cffunction>

    <cffunction name="UpdateUserAccountLimitationNew" access="remote" hint="Update User's limitation info">
        <cfargument name="inpUserId" required="yes" type="number">
        <cfargument name="inpValue" type="numeric" required="true">
        <cfargument name="inpType" type="numeric" required="true">
        <cfargument name="inpUserEmail" type="string" required="true">
        <cfargument name="inpPaymentGateway" type="string" required="false" default="1">
        <cfargument name="inpMonthYearSwitch" type="numeric" required="false" default="1">

        <cfparam name="form.payopt" default=""/>        
        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "Nothing to be updated!" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset dataout.PLANNAME = "" />

        <cfset var rsUpdateUser = {} />
        <cfset var rsUpdateUserBilling = {} />
        <cfset var fieldName = '' />
        <cfset var displayFieldName = '' />
        <cfset var oldValue = '' />
        <cfset var newValue = '' />
        <cfset var displayNewValue = '' />
        <cfset var getUserAccountLimit = '' />
        <cfset var GetEMSAdmins = '' />
        <cfset var displayOldValue = '' />
        <cfset var EBMAdminEMSList = '' />
        <cfset var rsSaveLog = '' />

        <cfset var RetVarGetAdminPermission = '' />

        <cfset var inpNumberSMSToSend = 0 />
        <cfset var inpNumberKeywordToSend = 0 />
        
        <cftry>
            <cfinvoke component="session.sire.models.cfc.admin"  method="GetAdminPermission" returnvariable="RetVarGetAdminPermission">
                <cfinvokeargument name="inpSkipRedirect" value="1"/>
            </cfinvoke>
            <cfif RetVarGetAdminPermission.ISADMINOK NEQ "1">
                <cfthrow message="You do not have permission!" />
            </cfif>

            <cfswitch expression="#arguments.inpType#">
                <cfcase value='1'>
                    <cfset fieldName = 'FirstSMSIncluded_int'/>
                    <cfset displayFieldName = 'SMS Credits'/>
                </cfcase>

                <cfcase value='2'>
                    <cfset fieldName = 'PriceMsgAfter_dec'/>
                    <cfset displayFieldName = 'Cost per Credit'/>
                </cfcase>

                <cfcase value='3'>
                    <cfset fieldName = 'KeywordsLimitNumber_int'/>
                    <cfset displayFieldName = 'Keywords'/>
                </cfcase>

                <cfcase value='4'>
                    <cfset fieldName = 'MlpsLimitNumber_int'/>
                    <cfset displayFieldName = 'MLPs'/>
                </cfcase>

                <cfcase value='5'>
                    <cfset fieldName = 'ShortUrlsLimitNumber_int'/>
                    <cfset displayFieldName = 'Short URLs'/>
                </cfcase>

                <cfcase value='6'>
                    <cfset fieldName = 'MlpsImageCapacityLimit_bi'/>
                    <cfset displayFieldName = 'Image Capicity'/>
                </cfcase>

                <cfcase value='7'>
                    <cfset fieldName = 'PlanId_int'/>
                    <cfset displayFieldName = 'User Plan'/>
                </cfcase>
                <cfcase value='8'>                 
                    <cfset fieldName = 'BuyCreditBalance_int'/>                       
                    <cfif form.payopt EQ "payopt1" OR arguments.inpPaymentGateway EQ 2>                        
                        <cfset displayFieldName = 'Buy Credits'/>
                    <cfelse>                        
                        <cfset displayFieldName = 'Buy Credits Without Payment'/>
                    </cfif>
                </cfcase>
                <cfcase value='9'>
                    <cfset fieldName = 'BuyKeywordNumber_int'/>
                    <cfif form.payopt EQ "payopt1" OR arguments.inpPaymentGateway EQ 2>
                        <cfset displayFieldName = 'Buy Keywords'/>
                    <cfelse>
                        <cfset displayFieldName = 'Buy Keywords Without Payment'/>
                    </cfif>
                </cfcase>
                <cfcase value='10'>
                    <cfset fieldName = 'BuyKeywordNumber_int'/>
                    <cfset displayFieldName = 'Set Keywords Purchased'/>
                </cfcase>
                 <cfcase value='11'>
                    <cfset fieldName = 'KeywordMinCharLimit_int'/>
                    <cfset displayFieldName = 'Set Min Char of Keywords'/>
                </cfcase>
            </cfswitch>
            <cfif Listfind("10",arguments.inpType) EQ 0> <!--- not check for set keywords purchased--->
                <cfif arguments.inpValue LT 0>
                    <cfset dataout.MESSAGE = "Invalid value!" />
                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfreturn dataout />    
                </cfif>
            </cfif>
            <cfif arguments.inpType EQ 8>
                <cfquery name="getUserAccountLimit" datasource="#Session.DBSourceEBM#">
					SELECT #fieldName# 
					FROM simplebilling.billing
					WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">
					LIMIT 1
				</cfquery>
            <cfelse>
                <cfquery name="getUserAccountLimit" datasource="#Session.DBSourceEBM#">
                    SELECT
                        #fieldName#
                    FROM
                        simplebilling.userplans
                    WHERE
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                    AND
                        Status_int = 1
                    ORDER BY 
                        UserPlanId_bi DESC
                    LIMIT 1
                </cfquery>
            </cfif>

            <cfset oldValue = getUserAccountLimit["#fieldName#"]>

            <cfif oldValue EQ arguments.inpValue AND Listfind("7,8,9",arguments.inpType) EQ 0>
                <cfset dataout.MESSAGE = "The inputted value is equal to the current value." />
                <cfset dataout.RXRESULTCODE = -1 />
                <cfreturn dataout />
            </cfif>

            <cfset newValue = arguments.inpValue>

            <cfif fieldName EQ "PriceMsgAfter_dec">
                <cfset newValue = newValue*100/>
            </cfif>
            <cfif arguments.inpType EQ 8 OR arguments.inpType EQ 9>
                <cfset newValue = oldValue+newValue/>
            </cfif>
            
            <cfif arguments.inpType EQ 8 OR arguments.inpType EQ 9 > <!---do payment for case buy credit , buy kw with worldpay --->
                <cfif form.payopt EQ "payopt1">                    
                    <cfif arguments.inpType EQ 8>
                        <cfset inpNumberSMSToSend=arguments.inpValue>
                    <cfelseif arguments.inpType EQ 9>
                        <cfset inpNumberKeywordToSend=arguments.inpValue>
                    <cfelse>
                    </cfif>

                    <cfif arguments.inpPaymentGateway EQ 1>
                        <cfinvoke method="DoProcessPayment">
                            <cfinvokeargument name="inpUserID" value="#arguments.inpUserId#">
                            <cfinvokeargument name="inpNumberSMSToSend" value="#inpNumberSMSToSend#">
                            <cfinvokeargument name="inpNumberKeywordToSend" value="#inpNumberKeywordToSend#">
                            <cfinvokeargument name="inpAmount" value="#form.inpCustomAmount#">                         
                        </cfinvoke>  
                    </cfif>
                    
                </cfif>
            </cfif>
            
            <cfif arguments.inpType EQ 7>
                <cfif arguments.inpPaymentGateway EQ 1>
                    <cfset rsUpdateUser = UpdateUserAccountPlanByAdminRole(arguments.inpUserId, newValue, arguments.inpUserEmail)/>    
                <cfelse>                    
                    <cfinvoke component="session.sire.models.cfc.order_plan" method="doUpgradePlan" returnvariable="rsUpdateUser">
                    <cfinvokeargument name="inpPlanId" value="#newValue#">
                    <cfinvokeargument name="inpMonthYearSwitch" value="#arguments.inpMonthYearSwitch#">
                    <cfinvokeargument name="inpPaymentGateWay" value="2"/>
                    <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    <cfinvokeargument name="inpAdminDo" value="1">
                    </cfinvoke>
                    <cfset dataout.PLANNAME = rsUpdateUser.PLANNAME />
                </cfif>
                
                <cfset rsUpdateUser.RECORDCOUNT = rsUpdateUser.RXRESULTCODE/>

                <!--- <cfset dataout.rsUpdateUser = rsUpdateUser/><!--- debug ---> --->

                <cfif rsUpdateUser.RXRESULTCODE NEQ 1>
                    <cfreturn rsUpdateUser/>
                </cfif>
            <cfelseif arguments.inpType EQ 8>
                <cfquery result="rsUpdateUser" datasource="#Session.DBSourceEBM#">
                    UPDATE
                        simplebilling.billing
                    SET 
                        #fieldName# = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#newValue#">
                    WHERE 
                        UserId_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">
                    LIMIT 1
                </cfquery>
            <cfelse>
                <cfquery result="rsUpdateUser" datasource="#Session.DBSourceEBM#">
                    UPDATE 
                        simplebilling.userplans
                    SET
                        #fieldName# = 
                            <cfif fieldName EQ 'PriceMsgAfter_dec'>
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#newValue#"> 
                            <cfelse> 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#newValue#"> 
                            </cfif>

                    WHERE
                        UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                    AND 
                        Status_int = 1
                    ORDER BY 
                        UserPlanId_bi DESC
                    LIMIT
                        1 
                </cfquery>
            </cfif>

            <cfif rsUpdateUser.RECORDCOUNT GT 0>
                <cfinvoke method="saveUpdateAccountLog" returnvariable="rsSaveLog">
                    <cfinvokeargument name="inpUserID" value="#arguments.inpUserId#">
                    <cfinvokeargument name="inpOldValue" value="#oldValue#">
                    <cfinvokeargument name="inpNewValue" value="#newValue#">
                    <cfinvokeargument name="inpFieldName" value="#displayFieldName#">
                </cfinvoke>

                <cfif (fieldName EQ 'MlpsLimitNumber_int' OR fieldName EQ 'ShortUrlsLimitNumber_int') AND oldValue EQ 0>
                    <cfset displayOldValue = 'Unlimited'>
                <cfelseif fieldName EQ "PriceMsgAfter_dec">
                    <cfset displayOldValue = '$' & oldValue/100 />
                <cfelse>
                    <cfset displayOldValue = oldValue>
                </cfif>

                <cfif (fieldName EQ 'MlpsLimitNumber_int' OR fieldName EQ 'ShortUrlsLimitNumber_int') AND newValue EQ 0>
                    <cfset displayNewValue = "Unlimited" />
                <cfelseif fieldName EQ "PriceMsgAfter_dec">
                    <cfset displayNewValue = '$' & newValue/100 />
                <cfelse>
                    <cfset displayNewValue = newValue>
                </cfif>

                <cfquery name="GetEMSAdmins" datasource="#Session.DBSourceREAD#">
                    SELECT
                        ContactAddress_vch                              
                    FROM 
                        simpleobjects.systemalertscontacts
                    WHERE
                        ContactType_int = 2
                    AND
                        EMSNotice_int = 1    
                </cfquery>

                <cfif GetEMSAdmins.RecordCount GT 0>
                    <cfset EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                   
                <cfelse>
                    <cfset EBMAdminEMSList = SupportEMailFrom>
                </cfif>                
                        
                <cfmail to="#EBMAdminEMSList#" subject="Admin Update UserAccount" type="html" from="#EBMAdminEMSList#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#"  port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                    <!DOCTYPE html>
                    <html>
                        <head>
                            <style>
                                table {
                                    font-family: arial, sans-serif;
                                    border-collapse: collapse;
                                    background-color: ##f1f1c1;
                                    width: 100%;
                                }

                                td, th {
                                    border: 1px solid ##dddddd;
                                    text-align: left;
                                    padding: 8px;
                                }

                                tr:nth-child(even) {
                                    background-color: ##dddddd;
                                }

                                strong.red {
                                    color: red;
                                }
                            </style>
                        </head>
                        <body>
                            <table cellpadding="5" cellspacing="5" style="">
                                <thead>
                                    <tr>
                                        <th colspan="4"><h3 style="text-align: center;">Admin User Modified Monthly Limits</h3></th>
                                    </tr>
                                    <tr>
                                        <th colspan="2">Status</th><th colspan="2"><cfoutput>#displayFieldName#</cfoutput></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="2">Before:</td>
                                        <td colspan="2"><strong><cfoutput>#displayOldValue#</cfoutput></strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">After:</td>
                                        <td colspan="2"><strong><cfoutput>#displayNewValue#</cfoutput></strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="text-align: center">Updated by: <strong><cfoutput>#Session.USERNAME#</cfoutput> admin user</strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="text-align: center">Update for: <strong><cfoutput>#arguments.inpUserEmail#</cfoutput> user account - ID: <cfoutput>#arguments.inpUserId#</cfoutput></strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </body>
                    </html>
                </cfmail>                
                
                <!--- Log to userlog --->
                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="Admin User Modified Monthly Limits">
                    <cfinvokeargument name="operator" value="Changed account limits for #arguments.inpUserId# by #Session.UserId#">
                </cfinvoke>

                <cfset dataout.MESSAGE = "Update Account successfully!" />
                <cfset dataout.RXRESULTCODE = 1 />
            </cfif>

            <!--- <cfquery result="rsUpdateUserBilling" datasource="#Session.DBSourceEBM#" >
                UPDATE 
                    simplebilling.billing
                SET
                    PromotionCreditBalance_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpPromotion#">
                WHERE
                    UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                LIMIT
                    1 
            </cfquery> --->

            <!--- <cfif rsUpdateUser.RECORDCOUNT GT 0 AND rsUpdateUserBilling.RECORDCOUNT GT 0> --->
            
            <cfcatch>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataout />
    </cffunction>

    <cffunction name="saveUpdateAccountLog" access="public" hint="save log update account">
        <cfargument name="inpUserID" type="numeric" required="true">
        <cfargument name="inpFieldName" type="string" required="true">
        <cfargument name="inpOldValue" type="numeric" required="true">
        <cfargument name="inpNewValue" type="numeric" required="true">
        
        <cfset var dataout  = {} />
        <cfset var rsInsert = {} />
        
        <cfset dataout.RXRESULTCODE = 0>
        <cfset dataout.DATA = ''>
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />

        <cftry>
            <cfquery result="rsInsert" datasource="#Session.DBSourceEBM#">
                INSERT INTO 
                    simplebilling.account_update_logs(UserId_int, OwnerID_int, FieldName_vch, OldValue_dec, NewValue_dec)
                VALUES (
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpFieldName#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#arguments.inpOldValue#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#arguments.inpNewValue#">
                )
            </cfquery>

            <cfset dataout.RXRESULTCODE = 1>
            <cfset dataout.MESSAGE = "" />

            <cfcatch>
                <cfset dataout = {}>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>

    <cffunction name="UpdateUserAccountPlanByAdminRole" access="private" hint="Used: admin set new plan for sire user">
        <cfargument name="inpUserID" type="numeric" required="true"/>
        <cfargument name="inpPlanID" type="numeric" required="true"/>
        <cfargument name="inpUserEmail" type="string" required="true"/>

        <cfparam name="form.monthYearSwitch" default="1"/>
        <cfparam name="form.payopt" default=""/>
        <cfparam name="form.inpCustomAmount" default="0"/>
        <cfparam name="form.inpPromotionCode" default=""/>
        <cfparam name="form.select_used_card" default="1"/>
        <cfparam name="form.payment_method" default="0"/>
        <cfparam name="form.number" default=""/>
        <cfparam name="form.cvv" default=""/>
        <cfparam name="form.expirationDate" default=""/>
        <cfparam name="form.firstName" default=""/>
        <cfparam name="form.lastName" default=""/>
        <cfparam name="form.line1" default=""/>
        <cfparam name="form.city" default=""/>
        <cfparam name="form.state" default=""/>
        <cfparam name="form.zip" default=""/>
        <cfparam name="form.country" default="US"/>
        <cfparam name="form.phone" default=""/>
        <cfparam name="form.email" default=""/>
        <cfparam name="form.save_cc_info" default="0"/>

        <cfparam name="form.listKeyword" default=""/>
        <cfparam name="form.listShortUrl" default=""/>
        <cfparam name="form.listMLP" default=""/>

        <cfset var dataout  = {} />
        <cfset dataout.RXRESULTCODE = 0/>
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />

        <cfset var BuyKeywordNumber = '' />
        <cfset var TotalKeyword = '' />
        <cfset var totalAmount  = '' />
        <cfset var purchaseAmount   = '' />
        <cfset var KEYWORD_CC   = '' />
        <cfset var MLP_CC   = '' />
        <cfset var URL_CC   = '' />
        <cfset var purchaseSuccess  = '' />
        <cfset var promotionId  = '' />
        <cfset var originId = '' />
        <cfset var recurringTime    = '' />
        <cfset var INP_PERCENT  = '0' />
        <cfset var INP_FLATRATE = '0' />
        <cfset var INP_CREDIT   = '0' />
        <cfset var INP_KEYWORD  = '0' />
        <cfset var INP_MLP  = '0' />
        <cfset var INP_URL  = '0' />
        <cfset var COUPONDISCOUNTTYPE   = '' />
        <cfset var percent_dis  = '0' />
        <cfset var flatrate_dis = '0' />
        <cfset var promotion_id = '' />
        <cfset var promotion_origin_id  = '' />
        <cfset var promotion_used_time  = '' />
        <cfset var promotion_credit = '0' />
        <cfset var promotion_mlp    = '0' />
        <cfset var promotion_keyword    = '0' />
        <cfset var promotion_shorturl   = '0' />
        <cfset var PERCENT_CC   = '0' />
        <cfset var FLATRATE_CC  = '0' />
        <cfset var CREDIT_CC    = '0' />
        <cfset var customerId   = '' />
        <cfset var paymentMethodId  = '' />
        <cfset var paymentTypeShortName = '' />
        <cfset var paymentType  = '' />
        <cfset var paymentData  = '' />
        <cfset var closeBatchesData = '' />
        <cfset var paymentResposeContent    = '' />
        <cfset var purchaseType = '' />
        <cfset var primaryPaymentMethodId   = '' />
        <cfset var index    = '' />
        <cfset var getPlan  = '' />
        <cfset var RetVarGetAdminPermission = '' />
        <cfset var getUserPlan  = '' />
        <cfset var checkpromocode   = '' />
        <cfset var promocode    = '' />
        <cfset var CouponForUser    = '' />
        <cfset var GetUserAuthen    = '' />
        <cfset var newUserPromoCode = '' />
        <cfset var RetCustomerInfo  = '' />
        <cfset var RetAddReferralCredits    = '' />
        <cfset var resultActiveUserData = '' />
        <cfset var RetVarmailChimpAPIs  = '' />
        <cfset var paymentRespose   = '' />
        <cfset var closeBatchesRequestResult    = '' />
        <cfset var txtUpgradePlan   = '' />

        <cftry>
            <cfinvoke component="session.sire.models.cfc.admin"  method="GetAdminPermission" returnvariable="RetVarGetAdminPermission">
                <cfinvokeargument name="inpSkipRedirect" value="1"/>
            </cfinvoke>
            <cfif RetVarGetAdminPermission.ISADMINOK NEQ "1">
                <cfthrow message="You do not have permission!" />
            </cfif>

            <cfquery name="getPlan" datasource="#Session.DBSourceEBM#">
                SELECT 
                    PlanId_int AS PLANID, 
                    PlanName_vch AS PLANNAME,
                    Amount_dec AS AMOUNT,
                    YearlyAmount_dec AS YEARLYAMOUNT,
                    UserAccountNumber_int AS USERACCOUNTNUMBER,
                    KeywordsLimitNumber_int AS KEYWORDSLIMITNUMBER,
                    FirstSMSIncluded_int AS FIRSTSMSINCLUDED,
                    PriceMsgAfter_dec AS PRICEMSGAFTER,
                    PriceKeywordAfter_dec AS PRICEKEYWORDAFTER,
                    MlpsLimitNumber_int AS MLPSLIMITNUMBER,
                    ShortUrlsLimitNumber_int AS SHORTURLSLIMITNUMBER,
                    MlpsImageCapacityLimit_bi AS IMAGECAPACITYLIMIT
                FROM 
                    simplebilling.plans
                WHERE
                    PlanId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPlanID#"/> 
                AND
                    (Status_int = 1 OR Status_int = 8)
            </cfquery>

            <cfif getPlan.RecordCount EQ 0>
                <cfthrow message="New plan is invalid." />
            </cfif>

            <cfinvoke method="GetUserPlan" component="session.sire.models.cfc.order_plan" returnvariable="getUserPlan">
                <cfinvokeargument name="InpUserId" value="#arguments.inpUserID#"/>
            </cfinvoke>

            <cfif getUserPlan.RecordCount EQ 0>
                <cfthrow message="Old plan is invalid." />
            </cfif>

            <cfset BuyKeywordNumber = 0/>
            <cfset TotalKeyword = getUserPlan.PLANKEYWORDSLIMITNUMBER + getUserPlan.KEYWORDPURCHASED/> 
            <cfif TotalKeyword GT getPlan.KEYWORDSLIMITNUMBER>
                <cfset BuyKeywordNumber = (TotalKeyword - getPlan.KEYWORDSLIMITNUMBER)/>
            </cfif>
            
            <cfif form.monthYearSwitch EQ 2>
                <cfset totalAmount = getPlan.YEARLYAMOUNT * 12/>
            <cfelse>
                <cfset totalAmount = getPlan.AMOUNT/>
            </cfif>
            
            <cfif isNumeric(form.inpCustomAmount) AND arguments.inpPlanID NEQ 1>
                <cfset totalAmount = form.inpCustomAmount/>
            </cfif>
            
            <cfset purchaseAmount = totalAmount/>

            <cfset KEYWORD_CC = 0/> <!--- getUserPlan.PROMOTIONKEYWORDSLIMITNUMBER/> --->
            <cfset MLP_CC = 0/> <!--- getUserPlan.PROMOTIONMLPSLIMITNUMBER/> --->
            <cfset URL_CC = 0/> <!--- getUserPlan.PROMOTIONSHORTURLSLIMITNUMBER/> --->

            <cfif form.email EQ ''>
                <cfset form.email = arguments.inpUserEmail/>
            </cfif>

            <cfif form.payopt EQ 'payopt1' AND purchaseAmount GT 0>
                <cfset purchaseSuccess = 0/>

                <cfif form.inpPromotionCode NEQ ''>
                    <cfinvoke component="session.sire.models.cfc.promotion" method="CheckCouponForUpgradePlan" returnvariable="checkpromocode">
                        <cfinvokeargument name="inpCouponCode" value="#form.inpPromotionCode#"/>
                    </cfinvoke>

                    <cfif checkpromocode.RXRESULTCODE EQ 1>
                        <!--- coupon input --->
                        <cfinvoke component="session.sire.models.cfc.promotion" method="GetCouponDetails" returnvariable="promocode">
                            <cfinvokeargument name="inpCouponId" value="#checkpromocode.promotion_id#"/>
                        </cfinvoke>

                        <cfset promotionId = promocode.PROMOTIONID/>
                        <cfset originId = promocode.ORIGINID/>
                        <cfset recurringTime = promocode.COUPONRECURRING/>

                        <cfset INP_PERCENT = promocode.COUPONDISCOUNTPRICEPERCENT/>
                        <cfset INP_FLATRATE = promocode.COUPONDISCOUNTPRICEFLATRATE/>
                        <cfset INP_CREDIT = promocode.COUPONCREDIT/>
                        <cfset INP_KEYWORD = promocode.COUPONKEYWORD/>
                        <cfset INP_MLP = promocode.COUPONMLP/>
                        <cfset INP_URL = promocode.COUPONSHORTURL/>
                    </cfif>
                </cfif>

                <!--- Promotion benefit --->
                <cfinvoke component="session.sire.models.cfc.promotion" method="CheckAvailableCouponForUser" returnvariable="CouponForUser">
                    <cfinvokeargument name="inpUserId" value="#arguments.inpUserID#">
                </cfinvoke>

                <!--- Check CouponForUser --->
                <cfif CouponForUser.rxresultcode EQ 1>
                    <cfloop array="#CouponForUser['DATALIST']#" index="index">
                        <cfif index.COUPONDISCOUNTTYPE EQ 1>
                            <cfset COUPONDISCOUNTTYPE = 1/>
                            <cfset percent_dis = percent_dis + index.COUPONDISCOUNTPRICEPERCENT/>
                            <cfset flatrate_dis = flatrate_dis + index.COUPONDISCOUNTPRICEFLATRATE/>
                            <cfset promotion_id = index.PROMOTIONID/>
                            <cfset promotion_origin_id = index.ORIGINID/>
                            <!--- <cfset promotion_used_time = index.USEDTIME/> --->
                        <cfelseif index.COUPONDISCOUNTTYPE EQ 2>
                            <cfset COUPONDISCOUNTTYPE = 2/>
                            <cfset promotion_id = index.PROMOTIONID/>
                            <cfset promotion_origin_id = index.ORIGINID/>
                            <!--- <cfset promotion_used_time = index.USEDTIME/> --->
                            <cfset promotion_credit = promotion_credit + index.COUPONCREDIT/>
                            <cfset promotion_mlp = promotion_mlp + index.COUPONMLP/>
                            <cfset promotion_keyword = promotion_keyword + index.COUPONKEYWORD/>
                            <cfset promotion_shorturl = promotion_shorturl + index.COUPONSHORTURL/>
                        <cfelse>
                            <cfset promotion_id = index.PROMOTIONID>
                            <cfset promotion_origin_id = index.ORIGINID>
                            <!--- <cfset promotion_used_time = index.USEDTIME> --->
                        </cfif>

                        <cfset arrayAppend(promocodeListToIncreaseUsedTimeTemp, [promotion_origin_id, promotion_id, arguments.inpUserID])/>
                    </cfloop>

                <cfelse>
                    <cfset promotion_id = 0>
                    <cfset promotion_origin_id = 0>
                </cfif>

                <cfset PERCENT_CC = INP_PERCENT + percent_dis/>
                <cfset FLATRATE_CC = INP_FLATRATE + flatrate_dis/>
                <cfset CREDIT_CC = INP_CREDIT/>
                <cfset KEYWORD_CC = INP_KEYWORD + promotion_keyword/>
                <cfset MLP_CC = INP_MLP + promotion_mlp/>
                <cfset URL_CC = INP_URL + promotion_shorturl/>
                <cfset purchaseAmount = (totalAmount - (totalAmount * INP_PERCENT / 100) - INP_FLATRATE)/>

                <cfset txtUpgradePlan =  "Ugraded Plan to: "&getPlan.PLANNAME>

                <cfif purchaseAmount GT 0>
                    <cfif form.select_used_card EQ 1> <!--- USE SAVED TOKEN TO PAYMENT --->
                        <cfinvoke component="session.sire.models.cfc.billing" method="getUserAuthenInfo" returnvariable="GetUserAuthen">
                            <cfinvokeargument name="userId" value="#arguments.inpUserID#"/>
                        </cfinvoke>

                        <cfif GetUserAuthen.RXRESULTCODE EQ 1 AND structKeyExists(GetUserAuthen.DATA, "PaymentMethodID_vch") AND GetUserAuthen.DATA.PaymentMethodID_vch NEQ ''  >
                            <cfset customerId = #arguments.inpUserID#>
                            <cfset paymentMethodId = #GetUserAuthen.DATA.PaymentMethodID_vch#>
                            <cfset paymentTypeShortName = #GetUserAuthen.DATA.PaymentType_vch#>

                            <cftry>
                                <cfset paymentType = StructFind(worldpayPaymentTypes, paymentTypeShortName)>
                                <cfcatch>
                                    <cfset paymentType = worldpayPaymentTypes.UNKNOWN>
                                </cfcatch>  
                            </cftry>

                        <cfelse>
                            <cfthrow message="We're Sorry! Unable to get your CreditCard info. Please update new CreditCard." />
                        </cfif>

                        <cfset paymentData = {
                            amount = purchaseAmount,
                            transactionDuplicateCheckIndicator = 1,
                            extendedInformation = {
                                notes = txtUpgradePlan,
                            },
                            paymentVaultToken = {  
                                customerId = customerId,
                                paymentMethodId = paymentMethodId,
                                paymentType = paymentType
                            },
                            developerApplication = worldpayApplication
                        }/>
                    <cfelse>  
                        <cfset paymentData = {
                            amount = purchaseAmount,
                            transactionDuplicateCheckIndicator = 1,
                            extendedInformation = {
                                notes = txtUpgradePlan,
                            },
                            developerApplication = {
                                developerId: worldpayApplication.developerId,
                                version: worldpayApplication.version
                            }
                        }/>

                        <!--- <cfif form.payment_method EQ 1>
                            <cfset paymentData.check = {
                                    routingNumber = form.number,
                                    accountNumber = form.cvv,
                                    firstName   = form.firstName,
                                    lastName    = form.lastName,
                                    address     = {
                                    line1   = form.line1,
                                    city  = form.city,
                                    state   = form.state,
                                    zip   = form.zip,
                                    country = form.country,
                                    phone = form.phone
                                },
                                email     = form.email
                            }/>
                        <cfelse> --->
                            <cfset paymentData.card = {
                                    number      = form.number,
                                    cvv       = form.cvv,
                                    expirationDate  = form.expirationDate,
                                    firstName   = form.firstName,
                                    lastName    = form.lastName,
                                    address     = {
                                    line1   = form.line1,
                                    city  = form.city,
                                    state   = form.state,
                                    zip   = form.zip,
                                    country = form.country,
                                    phone = form.phone,
                                },
                                email     = form.email,
                                emailReceipt = false
                            }/>
                        <!--- </cfif> --->
                    </cfif>

                    <cfset closeBatchesData = {
                        developerApplication = {
                            developerId: worldpayApplication.developerId,
                            version: worldpayApplication.version
                        }
                    }/>

                    <cfhttp url="#payment_url#" method="post" result="paymentRespose" port="443">
                        <cfhttpparam type="header" name="content-type" value="application/json"/>
                        <cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#"/>
                        <cfhttpparam type="body" value='#TRIM( serializeJSON(paymentData) )#'/>
                    </cfhttp>
                    
                    <cfif paymentRespose.status_code EQ 200>
                        <cfset paymentResposeContent = deserializeJSON(paymentRespose.filecontent)>
                        <cfset paymentResposeContent.planname = getplan.PLANNAME/>

                        <cfset purchaseType = '#NumberFormat(getPlan.FIRSTSMSINCLUDED)# Credit Purchase for $#NumberFormat(purchaseAmount)# USD'>

                        <cftry>
                            <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
                                <cfinvokeargument name="moduleName" value="Accounts management - Edit plan"/>
                                <cfinvokeargument name="status_code" value="#paymentRespose.status_code#"/>
                                <cfinvokeargument name="status_text" value="#paymentRespose.status_text#"/>
                                <cfinvokeargument name="errordetail" value="#paymentRespose.errordetail#"/>
                                <cfinvokeargument name="filecontent" value="#paymentRespose.filecontent#"/>
                                <cfinvokeargument name="inpUserId" value="#arguments.inpUserID#"/>
                            </cfinvoke>
                            <cfcatch type="any">
                            </cfcatch>
                        </cftry>

                        <cfif paymentResposeContent.success>

                            <cfhttp url="#close_batches_url#" method="post" result="closeBatchesRequestResult" port="443">
                                <cfhttpparam type="header" name="content-type" value="application/json"/>
                                <cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#"/>
                                <cfhttpparam type="body" value='#TRIM( serializeJSON(closeBatchesData) )#'/>
                            </cfhttp>

                            <cfset purchaseSuccess = 1/>

                        <cfelse>
                            <cfthrow message="Securenet Execute Transaction Failed!"/>
                        </cfif>
                    <cfelse>
                        <cfthrow message="Securenet Execute Transaction Failed!"/>
                    </cfif>
                <cfelse>
                    <cfset purchaseSuccess = 2/>
                </cfif>
            <cfelse>
                <cfset purchaseSuccess = 2/>
            </cfif>

            <cfif purchaseSuccess GT 0>
                <cftransaction>
                    <cftry>
                        <cftransaction action="begin"/>
                        <cfinvoke method="upgradeUserPlan" component="session.sire.models.cfc.order_plan">
                            <cfinvokeargument name="inpUserId" value="#arguments.inpUserID#">
                            <cfinvokeargument name="inpPlanData" value="#getPlan#">
                            <cfinvokeargument name="inpTotalAmount" value="#totalAmount#">
                            <cfinvokeargument name="inpBuyKeywordNumber" value="#BuyKeywordNumber#">
                            <cfinvokeargument name="inpKEYWORD_CC" value="#KEYWORD_CC#">
                            <cfinvokeargument name="inpMLP_CC" value="#MLP_CC#">
                            <cfinvokeargument name="inpURL_CC" value="#URL_CC#">
                            <cfinvokeargument name="inpCREDIT_CC" value="0">
                            <cfinvokeargument name="inpMonthYearSwitch" value="#monthYearSwitch#">
                        </cfinvoke>

                        <!--- Add Userlog --->
                        <cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
                            <cfinvokeargument name="userId" value="#arguments.inpUserID#"/>
                            <cfinvokeargument name="moduleName" value="Accounts management - Edit plan"/>
                            <cfinvokeargument name="operator" value="Change Plan Executed OK! Upgrade Plan for (#arguments.inpUserID#)#arguments.inpUserEmail# - planID: #getUserPlan.PLANID# -> #getplan.PLANID# - plan type: #getUserPlan.BILLINGTYPE# -> #monthYearSwitch#"/>
                        </cfinvoke>

                        <cfif promotionId GT 0>
                            <!--- New Promocode User --->
                            <cfinvoke component="session.sire.models.cfc.promotion" method="insertUserPromoCode" returnvariable="newUserPromoCode">
                                <cfinvokeargument name="inpUserId" value="#arguments.inpUserID#"/>
                                <cfinvokeargument name="inpPromoId" value="#promotionId#"/>
                                <cfinvokeargument name="inpPromoOriginId" value="#originId#"/>
                                <cfinvokeargument name="inpUsedDate" value="#Now()#"/>
                                <cfinvokeargument name="inpRecurringTime" value="#recurringTime#"/>
                                <cfinvokeargument name="inpPromotionStatus" value="1">
                            </cfinvoke>
                            <!---END New Promocode User --->
                            <!--- update used time coupon --->
                            <cfinvoke method="UpdateUserPromoCodeUsedTime" component="session.sire.models.cfc.promotion">
                                <cfinvokeargument name="inpUserId" value="#arguments.inpUserID#"/>
                                <cfinvokeargument name="inpUsedFor" value="3"/>
                                <cfinvokeargument name="inpPromotionId" value="#promotionId#"/>
                                <cfinvokeargument name="inpLimit" value="1">
                            </cfinvoke>
                            <!--- en update used time --->
                            <!--- Insert coupon code used log --->
                            <cfinvoke method="InsertPromotionCodeUsedLog" component="public.sire.models.cfc.promotion">
                                <cfinvokeargument name="inpCouponId" value="#promotionId#"/>
                                <cfinvokeargument name="inpOriginCouponId" value="#originId#"/>
                                <cfinvokeargument name="inpUserId" value="#arguments.inpUserID#"/>
                                <cfinvokeargument name="inpUsedFor" value="Change Plan By Admin"/>
                            </cfinvoke>
                            <cfinvoke method="IncreaseCouponUsedTime" component="public.sire.models.cfc.promotion">
                                <cfinvokeargument name="inpCouponId" value="#originId#"/>
                                <cfinvokeargument name="inpUsedFor" value="2"/>
                            </cfinvoke>
                        </cfif>

                        <cfif purchaseSuccess EQ 1>
                            <!--- Add payment --->
                            <cfinvoke method="updatePaymentWorlDay" component="session.sire.models.cfc.order_plan">
                                <cfinvokeargument name="planId" value="#getPlan.PLANID#"/>
                                <cfinvokeargument name="numberKeyword" value=""/>
                                <cfinvokeargument name="numberSMS" value=""/>
                                <cfinvokeargument name="moduleName" value="Accounts management - Edit plan"/>
                                <cfinvokeargument name="paymentRespose" value="#paymentRespose.filecontent#"/>
                                <cfinvokeargument name="inpUserId" value="#arguments.inpUserID#"/>
                            </cfinvoke>

                            <!--- Add Userlog --->
                            <cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
                                <cfinvokeargument name="userId" value="#arguments.inpUserID#"/>
                                <cfinvokeargument name="moduleName" value="Accounts management - Edit plan"/>
                                <cfinvokeargument name="operator" value="Securenet Payment Executed OK! Upgrade Plan for $#purchaseAmount# - plan #getplan.PLANNAME# - planID #getplan.PLANID# - plan type #monthYearSwitch#"/>
                            </cfinvoke>

                            <!--- CREATE ACCOUNT ON SERCUNET --->
                            <cfif form.select_used_card NEQ 1 OR form.save_cc_info EQ 1 > <!--- If do not have account or choice save then create --->
                                <cfinvoke component="session.sire.models.cfc.billing" method="RetrieveCustomer" returnvariable="RetCustomerInfo">
                                    <cfinvokeargument name="userId" value="#arguments.inpUserID#"/>
                                </cfinvoke>  
                                <cfif RetCustomerInfo.RXRESULTCODE EQ 1>
                                    <cfset primaryPaymentMethodId = 0> <!--- do not print --->
                                <cfelse>  
                                    <cfset primaryPaymentMethodId = 1> <!--- do not print --->
                                </cfif>
                                <cfinvoke method="UpdateCustomer" component="session.sire.models.cfc.billing">
                                    <cfinvokeargument name="payment_method" value="#form.payment_method#"/>
                                    <cfinvokeargument name="line1" value="#form.line1#"/>
                                    <cfinvokeargument name="city" value="#form.city#"/>
                                    <cfinvokeargument name="country" value="#form.country#"/>
                                    <cfinvokeargument name="phone" value="#form.phone#"/>
                                    <cfinvokeargument name="state" value="#form.state#"/>
                                    <cfinvokeargument name="zip" value="#form.zip#"/>
                                    <cfinvokeargument name="email" value="#form.email#"/>
                                    <cfinvokeargument name="cvv" value="#form.cvv#"/>
                                    <cfinvokeargument name="expirationDate" value="#form.expirationDate#"/>
                                    <cfinvokeargument name="firstName" value="#form.firstName#"/>
                                    <cfinvokeargument name="lastName" value="#form.lastName#"/>
                                    <cfinvokeargument name="number" value="#form.number#"/>
                                    <cfinvokeargument name="primaryPaymentMethodId" value="#primaryPaymentMethodId#"/>
                                    <cfinvokeargument name="userId" value="#arguments.inpUserID#"/>
                                </cfinvoke>
                            </cfif>  

                            <!--- Sendmail payment --->
                            <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                                <cfinvokeargument name="to" value="#email#"/>
                                <cfinvokeargument name="type" value="html"/>
                                <cfinvokeargument name="subject" value="[SIRE][Order Plan] Payment Info"/>
                                <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_order_plan.cfm"/>
                                <cfinvokeargument name="data" value="#serializeJSON(paymentResposeContent)#"/>
                            </cfinvoke>
                        <cfelse>
                            <!--- Sendmail payment --->
                            <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                                <cfinvokeargument name="to" value="#email#"/>
                                <cfinvokeargument name="type" value="html"/>
                                <cfinvokeargument name="subject" value="[SIRE][Order Plan] Payment Info"/>
                                <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_order_plan.cfm"/>
                                <cfinvokeargument name="data" value=""/>
                            </cfinvoke>
                        </cfif>

                        <!--- ADD CURRENT REFERRAL CREDITS TO BALANCE --->
                        <cfinvoke method="AddReferralCredits" component="session.sire.models.cfc.referral" returnvariable="RetAddReferralCredits">
                            <cfinvokeargument name="inpUserId" value="#arguments.inpUserID#"/>
                        </cfinvoke>

                        <!--- check to reactive keyword --->
                        <cfif form.listKeyword NEQ ''> 
                            <cfset form.listKeyword = listToArray(form.listKeyword,',')/>
                        <cfelse>
                            <cfset form.listKeyword = []/>
                        </cfif>

                        <cfif form.listShortUrl NEQ ''>
                            <cfset form.listShortUrl = listToArray(form.listShortUrl,',')/>
                        <cfelse>
                            <cfset form.listShortUrl = []/>
                        </cfif>

                        <cfif form.listMLP NEQ ''>
                            <cfset form.listMLP = listToArray(form.listMLP,',')/>
                        <cfelse>
                            <cfset form.listMLP = []/>
                        </cfif>
                        <!--- active user keywords, mlp, short url --->
                        <cfif arrayLen(form.listKeyword) GT 0 || arrayLen(form.listShortUrl) GT 0 || arrayLen(form.listMLP) GT 0 >
                            <cfinvoke component="session.sire.models.cfc.order_plan" method="activeUserData" returnvariable="resultActiveUserData">
                                <cfinvokeargument name="inpListKeyword" value="#form.listKeyword#"/>
                                <cfinvokeargument name="inpListShortUrl" value="#form.listShortUrl#"/>
                                <cfinvokeargument name="inpListMLP" value="#form.listMLP#"/>
                            </cfinvoke>
                        </cfif>

                        <cftransaction action="commit"/>
                        
                        <cfset dataout.RXRESULTCODE = 1>
                        <cfset dataout.MSG = "Transaction Complete.">

                        <cfinvoke method="mailChimpAPIs" component="public.sire.models.cfc.mailchimp" returnvariable="RetVarmailChimpAPIs">
                            <cfinvokeargument name="InpEmailString" value="#arguments.inpUserEmail#">
                            <cfinvokeargument name="InpAccountType" value="#getPlan.PLANNAME#">         
                        </cfinvoke>

                        <cfcatch type="any">
                            <cftransaction action="rollback"/>
                            <cfset dataout.RXRESULTCODE = -1>
                            <cfset dataout.MESSAGE = "Transaction Failed!">
                            <cfset dataout.ERRMESSAGE =  cfcatch.message & ": " & cfcatch.detail>
                        </cfcatch>
                    </cftry>
                </cftransaction>
            </cfif>

            <cfcatch>
                <cfset dataout = {}>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>
    <cffunction 
        name="getUsers"
        access="remote"
        returntype="struct" 
        output="false"
        hint="Return new users list."
        >
                		
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="iSortCol_1" default="-1">
        <cfargument name="sSortDir_1" default="">
        <cfargument name="iSortCol_2" default="-1">
        <cfargument name="sSortDir_2" default="">
        <cfargument name="iSortCol_0" default="-1">
        <cfargument name="sSortDir_0" default="">
        <cfargument name="customFilter" default="">

        <cfargument name="inpSkipPaging" default="0"/>

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.RESULT = []/>

        <cfset var item = {}/>
        <cfset var getNewUsers = ''/>
        <cfset var filterItem = '' />
        <cfset var countReport = '' />
		<cfset var rsCount=''/>		
		<cfset var ListAllSubAccSire=''/>

        <cfset dataout['datalist'] = []/>
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        

        <cftry>            

            <cfquery name="getNewUsers" datasource="#Session.DBSourceREAD#">
                SELECT 
					SQL_CALC_FOUND_ROWS
                    ua.UserId_int,
                    ua.Active_int,
                    ua.EmailAddress_vch,
                    ua.FirstName_vch,
                    ua.LastName_vch,
                    ua.Created_dt,
                    ua.LastLogIn_dt,
                    ua.MFAContactString_vch,
                    ua.ThresholdAlerts_int,
                    up.FirstSMSIncluded_int
                FROM 
                    simpleobjects.useraccount ua
                INNER JOIN
                    simplebilling.userplans up
                ON
                    ua.UserId_int = up.UserId_int
                AND
                    up.Status_int=1         
                WHERE 
                    ua.Active_int = 1
                AND
                    ua.IsTestAccount_ti = 0                
                AND
					ua.HigherUserID_int =<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#" >
                    <cfif customFilter NEQ "">
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                            AND 
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                            <cfelse>
                            AND 
                                #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfif>
                ORDER BY
                    ua.UserId_int DESC                
                    LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#"> OFFSET <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">                
            </cfquery>
			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>

            <cfset dataout["iTotalDisplayRecords"] = rsCount.iTotalRecords/>
            <cfset dataout["iTotalRecords"] = rsCount.iTotalRecords/>
			
        
            <cfif getNewUsers.RECORDCOUNT GT 0>
                <cfloop query="getNewUsers">
                    <cfset var tempItem = {
                        USERID = '#getNewUsers.UserId_int#',
                        USEREMAIL =  '#getNewUsers.EmailAddress_vch#',
                        REGISTEREDDATE = '#DateFormat(getNewUsers.Created_dt, "yyyy-mm-dd")#',
                        USERNAME = "#getNewUsers.FirstName_vch# #getNewUsers.LastName_vch#",
                        FIRSTNAME = "#getNewUsers.FirstName_vch#",
                        LASTNAME = "#getNewUsers.LastName_vch#",
                        LASTLOGIN = "#DateFormat(getNewUsers.LastLogIn_dt, "yyyy-mm-dd")# #TimeFormat(getNewUsers.LastLogIn_dt, "HH:mm:ss")#",
                        STATUS = "#getNewUsers.Active_int#",
                        CONTACTSTRING = "#getNewUsers.MFAContactString_vch#",
                        THRESHOLD= ThresholdAlerts_int,
                        FIRSTSMSINCLUDE=FirstSMSIncluded_int
                    } />
                    <cfset ArrayAppend(dataout["datalist"],tempItem)>
                </cfloop>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
                <cfset dataout.TYPE = "#cfcatch.TYPE#"/>
            </cfcatch>
        </cftry>
        
        <cfreturn dataout/>

    </cffunction>
    <cffunction name="GetUserBillingInfo" access="remote" output="true" hint="get user billing information">    
        <cfargument name="inpUserID" type="numeric" required="true"/>    
        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = ''>
        <cfset dataout.ERRMESSAGE = ''>    
        <cfset dataout.THRESHOLD=0>    
        <cfset dataout.BALANCE=0>
        <cfset var GetUserBillingInfo=''/>
        <cfset var CheckPlanExpiry=''/>
        <cftry>
            <cfquery name="CheckPlanExpiry" datasource="#Session.DBSourceREAD#">
                SELECT
                    UserId_int
                FROM
                    simplebilling.userplans
                WHERE
                    NOW() BETWEEN StartDate_dt AND EndDate_dt
                AND
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">   
                AND
                    Status_int=1
            </cfquery>
            <cfif CheckPlanExpiry.RECORDCOUNT EQ 0>
                <cfthrow type = "any" message = "Cannot Update Billing Info for expired plan" detail = "Cannot Update Billing Info for expired plan" >
            </cfif>
            <cfquery name="GetUserBillingInfo" datasource="#Session.DBSourceREAD#">
                SELECT
                    u.ThresholdAlerts_int,
                    up.FirstSMSIncluded_int
                FROM
                    simpleobjects.useraccount u
                INNER JOIN
                    simplebilling.userplans up
                ON
                    u.UserId_int = up.UserId_int
                WHERE
                    u.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">       
                AND
                    up.Status_int=1         
            </cfquery>
            <cfset dataout.THRESHOLD=GetUserBillingInfo.ThresholdAlerts_int>    
            <cfset dataout.BALANCE=GetUserBillingInfo.FirstSMSIncluded_int>
            <cfset dataout.RXRESULTCODE = 1 />
            <cfcatch type="any">                
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>        
        <cfreturn dataout >
    </cffunction>

    <cffunction name="UpdateUserBillingInfo" access="remote" output="true" hint="Update User Billing Information">    
        <cfargument name="inpUserID" type="numeric" required="true"/>    
        <cfargument name="inpThreshold" type="numeric" required="true"/> 
        <cfargument name="inpBallance" type="numeric" required="true"/> 
        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = ''>
        <cfset dataout.ERRMESSAGE = ''>    
        
        <cfset var UpdateCredit=''/>
        <cfset var UpdateThreshold=''/>
        <cfset var CheckCurrentBalanceIsGTUpdateBallance=''/>
        <cfset var UpdateUserPlan=''/>
        <cfset var userLogOperator=""  >
        <cftry>
            
            <cfquery name="UpdateThreshold" datasource="#Session.DBSourceEBM#">
                UPDATE
                    simpleobjects.useraccount
                SET
                    ThresholdAlerts_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpThreshold#">                 
                WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">                
            </cfquery>  
            <cfquery name="UpdateUserPlan" datasource="#Session.DBSourceEBM#">
                UPDATE
                    simplebilling.userplans
                SET
                    FirstSMSIncluded_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBallance#">                 
                WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">          
                AND
                    Status_int=1      
            </cfquery>      
            <!--- <cfquery name="CheckCurrentBalanceIsGTUpdateBallance" datasource="#Session.DBSourceREAD#">
                SELECT                    
                    Balance_int
                FROM
                    simplebilling.billing b                                
                WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">      
                AND
                    Balance_int >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpBallance#">  
            </cfquery>--->
            <cfquery name="UpdateCredit" datasource="#Session.DBSourceEBM#">
                UPDATE
                    simplebilling.billing
                SET
                    Balance_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#arguments.inpBallance#">                 
                WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">                
            </cfquery>  
            <cfset userLogOperator="Master Account (#Session.UserId#) Update Billing for Sub account (#arguments.inpUserID#). Credit Limit = #arguments.inpBallance#, Threshold = #arguments.inpThreshold#"  >
            <cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
                <cfinvokeargument name="userId" value="#arguments.inpUserID#">
                <cfinvokeargument name="moduleName" value="Sub account">
                <cfinvokeargument name="operator" value="#userLogOperator#">
            </cfinvoke>  
            <cfset dataout.RXRESULTCODE = 1 />
            <cfcatch type="any">                
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>        
        <cfreturn dataout >
    </cffunction>

    <cffunction name="captureUserCCData" access="public" hint="save user cc data to db">
        <cfargument name="inpUserID" type="numeric" required="true" default="SESSION.USERID"/>    
        <cfargument name="inpCardObject" TYPE="string" required="true" default="">
        <cfargument name="inpPaymentGateway" TYPE="numeric" required="true" default="3">

        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = 0>
        <cfset dataout.MESSAGE = ''>
        <cfset dataout.ERRMESSAGE = ''>    
        <cfset var cardinfo = {}/>
        <cfset var BODYDATA = ''/>
        <cfset var userCCInfoId = ''/>
        <cfset var RetGetUserInfor = ''/>
        <cfset var checkCaptureCC = 0/>
        <cfset var queryUserCCData	= '' />
        <cfset var captureUserCCData	= '' />
        <cfset var userInfo	= '' />
        <cfset var rxCaptureUserCCData	= '' />

        <cftry>
            <cfif isJson(arguments.inpCardObject) && arguments.inpUserID GT 0>
                <cfset cardinfo = DESERIALIZEJSON(inpCardObject)>    
                <cfsavecontent variable="BODYDATA"><cfoutput><?xml version="1.0" encoding="UTF-8"?>
                <userCCInfo>
                    <cardNumber>#cardinfo.inpNumber#</cardNumber>
                    <cardExpirationMonth>#cardinfo.inpExpireMonth#</cardExpirationMonth>
                    <cardExpirationYear>#cardinfo.inpExpreYear#</cardExpirationYear>
                    <cardCVV>#cardinfo.inpCvv2#</cardCVV>
                    <firstName>#cardinfo.inpFirstName#</firstName>
                    <lastName>#cardinfo.inpLastName#</lastName>
                    <addressLine1>#cardinfo.inpLine1#</addressLine1>
                    <city>#cardinfo.inpCity#</city>
                    <state>#cardinfo.inpState#</state>
                    <zipCode>#cardinfo.inpPostalCode#</zipCode>
                    <country>#cardinfo.inpCountryCode#</country>
                    <email>#cardinfo.inpEmail#</email>
                    <phone></phone>
                </userCCInfo></cfoutput></cfsavecontent>
            </cfif>

            <cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfoById" returnvariable="userInfo">
                <cfinvokeargument name="inpUserId" value="#arguments.inpUserID#"/>
            </cfinvoke>

            <cfif userInfo.USERID GT 0>
                <cfif userInfo.CAPTURECC EQ 0>
                    <cfreturn dataout> 
                </cfif>
            <cfelse>
                <cfreturn dataout>    
            </cfif>

            <!--- check if user Info exits --->
            <cfquery name="queryUserCCData" datasource="#Session.DBSourceEBM#">
                SELECT
                    UserCCInfo_Id
                FROM
                    simplebilling.userccinfo 
                WHERE    
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">
                AND 
                    PaymentGateway_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPaymentGateway#">
                AND 
                    Status_ti = 1
                LIMIT 1    
            </cfquery>
            <cfif queryUserCCData.RECORDCOUNT GT 0>
                <cfset userCCInfoId = queryUserCCData.UserCCInfo_Id/>

                <cfquery name="captureUserCCData" datasource="#Session.DBSourceEBM#" result="rxCaptureUserCCData">
                    UPDATE
                        simplebilling.userccinfo 
                    SET
                        XMLDataString_vch = AES_ENCRYPT(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#BODYDATA#">, '#APPLICATION.EncryptionKey_UserDB#'),
                        LastUpdateByUserId = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.USERID#">,
                        Updated_dt = NOW()
                    WHERE 
                        UserCCInfo_Id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userCCInfoId#"> 
                </cfquery>
                 <cfif rxCaptureUserCCData.RECORDCOUNT GT 0>
                    <cfset dataout.RXRESULTCODE = 1 />    
                <cfelse>
                    <cfset dataout.RXRESULTCODE = -2 />    
                </cfif>
            <cfelse>
                <cfquery name="captureUserCCData" datasource="#Session.DBSourceEBM#" result="rxCaptureUserCCData">
                    INSERT INTO 
                    simplebilling.userccinfo 
                    (
                        UserId_int,
                        XMLDataString_vch,
                        PaymentGateway_ti,
                        Status_ti,
                        Created_dt,
                        LastUpdateByUserId
                    )   
                    VALUES
                    (   
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">, 
                        AES_ENCRYPT(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#BODYDATA#">, '#APPLICATION.EncryptionKey_UserDB#'),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPaymentGateway#">,
                        1,
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.USERID#">
                    )
                </cfquery>
                <cfif rxCaptureUserCCData.GENERATED_KEY GT 0>
                    <cfset dataout.RXRESULTCODE = 1 />    
                <cfelse>
                    <cfset dataout.RXRESULTCODE = -3 />    
                </cfif>
            </cfif>
            
            
            <cfcatch type="any">                
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>

        </cftry>

        <cfreturn dataout >
    </cffunction>

    <cffunction name="getUserCCData" access="remote" hint="get user cc data from db">
        <cfargument name="inpUserID" type="numeric" required="true" default="#SESSION.USERID#"/>    
        <cfargument name="inpPaymentGateway" TYPE="numeric" required="true" default="3">

        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = 0>
        <cfset dataout.MESSAGE = ''>
        <cfset dataout.ERRMESSAGE = ''>    
        <cfset dataout.USERCCATA = {}>
        <cfset dataout.USERCCATA['cardNumber'] = ''>
        <cfset dataout.USERCCATA['cardExpirationMonth'] = ''>
        <cfset dataout.USERCCATA['cardExpirationYear'] = ''>
        <cfset dataout.USERCCATA['cardCVV'] = ''>
        <cfset dataout.USERCCATA['firstName'] = ''>
        <cfset dataout.USERCCATA['lastName'] = ''>
        <cfset dataout.USERCCATA['addressLine1'] = ''>
        <cfset dataout.USERCCATA['city'] = ''>
        <cfset dataout.USERCCATA['state'] = ''>
        <cfset dataout.USERCCATA['zipCode'] = ''>
        <cfset dataout.USERCCATA['country'] = ''>
        <cfset dataout.USERCCATA['email'] = ''>
        <cfset dataout.USERCCATA['phone'] = ''>
        <cfset var xmlData	= '' />
        <cfset var xmlDataParse	= '' />
        <cfset var cardNumber	= '' />
        <cfset var cardExpirationMonth	= '' />
        <cfset var cardExpirationYear	= '' />
        <cfset var cardCVV	= '' />
        <cfset var firstName	= '' />
        <cfset var lastName	= '' />
        <cfset var addressLine1	= '' />
        <cfset var city	= '' />
        <cfset var state	= '' />
        <cfset var zipCode	= '' />
        <cfset var country	= '' />
        <cfset var email	= '' />
        <cfset var phone	= '' />
        <cfset var getUserCC	= '' />
        <cfset var RetVarGetAdminPermission	= '' />

        <cfinvoke component="session.sire.models.cfc.admin"  method="GetAdminPermission" returnvariable="RetVarGetAdminPermission">
            <cfinvokeargument name="inpSkipRedirect" value="1"/>
        </cfinvoke>
        <cfif RetVarGetAdminPermission.ISADMINOK NEQ "1">
            <cfthrow message="You do not have permission!" />
        </cfif>

        <cftry>
            <cfquery name="getUserCC" datasource="#Session.DBSourceEBM#">
                Select 
                    UserCCInfo_Id, UserId_int, PaymentGateway_ti, AES_DECRYPT(XMLDataString_vch,'#APPLICATION.EncryptionKey_UserDB#') as XMLDataString_vch
                FROM 
                    simplebilling.userccinfo
                WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserID#">
                AND 
                    PaymentGateway_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPaymentGateway#">
                AND 
                    Status_ti = 1
            </cfquery>

            <cfif getUserCC.RecordCount GT 0>
                <cfset dataout.RXRESULTCODE = 1>
                <cfset xmlData = xmlParse(toString(getUserCC.XMLDataString_vch)) />

                <cfset xmlDataParse =  xmlParse(xmlData)>
                <cfset cardNumber = XmlSearch(xmlDataParse, "/userCCInfo/cardNumber/")>
                <cfif arrayLen(cardNumber) EQ 1>
                     <cfset dataout.USERCCATA['cardNumber'] = cardNumber[1].XmlText>
                </cfif>

                <cfset cardExpirationMonth = XmlSearch(xmlDataParse, "/userCCInfo/cardExpirationMonth/")>
                <cfif arrayLen(cardExpirationMonth) EQ 1>
                    <cfset dataout.USERCCATA['cardExpirationMonth'] = cardExpirationMonth[1].XmlText>
                </cfif>

                <cfset cardExpirationYear = XmlSearch(xmlDataParse, "/userCCInfo/cardExpirationYear/")>
                <cfif arrayLen(cardExpirationYear) EQ 1>
                    <cfset dataout.USERCCATA['cardExpirationYear'] = cardExpirationYear[1].XmlText>
                </cfif>

                <cfset cardCVV = XmlSearch(xmlDataParse, "/userCCInfo/cardCVV/")>
                <cfif arrayLen(cardCVV) EQ 1>
                    <cfset dataout.USERCCATA['cardCVV'] = cardCVV[1].XmlText>
                </cfif>

                <cfset firstName = XmlSearch(xmlDataParse, "/userCCInfo/firstName/")>
                <cfif arrayLen(firstName) EQ 1>
                    <cfset dataout.USERCCATA['firstName'] = firstName[1].XmlText>
                </cfif>

                <cfset lastName = XmlSearch(xmlDataParse, "/userCCInfo/lastName/")>
                <cfif arrayLen(lastName) EQ 1>
                    <cfset dataout.USERCCATA['lastName'] = lastName[1].XmlText>
                </cfif>

                <cfset addressLine1 = XmlSearch(xmlDataParse, "/userCCInfo/addressLine1/")>
                <cfif arrayLen(addressLine1) EQ 1>
                    <cfset dataout.USERCCATA['addressLine1'] = addressLine1[1].XmlText>
                </cfif>

                <cfset city = XmlSearch(xmlDataParse, "/userCCInfo/city/")>
                <cfif arrayLen(city) EQ 1>
                    <cfset dataout.USERCCATA['city'] = city[1].XmlText>
                </cfif>

                <cfset state = XmlSearch(xmlDataParse, "/userCCInfo/state/")>
                <cfif arrayLen(state) EQ 1>
                    <cfset dataout.USERCCATA['state'] = state[1].XmlText>
                </cfif>

                <cfset zipCode = XmlSearch(xmlDataParse, "/userCCInfo/zipCode/")>
                <cfif arrayLen(zipCode) EQ 1>
                    <cfset dataout.USERCCATA['zipCode'] = zipCode[1].XmlText>
                </cfif>

                <cfset country = XmlSearch(xmlDataParse, "/userCCInfo/country/")>
                <cfif arrayLen(country) EQ 1>
                    <cfset dataout.USERCCATA['country'] = country[1].XmlText>
                </cfif>

                <cfset email = XmlSearch(xmlDataParse, "/userCCInfo/email/")>
                <cfif arrayLen(email) EQ 1>
                    <cfset dataout.USERCCATA['email'] = email[1].XmlText>
                </cfif>


                <cfset phone = XmlSearch(xmlDataParse, "/userCCInfo/phone/")>
                <cfif arrayLen(phone) EQ 1>
                    <cfset dataout.USERCCATA['phone'] = phone[1].XmlText>
                </cfif>
            <cfelse>
                 <cfset dataout.RXRESULTCODE = -2>   
            </cfif>  

            <cfcatch>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>



</cfcomponent>
