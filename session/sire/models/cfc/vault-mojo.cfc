<cfcomponent>

	<cfinclude template="/public/paths.cfm" >
    <cfinclude template="/session/cfc/csc/constants.cfm">
    <cfinclude template="/session/sire/configs/credits.cfm"> 
    <!--- insert a credit card ---> 
    <cffunction name="TokenRegisterCardNumber" access="remote" output="false" hint="Token Register Card Number">
		<cfargument name="inporderId" TYPE="string" required="false" default="#Left(CreateUUID(),25)#">
        <cfargument name="inpCardObject" TYPE="string" required="true" default="">
		<cfargument name="inpUserId" TYPE="string" required="false" default="#Session.UserId#">

        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "Fail to create new Vault">
		<cfset dataout.USERCARDID = ""/>
        <cfset dataout.CCTYPE = ""/>

        
        <cfset var BODYDATA = "">
        <cfset var BODYDATATEMP = "">
        
		<cfset var returndata = ""/>
        <cfset var RegisterCardNumber = ""/>
        <cfset var selectedToken = ""/>
        <cfset var selectedCCType = ""/>
        <cfset var datainfor = DESERIALIZEJSON(inpCardObject)>
        <cfset var CCType = ''/>
        <cfset var RxcreateCreditCardVault = {}/>

        <cfset var statusMessage= ''>
        <cfset var httpStatusCode= ''>
        <cfset var processorResponse= ''>

        <cftry>			           
            <cfsavecontent variable="BODYDATA"><cfoutput>
                <UAPIRequest>
                    <transaction>
                        <process>online</process>
                        <type>tokenize</type> 
                    </transaction>
                    <requestData>
                        <orderId>#arguments.inporderId#</orderId>
                        <cardNumber>#datainfor.inpNumber#</cardNumber>
                    </requestData>
                </UAPIRequest></cfoutput></cfsavecontent>
            <cfsavecontent variable="BODYDATATEMP"><cfoutput>
                <UAPIRequest>
                    <transaction>
                        <process>online</process>
                        <type>tokenize</type> 
                    </transaction>
                    <requestData>
                        <orderId>#arguments.inporderId#</orderId>
                        <cardNumber>XXXXXXXXXXXX#right(datainfor.inpNumber,4)#</cardNumber>
                    </requestData>
                </UAPIRequest></cfoutput></cfsavecontent>
            
            <!--- Update vault customer --->
            <cfinvoke method="createCreditCardVault" returnvariable="RxcreateCreditCardVault">
                <cfinvokeargument name="inpFirstNameCard" value="#datainfor.inpFirstName#">
                <cfinvokeargument name="inpLastNameCard" value="#datainfor.inpLastName#">
                <cfinvokeargument name="inpCreditCardNumber" value="#datainfor.inpNumber#">
                <cfinvokeargument name="inpExpirationDate" value="#datainfor.inpExpireMonth##right(datainfor.inpExpreYear,2)#"> 
                <cfinvokeargument name="inpCardSecurityCode" value="#datainfor.inpCvv2#">
                <cfinvokeargument name="inpZipPostalCodeCard" value="#datainfor.inpPostalCode#"> 
            </cfinvoke> 

			<cfhttp url="#mojo_url#" method="post" result="RegisterCardNumber" timeout="30">
				<cfhttpparam type="header" name="Authorized" value="#mojo_token#">
				<cfhttpparam type="header" name="Content-Type" value="application/xml">
				<cfhttpparam type="body" value='#BODYDATA#'>
			</cfhttp>

            <cfif structKeyExists(RegisterCardNumber, "status_code") AND RegisterCardNumber.status_code EQ 200>                
                <cfset httpStatusCode= RegisterCardNumber.status_code>
                
                               
                <cfif isXML(RegisterCardNumber.fileContent)>
                    <cfset returndata =  xmlParse(RegisterCardNumber.fileContent)>
                    <cfset selectedToken = XmlSearch(returndata, "/UAPIResponse/token/")>
                    <cfset selectedCCType = XmlSearch(returndata, "/UAPIResponse/type/")>   
                    <cfset processorResponse = XmlSearch(returndata, "/UAPIResponse/processorResponse/")>                 
                    <cfset statusMessage = XmlSearch(returndata, "/UAPIResponse/statusMessage/")>                 

                    <cfset dataout.USERCARDID = selectedToken[1].XmlText/>
                    <cfset CCType = selectedCCType[1].XmlText/>
                    <cfswitch expression="#Trim(CCType)#"> 
                        <cfcase value="VI"> 
                            <cfset dataout.CCTYPE = "VISA"/>
                        </cfcase> 
                        <cfcase value="MC"> 
                            <cfset dataout.CCTYPE = "MASTERCARD"/>
                        </cfcase> 
                        <cfcase value="DI"> 
                            <cfset dataout.CCTYPE = "DISCOVER"/>
                        </cfcase> 
                         <cfcase value="AX"> 
                            <cfset dataout.CCTYPE = "AMERICANEXPRESS"/>
                        </cfcase>
                        <cfdefaultcase>
                            <cfset dataout.CCTYPE = "VISA"/> 
                        </cfdefaultcase> 
                    </cfswitch>                     
                    <cfset processorResponse = processorResponse[1].XmlText/>
                    <cfset statusMessage = statusMessage[1].XmlText/>
                </cfif>
               
        		<cfset dataout.RXRESULTCODE = 1 /> 
                <cfset dataout.RESULT = "SUCCESS">                    
                <cfset dataout.MESSAGE = "New card was created">
            <cfelse>
            	<cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <cfset dataout.MESSAGE = "Error while create new">
            </cfif>
            <!--- Log payment --->
            
            <cftry>
                <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
                    <cfinvokeargument name="moduleName" value="Update Vault">
                    <cfinvokeargument name="status_code" value="#httpStatusCode#">
                    <cfinvokeargument name="status_text" value="#processorResponse#">
                    <cfinvokeargument name="errordetail" value="#statusMessage#">
                    <cfinvokeargument name="filecontent" value="#returndata#">
                    <cfinvokeargument name="paymentdata" value="#BODYDATATEMP#">
                    <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    <cfinvokeargument name="paymentmethod" value="#_MOJO_GATEWAY#">
                </cfinvoke>
                <cfcatch type="any">
                </cfcatch>
            </cftry>

        	<cfcatch type="any">
        		<cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <cfset dataout.MESSAGE = cfcatch.MESSAGE> 				
        	</cfcatch>

        </cftry>
		<cfreturn dataout />
    </cffunction>

     <!--- get detail a credit card ---> 
    <cffunction name="getDetailCreditCardInVault" access="remote" output="false" hint="get detail of credit card in vault">
    	<cfargument name="inpCardId" type="string" required="false" default="">
		<cfargument name="inpUserId" TYPE="string" required="false" default="#session.UserId#">

    	<cfset var dataout = structNew()>
		<cfset var CheckExistsPaymentMethod = ""/>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.VAULTID = ''>        
        
        <cfset dataout.DATA = ''>
        <cfset dataout.MESSAGE = "Fail to get detail a card Vault">
		<cfset dataout.CUSTOMERINFO = structNew()>
        <cfset var content = ''>

        <cftry>
			<cfquery name="CheckExistsPaymentMethod" datasource="#Session.DBSourceEBM#">
				SELECT
					extraNote,
                    PaymentMethodID_vch
				FROM
					simplebilling.authorize_user
				WHERE
					UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"> 		
				AND 
					paymentGateway_ti = 3	
				AND 
					status_ti = 1	
			</cfquery>
			<cfif CheckExistsPaymentMethod.RecordCount GT 0>
                    <cfset dataout.VAULTID = CheckExistsPaymentMethod.PaymentMethodID_vch>
					<cfset dataout.RXRESULTCODE = 1 /> 
					<cfset dataout.RESULT = 'SUCCESS'>
					<cfset dataout.MESSAGE = 'Get ok'>    
					<cfset content= DeserializeJSON(CheckExistsPaymentMethod.extraNote)/>

					<cfset dataout.CUSTOMERINFO.firstName = content.INPFIRSTNAME/> 
					<cfset dataout.CUSTOMERINFO.lastName = content.INPLASTNAME>
                    <cfif structKeyExists(content, "INPEMAIL")>
					   <cfset dataout.CUSTOMERINFO.emailAddress = content.INPEMAIL>
                    <cfelse>
                        <cfset dataout.CUSTOMERINFO.emailAddress = ''>   
                    </cfif>
					<cfset dataout.CUSTOMERINFO.expirationDate = content.INPEXPIREMONTH &'/'& content.INPEXPREYEAR>                    
					<cfset dataout.CUSTOMERINFO.maskedNumber = "XXXXXXXX" & content.INPNUMBER>
					<cfset dataout.CUSTOMERINFO.line1 = content.INPLINE1>
					<cfset dataout.CUSTOMERINFO.city = content.INPCITY>
					<cfset dataout.CUSTOMERINFO.state = content.INPSTATE>
					<cfset dataout.CUSTOMERINFO.zip = content.INPPOSTALCODE>
					<cfset dataout.CUSTOMERINFO.country = content.INPCOUNTRYCODE> 
					<cfset dataout.CUSTOMERINFO.phone = ''>
					<cfset dataout.CUSTOMERINFO.paymentType = 'card'>
                    <cfif structKeyExists(content, "inpAVS")>					   
                       <cfset dataout.CUSTOMERINFO.AVSCODE = content.inpAVS>
                    <cfelse>
                        <cfset dataout.CUSTOMERINFO.AVSCODE = "">
                    </cfif>
                    
					<cfset dataout.CUSTOMERINFO.cvv = content.INPCVV2>

                    <cfswitch expression="#TRIM(ucase(content.INPTYPE))#"> 
                        <cfcase value="VI"> 
                            <cfset dataout.CUSTOMERINFO.creditCardType = "VISA"/>
                        </cfcase> 
                        <cfcase value="MC"> 
                            <cfset dataout.CUSTOMERINFO.creditCardType = "MASTERCARD"/>
                        </cfcase> 
                        <cfcase value="DI"> 
                            <cfset dataout.CUSTOMERINFO.creditCardType = "DISCOVER"/>
                        </cfcase> 
                         <cfcase value="AX"> 
                            <cfset dataout.CUSTOMERINFO.creditCardType = "AMERICANEXPRESS"/>
                        </cfcase>
                        <cfdefaultcase>
                            <cfset dataout.CUSTOMERINFO.creditCardType = "VISA"/> 
                        </cfdefaultcase> 
                    </cfswitch>     					
            <cfelse>
                <cfset dataout.RESULT = "">
                <cfset dataout.RXRESULTCODE = 0>
                <cfset dataout.MESSAGE = "Get Card Detail Fail">
            </cfif>

        	<cfcatch type="any">
        		<cfset dataout.RESULT = "FAIL">
		        <cfset dataout.RXRESULTCODE = -1>
		        <cfset dataout.MESSAGE = "Exception">
        	</cfcatch>
        </cftry>
		<cfreturn dataout />
    </cffunction>

    <cffunction name="Sale" access="remote" output="false" hint="Save with mojo">
        <cfargument name="inpUserId" TYPE="string" required="false" default="#Session.UserId#">
		<cfargument name="inporderId" TYPE="string" required="false" default="#Left(CreateUUID(),25)#">
        <cfargument name="inpcardNumber" TYPE="string" required="true" default="">		

        <cfargument name="inporderAmount" TYPE="string" required="true" default="0.00">
        <cfargument name="inpcurrencyCode" TYPE="string" required="false" default="840">
        <cfargument name="inpcardExpiration" TYPE="string" required="true" default="">
        <cfargument name="inpcardCVV" TYPE="string" required="true" default="">

        <cfargument name="inpbillingFirstName" TYPE="string" required="true" default="">
        <cfargument name="inpbillingLastName" TYPE="string" required="true" default="">
        <cfargument name="inpbillingAddressLine1" TYPE="string" required="true" default="">
        <cfargument name="inpbillingCity" TYPE="string" required="true" default=" ">
        <cfargument name="inpbillingState" TYPE="string" required="true" default=" ">
        <cfargument name="inpbillingZipCode" TYPE="string" required="true" default=" ">
        <cfargument name="inpbillingCountry" TYPE="string" required="true" default=" ">
        <cfargument name="inpbillingEmail" TYPE="string" required="false" default=" ">
        <cfargument name="inpbillingPhone" TYPE="string" required="false" default=" ">

        <cfargument name="inpshippingFirstName" TYPE="string" required="false" default=" ">
        <cfargument name="inpshippingLastName" TYPE="string" required="false" default=" ">
        <cfargument name="inpshippingAddressLine1" TYPE="string" required="false" default=" ">
        <cfargument name="inpshippingAddressLine2" TYPE="string" required="false" default=" ">
        <cfargument name="inpshippingCity" TYPE="string" required="false" default=" ">
        <cfargument name="inpshippingState" TYPE="string" required="false" default=" ">
        <cfargument name="inpshippingZipCode" TYPE="string" required="false" default=" ">
        <cfargument name="inpshippingCountry" TYPE="string" required="false" default=" ">
        <cfargument name="inpshippingEmail" TYPE="string" required="false" default=" ">
        <cfargument name="inpshippingPhone" TYPE="string" required="false" default=" ">
        <cfargument name="inpModuleName" TYPE="string" required="false" default="Buy Credits and Keywords">
                

        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">
		<cfset dataout.STATUSCODE = ""/>
        <cfset dataout.PROCESSORREPONSECODE = ""/>

        <cfset dataout.TRANSACTIONID = "">

        <cfset dataout.REPORT = {}/>
        <cfset dataout.REPORT.ORDERID = ''>
        <cfset dataout.REPORT.TRANSACTIONID = '' >
        <cfset dataout.REPORT.AMT = ''>
        <cfset dataout.REPORT.RESPONSE = ''> 

        <cfset dataout.REPORT.INPNUMBER = ''>
        <cfset dataout.REPORT.INPCVV2 = '' >
        <cfset dataout.REPORT.INPFIRSTNAME = '' >
        <cfset dataout.REPORT.INPLASTNAME =  ''>
        <cfset dataout.REPORT.INPLINE1 = '' >
        <cfset dataout.REPORT.INPCITY = ''>
        <cfset dataout.REPORT.INPSTATE = ''>
        <cfset dataout.REPORT.INPPOSTALCODE = '' >
        <cfset dataout.REPORT.PHONE = ''> 
        <cfset dataout.REPORT.EMAIL = ''>
        <cfset dataout.REPORT.TRANSACTIONTYPE = ''> 
        <cfset dataout.REPORT.METHOD = ''>

        <cfset var BODYDATA = "">
        <cfset var BODYDATATEMP = ""/>
		<cfset var returndata = ""/>
        <cfset var sale = ""/>
        <cfset var myXMLDocument=''>
        <cfset var selectedElements=''>        
        <Cfset var processorResponse = ''/>
        <Cfset var statusMessage = ''/>
        <Cfset var fileContent = ''/>
        <Cfset var httpStatusCode = ''/>
        <Cfset var dateFromCcInput = ''/>
        <cfset var selectStatusMessage = ''/>
        <cfset var selectProcessorResponse = ''/>
        <cfset var selectTransactionId = ''/>

        <cftry>            
            <cfif IsNumeric(arguments.inpbillingState) OR IsNumeric(arguments.inpbillingAddressLine1) OR IsNumeric(arguments.inpbillingCity)>
                <cfthrow  type = "any" message = "Billing Address, City, State should be string" detail = "Billing Address, City, State should be string" > 
            </cfif>
            
            <cfif Len(arguments.inpbillingState) GT 2>
                <cfthrow  type = "any" message = "Billing State should be 2 characters only" detail = "Billing State should be 2 characters only" > 
            </cfif>
            <cfif Len(arguments.inpbillingAddressLine1) GT 35>
                <cfthrow  type = "any" message = "Billing Address should be 35 characters only" detail = "Billing Address should be 35 characters only" > 
            </cfif>
            <cfif Len(arguments.inpbillingCity) GT 20>
                <cfthrow  type = "any" message = "Billing City should be 20 characters only" detail = "Billing City should be 20 characters only" > 
            </cfif>
            <!--- check for same month year compage with last day of month, if not compare first day is enough---->            
           <cfif Int(Month(NOW())) EQ Int(left(arguments.inpcardExpiration,2)) AND Int(right(Year(NOW()),2)) EQ Int(right(arguments.inpcardExpiration,2))>
                <cfset dateFromCcInput= createDate("20" & right(arguments.inpcardExpiration,2), left(arguments.inpcardExpiration,2), daysInMonth(NOW()))>
            <cfelse>            
                <cfset dateFromCcInput= createDate("20" & right(arguments.inpcardExpiration,2), left(arguments.inpcardExpiration,2), 28)>
            </cfif>
            <cfif DateCompare(dateFromCcInput, NOW()) LT 1>
                <cfthrow  type = "any" message = "Your Credit Card has been expired! Please check the Expiration Date or add another Credit Card." detail = "Your Credit Card has been expired! Please check the Expiration Date or add another Credit Card." > 
            </cfif>

            <cfsavecontent variable="BODYDATA"><cfoutput><?xml version="1.0" encoding="UTF-8"?>
                <UAPIRequest>
                    <transaction>
                        <process>online</process>
                        <type>sale</type>
                    </transaction>
                    <requestData>
                        <orderId>#arguments.inporderId#</orderId>
                        <orderAmount>#arguments.inporderAmount#</orderAmount>
                        <currencyCode>#arguments.inpcurrencyCode#</currencyCode>
                        <billing>
                            <billingFirstName>#arguments.inpbillingFirstName#</billingFirstName>
                            <billingLastName>#arguments.inpbillingLastName#</billingLastName>
                            <billingAddressLine1>#arguments.inpbillingAddressLine1#</billingAddressLine1>
                            <billingCity>#arguments.inpbillingCity#</billingCity>
                            <billingState>#arguments.inpbillingState#</billingState>
                            <billingZipCode>#arguments.inpbillingZipCode#</billingZipCode>
                            <billingCountry>#arguments.inpbillingCountry#</billingCountry>
                            #trim(arguments.inpbillingEmail) NEQ ""? "<billingEmail>#arguments.inpbillingEmail#</billingEmail>":""#
                            #trim(arguments.inpbillingPhone) NEQ ""? "<billingPhone>#arguments.inpbillingPhone#</billingPhone>":""#
                            
                        </billing>
                        <shipping>
                            #trim(arguments.inpshippingFirstName) NEQ ""? "<shippingFirstName>#arguments.inpshippingFirstName#</shippingFirstName>":""#
                            #trim(arguments.inpshippingLastName) NEQ ""? "<shippingLastName>#arguments.inpshippingLastName#</shippingLastName>":""#
                            #trim(arguments.inpshippingAddressLine1) NEQ ""? "<shippingAddressLine1>#arguments.inpshippingAddressLine1#</shippingAddressLine1>":""#
                            #trim(arguments.inpshippingAddressLine2) NEQ ""? "<shippingAddressLine2>#arguments.inpshippingAddressLine2#</shippingAddressLine2>":""#
                            #trim(arguments.inpshippingCity) NEQ ""? "<shippingCity>#arguments.inpshippingCity#</shippingCity>":""#
                            #trim(arguments.inpshippingState) NEQ ""? "<shippingState>#arguments.inpshippingState#</shippingState>":""#
                            #trim(arguments.inpshippingZipCode) NEQ ""? "<shippingZipCode>#arguments.inpshippingZipCode#</shippingZipCode>":""#
                            #trim(arguments.inpshippingCountry) NEQ ""? "<shippingCountry>#arguments.inpshippingCountry#</shippingCountry>":""#
                            #trim(arguments.inpshippingEmail) NEQ ""? "<shippingEmail>#arguments.inpshippingEmail#</shippingEmail>":""#
                            #trim(arguments.inpshippingPhone) NEQ ""? "<shippingPhone>#arguments.inpshippingPhone#</shippingPhone>":""#                                                                                        
                        </shipping>
                        <card>
                            <cardNumber>#arguments.inpcardNumber#</cardNumber>
                            <cardExpiration>#arguments.inpcardExpiration#</cardExpiration>
                            <cardCVV>#arguments.inpcardCVV#</cardCVV>
                        </card>
                    </requestData>
                </UAPIRequest></cfoutput></cfsavecontent>    

            <cfsavecontent variable="BODYDATATEMP"><cfoutput><?xml version="1.0" encoding="UTF-8"?>
                <UAPIRequest>
                    <transaction>
                        <process>online</process>
                        <type>sale</type>
                    </transaction>
                    <requestData>
                        <orderId>#arguments.inporderId#</orderId>
                        <orderAmount>#arguments.inporderAmount#</orderAmount>
                        <currencyCode>#arguments.inpcurrencyCode#</currencyCode>
                        <billing>
                            <billingFirstName>#arguments.inpbillingFirstName#</billingFirstName>
                            <billingLastName>#arguments.inpbillingLastName#</billingLastName>
                            <billingAddressLine1>#arguments.inpbillingAddressLine1#</billingAddressLine1>
                            <billingCity>#arguments.inpbillingCity#</billingCity>
                            <billingState>#arguments.inpbillingState#</billingState>
                            <billingZipCode>#arguments.inpbillingZipCode#</billingZipCode>
                            <billingCountry>#arguments.inpbillingCountry#</billingCountry>
                            #trim(arguments.inpbillingEmail) NEQ ""? "<billingEmail>#arguments.inpbillingEmail#</billingEmail>":""#
                            #trim(arguments.inpbillingPhone) NEQ ""? "<billingPhone>#arguments.inpbillingPhone#</billingPhone>":""#
                            
                        </billing>
                        <shipping>
                            #trim(arguments.inpshippingFirstName) NEQ ""? "<shippingFirstName>#arguments.inpshippingFirstName#</shippingFirstName>":""#
                            #trim(arguments.inpshippingLastName) NEQ ""? "<shippingLastName>#arguments.inpshippingLastName#</shippingLastName>":""#
                            #trim(arguments.inpshippingAddressLine1) NEQ ""? "<shippingAddressLine1>#arguments.inpshippingAddressLine1#</shippingAddressLine1>":""#
                            #trim(arguments.inpshippingAddressLine2) NEQ ""? "<shippingAddressLine2>#arguments.inpshippingAddressLine2#</shippingAddressLine2>":""#
                            #trim(arguments.inpshippingCity) NEQ ""? "<shippingCity>#arguments.inpshippingCity#</shippingCity>":""#
                            #trim(arguments.inpshippingState) NEQ ""? "<shippingState>#arguments.inpshippingState#</shippingState>":""#
                            #trim(arguments.inpshippingZipCode) NEQ ""? "<shippingZipCode>#arguments.inpshippingZipCode#</shippingZipCode>":""#
                            #trim(arguments.inpshippingCountry) NEQ ""? "<shippingCountry>#arguments.inpshippingCountry#</shippingCountry>":""#
                            #trim(arguments.inpshippingEmail) NEQ ""? "<shippingEmail>#arguments.inpshippingEmail#</shippingEmail>":""#
                            #trim(arguments.inpshippingPhone) NEQ ""? "<shippingPhone>#arguments.inpshippingPhone#</shippingPhone>":""#                                                                                        
                        </shipping>
                        <card>
                            <cardNumber>#RIGHT(arguments.inpcardNumber,4)#</cardNumber>
                            <cardExpiration>#arguments.inpcardExpiration#</cardExpiration>
                            <cardCVV>#arguments.inpcardCVV#</cardCVV>
                        </card>
                    </requestData>
                </UAPIRequest></cfoutput></cfsavecontent>  

			<cfhttp url="#mojo_url#" method="post" result="sale" timeout="30">
				<cfhttpparam type="header" name="Authorized" value="#mojo_token#">
				<cfhttpparam type="header" name="Content-Type" value="application/xml">
				<cfhttpparam type="body" value='#BODYDATA#'>
			</cfhttp>            

            <cfset fileContent = sale.fileContent/>  
            <cfset statusMessage = sale.status_text />
            <!--- statusCode:0 ---->
            <!--- processorResponse:Approved--->
            <cfset httpStatusCode = sale.status_code/>            			 
            <cfif structKeyExists(sale, "status_code") AND sale.status_code EQ 200>                
                <cfif isXML(fileContent)>
                    <cfset myXMLDocument = XmlParse(fileContent)> 
                    <cfset selectedElements = XmlSearch(myXMLDocument, "/UAPIResponse/statusCode")>
                    <cfif ArrayLen(selectedElements) GT 0>
                        <cfset dataout.STATUSCODE = selectedElements[1].XmlText/>            
                    </cfif>
                    <cfset selectedElements = XmlSearch(myXMLDocument, "/UAPIResponse/processorResponseCode")>
                    <cfif ArrayLen(selectedElements) GT 0>
                        <cfset dataout.PROCESSORREPONSECODE = selectedElements[1].XmlText/>
                        <cfset selectStatusMessage = XmlSearch(myXMLDocument, "/UAPIResponse/statusMessage")>
                        <cfif ArrayLen(selectStatusMessage) GT 0>
                            <cfset statusMessage = selectStatusMessage[1].XmlText/>
                        </cfif>

                        <cfset selectProcessorResponse = XmlSearch(myXMLDocument, "/UAPIResponse/processorResponse")>
                        <cfif ArrayLen(selectProcessorResponse) GT 0>
                            <cfset processorResponse = selectProcessorResponse[1].XmlText/>
                        </cfif>
                        <cfif selectedElements[1].XmlText EQ "000">
                            <cfset selectTransactionId = XmlSearch(myXMLDocument, "/UAPIResponse/transactionId")>
                            <cfset dataout.RXRESULTCODE = 1 /> 
                            <cfset dataout.RESULT = 'SUCCESS'>                    
                            <cfset dataout.MESSAGE = "Payment successfully">                
                            <cfset dataout.TRANSACTIONID = selectTransactionId[1].XmlText /> 

                            <!--- LOG TO PAMENT SUCCESS --->
                            <cfset dataout.REPORT.ORDERID = arguments.inpOrderId>
                            <cfset dataout.REPORT.TRANSACTIONID = dataout.TRANSACTIONID >
                            <cfset dataout.REPORT.AMT = arguments.inpOrderAmount>
                            <cfset dataout.REPORT.RESPONSE = processorResponse> 

                            <cfset dataout.REPORT.INPNUMBER =  right(arguments.inpcardNumber, 4) >
                            <cfset dataout.REPORT.INPCVV2 = arguments.inpcardCVV >
                            <cfset dataout.REPORT.INPFIRSTNAME = arguments.inpbillingFirstName >
                            <cfset dataout.REPORT.INPLASTNAME =  arguments.inpbillingLastName>
                            <cfset dataout.REPORT.INPLINE1 = arguments.inpbillingAddressLine1 >
                            <cfset dataout.REPORT.INPCITY = arguments.inpbillingCity>
                            <cfset dataout.REPORT.INPSTATE = arguments.inpbillingState>
                            <cfset dataout.REPORT.INPPOSTALCODE = arguments.inpbillingZipCode >
                            <cfset dataout.REPORT.PHONE = ''> 
                            <cfset dataout.REPORT.EMAIL = arguments.inpbillingEmail>
                            <cfset dataout.REPORT.TRANSACTIONTYPE = 'sale'> 
                            <cfset dataout.REPORT.METHOD = 'CC'>
                         <cfelse>
                            <cfset dataout.RESULT = "Fail">
                            <cfset dataout.RXRESULTCODE = -2>
                            <!--- <cfset dataout.MESSAGE = "Error while create payment with processorResponseCode."&processorResponse> --->
                            <cfif TRIM(processorResponse) NEQ "">
                                <cfset dataout.MESSAGE = HTMLEditFormat(processorResponse)>   
                            <cfelse>
                                <cfset dataout.MESSAGE = HTMLEditFormat(statusMessage)>   
                            </cfif>  
                        </cfif>
                    <cfelse>
                        <cfset selectedElements = XmlSearch(myXMLDocument, "/UAPIResponse")>
                        <cfif ArrayLen(selectedElements) GT 0>
                            <cfset selectStatusMessage = XmlSearch(myXMLDocument, "/UAPIResponse/statusMessage")>
                            <cfif ArrayLen(selectStatusMessage) GT 0>
                                <cfset statusMessage = selectStatusMessage[1].XmlText/>
                            </cfif>

                            <cfset selectProcessorResponse = XmlSearch(myXMLDocument, "/UAPIResponse/processorResponse")>
                            <cfif ArrayLen(selectProcessorResponse) GT 0>
                                <cfset processorResponse = selectProcessorResponse[1].XmlText/>
                            </cfif>
                        </cfif>

                        <cfset dataout.RXRESULTCODE = -5 />
                        <cfset dataout.RESULT = 'FAIL'>                    
                        <!--- <cfset dataout.MESSAGE = "Error while create payment with statusMessage."&HTMLEditFormat(statusMessage)>  --->
                        <cfif TRIM(processorResponse) NEQ "">
                            <cfset dataout.MESSAGE = HTMLEditFormat(processorResponse)>   
                        <cfelse>
                            <cfset dataout.MESSAGE = HTMLEditFormat(statusMessage)>   
                        </cfif>
                    </cfif>
                <cfelse>
                    <cfset dataout.RXRESULTCODE = -4 />
                    <cfset dataout.RESULT = 'FAIL'>                    
                    <!--- <cfset dataout.MESSAGE = "Error while create payment with statusMessage."&HTMLEditFormat(statusMessage)>  --->
                    <cfset dataout.MESSAGE = "Invalid XML response">   
                </cfif>         		
            <cfelse>
                <cfif isXML(fileContent)>
                    <cfset myXMLDocument = XmlParse(fileContent)> 
                    <cfset selectedElements = XmlSearch(myXMLDocument, "/UAPIResponse")>
                    <cfif ArrayLen(selectedElements) GT 0>
                        <cfset selectStatusMessage = XmlSearch(myXMLDocument, "/UAPIResponse/statusMessage")>
                        <cfif ArrayLen(selectStatusMessage) GT 0>
                            <cfset statusMessage = selectStatusMessage[1].XmlText/>
                        </cfif>

                        <cfset selectProcessorResponse = XmlSearch(myXMLDocument, "/UAPIResponse/processorResponse")>
                        <cfif ArrayLen(selectProcessorResponse) GT 0>
                            <cfset processorResponse = selectProcessorResponse[1].XmlText/>
                        </cfif>
                    </cfif>
                </cfif>    
                    
            	<cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <!--- <cfset dataout.MESSAGE = "Error while create payment with status msg."&HTMLEditFormat(statusMessage)&"" > --->
                <cfset dataout.MESSAGE = HTMLEditFormat(statusMessage)>
            </cfif>

            <!--- Log payment --->
            <cftry>
                <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
                    <cfinvokeargument name="moduleName" value="#arguments.inpModuleName#">
                    <cfinvokeargument name="status_code" value="#httpStatusCode#">
                    <cfinvokeargument name="status_text" value="#processorResponse#">
                    <cfinvokeargument name="errordetail" value="#statusMessage#">
                    <cfinvokeargument name="filecontent" value="#fileContent#">
                    <cfinvokeargument name="paymentdata" value="#BODYDATATEMP#">
                    <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    <cfinvokeargument name="paymentmethod" value="#_MOJO_GATEWAY#">
                </cfinvoke>
                <cfcatch type="any">
                </cfcatch>
            </cftry>

        	<cfcatch type="any">
        		<cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <cfset dataout.MESSAGE = cfcatch.MESSAGE> 				

                <!--- Log payment --->
                <!--- <cftry>
                    <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
                        <cfinvokeargument name="moduleName" value="#arguments.inpModuleName#">
                        <cfinvokeargument name="status_code" value="#httpStatusCode#">
                        <cfinvokeargument name="status_text" value="#processorResponse#">
                        <cfinvokeargument name="errordetail" value="#statusMessage#">
                        <cfinvokeargument name="filecontent" value="#fileContent#">
                        <cfinvokeargument name="paymentdata" value="#BODYDATATEMP#">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                        <cfinvokeargument name="paymentmethod" value="#_MOJO_GATEWAY#">
                    </cfinvoke>
                    <cfcatch type="any">
                    </cfcatch>
                </cftry> --->

        	</cfcatch>

        </cftry>
		<cfreturn dataout />
    </cffunction>

    <cffunction name="SaleWithTokenzine" access="remote" output="true" hint="Sale With tokenzine">
        <cfargument name="inpUserId" TYPE="string" required="false" default="#Session.UserId#">
		<cfargument name="inpOrderId" TYPE="string" required="false" default="#Left(CreateUUID(),25)#">
        <cfargument name="inpOrderAmount" TYPE="string" required="true" default="0.00">
        <cfargument name="inpCurrencyCode" TYPE="string" required="false" default="840">

        <cfargument name="inpModuleName" TYPE="string" required="false" default="Buy Credits and Keywords">        
            
        <!---
        <cfargument name="inpcardToken" TYPE="string" required="true" default="">
        <cfargument name="inpcardExpiration" TYPE="string" required="true" default="">
        <cfargument name="inpcardCVV" TYPE="string" required="true" default="">

        <cfargument name="inpbillingFirstName" TYPE="string" required="true" default="">
        <cfargument name="inpbillingLastName" TYPE="string" required="true" default="">
        <cfargument name="inpbillingAddressLine1" TYPE="string" required="true" default="">
        <cfargument name="inpbillingCity" TYPE="string" required="true" default=" ">
        <cfargument name="inpbillingState" TYPE="string" required="true" default=" ">
        <cfargument name="inpbillingZipCode" TYPE="string" required="true" default=" ">
        <cfargument name="inpbillingCountry" TYPE="string" required="true" default=" ">
        <cfargument name="inpbillingEmail" TYPE="string" required="false" default=" ">
        <cfargument name="inpbillingPhone" TYPE="string" required="false" default=" ">
        --->    

        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "Invalid input data">
        <cfset dataout.TRANSACTIONID = "">

        <cfset dataout.REPORT = {}/>
        <cfset dataout.REPORT.ORDERID = ''>
        <cfset dataout.REPORT.TRANSACTIONID = '' >
        <cfset dataout.REPORT.AMT = ''>
        <cfset dataout.REPORT.RESPONSE = ''> 

        <cfset dataout.REPORT.INPNUMBER = ''>
        <cfset dataout.REPORT.INPCVV2 = '' >
        <cfset dataout.REPORT.INPFIRSTNAME = '' >
        <cfset dataout.REPORT.INPLASTNAME =  ''>
        <cfset dataout.REPORT.INPLINE1 = '' >
        <cfset dataout.REPORT.INPCITY = ''>
        <cfset dataout.REPORT.INPSTATE = ''>
        <cfset dataout.REPORT.INPPOSTALCODE = '' >
        <cfset dataout.REPORT.PHONE = ''> 
        <cfset dataout.REPORT.TRANSACTIONTYPE = ''> 
        <cfset dataout.REPORT.METHOD = ''>
        <cfset dataout.FILECONTENT = ''>

        <cfset var BODYDATA = "">
		<cfset var returndata = ""/>
        <cfset var sale = ""/>
        <cfset var myXMLDocument=''>
        <cfset var selectedElements=''>        
        <cfset var selectTransactionId = 0>

        <cfset var selectStatusMessage=''>    
        <cfset var statusMessage=''>    
        <cfset var selectProcessorResponse=''>    
        <cfset var processorResponse=''>    
        <cfset var httpStatusCode = ''>
        <cfset var fileContent = ''>
        <cfset var content = ''>
        <cfset var CheckExistsPaymentMethod = ''>
        
        <cftry>
            <!--- Get token Mojo to payment --->
            <cfquery name="CheckExistsPaymentMethod" datasource="#Session.DBSourceEBM#">
				SELECT
					extraNote,
                    PaymentMethodID_vch
				FROM
					simplebilling.authorize_user
				WHERE
					UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"> 		
				AND 
					paymentGateway_ti = 3	
				AND 
					status_ti = 1	
			</cfquery>

			<cfif CheckExistsPaymentMethod.RecordCount GT 0>   

				<cfset content= DeserializeJSON(CheckExistsPaymentMethod.extraNote)/>
                
                <cfif !structKeyExists(content, "INPEMAIL")>
                    <cfset content.INPEMAIL="">
                </cfif>
                <cfif Len(content.INPSTATE) GT 2>
                    <cfthrow  type = "any" message = "Billing State len is 2 characters only" detail = "Billing State len is 2 characters only" > 
                </cfif>

                <cfsavecontent variable="BODYDATA"><cfoutput><?xml version="1.0" encoding="UTF-8"?>
                    <UAPIRequest>
                        <transaction>
                            <process>online</process>
                            <type>sale</type>
                        </transaction>
                        <requestData>
                            <orderId>#arguments.inpOrderId#</orderId>
                            <orderAmount>#arguments.inpOrderAmount#</orderAmount>
                            <billing>                                            
                                <billingFirstName>#content.INPFIRSTNAME#</billingFirstName>
                                <billingLastName>#content.INPLASTNAME#</billingLastName>
                                <billingAddressLine1>#content.INPLINE1#</billingAddressLine1>
                                <billingCity>#content.INPCITY#</billingCity>
                                <billingState>#content.INPSTATE#</billingState>
                                <billingZipCode>#content.INPPOSTALCODE#</billingZipCode>
                                <billingCountry>#content.INPCOUNTRYCODE#</billingCountry>
                                #trim(content.INPEMAIL) NEQ ""? "<billingEmail>#content.INPEMAIL#</billingEmail>":""#
                                <billingPhone></billingPhone>
                            </billing>
                            <cardToken>#CheckExistsPaymentMethod.PaymentMethodID_vch#</cardToken>
                            <expDate>#content.INPEXPIREMONTH##right(content.INPEXPREYEAR, 2)#</expDate>
                            <cardValidationNum>#content.INPCVV2#</cardValidationNum>
                            <recurringTransaction>true</recurringTransaction>
                            <currencyCode>#arguments.inpCurrencyCode#</currencyCode>                                        
                        </requestData>
                    </UAPIRequest></cfoutput></cfsavecontent>
                
                <cfhttp url="#mojo_url#" method="post" result="sale" timeout="30">
                    <cfhttpparam type="header" name="Authorized" value="#mojo_token#">
                    <cfhttpparam type="header" name="Content-Type" value="application/xml">
                    <cfhttpparam type="body" value='#BODYDATA#'>
                </cfhttp>
              
                <Cfset fileContent = sale.fileContent/>   
                <cfset dataout.FILECONTENT = fileContent>
                <cfset statusMessage = sale.status_text />
                
                <!--- statusCode:0 ---->
                <!--- processorResponse:Approved--->
                <cfset httpStatusCode = sale.status_code/>
                <cfif structKeyExists(sale, "status_code") AND sale.status_code EQ 200>           
                    <cfif isXML(fileContent)>        
                        <cfset myXMLDocument = XmlParse(sale.fileContent)> 
                        <cfset selectedElements = XmlSearch(myXMLDocument, "/UAPIResponse/statusCode")>
                        <cfif ArrayLen(selectedElements) GT 0>
                            <cfset dataout.STATUSCODE = selectedElements[1].XmlText/>            
                        </cfif>
                        
                        <cfset selectedElements = XmlSearch(myXMLDocument, "/UAPIResponse/processorResponseCode")>
                        <cfif ArrayLen(selectedElements) GT 0>
                            <cfset dataout.PROCESSORREPONSECODE = selectedElements[1].XmlText/>

                            <cfset selectStatusMessage = XmlSearch(myXMLDocument, "/UAPIResponse/statusMessage")>
                            <cfif ArrayLen(selectStatusMessage) GT 0>
                                <cfset statusMessage = selectStatusMessage[1].XmlText/>
                            </cfif>

                            <cfset selectProcessorResponse = XmlSearch(myXMLDocument, "/UAPIResponse/processorResponse")>
                            <cfif ArrayLen(selectProcessorResponse) GT 0>
                                <cfset processorResponse = selectProcessorResponse[1].XmlText/>
                            </cfif>

                            <cfif selectedElements[1].XmlText EQ "000">
                                <cfset selectTransactionId = XmlSearch(myXMLDocument, "/UAPIResponse/transactionId")>
                                <cfset dataout.RXRESULTCODE = 1 /> 
                                <cfset dataout.RESULT = 'SUCCESS'>                    
                                <cfset dataout.MESSAGE = "Payment successfully">				
                                <cfset dataout.TRANSACTIONID = selectTransactionId[1].XmlText /> 

                                <!--- LOG TO PAMENT SUCCESS --->
                                <cfset dataout.REPORT.ORDERID = arguments.inpOrderId>
                                <cfset dataout.REPORT.TRANSACTIONID = dataout.TRANSACTIONID >
                                <cfset dataout.REPORT.AMT = arguments.inpOrderAmount>
                                <cfset dataout.REPORT.RESPONSE = processorResponse> 

                                <cfset dataout.REPORT.INPNUMBER = content.INPNUMBER>
                                <cfset dataout.REPORT.INPCVV2 = content.INPCVV2 >
                                <cfset dataout.REPORT.INPFIRSTNAME = content.INPFIRSTNAME >
                                <cfset dataout.REPORT.INPLASTNAME =  content.INPLASTNAME>
                                <cfset dataout.REPORT.INPLINE1 = content.INPLINE1 >
                                <cfset dataout.REPORT.INPCITY = content.INPCITY>
                                <cfset dataout.REPORT.INPSTATE = content.INPSTATE>
                                <cfset dataout.REPORT.INPPOSTALCODE = content.INPPOSTALCODE >
                                <cfset dataout.REPORT.PHONE = ''>                             
                                <cfset dataout.REPORT.EMAIL = content.INPEMAIL>                            
                                <cfset dataout.REPORT.TRANSACTIONTYPE = 'sale'> 
                                <cfset dataout.REPORT.METHOD = 'CC'>
                            
                            <cfelse>
                                <cfset dataout.RXRESULTCODE = -3 />
                                <cfset dataout.RESULT = 'FAIL'>                    
                                <!--- <cfset dataout.MESSAGE = "Error while create payment with processorResponseCode."&processorResponse> --->
                                <cfif TRIM(processorResponse) NEQ "">
                                    <cfset dataout.MESSAGE = HTMLEditFormat(processorResponse)>   
                                <cfelse>
                                    <cfset dataout.MESSAGE = HTMLEditFormat(statusMessage)>   
                                </cfif>                                
                            </cfif>
                        <cfelse>

                            <cfset selectedElements = XmlSearch(myXMLDocument, "/UAPIResponse")>
                            <cfif ArrayLen(selectedElements) GT 0>
                                <cfset selectStatusMessage = XmlSearch(myXMLDocument, "/UAPIResponse/statusMessage")>
                                <cfif ArrayLen(selectStatusMessage) GT 0>
                                    <cfset statusMessage = selectStatusMessage[1].XmlText/>
                                </cfif>

                                <cfset selectProcessorResponse = XmlSearch(myXMLDocument, "/UAPIResponse/processorResponse")>
                                <cfif ArrayLen(selectProcessorResponse) GT 0>
                                    <cfset processorResponse = selectProcessorResponse[1].XmlText/>
                                </cfif>
                            </cfif>

                            <cfset dataout.RXRESULTCODE = -4 />
                            <cfset dataout.RESULT = 'FAIL'>                    
                            <!--- <cfset dataout.MESSAGE = "Error while create payment with statusMessage."&HTMLEditFormat(statusMessage)>  --->
                            <cfif TRIM(processorResponse) NEQ "">
                                <cfset dataout.MESSAGE = HTMLEditFormat(processorResponse)>   
                            <cfelse>
                                <cfset dataout.MESSAGE = HTMLEditFormat(statusMessage)>   
                            </cfif>

                        </cfif>    
                    <cfelse>
                        <cfset dataout.RXRESULTCODE = -5 />
                        <cfset dataout.RESULT = 'FAIL'>                    
                        <!--- <cfset dataout.MESSAGE = "Error while create payment with statusMessage."&HTMLEditFormat(statusMessage)>  --->
                        <cfset dataout.MESSAGE = 'Invalid XML response'>   
                    </cfif>                 		

                <cfelse>
                    <cfif isXML(fileContent)>
                        <cfset myXMLDocument = XmlParse(fileContent)> 
                        <cfset selectedElements = XmlSearch(myXMLDocument, "/UAPIResponse")>
                        <cfif ArrayLen(selectedElements) GT 0>
                            <cfset selectStatusMessage = XmlSearch(myXMLDocument, "/UAPIResponse/statusMessage")>
                            <cfif ArrayLen(selectStatusMessage) GT 0>
                                <cfset statusMessage = selectStatusMessage[1].XmlText/>
                            </cfif>

                            <cfset selectProcessorResponse = XmlSearch(myXMLDocument, "/UAPIResponse/processorResponse")>
                            <cfif ArrayLen(selectProcessorResponse) GT 0>
                                <cfset processorResponse = selectProcessorResponse[1].XmlText/>
                            </cfif>
                        </cfif>    
                    </cfif>

                    <cfset dataout.RXRESULTCODE = -2 /> 
                    <cfset dataout.RESULT = 'FAIL'>                    
                    <!--- <cfset dataout.MESSAGE = "Error while create payment with status msg. "&HTMLEditFormat(statusMessage)> --->
                    
                    <cfif TRIM(processorResponse) NEQ "">
                        <cfset dataout.MESSAGE = HTMLEditFormat(processorResponse)>   
                    <cfelse>
                        <cfset dataout.MESSAGE = HTMLEditFormat(statusMessage)>   
                    </cfif>
                </cfif>

                <!--- Log payment --->
                <cftry>
                    <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
                        <cfinvokeargument name="moduleName" value="#arguments.inpModuleName#">
                        <cfinvokeargument name="status_code" value="#httpStatusCode#">
                        <cfinvokeargument name="status_text" value="#processorResponse#">
                        <cfinvokeargument name="errordetail" value="#statusMessage#">
                        <cfinvokeargument name="filecontent" value="#fileContent#">
                        <cfinvokeargument name="paymentdata" value="#BODYDATA#">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                        <cfinvokeargument name="paymentmethod" value="#_MOJO_GATEWAY#">
                        <cfinvokeargument name="inpPaymentByUserId" value="#arguments.inpUserId#">                        
                    </cfinvoke>
                    <cfcatch type="any">
                    </cfcatch>
                </cftry>

            <cfelse>
                <cfset dataout.RESULT = "FAIL">
                <cfset dataout.RXRESULTCODE = 0>
                <cfset dataout.MESSAGE = "Payment fail!. CC not exist or inactive.">
                <cfreturn dataout>
            </cfif>

        	<cfcatch type="any">
        		<cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <cfset dataout.MESSAGE = cfcatch.MESSAGE> 				

                <!--- Log payment --->
                <!--- <cftry>
                    <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
                        <cfinvokeargument name="moduleName" value="#arguments.inpModuleName#">
                        <cfinvokeargument name="status_code" value="#statusCode#">
                        <cfinvokeargument name="status_text" value="#processorResponse#">
                        <cfinvokeargument name="errordetail" value="#statusMessage#">
                        <cfinvokeargument name="filecontent" value="#fileContent#">
                        <cfinvokeargument name="paymentdata" value="#BODYDATA#">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                        <cfinvokeargument name="paymentmethod" value="#_MOJO_GATEWAY#">
                    </cfinvoke>
                    <cfcatch type="any">
                    </cfcatch>
                </cftry> --->

        	</cfcatch>

        </cftry>
		<cfreturn dataout />
    </cffunction>
    <cffunction name="SaleWithSuspiciousVault" access="remote" output="true" hint="Sale With Suspicious Vault">
        <cfargument name="inpUserId" TYPE="string" required="true">
        <cfargument name="inpVaultId" TYPE="string" required="true">
        <cfargument name="inpPaymentReviewId" TYPE="string" required="true">
		<cfargument name="inpOrderId" TYPE="string" required="false" default="#Left(CreateUUID(),25)#">
        <cfargument name="inpOrderAmount" TYPE="string" required="true" default="0.00">
        <cfargument name="inpCurrencyCode" TYPE="string" required="false" default="840">

        <cfargument name="inpModuleName" TYPE="string" required="false" default="Buy Credits and Keywords">        
            

        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "Invalid input data">
        <cfset dataout.TRANSACTIONID = "">

        <cfset dataout.REPORT = {}/>
        <cfset dataout.REPORT.ORDERID = ''>
        <cfset dataout.REPORT.TRANSACTIONID = '' >
        <cfset dataout.REPORT.AMT = ''>
        <cfset dataout.REPORT.RESPONSE = ''> 

        <cfset dataout.REPORT.INPNUMBER = ''>
        <cfset dataout.REPORT.INPCVV2 = '' >
        <cfset dataout.REPORT.INPFIRSTNAME = '' >
        <cfset dataout.REPORT.INPLASTNAME =  ''>
        <cfset dataout.REPORT.INPLINE1 = '' >
        <cfset dataout.REPORT.INPCITY = ''>
        <cfset dataout.REPORT.INPSTATE = ''>
        <cfset dataout.REPORT.INPPOSTALCODE = '' >
        <cfset dataout.REPORT.PHONE = ''> 
        <cfset dataout.REPORT.TRANSACTIONTYPE = ''> 
        <cfset dataout.REPORT.METHOD = ''>
        <cfset dataout.FILECONTENT = ''>

        <cfset var BODYDATA = "">
		<cfset var returndata = ""/>
        <cfset var sale = ""/>
        <cfset var myXMLDocument=''>
        <cfset var selectedElements=''>        
        <cfset var selectTransactionId = 0>

        <cfset var selectStatusMessage=''>    
        <cfset var statusMessage=''>    
        <cfset var selectProcessorResponse=''>    
        <cfset var processorResponse=''>    
        <cfset var httpStatusCode = ''>
        <cfset var fileContent = ''>
        <cfset var content = ''>
        <cfset var CheckExistsPaymentMethod = ''>
        
        <cftry>
            <!--- Get token Mojo to payment --->
            <cfquery name="CheckExistsPaymentMethod" datasource="#Session.DBSourceEBM#">
				SELECT
					CardData_txt                    
				FROM
					simplebilling.list_payment_review
				WHERE
					UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"> 		
				AND 
					paymentGateway_ti = 3	
				AND 
					Id_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPaymentReviewId#"> 		
			</cfquery>

			<cfif CheckExistsPaymentMethod.RecordCount GT 0>   

				<cfset content= DeserializeJSON(CheckExistsPaymentMethod.CardData_txt)/>
                
                <cfif !structKeyExists(content, "INPEMAIL")>
                    <cfset content.INPEMAIL="">
                </cfif>
                <cfif Len(content.INPSTATE) GT 2>
                    <cfthrow  type = "any" message = "Billing State len is 2 characters only" detail = "Billing State len is 2 characters only" > 
                </cfif>

                <cfsavecontent variable="BODYDATA"><cfoutput><?xml version="1.0" encoding="UTF-8"?>
                    <UAPIRequest>
                        <transaction>
                            <process>online</process>
                            <type>sale</type>
                        </transaction>
                        <requestData>
                            <orderId>#arguments.inpOrderId#</orderId>
                            <orderAmount>#arguments.inpOrderAmount#</orderAmount>
                            <billing>                                            
                                <billingFirstName>#content.INPFIRSTNAME#</billingFirstName>
                                <billingLastName>#content.INPLASTNAME#</billingLastName>
                                <billingAddressLine1>#content.INPLINE1#</billingAddressLine1>
                                <billingCity>#content.INPCITY#</billingCity>
                                <billingState>#content.INPSTATE#</billingState>
                                <billingZipCode>#content.INPPOSTALCODE#</billingZipCode>
                                <billingCountry>#content.INPCOUNTRYCODE#</billingCountry>
                                #trim(content.INPEMAIL) NEQ ""? "<billingEmail>#content.INPEMAIL#</billingEmail>":""#
                                <billingPhone></billingPhone>
                            </billing>
                            <cardToken>#arguments.inpVaultId#</cardToken>
                            <expDate>#content.INPEXPIREMONTH##right(content.INPEXPREYEAR, 2)#</expDate>
                            <cardValidationNum>#content.INPCVV2#</cardValidationNum>
                            <recurringTransaction>true</recurringTransaction>
                            <currencyCode>#arguments.inpCurrencyCode#</currencyCode>                                        
                        </requestData>
                    </UAPIRequest></cfoutput></cfsavecontent>
                
                <cfhttp url="#mojo_url#" method="post" result="sale" timeout="30">
                    <cfhttpparam type="header" name="Authorized" value="#mojo_token#">
                    <cfhttpparam type="header" name="Content-Type" value="application/xml">
                    <cfhttpparam type="body" value='#BODYDATA#'>
                </cfhttp>
              
                <Cfset fileContent = sale.fileContent/>   
                <cfset dataout.FILECONTENT = fileContent>
                <cfset statusMessage = sale.status_text />
                
                <!--- statusCode:0 ---->
                <!--- processorResponse:Approved--->
                <cfset httpStatusCode = sale.status_code/>
                <cfif structKeyExists(sale, "status_code") AND sale.status_code EQ 200>           
                    <cfif isXML(fileContent)>        
                        <cfset myXMLDocument = XmlParse(sale.fileContent)> 
                        <cfset selectedElements = XmlSearch(myXMLDocument, "/UAPIResponse/statusCode")>
                        <cfif ArrayLen(selectedElements) GT 0>
                            <cfset dataout.STATUSCODE = selectedElements[1].XmlText/>            
                        </cfif>
                        
                        <cfset selectedElements = XmlSearch(myXMLDocument, "/UAPIResponse/processorResponseCode")>
                        <cfif ArrayLen(selectedElements) GT 0>
                            <cfset dataout.PROCESSORREPONSECODE = selectedElements[1].XmlText/>

                            <cfset selectStatusMessage = XmlSearch(myXMLDocument, "/UAPIResponse/statusMessage")>
                            <cfif ArrayLen(selectStatusMessage) GT 0>
                                <cfset statusMessage = selectStatusMessage[1].XmlText/>
                            </cfif>

                            <cfset selectProcessorResponse = XmlSearch(myXMLDocument, "/UAPIResponse/processorResponse")>
                            <cfif ArrayLen(selectProcessorResponse) GT 0>
                                <cfset processorResponse = selectProcessorResponse[1].XmlText/>
                            </cfif>

                            <cfif selectedElements[1].XmlText EQ "000">
                                <cfset selectTransactionId = XmlSearch(myXMLDocument, "/UAPIResponse/transactionId")>
                                <cfset dataout.RXRESULTCODE = 1 /> 
                                <cfset dataout.RESULT = 'SUCCESS'>                    
                                <cfset dataout.MESSAGE = "Payment successfully">				
                                <cfset dataout.TRANSACTIONID = selectTransactionId[1].XmlText /> 

                                <!--- LOG TO PAMENT SUCCESS --->
                                <cfset dataout.REPORT.ORDERID = arguments.inpOrderId>
                                <cfset dataout.REPORT.TRANSACTIONID = dataout.TRANSACTIONID >
                                <cfset dataout.REPORT.AMT = arguments.inpOrderAmount>
                                <cfset dataout.REPORT.RESPONSE = processorResponse> 

                                <cfset dataout.REPORT.INPNUMBER = content.INPNUMBER>
                                <cfset dataout.REPORT.INPCVV2 = content.INPCVV2 >
                                <cfset dataout.REPORT.INPFIRSTNAME = content.INPFIRSTNAME >
                                <cfset dataout.REPORT.INPLASTNAME =  content.INPLASTNAME>
                                <cfset dataout.REPORT.INPLINE1 = content.INPLINE1 >
                                <cfset dataout.REPORT.INPCITY = content.INPCITY>
                                <cfset dataout.REPORT.INPSTATE = content.INPSTATE>
                                <cfset dataout.REPORT.INPPOSTALCODE = content.INPPOSTALCODE >
                                <cfset dataout.REPORT.PHONE = ''>                             
                                <cfset dataout.REPORT.EMAIL = content.INPEMAIL>                            
                                <cfset dataout.REPORT.TRANSACTIONTYPE = 'sale'> 
                                <cfset dataout.REPORT.METHOD = 'CC'>
                            
                            <cfelse>
                                <cfset dataout.RXRESULTCODE = -3 />
                                <cfset dataout.RESULT = 'FAIL'>                    
                                <!--- <cfset dataout.MESSAGE = "Error while create payment with processorResponseCode."&processorResponse> --->                                
                                <cfif TRIM(processorResponse) NEQ "">
                                    <cfset dataout.MESSAGE = HTMLEditFormat(processorResponse)>   
                                <cfelse>
                                    <cfset dataout.MESSAGE = HTMLEditFormat(statusMessage)>   
                                </cfif>  
                            </cfif>
                        <cfelse>

                            <cfset selectedElements = XmlSearch(myXMLDocument, "/UAPIResponse")>
                            <cfif ArrayLen(selectedElements) GT 0>
                                <cfset selectStatusMessage = XmlSearch(myXMLDocument, "/UAPIResponse/statusMessage")>
                                <cfif ArrayLen(selectStatusMessage) GT 0>
                                    <cfset statusMessage = selectStatusMessage[1].XmlText/>
                                </cfif>

                                <cfset selectProcessorResponse = XmlSearch(myXMLDocument, "/UAPIResponse/processorResponse")>
                                <cfif ArrayLen(selectProcessorResponse) GT 0>
                                    <cfset processorResponse = selectProcessorResponse[1].XmlText/>
                                </cfif>
                            </cfif>

                            <cfset dataout.RXRESULTCODE = -4 />
                            <cfset dataout.RESULT = 'FAIL'>                    
                            <!--- <cfset dataout.MESSAGE = "Error while create payment with statusMessage."&HTMLEditFormat(statusMessage)>  --->
                            <cfif TRIM(processorResponse) NEQ "">
                                <cfset dataout.MESSAGE = HTMLEditFormat(processorResponse)>   
                            <cfelse>
                                <cfset dataout.MESSAGE = HTMLEditFormat(statusMessage)>   
                            </cfif>  

                        </cfif>    
                    <cfelse>
                        <cfset dataout.RXRESULTCODE = -5 />
                        <cfset dataout.RESULT = 'FAIL'>                    
                        <!--- <cfset dataout.MESSAGE = "Error while create payment with statusMessage."&HTMLEditFormat(statusMessage)>  --->
                        <cfset dataout.MESSAGE = 'Invalid XML response'>   
                    </cfif>                 		

                <cfelse>
                    <cfif isXML(fileContent)>
                        <cfset myXMLDocument = XmlParse(fileContent)> 
                        <cfset selectedElements = XmlSearch(myXMLDocument, "/UAPIResponse")>
                        <cfif ArrayLen(selectedElements) GT 0>
                            <cfset selectStatusMessage = XmlSearch(myXMLDocument, "/UAPIResponse/statusMessage")>
                            <cfif ArrayLen(selectStatusMessage) GT 0>
                                <cfset statusMessage = selectStatusMessage[1].XmlText/>
                            </cfif>

                            <cfset selectProcessorResponse = XmlSearch(myXMLDocument, "/UAPIResponse/processorResponse")>
                            <cfif ArrayLen(selectProcessorResponse) GT 0>
                                <cfset processorResponse = selectProcessorResponse[1].XmlText/>
                            </cfif>
                        </cfif>    
                    </cfif>

                    <cfset dataout.RXRESULTCODE = -2 /> 
                    <cfset dataout.RESULT = 'FAIL'>                    
                    <!--- <cfset dataout.MESSAGE = "Error while create payment with status msg. "&HTMLEditFormat(statusMessage)> --->
                    <cfif TRIM(processorResponse) NEQ "">
                        <cfset dataout.MESSAGE = HTMLEditFormat(processorResponse)>   
                    <cfelse>
                        <cfset dataout.MESSAGE = HTMLEditFormat(statusMessage)>   
                    </cfif>  
                </cfif>

                <!--- Log payment --->
                <cftry>
                    <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
                        <cfinvokeargument name="moduleName" value="#arguments.inpModuleName#">
                        <cfinvokeargument name="status_code" value="#httpStatusCode#">
                        <cfinvokeargument name="status_text" value="#processorResponse#">
                        <cfinvokeargument name="errordetail" value="#statusMessage#">
                        <cfinvokeargument name="filecontent" value="#fileContent#">
                        <cfinvokeargument name="paymentdata" value="#BODYDATA#">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                        <cfinvokeargument name="paymentmethod" value="#_MOJO_GATEWAY#">
                        <cfinvokeargument name="inpPaymentByUserId" value="#arguments.inpUserId#">                        
                    </cfinvoke>
                    <cfcatch type="any">
                    </cfcatch>
                </cftry>

            <cfelse>
                <cfset dataout.RESULT = "FAIL">
                <cfset dataout.RXRESULTCODE = 0>
                <cfset dataout.MESSAGE = "Payment fail!. CC not exist.">
                <cfreturn dataout>
            </cfif>

        	<cfcatch type="any">
        		<cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <cfset dataout.MESSAGE = cfcatch.MESSAGE> 				                
        	</cfcatch>

        </cftry>
		<cfreturn dataout />
    </cffunction>
    <cffunction name="UpdatePayment" access="remote" output="true" hint="Update Payment of mojo" >
        <cfargument name="moduleName" type="string" required="yes" default="">
        <cfargument name="inpUserId" type="string" required="yes" default="">
        <cfargument name="inpPaymentData" type="string" required="yes" default="">                
        <cfargument name="inpPlanId" type="string" required="yes" default="">
        
        <cfargument name="numberKeyword" type="string" required="no" default="0">
        <cfargument name="numberSMS" type="string" required="no" default="0">        
        <cfargument name="inpPaymentByUserId" type="string" required="no" default="">
        
        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "Update Fail">
        

        <cfset var updatePayment	= '' />
        <cftry>                
            <cfset var data=  deserializeJSON(arguments.inpPaymentData)>
            <cfquery name="updatePayment" datasource="#Session.DBSourceEBM#" result="updatePayment">
                INSERT INTO `simplebilling`.`payment_worldpay` 
                (   
                    `userId_int`,
                    `planId`, 
                    `moduleName`,
                    `bynumberKeyword`,
                    `bynumberSMS`,
                    `transactionType`,
                    `orderId`, 
                    `transactionId`, 
                    `authorizedAmount`, 
                    `paymentTypeCode`, 
                    `paymentTypeResult`, 
                    `transactionData_amount`, 
                    `cardNumber`, 
                    `avsCode`, 
                    `cardHolder_FirstName`, 
                    `cardHolder_LastName`, 
                    `billAddress_line1`, 
                    `billAddress_city`, 
                    `billAddress_state`, 
                    `billAddress_zip`, 
                    `billAddress_company`, 
                    `billAddress_phone`, 
                    `email`, 
                    `method`, 
                    `responseText`,
                    `paymentGateway_ti`,
                    `created`,
                    `PaymentByUserId_int`
                    
                )
                VALUES 
                (
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">,
                    <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.inpPlanId#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.moduleName#">,
                    <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.numberKeyword#">,
                    <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.numberSMS#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.TRANSACTIONTYPE#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.ORDERID#">,   
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.TRANSACTIONID#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.AMT#">,     
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.AMT#">,     
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.INPNUMBER#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.INPCVV2#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.INPFIRSTNAME#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.INPLASTNAME#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.INPLINE1#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.INPCITY#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.INPSTATE#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.INPPOSTALCODE#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.PHONE#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.METHOD#">, 
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.RESPONSE#">, 
                    3,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPaymentByUserId#">
                )
            </cfquery>
            <cfset dataout.RESULT = "Success">
            <cfset dataout.RXRESULTCODE = 1>
            <cfset dataout.MESSAGE = "Update successfully">
            <cfcatch type="any">
        		<cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <cfset dataout.MESSAGE = cfcatch.MESSAGE> 				
        	</cfcatch>
        </cftry>
        <cfreturn dataout />
    </cffunction>

    <cffunction name="createCreditCardVault" access="remote" output="true" hint="create Credit Card Vault">

        <cfargument name="inpFirstNameCard" type="string" required="yes" default="">
        <cfargument name="inpLastNameCard" type="string" required="yes" default="">
        <cfargument name="inpCreditCardNumber" type="string" required="yes" default="">
        <cfargument name="inpExpirationDate" type="string" required="yes" default=""> 
        <cfargument name="inpCardSecurityCode" type="string" required="yes" default="">
        <cfargument name="inpZipPostalCodeCard" type="string" required="yes" default=""> 
        
        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "Update Fail">

        <cfset var status_code = ''/>
        <cfset var createVaultUrl = ''/>
        <cfset var returndata = ''/>
        <cfset var ex_status_code = ''/>
        <cfset var createVaultResult = ''/>

        <cftry>

            <cfset createVaultUrl = "#mojo_createvault_url#?type=#mojo_createvault_type#&UAPIToken=#mojo_token#&processorID=#mojo_processorID#&creditCardNumber=#inpCreditCardNumber#&expirationDate=#inpExpirationDate#&cardSecurityCode=#inpCardSecurityCode#&firstNameCard=#inpFirstNameCard#&lastNameCard=#inpLastNameCard#&zipPostalCodeCard=#inpZipPostalCodeCard#">

            <cfhttp url="#createVaultUrl#" method="GET" result="createVaultResult" timeout="30"></cfhttp>  
            <cfset status_code = createVaultResult.status_code>

            <cfif status_code EQ 404>
                <cfset dataout.RXRESULTCODE = -3>
                <cfset dataout.MESSAGE = "Can't call create vault API">
            <cfelse>
                <cfset returndata =  DESERIALIZEJSON(createVaultResult.fileContent)>
                <cfset ex_status_code = structKeyExists(createVaultResult, "status_code")>
                
                <cfif ex_status_code AND status_code EQ 200>
                    <cfset dataout.RESULT = "SUCCESS">
                    <cfset dataout.RXRESULTCODE = 1>
                    <cfset dataout.MESSAGE = returndata.message>
                <cfelse>
                    <cfset dataout.RXRESULTCODE = -2>
                    <cfset dataout.MESSAGE = returndata.message>
                </cfif>
            </cfif> 
        
            <cfcatch type="any">
                <cfset dataout.RESULT ='FAIL'>
                <cfset dataout.RXRESULTCODE = -1>
                <cfset dataout.MESSAGE = cfcatch.MESSAGE>
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="Authorization" access="remote" output="false" hint="authorization request to get correct token">
        <cfargument name="inpUserId" TYPE="string" required="false" default="#Session.UserId#">
        <cfargument name="inporderId" TYPE="string" required="false" default="#Left(CreateUUID(),25)#">
        <cfargument name="inpcardNumber" TYPE="string" required="true" default="">      

        <cfargument name="inporderAmount" TYPE="string" required="true" default="1">
        <cfargument name="inpcurrencyCode" TYPE="string" required="false" default="840">
        <cfargument name="inpcardExpiration" TYPE="string" required="true" default="">
        <cfargument name="inpcardCVV" TYPE="string" required="true" default="">

        <cfargument name="inpbillingFirstName" TYPE="string" required="true" default="">
        <cfargument name="inpbillingLastName" TYPE="string" required="true" default="">
        <cfargument name="inpbillingAddressLine1" TYPE="string" required="true" default="">
        <cfargument name="inpbillingCity" TYPE="string" required="true" default=" ">
        <cfargument name="inpbillingState" TYPE="string" required="true" default=" ">
        <cfargument name="inpbillingZipCode" TYPE="string" required="true" default=" ">
        <cfargument name="inpbillingCountry" TYPE="string" required="true" default=" ">
        <cfargument name="inpbillingEmail" TYPE="string" required="false" default=" ">
        <cfargument name="inpbillingPhone" TYPE="string" required="false" default=" ">

        <cfargument name="inpshippingFirstName" TYPE="string" required="false" default=" ">
        <cfargument name="inpshippingLastName" TYPE="string" required="false" default=" ">
        <cfargument name="inpshippingAddressLine1" TYPE="string" required="false" default=" ">
        <cfargument name="inpshippingAddressLine2" TYPE="string" required="false" default=" ">
        <cfargument name="inpshippingCity" TYPE="string" required="false" default=" ">
        <cfargument name="inpshippingState" TYPE="string" required="false" default=" ">
        <cfargument name="inpshippingZipCode" TYPE="string" required="false" default=" ">
        <cfargument name="inpshippingCountry" TYPE="string" required="false" default=" ">
        <cfargument name="inpshippingEmail" TYPE="string" required="false" default=" ">
        <cfargument name="inpshippingPhone" TYPE="string" required="false" default=" ">
        <cfargument name="inpModuleName" TYPE="string" required="false" default="Create authorization transaction">
                

        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">
        <cfset dataout.STATUSCODE = ""/>
        <cfset dataout.PROCESSORREPONSECODE = ""/>

        <cfset dataout.TRANSACTIONID = "">
        <cfset dataout.AVSCODE = "">
        <cfset dataout.TOKEN = ""/>
        <cfset dataout.CCTYPE = ""/>

        <cfset dataout.REPORT = {}/>
        <cfset dataout.REPORT.ORDERID = ''>
        <cfset dataout.REPORT.TRANSACTIONID = '' >
        <cfset dataout.REPORT.AMT = ''>
        <cfset dataout.REPORT.RESPONSE = ''> 

        <cfset dataout.REPORT.INPNUMBER = ''>
        <cfset dataout.REPORT.INPCVV2 = '' >
        <cfset dataout.REPORT.INPFIRSTNAME = '' >
        <cfset dataout.REPORT.INPLASTNAME =  ''>
        <cfset dataout.REPORT.INPLINE1 = '' >
        <cfset dataout.REPORT.INPCITY = ''>
        <cfset dataout.REPORT.INPSTATE = ''>
        <cfset dataout.REPORT.INPPOSTALCODE = '' >
        <cfset dataout.REPORT.PHONE = ''> 
        <cfset dataout.REPORT.EMAIL = ''>
        <cfset dataout.REPORT.TRANSACTIONTYPE = ''> 
        <cfset dataout.REPORT.METHOD = ''>

        <cfset var BODYDATA = "">
        <cfset var BODYDATATEMP = ""/>
        <cfset var returndata = ""/>
        <cfset var sale = ""/>
        <cfset var myXMLDocument=''>
        <cfset var selectedElements=''>        
        <Cfset var processorResponse = ''/>
        <Cfset var statusMessage = ''/>
        <Cfset var fileContent = ''/>
        <Cfset var httpStatusCode = ''/>
        <Cfset var dateFromCcInput = ''/>
        <cfset var selectStatusMessage = ''/>
        <cfset var selectProcessorResponse = ''/>
        <cfset var selectTransactionId = ''/>

        <cfset var selectCCToken = ''/>
        <cfset var selectCCType = ''/>
        <cfset var avsResponse=''>

        <cftry>
            <cfif IsNumeric(arguments.inpbillingState) OR IsNumeric(arguments.inpbillingAddressLine1) OR IsNumeric(arguments.inpbillingCity)>
                <cfthrow  type = "any" message = "Billing Address, City, State should be string" detail = "Billing Address, City, State should be string" > 
            </cfif>
            <cfif Len(arguments.inpbillingState) GT 2>
                <cfthrow  type = "any" message = "Billing State should be 2 characters only" detail = "Billing State should be 2 characters only" > 
            </cfif>
            <cfif Len(arguments.inpbillingAddressLine1) GT 35>
                <cfthrow  type = "any" message = "Billing Address should be 35 characters only" detail = "Billing Address should be 35 characters only" > 
            </cfif>
            <cfif Len(arguments.inpbillingCity) GT 20>
                <cfthrow  type = "any" message = "Billing City should be 20 characters only" detail = "Billing City should be 20 characters only" > 
            </cfif>
            <!--- check for same month year compage with last day of month, if not compare first day is enough---->            
           <cfif Int(Month(NOW())) EQ Int(left(arguments.inpcardExpiration,2)) AND Int(right(Year(NOW()),2)) EQ Int(right(arguments.inpcardExpiration,2))>
                <cfset dateFromCcInput= createDate("20" & right(arguments.inpcardExpiration,2), left(arguments.inpcardExpiration,2), daysInMonth(  NOW()  ))>
                
            <cfelse>            
                <cfset dateFromCcInput= createDate("20" & right(arguments.inpcardExpiration,2), left(arguments.inpcardExpiration,2), 28)>
            </cfif>
            <cfif DateCompare(dateFromCcInput, NOW()) LT 1>
                <cfthrow  type = "any" message = "Your Credit Card has been expired! Please check the Expiration Date or add another Credit Card." detail = "Your Credit Card has been expired! Please check the Expiration Date or add another Credit Card." > 
            </cfif>
            
            <cfsavecontent variable="BODYDATA"><cfoutput><?xml version="1.0" encoding="UTF-8"?>
                <UAPIRequest>
                    <transaction>
                        <process>online</process>
                        <type>authorization</type>
                    </transaction>
                    <requestData>
                        <orderId>#arguments.inporderId#</orderId>
                        <orderAmount>#arguments.inporderAmount#</orderAmount>
                        <currencyCode>#arguments.inpcurrencyCode#</currencyCode>
                        <billing>
                            <billingFirstName>#arguments.inpbillingFirstName#</billingFirstName>
                            <billingLastName>#arguments.inpbillingLastName#</billingLastName>
                            <billingAddressLine1>#arguments.inpbillingAddressLine1#</billingAddressLine1>
                            <billingCity>#arguments.inpbillingCity#</billingCity>
                            <billingState>#arguments.inpbillingState#</billingState>
                            <billingZipCode>#arguments.inpbillingZipCode#</billingZipCode>
                            <billingCountry>#arguments.inpbillingCountry#</billingCountry>
                            #trim(arguments.inpbillingEmail) NEQ ""? "<billingEmail>#arguments.inpbillingEmail#</billingEmail>":""#
                            #trim(arguments.inpbillingPhone) NEQ ""? "<billingPhone>#arguments.inpbillingPhone#</billingPhone>":""#
                            
                        </billing>
                        <shipping>
                            #trim(arguments.inpshippingFirstName) NEQ ""? "<shippingFirstName>#arguments.inpshippingFirstName#</shippingFirstName>":""#
                            #trim(arguments.inpshippingLastName) NEQ ""? "<shippingLastName>#arguments.inpshippingLastName#</shippingLastName>":""#
                            #trim(arguments.inpshippingAddressLine1) NEQ ""? "<shippingAddressLine1>#arguments.inpshippingAddressLine1#</shippingAddressLine1>":""#
                            #trim(arguments.inpshippingAddressLine2) NEQ ""? "<shippingAddressLine2>#arguments.inpshippingAddressLine2#</shippingAddressLine2>":""#
                            #trim(arguments.inpshippingCity) NEQ ""? "<shippingCity>#arguments.inpshippingCity#</shippingCity>":""#
                            #trim(arguments.inpshippingState) NEQ ""? "<shippingState>#arguments.inpshippingState#</shippingState>":""#
                            #trim(arguments.inpshippingZipCode) NEQ ""? "<shippingZipCode>#arguments.inpshippingZipCode#</shippingZipCode>":""#
                            #trim(arguments.inpshippingCountry) NEQ ""? "<shippingCountry>#arguments.inpshippingCountry#</shippingCountry>":""#
                            #trim(arguments.inpshippingEmail) NEQ ""? "<shippingEmail>#arguments.inpshippingEmail#</shippingEmail>":""#
                            #trim(arguments.inpshippingPhone) NEQ ""? "<shippingPhone>#arguments.inpshippingPhone#</shippingPhone>":""#                                                                                        
                        </shipping>
                        <card>
                            <cardNumber>#arguments.inpcardNumber#</cardNumber>
                            <cardExpiration>#arguments.inpcardExpiration#</cardExpiration>
                            <cardCVV>#arguments.inpcardCVV#</cardCVV>
                        </card>
                    </requestData>
                </UAPIRequest></cfoutput></cfsavecontent>    

            <cfsavecontent variable="BODYDATATEMP"><cfoutput><?xml version="1.0" encoding="UTF-8"?>
                <UAPIRequest>
                    <transaction>
                        <process>online</process>
                        <type>authorization</type>
                    </transaction>
                    <requestData>
                        <orderId>#arguments.inporderId#</orderId>
                        <orderAmount>#arguments.inporderAmount#</orderAmount>
                        <currencyCode>#arguments.inpcurrencyCode#</currencyCode>
                        <billing>
                            <billingFirstName>#arguments.inpbillingFirstName#</billingFirstName>
                            <billingLastName>#arguments.inpbillingLastName#</billingLastName>
                            <billingAddressLine1>#arguments.inpbillingAddressLine1#</billingAddressLine1>
                            <billingCity>#arguments.inpbillingCity#</billingCity>
                            <billingState>#arguments.inpbillingState#</billingState>
                            <billingZipCode>#arguments.inpbillingZipCode#</billingZipCode>
                            <billingCountry>#arguments.inpbillingCountry#</billingCountry>
                            #trim(arguments.inpbillingEmail) NEQ ""? "<billingEmail>#arguments.inpbillingEmail#</billingEmail>":""#
                            #trim(arguments.inpbillingPhone) NEQ ""? "<billingPhone>#arguments.inpbillingPhone#</billingPhone>":""#
                            
                        </billing>
                        <shipping>
                            #trim(arguments.inpshippingFirstName) NEQ ""? "<shippingFirstName>#arguments.inpshippingFirstName#</shippingFirstName>":""#
                            #trim(arguments.inpshippingLastName) NEQ ""? "<shippingLastName>#arguments.inpshippingLastName#</shippingLastName>":""#
                            #trim(arguments.inpshippingAddressLine1) NEQ ""? "<shippingAddressLine1>#arguments.inpshippingAddressLine1#</shippingAddressLine1>":""#
                            #trim(arguments.inpshippingAddressLine2) NEQ ""? "<shippingAddressLine2>#arguments.inpshippingAddressLine2#</shippingAddressLine2>":""#
                            #trim(arguments.inpshippingCity) NEQ ""? "<shippingCity>#arguments.inpshippingCity#</shippingCity>":""#
                            #trim(arguments.inpshippingState) NEQ ""? "<shippingState>#arguments.inpshippingState#</shippingState>":""#
                            #trim(arguments.inpshippingZipCode) NEQ ""? "<shippingZipCode>#arguments.inpshippingZipCode#</shippingZipCode>":""#
                            #trim(arguments.inpshippingCountry) NEQ ""? "<shippingCountry>#arguments.inpshippingCountry#</shippingCountry>":""#
                            #trim(arguments.inpshippingEmail) NEQ ""? "<shippingEmail>#arguments.inpshippingEmail#</shippingEmail>":""#
                            #trim(arguments.inpshippingPhone) NEQ ""? "<shippingPhone>#arguments.inpshippingPhone#</shippingPhone>":""#                                                                                        
                        </shipping>
                        <card>
                            <cardNumber>#RIGHT(arguments.inpcardNumber,4)#</cardNumber>
                            <cardExpiration>#arguments.inpcardExpiration#</cardExpiration>
                            <cardCVV>#arguments.inpcardCVV#</cardCVV>
                        </card>
                    </requestData>
                </UAPIRequest></cfoutput></cfsavecontent>  

            <cfhttp url="#mojo_url#" method="post" result="sale" timeout="30">
                <cfhttpparam type="header" name="Authorized" value="#mojo_token#">
                <cfhttpparam type="header" name="Content-Type" value="application/xml">
                <cfhttpparam type="body" value='#BODYDATA#'>
            </cfhttp>            
            <cfset fileContent = sale.fileContent/>  
            <cfset statusMessage = sale.status_text />
            <!--- statusCode:0 ---->
            <!--- processorResponse:Approved--->
            <cfset httpStatusCode = sale.status_code/>                       
            <cfif structKeyExists(sale, "status_code") AND sale.status_code EQ 200>                
                <cfif isXML(fileContent)>
                    <cfset myXMLDocument = XmlParse(fileContent)> 
                    <cfset selectedElements = XmlSearch(myXMLDocument, "/UAPIResponse/statusCode")>
                    <cfif ArrayLen(selectedElements) GT 0>
                        <cfset dataout.STATUSCODE = selectedElements[1].XmlText/>            
                    </cfif>
                    
                    <cfset avsResponse = XmlSearch(myXMLDocument, "/UAPIResponse/avsResponse")>
                    <cfif ArrayLen(avsResponse) GT 0>
                        <cfset dataout.AVSCODE = avsResponse[1].XmlText/>            
                    </cfif>
                                    

                    <cfset selectedElements = XmlSearch(myXMLDocument, "/UAPIResponse/processorResponseCode")>
                    <cfif ArrayLen(selectedElements) GT 0>
                        <cfset dataout.PROCESSORREPONSECODE = selectedElements[1].XmlText/>
                        <cfset selectStatusMessage = XmlSearch(myXMLDocument, "/UAPIResponse/statusMessage")>
                        <cfif ArrayLen(selectStatusMessage) GT 0>
                            <cfset statusMessage = selectStatusMessage[1].XmlText/>
                        </cfif>

                        <cfset selectProcessorResponse = XmlSearch(myXMLDocument, "/UAPIResponse/processorResponse")>
                        <cfif ArrayLen(selectProcessorResponse) GT 0>
                            <cfset processorResponse = selectProcessorResponse[1].XmlText/>
                        </cfif>

                        <cfif selectedElements[1].XmlText EQ "000">
                            <cfset selectTransactionId = XmlSearch(myXMLDocument, "/UAPIResponse/transactionId")>
                            <cfset selectCCToken = XmlSearch(myXMLDocument, "/UAPIResponse/token")> 
                            <cfset selectCCType = XmlSearch(myXMLDocument, "/UAPIResponse/type")> 

                            <cfif arrayLen(selectCCToken) GT 0>
                                <cfset dataout.TOKEN = selectCCToken[1].XmlText />    
                            </cfif> 

                            <cfif arrayLen(selectCCType) GT 0>
                                <cfset dataout.CCTYPE = selectCCType[1].XmlText />    
                            </cfif> 

                            <cfset dataout.RXRESULTCODE = 1 /> 
                            <cfset dataout.RESULT = 'SUCCESS'>                    
                            <cfset dataout.MESSAGE = "Payment successfully">                
                            <cfset dataout.TRANSACTIONID = selectTransactionId[1].XmlText />
          
                            
                            <!--- LOG TO PAMENT SUCCESS --->
                            <cfset dataout.REPORT.ORDERID = arguments.inpOrderId>
                            <cfset dataout.REPORT.TRANSACTIONID = dataout.TRANSACTIONID >
                            <cfset dataout.REPORT.AMT = arguments.inpOrderAmount>
                            <cfset dataout.REPORT.RESPONSE = processorResponse> 

                            <cfset dataout.REPORT.INPNUMBER =  right(arguments.inpcardNumber, 4) >
                            <cfset dataout.REPORT.INPCVV2 = arguments.inpcardCVV >
                            <cfset dataout.REPORT.INPFIRSTNAME = arguments.inpbillingFirstName >
                            <cfset dataout.REPORT.INPLASTNAME =  arguments.inpbillingLastName>
                            <cfset dataout.REPORT.INPLINE1 = arguments.inpbillingAddressLine1 >
                            <cfset dataout.REPORT.INPCITY = arguments.inpbillingCity>
                            <cfset dataout.REPORT.INPSTATE = arguments.inpbillingState>
                            <cfset dataout.REPORT.INPPOSTALCODE = arguments.inpbillingZipCode >
                            <cfset dataout.REPORT.PHONE = ''> 
                            <cfset dataout.REPORT.EMAIL = arguments.inpbillingEmail>
                            <cfset dataout.REPORT.TRANSACTIONTYPE = 'sale'> 
                            <cfset dataout.REPORT.METHOD = 'CC'>
                         <cfelse>
                            <cfset dataout.RESULT = "Fail">
                            <cfset dataout.RXRESULTCODE = -2>
                            <!--- <cfset dataout.MESSAGE = "Error while create payment with processorResponseCode."&processorResponse> --->
                            <cfset dataout.MESSAGE = HTMLEditFormat(processorResponse)>
                            </cfif>
                    <cfelse>
                        <cfset selectedElements = XmlSearch(myXMLDocument, "/UAPIResponse")>
                        <cfif ArrayLen(selectedElements) GT 0>
                            <cfset selectStatusMessage = XmlSearch(myXMLDocument, "/UAPIResponse/statusMessage")>
                            <cfif ArrayLen(selectStatusMessage) GT 0>
                                <cfset statusMessage = selectStatusMessage[1].XmlText/>
                            </cfif>

                            <cfset selectProcessorResponse = XmlSearch(myXMLDocument, "/UAPIResponse/processorResponse")>
                            <cfif ArrayLen(selectProcessorResponse) GT 0>
                                <cfset processorResponse = selectProcessorResponse[1].XmlText/>
                            </cfif>
                        </cfif>

                        <cfset dataout.RXRESULTCODE = -5 />
                        <cfset dataout.RESULT = 'FAIL'>                    
                        <!--- <cfset dataout.MESSAGE = "Error while create payment with statusMessage."&HTMLEditFormat(statusMessage)>  --->
                        <cfset dataout.MESSAGE = 'Invalid XML Response.'>   
                    </cfif>
                <cfelse>
                    <cfset dataout.RXRESULTCODE = -4 />
                    <cfset dataout.RESULT = 'FAIL'>                    
                    <!--- <cfset dataout.MESSAGE = "Error while create payment with statusMessage."&HTMLEditFormat(statusMessage)>  --->
                    <cfset dataout.MESSAGE = HTMLEditFormat(statusMessage)>   
                </cfif>                 
            <cfelse>
                <cfif isXML(fileContent)>
                    <cfset myXMLDocument = XmlParse(fileContent)> 
                    <cfset selectedElements = XmlSearch(myXMLDocument, "/UAPIResponse")>
                    <cfif ArrayLen(selectedElements) GT 0>
                        <cfset selectStatusMessage = XmlSearch(myXMLDocument, "/UAPIResponse/statusMessage")>
                        <cfif ArrayLen(selectStatusMessage) GT 0>
                            <cfset statusMessage = selectStatusMessage[1].XmlText/>
                        </cfif>

                        <cfset selectProcessorResponse = XmlSearch(myXMLDocument, "/UAPIResponse/processorResponse")>
                        <cfif ArrayLen(selectProcessorResponse) GT 0>
                            <cfset processorResponse = selectProcessorResponse[1].XmlText/>
                        </cfif>
                    </cfif>
                </cfif>    
                    
                <cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <!--- <cfset dataout.MESSAGE = "Error while create payment with status msg."&HTMLEditFormat(statusMessage)&"" > --->
                <cfset dataout.MESSAGE = HTMLEditFormat(statusMessage)>
            </cfif>

            <!--- Log payment --->
            <cftry>
                <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
                    <cfinvokeargument name="moduleName" value="#arguments.inpModuleName#">
                    <cfinvokeargument name="status_code" value="#httpStatusCode#">
                    <cfinvokeargument name="status_text" value="#processorResponse#">
                    <cfinvokeargument name="errordetail" value="#statusMessage#">
                    <cfinvokeargument name="filecontent" value="#fileContent#">
                    <cfinvokeargument name="paymentdata" value="#BODYDATATEMP#">
                    <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    <cfinvokeargument name="paymentmethod" value="#_MOJO_GATEWAY#">
                </cfinvoke>
                <cfcatch type="any">
                </cfcatch>
            </cftry>

            <cfcatch type="any">
                <cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <cfset dataout.MESSAGE = cfcatch.MESSAGE>               
            </cfcatch>
        </cftry>
        <cfreturn dataout />
    </cffunction>

    <cffunction name="VoidTransaction" access="public" output="false" hint="void an transaction">
        <cfargument name="inpTransactionId" required="true"/>
        <cfargument name="inpOrderAmount" required="false" default="1"/>

        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "Fail to void transaction">
        <cfset dataout.USERCARDID = ""/>
        <cfset dataout.CCTYPE = ""/>

        <cfif trim(len(inpTransactionId)) GT 0>
            <cfset var BODYDATA = "">
            <cfset var returndata = ""/>
            <cfset var rxVoidTransaction = ""/>

            <cftry>
                <cfsavecontent variable="BODYDATA"><cfoutput><?xml version="1.0" encoding="UTF-8"?>
                <UAPIRequest>
                    <transaction>
                        <process>online</process>
                        <type>void</type>
                    </transaction>
                    <requestData>
                    <orderAmount>#arguments.inpOrderAmount#</orderAmount>
                    <uapiTransactionId>#arguments.inpTransactionId#</uapiTransactionId>
                    </requestData>
                </UAPIRequest></cfoutput></cfsavecontent>

                <cfhttp url="#mojo_url#" method="post" result="rxVoidTransaction" timeout="30">
                    <cfhttpparam type="header" name="Authorized" value="#mojo_token#">
                    <cfhttpparam type="header" name="Content-Type" value="application/xml">
                    <cfhttpparam type="body" value='#BODYDATA#'>
                </cfhttp>

                <cfif structKeyExists(rxVoidTransaction, "status_code") AND rxVoidTransaction.status_code EQ 200>
                    <cfif isXML(rxVoidTransaction.fileContent)>
                        <cfset returndata =  xmlParse(rxVoidTransaction.fileContent)>
                    </cfif>
                   
                    <cfset dataout.RXRESULTCODE = 1 /> 
                    <cfset dataout.RESULT = "SUCCESS">                    
                    <cfset dataout.MESSAGE = "Transaction was void.">
                <cfelse>
                    <cfset dataout.RXRESULTCODE = -1 /> 
                    <cfset dataout.RESULT = 'FAIL'>                    
                    <cfset dataout.MESSAGE = "Error while void transaction.">
                </cfif>

                <cfcatch type="any">
                    <cfset dataout.RXRESULTCODE = -1 /> 
                    <cfset dataout.RESULT = 'FAIL'>                    
                    <cfset dataout.MESSAGE = cfcatch.MESSAGE>               
                </cfcatch>

            </cftry>
        </cfif>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="storeCreditCardInVault" access="public" output="true" hint="store Credit Card In Vault">
        <cfinvokeargument name="inpCardObject" value="#arguments.inpCardObject#"/>
        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>

        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">
        <cfset dataout.DATA = {}>
        <cfset dataout.USERCARDID = ''>
        <cfset dataout.CCTYPE = 'MC'>
        <cfset dataout.AVSCODE = ''>
        <cfset var paymentData = {}/>
        <cfset var RxAuthorization = {}/>

        <cftry>
            
            <cfif isJson(inpCardObject)>
                <cfset paymentData = deserializeJson(inpCardObject)>
                <cfif structKeyExists(paymentData, "expirationDate")>
                    <cfset paymentData.expirationDate = LEFT(paymentData.expirationDate,2)&""&RIGHT(paymentData.expirationDate,2)>
                <cfelse>
                    <cfset paymentData.expirationDate = paymentData.inpExpireMonth&""&RIGHT(paymentData.inpExpreYear,2)>    
                </cfif>
                
                <cfinvoke component="session.sire.models.cfc.vault-mojo" method="Authorization" returnvariable="RxAuthorization">
                    <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    <cfinvokeargument name="inpcardNumber" value="#paymentData.inpNumber#">     
                    <cfinvokeargument name="inpcurrencyCode" value="840">
                    <cfinvokeargument name="inpcardExpiration" value="#paymentData.expirationDate#">
                    <cfinvokeargument name="inpcardCVV" value="#paymentData.inpCvv2#">
                    <cfinvokeargument name="inpbillingFirstName" value="#paymentData.inpFirstName#">
                    <cfinvokeargument name="inpbillingLastName" value="#paymentData.inpLastName#">
                    <cfinvokeargument name="inpbillingAddressLine1" value="#paymentData.inpLine1#">
                    <cfinvokeargument name="inpbillingCity" value="#paymentData.inpCity#">
                    <cfinvokeargument name="inpbillingState" value="#paymentData.inpState#">
                    <cfinvokeargument name="inpbillingZipCode" value="#paymentData.inpPostalCode#">
                    <cfinvokeargument name="inpbillingCountry" value="#paymentData.inpCountryCode#">
                    <cfinvokeargument name="inpbillingEmail" value="#paymentData.inpEmail#">
                    <!--- <cfinvokeargument name="inpbillingPhone" value="#paymentData.phone#"> --->
                </cfinvoke>
                
                <cfif RxAuthorization.RXRESULTCODE EQ 1>
                    <cfset dataout.RXRESULTCODE = 1>
                    <cfset dataout.RESULT = "Success">
                    <cfif structKeyExists(RxAuthorization, "REPORT")>
                        <cfset dataout.REPORT = RxAuthorization.REPORT>
                    </cfif>

                    <cfset dataout.USERCARDID = RxAuthorization.TOKEN>
                    <cfset dataout.CCTYPE = RxAuthorization.CCTYPE> 
                    <cfset dataout.AVSCODE = RxAuthorization.AVSCODE>                                           

                    <!--- VOID TRANSACTION --->

                <cfelse>
                    <cfset dataout.RXRESULTCODE = RxAuthorization.RXRESULTCODE>
                    <cfset dataout.MESSAGE = RxAuthorization.MESSAGE>
                    <cfset dataout.AVSCODE = RxAuthorization.AVSCODE>                                           
                </cfif>

            <cfelse>
                <cfset dataout.RXRESULTCODE = -2>
                <cfset dataout.MESSAGE = 'Invalid inpPaymentdata'>
            </cfif>

        <cfcatch type="any">
            <cfset dataout.RXRESULTCODE = -1 /> 
            <cfset dataout.RESULT = 'FAIL'>                    
            <cfset dataout.MESSAGE = cfcatch.MESSAGE>  
             <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>             
        </cfcatch>

        </cftry>
        <cfreturn dataout/>
    </cffunction>  

</cfcomponent>