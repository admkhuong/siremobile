<cfcomponent>

	<cfinclude template="/public/paths.cfm" >
    <cfinclude template="/session/cfc/csc/constants.cfm">
    <cfinclude template="/session/sire/configs/credits.cfm">
	<cffunction name="GetAccessTokenKey" access="remote" output="true" hint="">                
                
        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = 'FAIL'>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.TOKENKEY = '' />
        <cfset dataout.EXPIRES = 0 />
        <cfset var getAccessToken=''> 
        <cfset var fileContent=''> 
        <cfset var RetVarGetAdminPermission	= '' />
        
        <cftry>
            			
            <cfhttp url="#paypal_token#" method="post" result="getAccessToken" timeout="30">
                <cfhttpparam type="header" name="Authorization" value="Basic #ToBase64("#paypal_ClienID#:#paypal_Secret#")#" />
                <cfhttpparam type="header" name="Content-Type" value="application/x-www-form-urlencoded" >
                <cfhttpparam type="header" name="Accept" value=" application/json" >                    
                <cfhttpparam type="header" name="Accept-Language" value="en_US" >                  
                <cfhttpparam type="formField" name="grant_type" value="client_credentials" >
                
            </cfhttp>
            <cfset dataout.RESPONSE =getAccessToken />                 
            <cfif structKeyExists(getAccessToken, "status_code") AND getAccessToken.status_code EQ 200>
                <cfset dataout.RXRESULTCODE = 1 /> 
                <cfset dataout.RESULT = 'SUCCESS'>
                <cfset dataout.MESSAGE = getAccessToken.statuscode>    
                <cfset fileContent= getAccessToken.filecontent>
                
                <cfset dataout.TOKENKEY = DeserializeJSON(fileContent).access_token/>
                <cfset dataout.EXPIRES = DeserializeJSON(fileContent).expires_in />
            <cfelse>
                <cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = getAccessToken.errordetail>
                <cfset dataout.MESSAGE = getAccessToken.statuscode>                        
            </cfif>
                                        
            <cfcatch>                
                <cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <cfset dataout.MESSAGE = "Error">                                                
            </cfcatch>
        </cftry>
        
        <cfreturn dataout />
    </cffunction> 
    
	 <!--- get detail a credit card ---> 
    <cffunction name="getDetailCreditCardInVault" access="remote" output="false" hint="">
    	<cfargument name="inpCardId" type="string" required="false" default="">
		<cfargument name="inpUserId" TYPE="string" required="false" default="#session.UserId#">

    	<cfset var dataout = structNew()>
		<cfset var CheckExistsPaymentMethod = ""/>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.VAULTID = ''>        
        
        <cfset dataout.DATA = ''>
        <cfset dataout.MESSAGE = "Fail to get detail a card Vault">
		<cfset dataout.CUSTOMERINFO = structNew()>
        <cfset var content = ''>

        <cftry>
			<cfquery name="CheckExistsPaymentMethod" datasource="#Session.DBSourceEBM#">
				SELECT
					extraNote,
                    PaymentMethodID_vch
				FROM
					simplebilling.authorize_user
				WHERE
					UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"> 		
				AND 
					paymentGateway_ti = 2	
				AND 
					status_ti = 1	
			</cfquery>
			<cfif CheckExistsPaymentMethod.RecordCount GT 0>
                    <cfset dataout.VAULTID = CheckExistsPaymentMethod.PaymentMethodID_vch>
					<cfset dataout.RXRESULTCODE = 1 /> 
					<cfset dataout.RESULT = 'SUCCESS'>
					<cfset dataout.MESSAGE = 'Get ok'>    
					<cfset content= DeserializeJSON(CheckExistsPaymentMethod.extraNote)/>

					<cfset dataout.CUSTOMERINFO.firstName = content.INPFIRSTNAME/> 
					<cfset dataout.CUSTOMERINFO.lastName = content.INPLASTNAME>
                    <cfif structKeyExists(content, "INPEMAIL")>
					   <cfset dataout.CUSTOMERINFO.emailAddress = content.INPEMAIL>
                    <cfelse>
                        <cfset dataout.CUSTOMERINFO.emailAddress = ''>   
                    </cfif>
					<cfset dataout.CUSTOMERINFO.expirationDate = content.INPEXPIREMONTH &'/'& content.INPEXPREYEAR>                    
					<cfset dataout.CUSTOMERINFO.maskedNumber = "XXXXXXXX" & content.INPNUMBER>
					<cfset dataout.CUSTOMERINFO.line1 = content.INPLINE1>
					<cfset dataout.CUSTOMERINFO.city = content.INPCITY>
					<cfset dataout.CUSTOMERINFO.state = content.INPSTATE>
					<cfset dataout.CUSTOMERINFO.zip = content.INPPOSTALCODE>
					<cfset dataout.CUSTOMERINFO.country = content.INPCOUNTRYCODE> 
					<cfset dataout.CUSTOMERINFO.phone = ''>
					<cfset dataout.CUSTOMERINFO.paymentType = 'card'>
                    <cfif structKeyExists(content, "inpAVS")>					   
                       <cfset dataout.CUSTOMERINFO.AVSCODE = content.inpAVS>
                    <cfelse>
                        <cfset dataout.CUSTOMERINFO.AVSCODE = "">
                    </cfif>
                    
					<cfset dataout.CUSTOMERINFO.cvv = content.INPCVV2>

                    <cfswitch expression="#TRIM(LCASE(content.INPTYPE))#"> 
                        <cfcase value="visa"> 
                            <cfset dataout.CUSTOMERINFO.creditCardType = "VISA"/>
                        </cfcase> 
                        <cfcase value="mastercard"> 
                            <cfset dataout.CUSTOMERINFO.creditCardType = "MASTERCARD"/>
                        </cfcase> 
                        <cfcase value="discover"> 
                            <cfset dataout.CUSTOMERINFO.creditCardType = "DISCOVER"/>
                        </cfcase> 
                         <cfcase value="amex"> 
                            <cfset dataout.CUSTOMERINFO.creditCardType = "AMERICANEXPRESS"/>
                        </cfcase>
                        <cfdefaultcase>
                            <cfset dataout.CUSTOMERINFO.creditCardType = "VISA"/> 
                        </cfdefaultcase> 
                    </cfswitch>     					
            <cfelse>
                <cfset dataout.RESULT = "">
                <cfset dataout.RXRESULTCODE = 0>
                <cfset dataout.MESSAGE = "Get Card Detail Fail">
            </cfif>

        	<cfcatch type="any">
        		<cfset dataout.RESULT = "FAIL">
		        <cfset dataout.RXRESULTCODE = -1>
		        <cfset dataout.MESSAGE = "Exception">
        	</cfcatch>
        </cftry>
		<cfreturn dataout />
    </cffunction>

	<cffunction name="Authorization" access="remote" output="false" hint="">
        <cfargument name="inpUserId" TYPE="string" required="false" default="#Session.UserId#">
		<cfargument name="inporderId" TYPE="string" required="false" default="#Left(CreateUUID(),25)#">
        <cfargument name="inpcardNumber" TYPE="string" required="true" default="">		

        <cfargument name="inporderAmount" TYPE="string" required="false" default="1">
        <cfargument name="inpcurrencyCode" TYPE="string" required="false" default="USD">
        <cfargument name="inpcardExpiration" TYPE="string" required="true" default="">
        <cfargument name="inpcardCVV" TYPE="string" required="true" default="">

        <cfargument name="inpbillingFirstName" TYPE="string" required="true" default="">
        <cfargument name="inpbillingLastName" TYPE="string" required="true" default="">
        <cfargument name="inpbillingAddressLine1" TYPE="string" required="true" default="">
        <cfargument name="inpbillingCity" TYPE="string" required="true" default=" ">
        <cfargument name="inpbillingState" TYPE="string" required="true" default=" ">
        <cfargument name="inpbillingZipCode" TYPE="string" required="true" default=" ">
        <cfargument name="inpbillingCountry" TYPE="string" required="true" default=" ">
        <cfargument name="inpbillingEmail" TYPE="string" required="false" default=" ">
        <cfargument name="inpbillingPhone" TYPE="string" required="false" default=" ">

        <cfargument name="inpModuleName" TYPE="string" required="false" default="Create authorization transaction">
                

        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">
		<cfset dataout.STATUSCODE = ""/>
        <cfset dataout.PROCESSORREPONSECODE = ""/>

        <cfset dataout.TRANSACTIONID = "">
		<cfset dataout.AVSCODE = "">
        <cfset dataout.TOKEN = ""/>
        <cfset dataout.CCTYPE = ""/>

        <cfset dataout.REPORT = {}/>
        <cfset dataout.REPORT.ORDERID = ''>
        <cfset dataout.REPORT.TRANSACTIONID = '' >
        <cfset dataout.REPORT.AMT = ''>
        <cfset dataout.REPORT.RESPONSE = ''> 

        <cfset dataout.REPORT.INPNUMBER = ''>
        <cfset dataout.REPORT.INPCVV2 = '' >
        <cfset dataout.REPORT.INPFIRSTNAME = '' >
        <cfset dataout.REPORT.INPLASTNAME =  ''>
        <cfset dataout.REPORT.INPLINE1 = '' >
        <cfset dataout.REPORT.INPCITY = ''>
        <cfset dataout.REPORT.INPSTATE = ''>
        <cfset dataout.REPORT.INPPOSTALCODE = '' >
        <cfset dataout.REPORT.PHONE = ''> 
        <cfset dataout.REPORT.EMAIL = ''>
        <cfset dataout.REPORT.TRANSACTIONTYPE = ''> 
        <cfset dataout.REPORT.METHOD = ''>

        <cfset var BODYDATA = "">
        <cfset var BODYDATATEMP = ""/>
		<cfset var returndata = ""/>
        <cfset var sale = ""/>
        <cfset var myXMLDocument=''>
        <cfset var selectedElements=''>        
        <Cfset var processorResponse = ''/>
        <Cfset var statusMessage = ''/>
        <Cfset var fileContent = ''/>
        <Cfset var httpStatusCode = ''/>
        <Cfset var dateFromCcInput = ''/>
        <cfset var selectStatusMessage = ''/>
        <cfset var selectProcessorResponse = ''/>
        <cfset var selectTransactionId = ''/>
		<cfset var inpType="visa">
        <cfset var rtcheckCcType	= '' />

        <cftry>            
            <cfif IsNumeric(arguments.inpbillingState) OR IsNumeric(arguments.inpbillingAddressLine1) OR IsNumeric(arguments.inpbillingCity)>
                <cfthrow  type = "any" message = "Billing Address, City, State should be string" detail = "Billing Address, City, State should be string" > 
            </cfif>
            
            <cfif Len(arguments.inpbillingState) GT 2>
                <cfthrow  type = "any" message = "Billing State should be 2 characters only" detail = "Billing State should be 2 characters only" > 
            </cfif>
            <cfif Len(arguments.inpbillingAddressLine1) GT 35>
                <cfthrow  type = "any" message = "Billing Address should be 35 characters only" detail = "Billing Address should be 35 characters only" > 
            </cfif>
            <cfif Len(arguments.inpbillingCity) GT 20>
                <cfthrow  type = "any" message = "Billing City should be 20 characters only" detail = "Billing City should be 20 characters only" > 
            </cfif>
            <!--- check for same month year compage with last day of month, if not compare first day is enough---->            
           <cfif Int(Month(NOW())) EQ Int(left(arguments.inpcardExpiration,2)) AND Int(right(Year(NOW()),2)) EQ Int(right(arguments.inpcardExpiration,2))>
                <cfset dateFromCcInput= createDate("20" & right(arguments.inpcardExpiration,2), left(arguments.inpcardExpiration,2), daysInMonth(NOW()))>
            <cfelse>            
                <cfset dateFromCcInput= createDate("20" & right(arguments.inpcardExpiration,2), left(arguments.inpcardExpiration,2), 28)>
            </cfif>
            <cfif DateCompare(dateFromCcInput, NOW()) LT 1>
                <cfthrow  type = "any" message = "Your Credit Card has been expired! Please check the Expiration Date or add another Credit Card." detail = "Your Credit Card has been expired! Please check the Expiration Date or add another Credit Card." > 
            </cfif>
			<cfinvoke component="session.sire.models.cfc.payment.cc-tools" method="checkCcType" returnvariable="rtcheckCcType">
                <cfinvokeargument name="inpcardNumber" value="#arguments.inpcardNumber#">                                
            </cfinvoke> 
            <cfset dataout.CCTYPE = rtcheckCcType.CCTYPE/>
			
			<cfset var cfg = { path = "paypal.payflow.payflowGateway", Partner = "#pf_Partner#", MerchantAccount ='#pf_MerchantAccount#',Username = '#pf_Username#',Password = '#pf_Password#',CheckAVS = pf_CheckAVS, CheckCVV = pf_CheckCVV,  } />
			<cfset var svc = createObject("component", "cfpayment.api.core").init(cfg) />
			<cfset var gw = svc.getGateway() />

			<!--- <cfset account = svc.createCreditCard().setAccount(4111111111111111).setMonth(10).setYear(year(now())+1).setFirstName("John").setLastName("Doe").setAddress("608 Mina").setPostalCode("9200") /> --->
			<cfset var account = svc.createCreditCard().setAccount(arguments.inpcardNumber).setMonth(Int(left(arguments.inpcardExpiration,2))).setYear(Int(right(arguments.inpcardExpiration,4))).setFirstName(arguments.inpbillingFirstName).setLastName(arguments.inpbillingLastName).setAddress(arguments.inpbillingAddressLine1).setPostalCode(arguments.inpbillingZipCode).setCity(arguments.inpbillingCity).setRegion(arguments.inpbillingState).setCountry(arguments.inpbillingCountry) />

			<cfset var money = svc.createMoney(arguments.inporderAmount*100) />
			<cfset var sale = gw.authorize(money = money, account = account) />



			<cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="fileContent">
				<cfinvokeargument name="QueryString" value="#sale.getResult()#"/>	
			</cfinvoke>		
			

            

            <cfset BODYDATATEMP={
				number: Right(arguments.inpcardNumber,4),				
				order: arguments.inporderId,				
				expire_month:Int(left(arguments.inpcardExpiration,2)),
				expire_year: Int(right(arguments.inpcardExpiration,4)),
				cvv2: arguments.inpcardCVV,
				first_name: arguments.inpbillingFirstName,
				last_name: arguments.inpbillingLastName,
				billing_address:{
					line1: arguments.inpbillingAddressLine1,
					city: arguments.inpbillingCity,
					country_code: arguments.inpbillingCountry,
					postal_code: arguments.inpbillingZipCode,
					state:arguments.inpbillingState					
				},
				external_customer_id:arguments.inpUserId,				
				money: arguments.inporderAmount
			}>      
            
			
        	<cfset dataout.TOKEN = sale.GETTRANSACTIONID()/>
            <cfset statusMessage = fileContent.RESPMSG />            
            <cfset httpStatusCode = sale.GETSTATUSCODE()/>            			 
            <cfset dataout.AVSCODE = sale.GETAVSCODE()>
            <cfif httpStatusCode EQ 200>   
                <cfif structKeyExists(fileContent, "PROCAVS")>  
                    <cfset dataout.AVSCODE = fileContent.PROCAVS>  
                </cfif>
				<cfif fileContent.RESULT EQ 0>
					<cfset dataout.RXRESULTCODE = 1 /> 
					<cfset dataout.RESULT = 'SUCCESS'>                    
					<cfset dataout.MESSAGE = "Payment successfully">                
					<cfset dataout.TRANSACTIONID = fileContent.PNREF/> 
                    
					<!--- LOG TO PAMENT SUCCESS --->
					<cfset dataout.REPORT.ORDERID = arguments.inpOrderId>
					<cfset dataout.REPORT.TRANSACTIONID = dataout.TRANSACTIONID >
					<cfset dataout.REPORT.AMT = arguments.inpOrderAmount>
					<cfset dataout.REPORT.RESPONSE = statusMessage> 

					<cfset dataout.REPORT.INPNUMBER =  right(arguments.inpcardNumber, 4) >
					<cfset dataout.REPORT.INPCVV2 = arguments.inpcardCVV >
					<cfset dataout.REPORT.INPFIRSTNAME = arguments.inpbillingFirstName >
					<cfset dataout.REPORT.INPLASTNAME =  arguments.inpbillingLastName>
					<cfset dataout.REPORT.INPLINE1 = arguments.inpbillingAddressLine1 >
					<cfset dataout.REPORT.INPCITY = arguments.inpbillingCity>
					<cfset dataout.REPORT.INPSTATE = arguments.inpbillingState>
					<cfset dataout.REPORT.INPPOSTALCODE = arguments.inpbillingZipCode >
					<cfset dataout.REPORT.PHONE = ''> 
					<cfset dataout.REPORT.EMAIL = arguments.inpbillingEmail>
					<cfset dataout.REPORT.TRANSACTIONTYPE = 'sale'> 
					<cfset dataout.REPORT.METHOD = 'CC'>  
				<cfelse>
					<cfset dataout.RESULT = "Fail">
					<cfset dataout.RXRESULTCODE = -2>					
					<cfset dataout.MESSAGE = HTMLEditFormat(statusMessage)>   					
				</cfif>                 
                                          		
            <cfelse>                                   
            	<cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <!--- <cfset dataout.MESSAGE = "Error while create payment with status msg."&HTMLEditFormat(statusMessage)&"" > --->
                <cfset dataout.MESSAGE = HTMLEditFormat(statusMessage)>
            </cfif>

            <!--- Log payment --->
            <cftry>
                <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
                    <cfinvokeargument name="moduleName" value="#arguments.inpModuleName#">
                    <cfinvokeargument name="status_code" value="#httpStatusCode#">
                    <cfinvokeargument name="status_text" value="#statusMessage#">
                    <cfinvokeargument name="errordetail" value="#statusMessage#">
                    <cfinvokeargument name="filecontent" value="#serializeJSON(fileContent)#">
                    <cfinvokeargument name="paymentdata" value="#BODYDATATEMP#">
                    <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    <cfinvokeargument name="paymentmethod" value="2">
                </cfinvoke>
                <cfcatch type="any">
					
                </cfcatch>
            </cftry>

        	<cfcatch type="any">
        		<cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <cfset dataout.MESSAGE = cfcatch.MESSAGE> 				
        	</cfcatch>

        </cftry>
		<cfreturn dataout />
    </cffunction>
	<cffunction name="Void" access="remote" output="false" hint="">        
        <cfargument name="inpTransactionID" TYPE="string" required="true" default="">		        
        <cfargument name="inpModuleName" TYPE="string" required="false" default="Void authorization transaction">
                

        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">
		<cfset dataout.STATUSCODE = ""/>
        

        <cfset var BODYDATA = "">
        <cfset var BODYDATATEMP = ""/>
		<cfset var returndata = ""/>
        <cfset var void = ""/>
       <cfset var statusMessage	= '' />
        <cfset var httpStatusCode	= '' />
        <cfset var fileContent	= '' />

        <cftry>            
            
			
			<cfset var cfg = { path = "paypal.payflow.payflowGateway", Partner = "#pf_Partner#", MerchantAccount ='#pf_MerchantAccount#',Username = '#pf_Username#',Password = '#pf_Password#',CheckAVS = pf_CheckAVS, CheckCVV = pf_CheckCVV,  } />
			<cfset var svc = createObject("component", "cfpayment.api.core").init(cfg) />
			<cfset var gw = svc.getGateway() />
			
			<cfset var void = gw.void(transactionID = arguments.inpTransactionID) />



			<cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="fileContent">
				<cfinvokeargument name="QueryString" value="#void.getResult()#"/>	
			</cfinvoke>		
			  
			
            <cfset statusMessage = fileContent.RESPMSG />            
            <cfset httpStatusCode = void.GETSTATUSCODE()/>            			 
            <cfif httpStatusCode EQ 200>       
				<cfif fileContent.RESULT EQ 0>
					<cfset dataout.RXRESULTCODE = 1 /> 
					<cfset dataout.RESULT = 'SUCCESS'>                    
					<cfset dataout.MESSAGE = "Void successfully">                
					 
				<cfelse>
					<cfset dataout.RESULT = "Fail">
					<cfset dataout.RXRESULTCODE = -2>					
					<cfset dataout.MESSAGE = HTMLEditFormat(statusMessage)>   					
				</cfif>                 
                                          		
            <cfelse>                                   
            	<cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <!--- <cfset dataout.MESSAGE = "Error while create payment with status msg."&HTMLEditFormat(statusMessage)&"" > --->
                <cfset dataout.MESSAGE = HTMLEditFormat(statusMessage)>
            </cfif>

            

        	<cfcatch type="any">
        		<cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <cfset dataout.MESSAGE = cfcatch.MESSAGE> 				
        	</cfcatch>

        </cftry>
		<cfreturn dataout />
    </cffunction>
	<cffunction name="Capture" access="remote" output="false" hint="">        
        <cfargument name="inpTransactionID" TYPE="string" required="true" default="">	
		<cfargument name="inporderAmount" TYPE="string" required="true" default="0">	
			        
        <cfargument name="inpModuleName" TYPE="string" required="false" default="Capture authorization transaction">
                

        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">
		<cfset dataout.STATUSCODE = ""/>
        

        <cfset var BODYDATA = "">
        <cfset var BODYDATATEMP = ""/>
		<cfset var returndata = ""/>
        <cfset var capture = ""/>
        <cfset var statusMessage	= '' />
        <cfset var httpStatusCode	= '' />
        <cfset var fileContent	= '' />
       

        <cftry>            
            
			
			<cfset var cfg = { path = "paypal.payflow.payflowGateway", Partner = "#pf_Partner#", MerchantAccount ='#pf_MerchantAccount#',Username = '#pf_Username#',Password = '#pf_Password#',CheckAVS = pf_CheckAVS, CheckCVV = pf_CheckCVV,  } />
			<cfset var svc = createObject("component", "cfpayment.api.core").init(cfg) />
			<cfset var gw = svc.getGateway() />
			<cfset var money = svc.createMoney(arguments.inporderAmount*100) />
			<cfset var capture = gw.capture(money = money, authorization = arguments.inpTransactionID) />
						
			

			<cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="fileContent">
				<cfinvokeargument name="QueryString" value="#capture.getResult()#"/>	
			</cfinvoke>		
			  
			
            <cfset statusMessage = fileContent.RESPMSG />            
            <cfset httpStatusCode = capture.GETSTATUSCODE()/>            			 
            <cfif httpStatusCode EQ 200>       
				<cfif fileContent.RESULT EQ 0>
					<cfset dataout.RXRESULTCODE = 1 /> 
					<cfset dataout.RESULT = 'SUCCESS'>                    
					<cfset dataout.MESSAGE = "Void successfully">                
					 
				<cfelse>
					<cfset dataout.RESULT = "Fail">
					<cfset dataout.RXRESULTCODE = -2>					
					<cfset dataout.MESSAGE = HTMLEditFormat(statusMessage)>   					
				</cfif>                 
                                          		
            <cfelse>                                   
            	<cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <!--- <cfset dataout.MESSAGE = "Error while create payment with status msg."&HTMLEditFormat(statusMessage)&"" > --->
                <cfset dataout.MESSAGE = HTMLEditFormat(statusMessage)>
            </cfif>

            

        	<cfcatch type="any">
        		<cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <cfset dataout.MESSAGE = cfcatch.MESSAGE> 				
        	</cfcatch>

        </cftry>
		<cfreturn dataout />
    </cffunction>
    <cffunction name="Upload" access="remote" output="false" hint="">
        <cfargument name="inpUserId" TYPE="string" required="false" default="#Session.UserId#">
		<cfargument name="inporderId" TYPE="string" required="false" default="#Left(CreateUUID(),25)#">
        <cfargument name="inpcardNumber" TYPE="string" required="true" default="">		
        
        <cfargument name="inpcurrencyCode" TYPE="string" required="false" default="USD">
        <cfargument name="inpcardExpiration" TYPE="string" required="true" default="">
        <cfargument name="inpcardCVV" TYPE="string" required="true" default="">

        <cfargument name="inpbillingFirstName" TYPE="string" required="true" default="">
        <cfargument name="inpbillingLastName" TYPE="string" required="true" default="">
        <cfargument name="inpbillingAddressLine1" TYPE="string" required="true" default="">
        <cfargument name="inpbillingCity" TYPE="string" required="true" default=" ">
        <cfargument name="inpbillingState" TYPE="string" required="true" default=" ">
        <cfargument name="inpbillingZipCode" TYPE="string" required="true" default=" ">
        <cfargument name="inpbillingCountry" TYPE="string" required="true" default=" ">
        <cfargument name="inpbillingEmail" TYPE="string" required="false" default=" ">
        <cfargument name="inpbillingPhone" TYPE="string" required="false" default=" ">

        <cfargument name="inpModuleName" TYPE="string" required="false" default="Create Upload Card transaction">
                

        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">
		<cfset dataout.STATUSCODE = ""/>
        <cfset dataout.PROCESSORREPONSECODE = ""/>

        <cfset dataout.TRANSACTIONID = "">
		<cfset dataout.AVSCODE = "">
        <cfset dataout.TOKEN = ""/>
        <cfset dataout.CCTYPE = ""/>

        <cfset dataout.REPORT = {}/>
        <cfset dataout.REPORT.ORDERID = ''>
        <cfset dataout.REPORT.TRANSACTIONID = '' >
        <cfset dataout.REPORT.AMT = ''>
        <cfset dataout.REPORT.RESPONSE = ''> 

        <cfset dataout.REPORT.INPNUMBER = ''>
        <cfset dataout.REPORT.INPCVV2 = '' >
        <cfset dataout.REPORT.INPFIRSTNAME = '' >
        <cfset dataout.REPORT.INPLASTNAME =  ''>
        <cfset dataout.REPORT.INPLINE1 = '' >
        <cfset dataout.REPORT.INPCITY = ''>
        <cfset dataout.REPORT.INPSTATE = ''>
        <cfset dataout.REPORT.INPPOSTALCODE = '' >
        <cfset dataout.REPORT.PHONE = ''> 
        <cfset dataout.REPORT.EMAIL = ''>
        <cfset dataout.REPORT.TRANSACTIONTYPE = ''> 
        <cfset dataout.REPORT.METHOD = ''>

        <cfset var BODYDATA = "">
        <cfset var BODYDATATEMP = ""/>
		<cfset var returndata = ""/>
        <cfset var sale = ""/>
        <cfset var myXMLDocument=''>
        <cfset var selectedElements=''>        
        <Cfset var processorResponse = ''/>
        <Cfset var statusMessage = ''/>
        <Cfset var fileContent = ''/>
        <Cfset var httpStatusCode = ''/>
        <Cfset var dateFromCcInput = ''/>
        <cfset var selectStatusMessage = ''/>
        <cfset var selectProcessorResponse = ''/>
        <cfset var selectTransactionId = ''/>
		<cfset var inpType="visa">
        <cfset var rtcheckCcType	= '' />

        <cftry>            
            <cfif IsNumeric(arguments.inpbillingState) OR IsNumeric(arguments.inpbillingAddressLine1) OR IsNumeric(arguments.inpbillingCity)>
                <cfthrow  type = "any" message = "Billing Address, City, State should be string" detail = "Billing Address, City, State should be string" > 
            </cfif>
            
            <cfif Len(arguments.inpbillingState) GT 2>
                <cfthrow  type = "any" message = "Billing State should be 2 characters only" detail = "Billing State should be 2 characters only" > 
            </cfif>
            <cfif Len(arguments.inpbillingAddressLine1) GT 35>
                <cfthrow  type = "any" message = "Billing Address should be 35 characters only" detail = "Billing Address should be 35 characters only" > 
            </cfif>
            <cfif Len(arguments.inpbillingCity) GT 20>
                <cfthrow  type = "any" message = "Billing City should be 20 characters only" detail = "Billing City should be 20 characters only" > 
            </cfif>
            <!--- check for same month year compage with last day of month, if not compare first day is enough---->            
           <cfif Int(Month(NOW())) EQ Int(left(arguments.inpcardExpiration,2)) AND Int(right(Year(NOW()),2)) EQ Int(right(arguments.inpcardExpiration,2))>
                <cfset dateFromCcInput= createDate("20" & right(arguments.inpcardExpiration,2), left(arguments.inpcardExpiration,2), daysInMonth(NOW()))>
            <cfelse>            
                <cfset dateFromCcInput= createDate("20" & right(arguments.inpcardExpiration,2), left(arguments.inpcardExpiration,2), 28)>
            </cfif>
            <cfif DateCompare(dateFromCcInput, NOW()) LT 1>
                <cfthrow  type = "any" message = "Your Credit Card has been expired! Please check the Expiration Date or add another Credit Card." detail = "Your Credit Card has been expired! Please check the Expiration Date or add another Credit Card." > 
            </cfif>
			<cfinvoke component="session.sire.models.cfc.payment.cc-tools" method="checkCcType" returnvariable="rtcheckCcType">
                <cfinvokeargument name="inpcardNumber" value="#arguments.inpcardNumber#">                                
            </cfinvoke> 
            <cfset dataout.CCTYPE = rtcheckCcType.CCTYPE/>
			
			<cfset var cfg = { path = "paypal.payflow.payflowGateway", Partner = "#pf_Partner#", MerchantAccount ='#pf_MerchantAccount#',Username = '#pf_Username#',Password = '#pf_Password#',CheckAVS = pf_CheckAVS, CheckCVV = pf_CheckCVV,  } />
			<cfset var svc = createObject("component", "cfpayment.api.core").init(cfg) />
			<cfset var gw = svc.getGateway() />

			<!--- <cfset account = svc.createCreditCard().setAccount(4111111111111111).setMonth(10).setYear(year(now())+1).setFirstName("John").setLastName("Doe").setAddress("608 Mina").setPostalCode("9200") /> --->
			<cfset var account = svc.createCreditCard().setAccount(arguments.inpcardNumber).setMonth(Int(left(arguments.inpcardExpiration,2))).setYear(Int(right(arguments.inpcardExpiration,4))).setFirstName(arguments.inpbillingFirstName).setLastName(arguments.inpbillingLastName).setAddress(arguments.inpbillingAddressLine1).setPostalCode(arguments.inpbillingZipCode).setCity(arguments.inpbillingCity).setRegion(arguments.inpbillingState).setCountry(arguments.inpbillingCountry) />
			
			<cfset var sale = gw.Upload( account = account) />



			<cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="fileContent">
				<cfinvokeargument name="QueryString" value="#sale.getResult()#"/>	
			</cfinvoke>		
			

            

            <cfset BODYDATATEMP={
				number: Right(arguments.inpcardNumber,4),				
				order: arguments.inporderId,				
				expire_month:Int(left(arguments.inpcardExpiration,2)),
				expire_year: Int(right(arguments.inpcardExpiration,4)),
				cvv2: arguments.inpcardCVV,
				first_name: arguments.inpbillingFirstName,
				last_name: arguments.inpbillingLastName,
				billing_address:{
					line1: arguments.inpbillingAddressLine1,
					city: arguments.inpbillingCity,
					country_code: arguments.inpbillingCountry,
					postal_code: arguments.inpbillingZipCode,
					state:arguments.inpbillingState					
				},
				external_customer_id:arguments.inpUserId				
			}>      
			
        	<cfset dataout.TOKEN = fileContent.PNREF/>
            <cfset statusMessage = fileContent.RESPMSG />            
            <cfset httpStatusCode = sale.GETSTATUSCODE()/>            			 
            <cfif httpStatusCode EQ 200>       
                <cfif structKeyExists(fileContent, "PROCAVS")>  
                    <cfset dataout.AVSCODE = fileContent.PROCAVS>  
                </cfif>
				<cfif fileContent.RESULT EQ 0>
					<cfset dataout.RXRESULTCODE = 1 /> 
					<cfset dataout.RESULT = 'SUCCESS'>                    
					<cfset dataout.MESSAGE = "Payment successfully">                
					<cfset dataout.TRANSACTIONID = fileContent.PNREF/> 

					<!--- LOG TO PAMENT SUCCESS --->
					<cfset dataout.REPORT.ORDERID = arguments.inpOrderId>
					<cfset dataout.REPORT.TRANSACTIONID = dataout.TRANSACTIONID >
					<cfset dataout.REPORT.AMT = 0>
					<cfset dataout.REPORT.RESPONSE = statusMessage> 

					<cfset dataout.REPORT.INPNUMBER =  right(arguments.inpcardNumber, 4) >
					<cfset dataout.REPORT.INPCVV2 = arguments.inpcardCVV >
					<cfset dataout.REPORT.INPFIRSTNAME = arguments.inpbillingFirstName >
					<cfset dataout.REPORT.INPLASTNAME =  arguments.inpbillingLastName>
					<cfset dataout.REPORT.INPLINE1 = arguments.inpbillingAddressLine1 >
					<cfset dataout.REPORT.INPCITY = arguments.inpbillingCity>
					<cfset dataout.REPORT.INPSTATE = arguments.inpbillingState>
					<cfset dataout.REPORT.INPPOSTALCODE = arguments.inpbillingZipCode >
					<cfset dataout.REPORT.PHONE = ''> 
					<cfset dataout.REPORT.EMAIL = arguments.inpbillingEmail>
					<cfset dataout.REPORT.TRANSACTIONTYPE = 'sale'> 
					<cfset dataout.REPORT.METHOD = 'CC'>  
				<cfelse>
					<cfset dataout.RESULT = "Fail">
					<cfset dataout.RXRESULTCODE = -2>					
					<cfset dataout.MESSAGE = HTMLEditFormat(statusMessage)>   					
				</cfif>                 
                                          		
            <cfelse>                                   
            	<cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <!--- <cfset dataout.MESSAGE = "Error while create payment with status msg."&HTMLEditFormat(statusMessage)&"" > --->
                <cfset dataout.MESSAGE = HTMLEditFormat(statusMessage)>
            </cfif>

            <!--- Log payment --->
            <cftry>
                <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
                    <cfinvokeargument name="moduleName" value="#arguments.inpModuleName#">
                    <cfinvokeargument name="status_code" value="#httpStatusCode#">
                    <cfinvokeargument name="status_text" value="#statusMessage#">
                    <cfinvokeargument name="errordetail" value="#statusMessage#">
                    <cfinvokeargument name="filecontent" value="#serializeJSON(fileContent)#">
                    <cfinvokeargument name="paymentdata" value="#BODYDATATEMP#">
                    <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    <cfinvokeargument name="paymentmethod" value="2">
                </cfinvoke>
                <cfcatch type="any">
					
                </cfcatch>
            </cftry>

        	<cfcatch type="any">
        		<cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <cfset dataout.MESSAGE = cfcatch.MESSAGE> 				
        	</cfcatch>

        </cftry>
		<cfreturn dataout />
    </cffunction>
	<cffunction name="storeCreditCardInVault" access="public" output="true" hint="">
        <cfinvokeargument name="inpCardObject" value="#arguments.inpCardObject#"/>
        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>

        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">
        <cfset dataout.DATA = {}>
        <cfset dataout.USERCARDID = ''>
        <cfset dataout.CCTYPE = 'MC'>
        <cfset dataout.AVSCODE = ''>
        <cfset var paymentData = {}/>
        <cfset var RxAuthorization = {}/>
        <Cfset var RxUpload={}>

        <cftry>
            
            <cfif isJson(inpCardObject)>
                <cfset paymentData = deserializeJson(inpCardObject)>
                <cfif structKeyExists(paymentData, "expirationDate")>
                    <cfset paymentData.expirationDate = LEFT(paymentData.expirationDate,2)&""&RIGHT(paymentData.expirationDate,2)>
                <cfelse>
                    <cfset paymentData.expirationDate = paymentData.inpExpireMonth&""&RIGHT(paymentData.inpExpreYear,2)>    
                </cfif>
                <cfinvoke component="session.sire.models.cfc.vault-paypal" method="Authorization" returnvariable="RxAuthorization">
                    <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    <cfinvokeargument name="inpcardNumber" value="#paymentData.inpNumber#">     
                    <cfinvokeargument name="inpcurrencyCode" value="USD">
                    <cfinvokeargument name="inpcardExpiration" value="#paymentData.expirationDate#">
                    <cfinvokeargument name="inpcardCVV" value="#paymentData.inpCvv2#">
                    <cfinvokeargument name="inpbillingFirstName" value="#paymentData.inpFirstName#">
                    <cfinvokeargument name="inpbillingLastName" value="#paymentData.inpLastName#">
                    <cfinvokeargument name="inpbillingAddressLine1" value="#paymentData.inpLine1#">
                    <cfinvokeargument name="inpbillingCity" value="#paymentData.inpCity#">
                    <cfinvokeargument name="inpbillingState" value="#paymentData.inpState#">
                    <cfinvokeargument name="inpbillingZipCode" value="#paymentData.inpPostalCode#">
                    <cfinvokeargument name="inpbillingCountry" value="#paymentData.inpCountryCode#">
                    <cfinvokeargument name="inpbillingEmail" value="#paymentData.inpEmail#">
                </cfinvoke>

                <cfinvoke component="session.sire.models.cfc.vault-paypal" method="Upload" returnvariable="RxUpload">
                    <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    <cfinvokeargument name="inpcardNumber" value="#paymentData.inpNumber#">     
                    <cfinvokeargument name="inpcurrencyCode" value="840">
                    <cfinvokeargument name="inpcardExpiration" value="#paymentData.expirationDate#">
                    <cfinvokeargument name="inpcardCVV" value="#paymentData.inpCvv2#">
                    <cfinvokeargument name="inpbillingFirstName" value="#paymentData.inpFirstName#">
                    <cfinvokeargument name="inpbillingLastName" value="#paymentData.inpLastName#">
                    <cfinvokeargument name="inpbillingAddressLine1" value="#paymentData.inpLine1#">
                    <cfinvokeargument name="inpbillingCity" value="#paymentData.inpCity#">
                    <cfinvokeargument name="inpbillingState" value="#paymentData.inpState#">
                    <cfinvokeargument name="inpbillingZipCode" value="#paymentData.inpPostalCode#">
                    <cfinvokeargument name="inpbillingCountry" value="#paymentData.inpCountryCode#">
                    <cfinvokeargument name="inpbillingEmail" value="#paymentData.inpEmail#">
                    <!--- <cfinvokeargument name="inpbillingPhone" value="#paymentData.phone#"> --->
                </cfinvoke>
                
                <cfif RxUpload.RXRESULTCODE EQ 1>
                    <cfset dataout.RXRESULTCODE = 1>
                    <cfset dataout.RESULT = "Success">
                    <cfif structKeyExists(RxUpload, "REPORT")>
                        <cfset dataout.REPORT = RxUpload.REPORT>
                    </cfif>

                    <cfset dataout.USERCARDID = RxUpload.TOKEN>
                    <cfset dataout.CCTYPE = RxUpload.CCTYPE> 
                    <cfset dataout.AVSCODE = RxUpload.AVSCODE>                                           

                    <!--- VOID TRANSACTION --->

                <cfelse>
                    <cfset dataout.RXRESULTCODE = RxUpload.RXRESULTCODE>
                    <cfset dataout.MESSAGE = RxUpload.MESSAGE>
                    <cfset dataout.AVSCODE = RxUpload.AVSCODE>                                           
                </cfif>
                
                <cfif RxAuthorization.RXRESULTCODE EQ 1>                    
                    <cfset dataout.AVSCODE = RxAuthorization.AVSCODE>                                           
                <cfelse>
                    <cfset dataout.RXRESULTCODE = RxAuthorization.RXRESULTCODE>
                    <cfset dataout.MESSAGE = RxAuthorization.MESSAGE>
                    <cfset dataout.AVSCODE = RxAuthorization.AVSCODE>                                           
                </cfif>

            <cfelse>
                <cfset dataout.RXRESULTCODE = -2>
                <cfset dataout.MESSAGE = 'Invalid inpPaymentdata'>
            </cfif>

        <cfcatch type="any">
            <cfset dataout.RXRESULTCODE = -1 /> 
            <cfset dataout.RESULT = 'FAIL'>                    
            <cfset dataout.MESSAGE = cfcatch.MESSAGE>  
             <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>             
        </cfcatch>

        </cftry>
        <cfreturn dataout/>
    </cffunction>  
	<cffunction name="SaleWithSuspiciousVault" access="remote" output="true" hint="">
        <cfargument name="inpUserId" TYPE="string" required="true">
        <cfargument name="inpVaultId" TYPE="string" required="true">
        <cfargument name="inpPaymentReviewId" TYPE="string" required="true">
		<cfargument name="inpOrderId" TYPE="string" required="false" default="#Left(CreateUUID(),25)#">
        <cfargument name="inpOrderAmount" TYPE="string" required="true" default="0.00">    
		
    	<cfargument name="inpIntent" required="false" type="string" default="sale">

        <cfargument name="inpModuleName" TYPE="string" required="false" default="Buy Credits and Keywords">        
            

        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "Invalid input data">
        <cfset dataout.TRANSACTIONID = "">

        <cfset dataout.REPORT = {}/>
        <cfset dataout.REPORT.ORDERID = ''>
        <cfset dataout.REPORT.TRANSACTIONID = '' >
        <cfset dataout.REPORT.AMT = ''>
        <cfset dataout.REPORT.RESPONSE = ''> 

        <cfset dataout.REPORT.INPNUMBER = ''>
        <cfset dataout.REPORT.INPCVV2 = '' >
        <cfset dataout.REPORT.INPFIRSTNAME = '' >
        <cfset dataout.REPORT.INPLASTNAME =  ''>
        <cfset dataout.REPORT.INPLINE1 = '' >
        <cfset dataout.REPORT.INPCITY = ''>
        <cfset dataout.REPORT.INPSTATE = ''>
        <cfset dataout.REPORT.INPPOSTALCODE = '' >
        <cfset dataout.REPORT.PHONE = ''> 
        <cfset dataout.REPORT.TRANSACTIONTYPE = ''> 
        <cfset dataout.REPORT.METHOD = ''>
        <cfset dataout.FILECONTENT = ''>

        <cfset var BODYDATA = "">
		<cfset var returndata = ""/>
        <cfset var sale = ""/>
        <cfset var myXMLDocument=''>
        <cfset var selectedElements=''>        
        <cfset var selectTransactionId = 0>

        <cfset var selectStatusMessage=''>    
        <cfset var statusMessage=''>    
        <cfset var selectProcessorResponse=''>    
        <cfset var processorResponse=''>    
        <cfset var httpStatusCode = ''>
        <cfset var fileContent = ''>
        <cfset var content = ''>
        <cfset var CheckExistsPaymentMethod = ''>
		<cfset var inpToken=''>
        <cfset var vaultID=''>
        
        <cftry>
            <!--- Get token Mojo to payment --->
            <cfquery name="CheckExistsPaymentMethod" datasource="#Session.DBSourceEBM#">
				SELECT
					CardData_txt                    
				FROM
					simplebilling.list_payment_review
				WHERE
					UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"> 		
				AND 
					paymentGateway_ti = 2	
				AND 
					Id_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPaymentReviewId#"> 		
			</cfquery>

			<cfif CheckExistsPaymentMethod.RecordCount GT 0>   

				
				<cfset vaultID= arguments.inpVaultId>

				<cfset content= DeserializeJSON(CheckExistsPaymentMethod.CardData_txt)/>
                
                <cfif !structKeyExists(content, "INPEMAIL")>
                    <cfset content.INPEMAIL="">
                </cfif>
                <cfif Len(content.INPSTATE) GT 2>
                    <cfthrow  type = "any" message = "Billing State len is 2 characters only" detail = "Billing State len is 2 characters only" > 
                </cfif>

                <cfset var cfg = { path = "paypal.payflow.payflowGateway", Partner = "#pf_Partner#", MerchantAccount ='#pf_MerchantAccount#',Username = '#pf_Username#',Password = '#pf_Password#',CheckAVS = pf_CheckAVS, CheckCVV = pf_CheckCVV,  } />
				<cfset var svc = createObject("component", "cfpayment.api.core").init(cfg) />
				<cfset var gw = svc.getGateway() />
								

				<cfset var money = svc.createMoney(arguments.inpOrderAmount*100) />
				<cfset var sale = gw.SaleWithUpload(money = money, token = "#vaultID#") />								


				<cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="fileContent">
					<cfinvokeargument name="QueryString" value="#sale.getResult()#"/>	
				</cfinvoke>		
				

				

				<cfset var BODYDATATEMP={
							
					order: arguments.inporderId,									
					external_customer_id:arguments.inpUserId,				
					transaction_ref_id:vaultID,
					money: arguments.inporderAmount
				}>      
				<cfset dataout.filecontent = fileContent>   	
				<cfset statusMessage = fileContent.RESPMSG />            
				<cfset httpStatusCode = sale.GETSTATUSCODE()/>            			 
				<cfif httpStatusCode EQ 200>       
					<cfif fileContent.RESULT EQ 0>
						<cfset dataout.RXRESULTCODE = 1 /> 
						<cfset dataout.RESULT = 'SUCCESS'>                    
						<cfset dataout.MESSAGE = "Payment successfully">                
						<cfset dataout.TRANSACTIONID = fileContent.PNREF/> 

						<!--- LOG TO PAMENT SUCCESS --->
						<cfset dataout.REPORT.ORDERID = arguments.inpOrderId>
						<cfset dataout.REPORT.TRANSACTIONID = dataout.TRANSACTIONID >
						<cfset dataout.REPORT.AMT = arguments.inpOrderAmount>
						<cfset dataout.REPORT.RESPONSE = statusMessage> 

						<cfset dataout.REPORT.INPNUMBER = content.INPNUMBER>
						<cfset dataout.REPORT.INPCVV2 = content.INPCVV2 >
						<cfset dataout.REPORT.INPFIRSTNAME = content.INPFIRSTNAME >
						<cfset dataout.REPORT.INPLASTNAME =  content.INPLASTNAME>
						<cfset dataout.REPORT.INPLINE1 = content.INPLINE1 >
						<cfset dataout.REPORT.INPCITY = content.INPCITY>
						<cfset dataout.REPORT.INPSTATE = content.INPSTATE>
						<cfset dataout.REPORT.INPPOSTALCODE = content.INPPOSTALCODE >
						<cfset dataout.REPORT.PHONE = ''>                             
						<cfset dataout.REPORT.EMAIL = content.INPEMAIL>                            
						<cfset dataout.REPORT.TRANSACTIONTYPE = 'sale'> 
						<cfset dataout.REPORT.METHOD = 'CC'>
					<cfelse>
						<cfset dataout.RESULT = "Fail">
						<cfset dataout.RXRESULTCODE = -2>					
						<cfset dataout.MESSAGE = HTMLEditFormat(statusMessage)>   					
					</cfif>                 
													
				<cfelse>                                   
					<cfset dataout.RXRESULTCODE = -1 /> 
					<cfset dataout.RESULT = 'FAIL'>                    
					<!--- <cfset dataout.MESSAGE = "Error while create payment with status msg."&HTMLEditFormat(statusMessage)&"" > --->
					<cfset dataout.MESSAGE = HTMLEditFormat(statusMessage)>
				</cfif>

                <!--- Log payment --->
                <cftry>
                    <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
                        <cfinvokeargument name="moduleName" value="#arguments.inpModuleName#">
                        <cfinvokeargument name="status_code" value="#httpStatusCode#">
                        <cfinvokeargument name="status_text" value="#statusMessage#">
                        <cfinvokeargument name="errordetail" value="#statusMessage#">
                        <cfinvokeargument name="filecontent" value="#serializeJSON(fileContent)#">
                        <cfinvokeargument name="paymentdata" value="#BODYDATATEMP#">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                        <cfinvokeargument name="paymentmethod" value="2">
                        <cfinvokeargument name="inpPaymentByUserId" value="#arguments.inpUserId#">                        
                    </cfinvoke>
                    <cfcatch type="any">
                        
                    </cfcatch>
                </cftry>
				
            <cfelse>
                <cfset dataout.RESULT = "FAIL">
                <cfset dataout.RXRESULTCODE = 0>
                <cfset dataout.MESSAGE = "Payment fail!. CC not exist.">
                <cfreturn dataout>
            </cfif>

        	<cfcatch type="any">
        		<cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <cfset dataout.MESSAGE = cfcatch.MESSAGE> 				                
        	</cfcatch>

        </cftry>
		<cfreturn dataout />
    </cffunction>
	<cffunction name="Sale" access="remote" output="false" hint="">
        <cfargument name="inpUserId" TYPE="string" required="false" default="#Session.UserId#">
		<cfargument name="inporderId" TYPE="string" required="false" default="#Left(CreateUUID(),25)#">
        <cfargument name="inpcardNumber" TYPE="string" required="true" default="">		

        <cfargument name="inporderAmount" TYPE="string" required="true" default="0.00">
        <cfargument name="inpcurrencyCode" TYPE="string" required="false" default="USD">
        <cfargument name="inpcardExpiration" TYPE="string" required="true" default="">
        <cfargument name="inpcardCVV" TYPE="string" required="true" default="">

        <cfargument name="inpbillingFirstName" TYPE="string" required="true" default="">
        <cfargument name="inpbillingLastName" TYPE="string" required="true" default="">
        <cfargument name="inpbillingAddressLine1" TYPE="string" required="true" default="">
        <cfargument name="inpbillingCity" TYPE="string" required="true" default=" ">
        <cfargument name="inpbillingState" TYPE="string" required="true" default=" ">
        <cfargument name="inpbillingZipCode" TYPE="string" required="true" default=" ">
        <cfargument name="inpbillingCountry" TYPE="string" required="true" default=" ">
        <cfargument name="inpbillingEmail" TYPE="string" required="false" default=" ">
        <cfargument name="inpbillingPhone" TYPE="string" required="false" default=" ">

        <cfargument name="inpModuleName" TYPE="string" required="false" default="Buy Credits and Keywords">
                

        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">
		<cfset dataout.STATUSCODE = ""/>
        <cfset dataout.PROCESSORREPONSECODE = ""/>

        <cfset dataout.TRANSACTIONID = "">

        <cfset dataout.REPORT = {}/>
        <cfset dataout.REPORT.ORDERID = ''>
        <cfset dataout.REPORT.TRANSACTIONID = '' >
        <cfset dataout.REPORT.AMT = ''>
        <cfset dataout.REPORT.RESPONSE = ''> 

        <cfset dataout.REPORT.INPNUMBER = ''>
        <cfset dataout.REPORT.INPCVV2 = '' >
        <cfset dataout.REPORT.INPFIRSTNAME = '' >
        <cfset dataout.REPORT.INPLASTNAME =  ''>
        <cfset dataout.REPORT.INPLINE1 = '' >
        <cfset dataout.REPORT.INPCITY = ''>
        <cfset dataout.REPORT.INPSTATE = ''>
        <cfset dataout.REPORT.INPPOSTALCODE = '' >
        <cfset dataout.REPORT.PHONE = ''> 
        <cfset dataout.REPORT.EMAIL = ''>
        <cfset dataout.REPORT.TRANSACTIONTYPE = ''> 
        <cfset dataout.REPORT.METHOD = ''>

        <cfset var BODYDATA = "">
        <cfset var BODYDATATEMP = ""/>
		<cfset var returndata = ""/>
        <cfset var sale = ""/>
        <cfset var myXMLDocument=''>
        <cfset var selectedElements=''>        
        <Cfset var processorResponse = ''/>
        <Cfset var statusMessage = ''/>
        <Cfset var fileContent = ''/>
        <Cfset var httpStatusCode = ''/>
        <Cfset var dateFromCcInput = ''/>
        <cfset var selectStatusMessage = ''/>
        <cfset var selectProcessorResponse = ''/>
        <cfset var selectTransactionId = ''/>

        <cftry>            
            <cfif IsNumeric(arguments.inpbillingState) OR IsNumeric(arguments.inpbillingAddressLine1) OR IsNumeric(arguments.inpbillingCity)>
                <cfthrow  type = "any" message = "Billing Address, City, State should be string" detail = "Billing Address, City, State should be string" > 
            </cfif>
            
            <cfif Len(arguments.inpbillingState) GT 2>
                <cfthrow  type = "any" message = "Billing State should be 2 characters only" detail = "Billing State should be 2 characters only" > 
            </cfif>
            <cfif Len(arguments.inpbillingAddressLine1) GT 35>
                <cfthrow  type = "any" message = "Billing Address should be 35 characters only" detail = "Billing Address should be 35 characters only" > 
            </cfif>
            <cfif Len(arguments.inpbillingCity) GT 20>
                <cfthrow  type = "any" message = "Billing City should be 20 characters only" detail = "Billing City should be 20 characters only" > 
            </cfif>
            <!--- check for same month year compage with last day of month, if not compare first day is enough---->            
           <cfif Int(Month(NOW())) EQ Int(left(arguments.inpcardExpiration,2)) AND Int(right(Year(NOW()),2)) EQ Int(right(arguments.inpcardExpiration,2))>
                <cfset dateFromCcInput= createDate("20" & right(arguments.inpcardExpiration,2), left(arguments.inpcardExpiration,2), daysInMonth(NOW()))>
            <cfelse>            
                <cfset dateFromCcInput= createDate("20" & right(arguments.inpcardExpiration,2), left(arguments.inpcardExpiration,2), 28)>
            </cfif>
            <cfif DateCompare(dateFromCcInput, NOW()) LT 1>
                <cfthrow  type = "any" message = "Your Credit Card has been expired! Please check the Expiration Date or add another Credit Card." detail = "Your Credit Card has been expired! Please check the Expiration Date or add another Credit Card." > 
            </cfif>
			
			<cfset var cfg = { path = "paypal.payflow.payflowGateway", Partner = "#pf_Partner#", MerchantAccount ='#pf_MerchantAccount#',Username = '#pf_Username#',Password = '#pf_Password#',CheckAVS = pf_CheckAVS, CheckCVV = pf_CheckCVV,  } />
			<cfset var svc = createObject("component", "cfpayment.api.core").init(cfg) />
			<cfset var gw = svc.getGateway() />

			<!--- <cfset account = svc.createCreditCard().setAccount(4111111111111111).setMonth(10).setYear(year(now())+1).setFirstName("John").setLastName("Doe").setAddress("608 Mina").setPostalCode("9200") /> --->
			<cfset var account = svc.createCreditCard().setAccount(arguments.inpcardNumber).setMonth(Int(left(arguments.inpcardExpiration,2))).setYear(Int(right(arguments.inpcardExpiration,4))).setFirstName(arguments.inpbillingFirstName).setLastName(arguments.inpbillingLastName).setAddress(arguments.inpbillingAddressLine1).setPostalCode(arguments.inpbillingZipCode).setCity(arguments.inpbillingCity).setRegion(arguments.inpbillingState).setCountry(arguments.inpbillingCountry) />

			<cfset var money = svc.createMoney(arguments.inporderAmount*100) />
			<cfset var sale = gw.purchase(money = money, account = account) />



			<cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="fileContent">
				<cfinvokeargument name="QueryString" value="#sale.getResult()#"/>	
			</cfinvoke>		
			

            

            <cfset BODYDATATEMP={
				number: Right(arguments.inpcardNumber,4),				
				order: arguments.inporderId,				
				expire_month:Int(left(arguments.inpcardExpiration,2)),
				expire_year: Int(right(arguments.inpcardExpiration,4)),
				cvv2: arguments.inpcardCVV,
				first_name: arguments.inpbillingFirstName,
				last_name: arguments.inpbillingLastName,
				billing_address:{
					line1: arguments.inpbillingAddressLine1,
					city: arguments.inpbillingCity,
					country_code: arguments.inpbillingCountry,
					postal_code: arguments.inpbillingZipCode,
					state:arguments.inpbillingState					
				},
				external_customer_id:arguments.inpUserId,				
				money: arguments.inporderAmount
			}>      
			
            <cfset statusMessage = fileContent.RESPMSG />            
            <cfset httpStatusCode = sale.GETSTATUSCODE()/>            			 
            <cfif httpStatusCode EQ 200>       
				<cfif fileContent.RESULT EQ 0>
					<cfset dataout.RXRESULTCODE = 1 /> 
					<cfset dataout.RESULT = 'SUCCESS'>                    
					<cfset dataout.MESSAGE = "Payment successfully">                
					<cfset dataout.TRANSACTIONID = fileContent.PNREF/> 

					<!--- LOG TO PAMENT SUCCESS --->
					<cfset dataout.REPORT.ORDERID = arguments.inpOrderId>
					<cfset dataout.REPORT.TRANSACTIONID = dataout.TRANSACTIONID >
					<cfset dataout.REPORT.AMT = arguments.inpOrderAmount>
					<cfset dataout.REPORT.RESPONSE = statusMessage> 

					<cfset dataout.REPORT.INPNUMBER =  right(arguments.inpcardNumber, 4) >
					<cfset dataout.REPORT.INPCVV2 = arguments.inpcardCVV >
					<cfset dataout.REPORT.INPFIRSTNAME = arguments.inpbillingFirstName >
					<cfset dataout.REPORT.INPLASTNAME =  arguments.inpbillingLastName>
					<cfset dataout.REPORT.INPLINE1 = arguments.inpbillingAddressLine1 >
					<cfset dataout.REPORT.INPCITY = arguments.inpbillingCity>
					<cfset dataout.REPORT.INPSTATE = arguments.inpbillingState>
					<cfset dataout.REPORT.INPPOSTALCODE = arguments.inpbillingZipCode >
					<cfset dataout.REPORT.PHONE = ''> 
					<cfset dataout.REPORT.EMAIL = arguments.inpbillingEmail>
					<cfset dataout.REPORT.TRANSACTIONTYPE = 'sale'> 
					<cfset dataout.REPORT.METHOD = 'CC'>  
				<cfelse>
					<cfset dataout.RESULT = "Fail">
					<cfset dataout.RXRESULTCODE = -2>					
					<cfset dataout.MESSAGE = HTMLEditFormat(statusMessage)>   					
				</cfif>                 
                                          		
            <cfelse>                                   
            	<cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <!--- <cfset dataout.MESSAGE = "Error while create payment with status msg."&HTMLEditFormat(statusMessage)&"" > --->
                <cfset dataout.MESSAGE = HTMLEditFormat(statusMessage)>
            </cfif>

            <!--- Log payment --->
            <cftry>
                <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
                    <cfinvokeargument name="moduleName" value="#arguments.inpModuleName#">
                    <cfinvokeargument name="status_code" value="#httpStatusCode#">
                    <cfinvokeargument name="status_text" value="#statusMessage#">
                    <cfinvokeargument name="errordetail" value="#statusMessage#">
                    <cfinvokeargument name="filecontent" value="#serializeJSON(fileContent)#">
                    <cfinvokeargument name="paymentdata" value="#BODYDATATEMP#">
                    <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    <cfinvokeargument name="paymentmethod" value="2">
                </cfinvoke>
                <cfcatch type="any">
					
                </cfcatch>
            </cftry>

        	<cfcatch type="any">
        		<cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <cfset dataout.MESSAGE = cfcatch.MESSAGE> 				
        	</cfcatch>

        </cftry>
		<cfreturn dataout />
    </cffunction>
	<cffunction name="SaleWithTokenzine" access="remote" output="true" hint="">
        <cfargument name="inpUserId" TYPE="string" required="false" default="#Session.UserId#">
		<cfargument name="inpOrderId" TYPE="string" required="false" default="#Left(CreateUUID(),25)#">
        <cfargument name="inpOrderAmount" TYPE="string" required="true" default="0.00">
        <cfargument name="inpCurrencyCode" TYPE="string" required="false" default="840">

        <cfargument name="inpModuleName" TYPE="string" required="false" default="Buy Credits and Keywords">        

        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "Invalid input data">
        <cfset dataout.TRANSACTIONID = "">

        <cfset dataout.REPORT = {}/>
        <cfset dataout.REPORT.ORDERID = ''>
        <cfset dataout.REPORT.TRANSACTIONID = '' >
        <cfset dataout.REPORT.AMT = ''>
        <cfset dataout.REPORT.RESPONSE = ''> 

        <cfset dataout.REPORT.INPNUMBER = ''>
        <cfset dataout.REPORT.INPCVV2 = '' >
        <cfset dataout.REPORT.INPFIRSTNAME = '' >
        <cfset dataout.REPORT.INPLASTNAME =  ''>
        <cfset dataout.REPORT.INPLINE1 = '' >
        <cfset dataout.REPORT.INPCITY = ''>
        <cfset dataout.REPORT.INPSTATE = ''>
        <cfset dataout.REPORT.INPPOSTALCODE = '' >
        <cfset dataout.REPORT.PHONE = ''> 
        <cfset dataout.REPORT.TRANSACTIONTYPE = ''> 
        <cfset dataout.REPORT.METHOD = ''>
        <cfset dataout.FILECONTENT = ''>

        <cfset var BODYDATA = "">
		<cfset var returndata = ""/>
        <cfset var sale = ""/>
        <cfset var myXMLDocument=''>
        <cfset var selectedElements=''>        
        <cfset var selectTransactionId = 0>

        <cfset var selectStatusMessage=''>    
        <cfset var statusMessage=''>    
        <cfset var selectProcessorResponse=''>    
        <cfset var processorResponse=''>    
        <cfset var httpStatusCode = ''>
        <cfset var fileContent = ''>
        <cfset var content = ''>
        <cfset var CheckExistsPaymentMethod = ''>
		<cfset var vaultID=''>
        
        <cftry>
            <!--- Get token Mojo to payment --->
            <cfquery name="CheckExistsPaymentMethod" datasource="#Session.DBSourceEBM#">
				SELECT
					extraNote,
                    PaymentMethodID_vch
				FROM
					simplebilling.authorize_user
				WHERE
					UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"> 		
				AND 
					paymentGateway_ti = 2
				AND 
					status_ti = 1	
			</cfquery>

			<cfif CheckExistsPaymentMethod.RecordCount GT 0>   
				<cfset vaultID= CheckExistsPaymentMethod.PaymentMethodID_vch>

				<cfset content= DeserializeJSON(CheckExistsPaymentMethod.extraNote)/>
                
                <cfif !structKeyExists(content, "INPEMAIL")>
                    <cfset content.INPEMAIL="">
                </cfif>
                <cfif Len(content.INPSTATE) GT 2>
                    <cfthrow  type = "any" message = "Billing State len is 2 characters only" detail = "Billing State len is 2 characters only" > 
                </cfif>

                <cfset var cfg = { path = "paypal.payflow.payflowGateway", Partner = "#pf_Partner#", MerchantAccount ='#pf_MerchantAccount#',Username = '#pf_Username#',Password = '#pf_Password#',CheckAVS = pf_CheckAVS, CheckCVV = pf_CheckCVV,  } />
				<cfset var svc = createObject("component", "cfpayment.api.core").init(cfg) />
				<cfset var gw = svc.getGateway() />
								

				<cfset var money = svc.createMoney(arguments.inporderAmount*100) />
				<cfset var sale = gw.SaleWithUpload(money = money, token = "#vaultID#") />
				



				<cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="fileContent">
					<cfinvokeargument name="QueryString" value="#sale.getResult()#"/>	
				</cfinvoke>		
				

				

				<cfset var BODYDATATEMP={
							
					order: arguments.inporderId,									
					external_customer_id:arguments.inpUserId,				
					transaction_ref_id:vaultID,
					money: arguments.inporderAmount
				}>      
				<cfset dataout.FILECONTENT = SerializeJSON(fileContent) >
				<cfset statusMessage = fileContent.RESPMSG />            
				<cfset httpStatusCode = sale.GETSTATUSCODE()/>            			 
				<cfif httpStatusCode EQ 200>       
					<cfif fileContent.RESULT EQ 0>
						<cfset dataout.RXRESULTCODE = 1 /> 
						<cfset dataout.RESULT = 'SUCCESS'>                    
						<cfset dataout.MESSAGE = "Payment successfully">                
						<cfset dataout.TRANSACTIONID = fileContent.PNREF/> 

						<!--- LOG TO PAMENT SUCCESS --->
						<cfset dataout.REPORT.ORDERID = arguments.inpOrderId>
						<cfset dataout.REPORT.TRANSACTIONID = dataout.TRANSACTIONID >
						<cfset dataout.REPORT.AMT = arguments.inpOrderAmount>
						<cfset dataout.REPORT.RESPONSE = statusMessage> 

						<cfset dataout.REPORT.INPNUMBER = content.INPNUMBER>
						<cfset dataout.REPORT.INPCVV2 = content.INPCVV2 >
						<cfset dataout.REPORT.INPFIRSTNAME = content.INPFIRSTNAME >
						<cfset dataout.REPORT.INPLASTNAME =  content.INPLASTNAME>
						<cfset dataout.REPORT.INPLINE1 = content.INPLINE1 >
						<cfset dataout.REPORT.INPCITY = content.INPCITY>
						<cfset dataout.REPORT.INPSTATE = content.INPSTATE>
						<cfset dataout.REPORT.INPPOSTALCODE = content.INPPOSTALCODE >
						<cfset dataout.REPORT.PHONE = ''>                             
						<cfset dataout.REPORT.EMAIL = content.INPEMAIL>                            
						<cfset dataout.REPORT.TRANSACTIONTYPE = 'sale'> 
						<cfset dataout.REPORT.METHOD = 'CC'>
					<cfelse>
						<cfset dataout.RESULT = "Fail">
						<cfset dataout.RXRESULTCODE = -2>					
						<cfset dataout.MESSAGE = HTMLEditFormat(statusMessage)>   					
					</cfif>                 
													
				<cfelse>                                   
					<cfset dataout.RXRESULTCODE = -1 /> 
					<cfset dataout.RESULT = 'FAIL'>                    
					<!--- <cfset dataout.MESSAGE = "Error while create payment with status msg."&HTMLEditFormat(statusMessage)&"" > --->
					<cfset dataout.MESSAGE = HTMLEditFormat(statusMessage)>
				</cfif>

                <!--- Log payment --->
                <cftry>
                    <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
                        <cfinvokeargument name="moduleName" value="#arguments.inpModuleName#">
                        <cfinvokeargument name="status_code" value="#httpStatusCode#">
                        <cfinvokeargument name="status_text" value="#statusMessage#">
                        <cfinvokeargument name="errordetail" value="#statusMessage#">
                        <cfinvokeargument name="filecontent" value="#serializeJSON(fileContent)#">
                        <cfinvokeargument name="paymentdata" value="#BODYDATATEMP#">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                        <cfinvokeargument name="paymentmethod" value="2">
                        <cfinvokeargument name="inpPaymentByUserId" value="#arguments.inpUserId#">                        
                    </cfinvoke>
                    <cfcatch type="any">

                    </cfcatch>
                </cftry>

            <cfelse>
                <cfset dataout.RESULT = "FAIL">
                <cfset dataout.RXRESULTCODE = 0>
                <cfset dataout.MESSAGE = "Payment fail!. CC not exist or inactive.">
                <cfreturn dataout>
            </cfif>

        	<cfcatch type="any">
        		<cfset dataout.RXRESULTCODE = -1 /> 
                <cfset dataout.RESULT = 'FAIL'>                    
                <cfset dataout.MESSAGE = cfcatch.MESSAGE> 				

        	</cfcatch>

        </cftry>
		<cfreturn dataout />
    </cffunction>
</cfcomponent>