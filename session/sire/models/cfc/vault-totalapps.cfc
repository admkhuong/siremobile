<cfcomponent>

	<cfinclude template="/public/paths.cfm" >
    <cfinclude template="/session/cfc/csc/constants.cfm">
    <cfinclude template="/session/sire/configs/credits.cfm">
    <!---Customer Vault--->
         <!--- Add a credit card ---> 
        <cffunction name="CustomerVaultAdd" access="remote" output="true" hint="TotalApp: Add Customer Vault">   
            <cfargument name="inpCardObject" TYPE="string" required="true" default="">
            <cfargument name="inpUserId" TYPE="string" required="false" default="#session.UserId#">  
            <cfargument name="inpModuleName" TYPE="string" required="false" default="Customer Vault Add">

            <cfset var dataout = structNew()>
            <cfset dataout.RESULT = 'FAIL'>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = ''/>
            <cfset dataout.USERCARDID = ""/>
            <cfset dataout.CCTYPE = "MC"/>
            <cfset dataout.AVSCODE = "">

            <cfset var datatemp={}>
            <Cfset var httpStatusCode=''>
            <Cfset var statusMessage=''>

            <cfset var customervaultaddresult=''> 
            <cfset var cardinfo = DESERIALIZEJSON(inpCardObject)/>
            <cfset datatemp=DESERIALIZEJSON(inpCardObject)>
            <cfset datatemp.inpNumber=  right(datatemp.inpNumber, 4)>
            
            <cfif structKeyExists(cardinfo, "expirationDate")>
                <cfset cardinfo.expirationDate = LEFT(cardinfo.expirationDate,2)&""&RIGHT(cardinfo.expirationDate,2)>
            <cfelse>
                <cfset cardinfo.expirationDate = cardinfo.inpExpireMonth&""&RIGHT(cardinfo.inpExpreYear,2)>    
            </cfif>
            <cfset var statuscheckout = 0>
            <cfset var queryStruct ="">
            <cfset var rtcheckCcType="">
            <cfinvoke component="session.sire.models.cfc.payment.cc-tools" method="checkCcType" returnvariable="rtcheckCcType">
                <cfinvokeargument name="inpcardNumber" value="#cardinfo.inpNumber#">                                
            </cfinvoke> 
            <cfset dataout.CCTYPE = rtcheckCcType.CCTYPE/>           
            <cftry>
                <cfhttp url="#total_apps_apiurl#" method="post" result="customervaultaddresult">
                    <cfhttpparam type="FORMFIELD" name="username" value="#total_apps_username#">
                    <cfhttpparam type="FORMFIELD" name="password" value="#total_apps_password#">
                    <cfhttpparam type="FORMFIELD" name="customer_vault" value="add_customer">
                    <cfhttpparam type="FORMFIELD" name="customer_vault_id" value="">
                    <cfhttpparam type="FORMFIELD" name="ccnumber" value="#cardinfo.inpNumber#">
                    <cfhttpparam type="FORMFIELD" name="ccexp" value="#cardinfo.expirationDate#">
                    <cfhttpparam type="FORMFIELD" name="first_name" value="#cardinfo.inpFirstName#">
                    <cfhttpparam type="FORMFIELD" name="last_name" value="#cardinfo.inpLastName#">
                    <cfhttpparam type="FORMFIELD" name="address1" value="#cardinfo.inpLine1#">
                    <cfhttpparam type="FORMFIELD" name="city" value="#cardinfo.inpCity#">
                    <cfhttpparam type="FORMFIELD" name="state" value="#cardinfo.inpState#">
                    <cfhttpparam type="FORMFIELD" name="country" value="#cardinfo.inpCountryCode#">
                    
                    <cfhttpparam type="FORMFIELD" name="email" value="#cardinfo.inpEmail#">
                </cfhttp>   
                
                <cfif structKeyExists(customervaultaddresult, "status_code") AND customervaultaddresult.status_code EQ 200>
                    <Cfset httpStatusCode=customervaultaddresult.status_code>
                    
                    <cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="queryStruct">
                        <cfinvokeargument name="QueryString" value="#customervaultaddresult.filecontent#">
                    </cfinvoke>
                    <cfset statuscheckout = Find("response_code=100",customervaultaddresult.filecontent)>
        			<cfif statuscheckout GT 0> 
                        <cfset dataout.RXRESULTCODE = 1> 
                        <cfset dataout.RESULT = 'ADD VAULT SUCCESSFULLY.'>
                        <cfset dataout.MESSAGE = customervaultaddresult.filecontent>
                        <cfif structKeyExists(queryStruct, "customer_vault_id")>
                            <cfset dataout.USERCARDID = queryStruct['customer_vault_id']/>
                        </cfif>
                        <cfif structKeyExists(queryStruct, "avsresponse")>
                            <cfset dataout.AVSCODE = queryStruct['avsresponse']/>
                        </cfif>
                    <cfelse>
                        <cfset dataout.RXRESULTCODE = -1> 
                        <cfset dataout.RESULT = 'ADD VAULT FAIL.'>
                        <cfif structKeyExists(queryStruct, "responsetext")>
                            <cfset dataout.MESSAGE = queryStruct['responsetext']>
                        </cfif>
                    </cfif>
                    <cfif structKeyExists(queryStruct, "responsetext")>
                        <cfset statusMessage = queryStruct['responsetext']>
                    </cfif>                    
                <cfelse>
                    <cfset dataout.RXRESULTCODE = -1 /> 
                    <cfset dataout.RESULT = customervaultaddresult.errordetail>
                    <cfset dataout.MESSAGE = customervaultaddresult.statuscode>                        
                </cfif>
                <!--- Log payment --->
                
                <cftry>
                    <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
                        <cfinvokeargument name="moduleName" value="#arguments.inpModuleName#">
                        <cfinvokeargument name="status_code" value="#httpStatusCode#">
                        <cfinvokeargument name="status_text" value="#statusMessage#">
                        <cfinvokeargument name="errordetail" value="#statusMessage#">
                        <cfinvokeargument name="filecontent" value="#customervaultaddresult.filecontent#">
                        <cfinvokeargument name="paymentdata" value="#datatemp#">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                        <cfinvokeargument name="paymentmethod" value="#_TOTALAPPS_GATEWAY#">
                    </cfinvoke>
                    <cfcatch type="any">
                        
                    </cfcatch>
                </cftry>                           
                <cfcatch>                
                    <cfset dataout.RXRESULTCODE = -1 /> 
                    <cfset dataout.RESULT = 'FAIL'>                    
                    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">                                                     
                </cfcatch>
            </cftry>
            
            <cfreturn dataout />
        </cffunction> 

        <!--- update a credit card ---> 
        <cffunction name="CustomerVaultUpdate" access="remote" output="true" hint="TotalApp: Update Customer Vault">   
                <cfargument name="inpCardObject" TYPE="string" required="true" default="">
                <cfargument name="inpUserId" TYPE="string" required="false" default="#session.UserId#">  
                <cfargument name="inpCustomerVaultId" TYPE="string" required="false" default="">
        
                <cfset var dataout = structNew()>
                <cfset dataout.RESULT = 'FAIL'>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = ''/>

                <cfset var customervaultupdateresult=''> 
                <cfset var cardinfo = DESERIALIZEJSON(inpCardObject)/>
                <cfset var statuscheckout = 0>
                <cfset var queryStruct ="">
                
                <cftry>
                    <cfhttp url="#total_apps_apiurl#" method="post" result="customervaultupdateresult">
                        <cfhttpparam type="FORMFIELD" name="username" value="#total_apps_username#">
                        <cfhttpparam type="FORMFIELD" name="password" value="#total_apps_password#">
                        <cfhttpparam type="FORMFIELD" name="customer_vault" value="update_customer">
                        <cfhttpparam type="FORMFIELD" name="customer_vault_id" value="#arguments.inpCustomerVaultId#">
                        <cfhttpparam type="FORMFIELD" name="ccnumber" value="#cardinfo.inpNumber#">
                        <cfhttpparam type="FORMFIELD" name="ccexp" value="#cardinfo.inpExpireMonth##cardinfo.inpExpreYear#">
                        <cfhttpparam type="FORMFIELD" name="first_name" value="#cardinfo.inpFirstName#">
                        <cfhttpparam type="FORMFIELD" name="last_name" value="#cardinfo.inpLastName#">
                        <cfhttpparam type="FORMFIELD" name="address1" value="#cardinfo.inpLine1#">
                        <cfhttpparam type="FORMFIELD" name="city" value="#cardinfo.inpCity#">
                        <cfhttpparam type="FORMFIELD" name="state" value="#cardinfo.inpState#">
                        <cfhttpparam type="FORMFIELD" name="country" value="#cardinfo.inpCountryCode#">
                        <cfhttpparam type="FORMFIELD" name="phone" value="#cardinfo.inpPhone#">
                        <cfhttpparam type="FORMFIELD" name="email" value="#cardinfo.inpEmail#">
                    </cfhttp>              
                    <cfif structKeyExists(customervaultupdateresult, "status_code") AND customervaultupdateresult.status_code EQ 200>
                        <cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="queryStruct">
                            <cfinvokeargument name="QueryString" value="#customervaultupdateresult.filecontent#">
                        </cfinvoke> 
                        <cfset statuscheckout = Find("response_code=100",customervaultupdateresult.filecontent)>
                        <cfif statuscheckout GT 0>
                            <cfset dataout.RXRESULTCODE = 1> 
                            <cfset dataout.RESULT = "EDIT VAULT SUCCESSFULLY.">
                            <cfset dataout.MESSAGE = customervaultupdateresult.filecontent>
                        <cfelse>
                            <cfset dataout.RXRESULTCODE = -1> 
                            <cfset dataout.RESULT = 'EDIT VAULT FAIL.'>
                            <cfif structKeyExists(queryStruct, "responsetext")>
                                <cfset dataout.MESSAGE = queryStruct['responsetext']>
                            </cfif>
                        </cfif>                    
                    </cfif>
                                                
                    <cfcatch>                
                        <cfset dataout.RXRESULTCODE = -1 /> 
                        <cfset dataout.RESULT = 'FAIL'>                    
                        <cfset dataout.MESSAGE = "Error">                                                
                    </cfcatch>
                </cftry>
                
                <cfreturn dataout />
        </cffunction> 

        <!--- Customer Vault initiated Sale/Auth/Credit/Offline ---> 
        <cffunction name="SaleWithTokenzine" access="remote" output="false" hint="TotalApp: Sale With Tokenzine">
            <cfargument name="inpUserId" TYPE="string" required="false" default="#session.UserId#">
            <cfargument name="inpOrderAmount" TYPE="string" required="true" default="0.00">
            <cfargument name="inpCurrencyCode" TYPE="string" required="false" default="USD">

            <cfargument name="inpModuleName" TYPE="string" required="false" default="Buy Credits and Keywords"> 

             <cfset var dataout = structNew()>
            <cfset dataout.RESULT = "FAIL">
            <cfset dataout.RXRESULTCODE = -1>
            <cfset dataout.MESSAGE = "">
            <cfset dataout.STATUSCODE = ""/>
            <cfset dataout.PROCESSORREPONSECODE = ""/>
            <cfset dataout.FILECONTENT = ""/>
            <cfset dataout.TRANSACTIONID = "">

            <cfset dataout.REPORT = {}/>
            <cfset dataout.REPORT.ORDERID = ''>
            <cfset dataout.REPORT.TRANSACTIONID = '' >
            <cfset dataout.REPORT.AMT = ''>
            <cfset dataout.REPORT.RESPONSE = ''> 

            <cfset dataout.REPORT.INPNUMBER = ''>
            <cfset dataout.REPORT.INPCVV2 = '' >
            <cfset dataout.REPORT.INPFIRSTNAME = '' >
            <cfset dataout.REPORT.INPLASTNAME =  ''>
            <cfset dataout.REPORT.INPLINE1 = '' >
            <cfset dataout.REPORT.INPCITY = ''>
            <cfset dataout.REPORT.INPSTATE = ''>
            <cfset dataout.REPORT.INPPOSTALCODE = '' >
            <cfset dataout.REPORT.PHONE = ''> 
            <cfset dataout.REPORT.EMAIL = ''>
            <cfset dataout.REPORT.TRANSACTIONTYPE = ''> 
            <cfset dataout.REPORT.METHOD = ''>

            <cfset var transactionsaleresult = ''/>
            <cfset var CheckExistsPaymentMethod = ''/>
            <cfset var content = ''/>
            <cfset var fileContent = ''/>  
            <cfset var statusMessage = '' />
            <cfset var processorResponse = ''/>
            <cfset var httpStatusCode = ''/>
            <cfset var datapayment = ''/>
            <cfset var statuscheckout	= '' />
            <cfset var queryStruct	= '' />

            <cftry>

                 <!--- Get token Mojo to payment --->
                <cfquery name="CheckExistsPaymentMethod" datasource="#Session.DBSourceEBM#">
                    SELECT
                        extraNote,
                        PaymentMethodID_vch
                    FROM
                        simplebilling.authorize_user
                    WHERE
                        UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"> 		
                    AND 
                        paymentGateway_ti = 4	
                    AND 
                        status_ti = 1	
                </cfquery>

                <cfif CheckExistsPaymentMethod.RecordCount GT 0>

                    <cfset content= DeserializeJSON(CheckExistsPaymentMethod.extraNote)/>
                
                    <cfif !structKeyExists(content, "INPEMAIL")>
                        <cfset content.INPEMAIL="">
                    </cfif>
                    <cfif Len(content.INPSTATE) GT 2>
                        <cfthrow  type = "any" message = "Billing State len is 2 characters only" detail = "Billing State len is 2 characters only" > 
                    </cfif>

                    <cfhttp url="#total_apps_apiurl#" method="post" result="transactionsaleresult">
                        <cfhttpparam type="FORMFIELD" name="username" value="#total_apps_username#">
                        <cfhttpparam type="FORMFIELD" name="password" value="#total_apps_password#">
                        <cfhttpparam type="FORMFIELD" name="customer_vault_id" value="#CheckExistsPaymentMethod.PaymentMethodID_vch#">
                        <cfhttpparam type="FORMFIELD" name="amount" value="#arguments.inpOrderAmount#">
                        <cfhttpparam type="FORMFIELD" name="currency" value="#arguments.inpCurrencyCode#">
                        <!---<cfhttpparam type="FORMFIELD" name="dup_seconds" value="0">--->
                    </cfhttp>

                    <cfset fileContent = transactionsaleresult.fileContent/>   
                    <cfset dataout.FILECONTENT = fileContent>
                    <cfset statusMessage = transactionsaleresult.status_text />
                    <cfset httpStatusCode = transactionsaleresult.status_code/>

                    <cfset datapayment = {
                        inpcustomervaultid	= CheckExistsPaymentMethod.PaymentMethodID_vch,
                        inpamount		= arguments.inpOrderAmount,
                        inpcurrency  = arguments.inpCurrencyCode
                    }>

                    <cfif structKeyExists(transactionsaleresult, "status_code") AND transactionsaleresult.status_code EQ 200>
                        <cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="queryStruct">
                            <cfinvokeargument name="QueryString" value="#transactionsaleresult.filecontent#">
                        </cfinvoke>
                        <cfset statuscheckout = Find("response_code=100",transactionsaleresult.filecontent)>
                        <cfif statuscheckout GT 0> 
                            <cfset dataout.RXRESULTCODE = 1> 
                            <cfset dataout.RESULT = 'SUCCESS'>
                            <cfset dataout.MESSAGE = "Payment successfully">
                            <cfif structKeyExists(queryStruct, "transactionid")>
                                <cfset dataout.TRANSACTIONID = queryStruct['transactionid']>
                            </cfif>
                            <cfif structKeyExists(queryStruct, "responsetext")>
                                <cfset processorResponse = queryStruct['responsetext']>
                                <cfset statusMessage = queryStruct['responsetext'] />
                            </cfif>

                            <!--- LOG TO PAMENT SUCCESS --->
                            <cfif structKeyExists(queryStruct, "orderid")>
                                <cfset dataout.REPORT.ORDERID = queryStruct['orderid']>
                            <cfelse>
                                <cfset dataout.REPORT.ORDERID = 0>
                            </cfif>
                            <cfset dataout.REPORT.TRANSACTIONID = dataout.TRANSACTIONID >
                            <cfset dataout.REPORT.AMT = arguments.inpOrderAmount>
                            <cfset dataout.REPORT.RESPONSE = processorResponse> 

                            <cfset dataout.REPORT.INPNUMBER =  content.INPNUMBER >
                            <cfset dataout.REPORT.INPCVV2 = content.INPCVV2 >
                            <cfset dataout.REPORT.INPFIRSTNAME = content.INPFIRSTNAME >
                            <cfset dataout.REPORT.INPLASTNAME =  content.INPLASTNAME>
                            <cfset dataout.REPORT.INPLINE1 = content.INPLINE1 >
                            <cfset dataout.REPORT.INPCITY = content.INPCITY>
                            <cfset dataout.REPORT.INPSTATE = content.INPSTATE>
                            <cfset dataout.REPORT.INPPOSTALCODE = content.INPPOSTALCODE >
                            <cfset dataout.REPORT.PHONE = ''> 
                            <cfset dataout.REPORT.EMAIL = content.INPEMAIL>
                            <cfset dataout.REPORT.TRANSACTIONTYPE = 'sale'> 
                            <cfset dataout.REPORT.METHOD = 'CC'>
                        <cfelse>
                            <cfset dataout.RXRESULTCODE = -2> 
                            <cfset dataout.RESULT = 'PAYMENT FAIL.'>
                            <cfif structKeyExists(queryStruct, "responsetext")>
                                <cfset dataout.MESSAGE = queryStruct['responsetext']>
                            </cfif>
                        </cfif>
                    <cfelse>
                        <cfset dataout.RXRESULTCODE = -1 /> 
                        <cfset dataout.RESULT = transactionsaleresult.errordetail>
                        <cfset dataout.MESSAGE = transactionsaleresult.statuscode>                        
                    </cfif>

                     <!--- Log payment --->
                    <cftry>
                        <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
                            <cfinvokeargument name="moduleName" value="#arguments.inpModuleName#">
                            <cfinvokeargument name="status_code" value="#httpStatusCode#">
                            <cfinvokeargument name="status_text" value="#statusMessage#">
                            <cfinvokeargument name="errordetail" value="#statusMessage#">
                            <cfinvokeargument name="filecontent" value="#fileContent#">
                            <cfinvokeargument name="paymentdata" value="#datapayment#">
                            <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                            <cfinvokeargument name="paymentmethod" value="#_TOTALAPPS_GATEWAY#">
                            <cfinvokeargument name="inpPaymentByUserId" value="#arguments.inpUserId#">                        
                        </cfinvoke>
                        <cfcatch type="any">
                        </cfcatch>
                    </cftry>
                <cfelse>
                    <cfset dataout.RESULT = "FAIL">
                    <cfset dataout.RXRESULTCODE = 0>
                    <cfset dataout.MESSAGE = "Payment fail!. CC not exist.">
                    <cfreturn dataout>
                </cfif>

                <cfcatch type="any">
                    <cfset dataout.RESULT = "FAIL">
                    <cfset dataout.RXRESULTCODE = -1>
                    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
                </cfcatch>
            </cftry>
            <cfreturn dataout />
        </cffunction>
        <cffunction name="SaleWithSuspiciousVault" access="remote" output="false" hint="TotalApp: Sale With Suspicious Vault">
            <cfargument name="inpUserId" TYPE="string" required="false" default="#session.UserId#">
            <cfargument name="inpVaultId" TYPE="string" required="true">
            <cfargument name="inpPaymentReviewId" TYPE="string" required="true">
            <cfargument name="inpOrderAmount" TYPE="string" required="true" default="0.00">
            <cfargument name="inpCurrencyCode" TYPE="string" required="false" default="USD">
            <cfargument name="inpModuleName" TYPE="string" required="false" default="Buy Credits and Keywords"> 

             <cfset var dataout = structNew()>
            <cfset dataout.RESULT = "FAIL">
            <cfset dataout.RXRESULTCODE = -1>
            <cfset dataout.MESSAGE = "">
            <cfset dataout.TRANSACTIONID = "">

            <cfset dataout.REPORT = {}/>
            <cfset dataout.REPORT.ORDERID = ''>
            <cfset dataout.REPORT.TRANSACTIONID = '' >
            <cfset dataout.REPORT.AMT = ''>
            <cfset dataout.REPORT.RESPONSE = ''> 

            <cfset dataout.REPORT.INPNUMBER = ''>
            <cfset dataout.REPORT.INPCVV2 = '' >
            <cfset dataout.REPORT.INPFIRSTNAME = '' >
            <cfset dataout.REPORT.INPLASTNAME =  ''>
            <cfset dataout.REPORT.INPLINE1 = '' >
            <cfset dataout.REPORT.INPCITY = ''>
            <cfset dataout.REPORT.INPSTATE = ''>
            <cfset dataout.REPORT.INPPOSTALCODE = '' >
            <cfset dataout.REPORT.PHONE = ''> 
            <cfset dataout.REPORT.EMAIL = ''>
            <cfset dataout.REPORT.TRANSACTIONTYPE = ''> 
            <cfset dataout.REPORT.METHOD = ''>
            <cfset dataout.FILECONTENT = ''>

            <cfset var transactionsaleresult = ''/>
            <cfset var CheckExistsPaymentMethod = ''/>
            <cfset var content = ''/>
            <cfset var fileContent = ''/>  
            <cfset var statusMessage = '' />
            <cfset var processorResponse = ''/>
            <cfset var httpStatusCode = ''/>
            <cfset var datapayment = ''/>
            <cfset var vaultID=''>
            <cfset var statuscheckout	= '' />
            <cfset var queryStruct	= '' />

            <cftry>

                 <!--- Get token Mojo to payment --->
                <cfquery name="CheckExistsPaymentMethod" datasource="#Session.DBSourceEBM#">
                    SELECT
                        CardData_txt                    
                    FROM
                        simplebilling.list_payment_review
                    WHERE
                        UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"> 		
                    AND 
                        paymentGateway_ti =4
				AND 
					Id_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPaymentReviewId#"> 
                </cfquery>

                <cfif CheckExistsPaymentMethod.RecordCount GT 0>
                    <cfset vaultID= arguments.inpVaultId>
                    <cfset content= DeserializeJSON(CheckExistsPaymentMethod.CardData_txt)/>
                
                    <cfif !structKeyExists(content, "INPEMAIL")>
                        <cfset content.INPEMAIL="">
                    </cfif>
                    <cfif Len(content.INPSTATE) GT 2>
                        <cfthrow  type = "any" message = "Billing State len is 2 characters only" detail = "Billing State len is 2 characters only" > 
                    </cfif>

                    <cfhttp url="#total_apps_apiurl#" method="post" result="transactionsaleresult">
                        <cfhttpparam type="FORMFIELD" name="username" value="#total_apps_username#">
                        <cfhttpparam type="FORMFIELD" name="password" value="#total_apps_password#">
                        <cfhttpparam type="FORMFIELD" name="customer_vault_id" value="#vaultID#">
                        <cfhttpparam type="FORMFIELD" name="amount" value="#arguments.inpOrderAmount#">
                        <cfhttpparam type="FORMFIELD" name="currency" value="#arguments.inpCurrencyCode#">
                        <!---<cfhttpparam type="FORMFIELD" name="dup_seconds" value="0">--->
                    </cfhttp>

                    <cfset fileContent = transactionsaleresult.fileContent/>   
                    <cfset dataout.FILECONTENT = fileContent>
                    <cfset statusMessage = transactionsaleresult.status_text />
                    <cfset httpStatusCode = transactionsaleresult.status_code/>

                    <cfset datapayment = {
                        inpcustomervaultid	= vaultID,
                        inpamount		= arguments.inpOrderAmount,
                        inpcurrency  = arguments.inpCurrencyCode
                    }>
                    
                    <cfif structKeyExists(transactionsaleresult, "status_code") AND transactionsaleresult.status_code EQ 200>
                        <cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="queryStruct">
                            <cfinvokeargument name="QueryString" value="#transactionsaleresult.filecontent#">
                        </cfinvoke>
                        <cfset statuscheckout = Find("response_code=100",transactionsaleresult.filecontent)>
                        <cfif statuscheckout GT 0> 
                            <cfset dataout.RXRESULTCODE = 1> 
                            <cfset dataout.RESULT = 'SUCCESS'>
                            <cfset dataout.MESSAGE = "Payment successfully">
                            <cfif structKeyExists(queryStruct, "transactionid")>
                                <cfset dataout.TRANSACTIONID = queryStruct['transactionid']>
                            </cfif>
                            <cfif structKeyExists(queryStruct, "responsetext")>
                                <cfset processorResponse = queryStruct['responsetext']>
                                <cfset statusMessage = queryStruct['responsetext'] />
                            </cfif>

                            <!--- LOG TO PAMENT SUCCESS --->
                            <cfif structKeyExists(queryStruct, "orderid")>
                                <cfset dataout.REPORT.ORDERID = queryStruct['orderid']>
                            <cfelse>
                                <cfset dataout.REPORT.ORDERID = 0>
                            </cfif>
                            <cfset dataout.REPORT.TRANSACTIONID = dataout.TRANSACTIONID >
                            <cfset dataout.REPORT.AMT = arguments.inpOrderAmount>
                            <cfset dataout.REPORT.RESPONSE = processorResponse> 

                            <cfset dataout.REPORT.INPNUMBER =  content.INPNUMBER >
                            <cfset dataout.REPORT.INPCVV2 = content.INPCVV2 >
                            <cfset dataout.REPORT.INPFIRSTNAME = content.INPFIRSTNAME >
                            <cfset dataout.REPORT.INPLASTNAME =  content.INPLASTNAME>
                            <cfset dataout.REPORT.INPLINE1 = content.INPLINE1 >
                            <cfset dataout.REPORT.INPCITY = content.INPCITY>
                            <cfset dataout.REPORT.INPSTATE = content.INPSTATE>
                            <cfset dataout.REPORT.INPPOSTALCODE = content.INPPOSTALCODE >
                            <cfset dataout.REPORT.PHONE = ''> 
                            <cfset dataout.REPORT.EMAIL = content.INPEMAIL>
                            <cfset dataout.REPORT.TRANSACTIONTYPE = 'sale'> 
                            <cfset dataout.REPORT.METHOD = 'CC'>
                        <cfelse>
                            <cfset dataout.RXRESULTCODE = -2> 
                            <cfset dataout.RESULT = 'PAYMENT FAIL.'>
                            <cfif structKeyExists(queryStruct, "responsetext")>
                                <cfset dataout.MESSAGE = queryStruct['responsetext']>
                            </cfif>
                        </cfif>
                    <cfelse>
                        <cfset dataout.RXRESULTCODE = -1 /> 
                        <cfset dataout.RESULT = transactionsaleresult.errordetail>
                        <cfset dataout.MESSAGE = transactionsaleresult.statuscode>                        
                    </cfif>

                     <!--- Log payment --->
                    <cftry>
                        <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
                            <cfinvokeargument name="moduleName" value="#arguments.inpModuleName#">
                            <cfinvokeargument name="status_code" value="#httpStatusCode#">
                            <cfinvokeargument name="status_text" value="#statusMessage#">
                            <cfinvokeargument name="errordetail" value="#statusMessage#">
                            <cfinvokeargument name="filecontent" value="#fileContent#">
                            <cfinvokeargument name="paymentdata" value="#datapayment#">
                            <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                            <cfinvokeargument name="paymentmethod" value="#_TOTALAPPS_GATEWAY#">
                            <cfinvokeargument name="inpPaymentByUserId" value="#arguments.inpUserId#">                        
                        </cfinvoke>
                        <cfcatch type="any">
                        </cfcatch>
                    </cftry>
                <cfelse>
                    <cfset dataout.RESULT = "FAIL">
                    <cfset dataout.RXRESULTCODE = 0>
                    <cfset dataout.MESSAGE = "Payment fail!. CC not exist.">
                    <cfreturn dataout>
                </cfif>

                <cfcatch type="any">
                    <cfset dataout.RESULT = "FAIL">
                    <cfset dataout.RXRESULTCODE = -1>
                    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
                </cfcatch>
            </cftry>
            <cfreturn dataout />
        </cffunction>

        <!--- Delete Customer Vault---> 
        <cffunction name="CustomerVaultDelete" access="remote" output="false" hint="TotalApp: Delete customer vault">
            <cfargument name="inpCustomerVaultId" TYPE="string" required="true" default="">
            <cfargument name="inpUserId" TYPE="string" required="false" default="#session.UserId#">

            <cfset var dataout = structNew()>
            <cfset dataout.RESULT = "FAIL">
            <cfset dataout.RXRESULTCODE = -1>
            <cfset dataout.MESSAGE = "">
            <cfset var customervaultdeleteresult = ""/>
            <cfset var statuscheckout = 0>
            <cfset var queryStruct	= '' />


            <cftry>
                <cfhttp url="#total_apps_apiurl#" method="post" result="customervaultdeleteresult">
                    <cfhttpparam type="FORMFIELD" name="username" value="#total_apps_username#">
                    <cfhttpparam type="FORMFIELD" name="password" value="#total_apps_password#">
                    <cfhttpparam type="FORMFIELD" name="customer_vault" value="delete_customer">
                    <cfhttpparam type="FORMFIELD" name="customer_vault_id" value="#arguments.inpCustomerVaultId#">
                </cfhttp>
                <cfif structKeyExists(customervaultdeleteresult, "status_code") AND customervaultdeleteresult.status_code EQ 200>
                    <cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="queryStruct">
                        <cfinvokeargument name="QueryString" value="#customervaultdeleteresult.filecontent#">
                    </cfinvoke> 
                    <cfset statuscheckout = Find("response_code=100",customervaultdeleteresult.filecontent)>
                    <cfif statuscheckout GT 0>
                        <cfset dataout.RXRESULTCODE = 1> 
                        <cfset dataout.RESULT = "REMOVE VAULT SUCCESSFULLY.">
                        <cfset dataout.MESSAGE = customervaultdeleteresult.filecontent>
                    <cfelse>
                        <cfif structKeyExists(queryStruct, "responsetext")>
                            <cfset dataout.RXRESULTCODE = -1> 
                            <cfset dataout.RESULT = 'REMOVE VAULT FAIL.'>
                            <cfset dataout.MESSAGE = queryStruct['responsetext']>
                        </cfif>
                    </cfif>
                </cfif>

                <cfcatch type="any">
                    <cfset dataout.RESULT = "FAIL">
                    <cfset dataout.RXRESULTCODE = -1>
                    <cfset dataout.MESSAGE = "Exception">
                </cfcatch>
            </cftry>
            <cfreturn dataout />
        </cffunction>

    <!--- Transaction Variables --->
        <!---Capture --->
        <cffunction name="TransactionCapture" access="remote" output="false" hint="TotalApp: Transaction variables">
            <cfargument name="inpUserId" TYPE="string" required="false" default="#session.UserId#">
            <cfargument name="inpTransactionId" TYPE="string" required="true" default="">
            <cfargument name="inpAmount" TYPE="string" required="true" default="">

            <cfset var dataout = structNew()>
            <cfset dataout.RESULT = "FAIL">
            <cfset dataout.RXRESULTCODE = -1>
            <cfset dataout.MESSAGE = "">
            <cfset var transactioncaptureresult = ""/>
            <cfset var statuscheckout = 0>
            <cfset var queryStruct	= '' />


            <cftry>
                <cfhttp url="#total_apps_apiurl#" method="post" result="transactioncaptureresult">
                    <cfhttpparam type="FORMFIELD" name="username" value="#total_apps_username#">
                    <cfhttpparam type="FORMFIELD" name="password" value="#total_apps_password#">
                    <cfhttpparam type="FORMFIELD" name="type" value="capture">
                    <cfhttpparam type="FORMFIELD" name="transactionid" value="#arguments.inpTransactionId#">
                    <cfhttpparam type="FORMFIELD" name="amount" value="#arguments.inpAmount#">
                </cfhttp>
                <cfif structKeyExists(transactioncaptureresult, "status_code") AND transactioncaptureresult.status_code EQ 200>
                    <cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="queryStruct">
                        <cfinvokeargument name="QueryString" value="#transactioncaptureresult.filecontent#">
                    </cfinvoke> 
                    <cfset statuscheckout = Find("response_code=100",transactioncaptureresult.filecontent)>
                    <cfif statuscheckout GT 0>
                        <cfset dataout.RXRESULTCODE = 1> 
                        <cfset dataout.RESULT = "TRANSACTION CAPTURE SUCCESSFULLY.">
                        <cfset dataout.MESSAGE = transactioncaptureresult.filecontent>
                    <cfelse>
                        <cfif structKeyExists(queryStruct, "responsetext")>
                            <cfset dataout.RXRESULTCODE = -1> 
                            <cfset dataout.RESULT = 'TRANSACTION CAPTURE FAIL.'>
                            <cfset dataout.MESSAGE = queryStruct['responsetext']>
                        </cfif>
                    </cfif>
                </cfif>

                <cfcatch type="any">
                    <cfset dataout.RESULT = "FAIL">
                    <cfset dataout.RXRESULTCODE = -1>
                    <cfset dataout.MESSAGE = "Exception">
                </cfcatch>
            </cftry>
            <cfreturn dataout />
        </cffunction>
        <!--- Void --->
        <cffunction name="TransactionVoid" access="remote" output="false" hint="TotalApp: Void a transaction">
            <cfargument name="inpUserId" TYPE="string" required="false" default="#session.UserId#">
            <cfargument name="inpTransactionId" TYPE="string" required="true" default="">

            <cfset var dataout = structNew()>
            <cfset dataout.RESULT = "FAIL">
            <cfset dataout.RXRESULTCODE = -1>
            <cfset dataout.MESSAGE = "">
            <cfset var transactionvoidresult = ""/>
            <cfset var statuscheckout = 0>
            <cfset var queryStruct	= '' />


            <cftry>
                <cfhttp url="#total_apps_apiurl#" method="post" result="transactionvoidresult">
                    <cfhttpparam type="FORMFIELD" name="username" value="#total_apps_username#">
                    <cfhttpparam type="FORMFIELD" name="password" value="#total_apps_password#">
                    <cfhttpparam type="FORMFIELD" name="type" value="void">
                    <cfhttpparam type="FORMFIELD" name="transactionid" value="#arguments.inpTransactionId#">
                </cfhttp>
                <cfif structKeyExists(transactionvoidresult, "status_code") AND transactionvoidresult.status_code EQ 200>
                    <cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="queryStruct">
                        <cfinvokeargument name="QueryString" value="#transactionvoidresult.filecontent#">
                    </cfinvoke> 
                    <cfset statuscheckout = Find("response_code=100",transactionvoidresult.filecontent)>
                    <cfif statuscheckout GT 0>
                        <cfset dataout.RXRESULTCODE = 1> 
                        <cfset dataout.RESULT = "TRANSACTION VOID SUCCESSFULLY.">
                        <cfset dataout.MESSAGE = transactionvoidresult.filecontent>
                    <cfelse>
                        <cfif structKeyExists(queryStruct, "responsetext")>
                            <cfset dataout.RXRESULTCODE = -1> 
                            <cfset dataout.RESULT = 'TRANSACTION VOID FAIL.'>
                            <cfset dataout.MESSAGE = queryStruct['responsetext']>
                        </cfif>
                    </cfif>
                </cfif>

                <cfcatch type="any">
                    <cfset dataout.RESULT = "FAIL">
                    <cfset dataout.RXRESULTCODE = -1>
                    <cfset dataout.MESSAGE = "Exception">
                </cfcatch>
            </cftry>
            <cfreturn dataout />
        </cffunction>
        <!--- Refund --->
         <cffunction name="TransactionRefund" access="remote" output="false" hint="TotalApp: Refun a transaction">
            <cfargument name="inpUserId" TYPE="string" required="false" default="#session.UserId#">
            <cfargument name="inpTransactionId" TYPE="string" required="true" default="">
            <cfargument name="inpAmount" TYPE="string" required="false" default="0.00">

            <cfset var dataout = structNew()>
            <cfset dataout.RESULT = "FAIL">
            <cfset dataout.RXRESULTCODE = -1>
            <cfset dataout.MESSAGE = "">
            <cfset var transactionrefundresult = ""/>
            <cfset var statuscheckout = 0>
            <cfset var queryStruct	= '' />
            <cftry>
                <cfhttp url="#total_apps_apiurl#" method="post" result="transactionrefundresult">
                    <cfhttpparam type="FORMFIELD" name="username" value="#total_apps_username#">
                    <cfhttpparam type="FORMFIELD" name="password" value="#total_apps_password#">
                    <cfhttpparam type="FORMFIELD" name="type" value="refund">
                    <cfhttpparam type="FORMFIELD" name="transactionid" value="#arguments.inpTransactionId#">
                    <cfhttpparam type="FORMFIELD" name="amount" value="#arguments.inpAmount#">
                </cfhttp>
                <cfif structKeyExists(transactionrefundresult, "status_code") AND transactionrefundresult.status_code EQ 200>
                    <cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="queryStruct">
                        <cfinvokeargument name="QueryString" value="#transactionrefundresult.filecontent#">
                    </cfinvoke> 
                    <cfset statuscheckout = Find("response_code=100",transactionrefundresult.filecontent)>
                    <cfif statuscheckout GT 0>
                        <cfset dataout.RXRESULTCODE = 1> 
                        <cfset dataout.RESULT = "TRANSACTION REFUND SUCCESSFULLY.">
                        <cfset dataout.MESSAGE = transactionrefundresult.filecontent>
                    <cfelse>
                        <cfif structKeyExists(queryStruct, "responsetext")>
                            <cfset dataout.RXRESULTCODE = -1> 
                            <cfset dataout.RESULT = 'TRANSACTION REFUND FAIL.'>
                            <cfset dataout.MESSAGE = queryStruct['responsetext']>
                        </cfif>
                    </cfif>
                </cfif>

                <cfcatch type="any">
                    <cfset dataout.RESULT = "FAIL">
                    <cfset dataout.RXRESULTCODE = -1>
                    <cfset dataout.MESSAGE = "Exception">
                </cfcatch>
            </cftry>
            <cfreturn dataout />
        </cffunction>

        <!--- Sales --->
        <cffunction name="Sale" access="remote" output="true" hint="TotalApp: Sale">   
            <cfargument name="inpUserId" TYPE="string" required="false" default="#Session.UserId#">
            <cfargument name="inpcardNumber" TYPE="string" required="true" default="">		

            <cfargument name="inporderAmount" TYPE="string" required="true" default="0.00">
            <cfargument name="inpcurrencyCode" TYPE="string" required="false" default="USD">
            <cfargument name="inpcardExpiration" TYPE="string" required="true" default="">
            <cfargument name="inpcardCVV" TYPE="string" required="true" default="">

            <cfargument name="inpbillingFirstName" TYPE="string" required="true" default="">
            <cfargument name="inpbillingLastName" TYPE="string" required="true" default="">
            <cfargument name="inpbillingAddressLine1" TYPE="string" required="true" default="">
            <cfargument name="inpbillingCity" TYPE="string" required="true" default=" ">
            <cfargument name="inpbillingState" TYPE="string" required="true" default=" ">
            <cfargument name="inpbillingZipCode" TYPE="string" required="true" default=" ">
            <cfargument name="inpbillingCountry" TYPE="string" required="true" default=" ">
            <cfargument name="inpbillingEmail" TYPE="string" required="false" default=" ">

            <cfargument name="inpshippingFirstName" TYPE="string" required="false" default=" ">
            <cfargument name="inpshippingLastName" TYPE="string" required="false" default=" ">
            <cfargument name="inpshippingAddressLine1" TYPE="string" required="false" default=" ">
            <cfargument name="inpshippingAddressLine2" TYPE="string" required="false" default=" ">
            <cfargument name="inpshippingCity" TYPE="string" required="false" default=" ">
            <cfargument name="inpshippingState" TYPE="string" required="false" default=" ">
            <cfargument name="inpshippingZipCode" TYPE="string" required="false" default=" ">
            <cfargument name="inpshippingCountry" TYPE="string" required="false" default=" ">
            <cfargument name="inpshippingEmail" TYPE="string" required="false" default=" ">
            <cfargument name="inpModuleName" TYPE="string" required="false" default="Buy Credits and Keywords">  
    
            <cfset var dataout = structNew()>
            <cfset dataout.RESULT = "FAIL">
            <cfset dataout.RXRESULTCODE = -1>
            <cfset dataout.MESSAGE = "">
            <cfset dataout.STATUSCODE = ""/>
            <cfset dataout.PROCESSORREPONSECODE = ""/>

            <cfset dataout.TRANSACTIONID = "">

            <cfset dataout.REPORT = {}/>
            <cfset dataout.REPORT.ORDERID = ''>
            <cfset dataout.REPORT.TRANSACTIONID = '' >
            <cfset dataout.REPORT.AMT = ''>
            <cfset dataout.REPORT.RESPONSE = ''> 

            <cfset dataout.REPORT.INPNUMBER = ''>
            <cfset dataout.REPORT.INPCVV2 = '' >
            <cfset dataout.REPORT.INPFIRSTNAME = '' >
            <cfset dataout.REPORT.INPLASTNAME =  ''>
            <cfset dataout.REPORT.INPLINE1 = '' >
            <cfset dataout.REPORT.INPCITY = ''>
            <cfset dataout.REPORT.INPSTATE = ''>
            <cfset dataout.REPORT.INPPOSTALCODE = '' >
            <cfset dataout.REPORT.PHONE = ''> 
            <cfset dataout.REPORT.EMAIL = ''>
            <cfset dataout.REPORT.TRANSACTIONTYPE = ''> 
            <cfset dataout.REPORT.METHOD = ''>

            <cfset var transactionsaleresult = ''/>
            <cfset var dateFromCcInput = ''/>
            <cfset var processorResponse = ''/>
            <cfset var fileContent = ''/>  
            <cfset var statusMessage = '' />
            <cfset var httpStatusCode = ''/>
            <cfset var datatemp = ''/>
            <cfset var statuscheckout	= '' />
            <cfset var queryStruct	= '' />
            
            <cftry>
                <cfif IsNumeric(arguments.inpbillingState) OR IsNumeric(arguments.inpbillingAddressLine1) OR IsNumeric(arguments.inpbillingCity)>
                    <cfthrow  type = "any" message = "Billing Address, City, State should be string" detail = "Billing Address, City, State should be string" > 
                </cfif>
                
                <cfif Len(arguments.inpbillingState) GT 2>
                    <cfthrow  type = "any" message = "Billing State should be 2 characters only" detail = "Billing State should be 2 characters only" > 
                </cfif>
                <cfif Len(arguments.inpbillingAddressLine1) GT 35>
                    <cfthrow  type = "any" message = "Billing Address should be 35 characters only" detail = "Billing Address should be 35 characters only" > 
                </cfif>
                <cfif Len(arguments.inpbillingCity) GT 20>
                    <cfthrow  type = "any" message = "Billing City should be 20 characters only" detail = "Billing City should be 20 characters only" > 
                </cfif>
                <!--- check for same month year compage with last day of month, if not compare first day is enough---->            
                <cfif Int(Month(NOW())) EQ Int(left(arguments.inpcardExpiration,2)) AND Int(right(Year(NOW()),2)) EQ Int(right(arguments.inpcardExpiration,2))>
                    <cfset dateFromCcInput= createDate("20" & right(arguments.inpcardExpiration,2), left(arguments.inpcardExpiration,2), daysInMonth(NOW()))>
                <cfelse>            
                    <cfset dateFromCcInput= createDate("20" & right(arguments.inpcardExpiration,2), left(arguments.inpcardExpiration,2), 28)>
                </cfif>
                <cfif DateCompare(dateFromCcInput, NOW()) LT 1>
                    <cfthrow  type = "any" message = "Your Credit Card has been expired! Please check the Expiration Date or add another Credit Card." detail = "Your Credit Card has been expired! Please check the Expiration Date or add another Credit Card." > 
                </cfif>

                <cfhttp url="#total_apps_apiurl#" method="post" result="transactionsaleresult">
                    <cfhttpparam type="FORMFIELD" name="username" value="#total_apps_username#">
                    <cfhttpparam type="FORMFIELD" name="password" value="#total_apps_password#">
                    <cfhttpparam type="FORMFIELD" name="type" value="sale">
                    <cfhttpparam type="FORMFIELD" name="amount" value="#arguments.inporderAmount#">

                    <cfhttpparam type="FORMFIELD" name="ccnumber" value="#arguments.inpcardNumber#">
                    <cfhttpparam type="FORMFIELD" name="cvv" value="#arguments.inpcardCVV#">
                    <cfhttpparam type="FORMFIELD" name="ccexp" value="#arguments.inpcardExpiration#">

                    <cfhttpparam type="FORMFIELD" name="first_name" value="#arguments.inpbillingFirstName#">
                    <cfhttpparam type="FORMFIELD" name="last_name" value="#arguments.inpbillingLastName#">
                    <cfhttpparam type="FORMFIELD" name="address1" value="#arguments.inpbillingAddressLine1#">
                    <cfhttpparam type="FORMFIELD" name="city" value="#arguments.inpbillingCity#">
                    <cfhttpparam type="FORMFIELD" name="state" value="#arguments.inpbillingState#">
                    <cfhttpparam type="FORMFIELD" name="zip" value="#arguments.inpbillingZipCode#">
                    <cfhttpparam type="FORMFIELD" name="country" value="#arguments.inpbillingCountry#">
                    <cfhttpparam type="FORMFIELD" name="email" value="#arguments.inpbillingEmail#">

                    <cfhttpparam type="FORMFIELD" name="shipping_firstname" value="#arguments.inpshippingFirstName#">
                    <cfhttpparam type="FORMFIELD" name="shipping_lastname" value="#arguments.inpshippingLastName#">
                    <cfhttpparam type="FORMFIELD" name="shipping_address1" value="#arguments.inpshippingAddressLine1#">
                    <cfhttpparam type="FORMFIELD" name="shipping_address2" value="#arguments.inpshippingAddressLine2#">
                    <cfhttpparam type="FORMFIELD" name="shipping_city" value="#arguments.inpshippingCity#">
                    <cfhttpparam type="FORMFIELD" name="shipping_state" value="#arguments.inpshippingState#">
                    <cfhttpparam type="FORMFIELD" name="shipping_zip" value="#arguments.inpshippingZipCode#">
                    <cfhttpparam type="FORMFIELD" name="shipping_country" value="#arguments.inpshippingCountry#">
                    <cfhttpparam type="FORMFIELD" name="shipping_email" value="#arguments.inpshippingEmail#">
                    <!---<cfhttpparam type="FORMFIELD" name="dup_seconds" value="0">--->
                </cfhttp>

                <cfset fileContent = transactionsaleresult.filecontent/>  
                <cfset statusMessage = transactionsaleresult.status_text />
                <cfset httpStatusCode = transactionsaleresult.status_code/>    
                <cfset datatemp = {
                    inpNumber	= right(arguments.inpcardNumber, 4),
                    inpType		= 'sale',
                    inpcardExpiration  = arguments.inpcardExpiration,
                    inpCvv			= arguments.inpcardCVV,
                    inpAmount       = arguments.inporderAmount,

                    inpFirstName	= arguments.inpbillingFirstName,
                    inpLastName		= arguments.inpbillingLastName,
                    inpLine1		= arguments.inpbillingAddressLine1,
                    inpCity         = arguments.inpbillingCity,
                    inpCountryCode 	= arguments.inpbillingCountry,
                    inpPostalCode   = arguments.inpbillingZipCode,
                    inpState		= arguments.inpbillingState,
                    inpEmail		= arguments.inpbillingEmail,

                    inpShipFirstName	= arguments.inpshippingFirstName,
                    inpShipLastName		= arguments.inpshippingLastName,
                    inpShipLine1		= arguments.inpshippingAddressLine1,
                    inpShipLine2		= arguments.inpshippingAddressLine2,
                    inpShipCity         = arguments.inpshippingCity,
                    inpShipCountryCode 	= arguments.inpshippingCountry,
                    inpShipPostalCode   = arguments.inpshippingZipCode,
                    inpShipState		= arguments.inpshippingState,
                    inpShipEmail		= arguments.inpshippingEmail
                }>

                <cfif structKeyExists(transactionsaleresult, "status_code") AND transactionsaleresult.status_code EQ 200>
                    <cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="queryStruct">
                        <cfinvokeargument name="QueryString" value="#transactionsaleresult.filecontent#">
                    </cfinvoke>
                    <cfset statuscheckout = Find("response_code=100",transactionsaleresult.filecontent)>
        			<cfif statuscheckout GT 0> 
                        <cfset dataout.RXRESULTCODE = 1> 
                        <cfset dataout.RESULT = 'SUCCESS'>
                        <cfset dataout.MESSAGE = "Payment successfully">
                        <cfif structKeyExists(queryStruct, "transactionid")>
                            <cfset dataout.TRANSACTIONID = queryStruct['transactionid']>
                        </cfif>
                        <cfif structKeyExists(queryStruct, "responsetext")>
                            <cfset processorResponse = queryStruct['responsetext']>
                            <cfset statusMessage = queryStruct['responsetext'] />
                        </cfif>

                        <!--- LOG TO PAMENT SUCCESS --->
                        <cfif structKeyExists(queryStruct, "orderid")>
                            <cfset dataout.REPORT.ORDERID = queryStruct['orderid']>
                        <cfelse>
                            <cfset dataout.REPORT.ORDERID = 0>
                        </cfif>
                        <cfset dataout.REPORT.TRANSACTIONID = dataout.TRANSACTIONID >
                        <cfset dataout.REPORT.AMT = arguments.inpOrderAmount>
                        <cfset dataout.REPORT.RESPONSE = processorResponse> 

                        <cfset dataout.REPORT.INPNUMBER =  right(arguments.inpcardNumber, 4) >
                        <cfset dataout.REPORT.INPCVV2 = arguments.inpcardCVV >
                        <cfset dataout.REPORT.INPFIRSTNAME = arguments.inpbillingFirstName >
                        <cfset dataout.REPORT.INPLASTNAME =  arguments.inpbillingLastName>
                        <cfset dataout.REPORT.INPLINE1 = arguments.inpbillingAddressLine1 >
                        <cfset dataout.REPORT.INPCITY = arguments.inpbillingCity>
                        <cfset dataout.REPORT.INPSTATE = arguments.inpbillingState>
                        <cfset dataout.REPORT.INPPOSTALCODE = arguments.inpbillingZipCode >
                        <cfset dataout.REPORT.PHONE = ''> 
                        <cfset dataout.REPORT.EMAIL = arguments.inpbillingEmail>
                        <cfset dataout.REPORT.TRANSACTIONTYPE = 'sale'> 
                        <cfset dataout.REPORT.METHOD = 'CC'>
                    <cfelse>
                        <cfset dataout.RXRESULTCODE = -2> 
                        <cfset dataout.RESULT = 'PAYMENT FAIL.'>
                        <cfif structKeyExists(queryStruct, "responsetext")>
                            <cfset dataout.MESSAGE = queryStruct['responsetext']>
                        </cfif>
                    </cfif>
                <cfelse>
                    <cfset dataout.RXRESULTCODE = -1 /> 
                    <cfset dataout.RESULT = transactionsaleresult.errordetail>
                    <cfset dataout.MESSAGE = transactionsaleresult.statuscode>                        
                </cfif>
                <!--- Log payment --->
                <cftry>
                    <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
                        <cfinvokeargument name="moduleName" value="#arguments.inpModuleName#">
                        <cfinvokeargument name="status_code" value="#httpStatusCode#">
                        <cfinvokeargument name="status_text" value="#statusMessage#">
                        <cfinvokeargument name="errordetail" value="#statusMessage#">
                        <cfinvokeargument name="filecontent" value="#fileContent#">
                        <cfinvokeargument name="paymentdata" value="#datatemp#">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                        <cfinvokeargument name="paymentmethod" value="#_TOTALAPPS_GATEWAY#">
                    </cfinvoke>
                    <cfcatch type="any">
                    </cfcatch>
                </cftry>                           
                <cfcatch>                
                    <cfset dataout.RXRESULTCODE = -1 /> 
                    <cfset dataout.RESULT = 'FAIL'>                    
                    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">                                                
                </cfcatch>
            </cftry>
            
            <cfreturn dataout />
        </cffunction> 

        <!--- Authorization --->
        <cffunction name="Authorization" access="remote" output="true" hint="TotalApp: Authorization">   
            <cfargument name="inpUserId" TYPE="string" required="false" default="#Session.UserId#">
            <cfargument name="inpcardNumber" TYPE="string" required="true" default="">		

            <cfargument name="inporderAmount" TYPE="string" required="true" default="1">
            <cfargument name="inpcurrencyCode" TYPE="string" required="false" default="USD">
            <cfargument name="inpcardExpiration" TYPE="string" required="true" default="">
            <cfargument name="inpcardCVV" TYPE="string" required="true" default="">

            <cfargument name="inpbillingFirstName" TYPE="string" required="true" default="">
            <cfargument name="inpbillingLastName" TYPE="string" required="true" default="">
            <cfargument name="inpbillingAddressLine1" TYPE="string" required="true" default="">
            <cfargument name="inpbillingCity" TYPE="string" required="true" default=" ">
            <cfargument name="inpbillingState" TYPE="string" required="true" default=" ">
            <cfargument name="inpbillingZipCode" TYPE="string" required="true" default=" ">
            <cfargument name="inpbillingCountry" TYPE="string" required="true" default=" ">
            <cfargument name="inpbillingEmail" TYPE="string" required="false" default=" ">
            <cfargument name="inpModuleName" TYPE="string" required="false" default="Create authorization transaction">
    
            <cfset var dataout = structNew()>
            <cfset dataout.RESULT = "FAIL">
            <cfset dataout.RXRESULTCODE = -1>
            <cfset dataout.MESSAGE = "">
            <cfset dataout.STATUSCODE = ""/>
            <cfset dataout.PROCESSORREPONSECODE = ""/>

            <cfset dataout.TRANSACTIONID = "">
            <cfset dataout.AVSCODE = "">
            <cfset dataout.TOKEN = ""/>
            <cfset dataout.CCTYPE = ""/>

            <cfset dataout.REPORT = {}/>
            <cfset dataout.REPORT.ORDERID = ''>
            <cfset dataout.REPORT.TRANSACTIONID = '' >
            <cfset dataout.REPORT.AMT = ''>
            <cfset dataout.REPORT.RESPONSE = ''> 

            <cfset dataout.REPORT.INPNUMBER = ''>
            <cfset dataout.REPORT.INPCVV2 = '' >
            <cfset dataout.REPORT.INPFIRSTNAME = '' >
            <cfset dataout.REPORT.INPLASTNAME =  ''>
            <cfset dataout.REPORT.INPLINE1 = '' >
            <cfset dataout.REPORT.INPCITY = ''>
            <cfset dataout.REPORT.INPSTATE = ''>
            <cfset dataout.REPORT.INPPOSTALCODE = '' >
            <cfset dataout.REPORT.PHONE = ''> 
            <cfset dataout.REPORT.EMAIL = ''>
            <cfset dataout.REPORT.TRANSACTIONTYPE = ''> 
            <cfset dataout.REPORT.METHOD = ''>

            <cfset var transactionsaleresult = ''/>
            <cfset var dateFromCcInput = ''/>
            <cfset var processorResponse = ''/>
            <cfset var fileContent = ''/>  
            <cfset var statusMessage = '' />
            <cfset var httpStatusCode = ''/>
            <cfset var datatemp = ''/>
            <cfset var inpType="">
            <cfset var statuscheckout	= '' />
            <cfset var queryStruct	= '' />
            
            <cftry>
                <cfif IsNumeric(arguments.inpbillingState) OR IsNumeric(arguments.inpbillingAddressLine1) OR IsNumeric(arguments.inpbillingCity)>
                    <cfthrow  type = "any" message = "Billing Address, City, State should be string" detail = "Billing Address, City, State should be string" > 
                </cfif>
                
                <cfif Len(arguments.inpbillingState) GT 2>
                    <cfthrow  type = "any" message = "Billing State should be 2 characters only" detail = "Billing State should be 2 characters only" > 
                </cfif>
                <cfif Len(arguments.inpbillingAddressLine1) GT 35>
                    <cfthrow  type = "any" message = "Billing Address should be 35 characters only" detail = "Billing Address should be 35 characters only" > 
                </cfif>
                <cfif Len(arguments.inpbillingCity) GT 20>
                    <cfthrow  type = "any" message = "Billing City should be 20 characters only" detail = "Billing City should be 20 characters only" > 
                </cfif>
                <!--- check for same month year compage with last day of month, if not compare first day is enough---->            
                <cfif Int(Month(NOW())) EQ Int(left(arguments.inpcardExpiration,2)) AND Int(right(Year(NOW()),2)) EQ Int(right(arguments.inpcardExpiration,2))>
                    <cfset dateFromCcInput= createDate("20" & right(arguments.inpcardExpiration,2), left(arguments.inpcardExpiration,2), daysInMonth(NOW()))>
                <cfelse>            
                    <cfset dateFromCcInput= createDate("20" & right(arguments.inpcardExpiration,2), left(arguments.inpcardExpiration,2), 28)>
                </cfif>
                <cfif DateCompare(dateFromCcInput, NOW()) LT 1>
                    <cfthrow  type = "any" message = "Your Credit Card has been expired! Please check the Expiration Date or add another Credit Card." detail = "Your Credit Card has been expired! Please check the Expiration Date or add another Credit Card." > 
                </cfif>
                
                <cfset var rtcheckCcType="">
                <cfinvoke component="session.sire.models.cfc.payment.cc-tools" method="checkCcType" returnvariable="rtcheckCcType">
                    <cfinvokeargument name="inpcardNumber" value="#arguments.inpcardNumber#">                                
                </cfinvoke> 
                <cfset dataout.CCTYPE = rtcheckCcType.CCTYPE/>

                <cfhttp url="#total_apps_apiurl#" method="post" result="transactionsaleresult">
                    <cfhttpparam type="FORMFIELD" name="username" value="#total_apps_username#">
                    <cfhttpparam type="FORMFIELD" name="password" value="#total_apps_password#">
                    <cfhttpparam type="FORMFIELD" name="type" value="auth">
                    <cfhttpparam type="FORMFIELD" name="amount" value="#arguments.inporderAmount#">

                    <cfhttpparam type="FORMFIELD" name="ccnumber" value="#arguments.inpcardNumber#">
                    <cfhttpparam type="FORMFIELD" name="cvv" value="#arguments.inpcardCVV#">
                    <cfhttpparam type="FORMFIELD" name="ccexp" value="#arguments.inpcardExpiration#">

                    <cfhttpparam type="FORMFIELD" name="first_name" value="#arguments.inpbillingFirstName#">
                    <cfhttpparam type="FORMFIELD" name="last_name" value="#arguments.inpbillingLastName#">
                    <cfhttpparam type="FORMFIELD" name="address1" value="#arguments.inpbillingAddressLine1#">
                    <cfhttpparam type="FORMFIELD" name="city" value="#arguments.inpbillingCity#">
                    <cfhttpparam type="FORMFIELD" name="state" value="#arguments.inpbillingState#">
                    <cfhttpparam type="FORMFIELD" name="zip" value="#arguments.inpbillingZipCode#">
                    <cfhttpparam type="FORMFIELD" name="country" value="#arguments.inpbillingCountry#">
                    <cfhttpparam type="FORMFIELD" name="email" value="#arguments.inpbillingEmail#">
                    <!---<cfhttpparam type="FORMFIELD" name="dup_seconds" value="0">--->
                </cfhttp>

                <cfset fileContent = transactionsaleresult.filecontent/>  
                <cfset statusMessage = transactionsaleresult.status_text />
                <cfset httpStatusCode = transactionsaleresult.status_code/>    
                <cfset datatemp = {
                    inpNumber	= right(arguments.inpcardNumber, 4),
                    inpType		= 'sale',
                    inpcardExpiration  = arguments.inpcardExpiration,
                    inpCvv			= arguments.inpcardCVV,
                    inpAmount       = arguments.inporderAmount,

                    inpFirstName	= arguments.inpbillingFirstName,
                    inpLastName		= arguments.inpbillingLastName,
                    inpLine1		= arguments.inpbillingAddressLine1,
                    inpCity         = arguments.inpbillingCity,
                    inpCountryCode 	= arguments.inpbillingCountry,
                    inpPostalCode   = arguments.inpbillingZipCode,
                    inpState		= arguments.inpbillingState,
                    inpEmail		= arguments.inpbillingEmail
                }>
                
                <cfif structKeyExists(transactionsaleresult, "status_code") AND transactionsaleresult.status_code EQ 200>
                    <cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="queryStruct">
                        <cfinvokeargument name="QueryString" value="#transactionsaleresult.filecontent#">
                    </cfinvoke>

                    <cfset statuscheckout = Find("response_code=100",transactionsaleresult.filecontent)>
        			<cfif statuscheckout GT 0> 
                        <cfset dataout.RXRESULTCODE = 1> 
                        <cfset dataout.RESULT = 'SUCCESS'>
                        <cfset dataout.MESSAGE = "Payment successfully">
                        <cfif structKeyExists(queryStruct, "transactionid")>
                            <cfset dataout.TRANSACTIONID = queryStruct['transactionid']>
                        </cfif>
                        <cfif structKeyExists(queryStruct, "responsetext")>
                            <cfset processorResponse = queryStruct['responsetext']>
                            <cfset statusMessage = queryStruct['responsetext']>
                        </cfif>
                        <cfif structKeyExists(queryStruct, "avsresponse")>
                            <cfset dataout.AVSCODE = queryStruct['avsresponse']/>
                        </cfif>

                        <!--- LOG TO PAMENT SUCCESS --->
                        <cfif structKeyExists(queryStruct, "orderid")>
                            <cfset dataout.REPORT.ORDERID = queryStruct['orderid']>
                        <cfelse>
                            <cfset dataout.REPORT.ORDERID = 0>
                        </cfif>
                        <cfset dataout.REPORT.TRANSACTIONID = dataout.TRANSACTIONID >
                        <cfset dataout.REPORT.AMT = arguments.inpOrderAmount>
                        <cfset dataout.REPORT.RESPONSE = processorResponse> 

                        <cfset dataout.REPORT.INPNUMBER =  right(arguments.inpcardNumber, 4) >
                        <cfset dataout.REPORT.INPCVV2 = arguments.inpcardCVV >
                        <cfset dataout.REPORT.INPFIRSTNAME = arguments.inpbillingFirstName >
                        <cfset dataout.REPORT.INPLASTNAME =  arguments.inpbillingLastName>
                        <cfset dataout.REPORT.INPLINE1 = arguments.inpbillingAddressLine1 >
                        <cfset dataout.REPORT.INPCITY = arguments.inpbillingCity>
                        <cfset dataout.REPORT.INPSTATE = arguments.inpbillingState>
                        <cfset dataout.REPORT.INPPOSTALCODE = arguments.inpbillingZipCode >
                        <cfset dataout.REPORT.PHONE = ''> 
                        <cfset dataout.REPORT.EMAIL = arguments.inpbillingEmail>
                        <cfset dataout.REPORT.TRANSACTIONTYPE = 'sale'> 
                        <cfset dataout.REPORT.METHOD = 'CC'>
                    <cfelse>
                        <cfset dataout.RXRESULTCODE = -2> 
                        <cfset dataout.RESULT = 'PAYMENT FAIL.'>
                        <cfif structKeyExists(queryStruct, "responsetext")>
                            <cfset dataout.MESSAGE = queryStruct['responsetext']>
                        </cfif>
                    </cfif>
                <cfelse>
                    <cfset dataout.RXRESULTCODE = -1 /> 
                    <cfset dataout.RESULT = transactionsaleresult.errordetail>
                    <cfset dataout.MESSAGE = transactionsaleresult.statuscode>                        
                </cfif>
                <!--- Log payment --->
                <cftry>
                    <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
                        <cfinvokeargument name="moduleName" value="#arguments.inpModuleName#">
                        <cfinvokeargument name="status_code" value="#httpStatusCode#">
                        <cfinvokeargument name="status_text" value="#statusMessage#">
                        <cfinvokeargument name="errordetail" value="#statusMessage#">
                        <cfinvokeargument name="filecontent" value="#fileContent#">
                        <cfinvokeargument name="paymentdata" value="#datatemp#">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                        <cfinvokeargument name="paymentmethod" value="#_TOTALAPPS_GATEWAY#">
                    </cfinvoke>
                    <cfcatch type="any">                        
                    </cfcatch>
                </cftry>                           
                <cfcatch>                
                    <cfset dataout.RXRESULTCODE = -1 /> 
                    <cfset dataout.RESULT = 'FAIL'>                    
                    <cfset dataout.MESSAGE = "#cfcatch.Message#">                                                
                </cfcatch>
            </cftry>
            
            <cfreturn dataout />
        </cffunction> 

        <!--- get detail a credit card ---> 
        <cffunction name="getDetailCreditCardInVault" access="remote" output="false" hint="TotalApp: get detail a credit card">
            <cfargument name="inpCardId" type="string" required="false" default="">
            <cfargument name="inpUserId" TYPE="string" required="false" default="#session.UserId#">

            <cfset var dataout = structNew()>
            <cfset var CheckExistsPaymentMethod = ""/>
            <cfset dataout.RESULT = "FAIL">
            <cfset dataout.RXRESULTCODE = -1>
            <cfset dataout.VAULTID = ''>        
            
            <cfset dataout.DATA = ''>
            <cfset dataout.MESSAGE = "Fail to get detail a card Vault">
            <cfset dataout.CUSTOMERINFO = structNew()>
            <cfset var content = ''>

            <cftry>
                <cfquery name="CheckExistsPaymentMethod" datasource="#Session.DBSourceEBM#">
                    SELECT
                        extraNote,
                        PaymentMethodID_vch
                    FROM
                        simplebilling.authorize_user
                    WHERE
                        UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"> 		
                    AND 
                        paymentGateway_ti = 4	
                    AND 
                        status_ti = 1	
                </cfquery>
                <cfif CheckExistsPaymentMethod.RecordCount GT 0>
                        <cfset dataout.VAULTID = CheckExistsPaymentMethod.PaymentMethodID_vch>
                        <cfset dataout.RXRESULTCODE = 1 /> 
                        <cfset dataout.RESULT = 'SUCCESS'>
                        <cfset dataout.MESSAGE = 'Get ok'>    
                        <cfset content= DeserializeJSON(CheckExistsPaymentMethod.extraNote)/>

                        <cfset dataout.CUSTOMERINFO.firstName = content.INPFIRSTNAME/> 
                        <cfset dataout.CUSTOMERINFO.lastName = content.INPLASTNAME>
                        <cfif structKeyExists(content, "INPEMAIL")>
                        <cfset dataout.CUSTOMERINFO.emailAddress = content.INPEMAIL>
                        <cfelse>
                            <cfset dataout.CUSTOMERINFO.emailAddress = ''>   
                        </cfif>
                        <cfset dataout.CUSTOMERINFO.expirationDate = content.INPEXPIREMONTH &'/'& content.INPEXPREYEAR>                    
                        <cfset dataout.CUSTOMERINFO.maskedNumber = "XXXXXXXX" & content.INPNUMBER>
                        <cfset dataout.CUSTOMERINFO.line1 = content.INPLINE1>
                        <cfset dataout.CUSTOMERINFO.city = content.INPCITY>
                        <cfset dataout.CUSTOMERINFO.state = content.INPSTATE>
                        <cfset dataout.CUSTOMERINFO.zip = content.INPPOSTALCODE>
                        <cfset dataout.CUSTOMERINFO.country = content.INPCOUNTRYCODE> 
                        <cfset dataout.CUSTOMERINFO.phone = ''>
                        <cfset dataout.CUSTOMERINFO.paymentType = 'card'>
                        <cfif structKeyExists(content, "inpAVS")>					   
                        <cfset dataout.CUSTOMERINFO.AVSCODE = content.inpAVS>
                        <cfelse>
                            <cfset dataout.CUSTOMERINFO.AVSCODE = "">
                        </cfif>
                        
                        <cfset dataout.CUSTOMERINFO.cvv = content.INPCVV2>

                        <cfswitch expression="#TRIM(LCASE(content.INPTYPE))#"> 
                            <cfcase value="visa"> 
                                <cfset dataout.CUSTOMERINFO.creditCardType = "VISA"/>
                            </cfcase> 
                            <cfcase value="mastercard"> 
                                <cfset dataout.CUSTOMERINFO.creditCardType = "MASTERCARD"/>
                            </cfcase> 
                            <cfcase value="discover"> 
                                <cfset dataout.CUSTOMERINFO.creditCardType = "DISCOVER"/>
                            </cfcase> 
                            <cfcase value="amex"> 
                                <cfset dataout.CUSTOMERINFO.creditCardType = "AMERICANEXPRESS"/>
                            </cfcase>
                            <cfdefaultcase>
                                <cfset dataout.CUSTOMERINFO.creditCardType = "VISA"/> 
                            </cfdefaultcase> 
                        </cfswitch>       					
                <cfelse>
                    <cfset dataout.RESULT = "">
                    <cfset dataout.RXRESULTCODE = 0>
                    <cfset dataout.MESSAGE = "Get Card Detail Fail">
                </cfif>

                <cfcatch type="any">
                    <cfset dataout.RESULT = "FAIL">
                    <cfset dataout.RXRESULTCODE = -1>
                    <cfset dataout.MESSAGE = "Exception">
                </cfcatch>
            </cftry>
            <cfreturn dataout />
        </cffunction>

        <!--- storeCreditCardInVault --->
        <cffunction name="storeCreditCardInVault" access="public" output="true" hint="TotalApp: store Credit Card In Vault">
        <cfinvokeargument name="inpCardObject" value="#arguments.inpCardObject#"/>
        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>

        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = "FAIL">
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = "">
        <cfset dataout.DATA = {}>
        <cfset dataout.USERCARDID = ''>
        <cfset dataout.CCTYPE = 'MC'>
        <cfset dataout.AVSCODE = ''>
        <cfset var paymentData = {}/>
        <cfset var RxAuthorization = {}/>
        <cfset var RxCustomerVaultAdd = {}/>

        <cftry>
            
            <cfif isJson(inpCardObject)>
                <cfset paymentData = deserializeJson(inpCardObject)>
                <cfif structKeyExists(paymentData, "expirationDate")>
                    <cfset paymentData.expirationDate = LEFT(paymentData.expirationDate,2)&""&RIGHT(paymentData.expirationDate,2)>
                <cfelse>
                    <cfset paymentData.expirationDate = paymentData.inpExpireMonth&""&RIGHT(paymentData.inpExpreYear,2)>    
                </cfif>
                
                <cfinvoke component="session.sire.models.cfc.vault-totalapps" method="Authorization" returnvariable="RxAuthorization">
                    <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    <cfinvokeargument name="inpcardNumber" value="#paymentData.inpNumber#">     
                    <cfinvokeargument name="inpcurrencyCode" value="USD">
                    <cfinvokeargument name="inpcardExpiration" value="#paymentData.expirationDate#">
                    <cfinvokeargument name="inpcardCVV" value="#paymentData.inpCvv2#">
                    <cfinvokeargument name="inpbillingFirstName" value="#paymentData.inpFirstName#">
                    <cfinvokeargument name="inpbillingLastName" value="#paymentData.inpLastName#">
                    <cfinvokeargument name="inpbillingAddressLine1" value="#paymentData.inpLine1#">
                    <cfinvokeargument name="inpbillingCity" value="#paymentData.inpCity#">
                    <cfinvokeargument name="inpbillingState" value="#paymentData.inpState#">
                    <cfinvokeargument name="inpbillingZipCode" value="#paymentData.inpPostalCode#">
                    <cfinvokeargument name="inpbillingCountry" value="#paymentData.inpCountryCode#">
                    <cfinvokeargument name="inpbillingEmail" value="#paymentData.inpEmail#">
                </cfinvoke>
                
                <cfinvoke component="session.sire.models.cfc.vault-totalapps" method="CustomerVaultAdd" returnvariable="RxCustomerVaultAdd">
                    <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    <cfinvokeargument name="inpCardObject" value="#arguments.inpCardObject#">     
                </cfinvoke>
                
                <cfif RxCustomerVaultAdd.RXRESULTCODE EQ 1>
                    <cfset dataout.RXRESULTCODE = 1>
                    <cfset dataout.RESULT = "Success">
                    <cfset dataout.USERCARDID = RxCustomerVaultAdd.USERCARDID>
                    <cfset dataout.CCTYPE = RxCustomerVaultAdd.CCTYPE>
                <cfelse>
                    <cfset dataout.RXRESULTCODE = -1>
                    <cfset dataout.RESULT = "Fail">
                </cfif>
                
                <cfif RxAuthorization.RXRESULTCODE EQ 1>
                    <cfset dataout.RXRESULTCODE = 1>
                    <cfset dataout.RESULT = "Success">
                    <cfif structKeyExists(RxAuthorization, "REPORT")>
                        <cfset dataout.REPORT = RxAuthorization.REPORT>
                    </cfif>
                    <cfset dataout.AVSCODE = RxAuthorization.AVSCODE>                     
                <cfelse>
                    <cfset dataout.RXRESULTCODE = RxAuthorization.RXRESULTCODE>
                    <cfset dataout.MESSAGE = RxAuthorization.MESSAGE>
                    <cfset dataout.AVSCODE = RxAuthorization.AVSCODE>                                           
                </cfif>
                
                
            <cfelse>
                <cfset dataout.RXRESULTCODE = -2>
                <cfset dataout.MESSAGE = 'Invalid inpPaymentdata'>
            </cfif>

        <cfcatch type="any">
            <cfset dataout.RXRESULTCODE = -1 /> 
            <cfset dataout.RESULT = 'FAIL'>                    
            <cfset dataout.MESSAGE = cfcatch.MESSAGE>  
             <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>             
        </cfcatch>

        </cftry>
        <cfreturn dataout/>
    </cffunction>
</cfcomponent>