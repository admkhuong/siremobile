<cfcomponent>
	<cfinclude template="/public/sire/configs/paths.cfm" >
	<cfinclude template="../../configs/paths.cfm" >

	<cffunction name="GetWaitList" access="remote" output="false" hint="Get wait list app">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho" />
		<cfargument name="iSortCol_1" default="-1" />
		<cfargument name="sSortDir_1" default="" />
		<cfargument name="iSortCol_2" default="-1" />
		<cfargument name="sSortDir_2" default="" />
		<cfargument name="iSortCol_0" default="-1" />
		<cfargument name="sSortDir_0" default="" />
		<cfargument name="customFilter" default="" />
		<cfargument name="inpListId" required="true" type="numeric"/>
		<cfset var dataout = {} />
		<cfset var filterData = DeserializeJSON(arguments.customFilter) />

		<cfset var GetWaitList = "" />
		<cfset var rsGetWaitList = '' />
		<cfset var WaitListSetting = '' />
		<cfset var filterItem = '' />
		<cfset var rsCount = '' />
		<cfset var contactArray = [] />
		<cfset var contact = '' />
		<cfset var newMessage = structNew() />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset dataout["iTotalRecords"] = 0 />
		<cfset dataout["iTotalDisplayRecords"] = 0 />

		<cfset dataout["WaitList"] = ArrayNew() />

		<cftry>
			<cfquery datasource="#Session.DBSourceREAD#" name="GetWaitList" result="rsGetWaitList">
				SELECT SQL_CALC_FOUND_ROWS
					w.WaitListId_bi,
					w.CustomerName_vch,
					w.CustomerEmail_vch,
					w.CustomerContactString_vch,
					w.WaitListSize_int,
					w.WaitTime_int,
					w.Status_int,
					w.Note_vch,
					w.Created_dt,
					w.Updated_dt,
					w.AlertedEmail_ti,
					w.AlertedSMS_ti,
					w.Order_bi,
					s.ServiceName_vch
				FROM 
					simpleobjects.waitlistapp w
				LEFT JOIN
					simpleobjects.waitlistservice s
				ON
					w.ServiceId_int = s.ServiceId_int AND s.Active_ti = 1
				WHERE
					1
					<cfif customFilter NEQ "">
						<cfloop array="#filterData#" index="filterItem">
							<cfif filterItem.NAME EQ ' w.WaitListId_bi ' AND  filterItem.VALUE LT 0>
							<!--- DO nothing --->
							<cfelseif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
							<cfelse>
								AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
							</cfif>
						</cfloop>
					</cfif>
					AND
						w.Active_ti = 1
					AND
						w.Status_int = 1
					AND
						w.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#">
					AND
						w.ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
					ORDER BY
						w.Order_bi #arguments.sSortDir_0#
					LIMIT
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#" />
					OFFSET
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#" />
			</cfquery>
			<cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			<cfloop query="rsCount">
				<cfset dataout["iTotalDisplayRecords"] = iTotalRecords />
				<cfset dataout["iTotalRecords"] = iTotalRecords />
			</cfloop>

			<cfset WaitListSetting = GetUserWaitlistAppSetting(arguments.inpListId) />

			<cfif WaitListSetting.RXRESULTCODE LT 1>
				<cfthrow type="Application" message="Get setting failed" />
			</cfif>

			<cfloop array="#valueArray(GetWaitList.CustomerContactString_vch)#" index="contact">
				<cfset arrayAppend(contactArray, contact)/>
			</cfloop>

			<cfset newMessage = CountNewMessage(contactArray, arguments.inpListId) />
			<cfset newMessage = newMessage.RESULT />
			<cfloop query="GetWaitList">
				<cfset var tmpWait = '' />
				<cfset var tempIndex = 0/>
				<cfset var waitTime = '' />
				<cfset var alertSetting = '' />
				<cfset var waited = '' />
				<cfset var min = 0 />
				<cfset var identifier = '' />
				<cfset var newMessageCount = '' />

				<cfif structKeyExists(newMessage, "#CustomerContactString_vch#")>
					<cfset newMessageCount = newMessage["#CustomerContactString_vch#"].COUNT />
				</cfif>

				<cfif CustomerName_vch NEQ "">
					<cfset identifier = CustomerName_vch/>
				<cfelseif CustomerContactString_vch NEQ "">
					<cfset identifier = FormatPhoneNumber(CustomerContactString_vch)/>
				</cfif>

				<cfif WaitTime_int GT 60>
					<cfset waitTime = WaitTime_int\60 & ' hour' />
					<cfif WaitTime_int%60 GT 0>
						<cfset waitTime = waitTime & ' ' & WaitTime_int%60 & ' min'/>
					</cfif>
				<cfelseif WaitTime_int EQ 60>
					<cfset waitTime = '1 hour' />
				<cfelseif WaitTime_int GT 0>
					<cfset waitTime = '#WaitTime_int#' & ' min' />
				<cfelse>
					<cfset waitTime = '' />
				</cfif>
				<cfif waitTime NEQ ''>
					<cfif WaitTime_int LT dateDiff('n', Created_dt, NOW())>
						<cfset waitTime = '<span class="red-text">#waitTime#</span>'/>
					<cfelse>
						<cfset waitTime = '<span class="green-text">#waitTime#</span>'/>
					</cfif>

					<cfset min = dateDiff('n', Created_dt, NOW())/>
					<cfif min GTE (60*24)>
						<cfset waited = waited & "#min\(60*24)#d"/>
						<cfset min = min%(60*24) />
						<cfif min GTE 60>
							<cfset waited = waited & ":#min\60#h"/>
							<cfset min = min%60 />
							<cfif min GT 0>
								<cfset waited = waited & ":#min#m"/>
							</cfif>
						<cfelse>
							<cfset waited = waited & ":#min#m"/>
						</cfif>
					<cfelseif min GTE 60>
						<cfset waited = waited & "#min\60#h"/>
						<cfset min = min%60>
						<cfif min GT 0>
							<cfset waited = waited & ":#min#m"/>
						</cfif>
					<cfelseif min GT 0>
						<cfset waited = waited & "#REReplace(min,"^0+","")#m"/>
					</cfif>
					<cfset min = dateDiff('s', Created_dt, NOW())/>
					<cfif min GT 0>
						<cfset waited = waited & iIf(waited NEQ '', DE(":"), DE("")) & "#min%60#s" />
					</cfif>
				</cfif>

				<cfif (WaitListSetting.ALERTEMAILSTA GT 0 AND CustomerEmail_vch NEQ '' AND AlertedEmail_ti EQ 0) OR (WaitListSetting.ALERTTEXTSTA GT 0 AND CustomerContactString_vch NEQ '' AND AlertedSMS_ti EQ 0)>
					<cfset alertSetting =
						'<div class="dropdown" style="display:inline">
							<a type="button" class="btn btn-icon btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="glyphicon glyphicon-bell custom-icon"></span></a>
							<ul class="dropdown-menu">' />
					<cfif WaitListSetting.ALERTEMAILSTA GT 0 AND CustomerEmail_vch NEQ '' AND AlertedEmail_ti EQ 0>
						<cfset alertSetting = alertSetting & '<li><a class="btn btn-alert btn-send-alert" data-waitid="#WaitListId_bi#" data-type="email">Alert Email</a></li>' />
					</cfif>
					<cfif WaitListSetting.ALERTTEXTSTA GT 0 AND CustomerContactString_vch NEQ '' AND AlertedSMS_ti EQ 0>
						<cfset alertSetting = alertSetting & '<li><a class="btn btn-alert btn-send-alert" data-waitid="#WaitListId_bi#" data-type="sms">Alert SMS</a></li>' />
					</cfif>
					<cfset alertSetting = alertSetting & '</ul></div>' />
				</cfif>

				<cfset tmpWait = [
					'#Order_bi#',
					'#identifier#</br>
					#iIf(WaitListSize_int GT 0, DE("#WaitListSize_int# Person"), DE(""))#
					#iIf(WaitListSize_int NEQ 0 AND WaitTime_int NEQ 0, DE(" | "), DE(""))#
					#iIf(WaitTime_int GT 0, DE("<span class='waited-timer' data-now='#dateFormat(NOW(), "yyyy-mm-dd")#T#timeFormat(NOW(), "HH:mm:ss")#' data-created='#dateFormat(Created_dt, "yyyy-mm-dd")#T#timeFormat(Created_dt, "HH:mm:ss")#' data-timezone='#getTimezone()#'>#waited#</span><span>/</span>#waitTime#"), DE(""))#
					#iIf((WaitTime_int NEQ 0 OR WaitListSize_int NEQ 0) AND ServiceName_vch NEQ "", DE(" | "), DE(""))#
					#iIf(ServiceName_vch NEQ "", DE("#ServiceName_vch#"), DE(""))#
					#iIf((WaitTime_int NEQ 0 OR WaitListSize_int NEQ 0 OR ServiceName_vch NEQ "") AND Note_vch NEQ "", DE(" | "), DE(""))#
					#iIf(Note_vch NEQ "", DE("<a class='custom-icon-small' data-toggle='popover' data-placement='top' data-trigger='hover' data-original-title title data-content='#Note_vch#'>
						<span class='glyphicon glyphicon-file' aria-hidden='true'></span>
					</a>"), DE(""))#
					#iIf((WaitTime_int NEQ 0 OR WaitListSize_int NEQ 0 OR ServiceName_vch NEQ "" OR Note_vch NEQ "") AND CustomerContactString_vch NEQ "", DE(" | "), DE(""))#
					#iIf(CustomerContactString_vch NEQ "", DE("<a data-toggle='popover' data-placement='top' data-trigger='hover' data-original-title title data-contactstring='#CustomerContactString_vch#' class='contact-string custom-icon-small'  data-content='#FormatPhoneNumber(CustomerContactString_vch)#'>
						<span class='glyphicon glyphicon-earphone' aria-hidden='true'></span>
					</a>"), DE(""))#
					#iIf((WaitTime_int NEQ 0 OR WaitListSize_int NEQ 0 OR ServiceName_vch NEQ "" OR Note_vch NEQ "" OR CustomerContactString_vch NEQ "") AND CustomerEmail_vch NEQ "", DE(" | "), DE(""))#
					#iIf(CustomerEmail_vch NEQ "", DE("<a class='custom-icon-small' data-toggle='popover' data-placement='top' data-trigger='hover' data-original-title title data-content='#CustomerEmail_vch#'>
						<span class='glyphicon glyphicon-envelope' aria-hidden='true'></span>
					</a>"), DE(""))#',
					'#alertSetting#
					#iIf(CustomerContactString_vch NEQ "", DE("<button data-toggle='popover' data-placement='top' data-trigger='hover' data-content='SMS Conversation' type='button' class='btn btn-icon btn-default btn-sms' data-customername='#CustomerName_vch#' data-created='#dateFormat(Created_dt, 'yyyy-mm-dd')# #timeFormat(Created_dt, 'HH:mm:ss')#' data-waitid='#WaitListId_bi#' data-contactstring='#CustomerContactString_vch#'>
						<span id='span-#CustomerContactString_vch#'>#newMessageCount#</span> <span class='glyphicon glyphicon-comment custom-icon'></span>
					</button>&nbsp;"), DE(""))#
					#iIf("#dateFormat(Created_dt, "yyyy-mm-dd")#" EQ "#dateFormat(NOW(), "yyyy-mm-dd")#", DE("<button data-toggle='popover' data-placement='top' data-trigger='hover' data-content='Served' type='button' class='btn btn-icon btn-default btn-served' data-waitid='#WaitListId_bi#'>
						<span class='glyphicon glyphicon-ok custom-icon'></span>
					</button>&nbsp;"), DE(""))#
					<button data-toggle="popover" data-placement="top" data-trigger="hover" data-content="No-show" type="button" class="btn btn-icon btn-default btn-noshow" data-waitid="#WaitListId_bi#">
						<span class="glyphicon glyphicon-remove custom-icon"></span>
					</button>&nbsp;
					<button data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Edit" type="button" class="btn btn-icon btn-default btn-edit" data-waitid="#WaitListId_bi#">
						<span class="glyphicon glyphicon-pencil custom-icon"></span>
					</button>'
				] />

				<cfset ArrayAppend(dataout["WaitList"], tmpWait) />
			</cfloop>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch type="any">
				<cfset dataout.TYPE = cfcatch.TYPE />
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
			</cfcatch>
		</cftry>
		<cfreturn serializeJSON(dataout) />
	</cffunction>

	<cffunction name="GetWaitListHistory" access="remote" output="false" hint="Get wait list app">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho" />
		<cfargument name="iSortCol_1" default="-1" />
		<cfargument name="sSortDir_1" default="" />
		<cfargument name="iSortCol_2" default="-1" />
		<cfargument name="sSortDir_2" default="" />
		<cfargument name="iSortCol_0" default="-1" />
		<cfargument name="sSortDir_0" default="" />
		<cfargument name="customFilter" default="" />
		<cfargument name="inpListId" required="true" type="numeric"/>
		<cfset var dataout = {} />

		<cfset var filter = "%" & Replace(arguments.customFilter, """", "", "ALL") & "%" />

		<cfset var GetWaitList = "" />
		<cfset var rsGetWaitList = '' />
		<cfset var i = 1 />
		<cfset var rsCount = '' />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset dataout["iTotalRecords"] = 0 />
		<cfset dataout["iTotalDisplayRecords"] = 0 />

		<cfset dataout["WaitList"] = ArrayNew() />

		<cftry>
			<cfquery datasource="#Session.DBSourceEBM#" name="GetWaitList" result="rsGetWaitList">
				SELECT SQL_CALC_FOUND_ROWS
					w.WaitListId_bi,
					w.CustomerName_vch,
					w.CustomerEmail_vch,
					w.CustomerContactString_vch,
					w.WaitListSize_int,
					w.WaitTime_int,
					w.Status_int,
					w.Note_vch,
					w.Created_dt,
					s.ServiceName_vch
				FROM 
					simpleobjects.waitlistapp w
				LEFT JOIN
					simpleobjects.waitlistservice s
				ON
					w.ServiceId_int = s.ServiceId_int
				WHERE
					1
					<cfif customFilter NEQ "">
						AND (
								CustomerName_vch LIKE  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE="#filter#">
								OR
								CustomerEmail_vch LIKE  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE="#filter#">
								OR
								CustomerContactString_vch LIKE  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE="#filter#">
							)
					</cfif>
					AND
						w.Active_ti = 1
					AND
						w.Status_int = 2
					AND
						w.IsDisplay_ti = 1
					AND
						w.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#">
					AND
						w.ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
					ORDER BY
						w.WaitListId_bi #arguments.sSortDir_0#
					LIMIT
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#" />
					OFFSET
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#" />
			</cfquery>
			<cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			<cfloop query="rsCount">
				<cfset dataout["iTotalDisplayRecords"] = iTotalRecords />
				<cfset dataout["iTotalRecords"] = iTotalRecords />
			</cfloop>
			<cfloop query="GetWaitList">
				<cfset var identifier = '' />
				<cfset var tmpIndex = 0 />

				<cfif CustomerName_vch NEQ "">
					<cfset identifier = CustomerName_vch/>
				<cfelseif CustomerContactString_vch NEQ "">
					<cfset identifier = FormatPhoneNumber(CustomerContactString_vch)/>
				</cfif>

				<cfset tmpIndex = iIf((arguments.sSortDir_0 EQ "asc"), i, dataout["iTotalRecords"]+1-i) />
				<cfset i++>

				<cfset var tmpWait = [
					'#tmpIndex#',
					'#identifier#</br>
					#dateFormat(Created_dt, "yyyy-mm-dd")# | 
					#iIf(WaitListSize_int GT 0, DE("#WaitListSize_int# Person"), DE(""))#
					#iIf(WaitListSize_int NEQ 0 AND ServiceName_vch NEQ "", DE(" | "), DE(""))#
					#iIf(ServiceName_vch NEQ "", DE("#ServiceName_vch#"), DE(""))#
					#iIf((WaitListSize_int NEQ 0 OR ServiceName_vch NEQ "") AND Note_vch NEQ "", DE(" | "), DE(""))#
					#iIf(Note_vch NEQ "", DE("<a class='custom-icon-small' data-toggle='popover' data-placement='top' data-trigger='hover' data-original-title title data-content='#Note_vch#'>
						<span class='glyphicon glyphicon-file' aria-hidden='true'></span>
					</a>"), DE(""))#
					#iIf(( WaitListSize_int NEQ 0 OR ServiceName_vch NEQ "" OR Note_vch NEQ "") AND CustomerContactString_vch NEQ "", DE(" | "), DE(""))#
					#iIf(CustomerContactString_vch NEQ "", DE("<a data-toggle='popover' data-placement='top' data-trigger='hover' data-original-title title data-contactstring='#CustomerContactString_vch#' class='contact-string custom-icon-small'  data-content='#FormatPhoneNumber(CustomerContactString_vch)#'>
						<span class='glyphicon glyphicon-earphone' aria-hidden='true'></span>
					</a>"), DE(""))#
					#iIf((WaitListSize_int NEQ 0 OR ServiceName_vch NEQ "" OR Note_vch NEQ "" OR CustomerContactString_vch NEQ "") AND CustomerEmail_vch NEQ "", DE(" | "), DE(""))#
					#iIf(CustomerEmail_vch NEQ "", DE("<a class='custom-icon-small' data-toggle='popover' data-placement='top' data-trigger='hover' data-original-title title data-content='#CustomerEmail_vch#'>
						<span class='glyphicon glyphicon-envelope' aria-hidden='true'></span>
					</a>"), DE(""))#',
					'<button data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Done" type="button" class="btn btn-icon btn-default btn-done" data-waitid="#WaitListId_bi#">
						<span class="glyphicon custom-icon glyphicon-ok"></span>
					</button>&nbsp;
					#iIf("#dateFormat(Created_dt, "yyyy-mm-dd")#" EQ "#dateFormat(NOW(), "yyyy-mm-dd")#", DE("<button data-toggle='popover' data-placement='top' data-trigger='hover' data-content='Undo' type='button' class='btn btn-icon btn-default btn-undo-served' data-waitid='#WaitListId_bi#'>
						<span class='glyphicon custom-icon glyphicon-repeat'></span>
					</button>"), DE(""))#'
				] />

				<cfset ArrayAppend(dataout["WaitList"], tmpWait) />
			</cfloop>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch type="any">
				<cfset dataout.TYPE = cfcatch.TYPE />
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
			</cfcatch>
		</cftry>
		<cfreturn serializeJSON(dataout) />
	</cffunction>

	<cffunction name="ServedWaitList" access="remote" output="false" hint="Update wait list as served">
		<cfargument name="inpWaitListId" type="numeric" required="true">
		<cfargument name="inpListId" required="true" type="numeric"/>
		<cfset var dataout = {} />

		<cfset var UpdateWaitList = "" />
		<cfset var GetWaitlist = '' />
		<cfset var lastOrderResult = '' />
		<cfset var updateLowerOrder = '' />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cftry>
			<cfquery datasource="#session.DBSourceEBM#" result="UpdateWaitList">
				UPDATE
					simpleobjects.waitlistapp
				SET
					Status_int = 2
				WHERE
					WaitListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpWaitListId#">
				AND
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#">
				AND
					ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
				AND
					DATE(Created_dt) = CURDATE()
				AND
					Status_int = 1
				AND
					Active_ti = 1
			</cfquery>

			<cfif UpdateWaitList.RECORDCOUNT GT 0>
				<cfquery datasource="#session.DBSourceEBM#" name="GetWaitlist">
					SELECT
						Order_bi
					FROM
						simpleobjects.waitlistapp
					WHERE
						WaitListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpWaitListId#">
					AND
						UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#">
					AND
						ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
					LIMIT
						1
				</cfquery>

				<cfinvoke component="session.sire.models.cfc.waitlistapp" method="GetLastOrderByUserId" returnvariable="lastOrderResult">
					<cfinvokeargument name="inpListId" value="#arguments.inpListId#"/>
				</cfinvoke>
				<cfinvoke component="session.sire.models.cfc.waitlistapp" method="UpdateLowerOrder" returnvariable="updateLowerOrder">
					<cfinvokeargument name="inpWaitlistId" value="#arguments.inpWaitListId#">
					<cfinvokeargument name="inpCurrentCustomerOrder" value="#GetWaitlist.Order_bi#">
					<cfinvokeargument name="inpCustomerOrder" value="#lastOrderResult.LASTORDER#">
					<cfinvokeargument name="inpListId" value="#arguments.inpListId#"/>
				</cfinvoke>

				<cfset dataout.RXRESULTCODE = 1 />
				<cfinvoke component="session.sire.models.cfc.waitlistapp" method="LogState">
					<cfinvokeargument name="inpWaitlistID" value="#arguments.inpWaitListId#">
					<cfinvokeargument name="inpWaitlistStatus" value="2">
				</cfinvoke>
			<cfelse>
				<cfset dataout.MESSAGE = "Customer cancelled this waitlist or it was expired!" />
			</cfif>

			<cfcatch type="any">
				<cfset dataout.TYPE = cfcatch.TYPE />
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
			</cfcatch>
		</cftry>

		<cfreturn serializeJSON(dataout) />

	</cffunction>

	<cffunction name="DoneWaitList" access="remote" output="false" hint="Update wait list as done">
		<cfargument name="inpWaitListId" type="numeric" required="true">
		<cfargument name="inpListId" required="true" type="numeric"/>
		<cfset var dataout = {} />

		<cfset var UpdateWaitList = "" />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cftry>
			<cfquery datasource="#session.DBSourceEBM#" result="UpdateWaitList">
				UPDATE
					simpleobjects.waitlistapp
				SET
					IsDisplay_ti = 0,
					Updated_dt = Updated_dt
				WHERE
					WaitListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpWaitListId#">
				AND
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#">
				AND
					ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
				AND
					Active_ti = 1
				AND
					Status_int = 2
			</cfquery>

			<cfif UpdateWaitList.RECORDCOUNT GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
			<cfelse>
				<cfset dataout.MESSAGE = "Customer cancelled this waitlist!" />
			</cfif>

			<cfcatch type="any">
				<cfset dataout.TYPE = cfcatch.TYPE />
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
			</cfcatch>
		</cftry>

		<cfreturn serializeJSON(dataout) />

	</cffunction>

	<cffunction name="UndoServedWaitList" access="remote" output="false" hint="Update wait list as waiting">
		<cfargument name="inpWaitListId" type="numeric" required="true">
		<cfargument name="inpListId" type="numeric" required="true"/>
		<cfset var dataout = {} />

		<cfset var UpdateWaitList = "" />
		<cfset var GetWaitList = '' />
		<cfset var CheckDuplicate = '' />
		<cfset var lastOrderResult = '' />
		<cfset var newOrder = 0 />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cftry>
			<cfquery datasource="#session.DBSourceREAD#" name="GetWaitList">
				SELECT
					CustomerEmail_vch,
					CustomerContactString_vch,
					ServiceId_int
				FROM
					simpleobjects.waitlistapp
				WHERE
					WaitListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpWaitListId#"/>
				AND
					ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
				AND
					Active_ti = 1
				AND
					Status_int = 2
			</cfquery>

			<cfif GetWaitList.RECORDCOUNT LT 1>
				<cfthrow type="Application" message="Waitlist not exist!"/>
			</cfif>

			<cfif GetWaitList.CustomerEmail_vch NEQ ''>
				<cfquery datasource="#session.DBSourceREAD#" name="CheckDuplicate">
					SELECT
						WaitListId_bi
					FROM
						simpleobjects.waitlistapp
					WHERE
						CustomerEmail_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#GetWaitList.CustomerEmail_vch#"/>
					AND
						UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
					AND
						ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
					AND
						Active_ti = 1
					AND
						Status_int = 1
				</cfquery>

				<cfif CheckDuplicate.RECORDCOUNT GT 0>
					<cfthrow type="Application" message="User email already in use!"/>
				</cfif>
			</cfif>

			<cfif GetWaitList.CustomerContactString_vch NEQ ''>
				<cfquery datasource="#session.DBSourceREAD#" name="CheckDuplicate">
					SELECT
						WaitListId_bi
					FROM
						simpleobjects.waitlistapp
					WHERE
						CustomerContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#GetWaitList.CustomerContactString_vch#"/>
					AND
						UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
					AND
						ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
					AND
						Active_ti = 1
					AND
						Status_int = 1
				</cfquery>

				<cfif CheckDuplicate.RECORDCOUNT GT 0>
					<cfthrow type="Application" message="User phone number already in use!"/>
				</cfif>
			</cfif>

			<cfinvoke component="session.sire.models.cfc.waitlistapp" method="GetLastOrderByUserId" returnvariable="lastOrderResult">
				<cfinvokeargument name="inpListId" value="#arguments.inpListId#"/>
			</cfinvoke>
			<cfif lastOrderResult.RXRESULTCODE GT 0>
				<cfset newOrder = lastOrderResult.LASTORDER + 1/>
			<cfelseif lastOrderResult.RXRESULTCODE EQ 0>
				<cfset newOrder = 1/>
			<cfelse>
				<cfthrow type="Application" message="Can't get waitlist order! Undo failed"/>
			</cfif>
			<cfquery datasource="#session.DBSourceEBM#" result="UpdateWaitList">
				UPDATE
					simpleobjects.waitlistapp
				SET
					Status_int = 1,
					Created_dt = NOW(),
					Order_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#newOrder#"/>
				WHERE
					WaitListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpWaitListId#">
				AND
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#">
				AND
					ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
				AND
					Active_ti = 1
				AND
					Status_int = 2
				AND
					DATE(Created_dt) = CURDATE()
			</cfquery>

			<cfif UpdateWaitList.RECORDCOUNT GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
			<cfelse>
				<cfset dataout.MESSAGE = "This waiting is too old! Please create new waiting instead!" />
			</cfif>

			<cfcatch type="any">
				<cfset dataout.TYPE = cfcatch.TYPE />
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
			</cfcatch>
		</cftry>

		<cfreturn serializeJSON(dataout) />

	</cffunction>

	<cffunction name="NoShowWaitList" access="remote" output="false" hint="Update wait list as now-show">
		<cfargument name="inpWaitListId" type="numeric" required="true">
		<cfargument name="inpListId" type="numeric" required="true"/>
		<cfset var dataout = {} />

		<cfset var UpdateWaitList = "" />
		<cfset var GetWaitlist = '' />
		<cfset var lastOrderResult = '' />
		<cfset var updateLowerOrder = '' />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cftry>
			<cfquery datasource="#session.DBSourceEBM#" result="UpdateWaitList">
				UPDATE
					simpleobjects.waitlistapp
				SET
					Status_int = 3
				WHERE
					WaitListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpWaitListId#">
				AND
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#">
				AND
					ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
				AND
					Active_ti = 1
				AND
					Status_int = 1
			</cfquery>
			
			<cfif UpdateWaitList.RECORDCOUNT GT 0>
				<cfquery datasource="#session.DBSourceEBM#" name="GetWaitlist">
					SELECT
						Order_bi
					FROM
						simpleobjects.waitlistapp
					WHERE
						WaitListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpWaitListId#">
					AND
						UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#">
					AND
						ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
				</cfquery>

				<cfinvoke component="session.sire.models.cfc.waitlistapp" method="GetLastOrderByUserId" returnvariable="lastOrderResult">
					<cfinvokeargument name="inpListId" value="#arguments.inpListId#"/>
				</cfinvoke>
				<cfinvoke component="session.sire.models.cfc.waitlistapp" method="UpdateLowerOrder" returnvariable="updateLowerOrder">
					<cfinvokeargument name="inpWaitlistId" value="#arguments.inpWaitListId#">
					<cfinvokeargument name="inpCurrentCustomerOrder" value="#GetWaitlist.Order_bi#">
					<cfinvokeargument name="inpCustomerOrder" value="#lastOrderResult.LASTORDER#">
					<cfinvokeargument name="inpListId" value="#arguments.inpListId#"/>
				</cfinvoke>

				<cfset dataout.RXRESULTCODE = 1 />

				<cfinvoke component="session.sire.models.cfc.waitlistapp" method="LogState">
					<cfinvokeargument name="inpWaitlistID" value="#arguments.inpWaitListId#">
					<cfinvokeargument name="inpWaitlistStatus" value="3">
				</cfinvoke>
			<cfelse>
				<cfset dataout.MESSAGE = "Customer cancelled this waitlist!" />
			</cfif>

			<cfcatch type="any">
				<cfset dataout.TYPE = cfcatch.TYPE />
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
			</cfcatch>
		</cftry>

		<cfreturn serializeJSON(dataout) />

	</cffunction>

	<cffunction name="GetTodayStatistic" access="remote" output="false" hint="Get statistics of today">
		<cfargument name="inpListId" type="numeric" required="true"/>
		<cfset var dataout = {} />

		<cfset var GetTodayStatistic = "" />
		<cfset var todayStatisticData = {} />
		<cfset var batchId = '' />
		<cfset var ireQuery = '' />
		<cfset var total = '' />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.STATUS = {} />
		<cfset dataout.STATUS['waitlisted'] = 0 />
		<cfset dataout.STATUS['served'] = 0 />
		<cfset dataout.STATUS['noshow'] = 0 />
		<cfset dataout.STATUS['texted'] = 0 />


		<cftry>
			<cfinvoke method="GetAnalyticData" returnvariable="todayStatisticData">
				<cfinvokeargument name="inpDateStart" value="#DateFormat(NOW(), "yyyy-mm-dd")#">
				<cfinvokeargument name="inpDateEnd" value="#DateFormat(NOW(), "yyyy-mm-dd")#">
				<cfinvokeargument name="inpListId" value="#arguments.inpListId#">
			</cfinvoke>

			<cfset todayStatisticData = DeserializeJSON(todayStatisticData) />

			<cfset dataout.STATUS['served'] = todayStatisticData.DATA.SUMMARY.served />
			<cfset dataout.STATUS['noshow'] = todayStatisticData.DATA.SUMMARY.noshow/>

			<cfset dataout.STATUS['waitlisted'] = todayStatisticData.DATA.SUMMARY.noshow/>


			<cfset batchId = GetWaitListBatch(arguments.inpListId) />

			<cfif batchId.RXRESULTCODE GT 0>
				<cfquery datasource="#session.DBSourceREAD#" name="ireQuery">
					SELECT
						COUNT(DISTINCT ContactString_vch) AS COUNT
					FROM
						simplexresults.ireresults
					WHERE
						BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#batchId.BatchId#" />
					AND
						UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#" />
					AND
						DATE(Created_dt) = CURDATE()
				</cfquery>
				
				<cfset dataout.STATUS['texted'] = ireQuery.COUNT />
			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch type="any">
				<cfset dataout.TYPE = cfcatch.TYPE />
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
			</cfcatch>
		</cftry>

		<cfreturn serializeJSON(dataout) />
	</cffunction>

	<cffunction name="CreateWaitListBatch" access="private" output="false" hint="Create wait list">
		<cfargument name="inpListId" type="numeric" required="true"/>
		<cfset var newBatch = '' />
		<cfset var updateBatch = '' />
		<cfset var dataout = {} />
		<cfset var XMLCONTROLSTRING = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.BatchId = -1 />

		<cfset XMLCONTROLSTRING = "<RXSSCSC>
			<Q AF='NOFORMAT' BOFNQ='0' GID='1' ID='38' IENQID='0' IHOUR='0' IMIN='0' IMRNR='2' INOON='01' INRMO='END' ITYPE='MINUTES' IVALUE='0' MDF='0' REQANS='undefined' RQ='1' SWT='1' TDESC='Survey Question' TEXT='{%inpText%}' TYPE='SHORTANSWER' errMsgTxt='undefined'>
			</Q>
		</RXSSCSC>"/>

		<cftry>
			<cfinvoke component="session.cfc.distribution" method="AddNewBatch" returnvariable="newBatch">
			</cfinvoke>

			<cfif newBatch.RXRESULTCODE GT 0>
				<cfquery datasource="#session.DBSourceEBM#" result="updateBatch">
					UPDATE
						simpleobjects.batch
					SET
						EMS_Flag_int = 10,
						XMLControlString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#XMLCONTROLSTRING#" />
					WHERE
						BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#newBatch.NEXTBATCHID#" />
				</cfquery>

				<cfif updateBatch.RECORDCOUNT GT 0>
					<cfset dataout.RXRESULTCODE = 1 />
					<cfset dataout.BatchId = newBatch.NEXTBATCHID />
				</cfif>

				<cfquery datasource="#session.DBSourceEBM#">
					UPDATE
						simpleobjects.waitlist
					SET
						MessageBatchId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#newBatch.NEXTBATCHID#" />
					WHERE
						UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
					AND
						ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
					AND
						Active_ti = 1
				</cfquery>
			<cfelse>
				<cfset dataout.MESSAGE = 'Create batch failed!' />
			</cfif>

			<cfcatch type="any">
				<cfset dataout.TYPE = cfcatch.type />
				<cfset dataout.MESSAGE = cfcatch.message />
				<cfset dataout.ERRMESSAGE = cfcatch.detail />
			</cfcatch>
		</cftry>

		<cfreturn dataout>
	</cffunction>

	<cffunction name="CreateDefaultWaitlistAppSetting" access="remote" hint="Create default Waitlist App setting for Sire's user when they first view Waitlist Page">
		<cfargument name="inpListId" required="true" type="numeric"/>

		<cfset var defaultConfirmText = 
'Hi [name]! You’re waitlisted to [company] as ##[order] in line.
Please go to [link] for your waitlist’s details. Click on “Cancel Visit” if you can’t make it.'>

		<cfset var defaultAlertText = 
'Hi [name]! It’s your turn at [company]. Please reply within 5 min. In case you can’t make it, go to [link] to cancel.'>

		<cfset var defaultConfirmEmailSub = 
'You have been waitlisted to [company] on [date].'>

		<cfset var defaultConfirmEmailMess = 
'Hi [name]!
You’ve been waitlisted to [company] on [date] as ##[order] in line. Please go to [link] to view your waitlist’s details and cancel visit if you can’t make it.
We look forward to serve you soon!
Sincerely,
[company]'>

		<cfset var defaultAlertEmailSub = 'Great! It’s your turn at [company].'>

		<cfset var defaultAlertEmailMess = 
'Hi [name]! 
You’re waitlisted to [company] as ##[order] in line.
Please go to [link] for your waitlist’s details. Click on “Cancel Visit” if you can’t make it.
Sincerely, 
[company]'>

		<cfset var defaultNoticeEmailSub = 'Your waiting time at [company] has changed.'>

		<cfset var defaultNoticeEmailMess = 
'Hi [name]!
Your waiting time at [company] remain [quote]. Please go to [link] for more details.
We look forward to serve you soon!
Sincerely,
[company]' />

		<cfset var defaultNoticeText = 
'Hi [name]! Your waiting time at [company] remain [quote]. Go to [link] for more details.'/>

		<cfset var defaultMaxPartySize = 30>
		<cfset var defaultQuoteTime = 30>
		<cfset var defaultAlertTextSta = 1>
		<cfset var defaultAlertEmailSta = 1>
		<cfset var defaultConfirmTextSta = 1>
		<cfset var defaultConfirmEmailSta = 1>
		<cfset var defaultQuoteWaitSta = 1>
		<cfset var defaultAddServiceSta = 0>
		<cfset var defaultNoticeEmailSta = 1 />
		<cfset var defaultNoticeTextSta = 1 />


		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>

		<cfset var checkUserWaitlistAppSetting = '' />
		<cfset var createDefaultWaitlistSetting = '' />
		<cfset var createDefaultWaitlistSettingResult = '' />

		<cftry>
        	<cfquery name="checkUserWaitlistAppSetting" datasource="#Session.DBSourceREAD#">
        		SELECT
        			UserId_int
        		FROM
        			simpleobjects.waitlistsetting
        		WHERE
        			UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
        		AND
        			ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
        	</cfquery>
        	<cfif checkUserWaitlistAppSetting.Recordcount GT 0>
        		<cfset dataout.RXRESULTCODE = 2>
        		<cfset dataout.MESSAGE = "Waitlist Setting for this user was created before">
        	<cfelse>
        		<cfquery name="createDefaultWaitlistSetting" datasource="#Session.DBSourceEBM#" result="createDefaultWaitlistSettingResult">
        			INSERT INTO
        				simpleobjects.waitlistsetting
        				(
        					UserId_int,
        					MaxSize_int,
        					ConfirmText_vch,
        					AlertText_vch,
        					ConfirmEmailSubject_vch,
        					ConfirmEmailMessage_vch,
        					AlertEmailSubject_vch,
        					AlertEmailMessage_vch,
        					QuoteTime_int,
        					AlertTextStatus_int,
        					ConfirmTextStatus_int,
        					ConfirmEmailStatus_int,
        					AlertEmailStatus_int,
        					QuoteWaitStatus_int,
        					AddServiceStatus_int,
        					ListId_bi,
        					NoticeEmailStatus_int,
        					NoticeTextStatus_int,
        					NoticeEmailSubject_vch,
        					NoticeEmailMessage_vch,
        					NoticeText_vch
        				)
        			VALUES
        				(
        					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
        					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#defaultMaxPartySize#">,
        					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#defaultConfirmText#">,
        					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#defaultAlertText#">,
        					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#defaultConfirmEmailSub#">,
        					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#defaultConfirmEmailMess#">,
        					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#defaultAlertEmailSub#">,
        					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#defaultAlertEmailMess#">,
        					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#defaultQuoteTime#">,
        					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#defaultAlertTextSta#">,
        					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#defaultAlertEmailSta#">,
        					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#defaultConfirmTextSta#">,
        					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#defaultConfirmEmailSta#">,
        					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#defaultQuoteWaitSta#">,
        					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#defaultAddServiceSta#">,
        					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>,
        					<cfqueryparam cfsqltype="cf_sql_integer" value="#defaultNoticeEmailSta#"/>,
        					<cfqueryparam cfsqltype="cf_sql_integer" value="#defaultNoticeTextSta#"/>,
        					<cfqueryparam cfsqltype="cf_sql_varchar" value="#defaultNoticeEmailSub#"/>,
        					<cfqueryparam cfsqltype="cf_sql_varchar" value="#defaultNoticeEmailMess#"/>,
        					<cfqueryparam cfsqltype="cf_sql_varchar" value="#defaultNoticeText#"/>
        				)
        		</cfquery>

        		<cfset dataout.RXRESULTCODE = 1 >
        		<cfset dataout.MESSAGE = "Create Default Waitlist Setting successfully">
        	</cfif>
			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout>
	</cffunction>

	<cffunction name="UpdateWaitlistAppSetting" access="remote" hint="update Sire's user waitlist app setting">
		<cfargument name="inpListId" required="true" type="numeric"/>
		
		<cfargument name="inpMaxPartySize" required="false">
		<cfargument name="inpConfirmationText" required="false">
		<cfargument name="inpAlertText" required="false">
		<cfargument name="inpConfirmationEmailSubject" required="false">
		<cfargument name="inpConfirmationEmailMessage" required="false">
		<cfargument name="inpAlertEmailSubject" required="false">
		<cfargument name="inpAlertEmailMessage" required="false">
		<cfargument name="inpQuoteWait" required="false">
		<cfargument name="inpConfirmationTextStatus" required="false">
		<cfargument name="inpAlertTextStatus" required="false">
		<cfargument name="inpConfirmationEmailStatus" required="false">
		<cfargument name="inpAlertEmailStatus" required="false">
		<cfargument name="inpQuoteWaitStatus" required="false">
		<cfargument name="inpAddServiceStatus" required="false">
		<cfargument name="inpNoticeText" required="false">
		<cfargument name="inpNoticeEmailSubject" required="false">
		<cfargument name="inpNoticeEmailMessage" required="false">
		<cfargument name="inpNoticeEmailStatus" required="false">
		<cfargument name="inpNoticeTextStatus" required="false">

		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset var inputContent = ["#arguments.inpConfirmationText#", "#arguments.inpAlertText#", "#arguments.inpNoticeText#", "#arguments.inpConfirmationEmailSubject#", "#arguments.inpConfirmationEmailMessage#", "#arguments.inpAlertEmailSubject#", "#arguments.inpAlertEmailMessage#", "#arguments.inpNoticeEmailSubject#", "#arguments.inpNoticeEmailMessage#"]>

		<cfset var UpdateWaitlistSetting = '' >
		<cfset var content = '' >
		<cfset var validationResult = '' >

			<cftry>
				<cfloop array="#inputContent#" index="content">
					<cfif content NEQ ''>
						<cfinvoke method="VldContent" component="public.cfc.validation" returnvariable="validationResult">
						    <cfinvokeargument name="Input" value="#content#"/>
						</cfinvoke>

						<cfif validationResult NEQ true>
						    <cfset dataout.RXRESULTCODE = -3>
						    <cfset dataout.MESSAGE = 'Please do not input special character.'>
						    <cfreturn dataout>
						</cfif>
					</cfif>
				</cfloop>

				<cfquery name="UpdateWaitlistSetting" datasource="#Session.DBSourceEBM#">
					UPDATE 
						simpleobjects.waitlistsetting
					<cfif arguments.inpMaxPartySize GT 0>
						SET
							MaxSize_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpMaxPartySize#">
					</cfif>

					<cfif arguments.inpConfirmationText NEQ ''>
						SET
							ConfirmText_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpConfirmationText#">
					</cfif>

					<cfif arguments.inpAlertText NEQ '' >
						SET
							AlertText_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpAlertText#">
					</cfif>

					<cfif arguments.inpNoticeText NEQ '' >
						SET
							NoticeText_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpNoticeText#">
					</cfif>

					<cfif arguments.inpConfirmationEmailSubject NEQ '' && arguments.inpConfirmationEmailMessage NEQ ''>
						SET
							ConfirmEmailSubject_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpConfirmationEmailSubject#">,
							ConfirmEmailMessage_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpConfirmationEmailMessage#">
					</cfif>

					<cfif arguments.inpAlertEmailSubject NEQ '' && arguments.inpAlertEmailMessage NEQ ''>
						SET
							AlertEmailSubject_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpAlertEmailSubject#">,
							AlertEmailMessage_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpAlertEmailMessage#">
					</cfif>

					<cfif arguments.inpNoticeEmailSubject NEQ '' && arguments.inpNoticeEmailMessage NEQ ''>
						SET
							NoticeEmailSubject_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpNoticeEmailSubject#">,
							NoticeEmailMessage_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpNoticeEmailMessage#">
					</cfif>

					<cfif arguments.inpQuoteWait NEQ ''>
						SET
							QuoteTime_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpQuoteWait#">
					</cfif>

					<cfif arguments.inpConfirmationTextStatus NEQ ''>
						SET
							ConfirmTextStatus_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpConfirmationTextStatus#">
					</cfif>

					<cfif arguments.inpAlertTextStatus NEQ ''>
						SET
							AlertTextStatus_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpAlertTextStatus#">
					</cfif>

					<cfif arguments.inpNoticeTextStatus NEQ ''>
						SET
							NoticeTextStatus_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpNoticeTextStatus#">
					</cfif>

					<cfif arguments.inpConfirmationEmailStatus NEQ ''>
						SET
							ConfirmEmailStatus_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpConfirmationEmailStatus#">
					</cfif>

					<cfif arguments.inpAlertEmailStatus NEQ ''>
						SET
							AlertEmailStatus_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpAlertEmailStatus#">
					</cfif>

					<cfif arguments.inpNoticeEmailStatus NEQ ''>
						SET
							NoticeEmailStatus_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpNoticeEmailStatus#">
					</cfif>

					<cfif arguments.inpQuoteWaitStatus NEQ ''>
						SET
							QuoteWaitStatus_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpQuoteWaitStatus#">
					</cfif>

					<cfif arguments.inpAddServiceStatus NEQ ''>
						SET
							AddServiceStatus_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpAddServiceStatus#">
					</cfif>

					WHERE
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					AND
						ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
				</cfquery>

				<cfset dataout.RXRESULTCODE = 1>
				<cfset dataout.MESSAGE = 'Update Successfully'>
				<cfcatch type="any">
					<cfset dataout.RXRESULTCODE = -1 />
		            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
				</cfcatch>
			</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="GetWaitListBatch" access="remote" output="false" hint="Get wait list">
		<cfargument name="inpListId" type="numeric" required="true"/>
		<cfset var dataout = {} />
		<cfset var batchQuery = '' />
		<cfset var batchId = 0 />
		<cfset dataout.TYPE = '' />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.BatchId = -1 />

		<cftry>
			<cfquery datasource="#session.DBSourceREAD#" name="batchQuery">
				SELECT
					MessageBatchId_int
				FROM
					simpleobjects.waitlist
				WHERE
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#" />
				AND
					ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
				AND
					Active_ti = 1
				LIMIT
					1
			</cfquery>

			<cfif batchQuery.MessageBatchId_int GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.BatchId = batchQuery.MessageBatchId_int />
			<cfelse>
				<cfinvoke component="session.sire.models.cfc.waitlistapp" method="CreateWaitListBatch" returnvariable="batchId">
					<cfinvokeargument name="inpListId" value="#arguments.inpListId#"/>
				</cfinvoke>
				<cfif batchId.RXRESULTCODE GT 0>
					<cfset dataout.RXRESULTCODE = 1 />
					<cfset dataout.BatchId = batchId.BatchId />
				<cfelse>
					<cfthrow type="Application" message="Create batch failed!"/>
				</cfif>
			</cfif>

			<cfcatch type="any">
				<cfset dataout.TYPE = cfcatch.type />
				<cfset dataout.MESSAGE = cfcatch.message />
				<cfset dataout.ERRMESSAGE = cfcatch.detail />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="GetUserWaitlistAppSetting" access="remote" hint="get Sire's user waitlist app setting">
		<cfargument name="inpListId" type="numeric" required="true"/>

		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>

		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.MAXSIZE = "" />
		<cfset dataout.CONFIRMTEXT = "" />
		<cfset dataout.ALERTTEXT = "" />
		<cfset dataout.CONFIRMEMAILSUB = "" />
		<cfset dataout.CONFIRMEMAILMESS = "" />
		<cfset dataout.QUOTETIME = "" />
		<cfset dataout.ALERTEMAILSUB = "" />
		<cfset dataout.ALERTEMAILMESS = "" />
		<cfset dataout.CONFIRMTEXTSTA = "" />
		<cfset dataout.ALERTTEXTSTA = "" />
		<cfset dataout.CONFIRMEMAILSTA = "" />
		<cfset dataout.ALERTEMAILSTA = "" />
		<cfset dataout.QUOTEWAITSTA = "" />
		<cfset dataout.ADDSERVICESTA = "" />
		<cfset dataout.NOTICETEXTSTA = "" />
		<cfset dataout.NOTICEEMAILSTA = "" />
		<cfset dataout.NOTICEEMAILSUB = "" />
		<cfset dataout.NOTICEEMAILMESS = "" />
		<cfset dataout.NOTICETEXT = "" />

		<cfset var GetWaitlistSetting = '' >
			<cftry>
				<cfquery name="GetWaitlistSetting" datasource="#Session.DBSourceREAD#">
					SELECT
						MaxSize_int,
						ConfirmText_vch,
						AlertText_vch,
						ConfirmEmailSubject_vch,
						ConfirmEmailMessage_vch,
						QuoteTime_int,
						AlertEmailSubject_vch,
						AlertEmailMessage_vch,
						ConfirmTextStatus_int,
						AlertTextStatus_int,
						ConfirmEmailStatus_int,
						AlertEmailStatus_int,
						QuoteWaitStatus_int,
						AddServiceStatus_int,
						NoticeTextStatus_int,
						NoticeEmailStatus_int,
						NoticeEmailSubject_vch,
						NoticeEmailMessage_vch,
						NoticeText_vch
					FROM
						simpleobjects.waitlistsetting
					WHERE
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					AND
						ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
				</cfquery>
				<cfset dataout.MAXSIZE = "#GetWaitlistSetting.MaxSize_int#" />
				<cfset dataout.CONFIRMTEXT = "#GetWaitlistSetting.ConfirmText_vch#" />
				<cfset dataout.ALERTTEXT = "#GetWaitlistSetting.AlertText_vch#" />
				<cfset dataout.CONFIRMEMAILSUB = "#GetWaitlistSetting.ConfirmEmailSubject_vch#" />
				<cfset dataout.CONFIRMEMAILMESS = "#GetWaitlistSetting.ConfirmEmailMessage_vch#" />
				<cfset dataout.QUOTETIME = "#GetWaitlistSetting.QuoteTime_int#" />
				<cfset dataout.ALERTEMAILSUB = "#GetWaitlistSetting.AlertEmailSubject_vch#" />
				<cfset dataout.ALERTEMAILMESS = "#GetWaitlistSetting.AlertEmailMessage_vch#" />
				<cfset dataout.CONFIRMTEXTSTA = "#GetWaitlistSetting.ConfirmTextStatus_int#" />
				<cfset dataout.ALERTTEXTSTA = "#GetWaitlistSetting.AlertTextStatus_int#" />
				<cfset dataout.CONFIRMEMAILSTA = "#GetWaitlistSetting.ConfirmEmailStatus_int#" />
				<cfset dataout.ALERTEMAILSTA = "#GetWaitlistSetting.AlertEmailStatus_int#" />
				<cfset dataout.QUOTEWAITSTA = "#GetWaitlistSetting.QuoteWaitStatus_int#" />
				<cfset dataout.ADDSERVICESTA = "#GetWaitlistSetting.AddServiceStatus_int#" />
				<cfset dataout.NOTICETEXTSTA = "#GetWaitlistSetting.NoticeTextStatus_int#" />
				<cfset dataout.NOTICEEMAILSTA = "#GetWaitlistSetting.NoticeEmailStatus_int#" />
				<cfset dataout.NOTICEEMAILSUB = "#GetWaitlistSetting.NoticeEmailSubject_vch#" />
				<cfset dataout.NOTICEEMAILMESS = "#GetWaitlistSetting.NoticeEmailMessage_vch#" />
				<cfset dataout.NOTICETEXT = "#GetWaitlistSetting.NoticeText_vch#" />

				<cfset dataout.RXRESULTCODE = 1>
				<cfset dataout.MESSAGE = 'Get User Waitlist Setting Successfully'>
				<cfcatch type="any">
					<cfset dataout.RXRESULTCODE = -1 />
		            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
				</cfcatch>
			</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="GetMessageByContactString" access="remote" output="false" hint="Get all message of contactring">
		<cfargument name="inpContactString" required="true" type="string" />
		<cfargument name="inpCreated" required="true" type="date" />
		<cfargument name="inpListId" required="true" type="numeric"/>

		<cfset var dataout = {} />
		<cfset var messageQuery = '' />
		<cfset var waitListBatchId = -1 />
		<cfset var checkRead = '' />

		<cfset dataout.TYPE = '' />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGES = [] />

		<cftry>
			<cfset waitListBatchId = GetWaitListBatch(arguments.inpListId) />

			<cfif waitListBatchId.RXRESULTCODE LT 1>
				<cfthrow type="Application" message="Can't get waitlist batch" />
			</cfif>

			<cfquery datasource="#session.DBSourceREAD#" name="messageQuery">
				SELECT
					IREResultsId_bi,
					IREType_int,
					IRESessionId_bi,
					ContactString_vch,
					Response_vch,
					Created_dt
				FROM
					simplexresults.ireresults
				WHERE
					Created_dt > <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpCreated#" />
				AND
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#" />
				AND
					ContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#" />
				AND
					BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#waitListBatchId.BatchId#" />
				AND
					(IREType_int = 1 OR IREType_int = 2)
				ORDER BY
					IREResultsId_bi ASC
			</cfquery>

			<cfif messageQuery.RECORDCOUNT GT 0>
				<cfloop query="messageQuery">
					<cfset var msgTemp = {} />
					<cfif IREType_int EQ 2>
						<cfset msgTemp.TYPE = 'in' />

						<cfquery datasource="#session.DBSourceREAD#" name="checkRead">
							SELECT
								IREResultsId_bi
							FROM
								simpleobjects.waitlistmessage
							WHERE
								IREResultsId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#IREResultsId_bi#"/>
						</cfquery>

						<cfif checkRead.RECORDCOUNT LT 1>
							<cfquery datasource="#session.DBSourceEBM#">
								INSERT INTO
									simpleobjects.waitlistmessage
									(
										UserId_int,
										IREResultsId_bi,
										IREType_int,
										ContactString_vch,
										Response_vch,
										Created_dt,
										ListId_bi
									)
								VALUES
									(
										<cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>,
										<cfqueryparam cfsqltype="cf_sql_integer" value="#IREResultsId_bi#"/>,
										<cfqueryparam cfsqltype="cf_sql_integer" value="#IREType_int#"/>,
										<cfqueryparam cfsqltype="cf_sql_varchar" value="#ContactString_vch#"/>,
										<cfqueryparam cfsqltype="cf_sql_varchar" value="#Response_vch#"/>,
										<cfqueryparam cfsqltype="cf_sql_timestamp" value="#Created_dt#"/>,
										<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
									)
							</cfquery>
						</cfif>

					<cfelse>
						<cfset msgTemp.TYPE = 'out' />
					</cfif>
					<cfset msgTemp.message = Response_vch />
					<cfset msgTemp.contactstring = ContactString_vch />

					<cfset arrayAppend(dataout.MESSAGES, msgTemp) />
				</cfloop>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch type="any">
				<cfset dataout.TYPE = cfcatch.type />
				<cfset dataout.MESSAGE = cfcatch.message />
				<cfset dataout.ERRMESSAGE = cfcatch.detail />
			</cfcatch>
		</cftry>

		<cfreturn serializeJSON(dataout) />
	</cffunction>

	<cffunction name="GetUserWaitlistServices" access="remote" hint="get Sire's user wailist app services">
		<cfargument name="inpListId" required="true" type="numeric"/>

		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout['services'] = ArrayNew(1)>

		<cfset var GetWaitlistServices = '' />
		<cfset var service = '' />

			<cftry>
				<cfquery name="GetWaitlistServices" datasource="#Session.DBSourceREAD#">
					SELECT
						ServiceId_int,
						ServiceName_vch,
						ServiceDescription_vch
					FROM
						simpleobjects.waitlistservice
					WHERE
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					AND
						ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
					AND
						Active_ti = 1
				</cfquery>

				<cfif GetWaitlistServices.Recordcount GT 0>
					<cfloop query="GetWaitlistServices">
						<cfset service = [
							'#ServiceId_int#',
							'#ServiceName_vch#',
							'#ServiceDescription_vch#']>

						<cfset arrayAppend(dataout['services'], service)>

						<cfset dataout.RXRESULTCODE = 1>
						<cfset dataout.MESSAGE = 'Get User Waitlist Services Successfully'>
					</cfloop>
				<cfelse>
					<cfset dataout.RXRESULTCODE = 1>
					<cfset dataout.MESSAGE = 'No service found for this user'>
				</cfif>
				<cfcatch type="any">
					<cfset dataout.RXRESULTCODE = -1 />
		            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
				</cfcatch>
			</cftry>

		<cfreturn dataout>
	</cffunction>

	<cffunction name="DeleteUserWaitlistAppService" access="remote" hint="Delete Sire's user Waitlist App Services">
		<cfargument name="inpListId" required="true" type="numeric"/>
		<cfargument name="inpServiceId" required="true">
		
		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>

		<cfset var deleteWaitlistAppService = '' >
		<cfset var deleteWaitlistAppServiceResult = '' >
		<cfset var checkServiceInUse = '' />

			<cftry>
				<cfquery name="checkServiceInUse" datasource="#session.DBSourceREAD#">
					SELECT
						WaitListId_bi
					FROM
						simpleobjects.waitlistapp
					WHERE
						Active_ti = 1
					AND
						Status_int = 1
					AND
						ServiceId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpServiceId#">
					AND
						ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
					AND
						UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
				</cfquery>

				<cfif checkServiceInUse.RECORDCOUNT GT 0>
					<cfthrow type="Application" message="Delete failed! Service is currently in use."/>
				</cfif>

            	<cfquery name="deleteWaitlistAppService" datasource="#Session.DBSourceEBM#" result="deleteWaitlistAppServiceResult">
					UPDATE
						simpleobjects.waitlistservice
					SET
						Active_ti = 0
					WHERE
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
					AND
						ServiceId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpServiceId#">
					AND
						ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
				</cfquery>


				<cfif deleteWaitlistAppServiceResult.Recordcount GT 0>
					<cfset dataout.RXRESULTCODE = 1>
					<cfset dataout.MESSAGE = 'Delete Waitlist Service Successfully'>
				<cfelse>
					<cfset dataout.RXRESULTCODE = -3>
					<cfset dataout.MESSAGE = 'Delete Waitlist Service Failed'>
				</cfif>
				<cfcatch type="any">
					<cfset dataout.RXRESULTCODE = -1 />
		            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
				</cfcatch>
			</cftry>

		<cfreturn dataout>
	</cffunction>

	<cffunction name="CreateNewWaitlistAppService" access="remote" hint="Add new Waitlist App Services for Sire's user">
		<cfargument name="inpListId" required="true" type="numeric"/>
		<cfargument name="inpServiceName" required="true">
		<cfargument name="inpServiceDescription" required="false" default="">

		<cfset var createWaitlistAppService = '' />
		<cfset var createWaitlistAppServiceResult = '' />
		<cfset var checkDuplicateServiceName = '' />
		<cfset var validationResult = '' />

		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>

		<cftry>
        	<cfif arguments.inpServiceName NEQ ''>
        		<cfinvoke method="VldContent" component="public.cfc.validation" returnvariable="validationResult">
				    <cfinvokeargument name="Input" value="#arguments.inpServiceName#"/>
				</cfinvoke>

				<cfif validationResult NEQ true>
				    <cfset dataout.RXRESULTCODE = -3>
				    <cfset dataout.MESSAGE = 'Please do not input special character.'>
				    <cfreturn dataout>
				</cfif>

				<cfif arguments.inpServiceDescription NEQ ''>
					<cfinvoke method="VldContent" component="public.cfc.validation" returnvariable="validationResult">
					    <cfinvokeargument name="Input" value="#arguments.inpServiceDescription#"/>
					</cfinvoke>

					<cfif validationResult NEQ true>
					    <cfset dataout.RXRESULTCODE = -3>
					    <cfset dataout.MESSAGE = 'Please do not input special character.'>
					    <cfreturn dataout>
					</cfif>
				</cfif>
				
        		<cfquery name="checkDuplicateServiceName" datasource="#session.DBSourceREAD#">
        			SELECT
        				ServiceId_int
        			FROM
        				simpleobjects.waitlistservice
        			WHERE
        				UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
        			AND
        				ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
        			AND
        				ServiceName_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpServiceName#"/>
        		</cfquery>

        		<cfif checkDuplicateServiceName.RECORDCOUNT GT 0>
        			<cfthrow type="Application" message="Service name is already in use! Please choose another!"/>
        		</cfif>

        		<cfquery name="createWaitlistAppService" datasource="#Session.DBSourceEBM#" result="createWaitlistAppServiceResult">
					INSERT INTO
						simpleobjects.waitlistservice 
						(
							ServiceName_vch,
							ServiceDescription_vch,
							UserId_int,
							ListId_bi
						)

					VALUES
						(
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpServiceName#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpServiceDescription#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
							<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
						)
				</cfquery>
				<cfif createWaitlistAppServiceResult.Recordcount GT 0>
					<cfset dataout.RXRESULTCODE = 1>
					<cfset dataout.MESSAGE = 'Add new Waitlist Service Successfully'>
				<cfelse>
					<cfset dataout.RXRESULTCODE = -4>
					<cfset dataout.MESSAGE = 'Add new Waitlist Service Failed'>
				</cfif>
        	<cfelse>
        		<cfset dataout.RXRESULTCODE = -3>
				<cfset dataout.MESSAGE = 'Invalid content!'>
        	</cfif>
			<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -1 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction>

	<cffunction name="GetLastOrderByUserId" access="public" hint="Get last customer order by userId">
		<cfargument name="inpListId" required="true" type="numeric"/>
		<cfset var dataout = {} />
		<cfset var dataout.MESSAGE = '' />
		<cfset var dataout.ERRMESSAGE = '' />
		<cfset var dataout.LASTORDER = '' />

		<cfset var getLastOrder = '' />

		<cftry>
			<cfquery name="getLastOrder" datasource="#Session.DBSourceREAD#">
				SELECT
					Order_bi
				FROM
					simpleobjects.waitlistapp
				WHERE
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				AND
					ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
				AND
					Active_ti = 1
				AND
					Status_int = 1
				ORDER BY
					Order_bi
				DESC
				LIMIT 1
			</cfquery>
			<cfif getLastOrder.Recordcount GT 0>
				<cfset dataout.LASTORDER = getLastOrder.Order_bi>
				<cfset dataout.MESSAGE = "Get Last Order successfully">
				<cfset dataout.RXRESULTCODE = 1>
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0>
				<cfset dataout.MESSAGE = "No record found">
			</cfif>
			<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -1 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="UpdateHigherOrder" access="public" hint="Update all the higher order than the input order number">
		<cfargument name="inpCustomerOrder" required="true">
		<cfargument name="inpWaitlistId" required="true">
		<cfargument name="inpCurrentCustomerOrder" required="true">
		<cfargument name="inpListId" required="true" type="numeric"/>

 		<cfset var dataout = {} />
 		<cfset var updateHigherOrder = '' />
 		<cfset var updateHigherOrderResult = '' />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />

		<cftry>
			<cfquery name="updateHigherOrder" datasource="#Session.DBSourceEBM#" result="updateHigherOrderResult">
				UPDATE
					simpleobjects.waitlistapp
				SET
					Order_bi = Order_bi + 1
				WHERE
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				AND
					ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
				AND 
					Status_int = 1
				AND
					Active_ti = 1
				AND 
					Order_bi >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCustomerOrder#">
				AND 
					WaitListId_bi != <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpWaitlistId#">
				AND 
					Order_bi < <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCurrentCustomerOrder#">
			</cfquery>
			<cfif updateHigherOrderResult.Recordcount GT 0>
				<cfset dataout.RXRESULTCODE = 1>
				<cfset dataout.MESSAGE = "Update order successfully">
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0>
				<cfset dataout.MESSAGE = "Update order failed">
			</cfif>
			<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -1 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="UpdateLowerOrder" access="public" hint="Update all the lower order than the input order number">
		<cfargument name="inpWaitlistId" required="true">
		<cfargument name="inpCurrentCustomerOrder" required="true">
		<cfargument name="inpCustomerOrder" required="true">
		<cfargument name="inpListId" required="true" type="numeric"/>
 		<cfset var dataout = {} />
 		<cfset var updateHigherOrder = '' />
 		<cfset var updateHigherOrderResult = '' />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />

		<cftry>
			<cfquery name="updateHigherOrder" datasource="#Session.DBSourceEBM#" result="updateHigherOrderResult">
				UPDATE
					simpleobjects.waitlistapp
				SET
					Order_bi = Order_bi - 1
				WHERE
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				AND
					ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
				AND 
					Status_int = 1
				AND
					Active_ti = 1
				AND 
					WaitListId_bi != <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpWaitlistId#">
				AND
					Order_bi > <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCurrentCustomerOrder#">
				AND 
					Order_bi <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCustomerOrder#">
			</cfquery>
			<cfif updateHigherOrderResult.Recordcount GT 0>
				<cfset dataout.RXRESULTCODE = 1>
				<cfset dataout.MESSAGE = "Update order successfully">
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0>
				<cfset dataout.MESSAGE = "Update order failed">
			</cfif>
			<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -1 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="CreateNewWaitlist" access="remote" hint="Add new Waitlist for Sire's user">
		<cfargument name="inpServiceId" required="false" default="0">
		<cfargument name="inpStatus" required="false" default="1">
		<cfargument name="inpCustomerName" required="false">
		<cfargument name="inpCustomerEmail" required="false">
		<cfargument name="inpCustomerContactString" required="false">
		<cfargument name="inpWaitlistSize" required="false" default="0">
		<cfargument name="inpWaitTime" required="false" default="0">
		<cfargument name="inpNote" required="false">
		<cfargument name="inpCustomerOrder" required="false" default="0">
		<cfargument name="inpListId" required="true" type="numeric"/>

		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = ""/>
		<cfset dataout.ERRMESSAGE = ""/>

		<cfset var addToWaitlistResult = '' />

		<cfset arguments.inpCustomerContactString = reReplaceNoCase(arguments.inpCustomerContactString,"[^[:digit:]]", "", "ALL") />

		<cftry>
	    	<cfif arguments.inpCustomerName EQ '' && arguments.inpCustomerContactString EQ ''>
	    		<cfset dataout.RXRESULTCODE = -3>
				<cfset dataout.MESSAGE = 'Invalid content! Customer Name or Customer Phone is required!'>
			<cfelseif arguments.inpCustomerContactString NEQ '' && arguments.inpCustomerEmail EQ ''>
	            <cfinvoke component="session.sire.models.cfc.waitlistapp" method="AddToWaitlist" returnvariable="addToWaitlistResult">
        			<cfinvokeargument name="inpCustomerContactString" value="#arguments.inpCustomerContactString#">
        			<cfinvokeargument name="inpServiceId" value="#arguments.inpServiceId#">
        			<cfinvokeargument name="inpListId" value="#arguments.inpListId#">
        			<cfinvokeargument name="inpCustomerOrder" value="#arguments.inpCustomerOrder#">
        			<cfinvokeargument name="inpCustomerName" value="#arguments.inpCustomerName#">
        			<cfinvokeargument name="inpCustomerEmail" value="#arguments.inpCustomerEmail#">
        			<cfinvokeargument name="inpNote" value="#arguments.inpNote#">
        			<cfinvokeargument name="inpWaitlistSize" value="#arguments.inpWaitlistSize#">
        			<cfinvokeargument name="inpWaitTime" value="#arguments.inpWaitTime#">
		        </cfinvoke>

		        <cfset dataout.RXRESULTCODE = "#addToWaitlistResult.RXRESULTCODE#"/>
		        <cfset dataout.MESSAGE = "#addToWaitlistResult.MESSAGE#"/>
		        <cfset dataout.ERRMESSAGE = "#addToWaitlistResult.ERRMESSAGE#"/>
		        <cfreturn dataout />
	        <cfelseif arguments.inpCustomerEmail NEQ '' && arguments.inpCustomerContactString EQ ''>
	        	<cfinvoke component="session.sire.models.cfc.waitlistapp" method="AddToWaitlist" returnvariable="addToWaitlistResult">
        			<cfinvokeargument name="inpCustomerContactString" value="#arguments.inpCustomerContactString#">
        			<cfinvokeargument name="inpServiceId" value="#arguments.inpServiceId#">
        			<cfinvokeargument name="inpListId" value="#arguments.inpListId#">
        			<cfinvokeargument name="inpCustomerOrder" value="#arguments.inpCustomerOrder#">
        			<cfinvokeargument name="inpCustomerName" value="#arguments.inpCustomerName#">
        			<cfinvokeargument name="inpCustomerEmail" value="#arguments.inpCustomerEmail#">
        			<cfinvokeargument name="inpNote" value="#arguments.inpNote#">
        			<cfinvokeargument name="inpWaitlistSize" value="#arguments.inpWaitlistSize#">
        			<cfinvokeargument name="inpWaitTime" value="#arguments.inpWaitTime#">
		        </cfinvoke>

		        <cfset dataout.RXRESULTCODE = "#addToWaitlistResult.RXRESULTCODE#"/>
		        <cfset dataout.MESSAGE = "#addToWaitlistResult.MESSAGE#"/>
		        <cfset dataout.ERRMESSAGE = "#addToWaitlistResult.ERRMESSAGE#"/>
		        <cfreturn dataout />
	        <cfelseif arguments.inpCustomerEmail EQ '' && arguments.inpCustomerContactString EQ ''>
	        	<cfinvoke component="session.sire.models.cfc.waitlistapp" method="AddToWaitlist" returnvariable="addToWaitlistResult">
        			<cfinvokeargument name="inpCustomerContactString" value="#arguments.inpCustomerContactString#">
        			<cfinvokeargument name="inpServiceId" value="#arguments.inpServiceId#">
        			<cfinvokeargument name="inpListId" value="#arguments.inpListId#">
        			<cfinvokeargument name="inpCustomerOrder" value="#arguments.inpCustomerOrder#">
        			<cfinvokeargument name="inpCustomerName" value="#arguments.inpCustomerName#">
        			<cfinvokeargument name="inpCustomerEmail" value="#arguments.inpCustomerEmail#">
        			<cfinvokeargument name="inpNote" value="#arguments.inpNote#">
        			<cfinvokeargument name="inpWaitlistSize" value="#arguments.inpWaitlistSize#">
        			<cfinvokeargument name="inpWaitTime" value="#arguments.inpWaitTime#">
		        </cfinvoke>

		        <cfset dataout.RXRESULTCODE = "#addToWaitlistResult.RXRESULTCODE#"/>
		        <cfset dataout.MESSAGE = "#addToWaitlistResult.MESSAGE#"/>
		        <cfset dataout.ERRMESSAGE = "#addToWaitlistResult.ERRMESSAGE#"/>
		        <cfreturn dataout />
	        <cfelseif arguments.inpCustomerEmail NEQ '' && arguments.inpCustomerContactString NEQ ''>
	        	<cfinvoke component="session.sire.models.cfc.waitlistapp" method="AddToWaitlist" returnvariable="addToWaitlistResult">
        			<cfinvokeargument name="inpCustomerContactString" value="#arguments.inpCustomerContactString#">
        			<cfinvokeargument name="inpServiceId" value="#arguments.inpServiceId#">
        			<cfinvokeargument name="inpListId" value="#arguments.inpListId#">
        			<cfinvokeargument name="inpCustomerOrder" value="#arguments.inpCustomerOrder#">
        			<cfinvokeargument name="inpCustomerName" value="#arguments.inpCustomerName#">
        			<cfinvokeargument name="inpCustomerEmail" value="#arguments.inpCustomerEmail#">
        			<cfinvokeargument name="inpNote" value="#arguments.inpNote#">
        			<cfinvokeargument name="inpWaitlistSize" value="#arguments.inpWaitlistSize#">
        			<cfinvokeargument name="inpWaitTime" value="#arguments.inpWaitTime#">
		        </cfinvoke>

		        <cfset dataout.RXRESULTCODE = "#addToWaitlistResult.RXRESULTCODE#"/>
		        <cfset dataout.MESSAGE = "#addToWaitlistResult.MESSAGE#"/>
		        <cfset dataout.ERRMESSAGE = "#addToWaitlistResult.ERRMESSAGE#"/>
		        <cfreturn dataout />
	        </cfif>
			<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -1 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout>

	</cffunction>

	<cffunction name="PrepareEmailContent" access="private" output="false" hint="Prepare email content from setting">
		<cfargument name="inpInfo" required="true" type="struct"/>
		<cfargument name="inpType" required="true" type="string"/>
		<cfargument name="inpListId" required="true" type="numeric"/>

		<cfset var dataout = {} />
		<cfset var userOrgInfo = '' />
		<cfset var emailSubject = '' />
		<cfset var emailMessage = '' />
		<cfset var userWaitlistAppSetting = '' />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.TYPE = '' />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.SUBJECT = '' />
		<cfset dataout.CONTENT = '' />

		<cftry>

			<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="userOrgInfo">
			</cfinvoke>

			<cfinvoke component="session.sire.models.cfc.waitlistapp" method="GetUserWaitlistAppSetting" returnvariable="userWaitlistAppSetting">
				<cfinvokeargument name="inpListId" value="#arguments.inpListId#"/>
			</cfinvoke>

			<cfif arguments.inpType EQ "confirm">
				<cfset emailSubject = userWaitlistAppSetting.CONFIRMEMAILSUB />
				<cfset emailMessage = userWaitlistAppSetting.CONFIRMEMAILMESS />
			<cfelseif arguments.inpType EQ "alert">
				<cfset emailSubject = userWaitlistAppSetting.ALERTEMAILSUB />
				<cfset emailMessage = userWaitlistAppSetting.ALERTEMAILMESS />
			<cfelseif arguments.inpType EQ "notice">
				<cfset emailSubject = userWaitlistAppSetting.NOTICEEMAILSUB />
				<cfset emailMessage = userWaitlistAppSetting.NOTICEEMAILMESS />
			<cfelse>
				<cfthrow type="Application" message="Wrong sms type!" />
			</cfif>

			<cfset emailSubject = REReplace(emailSubject, '\[name\]', '#arguments.inpInfo.CUSTOMERNAME#', 'ALL')>
			<cfset emailSubject = REReplace(emailSubject, '\[order\]', '#arguments.inpInfo.POSITION#', 'ALL')>
			<cfset emailSubject = REReplace(emailSubject, '\[quote\]', '#arguments.inpInfo.ESTIMATEDWAIT#', 'ALL')>
			<cfset emailSubject = REReplace(emailSubject, '\[date\]', '#DateFormat(NOW(), "mmm-dd-yyyy")#')>
			
			<cfif userOrgInfo.ORGINFO.ORGANIZATIONNAME_VCH NEQ ''>
				<cfset emailSubject = REReplace(emailSubject, '\[company\]', '#userOrgInfo.ORGINFO.ORGANIZATIONNAME_VCH#', 'ALL')>
			<cfelse>
				<cfset emailSubject = REReplace(emailSubject, '\[company\]', 'CompanyName', 'ALL')>
			</cfif>
			
			<cfset emailSubject = REReplace(emailSubject, '\[size\]', '#arguments.inpInfo.TOTALINLINE#', 'ALL')>
			<cfset emailSubject = HTMLEditFormat(emailSubject)>

			
			<cfset emailMessage = REReplace(emailMessage, '\[name\]', '#arguments.inpInfo.CUSTOMERNAME#', 'ALL')>
			<cfset emailMessage = REReplace(emailMessage, '\[order\]', '#arguments.inpInfo.POSITION#', 'ALL')>
			<cfset emailMessage = REReplace(emailMessage, '\[quote\]', '#TransformTime(arguments.inpInfo.ESTIMATEDWAIT)#', 'ALL')>
			<cfset emailMessage = REReplace(emailMessage, '\[date\]', '#DateFormat(NOW(), "mmm-dd-yyyy")#')>
			
			<cfif userOrgInfo.ORGINFO.ORGANIZATIONNAME_VCH NEQ ''>
				<cfset emailMessage = REReplace(emailMessage, '\[company\]', '#userOrgInfo.ORGINFO.ORGANIZATIONNAME_VCH#', 'ALL')>
			<cfelse>
				<cfset emailMessage = REReplace(emailMessage, '\[company\]', 'CompanyName', 'ALL')>
			</cfif>
			
			<cfset emailMessage = REReplace(emailMessage, '\[size\]', '#arguments.inpInfo.TOTALINLINE#', 'ALL')>
			
			<cfset emailMessage = HTMLEditFormat(emailMessage)>
			<cfset emailMessage = REReplace(emailMessage, '\n', '<br />', 'ALL')>
			<cfset emailMessage = REReplace(emailMessage, '\[link\]', '<a href="https://#CGI.SERVER_NAME#/waitlist-customer-view?token=#arguments.inpInfo.TOKEN#">link</a>', 'ALL')>


			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.SUBJECT = emailSubject />
			<cfset dataout.CONTENT = emailMessage />

			<cfcatch type="any">
				<cfset dataout.TYPE = cfcatch.type />
				<cfset dataout.MESSAGE = cfcatch.message />
				<cfset dataout.ERRMESSAGE = cfcatch.detail />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="PrepareSMSContent" access="private" output="false" hint="Prepare sms content from setting">
		<cfargument name="inpInfo" required="true" type="struct"/>
		<cfargument name="inpType" required="true" type="string"/>
		<cfargument name="inpListId" required="true" type="numeric"/>

		<cfset var dataout = {} />
		<cfset var userOrgInfo = '' />
		<cfset var smsContent = '' />
		<cfset var userWaitlistAppSetting = '' />
		<cfset var shortUrl = '' />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.TYPE = '' />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.CONTENT = structNew() />

		<cftry>

			<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="userOrgInfo">
			</cfinvoke>

			<cfinvoke component="session.sire.models.cfc.waitlistapp" method="GetUserWaitlistAppSetting" returnvariable="userWaitlistAppSetting">
				<cfinvokeargument name="inpListId" value="#arguments.inpListId#"/>
			</cfinvoke>

			<cfinvoke component="session.sire.models.cfc.waitlistapp" method="getShortUrl" returnvariable="shortUrl">
				<cfinvokeargument name="inpLongUrl" value="https://#CGI.SERVER_NAME#/waitlist-customer-view?token=#arguments.inpInfo.TOKEN#" />
			</cfinvoke>

			<cfif arguments.inpType EQ "confirm">
				<cfset smsContent = userWaitlistAppSetting.CONFIRMTEXT />
			<cfelseif arguments.inpType EQ "alert">
				<cfset smsContent = userWaitlistAppSetting.ALERTTEXT />
			<cfelseif arguments.inpType EQ "notice">
				<cfset smsContent = userWaitlistAppSetting.NOTICETEXT />
			<cfelse>
				<cfthrow type="Application" message="Wrong sms type!" />
			</cfif>
			
			<cfset smsContent = REReplace(smsContent, '\[name\]', '#arguments.inpInfo.CUSTOMERNAME#', 'ALL')>
			<cfset smsContent = REReplace(smsContent, '\[order\]', '#arguments.inpInfo.POSITION#', 'ALL')>
			<cfset smsContent = REReplace(smsContent, '\[quote\]', '#TransformTime(arguments.inpInfo.ESTIMATEDWAIT)#', 'ALL')>
			<cfset smsContent = REReplace(smsContent, '\[date\]', '#DateFormat(NOW(), "mmm-dd-yyyy")#')>
			
			<cfif userOrgInfo.ORGINFO.ORGANIZATIONNAME_VCH NEQ ''>
				<cfset smsContent = REReplace(smsContent, '\[company\]', '#userOrgInfo.ORGINFO.ORGANIZATIONNAME_VCH#', 'ALL')>
			<cfelse>
				<cfset smsContent = REReplace(smsContent, '\[company\]', 'CompanyName', 'ALL')>
			</cfif>
			
			<cfset smsContent = REReplace(smsContent, '\[size\]', '#arguments.inpInfo.TOTALINLINE#', 'ALL')>
			<cfset smsContent = REReplace(smsContent, '\[link\]', '#shortUrl.shortUrl#', 'ALL')>
			<cfset smsContent = REReplace(smsContent, '\n', '<br />', 'ALL')>

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.CONTENT = smsContent />

			<cfcatch type="any">
				<cfset dataout.TYPE = cfcatch.type />
				<cfset dataout.MESSAGE = cfcatch.message />
				<cfset dataout.ERRMESSAGE = cfcatch.detail />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

    <cffunction name="GetShortUrl" access="public" hint="Convert long url to short url by Google shorten url">
        <cfargument name="inpLongUrl" type="string">
        <cfset var dataout = {} />
        <cfset var apiUrl = "https://www.googleapis.com/urlshortener/v1/url?key="&#ServerAPIKeyShortUrl# />
        <cfset var httpResp = {} />
        <cfset var dataResp = {} />
        <cfset var dataPost = "{longUrl: '"&#arguments.inpLongUrl#&"'}" />
       
        <!--- SET DEFAULT VALUES RETURN --->
        <cfset dataout.RXResultCode = "-1" />
        <cfset dataout.Type = ""/>
        <cfset dataout.Message = "" />
        <cfset dataout.ErrMessage = "" />
        <cfset dataout.shortUrl = "" />

        <cftry>

            <cfhttp url="#apiUrl#" charset="utf-8" method="POST" result="httpResp" timeout="120">
                <cfhttpparam type="header" name="Content-Type" value="application/json" />
                <cfhttpparam type="body" value="#dataPost#">
            </cfhttp>

            <cfset dataResp = DeserializeJSON(httpResp.filecontent) />
            <cfset dataout.shortUrl = dataResp.id />
            <cfset dataout.RXResultCode = "1" />
            <cfcatch>
                <cfset dataout.Type = "#cfcatch.Type#" />
                <cfset dataout.Message = "#cfcatch.Message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataout />
    </cffunction>

	<cffunction name="GenWaitlistToken" access="public" output="false" hint="Generate an random token for waitlist">
        <cfset var result = ''/>
        <cfset var i = '' />
        <!--- Create string --->
        <cfloop index="i" from="1" to="9">
            <!--- Random character in range A-Z --->
            <cfset result=result&Chr(RandRange(97, 122))>
        </cfloop>
        <!--- Encrtypt --->
        <cfset result = toBase64(result&'_'&NOW())>
        <cfreturn result />
    </cffunction>

    <cffunction name="GetWaitingCustomer" access="public" output="false" hint="Get Customer of Sire's user waitlist information by token">
    	<cfargument name="inpCustomerToken" required="true">

    	<cfset var getCustomerInfo = '' />
    	<cfset var getCustomerList = '' />

    	<cfset var dataout = {} />
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset dataout.ESTIMATEDWAIT = ''>
    	<cfset dataout.AHEADINLINE = ''>
    	<cfset dataout.TOTALINLINE = ''>
    	<cfset dataout.POSITION = '' >
    	<cfset dataout.TOKEN = '' />
    	<cfset dataout.CUSTOMERPHONE = '' />
    	<cfset dataout.CUSTOMERNAME = '' />
    	<cfset dataout.CUSTOMEREMAIL = '' />

    	<cftry>
    		<cfquery name="getCustomerInfo" datasource="#Session.DBSourceEBM#">
	    		SELECT
		    		CustomerEmail_vch, ServiceId_int, WaitTime_int, Created_dt, Token_vch, CustomerName_vch, CustomerContactString_vch, ListId_bi
		    	FROM
		    		simpleobjects.waitlistapp
		    	WHERE
		    		Token_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCustomerToken#">
		    	AND
		    		Active_ti = 1
		    	AND
		    		Status_int = 1
		    	LIMIT
		    	 	1
	    	</cfquery>

	    	<cfif getCustomerInfo.Recordcount GT 0>
	    		<cfset var customerWaitPosition = 1/>
	    		<cfquery name="getCustomerList" datasource="#Session.DBSourceEBM#">
			    	SELECT
			    		CustomerEmail_vch, Token_vch, WaitTime_int, Created_dt
			    	FROM
			    		simpleobjects.waitlistapp
			    	WHERE
			    		ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#getCustomerInfo.ListId_bi#"/>
			    	AND
			    		Status_int = 1
			    	ORDER BY
			    		Order_bi
			    	ASC
		    	</cfquery>

		    	<cfif getCustomerList.Recordcount EQ 0>
		    		<cfset dataout.POSITION = 1>
		    		<cfset dataout.AHEADINLINE = 0>
		    		<cfset dataout.TOTALINLINE = 1>
		    		<cfset dataout.RXRESULTCODE = 1>
		    		<cfset dataout.ESTIMATEDWAIT = getCustomerInfo.WaitTime_int - (dateDiff("n", getCustomerInfo.Created_dt, NOW()))>
		    		<cfset dataout.MESSAGE = 'You are the only one in this service'>
		    		<cfset dataout.TOKEN = getCustomerInfo.Token_vch />
		    		<cfset dataout.CUSTOMERNAME = getCustomerInfo.CustomerName_vch />
		    		<cfset dataout.CUSTOMERPHONE = FormatPhoneNumber(getCustomerInfo.CustomerContactString_vch) />
		    		<cfset dataout.CUSTOMEREMAIL = getCustomerInfo.CustomerEmail_vch />
		    	<cfelse>
		    		<cfloop query="getCustomerList">
		    			<cfif arguments.inpCustomerToken NEQ getCustomerList.Token_vch>
		    				<cfset customerWaitPosition = customerWaitPosition + 1>
		    			<cfelse>
		    				<cfbreak>
		    			</cfif> 
		    		</cfloop>
		    		<cfset dataout.POSITION = customerWaitPosition>
		    		<cfset dataout.AHEADINLINE = customerWaitPosition-1>
		    		<cfset dataout.TOTALINLINE = getCustomerList.Recordcount>
		    		<cfset dataout.RXRESULTCODE = 2>
		    		<cfset dataout.ESTIMATEDWAIT = getCustomerInfo.WaitTime_int - (dateDiff("n", getCustomerInfo.Created_dt, NOW()))>
		    		<cfset dataout.MESSAGE = 'Get waitlist information successfully'>
		    		<cfset dataout.TOKEN = getCustomerInfo.Token_vch />
		    		<cfset dataout.CUSTOMERNAME = getCustomerInfo.CustomerName_vch />
		    		<cfset dataout.CUSTOMERPHONE = FormatPhoneNumber(getCustomerInfo.CustomerContactString_vch) />
		    		<cfset dataout.CUSTOMEREMAIL = getCustomerInfo.CustomerEmail_vch />
		    	</cfif>
		    <cfelse>
		    	<cfset dataout.RXRESULTCODE = -1>
		    	<cfset dataout.MESSAGE = 'Invalid waitlist'>
	    	</cfif>
	    	<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -2 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
    	</cftry>

    	<cfreturn dataout>
    </cffunction>

    <cffunction name="GetWaitingCustomerById" access="remote" output="false" hint="Get Customer of Sire's user waitlist information by waitlist Id">
    	<cfargument name="inpWaitListId" required="true">
    	<cfargument name="inpListId" required="true" type="numeric"/>

    	<cfset var getCustomerInfo = '' />
    	<cfset var getCustomerList = '' />

    	<cfset var dataout = {} />
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset dataout.ESTIMATEDWAIT = ''>
    	<cfset dataout.AHEADINLINE = ''>
    	<cfset dataout.TOTALINLINE = ''>
    	<cfset dataout.POSITION = '' >
    	<cfset dataout.TOKEN = '' />
    	<cfset dataout.CUSTOMERNAME = '' />
    	<cfset dataout.CUSTOMEREMAIL = '' />
    	<cfset dataout.CUSTOMERPHONE = '' />

    	<cftry>
    		<cfquery name="getCustomerInfo" datasource="#Session.DBSourceREAD#">
	    		SELECT
		    		CustomerEmail_vch, ServiceId_int, WaitTime_int, Token_vch, Created_dt, CustomerName_vch, CustomerContactString_vch
		    	FROM
		    		simpleobjects.waitlistapp
		    	WHERE
		    		WaitListId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpWaitListId#">
				AND
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#" />
				AND
					Active_ti = 1
		    	AND
		    		Status_int = 1
		    	LIMIT
		    	 	1
	    	</cfquery>

	    	<cfif getCustomerInfo.Recordcount GT 0>
	    		<cfset var customerWaitPosition = 1/>
	    		<cfquery name="getCustomerList" datasource="#Session.DBSourceREAD#">
			    	SELECT
			    		CustomerEmail_vch, WaitListId_bi, WaitTime_int, Created_dt
			    	FROM
			    		simpleobjects.waitlistapp
			    	WHERE
			    		UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
			    	AND
			    		ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
			    	AND
			    		Status_int = 1
			    	ORDER BY
			    		Order_bi
			    	ASC
		    	</cfquery>

		    	<cfif getCustomerList.Recordcount EQ 0>
		    		<cfset dataout.POSITION = 1>
		    		<cfset dataout.AHEADINLINE = 0>
		    		<cfset dataout.TOTALINLINE = 1>
		    		<cfset dataout.RXRESULTCODE = 1>
		    		<cfset dataout.ESTIMATEDWAIT = getCustomerInfo.WaitTime_int - (dateDiff("n", getCustomerInfo.Created_dt, NOW()))>
		    		<cfset dataout.TOKEN = getCustomerInfo.Token_vch />
		    		<cfset dataout.CUSTOMERNAME = getCustomerInfo.CustomerName_vch />
		    		<cfset dataout.CUSTOMERPHONE = FormatPhoneNumber(getCustomerInfo.CustomerContactString_vch) />
		    		<cfset dataout.CUSTOMEREMAIL = getCustomerInfo.CustomerEmail_vch />
		    		<cfset dataout.MESSAGE = 'You are the only one in this service'>
		    	<cfelse>
		    		<cfloop query="getCustomerList">
		    			<cfif arguments.inpWaitListId NEQ getCustomerList.WaitListId_bi>
		    				<cfset customerWaitPosition = customerWaitPosition + 1>
		    			<cfelse>
		    				<cfbreak>
		    			</cfif> 
		    		</cfloop>
		    		<cfset dataout.POSITION = customerWaitPosition>
		    		<cfset dataout.AHEADINLINE = customerWaitPosition-1>
		    		<cfset dataout.TOTALINLINE = getCustomerList.Recordcount>
		    		<cfset dataout.RXRESULTCODE = 2>
		    		<cfset dataout.ESTIMATEDWAIT = getCustomerInfo.WaitTime_int - (dateDiff("n", getCustomerInfo.Created_dt, NOW()))>
		    		<cfset dataout.TOKEN = getCustomerInfo.Token_vch />
		    		<cfset dataout.CUSTOMERNAME = getCustomerInfo.CustomerName_vch />
		    		<cfset dataout.CUSTOMERPHONE = FormatPhoneNumber(getCustomerInfo.CustomerContactString_vch) />
		    		<cfset dataout.CUSTOMEREMAIL = getCustomerInfo.CustomerEmail_vch />
		    		<cfset dataout.MESSAGE = 'Get waitlist information successfully'>	
		    	</cfif>
		    <cfelse>
		    	<cfset dataout.RXRESULTCODE = -1>
		    	<cfset dataout.MESSAGE = 'Invalid waitlist'>
	    	</cfif>
	    	<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -2 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
    	</cftry>
    	<cfreturn dataout>
    </cffunction>

    <cffunction name="GetWaitingCustomerByUserId" access="remote" output="false" hint="Get Waiting Customer of Sire's user waitlist information by UserId">
    	<cfargument name="inpListId" required="true" type="numeric"/>
    	<cfset var dataout = {} />
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />

    	<cfset var getWaitingCustomerByUserId = '' />

    	<cftry>
    		<cfquery name="getWaitingCustomerByUserId" datasource="#Session.DBSourceREAD#">
	    		SELECT
	    			WaitListId_bi
	    		FROM
	    			simpleobjects.waitlistapp
	    		WHERE
	    			UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
	    		AND
	    			ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
	    		AND
	    			Active_ti = 1
	    		AND 
	    			Status_int = 1
	    	</cfquery>
	    	<cfif getWaitingCustomerByUserId.Recordcount GT 0>
	    		<cfset dataout.RXRESULTCODE = 1>
	    		<cfset dataout.TOTALINLINE = getWaitingCustomerByUserId.Recordcount>
	    		<cfset dataout.MESSAGE = 'Get waiting customer successfully'>
	    	<cfelse>
	    		<cfset dataout.RXRESULTCODE = 0>
	    		<cfset dataout.TOTALINLINE = getWaitingCustomerByUserId.Recordcount>
	    		<cfset dataout.MESSAGE = 'No record found'>
	    	</cfif>
	    	<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -1 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
    	</cftry>
    	<cfreturn dataout />

	</cffunction>

	<cffunction name="LogState" access="public" hint="save state of waitlist">
		<cfargument name="inpWaitlistID" type="numeric" required="yes" default="0">
		<cfargument name="inpWaitlistStatus" type="numeric" required="yes" default="0">
		<cfset var dataout = {}>
		<cfset var rsInsertLog = "">

		<cfset dataout.RXRESULTCODE = 0 />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "##" />                
        <cfset dataout.ERRMESSAGE= "" />
		<cftry>
			<cfquery result="rsInsertLog" datasource="#Session.DBSourceEBM#">
				INSERT IGNORE INTO 
					simpleobjects.waitlistapplogs
					(
						WaitlistID_bi, 
						Status_int, 
						CreatedAt_dt
					)
				VALUES
					(
						<cfqueryparam cfsqltype="CF_SQL_BIGINT" value="#arguments.inpWaitlistID#">,
						<cfqueryparam cfsqltype="CF_SQL_BIGINT" value="#arguments.inpWaitlistStatus#">,
						NOW()
					)
			</cfquery>
			<cfset dataout.RXRESULTCODE = 1 />
			<cfcatch>
		        <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
                <cfset dataout.ERRMESSAGE= "#cfcatch.detail#" />    
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="GetAnalyticSummaryData" access="remote" hint="Get summary data for statistics">	

	</cffunction>

	<cffunction name="GetAnalyticData" access="remote" hint="Get data for statistics">
		<cfargument name="inpDateStart" type="string" required="yes">
		<cfargument name="inpDateEnd" type="string" required="yes">
		<cfargument name="inpUserID" type="numeric" required="no" default="#session.userId#">
		<cfargument name="inpListId" type="numeric" required="true"/>

		<cfset var dataout = {} />
		<cfset var GetAnalyticData = {} />
		<cfset var GetAnalyticSummaryData = "" />

		
		<cfset var curUserID = arguments.inpUserID />
		<cfset var defineItem = "" />
		<cfset var sqlDefinition = [
			{"TYPE" =  "TIMELINE", "VALUE" = "FROM_UNIXTIME(UNIX_TIMESTAMP(waitlistapplogs.CreatedAt_dt), ""%Y-%m-%d"")"},
			{"TYPE" =  "WEEK", "VALUE" = "SUBSTR(DAYNAME(waitlistapplogs.CreatedAt_dt), 1, 3)"},
			{"TYPE" =  "HOUR", "VALUE" = "FROM_UNIXTIME(UNIX_TIMESTAMP(waitlistapplogs.CreatedAt_dt), ""%H:00"")"},
		] />

		<cfset dataout["DATA"] = {} />
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "##" />                
        <cfset dataout.ERRMESSAGE= "" />

        <cfset dataout["DATA"]["SUMMARY"] = { "waitlisted": 0, "served": 0, "noshow": 0} />

		<cftry>
			
			<cfloop array="#sqlDefinition#" index="defineItem">
				<cfset dataout["DATA"]["#defineItem.TYPE#"] = ArrayNew() />
				<cfquery name="GetAnalyticData" datasource="#Session.DBSourceEBM#">
					SELECT 
						COUNT(IF(waitlistapplogs.Status_int = 1, 1, NULL)) AS WAITLISTED,
						COUNT(IF(waitlistapplogs.Status_int = 2, 1, NULL)) AS SERVED,
						COUNT(IF(waitlistapplogs.Status_int = 3, 1, NULL)) AS NOSHOW,
						#REReplace(defineItem.VALUE,"''","'","ALL")# AS CreatedAt
					FROM 
						simpleobjects.waitlistapp INNER JOIN simpleobjects.waitlistapplogs ON waitlistapp.WaitListId_bi = waitlistapplogs.WaitListId_bi
					WHERE
							waitlistapp.UserId_int = #inpUserID#
						<cfif arguments.inpDateStart NEQ "" AND  arguments.inpDateEnd NEQ "">
							AND
								waitlistapplogs.CreatedAt_dt 
							BETWEEN 
								STR_TO_DATE(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpDateStart#">,'%Y-%m-%d') 
							AND 
								ADDDATE(STR_TO_DATE(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpDateEnd#">, '%Y-%m-%d'), INTERVAL 1 DAY)
						</cfif>
						<cfif arguments.inpListId GT 0 >
							AND ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
						</cfif>
					GROUP BY
						CreatedAt
				</cfquery>

				<cfloop query="GetAnalyticData">
					<cfset var item = {"period": "#CreatedAt#", "waitlisted": #WAITLISTED#, "served": #SERVED#, "noshow": #NOSHOW#} />
					<cfset ArrayAppend(dataout["DATA"]["#defineItem.TYPE#"], item) />
				</cfloop>
			</cfloop>

			<cfquery name="GetAnalyticSummaryData" datasource="#Session.DBSourceEBM#">
				SELECT 
					COUNT(IF(waitlistapplogs.Status_int = 1, 1, NULL)) AS WAITLISTED,
					COUNT(IF(waitlistapplogs.Status_int = 2, 1, NULL)) AS SERVED,
					COUNT(IF(waitlistapplogs.Status_int = 3, 1, NULL)) AS NOSHOW
				FROM 
					simpleobjects.waitlistapp INNER JOIN simpleobjects.waitlistapplogs ON waitlistapp.WaitListId_bi = waitlistapplogs.WaitListId_bi
				WHERE
						waitlistapp.UserId_int = #inpUserID#
					AND
						waitlistapplogs.CreatedAt_dt 
					<cfif arguments.inpDateStart NEQ "" AND  arguments.inpDateEnd NEQ "">
						AND
							waitlistapplogs.CreatedAt_dt 
						BETWEEN 
							STR_TO_DATE(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpDateStart#">,'%Y-%m-%d') 
						AND 
							ADDDATE(STR_TO_DATE(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpDateEnd#">, '%Y-%m-%d'), INTERVAL 1 DAY)
					</cfif>
					<cfif arguments.inpListId GT 0 >
						AND ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
					</cfif>
			</cfquery>
			
			<cfloop query="GetAnalyticSummaryData">
				<cfset var item = { "waitlisted": #WAITLISTED#, "served": #SERVED#, "noshow": #NOSHOW#} />
				<cfset dataout["DATA"]["SUMMARY"] = item />
			</cfloop>
			<cfset dataout.RXRESULTCODE = 1 />
			<cfcatch  type="any">
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
                <cfset dataout.ERRMESSAGE= "#cfcatch.detail#" />    
			</cfcatch>
		</cftry>
		
		
		<cfreturn SerializeJSON(dataout)  />
	</cffunction>

	<cffunction name="GetCustomerList" access="remote" hint="Get Customer List">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1">
		<cfargument name="sSortDir_1" default="">
		<cfargument name="iSortCol_2" default="-1">
		<cfargument name="sSortDir_2" default="">
		<cfargument name="iSortCol_0" default="-1">
		<cfargument name="sSortDir_0" default="">
		<cfargument name="customFilter" default="">
		<cfargument name="inpListId" required="true" type="numeric"/>

		<cfset var dataout =  {}/>
		<cfset var customerQuery = "" />
		<cfset var rsCount = '' />

		<cfset arguments.customFilter = Replace(arguments.customFilter, """", "", "ALL") />

		<cfset dataout["CUSTOMERLIST"] = ArrayNew() />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>

		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "##" />                
        <cfset dataout.ERRMESSAGE= "" />

		<cftry>

			<cfquery name="customerQuery" datasource="#Session.DBSourceEBM#">
				SELECT SQL_CALC_FOUND_ROWS
					WaitListId_bi, 
					UserId_int, 
					ServiceId_int, 
					Status_int, 
					CustomerName_vch, 
					CustomerEmail_vch, 
					CustomerContactString_vch, 
					WaitListSize_int, 
					WaitTime_int, 
					Note_vch, 
					Active_ti, 
					DATE_FORMAT(Created_dt, '%Y/%m/%d  at  %h:%i:%s %p') AS Created_dt
				FROM 
					simpleobjects.waitlistapp 
				WHERE 
					1
					<cfif arguments.customFilter NEQ "">
						AND (
								CustomerName_vch LIKE  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#arguments.customFilter#%'>
								OR
								CustomerEmail_vch LIKE  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#arguments.customFilter#%'>
								OR
								CustomerContactString_vch LIKE  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#arguments.customFilter#%'>
							)
					</cfif>
				AND
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#">
				<cfif arguments.inpListId GT 0 >
					AND ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
				</cfif>
				ORDER BY
					<cfswitch expression="#arguments.iSortCol_0#">
						<cfcase value="0">
							CustomerName_vch #arguments.sSortDir_0#
						</cfcase>
						<cfcase value="1">
							CustomerContactString_vch #arguments.sSortDir_0#
						</cfcase>
						<cfcase value="2">
							CustomerEmail_vch #arguments.sSortDir_0#
						</cfcase>
						<cfdefaultcase>
							WaitListId_bi #arguments.sSortDir_0#
						</cfdefaultcase>
					</cfswitch>
				LIMIT 
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
				OFFSET 
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 

			</cfquery>
			<cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			<cfloop query="rsCount">
				<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
				<cfset dataout["iTotalRecords"] = iTotalRecords>
			</cfloop>

			<cfloop query="customerQuery" >
				<cfset var item = [
					"#CustomerName_vch#",
					"#CustomerContactString_vch#",
					"#CustomerEmail_vch#",
					"#Created_dt#",
					"#Note_vch#",
					{
						"ID" = "#WaitListId_bi#",
						"ListSize" = "#WaitListSize_int#",
						"WaitTime" = "#WaitTime_int#",
						"Created_dt" = "#Created_dt#"
					}
				] />
				<cfset ArrayAppend(dataout["CUSTOMERLIST"], item) />
			</cfloop>
			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch type="any">
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
                <cfset dataout.ERRMESSAGE= "#cfcatch.detail#" />   
			</cfcatch>
		</cftry>
		
		<cfreturn SerializeJSON(dataout) />

	</cffunction>

	<cffunction name="GetNewMessageByContactString" access="remote" output="false" hint="Get new message incomming from contact string">
		<cfargument name="inpContactString" required="true" type="string">
		<cfargument name="inpListId" required="true" type="numeric"/>

		<cfset var dataout = {} />
		<cfset var ireQuery = '' />
		<cfset var logMessage = '' />
		<cfset var waitListBatch = '' />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.TYPE = '' />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.MESSAGES = [] />

		<cftry>
			<cfset waitListBatch = GetWaitListBatch(arguments.inpListId) />

			<cfif waitListBatch.RXRESULTCODE LT 1>
				<cfthrow type="Application" message="Can't get waitlist batch" />
			</cfif>

			<cfquery datasource="#session.DBSourceREAD#" name="ireQuery">
				SELECT
					IREResultsId_bi,
					IREType_int,
					ContactString_vch,
					Response_vch,
					Created_dt
				FROM
					simplexresults.ireresults
				WHERE
					DATE(Created_dt) = CURDATE()
				AND
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#" />
				AND
					BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#waitListBatch.BatchId#" />
				AND
					IREType_int = 2
				AND
					ContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#"/>
				AND
					IREResultsId_bi NOT IN (
							SELECT
								IREResultsId_bi
							FROM
								simpleobjects.waitlistmessage
							WHERE
								UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#" />
							AND
								DATE(Created_dt) = CURDATE()
							AND
								ContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#"/>
						)
			</cfquery>

			<cfif ireQuery.RECORDCOUNT GT 0>
				<cfloop query="ireQuery">
					<cfset arrayAppend(dataout.MESSAGES, Response_vch) />

					<cfquery datasource="#session.DBSourceEBM#">
						INSERT INTO
							simpleobjects.waitlistmessage
							(
								UserId_int,
								IREResultsId_bi,
								IREType_int,
								ContactString_vch,
								Response_vch,
								Created_dt,
								ListId_bi
							)
						VALUES
							(
								<cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>,
								<cfqueryparam cfsqltype="cf_sql_integer" value="#IREResultsId_bi#"/>,
								<cfqueryparam cfsqltype="cf_sql_integer" value="#IREType_int#"/>,
								<cfqueryparam cfsqltype="cf_sql_varchar" value="#ContactString_vch#"/>,
								<cfqueryparam cfsqltype="cf_sql_varchar" value="#Response_vch#"/>,
								<cfqueryparam cfsqltype="cf_sql_timestamp" value="#Created_dt#"/>,
								<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
							)
					</cfquery>
				</cfloop>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>

		<cfreturn serializeJSON(dataout) />
	</cffunction>

	<cffunction name="CountNewMessage" access="private" output="false" hint="Count new message incomming from array of contact strings">
		<cfargument name="inpContactStrings" required="true" type="array">
		<cfargument name="inpListId" required="true" type="numeric"/>

		<cfset var dataout = {} />
		<cfset var ireQuery = '' />
		<cfset var logMessage = '' />
		<cfset var waitListBatch = '' />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.TYPE = '' />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.RESULT = structNew() />

		<cftry>
			<cfset waitListBatch = GetWaitListBatch(arguments.inpListId) />

			<cfif waitListBatch.RXRESULTCODE LT 1>
				<cfthrow type="Application" message="Can't get waitlist batch" />
			</cfif>

			<cfquery datasource="#session.DBSourceREAD#" name="ireQuery">
				SELECT
					IREResultsId_bi,
					IREType_int,
					ContactString_vch,
					Response_vch,
					Created_dt
				FROM
					simplexresults.ireresults
				WHERE
					DATE(Created_dt) = CURDATE()
				AND
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#" />
				AND
					BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#waitListBatch.BatchId#" />
				AND
					IREType_int = 2
				AND
					ContactString_vch IN (#arrayToList(arguments.inpContactStrings)#)
				AND
					IREResultsId_bi NOT IN (
							SELECT
								IREResultsId_bi
							FROM
								simpleobjects.waitlistmessage
							WHERE
								UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#" />
							AND
								DATE(Created_dt) = CURDATE()
							AND
								ContactString_vch IN (#arrayToList(arguments.inpContactStrings)#)
						)
			</cfquery>

			<cfif ireQuery.RECORDCOUNT GT 0>
				<cfloop query="ireQuery">
					<cfif !structKeyExists(dataout.RESULT, '#ContactString_vch#')>
						<cfset StructInsert(dataout.RESULT, '#ContactString_vch#', {}) />
						<cfset StructInsert(dataout.RESULT["#ContactString_vch#"], 'COUNT', 0) />
					</cfif>
					<cfset dataout.RESULT["#ContactString_vch#"].COUNT += 1 />
				</cfloop>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

    <cffunction name="GetWaitlistStatusByToken" access="public" output="false" hint="Get Waitlist status by token">
    	<cfargument name="inpCustomerToken" required="true">

    	<cfset var getWaitlistCustomer = '' />
    	<cfset var dataout = {} />
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset dataout.STATUS = '' />
    	<cfset dataout.WAITTIME = '' />

		<cftry>
    		<cfquery name="getWaitlistCustomer" datasource="#Session.DBSourceREAD#">
	    		SELECT
		    		Status_int,
		    		WaitTime_int
		    	FROM
		    		simpleobjects.waitlistapp
		    	WHERE
		    		Token_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCustomerToken#">
		    	AND
		    		Active_ti = 1
		    	LIMIT
		    	 	1
	    	</cfquery>

	    	<cfif getWaitlistCustomer.Recordcount GT 0>
	    		<cfset dataout.RXRESULTCODE = 1>
	    		<cfset dataout.STATUS = getWaitlistCustomer.Status_int />
	    		<cfset dataout.WAITTIME = getWaitlistCustomer.WaitTime_int /> 
	    		<cfset dataout.MESSAGE = 'Get customer waitlist status successfully'>
		    <cfelse>
		    	<cfset dataout.RXRESULTCODE = -1>
		    	<cfset dataout.MESSAGE = 'Invalid waitlist token'>
	    	</cfif>
	    	<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -2 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
    	</cftry>

    	<cfreturn dataout>    	
   	</cffunction>

   	<cffunction name="UpdateWaitlistStatusByToken" access="remote" output="false" hint="Update Waitlist status by token when user click cancel">
   		<cfargument name="inpCustomerToken" required="true">

   		<cfset var checkWaitlistExist = '' />
   		<cfset var updateWaitlistStatus = '' />
   		<cfset var updateWaitlistStatusResult = '' />
   		<cfset var dataout = {} />
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />

    	<cftry>
    		<cfquery name="checkWaitlistExist" datasource="#Session.DBSourceREAD#">
	    		SELECT
		    		Status_int,
		    		WaitListId_bi
		    	FROM
		    		simpleobjects.waitlistapp
		    	WHERE
		    		Token_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCustomerToken#">
		    	LIMIT
		    	 	1
	    	</cfquery>

	    	<cfif checkWaitlistExist.Recordcount GT 0>
	    		<cfquery name="updateWaitlistStatus" datasource="#Session.DBSourceEBM#" result="updateWaitlistStatusResult">
		    		UPDATE
		    			simpleobjects.waitlistapp
		    		SET
		    			Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="4">
		    		WHERE
		    			Token_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCustomerToken#">
	    		</cfquery>
	    		<cfif updateWaitlistStatusResult.Recordcount GT 0>
	    			<cfset dataout.RXRESULTCODE = 1>
	    			<cfset dataout.MESSAGE = "Update waitlist status successfully">
					<cfinvoke component="session.sire.models.cfc.waitlistapp" method="LogState">
						<cfinvokeargument name="inpWaitlistID" value="#checkWaitlistExist.WaitListId_bi#">
						<cfinvokeargument name="inpWaitlistStatus" value="4">
					</cfinvoke>
	    		<cfelse>
	    			<cfset dataout.RXRESULTCODE = 0>
	    			<cfset dataout.MESSAGE = "Update waitlist status failed">
	    		</cfif>
		    <cfelse>
		    	<cfset dataout.RXRESULTCODE = -1>
		    	<cfset dataout.MESSAGE = 'Invalid waitlist token'>
	    	</cfif>
	    	<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -2 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
    	</cftry>

    	<cfreturn dataout>
   	</cffunction>

   	<cffunction name="GetWaitlistById" access="remote" output="false" hint="Get waitlist by waitlist Id">
   		<cfargument name="inpWaitlistId" required="true">

   		<cfset var getWaitlistDetail = '' />
   		<cfset var dataout = {} />
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />

   		<cfif arguments.inpWaitlistId GT 0>
   			<cftry>
   				<cfquery name="getWaitlistDetail" datasource="#Session.DBSourceREAD#">
	   				SELECT
	   					Order_bi,
	   					ServiceId_int,
	   					CustomerName_vch,
	   					CustomerEmail_vch,
	   					CustomerContactString_vch,
	   					WaitListSize_int,
	   					WaitTime_int,
	   					Note_vch,
	   					Status_int
	   				FROM
	   					simpleobjects.waitlistapp
	   				WHERE
	   					WaitListId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpWaitlistId#">
   				</cfquery>
   				<cfif getWaitlistDetail.Recordcount GT 0>
   					<cfset dataout.RXRESULTCODE = 1>
   					<cfset dataout.MESSAGE = "Get waitlist detail successfully">
   					<cfset dataout.ORDER = getWaitlistDetail.Order_bi>
   					<cfset dataout.SERVICEID = getWaitlistDetail.ServiceId_int>
   					<cfset dataout.CUSTOMERNAME = getWaitlistDetail.CustomerName_vch>
   					<cfset dataout.CUSTOMEREMAIL = getWaitlistDetail.CustomerEmail_vch>
   					<cfset dataout.CUSTOMERPHONE = FormatPhoneNumber(getWaitlistDetail.CustomerContactString_vch)>
   					<cfset dataout.CUSTOMERSIZE = getWaitlistDetail.WaitListSize_int>
   					<cfset dataout.CUSTOMERWAITTIME = getWaitlistDetail.WaitTime_int>
   					<cfset dataout.CUSTOMERNOTE = getWaitlistDetail.Note_vch>
   					<cfset dataout.STATUS = getWaitlistDetail.Status_int />
   				<cfelse>
   					<cfset dataout.RXRESULTCODE = 0>
   					<cfset dataout.MESSAGE = 'No waitlist found'>
   				</cfif>
   				<cfcatch type="any">
	   				<cfset dataout.RXRESULTCODE = -1 />
		            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
   				</cfcatch>
   			</cftry>
   		<cfelse>
   			<cfset dataout.RXRESULTCODE = -2 />
            <cfset dataout.MESSAGE = "Invalid waitlist ID" />
   		</cfif>
   		<cfreturn dataout />
   	</cffunction>

   	<cffunction name="UpdateCustomerWaitlist" access="remote" output="false" hint="Update customer waitlist">
		<cfargument name="inpWaitlistId" required="true">
		<cfargument name="inpServiceId" required="false" default="0">
		<cfargument name="inpCustomerName" required="false">
		<cfargument name="inpCustomerEmail" required="false">
		<cfargument name="inpCustomerContactString" required="false">
		<cfargument name="inpWaitlistSize" required="false" default="0">
		<cfargument name="inpWaitTime" required="false" default="0">
		<cfargument name="inpNote" required="false">
		<cfargument name="inpCustomerOrder" required="false" default="0">
		<cfargument name="inpListId" required="true" type="numeric"/>
		<!--- inpCustomerOrder = 0 means last order --->

		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />

		<cfset var updateCustomerWaitlistResult = '' />

		<cfset var waitlistQuery = '' />
		<cfset var checkTime = '' />
		<cfset var sendNoticeSMS = '' />
		<cfset var sendNoticeEmail = '' />

		<cfset arguments.inpCustomerContactString = reReplaceNoCase(arguments.inpCustomerContactString,"[^[:digit:]]", "", "ALL") />

		<cfif arguments.inpWaitlistId GT 0>
			<cftry>
				<cfquery datasource="#session.DBSourceREAD#" name="waitlistQuery">
					SELECT
						CustomerContactString_vch,
						CustomerEmail_vch,
						WaitTime_int,
						Created_dt
					FROM
						simpleobjects.waitlistapp
					WHERE
						UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
					AND
						WaitListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpWaitListId#"/>
					AND
						ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
					AND
						Active_ti = 1
					AND
						Status_int = 1
				</cfquery>

				<cfif waitlistQuery.RECORDCOUNT LT 1>
					<cfthrow type="Application" message="Waitlist item not existed!"/>
				</cfif>

				<cfif arguments.inpCustomerName EQ '' && arguments.inpCustomerContactString EQ ''>
	        		<cfset dataout.RXRESULTCODE = -3>
					<cfset dataout.MESSAGE = 'Invalid content! Customer Name or Customer Phone is required!'>
				<cfelseif arguments.inpCustomerEmail NEQ '' && arguments.inpCustomerContactString EQ ''>
		   			<cfinvoke component="session.sire.models.cfc.waitlistapp" method="UpdateCustomerWaitlistDetail" returnvariable="updateCustomerWaitlistResult">
						<cfinvokeargument name="inpWaitlistId" value="#arguments.inpWaitlistId#">
						<cfinvokeargument name="inpServiceId" value="#arguments.inpServiceId#">
						<cfinvokeargument name="inpCustomerName" value="#arguments.inpCustomerName#">
						<cfinvokeargument name="inpCustomerEmail" value="#arguments.inpCustomerEmail#">
						<cfinvokeargument name="inpCustomerContactString" value="#arguments.inpCustomerContactString#">
						<cfinvokeargument name="inpWaitlistSize" value="#arguments.inpWaitlistSize#">
						<cfinvokeargument name="inpWaitTime" value="#arguments.inpWaitTime#">
						<cfinvokeargument name="inpNote" value="#arguments.inpNote#">
						<cfinvokeargument name="inpCustomerOrder" value="#arguments.inpCustomerOrder#">
						<cfinvokeargument name="inpListId" value="#arguments.inpListId#">
					</cfinvoke>

					<cfset dataout.RXRESULTCODE = "#updateCustomerWaitlistResult.RXRESULTCODE#"/>
			        <cfset dataout.MESSAGE = "#updateCustomerWaitlistResult.MESSAGE#"/>
			        <cfset dataout.ERRMESSAGE = "#updateCustomerWaitlistResult.ERRMESSAGE#"/>
	   			<cfelseif arguments.inpCustomerContactString NEQ '' && arguments.inpCustomerEmail EQ ''>
	   				<cfinvoke component="session.sire.models.cfc.waitlistapp" method="UpdateCustomerWaitlistDetail" returnvariable="updateCustomerWaitlistResult">
						<cfinvokeargument name="inpWaitlistId" value="#arguments.inpWaitlistId#">
						<cfinvokeargument name="inpServiceId" value="#arguments.inpServiceId#">
						<cfinvokeargument name="inpCustomerName" value="#arguments.inpCustomerName#">
						<cfinvokeargument name="inpCustomerEmail" value="#arguments.inpCustomerEmail#">
						<cfinvokeargument name="inpCustomerContactString" value="#arguments.inpCustomerContactString#">
						<cfinvokeargument name="inpWaitlistSize" value="#arguments.inpWaitlistSize#">
						<cfinvokeargument name="inpWaitTime" value="#arguments.inpWaitTime#">
						<cfinvokeargument name="inpNote" value="#arguments.inpNote#">
						<cfinvokeargument name="inpCustomerOrder" value="#arguments.inpCustomerOrder#">
						<cfinvokeargument name="inpListId" value="#arguments.inpListId#">
					</cfinvoke>

					<cfset dataout.RXRESULTCODE = "#updateCustomerWaitlistResult.RXRESULTCODE#"/>
			        <cfset dataout.MESSAGE = "#updateCustomerWaitlistResult.MESSAGE#"/>
			        <cfset dataout.ERRMESSAGE = "#updateCustomerWaitlistResult.ERRMESSAGE#"/>
	   			<cfelseif arguments.inpCustomerEmail EQ '' && arguments.inpCustomerContactString EQ ''>
	   				<cfinvoke component="session.sire.models.cfc.waitlistapp" method="UpdateCustomerWaitlistDetail" returnvariable="updateCustomerWaitlistResult">
						<cfinvokeargument name="inpWaitlistId" value="#arguments.inpWaitlistId#">
						<cfinvokeargument name="inpServiceId" value="#arguments.inpServiceId#">
						<cfinvokeargument name="inpCustomerName" value="#arguments.inpCustomerName#">
						<cfinvokeargument name="inpCustomerEmail" value="#arguments.inpCustomerEmail#">
						<cfinvokeargument name="inpCustomerContactString" value="#arguments.inpCustomerContactString#">
						<cfinvokeargument name="inpWaitlistSize" value="#arguments.inpWaitlistSize#">
						<cfinvokeargument name="inpWaitTime" value="#arguments.inpWaitTime#">
						<cfinvokeargument name="inpNote" value="#arguments.inpNote#">
						<cfinvokeargument name="inpCustomerOrder" value="#arguments.inpCustomerOrder#">
						<cfinvokeargument name="inpListId" value="#arguments.inpListId#">
					</cfinvoke>

					<cfset dataout.RXRESULTCODE = "#updateCustomerWaitlistResult.RXRESULTCODE#"/>
			        <cfset dataout.MESSAGE = "#updateCustomerWaitlistResult.MESSAGE#"/>
			        <cfset dataout.ERRMESSAGE = "#updateCustomerWaitlistResult.ERRMESSAGE#"/>
	   			<cfelseif arguments.inpCustomerEmail NEQ '' && arguments.inpCustomerContactString NEQ ''>
	   				<cfinvoke component="session.sire.models.cfc.waitlistapp" method="UpdateCustomerWaitlistDetail" returnvariable="updateCustomerWaitlistResult">
						<cfinvokeargument name="inpWaitlistId" value="#arguments.inpWaitlistId#">
						<cfinvokeargument name="inpServiceId" value="#arguments.inpServiceId#">
						<cfinvokeargument name="inpCustomerName" value="#arguments.inpCustomerName#">
						<cfinvokeargument name="inpCustomerEmail" value="#arguments.inpCustomerEmail#">
						<cfinvokeargument name="inpCustomerContactString" value="#arguments.inpCustomerContactString#">
						<cfinvokeargument name="inpWaitlistSize" value="#arguments.inpWaitlistSize#">
						<cfinvokeargument name="inpWaitTime" value="#arguments.inpWaitTime#">
						<cfinvokeargument name="inpNote" value="#arguments.inpNote#">
						<cfinvokeargument name="inpCustomerOrder" value="#arguments.inpCustomerOrder#">
						<cfinvokeargument name="inpListId" value="#arguments.inpListId#">
					</cfinvoke>

					<cfset dataout.RXRESULTCODE = "#updateCustomerWaitlistResult.RXRESULTCODE#"/>
			        <cfset dataout.MESSAGE = "#updateCustomerWaitlistResult.MESSAGE#"/>
			        <cfset dataout.ERRMESSAGE = "#updateCustomerWaitlistResult.ERRMESSAGE#"/>
	   			</cfif>
		        <cfif dataout.RXRESULTCODE GT 0>
					<cfif waitlistQuery.WaitTime_int NEQ arguments.inpWaitTime>
						<cfset checkTime = dateAdd('n', arguments.inpWaitTime, waitlistQuery.Created_dt) />
						<cfif dateDiff('s', checkTime, now()) LT 0>
							<cfif waitlistQuery.CustomerEmail_vch NEQ ''>
				   				<cfinvoke component="session.sire.models.cfc.waitlistapp" method="SendNotice" returnvariable="sendNoticeEmail">
									<cfinvokeargument name="inpListId" value="#arguments.inpListId#"/>
									<cfinvokeargument name="inpWaitTime" value="#arguments.inpWaitTime#"/>
									<cfinvokeargument name="inpType" value="email"/>
									<cfinvokeargument name="inpWaitlistId" value="#arguments.inpWaitlistId#"/>
								</cfinvoke>
							</cfif>
							<cfif waitlistQuery.CustomerContactString_vch NEQ ''>
				   				<cfinvoke component="session.sire.models.cfc.waitlistapp" method="SendNotice" returnvariable="sendNoticeSMS">
									<cfinvokeargument name="inpListId" value="#arguments.inpListId#"/>
									<cfinvokeargument name="inpWaitTime" value="#arguments.inpWaitTime#"/>
									<cfinvokeargument name="inpType" value="sms"/>
									<cfinvokeargument name="inpWaitlistId" value="#arguments.inpWaitlistId#"/>
								</cfinvoke>
							</cfif>
						</cfif>
					</cfif>
		        </cfif>
				<cfcatch type="any">
	   				<cfset dataout.RXRESULTCODE = -1 />
		            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
				</cfcatch>
			</cftry>
		<cfelse>
			<cfset dataout.RXRESULTCODE = -2 />
        	<cfset dataout.MESSAGE = "Invalid waitlist ID" />
		</cfif>

		<cfreturn dataout />
	</cffunction>

   	<cffunction name="SendAlert" access="remote" output="false" hint="Send sms or email alert to customer">
   		<cfargument name="inpWaitListId" required="true" type="numeric"/>
   		<cfargument name="inpType" required="true" type="string"/>
   		<cfargument name="inpListId" required="true" type="numeric"/>

   		<cfset var dataout = {} />
   		<cfset var setting = '' />
   		<cfset var waitlist = '' />
   		<cfset var emailContent = structNew() />
   		<cfset var smsContent = structNew() />
   		<cfset var sendSMS = structNew() />
   		<cfset var sendEmail = '' />
   		<cfset var dataMail = structNew() />
   		<cfset var updateWaitlist = '' />

   		<cfset dataout.TYPE = '' />
   		<cfset dataout.RXRESULTCODE = -1 />
   		<cfset dataout.MESSAGE = '' />
   		<cfset dataout.ERRMESSAGE = '' />

   		<cftry>
   			<cfset setting = GetUserWaitlistAppSetting(arguments.inpListId) />

   			<cfif setting.RXRESULTCODE LT 1>
   				<cfthrow type="Application" message="Get setting failed!" />
   			</cfif>

   			<cfset waitlist = GetWaitingCustomerById(arguments.inpWaitListId, arguments.inpListId) />

   			<cfif waitlist.RXRESULTCODE LT 1>
   				<cfthrow type="Application" message="Waitlist not found!" />
   			</cfif>

   			<cfif arguments.inpType EQ "sms">
				<cfif setting.ALERTTEXTSTA LT 1>
					<cfthrow type="Application" message="Alert text setting is off!" />
				</cfif>

				<cfset smsContent = PrepareSMSContent(waitlist, 'alert', arguments.inpListId) />

				<cfif smsContent.RXRESULTCODE LT 1>
					<cfthrow type="Application" message="Can't get sms alert content!" />
				</cfif>

   				<cfset sendSMS = SendSMSMessage(waitlist.CUSTOMERPHONE, smsContent.CONTENT, arguments.inpListId) />

   				<cfset sendSMS = deserializeJSON(sendSMS) />

   				<cfif sendSMS.RXRESULTCODE LT 1>
   					<cfthrow type="Application" message="Send SMS failed!" />
   				</cfif>

   				<cfquery datasource="#session.DBSourceEBM#" result="updateWaitlist">
   					UPDATE
   						simpleobjects.waitlistapp
   					SET
   						AlertedSMS_ti = 1
   					WHERE
   						WaitListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpWaitListId#">
   				</cfquery>

   			<cfelseif arguments.inpType EQ "email">
				<cfif setting.ALERTEMAILSTA LT 1>
					<cfthrow type="Application" message="Alert text setting is off!" />
				</cfif>

				<cfset emailContent = PrepareEmailContent(waitlist, 'alert', arguments.inpListId) />

				<cfif emailContent.RXRESULTCODE LT 1>
					<cfthrow type="Application" message="Can't get email alert content!" />
				</cfif>

				<cfset dataMail = {
				    MESSAGE: emailContent.CONTENT
				}/>

				<!--- <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
				    <cfinvokeargument name="to" value="#waitlist.CUSTOMEREMAIL#"/>
				    <cfinvokeargument name="type" value="html"/>
				    <cfinvokeargument name="subject" value="#emailContent.SUBJECT#"/>
				    <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_confirmation_alert_waitlist.cfm"/>
				    <cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#"/>
				</cfinvoke> --->

				<cfinvoke method="AddEmailToQueue" component="session.sire.models.cfc.waitlistapp">
					<cfinvokeargument name="inpEmailAddress" value="#waitlist.CUSTOMEREMAIL#"/>
					<cfinvokeargument name="inpEmailSubject" value="#emailContent.SUBJECT#" />
					<cfinvokeargument name="inpEmailMessage" value="#emailContent.CONTENT#" />
				</cfinvoke>

   				<cfquery datasource="#session.DBSourceEBM#" result="updateWaitlist">
   					UPDATE
   						simpleobjects.waitlistapp
   					SET
   						AlertedEmail_ti = 1
   					WHERE
   						WaitListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpWaitListId#">
   				</cfquery>
   			<cfelse>
   				<cfthrow type="Application" message="Wrong type of alert!" />
   			</cfif>

   			<cfset dataout.RXRESULTCODE = 1 />
   			<cfset dataout.MESSAGE = "Send alert success!" />

   			<cfcatch type="any">
   				<cfset dataout.TYPE = cfcatch.type />
   				<cfset dataout.MESSAGE = cfcatch.message />
   				<cfset dataout.ERRMESSAGE = cfcatch.detail />
   			</cfcatch>
   		</cftry>

   		<cfreturn serializeJSON(dataout) />
   	</cffunction>

   	<cffunction name="SendNotice" access="private" output="false" hint="Send sms or email alert to customer">
   		<cfargument name="inpWaitListId" required="true" type="numeric"/>
   		<cfargument name="inpType" required="true" type="string"/>
   		<cfargument name="inpListId" required="true" type="numeric"/>
   		<cfargument name="inpWaitTime" required="true" type="numeric"/>

   		<cfset var dataout = {} />
   		<cfset var setting = '' />
   		<cfset var waitlist = '' />
   		<cfset var emailContent = structNew() />
   		<cfset var smsContent = structNew() />
   		<cfset var sendSMS = structNew() />
   		<cfset var sendEmail = '' />
   		<cfset var dataMail = structNew() />
   		<cfset var updateWaitlist = '' />

   		<cfset dataout.TYPE = '' />
   		<cfset dataout.RXRESULTCODE = -1 />
   		<cfset dataout.MESSAGE = '' />
   		<cfset dataout.ERRMESSAGE = '' />

   		<cftry>
   			<cfset setting = GetUserWaitlistAppSetting(arguments.inpListId) />

   			<cfif setting.RXRESULTCODE LT 1>
   				<cfthrow type="Application" message="Get setting failed!" />
   			</cfif>

   			<cfset waitlist = GetWaitingCustomerById(arguments.inpWaitListId, arguments.inpListId) />

   			<cfif waitlist.RXRESULTCODE LT 1>
   				<cfthrow type="Application" message="Waitlist not found!" />
   			</cfif>

   			<cfif arguments.inpType EQ "sms" AND waitlist.CUSTOMERPHONE NEQ ''>
				<cfif setting.NOTICETEXTSTA LT 1>
					<cfthrow type="Application" message="Notice text setting is off!" />
				</cfif>

				<cfset smsContent = PrepareSMSContent(waitlist, 'notice', arguments.inpListId) />

				<cfif smsContent.RXRESULTCODE LT 1>
					<cfthrow type="Application" message="Can't get sms notice content!" />
				</cfif>

   				<cfset sendSMS = SendSMSMessage(waitlist.CUSTOMERPHONE, smsContent.CONTENT, arguments.inpListId) />

   				<cfset sendSMS = deserializeJSON(sendSMS) />

   				<cfif sendSMS.RXRESULTCODE LT 1>
   					<cfthrow type="Application" message="Send SMS failed!" />
   				</cfif>

   			<cfelseif arguments.inpType EQ "email" AND waitlist.CUSTOMEREMAIL NEQ ''>
				<cfif setting.NOTICEEMAILSTA LT 1>
					<cfthrow type="Application" message="Notice text setting is off!" />
				</cfif>

				<cfset emailContent = PrepareEmailContent(waitlist, 'notice', arguments.inpListId) />

				<cfif emailContent.RXRESULTCODE LT 1>
					<cfthrow type="Application" message="Can't get email notice content!" />
				</cfif>

				<cfset dataMail = {
				    MESSAGE: emailContent.CONTENT
				}/>

				<!--- <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
				    <cfinvokeargument name="to" value="#waitlist.CUSTOMEREMAIL#"/>
				    <cfinvokeargument name="type" value="html"/>
				    <cfinvokeargument name="subject" value="#emailContent.SUBJECT#"/>
				    <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_confirmation_alert_waitlist.cfm"/>
				    <cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#"/>
				</cfinvoke> --->

				<cfinvoke method="AddEmailToQueue" component="session.sire.models.cfc.waitlistapp">
					<cfinvokeargument name="inpEmailAddress" value="#waitlist.CUSTOMEREMAIL#"/>
					<cfinvokeargument name="inpEmailSubject" value="#emailContent.SUBJECT#" />
					<cfinvokeargument name="inpEmailMessage" value="#emailContent.CONTENT#" />
				</cfinvoke>
   			<cfelse>
   				<cfthrow type="Application" message="Wrong type of notice!" />
   			</cfif>

   			<cfset dataout.RXRESULTCODE = 1 />
   			<cfset dataout.MESSAGE = "Send notice success!" />

   			<cfcatch type="any">
   				<cfset dataout.TYPE = cfcatch.type />
   				<cfset dataout.MESSAGE = cfcatch.message />
   				<cfset dataout.ERRMESSAGE = cfcatch.detail />
   			</cfcatch>
   		</cftry>

   		<cfreturn serializeJSON(dataout) />
   	</cffunction>

	<cffunction name="SendSMSToCustomer" access="remote" output="false" hint="Send sms to walitlist customer">
		<cfargument name="inpWaitListId" required="true" type="string" />
		<cfargument name="inpText" required="true" type="string" />
		<cfargument name="inpListId" required="true" type="numeric"/>
		<cfset var dataout = {} />
		<cfset dataout.TYPE = '' />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.RESULT = '' />

		<cfset var sendSMS = '' />
		<cfset var waitlist = '' />

		<cftry>
			<cfset waitlist = GetWaitlistById(arguments.inpWaitlistId) />

			<cfif waitlist.RXRESULTCODE LT 1>
				<cfthrow type="Application" message="Can't get waitlist" />
			</cfif>

			<cfset sendSMS = SendSMSMessage(waitlist.CUSTOMERPHONE, arguments.inpText, arguments.inpListId) />

			<cfset dataout.RESULT = deserializeJSON(sendSMS) />

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch type="any">
				<cfset dataout.TYPE = cfcatch.type />
				<cfset dataout.MESSAGE = cfcatch.message />
				<cfset dataout.ERRMESSAGE = cfcatch.detail />
			</cfcatch>
		</cftry>

		<cfreturn serializeJSON(dataout) />
	</cffunction>

	<cffunction name="SendSMSMessage" access="private" output="false" hint="Send sms to walitlist customer">
		<cfargument name="inpContactString" required="true" type="string" />
		<cfargument name="inpText" required="true" type="string" />
		<cfargument name="inpListId" required="true" type="numeric"/>

		<cfset var dataout = {} />
		<cfset dataout.TYPE = '' />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.RESULT = '' />

        <cfset var variables = {} />
        <cfset var returnStruct = {} />
        <cfset var datetime = '' />
        <cfset var signature = '' />
        <cfset var authorization = '' />
        <cfset var InsertToAPILog = '' />
        <cfset var userCredential = '' />
        <cfset var waitListBatch = '' />
        <cfset var apiUrl = '' />

		<cftry>
			<cfquery datasource="#session.DBSourceREAD#" name="userCredential">
				SELECT 
					AccessKey_vch,
					SecretKey_vch,
					Active_ti,
					SecretName_vch
				FROM 
					simpleobjects.securitycredentials
				WHERE
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#session.userId#">
				AND 
					Active_ti = 1
				LIMIT 1
			</cfquery>

			<cfif userCredential.RECORDCOUNT LT 1>
				<cfthrow type="Database" message="Can't get credential" />
			</cfif>

			<cfset waitListBatch = GetWaitListBatch(arguments.inpListId) />

			<cfif waitListBatch.RXRESULTCODE LT 1>
				<cfthrow type="Application" message="Can't get waitlist batch" />
			</cfif>

			<cfset variables.accessKey = '#userCredential.AccessKey_vch#' />
			<cfset variables.secretKey = '#userCredential.SecretKey_vch#' />

			<cfset variables.sign = generateSignature("POST", variables.secretKey) />
			<cfset datetime = variables.sign.DATE>
			<cfset signature = variables.sign.SIGNATURE>
			<cfset authorization = variables.accessKey & ":" & signature>

			<cfif FINDNOCASE(CGI.SERVER_NAME,'siremobile.com') GT 0 OR FINDNOCASE(CGI.SERVER_NAME,'www.siremobile.com') GT 0 OR FINDNOCASE(CGI.SERVER_NAME,'cron.siremobile.com') GT 0 >
			  	<cfset apiUrl = "https://api.siremobile.com/ire/secure/triggerSMS" />
			<cfelse>
				<cfset apiUrl = "https://apiqa.siremobile.com/ire/secure/triggerSMS" />
			</cfif>

			<cfhttp url="#apiUrl#" method="POST" result="returnStruct" >

				<!--- Required components --->

				<!--- By default EBM API will return json or XML --->
				<cfhttpparam name="Accept" type="header" value="application/json" />     
				<cfhttpparam type="header" name="datetime" value="#datetime#" />
				<cfhttpparam type="header" name="authorization" value="#authorization#" /> 

				<!--- Batch Id controls which pre-defined campaign to run --->
				<cfhttpparam type="formfield" name="inpBatchId" value="#waitListBatch.BatchId#" />

				<!--- Contact string--->
				<cfhttpparam type="formfield" name="inpContactString" value="#arguments.inpContactString#" />

				<!--- 1=voice 2=email 3=SMS--->
				<cfhttpparam type="formfield" name="inpContactTypeId" value="3" />


				<!--- ***TODO: Remove this after adding parse for DNC STOP Requests --->
				<cfhttpparam type="formfield" name="inpSkipLocalUserDNCCheck" value="1" />

				<!--- Optional Components --->

				<!--- Custom data element for Batch --->
				<cfhttpparam type="formfield" name="inpText" value="#arguments.inpText#" />

			</cfhttp>

			<cfset dataout.RESULT = returnStruct />

			<cfset dataout.RXRESULTCODE = 1 />

			<!--- Insert into Log --->   
			<cfquery name="InsertToAPILog" datasource="#Session.DBSourceEBM#">
				INSERT INTO simplexresults.apitracking
				(
					EventId_int,
					Created_dt,
					UserId_int,
					Subject_vch,
					Message_vch,
					TroubleShootingTips_vch,
					Output_vch,
					DebugStr_vch,
					DataSource_vch,
					Host_vch,
					Referer_vch,
					UserAgent_vch,
					Path_vch,
					QueryString_vch
				)
				VALUES
				(
					2000,
					NOW(),
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="SendWaitListSMS">, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="SendWaitListSMS Debugging Info">, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="See Query String and Result">,     
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="BatchId={#waitListBatch.BatchId#} #arguments.inpContactString#, 3, #inpText#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="BatchId={#waitListBatch.BatchId#}">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.DBSourceEBM#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_HOST),2024)#">, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.REMOTE_ADDR),2024)#">, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_USER_AGENT),2024)#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.PATH_TRANSLATED),2024)#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
				)
			</cfquery>

			<cfcatch type="any">
				<cfset dataout.TYPE = cfcatch.type />
				<cfset dataout.MESSAGE = cfcatch.message />
				<cfset dataout.ERRMESSAGE = cfcatch.detail />
			</cfcatch>
		</cftry>

		<cfreturn serializeJSON(dataout) />
	</cffunction>

    <cffunction name="generateSignature" returnformat="json" access="private" output="false" hint="generate Signature">
        <cfargument name="method" required="true" type="string" default="GET" hint="Method" />
        <cfargument name="secretKey" required="true" type="string" default="gsdagadfgadsfgsdfgdsfgsdfg" hint="Secret Key" />
                 
        <cfset var retval = {} />                 
        <cfset var dateTimeString = GetHTTPTimeString(Now())>
        
        <!--- Create a canonical string to send --->
        <cfset var cs = "#arguments.method#\n\n\n#dateTimeString#\n\n\n">
        <cfset var signature = createSignature(cs,secretKey)>
        
        <cfset retval.SIGNATURE = signature>
        <cfset retval.DATE = dateTimeString>
        
        <cfreturn retval>
    </cffunction>

    <cffunction name="HMAC_SHA1" returntype="binary" access="private" output="false" hint="NSA SHA-1 Algorithm">
       <cfargument name="signKey" type="string" required="true" />
       <cfargument name="signMessage" type="string" required="true" />
    
       <cfset var jMsg = JavaCast("string",arguments.signMessage).getBytes("iso-8859-1") />
       <cfset var jKey = JavaCast("string",arguments.signKey).getBytes("iso-8859-1") />
       <cfset var key = createObject("java","javax.crypto.spec.SecretKeySpec") />
       <cfset var mac = createObject("java","javax.crypto.Mac") />
    
       <cfset key = key.init(jKey,"HmacSHA1") />
       <cfset mac = mac.getInstance(key.getAlgorithm()) />
       <cfset mac.init(key) />
       <cfset mac.update(jMsg) />
    
       <cfreturn mac.doFinal() />
    </cffunction>

    <cffunction name="createSignature" returntype="string" access="private" output="false" hint="create Signature">
        <cfargument name="stringIn" type="string" required="true" />
        <cfargument name="scretKey" type="string" required="true" />
        <!--- Replace "\n" with "chr(10) to get a correct digest --->
        <cfset var fixedData = replace(arguments.stringIn,"\n","#chr(10)#","all")>
        <!--- Calculate the hash of the information --->
        <cfset var digest = HMAC_SHA1(scretKey,fixedData)>
        <!--- fix the returned data to be a proper signature --->
        <cfset var signature = ToBase64("#digest#")>
        
        <cfreturn signature>
    </cffunction>

    <cffunction name="GetListById" access="remote" hint="Get List by ListId">
    	<cfargument name="inpListId" required="true">

    	<cfset var dataout = {} />
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset var getList = ''>
    	<cftry>
    		<cfquery name="getList" datasource="#Session.DBSourceREAD#">
    			SELECT
    				CaptureBatchId_int
    			FROM
    				simpleobjects.waitlist
    			WHERE
    				ListId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpListId#">
    			AND
    				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
    			LIMIT
    				1
    		</cfquery>
    		<cfif getList.Recordcount GT 0>
    			<cfset dataout.CAPTUREBATCHID = getList.CaptureBatchId_int>
    			<cfset dataout.MESSAGE = 'Get Capture Batch Id successfully'>
    			<cfset dataout.RXRESULTCODE = 1>
    		<cfelse>
    			<cfset dataout.MESSAGE = 'No record found'>
    			<cfset dataout.RXRESULTCODE = 0>
    		</cfif>
    		<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -1 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
    	</cftry>
    	<cfreturn dataout />
	</cffunction>

	<cffunction name="UpdateCaptureBatchId" access="remote" hint="Update date capture batch id after Sire's user click create campaign">
		<cfargument name="inpTemplateId" required="true">
		<cfargument name="inpListId" required="true">
		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset var updateCaptureBatchId = '' />
		<cfset var shortCode = '' />
		<cfset var RetVarAddNewBatchFromTemplate = '' />
		<cfset var updateResult = '' />
		<cftry>
			<!--- remove old method: 
			<cfinvoke method="getUserShortCode" component="public.sire.models.cfc.userstools" returnvariable="shortCode">				
			--->
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>	

			<cfinvoke method="AddNewBatchFromTemplate" component="session.sire.models.cfc.control-point" returnvariable="RetVarAddNewBatchFromTemplate">
				<cfinvokeargument name="inpTemplateId" value="#arguments.inpTemplateId#">
				<cfinvokeargument name="inpDesc" value="">
				<cfinvokeargument name="inpKeyword" value="">
				<cfinvokeargument name="inpShortCode" value="#shortCode.SHORTCODE#">
				<cfinvokeargument name="inpAddList" value="1">
			</cfinvoke>

			<cfquery name="updateCaptureBatchId" datasource="#Session.DBSourceEBM#" result="updateResult">
				UPDATE
					simpleobjects.waitlist
				SET
					CaptureBatchId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RetVarAddNewBatchFromTemplate.BATCHID#">
				WHERE
					ListId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpListId#">
			</cfquery>
			<cfif updateResult.Recordcount GT 0>
				<cfset dataout.RXRESULTCODE = 1>
				<cfset dataout.CAPTUREBATCHID = "#updateResult.sqlparameters[1]#">
				<cfset dataout.MESSAGE = 'Update capture batch id successfully'>
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0>
				<cfset dataout.MESSAGE = 'Update capture batch id failed'>
			</cfif>
			<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -1 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout />

	</cffunction>

	<cffunction name="AddToWaitlistQuery" access="remote" output="false" hint="Do insert query to add customer to waitlist">
		<cfargument name="inpServiceId" required="false" default="0">
		<cfargument name="inpStatus" required="false" default="1">
		<cfargument name="inpCustomerName" required="false">
		<cfargument name="inpCustomerEmail" required="false">
		<cfargument name="inpCustomerContactString" required="false">
		<cfargument name="inpWaitlistSize" required="false" default="0">
		<cfargument name="inpWaitTime" required="false" default="0">
		<cfargument name="inpNote" required="false">
		<cfargument name="inpCustomerOrder" required="false" default="0">
		<cfargument name="inpListId" required="true" type="numeric"/>
		<cfargument name="Token" required="true"/>

		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset var createWaitlistResult = '' />
		<cfset var createWaitlist = '' />

		<cftry>
			<cfquery name="createWaitlist" datasource="#Session.DBSourceEBM#" result="createWaitlistResult">
				INSERT INTO
					simpleobjects.waitlistapp 
					(
						Order_bi,
						ServiceId_int,
						Status_int,
						CustomerName_vch,
						CustomerEmail_vch,
						CustomerContactString_vch,
						WaitListSize_int,
						WaitTime_int,
						Note_vch,
						Created_dt,
						UserId_int,
						Token_vch,
						ListId_bi
					)
				VALUES
					(
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCustomerOrder#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpServiceId#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCustomerName#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCustomerEmail#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCustomerContactString#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpWaitlistSize#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpWaitTime#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpNote#">,
						NOW(),
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.Token#">,
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
					)
			</cfquery>
			<cfif createWaitlistResult.Recordcount GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.GENERATED_KEY = createWaitlistResult.GENERATED_KEY />
				<cfset dataout.MESSAGE = 'Do Query Successfully'>
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = 'No record done'>
			</cfif>
			<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -1 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
		
	</cffunction>

	<cffunction name="UpdateCustomerWaitlistQuery" access="remote" output="false" hint="Do update query to update customer waitlist info">
		<cfargument name="inpServiceId" required="false" default="0">
		<cfargument name="inpCustomerOrder" required="false" default="1">
		<cfargument name="inpCustomerName" required="false">
		<cfargument name="inpCustomerEmail" required="false">
		<cfargument name="inpCustomerContactString" required="false">
		<cfargument name="inpWaitlistSize" required="false" default="0">
		<cfargument name="inpWaitTime" required="false" default="0">
		<cfargument name="inpNote" required="false">
		<cfargument name="inpWaitlistId" required="true">

		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />

		<cfset var updateWaitlistDetail = '' />
		<cfset var updateWaitlistDetailResult = '' />

		<cftry>
			<cfquery name="updateWaitlistDetail" datasource="#Session.DBSourceEBM#" result="updateWaitlistDetailResult">
   				UPDATE
   					simpleobjects.waitlistapp
   				SET 
   					Order_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCustomerOrder#">,
   					ServiceId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpServiceId#">,
   					CustomerName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCustomerName#">,
   					CustomerEmail_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCustomerEmail#">,
   					CustomerContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCustomerContactString#">,
   					WaitListSize_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpWaitlistSize#">,
   					WaitTime_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpWaitTime#">,
   					Note_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpNote#">
   				WHERE
   					WaitListId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpWaitlistId#">
			</cfquery>

			<cfif updateWaitlistDetailResult.Recordcount GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = 'Do Query Successfully'>
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = 'No record done'>
			</cfif>
			<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -1 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>

		</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="SelectWaitlistByPhoneOrEmail" access="remote" output="false" hint="Do select query to select customer from waitlist by phone or Email">
		<cfargument name="inpCustomerContactString" required="false">
		<cfargument name="inpCustomerEmail" required="false">
		<cfargument name="inpListId" required="true">

		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset var selectWaitlist = '' />

		<cftry>
			<cfquery name="selectWaitlist" datasource="#Session.DBSourceREAD#">
	    		SELECT
	    			CustomerEmail_vch,
	    			CustomerContactString_vch
	    		FROM
	    			simpleobjects.waitlistapp
	    		WHERE
	    			<cfif arguments.inpCustomerEmail NEQ '' && arguments.inpCustomerContactString NEQ ''>
	    				(CustomerContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpCustomerContactString#"/>
            			OR
            			CustomerEmail_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpCustomerEmail#"/>)
	    			<cfelseif arguments.inpCustomerEmail EQ '' && arguments.inpCustomerContactString NEQ ''>
	    				CustomerContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpCustomerContactString#"/>
	    			<cfelseif arguments.inpCustomerEmail NEQ '' && arguments.inpCustomerContactString EQ ''>
	    				CustomerEmail_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpCustomerEmail#"/>
	    			</cfif>
	    		AND
	    			UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
	    		AND
	    			ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
	    		AND
	    			Active_ti = 1
	    		AND
	    			Status_int = 1
	    		LIMIT 
	    			1
			</cfquery>
			<cfif selectWaitlist.Recordcount GT 0>
				<cfset dataout.RXRESULTCODE = 1>
				<cfset dataout.MESSAGE = "Select successfully">
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0>
				<cfset dataout.MESSAGE = "No record found">
			</cfif>
			<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -1 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="CheckDuplicateWhenUpdate" access="remote" output="false" hint="Check duplicate when update customer waitlist detail">
		<cfargument name="inpCustomerContactString" required="false">
		<cfargument name="inpCustomerEmail" required="false">
		<cfargument name="inpWaitlistId" required="true">
		<cfargument name="inpListId" required="true">

		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset var checkDuplicate = '' />

		<cftry>
			<cfquery name="checkDuplicate" datasource="#session.DBSourceREAD#">
				SELECT
					WaitListId_bi
				FROM
					simpleobjects.waitlistapp
				WHERE
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#" />
				AND
					Active_ti = 1
				AND
					Status_int = 1
				AND
					WaitListId_bi != <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpWaitlistId#" />
				AND
					<cfif arguments.inpCustomerEmail NEQ '' && arguments.inpCustomerContactString NEQ ''>
						(CustomerContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpCustomerContactString#"/>
		    			OR
		    			CustomerEmail_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpCustomerEmail#"/>)
					<cfelseif arguments.inpCustomerEmail EQ '' && arguments.inpCustomerContactString NEQ ''>
						CustomerContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpCustomerContactString#"/>
					<cfelseif arguments.inpCustomerEmail NEQ '' && arguments.inpCustomerContactString EQ ''>
						CustomerEmail_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpCustomerEmail#"/>
					</cfif>
				AND
					ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
			</cfquery>

			<cfif checkDuplicate.Recordcount GT 0>
				<cfset dataout.RXRESULTCODE = 1>
				<cfset dataout.MESSAGE = "Duplicated">
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0>
				<cfset dataout.MESSAGE = "Not duplicate!">
			</cfif>
			<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -1 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="AddToWaitlist" access="remote" output="false" hint="Add new customer to waitlist">
		<cfargument name="inpServiceId" required="false" default="0">
		<cfargument name="inpStatus" required="false" default="1">
		<cfargument name="inpCustomerName" required="false">
		<cfargument name="inpCustomerEmail" required="false">
		<cfargument name="inpCustomerContactString" required="false">
		<cfargument name="inpWaitlistSize" required="false" default="0">
		<cfargument name="inpWaitTime" required="false" default="0">
		<cfargument name="inpNote" required="false">
		<cfargument name="inpCustomerOrder" required="false" default="0">
		<cfargument name="inpListId" required="true" type="numeric"/>

		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />

		<cfset var userWaitlistAppSetting = '' >
		<cfset var userOrgInfo = '' >
		<cfset var Token = '' >
		<cfset var customerWaitlistInfo = '' >
		<cfset var confirmText = '' />
		<cfset var sendConfirmText = '' />
		<cfset var updateHigherOrder = '' />
		<cfset var selectWaitlistResult = '' />
		<cfset var lastOrderResult = '' />
		<cfset var addToWaitlistResult = '' />
		<cfset var sendMailResult = '' />
		<cfset var validationResult = '' />

		<cftry>

			<cfif arguments.inpCustomerName NEQ ''>
				<cfinvoke method="VldContent" component="public.cfc.validation" returnvariable="validationResult">
				    <cfinvokeargument name="Input" value="#arguments.inpCustomerName#"/>
				</cfinvoke>

				<cfif validationResult NEQ true>
				    <cfset dataout.RXRESULTCODE = -3>
				    <cfset dataout.MESSAGE = 'Please do not input special character.'>
				    <cfreturn dataout>
				</cfif>
			</cfif>

			<cfif arguments.inpNote NEQ ''>
				<cfinvoke method="VldContent" component="public.cfc.validation" returnvariable="validationResult">
				    <cfinvokeargument name="Input" value="#arguments.inpNote#"/>
				</cfinvoke>

				<cfif validationResult NEQ true>
				    <cfset dataout.RXRESULTCODE = -3>
				    <cfset dataout.MESSAGE = 'Please do not input special character.'>
				    <cfreturn dataout>
				</cfif>
			</cfif>

			<cfif arguments.inpCustomerEmail NEQ ''>
				<cfinvoke method="VldContent" component="public.cfc.validation" returnvariable="validationResult">
				    <cfinvokeargument name="Input" value="#arguments.inpCustomerEmail#"/>
				</cfinvoke>

				<cfif validationResult NEQ true>
				    <cfset dataout.RXRESULTCODE = -3>
				    <cfset dataout.MESSAGE = 'Please do not input special character.'>
				    <cfreturn dataout>
				</cfif>
			</cfif>

			<cfif arguments.inpCustomerContactString NEQ ''>
				<cfif NOT isvalid('telephone', arguments.inpCustomerContactString)>
	                <cfset dataout.RXRESULTCODE = -6>
	                <cfset dataout.MESSAGE = 'Phone Number is not a valid US telephone number !'>
	                <cfreturn dataout>
	            </cfif>
			</cfif>
			<cfinvoke component="session.sire.models.cfc.waitlistapp" method="SelectWaitlistByPhoneOrEmail" returnvariable="selectWaitlistResult">
    			<cfinvokeargument name="inpCustomerContactString" value="#arguments.inpCustomerContactString#">
    			<cfinvokeargument name="inpListId" value="#arguments.inpListId#">
    			<cfinvokeargument name="inpCustomerEmail" value="#arguments.inpCustomerEmail#">
	        </cfinvoke>

	        <cfif selectWaitlistResult.RXRESULTCODE GT 0>
    			<cfset dataout.RXRESULTCODE = -5>
    			<cfset dataout.MESSAGE = "Customer email or phone number already in use!">
    			<cfreturn dataout />
    		</cfif>

    		<cfinvoke component="session.sire.models.cfc.waitlistapp" method="GenWaitlistToken" returnvariable="Token">
            </cfinvoke>

            <cfinvoke component="session.sire.models.cfc.waitlistapp" method="GetLastOrderByUserId" returnvariable="lastOrderResult">
				<cfinvokeargument name="inpListId" value="#arguments.inpListId#"/>
			</cfinvoke>

			<cfif arguments.inpCustomerOrder EQ 0>
				<cfif lastOrderResult.RXRESULTCODE NEQ 1>
					<cfinvoke component="session.sire.models.cfc.waitlistapp" method="AddToWaitlistQuery" returnvariable="addToWaitlistResult">
	        			<cfinvokeargument name="inpCustomerContactString" value="#arguments.inpCustomerContactString#">
	        			<cfinvokeargument name="inpServiceId" value="#arguments.inpServiceId#">
	        			<cfinvokeargument name="inpListId" value="#arguments.inpListId#">
	        			<cfinvokeargument name="inpCustomerOrder" value="1">
	        			<cfinvokeargument name="inpCustomerName" value="#arguments.inpCustomerName#">
	        			<cfinvokeargument name="inpCustomerEmail" value="#arguments.inpCustomerEmail#">
	        			<cfinvokeargument name="inpNote" value="#arguments.inpNote#">
	        			<cfinvokeargument name="inpWaitlistSize" value="#arguments.inpWaitlistSize#">
	        			<cfinvokeargument name="inpWaitTime" value="#arguments.inpWaitTime#">
	        			<cfinvokeargument name="Token" value="#Token#">
			        </cfinvoke>

			        <cfif addToWaitlistResult.RXRESULTCODE GT 0>
						<cfinvoke component="session.sire.models.cfc.waitlistapp" method="GetUserWaitlistAppSetting" returnvariable="userWaitlistAppSetting">
							<cfinvokeargument name="inpListId" value="#arguments.inpListId#"/>
						</cfinvoke>
						<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="userOrgInfo">
						</cfinvoke>

						<cfinvoke component="session.sire.models.cfc.waitlistapp" method="GetWaitingCustomer" returnvariable="customerWaitlistInfo">
							<cfinvokeargument name="inpCustomerToken" value="#Token#">
						</cfinvoke>

						<cfif arguments.inpCustomerEmail NEQ ''>
							<cfif userWaitlistAppSetting.CONFIRMEMAILSTA EQ 1>
		                        <cfinvoke component="session.sire.models.cfc.waitlistapp" method="SendConfirmEmailToCustomer" returnvariable="sendMailResult">
									<cfinvokeargument name="inpCustomerName" value="#arguments.inpCustomerName#"/>
									<cfinvokeargument name="inpCustomerEmail" value="#arguments.inpCustomerEmail#"/>
									<cfinvokeargument name="inpCustomerPosition" value="#customerWaitlistInfo.POSITION#"/>
									<cfinvokeargument name="inpWaitlistTotalInline" value="#customerWaitlistInfo.TOTALINLINE#"/>
									<cfinvokeargument name="inpUserCompany" value="#userOrgInfo.ORGINFO.ORGANIZATIONNAME_VCH#"/>
									<cfinvokeargument name="inpCustomerWaitTime" value="#customerWaitlistInfo.ESTIMATEDWAIT#"/>
									<cfinvokeargument name="inpToken" value="#Token#"/>
									<cfinvokeargument name="inpEmailSubject" value="#userWaitlistAppSetting.CONFIRMEMAILSUB#"/>
									<cfinvokeargument name="inpEmailMessage" value="#userWaitlistAppSetting.CONFIRMEMAILMESS#"/>
								</cfinvoke>

		                        <cfset dataout.RXRESULTCODE = 1>
								<cfset dataout.MESSAGE = 'Add new Waitlist Successfully! An email sent to your customer. Please ask them to check the time and queue.'>
								<cfinvoke method="LogState">
									<cfinvokeargument name="inpWaitlistID" value="#addToWaitlistResult.GENERATED_KEY#">
									<cfinvokeargument name="inpWaitlistStatus" value="1">
								</cfinvoke>

								<cfreturn dataout>
							</cfif>
						</cfif>

						<cfif userWaitlistAppSetting.CONFIRMTEXTSTA GT 0>
							<cfset confirmText = PrepareSMSContent(customerWaitlistInfo, 'confirm', arguments.inpListId) />

							<cfif confirmText.RXRESULTCODE GT 0>
								<cfset sendConfirmText = SendSMSMessage(customerWaitlistInfo.CUSTOMERPHONE, confirmText.CONTENT, arguments.inpListId) />
							</cfif>
						</cfif>
						<cfset dataout.RXRESULTCODE = 1>
						<cfset dataout.MESSAGE = 'Add new Waitlist Successfully!'>

						<cfinvoke method="LogState">
							<cfinvokeargument name="inpWaitlistID" value="#addToWaitlistResult.GENERATED_KEY#">
							<cfinvokeargument name="inpWaitlistStatus" value="1">
						</cfinvoke>
					<cfelse>
						<cfset dataout.RXRESULTCODE = -4>
						<cfset dataout.MESSAGE = 'Add new Waitlist Failed'>
					</cfif>
				<cfelseif lastOrderResult.RXRESULTCODE EQ 1>
					<cfinvoke component="session.sire.models.cfc.waitlistapp" method="AddToWaitlistQuery" returnvariable="addToWaitlistResult">
	        			<cfinvokeargument name="inpCustomerContactString" value="#arguments.inpCustomerContactString#">
	        			<cfinvokeargument name="inpServiceId" value="#arguments.inpServiceId#">
	        			<cfinvokeargument name="inpListId" value="#arguments.inpListId#">
	        			<cfinvokeargument name="inpCustomerOrder" value="#lastOrderResult.LASTORDER+1#">
	        			<cfinvokeargument name="inpCustomerName" value="#arguments.inpCustomerName#">
	        			<cfinvokeargument name="inpCustomerEmail" value="#arguments.inpCustomerEmail#">
	        			<cfinvokeargument name="inpNote" value="#arguments.inpNote#">
	        			<cfinvokeargument name="inpWaitlistSize" value="#arguments.inpWaitlistSize#">
	        			<cfinvokeargument name="inpWaitTime" value="#arguments.inpWaitTime#">
	        			<cfinvokeargument name="Token" value="#Token#">
			        </cfinvoke>

			        <cfif addToWaitlistResult.RXRESULTCODE GT 0>
						<cfinvoke component="session.sire.models.cfc.waitlistapp" method="GetUserWaitlistAppSetting" returnvariable="userWaitlistAppSetting">
							<cfinvokeargument name="inpListId" value="#arguments.inpListId#"/>
						</cfinvoke>
						<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="userOrgInfo">
						</cfinvoke>

						<cfinvoke component="session.sire.models.cfc.waitlistapp" method="GetWaitingCustomer" returnvariable="customerWaitlistInfo">
							<cfinvokeargument name="inpCustomerToken" value="#Token#">
						</cfinvoke>

						<cfif arguments.inpCustomerEmail NEQ ''>
							<cfif userWaitlistAppSetting.CONFIRMEMAILSTA EQ 1>
								<cfinvoke component="session.sire.models.cfc.waitlistapp" method="SendConfirmEmailToCustomer" returnvariable="sendMailResult">
									<cfinvokeargument name="inpCustomerName" value="#arguments.inpCustomerName#"/>
									<cfinvokeargument name="inpCustomerEmail" value="#arguments.inpCustomerEmail#"/>
									<cfinvokeargument name="inpCustomerPosition" value="#customerWaitlistInfo.POSITION#"/>
									<cfinvokeargument name="inpWaitlistTotalInline" value="#customerWaitlistInfo.TOTALINLINE#"/>
									<cfinvokeargument name="inpUserCompany" value="#userOrgInfo.ORGINFO.ORGANIZATIONNAME_VCH#"/>
									<cfinvokeargument name="inpCustomerWaitTime" value="#customerWaitlistInfo.ESTIMATEDWAIT#"/>
									<cfinvokeargument name="inpToken" value="#Token#"/>
									<cfinvokeargument name="inpEmailSubject" value="#userWaitlistAppSetting.CONFIRMEMAILSUB#"/>
									<cfinvokeargument name="inpEmailMessage" value="#userWaitlistAppSetting.CONFIRMEMAILMESS#"/>
								</cfinvoke>
		                        <cfset dataout.RXRESULTCODE = 1>
								<cfset dataout.MESSAGE = 'Add new Waitlist Successfully! An email sent to your customer. Please ask them to check the time and queue.'>
								<cfinvoke method="LogState">
									<cfinvokeargument name="inpWaitlistID" value="#addToWaitlistResult.GENERATED_KEY#">
									<cfinvokeargument name="inpWaitlistStatus" value="1">
								</cfinvoke>

								<cfreturn dataout>
							</cfif>
						</cfif>

						<cfif userWaitlistAppSetting.CONFIRMTEXTSTA GT 0>
							<cfset confirmText = PrepareSMSContent(customerWaitlistInfo, 'confirm', arguments.inpListId) />

							<cfif confirmText.RXRESULTCODE GT 0>
								<cfset sendConfirmText = SendSMSMessage(customerWaitlistInfo.CUSTOMERPHONE, confirmText.CONTENT, arguments.inpListId) />
							</cfif>
						</cfif>
						<cfset dataout.RXRESULTCODE = 1>
						<cfset dataout.MESSAGE = 'Add new Waitlist Successfully!'>

						<cfinvoke method="LogState">
							<cfinvokeargument name="inpWaitlistID" value="#addToWaitlistResult.GENERATED_KEY#">
							<cfinvokeargument name="inpWaitlistStatus" value="1">
						</cfinvoke>
					<cfelse>
						<cfset dataout.RXRESULTCODE = -4>
						<cfset dataout.MESSAGE = 'Add new Waitlist Failed'>
					</cfif>
				</cfif>
			<cfelse>
				<cfinvoke component="session.sire.models.cfc.waitlistapp" method="AddToWaitlistQuery" returnvariable="addToWaitlistResult">
        			<cfinvokeargument name="inpCustomerContactString" value="#arguments.inpCustomerContactString#">
        			<cfinvokeargument name="inpServiceId" value="#arguments.inpServiceId#">
        			<cfinvokeargument name="inpListId" value="#arguments.inpListId#">
        			<cfinvokeargument name="inpCustomerOrder" value="#arguments.inpCustomerOrder#">
        			<cfinvokeargument name="inpCustomerName" value="#arguments.inpCustomerName#">
        			<cfinvokeargument name="inpCustomerEmail" value="#arguments.inpCustomerEmail#">
        			<cfinvokeargument name="inpNote" value="#arguments.inpNote#">
        			<cfinvokeargument name="inpWaitlistSize" value="#arguments.inpWaitlistSize#">
        			<cfinvokeargument name="inpWaitTime" value="#arguments.inpWaitTime#">
        			<cfinvokeargument name="Token" value="#Token#">
		        </cfinvoke>

		        <cfif addToWaitlistResult.RXRESULTCODE GT 0>

		        	<cfinvoke component="session.sire.models.cfc.waitlistapp" method="UpdateHigherOrder" returnvariable="updateHigherOrder">
						<cfinvokeargument name="inpCustomerOrder" value="#arguments.inpCustomerOrder#">
						<cfinvokeargument name="inpWaitlistId" value="#addToWaitlistResult.GENERATED_KEY#">
						<cfinvokeargument name="inpCurrentCustomerOrder" value="#lastOrderResult.LASTORDER+1#">
						<cfinvokeargument name="inpListId" value="#arguments.inpListId#"/>
					</cfinvoke>

					<cfinvoke component="session.sire.models.cfc.waitlistapp" method="GetUserWaitlistAppSetting" returnvariable="userWaitlistAppSetting">
						<cfinvokeargument name="inpListId" value="#arguments.inpListId#"/>
					</cfinvoke>
					<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="userOrgInfo">
					</cfinvoke>

					<cfinvoke component="session.sire.models.cfc.waitlistapp" method="GetWaitingCustomer" returnvariable="customerWaitlistInfo">
						<cfinvokeargument name="inpCustomerToken" value="#Token#">
					</cfinvoke>

					<cfif arguments.inpCustomerEmail NEQ ''>
						<cfif userWaitlistAppSetting.CONFIRMEMAILSTA EQ 1>
							<cfinvoke component="session.sire.models.cfc.waitlistapp" method="SendConfirmEmailToCustomer" returnvariable="sendMailResult">
								<cfinvokeargument name="inpCustomerName" value="#arguments.inpCustomerName#"/>
								<cfinvokeargument name="inpCustomerEmail" value="#arguments.inpCustomerEmail#"/>
								<cfinvokeargument name="inpCustomerPosition" value="#customerWaitlistInfo.POSITION#"/>
								<cfinvokeargument name="inpWaitlistTotalInline" value="#customerWaitlistInfo.TOTALINLINE#"/>
								<cfinvokeargument name="inpUserCompany" value="#userOrgInfo.ORGINFO.ORGANIZATIONNAME_VCH#"/>
								<cfinvokeargument name="inpCustomerWaitTime" value="#customerWaitlistInfo.ESTIMATEDWAIT#"/>
								<cfinvokeargument name="inpToken" value="#Token#"/>
								<cfinvokeargument name="inpEmailSubject" value="#userWaitlistAppSetting.CONFIRMEMAILSUB#"/>
								<cfinvokeargument name="inpEmailMessage" value="#userWaitlistAppSetting.CONFIRMEMAILMESS#"/>
							</cfinvoke>
	                        <cfset dataout.RXRESULTCODE = 1>
							<cfset dataout.MESSAGE = 'Add new Waitlist Successfully! An email sent to your customer. Please ask them to check the time and queue.'>
							<cfinvoke method="LogState">
								<cfinvokeargument name="inpWaitlistID" value="#addToWaitlistResult.GENERATED_KEY#">
								<cfinvokeargument name="inpWaitlistStatus" value="1">
							</cfinvoke>

							<cfreturn dataout>
						</cfif>
					</cfif>

					<cfif userWaitlistAppSetting.CONFIRMTEXTSTA GT 0>
						<cfset confirmText = PrepareSMSContent(customerWaitlistInfo, 'confirm', arguments.inpListId) />

						<cfif confirmText.RXRESULTCODE GT 0>
							<cfset sendConfirmText = SendSMSMessage(customerWaitlistInfo.CUSTOMERPHONE, confirmText.CONTENT, arguments.inpListId) />
						</cfif>
					</cfif>
					<cfset dataout.RXRESULTCODE = 1>
					<cfset dataout.MESSAGE = 'Add new Waitlist Successfully!'>

					<cfinvoke method="LogState">
						<cfinvokeargument name="inpWaitlistID" value="#addToWaitlistResult.GENERATED_KEY#">
						<cfinvokeargument name="inpWaitlistStatus" value="1">
					</cfinvoke>
				<cfelse>
					<cfset dataout.RXRESULTCODE = -4>
					<cfset dataout.MESSAGE = 'Add new Waitlist Failed'>
				</cfif>

			</cfif>	
			<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -1 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="UpdateCustomerWaitlistDetail" access="remote" output="false" hint="Update customer waitlist detail">
		<cfargument name="inpServiceId" required="false" default="0">
		<cfargument name="inpCustomerOrder" required="false" default="1">
		<cfargument name="inpCustomerName" required="false">
		<cfargument name="inpCustomerEmail" required="false">
		<cfargument name="inpCustomerContactString" required="false">
		<cfargument name="inpWaitlistSize" required="false" default="0">
		<cfargument name="inpWaitTime" required="false" default="0">
		<cfargument name="inpNote" required="false">
		<cfargument name="inpWaitlistId" required="true">
		<cfargument name="inpListId" required="true">

		<cfset var currentOrder = '' />
		<cfset var getWaitlistById = '' />
		<cfset var lastOrderResult = '' />
		<cfset var updateLowerOrder = '' />
		<cfset var updateHigherOrder = '' />
		<cfset var checkDuplicateResult = '' />
		<cfset var updateDetailResult = '' />
		<cfset var validationResult = '' />

		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />

		<cftry>
			<cfif arguments.inpCustomerName NEQ ''>
				<cfinvoke method="VldContent" component="public.cfc.validation" returnvariable="validationResult">
				    <cfinvokeargument name="Input" value="#arguments.inpCustomerName#"/>
				</cfinvoke>

				<cfif validationResult NEQ true>
				    <cfset dataout.RXRESULTCODE = -3>
				    <cfset dataout.MESSAGE = 'Please do not input special character.'>
				    <cfreturn dataout>
				</cfif>
			</cfif>

			<cfif arguments.inpNote NEQ ''>
				<cfinvoke method="VldContent" component="public.cfc.validation" returnvariable="validationResult">
				    <cfinvokeargument name="Input" value="#arguments.inpNote#"/>
				</cfinvoke>

				<cfif validationResult NEQ true>
				    <cfset dataout.RXRESULTCODE = -3>
				    <cfset dataout.MESSAGE = 'Please do not input special character.'>
				    <cfreturn dataout>
				</cfif>
			</cfif>

			<cfif arguments.inpCustomerEmail NEQ ''>
				<cfinvoke method="VldContent" component="public.cfc.validation" returnvariable="validationResult">
				    <cfinvokeargument name="Input" value="#arguments.inpCustomerEmail#"/>
				</cfinvoke>

				<cfif validationResult NEQ true>
				    <cfset dataout.RXRESULTCODE = -3>
				    <cfset dataout.MESSAGE = 'Please do not input special character.'>
				    <cfreturn dataout>
				</cfif>
			</cfif>

			<cfinvoke component="session.sire.models.cfc.waitlistapp" method="GetWaitlistById" returnvariable="getWaitlistById">
				<cfinvokeargument name="inpWaitlistId" value="#arguments.inpWaitlistId#">
			</cfinvoke>

			<cfset currentOrder = getWaitlistById.ORDER>
			<cfif arguments.inpCustomerContactString NEQ ''>
				<cfif NOT isvalid('telephone', arguments.inpCustomerContactString)>
	                <cfset dataout.RXRESULTCODE = -6>
	                <cfset dataout.MESSAGE = 'Phone Number is not a valid US telephone number !'>
	                <cfreturn dataout>
	            </cfif>
			</cfif>

			<cfinvoke component="session.sire.models.cfc.waitlistapp" method="CheckDuplicateWhenUpdate" returnvariable="checkDuplicateResult">
    			<cfinvokeargument name="inpCustomerContactString" value="#arguments.inpCustomerContactString#">
    			<cfinvokeargument name="inpListId" value="#arguments.inpListId#">
    			<cfinvokeargument name="inpCustomerEmail" value="#arguments.inpCustomerEmail#">
				<cfinvokeargument name="inpWaitlistId" value="#arguments.inpWaitlistId#">
	        </cfinvoke>

	        <cfif checkDuplicateResult.RXRESULTCODE GT 0>
    			<cfset dataout.RXRESULTCODE = -5>
    			<cfset dataout.MESSAGE = "Customer email or phone number already in use!">
    			<cfreturn dataout />
    		</cfif>

    		<cfinvoke component="session.sire.models.cfc.waitlistapp" method="GetLastOrderByUserId" returnvariable="lastOrderResult">
				<cfinvokeargument name="inpListId" value="#arguments.inpListId#"/>
			</cfinvoke>
			<cfif arguments.inpCustomerOrder EQ 0>
				<cfif lastOrderResult.RXRESULTCODE NEQ 1>
	   				<cfinvoke component="session.sire.models.cfc.waitlistapp" method="UpdateCustomerWaitlistQuery" returnvariable="updateDetailResult">
						<cfinvokeargument name="inpCustomerOrder" value="1"/>
						<cfinvokeargument name="inpServiceId" value="#arguments.inpServiceId#"/>
						<cfinvokeargument name="inpNote" value="#arguments.inpNote#"/>
						<cfinvokeargument name="inpCustomerName" value="#arguments.inpCustomerName#"/>
						<cfinvokeargument name="inpCustomerEmail" value="#arguments.inpCustomerEmail#"/>
						<cfinvokeargument name="inpCustomerContactString" value="#arguments.inpCustomerContactString#"/>
						<cfinvokeargument name="inpWaitlistSize" value="#arguments.inpWaitlistSize#"/>
						<cfinvokeargument name="inpWaitTime" value="#arguments.inpWaitTime#"/>
						<cfinvokeargument name="inpWaitlistId" value="#arguments.inpWaitlistId#"/>
					</cfinvoke>
	   				<cfif updateDetailResult.RXRESULTCODE GT 0>
	   					<cfset dataout.RXRESULTCODE = 1>
	   					<cfset dataout.MESSAGE = "Update waitlist successfully">
	   				<cfelse>
	   					<cfset dataout.RXRESULTCODE = 0>
	   					<cfset dataout.MESSAGE = 'Update failed'>
	   				</cfif>
	   			<cfelseif lastOrderResult.RXRESULTCODE EQ 1>
	   				<cfinvoke component="session.sire.models.cfc.waitlistapp" method="UpdateCustomerWaitlistQuery" returnvariable="updateDetailResult">
						<cfinvokeargument name="inpCustomerOrder" value="#lastOrderResult.LASTORDER#"/>
						<cfinvokeargument name="inpServiceId" value="#arguments.inpServiceId#"/>
						<cfinvokeargument name="inpNote" value="#arguments.inpNote#"/>
						<cfinvokeargument name="inpCustomerName" value="#arguments.inpCustomerName#"/>
						<cfinvokeargument name="inpCustomerEmail" value="#arguments.inpCustomerEmail#"/>
						<cfinvokeargument name="inpCustomerContactString" value="#arguments.inpCustomerContactString#"/>
						<cfinvokeargument name="inpWaitlistSize" value="#arguments.inpWaitlistSize#"/>
						<cfinvokeargument name="inpWaitTime" value="#arguments.inpWaitTime#"/>
						<cfinvokeargument name="inpWaitlistId" value="#arguments.inpWaitlistId#"/>
					</cfinvoke>
					<cfif updateDetailResult.RXRESULTCODE GT 0>
						<cfif currentOrder LT lastOrderResult.LASTORDER>
	   						<cfinvoke component="session.sire.models.cfc.waitlistapp" method="UpdateLowerOrder" returnvariable="updateLowerOrder">
	   							<cfinvokeargument name="inpWaitlistId" value="#arguments.inpWaitlistId#">
	   							<cfinvokeargument name="inpCurrentCustomerOrder" value="#currentOrder#">
	   							<cfinvokeargument name="inpCustomerOrder" value="#lastOrderResult.LASTORDER#">
	   							<cfinvokeargument name="inpListId" value="#arguments.inpListId#"/>
	   						</cfinvoke>
	   					</cfif>
	   					<cfset dataout.RXRESULTCODE = 1>
	   					<cfset dataout.MESSAGE = "Update waitlist successfully">
	   				<cfelse>
	   					<cfset dataout.RXRESULTCODE = 0>
	   					<cfset dataout.MESSAGE = 'Update failed'>
	   				</cfif>
	   			</cfif>
	   		<cfelse>
	   			<cfinvoke component="session.sire.models.cfc.waitlistapp" method="UpdateCustomerWaitlistQuery" returnvariable="updateDetailResult">
					<cfinvokeargument name="inpCustomerOrder" value="#arguments.inpCustomerOrder#"/>
					<cfinvokeargument name="inpServiceId" value="#arguments.inpServiceId#"/>
					<cfinvokeargument name="inpNote" value="#arguments.inpNote#"/>
					<cfinvokeargument name="inpCustomerName" value="#arguments.inpCustomerName#"/>
					<cfinvokeargument name="inpCustomerEmail" value="#arguments.inpCustomerEmail#"/>
					<cfinvokeargument name="inpCustomerContactString" value="#arguments.inpCustomerContactString#"/>
					<cfinvokeargument name="inpWaitlistSize" value="#arguments.inpWaitlistSize#"/>
					<cfinvokeargument name="inpWaitTime" value="#arguments.inpWaitTime#"/>
					<cfinvokeargument name="inpWaitlistId" value="#arguments.inpWaitlistId#"/>
				</cfinvoke>
				<cfif updateDetailResult.RXRESULTCODE GT 0>
					<cfif currentOrder GT arguments.inpCustomerOrder>
	   					<cfinvoke component="session.sire.models.cfc.waitlistapp" method="UpdateHigherOrder" returnvariable="updateHigherOrder">
							<cfinvokeargument name="inpCustomerOrder" value="#arguments.inpCustomerOrder#">
							<cfinvokeargument name="inpWaitlistId" value="#arguments.inpWaitlistId#">
							<cfinvokeargument name="inpCurrentCustomerOrder" value="#currentOrder#">
							<cfinvokeargument name="inpListId" value="#arguments.inpListId#"/>
						</cfinvoke>
					<cfelseif currentOrder LT arguments.inpCustomerOrder>
						<cfinvoke component="session.sire.models.cfc.waitlistapp" method="UpdateLowerOrder" returnvariable="updateLowerOrder">
   							<cfinvokeargument name="inpWaitlistId" value="#arguments.inpWaitlistId#">
   							<cfinvokeargument name="inpCurrentCustomerOrder" value="#currentOrder#">
   							<cfinvokeargument name="inpCustomerOrder" value="#arguments.inpCustomerOrder#">
   							<cfinvokeargument name="inpListId" value="#arguments.inpListId#"/>
   						</cfinvoke>
   					</cfif>
   					<cfset dataout.RXRESULTCODE = 1>
   					<cfset dataout.MESSAGE = "Update waitlist successfully">
   				<cfelse>
   					<cfset dataout.RXRESULTCODE = 0>
   					<cfset dataout.MESSAGE = 'Update failed'>
   				</cfif>
   			</cfif>
			<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -1 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="SendConfirmEmailToCustomer" access="remote" output="false" hint="Send confirmation Email to Customer">
		<cfargument name="inpCustomerName" required="false">
		<cfargument name="inpCustomerEmail" required="true">
		<cfargument name="inpCustomerPosition" required="false">
		<cfargument name="inpWaitlistTotalInline" required="false">
		<cfargument name="inpUserCompany" required="false">
		<cfargument name="inpCustomerWaitTime" required="false">
		<cfargument name="inpToken" required="false">
		<cfargument name="inpEmailSubject" required="false">
		<cfargument name="inpEmailMessage" required="false">

		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />

		<cfset var emailSubject = '' >
		<cfset var emailMessage = '' >
		<cfset var dataMail = '' >
		<cfset var addEmailResult = '' >

		<cftry>
			<cfset emailSubject = REReplace(arguments.inpEmailSubject, '\[name\]', '#arguments.inpCustomerName#', 'ALL')>
			<cfset emailSubject = REReplace(emailSubject, '\[order\]', '#arguments.inpCustomerPosition#', 'ALL')>
			<cfset emailSubject = REReplace(emailSubject, '\[quote\]', '#arguments.inpCustomerWaitTime#', 'ALL')>
			<cfset emailSubject = REReplace(emailSubject, '\[date\]', '#DateFormat(NOW(), "mmm-dd-yyyy")#')>
			
			<cfif arguments.inpUserCompany NEQ ''>
				<cfset emailSubject = REReplace(emailSubject, '\[company\]', '#arguments.inpUserCompany#', 'ALL')>
			<cfelse>
				<cfset emailSubject = REReplace(emailSubject, '\[company\]', 'CompanyName', 'ALL')>
			</cfif>
			
			<cfset emailSubject = REReplace(emailSubject, '\[size\]', '#arguments.inpWaitlistTotalInline#', 'ALL')>
			<cfset emailSubject = HTMLEditFormat(emailSubject)>
			<!--- <cfset emailSubject = REReplace(emailSubject, '\[link\]', '<a href="https://#CGI.SERVER_NAME#/waitlist-customer-view?token=#Token#">link</a>', 'ALL')> --->
			
			<cfset emailMessage = REReplace(arguments.inpEmailMessage, '\[name\]', '#arguments.inpCustomerName#', 'ALL')>
			<cfset emailMessage = REReplace(emailMessage, '\[order\]', '#arguments.inpCustomerPosition#', 'ALL')>
			<cfset emailMessage = REReplace(emailMessage, '\[quote\]', '#arguments.inpCustomerWaitTime#', 'ALL')>
			<cfset emailMessage = REReplace(emailMessage, '\[date\]', '#DateFormat(NOW(), "mmm-dd-yyyy")#')>
			
			<cfif arguments.inpUserCompany NEQ ''>
				<cfset emailMessage = REReplace(emailMessage, '\[company\]', '#arguments.inpUserCompany#', 'ALL')>
			<cfelse>
				<cfset emailMessage = REReplace(emailMessage, '\[company\]', 'CompanyName', 'ALL')>
			</cfif>
			
			<cfset emailMessage = REReplace(emailMessage, '\[size\]', '#arguments.inpWaitlistTotalInline#', 'ALL')>
			
			<cfset emailMessage = HTMLEditFormat(emailMessage)>
			<cfset emailMessage = REReplace(emailMessage, '\n', '<br />', 'ALL')>
			<cfset emailMessage = REReplace(emailMessage, '\[link\]', '<a href="https://#CGI.SERVER_NAME#/waitlist-customer-view?token=#arguments.inpToken#">link</a>', 'ALL')>
			
			
			<cfset dataMail = {
	            MESSAGE: emailMessage
	        }/>
	        <!--- <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
	            <cfinvokeargument name="to" value="#arguments.inpCustomerEmail#">
	            <cfinvokeargument name="type" value="html">
	            <cfinvokeargument name="subject" value="#emailSubject#">
	            <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_confirmation_alert_waitlist.cfm">
	            <cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#">
	        </cfinvoke> --->

	        <cfinvoke method="AddEmailToQueue" component="session.sire.models.cfc.waitlistapp" returnvariable="addEmailResult">
				<cfinvokeargument name="inpEmailAddress" value="#arguments.inpCustomerEmail#"/>
				<cfinvokeargument name="inpEmailSubject" value="#emailSubject#" />
				<cfinvokeargument name="inpEmailMessage" value="#emailMessage#" />
			</cfinvoke>
			<cfset dataout.RXRESULTCODE = "#addEmailResult.RXRESULTCODE#">
			<cfset dataout.MESSAGE = "#addEmailResult.MESSAGE#">
			<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -1 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		
		<cfreturn dataout />
	</cffunction>

	<cffunction name="InitSettingForCreateEdit" access="remote" output="false" hint="Init setting for create or edit waitlist item">
		<cfargument name="inpListId" type="numeric" required="true"/>
		<cfargument name="inpWaitListId" type="numeric" required="false" default="0" />

		<cfset var dataout = {} />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.WAITLISTSETTING = '' />
		<cfset dataout.WAITLISTSERVICES = '' />
		<cfset dataout.LISTORDER = '' />
		<cfset dataout.WAITLISTINFO = '' />
		<cfset dataout.CUSTOMERINFO = '' />

		<cftry>
			<cfset dataout.WAITLISTSETTING = GetUserWaitlistAppSetting(arguments.inpListId) />
			<cfset dataout.WAITLISTSERVICES = GetUserWaitlistServices(arguments.inpListId) />
			<cfset dataout.LISTORDER = GetWaitingCustomerByUserId(arguments.inpListId) />
			<cfif arguments.inpWaitListId NEQ 0>
				<cfset dataout.WAITLISTINFO = GetWaitlistById(arguments.inpWaitListId) />
				<cfset dataout.CUSTOMERINFO = GetWaitingCustomerById(arguments.inpWaitListId, arguments.inpListId) />
			</cfif>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.message />
				<cfset dataout.ERRMESSAGE = cfcatch.detail />
				<cfset dataout.TYPE = cfcatch.type />
			</cfcatch>
		</cftry>

		<cfreturn serializeJSON(dataout)/>
	</cffunction>

	<cffunction name="FormatPhoneNumber" access="private" output="false" hint="Format phone number as 10 gidit US number">
		<cfargument name="inpNumber" type="string" required="true"/>

	   <cfif len(arguments.inpNumber) EQ 10>
	       <!--- This only works with 10-digit US/Canada phone numbers --->
	       <cfreturn "(#left(arguments.inpNumber,3)#)#mid(arguments.inpNumber,4,3)#-#right(arguments.inpNumber,4)#" />
	   <cfelse>
	       <cfreturn arguments.inpNumber />
	   </cfif>
	</cffunction>

	<cffunction name="TransformTime" access="private" output="false" hint="Transform time from minute to readable format">
		<cfargument name="inpTime" type="numeric" required="true"/>

		<cfset var timestring = '' />

		<cfif arguments.inpTime GT 60>
			<cfset timestring = arguments.inpTime\60 & ' hour' />
			<cfif arguments.inpTime%60 GT 0>
				<cfset timestring = timestring & ' ' & arguments.inpTime%60 & ' min'/>
			</cfif>
		<cfelseif arguments.inpTime EQ 60>
			<cfset timestring = '1 hour' />
		<cfelseif arguments.inpTime GT 0>
			<cfset timestring = '#arguments.inpTime#' & ' min' />
		<cfelse>
			<cfset timestring = '' />
		</cfif>

		<cfreturn timestring />
	</cffunction>

	<cffunction name="AddEmailToQueue" access="private" output="false" hint="Add alert, notification, notice waitlist email to queue">
		<cfargument name="inpEmailAddress" required="true">
		<cfargument name="inpEmailSubject" required="true">
		<cfargument name="inpEmailMessage" required="true">

		<cfset var dataout = {} />
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />

		<cfset var addEmailToQueue= '' />
		<cfset var addResult= '' />

		<cftry>
			<cfquery name="addEmailToQueue" datasource="#Session.DBSourceEBM#" result="addResult">
				INSERT INTO
					simplequeue.sire_waitlist_email_queue
					(
						UserId_int,
						EmailAddress_vch,
						EmailSubject_vch,
						EmailMessage_txt,
						Status_ti,
						Created_dt
					)
				VALUES
					(
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpEmailAddress#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpEmailSubject#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpEmailMessage#">,
						1,
						NOW()

					)
			</cfquery>
			<!--- <cfdump var="#addResult#" abort="true"> --->
			<cfif addResult.Recordcount GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = 'Insert email to queue successfully' />
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = 'No record done' />
			</cfif>
			<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -1 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

</cfcomponent>