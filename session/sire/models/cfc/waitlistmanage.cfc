<cfcomponent>
	<cfinclude template="/public/sire/configs/paths.cfm" >
	<cfinclude template="../../configs/paths.cfm" >

	<cffunction name="GetWaitListTable" access="remote" output="false" hint="Get wait list for display on datatable">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho" />
		<cfargument name="iSortCol_1" default="-1" />
		<cfargument name="sSortDir_1" default="" />
		<cfargument name="iSortCol_2" default="-1" />
		<cfargument name="sSortDir_2" default="" />
		<cfargument name="iSortCol_0" default="-1" />
		<cfargument name="sSortDir_0" default="" />
		<cfargument name="customFilter" default="" />
		<cfset var dataout = {} />

		<cfset var GetWaitList = "" />
		<cfset var index = 1 />
		<cfset var WaitListSetting = '' />
		<cfset var filterItem = '' />
		<cfset var rsCount = '' />

		<cfset arguments.customFilter = Replace(arguments.customFilter, """", "", "ALL") />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset dataout["iTotalRecords"] = 0 />
		<cfset dataout["iTotalDisplayRecords"] = 0 />

		<cfset dataout["WaitList"] = ArrayNew() />

		<cftry>
			<cfquery datasource="#Session.DBSourceEBM#" name="GetWaitList">
				SELECT SQL_CALC_FOUND_ROWS
					ListId_bi,
					ListName_vch,
					Created_dt,
					IsDefault_ti
				FROM 
					simpleobjects.waitlist
				WHERE
					1
					<cfif arguments.customFilter NEQ "">
						AND
						ListName_vch LIKE  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#arguments.customFilter#%'>
					</cfif>
					AND
						Active_ti = 1
					AND
						UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#">
					ORDER BY
						<cfswitch expression="#arguments.iSortCol_0#">
							<cfcase value="0">
								ListId_bi #arguments.sSortDir_0#
							</cfcase>
							<cfcase value="1">
								ListName_vch #arguments.sSortDir_0#
							</cfcase>
							<cfcase value="2">
								Created_dt #arguments.sSortDir_0#
							</cfcase>
							<cfdefaultcase>
								ListId_bi #arguments.sSortDir_0#
							</cfdefaultcase>
						</cfswitch>
					LIMIT
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#" />
					OFFSET
						<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#" />
			</cfquery>
			<cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			<cfloop query="rsCount">
				<cfset dataout["iTotalDisplayRecords"] = iTotalRecords />
				<cfset dataout["iTotalRecords"] = iTotalRecords />
			</cfloop>

			<cfloop query="GetWaitList" >
				<cfset var item = [
					"#index#",
					"<a href='/session/sire/pages/wait-list-app?listid=#ListId_bi#'>#ListName_vch#</a>",
					"#dateFormat(Created_dt, "yyyy-mm-dd")# #timeFormat(Created_dt, "HH:mm:ss")#",
					'<button data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Edit" type="button" class="btn btn-icon btn-success-custom btn-edit" data-listname="#ListName_vch#" data-listid="#ListId_bi#">
						<span class="glyphicon glyphicon-pencil"></span>
					</button>&nbsp;
					<button data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Remove" type="button" class="btn btn-icon btn-success-custom btn-remove" data-listid="#ListId_bi#">
						<span class="glyphicon glyphicon-remove"></span>
					</button>
					#iIf(IsDefault_ti LT 1, DE("&nbsp;<button data-toggle='popover' data-placement='top' data-trigger='hover' data-content='Set as default' type='button' class='btn btn-icon btn-success-custom btn-set-default' data-listid='#ListId_bi#'>
						<span class='glyphicon glyphicon-bookmark'></span>
					</button>"), DE(""))#',
					"#iIf(IsDefault_ti GT 0, DE('Default'), DE(''))#",
					{listID = "#ListId_bi#", listName = "#ListName_vch#", Created = "#Created_dt#", IdDefault = "#IsDefault_ti#"}
				] />
				<cfset ArrayAppend(dataout["WaitList"], item) />
				<cfset index += 1/>
			</cfloop>

			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch type="any">
				<cfset dataout.TYPE = cfcatch.TYPE />
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
			</cfcatch>
		</cftry>
		<cfreturn serializeJSON(dataout) />
	</cffunction>

	<cffunction name="GetWaitLists" access="public" hint="Get waitlist with id and name">
		<cfset var dataout = {} />
		<cfset var getWaitListsQuery = '' />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.RESULT = [] />

		<cftry>
			<cfquery datasource="#session.DBSourceREAD#" name="getWaitListsQuery">
				SELECT
					ListId_bi,
					ListName_vch
				FROM
					simpleobjects.waitlist
				WHERE
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
				AND
					Active_ti = 1
			</cfquery>
			<cfif getWaitListsQuery.RECORDCOUNT GT 0>
				<cfloop query="getWaitListsQuery">
					<cfset var temp = {} />
					<cfset temp.ID = ListId_bi/>
					<cfset temp.NAME = ListName_vch/>
					<cfset arrayAppend(dataout.RESULT, temp)/>
				</cfloop>
			</cfif>
			<cfcatch type="any">
				<cfset dataout.TYPE = cfcatch.type />
				<cfset dataout.MESSAGE = cfcatch.message/>
				<cfset dataout.ERRMESSAGE = cfcatch.detail />
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>

	<cffunction name="GetDefaultList" access="public" hint="Get default waitlist">
		<cfset var dataout = {} />
		<cfset var getDefaultList = '' />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.LISTID = -1 />

		<cftry>
			<cfquery datasource="#session.DBSourceREAD#" name="getDefaultList">
				SELECT
					ListId_bi
				FROM
					simpleobjects.waitlist
				WHERE
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
				AND
					Active_ti = 1
				AND
					IsDefault_ti = 1
			</cfquery>

			<cfif getDefaultList.RECORDCOUNT GT 0>
				<cfset dataout.LISTID = getDefaultList.ListId_bi />
				<cfset dataout.RXRESULTCODE = 1/>
			</cfif>
			<cfcatch type="any">
				<cfset dataout.TYPE = cfcatch.type />
				<cfset dataout.MESSAGE = cfcatch.message/>
				<cfset dataout.ERRMESSAGE = cfcatch.detail />
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>

	<cffunction name="SetDefaultList" access="remote" hint="Set default waitlist">
		<cfargument name="inpListId" required="true" type="numeric"/>

		<cfset var dataout = {} />
		<cfset var setDefaultList = '' />
		<cfset var updateCurrentDefault = '' />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />

		<cftry>
			<cftransaction>
				<cfquery datasource="#session.DBSourceEBM#" result="setDefaultList">
					UPDATE
						simpleobjects.waitlist
					SET
						IsDefault_ti = 1	
					WHERE
						UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
					AND
						ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
					AND
						Active_ti = 1
					AND
						IsDefault_ti = 0
				</cfquery>

				<cfquery datasource="#session.DBSourceEBM#" result="updateCurrentDefault">
					UPDATE
						simpleobjects.waitlist
					SET
						IsDefault_ti = 0	
					WHERE
						UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
					AND
						ListId_bi != <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
					AND
						Active_ti = 1
					AND
						IsDefault_ti = 1
				</cfquery>
			</cftransaction>

			<cfif setDefaultList.RECORDCOUNT GT 0 AND updateCurrentDefault.RECORDCOUNT GT 0>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.MESSAGE = "Set default waitlist successfully!"/>
			<cfelse>
				<cfthrow type="Database" message="Database error! Set default waitlist failed!"/>
			</cfif>

			<cfcatch type="any">
				<cfset dataout.TYPE = cfcatch.type />
				<cfset dataout.MESSAGE = cfcatch.message/>
				<cfset dataout.ERRMESSAGE = cfcatch.detail />
			</cfcatch>
		</cftry>

		<cfreturn serializeJSON(dataout)/>
	</cffunction>

	<cffunction name="CreateNewWaitlist" access="remote" output="false" hint="Create new waitlist">
		<cfargument name="inpListName" required="true" type="string">

		<cfset var dataout = {} />
		<cfset var createNewQuery = '' />
		<cfset var checkListNameQuery = '' />
		<cfset var checkDefaultList = '' />
		<cfset var isDefault = 0 />
		<cfset var CreateDefaultWaitlistAppSettingResult = '' />
		<cfset var validationResult = '' />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />

		<cftry>
			<cfif arguments.inpListName EQ "" OR len(arguments.inpListName) GT 50>
				<cfthrow type="Application" message="Invalid Waitlist name!" />
			</cfif>

			<cfif arguments.inpListName NEQ ''>
				<cfinvoke method="VldContent" component="public.cfc.validation" returnvariable="validationResult">
				    <cfinvokeargument name="Input" value="#arguments.inpListName#"/>
				</cfinvoke>

				<cfif validationResult NEQ true>
				    <cfset dataout.RXRESULTCODE = -3>
				    <cfset dataout.MESSAGE = 'Please do not input special character.'>
				    <cfreturn serializeJSON(dataout) />
				</cfif>
			</cfif>

			<cfquery datasource="#session.DBSourceREAD#" name="checkListNameQuery">
				SELECT
					ListId_bi
				FROM
					simpleobjects.waitlist
				WHERE
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
				AND
					ListName_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpListName#"/>
				AND
					Active_ti = 1
			</cfquery>

			<cfif checkListNameQuery.RECORDCOUNT GT 0>
				<cfthrow type="Application" message="Waitlist name is already in use! Please choose another"/>
			</cfif>

			<cfset checkDefaultList = GetDefaultList() />

			<cfif checkDefaultList.RXRESULTCODE LT 1>
				<cfset isDefault = 1/>
			</cfif>

			<cfquery datasource="#session.DBSourceEBM#" result="createNewQuery">
				INSERT INTO
					simpleobjects.waitlist
					(
						ListName_vch,
						UserId_int,
						IsDefault_ti
					)
				VALUES
					(
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpListName#"/>,
						<cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>,
						<cfqueryparam cfsqltype="cf_sql_integer" value="#isDefault#"/>
					)
			</cfquery>

			<cfif createNewQuery.RECORDCOUNT LT 1>
				<cfthrow type="Database" message="Create waitlist failed! Database error!"/>
			</cfif>

			<cfinvoke component="session.sire.models.cfc.waitlistapp" method="CreateDefaultWaitlistAppSetting" returnvariable="CreateDefaultWaitlistAppSettingResult">
				<cfinvokeargument name="inpUserId" value="#Session.USERID#"/>
				<cfinvokeargument name="inpListId" value="#createNewQuery.GENERATED_KEY#"/>
			</cfinvoke>

			<cfset dataout.RXRESULTCODE = 1/>
			<cfset dataout.MESSAGE = "New waitlist create successfully!"/>

			<cfcatch type="any">
				<cfset dataout.TYPE = cfcatch.type />
				<cfset dataout.MESSAGE = cfcatch.message />
				<cfset dataout.ERRMESSAGE = cfcatch.detail />
			</cfcatch>
		</cftry>

		<cfreturn serializeJSON(dataout) />
	</cffunction>

	<cffunction name="DeleteWaitList" access="remote" output="false" hint="Delete waitlist">
		<cfargument name="inpListId" required="true" type="numeric">

		<cfset var dataout = {} />
		<cfset var deleteQuery = '' />
		<cfset var checkDefault = '' />
		<cfset var updateDefault = '' />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.TYPE = '' />

		<cftry>
			<cfquery datasource="#session.DBSourceREAD#" name="checkDefault">
				SELECT
					ListId_bi
				FROM
					simpleobjects.waitlist
				WHERE
					Active_ti = 1
				AND
					IsDefault_ti = 1
				AND
					ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
				AND
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
			</cfquery>

			<cfquery datasource="#session.DBSourceEBM#" result="deleteQuery">
				UPDATE
					simpleobjects.waitlist
				SET
					Active_ti = 0
				WHERE
					ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
				AND
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
				AND
					Active_ti = 1
			</cfquery>

			<cfif checkDefault.RECORDCOUNT GT 0>
				<cfquery datasource="#session.DBSourceEBM#" result="updateDefault">
					UPDATE
						simpleobjects.waitlist
					SET
						IsDefault_ti = 1
					WHERE
						UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
					AND
						Active_ti = 1
					ORDER BY
						ListId_bi ASC
					LIMIT
						1
				</cfquery>
			</cfif>

			
			<cfif deleteQuery.RECORDCOUNT LT 1>
				<cfthrow type="Application" message="Delete waitlist failed!"/>
			</cfif>
			<cfset dataout.RXRESULTCODE = 1/>
			<cfset dataout.MESSAGE = "Delete waitlist successfully!"/>
			
			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.message />
				<cfset dataout.ERRMESSAGE = cfcatch.detail />
				<cfset dataout.TYPE = cfcatch.type />
			</cfcatch>
		</cftry>

		<cfreturn serializeJSON(dataout)/>
	</cffunction>

	<cffunction name="UpdateWaitList" access="remote" output="false" hint="Update waitlist">
		<cfargument name="inpListId" required="true" type="numeric"/>
		<cfargument name="inpListName" required="true" type="string"/>

		<cfset var dataout = {} />
		<cfset var updateQuery = '' />
		<cfset var checkNameQuery = '' />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.TYPE = '' />

		<cftry>
			<cfquery datasource="#session.DBSourceREAD#" name="checkNameQuery">
				SELECT
					ListId_bi
				FROM
					simpleobjects.waitlist
				WHERE
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
				AND
					ListName_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpListName#"/>
				AND
					ListId_bi != <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpListId#"/>
				AND
					Active_ti = 1
			</cfquery>

			<cfif checkNameQuery.RECORDCOUNT GT 0>
				<cfthrow type="Application" message="Waitlist name is already in use! Please choose another"/>
			</cfif>

			<cfquery datasource="#session.DBSourceEBM#" result="updateQuery">
				UPDATE
					simpleobjects.waitlist
				SET
					ListName_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpListName#"/>
				WHERE
					ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
				AND
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
				AND
					Active_ti = 1
			</cfquery>

			<cfif updateQuery.RECORDCOUNT LT 1>
				<cfthrow type="Application" message="Update waitlist failed!"/>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1/>
			<cfset dataout.MESSAGE = "Update waitlist successfully!"/>

			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.message />
				<cfset dataout.ERRMESSAGE = cfcatch.detail />
				<cfset dataout.TYPE = cfcatch.type />
			</cfcatch>
		</cftry>

		<cfreturn serializeJSON(dataout)/>
	</cffunction>

	<cffunction name="GetWaitListById" output="false" access="remote" hint="Get waitlist by id">
		<cfargument name="inpListId" type="numeric" required="true">

		<cfset var dataout = {} />
		<cfset var waitlistQuery = '' />

		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />
		<cfset dataout.RESULT = structNew() />

		<cftry>
			<cfquery datasource="#session.DBSourceREAD#" name="waitlistQuery">
				SELECT
					ListId_bi,
					ListName_vch,
					MessageBatchId_int,
					CaptureBatchId_int,
					IsDefault_ti
				FROM
					simpleobjects.waitlist
				WHERE
					ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
				AND
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
				AND
					Active_ti = 1
				LIMIT 1
			</cfquery>
			<cfif waitlistQuery.RECORDCOUNT GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.RESULT = queryRowData(waitlistQuery, 1) />
			</cfif>
			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.message />
				<cfset dataout.ERRMESSAGE = cfcatch.detail />
				<cfset dataout.TYPE = cfcatch.type />
			</cfcatch>
		</cftry>

		<cfreturn serializeJSON(dataout) />
	</cffunction>

</cfcomponent>