<cfparam name="url.couponid" default="">
<cfparam name="url.startdate" default="">
<cfparam name="url.enddate" default="">
<cfparam name="url.couponcode" default="">

<cfinclude template="/session/sire/configs/paths.cfm" >

<cfif url.startdate EQ '' AND url.enddate EQ ''>
	<cfset file = "Coupon Report - Coupon Code-#url.couponcode#">
<cfelseif url.startdate EQ '' AND url.enddate NEQ ''>
	<cfset file = "Coupon Report - Coupon Code-#url.couponcode# - To-#url.enddate#">
<cfelseif  url.startdate NEQ '' AND url.enddate EQ ''>
	<cfset file = "Coupon Report - Coupon Code-#url.couponcode# - From-#url.startdate#">
<cfelse>
	<cfset file = "Coupon Report - Coupon Code-#url.couponcode# - From-#url.startdate# - To-#url.enddate#">
</cfif>
<cfset filename = file & ".csv">
<cfset RetVarQueryToCSV = "">

<cfheader name="Content-Disposition" value="inline; filename=#filename#"> 
<cfcontent type="application/csv">

<cfinvoke method="GetCouponReportById" component="session.sire.models.cfc.promotion" returnvariable="data">
	<cfinvokeargument name="inpStartDate" value="#url.startdate#"/>
	<cfinvokeargument name="inpEndDate" value="#url.enddate#"/>
	<cfinvokeargument name="inpCouponId" value="#url.couponid#"/>
	<cfinvokeargument name="inpSkipLimit" value="1"/>
</cfinvoke>

<cfif len(data['DATALIST']) GT 0>
	<cfset reportList =  QueryNew("CUSTOMER'S NAME,PHONE NUMBER,EMAIL,REDEEMED,TYPE")>
	<cfloop array="#data['DATALIST']#" index="item">

		<cfset QueryAddRow(reportList) />	
		<cfset QuerySetCell(reportList, "CUSTOMER'S NAME", item.NAME) />
		<cfset QuerySetCell(reportList, "PHONE NUMBER", item.PHONE) />
		<cfset QuerySetCell(reportList, "EMAIL", item.EMAIL) />
		<cfset QuerySetCell(reportList, "REDEEMED", item.REDEEMED) />
		<cfset QuerySetCell(reportList, "TYPE", item.TYPE) />
	</cfloop>

	<cfinvoke method="QueryToCSV" component="session.sire.models.cfc.admin" returnvariable="RetVarQueryToCSV">
    <cfinvokeargument name="Query" value="#reportList#">
	    <cfinvokeargument name="CreateHeaderRow" value="true">
	    <cfinvokeargument name="Delimiter" value=",">
	</cfinvoke>
</cfif>

<cfoutput>
	#RetVarQueryToCSV#
	<cfflush/>
</cfoutput>
