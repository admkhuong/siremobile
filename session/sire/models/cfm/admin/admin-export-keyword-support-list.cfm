<cfparam name="url.startdate" default="">
<cfparam name="url.enddate" default="">
<cfparam name="url.filter" default=""/>

<cfset startDate = deserializeJSON(url.startdate) />
<cfset endDate = deserializeJSON(url.enddate) />

<cfset startDate = "#startDate.year#-#startDate.month#-#startDate.day#" />
<cfset endDate = "#endDate.year#-#endDate.month#-#endDate.day#" />

<cfinclude template="/session/sire/configs/paths.cfm" >

<cfset file = "Keyword-Support-Request-#startDate#-#endDate#">
<cfset filename = file & ".csv">
<cfset RetVarQueryToCSV = "">

<cfheader name="Content-Disposition" value="inline; filename=#filename#"> 
<cfcontent type="application/csv">

<cfinvoke method="GetKeywordSupportRequestReports" component="session.sire.models.cfc.admin" returnvariable="data">
	<cfinvokeargument name="inpStartDate" value="#url.startdate#"/>
	<cfinvokeargument name="inpEndDate" value="#url.enddate#"/>
	<cfinvokeargument name="inpSkipPaging" value="1"/>
	<cfinvokeargument name="customFilter" value="#url.filter#"/>
</cfinvoke>

<cfif len(data['datalist']) GT 0>
	<cfset reportList =  QueryNew("REQUESTID,BATCHID,STATUS,NOTIFIED,CREATED,UPDATED,CONTACTSTRING,CUSTOMERMESSAGE,NOTE")>
	<cfloop array="#data['datalist']#" index="item">

		<cfset QueryAddRow(reportList) />
		<cfset QuerySetCell(reportList, "REQUESTID", item.REQUESTID) />
		<cfset QuerySetCell(reportList, "BATCHID", item.BATCHID) />
		<cfset QuerySetCell(reportList, "STATUS", item.STATUS) />
		<cfset QuerySetCell(reportList, "NOTIFIED", item.NOTIFIED) />
		<cfset QuerySetCell(reportList, "CREATED", item.CREATED) />
		<cfset QuerySetCell(reportList, "UPDATED", item.UPDATED) />
		<cfset QuerySetCell(reportList, "CONTACTSTRING", item.CONTACTSTRING) />
		<cfset QuerySetCell(reportList, "CUSTOMERMESSAGE", item.CUSTOMERMESSAGE) />
		<cfset QuerySetCell(reportList, "NOTE", item.NOTE) />
	</cfloop>

	<cfinvoke method="QueryToCSV" component="session.sire.models.cfc.admin" returnvariable="RetVarQueryToCSV">
    <cfinvokeargument name="Query" value="#reportList#">
	    <cfinvokeargument name="CreateHeaderRow" value="true">
	    <cfinvokeargument name="Delimiter" value=",">
	</cfinvoke>
</cfif>

<cfoutput>
	#RetVarQueryToCSV#
	<cfflush/>
</cfoutput>
