<cfparam name="url.campaignName" default="">
<cfparam name="url.batchId" default="">
<cfparam name="url.question" default=""/>

<!--- <cfset startDate = deserializeJSON(url.startdate) />
<cfset endDate = deserializeJSON(url.enddate) />

<cfset startDate = "#startDate.year#-#startDate.month#-#startDate.day#" />
<cfset endDate = "#endDate.year#-#endDate.month#-#endDate.day#" /> --->

<cfinclude template="/session/sire/configs/paths.cfm" >

<cfset realquestionOrder = url.question + 1/>

<cfset file = "Customer-Survey-#url.campaignName#-Question #realquestionOrder#-Report">
<cfset filename = file & ".csv">
<cfset RetVarQueryToCSV = "">

<cfheader name="Content-Disposition" value="inline; filename=#filename#"> 
<cfcontent type="application/csv">

<cfinvoke method="GetReportByQuestionInCustomerSurvey" component="session.sire.models.cfc.reports" returnvariable="data">
	<cfinvokeargument name="inpBatchId" value="#url.batchId#"/>
	<cfinvokeargument name="inpQuestionOrder" value="#url.question#"/>
</cfinvoke>

<cfif data.TYPE NEQ "">
	<cfif data.TYPE EQ "SHORTANSWER">
		<cfset reportList =  QueryNew("##,RESPONSES,DATE")>
		<cfloop array="#data['datalist']#" index="tmpItem">

			<cfset QueryAddRow(reportList) />	
			<cfset QuerySetCell(reportList, "##", tmpItem.ORDER) />
			<cfset QuerySetCell(reportList, "RESPONSES", tmpItem.RESPONSE) />
			<cfset QuerySetCell(reportList, "DATE", tmpItem.CREATED) />
		</cfloop>

		<cfinvoke method="QueryToCSV" component="session.sire.models.cfc.admin" returnvariable="RetVarQueryToCSV">
	    <cfinvokeargument name="Query" value="#reportList#">
		    <cfinvokeargument name="CreateHeaderRow" value="true">
		    <cfinvokeargument name="Delimiter" value=",">
		</cfinvoke>
	<cfelse>
		<cfset reportList =  QueryNew("ANSWER CHOICE,RESPONSES,PERCENTAGE")>
		<cfloop array="#data['ALLVAL']#" index="tmpItem">

			<cfset QueryAddRow(reportList) />	
			<cfset QuerySetCell(reportList, "ANSWER CHOICE", tmpItem.TEXT) />
			<cfset QuerySetCell(reportList, "RESPONSES", tmpItem.TOTALCHOOSE) />
			<cfset QuerySetCell(reportList, "PERCENTAGE", tmpItem.PERCENT) />
		</cfloop>

		<cfinvoke method="QueryToCSV" component="session.sire.models.cfc.admin" returnvariable="RetVarQueryToCSV">
	    <cfinvokeargument name="Query" value="#reportList#">
		    <cfinvokeargument name="CreateHeaderRow" value="true">
		    <cfinvokeargument name="Delimiter" value=",">
		</cfinvoke>
	</cfif>
	
</cfif>

<cfoutput>
	#RetVarQueryToCSV#
	<cfflush/>
</cfoutput>
