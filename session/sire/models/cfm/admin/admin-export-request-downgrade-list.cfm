<cfparam name="url.startdate" default="">
<cfparam name="url.enddate" default="">
<cfparam name="url.filter" default=""/>

<cfset startDate = deserializeJSON(url.startdate) />
<cfset endDate = deserializeJSON(url.enddate) />

<cfset startDate = "#startDate.year#-#startDate.month#-#startDate.day#" />
<cfset endDate = "#endDate.year#-#endDate.month#-#endDate.day#" />

<cfinclude template="/session/sire/configs/paths.cfm" >

<cfset file = "Last-30Days-User-Request-Downgrade-#startDate#-#endDate#">
<cfset filename = file & ".csv">
<cfset RetVarQueryToCSV = "">

<cfheader name="Content-Disposition" value="inline; filename=#filename#"> 
<cfcontent type="application/csv">

<cfinvoke method="getUserRequestDowngrades" component="session.sire.models.cfc.admin" returnvariable="data">
	<cfinvokeargument name="inpStartDate" value="#url.startdate#"/>
	<cfinvokeargument name="inpEndDate" value="#url.enddate#"/>
	<cfinvokeargument name="inpSkipPaging" value="1"/>
	<cfinvokeargument name="customFilter" value="#url.filter#"/>	
</cfinvoke>

<cfif len(data['datalist']) GT 0>
	<cfset reportList =  QueryNew("USERID,USEREMAIL,CONTACTSTRING,REQUESTDOWNGRADEDATE,FROMPLANNAME,TOPLANNAME,DOWNGRADESDATE,APPROVEDBY")>
	<cfloop array="#data['datalist']#" index="tmpItem">
		<cfset QueryAddRow(reportList) />
			
		<cfset QuerySetCell(reportList, "USERID", tmpItem.USERID) />
		<cfset QuerySetCell(reportList, "USEREMAIL", tmpItem.USEREMAIL) />				
		<cfset QuerySetCell(reportList, "CONTACTSTRING", tmpItem.CONTACTSTRING) />

		<cfset QuerySetCell(reportList, "REQUESTDOWNGRADEDATE", tmpItem.REQUESTDOWNGRADES)/>
		<cfset QuerySetCell(reportList, "FROMPLANNAME", tmpItem.FROMPLANNAME) />	
		<cfset QuerySetCell(reportList, "TOPLANNAME", tmpItem.TOPLANNAME) />	

		<cfset QuerySetCell(reportList, "DOWNGRADESDATE", tmpItem.DOWNGRADESDATE) />			
		<cfset QuerySetCell(reportList, "APPROVEDBY", tmpItem.APPROVEDBY) />	
	</cfloop>

	<cfinvoke method="QueryToCSV" component="session.sire.models.cfc.admin" returnvariable="RetVarQueryToCSV">
    <cfinvokeargument name="Query" value="#reportList#">
	    <cfinvokeargument name="CreateHeaderRow" value="true">
	    <cfinvokeargument name="Delimiter" value=",">
	</cfinvoke>
</cfif>

<cfoutput>
	#RetVarQueryToCSV#
	<cfflush/>
</cfoutput>
