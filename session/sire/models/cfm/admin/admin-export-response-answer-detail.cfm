<cfparam name="url.cpid" default="">
<cfparam name="url.qid" default="">
<cfparam name="url.format" default="">
<cfparam name="url.batchid" default="">
<cfparam name="url.filter" default="">
<cfparam name="url.answertext" default="">
<cfparam name="url.campaignname" default="">
<cfparam name="url.question" default="">
<cfparam name="url.text" default="">
<cfparam name="url.regex" default="">

<cfinclude template="/session/sire/configs/paths.cfm" >

<cfset file = "Response answer detail-#url.campaignname#-#url.text# for #url.question#">
<cfset filename = file & ".csv">
<cfset RetVarQueryToCSV = "">

<cfheader name="Content-Disposition" value="inline; filename=#filename#"> 
<cfcontent type="application/csv">

<cfinvoke method="GetOneSelectionResponseDetailList" component="session.sire.models.cfc.reports" returnvariable="data">
	<cfinvokeargument name="inpCPID" value="#url.cpid#"/>
	<cfinvokeargument name="inpQID" value="#url.qid#"/>
	<cfinvokeargument name="inpSkipPaging" value="1"/>
	<cfinvokeargument name="inpBatchId" value="#url.batchid#"/>
	<cfinvokeargument name="inpFormat" value="#url.format#"/>
	<cfinvokeargument name="customFilter" value="#url.filter#"/>
	<cfinvokeargument name="inpRegex" value="#url.regex#"/>
</cfinvoke>

<cfif len(data['DATALIST']) GT 0>
	<cfset reportList =  QueryNew("PHONE,CREATED")>
	<cfloop array="#data['DATALIST']#" index="item">

		<cfset QueryAddRow(reportList) />
		<cfset QuerySetCell(reportList, "PHONE", item.PHONE) />
		<cfset QuerySetCell(reportList, "CREATED", item.CREATED) />
		<!--- <cfset QuerySetCell(reportList, "ISACTIVE", item.STATUS) /> --->
	</cfloop>

	<cfinvoke method="QueryToCSV" component="session.sire.models.cfc.admin" returnvariable="RetVarQueryToCSV">
    <cfinvokeargument name="Query" value="#reportList#">
	    <cfinvokeargument name="CreateHeaderRow" value="true">
	    <cfinvokeargument name="Delimiter" value=",">
	</cfinvoke>
</cfif>

<cfoutput>
	#RetVarQueryToCSV#
	<cfflush/>
</cfoutput>
