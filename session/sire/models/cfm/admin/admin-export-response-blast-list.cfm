<cfparam name="url.inpBatchId" default="">
<cfparam name="url.sortDir" default="DESC">
<cfparam name="url.filter" default=""/>

<cfinclude template="/session/sire/configs/paths.cfm" >

<cfset file = "Response-Blast-By-CampainId-#url.inpBatchId#">
<cfset filename = file & ".csv">
<cfset RetVarQueryToCSV = "">

<cfheader name="Content-Disposition" value="inline; filename=#filename#"> 
<cfcontent type="application/csv">

<cfinvoke method="GetResponseBlastListByCampaignId" component="session.sire.models.cfc.campaign" returnvariable="data">
	<cfinvokeargument name="inpBatchId" value="#url.inpBatchId#"/>
	<cfinvokeargument name="sSortDir_0" value="#url.sortDir#"/>
	<cfinvokeargument name="inpSkipPaging" value="1"/>
	<cfinvokeargument name="customFilter" value="#url.filter#"/>
</cfinvoke>

<cfif len(data['datalist']) GT 0>
	<cfset reportList =  QueryNew("BATCHID,RESPONSE,PHONE,CREATED")>
	<cfloop array="#data['datalist']#" index="item">

		<cfset QueryAddRow(reportList) />
		<cfset QuerySetCell(reportList, "BATCHID", item.BATCHID) />
		<cfset QuerySetCell(reportList, "RESPONSE", item.RESPONSE) />
		<cfset QuerySetCell(reportList, "PHONE", item.PHONE) />
		<cfset QuerySetCell(reportList, "CREATED", item.CREATED) />		
	</cfloop>

	<cfinvoke method="QueryToCSV" component="session.sire.models.cfc.admin" returnvariable="RetVarQueryToCSV">
    <cfinvokeargument name="Query" value="#reportList#">
	    <cfinvokeargument name="CreateHeaderRow" value="true">
	    <cfinvokeargument name="Delimiter" value=",">
	</cfinvoke>
</cfif>

<cfoutput>
	#RetVarQueryToCSV#
	<cfflush/>
</cfoutput>
