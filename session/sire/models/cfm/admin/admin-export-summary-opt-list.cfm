<cfparam name="url.startdate" default="">
<cfparam name="url.enddate" default="">
<cfparam name="url.filter" default=""/>
<cfparam name="url.userId" default=""/>

<cfset startDate = deserializeJSON(url.startdate) />
<cfset endDate = deserializeJSON(url.enddate) />

<cfset startDate = "#startDate.year#-#startDate.month#-#startDate.day#" />
<cfset endDate = "#endDate.year#-#endDate.month#-#endDate.day#" />

<cfinclude template="/session/sire/configs/paths.cfm" >

<cfset file = "Summary-Opt-In-Out-#startDate#-#endDate#">
<cfset filename = file & ".csv">
<cfset RetVarQueryToCSV = "">

<cfheader name="Content-Disposition" value="inline; filename=#filename#"> 
<cfcontent type="application/csv">

<cfinvoke method="GetOptInOutSummaryList" component="session.sire.models.cfc.admin" returnvariable="data">
	<cfinvokeargument name="inpStartDate" value="#url.startdate#"/>
	<cfinvokeargument name="inpEndDate" value="#url.enddate#"/>
	<cfinvokeargument name="inpSkipPaging" value="1"/>
	<cfinvokeargument name="customFilter" value="#url.filter#"/>
	<cfinvokeargument name="inpUserId" value="#url.userId#"/>
</cfinvoke>

<cfif len(data['datalist']) GT 0>
	<cfset reportList =  QueryNew("NAME,OPT-IN COUNT,OPT-OUT COUNT")>
	<cfloop array="#data['datalist']#" index="tmpItem">

		<cfset QueryAddRow(reportList) />
		<cfset QuerySetCell(reportList, "NAME", tmpItem.NAME) />
		<cfset QuerySetCell(reportList, "OPT-IN COUNT", tmpItem.OPTIN) />
		<cfset QuerySetCell(reportList, "OPT-OUT COUNT", tmpItem.OPTOUT) />
	</cfloop>

	<cfinvoke method="QueryToCSV" component="session.sire.models.cfc.admin" returnvariable="RetVarQueryToCSV">
    <cfinvokeargument name="Query" value="#reportList#">
	    <cfinvokeargument name="CreateHeaderRow" value="true">
	    <cfinvokeargument name="Delimiter" value=",">
	</cfinvoke>
</cfif>

<cfoutput>
	#RetVarQueryToCSV#
	<cfflush/>
</cfoutput>
