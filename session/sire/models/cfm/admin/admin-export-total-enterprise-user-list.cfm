<cfparam name="url.startdate" default="">
<cfparam name="url.enddate" default="">
<cfparam name="url.filter" default=""/>

<cfset startDate = deserializeJSON(url.startdate) />
<cfset endDate = deserializeJSON(url.enddate) />

<cfset startDate = "#startDate.year#-#startDate.month#-#startDate.day#" />
<cfset endDate = "#endDate.year#-#endDate.month#-#endDate.day#" />

<cfinclude template="/session/sire/configs/paths.cfm" >

<cfset file = "Total-Enterprise-User-#startDate#-#endDate#">
<cfset filename = file & ".csv">
<cfset RetVarQueryToCSV = "">

<cfheader name="Content-Disposition" value="inline; filename=#filename#"> 
<cfcontent type="application/csv">

<cfinvoke method="GetEnterpriseUsers" component="session.sire.models.cfc.admin" returnvariable="data">
	<cfinvokeargument name="inpStartDate" value="#url.startdate#"/>
	<cfinvokeargument name="inpEndDate" value="#url.enddate#"/>
	<cfinvokeargument name="inpSkipPaging" value="1"/>
	<cfinvokeargument name="customFilter" value="#url.filter#"/>
</cfinvoke>

<cfif len(data['datalist']) GT 0>
	<cfset reportList =  QueryNew("USERID,PHONE,NAME,EMAIL,THRESHOLD,CREDIT,CREATED")>
	<cfloop array="#data['datalist']#" index="item">

		<cfset QueryAddRow(reportList) />	
		<cfset QuerySetCell(reportList, "USERID", item.USERID) />
		<cfset QuerySetCell(reportList, "PHONE", item.PHONE) />
		<cfset QuerySetCell(reportList, "NAME", item.NAME) />
		<cfset QuerySetCell(reportList, "EMAIL", item.EMAIL) />
		<cfset QuerySetCell(reportList, "THRESHOLD", item.THRESHOLD) />
		<cfset QuerySetCell(reportList, "CREDIT", item.CREDIT) />
		<cfset QuerySetCell(reportList, "CREATED", item.CREATED) />
		<!--- <cfset QuerySetCell(reportList, "ISACTIVE", item.STATUS) /> --->
	</cfloop>

	<cfinvoke method="QueryToCSV" component="session.sire.models.cfc.admin" returnvariable="RetVarQueryToCSV">
    <cfinvokeargument name="Query" value="#reportList#">
	    <cfinvokeargument name="CreateHeaderRow" value="true">
	    <cfinvokeargument name="Delimiter" value=",">
	</cfinvoke>
</cfif>

<cfoutput>
	#RetVarQueryToCSV#
	<cfflush/>
</cfoutput>
