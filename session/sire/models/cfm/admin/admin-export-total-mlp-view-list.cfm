<cfparam name="url.filter" default=""/>

<cfinclude template="/session/sire/configs/paths.cfm" >

<cfset file = "Total-MLP-Views">
<cfset filename = file & ".csv">
<cfset RetVarQueryToCSV = "">

<cfheader name="Content-Disposition" value="inline; filename=#filename#"> 
<cfcontent type="application/csv">

<cfinvoke method="MLPViewList" component="session.sire.models.cfc.reports.dashboard" returnvariable="data">
	<cfinvokeargument name="inpSkipPaging" value="1"/>
	<cfinvokeargument name="customFilter" value="#url.filter#"/>
</cfinvoke>

<cfif len(data['datalist']) GT 0>
	<cfset reportList =  QueryNew("MLP Name,MLP URL,MLP Views,Created")>
	<cfloop array="#data['datalist']#" index="item">

		<cfset QueryAddRow(reportList) />	
		<cfset QuerySetCell(reportList, "MLP Name", item.NAME) />
		<cfset QuerySetCell(reportList, "MLP URL", item.URL) />
		<cfset QuerySetCell(reportList, "MLP Views", item.COUNT) />
		<cfset QuerySetCell(reportList, "Created", item.CREATED) />
	</cfloop>

	<cfinvoke method="QueryToCSV" component="session.sire.models.cfc.admin" returnvariable="RetVarQueryToCSV">
    <cfinvokeargument name="Query" value="#reportList#">
	    <cfinvokeargument name="CreateHeaderRow" value="true">
	    <cfinvokeargument name="Delimiter" value=",">
	</cfinvoke>
</cfif>

<cfoutput>
	#RetVarQueryToCSV#
	<cfflush/>
</cfoutput>
