<cfparam name="url.filter" default=""/>

<cfinclude template="/session/sire/configs/paths.cfm" >

<cfset file = "Total-ShortURLs-Click">
<cfset filename = file & ".csv">
<cfset RetVarQueryToCSV = "">

<cfheader name="Content-Disposition" value="inline; filename=#filename#"> 
<cfcontent type="application/csv">

<cfinvoke method="ShortUrlClickList" component="session.sire.models.cfc.reports.dashboard" returnvariable="data">
	<cfinvokeargument name="inpSkipPaging" value="1"/>
	<cfinvokeargument name="customFilter" value="#url.filter#"/>
</cfinvoke>

<cfif len(data['datalist']) GT 0>
	<cfset reportList =  QueryNew("Short URL,Description,Target,Dynamic Data,Count,Created")>
	<cfloop array="#data['datalist']#" index="item">

		<cfset QueryAddRow(reportList) />	
		<cfset QuerySetCell(reportList, "Short URL", item.URL) />
		<cfset QuerySetCell(reportList, "Description", item.DESC) />
		<cfset QuerySetCell(reportList, "Target", item.TARGET) />
		<cfset QuerySetCell(reportList, "Dynamic Data", item.DYNAMICDATA) />
		<cfset QuerySetCell(reportList, "Count", item.COUNT) />
		<cfset QuerySetCell(reportList, "Created", item.CREATED) />
	</cfloop>

	<cfinvoke method="QueryToCSV" component="session.sire.models.cfc.admin" returnvariable="RetVarQueryToCSV">
    <cfinvokeargument name="Query" value="#reportList#">
	    <cfinvokeargument name="CreateHeaderRow" value="true">
	    <cfinvokeargument name="Delimiter" value=",">
	</cfinvoke>
</cfif>

<cfoutput>
	#RetVarQueryToCSV#
	<cfflush/>
</cfoutput>
