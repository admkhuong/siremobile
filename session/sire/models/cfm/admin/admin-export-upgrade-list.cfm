<cfparam name="url.startdate" default="">
<cfparam name="url.enddate" default="">
<cfparam name="url.filter" default=""/>

<cfset startDate = deserializeJSON(url.startdate) />
<cfset endDate = deserializeJSON(url.enddate) />

<cfset startDate = "#startDate.year#-#startDate.month#-#startDate.day#" />
<cfset endDate = "#endDate.year#-#endDate.month#-#endDate.day#" />

<cfinclude template="/session/sire/configs/paths.cfm" >

<cfset file = "User-Upgrade-#startDate#-#endDate#">
<cfset filename = file & ".csv">
<cfset RetVarQueryToCSV = "">

<cfheader name="Content-Disposition" value="inline; filename=#filename#"> 
<cfcontent type="application/csv">

<cfinvoke method="getUserUpgrades" component="session.sire.models.cfc.admin" returnvariable="data">
	<cfinvokeargument name="inpStartDate" value="#url.startdate#"/>
	<cfinvokeargument name="inpEndDate" value="#url.enddate#"/>
	<cfinvokeargument name="inpSkipPaging" value="1"/>
	<cfinvokeargument name="customFilter" value="#url.filter#"/>
</cfinvoke>

<cfif len(data['datalist']) GT 0>
	<cfset reportList =  QueryNew("USERID,USEREMAIL,FIRSTNAME,LASTNAME,CONTACTSTRING,REGISTEREDDATE,LASTLOGIN,UPGRADEDATE")>
	<cfloop array="#data['datalist']#" index="tmpItem">

		<cfset QueryAddRow(reportList) />	
		<cfset QuerySetCell(reportList, "USERID", tmpItem.USERID) />
		<cfset QuerySetCell(reportList, "USEREMAIL", tmpItem.USEREMAIL) />
		<cfset QuerySetCell(reportList, "FIRSTNAME", tmpItem.FIRSTNAME) />
		<cfset QuerySetCell(reportList, "LASTNAME", tmpItem.LASTNAME) />
		<cfset QuerySetCell(reportList, "CONTACTSTRING", tmpItem.CONTACTSTRING) />		
		<cfset QuerySetCell(reportList, "REGISTEREDDATE", tmpItem.REGISTEREDDATE) />
		<cfset QuerySetCell(reportList, "LASTLOGIN", tmpItem.LASTLOGIN)/>
		<cfset QuerySetCell(reportList, "UPGRADEDATE", tmpItem.UPGRADES)/>		
	</cfloop>

	<cfinvoke method="QueryToCSV" component="session.sire.models.cfc.admin" returnvariable="RetVarQueryToCSV">
    <cfinvokeargument name="Query" value="#reportList#">
	    <cfinvokeargument name="CreateHeaderRow" value="true">
	    <cfinvokeargument name="Delimiter" value=",">
	</cfinvoke>
</cfif>

<cfoutput>
	#RetVarQueryToCSV#
	<cfflush/>
</cfoutput>
