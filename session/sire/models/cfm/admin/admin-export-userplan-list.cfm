<cfparam name="url.planname" default="">
<cfparam name="url.startdate" default="">
<cfparam name="url.enddate" default="">
<cfparam name="url.filter" default=""/>

<cfset startDate = deserializeJSON(url.startdate) />
<cfset endDate = deserializeJSON(url.enddate) />

<cfset startDate = "#startDate.year#-#startDate.month#-#startDate.day#" />
<cfset endDate = "#endDate.year#-#endDate.month#-#endDate.day#" />

<cfinclude template="/session/sire/configs/paths.cfm" >

<cfset file = "Plan-#url.planname#-#startDate#-#endDate#">
<cfset filename = file & ".csv">
<cfset RetVarQueryToCSV = "">

<cfheader name="Content-Disposition" value="inline; filename=#filename#">
<cfcontent type="application/csv">

<cfinvoke method="GetAllUserPlanReport" component="session.sire.models.cfc.admin" returnvariable="data">
	<cfinvokeargument name="inpStartDate" value="#url.startdate#"/>
	<cfinvokeargument name="inpEndDate" value="#url.enddate#"/>
	<cfinvokeargument name="inpPlanName" value="#url.planname#"/>
	<cfinvokeargument name="inpSkipPaging" value="1"/>
	<cfinvokeargument name="customFilter" value="#url.filter#"/>
</cfinvoke>

<cfif len(data['datalist']) GT 0>
	<cfset reportList =  QueryNew("USERID,USEREMAIL,USERNAME,ORGNAME,CONTACTSTRING,REGISTEREDDATE,PLANSTARTDATE,PLANENDDATE,PLANNAME")>
	<cfloop array="#data['datalist']#" index="item">

		<cfset QueryAddRow(reportList) />
		<cfset QuerySetCell(reportList, "USERID", item.USERID) />
		<cfset QuerySetCell(reportList, "USEREMAIL", item.USEREMAIL) />
		<cfset QuerySetCell(reportList, "USERNAME", item.USERNAME) />
		<cfset QuerySetCell(reportList, "ORGNAME", item.USERNAME) />
		<cfset QuerySetCell(reportList, "CONTACTSTRING", item.CONTACTSTRING) />
		<cfset QuerySetCell(reportList, "REGISTEREDDATE", item.REGISTEREDDATE) />
		<cfset QuerySetCell(reportList, "PLANSTARTDATE", item.PLANSTARTDATE) />
		<cfset QuerySetCell(reportList, "PLANENDDATE", item.PLANENDDATE) />
		<!--- <cfset QuerySetCell(reportList, "PLANSTATUS", item.PLANSTATUS) /> --->
		<cfset QuerySetCell(reportList, "PLANNAME", item.PLANNAME) />
	</cfloop>

	<cfinvoke method="QueryToCSV" component="session.sire.models.cfc.admin" returnvariable="RetVarQueryToCSV">
    <cfinvokeargument name="Query" value="#reportList#">
	    <cfinvokeargument name="CreateHeaderRow" value="true">
	    <cfinvokeargument name="Delimiter" value=",">
	</cfinvoke>
</cfif>

<cfoutput>
	#RetVarQueryToCSV#
	<cfflush/>
</cfoutput>
