<cfparam name="url.id" default="">

<cfinclude template="/session/sire/configs/paths.cfm" >

<cfset file = "Billing History-#Session.FULLNAME#-ID:#url.id#">
<cfset filename = file & ".csv">
<cfset RetVarQueryToCSV = "">

<cfheader name="Content-Disposition" value="inline; filename=#filename#"> 
<cfcontent type="application/csv">

<cfinvoke method="GetBillingHistoryById" component="session.sire.models.cfc.billing" returnvariable="data">
	<cfinvokeargument name="inpId" value="#url.id#"/>
</cfinvoke>

<cfif data.RXRESULTCODE GT 0>
	<cfset reportList =  QueryNew("DATE,AMOUNT,PRODUCT,STATUS")>

		<cfset QueryAddRow(reportList) />	
		<cfset QuerySetCell(reportList, "DATE", data.CREATED) />
		<cfset QuerySetCell(reportList, "AMOUNT", data.AMOUNT) />
		<cfset QuerySetCell(reportList, "PRODUCT", data.PRODUCT) />
		<cfset QuerySetCell(reportList, "STATUS", data.STATUS) />

	<cfinvoke method="QueryToCSV" component="session.sire.models.cfc.admin" returnvariable="RetVarQueryToCSV">
    <cfinvokeargument name="Query" value="#reportList#">
	    <cfinvokeargument name="CreateHeaderRow" value="true">
	    <cfinvokeargument name="Delimiter" value=",">
	</cfinvoke>
</cfif>

<cfoutput>
	#RetVarQueryToCSV#
	<cfflush/>
</cfoutput>
