<cfparam name="form.payment_method" default="0">
<cfparam name="form.line1" default="">
<cfparam name="form.plan" default="">
<cfparam name="form.city" default="">
<cfparam name="form.country" default="">
<cfparam name="form.cvv" default="">
<cfparam name="form.email" default="">
<cfparam name="form.expirationDate" default="">
<cfparam name="form.firstName" default="">
<cfparam name="form.lastName" default="">
<cfparam name="form.number" default="">
<cfparam name="form.phone" default="">
<cfparam name="form.state" default="">
<cfparam name="form.zip" default="">
<cfparam name="form.numberKeyword" default="" >
<cfparam name='form.select_used_card' default="0">
<cfparam name='form.save_cc_info' default="0">


<cfset email = form.email/>
<cfif form.select_used_card EQ 1>
	<cfif form.h_email_save EQ ''>
		<cfset email = form.h_email/>
	<cfelse>
		<cfset email = form.h_email_save/>
	</cfif>
<cfelse>
	<cfif form.email EQ ''>
		<cfset email = form.h_email/>
	</cfif>
</cfif>

<cfif (!structKeyExists(form,'number'))>
	<cfabort>
</cfif>

<cfinvoke method="GetUserPlan" component="session/sire/models/cfc/order_plan" returnvariable="getUserPlan">
</cfinvoke>

<cfset amount = (form.numberKeyword * getUserPlan.PRICEKEYWORDAFTER) >

<cfset txtTotalKeywords = "Buy " & form.numberKeyword>

<cfif form.numberKeyword GT 1>
	<cfset txtTotalKeywords &= ' Keywords'>
<cfelse>	
	<cfset txtTotalKeywords &= ' Keyword'>
</cfif>


<cftry>
	
<cfinclude template="../../configs/credits.cfm">
<cfif form.select_used_card EQ 1> <!--- USE SAVED TOKEN TO PAYMENT --->
	<cfinvoke component="session.sire.models.cfc.billing" method="getUserAuthenInfo" returnvariable="GetUserAuthen"></cfinvoke> 

	<cfif GetUserAuthen.RXRESULTCODE EQ 1 AND structKeyExists(GetUserAuthen.DATA, "PaymentMethodID_vch") AND GetUserAuthen.DATA.PaymentMethodID_vch NEQ ''  >
		<cfset customerId = #session.USERID#>
		<cfset paymentMethodId = #GetUserAuthen.DATA.PaymentMethodID_vch#>
		<cfset paymentTypeShortName = #GetUserAuthen.DATA.PaymentType_vch#>

		<cftry>
			<cfset paymentType = StructFind(worldpayPaymentTypes, paymentTypeShortName)>
		<cfcatch>
			<cfset paymentType = worldpayPaymentTypes.UNKNOWN>
		</cfcatch>	
		</cftry>

	<cfelse>
			<cfset dataout.ID="-4">
			<cfset dataout.MSG="We're Sorry! Unable to get your CreditCard info.Please update in [My Plan] page.">
	</cfif>

	<cfset paymentData = {
		amount = amount,
		transactionDuplicateCheckIndicator = 1,
		extendedInformation = {
    			notes = txtTotalKeywords,
  		},
		paymentVaultToken = {  
		   customerId = customerId,
		   paymentMethodId = paymentMethodId,
		   paymentType = paymentType
		},
		developerApplication = worldpayApplication
	}>
<cfelse>	
	<cfset paymentData = {
			amount = amount,
			transactionDuplicateCheckIndicator = 1,
			extendedInformation = {
    			notes = txtTotalKeywords,
  			},
			developerApplication = {
				developerId: worldpayApplication.developerId,
				Version: worldpayApplication.Version
		  }
		}>

	<cfif form.payment_method EQ 1>
		<cfset paymentData.check = {
				routingNumber	= form.number,
				accountNumber	= form.cvv,
				firstName		= form.firstName,
				lastName		= form.lastName,
				address 		= {
					line1 	= form.line1,
					city 	= form.city,
					state 	= form.state,
					zip 	= form.zip,
					country = form.country,
					phone	= form.phone
				},
				email 		= form.email
			}>
	<cfelse>
		<cfset paymentData.card = {
				number 			= form.number,
				cvv 			= form.cvv,
				expirationDate 	= form.expirationDate,
				firstName		= form.firstName,
				lastName		= form.lastName,
				address 		= {
					line1 	= form.line1,
					city 	= form.city,
					state 	= form.state,
					zip 	= form.zip,
					country = form.country,
					phone	= form.phone
				},
				email 		= form.email
			}>
	</cfif>
</cfif>
<cfset closeBatchesData = {
				developerApplication = {
					developerId: worldpayApplication.developerId,
					Version: worldpayApplication.Version
			  }
			}>
	

<cfhttp url="#payment_url#" method="post" result="paymentRespose" port="443">
	<cfhttpparam type="header" name="content-type" value="application/json">
	<cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
	<cfhttpparam type="body" value='#TRIM( serializeJSON(paymentData) )#'>
</cfhttp>

<cfset dataout = {} />
<cfset dataout.ID = "-1">
<cfset dataout.MSG = "We're Sorry! Unable to complete your transaction at this time. Transaction Failed.">
<cfset creditsPurchased = form.numberKeyword>


<cfif paymentRespose.status_code EQ 200>
	<cfif trim(paymentRespose.filecontent) NEQ "" AND isJson(paymentRespose.filecontent)>
		<cfset paymentResposeContent = deserializeJSON(paymentRespose.filecontent)>
		
			<cfif paymentResposeContent.success>
				<cfset dataout.ID = "1">
				<cfset dataout.MSG = "Transaction Complete. Thank you!">
				
				<!--- close session--->
				<cfhttp url="#close_batches_url#" method="post" result="closeBatchesRequestResult" port="443">
				<cfhttpparam type="header" name="content-type" value="application/json">
				<cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
				<cfhttpparam type="body" value='#TRIM( serializeJSON(closeBatchesData) )#'>
				</cfhttp>

				<!--- CHECK USER PLAN EXPRIED --->
				<cfinvoke component="session.sire.models.cfc.order_plan" method="checkUserPlanExpried" returnvariable="checkUserPlan">
					<cfinvokeargument name="userId" value="#SESSION.USERID#">
				</cfinvoke>

				<!--- IF PLAN EXPRIED in 30 days -> Update BuyKeywordNumberExpired_int --->
				<cfif checkUserPlan.RXRESULTCODE EQ 1>
					<!--- Add keyword After Expried --->
					<cfquery name="BuyKeyword" datasource="#Session.DBSourceEBM#">
				        UPDATE	
				        	simplebilling.userplans
				        SET		
				        	BuyKeywordNumberExpired_int = BuyKeywordNumberExpired_int + #form.numberKeyword#
				        WHERE	
				        	userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#session.UserId#">
				    </cfquery>
				<cfelse>
					<!--- Add keyword --->
					<cfquery name="BuyKeyword" datasource="#Session.DBSourceEBM#">
				        UPDATE	
				        	simplebilling.userplans
				        SET		
				        	BuyKeywordNumber_int = BuyKeywordNumber_int + #form.numberKeyword#
				        WHERE	
				        	userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#session.UserId#">
				    </cfquery>
				</cfif>
				
				<!--- Sendmail payment --->
				<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
		            <cfinvokeargument name="to" value="#email#">
		            <cfinvokeargument name="type" value="html">
		            <cfinvokeargument name="subject" value="[SIRE][Buy Keyword] Payment Info">
		            <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_buy_keyword.cfm">
		            <cfinvokeargument name="data" value="#paymentRespose.filecontent#">
		        </cfinvoke>
				
				<!--- Add payment --->
				
				<cfinvoke method="updatePaymentWorlDay" component="session.sire.models.cfc.order_plan">
		            <cfinvokeargument name="planId" value="#getUserPlan.PLANID#">
		            <cfinvokeargument name="numberKeyword" value="#form.numberKeyword#">
		            <cfinvokeargument name="numberSMS" value="">
		            <cfinvokeargument name="moduleName" value="By Keyword">
		            <cfinvokeargument name="paymentRespose" value="#paymentRespose.filecontent#">
		        </cfinvoke>
		        
		        			
				<!--- Add Userlog --->
				<cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
		            <cfinvokeargument name="userId" value="#session.userid#">
		            <cfinvokeargument name="moduleName" value="Payment Processing">
		            <cfinvokeargument name="operator" value="Securenet Payment Executed OK! Keyword Added = #creditsPurchased# for $#amount#">
		        </cfinvoke>

		       	<!--- CREATE ACCOUNT ON SERCUNET --->
		       	<cfif form.select_used_card EQ 0 OR form.save_cc_info EQ 1 > <!--- If do not have account or choice save then create --->
			        <cfinvoke component="session.sire.models.cfc.billing" method="RetrieveCustomer" returnvariable="RetCustomerInfo"></cfinvoke> 	
			        <cfif RetCustomerInfo.RXRESULTCODE EQ 1>
			        	<cfset primaryPaymentMethodId = 0> <!--- do not print --->
			        <cfelse>	
			        	<cfset primaryPaymentMethodId = 1> <!--- do not print --->
			        </cfif>
			         <cfinvoke method="UpdateCustomer" component="session.sire.models.cfc.billing">
			            <cfinvokeargument name="payment_method" value="#form.payment_method#">
			            <cfinvokeargument name="line1" value="#form.line1#">
			            <cfinvokeargument name="city" value="#form.city#">
			            <cfinvokeargument name="country" value="#form.country#">
			            <cfinvokeargument name="phone" value="#form.phone#">
			            <cfinvokeargument name="state" value="#form.state#">
			            <cfinvokeargument name="zip" value="#form.zip#">
			            <cfinvokeargument name="email" value="#form.email#">
			            <cfinvokeargument name="cvv" value="#form.cvv#">
			            <cfinvokeargument name="expirationDate" value="#form.expirationDate#">
			            <cfinvokeargument name="firstName" value="#form.firstName#">
			            <cfinvokeargument name="lastName" value="#form.lastName#">
				    	<cfinvokeargument name="number" value="#form.number#">
				    	<cfinvokeargument name="primaryPaymentMethodId" value="#primaryPaymentMethodId#">
			        </cfinvoke>
			    </cfif>    
				
			</cfif>
		
	</cfif>
<cfelse>

	
	<!--- Log activity --->
	<cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
        <cfinvokeargument name="userId" value="#session.userid#">
        <cfinvokeargument name="moduleName" value="Payment Processing">
        <cfinvokeargument name="operator" value="Securenet Execute Transaction Failed!">
    </cfinvoke>
</cfif>

	<!--- Log payment --->
	<cftry>
	<cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
			            <cfinvokeargument name="moduleName" value="Buy Keyword">
			            <cfinvokeargument name="status_code" value="#paymentRespose.status_code#">
			            <cfinvokeargument name="status_text" value="#paymentRespose.status_text#">
			            <cfinvokeargument name="errordetail" value="#paymentRespose.errordetail#">
			            <cfinvokeargument name="filecontent" value="#paymentRespose.filecontent#">
			            <cfinvokeargument name="paymentdata" value="#paymentData#">
	</cfinvoke>
	<cfcatch type="any">
	</cfcatch>
	</cftry>
<cfcatch type="any">
	<cftry>
    	<!--- Log bug to file /session/sire/logs/bugs/payment  --->
		<cfif !isDefined('variables._Response')>
			<cfset variables._PageContext = GetPageContext()>
			<!--- Coldfusion --->
			<!---<cfset variables._Response = variables._PageContext.getCFOutput()>--->
			<cfset variables._Response = variables._PageContext.getOut()>
		</cfif>
		<cfset variables._Response.clear()>
		
    	<cfdump var="#cfcatch#">
		<cfset catchContent = variables._Response.getString()>
		<cfset variables._Response.clear()>
    	
    	<cffile action="write" output="#catchContent#"
    	file="../../logs/bugs/payment/securenet_payment_error_#REReplace('#now()#', '\W', '_', 'all')#.txt" 
    	>
         <cfcatch type="any">
        </cfcatch>

    </cftry>
	<cfset dataout.ID="-3">
	<cfset dataout.MSG="We're Sorry! Unable to complete your transaction at this time. Transaction Failed.">
</cfcatch>
</cftry>

<cfoutput>#serializeJSON(dataout)#</cfoutput>