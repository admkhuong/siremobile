<!--- This templateid is used to look up data for a preview bootstrap modal page --->
<cfparam name="inpbatchId" default="">
<cfparam name="inpcontactstring" default="0">
<cfparam name="inpcontactqueueId" default="0">
<cfparam name="inpuserId" default="0">
<cfparam name="inpsessionId" default="0">

<cfoutput>
<div class ="row">
	<cfif LEN(TRIM(inpcontactstring)) GT 0 >

					<cfquery name="GetSessionReminder" datasource="#Session.DBSourceEBM#">
							SELECT 
								SessionId_bi
							FROM 
								simplequeue.sessionire
							WHERE 
								DTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcontactqueueId#">
					</cfquery>
					<cfif GetSessionReminder.RecordCount GT 0>
						<cfset inpsessionId = GetSessionReminder.SessionId_bi>
					</cfif>
		            <cfquery name="GetIREResults" datasource="#Session.DBSourceEBM#">
		               	SELECT 
		               		IREResultsId_bi,
							BatchId_bi,
							UserId_int,
							CPId_int,
							QID_int,
							IREType_int,
							CarrierId_int,
							InitialSendResult_int,
							Increments_ti,
							IRESessionId_bi,
							MasterRXCallDetailId_bi,
							moInboundQueueId_bi,
							DATE_FORMAT(Created_dt,'%b %d %Y %T') AS Created_dt,
							ContactString_vch,
							NPA_vch,
							NXX_vch,
							Response_vch,
							ShortCode_vch
		               	FROM 
		               		simplexresults.ireresults
						WHERE
							ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcontactstring#"> 
                       	AND
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpbatchId#">
						AND 
							IRESessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpsessionId#">
						ORDER BY 
							IREResultsId_bi    
						LIMIT 100	            
		            </cfquery>  
					<cfif GetIREResults.RECORDCOUNT EQ 0>
						<div style="text-align: center; display: block" class="cshr-text">No result found!</div>
					</cfif>
					<cfloop query="GetIREResults">
						<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12">
							<cfif GetIREResults.IREType_int EQ 1 >
								<div style="text-align: right;">
									#GetIREResults.Created_dt#
								</div>
								<div class="control-point-text bubble me">            				 	
									#GetIREResults.Response_vch# 
			        			</div>		
							<cfelseif GetIREResults.IREType_int EQ 2 >
								<div style="text-align: left;">
									#GetIREResults.Created_dt#
								</div>
								<div class="control-point-text bubble you" >           				 	
									#GetIREResults.Response_vch#
			        			</div>
							<cfelse>
								<!---<div class="">    
									#GetIREResults.Response_vch#
								</div>	
                                --->
							</cfif>
						</div>
					</cfloop>
			</cfif>									
		</div>
    </div>
</cfoutput>