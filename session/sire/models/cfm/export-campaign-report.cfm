<cfparam name="url.batchid" default="0">
<cfparam name="url.type" default="0">
<cfparam name="url.groupid" default="0">
<cfparam name="url.startdate" default="">
<cfparam name="url.enddate" default="">

<cfset startDate = deserializeJSON(url.startdate) />
<cfset endDate = deserializeJSON(url.enddate) />

<cfset startDate = "#startDate.year#-#startDate.month#-#startDate.day#" />
<cfset endDate = "#endDate.year#-#endDate.month#-#endDate.day#" />




<cfinclude template="../../configs/paths.cfm" >


<!--- <cfset listFile = {}/> --->
<cfset listFile = ArrayNew(1)>
<cfset file = "Campaign-report-#url.batchid##REReplace(now(), '[\W\s]+', '-', 'all')##Session.USERID#">
<cfset filename = file & ".csv">
<cfset RetVarQueryToCSV = "">

<cfheader name="Content-Disposition" value="inline; filename=#filename#"> 
<cfcontent type="application/octet-stream">


<cfinvoke method="GetCampaignReportById" component="session.sire.models.cfc.campaign" returnvariable="rsData">
	<cfinvokeargument name="inpBatchId" value="#url.batchid#">
	<cfinvokeargument name="inpGroupId" value="#url.groupid#">
	<cfinvokeargument name="inpType" value="#url.type#">
	<cfinvokeargument name="inpStartDate" value="#startDate#">
	<cfinvokeargument name="inpEndDate" value="#endDate#">
</cfinvoke>






<!--- <cfdump var="#rsData.DATALIST#" abort="true"> --->

<cfif rsData.DATALIST.len() GT 0>
	<cfset subcirberList =  QueryNew("SUBCRIBER LIST,NUMBER OF MESSAGE SENT,OPTIN'S,OPTOUT'S,Response Percentage")>
	<cfloop array="#rsData.DATALIST#" index="row">

		<cfset QueryAddRow(subcirberList) />	
		<cfset QuerySetCell(subcirberList, "SUBCRIBER LIST", row[1]) />
		<cfset QuerySetCell(subcirberList, "NUMBER OF MESSAGE SENT", row[2]) />
		<cfset QuerySetCell(subcirberList, "OPTIN'S", row[3]) />
		<cfset QuerySetCell(subcirberList, "OPTOUT'S", row[4]) />
		<cfset QuerySetCell(subcirberList, "Response Percentage", row[5]) />
	</cfloop>

	<cfinvoke method="QueryToCSV" component="session.sire.models.cfc.admin" returnvariable="RetVarQueryToCSV">
    <cfinvokeargument name="Query" value="#subcirberList#">
	    <cfinvokeargument name="CreateHeaderRow" value="true">
	    <cfinvokeargument name="Delimiter" value=",">
	</cfinvoke>
</cfif>

<cfoutput>
	#RetVarQueryToCSV#
	<cfflush/>
</cfoutput>
