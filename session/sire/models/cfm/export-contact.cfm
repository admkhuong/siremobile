<cfparam name="url.file_id" default="0">


<cfinclude template="../../configs/paths.cfm" >
<cfinclude template="/public/sire/configs/bulk_insert_constants.cfm"/>
<!--- <cfset listFile = {}/> --->
<cfset listFile = {}>
<cfset file = "contact-file-#url.file_id##REReplace(now(), '[\W\s]+', '-', 'all')##Session.USERID#">
<cfset filename = file & ".csv">
<cfset RetVarQueryToCSV = "">

<cfheader name="Content-Disposition" value="inline; filename=#filename#"> 
<cfcontent type="application/msexcel">
        
<!--- Make sure we dont end up in infinite loops here - will crash web server if we do --->
  <cfset tableName = "simplexuploadstage.Contactlist_stage_"&#url.file_id#>
<cfquery name="contactList" datasource="#Session.DBSourceREAD#">
	SELECT ContactStringCorrect_vch FROM #tableName# WHERE Status_ti = #STAGE_ERROR_DUPLICATED_DATA#
</cfquery>

<cfif contactList.recordCount GT 0>
	<cfset contactLists =  QueryNew("CONTACT")>
	<cfloop query="contactList">
		<cfset QueryAddRow(contactLists) />	
		<cfset QuerySetCell(contactLists, "CONTACT", ContactStringCorrect_vch) />
	</cfloop>
	
	<cfinvoke method="QueryToCSV" component="session.sire.models.cfc.admin" returnvariable="RetVarQueryToCSV">
		<cfinvokeargument name="Query" value="#contactLists#">
	    <cfinvokeargument name="CreateHeaderRow" value="true">
	    <cfinvokeargument name="Delimiter" value=",">
	</cfinvoke>

			
</cfif>


<cfoutput>
	#RetVarQueryToCSV#
	<cfflush/>
</cfoutput>



