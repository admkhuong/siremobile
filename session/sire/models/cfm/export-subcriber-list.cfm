<cfparam name="url.subid" default="0">
<cfparam name ="url.download" default="1"/>

<cfinclude template="../../configs/paths.cfm" >

<!--- <cfset listFile = {}/> --->
<cfset listFile = ArrayNew(1)>
<cfset file = "subscribers-list-#url.subid##REReplace(now(), '[\W\s]+', '-', 'all')##Session.USERID#">
<cfset filename = file & ".csv">
<cfset RetVarQueryToCSV = "">

<cfset fieldTitle = ["CONTACT","Last Subscription Date"]/>
<cfif url.download EQ 1>
	<cfheader name="Content-Disposition" value="inline; filename=#filename#"> 
	<cfcontent type="application/msexcel">	
</cfif>
        
<cfquery name="contactList" datasource="#Session.DBSourceREAD#">
	SELECT 
		simplelists.contactstring.ContactString_vch, simplelists.contactstring.contactid_bi
	FROM
		simplelists.groupcontactlist 
		INNER JOIN simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
		INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi	   						
	WHERE                
		simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
	<cfif url.subid GT 0>
		AND simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.subid#">
	</cfif>		
</cfquery>

<!--- get custom fields --->
<cfquery name="GetCustomFields" datasource="#Session.DBSourceREAD#">
	SELECT 
		CdfId_int,
		CdfName_vch as VariableName_vch,
		CdfDefaultValue_vch,
		type_field
	FROM 
		simplelists.customdefinedfields
	WHERE
		userid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#SESSION.USERID#">
	ORDER BY 
		Created_dt ASC
</cfquery>

<cfset num = {}/>
<cfif GetCustomFields.RECORDCOUNT GT 0>
	<cfloop query="#GetCustomFields#">
		<cfif NOT structKeyExists(num, VariableName_vch)>
			<cfset num[VariableName_vch] = 0 />
		</cfif>

		<cfif ArrayFindNoCase(fieldTitle,VariableName_vch) GT 0>
			<cfset num[VariableName_vch] ++ />
			<cfset VariableName_vch &= "(" & num[VariableName_vch] & ")">
		</cfif>
		<cfset arrayAppend(fieldTitle,VariableName_vch)>
	</cfloop>
</cfif>

<cfif contactList.recordCount GT 0>
	<cfset contactLists =  QueryNew(fieldTitle)>
	<cfloop query="contactList">

		<cfinvoke component="session.sire.models.cfc.subscribers" method="getSubscriber4Export" returnvariable="contactData">
			<cfinvokeargument name="inpContactId" value="#contactid_bi#">
		</cfinvoke>
		
		<cfset QueryAddRow(contactLists) />	
		<cfset QuerySetCell(contactLists, "CONTACT", ContactString_vch) />
		<!--- <cfset QuerySetCell(contactLists, "Mobile Phone Carrier", contactData.operatorName) /> --->
		<cfset QuerySetCell(contactLists, "Last Subscription Date", contactData.lastUpdateDate) />

		<cfif contactData.GetCustomFieldsData.recordCount GT 0>
			<cfloop query="#contactData.GetCustomFieldsData#">
				<cfif ArrayFindNoCase(fieldTitle,VariableName_vch) GT 0>
					<cfset QuerySetCell(contactLists, VariableName_vch, VariableValue_vch) />	
				</cfif>
			</cfloop>
		</cfif>
	</cfloop>

	<cfinvoke method="QueryToCSV" component="session.sire.models.cfc.admin" returnvariable="RetVarQueryToCSV">
	    <cfinvokeargument name="Query" value="#contactLists#">
		<cfinvokeargument name="CreateHeaderRow" value="true">
		<cfinvokeargument name="Delimiter" value=",">
	</cfinvoke>
</cfif>

<cfoutput>
	#RetVarQueryToCSV#
	<cfflush/>
</cfoutput>
