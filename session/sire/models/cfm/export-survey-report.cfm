<cfparam name="url.BatchId" default="">
<cfparam name="url.BatchName" default="">
<cfinclude template="/session/sire/configs/paths.cfm" >

<cfset file = "Multiple Choice Survey Report-#url.BatchName#-#Session.FULLNAME#">
<cfset filename = file & ".csv">
<cfset RetVarQueryToCSV = "">

<cfheader name="Content-Disposition" value="inline; filename=#filename#"> 
<cfcontent type="application/csv">

<cfinvoke method="GetMultipleChoiceBySurveyBatch" component="session.sire.models.cfc.reports" returnvariable="data">
	<cfinvokeargument name="inpBatchId" value="#url.BatchId#"/>
</cfinvoke>

<cfset answerList = {1 = 'A', 2 = 'B', 3 = 'C', 4 = 'D', 5 = 'E', 6 = 'F', 7 = 'G', 8 = 'H', 9 = 'I', 10 = 'J', 11 = 'K', 12 = 'L', 13 = 'M', 14 = 'N', 15 = 'O', 16 = 'P', 17 = 'Q', 18 = 'R', 19 = 'S', 20 = 'T', 21 = 'U', 22 = 'V', 23 = 'W', 24 = 'X', 25 = 'Y', 26 = 'Z'}/>

<cfset maxTotalChoice = 0 />
<cfloop array="#data['DATALIST']#" index="index">
    <cfif maxTotalChoice < arrayLen(index.ALLVAL)>
        <cfset maxTotalChoice = arrayLen(index.ALLVAL)/>
    </cfif>
</cfloop>

<cfset answer = []/>

<cfloop from="1" to="#maxTotalChoice#" index="i">
	<cfset arrayAppend(answer, answerList[i])/>
</cfloop>

<!--- <cfdump var="#data['DATALIST']#" abort="true"/> --->

<!--- <cfinvoke method="GetCPByBatchIdAndType" component="session.sire.models.cfc.reports" returnvariable="data1">
	<cfinvokeargument name="inpBatchId" value="#url.BatchId#"/>
	<cfinvokeargument name="inpCPType" value="ONESELECTION"/>
</cfinvoke> --->

<cfif data.RXRESULTCODE GT 0>
	<cfset reportList =  QueryNew("Question, #arrayToList(answer)#, Total")>
		<cfloop array="#data['DATALIST']#" index="index">
			<cfset QueryAddRow(reportList) />
			<cfset QuerySetCell(reportList,'Question', index.NAME) />
			<cfloop from="1" to="#maxTotalChoice#" index="i">
				<cfif arrayIsDefined(index.ALLVAL, i)>
					<cfset QuerySetCell(reportList, answerList[i], index.ALLVAL[i].TOTALCHOOSE)/>
				<cfelse>
					<cfset QuerySetCell(reportList, answerList[i], "")/>
				</cfif>
			</cfloop>
			<cfset QuerySetCell(reportList,'Total', index.ALLCHOOSE) />
		</cfloop>

	<cfinvoke method="QueryToCSV" component="session.sire.models.cfc.admin" returnvariable="RetVarQueryToCSV">
    <cfinvokeargument name="Query" value="#reportList#">
	    <cfinvokeargument name="CreateHeaderRow" value="true">
	    <cfinvokeargument name="Delimiter" value=",">
	</cfinvoke>
</cfif>

<cfoutput>
	#RetVarQueryToCSV#
	<cfflush/>
</cfoutput>
