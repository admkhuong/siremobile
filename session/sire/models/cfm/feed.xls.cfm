<cfparam name="url.id" default="0">
<cfparam name="url.af_start_date" default="">
<cfparam name="url.af_end_date" default="">
<cfparam name="url.af_phone_number" default="">
<cfparam name="url.af_batch_id" default="0">

<cfinclude template="../../configs/paths.cfm" >

<cfif !isNumeric(url.af_batch_id) OR url.af_batch_id LT 1>
	<cfset url.af_batch_id = 0>
</cfif>
<cfif !isNumeric(url.id) OR url.id LT 1>
	<cfif url.af_batch_id GT 0>
		<cfset url.id = url.af_batch_id>
	<cfelse>
		<cfset url.id = 0>
	</cfif>
</cfif>

<cfset start = 0>

<cfset url.af_phone_number = trim(url.af_phone_number)>

<cfquery name="campaignBatchIds" datasource="#Session.DBSourceREAD#">
	SELECT BatchId_bi FROM simpleobjects.batch WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">; 
</cfquery>

<cfif campaignBatchIds.RecordCount GT 0>
	<cfset batchIds = valueList(campaignBatchIds.BatchId_bi,',')>
<cfelse>
	<cfset batchIds = '-1'>
</cfif>

<cfset limit = 60000> 
<cfset activeFeedQuery.RecordCount = limit/>
<cfset n = 1/>


<!--- <cfset listFile = {}/> --->
<cfset listFile = ArrayNew(1)>
<cfset file = "activity-feed-#url.id##REReplace(now(), '[\W\s]+', '-', 'all')##Session.USERID#">
<cfset filename = file & ".csv">

<cfheader name="Content-Disposition" value="inline; filename=#filename#"> 
<cfcontent type="application/msexcel">

<cfset SecurityCheck_iDisplayStart = start>

<cfquery name="activeFeedQuery" datasource="#Session.DBSourceREAD#">
	SELECT * FROM (
		(
		SELECT
			OT.OptId_int AS Id_bi,
	    	OT.BatchId_bi, 
	        OT.ContactString_vch, 
			IF(ISNULL(OT.OptOut_dt) OR OT.OptIn_dt < OT.OptOut_dt, OT.OptIn_dt, OT.OptOut_dt) AS dt,
			0 AS sms_type,
			'' AS sms,
			IF(ISNULL(OT.OptOut_dt) OR OT.OptIn_dt < OT.OptOut_dt, 1, 2) AS log_type 
		FROM 
	    	simplelists.optinout OT INNER JOIN simplelists.contactstring CS ON OT.ContactString_vch = CS.ContactString_vch
			INNER JOIN simplelists.contactlist CL ON CL.ContactId_bi = CS.ContactId_bi AND CL.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
		<cfif url.id GT 0>
			WHERE OT.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.id#">
		<cfelse>
			WHERE (OT.BatchId_bi IN (#batchIds#) OR OT.OptOutSource_vch = 'MO Stop Request')
		</cfif>
			AND LENGTH(OT.ContactString_vch) < 14
			<cfif url.af_phone_number NEQ "">
				AND OT.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#url.af_phone_number#%">
			</cfif>
		)
		
		UNION
		
		(
		SELECT
			IREResultsId_bi AS Id_bi,
	    	BatchId_bi, 
	        ContactString_vch, 
			Created_dt AS dt,
			IREType_int AS sms_type,
			Response_vch AS sms,
			0 AS log_type 
		FROM 
	    	simplexresults.ireresults
		<cfif url.id GT 0>
			WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.id#">
		<cfelse>
			WHERE BatchId_bi IN (#batchIds#)
		</cfif>
			<!---AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">--->
			AND IREType_int IN (1,2)
			AND LENGTH(ContactString_vch) < 14
			<cfif url.af_phone_number NEQ "">
				AND ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#url.af_phone_number#%">
			</cfif>
		)
		<cfif url.af_batch_id EQ 0>
		UNION 
		(
			SELECT CT.CPPX_trackingId_int AS Id_bi, 
				0 AS BatchId_bi, 
				CT.ContactString_vch, 
				CT.Created_dt AS dt, 
				999 AS sms_type, 
				CD.cppxName_vch AS sms, 
				3 AS log_type 
			FROM simplexresults.cppx_tracking CT INNER JOIN simplelists.cppx_data CD ON CT.CPPId_int = CD.ccpxDataId_int
			WHERE CT.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
			AND LENGTH(CT.ContactString_vch) < 14
			<cfif url.af_phone_number NEQ "">
				AND CT.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#url.af_phone_number#%">
			</cfif>
		)
		</cfif>
		
	) AS ActiveFeed
	<cfset af_sd_valid = IsDate(url.af_start_date)>
	<cfset af_ed_valid = IsDate(url.af_end_date)>
	<cfif af_sd_valid OR af_ed_valid>
	WHERE
		<cfif af_sd_valid>
			DATE(dt) >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#url.af_start_date#"> 
		</cfif>
		<cfif af_ed_valid>
			<cfif af_sd_valid>AND</cfif>
			DATE(dt) <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#url.af_end_date#"> 
		</cfif>
	</cfif>
	
	ORDER BY 
    	dt DESC, 
        Id_bi DESC
	LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#start#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#limit#">
</cfquery>

<cfset start += limit>

<cfif activeFeedQuery.RecordCount GT 0>

	<cfset campaignNames = []>
	
	<cfif isDefined('campaignDetail')>
		<cfset campaignNames[url.id] = campaignDetail.BatchObject.Desc_vch>
	<cfelse>
		<cfset campaignIds = []>
		<cfloop query="activeFeedQuery">
			<cfset arrayAppend(campaignIds, activeFeedQuery.BatchId_bi)>
		</cfloop>
		
		<cfquery name="campaignList" datasource="#Session.DBSourceREAD#">
			SELECT BatchId_bi, Desc_vch 
			FROM simpleobjects.batch
			WHERE BatchId_bi IN (#arrayToList(campaignIds, ',')#);
		</cfquery>
		
		<cfif campaignList.RecordCount GT 0>
			<cfloop query="campaignList">
				<cfset campaignNames[campaignList.BatchId_bi] = campaignList.Desc_vch>
			</cfloop>
		</cfif>
	</cfif>

</cfif>


<cfset activeFeeds =  QueryNew("Phone Number,Action,Message,Date")>


<cfloop query="activeFeedQuery">
	<cfset action = "">
	<cfswitch expression="#log_type#">
		<cfdefaultcase>
			<cfswitch expression="#sms_type#">
					<cfcase value="1">
						<cfset action = "received"/>
					</cfcase>
					<cfcase value="2">
						<cfset action = "sent"/>
					</cfcase>
					<cfcase value="999">
						<cfset action = "cpp"/>
					</cfcase>
			</cfswitch>
		</cfdefaultcase>
		<cfcase value="1">
			<cfset action = "joined "/>
			<cfif arrayIsDefined(campaignNames, BatchId_bi)>
				<cfset action = action&#campaignNames[BatchId_bi]#/>	 
			</cfif>
			<cfset action = action&" Campaign"/>	 
		</cfcase>
		<cfcase value="2">
			<cfset action = "left your Campaign "/>	 
		</cfcase>
		
	</cfswitch>
	
	<cfset QueryAddRow(activeFeeds) />
	<cfset QuerySetCell(activeFeeds, "Phone Number", ContactString_vch) />
	<cfset QuerySetCell(activeFeeds, "Action", action) />
	<cfset QuerySetCell(activeFeeds, "Message", sms) />
	<cfset QuerySetCell(activeFeeds, "Date", "#DateFormat(dt, 'long')# #TimeFormat(dt, 'short')#" ) />
</cfloop>

<cfif activeFeeds.recordCount GT 0>
	<!---<cfspreadsheet action="update" filename="#ExpandPath('/')#session/sire/cache/downloads/#filename#" overwrite="true" password="password" query="activeFeeds" sheetname="ActiveFeed">--->
	<cfinvoke method="QueryToCSV" component="session.sire.models.cfc.admin" returnvariable="RetVarQueryToCSV">
    	<cfinvokeargument name="Query" value="#activeFeeds#">
	    <cfinvokeargument name="CreateHeaderRow" value="true">
	    <cfinvokeargument name="Delimiter" value=",">
	</cfinvoke>

	<cfset ArrayAppend(listFile, #RetVarQueryToCSV#)>

	<cfset SecurityCheck = 0>
	<cfset MaxLoops = 100000>
	<!--- <cfset url.start += limit> --->
</cfif>

<cfloop condition = "activeFeedQuery.RecordCount EQ limit"> 
	<!--- <cfset filename = file & "#n#.csv"/> --->

	<!--- Robustness check - dont keep looping over same data --->
    <cfif SecurityCheck_iDisplayStart EQ start>
        <cfoutput>Export Error - Same Data found in multiple loops</cfoutput>
    	<cfbreak/>
    </cfif>


    <cfquery name="activeFeedQuery" datasource="#Session.DBSourceREAD#">
	SELECT * FROM (
		(
		SELECT
			OT.OptId_int AS Id_bi,
	    	OT.BatchId_bi, 
	        OT.ContactString_vch, 
			IF(ISNULL(OT.OptOut_dt) OR OT.OptIn_dt < OT.OptOut_dt, OT.OptIn_dt, OT.OptOut_dt) AS dt,
			0 AS sms_type,
			'' AS sms,
			IF(ISNULL(OT.OptOut_dt) OR OT.OptIn_dt < OT.OptOut_dt, 1, 2) AS log_type 
		FROM 
	    	simplelists.optinout OT INNER JOIN simplelists.contactstring CS ON OT.ContactString_vch = CS.ContactString_vch
			INNER JOIN simplelists.contactlist CL ON CL.ContactId_bi = CS.ContactId_bi AND CL.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
		<cfif url.id GT 0>
			WHERE OT.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.id#">
		<cfelse>
			WHERE (OT.BatchId_bi IN (#batchIds#) OR OT.OptOutSource_vch = 'MO Stop Request')
		</cfif>
			AND LENGTH(OT.ContactString_vch) < 14
			<cfif url.af_phone_number NEQ "">
				AND OT.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#url.af_phone_number#%">
			</cfif>
		)
		
		UNION
		
		(
		SELECT
			IREResultsId_bi AS Id_bi,
	    	BatchId_bi, 
	        ContactString_vch, 
			Created_dt AS dt,
			IREType_int AS sms_type,
			Response_vch AS sms,
			0 AS log_type 
		FROM 
	    	simplexresults.ireresults
		<cfif url.id GT 0>
			WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.id#">
		<cfelse>
			WHERE BatchId_bi IN (#batchIds#)
		</cfif>
			<!---AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">--->
			AND IREType_int IN (1,2)
			AND LENGTH(ContactString_vch) < 14
			<cfif url.af_phone_number NEQ "">
				AND ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#url.af_phone_number#%">
			</cfif>
		)
		<cfif url.af_batch_id EQ 0>
		UNION 
		(
			SELECT CT.CPPX_trackingId_int AS Id_bi, 
				0 AS BatchId_bi, 
				CT.ContactString_vch, 
				CT.Created_dt AS dt, 
				999 AS sms_type, 
				CD.cppxName_vch AS sms, 
				3 AS log_type 
			FROM simplexresults.cppx_tracking CT INNER JOIN simplelists.cppx_data CD ON CT.CPPId_int = CD.ccpxDataId_int
			WHERE CT.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
			AND LENGTH(CT.ContactString_vch) < 14
			<cfif url.af_phone_number NEQ "">
				AND CT.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#url.af_phone_number#%">
			</cfif>
		)
		</cfif>
		
	) AS ActiveFeed
	<cfset af_sd_valid = IsDate(url.af_start_date)>
	<cfset af_ed_valid = IsDate(url.af_end_date)>
	<cfif af_sd_valid OR af_ed_valid>
	WHERE
		<cfif af_sd_valid>
			DATE(dt) >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#url.af_start_date#"> 
		</cfif>
		<cfif af_ed_valid>
			<cfif af_sd_valid>AND</cfif>
			DATE(dt) <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#url.af_end_date#"> 
		</cfif>
	</cfif>
	
	ORDER BY 
    	dt DESC, 
        Id_bi DESC
	LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#start#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#limit#">
	</cfquery>
	<cfset start += limit>

	<cfif activeFeedQuery.RecordCount GT 0>
	
		<cfset campaignNames = []>
		
		<cfif isDefined('campaignDetail')>
			<cfset campaignNames[url.id] = campaignDetail.BatchObject.Desc_vch>
		<cfelse>
			<cfset campaignIds = []>
			<cfloop query="activeFeedQuery">
				<cfset arrayAppend(campaignIds, activeFeedQuery.BatchId_bi)>
			</cfloop>
			
			<cfquery name="campaignList" datasource="#Session.DBSourceREAD#">
				SELECT BatchId_bi, Desc_vch 
				FROM simpleobjects.batch
				WHERE BatchId_bi IN (#arrayToList(campaignIds, ',')#);
			</cfquery>
			
			<cfif campaignList.RecordCount GT 0>
				<cfloop query="campaignList">
					<cfset campaignNames[campaignList.BatchId_bi] = campaignList.Desc_vch>
				</cfloop>
			</cfif>
		</cfif>
	
	</cfif>
	
	
	<cfset activeFeeds =  QueryNew("Phone Number,Action,Message,Date")>
	
	
	<cfloop query="activeFeedQuery">
		<cfset action = "">
		<cfswitch expression="#log_type#">
			<cfdefaultcase>
				<cfswitch expression="#sms_type#">
						<cfcase value="1">
							<cfset action = "received"/>
						</cfcase>
						<cfcase value="2">
							<cfset action = "sent"/>
						</cfcase>
						<cfcase value="999">
							<cfset action = "cpp"/>
						</cfcase>
				</cfswitch>
			</cfdefaultcase>
			<cfcase value="1">
				<cfset action = "joined "/>
				<cfif arrayIsDefined(campaignNames, BatchId_bi)>
					<cfset action = action&#campaignNames[BatchId_bi]#/>	 
				</cfif>
				<cfset action = action&" Campaign"/>	 
			</cfcase>
			<cfcase value="2">
				<cfset action = "left your Campaign "/>	 
			</cfcase>
			
		</cfswitch>
		
		<cfset QueryAddRow(activeFeeds) />
		<cfset QuerySetCell(activeFeeds, "Phone Number", ContactString_vch) />
		<cfset QuerySetCell(activeFeeds, "Action", action) />
		<cfset QuerySetCell(activeFeeds, "Message", sms) />
		<cfset QuerySetCell(activeFeeds, "Date", "#DateFormat(dt, 'long')# #TimeFormat(dt, 'short')#" ) />
	</cfloop>

	<cfif activeFeeds.recordCount GT 0>
		<!---<cfspreadsheet action="update" filename="#ExpandPath('/')#session/sire/cache/downloads/#filename#" overwrite="true" password="password" query="activeFeeds" sheetname="ActiveFeed">--->
		<cfinvoke method="QueryToCSV" component="session.sire.models.cfc.admin" returnvariable="RetVarQueryToCSV">
	    	<cfinvokeargument name="Query" value="#activeFeeds#">
		    <cfinvokeargument name="CreateHeaderRow" value="true">
		    <cfinvokeargument name="Delimiter" value=",">
		</cfinvoke>
		<!--- <cffile action = "write"  file = "#ExpandPath('/')#session/sire/cache/downloads/#filename#"  output = "#RetVarQueryToCSV#" charset="utf-8"> --->

		<cfset ArrayAppend(listFile, #RetVarQueryToCSV#)>
		<cfset n += 1>
		<cfset SecurityCheck = SecurityCheck + 1>
			
		<!--- Sanity check to make sure we never end up in endless loop by accident - either need bigger loops or dont use this export tool --->
		<cfif SecurityCheck GT MaxLoops>  
			<cfoutput>Export Error - You have exceeded a reasonable number of loops - try increasing your iDisplayLength size</cfoutput>	
			<cfbreak/>					
		</cfif>	
	</cfif>
	
</cfloop>

<cfloop index="i" from="1" to="#n#">
	<cfoutput>
		#listFile[i]#
		<cfflush/>
	</cfoutput>
</cfloop>
