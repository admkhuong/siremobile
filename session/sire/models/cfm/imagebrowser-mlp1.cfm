<style type="text/css">
	body {
		background-color: white;
		overflow-x: hidden;
	}
	#page-content {
		padding: 0;
	}
	img.image:hover {
		cursor: pointer;
	}
	img.image {
	    
	}
	.page-title{
		font-weight: bold;
	}
	.voice-simon{
		margin-left: 25px;
	}

	#actions {
	    margin-left: 0px;
	}
	
	
	/* Mimic table appearance */
	div.table {
	    display: table;
	}
	div.table .file-row {
	    display: table-row;
	}
	div.table .file-row > div {
	    display: table-cell;
	    vertical-align: top;
	    border-top: 1px solid #ddd;
	    padding: 8px;
	}
	div.table .file-row:nth-child(odd) {
	    background: #f9f9f9;
	}
	
	
	
	/* The total progress gets shown by event listeners */
	#total-progress {
	    opacity: 0;
	    transition: opacity 0.3s linear;
	}
	
	/* Hide the progress bar when finished */
	#previews .file-row.dz-success .progress {
	    opacity: 0;
	    transition: opacity 0.3s linear;
	}
	
	/* Hide the delete button initially */
	#previews .file-row .delete {
	    display: none;
	}
	
	/* Hide the start and cancel buttons and show the delete button */
	
	#previews .file-row.dz-success .start,
	#previews .file-row.dz-success .cancel {
	    display: none;
	}
	#previews .file-row.dz-success .delete {
	    display: block;
	}
	
	.image-list-item {
	    position: relative;
	    margin-bottom: 5%;
	}
	
	.image-list-item img.selected {
	    box-shadow:0px 12px 22px 1px #333;
	}
	
	.image-list-item .image-delete {
	    position: absolute;
	    top: 0px;
	    left: 0px;
	    display: none;
	}
	
	@media only screen and (max-width: 1235px) {
	    div#previews {
	        max-width: 1100px;
	    }
	}
	
	@media only screen and (max-width: 1002px) {
	    div#previews {
	        max-width: 900px;
	    }
	}
	
	@media only screen and (max-width: 890px) {
	    p.name {
	        width: 400px;
	        word-wrap: break-word;
	        margin: 0px;
	    }
	}
	
	@media only screen and (max-width: 790px) {
	    p.name {
	        width: 300px;
	        word-wrap: break-word;
	        margin: 0px;
	    }
	}
	
	@media only screen and (max-width: 700px){
	    p.name {
	        width: 200px;
	        word-wrap: break-word;
	        margin: 0px;
	    }
	}
	
	@media only screen and (max-width: 540px){
	    p.name {
	        width: 100px;
	        word-wrap: break-word;
	        text-overflow: ellipsis;
	    }
	}
	
	@media only screen and (max-width: 430px){
	    p.name {
	        display: none;
	    }
	}
	
	span.preview img {
	    width: 100px;
	    height: 100px;
	}

</style>

<cfinvoke method="BrowseImage" component="session.sire.models.cfc.image" returnvariable="retValBrowseImage">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/image-browse.css">
</cfinvoke>


	<div class="row heading">
		<div class="col-sm-6 page-title" style="padding-left: 25px"></div>
	</div>
	<div class="row image-list" id="image-list">
		<cfif retValBrowseImage.RXRESULTCODE GT 0>
			<cfif arrayLen(retValBrowseImage.USERIMAGES) LT 1>
				<cfoutput>
					You haven't upload any image yet
				</cfoutput>
			<cfelse>
				<cfloop array="#retValBrowseImage.USERIMAGES#" index="image">
					<cfoutput>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 image-list-item">
							<img class="image" data-url="#image.IMAGEURL#" src="#image.IMAGETHUMB#" alt="#image.IMAGENAME#">
<!--- 							<button class="btn btn-small btn-danger image-delete" data-imageid="#image.IMAGEID#">Delete</button> --->
						</div>
					</cfoutput>
				</cfloop>
			</cfif>
		</cfif>
	</div>


<script type="text/javascript">
	function getUrlParam(paramName)
	{
		var reParam = new RegExp('(?:[\?&]|&amp;)' + paramName + '=([^&]+)') ;
		var match = window.location.search.match(reParam) ;
		return (match && match.length > 1) ? match[1] : '' ;
	}

	function selectFile( fileUrl )
	{
		$('#processPayment').show();
		if (typeof StoreMLPBeforeAction == 'function') 
			StoreMLPBeforeAction();
		
		<!--- presumes this is getting called in a bootsrap modal #ImageChooserModal and that it is bind(ed) to a valid img editor section --->
		var $TargetObj = $('#ImageChooserModal').data('data-target');
		
		<!--- make sure this obj exists --->		
		<!--- update img obj or background depeneding on where this is opened from  --->
		// if($('#ImageChooserModal').attr('data-image-target') == "img" && $TargetObj.find('img').length > 0)
		// {
		// 	$TargetObj.find('img').attr('src', fileUrl)				
		// }
		// else
		// {
		// 	$TargetObj.css('background-image', 'url(' + fileUrl + ')');	
		// 	$('#mlp-section-settings #mlp-background-image-url').html(fileUrl);			
		// }

		$('#campaign-mlp-logo').attr('src', fileUrl);
		$('#campaign-mlp-logo').attr('name', fileUrl);
		if (typeof(checkImageToConvert) != 'undefined'){
			checkImageToConvert();
		}
		
		$('#processPayment').hide();	
		$('#ImageChooserModal').modal('hide');
			
	}

	$('body').on('click', 'img.image', function(event) {
		event.preventDefault();
		selectFile($(this).data('url'));
	});

	$(document).ready(function() {
		$('#header').hide();
		$('footer').hide();
	});
</script>