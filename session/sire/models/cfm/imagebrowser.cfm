<style type="text/css">
	body {
		background-color: white;
		overflow-x: hidden;
	}
	#page-content {
		padding: 0;
	}
	img.image:hover {
		cursor: pointer;
	}
	img.image {
	    margin-left: 30px;
	}
	.page-title{
		font-weight: bold;
	}
	.voice-simon{
		margin-left: 25px;
	}
</style>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/image-manage.css">
</cfinvoke>
<br>
<cfinclude template="/session/sire/views/images/imagebrowse.cfm">

<cfinclude template="/session/sire/views/layouts/main.cfm">

<script type="text/javascript">
	function getUrlParam(paramName)
	{
		var reParam = new RegExp('(?:[\?&]|&amp;)' + paramName + '=([^&]+)') ;
		var match = window.location.search.match(reParam) ;
		return (match && match.length > 1) ? match[1] : '' ;
	}

	function selectFile( fileUrl )
	{
		var funcNum = getUrlParam('CKEditorFuncNum');
		window.opener.CKEDITOR.tools.callFunction(funcNum,fileUrl);
		window.close();
	}

	$('body').on('click', 'img.image', function(event) {
		event.preventDefault();
		selectFile($(this).data('url'));
	});

	$(document).ready(function() {
		$('#header').hide();
		$('footer').hide();
	});
</script>