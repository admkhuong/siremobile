<cfinclude template="/session/sire/configs/paths.cfm">
<cfinclude template="/session/sire/configs/env_paths.cfm">
<cfinclude template="/session/sire/configs/userConstants.cfm">

<cfset uploadPath = _USERUPLOADFOLDER />

<cfset funcNum = URL.CKEditorFuncNum />

<cftry>
	<!--- Upload file to server for temporary --->
	<cffile action="upload" fileField="upload" destination="#uploadPath#" accept="image/*" nameconflict="overwrite" result="uploadResult" mode="777">

	<cfif arrayIsEmpty(REmatch(_NAMEREG, uploadResult.serverfilename))>
		<cfthrow type="Object" message="Filename syntax error" detail="Filename syntax error!<br>Alphanumeric characters [0-9a-zA-Z] and Special characters -, _, (, ) accepted only">
	</cfif>

	<cffile action="rename" source="#uploadResult.serverdirectory#/#uploadResult.serverfile#" destination="#uploadResult.serverdirectory#/#uploadResult.serverfilename#.#lCase(uploadResult.clientfileext)#" attributes="normal">

	<cfset uploadResult.serverfile = uploadResult.serverfilename & '.' & lCase(uploadResult.clientfileext) />

	<cfif uploadResult.filesize GT _IMGMAXSIZE>
		<cffile action="delete" file="#uploadResult.serverdirectory#/#uploadResult.serverfile#" />

		<cfoutput>
			File too big! Max file size is #_IMGMAXSIZE/1048576#MB
		</cfoutput>
	<cfelseif !arrayContains(_ACCEPTEDIMAGETYPE, lCase(uploadResult.clientfileext))>
		<cfoutput>
			<cffile action="delete" file="#uploadResult.serverdirectory#/#uploadResult.serverfile#" />
			File type not accepted! (<cfloop array="#_ACCEPTEDIMAGETYPE#" index="type">#type#&nbsp;&nbsp;</cfloop>)
		</cfoutput>
	<cfelse>

		<cfinvoke method="GetUserStorageInfo" component="session.sire.models.cfc.image" returnvariable="retValUserUsage">
		</cfinvoke>

		<cfif retValUserUsage.RXRESULTCODE GT 0>
			<cfif (retValUserUsage.USERUSAGE + uploadResult.filesize) GT retValUserUsage.USERSTORAGEPLAN>
				<cfoutput>
					Storage limit exceeded!
				</cfoutput>
			<cfelse>
				<!--- Check file name --->
				<cfinvoke method="CheckImageName" component="session.sire.models.cfc.image" returnvariable="retValCheckFileName">
					<cfinvokeargument name="inpPlainFileName" value="#uploadResult.serverfilename#" />
					<cfinvokeargument name="inpFileType" value="#uploadResult.clientfileext#" />
				</cfinvoke>

				<cfif retValCheckFileName.RXRESULTCODE GT 0>
					<cfif retValCheckFileName.CHECKRESULT GT 0>
						<!--- File name is already existed, display a dialog for user to choose if they want to replace file or not --->
						<cfoutput>
							<script type="text/javascript">
								var box = window.parent.bootbox.dialog({
									message: "This file name is already existed!<br>Do you want to use the existing image or replace it?<br><br><i>Note: it might take a refresh to display correctly if you replace old image, cause of browser cache.</i",
									title: "Oops!",
									buttons: {
										success: {
											label: "Replace",
											className: "btn btn-medium btn-success-custom",
											callback: function() {
												// Upload file anyway, replace old file
												window.parent.$.ajax({
													url: '/session/sire/models/cfc/image.cfc?method=UploadImageToS3&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
													type: 'POST',
													dataType: 'json',
													beforeSend: function () {
														window.parent.$('##processingPayment').show();
													},
													data: {
														inpFullFileName: "#uploadResult.serverfile#",
														inpLocalPath: "#uploadResult.serverdirectory#/",
														inpFileSize: "#uploadResult.filesize#",
														inpImageType: "#uploadResult.clientfileext#",
														inpPlainFileName: "#uploadResult.serverfilename#"
													}
												})
												.done(function(data) {
													// callback function to display image
													if (parseInt(data.RXRESULTCODE) == 1) {
														window.parent.CKEDITOR.tools.callFunction(#funcNum#, data.IMAGEURL);
													} else {
														// upload failed
														var box = window.parent.bootbox.dialog({
															message: (data.MESSAGE == '' ? "Error when upload file! Please try again later" : data.MESSAGE),
															title: "Oops!",
															buttons: {
																success: {
																	label: "Ok",
																	className: "btn btn-medium btn-success-custom",
																	callback: function() {
																	}
																}
															},
														});
														window.parent.$('div.bootbox').css('z-index', '99999999');
													}
												})
												.error(function() {
													// upload failed
													var box = window.parent.bootbox.dialog({
														message: "Error when upload file! Please try again later",
														title: "Oops!",
														buttons: {
															success: {
																label: "Ok",
																className: "btn btn-medium btn-success-custom",
																callback: function() {
																}
															}
														},
													});
													window.parent.$('div.bootbox').css('z-index', '99999999');
												})
												.complete(function() {
													window.parent.$('##processingPayment').hide();
												});
											}
										},
										reuse: {
											label: "Use existing",
											className: "btn btn-medium btn-success-custom",
											callback: function() {
												window.parent.CKEDITOR.tools.callFunction(#funcNum#, '#retValCheckFileName.IMAGEURL#');
											}
										},
										cancel: {
											label: "Cancel",
											className: "btn btn-medium btn-primary btn-back-custom",
											callback: function() {
												// delete temp file
												try {
													window.parent.$.ajax({
														url: '/session/sire/models/cfc/image.cfc?method=ClearTempFile&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
														type: 'POST',
														data: {inpFilename: '#uploadResult.serverfile#'},
													})
													.done(function() {
													});
												} catch (e) {

												}
												// close dialog
												window.parent.$('span:contains("Cancel")').trigger('click');
											}
										}
									},
								});
								window.parent.$('div.bootbox').css('z-index', '99999999');
							</script>
						</cfoutput>
					<cfelse>
						<!--- File name not exist, upload to s3 --->
						<cfinvoke component="session.sire.models.cfc.image" method="UploadImageToS3" returnvariable="retValUploadImage">
							<cfinvokeargument name="inpFullFileName" value="#uploadResult.serverfile#">
							<cfinvokeargument name="inpLocalPath" value="#uploadResult.serverdirectory#/">
							<cfinvokeargument name="inpFileSize" value="#uploadResult.filesize#">
							<cfinvokeargument name="inpImageType" value="#uploadResult.clientfileext#">
							<cfinvokeargument name="inpPlainFileName" value="#uploadResult.serverfilename#">
						</cfinvoke>

						<cfif retValUploadImage.RXRESULTCODE GT 0>
							<!--- Upload success, callback function display image --->
							<cfoutput><script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction(#funcNum#, '#retValUploadImage.IMAGEURL#');</script></cfoutput>
						<cfelse>
							<cfoutput>
								<script type="text/javascript">
									// delete temp file
									try {
										window.parent.$.ajax({
											url: '/session/sire/models/cfc/image.cfc?method=ClearTempFile&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
											type: 'POST',
											data: {inpFilename: '#uploadResult.serverfile#'},
										})
										.done(function() {
										});
									} catch (e) {

									}
									// close dialog
									window.parent.$('span:contains("Cancel")').trigger('click');
									var box = window.parent.bootbox.dialog({
										message: "Error when upload file! Please try again later",
										title: "Oops!",
										buttons: {
											success: {
												label: "Ok",
												className: "btn btn-medium btn-success-custom",
												callback: function() {
												}
											}
										},
									});
									window.parent.$('div.bootbox').css('z-index', '99999999');
								</script>
							</cfoutput>
						</cfif>
					</cfif>
				<cfelse>
					<cfoutput>
						<script type="text/javascript">
							// delete temp file
							try {
								window.parent.$.ajax({
									url: '/session/sire/models/cfc/image.cfc?method=ClearTempFile&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
									type: 'POST',
									data: {inpFilename: '#uploadResult.serverfile#'},
								})
								.done(function() {
								});
							} catch (e) {

							}
							// close dialog
							window.parent.$('span:contains("Cancel")').trigger('click');
							var box = window.parent.bootbox.dialog({
								message: "Error when checking file! Please try again later",
								title: "Oops!",
								buttons: {
									success: {
										label: "Ok",
										className: "btn btn-medium btn-success-custom",
										callback: function() {
										}
									}
								},
							});
							window.parent.$('div.bootbox').css('z-index', '99999999');
						</script>
					</cfoutput>
				</cfif>
			</cfif>
		<cfelse>
			<cfoutput>
				<script type="text/javascript">
					// delete temp file
					try {
						window.parent.$.ajax({
							url: '/session/sire/models/cfc/image.cfc?method=ClearTempFile&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
							type: 'POST',
							data: {inpFilename: '#uploadResult.serverfile#'},
						})
						.done(function() {
						});
					} catch (e) {

					}
					// close dialog
					window.parent.$('span:contains("Cancel")').trigger('click');
					var box = window.parent.bootbox.dialog({
						message: "Error when checking file! Please try again later",
						title: "Oops!",
						buttons: {
							success: {
								label: "Ok",
								className: "btn btn-medium btn-success-custom",
								callback: function() {
								}
							}
						},
					});
					window.parent.$('div.bootbox').css('z-index', '99999999');
				</script>
			</cfoutput>
		</cfif>
	</cfif>

	<cfcatch type="any">
		<cfoutput>
			<script type="text/javascript">
				// delete temp file
				try {
					window.parent.$.ajax({
						url: '/session/sire/models/cfc/image.cfc?method=ClearTempFile&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
						type: 'POST',
						data: {inpFilename: '#uploadResult.serverfile#'},
					})
					.done(function() {
					});
				} catch (e) {

				}
				// close dialog
				window.parent.$('span:contains("Cancel")').trigger('click');
				var box = window.parent.bootbox.dialog({
					message: "#cfcatch.detail#",
					title: "Oops!",
					buttons: {
						success: {
							label: "Ok",
							className: "btn btn-medium btn-success-custom",
							callback: function() {
							}
						}
					},
				});
				window.parent.$('div.bootbox').css('z-index', '99999999');
			</script>
		</cfoutput>
	</cfcatch>
</cftry>
