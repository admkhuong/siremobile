<div class="row">
<cfif objectType EQ 'text-content'>
	<cfset objData = deserializeJSON(objData)>
	<div class="col-md-12">
		<div class="stepNote"><cfoutput>#objData[1].value#</cfoutput></div>
	</div>

</cfif>

<cfif objectType EQ 'sms-number'>
	<cfset objData = deserializeJSON(objData)>
	
	
		<div class="col-md-12">
			<div class="form-group">
			    <label for="sms-number"><cfoutput>#objData[1].value#</cfoutput></label>
			    <div class="input-group">
				    <input type="input" class="form-control validate[required,custom[usPhoneNumber]] cpp-object" name="sms-number-preview" placeholder="SMS Number">
				    <span class="input-group-addon remove-input-object" style="display:none"><i class="fa fa-minus"></i></span>
				    <span class="input-group-addon add-input-object"><i class="fa fa-plus-circle fa-lg"></i></span>

				</div>    
			</div>
		</div>
	
</cfif>

<cfif objectType EQ 'email-address'>
	<cfset objData = deserializeJSON(objData)>
	<div class="col-md-12">
		<div class="form-group">
		    <label for="email-address"><cfoutput>#objData[1].value#</cfoutput></label>
		    <div class="input-group">
			    <input type="input" class="validate[required,custom[email]] form-control cpp-object" name="email-address" placeholder="Email Address">
			    <span class="input-group-addon remove-input-object" style="display:none"><i class="fa fa-minus"></i></span>
			    <span class="input-group-addon add-input-object"><i class="fa fa-plus-circle fa-lg"></i></span>

			</div>    
		</div>
	</div>	

</cfif>

<cfif objectType EQ 'voice-number'>
	<cfset objData = deserializeJSON(objData)>
	<div class="col-md-12">
		<div class="form-group">
		    <label for="sms-number"><cfoutput>#objData[1].value#</cfoutput></label>
		    <div class="input-group">
			    <input type="input" class="form-control validate[required,custom[usPhoneNumber]] cpp-object" name="voice-number" placeholder="Voice Number">
			    <span class="input-group-addon remove-input-object" style="display:none"><i class="fa fa-minus"></i></span>
			    <span class="input-group-addon add-input-object"><i class="fa fa-plus-circle fa-lg"></i></span>

			</div>    
		</div>
	</div>	

</cfif>

<cfif objectType EQ 'subcriber-name'>
	<cfset objData = deserializeJSON(objData)>
		<cfset checkTotalSubList = objData[1].checkTotalSubList-1 >
		
		<cfif objData[1].checkTotalSubList GT 1>
			<cfloop index="index" from="0" to="#checkTotalSubList#">
				<div class="col-md-12 col-lg-6">
				<cfif objData[1].checkMultiSubList EQ 1>
		   			<div class="checkbox">
					    <label>
					    	<input type="checkbox" class="validate[minCheckbox[1]] cpp-object" name="subcriber-list"> <cfoutput>#objData[1]['value'][index]#</cfoutput>		
					    </label>
					</div>
				<cfelse>
					<div class="radio">
					    <label>
					    	<input type="radio" class="validate[minCheckbox[1]] cpp-object" name="subcriber-list"> <cfoutput>#objData[1]['value'][index]#</cfoutput>
					    </label>
					</div>
				</cfif>
			</div>	
			</cfloop>
		</cfif>
</cfif>


</div>