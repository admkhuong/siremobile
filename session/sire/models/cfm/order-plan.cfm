<cfset COUPONDISCOUNTTYPE = 0/>
<cfset PERCENT_CC = 0>
<cfset FLATRATE_CC = 0>
<cfset CREDIT_CC = 0>
<cfset KEYWORD_CC = 0>
<cfset MLP_CC = 0>
<cfset URL_CC = 0>

<cfset percent_dis = 0>
<cfset flatrate_dis = 0>
<cfset promotion_credit = 0>
<cfset promotion_mlp = 0>
<cfset promotion_keyword = 0>
<cfset promotion_shorturl = 0>

<cfset INP_PERCENT = 0 >
<cfset INP_FLATRATE = 0 >
<cfset INP_CREDIT = 0 >
<cfset INP_KEYWORD = 0 >
<cfset INP_MLP = 0 >
<cfset INP_URL = 0 >
<cfset userListToIncreasePromotionUsedTime = []>
<cfset promocodeListToIncreaseUsedTime = []>
<cfset promocodeListToIncreaseUsedTimeTemp = []>

<cfset promotionId = 0>
<cfset originId = 0>
<cfset recurringTime = 0>

<cfparam name="form.payment_method" default="0">
<cfparam name="form.line1" default="">
<cfparam name="form.planId" default="">
<cfparam name="form.city" default="">
<cfparam name="form.country" default="">
<cfparam name="form.cvv" default="">
<cfparam name="form.email" default="">
<cfparam name="form.expirationDate" default="">
<cfparam name="form.firstName" default="">
<cfparam name="form.lastName" default="">
<cfparam name="form.number" default="">
<cfparam name="form.phone" default="">
<cfparam name="form.state" default="">
<cfparam name="form.zip" default="">
<cfparam name='form.select_used_card' default="0">
<cfparam name='form.save_cc_info' default="0">
<cfparam name='form.listKeyword' default="">
<cfparam name='form.listMLP' default="">
<cfparam name='form.listShortUrl' default="">
<cfparam name='form.inpPromotionCode' default="">
<cfparam name='form.monthYearSwitch' default="1">
<cfparam name='form.payment_method_value' default="1">

<cfparam name='processPayment' default="0">
<cfparam name='usedPromotionCode' default="0">

<cfset email = form.email/>
<cfif form.select_used_card EQ 1>
	<cfif form.h_email_save EQ ''>
		<cfset email = form.h_email/>
	<cfelse>
		<cfset email = form.h_email_save/>
	</cfif>
<cfelse>
	<cfif form.email EQ ''>
		<cfset email = form.h_email/>
	</cfif>
</cfif>
<cfif (!structKeyExists(form,'number'))>
  <cfabort>
</cfif>
<cfquery name="checkExistPlan" datasource="#Session.DBSourceREAD#">
	SELECT PlanId_int,Order_int,Status_int FROM simplebilling.plans WHERE Status_int in(1,8,4,2) AND PlanId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#form.planId#"> 
</cfquery>

<cfif checkExistPlan.RecordCount EQ 0>
	<cfabort>
</cfif>

<cfinvoke method="GetOrderPlan" component="session/sire/models/cfc/order_plan" returnvariable="getPlan">
  <cfinvokeargument name="plan" value="#form.planId#"/>
</cfinvoke>

<cfinvoke method="GetUserPlan" component="session/sire/models/cfc/order_plan" returnvariable="getUserPlan"></cfinvoke>

<cfif getUserPlan.PLANORDER GTE checkExistPlan.Order_int AND getUserPlan.PLANSTATUS EQ 1 AND checkExistPlan.Status_int EQ 1 >
  <cfabort>
</cfif>

<cfset BuyKeywordNumber = 0>
<cfset form.PLANNAME = getPlan.PLANNAME>

<cfset TotalKeyword = getUserPlan.PLANKEYWORDSLIMITNUMBER + getUserPlan.KEYWORDPURCHASED > 

<cfif TotalKeyword GT getPlan.KEYWORDSLIMITNUMBER>
  <cfset BuyKeywordNumber = (TotalKeyword - getPlan.KEYWORDSLIMITNUMBER)>
</cfif>

<!--- <cfset form.monthYearSwitch = getUserPlan.BILLINGTYPE/> --->

<cfif form.monthYearSwitch EQ 1>
  <cfset totalAmount = getPlan.AMOUNT>
<cfelse>
  <cfset totalAmount = getPlan.YEARLYAMOUNT * 12>
</cfif>

<!--- UPGRADE PLAN HANDLE AMOUNT --->
  <cfif form.inpPromotionCode NEQ ''>
      <cfinvoke component="session.sire.models.cfc.promotion" method="CheckCouponForUpgradePlan" returnvariable="checkpromocode">
          <cfinvokeargument name="inpCouponCode" value="#form.inpPromotionCode#">
      </cfinvoke>

      <cfif checkpromocode.RXRESULTCODE EQ 1>
        
        <!--- coupon input --->
        <cfinvoke component="session.sire.models.cfc.promotion" method="GetCouponDetails" returnvariable="promocode">
            <cfinvokeargument name="inpCouponId" value="#checkpromocode.promotion_id#">
        </cfinvoke>
        
        <cfset promotionId = promocode.PROMOTIONID>
        <cfset originId = promocode.ORIGINID>
        <cfset recurringTime = promocode.COUPONRECURRING>

        <cfset INP_PERCENT = promocode.COUPONDISCOUNTPRICEPERCENT>
        <cfset INP_FLATRATE = promocode.COUPONDISCOUNTPRICEFLATRATE >
        <cfset INP_CREDIT = promocode.COUPONCREDIT>
        <cfset INP_KEYWORD = promocode.COUPONKEYWORD>
        <cfset INP_MLP = promocode.COUPONMLP>
        <cfset INP_URL = promocode.COUPONSHORTURL>
      </cfif>
  </cfif>

  <!--- Promotion benefit --->
  <cfinvoke component="session.sire.models.cfc.promotion" method="CheckAvailableCouponForUser" returnvariable="CouponForUser">
     <cfinvokeargument name="inpUserId" value="#session.UserId#">
  </cfinvoke>

  <!--- Check CouponForUser --->
  <cfif CouponForUser.rxresultcode EQ 1>
  <cfloop array="#CouponForUser['DATALIST']#" index="index">
   <cfif index.COUPONDISCOUNTTYPE EQ 1>
    <cfset COUPONDISCOUNTTYPE = 1>
    <cfset percent_dis = percent_dis + index.COUPONDISCOUNTPRICEPERCENT>
    <cfset flatrate_dis = flatrate_dis + index.COUPONDISCOUNTPRICEFLATRATE>
    <cfset promotion_id = index.PROMOTIONID>
    <cfset promotion_origin_id = index.ORIGINID>
    <!--- <cfset promotion_used_time = index.USEDTIME> --->
   <cfelseif index.COUPONDISCOUNTTYPE EQ 2>
    <cfset COUPONDISCOUNTTYPE = 2>
    <cfset promotion_id = index.PROMOTIONID>
    <cfset promotion_origin_id = index.ORIGINID>
    <!--- <cfset promotion_used_time = index.USEDTIME> --->
    <cfset promotion_credit = promotion_credit + index.COUPONCREDIT>
    <cfset promotion_mlp = promotion_mlp + index.COUPONMLP>
    <cfset promotion_keyword = promotion_keyword + index.COUPONKEYWORD>
    <cfset promotion_shorturl = promotion_shorturl + index.COUPONSHORTURL>
   <cfelse>
    <cfset promotion_id = index.PROMOTIONID>
    <cfset promotion_origin_id = index.ORIGINID>
    <!--- <cfset promotion_used_time = index.USEDTIME> --->
   </cfif>

   <cfset arrayAppend(promocodeListToIncreaseUsedTimeTemp, [promotion_origin_id, promotion_id, session.UserId])/>
  </cfloop>

  <cfelse>
  <cfset promotion_id = 0>
  <cfset promotion_origin_id = 0>
  </cfif>

  
  <cfset PERCENT_CC = INP_PERCENT + percent_dis>
  <cfset FLATRATE_CC = INP_FLATRATE + flatrate_dis>
  <cfset CREDIT_CC = INP_CREDIT>
  <cfset KEYWORD_CC = INP_KEYWORD + promotion_keyword>
  <cfset MLP_CC = INP_MLP + promotion_mlp>
  <cfset URL_CC = INP_URL + promotion_shorturl>
  <cfset purchaseAmount = (totalAmount - (totalAmount * INP_PERCENT / 100) - INP_FLATRATE)>

  <!--- add amount when last plan is postpaid--->
  <cfif getUserPlan.PLANSTATUS EQ 4>
      <cfquery name="TotalSentReceived" datasource="#Session.DBSourceREAD#">
          SELECT                        
              SUM(IF(ire.IREType_int = 1, 1, 0)) AS Sent,
              SUM(IF(ire.IREType_int = 2, 1, 0)) AS Received
          FROM
              simplexresults.ireresults as ire 
          LEFT JOIN
              simpleobjects.useraccount as ua
          ON
              ire.UserId_int = ua.UserId_int					
          WHERE
              LENGTH(ire.ContactString_vch) < 14
          AND 
              ua.UserId_int  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#" >	
          AND
              ua.IsTestAccount_ti = 0                    
          AND
              ire.Created_dt
          BETWEEN 
              <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#getUserPlan.STARTDATE#">
          AND 
              
              DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#getUserPlan.ENDDATE#">, INTERVAL 86399 SECOND)
          GROUP BY
              ire.UserId_int     
      </cfquery>
      <cfif TotalSentReceived.RecordCount GT 0>          
          <cfset purchaseAmount=purchaseAmount+  	(LSParseNumber(TotalSentReceived.Sent) + LSParseNumber(TotalSentReceived.Received) )*getUserPlan.PRICEMSGAFTER/100 />							                                      
      </cfif>	                            
  </cfif>



<!--- END UPGRADE PLAN HANDLE AMOUNT --->


<cfset txtUpgradePlan =  "Ugraded Plan to: "&getPlan.PLANNAME>

<!--- check to reactive keyword --->
<cfif form.listKeyword NEQ ''> 
  <cfset form.listKeyword = listToArray(form.listKeyword,',')> 
<cfelse>
   <cfset form.listKeyword = []/>
</cfif>

<cfif form.listShortUrl NEQ ''> 
  <cfset form.listShortUrl = listToArray(form.listShortUrl,',')> 
<cfelse>
   <cfset form.listShortUrl = []/>
</cfif>

<cfif form.listMLP NEQ ''> 
  <cfset form.listMLP = listToArray(form.listMLP,',')> 
<cfelse>
   <cfset form.listMLP = []/>
</cfif>


<cftry>
  
<cfinclude template="../../configs/credits.cfm">

<cfif purchaseAmount GT 0>
  <cfset processPayment = 1 >
</cfif>  

<cfset planCredits = getPlan.FIRSTSMSINCLUDED>

<!--- BEGIN: IF USER DO NOT HAVE TO PAYMENT -> just update db --->
<cfif processPayment EQ 0 >
    <!--- Update Plan and Billing--->
    <cftransaction>
      <cftry>
        <cftransaction action="begin">
            
           <!--- Insert new plan & add benefit from plan & coupon --->
              <cfinvoke method="upgradeUserPlan" component="session.sire.models.cfc.order_plan">
                <cfinvokeargument name="inpUserId" value="#Session.USERID#">
                <cfinvokeargument name="inpPlanData" value="#getPlan#">
                <cfinvokeargument name="inpTotalAmount" value="#totalAmount#">
                <cfinvokeargument name="inpBuyKeywordNumber" value="#BuyKeywordNumber#">
                <cfinvokeargument name="inpKEYWORD_CC" value="#KEYWORD_CC#">
                <cfinvokeargument name="inpMLP_CC" value="#MLP_CC#">
                <cfinvokeargument name="inpURL_CC" value="#URL_CC#">
                <cfinvokeargument name="inpCREDIT_CC" value="#CREDIT_CC#">
                <cfinvokeargument name="inpMonthYearSwitch" value="#form.monthYearSwitch#">
              </cfinvoke>

            <!--- New Promocode User --->
                <cfinvoke component="session.sire.models.cfc.promotion" method="insertUserPromoCode" returnvariable="newUserPromoCode">
                  <cfinvokeargument name="inpUserId" value="#session.UserId#">
                  <cfinvokeargument name="inpPromoId" value="#promotionId#">
                  <cfinvokeargument name="inpPromoOriginId" value="#originId#">
                  <cfinvokeargument name="inpUsedDate" value="#Now()#">
                  <cfinvokeargument name="inpRecurringTime" value="#recurringTime#">
                  <cfinvokeargument name="inpPromotionStatus" value="1">
                </cfinvoke>
              <!---END New Promocode User --->

              <!--- update used time coupon --->
              <cfinvoke method="UpdateUserPromoCodeUsedTime" component="session.sire.models.cfc.promotion">
                  <cfinvokeargument name="inpUserId" value="#session.UserId#">
                  <cfinvokeargument name="inpUsedFor" value="3">
                  <cfinvokeargument name="inpPromotionId" value="#promotionId#">
                  <cfinvokeargument name="inpLimit" value="1">
              </cfinvoke>
              <!--- en update used time --->

              <!--- Insert coupon code used log --->
              <cfinvoke method="InsertPromotionCodeUsedLog" component="public.sire.models.cfc.promotion">
                  <cfinvokeargument name="inpCouponId" value="#promotionId#"/>
                  <cfinvokeargument name="inpOriginCouponId" value="#originId#"/>
                  <cfinvokeargument name="inpUserId" value="#session.UserId#"/>
                  <cfinvokeargument name="inpUsedFor" value="Upgrade Plan"/>
              </cfinvoke>    

              <cfinvoke method="IncreaseCouponUsedTime" component="public.sire.models.cfc.promotion">
                  <cfinvokeargument name="inpCouponId" value="#originId#"/>
                  <cfinvokeargument name="inpUsedFor" value="2"/>
              </cfinvoke>

            <!--- Sendmail payment --->
            <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                  <cfinvokeargument name="to" value="#email#">
                  <cfinvokeargument name="type" value="html">
                  <cfinvokeargument name="subject" value="[SIRE][Order Plan] Payment Info">
                  <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_order_plan.cfm">
                  <cfinvokeargument name="data" value="">
            </cfinvoke>
  
             
            <!--- Add Userlog --->
            <cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
                  <cfinvokeargument name="userId" value="#session.userid#">
                  <cfinvokeargument name="moduleName" value="Payment Processing">
                  <cfinvokeargument name="operator" value="Securenet Payment Executed OK! Upgrade Plan for $#purchaseAmount# - plan #getplan.PLANNAME# - planID #getplan.PLANID# - plan type #form.monthYearSwitch#">
            </cfinvoke>

            <!--- ADD CURRENT REFERRAL CREDITS TO BALANCE --->
            <cfinvoke method="AddReferralCredits" component="session.sire.models.cfc.referral" returnvariable="RetAddReferralCredits">
              <cfinvokeargument name="inpUserId" value="#SESSION.USERID#">
            </cfinvoke>

               <!--- active user keywords, mlp, short url --->
              <cfif arrayLen(form.listKeyword) GT 0 || arrayLen(form.listShortUrl) GT 0 || arrayLen(form.listMLP) GT 0 >
                <cfinvoke component="session.sire.models.cfc.order_plan" method="activeUserData" returnvariable="resultActiveUserData">
                    <cfinvokeargument name="inpListKeyword" value="#form.listKeyword#">
                    <cfinvokeargument name="inpListShortUrl" value="#form.listShortUrl#">
                    <cfinvokeargument name="inpListMLP" value="#form.listMLP#">
                </cfinvoke>
              </cfif>

            <cfset dataout.ID = "1">
            <cfset dataout.MSG = "Transaction Complete. Thank you!">
            
          <cftransaction action="commit">
          <cfcatch type="any">
              <cftransaction action="rollback">
                <cfset dataout.ID = "-1">
                <cfset dataout.MSG = "Transaction Failed!">
                <cfset dataout.ERRMSG =  cfcatch.message & " " & cfcatch.detail>
          </cfcatch>
      </cftry>
    </cftransaction>
    <cfoutput>#serializeJSON(dataout)#</cfoutput>
    <cfexit>
</cfif>
<!--- END: IF USER DO NOT HAVE TO PAYMENT --->

<cfif form.select_used_card EQ 1> <!--- USE SAVED TOKEN TO PAYMENT --->
  <cfinvoke component="session.sire.models.cfc.billing" method="getUserAuthenInfo" returnvariable="GetUserAuthen"></cfinvoke> 

  <cfif GetUserAuthen.RXRESULTCODE EQ 1 AND structKeyExists(GetUserAuthen.DATA, "PaymentMethodID_vch") AND GetUserAuthen.DATA.PaymentMethodID_vch NEQ ''  >
    <cfset customerId = #session.USERID#>
    <cfset paymentMethodId = #GetUserAuthen.DATA.PaymentMethodID_vch#>
    <cfset paymentTypeShortName = #GetUserAuthen.DATA.PaymentType_vch#>

    <cftry>
      <cfset paymentType = StructFind(worldpayPaymentTypes, paymentTypeShortName)>
    <cfcatch>
      <cfset paymentType = worldpayPaymentTypes.UNKNOWN>
    </cfcatch>  
    </cftry>

  <cfelse>
      <cfset dataout.ID="-4">
      <cfset dataout.MSG="We're Sorry! Unable to get your CreditCard info.Please update in [My Plan] page.">
  </cfif>

  <cfset paymentData = {
    amount = purchaseAmount,
    transactionDuplicateCheckIndicator = 1,
    extendedInformation = {
        notes = txtUpgradePlan,
    },
    paymentVaultToken = {  
       customerId = customerId,
       paymentMethodId = paymentMethodId,
       paymentType = paymentType
    },
    developerApplication = worldpayApplication
  }>
<cfelse>  
  <cfset paymentData = {
    amount = purchaseAmount,
    transactionDuplicateCheckIndicator = 1,
    extendedInformation = {
        notes = txtUpgradePlan,
    },
    developerApplication = {
      developerId: worldpayApplication.developerId,
      version: worldpayApplication.version
    }
  }>

  <cfif form.payment_method EQ 1>
  <cfset paymentData.check = {
      routingNumber = form.number,
      accountNumber = form.cvv,
      firstName   = form.firstName,
      lastName    = form.lastName,
      address     = {
        line1   = form.line1,
        city  = form.city,
        state   = form.state,
        zip   = form.zip,
        country = form.country,
        phone = form.phone
      },
      email     = form.email

    }>
  <cfelse>
  <cfset paymentData.card = {
      number      = form.number,
      cvv       = form.cvv,
      expirationDate  = form.expirationDate,
      firstName   = form.firstName,
      lastName    = form.lastName,
      address     = {
        line1   = form.line1,
        city  = form.city,
        state   = form.state,
        zip   = form.zip,
        country = form.country,
        phone = form.phone,
      },
      email     = form.email,
      emailReceipt = false
    }>
  </cfif>
</cfif>

<cfset closeBatchesData = {
        developerApplication = {
          developerId: worldpayApplication.developerId,
          version: worldpayApplication.version
        }
      }>

<cfhttp url="#payment_url#" method="post" result="paymentRespose" port="443">
  <cfhttpparam type="header" name="content-type" value="application/json">
  <cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
  <cfhttpparam type="body" value='#TRIM( serializeJSON(paymentData) )#'>
</cfhttp>


<cfset dataout = {} />
<cfset dataout.ID = "-1">
<cfset dataout.MSG = "We're Sorry! Unable to complete your transaction at this time. Transaction Failed.">

<cfif paymentRespose.status_code EQ 200>
  <cfif trim(paymentRespose.filecontent) NEQ "" AND isJson(paymentRespose.filecontent)>
    <cfset paymentResposeContent = deserializeJSON(paymentRespose.filecontent)>
    <cfset paymentResposeContent.planname = getplan.PLANNAME/>

    <cfset purchaseType = '#NumberFormat(getPlan.FIRSTSMSINCLUDED)# Credit Purchase for $#NumberFormat(purchaseAmount)# USD'>
    
      <cfif paymentResposeContent.success>
        <cfset dataout.ID = "1">
        <cfset dataout.MSG = "Transaction Complete. Thank you!">
        
        <!--- close session--->
        <cfhttp url="#close_batches_url#" method="post" result="closeBatchesRequestResult" port="443">
        <cfhttpparam type="header" name="content-type" value="application/json">
        <cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
        <cfhttpparam type="body" value='#TRIM( serializeJSON(closeBatchesData) )#'>
        </cfhttp>

        <!--- Update Plan and Billing--->
        <cftransaction>
          <cftry>
            <cftransaction action="begin">
              
                  <!--- Insert new plan & add benefit from plan & coupon --->
                  <cfinvoke method="upgradeUserPlan" component="session.sire.models.cfc.order_plan">
                    <cfinvokeargument name="inpUserId" value="#Session.USERID#">
                    <cfinvokeargument name="inpPlanData" value="#getPlan#">
                    <cfinvokeargument name="inpTotalAmount" value="#totalAmount#">
                    <cfinvokeargument name="inpBuyKeywordNumber" value="#BuyKeywordNumber#">
                    <cfinvokeargument name="inpKEYWORD_CC" value="#KEYWORD_CC#">
                    <cfinvokeargument name="inpMLP_CC" value="#MLP_CC#">
                    <cfinvokeargument name="inpURL_CC" value="#URL_CC#">
                    <cfinvokeargument name="inpCREDIT_CC" value="#CREDIT_CC#">
                    <cfinvokeargument name="inpMonthYearSwitch" value="#form.monthYearSwitch#">
                  </cfinvoke>

                  <!--- New Promocode User --->
                    <cfinvoke component="session.sire.models.cfc.promotion" method="insertUserPromoCode" returnvariable="newUserPromoCode">
                      <cfinvokeargument name="inpUserId" value="#session.UserId#">
                      <cfinvokeargument name="inpPromoId" value="#promotionId#">
                      <cfinvokeargument name="inpPromoOriginId" value="#originId#">
                      <cfinvokeargument name="inpUsedDate" value="#Now()#">
                      <cfinvokeargument name="inpRecurringTime" value="#recurringTime#">
                      <cfinvokeargument name="inpPromotionStatus" value="1">
                    </cfinvoke>
                  <!---END New Promocode User --->

                  <!--- update used time coupon --->
                  <cfinvoke method="UpdateUserPromoCodeUsedTime" component="session.sire.models.cfc.promotion">
                      <cfinvokeargument name="inpUserId" value="#session.UserId#">
                      <cfinvokeargument name="inpUsedFor" value="3">
                      <cfinvokeargument name="inpPromotionId" value="#promotionId#">
                      <cfinvokeargument name="inpLimit" value="1">
                  </cfinvoke>
                  <!--- en update used time --->

                  <!--- Insert coupon code used log --->
                  <cfinvoke method="InsertPromotionCodeUsedLog" component="public.sire.models.cfc.promotion">
                      <cfinvokeargument name="inpCouponId" value="#promotionId#"/>
                      <cfinvokeargument name="inpOriginCouponId" value="#originId#"/>
                      <cfinvokeargument name="inpUserId" value="#session.UserId#"/>
                      <cfinvokeargument name="inpUsedFor" value="Upgrade Plan"/>
                  </cfinvoke>    

                  <cfinvoke method="IncreaseCouponUsedTime" component="public.sire.models.cfc.promotion">
                      <cfinvokeargument name="inpCouponId" value="#originId#"/>
                      <cfinvokeargument name="inpUsedFor" value="2"/>
                  </cfinvoke>

                <!--- Add payment --->
                <cfinvoke method="updatePaymentWorlDay" component="session.sire.models.cfc.order_plan">
                  <cfinvokeargument name="planId" value="#form.planId#">
                  <cfinvokeargument name="numberKeyword" value="">
                  <cfinvokeargument name="numberSMS" value="">
                  <cfinvokeargument name="moduleName" value="By Plan">
                  <cfinvokeargument name="paymentRespose" value="#paymentRespose.filecontent#">
                </cfinvoke>
                  
                <!--- Add Userlog --->
                <cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
                  <cfinvokeargument name="userId" value="#session.userid#">
                  <cfinvokeargument name="moduleName" value="Payment Processing">
                  <cfinvokeargument name="operator" value="Securenet Payment Executed OK! Upgrade Plan for $#purchaseAmount# - plan #getplan.PLANNAME# - planID #getplan.PLANID# - plan type #form.monthYearSwitch#">
                </cfinvoke>

                <!--- CREATE ACCOUNT ON SERCUNET --->
                <cfif form.select_used_card EQ 0 OR form.save_cc_info EQ 1 > <!--- If do not have account or choice save then create --->
                  <cfinvoke component="session.sire.models.cfc.billing" method="RetrieveCustomer" returnvariable="RetCustomerInfo"></cfinvoke>  
                  <cfif RetCustomerInfo.RXRESULTCODE EQ 1>
                    <cfset primaryPaymentMethodId = 0> <!--- do not print --->
                  <cfelse>  
                    <cfset primaryPaymentMethodId = 1> <!--- do not print --->
                  </cfif>
                   <cfinvoke method="UpdateCustomer" component="session.sire.models.cfc.billing">
                      <cfinvokeargument name="payment_method" value="#form.payment_method#">
                      <cfinvokeargument name="line1" value="#form.line1#">
                      <cfinvokeargument name="city" value="#form.city#">
                      <cfinvokeargument name="country" value="#form.country#">
                      <cfinvokeargument name="phone" value="#form.phone#">
                      <cfinvokeargument name="state" value="#form.state#">
                      <cfinvokeargument name="zip" value="#form.zip#">
                      <cfinvokeargument name="email" value="#form.email#">
                      <cfinvokeargument name="cvv" value="#form.cvv#">
                      <cfinvokeargument name="expirationDate" value="#form.expirationDate#">
                      <cfinvokeargument name="firstName" value="#form.firstName#">
                      <cfinvokeargument name="lastName" value="#form.lastName#">
                      <cfinvokeargument name="number" value="#form.number#">
                      <cfinvokeargument name="primaryPaymentMethodId" value="#primaryPaymentMethodId#">
                  </cfinvoke>
                </cfif>  

                <!--- ADD CURRENT REFERRAL CREDITS TO BALANCE --->
                <cfinvoke method="AddReferralCredits" component="session.sire.models.cfc.referral" returnvariable="RetAddReferralCredits">
                  <cfinvokeargument name="inpUserId" value="#SESSION.USERID#">
                </cfinvoke>
                
                <cfinvoke method="mailChimpAPIs" component="public.sire.models.cfc.mailchimp" returnvariable="RetVarmailChimpAPIs">
                    <cfinvokeargument name="InpEmailString" value="#Session.EmailAddress#">
                    <cfinvokeargument name="InpAccountType" value="#form.PLANNAME#">         
                </cfinvoke>

                  <!--- active user keywords, mlp, short url --->
                <cfif arrayLen(form.listKeyword) GT 0 || arrayLen(form.listShortUrl) GT 0 || arrayLen(form.listMLP) GT 0 >
                    <cfinvoke component="session.sire.models.cfc.order_plan" method="activeUserData" returnvariable="resultActiveUserData">
                        <cfinvokeargument name="inpListKeyword" value="#form.listKeyword#">
                        <cfinvokeargument name="inpListShortUrl" value="#form.listShortUrl#">
                        <cfinvokeargument name="inpListMLP" value="#form.listMLP#">
                    </cfinvoke>
                </cfif>

                <!--- Sendmail payment --->
                <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                  <cfinvokeargument name="to" value="#email#">
                  <cfinvokeargument name="type" value="html">
                  <cfinvokeargument name="subject" value="[SIRE][Order Plan] Payment Info">
                  <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_order_plan.cfm">
                  <cfinvokeargument name="data" value="#serializeJSON(paymentResposeContent)#">
                </cfinvoke>
                
                <cftransaction action="commit">
              <cfcatch type="any">
                  <cftransaction action="rollback">
                    <cfset dataout.ID = "-1">
                    <cfset dataout.MSG = "Transaction Failed!">
                    <cfset dataout.ERRMSG =  cfcatch.message & " " & cfcatch.detail>
              </cfcatch>
          </cftry>
        </cftransaction>
      </cfif>
  </cfif>
<cfelse>

  <!--- Log activity --->
  <cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
        <cfinvokeargument name="userId" value="#session.userid#">
        <cfinvokeargument name="moduleName" value="Payment Processing">
        <cfinvokeargument name="operator" value="Securenet Execute Transaction Failed!">
    </cfinvoke>
</cfif>
  <!--- Log payment --->
  
<cftry>
    <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
      <cfinvokeargument name="moduleName" value="Buy Plan">
      <cfinvokeargument name="status_code" value="#paymentRespose.status_code#">
      <cfinvokeargument name="status_text" value="#paymentRespose.status_text#">
      <cfinvokeargument name="errordetail" value="#paymentRespose.errordetail#">
      <cfinvokeargument name="filecontent" value="#paymentRespose.filecontent#">
      <cfinvokeargument name="paymentdata" value="#paymentData#">
    </cfinvoke>
    <cfcatch type="any">
    </cfcatch>
  </cftry>

<cfcatch type="any">
	<cftry>
    	<!--- Log bug to file /session/sire/logs/bugs/payment  --->
		<cfif !isDefined('variables._Response')>
			<cfset variables._PageContext = GetPageContext()>
			<!--- Coldfusion --->
			<!---<cfset variables._Response = variables._PageContext.getCFOutput()>--->
			<cfset variables._Response = variables._PageContext.getOut()>
		</cfif>
		<cfset variables._Response.clear()>
		

		<cfset catchContent = variables._Response.getString()>
		<cfset variables._Response.clear()>
    	
    	<cffile action="write" output="#catchContent#"
    	file="../../logs/bugs/payment/securenet_payment_error_#REReplace('#now()#', '\W', '_', 'all')#.txt" 
    	>
         <cfcatch type="any">
        </cfcatch>

    </cftry>
  <cfset dataout.ID="-3">
  <cfset dataout.MSG="We're Sorry! Unable to complete your transaction at this time. Transaction Failed.">
  <cfset dataout.ERRMSG =  cfcatch.message & " " & cfcatch.detail>
</cfcatch> 
</cftry>

<cfoutput>#serializeJSON(dataout)#</cfoutput>