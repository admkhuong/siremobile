<cfinclude template="../../configs/credits.cfm">

<cfparam name="url.token" default="">
<cfparam name="url.PayerID" default="">
<cfparam name="url.action" default="">

<cfset dataout ={}/>
<cfset dataout.RESULT = 0/>
<cfset dataout.MSG = ''/>
<cfset redirectUrl = '/'/>
<cfset billingAgreementId =''/>



<cfif url.token NEQ ''>
	<cfinvoke component="session.sire.models.cfc.billing" method="DoCreateBillingAgreement" returnvariable="DoCreateBillingAgreementResult">
		<cfinvokeargument name="inptoken" value="#url.token#">
	</cfinvoke>
	<cfif DoCreateBillingAgreementResult.RXRESULTCODE EQ 1>
			<cfinvoke method='updateUserAuthen' component="session.sire.models.cfc.billing" returnvariable="updateUserAuthenResult">
				<cfinvokeargument name="PaymentMethodID" value="#URLDecode(DoCreateBillingAgreementResult.RESULT)#">
				<cfinvokeargument name="PaymentType" value="PP">
				<cfinvokeargument name="intPaymentGateway" value="2">
			</cfinvoke>
			<cfif updateUserAuthenResult.RXRESULTCODE EQ 1>
				<cfset dataout.RESULT = 1/>	
				<cfset dataout.MSG = 'Update billing agreement successfully'/>
			<cfelse>	
				<cfset dataout.RESULT = -21/>	
				<cfset dataout.MSG = 'Update billing agreement failed.'/>
			</cfif>
		<cfelse>
			<cfset dataout.RESULT = -2/>
			<cfset dataout.MSG = 'Payment failed (ErrCode:#dataout.RESULT#)'/>
	</cfif>
<cfelse>
	<cfset dataout.RESULT = -3/>
	<cfset dataout.MSG = 'Get api failed (ErrCode:#dataout.RESULT#)'/>	
</cfif>

<cfset redirectUrl = "/session/sire/pages/billing?action=update_billing&result="&#dataout.RESULT#&"&msg="&#urlencode(dataout.MSG)#/>
<cflocation url="#redirectUrl#" addtoken="false"/>

 