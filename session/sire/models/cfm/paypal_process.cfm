<cfinclude template="../../configs/credits.cfm">

<cfparam name="url.token" default="">
<cfparam name="url.PayerID" default="">
<cfparam name="url.action" default="">
<cfparam name="url.numberSMSToSend" default="">
<cfparam name="url.numberKeywordToSend" default="">

<cfparam name="url.planId" default="">
<cfparam name='url.listKeyword' default="">
<cfparam name='url.listMLP' default="">
<cfparam name='url.listShortUrl' default="">
<cfparam name='url.inpPromotionCode' default="">
<cfparam name='url.monthYearSwitch' default="1">
<cfparam name='url.userId' default="0">

<cfparam name='url.type' default="0">
<cfparam name='url.value' default="0">
<cfparam name='url.userEmail' default="">

<!--- above $99--->
<cfparam name='url.inpPaymentID' default="0">


<cfset dataout ={}/>
<cfset dataout.RESULT = 0/>
<cfset dataout.MSG = ''/>
<cfset redirectUrl = '/'/>
<cfset inpModuleNamevalue = ''>
<cfif url.type EQ '8' OR url.type EQ '9'>
	<cfset inpModuleNamevalue = 'Admin Buy Credits Keywords Plan'>
<cfelseif url.type EQ '7'>
	<cfset inpModuleNamevalue = 'Accounts management - Edit plan'>
</cfif>

<cfif url.monthYearSwitch EQ ''>
	<cfset url.monthYearSwitch = 1>
</cfif>	

<cfif url.token NEQ '' AND url.PayerID NEQ ''>
	<cfinvoke component="session.sire.models.cfc.billing" method="GetExpressCheckoutDetails" returnvariable="GetExpressCheckoutDetailsResult">
		<cfinvokeargument name="inptoken" value="#url.token#">
	</cfinvoke> 

	<cfif GetExpressCheckoutDetailsResult.RXRESULTCODE EQ 1>
		<cfset getExpressToken = URLDecode(GetExpressCheckoutDetailsResult.TOKEN)>
		<cfset getExpressPayerId = URLDecode(GetExpressCheckoutDetailsResult.PAYERID)>
		<cfset getExpressAmount = URLDecode(GetExpressCheckoutDetailsResult.PAYMENTREQUEST_0_AMT) >
		<!--- BUY CREDITS & KEYWORD --->
		<cfif action EQ 'buy_addon'>
			<cfinvoke component="session.sire.models.cfc.billing" method="DoExpressCheckoutPayment" returnvariable="DoExpressCheckoutPaymentResult">
				<cfinvokeargument name="inptoken" value="#getExpressToken#">
				<cfinvokeargument name="inpPayerId" value="#getExpressPayerId#">
				<cfinvokeargument name="inpAMT" value="#getExpressAmount#">	
				<cfinvokeargument name="numberKeywordToSend" value="#url.numberKeywordToSend#">
				<cfinvokeargument name="numberSMSToSend" value="#url.numberSMSToSend#">	
				<cfinvokeargument name="inpModuleName" value="Buy Credits and Keywords">	
			</cfinvoke>

			<cfif DoExpressCheckoutPaymentResult.RXRESULTCODE EQ 1>
				<!--- ADD CREDIT & KEYWORD FOR USER --->
				<cfinvoke component="session.sire.models.cfc.billing" method="DoAddPurchasedCreditKeyword" returnvariable="DoAddPurchasedCreditKeywordResult">
					<cfinvokeargument name="inpNumberSMS" value="#url.numberSMSToSend#">
					<cfinvokeargument name="inpNumberKeyword" value="#url.numberKeywordToSend#">
				</cfinvoke>
				<cfif DoAddPurchasedCreditKeywordResult.RXRESULTCODE EQ 1>
					<!--- UPDATE BILLINGAGREEMENTID --->
					<cfinvoke component="session.sire.models.cfc.billing" method="updateUserAuthen" returnvariable="updateUserAuthenResult">
						<cfinvokeargument name="PaymentMethodID" value="#DoExpressCheckoutPaymentResult['BILLINGAGREEMENTID']#">
						<cfinvokeargument name="PaymentType" value="PP">
				        <cfinvokeargument name="intPaymentGateway" value="2">
					</cfinvoke>

					<cfif SESSION.EMAILADDRESS NEQ ''>
						<cfset emailData ={}/>
						<cfset emailData['FullName'] = SESSION.FULLNAME/>
						<cfset emailData['numberKeywords'] =  url.numberKeywordToSend />
						<cfset emailData['numberSMS'] = url.numberSMSToSend/>
						<cfset emailData['authorizedAmount'] = getExpressAmount/>
						<cfset emailData['transactionId'] = DoExpressCheckoutPaymentResult['TRANSACTIONID']/>

						<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
				            <cfinvokeargument name="to" value="#SESSION.EMAILADDRESS#">
				            <cfinvokeargument name="type" value="html">
				            <cfinvokeargument name="subject" value="[SIRE][Buy Keyword - Credit] Payment Info">
				            <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_buy_keyword_credit_pp.cfm">
				            <cfinvokeargument name="data" value="#SerializeJson(emailData)#">
			        	</cfinvoke>
					</cfif>
					<cfset dataout.RESULT = 1/>	
					<cfset dataout.MSG = 'Payment transaction successfully'/>
				<cfelse>	
					<cfset dataout.RESULT = -3/>	
					<cfset dataout.MSG = 'Payment successfully but can not add credits/keywords.'/>
				</cfif>
			<cfelse>
				<cfset dataout.RESULT = -2/>	
				<cfset dataout.MSG = 'Payment failed (ErrCode: #dataout.RESULT#)'/>
			</cfif>
			<cfset redirectUrl = "/session/sire/pages/my-plan?action=finish_payment&result="&#dataout.RESULT#&"&msg="&#urlencode(dataout.MSG)#/>
		<!--- UPGRADE PLAN --->	
		<cfelseif action EQ 'upgrade_plan'>
			<!--- Update Plan and Billing--->
			<cfinvoke component="session.sire.models.cfc.billing" method="DoExpressCheckoutPayment" returnvariable="DoExpressCheckoutPaymentResult">
				<cfinvokeargument name="inptoken" value="#getExpressToken#">
				<cfinvokeargument name="inpPayerId" value="#getExpressPayerId#">
				<cfinvokeargument name="inpAMT" value="#getExpressAmount#">	
				<cfinvokeargument name="numberKeywordToSend" value="#url.numberKeywordToSend#">
				<cfinvokeargument name="numberSMSToSend" value="#url.numberSMSToSend#">	
				<cfinvokeargument name="inpModuleName" value="Buy Plan">
			</cfinvoke>
			<cfif DoExpressCheckoutPaymentResult.RXRESULTCODE EQ 1>

				<cfinvoke component="session.sire.models.cfc.order_plan" method="doUpgradePlan" returnvariable="doUpgradePlanResult">
					<cfinvokeargument name="inpPlanId" value="#url.planId#">
					<cfinvokeargument name="inpPromotionCode" value="#url.inpPromotionCode#">
					<cfinvokeargument name="inpMonthYearSwitch" value="#url.monthYearSwitch#">
					<cfinvokeargument name="inpListKeyword" value="#url.listKeyword#">
					<cfinvokeargument name="inpListMLP" value="#url.listMLP#">
					<cfinvokeargument name="inpListShortUrl" value="#url.listShortUrl#">
					<cfinvokeargument name="inpPaymentGateWay" value="2"/>
				</cfinvoke>
				<cfif doUpgradePlanResult.RXRESULTCODE EQ 1>
					<!--- UPDATE BILLINGAGREEMENTID --->
					<cfinvoke component="session.sire.models.cfc.billing" method="updateUserAuthen" returnvariable="updateUserAuthenResult">
						<cfinvokeargument name="PaymentMethodID" value="#DoExpressCheckoutPaymentResult['BILLINGAGREEMENTID']#">
						<cfinvokeargument name="PaymentType" value="PP">
				        <cfinvokeargument name="intPaymentGateway" value="2">
					</cfinvoke>

					<cfif SESSION.EMAILADDRESS NEQ ''>
						<cfset emailData ={}/>
						<cfset emailData['FullName'] = SESSION.FULLNAME/>
						<cfset emailData['numberKeywords'] = url.numberSMSToSend/>
						<cfset emailData['numberSMS'] = url.numberKeywordToSend/>
						<cfset emailData['authorizedAmount'] = getExpressAmount/>
						<cfset emailData['transactionId'] = DoExpressCheckoutPaymentResult['TRANSACTIONID']/>
						<cfset emailData['planName'] = doUpgradePlanResult['PLANNAME']/>
						<!--- Sendmail payment --->
		                <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
		                  <cfinvokeargument name="to" value="#SESSION.EMAILADDRESS#">
		                  <cfinvokeargument name="type" value="html">
		                  <cfinvokeargument name="subject" value="[SIRE][Order Plan] Payment Info">
		                  <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_order_plan_pp.cfm">
		                  <cfinvokeargument name="data" value="#serializeJSON(emailData)#">
		                </cfinvoke>
                	</cfif>

					<cfset dataout.RESULT = 1/>	
					<cfset dataout.MSG = 'Payment transaction successfully'/>
				<cfelse>
					<cfset dataout.RESULT = -3/>	
					<cfset dataout.MSG = 'Payment successfully but can not add credits/keywords.'/>
				</cfif>
				
			<cfelse>
				<cfset dataout.RESULT = -2/>	
				<cfset dataout.MSG = 'Payment failed (ErrCode: #dataout.RESULT#)'/>
			</cfif>	
			<cfset redirectUrl = "/session/sire/pages/my-plan?action=finish_payment&result="&#dataout.RESULT#&"&msg="&#urlencode(dataout.MSG)#/>

		<cfelseif action EQ 'admin_upgrade_plan'>	
			<!--- Update Plan and Billing--->
			<cfinvoke component="session.sire.models.cfc.billing" method="DoExpressCheckoutPayment" returnvariable="DoExpressCheckoutPaymentResult">
				<cfinvokeargument name="inptoken" value="#getExpressToken#">
				<cfinvokeargument name="inpPayerId" value="#getExpressPayerId#">
				<cfinvokeargument name="inpAMT" value="#getExpressAmount#">	
				<cfinvokeargument name="numberKeywordToSend" value="#url.numberKeywordToSend#">
				<cfinvokeargument name="numberSMSToSend" value="#url.numberSMSToSend#">	
				<cfinvokeargument name="inpModuleName" value="Buy Plan">
				<cfinvokeargument name="inpUserId" value="#url.userId#">

			</cfinvoke>

			<cfif DoExpressCheckoutPaymentResult.RXRESULTCODE EQ 1>

				<cfinvoke component="public.sire.models.users" method="GetUserInfor" returnvariable="GetUserInforResult">
					<cfinvokeargument name="inpiduser" value="#url.userId#"/>
				</cfinvoke>

				<cfif GetUserInforResult.RXRESULTCODE EQ 1>
					<cfset emailAdd = GetUserInforResult.EMAIL>
					<cfset fullname = GetUserInforResult.FULLNAME>
				<cfelse>
					 <cfset emailAdd = ''>
					 <cfset fullname = ''>
				</cfif>

				<cfinvoke component="session.sire.models.cfc.order_plan" method="doUpgradePlan" returnvariable="doUpgradePlanResult">
					<cfinvokeargument name="inpPlanId" value="#url.planId#">
					<cfinvokeargument name="inpPromotionCode" value="#url.inpPromotionCode#">
					<cfinvokeargument name="inpMonthYearSwitch" value="#url.monthYearSwitch#">
					<cfinvokeargument name="inpListKeyword" value="#url.listKeyword#">
					<cfinvokeargument name="inpListMLP" value="#url.listMLP#">
					<cfinvokeargument name="inpListShortUrl" value="#url.listShortUrl#">
					<cfinvokeargument name="inpPaymentGateWay" value="2"/>
					<cfinvokeargument name="inpUserId" value="#url.userId#">
					<cfinvokeargument name="inpAdminDo" value="1">
					<cfinvokeargument name="inpUserEmail" value="#emailAdd#">
				</cfinvoke>
				<cfif doUpgradePlanResult.RXRESULTCODE EQ 1>

					<cfif emailAdd NEQ ''>
						<cfset emailData ={}/>
						<cfset emailData['FullName'] = fullname/>
						<cfset emailData['authorizedAmount'] = getExpressAmount/>
						<cfset emailData['transactionId'] = DoExpressCheckoutPaymentResult['TRANSACTIONID']/>
						<cfset emailData['planName'] = doUpgradePlanResult['PLANNAME']/>
						<!--- Sendmail payment --->
		                <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
		                  <cfinvokeargument name="to" value="#emailAdd#">
		                  <cfinvokeargument name="type" value="html">
		                  <cfinvokeargument name="subject" value="[SIRE][Order Plan] Payment Info">
		                  <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_order_plan_pp.cfm">
		                  <cfinvokeargument name="data" value="#serializeJSON(emailData)#">
		                </cfinvoke>
                	</cfif>

					<cfset dataout.RESULT = 1/>	
					<cfset dataout.MSG = 'Payment transaction successfully. UserId:'&url.userId/>
				<cfelse>
					<cfset dataout.RESULT = -3/>	
					<cfset dataout.MSG = 'Payment successfully but can not upgrade plan.'/>
				</cfif>
				
			<cfelse>
				<cfset dataout.RESULT = -2/>	
				<cfset dataout.MSG = 'Payment failed (ErrCode: #dataout.RESULT#)'/>
			</cfif>	
			<cfset redirectUrl = "/session/sire/pages/admin-account-update?action=finish_payment&result="&#dataout.RESULT#&"&msg="&#urlencode(dataout.MSG)#/>

		<cfelseif action EQ 'admin_buy_credits_keywords'>

			<!--- Update Plan and Billing--->
			<cfinvoke component="session.sire.models.cfc.billing" method="DoExpressCheckoutPayment" returnvariable="DoExpressCheckoutPaymentResult">
				<cfinvokeargument name="inptoken" value="#getExpressToken#">
				<cfinvokeargument name="inpPayerId" value="#getExpressPayerId#">
				<cfinvokeargument name="inpAMT" value="#getExpressAmount#">	
				<cfinvokeargument name="numberKeywordToSend" value="#url.numberKeywordToSend#">
				<cfinvokeargument name="numberSMSToSend" value="#url.numberSMSToSend#">	
				<cfinvokeargument name="inpModuleName" value="#inpModuleNamevalue#">
				<cfinvokeargument name="inpUserId" value="#url.userId#">
				
			</cfinvoke>

			<cfif DoExpressCheckoutPaymentResult.RXRESULTCODE EQ 1>

				<cfinvoke component="public.sire.models.users" method="GetUserInfor" returnvariable="GetUserInforResult">
					<cfinvokeargument name="inpiduser" value="#url.userId#"/>
				</cfinvoke>

				<cfinvoke component="public.sire.models.users" method="GetUserInfor" returnvariable="GetUserInforResult">
					<cfinvokeargument name="inpiduser" value="#url.userId#"/>
				</cfinvoke>

				<cfif GetUserInforResult.RXRESULTCODE EQ 1>
					<cfset emailAdd = GetUserInforResult.EMAIL>
					<cfset fullname = GetUserInforResult.FULLNAME>
				<cfelse>
					 <cfset emailAdd = ''>
					 <cfset fullname = ''>
				</cfif>

				<cfinvoke component="session.sire.models.cfc.users" method="UpdateUserAccountLimitationNew" returnvariable="UpdateUserAccountLimitationNewResult">
					<cfinvokeargument name="inpUserId" value="#url.userId#">
					<cfinvokeargument name="inpValue" value="#URLDecode(url.value)#">
					<cfinvokeargument name="inpType" value="#url.type#">
					<cfinvokeargument name="inpUserEmail" value="#emailAdd#">
					<cfinvokeargument name="inpPaymentGateWay" value="2"/>
					<cfinvokeargument name="inpMonthYearSwitch" value="#url.monthYearSwitch#"/>
				</cfinvoke>
				
				<cfif UpdateUserAccountLimitationNewResult.RXRESULTCODE EQ 1>

					<cfif emailAdd NEQ ''>
						<cfif url.type EQ 7>
							<cfset emailData ={}/>
							<cfset emailData['FullName'] = fullname/>
							<cfset emailData['authorizedAmount'] = getExpressAmount/>
							<cfset emailData['transactionId'] = DoExpressCheckoutPaymentResult['TRANSACTIONID']/>
							<cfset emailData['planName'] = UpdateUserAccountLimitationNewResult['PLANNAME']/>
							<!--- Sendmail payment --->
			                <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
			                  <cfinvokeargument name="to" value="#emailAdd#">
			                  <cfinvokeargument name="type" value="html">
			                  <cfinvokeargument name="subject" value="[SIRE][Order Plan] Payment Info">
			                  <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_order_plan_pp.cfm">
			                  <cfinvokeargument name="data" value="#serializeJSON(emailData)#">
			                </cfinvoke>
			            <cfelseif url.type EQ 8  OR url.type EQ 9>
			            	<cfif url.type EQ 8>
			            		<cfset url.numberSMSToSend = URLDecode(url.value)>
			            	<cfelseif url.type EQ 9>
			            		<cfset url.numberKeywordToSend = URLDecode(url.value)>
			            	</cfif>
							<cfset emailData ={}/>
							<cfset emailData['FullName'] = fullname/>
							<cfset emailData['numberKeywords'] = url.numberSMSToSend/>
							<cfset emailData['numberSMS'] = url.numberKeywordToSend/>
							<cfset emailData['authorizedAmount'] = getExpressAmount/>
							<cfset emailData['transactionId'] = DoExpressCheckoutPaymentResult['TRANSACTIONID']/>

							<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
					            <cfinvokeargument name="to" value="#emailAdd#">
					            <cfinvokeargument name="type" value="html">
					            <cfinvokeargument name="subject" value="[SIRE][Buy Keyword - Credit] Payment Info">
					            <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_buy_keyword_credit_pp.cfm">
					            <cfinvokeargument name="data" value="#SerializeJson(emailData)#">
				        	</cfinvoke>
			            </cfif>    
                	</cfif>

					<cfset dataout.RESULT = 1/>	
					<cfset dataout.MSG = UpdateUserAccountLimitationNewResult.MESSAGE&" Userid:"&#url.userId#&"-"&#emailAdd#/>
				<cfelse>
					<cfset dataout.RESULT = -3/>	
					<cfset dataout.MSG = 'Payment successfully but can not update infomation for user.'/>
				</cfif>
				
			<cfelse>
				<cfset dataout.RESULT = -2/>	
				<cfset dataout.MSG = 'Payment failed (ErrCode: #dataout.RESULT#)'/>
			</cfif>	
			<cfset redirectUrl = "/session/sire/pages/admin-account-update?action=finish_payment&result="&#dataout.RESULT#&"&msg="&#urlencode(dataout.MSG)#/>
		</cfif>
	<cfelse>
		<cfset dataout.RESULT = -1/>
		<cfset dataout.MSG = 'Payment failed (ErrCode:#dataout.RESULT#)'/>
	</cfif>
<cfelseif action EQ "buy_credit_keyword_above99">
	<cfinvoke component="session.sire.models.cfc.billing" method="getUserAuthenInfo" returnvariable="RetUserAuthenInfo">
		<cfinvokeargument name="userId" value="#url.userId#">
		<cfinvokeargument name="intPaymentGateway" value="2">
	</cfinvoke>
	<cfif RetUserAuthenInfo.RXRESULTCODE LT 1>
		<cfset dataout.RESULT = -2/>	
		<cfset dataout.MSG = 'Get User Authen Info Fail.'/>
	</cfif>
	<!---getUserAuthenInfo --->
	<cfinvoke component="session.sire.models.cfc.billing" method="getUserAuthenInfo" returnvariable="userPayInfo">
		<cfinvokeargument name="userId" value="#url.userId#"/>
		<cfinvokeargument name="intPaymentGateway" value="2"/>
	</cfinvoke>
	<!--- get userplan info --->
	<cfinvoke component="session.sire.models.cfc.order_plan" method="GetUserPlan" returnvariable="GetUserPlanInfo">
		<cfinvokeargument name="InpUserId" value="#url.userId#"/>
	</cfinvoke>
	<!--- get user info --->
	<cfinvoke component="public.sire.models.users" method="GetUserInfor" returnvariable="RevalGetUserInfo">
		<cfinvokeargument name="inpiduser" value="#url.userId#"/>
	</cfinvoke>

	<cfinvoke component="session.sire.models.cfc.billing" method="DoReferenceTransaction" returnvariable="DoReferenceTransactionRX">
		<cfinvokeargument name="inpAMT" value="#url.PaymentAmount#"/>
		<cfinvokeargument name="inpReferenceId" value="#userPayInfo.DATA.PaymentMethodID_vch#"/>
		<cfinvokeargument name="inpPlanId" value="#GetUserPlanInfo.PLANID#"/>
		<cfinvokeargument name="inpModuleName" value="Buy Credits and Keywords"/>
		<cfinvokeargument name="inpUserId" value="#url.userId#">
		<cfinvokeargument name="inpNumberSMS" value="#url.numberSMSToSend#">
		<cfinvokeargument name="inpNumberKeyword" value="#url.numberKeywordToSend#">
	</cfinvoke>
	<cfif DoReferenceTransactionRX.RXRESULTCODE EQ 1>
		<cfinvoke component="session.sire.models.cfc.billing" method="DoAddPurchasedCreditKeyword" returnvariable="DoAddPurchasedCreditKeywordResult">
			<cfinvokeargument name="inpNumberSMS" value="#url.numberSMSToSend#">
			<cfinvokeargument name="inpNumberKeyword" value="#url.numberKeywordToSend#">
			<cfinvokeargument name="inpUserId" value="#url.userId#">
		</cfinvoke>
		<cfif DoAddPurchasedCreditKeywordResult.RXRESULTCODE EQ 1>
			<cfif RevalGetUserInfo.EMAIL NEQ ''>
				<cfset emailData ={}/>
				<cfset emailData['FullName'] = RevalGetUserInfo.FIRSTNAME & " " & RevalGetUserInfo.LASTNAME/>
				<cfset emailData['numberKeywords'] =  url.numberKeywordToSend />
				<cfset emailData['numberSMS'] = url.numberSMSToSend/>
				<cfset emailData['authorizedAmount'] = url.PaymentAmount/>
				<cfset emailData['transactionId'] = DoReferenceTransactionRX.TRANSACTIONID/>

				<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
					<cfinvokeargument name="to" value="#RevalGetUserInfo.EMAIL#">
					<cfinvokeargument name="type" value="html">
					<cfinvokeargument name="subject" value="[SIRE][Buy Keyword - Credit] Payment Info">
					<cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_buy_keyword_credit_pp.cfm">
					<cfinvokeargument name="data" value="#SerializeJson(emailData)#">
				</cfinvoke>
			</cfif>
			<!--- Update status transaction verify --->
			<cfinvoke component="session.sire.models.cfc.billing" method="UpdateStatusPaymentTransaction" returnvariable="UpdateStatusPaymentTransactionResult">
				<cfinvokeargument name="inpPaymentID" value="#url.inpPaymentID#">
				<cfinvokeargument name="inpPaymentStatus" value="1">
			</cfinvoke>
			<cfset dataout.RESULT = 1/>	
			<cfset dataout.MSG = 'Payment transaction successfully'/>
		<cfelse>	
			<cfset dataout.RESULT = -3/>	
			<cfset dataout.MSG = 'Payment successfully but can not add credits/keywords.'/>
		</cfif>
	<cfelse> <!---PAYMENT PAYPAL ERROR --->
		<cfset dataout.RESULT = -1/>	
		<cfset dataout.MSG = 'Payment fail.'/>
	</cfif>	
	<cfset redirectUrl = "/session/sire/pages/admin_verify_payment_transaction?action=finish_payment&result="&#dataout.RESULT#&"&msg="&#urlencode(dataout.MSG)#/>
</cfif>
<cflocation url="#redirectUrl#" addtoken="false"/>

 