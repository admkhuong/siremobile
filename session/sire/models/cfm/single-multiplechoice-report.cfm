<cfparam name="url.BatchId" default="">
<cfparam name="url.BatchName" default="">

<cfinclude template="/session/sire/configs/paths.cfm" >

<cfset file = "Single Multiple Choice Question Report-#url.BatchName#-#Session.FULLNAME#">
<cfset filename = file & ".csv">
<cfset RetVarQueryToCSV = "">

<cfheader name="Content-Disposition" value="inline; filename=#filename#"> 
<cfcontent type="application/csv">

<cfinvoke method="GetCPByBatchIdAndType" component="session.sire.models.cfc.reports" returnvariable="getCPResult">
	<cfinvokeargument name="inpBatchId" value="#url.BatchId#"/>
	<cfinvokeargument name="inpCPType" value="ONESELECTION"/>
</cfinvoke>

<cfinvoke method="GetMultipleChoiceByBatch" component="session.sire.models.cfc.reports" returnvariable="data">
	<cfinvokeargument name="inpBatchId" value="#url.BatchId#"/>
	<cfinvokeargument name="inpCPID" value="#getCPResult['DATALIST'][1].RQ#"/>
	<cfinvokeargument name="inpQID" value="#getCPResult['DATALIST'][1].ID#"/>
</cfinvoke>

<cfif data.RXRESULTCODE GT 0>
	<cfset reportList =  QueryNew("RESPONSE,COUNT")>
		<cfloop array="#data['DATALIST']#" index="index">
			<cfset QueryAddRow(reportList) />	
			<cfset QuerySetCell(reportList, "RESPONSE", index.TEXT) />
			<cfset QuerySetCell(reportList, "COUNT", index.SUM) />
		</cfloop>

	<cfinvoke method="QueryToCSV" component="session.sire.models.cfc.admin" returnvariable="RetVarQueryToCSV">
    <cfinvokeargument name="Query" value="#reportList#">
	    <cfinvokeargument name="CreateHeaderRow" value="true">
	    <cfinvokeargument name="Delimiter" value=",">
	</cfinvoke>
</cfif>

<cfoutput>
	#RetVarQueryToCSV#
	<cfflush/>
</cfoutput>
