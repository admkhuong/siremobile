<cfparam name="url.BatchId" default="">
<cfparam name="url.BatchName" default="">

<cfinclude template="/session/sire/configs/paths.cfm" >

<cfset file = "Single Yes/No Question Report-#url.BatchName#-#Session.FULLNAME#-#LSDateFormat(now(), "yyyy-mm-dd")#">
<cfset filename = file & ".csv">
<cfset RetVarQueryToCSV = "">

<cfheader name="Content-Disposition" value="inline; filename=#filename#"> 
<cfcontent type="application/csv">

<cfinvoke method="GetSingleYesNoResponseForReport" component="session.sire.models.cfc.reports" returnvariable="data">
	<cfinvokeargument name="inpBatchId" value="#url.BatchId#"/>
</cfinvoke>

<cfif data.RXRESULTCODE GT 0>
	<cfset reportList =  QueryNew("CONTACTSTRING,RESPONSE,CREATED")>
		<cfloop array="#data['DATALIST']#" index="index">
			<cfset QueryAddRow(reportList) />	
			<cfset QuerySetCell(reportList, "CONTACTSTRING", index.CONTACTSTRING) />
			<cfset QuerySetCell(reportList, "RESPONSE", index.RESPONSE) />
			<cfset QuerySetCell(reportList, "CREATED", index.CREATED) />
		</cfloop>

	<cfinvoke method="QueryToCSV" component="session.sire.models.cfc.admin" returnvariable="RetVarQueryToCSV">
    <cfinvokeargument name="Query" value="#reportList#">
	    <cfinvokeargument name="CreateHeaderRow" value="true">
	    <cfinvokeargument name="Delimiter" value=",">
	</cfinvoke>
</cfif>

<cfoutput>
	#RetVarQueryToCSV#
	<cfflush/>
</cfoutput>
