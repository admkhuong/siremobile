<cfparam name="url.id" default="0">
<cfparam name="url.search" default="">
<cfparam name="url.file" default="">
<cfparam name="url.start" default="0">
<cfparam name="url.status" default="-1">
<cfparam name="url.listfile" default="">
<cfparam name="url.total" default="0" >

<cfinclude template="../../configs/paths.cfm" >

<cfif !isNumeric(url.id) OR url.id LT 0>
	<cfset url.id = 0>
</cfif>

<cfif !isNumeric(url.start) OR url.start LT 0>
	<cfset url.start = 0>
</cfif>

<cfif !isNumeric(url.status)>
	<cfset url.start = -1>
</cfif>

<cfset url.search = trim(url.search)>

<cfset limit = 60000> 

<cfset contactList.recordCount = limit>

<cfset n = 1>

<!--- <cfset listFile = {}/> --->
<cfset listFile = ArrayNew(1)>
<cfset file = "subscribers-list-#url.id##REReplace(now(), '[\W\s]+', '-', 'all')##Session.USERID#">
<cfset filename = file & ".csv">
<cfset RetVarQueryToCSV = "">

<cfheader name="Content-Disposition" value="inline; filename=#filename#"> 
<cfcontent type="application/msexcel">
        
	<!--- Make sure we dont end up in infinite loops here - will crash web server if we do --->
	<cfset SecurityCheck_iDisplayStart = url.start>

	<cfquery name="contactList" datasource="#Session.DBSourceREAD#">
		SELECT 
			simplelists.contactstring.ContactString_vch AS `PhoneNumber`, simplelists.contactstring.ContactId_bi, simplelists.contactstring.OptIn_dt AS `DateSubscribed`, simplelists.contactstring.OptOut_dt AS `DateUnsubscribed`, simplelists.contactstring.Created_dt
		FROM
			simplelists.groupcontactlist 
			INNER JOIN simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
			INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi	   						
		WHERE                
			simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
		<cfif url.id GT 0>
		AND
			simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.id#">
		</cfif>		
		AND 
			simplelists.contactstring.ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.contactstring INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi WHERE UserId_int = 50)
		AND 
			simplelists.contactstring.ContactType_int =3	

		<cfif url.search NEQ ''> 
			AND simplelists.contactstring.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="%#url.search#%">
		</cfif>
		GROUP BY simplelists.contactstring.ContactString_vch
		LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.start#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#limit#">	
	</cfquery>

	

	<cfif contactList.recordCount GT 0>
		<!--- <cfset contactLists =  QueryNew("PHONE NUMBER,DATE SUBSCRIBED,DATE UNSUBSCRIBED")> --->
		<cfset contactLists =  QueryNew("PHONE NUMBER,DATE SUBSCRIBED")>
		<cfloop query="contactList">
			<cfset dispDateSubscribed = ''/>
			<cfif DateSubscribed NEQ ''>	
				<cfset dispDateSubscribed = DateFormat(DateSubscribed, 'long')&" "&TimeFormat(DateSubscribed, 'short') />
			<cfelse>
				<cfset dispDateSubscribed = DateFormat(Created_dt, 'long')&" "&TimeFormat(Created_dt, 'short') />
			</cfif>
			<cfset QueryAddRow(contactLists) />	
			<cfset QuerySetCell(contactLists, "PHONE NUMBER", PhoneNumber) />
			<cfset QuerySetCell(contactLists, "DATE SUBSCRIBED", dispDateSubscribed) />
			<!--- <cfset QuerySetCell(contactLists, "DATE UNSUBSCRIBED", "#DateFormat(DateUnsubscribed, 'long')# #TimeFormat(DateUnsubscribed, 'short')#" ) /> --->
		</cfloop>
		
		<!---<cfspreadsheet action="update" filename="#ExpandPath('/')#session/sire/cache/downloads/#filename#" overwrite="true" password="password" query="contactList" sheetname="Sheet#Ceiling((url.start+1)/limit)#">--->

		<cfinvoke method="QueryToCSV" component="session.sire.models.cfc.admin" returnvariable="RetVarQueryToCSV">
	    <cfinvokeargument name="Query" value="#contactLists#">
		    <cfinvokeargument name="CreateHeaderRow" value="true">
		    <cfinvokeargument name="Delimiter" value=",">
		</cfinvoke>

		<cfset ArrayAppend(listFile, #RetVarQueryToCSV#)>

		<cfset SecurityCheck = 0>
		<cfset MaxLoops = 100000>
		<cfset url.start += limit>
	</cfif>


<cfloop condition="url.start LESS THAN url.total">

    <!--- Robustness check - dont keep looping over same data --->
    <cfif SecurityCheck_iDisplayStart EQ url.start>
        <cfoutput>Export Error - Same Data found in multiple loops</cfoutput>
    	<cfbreak/>
    </cfif>

	<cfquery name="contactList" datasource="#Session.DBSourceREAD#">
		SELECT 
			simplelists.contactstring.ContactString_vch AS `PhoneNumber`, simplelists.contactstring.ContactId_bi, simplelists.contactstring.OptIn_dt AS `DateSubscribed`, simplelists.contactstring.OptOut_dt AS `DateUnsubscribed`, simplelists.contactstring.Created_dt
		FROM
			simplelists.groupcontactlist 
			INNER JOIN simplelists.contactstring on simplelists.groupcontactlist.contactaddressid_bi = simplelists.contactstring.contactaddressid_bi
			INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi	   						
		WHERE                
			simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
		<cfif url.id GT 0>
		AND
			simplelists.groupcontactlist.groupid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.id#">
		</cfif>		
		AND 
			simplelists.contactstring.ContactString_vch NOT IN (SELECT ContactString_vch FROM simplelists.contactstring INNER JOIN simplelists.contactlist on simplelists.contactstring.contactid_bi = simplelists.contactlist.contactid_bi WHERE UserId_int = 50)
		AND 
			simplelists.contactstring.ContactType_int =3	

		<cfif url.search NEQ ''> 
			AND simplelists.contactstring.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="%#url.search#%">
		</cfif>
		GROUP BY simplelists.contactstring.ContactId_bi
		LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.start#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#limit#">	
	</cfquery>

	<cfif contactList.recordCount GT 0>
		<cfset contactLists =  QueryNew("PHONE NUMBER,DATE SUBSCRIBED,DATE UNSUBSCRIBED")>
		<cfloop query="contactList">
			<cfset dispDateSubscribed = ''/>
			<cfif DateSubscribed NEQ ''>	
				<cfset dispDateSubscribed = DateFormat(DateSubscribed, 'long')&" "&TimeFormat(DateSubscribed, 'short') />
			<cfelse>
				<cfset dispDateSubscribed = DateFormat(Created_dt, 'long')&" "&TimeFormat(Created_dt, 'short') />
			</cfif>
			<cfset QueryAddRow(contactLists) />	
			<cfset QuerySetCell(contactLists, "PHONE NUMBER", PhoneNumber) />
			<cfset QuerySetCell(contactLists, "DATE SUBSCRIBED", dispDateSubscribed) />
			<!--- <cfset QuerySetCell(contactLists, "DATE UNSUBSCRIBED", "#DateSubscribed#" ) /> --->
		</cfloop>
		
		<!---<cfspreadsheet action="update" filename="#ExpandPath('/')#session/sire/cache/downloads/#filename#" overwrite="true" password="password" query="contactList" sheetname="Sheet#Ceiling((url.start+1)/limit)#">--->
		<cfinvoke method="QueryToCSV" component="session.sire.models.cfc.admin" returnvariable="RetVarQueryToCSV">
	    <cfinvokeargument name="Query" value="#contactLists#">
		    <cfinvokeargument name="CreateHeaderRow" value="true">
		    <cfinvokeargument name="Delimiter" value=",">
		</cfinvoke>

		<cfset ArrayAppend(listFile, #RetVarQueryToCSV#)>

		<!--- <cfset GetPageContext().GetOut().ClearBuffer() /> --->
		
		<cfset url.start += limit>
		<cfset n += 1>
		<cfset SecurityCheck = SecurityCheck + 1>
			
		<!--- Sanity check to make sure we never end up in endless loop by accident - either need bigger loops or dont use this export tool --->
		<cfif SecurityCheck GT MaxLoops>  
			<cfoutput>Export Error - You have exceeded a reasonable number of loops - try increasing your iDisplayLength size</cfoutput>	
			<cfbreak/>					
		</cfif>	
		

		
	</cfif>

</cfloop>

<cfloop index="i" from="1" to="#n#">
	<cfoutput>
		#listFile[i]#
		<cfflush/>
	</cfoutput>
</cfloop>
