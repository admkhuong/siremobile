<!--- This templateid is used to look up data for a preview bootstrap modal page --->
<cfparam name="templateid" default="">
<cfparam name="templateFlag" default="1">
<cfparam name="userId" default="0">

<!--- If bad templateid is passed in just redirect them to the campaign manage page --->
<cfif !isNumeric(templateid)>
	<cflocation url="/session/sire/pages/campaign-manage" addtoken="false">
</cfif>

<link rel="stylesheet" href="/session/sire/css/campaign-edit/main.css" />
<link rel="stylesheet" href="/session/sire/css/campaign-edit/gears.css" />
<link rel="stylesheet" href="/session/sire/css/campaign-edit/media-hacks.css" />
<link rel="stylesheet" href="/session/sire/css/campaign-edit/on-off-slider.css" />


<style>
	.LoadingCPs
	{
		display: none;
	}

	.div-toggle-inteval, .toggle-inteval-question
	{
		display: none;
	}

	.wrapper-answer-text, .toggle-answer-intents
	{
		display: none;
	}

	.info-popover
	{
		display: none;

	}

	textarea
	{

	}

</style>


<div class="row">

	<!--- Read list of CP from template --->
	<cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPs" returnvariable="RetVarReadCPs">
		<cfinvokeargument name="inpBatchId" value="#templateid#">
		<cfinvokeargument name="inpTemplateFlag" value="#templateFlag#">
		<cfinvokeargument name="inpUserId" value="#userId#">
	</cfinvoke>



	<!--- Loop over each Template CP --->
	<cfloop array="#RetVarReadCPs.CPOBJ#" index="CPOBJ">


	<!--- <cfdump var="#RetVarReadCPs#" />

	--->

		<cfif ListContains("BRANCH, API", CPOBJ.TYPE) GT 0 >
			<cfcontinue />
		</cfif>

	    <!--- Read CP data for each CP in Template --->
	 	<cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPDataById" returnvariable="RetVarReadCPDataById">
			<cfinvokeargument name="inpBatchId" value="#templateid#">
			<cfinvokeargument name="inpTemplateFlag" value="#templateFlag#">
			<cfinvokeargument name="inpQID" value="#CPOBJ.RQ#">
			<cfinvokeargument name="inpUserId" value="#userId#">
		</cfinvoke>


		<!--- Render CP data for each CP in Template --->
		<cfinvoke component="session.sire.models.cfc.control-point" method="RenderCP" returnvariable="RetVarRenderCP">
			<cfinvokeargument name="controlPoint" value="#RetVarReadCPDataById.CPOBJ#">
			<cfinvokeargument name="inpBatchId" value="#templateid#">
			<cfinvokeargument name="inpTemplateFlag" value="#templateFlag#">
			<cfinvokeargument name="inpPreview" value="1">
			<cfinvokeargument name="inpUserId" value="#userId#">
		</cfinvoke>


		<cfoutput>
			<!--- This is the HTML for a CP object - HTML is maintained in RenderCP function --->
			#RetVarRenderCP.HTML#
		</cfoutput>

	</cfloop>

</div>
