<cfinclude template="/public/sire/configs/paths.cfm" >
<cfparam name="form.amount_of_credit" default="0">
<!--- <cfparam name="form.amount_of_promotion" default="0"> --->
<cfparam name="form.amount_of_keyword" default="0">
<cfparam name="form.amount_of_mlp" default="0">
<cfparam name="form.amount_of_short_url" default="0">
<cfparam name="form.amount_of_image_capacity" default="0">
<cfparam name="form.user_id" default="0">
<cfparam name="form.user_name" default="">
<cfparam name="form.user_email" default="">

<cfset dataout = {} />
<cfset dataout.RESULT = 0/>
<cfset dataout.MESSAGE = "Update fail! Sorry, we have some issue at the moment, please try again later."/>



<!--- Check admin permission --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission">
	<cfinvokeargument name="inpSkipRedirect" value="1">
</cfinvoke>

<cfif RetVarGetAdminPermission.ISADMINOK EQ 1>

	<cfinvoke component="session.sire.models.cfc.order_plan" method="GetUserPlan" returnvariable="userPlan">
		<cfinvokeargument name="InpUserId" value="#form.user_id#">
	</cfinvoke>


	<cfinvoke component="public.sire.models.users" method="UpdateUserAccountLimitation" returnvariable="rsUpdate">
		<cfinvokeargument name="inpUserId" value="#form.user_id#">
		<cfinvokeargument name="inpCredit" value="#form.amount_of_credit#">
		<!--- <cfinvokeargument name="inpPromotion" value="#form.amount_of_promotion#"> --->
		<cfinvokeargument name="inpKeyword" value="#form.amount_of_keyword#">
		<cfinvokeargument name="inpMlp" value="#form.amount_of_mlp#">
		<cfinvokeargument name="inpShortUrl" value="#form.amount_of_short_url#">
		<cfinvokeargument name="inpMlpImageCapacity" value="#form.amount_of_image_capacity#">
	</cfinvoke>

	<cfif rsUpdate.RXRESULTCODE eq 1>
		<cfset dataout.RESULT = 1>
		<cfset dataout.MESSAGE = "Update Success!"/>


		<!--- Update balance  --->
		<!--- <cfinvoke component="session.sire.models.cfc.billing" method="UpdateBalance">
			<cfinvokeargument name="inpUserId" value="#form.user_id#">
			<cfinvokeargument name="inpValue" value="#form.amount_of_credit#">
		</cfinvoke> --->


		<!--- Send an email to notify --->

		<cfquery name="GetEMSAdmins" datasource="#Session.DBSourceREAD#">
		     SELECT
		         ContactAddress_vch                              
		     FROM 
		      simpleobjects.systemalertscontacts
		     WHERE
		      ContactType_int = 2
		     AND
		      EMSNotice_int = 1    
		</cfquery>

		<cfif GetEMSAdmins.RecordCount GT 0>
			<cfset EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                   
		<cfelse>
			<cfset EBMAdminEMSList = SupportEMailFrom>
		</cfif>

		<cfmail to="#EBMAdminEMSList#" subject="Admin Update UserAccount" type="html" from="#EBMAdminEMSList#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#"  port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
		 	<!DOCTYPE html>
			<html>
			<head>
			<style>
			table {
			    font-family: arial, sans-serif;
			    border-collapse: collapse;
			    background-color: ##f1f1c1;
			    width: 100%;
			}

			td, th {
			    border: 1px solid ##dddddd;
			    text-align: left;
			    padding: 8px;
			}

			tr:nth-child(even) {
			    background-color: ##dddddd;
			}

			strong.red {
				color: red;
			}
			</style>
			</head>
			<body>
				<table cellpadding="5" cellspacing="5" style="">
			 		<thead>
			 			<tr>
			 				<th colspan="7"><h3 style="text-align: center;">Admin User Modified Monthly Limits</h3></th>
			 			</tr>
			 			<tr>
			 				<th>Status</th><th>Credits</th><!--- <th>Promotion Credits</th> ---><th>Keywords</th><th>Mlps</th><th>Short Urls</th><th>Image Capacity</th>
			 			</tr>
			 		</thead>
			 		<tbody>
			 			<tr>
			 				<td>Before:</td>
			 				<td><strong><cfoutput>#userPlan.USERPLANFIRSTSMSINCLUDED#</cfoutput></strong></td>
			 				<!--- <td><strong><cfoutput>#userPlan.PROMOTIONCREDIT#</cfoutput></strong></td> --->
							<td><strong><cfoutput>#userPlan.KEYWORDSLIMITNUMBER#</cfoutput></strong></td>
			 				<td><strong><cfoutput>#userPlan.MLPSLIMITNUMBER#</cfoutput></strong></td>
			 				<td><strong><cfoutput>#userPlan.SHORTURLSLIMITNUMBER#</cfoutput></strong></td>
			 				<td><strong><cfoutput>#userPlan.IMAGECAPACITYLIMIT#</cfoutput></strong></td>
			 			</tr>
			 			<tr>
			 				<td>After:</td>
			 				<td><strong <cfif form.amount_of_credit neq  userPlan.USERPLANFIRSTSMSINCLUDED>style="color: red;"</cfif> ><cfoutput>#form.amount_of_credit#</cfoutput></strong></td>
			 				<!--- <td><strong <cfif form.amount_of_credit neq  userPlan.PROMOTIONCREDIT>style="color: red;"</cfif> ><cfoutput>#form.amount_of_promotion#</cfoutput></strong></td> --->
			 				<td><strong <cfif form.amount_of_keyword neq  userPlan.KEYWORDSLIMITNUMBER>style="color: red;"</cfif>><cfoutput>#form.amount_of_keyword#</cfoutput></strong></td>
			 				<td><strong <cfif form.amount_of_mlp neq  userPlan.MLPSLIMITNUMBER>style="color: red;"</cfif>><cfoutput>#form.amount_of_mlp#</cfoutput></strong></td>
			 				<td><strong <cfif form.amount_of_short_url neq  userPlan.SHORTURLSLIMITNUMBER>style="color: red;"</cfif>><cfoutput>#form.amount_of_short_url#</cfoutput></strong></td>
			 				<td><strong <cfif form.amount_of_image_capacity neq  userPlan.IMAGECAPACITYLIMIT>style="color: red;"</cfif>><cfoutput>#form.amount_of_image_capacity#</cfoutput></strong></td>
			 			</tr>
			 			<tr>
			 				<td colspan="7" style="text-align: center">Updated by: <strong><cfoutput>#Session.USERNAME#</cfoutput> admin user</strong></td>
			 			</tr>
			 			<tr>
			 				<td colspan="7" style="text-align: center">Update for: <strong><cfoutput>#form.user_email#</cfoutput> user account - ID: <cfoutput>#form.user_id#</cfoutput></strong></td>
			 			</tr>
			 		</tbody>
			 	</table>
			</body>
			</html>
		 </cfmail>

		<!--- Log to userlog --->
		<cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
			<cfinvokeargument name="moduleName" value="Admin User Modified Monthly Limits">
			<cfinvokeargument name="operator" value="Changed account limits for #form.user_id# by #Session.UserId#">
		</cfinvoke>

	</cfif>
</cfif>



<cfoutput>#serializeJSON(dataout)#</cfoutput>