<cfparam name="batchID" type="numeric">
<cfparam name="xmlcs" type="string" default="">
<cfparam name="action" type="string" default="getDetail">
<cfparam name="XMLCSEditableCheck" type="string" default="false">

<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="GetAdminPermission">
	 <cfinvokeargument name="inpSkipRedirect" value="1">
</cfinvoke>
<cfinvoke method="GetUserInfor" component="public.sire.models.users" returnvariable="RevalGetUserInfor">
	 <cfinvokeargument name="inpiduser" value="#Session.USERID#">
</cfinvoke>

<cfif GetAdminPermission.ISADMINOK EQ "1">
	<cfset XMLCSEditableCheck = true />
<cfelse>
	<cfif RevalGetUserInfor.ALLEDITXMLCS EQ "0">
		<cfset XMLCSEditableCheck = false />
	<cfelse>
		<cfset XMLCSEditableCheck = true />
	</cfif>
</cfif>

<cfif XMLCSEditableCheck EQ true >
	<cfif action EQ "saveXMLCS">
		<cfinvoke method="updateBatchXMLCS" component="session.sire.models.cfc.control-point" returnvariable="RetVarUpdateBatch">
			<cfinvokeargument name="inpBatchID" value="#batchID#">
			<cfinvokeargument name="inpXMLCS" value="#xmlcs#">
		</cfinvoke>

		<cfoutput>
			#SerializeJson(RetVarUpdateBatch)#
		</cfoutput>
	<cfelse>
		<cfinvoke method="GetBatchDetails" component="session.sire.models.cfc.control-point" returnvariable="RetVarGetBatchDetails">
			<cfinvokeargument name="inpBatchID" value="#batchID#">
		</cfinvoke>

		<cfoutput>
			#SerializeJson(RetVarGetBatchDetails)#
		</cfoutput>
	</cfif>
</cfif>
