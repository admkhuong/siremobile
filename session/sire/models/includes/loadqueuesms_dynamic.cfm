<cfset tickBegin = GetTickCount()>

<cfquery name="getShortCodeData" datasource="#Session.DBSourceEBM#">
    SELECT 
        k.Response_vch,
        k.Survey_int,
        scr.RequesterId_int,
        k.Keyword_vch,
        sc.ShortCode_vch
    FROM
        SMS.Keyword AS k
    LEFT OUTER JOIN
        SMS.shortcoderequest AS scr
    ON
        k.ShortCodeRequestId_int = scr.ShortCodeRequestId_int
    JOIN 
        SMS.ShortCode AS sc
    ON 
        sc.ShortCodeId_int = scr.ShortCodeId_int OR sc.ShortCodeId_int = k.ShortCodeId_int
    WHERE
        k.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#nextBatchId#">
    AND
        k.Active_int IN (1,-3)
    AND
        LENGTH(sc.ShortCode_vch) >= 10
    ORDER BY    
        k.BatchId_bi
    LIMIT 1
</cfquery>

<cfif getShortCodeData.RecordCount GT 0>
    <!--- Set chort code data here for API requests even if on DNC  --->
    <cfset inpShortCode = "#getShortCodeData.ShortCode_vch#"/>

    <cfset rowCountVar = 0>

    <cfloop index="inpContactString" array="#inpContactStringArray#">

        <cfset var inpContactStringTemp = inpContactString.phone />

        <cfif inpContactStringTemp NEQ "">

            <!---Find and replace all non numerics except P X * #--->
            <cfset inpContactStringTemp = REReplaceNoCase(inpContactStringTemp, "[^\d^\*^P^X^##]", "", "ALL")>

            <!--- Clean up where start character is a 1 --->
            <cfif LEFT(inpContactStringTemp, 1) EQ "1">
                <cfset inpContactStringTemp = RemoveChars(inpContactStringTemp, 1, 1)>
            </cfif>

            <cfif LEN(LEFT(inpContactStringTemp, 10)) GTE 10 OR ISNUMERIC(LEFT(inpContactStringTemp, 10))>

                <!--- Check for business rules, no same phone number invite within 30 days --->

                <cfquery name="checkNumber30DaysLimit" datasource="#Session.DBSourceEBM#">
                    SELECT
                        ContactString_vch
                    FROM
                        simplequeue.contactqueue
                    WHERE
                        DTSStatusType_ti IN ('1', '2', '5')
                    AND
                        ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactStringTemp#">
                    AND
                        DATEDIFF(CURDATE(), DATE(Scheduled_dt)) <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#_SameNumberInviteDuration#">
                    AND
                        ContactType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="10"> -- SMS Invite type
                    ORDER BY
                        DTSId_int
                </cfquery>

                <cfif checkNumber30DaysLimit.RECORDCOUNT LT 1>

                    <cfset rowCountVar = rowCountVar + 1>

                    <cfset inpCarrier = ""/>
                    <cfset inpShortCode = "#getShortCodeData.ShortCode_vch#"/>
                    <cfset inpKeyword = "#getShortCodeData.Keyword_vch#"/>
                    <cfset inpTransactionId = ""/>
                    <cfset inpServiceId = ""/>
                    <cfset inpXMLDATA = ""/>
                    <cfset inpOverRideInterval = "0"/>
                    <cfset inpTimeOutNextQID = "0"/>
                    <cfset inpQAToolRequest = "0"/>
                    <cfset local.SMSONLYXMLCONTROLSTRING_VCH = "">

                    <cfset inpData = {
                        inpFriendName = arguments.inpFriendName,
                        inpSignupURL = arguments.inpSignupURL
                    } />

                    <cfset local.SMSONLYXMLCONTROLSTRING_VCH = serializeJSON(inpData) />

                    <!--- Calculate final result --->
                    <cfset tickEnd = GetTickCount()>
                    <cfset testTime = tickEnd - tickBegin>

                    <cfset newUUID = createobject("java", "java.util.UUID").randomUUID().toString() />

                    <!--- Still add entry to queue even if only for tracking purposes--->
                    <!--- Insert customized message --->
                    <cfquery name="InsertIntocontactqueue" datasource="#Session.DBSourceEBM#" result="InsertIntocontactqueueResult">
                        INSERT INTO
                            simplequeue.contactqueue
                        (
                            BatchId_bi,
                            DTSStatusType_ti,
                            DTS_UUID_vch,
                            TypeMask_ti,
                            TimeZone_ti,
                            CurrentRedialCount_ti,
                            UserId_int,
                            PushLibrary_int,
                            PushElement_int,
                            PushScript_int,
                            EstimatedCost_int,
                            ActualCost_int,
                            CampaignTypeId_int,
                            GroupId_int,
                            Scheduled_dt,
                            Queue_dt,
                            Queued_DialerIP_vch,
                            ContactString_vch,
                            Sender_vch,
                            XMLCONTROLSTRING_VCH,
                            ShortCode_vch,
                            ProcTime_int,
                            DistributionProcessId_int,
                            ContactType_int
                        )
                        VALUES
                        (
                            #nextBatchId#,
                            1,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#newUUID#">,
                            3,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#timezone.timezone#">,
                            0,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                            0,
                            0.000,
                            30,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                            #queuedScheduledDate#,  <!--- Be sure if you adjust this to include single quotes around values if you specify an exact a date--->
                            NULL,
                            NULL,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactStringTemp#">,
                            "Invite SMS",
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#local.SMSONLYXMLCONTROLSTRING_VCH#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">,
                            #testTime#,
                            0,
                            10
                        )
                    </cfquery>
                </cfif>
            </cfif>
        </cfif>
    </cfloop>
</cfif>