
<!--- Advanced user and Admin options --->
<cfparam name="adv" default="0">
<cfparam name="inpTemplateFlag" default="0">

<cfinclude template="../../sire/configs/paths.cfm">

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/js/campaign-manage.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/campaign.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/sire/css/yamm.css">
</cfinvoke>

<cfset content.ROOTURL_HTTP = "https://#CGI.SERVER_NAME#">

<!--- <cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/js/js.cookie.js">
</cfinvoke> --->

<style>
	.BuilderOption
	{
		<cfif adv EQ 0>
	    	display: none;	
	    </cfif>		
	}

</style>

<cfif inpTemplateFlag EQ 1>
  <cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />
</cfif>

<!--- TEMPLATE PICKER --->
<cfinvoke component="session.sire.models.cfc.campaign" method="getUserCampaignTemplateList" returnvariable="listTemplate"></cfinvoke>

<cfquery name="getTemplateCategory" result="listCategory" datasource="#Session.DBSourceREAD#">
  SELECT *
  FROM simpleobjects.template_category
  ORDER BY Order_int ASC
</cfquery>

<cfquery name="getTemplateCategoryGroup1" result="listCategoryGroup1" datasource="#Session.DBSourceREAD#">
  SELECT CID_int, DisplayName_vch, Description_txt, CategoryName_vch, GroupId_int 
  FROM simpleobjects.template_category
  WHERE GroupId_int = 1
  ORDER BY Order_int ASC
</cfquery>

<cfquery name="getTemplateCategoryGroup2" result="listCategoryGroup2" datasource="#Session.DBSourceREAD#">
  SELECT CID_int, DisplayName_vch, Description_txt, CategoryName_vch, GroupId_int 
  FROM simpleobjects.template_category
  WHERE GroupId_int = 2
  ORDER BY Order_int ASC
</cfquery>


<cfquery name="getTemplateCategoryGroup" result="listCategoryGroup2" datasource="#Session.DBSourceREAD#">
  SELECT CID_int, DisplayName_vch, Description_txt, CategoryName_vch, GroupId_int 
  FROM simpleobjects.template_category
  ORDER BY Order_int ASC
</cfquery>

<div class="wrap-navi-template">
          <nav class="navbar yamm navbar-default menu-template" role="navigation">
            <div class="container">
              <div class="row">
                <div class="col-sm-10 col-sm-offset-1">

                  <div class="clearfix navi-template">
                     <ul class="nav navbar-nav">
                      <li class="dropdown yamm-fw" data-tab="tabReady">
                        <a href="#" class="dropdown-toggle template-tab" data-id="FilterDesc-All-1" data-toggle="dropdown">Ready To Use <span class="hidden-xs">Templates</span></a>
                        <ul class="dropdown-menu">
                          <li>
                            <div class="yamm-content">
                              <div class="row">
                                <div class="col-sm-5 box-border">
                                  <ul class="nav main-menu">
                                   
                                    <li>
                                      <a data-breadcrumb="Show All Templates" data-id="FilterDesc-All-1" class="Template-Picker-Filter-Type" ref1="ALL">
                                        <cfoutput>Show All Templates</cfoutput></a>
                                    </li>
                                    <cfloop query="getTemplateCategoryGroup1">  
                                      <li>
                                        <a data-breadcrumb="<cfoutput>#getTemplateCategoryGroup1.DisplayName_vch#</cfoutput>" data-id="FiterDesc-<cfoutput>#getTemplateCategoryGroup1.CID_int#</cfoutput>" class="Template-Picker-Filter-Type" ref1="<cfoutput>#getTemplateCategoryGroup1.CID_int#</cfoutput>"><cfoutput>#getTemplateCategoryGroup1.DisplayName_vch#</cfoutput></a>
                                      </li>
                                     
                                    </cfloop>
                                  </ul>
                                </div>
                                <div class="col-sm-7 hidden-xs">

                                  <div class="contentFiltersDesc hidden" id="FilterDesc-All-1">
                                    <p class="AlignCenter imgDes col-sm-4"><img class="img-responsive" src="/public/sire/images/learning/ReadyToUse.png" alt=""></p>
                                    <p class="col-sm-8">
                                      <h4><b>Ready To Use Templates</b></h4>
                                      These templates are all Ready to Use! Simply select a category, choose a template, customize your message, and click send!</p>
                                  </div>

                                  <cfset i = 0 >
                                  <cfloop query="getTemplateCategoryGroup1">
                                      <cfif getTemplateCategoryGroup1.CategoryName_vch EQ 'Alerts'>
                                      <!--- <cfset getTemplateCategoryGroup1.Description_txt = REPLACE(getTemplateCategoryGroup1.Description_txt, '/session/sire/pages/support-transactional-api', 'cs-transactional-api') /> --->
                                      </cfif>
                                      <div class="contentFiltersDesc hidden FiterDesc-<cfoutput>#getTemplateCategoryGroup1.CID_int#</cfoutput>" id="FiterDesc-<cfoutput>#getTemplateCategoryGroup1.CID_int#</cfoutput>">
                                        <cfoutput>#getTemplateCategoryGroup1.Description_txt#</cfoutput>
                                      </div>
                                      <cfset i++/>
                                   </cfloop>
                                </div>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </li>

                      <li class="dropdown yamm-fw" data-tab="tabAdvanced">
                        <a href="#" class="dropdown-toggle template-tab" data-id="FilterDesc-All-2" data-toggle="dropdown">Advanced <span class="hidden-xs">Templates</span></a>
                        <ul class="dropdown-menu">
                          <li>
                            <div class="yamm-content">
                              <div class="row">
                                <div class="col-sm-5 box-border">
                                  <ul class="nav main-menu">
                                    <li>
                                      <a data-breadcrumb="Show All Templates" data-id="FilterDesc-All-2" class="Template-Picker-Filter-Type" ref2="ALL">
                                        <cfoutput>Show All Templates</cfoutput></a>
                                    </li>
                                    <cfloop query="getTemplateCategoryGroup2">  
                                      <li><a data-breadcrumb="<cfoutput>#getTemplateCategoryGroup2.DisplayName_vch#</cfoutput>" data-id="FiterDesc-<cfoutput>#getTemplateCategoryGroup2.CID_int#</cfoutput>" class="Template-Picker-Filter-Type" ref1="<cfoutput>#getTemplateCategoryGroup2.CID_int#</cfoutput>"><cfoutput>#getTemplateCategoryGroup2.DisplayName_vch#</cfoutput></a></li>
                                    </cfloop>
                                  </ul>
                                </div>
                                <div class="col-sm-7 hidden-xs">
                                  <div class="contentFiltersDesc hidden" id="FilterDesc-All-2">
                                    <p class="AlignCenter imgDes col-sm-4"><img class="img-responsive" src="/public/sire/images/learning/Advanced_Icon.png" alt=""></p>
                                    <p class="col-sm-8">
                                      <h4><b>Advanced Templates</b></h4>
                                      These advanced templates requires integration with your internal system, please refer to our <a id="api-link" href="" target="_blank">API guide</a> for more information. If you need further assistance, please contact Sire at <a id="mail-to-sp" href="">support@siremobile.com</a> or text "support" to 39492.
                                    </p>
                                  </div>

                                  
                                  <cfloop query="getTemplateCategoryGroup2">
                                      <cfif getTemplateCategoryGroup1.CategoryName_vch EQ 'Alerts'>
                                      <!--- <cfset getTemplateCategoryGroup1.Description_txt = REPLACE(getTemplateCategoryGroup1.Description_txt, '/session/sire/pages/support-transactional-api', 'cs-transactional-api') /> --->
                                      </cfif>
                                      <div class="contentFiltersDesc hidden FiterDesc-<cfoutput>#getTemplateCategoryGroup2.CID_int#</cfoutput>" id="FiterDesc-<cfoutput>#getTemplateCategoryGroup2.CID_int#</cfoutput>">
                                        <cfoutput>#getTemplateCategoryGroup2.Description_txt#</cfoutput>
                                      </div>
                                    
                                   </cfloop>
                                </div>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </div>

                </div>
              </div>
              
            </div>
          </nav>
        </div>


<main class="container-fluid page" style="padding-top: 0;">
  <cfinclude template="../views/commons/credits_available.cfm">
  <section class="row bg-white">
    <div class="content-header">
      <div class="row">
          <div class="col-sm-6 col-md-6 col-lg-6 content-title">
            
            <p class="title">Here's all your Campaign Templates</span></p>
            
            <!--- remove old method: 
			<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>									
			--->
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>
            <cfif shortCode.SHORTCODE NEQ ''>
                <p class="sub-title">Your Short Code is <span class="active"><cfoutput>#shortCode.SHORTCODE#</cfoutput></span></p>  
            <cfelse>
                <p class="sub-title"><a class="active" href="##">Set Keywords</a></p>
            </cfif>
            
          </div>

          <div class="col-sm-4 col-md-4 col-lg-2 more-templates">
            <a class="btn btn-success-custom more-templates-btn" href="/session/sire/pages/template-request">More Templates</a>
          </div>
          
      </div>
    </div>
    <hr class="hrt0">
    <div class="content-body">
        <div class="col-md-12">
          
          <!---
      
        All
        Keyword Response Informational
        Keyword Response Opt In Confirm
        Surveys
        Triggered Transactional SMS
        Complex Interactive Campaigns
      
      
      Industry Filters
      
          border-bottom: 1px solid #ddd;
    border-left: 1px solid #ddd;
    border-right: 1px solid #ddd;
              
      
    <cfdump var="#_CampaignCategory#">
       
     --->

        <div id="createCampaignModal">
          


          <cfif listTemplate.RXRESULTCODE GT 0>  
            <cfset listData = listTemplate.DATA>
            <form action="##" name="form-create-campaign" id="form-create-campaign" autocomplete="off">
                  
                <div class="container-fluid TemplatePickerContainer">
                  <div class="row">
                  <p class="template-breadcrumb"></p>
         
         
                <cfset num =1>
                
                <cfoutput>
                <cfloop query="getTemplateCategoryGroup">
                    <cfset categoryName = getTemplateCategoryGroup.CID_int>
                      
                        <cfif arrayLen(#listData[getTemplateCategoryGroup.CID_int]#) GT 0>
                          <cfloop array="#listData[getTemplateCategoryGroup.CID_int]#" index="item">
                              <cfif Len(item.NAME) GT 150>
                                <cfset displayName = Left(item.NAME, 150) & ' ...'>
                                <cfelse>
                                <cfset displayName = item.NAME>                              
                              </cfif>

                               <cfif Len(item.DESCRIPTION) GT 120>
                                <cfset displayDesc = Left(item.DESCRIPTION, 120) & '<i title= "Read more..." data-description="'&htmlEditFormat(item.DESCRIPTION)&'" class="glyphicon glyphicon-plus read-more-description"></i>'>
                                <cfelse>
                                <cfset displayDesc = htmlEditFormat(item.DESCRIPTION)>                              
                              </cfif>
                             
                              
                             <div class="col-sm-6 col-md-4 col-lg-3 item-grid cat-class-#categoryName# cat-class-all cat-class-all-#getTemplateCategoryGroup.GroupId_int#">
                              
                                <div id="templateid-#item.ID#" href='/session/sire/pages/campaign-edit?templateid=#item.ID#<cfif ADV EQ 1>&ADV=1</cfif><cfif inpTemplateFlag EQ 1>&inpTemplateFlag=1</cfif>' class="content TemplatePickerImage" <cfif item.IMAGE NEQ ''>style="background-image: url(../images/template-picker/#item.IMAGE#);"</cfif>> 
                            
                                  <p class="template-name">#displayName#</p>                                  
                                  
<!---
                                  <h5>
                                    <cfif Len(item.DESCRIPTION) GT 70>
                                    #Left(item.DESCRIPTION, 70) & ' ...'#
                                  <cfelse>
                                    #item.DESCRIPTION#                              
                                  </cfif>
                                  </h5>
--->
                                  
                                  <div class="item_action">
                                    <p class="template-name">#item.NAME#</p>
                                    <div class="select_group_btn">
                                      <button type="button" class="btn btn-success-custom btn-lg btn-select-campaign" >Select</button>
                                      <button type="button" class="btn btn-success-custom btn-lg btn-preview-campaign" data-template-id = '#item.ID#' >Preview</button>
                                    </div>  
                                    <p class="template-desc"><a href="##"> <cfoutput>#displayDesc#</cfoutput> </a> </p>
                                  </div>
                                </div>
                                
                              </div>
                          </cfloop>
                        <cfelse>
                            <div class="col-sm-12 col-md-12 col-lg-12 item-grid item-grid-hidden cat-class-<cfoutput>#categoryName#</cfoutput> cat-class-all" style="display:none; padding: 0 !important">
                            <p class="template-name"> No template.</p>
                          </div>
                        </cfif>
                     
                  <cfset num++>
                </cfloop>
               
               </cfoutput>
               
                </div>
           </div> 
              
            </form>
          <cfelse>  
            <form action="##" name="form-create-campaign" id="form-create-campaign" autocomplete="off">
              <p class="title">No template!</p>
            </form>  
          </cfif>  
          </div>
        </div>
      </div>
  </section>
  <!--- <a href="#LiveDemoVideoModal" id="LiveDemoVideo" class="btn btn-lg btn-primary hidden" data-toggle="modal">Launch Live Demo Video Modal</a> --->
</main>

<!--- Live Demo Video Modal --->
<!--- <div id="LiveDemoVideoModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">How to Create Your First Campaign</h4>
            </div>
            <div class="modal-body">
              <div class="embed-responsive embed-responsive-16by9">
                <iframe id="LiveDemoVideoLink" class="embed-responsive-item" src="https://www.youtube.com/embed/A_tV18dQtSw?rel=0&modestbranding=1" frameborder="0" allowfullscreen></iframe>
              </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-success-custom" data-dismiss="modal">Close</button>
          </div>
        </div>
    </div>
</div> --->

<!--- MODAL POPUP --->
<div class="bootbox modal fade" id="previewCampaignModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel">Preview Campaign</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <!--- <button type="button" class="btn btn-primary btn-success-custom" id="btn-rename-campaign">Save</button> --->
        <button type="button" class="btn btn-default btn-back-custom" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!--- Default bootstrap modal example --->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<cfparam name="variables._title" default="Campaign Manage - Sire">
<cfinclude template="../views/layouts/main.cfm">

<style type="text/css">
  li.yamm-fw a.actived{
    color: #578ca5;
    /*text-decoration: underline;*/
  }

  a.dropdown-toggle.template-tab.actived::after{
    background-color: #578ca5;
    color: #578ca5;
    text-decoration: none;
  }

  .open .dropdown-toggle::after{
    background-color: #578ca5 !important;
    color: #578ca5 !important;
    text-decoration: none !important;
  }
</style>

<script type="application/javascript">

  <!--- Set up page tour for new users --->
  $(function() {
  var isSiblingsActived = 0;  
  var currenttab = "";

  $(document).ready(function(){
    if (!("ontouchstart" in document.documentElement)) {
      document.documentElement.className += " no-touch";
    }
    // Mobile Environment
    if (!$('html').hasClass('no-touch')) {

      $('.Template-Picker-Filter-Type').click(function(){
      isSiblingsActived = 0;
      $(this).parents(".yamm-fw").find(".template-tab").addClass('actived');
      $(this).parents(".yamm-fw").siblings().find(".template-tab").removeClass('actived');
      // if($(this).parents(".yamm-fw").siblings().find('.template-tab').hasClass('actived')){
      //   $(this).parents(".yamm-fw").siblings().find(".template-tab").removeClass('actived');
      //   isSiblingsActived = 1;
      // }
      

      // $('.Template-Picker-Filter-Type').parents(".yamm-fw").find(".template-tab").removeClass('actived');
      // $(this).parents(".yamm-fw").find(".template-tab").addClass('actived');
      $('.cat-class-all').hide();
      
      if($(this).attr("ref1") == "ALL")
      {
        $('.cat-class-all-1').show();
        $('.item-grid-hidden').hide();
        $('#FiterDesc-ALL').show();
      }

      if($(this).attr("ref2") == "ALL")
      {
        $('.cat-class-all-2').show();
        $('.item-grid-hidden').hide();
        $('#FiterDesc-ALL').show();
      }

      <cfoutput>      
        <cfloop query="getTemplateCategoryGroup">
          <cfset categoryName = getTemplateCategoryGroup.CID_int>
            
          if($(this).attr("ref1") == "#categoryName#")
          {
            <!--- $('.contentFiltersDesc').hide(); --->
            $('.cat-class-all').hide();
            $('.cat-class-#categoryName#').show();
            $('##FiterDesc-#categoryName#').show();
            
          }

        </cfloop>      
      </cfoutput>

      var breadcrumb = $(this).data('breadcrumb');
        $(".template-breadcrumb").html(breadcrumb);

      });

      if(!$(".template-tab").hasClass('actived')){

      }

      $(".navi-template .yamm-fw a").on("click", function(e){
          e.preventDefault();
          var sibling = $(this).parent("li").siblings().find(".template-tab");
          var tab = $(this);
          if (sibling.hasClass('actived')) {
            sibling.removeClass('actived');

            $(window).click(function(event) {
              if (tab.parent("li").find($(event.target)).length <= 0) {
                sibling.addClass('actived');
                tab.removeClass('actived');
              } else {
                tab.addClass('actived');
                sibling.removeClass('actived');
              }
            });
          }
      });

    }
    //Desktop environment
    else{
      $('.Template-Picker-Filter-Type').click(function(){
        isSiblingsActived = 0;
        $(this).parents(".yamm-fw").find(".template-tab").addClass('actived');
        $(this).parents(".yamm-fw").siblings().find(".template-tab").removeClass('actived');
        $('.cat-class-all').hide();
        
        if($(this).attr("ref1") == "ALL")
        {
          $('.cat-class-all-1').show();
          $('.item-grid-hidden').hide();
          $('#FiterDesc-ALL').show();
        }

        if($(this).attr("ref2") == "ALL")
        {
          $('.cat-class-all-2').show();
          $('.item-grid-hidden').hide();
          $('#FiterDesc-ALL').show();
        }

        <cfoutput>      
          <cfloop query="getTemplateCategoryGroup">
            <cfset categoryName = getTemplateCategoryGroup.CID_int>
              
            if($(this).attr("ref1") == "#categoryName#")
            {
              <!--- $('.contentFiltersDesc').hide(); --->
              $('.cat-class-all').hide();
              $('.cat-class-#categoryName#').show();
              $('##FiterDesc-#categoryName#').show();
              
            }

          </cfloop>      
        </cfoutput>

        var breadcrumb = $(this).data('breadcrumb');
        $(".template-breadcrumb").html(breadcrumb);

      });

      $(".navi-template .yamm-fw a").on("click", function(e){
          e.preventDefault();

          $(this).parent("li").siblings().removeClass('active');
          $(this).parent("li").addClass('active');
      });

      // Hover group template category
      $(".navi-template .yamm-fw").hover(function(){
          
          $(".main-menu li").removeClass('active');
                
          $(this).siblings().removeClass('active open');
          $(this).addClass('active open');

          $(".contentFiltersDesc").addClass('hidden');

          var contentId = $(this).find('a').data('id');

          $("#"+contentId).removeClass('hidden');
          if ($(this).siblings().find(".template-tab").hasClass('actived')){
            $(this).siblings().find(".template-tab").removeClass('actived');
            isSiblingsActived = 1;
          }
          

      }, function(e){
         $(this).siblings().removeClass('active open');
         $(this).removeClass('active open');
         if (isSiblingsActived == 1 && !$(this).find(".template-tab").hasClass('actived')){
          $(this).siblings().find(".template-tab").addClass('actived');
         }

      });

      // Hover template category
      $(".main-menu li a").hover(function(){
        var dataIdBlock = $(this).data("id");

        $(".contentFiltersDesc").addClass('hidden');
        $("#"+dataIdBlock).removeClass('hidden');

        $(this).parent().siblings().removeClass('active');
        $(this).parent().addClass('active');

      });
    }
  });
    
    
    // Fill modal with content from link href
    $("#myModal").on("show.bs.modal", function(e) {
      var link = $(e.relatedTarget);
      $(this).find(".modal-body").load(link.attr("href"));
    });

    
    $('#FiterDesc-ALL').show();
    
    // only show Ready to use
    $('.cat-class-all').hide();
    $('.cat-class-all-1').show();

  
    <!--- This is moved here so I can override a public section version that does not redirect when not logged in --->
    $( document ).on( "click", ".btn-preview-campaign", function(event) {
    var templateid = $(this).data('template-id');
    
    try{
      $.ajax({
        type: "GET",
        url: '/session/sire/models/cfm/template_preview.cfm?templateid='+templateid,   
        beforeSend: function( xhr ) {
          $('#processingPayment').show();
        },            
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          $('#processingPayment').hide();
          bootbox.dialog({
              message: "Get preview Fail",
              title: "Campaigns",
              buttons: {
                  success: {
                      label: "Ok",
                      className: "btn btn-medium btn-success-custom",
                      callback: function() {}
                  }
              }
          });
        },            
        success:function(d){

          if(d.indexOf('class="home_page"') > -1){
            return false;
          }
          
          $('#processingPayment').hide();
          $('#previewCampaignModal .modal-body').html(d);
          $('#previewCampaignModal').modal('show');
        }
      });
    }catch(ex){
      $('#processingPayment').hide();
      bootbox.dialog({
          message: "Get preview Fail",
          title: "Campaigns",
          buttons: {
              success: {
                  label: "Ok",
                  className: "btn btn-medium btn-success-custom",
                  callback: function() {}
              }
          }
      });
    }
  });

    
    
    
  });


    $('i.read-more-description').click(function(event){
      var description = $(this).data('description');
      var templateName = $(this).parent().parent().parent().find("p.template-name").html();
      bootbox.dialog({
          message: description,
          title: templateName,
          buttons: {
              success: {
                  label: "Close",
                  className: "btn btn-medium btn-success-custom",
                  callback: function() {}
              }
          }
      });

    /* Assign the initially stored url back to the iframe src
    attribute when modal is displayed again */

    $("#LiveDemoVideoModal").on('show.bs.modal', function(){
        $("#LiveDemoVideoLink").attr('src', url);
    });
});

    $('#api-link').click(function(event){
      window.open('<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/cs-transactional-api','_blank');
    });

    $("#mail-to-sp").click(function(event){
      window.location.href = "mailto:support@siremobile.com";
    });

</script>