<cfset variables.menuToUse = 2>
<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addJs("/public/js/datatables/js/jquery.dataTables.min.js")
        .addJs("/public/sire/js/jquery.validationEngine.Functions.js")
        .addJs("/session/sire/assets/pages/scripts/security_credential.js")
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)
        .addCss("/public/sire/js/jquery-ui-1.12.1.custom/jquery-ui.min.css", true)      
        .addJs("/public/sire/js/jquery-ui-1.12.1.custom/jquery-ui.min.js")
        .addJs("/public/sire/js/jquery.mask.min.js")
        .addJs("../assets/global/plugins/jquery-knob/js/jquery.knob.js", true)
        .addCss("/session/sire/assets/global/plugins/uniform/css/uniform.default.css", true)  
        .addJs("../assets/pages/scripts/account.js")      
        .addJs("/session/sire/assets/global/plugins/uniform/jquery.uniform.min.js",true)
        .addJs("../assets/global/plugins/chartjs/Chart.bundle.js", true);
</cfscript>


<!--- get list question --->
<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="getSecurityQuestionList" returnvariable="questionList"></cfinvoke>

<!--- get user info --->
<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfoAccount" returnvariable="userInfo"></cfinvoke>
<!--- <cfdump var="#userInfo#" abort="true"/> --->
<cfoutput>
    <script type="text/javascript">
        var UserIDLogin = "#userInfo.USERID#";
    </script>
</cfoutput>


<cfset check_user_security_question = false>
<cfset hidden_char = "">

<!--- get List Question of User --->
<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserListQuestion" returnvariable="userListQuestion">
    <cfinvokeargument name="detail_question" value=0>
</cfinvoke>


<cfif userListQuestion.RESULT EQ 'SUCCESS' >
    <cfset check_user_security_question = true>
    <cfset hidden_char = "******">
</cfif>
<cfset arrUserQuestion = userListQuestion.LISTQUESTION>
<cfif !isArray(arrUserQuestion)>
    <cfset arrUserQuestion =array()>
    <cfset arrUserQuestion[1] ={QUESTIONID:"",ANSWER:"",ANSWERID:""}>
    <cfset arrUserQuestion[2] ={QUESTIONID:"",ANSWER:"",ANSWERID:""}>
    <cfset arrUserQuestion[3] ={QUESTIONID:"",ANSWER:"",ANSWERID:""}>
</cfif>

<!--- <cfdump var="#arrUserQuestion#" abort="true"> --->

<cfinvoke component="session.cfc.administrator.credential" method="countCredentials" returnvariable="countCredentials">

<cfset collectionListQuestions = createObject("component", "session.sire.models.cfc.myaccountinfo").getSecurityQuestionList().DATA />

<cfset userInfoSecu = createObject("component", "session.sire.models.cfc.myaccountinfo").GetUserListQuestion() />
<cfset listUserAQ = userInfoSecu.LISTQUESTION />

<!--- <cfdump var="#userInfo#" abort="true"> --->
<!--- <cfinclude template="/session/sire/configs/paths.cfm"> --->
<!--- Profile --->

<cfif userListQuestion.RESULT EQ 'SUCCESS' >
    <cfset check_user_security_question = true>
</cfif>
<cfif userInfo.RESULT NEQ 'SUCCESS'>
    <cflocation url = '/' addtoken="false">
</cfif>
<!---END Profile --->


<div class="portlet light bordered">
    <div class="portlet-body">
        <div class="new-inner-body-portlet">
            <form role="form" class="form-gd" autocomplete="off" name="my_account_form" id="my_account_form">
                <div class="form-body">
                    <div class="clearfix">
                        <h3 class="form-heading">My Profile</h3>
                    </div>

                    <div class="uk-grid">
                        <div class="uk-width-5-6@l">
                            <div class="uk-grid uk-grid-small my-profile-padding" uk-grid>
                                <div class="uk-width-1-2@s">
                                    <div class="form-group">
                                        <label>First Name <span>*</span></label>
                                        <input type="text" class="form-control validate[required,custom[noHTML]]" placeholder="" id="fname" name="fname" maxlength="50" value="<cfoutput>#userInfo.USERNAME#</cfoutput>" data-prompt-position="topLeft:100">
                                    </div>
                                </div>
                                
                                <div class="uk-width-1-2@s">
                                    <div class="form-group">
                                        <label>Last Name <span>*</span></label>
                                        <input type="text" class="form-control validate[required,custom[noHTML]]" id="lname" name="lname" maxlength="50" value="<cfoutput>#userInfo.LASTNAME#</cfoutput>" data-prompt-position="topLeft:100">
                                    </div>
                                </div>

                                <div class="uk-width-1-2@s">
                                    <div class="form-group">
                                        <label>SMS Number <span>*</span></label>
                                        <input type="text" class="form-control validate[required,custom[usPhoneNumberII]]" id="sms_number" name="sms_number" maxlength="15" value="<cfoutput>#userInfo.FULLMFAPHONE#</cfoutput>" data-prompt-position="topLeft:100">
                                    </div>
                                </div>

                                <div class="uk-width-1-2@s">
                                    <div class="form-group">
                                        <label>E-mail <span>*</span></label>
                                        <input type="email" class="form-control validate[required,custom[email]]" id="emailadd" name="emailadd" value="<cfoutput>#userInfo.FULLEMAIL#</cfoutput>" maxlength="250" data-prompt-position="topLeft:100" disabled>
                                    </div>
                                </div>

                                <div class="uk-width-1-2@s">
                                    <div class="form-group">
                                        <label>Address</label>
                                        <input type="text" class="form-control" id="Address1_vch" name="Address1_vch" maxlength="250" value="<cfoutput>#userInfo.Address1_vch#</cfoutput>" data-prompt-position="topLeft:100">
                                    </div>
                                </div>

                                <div class="uk-width-1-2@s">
                                    <div class="form-group">
                                        <label>City</label>
                                        <input type="text" class="form-control" id="City_vch" name="City_vch" maxlength="250" value="<cfoutput>#userInfo.City_vch#</cfoutput>" data-prompt-position="topLeft:100">
                                    </div>
                                </div>

                                <div class="uk-width-1-2@s">
                                    <div class="form-group">
                                        <label>State</label>
                                        <input type="text" class="form-control" id="State_vch" name="State_vch" maxlength="250" data-prompt-position="topLeft:100"
                                        value="<cfoutput>#userInfo.State#</cfoutput>">
                                    </div>
                                </div>

                                <div class="uk-width-1-2@s">
                                    <div class="form-group">
                                        <label>Zip Code</label>
                                        <input type="text" class="form-control" id="zip_code" name="zip_code" maxlength="10" value="<cfoutput>#userInfo.POSTALCODE#</cfoutput>" data-prompt-position="topLeft:100">
                                    </div>
                                </div>

                                <div class="uk-width-1-1 hidden">
                                    <div class="form-group new-control-checkbox">
                                        <div class="uk-grid-small uk-child-width-auto" uk-grid>
<!---                                             <label>Default Authentication Method:</label>
                                            <label>Mobile <input class="uk-checkbox uk-margin-remove-top uk-margin-small-left radio-sendtype" value="3" type="checkbox" <cfif userInfo.MFACONTACTTYPE EQ 3> <cfoutput> checked </cfoutput> </cfif> ></label>
                                            <label>Email <input class="uk-checkbox uk-margin-remove-top uk-margin-small-left radio-sendtype" value="2" type="checkbox"  <cfif userInfo.MFACONTACTTYPE EQ 2> <cfoutput> checked </cfoutput> </cfif>></label>
                                            <input type="hidden" name="" id="sendType" value=""> --->
                                            <label class="checkbox-inline">
                                                Default Authentication Method:
                                            </label>
                                            <label class="checkbox-inline">
                                                Mobile
                                                <input type="radio" id="inlineRadio21" class="radio-sendtype" value="3" name="sendType" <cfif userInfo.MFACONTACTTYPE EQ 3> <cfoutput> checked </cfoutput> </cfif> >
                                            </label>
                                            <label class="checkbox-inline">
                                                Email
                                                <input type="radio" id="inlineRadio22" class="radio-sendtype" value="2" name="sendType" <cfif userInfo.MFACONTACTTYPE EQ 2> <cfoutput> checked </cfoutput> </cfif>>
                                            </label>
                                            <input type="hidden" name="" id="sendType" value="">
                                        </div>
                                    </div>
                                </div>

                                <div class="uk-width-1-1">
                                    <div class="form-group">
                                        <button type="submit" class="btn newbtn green-gd" id="btn_save_my_account">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="uk-grid uk-grid-upupsmall uk-grid-match" uk-grid>

    <!--- Change password --->
    <div class="uk-width-1-2@m">
        <div class="portlet light bordered">
            <div class="portlet-body">
                <div class="new-inner-body-portlet">
                    <form  role="form" name="change-password" class="form-gd">
                        <div class="form-body">
                            <div class="clearfix">
                                <h3 class="form-heading">Change Password</h3>
                            </div>

                            <div class="guide-password">
                                <p class="new-sub-title">Minimum Secure Password Requirements</p>
                                <div class="uk-grid">
                                    <div class="uk-width-1-2@s">
                                        <ul class="list-guide-pass">
                                            <li><i>must be at least 8 characters</i></li>
                                            <li><i>must have at least 1 number</i></li>
                                            <li><i>must have at least 1 letter</i></li>
                                            <!--- Reduced this requirement to any case letter2017-08-18 Lee Peterson  --->
<!---                                             <li><i>must have at least 1 uppercase letter</i></li> --->
                                        </ul>
                                    </div>
                                    <div class="uk-width-1-2@s">
                                        <ul class="list-guide-pass">
<!---
                                            <li><i>must have at least 1 lower case letter</i></li>
                                            
                                            <!--- Removed this requirement	2017-08-18 Lee Peterson --->
                                            
                                            li><i>must have at least 1 special character</i></li>
--->
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="uk-grid">
                                <div class="uk-width-5-6@l">
                                    <div class="uk-grid uk-grid-small my-profile-padding" uk-grid>
                                        <div class="uk-width-1-1">
                                            <div class="form-group">
                                                <label>E-mail</label>
                                                <input type="text"  name="email" value="<cfoutput>#SESSION.EmailAddress#</cfoutput>" readonly="readonly" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        
                                        <div class="uk-width-1-1">
                                            <div class="form-group">
                                                <label>Old Password <span>*</span></label>
                                                <input type="password" name="old-password" class="form-control validate[required,funcCall[isValidPassword]]" placeholder="">
                                            </div>
                                        </div>

                                        <div class="uk-width-1-1">
                                            <div class="form-group">
                                                <label>New Password <span>*</span></label>
                                                <input type="password" name="new-password" id="new-password" class="form-control validate[required,funcCall[isValidPassword]]" placeholder="">
                                            </div>
                                        </div>

                                        <div class="uk-width-1-1">
                                            <div class="form-group uk-margin-bottom">
                                                <label>Confirm New Password <span>*</span></label>
                                                <input type="password"  name="renew-password" class="form-control validate[required,equals[new-password],funcCall[isValidPassword]]"  placeholder="">
                                            </div>
                                        </div>

                                        <div class="uk-width-1-1">
                                            <div class="form-group">
                                                <button type="submit" class="btn newbtn green-gd">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <div class="uk-width-1-2@m">
        <div class="portlet light bordered">
            <div class="portlet-body">
                <div class="new-inner-body-portlet">
                    <div class="form-gd">
                        <div class="form-body">
                            
                            <div class="clearfix">
                                <h3 class="form-heading"><cfif userInfoSecu.RESULT == "SUCCESS"> Update <cfelse> Create </cfif> Security Questions</h3>
                            </div>

                            <cfif check_user_security_question>

                                <p class="new-sub-title">Select three security questions below. These questions will help us verify your identity should you forget your password.</p>
                                <div class="uk-grid">
                                    <div class="uk-width-1-1">
                                        <div class="uk-grid uk-grid-small my-quesstion-padding" uk-grid>
                                            <cfoutput>
                                            <form name="frm-security-questions">
                                                <cfset qstNumber = 0 />
                                                <cfloop from="1" to="#collectionListQuestions.len()#" index="key">
                                                    <cfset qstNumber++  />
                                                    <div class="uk-width-1-1">
                                                        <div class="form-group">
                                                            <label>Question #key# </label>
                                                            <select data-number="qst-#qstNumber#" name="question_id[]" class="form-control">
                                                                <cfloop array="#collectionListQuestions["list_"&key]#" index="qstItem">
                                                                    <option <cfif qstItem.ID EQ listUserAQ[qstNumber]["QUESTIONID"]>selected</cfif> value="#qstItem.ID#">#qstItem.QUESTION#</option>
                                                                </cfloop>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="uk-width-1-1">
                                                        <div class="form-group">
                                                            <label>Your Answer <span>*</span></label>
                                                            <input type="text" class="form-control validate[required]" data-number="anw-#qstNumber#" id="inp_simon_#qstNumber#" name="answer_txt[]" placeholder="" value="#hidden_char#">
                                                            <input type="hidden" class="form-control" name="hidden_answer_id[]" value="#listUserAQ[qstNumber]['ANSWERID']#">
                                                        </div>
                                                    </div>
                                                </cfloop>
                                                <br>
                                                <div class="uk-width-1-1">
                                                    <div class="form-group">
                                                        <button type="submit" class="btn newbtn green-gd">Save</button>
                                                        <a href='/session/sire/pages/account' class="btn newbtn green-cancel">Cancel</a>
                                                    </div>
                                                </div>

                                            </form>
                                            </cfoutput>
                                        </div>
                                    </div>
                                </div>
                            <cfelse>

                                <p class="new-sub-title">Please define 3 security questions and answers, in the event you lost your device or password.</p>
                                <!--- ADD Security FORM --->
                                <div class="uk-grid uk-sin-padding">
                                    <div class="uk-width-1-1">
                                        <div class="uk-grid uk-grid-small my-quesstion-padding" uk-grid>

                                            <form class="form-horizontal " method="post" action="##" name="add_security_question_form" id="add_security_question_form" autocomplete="off">
                                                <cfset num =1>
                                                <cfloop index="userQuestion" array="#arrUserQuestion#">
                                                <cfset selected = ''>
                                                <cfset questionListIndex = 'list_'&num>
                                                    <div class="uk-width-1-1">
                                                        <div class="form-group">
                                                            <label>Question <cfoutput>#num#</cfoutput></label>
                                                            <div class="">
                                                                <select class="form-control" name="question_id[]" id="select_question_<cfoutput>#num#</cfoutput>">
                                                                    <cfloop array="#questionList.DATA[questionListIndex]#" index="question_item">
                                                                        <cfif userQuestion['QUESTIONID'] EQ question_item.ID >
                                                                            <cfset selected = 'selected'>           
                                                                        <cfelse>    
                                                                            <cfset selected = ''>       
                                                                        </cfif>
                                                                        <option value="<cfoutput>#question_item.ID#</cfoutput>" <cfoutput>#selected#</cfoutput> >
                                                                            <cfoutput>#question_item.QUESTION#</cfoutput>
                                                                        </option>
                                                                    </cfloop>
                                                                </select> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="uk-width-1-1">    
                                                        <div class="form-group">
                                                            <label>Your Answer <span class="text-danger">*</span></label>
                                                            <div class="">
                                                                <input type="text" class="form-control validate[required] input_answer" id="input_answer_<cfoutput>#num#</cfoutput>" maxlength="255" name="answer_txt[]" value="<cfoutput>#hidden_char#</cfoutput>" data-prompt-position="topLeft:100">
                                                            </div>
                                                        </div>
                                                    </div>    
                                                    <cfset num++>
                                                </cfloop>
                                                <div class="uk-width-1-1">
                                                    <div class="form-group">
                                                        <button type="submit" class="btn newbtn green-gd" id="btn_add_security_question text-uppercase">Create</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </cfif>
                       </div>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="portlet light bordered mt-25">
    <div class="portlet-body">
        <div class="new-inner-body-portlet">
                <div class="form-gd">
                    <div class="clearfix">
                        <h3 class="form-heading">Security Credentials</h3>
                    </div>
                    <div class="table-responsive re-table">          
                        <table id="tblListCredentials" class="table-responsive table-striped dataTables_wrapper dataTable">
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <button class="btn green-gd pull-right btn-new-access-key mt-20">Create new access key</button>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
<div class="modal fade" id="md-contact-support" tabindex="-1" role="dialog" aria-hidden="true">	
    <div class="modal-dialog set-width-350">
        <div class="modal-content">                    
            <div class="modal-body"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <div class="form-group  ">
                    <center>
                        <h4 class="portlet-heading2">Customer Support</h4>                                                
                        <div class="md-import-row-span"><span class="span-title">EmailL:</span> <a href="mailto:hello@siremobile.com?Subject=SupportImportTools" target="_top">hello@siremobile.com</a></div>
                        <div class="md-import-row-span"><span class="span-title">Phone:</span> (888) 747-4411</div>
                        <div class="md-import-row-span"><span class="span-title">Chat with us: </span> 9AM - 6PM Pacific </div>                         
                    </center>
                </div>
            </div>        
        </div>
    </div>
</div>  


<cfsavecontent variable="variables.portleft">
<div class="portlet light bordered box-widget-sidebar hidden-sm hidden-xs">
    <cfset TotalOrgSteps = 9 />

    <!--- Assume user name and password are already set from sign up --->
    <cfset TotalStepsComplete = 0 />

    <!--- Check for first name --->
    <cfif LEN(TRIM(userInfo.USERNAME)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>

    <!--- Check for last name --->
    <cfif LEN(TRIM(userInfo.LASTNAME)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>

    <!--- Check for SMS number --->
    <cfif LEN(TRIM(userInfo.FULLMFAPHONE)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>

    <!--- Check for Email number --->
    <cfif LEN(TRIM(userInfo.FULLEMAIL)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>

    <!--- Check for SMS number --->
    <cfif LEN(TRIM(userInfo.Address1_vch)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>
    <cfif LEN(TRIM(userInfo.City_vch)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>
    <cfif LEN(TRIM(userInfo.State)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>
    <cfif LEN(TRIM(userInfo.POSTALCODE)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>
    <cfif userInfo.MFACONTACTTYPE GTE 2> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>

    <div class="portlet-body">
        <div class="wrap-dial-step">
            <!--- <input type="hidden" name="" id="percent-complete" value="<cfoutput>#(TotalStepsComplete/TotalOrgSteps)*100#</cfoutput>"> --->
            <input type="hidden" name="" id="percent-complete" value="<cfoutput>#(TotalStepsComplete/TotalOrgSteps)*100#</cfoutput>">
            <h4 class="stt-step"><span class="pie_value"></span>% Complete</h4>
            <h4 class="sub-stt-step hide">You're Ready to Create a Campaign</h4>
            <!--- <div class="">
                <input type="text" id="dialStep" class="dial">
            </div> --->

            <div class="bg-step">
                <canvas id="dialStep"></canvas>
            </div>

            <!--- <a href="##" class="click-test">Test</a> --->
        </div>
    </div>
</div>
</cfsavecontent>

<cfparam name="variables._title" default="Account - Sire">
<cfinclude template="../views/layouts/master.cfm">