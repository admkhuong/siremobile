<cfset variables.menuToUse = 2 />

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true);
</cfscript>

<div class="portlet light bordered">
	<div class="portlet-body">
		<div class="new-inner-body-portlet">
			<form class="form-gd">
				<div class="form-body">
					<div class="clearfix">
	                    <h3 class="form-heading">My Profile</h3>
	                </div>

	                <div class="uk-grid">
	                	<div class="uk-width-5-6@l">
	                		<div class="uk-grid uk-grid-small my-profile-padding" uk-grid>
			                	<div class="uk-width-1-2@s">
			                		<div class="form-group">
			                            <label>First Name <span>*</span></label>
			                            <input type="text" class="form-control" placeholder="">
			                        </div>
			                	</div>
			                	
			                	<div class="uk-width-1-2@s">
			                		<div class="form-group">
			                            <label>Last Name <span>*</span></label>
			                            <input type="text" class="form-control" placeholder="">
			                        </div>
			                	</div>

			                	<div class="uk-width-1-2@s">
			                		<div class="form-group">
			                            <label>SMS Number <span>*</span></label>
			                            <input type="text" class="form-control" placeholder="">
			                        </div>
			                	</div>

			                	<div class="uk-width-1-2@s">
			                		<div class="form-group">
			                            <label>E-mail <span>*</span></label>
			                            <input type="text" class="form-control" placeholder="">
			                        </div>
			                	</div>

			                	<div class="uk-width-1-2@s">
			                		<div class="form-group">
			                            <label>Address</label>
			                            <input type="text" class="form-control" placeholder="">
			                        </div>
			                	</div>

			                	<div class="uk-width-1-2@s">
			                		<div class="form-group">
			                            <label>Address 2</label>
			                            <input type="text" class="form-control" placeholder="">
			                        </div>
			                	</div>

			                	<div class="uk-width-1-2@s">
			                		<div class="form-group">
			                            <label>State</label>
			                            <input type="text" class="form-control" placeholder="">
			                        </div>
			                	</div>

			                	<div class="uk-width-1-2@s">
			                		<div class="form-group">
			                            <label>Zip Code</label>
			                            <input type="text" class="form-control" placeholder="">
			                        </div>
			                	</div>

			                	<div class="uk-width-1-1">
			                		<div class="form-group new-control-checkbox">
			                			<div class="uk-grid-small uk-child-width-auto" uk-grid>
			                				<label>Default Authentication Method:</label>
								            <label>Mobile <input class="uk-checkbox uk-margin-remove-top uk-margin-small-left" type="checkbox"></label>
								            <label>Email <input class="uk-checkbox uk-margin-remove-top uk-margin-small-left" type="checkbox"></label>
								        </div>
			                		</div>
			                	</div>

			                	<div class="uk-width-1-1">
			                		<div class="form-group">
			                			<button type="submit" class="btn newbtn green-gd">Save</button>
			                		</div>
			                	</div>
			                </div>
	                	</div>
	                </div>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="uk-grid uk-grid-upupsmall uk-grid-match" uk-grid>
	<div class="uk-width-1-2@m">
		<div class="portlet light bordered">
			<div class="portlet-body">
				<div class="new-inner-body-portlet">
					<form class="form-gd">
						<div class="form-body">
							<div class="clearfix">
			                    <h3 class="form-heading">Change Password</h3>
			                </div>

			                <div class="guide-password">
			                	<p class="new-sub-title">Secure Password Requirements</p>
			                	<div class="uk-grid">
			                		<div class="uk-width-1-2@s">
			                			<ul class="list-guide-pass">
			                				<li>must be at least 8 characters</li>
			                				<li>must have at least 1 number</li>
			                				<li>must have at least 1 uppercase letter</li>
			                			</ul>
			                		</div>
			                		<div class="uk-width-1-2@s">
			                			<ul class="list-guide-pass">
			                				<li>must have at least 1 lower case letter</li>
			                				<li>must have at least 1 special character</li>
			                			</ul>
			                		</div>
			                	</div>
			                </div>

			                <div class="uk-grid">
			                	<div class="uk-width-5-6@l">
			                		<div class="uk-grid uk-grid-small my-profile-padding" uk-grid>
					                	<div class="uk-width-1-1">
					                		<div class="form-group">
					                            <label>E-mail</label>
					                            <input type="text" class="form-control" placeholder="">
					                        </div>
					                	</div>
					                	
					                	<div class="uk-width-1-1">
					                		<div class="form-group">
					                            <label>Old Password <span>*</span></label>
					                            <input type="text" class="form-control" placeholder="">
					                        </div>
					                	</div>

					                	<div class="uk-width-1-1">
					                		<div class="form-group">
					                            <label>New Password <span>*</span></label>
					                            <input type="text" class="form-control" placeholder="">
					                        </div>
					                	</div>

					                	<div class="uk-width-1-1">
					                		<div class="form-group uk-margin-bottom">
					                            <label>Confirm New Password <span>*</span></label>
					                            <input type="text" class="form-control" placeholder="">
					                        </div>
					                	</div>

					                	<div class="uk-width-1-1">
					                		<div class="form-group">
					                			<button type="submit" class="btn newbtn green-gd">Save</button>
					                		</div>
					                	</div>
					                </div>
			                	</div>
			                </div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="uk-width-1-2@m">
		<div class="portlet light bordered">
			<div class="portlet-body">
				<div class="new-inner-body-portlet">
					<form class="form-gd">
						<div class="form-body">
							<div class="clearfix">
			                    <h3 class="form-heading">Create Security Questions</h3>
			                </div>

			                <p class="new-sub-title">Please define 3 security questions and answers, in the event you lost your devide or password.</p>

			                <div class="uk-grid">
			                	<div class="uk-width-1-1">
			                		<div class="uk-grid uk-grid-small my-quesstion-padding" uk-grid>
					                	<div class="uk-width-1-1">
					                		<div class="form-group">
					                            <label>Question 1</label>
					                            <select name="" id="" class="form-control">
					                            	<option value="">test</option>
					                            	<option value="">test</option>
					                            	<option value="">test</option>
					                            </select>
					                        </div>
					                	</div>
					                	
					                	<div class="uk-width-1-1">
					                		<div class="form-group">
					                            <label>Your Answer <span>*</span></label>
					                            <input type="text" class="form-control" placeholder="">
					                        </div>
					                	</div>

					                	<div class="uk-width-1-1">
					                		<div class="form-group">
					                            <label>Question 2</label>
					                            <select name="" id="" class="form-control">
					                            	<option value="">test</option>
					                            	<option value="">test</option>
					                            	<option value="">test</option>
					                            </select>
					                        </div>
					                	</div>
					                	
					                	<div class="uk-width-1-1">
					                		<div class="form-group">
					                            <label>Your Answer <span>*</span></label>
					                            <input type="text" class="form-control" placeholder="">
					                        </div>
					                	</div>

					                	<div class="uk-width-1-1">
					                		<div class="form-group">
					                            <label>Question 3</label>
					                            <select name="" id="" class="form-control">
					                            	<option value="">test</option>
					                            	<option value="">test</option>
					                            	<option value="">test</option>
					                            </select>
					                        </div>
					                	</div>
					                	
					                	<div class="uk-width-1-1">
					                		<div class="form-group uk-margin-bottom">
					                            <label>Your Answer <span>*</span></label>
					                            <input type="text" class="form-control" placeholder="">
					                        </div>
					                	</div>

					                	<div class="uk-width-1-1">
					                		<div class="form-group">
					                			<button type="submit" class="btn newbtn green-gd">Save</button>
					                			<button type="button" class="btn newbtn green-cancel">Cancel</button>
					                		</div>
					                	</div>
					                </div>
			                	</div>
			                </div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="portlet light bordered mt-25">
	<div class="portlet-body">
		<div class="new-inner-body-portlet">
			<div class="form-gd">
				<div class="form-body">
					<div class="clearfix">
	                    <h3 class="form-heading">Security Credentials</h3>
	                </div>

	                <div class="">
	                	<h4>Table here...</h4>
	                </div>
				</div>
			</div>
		</div>
	</div>
</div>

<cfparam name="variables._title" default="My Account - Sire">

<cfinclude template="../views/layouts/master.cfm">