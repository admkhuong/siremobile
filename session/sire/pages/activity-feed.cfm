<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/js/jquery.tmpl.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/js/jquery-ui-1.11.4.custom/jquery-ui.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/filter_table.css">
</cfinvoke>

<cfinclude template="/session/sire/configs/paths.cfm" />

<cfparam name="variables._title" default="Activity Feed - Sire">

<main class="container-fluid page my-plan-page">
    <cfinclude template="../views/commons/credits_available.cfm">

	<cfinvoke component="session.sire.models.cfc.order_plan" method="GetNextPlanUpgrade" returnvariable="nextPlanUpgrade">
		<cfinvokeargument name="inpPlanOrder" value="#RetUserPlan.PLANORDER#">
	</cfinvoke>

    <section class="row bg-white">
        <div class="content-header">
        	<div class="row">
				<div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 content-title">
                    <cfinclude template="../views/commons/welcome.cfm">
                </div>
            </div>
        </div>
        <hr class="hrt0">
		<div class="content-body">
			<div class="container-fluid">
				<p class="sub-title">My Activity Feed</p>
				<div class="col-lg-12 col-sm-12 col-md-12 no-padding-right">
					<div class="row">
						<div id="filter">
				            <cfoutput>
					            <!---set up column --->
					            <cfset datatable_ColumnModel = arrayNew(1)>
					            <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Campaign Name', CF_SQL_TYPE = 'CF_SQL_VARCHAR', TYPE='TEXT', SQL_FIELD_NAME = ' Desc_vch '})>
					            <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Created date', CF_SQL_TYPE = 'CF_SQL_TIMESTAMP', TYPE='DATE', SQL_FIELD_NAME = ' dt '})>
					            <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Contact', CF_SQL_TYPE = 'CF_SQL_VARCHAR', TYPE='KEYWORD', SQL_FIELD_NAME = ' ContactString_vch '})>
					            <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'SMS', CF_SQL_TYPE = 'CF_SQL_VARCHAR', TYPE='KEYWORD', SQL_FIELD_NAME = ' sms '})>
					            <cfset datatable_jsCallback = "GetActivityList">
					            <div class="col-lg-12"><cfinclude template="campaign-datatable-filter.cfm" ></div>
				            </cfoutput>
			        	</div>
			        </div>
					<div class="row list">
						<div class="col-md-12 col-sm-12 col-lg-12">
							<div class="table-responsive">
								<table id="activityList" class="table table-striped table-bordered table-hover table-custom">
									<thead>
										<tr class="center">
											<th style="width: 5%">No.</th><th style="width: 10%">CampaignID</th><th style="width: 20%">Campaign Name</th><th style="width: 10%">Contact</th><th style="width: 5%">Type</th><th style="width: 40%">SMS</th><th style="width: 20%">Created</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/font-awesome.min.css"/>
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js"/>
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/site/activity-feed.js"/>
</cfinvoke>

<cfinclude template="../views/layouts/main.cfm" />

<style type="text/css">
	th {
		text-align: center;
	}
	td {
		text-align: center;
	}
</style>