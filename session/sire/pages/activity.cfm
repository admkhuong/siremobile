<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/js/jquery.tmpl.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/js/jquery-ui-1.11.4.custom/jquery-ui.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/filter_table.css">
</cfinvoke>

<!--- <cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/activityapp.css">
</cfinvoke>  --->

<cfinclude template="/session/sire/configs/paths.cfm" />

<cfparam name="variables._title" default="Activity - Sire">

<main class="container-fluid page my-plan-page">
    <cfinclude template="../views/commons/credits_available.cfm">

	<cfinvoke component="session.sire.models.cfc.order_plan" method="GetNextPlanUpgrade" returnvariable="nextPlanUpgrade">
		<cfinvokeargument name="inpPlanOrder" value="#RetUserPlan.PLANORDER#">
	</cfinvoke>

    <section class="row bg-white">
        <div class="content-header">
        	<div class="row">
				<div class="col-sm-4 col-lg-3 col-md-4 col-xs-6 content-title">
                    <cfinclude template="../views/commons/welcome.cfm">
                </div>
            </div>
        </div>
        <hr class="hrt0">
		<div class="content-body">
			<div class="container-fluid">
				<p class="sub-title">My Activity</p>

				<div class="col-lg-12 col-sm-12 col-md-12 no-padding-right">
					<div class="row list">
						<div class="col-md-12 col-sm-12 col-lg-12">
							<div class="table-responsive">
								<table id="activityList" class="table table-striped table-bordered table-hover table-custom">
									<thead>
										<tr class="center">
											<th style="width: 5%">ID</th><th style="width: 5%">CampaignID</th><th style="width: 20%">Campaign Name</th><th style="width: 10%">Contact</th><th style="width: 5%">Type</th><th style="width: 40%">SMS</th><th style="width: 10%">Created</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/font-awesome.min.css"/>
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js"/>
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/site/activity.js"/>
</cfinvoke>

<cfinclude template="../views/layouts/main.cfm" />

<style type="text/css">
	th {
		text-align: center;
	}
	td {
		text-align: center;
	}
</style>