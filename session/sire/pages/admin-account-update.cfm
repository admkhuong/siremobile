<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
    .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
    .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true);
    // .addJs("/session/sire/assets/global/plugins/uniform/jquery.uniform.min.js",true)
    // .addCss("/session/sire/assets/global/plugins/uniform/css/uniform.default.css", true) ;
</cfscript>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js">
</cfinvoke>
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/js/jquery.tmpl.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/sire/js/moment.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/js/jquery-ui-1.11.4.custom/jquery-ui.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/filter_table.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/assets/global/scripts/filter-bar-generator.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/js/site/admin_account_update.js">
</cfinvoke>

<cfinvoke component="public.sire.models.cfc.plan" method="GetActivedPlans" returnvariable="plans"></cfinvoke>

<!--- Get Payment method setting --->
<cfinvoke component="session.sire.models.cfc.billing" method="GetPaymentMethodSetting" returnvariable="RetGetPaymentMethodSetting"></cfinvoke>

<cfinclude template="inc_upgrade_plan_popup_admin.cfm"/>

<cfinclude template="/session/sire/configs/paths.cfm">
<cfinclude template="/session/sire/configs/credits.cfm">


<style>
	span.has-changed {
		/*color: red;*/
	}
</style>

<div class="portlet light bordered ">
	<div class="portlet-body row">
	<div class="col-sm-12 col-md-4">
		<h2 class="page-title">Admin - Accounts management</h2>
	</div>
	<div class="col-sm-12 col-md-8">
		<div id="box-filter">

		</div>
	</div>
		<div class="content-body">
			<div class="container-fluid">
				<div class="re-table" id="tableaccountmanagement">
					<div class="table-responsive">
						<table id="tblListEMS" class="table table-striped table-bordered table-hover">

						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="user-account-profile" role="dialog">
	<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong>Account Profile Update</strong></h4>
				</div>
				<div class="modal-body form-gd">
					<div class="form-group">
						<label><b>First Name</b> <span>*</span></label>
						<input type="text" class ="form-control validate[required,custom[noHTML]]" id="user_firstname" value="">
						<p id ="invalidfname" style="color: red; display:none; margin: 2px 0;">* First Name is required.</p>
					</div>
					<div class="form-group">
						<label><b>Last Name</b> <span>*</span></label>
						<input type="text" class ="form-control validate[required,custom[noHTML]]" id="user_lastname" value="">
						<p id ="invalidlname" style="color: red; display:none; margin: 2px 0;">* Last Name is required.</p>
					</div>
					<div class="form-group">
						<label><b>SMS Number</b> <span>*</span></label>
						<input type="text" class ="form-control" id="user_phone" value="">
						<p id ="invalidphone" style="color: red; display:none; margin: 2px 0;">* Invalid phone number.</p>
					</div>
					<div class="form-group">
						<label><b>E-mail</b> <span>*</span></label>
						<input type="text" class ="form-control validate[required,custom[email]]" id="user_email" value="">
						<p id ="invalidemail" style="color: red; display:none; margin: 2px 0;">* Invalid email address.</p>
						<input type="hidden" class ="form-control" id="user_id" value="">
					</div>
					<div class="form-group">
						<label><b>MFA Type</b> <span>*</span></label>
						<select class="form-control validate[required]" id="user_mfa">
							<option value="0">Off</option>
							<option value="1">Phone Only</option>
							<option value="2">Phone and Email</option>
						</select>
					</div>
					<div class="form-group">
						<label><b>Security Credentials</b> <span>*</span></label>
						<select class="form-control validate[required]" id="user_security_credential">
							<option value="0">Not Allow</option>
							<option value="1">Allow</option>
						</select>
					</div>
					<div class="form-group">
						<label><b>Allow Google MFA</b> <span>*</span></label>
						<select class="form-control validate[required]" id="user_google_mfa">
							<option value="1">Yes</option>
							<option value="0">No</option>
						</select>
					</div>
					<div class="form-group">
						<label><b>Limited Purchase Amount </b> <span>*</span></label>
						<input type="text" id="user_amountlimited" class="form-control validate[required,custom[Number]]" name="value" value="">
						<p id ="invalidnumber" style="color: red; display:none; margin: 2px 0;">* Invalid number [maximum is 2 decimals].</p>
					</div>
					<div class="form-group">
						<label><b>Allow Edit XMLControlstring</b> <span>*</span></label>
						<select class="form-control validate[required]" id="user_XMLCSEditable">
							<option value="1">Yes</option>
							<option value="0">No</option>
						</select>
					</div>
					<div class="form-group">
						<label><b>Default Payment Gateway</b> <span>*</span></label>
						<select class="form-control validate[required]" id="user_default_payment_gw">
							<!--- <option value="1">Worldpay</option> --->
							<option value="2">Paypal</option>
							<option value="3">Mojo</option>
							<option value="4">Total Apps</option>
						</select>
					</div>
					<div class="form-group">
						<label><b>Allow Capture User Credit Card Info</b> <span>*</span></label>
						<select class="form-control validate[required]" id="userCaptureCCInfo">
							<option value="1">Yes</option>
							<option value="0">No</option>
						</select>
					</div>
				</div>
				<div class="modal-footer">
					<button id ="btn-acc-profile" type="submit" class="btn-save btn btn-success-custom" >Save</button>
				</div>
			</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="user-credit-update" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><strong>Credit Adjustment</strong></h4>
			</div>
			<div class="modal-body">
				<!--- <ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#credit-update-action">Add Credit</a></li>
					<li><a data-toggle="tab" href="#credit-update-log">View Log</a></li>
				</ul> --->
				<div <!--- class="tab-content" --->>
					<div id="credit-update-action" <!--- class="tab-pane fade in active" --->>
						<div class="row">
							<div class="col-xs-12">
								<div class="row">
									<div class="col-md-12">
										<p for=""><strong id="user-name"></strong> - <strong id="user-email"></strong></p>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 table-responsive">
										<table id="account-credit-table" class="table table-striped table-responsive">
											<thead>
												<tr>
													<th></th><th>Current Credit</th><th>Purchased Credit ($<span id="price"></span>/cr)</th><th>Promotion Credit</th><th>Total Credit</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Credit available</td>
													<td id="plan-credit"></td>
													<td id="buy-credit"></td>
													<td id="promotion-credit"></td>
													<td id="total-credit"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<br>
								<div class="row">
									<form id="update-credit" action="">
										<div class="col-md-4">
											<div class="form-group">
												<select name="field-type" class="form-control select2">
													<option value="3">Adjust Current Credit</option>
													<option value="1">Adjust Purchased Credit</option>
													<option value="2">Adjust Promotion Credit</option>
												</select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<input type="number" class="form-control validate[required,custom[integer]]" name="value" value="">
											</div>
										</div>
										<div class="col-md-1">
											<button type="submit" class="btn btn-add-credit green-gd">Save</button>
										</div>
										<div class="col-md-3">
											<button type="submit" id="re_charge" class="btn btn-re-charge green-gd">Re-charge</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<br/>
					<div id="credit-update-log" <!--- class="tab-pane fade" --->>
						<!--- <div class="row">
							<div class="col-xs-12">
								<div id="filterBox">

								</div>
							</div>
						</div> --->
						<div class="row">
							<div class="col-xs-12 table-responsive">
								<table id="logList" class="table table-striped table-responsive">

								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
				<input type="hidden" name="userID" value="">
			</div>
		</div>
	</div>
</div>



<!-- Modal -->
<div class="bootbox modal fade" id="CaptchaConfirmModel" tabindex="-1" role="dialog" aria-labelledby="CaptchaConfirmModel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title title">Confirm <span class="modal-text">deactivate</span> account</h4>
			</div>
			<div class="modal-body">
				<p>Are you sure you want to <span class="modal-text">deactivate</span> this account?</p>
				<div id="capcha">
					<div class="form-group">
						<script src='https://www.google.com/recaptcha/api.js'></script>
						<cfoutput>
							<div class="g-recaptcha" data-sitekey="#PUBLICKEY#"></div>
						</cfoutput>
					</div>
					<div id="captcha-message" style="color: #dd1037"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success btn-success-custom" id="btn-captcha-confirm"> Confirm </button>
				&nbsp; &nbsp; &nbsp;
				<button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!--- Modal --->
<div class="modal fade" id="user-account-update" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><strong>Monthly Plan Limit</strong></h4>
			</div>
			<div class="modal-body">
				<!--- <ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#credit-update-action">Add Credit</a></li>
					<li><a data-toggle="tab" href="#credit-update-log">View Log</a></li>
				</ul> --->
				<div <!--- class="tab-content" --->>
					<div id="credit-update-action" <!--- class="tab-pane fade in active" --->>
						<div class="row">
							<div class="col-xs-12">
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-12">
										<p for=""><strong id="user-name"></strong> - <strong id="user-email"></strong></p>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12 plan-active-since">
										<div>Plan: <span id="acc-plan-name"></span></div>
										<div>Active since: <span id="acc-plan-active-date"></span></div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 table-responsive">
										<table id="account-details-table" class="table table-striped">
											<thead>
												<tr>
													<th></th>
													<th>SMS credits</th>
													<th>Cost per Credit</th>
													<th>Keywords</th>
													<th>Min Char Keywords</th>
													<th>Keywords Purchased</th>
													<th>MLPs</th>
													<th>Short URLs</th>
													<th>Image Capicity</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Credit available</td>
													<td id="amount_of_credit"></td>
													<td id="cost_per_credit"></td>
													<td id="amount_of_keyword"></td>
													<td id="min_of_char_keyword"></td>
													<td id="keyword_purchased"></td>
													<td id="amount_of_mlp_"></td>
													<td id="amount_of_short_url_"></td>
													<td id="amount_of_image_capacity"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<br>
								<div class="row">
									<form id="update-acc-limit" action="" autocomplete="off">
										<input type="hidden" name="user_id" value="">
										<input type="hidden" name="user_name" value="">
										<input type="hidden" name="user_email" value="">
										<input type="hidden" name="monthYearSwitchAdmin" id="monthYearSwitchAdmin" value="1">
										<div class="col-md-4">
											<div class="form-group">
												<select name="field-type" class="form-control select2">
													<option value="1">SMS credits</option>
													<option value="2">Cost per Credit</option>
													<option value="3">Keywords</option>
													<option value="4">MLPs</option>
													<option value="5">Short URLs</option>
													<option value="6">Image Capacity</option>
													<option value="7">User Plan</option>
													<option value="8">Buy Credits</option>
													<option value="9">Buy Keywords</option>
													<option value="10">Set Keywords Purchased</option>
													<option value="11">Set Min Char of Keywords</option>
												</select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<input type="text" id="field-to-change" maxlength="9" class="form-control validate[required,custom[onlyNumber],maxSize[9]]" name="value" value="">
												<cfinvoke component="public.sire.models.cfc.plan" method="GetActivedAndPrivatePlans" returnvariable="plans"></cfinvoke>
												<select id="field-to-change-plan" class="form-control validate[required]" name="" style="display:none">
	    											<cfoutput>
	    												<cfloop query="#plans.DATA#">
	    													<option class="monthly-plan" value="#id#" data-price="#price#" data-yearprice="#yearlyAmount#">
	    														<b>#name#</b>
	    														<!--- <cfif name neq "Free"> - $#price#/month</cfif> --->
	    													</option>
	    												</cfloop>
	    											</cfoutput>
												</select>
											</div>
										</div>
										<div class="col-md-4">
											<button type="submit" class="btn btn-add-credit green-gd">Save</button>
										</div>
										<div class="fieldset-payment col-md-12" style="display: none">
											<div class="col-md-12">
												<label><input class="uk-checkbox payopt" type="checkbox" name="payopt" value="payopt1" style="margin-top: -2px"> Payment</label>
											</div>
											<div class="col-md-12 inner-update-cardholder" style="padding-top: 0;display: none">
												<div class="uk-grid uk-grid-medium" uk-grid>
													<div class="uk-width-1-1">
														<div class="uk-grid" uk-grid>
															<div class="uk-width-1-1@m">
																<!--- <cfif RetUserPlan.BILLINGTYPE NEQ 2> --->
																<div class="form-group monthly-yearly-update">
																	<div class="text-center month-year-plan-switch">
																		<div class="inner">
																			<span class="month-year-plan-text" <!--- id="monthly-plan-text" --->>MONTHLY</span>
																			<label class="switch">
																			<input name="month-Year-Switch-Admin" class="month-Year-Switch-Admin" id="month-Year-Switch-Admin" type="checkbox" value="1">
																				<span class="slider round"></span>
																			</label>
																			<span class="month-year-plan-text" <!--- id="yearly-plan-text" --->>YEARLY</span>
																			<!--- <span class="triangle-isosceles left" style="float: inherit;">Save up to 20%</span> --->
																		</div>
																	</div>
																</div>
																<!--- </cfif> --->

																<!--- <cfif RetUserPlan.BILLINGTYPE EQ 1> --->
																<div class="uk-grid uk-grid-small uk-flex-middle monthly_price">
																	<div class="uk-width-1-3@m">
																		<div class="form-group">
																			<span class="uk-text-bold">Monthly Amount:</span>
																		</div>
																	</div>
																	<div class="uk-width-1-2@m">
																		<input type="text" name="inpCustomAmount" class="form-control validate[required,custom[number]] inpCustomAmount">
																		<!--- <cfoutput>
																			<cfloop query="#plans.DATA#">
																				<span id="span-plan-price-#id#" class="span-plan-price">$#price#</span>
																			</cfloop>
																		</cfoutput> --->
																	</div>
																</div>
																<!--- <cfelse> --->
																<!--- <div class="uk-grid uk-grid-small uk-flex-middle yearly_price hidden">
																	<div class="uk-width-1-3@m">
																		<div class="form-group">
																			<span class="uk-text-bold">Yearly Amount:</span>
																		</div>
																	</div>
																	<div class="uk-width-1-2@m">
																		<input type="text" name="inpCustomAmount" class="form-control">
																		<!--- <cfoutput>
																			<cfloop query="#plans.DATA#">
																				<span id="span-plan-price-yearly-#id#" class="span-plan-price">$#yearlyAmount*12#</span>
																			</cfloop>
																		</cfoutput> --->
																	</div>
																</div> --->
																<!--- </cfif> --->

																<div class="uk-grid uk-grid-small uk-flex-middle uk-hidden" uk-grid>
																	<div class="uk-width-1-3@m">
																		<div class="form-group">
																			<span class="uk-text-bold">Coupon Code: </span>
																		</div>
																	</div>
																	<div class="uk-width-1-2@m">
																		<input type="text" name="inpPromotionCode" class="form-control">
																		<span class="help-block help-block-coupon-signup-input help-block" style="display: none;font-weight: bold;"></span>
																	</div>

																	<div class="uk-width-expand">
																		<a class="btn green-gd apply-btn">apply</a>
																		<a class="btn green-cancel remove-btn" style="display: none">remove</a>
																	</div>

																</div>

																<div class="form-group">
												                    <div class="radio-inline" id="rad-Crecard">
																		<label>
																			<input type="radio" name="select_payment_method" class="select_payment_method" id="optionsRadiosMethod1" value="1" checked>
																			Credit/Debit Card
																		</label>
																	</div>
												                </div>

												                <div class="form-group">
												                    <div class="card-support-type">
												                        <img class="img-responsive" src="/session/sire/assets/layouts/layout4/img/supported-payment-types.png" alt="">
												                    </div>
												                </div>

																<div class="form-groups select_card_type">
																	<input type="hidden" class ="form-control" id="select_used_card_value" value="">
																	<div class="uk-grid uk-grid-small uk-flex-middle" uk-grid>
																		<div class="radio-inline rad-currentcard" style="margin-left: 20px !important;">
																			<label>
																				<input type="radio" name="select_used_card" class="select_used_card" value="1" checked>
																				<b>Current Card</b>
																			</label>
																		</div>
																		<div class="radio-inline rad-newcard" style="padding-left: 22px !important;">
																			<label>
																				<input type="radio" name="select_used_card" class="select_used_card" value="2">
																				<b>New Card</b>
																			</label>
																		</div>
																	</div>
																</div>

																<div class="fset_cardholder_info" style="display: none">
																	<input type="radio" class="hidden" name="payment_method" class="check_payment_method payment_method_0" value="0" checked/>

																	<div class="row row-small">
																		<div class="col-lg-8 col-md-6">
																			<div class="form-group">
																				<label>Card Number <span>*</span></label>
																				<input type="text" class="form-control validate[required,custom[creditCardFunc]] card_number" maxlength="32" name="number" value='' placeholder="---- ---- ---- 1234">
																			</div>
																		</div>
																		<div class="col-lg-4 col-md-6">
																			<div class="form-group">
																				<label>Security Code <span>*</span></label>
																				<input type="text" class="form-control validate[required,custom[onlyNumber]] , minSize[3] security_code" maxlength="4" name="cvv" data-errormessage-custom-error="* Invalid security code" data-errormessage-range-underflow="* Security code should be 3 or 4 characters">
																			</div>
																		</div>
																	</div>

																	<div class="row row-small">
																		<div class="col-lg-8 col-md-6">
																			<div class="form-group">
																				<label>Expiration Date <span>*</span></label>
																				<input type="hidden" class="expiration_date_admin" name="expiration_date_admin" id="expiration_date_admin" value="">
																				<select class="form-control validate[required] expiration_date_month expiration_date_month_upgrade" name="expiration_date_month_admin" id="expiration_date_month_admin">
																					<option value="" disabled selected>Month</option>
																					<option value="01">January</option>
																					<option value="02">February</option>
																					<option value="03">March</option>
																					<option value="04">April</option>
																					<option value="05">May</option>
																					<option value="06">June</option>
																					<option value="07">July</option>
																					<option value="08">August</option>
																					<option value="09">September</option>
																					<option value="10">October</option>
																					<option value="11">November</option>
																					<option value="12">December</option>
																				</select>
																			</div>
																		</div>

																		<div class="col-lg-4 col-md-6">
																			<div class="form-group">
																				<label>&nbsp;</label>
																				<select class="form-control validate[required] expiration_date_year expiration_date_year_upgrade" name="expiration_date_year_admin" id="expiration_date_year_admin">
																					<option value="" disabled selected>Year</option>
																					<cfloop from="#year(now())#" to="#year(now())+50#" index="i">
																						<cfoutput>
																							<option value="#i#">#i#</option>
																						</cfoutput>
																					</cfloop>
																				</select>
																			</div>
																		</div>
																	</div>

																	<div class="row row-small">
																		<div class="col-lg-6 col-md-6">
																			<div class="form-group">
																				<label>Cardholder Name <span>*</span></label>
																				<input type="text" class="form-control validate[required,custom[onlyLetterNumberSp]] first_name" maxlength="50" name="firstName" placeholder="First Name" value="">
																			</div>
																		</div>

																		<div class="col-lg-6 col-md-6">
																			<div class="form-group">
																				<label>&nbsp;</label>
																				<input type="text" class="form-control validate[required,custom[onlyLetterNumberSp]] last_name" maxlength="50" name="lastName" placeholder="Last Name" value="">
																			</div>
																		</div>
																	</div>

																	<div class="form-group">
																		<label>Address:<span>*</span></label>
																		<input type="text" class="form-control validate[required,custom[noHTML], custom[validateAddrAndCity]] cardholder line1" maxlength="60" name="line1" data-value=''>
																	</div>

																	<div class="form-group">
																		<label>City:<span>*</span></label>
																		<input type="text" class="form-control validate[required,custom[noHTML], custom[validateAddrAndCity]] cardholder city" maxlength="40" name="city" data-value=''>
																	</div>


																	<div class="row">
																		<div class="col-md-6">
																			<div class="form-group">
																				<label>State:<span>*</span></label>
																				<input type="text" class="form-control validate[required,custom[noHTML], custom[onlyLetterNumberSp]] cardholder state" maxlength="40" name="state" data-value="">
																			</div>
																		</div>
																		<div class="col-md-6">
																			<div class="form-group">
																				<label>Zip Code:<span>*</span></label>
																				<input type="text" class="form-control validate[required,custom[onlyNumber]] cardholder zip" maxlength="5" name="zip" data-value="" data-errormessage-custom-error="* Invalid zip code">
																			</div>
																		</div>
																	</div>
																	<div class="form-group">
																		<label>Country:<span>*</span></label>
																		<select class="form-control cardholder validate[required] country" name="country" data-value="" >
																			<cfinclude template="../views/commons/payment/country.cfm">
																		</select>
																	</div>

																	<input type="hidden" class="form-control validate[custom[phone]] cardholder phone" maxlength="30" name="phone" data-value="">

																	<div class="form-group">
																		<label>Email:</label>
																		<input type="text" class="form-control validate[custom[email]] cardholder email" maxlength="128" name="email" data-value="">
																	</div>
																	<div class="form-group hidden">
																		<label>
																			<input type="checkbox" class="save_cc_info" name="save_cc_info" value="1" checked>
																			Save card information
																		</label>
																	</div>
																</div>



															</div>
														</div>
													</div>
												</div>
											</div>

										</div>
										<!--- <div class="fieldset-payment col-md-12" style="display: none">
											<div class="form-check">
												<label class="form-check-label">
													<input class="form-check-input payopt" type="radio" name="payopt" value="payopt1" checked>
													Non payment
												</label>
											</div>
											<div class="form-check">
												<label class="form-check-label">
													<input class="form-check-input payopt" type="radio" name="payopt" value="payopt2">
													Payment with CC of user
												</label>
											</div>
											<div class="payopt2-param row" style="display: none">
												<div class="col-ms-12 col-md-12">
													<div class="col-ms-6 col-md-4">
														<label class="form-check-label">
															<input type="checkbox" class="form-check-input payopt2amoutopt" name="payopt2amoutopt"/>
															Amout other
														</label>
														<div class="payopt2amoutoptval" style="display: none">
															<input type="text" class="form-control validate[required,custom[number]]" name="payopt2amoutval" placeholder="Amount"/>
														</div>
													</div>
												</div>
											</div>
											<div class="form-check">
												<label class="form-check-label">
													<input class="form-check-input payopt" type="radio" name="payopt" value="payopt3">
													Payment with CC other
												</label>
											</div>
											<div class="payopt3-param row" style="display: none">
												<div class="col-ms-12 col-md-12">
													<div class="col-ms-6 col-md-4">
														<label class="form-check-label">
															<input type="checkbox" class="form-check-input payopt3amoutopt" name="payopt3amoutopt"/>
															Amout other
														</label>
														<div class="payopt3amoutoptval" style="display: none">
															<input type="text" class="form-control validate[required,custom[number]]" name="payopt3amoutval" placeholder="Amount"/>
														</div>
													</div>
												</div>
												<div class="col-ms-12 col-md-12">
													<div class="col-ms-12 col-md-12">
														<hr style='margin: 5px 0'/>
													</div>
													<div class="col-ms-12 col-md-12">
														CC
													</div>
												</div>
											</div>

										</div> --->
									</form>
								</div>
							</div>
						</div>
					</div>
					<br/>
					<div id="account-update-log" <!--- class="tab-pane fade" --->>
						<!--- <div class="row">
							<div class="col-xs-12">
								<div id="filterBox">

								</div>
							</div>
						</div> --->
						<div class="row">
							<div class="col-xs-12 table-responsive">
								<table id="accountUpdateLogList" class="table table-striped table-responsive">

								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
				<input type="hidden" name="userID" value="">
			</div>
		</div>
	</div>
</div>
 <div class="modal fade" id="ModalLimitUploadContact" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="frm_limit_upload_contact">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <b><h4 >Upload Contact Setting</h4></b>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Set Limit Upload Contact: </label>
                        <input id="inpLimitUploadContact" class="form-control validate[required, custom[number],max[5000]]" maxlength="4">
						<input type="hidden" id="hiddenUserID"/>
                    </div>
					<div class="form-group">
						<label for="">Permission</label>
						<select class="form-control validate[required] Select2" id="inpUploadPermission">
							<option value="1">Not allowed to use</option>
							<option value="2">Allowed to use but need approval</option>
							<option value="3">Allowed to use without approval</option>
						</select>
                    </div>

                    <div class="text-right">
                        <button type="button" id="btn-limit-upload-contact" class="btn green-gd">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
 </div>
  <div class="modal fade" id="ModalRenewPlan" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="frm_renew_plan">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <b><h4 >Renew User Plan</h4></b>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Set First SMS Included: </label>
                        <input id="inpFirstSMSIncluded" type="text" class="form-control validate[required, custom[number]]">
						<input type="hidden" id="hiddenUserID"/>
                    </div>
                    <div class="form-group">
                        <label for="">Set Keyword Limit: </label>
                        <input id="inpKeyWordLimit" type="text" class="form-control validate[required, custom[number]]">
                    </div>
                    <div class="form-group">
                        <label for="">Set EndDate: </label>
                        <input id="inpEndDate" type="text" class="form-control" >
                    </div>
                    <div class="text-right">
                        <button type="button" id="btn-save-renew-plan" class="btn green-gd">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
 </div>
<!--- end modal --->

<script type="text/javascript">
    var paypalCheckoutUrl = <cfoutput>'#_PaypalCheckOutUrl#'</cfoutput>
</script>
<!--- Account Report Modal --->
<!--- Modal --->
<div class="modal fade" id="user-account-report" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><strong>User account report</strong></h4>
			</div>
			<div class="modal-body">
				<form id="acc-report-form">
					<div class="form-group" id="select-report">
						<label style="font-weight: bold;">Select Report Type: </label>
						<select id="report-select" class="form-control">
							<option value="1">Dashboard</option>
							<option value="2">Campaign</option>
							<option value="3">Activity</option>
							<option value="4">API Request</option>
						</select>
					</div>
					<div class="" id="acc-dashboard-report">
						<h4 id="acc-total-message-sent"></h4>

						<h4 id="acc-opt-in"></h4>

						<h4 id="acc-opt-out"></h4>

						<h4 id="acc-short-url-clicks"></h4>

	                    <h4 id="acc-mlp-views"></h4>

						<hr>

						<div class="acc-subscriber-list">
							<h4>Subscriber list:</h4>
		        	        <div class="re-table">
		        	        	<div class="table-responsive">
		        	                <table id="tblSubcriberList" class="table table-bordered table-striped">

		        	                </table>
		        	            </div>
		        	        </div>
				        </div>

				        <hr>

				        <div class="acc-campaign-list">
				        	<h4>Campaign list:</h4>
	                        <div class="re-table">
	                            <div class="table-responsive">
	                                <table id="tblCampaignList" class="table table-responsive table-striped">

	                                </table>
	                            </div>
	                        </div>
	                    </div>

	                    <hr>

	                    <div class="acc-keyword-list">
	                    	<h4>Keyword list:</h4>
		        	        <div class="re-table">
		                        <div class="table-responsive">
		                            <table id="tblKeywordList" class="table table-bordered table-striped">

		                            </table>
		                        </div>
		        	        </div>
				        </div>
					</div>

					<div class="" id="acc-campaign-in-queue">
						<h4>Blast Campaign list:</h4>
                        <div class="re-table">
                            <div class="table-responsive">
                                <table id="tblBlastCampaignList" class="table table-responsive table-striped">

                                </table>
                            </div>
                        </div>
					</div>

					<div class="" id="acc-activity">
						<div class="re-table">
	        	        	<div class="table-responsive">
	        	                <table id="tblAccActivityList" class="table table-bordered table-striped">

	        	                </table>
	        	            </div>
	        	        </div>
					</div>

					<div class="" id="acc-api-report">
						<div class="re-table">
	        	        	<div class="table-responsive">
	        	                <table id="tblApiTrackingList" class="table table-bordered table-striped">

	        	                </table>
	        	            </div>
	        	        </div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
				<input type="hidden" name="userID" id="userIdToGetReport" value="">
			</div>
		</div>
	</div>
</div>
<!--- end modal --->
<div class="modal fade" id="mdApplyAffiliate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="frmApplyAffiliate">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <b><h4 >Apply Affiliate Code</h4></b>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Enter Affiliate Code: </label>
                        <input id="inpAffiliateCode" class="form-control validate[required]">
						<input type="hidden" id="selectUserID"/>
                    </div>

                    <div class="text-right">
                        <button type="button" id="btn-update-affiliatecode" class="btn green-gd">Apply</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
 </div>

 <div class="modal fade" id="mdCommissionSetting" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="frmCommissionSetting">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <b><h4 >Commission Setting</h4></b>
					<input type="hidden" id="selectUserID"/>
                </div>
                <div class="modal-body">
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								Condition
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								Value (Users)
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								Commission rate (%)
							</div>
						</div>
						<div class="col-md-1">
							<div class="form-group">

							</div>
						</div>
					</div>
					<div id="setting-content">

					</div>
                    <div class="text-right">
                        <button type="button" id="btn-update-commission-setting" class="btn green-gd">Apply</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
 </div>
 <div class="modal fade" id="mdAffiliateCoupon" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="frAffiliateCoupon">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <b><h4 >Affiliate Code linkage with Coupon Code</h4></b>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Affiliate Code</label>
						<strong><span id="affiliate-code"></span></strong>
						<input type="hidden" id="affiliateId"/>
						<input type="hidden" id="userId"/>
                    </div>
					<div class="form-group">
						<label for="">Coupon Code</label>
						<input id="couponCode" class="form-control validate[required]">
                    </div>

                    <div class="text-right">
                        <button type="button" id="btn-affiliate-coupon-submit" class="btn green-gd">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
 </div>
 <!-- Modal For User List level 2-->
<div class="modal fade" id="user-modal-2" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="user-modal-title">Total Sub Users Level 2</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="userFilterBox-2">

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="userTable-2" class="table table-striped table-responsive">

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <!--- <button class="btn-re-dowload user-list"><i class="fa fa-download" aria-hidden="true"></i></button> --->
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <input type="hidden" name="userID" value="">
            </div>
        </div>
    </div>
</div>
<!-- Modal For User List lv 3-->
<div class="modal fade" id="user-modal-3" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="user-modal-title">Total Sub Users Level 3</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="userFilterBox-3">

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="userTable-3" class="table table-striped table-responsive">

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <!--- <button class="btn-re-dowload user-list"><i class="fa fa-download" aria-hidden="true"></i></button> --->
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <input type="hidden" name="userID" value="">
            </div>
        </div>
    </div>
</div>
<!-- Modal List keyword-->
<div class="modal fade" id="list-keyword-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">List Keyword</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="list-keyword-table" class="table table-striped table-responsive">

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<cfparam name="variables._title" default="Admin - Accounts management">
<cfinclude template="../views/layouts/master.cfm">
