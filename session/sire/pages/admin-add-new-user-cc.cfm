<cfscript>
	createObject("component", "public.sire.models.helpers.layout")
		.addJs("/public/sire/js/select2.min.js", true)
		.addCss("/public/sire/css/select2.min.css", true)
		.addCss("/public/sire/css/select2-bootstrap.min.css", true)
		.addJs("/session/sire/assets/pages/scripts/admin-add-user-cc.js");
</cfscript>

<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<!--- 
********************************************************************************************	
************************************** DANGER!!!!!!!! **************************************	
********************************************************************************************	
--->

<cfparam name="AddKey" default="0">
<cfparam name="RemoveKey" default="0">
<cfparam name="IUserId" default="0">

<cfset AdminKeySet = 0 />
<cfset IUserIdSet = 0 />

<cfif AddKey EQ 1>

	<!--- Create new string for Cookie---> 
    <cfset strAdminKey = (
            CreateUUID() & ":" &
            CreateUUID() & ":" &
            #Session.USERID# & ":" &
            7 & ":" &  <!--- 9 is not on --- 7 is on --->
            CreateUUID() & ":" &                 
            CreateUUID()  
    ) />

	<cfset  strAdminKey = Encrypt(strAdminKey,APPLICATION.EncryptionKey_Cookies,"cfmx_compat","hex") />

	<!--- Store the cookie such that it will expire after 30 days. --->
    <cfcookie
        name="_ga_105"
        value="#strAdminKey#"
        expires="365"
    />

	<!--- Add cookie to this device so admins can impersonate users --->
	<cfset AdminKeySet = 1 />
	
</cfif>	

<cfif RemoveKey EQ 1>
	
	<cfcookie
        name="_ga_105"
        value="000000"
        expires="365"
    />
	
</cfif>

<cfset AdminCookieData_ga_105 = "" />

<cftry>
	<!--- Read and decrypt admin cookie --->
	<cfset AdminCookieData_ga_105 = Decrypt(evaluate("COOKIE._ga_105"),APPLICATION.EncryptionKey_Cookies,"cfmx_compat","hex") />

	<cfcatch  type="any">

	</cfcatch>

</cftry>

<!--- See if admin cookie is set --->
<cfif ListLen(AdminCookieData_ga_105, ":") EQ 6>
	<!--- See if admin cookie is proper value --->
	<cfif ListGetAt(AdminCookieData_ga_105, 4, ":") EQ 7>
		<cfset AdminKeySet = 1 />
	</cfif>	
</cfif>	 


<div class="portlet light bordered subscriber">
	<div class="row">
		<div class="col-md-12">
			<h2 class="page-title">Admin Add New User CC</h2>
		</div>
	</div>
	<div style="width: 50%;height: auto;">
		<!--- <div class="row" id="userSession">	
		<form name="frm-userSession" method="POST">	
			<div class="col-md-4 col-sm-6 col-xs-12">				
				<div class="form-group">					
					<select name="user-id" id="user-id" class="form-control validate[required]"></select>
					<input type="hidden" name="cur-user-id" value="<cfoutput>#Session.USERID#</cfoutput>"/>
					<input type="hidden" name="selected-user-id" id="selected-user-id" value=""/>					
				</div>
				<div class="form-group">
					<button type="submit" class="btn green-gd btn-re form-control" id="btn_NextToPaymentGateway">Next</button>
				</div>								
			</div>		
		</form>			
	</div>
		<div class="row" id="paymentGatewaySession">
			<form name="frm-paymentGatewaySession" method="POST">	
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="form-group">	
						<select id="paymentGatewayList" class="filter-form-control form-control validate[required]">
							<!--- <option value="">Payment Gateway</option> --->
							<option value="1">WorldPay</option>
							<option value="2">PayPal</option>
							<option value="3">Mojo</option>
						</select>						
					</div>							
					<div class="form-group">
						<button type="submit" class="btn green-gd btn-re form-control" id="btn_NextToCCinfo">Next</button>
					</div>
				</div>
			</form>
		</div>	 --->
		<cfparam name="expirationDate" default="">
		<cfparam name="firstName" default="">
		<cfparam name="lastName" default="">
		<cfparam name="line1" default="">
		<cfparam name="city" default="">
		<cfparam name="state" default="">
		<cfparam name="zip" default="">
		<cfparam name="country" default="">
		<cfparam name="phone" default="">
		<cfparam name="emailAddress" default="">

		<div class="row" id="ccInfomationSession">
			<form name="frm-ccInfomationSession" method="POST">
				<div class="uk-modal-body form-gd">
	                <div class="inner-update-cardholder">   
	                	<div class="row row-small">
		                    <div class="col-md-6">
		                        <div class="form-group">
			                        <label>Select User<span>*</span></label>
			                        <select name="user-id" id="user-id" class="form-control validate[required]"></select>
									<input type="hidden" name="cur-user-id" value="<cfoutput>#Session.USERID#</cfoutput>"/>
									<input type="hidden" name="selected-user-id" id="selected-user-id" value=""/>	
		                        </div>
		                    </div>
		                    <div class="col-md-6">
		                        <div class="form-group">
		                            <label>Payment Gateway:<span>*</span></label>
		                            <select id="paymentGatewayList" class="filter-form-control form-control validate[required]">
										<!--- <option value="">Payment Gateway</option> --->
										<!--- <option value="1">WorldPay</option> --->
										<option value="2">PayPal</option>
										<option value="3">Mojo</option>
										<option value="4">Total Apps</option>
									</select>										
		                        </div>
		                    </div>  
		                </div>                               
	                    <br>
	                    <cfoutput>
		                    <div class="update_cardholder_info">                                                            
		                        <div class="form-group">
		                                    <div class="card-support-type">
		                                        <img class="img-responsive" src="../assets/layouts/layout4/img/supported-payment-types.png" alt="">
		                                    </div>
		                        </div>      
		                        <div class="row row-small">
	                                <div class="col-lg-8 col-md-6">
	                                    <div class="form-group">
	                                        <label>Card Number <span>*</span></label>
	                                        <input type="text" class="form-control validate[required,custom[creditCardFunc]]" maxlength="32" id="card_number" name="number" value='' placeholder="---- ---- ---- 1234">
	                                    </div>
	                                </div>
	                                <div class="col-lg-4 col-md-6">
	                                    <div class="form-group">
	                                        <label>Security Code <span>*</span></label>
	                                        <input type="text" class="form-control validate[required,custom[onlyNumber]]" maxlength="4" id="security_code" name="cvv" data-errormessage-custom-error="* Invalid security code">
	                                    </div>
	                                </div>
                            	</div>                          
		                        <div class="row row-small">
		                                    <div class="col-lg-8 col-md-6">
		                                        <div class="form-group">
		                                            <label>Expiration Date <span>*</span></label>
		                                            <input type="hidden" id="expiration_date_upgrade" name="expirationDate" value="">
		                                            <select class="form-control validate[required] expiration_date_month expiration_date_month_upgrade" id="expiration_date_month_upgrade" class="expiration_date_month">
		                                                <option value="" disabled selected>Month</option>
		                                                <option value="01">January</option>
		                                                <option value="02">February</option>
		                                                <option value="03">March</option>
		                                                <option value="04">April</option>
		                                                <option value="05">May</option>
		                                                <option value="06">June</option>
		                                                <option value="07">July</option>
		                                                <option value="08">August</option>
		                                                <option value="09">September</option>
		                                                <option value="10">October</option>
		                                                <option value="11">November</option>
		                                                <option value="12">December</option>
		                                            </select>
		                                        </div>
		                                    </div>

		                                    <div class="col-lg-4 col-md-6">
		                                        <div class="form-group">
		                                            <label>&nbsp;</label>
		                                            <select class="form-control validate[required]" id="expiration_date_year_upgrade" class="expiration_date_year">
		                                                <option value="" disabled selected>Year</option>
		                                                <cfloop from="#year(now())#" to="#year(now())+50#" index="i">
		                                                    <option value="#i#">#i#</option>
		                                                </cfloop>
		                                            </select>
		                                        </div>
		                                    </div>
		                        </div>
		                        <div class="row row-small">
		                                    <div class="col-lg-6 col-md-6">
		                                        <div class="form-group">
		                                            <label>Cardholder Name <span>*</span></label>
		                                            <input type="text" class="form-control validate[required,custom[onlyLetterNumberSp]] first_name" maxlength="50" id="first_name" name="firstName" placeholder="First Name" value="">
		                                        </div>
		                                    </div>
		                                    <div class="col-lg-6 col-md-6">
		                                        <div class="form-group">
		                                            <label>&nbsp;</label>
		                                            <input type="text" class="form-control validate[required,custom[onlyLetterNumberSp]] last_name" maxlength="50" id="last_name" name="lastName" placeholder="Last Name" value="">
		                                        </div>
		                                    </div>
		                        </div>		                        
		                        <div class="row">
		                                    <div class="col-md-12">
		                                        <div class="form-group">
		                                            <label>Address:<span>*</span></label>
		                                   			 <input type="text" class="form-control validate[required,custom[noHTML]] cardholder line1" maxlength="60" id="line1" name="line1" data-value='#line1#'>
		                                        </div>
		                                    </div>		                                   
		                        </div>
		                         <div class="row">		                    
		                                    <div class="col-md-12">
		                                        <div class="form-group">
		                                            <label>City:<span>*</span></label>
		                                    		<input type="text" class="form-control validate[required,custom[noHTML]] cardholder city" maxlength="40" id="city" name="city" data-value='#city#'>
		                                        </div>
		                                    </div>
		                        </div>
		                        <div class="row">
		                                    <div class="col-md-6">
		                                        <div class="form-group">
		                                            <label>State:<span>*</span></label>
		                                            <input type="text" class="form-control validate[required,custom[noHTML]] cardholder state" maxlength="40" id="state" name="state" data-value="#state#">
		                                        </div>
		                                    </div>
		                                    <div class="col-md-6">
		                                        <div class="form-group">
		                                            <label>Zip Code:<span>*</span></label>
		                                            <input type="text" class="form-control validate[required,custom[onlyNumber]] cardholder zip" maxlength="5" id="zip" name="zip" data-value="#zip#" data-errormessage-custom-error="* Invalid zip code">
		                                        </div>
		                                    </div>
		                        </div>
		                        <div class="form-group">
		                                    <label>Country:<span>*</span></label>
		                                    <select class="form-control cardholder validate[required] country" id="country" name="country" data-value="#country#" >
		                                        <cfinclude template="../views/commons/payment/country.cfm">
		                                    </select>
		                        </div>                               
		                        <div class="form-group">
		                                    <label>Email:</label>
		                                    <input type="text" class="form-control validate[custom[email]] cardholder email" maxlength="128" id="email" name="email" data-value="#emailAddress#">
		                        </div>                             
		                        <hr>
		                        <div class="row">
											<div class="col-md-2">
												<button type="submit" class="btn newbtn green-gd" id="btn_SaveCCinfo">Save</button>
											</div>					
											<div class="col-md-2">
												<button type="button" class="btn newbtn1 green-cancel " id="btn_CancelAddCCInfo">Cancel</button>
											</div>					
								</div>  
		                    </div>
	                    </cfoutput>                    
	                </div>
	            </div>
	        </form>                			                                       			
        </div>	
	</div>
</div>	
<cfparam name="variables._title" default="Admin Add New User CC">
<cfinclude template="../views/layouts/master.cfm">

