

<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />
<cfoutput>
	<cfscript>
	    CreateObject("component","public.sire.models.helpers.layout")
	    .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
	    .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)	 
	    .addCss("../assets/layouts/layout4/css/custom3.css") 	     	 
	    .addJs("../assets/pages/scripts/admin-block-ip-address.js");  	    	    
	</cfscript>
</cfoutput>

<div class="portlet light bordered">
	<div class="portlet-body">
		<div class="new-inner-body-portlet2">
			<div class="form-gd">
				<div class="row">
					<div>
						<div class="col-sm-6 col-xs-12">
							<h4 class="portlet-heading2">Block IP Address List</h4>
						</div>
					</div>
					<div>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="inner-addon left-addon">
								<i class="glyphicon glyphicon-search"></i>
								<input type="search" class="form-control" id="searchbox" placeholder="Search Ip Address">
							</div>
						</div>
						<div class="col-sm-2 col-xs-12">
							<div class="pull-right">
								<a href="#" class="btn btn-block green-gd pop-phone" data-id="0" data-toggle="modal" data-phone-action="new" data-target="#form-new-ip-address">Add New IP Address</a>
							</div>
						</div>
					</div>
				</div>
				<div class="" style="margin-top: 10px">
			        <div class="row">
			            <div class="col-xs-12">
			                <div class="re-table">
			                    <div class="table-responsive">
			                        <table id="adm-block-ip-address-list" class="table table-striped table-responsive">
			            
			                        </table>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>			
			</div>
	    </div>
	</div>
</div>
<!--- modal--->
 <!--- edit message--->
 <div class="modal fade" id="form-new-ip-address" tabindex="-1" role="dialog" aria-hidden="true">
	<form name="form-new-ip-address" class="col-sm-12" id="form-new-ip-address">
	    <div class="modal-dialog">
	        <div class="modal-content">
	        	<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<b><h4 class="modal-title" id="modal-tit">
						NEW IP ADDRESS
					</h4></b>	
				</div>
	            <div class="modal-body">	               
	               <div class="modal-body-inner">  
	                   <div class="form-group">
                            <label><b>Ip Address</b></label>
                            <input type="text" class ="form-control" id="ip_address" value="">
                        </div>
	             	</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn green-cancel" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn green-gd"  id="btnUpdateIpAddress">Save</button>
				</div>
			</div>
		</div>
	</form>
 </div>
<cfparam name="variables._title" default="Admin - Block Ip Address">
<cfinclude template="../views/layouts/master.cfm">
