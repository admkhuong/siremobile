<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />


<cfscript>
	createObject("component", "public.sire.models.helpers.layout")
		.addJs("/public/js/jquery.tmpl.min.js", true)
		.addJs("/public/sire/js/select2.min.js", true)
		.addJs("/session/sire/js/vendors/bootstrapeditor/plugins/editor/ckeditor/ckeditor.js", true)
		.addJs("/session/sire/js/site/template-category.js")
		.addCss("/public/sire/css/select2.min.css", true)
		.addCss("/public/sire/css/select2-bootstrap.min.css", true);
</cfscript>


<cfinvoke component="session.sire.models.cfc.category" method="GetCategoryGroup" returnvariable="catGroups">
</cfinvoke>




<cfset catGroups = catGroups.DATALIST />


<cfinclude template="/session/sire/configs/paths.cfm">

<style type="text/css">
.bigdrop {
	width: auto!important;
}

table a.btn-success-custom {
	margin: 0 0 5px 5px;
}
</style>

<div class="portlet light bordered">
	<div class="row">
		<div class="col-xs-12">
	        <h2 class="page-title">Administration - Sub Category Management</h2>
		</div>  
    </div>
    <div class="row">
        <div class="col-xs-12">
        	
			<!--- END : content-header --->
			<div class="content-body">
				<div class="row">
					<div class="col-md-3 col-sm-4">
						<div class="form-group">
							<label>Category Name</label>
							<select class="list-filter-control form-control select2" data-type="integer" data-field="tc.GroupId_int" data-operator="=" id="inpGroupID">
								<option value="0"> -- All -- </option>
								<cfloop array="#catGroups#" index="item">
									<cfoutput><option value="#item.ID#">#item.NAME#</option></cfoutput>
								</cfloop>
							</select>
						</div>
					</div>
					<div class="col-md-3 col-sm-4">
						<div class="form-group">
							<label>Sub Category Name</label>
							<input type="text" class="list-filter-control form-control" placeholder=" Sub-category name" data-type="varchar" data-field="tc.CategoryName_vch" data-operator="LIKE" id="inpCatename">
						</div>
					</div>
					
					<div class="col-md-6 col-sm-4">
						<div class="form-group">
							<label style="display: block;">&nbsp;</label>
							<button name="addNew" id="addNew" class="btn btn-success-custom pull-right">Create Sub Category</button>
						</div>
					</div>
				</div>
				<div class="re-table">
					<div class="table-responsive">
						<table id="tblListEMS" class="<!--- table-bordered ---> table-responsive <!--- table-hover ---> table-striped dataTables_wrapper">
							<thead>
								<tr>
									<th>Name</th>
									<th>Display Name</th>
									<th>Description</th>
									<th>Order</th>
									<th>In Group</th>
									<th>Actions</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		
		</div>
	</div>
</div>

<!-- Modal add new -->
<div class="modal fade" id="add-new-category" role="dialog">
	<div class="modal-dialog">
		<form id="add-new-category-form" name="add-new-category-form">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong>Create New Sub-Category</strong></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="usr">Sub-Category Name:</label>
						<input type="text" class="form-control validate[required]" name="inpName">
					</div>
					<div class="form-group">
						<label for="pwd">Sub-Category Display:</label>
						<input type="text" class="form-control validate[required]" name="inpDisplayName">
					</div>
					<div class="form-group">
						<label for="pwd">Category:</label>
						<select class="form-control select2" name="inpGroupID">
							<cfloop array="#catGroups#" index="item">
								<cfoutput><option value="#item.ID#">#item.NAME#</option></cfoutput>
							</cfloop>
						</select>
					</div>
					<div class="form-group">
						<label for="pwd">Order:</label>
						<input type="number" value="" class="form-control validate[required, min[1]]" name="inpOrder" >
					</div>
					<div class="form-group">
						<label for="pwd">Sub-Category Description:</label>
						<textarea form="add-new-category-form" class="ckeditor" id="inpDescription" name="inpDescription"></textarea> 
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn-save btn btn-success-custom" >Save</button>
					<button type="button" class="btn btn-back-custom" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>




<!-- Modal add new -->
<div class="modal fade" id="edit-category" role="dialog" tabindex="-1">
	<div class="modal-dialog">
		<form id="edit-category-form" name="edit-category-form">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong>Update Sub-Category</strong></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="usr">Sub-Category Name:</label>
						<input type="text" class="form-edit-control form-control validate[required]" name="inpName">
					</div>
					<div class="form-group">
						<label for="pwd">Sub-Category Display:</label>
						<input type="text" class="form-edit-control form-control validate[required]" name="inpDisplayName">
					</div>
					<div class="form-group">
						<label for="pwd">Category:</label>
						<select class="form-edit-control form-control select2" name="inpGroupID">
							<cfloop array="#catGroups#" index="item">
								<cfoutput><option value="#item.ID#">#item.NAME#</option></cfoutput>
							</cfloop>
						</select>
					</div>
					<div class="form-group">
						<label for="pwd">Order:</label>
						<input type="number" value="" class="form-edit-control form-control validate[required, min[1]]" name="inpOrder" >
					</div>
					<div class="form-group">
						<label for="pwd">Sub-Category Description:</label>
						<textarea form="edit-category-form" class="form-edit-control ckeditor" id="inpDescriptionEdit" name="inpDescriptionEdit"></textarea> 
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="inpID" id="inpID" value="">
					<button type="submit" class="btn-save btn btn-success-custom" >Save</button>
					<button type="button" class="btn btn-back-custom" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>


<div class="modal fade" id="show-description-mdl" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><strong>Sub-Category Description</strong></h4>
			</div>
			<div class="modal-body  pre-scrollable">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>




<cfparam name="variables._title" default="Admin - Sub Category Management">
<cfinclude template="../views/layouts/master.cfm">