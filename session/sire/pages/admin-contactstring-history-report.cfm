<cfparam name="inpcontactstring" default="">

<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfinclude template="/session/cfc/csc/constants.cfm">

<!--- Load Setting Defaults --->
<cfset DBSourceManagementRead = "Bishop" />


<style>

.bubble {
	background-color: #28CBEF;
	border-radius: 5px;
	box-shadow: 0 0 6px #B2B2B2;
	display: inline-block;
	padding: 10px 18px;
	position: relative;
	vertical-align: top;
	font-family: Helvetica, sans-serif;
	font-size: 20px;
}
.bubble::before {
	background-color: #28CBEF;
	content: "\00a0";
	display: block;
	height: 16px;
	position: absolute;
	top: 11px;
	transform: rotate( 29deg) skew( -35deg);
	-moz-transform: rotate( 29deg) skew( -35deg);
	-ms-transform: rotate( 29deg) skew( -35deg);
	-o-transform: rotate( 29deg) skew( -35deg);
	-webkit-transform: rotate( 29deg) skew( -35deg);
	width: 20px;
}
.you {
	float: left;
	margin: 5px 45px 5px 20px;
	background-color: #F2F2F2;
}
.you::before {
	box-shadow: -2px 2px 2px 0 rgba( 178, 178, 178, .4);
	left: -9px;
	background-color: #F2F2F2;
}
.me {
	float: right;
	margin: 5px 20px 5px 45px;
}
.me::before {
	box-shadow: 2px -2px 2px 0 rgba( 178, 178, 178, .4);
	right: -9px;
}	

.cshr-text{
	font-size: 18px !important;
}
</style>	


<div class="portlet light bordered">
	<div class="row">
		<div class="col-xs-12">
	        <h2 class="page-title">Administration - Contact String History Report</h2>
		</div>  
    </div>
    <div class="row">
        <div class="col-xs-12">
		
		<cfoutput>
		<div class="content-body">
            
            <!--- Simple list of users for now - need to add full pagination later --->
			<div class="form-group">
			    <label for="inpBatchId" class="col-md-12 col-sm-12 col-xs-12 control-label cshr-text">Contact String History Report</label>
			    <div class="col-md-12 col-sm-12 col-xs-12">
			    
			    	<div class="col-md-12 col-sm-12 col-xs-12 cshr-text">
						<p>Enter a phone number or other contact string to search for history. Wildcards (%) are allowed.</p>
			    	</div>
			    
			    	<form id="send_one_form">
			    		<!--- Get Select Box of Active Batches --->
				    	<label for="inpcontactstring" class="cshr-text" style="display:block" >Contact String</label> 
						<div class="col-xs-12 col-md-4 col-sm-6 col-lg-3">
	               			
	                       	<input class="form-control validate[required,custom[phone],custom[onlyNumber]]" type="text" id="inpcontactstring" value="#inpcontactstring#" data-prompt-position="topLeft:95"/>
	               
	                    </div>
			    	</form>
			    	


					  
			    
			    </div>
			</div>
		
			<div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 2em; margin-bottom: 3em">
				
				<div class="form-group">
					<div class="btn-group-setting">									
						<div class="col-md-12 col-sm-12 col-xs-12">
							<button type="button" class="btn btn-success btn-success-custom btn-custom_size" id="btn_SendNow">Look up now</button>									
						</div>
					</div>	
				</div>
		
			</div>


			<cfif LEN(TRIM(inpcontactstring)) GT 0 >

				<hr/>

				<!--- <cfif NOT isvalid('telephone', LEN(TRIM(inpcontactstring)))>
	                <div style="text-align: center; display: block">Phone Number is not a valid US telephone number!</div>
	            <cfelse> --->
					<!--- Get New Users Last 1 days --->
		            <cfquery name="GetIREResults" datasource="#DBSourceManagementRead#">
		               	SELECT 
		               		IREResultsId_bi,
							BatchId_bi,
							UserId_int,
							CPId_int,
							QID_int,
							IREType_int,
							CarrierId_int,
							InitialSendResult_int,
							Increments_ti,
							IRESessionId_bi,
							MasterRXCallDetailId_bi,
							moInboundQueueId_bi,
							DATE_FORMAT(Created_dt,'%b %d %Y %T') AS Created_dt,
							ContactString_vch,
							NPA_vch,
							NXX_vch,
							Response_vch,
							ShortCode_vch
		               	FROM 
		               		simplexresults.ireresults
						WHERE
							ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcontactstring#"> 
						ORDER BY 
							IREResultsId_bi DESC      
						LIMIT 100	            
		            </cfquery>  
	            
					
					<cfif GetIREResults.RECORDCOUNT EQ 0>
						<div style="text-align: center; display: block" class="cshr-text">No result found!</div>
					</cfif>

					<cfloop query="GetIREResults">
						
						
						<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12">
							
							

							<cfif GetIREResults.IREType_int EQ 1 >
							
								<div style="text-align: right;">
									Message Sent to Device (MT)
									<br/>
									#GetIREResults.Created_dt#
									<br/>
									From Address <b>#GetIREResults.ShortCode_vch#</b>
									<br/>
									IRE Unique Id (#GetIREResults.IREResultsId_bi#)
									<br/>
									Batch Id (#GetIREResults.BatchId_bi#)
									<br/>
									User Id (#GetIREResults.UserId_int#)
									<br/>
									Control Point Id (#GetIREResults.CPId_int#)
								</div>	
								
								<div class="control-point-text bubble me">            				 	
									#GetIREResults.Response_vch# 
			        			</div>
													
							<cfelseif GetIREResults.IREType_int EQ 2 >
							
								<div style="text-align: left;">
									Message Received from Device (MO)
									<br/>
									#GetIREResults.Created_dt#
									<br/>
									From Address <b>#GetIREResults.ShortCode_vch#</b>
									<br/>								
									IRE Unique Id (#GetIREResults.IREResultsId_bi#)
									<br/>
									Batch Id (#GetIREResults.BatchId_bi#)
									<br/>
									User Id (#GetIREResults.UserId_int#)
									<br/>
									Control Point Id (#GetIREResults.CPId_int#)

								</div>	
								
								<div class="control-point-text bubble you" >           				 	
									#GetIREResults.Response_vch#
			        			</div>
							
							
							<cfelse>
							
							
								<div style="text-align: left;">
									System Logic (#GetIREResults.IREType_int#)
									<br/>
									#GetIREResults.Created_dt#
									<br/>
									IRE Unique Id (#GetIREResults.IREResultsId_bi#)
									<br/>
									Batch Id (#GetIREResults.BatchId_bi#)
									<br/>
									User Id (#GetIREResults.UserId_int#)
									<br/>
									Control Point Id (#GetIREResults.CPId_int#)
								</div>	
									
								<div class="">    
									#GetIREResults.Response_vch#
								</div>	
								
							</cfif>
							
						</div>
						
						
					</cfloop>
	            <!--- </cfif> --->

			</cfif>	

												
		</div>
		</cfoutput>
	
		</div>
	</div>
</div>


<!--- https://select2.github.io/  --->
<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/sire/css/select2.min.css">
</cfinvoke>

<!--- https://select2.github.io/  --->
<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/sire/css/select2-bootstrap.min.css">
</cfinvoke>

<!--- https://select2.github.io/  --->
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/sire/js/select2.min.js">
</cfinvoke>
		

<cfparam name="variables._title" default="Contact String History Tool">
<cfinclude template="../views/layouts/master.cfm">



<script type="text/javascript">
		
	$(document).ready(function()
	{							
		// Make all select boxes select2 style
		$("SELECT").select2( { theme: "bootstrap"} );
		
		<!--- Give user chance to cancel request --->
		$('#btn_SendNow').on('click', function(e){
			var inpContactString = $("#inpcontactstring").val();
			var validatePhone = 0;
			if ($('#send_one_form').validationEngine('validate', {promptPosition : "topLeft", scroll: false,focusFirstField : false}))
			{
				validatePhone = 0;
                $.ajax({
                    async: false,
                    type: "POST",
                    url: '/public/sire/models/cfc/validate.cfc?method=ValidatePhoneNumber&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
                    dataType: 'json',
                    data: {PhoneNumber:inpContactString},
                    beforeSend: function( xhr ) {
                            
                    },                    
                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        
                    },                    
                    success: function(data) {
                        if(data.RXRESULTCODE == -1) {
                             validatePhone = 1;
                        }
                    }
                        
                });
                if (validatePhone == 1) {
                    bootbox.dialog({
                                title: "Contact String History Report",
                                message: "Phone Number is not a valid US telephone number",
                                buttons: {
                                    success: {
                                        label: "Ok",
                                        className: "btn btn-medium btn-success-custom",
                                        callback: function() {
                        
                                        }
                                    }
                                }
                            });
                    return false;
                }

                else{
                	$('#btn_SendNow').attr("disabled", true);
				
					window.location = "admin-contactstring-history-report?inpcontactstring=" + $("#inpcontactstring").val();
                }
				
			}		
		})						    					
	});

</script>
