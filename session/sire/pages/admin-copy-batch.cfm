<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<!--- https://select2.github.io/  --->
<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/sire/css/select2.min.css">
</cfinvoke>

<!--- https://select2.github.io/  --->
<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/sire/css/select2-bootstrap.min.css">
</cfinvoke>

<!--- https://select2.github.io/  --->
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/sire/js/select2.min.js">
</cfinvoke>

<!--- https://select2.github.io/  --->
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/js/site/admin-copy-batch.js">
</cfinvoke>

<div class="portlet light bordered">
	<div class="row">
		<div class="col-xs-12">
	        <h2 class="page-title">Administration - Copy Batch</h2>
		</div>  
    </div>
    <div class="row">
        <div class="col-xs-12">
		<!--- END : content-header --->
		<div class="content-body">
			<div class="row">
				<form id="frm-copy-batch">
					<div class="col-sm-10 col-md-6 padding20">
						<div class="form-group">
						    <label for="exampleInputEmail1">Select Source User</label>
						    <!--- <input type="email" class="form-control" id="source-user-email" placeholder="Email address"> --->
						    <select class="select-source-user form-control" id="select-source-user"></select>

						</div>

						<div class="form-group">
						    <label for="exampleInputEmail1">Select Batch</label>
						    <!--- <input type="email" class="form-control" id="source-user-email" placeholder="Email address"> --->
						    <select class="select-batch form-control hidden" id="select-batch"></select>
						</div>

						<div class="form-group">
						    <a id="btn-preview" class="hidden" href="#">Preview</a>
						</div>
					</div>

					<div class="col-sm-10 col-md-6 padding20">
						<div class="form-group">
						    <label for="exampleInputEmail1">Select Target User</label>
						    <select class="select-target-user form-control" id="select-target-user"></select>
						</div>

						<div class="form-group">
						    <label for="exampleInputEmail1">Trouble ticket Id</label>
						    <input type="text" class="form-control" id="txt-trouble-ticket-id" placeholder="Trouble ticket Id">
						</div>
					</div>

					<div class="col-sm-12 padding20">
						<div class="form-group">
							<button type="button" class="btn btn-success-custom" id="btn-popup-confirm">Copy</button>
							<button type="button" class="btn btn-primary btn-back-custom" id="btn-reset-form">Reset</button>
						</div>
					</div>	

				</form>
			</div>	
		</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="user-account-update" role="dialog">
	<div class="modal-dialog">
		<form id="update-account-info" name="update-account-info">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong>Copy Batch</strong></h4>
				</div>
				<div class="modal-body">
					<p> Are you sure to copy these batch ?</p>
				</div>
				<div class="modal-footer">
					<button type="button" id="btn-copy-campaign" class="btn-copy-campaign btn btn-success-custom" >Confirm</button>
					<button type="button" class="btn btn-back-custom" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>

<!--- MODAL POPUP --->
<div class="bootbox modal fade" id="previewCampaignModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel">Preview Campaign</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <!--- <button type="button" class="btn btn-primary btn-success-custom" id="btn-rename-campaign">Save</button> --->
        <button type="button" class="btn btn-default btn-back-custom" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



<cfparam name="variables._title" default="Copy Batch - Sire">
<cfinclude template="../views/layouts/master.cfm">
