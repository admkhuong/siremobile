<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />
<cfinclude template="/session/sire/configs/paths.cfm">

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("../assets/global/plugins/daterangepicker/daterangepicker.css", true)
        .addCss("/session/sire/css/filter_table.css", true)
        .addJs("../assets/global/plugins/highchart/highcharts.js", true)
        .addJs("../assets/global/plugins/highchart/exporting.js", true)
        .addJs("../assets/pages/scripts/jquery.textfill.min.js")
        .addJs("../assets/global/plugins/daterangepicker/moment.min.js", true)
        .addJs("../assets/global/plugins/daterangepicker/daterangepicker.js", true)
        .addJs("/session/sire/assets/global/scripts/filter-bar-generator.js")
        .addJs("../assets/pages/scripts/admin_coupon_manage.js");
</cfscript>

<div class="portlet light bordered ">
	<div class="portlet-body row">
		<div class="col-sm-12 col-md-12">
			<h3 class="page-title coupon-page-title">Coupons</h2>
			<form id="search-coupon-form">
				<div class="rows">
					<div class="col-lg-10 col-md-12 col-sm-12 coupon-search-section">
						<div class="form-group col-lg-6 col-sm-6 col-xs-12">
							<input type="text" class="form-control" id="promo-code" value="" placeholder="Coupon Code" maxlength="30">
						</div>

						<div class="form-group col-lg-3 col-sm-3 col-xs-12">
							<input type="text" class="form-control" id="promo-startdate" value="" placeholder="Start Date">
						</div>

						<div class="form-group col-lg-3 col-sm-3  col-xs-12">
							<input type="text" class="form-control" id="promo-enddate" value="" placeholder="End Date">
						</div>
					</div>

					<div class="form-group col-lg-2 col-md-12 col-sm-12">
						<button type="submit" class="btn green-cancel pull-left search-coupon-btn">SEARCH</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="portlet light bordered ">
	<div class="portlet-body row">
		<div class="col-sm-12 col-md-12">
			<div class="content-body">
				<div class="container-fluid">
					<div class="pull-right new-coupon-btn-div">
						<a href="admin-new-coupon" class="btn green-gd">NEW COUPON</a>
					</div>
					<div class="re-table">
						<div class="table-responsive">
							<table id="coupon-list" class="table table-striped table-bordered table-hover">
								
							</table>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>

<cfparam name="variables._title" default="Admin - Promotion code management">
<cfinclude template="../views/layouts/master.cfm">