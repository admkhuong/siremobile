<cfparam name="couponid" default="0">

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addJs("../assets/pages/scripts/admin_coupon_report.js");
</cfscript>

<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfinvoke method="GetCouponDetails" component="session.sire.models.cfc.promotion" returnvariable="RetCoupon">
	<cfinvokeargument name="inpCouponId" value="#couponid#"/>
</cfinvoke>

<cfinclude template="/session/sire/configs/paths.cfm">
<input type="hidden" name="couponid" id="couponid" value="<cfoutput>#couponid#</cfoutput>">
<input type="hidden" name="couponcode" id="couponcode" value="<cfoutput>#RetCoupon.COUPONCODE#</cfoutput>">
<div class="portlet light bordered ">
	<div class="portlet-body row">
		<div class="col-sm-12 col-md-12">
			<h3 class="page-title coupon-page-title">Coupon Name: <cfoutput>#RetCoupon.COUPONNAME#</cfoutput></h2>
			<form id="search-coupon-form">
				<div class="rows">
					<div class="col-lg-10 col-md-12 col-sm-12 coupon-search-section">

						<div class="form-group col-lg-6 col-sm-6 col-xs-12">
							<input type="text" class="form-control" id="promo-startdate" value="" placeholder="Start Date">
						</div>

						<div class="form-group col-lg-6 col-sm-6  col-xs-12">
							<input type="text" class="form-control" id="promo-enddate" value="" placeholder="End Date">
						</div>
					</div>

					<div class="form-group col-lg-2 col-md-12 col-sm-12">
						<button type="submit" class="btn green-cancel pull-left search-coupon-btn">APPLY</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="portlet light bordered ">
	<div class="portlet-body row">
		<div class="col-sm-12 col-md-12">
			<div class="content-body">
				<div class="container-fluid">
					<!--- <div class="pull-right new-coupon-btn-div">
						<a href="admin-new-coupon" class="btn green-gd">NEW COUPON</a>
					</div> --->

					<div class="col-md-12 col-lg-12 col-sm-12 coupon-report-detail">
						<a href="admin-coupon-manage" class="back-coupon-page">Back to Coupon Mainpage</a>
						<h4>Total</h4>
						<ul class="count-coupon-used">
							<li id="total-signup">Sign up: </li>
							<li id="total-upgrade">Upgrade: </li>
							<li id="total-recurring">Recurring: </li>
						</ul>
					</div>
					<div class="col-md-12 col-lg-12 col-sm-12 coupon-report-export">
						<button class="btn green-gd download-coupon-report"><i class="fa fa-download" aria-hidden="true" style="padding-right: 15px"></i>Export</button>
					</div>
					<div class="re-table">
						<div class="table-responsive">
							<table id="report-list" class="table table-striped table-bordered table-hover">
								
							</table>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>

<cfparam name="variables._title" default="Admin - Promotion code report">
<cfinclude template="../views/layouts/master.cfm">