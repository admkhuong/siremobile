<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfinvoke method="Getgoogletoken" component="session.sire.models.cfc.admin" returnvariable="RetVarggtoken" />
<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("../assets/global/plugins/daterangepicker/daterangepicker.css", true)
        .addCss("/session/sire/assets/global/plugins/Chartist/chartist.min.css", true)
        .addCss("/session/sire/css/filter_table.css", true)
        .addJs("../assets/global/plugins/highchart/highcharts.js", true)
        .addJs("../assets/global/plugins/highchart/exporting.js", true)
        .addJs("../assets/pages/scripts/jquery.textfill.min.js")
        .addJs("/session/sire/assets/global/plugins/Chartist/chartist.min.js", true)
        .addJs("../assets/global/plugins/daterangepicker/moment.min.js", true)
        .addJs("../assets/global/plugins/daterangepicker/daterangepicker.js", true)
        .addJs("/session/sire/assets/global/scripts/filter-bar-generator.js")
        .addJs("../assets/pages/scripts/admin_crm_reports.js")
        .addJs("/public/sire/js/select2.min.js", true)
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true);
</cfscript>
<cfoutput>

<div class="row dashboard">
    <div class="col-xs-12 col-sm-6 col-lg-6">
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="admin-dashboard-block-title">Total Users:</div>
                        <div class="col-md-8 col-lg-8 col-sm-8" style="padding-left: 0px">
                            <div class="admin-dashboard-value">
                                <div class="" id="total-user-div"><span id="total-user"></span></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="admin-dashboard-block-title">Total of New Users:</div>
                        <div class="col-md-8 col-lg-8 col-sm-8" style="padding-left: 0px">
                            <div class="admin-dashboard-value">
                                <div class="" id="new-user-div"><span id="new-user"></span></div>
                            </div>
                        </div>
                    </div>
                    <!--- <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 hidden-lg hidden-sm hidden-md">
                        <p></p>
                        <img src="/public/sire/images/new_user_icon.png">
                    </div> --->
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg-6">
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">
                <div class="admin-dashboard-block-title">Selected Date Range <a id ="refresh-data-crm" class="btn btn-medium"><i class="fa fa-refresh" aria-hidden="true"></i></a></div>
                <div class="col-xs-12 col-sm-12 col-lg-10 col-md-10" style="padding-left: 0px;margin-top: 20px">
                    <div class="date-range">
                        <div class="reportrange">
                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                            <span class="time"></span> <span class="pull-right" style="margin-right: 0"><b class="caret"></b></span>
                            <input type="hidden" name="dateStart" value="">
                            <input type="hidden" name="dateEnd" value="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row dashboard">
    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">
                <div class="admin-dashboard-block-title blue-text">Free</div>
                <div class="admin-dashboard-value">
                    <div class="" id="plan-free-div"><span class="plan-info" id="plan-free" data-plan="Free"></span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">
                <div class="admin-dashboard-block-title blue-text">Indv
                </div>
                <div class="admin-dashboard-value">
                    <div class="" id="plan-individual-div"><span class="plan-info" id="plan-individual" data-plan="Individual"></span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">
                <div class="admin-dashboard-block-title blue-text">Pro
                </div>
                <div class="admin-dashboard-value">
                    <div class="" id="plan-pro-div"><span class="plan-info" id="plan-pro" data-plan="Pro"></span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">
                <div class="admin-dashboard-block-title blue-text">SMB
                </div>
                <div class="admin-dashboard-value">
                    <div class="" id="plan-smb-div"><span class="plan-info" id="plan-smb" data-plan="SMB"></span></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row dashboard">
    <div class="col-xs-12 col-sm-6 col-lg-6">
        <div class="portlet light bordered admin-dashboard-large-card">
            <div class="portlet-body form">
                <div class="row">
                    <div class="col-xs-6 col-md-6 col-lg-6 col-sm-6">
                        <div class="admin-dashboard-block-title">Total Active Users:</div>
                        <div class="admin-dashboard-value">
                            <div id="user-active-div"><span class="user-info green-text" id="user-active" data-type="active"></span></div>
                        </div>
                    </div>
                    <!--- <div class="col-xs-6 col-md-6 col-lg-6 col-sm-6">
                        <div class="admin-dashboard-block-title">Total Inactive Users:</div>
                        <div class="admin-dashboard-value">
                            <div id="user-inactive-div"><span class="user-info green-text" id="user-inactive" data-type="inactive"></span></div>
                        </div>
                    </div> --->
                    <div class="col-xs-6 col-md-6 col-lg-6 col-sm-6">
                        <div class="admin-dashboard-block-title">New Paid Users:</div>
                        <div class="admin-dashboard-value">
                            <div id="user-paid-div"><span class="user-info green-text" id="user-paid"></span></div>
                        </div>
                    </div>
                </div><br/>
                <div class="row">
                    <div class="col-xs-6 col-md-6 col-lg-6 col-sm-6">
                        <div class="admin-dashboard-block-title">Total Upgrades:</div>
                        <div class="admin-dashboard-value">
                            <div id="upgrade-div"><span class="green-text" id="upgrade"></span></div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-6 col-lg-6 col-sm-6">
                        <div class="admin-dashboard-block-title">Total Downgrades:</div>
                        <div class="admin-dashboard-value">
                            <div id="downgrade-div"><span class="green-text" id="downgrade"></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg-6">
        <div class="portlet light bordered admin-dashboard-large-card">
            <div class="portlet-body form">
                <div class="row">
                    <div class="col-xs-6 col-md-6 col-lg-6 col-sm-6">
                        <div class="admin-dashboard-block-title">Total Support Request:</div>
                        <div class="admin-dashboard-value">
                            <div id="support-request-div"><span class="support-info blue-text" id="support-request" data-plan="request">0</span></div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-6 col-lg-6 col-sm-6">
                        <div class="admin-dashboard-block-title">Total Support Keyword Request:</div>
                        <div class="admin-dashboard-value">
                            <div id="support-keyword-div"><span class="support-info blue-text" id="support-keyword" data-plan="keyword"></span></div>
                        </div>
                    </div>
                </div><br/>
                <div class="row">
                    <div class="col-xs-6 col-md-6 col-lg-6 col-sm-6">
                        <div class="admin-dashboard-block-title">Total Users Not Active Since Sign Up:</div>
                        <div class="admin-dashboard-value">
                            <div id="signin-inactive-div"><span class="signin-info blue-text" id="signin-inactive" data-signin="inactive"></span></div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-6 col-lg-6 col-sm-6">
                        <div class="admin-dashboard-block-title">Total Unique Users Sign In:</div>
                        <div class="admin-dashboard-value">
                            <div id="signin-unique-div"><span class="signin-info blue-text" id="signin-unique" data-signin="unique"></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row dashboard">
    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">
                <div class="admin-dashboard-block-title">Total Message Sent</div>
                <div class="admin-dashboard-value">
                    <!--- <a class="" id="crm-message-sent">12345</a> --->
                    <div id="crm-message-sent" class="crm-message-sent"><span id="crm-msg-sent-numb"></span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">
                <div class="admin-dashboard-block-title">Total Message Received
                </div>
                <div class="admin-dashboard-value">
                    <div class="" id="crm-message-received"><span id="crm-msg-received-numb"></span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">
                <div class="admin-dashboard-block-title">Total Opt-In
                </div>
                <div class="admin-dashboard-value">
                    <div class="" id="crm-opt-in"><span id="crm-opt-in-numb"></span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">
                <div class="admin-dashboard-block-title">Total Opt-Out
                </div>
                <div class="admin-dashboard-value">
                    <div class="" id="crm-opt-out"><span id="crm-opt-out-numb"></span></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row dashboard">
    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">
                <div class="admin-dashboard-block-title">Total Buy Credits</div>
                <div class="admin-dashboard-value">
                    <!--- <a class="" id="crm-message-sent">12345</a> --->
                    <div id="crm-buy-credit" class=""><span id="crm-buy-credit-numb"></span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">
                <div class="admin-dashboard-block-title">Total Buy Keywords
                </div>
                <div class="admin-dashboard-value">
                    <div class="" id="crm-buy-keyword"><span id="crm-buy-keyword-numb"></span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">
                <div class="admin-dashboard-block-title">Total Revenue</div>
                <div class="admin-dashboard-value">
                    <!--- <div class="rows"> --->
                        <!--- <label>.com: </label>
                        <span id="txt_loading_revenue1">Processing ...</span>
                        <span id="crm-revenue-numb"></span> --->
                    <!--- </div> --->
                        <!--- <br> --->
                    <!--- <div class="rows"> --->
                        <!--- <label>Enterprise: </label>
                        <span id="txt_loading_revenue2">Processing ...</span>
                        <span id="total-enterprise-revenue-numb"></span> --->
                    <!--- </div> --->
                    <!---<div class="" id="txt_loading_revenue" style="display: none; font-size: 24px; margin-top: 35px"><span>Processing ...</span></div>--->
                    <div class="" id="crm-revenue"><span id="crm-revenue-numb"></span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="portlet light bordered admin-dashboard-mid-card">
            <input id="ggtoken" name="ggtoken" type="hidden" value="#RetVarggtoken.DATA#">
            <input id="ggspend" name="ggspend" type="hidden" value="0">
            <input id="bingspend" name="bingspend" type="hidden" value="0">
            <input id="fbadsspend" name="fbadsspend" type="hidden" value="0">
            <div class="portlet-body form">
                <div class="admin-dashboard-block-title">Total Marketing Costs</div>
                <div class="admin-dashboard-value">
                    <div class="" id="crm-acquistion-cost"><span id="crm-acquistion-cost-numb">0</span></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row dashboard">
    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">
                <div class="admin-dashboard-block-title">Enterprise Users</div>
                <div class="admin-dashboard-value">
                    <!--- <a class="" id="crm-message-sent">12345</a> --->
                    <div id="total-enterprise-user" class=""><span id="total-enterprise-user-numb"></span></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">
                <div class="admin-dashboard-block-title">Failed MFA</div>
                <div class="admin-dashboard-value">
                    <!--- <a class="" id="crm-message-sent">12345</a> --->
                    <div id="total-failed-mfa" class=""><span id="total-failed-mfa-numb"></span></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">
                <div class="admin-dashboard-block-title">Last 30 Days User Request Downgrade</div>
                <div class="admin-dashboard-value">    
                    <!--- <a class="" id="crm-message-sent">12345</a>                 --->
                    <div id="total-last30days-request-downgrade" class=""><span id="total-last30days-request-downgrade"></span></div>
                </div>
            </div>
        </div>
    </div>
     <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">
                <div class="admin-dashboard-block-title">Total Message Predicted </div>
                <div class="admin-dashboard-value">                                   
                    <div id="total-predicted" class=""><span id="total-predicted"></span></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">
                <div class="admin-dashboard-block-title">Enterprise Billing</div>
                <div class="admin-dashboard-value">    
                    <div id="enterprise-billing" class="">
                        <span id="enterprise-billing-numb"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--- 

    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">
                <div class="admin-dashboard-block-title">Enterprise Revenue</div>
                <div class="admin-dashboard-value">
                    <!--- <a class="" id="crm-message-sent">12345</a> --->
                    <div id="total-enterprise-revenue" class=""><span id="total-enterprise-revenue-numb"></span></div>
                </div>
            </div>
        </div>
    </div> --->
</div>

</cfoutput>

<!-- Modal For Plan List-->
<div class="modal fade" id="user-plan-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong>Plan <span id="user-plan-modal-title"></span></strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="userPlanFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="userPlanTable" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload user-plan"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <input type="hidden" name="userID" value="">
            </div>
        </div>
    </div>
</div>

<!-- Modal For User List-->
<div class="modal fade" id="user-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="user-modal-title"></strong></h4>                
            </div>
            <div class="modal-body">
                <div class="source-section">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="row">                    
                                                                        
                                <div class="col-xs-12 type-item" data-type="free">
                                    <div class="portlet light bordered ">
                                        <div class="form">
                                            <div class="admin-dashboard-block-title blue-text">
                                                Free accounts: <span class="ttFree">0</span>
                                            </div>                                        
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 type-item" data-type="freenomfa">
                                    <div class="portlet light bordered ">
                                        <div class="form">
                                            <div class="admin-dashboard-block-title blue-text">
                                                Free account that have not MFA yet: <span class="ttFreeNoMFA">0</span>
                                            </div>                                        
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 type-item" data-type="freeyesmfa">
                                    <div class="portlet light bordered ">
                                        <div class="form">
                                            <div class="admin-dashboard-block-title blue-text">
                                                Free accounts that have MFA already: <span class="ttFreeYesMFA">0</span>
                                            </div>                                        
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 type-item" data-type="paid">
                                    <div class="portlet light bordered ">
                                        <div class="form">
                                            <div class="admin-dashboard-block-title blue-text">
                                                Paid account: <span class="ttPaid">0</span>
                                            </div>                                        
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 type-item" data-type="freebuycredits">
                                    <div class="portlet light bordered ">
                                        <div class="form">
                                            <div class="admin-dashboard-block-title blue-text">
                                                Free accounts that have bought credits: <span class="ttFreeBuyCredit">0</span>
                                            </div>                                        
                                        </div>
                                    </div>
                                </div>                    
                                                                                    
                            </div>
                        </div>
                        
                        <div class="col-sm-6">
                            <div class="row source-loop"> 
                                <!--- <div class="col-xs-12">
                                    <div class="portlet light bordered ">
                                        <div class="form">
                                            <div class="admin-dashboard-block-title blue-text">
                                                Bing: <span class="">1</span>
                                            </div>                                        
                                        </div>
                                    </div>
                                </div> --->                            

                            </div>    
                        </div>    
                    </div>  
                    
                </div>  
                    
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="userFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="userTable" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload user-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <input type="hidden" name="userID" value="">
            </div>
        </div>
    </div>
</div>

<!-- Modal For Keyword Support List-->
<div class="modal fade" id="keyword-support-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">Keyword Support Request</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="keywordSuportFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="keywordSuportTable" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload keyword-support-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <input type="hidden" name="userID" value="">
            </div>
        </div>
    </div>
</div>

<!-- Modal For Upgrade/Downgrade List-->
<div class="modal fade" id="down-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="down-modal-title"></strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="DownFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="DownTable" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload up-down-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <input type="hidden" name="userID" value="">
            </div>
        </div>
    </div>
</div>
<!-- Modal For Upgrade List-->
<div class="modal fade" id="up-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="up-modal-title"></strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="upFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="upTable" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload up-down-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <input type="hidden" name="userID" value="">
            </div>
        </div>
    </div>
</div>

<!-- Modal For last 30 days Downgrade List-->
<div class="modal fade" id="last30days-down-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="last30days-down-modal-title"></strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="last30DaysDownFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="Last30daysDownTable" class="table table-striped table-responsive">
                                    
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload up-down-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <input type="hidden" name="userID" value="">
            </div>
        </div>
    </div>
</div>

<!-- Modal For Total Sent - Received report-->
<div class="modal fade" id="total-message-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">Total Sent and Received</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="totalMessageFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="total-message-table" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-message-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>

<!-- Modal For Total Sent - Received summary report-->
<div class="modal fade" id="total-message-summary-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">Summary</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="totalMessageSummaryFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="total-message-summary-table" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-message-summary-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>

<!-- Modal For Total Opt In - Out report-->
<div class="modal fade" id="total-optin-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">Total Opt-in</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="totalOptInFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="total-optin-table" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-optin-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>
<!-- Modal For Total Opt In - Out report-->
<div class="modal fade" id="total-optout-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">Total Opt-out</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="totalOptOutFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="total-optout-table" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-optout-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>

<!-- Modal For Opt In - Out summary report-->
<div class="modal fade" id="total-opt-summary-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">Summary</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="totalOptSummaryFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="total-opt-summary-table" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-opt-summary-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>

<!-- Modal For Credit report-->
<div class="modal fade" id="total-credit-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="">Total Buy Credits</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="totalCreditFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="total-credit-table" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-credit-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>

<!-- Modal For Keyword report-->
<div class="modal fade" id="total-keyword-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="">Total Buy Keywords</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="totalKeywordFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="total-keyword-table" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-keyword-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>

<!--- Modal For Credit Details report --->
<div class="modal fade" id="total-buy-credit-details-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">Total Buy Credits Details</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="totalCreditDetailsFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="total-credit-details-table" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-credit-details-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>

<!--- Modal For Keyword Details report --->
<div class="modal fade" id="total-buy-keyword-details-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">Total Buy Keywords Details</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="totalKeywordDetailsFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="total-keyword-details-table" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-keyword-details-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>

<!--- Modal For Enterprise Users report --->
<div class="modal fade" id="total-enterprise-users-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">Enterprise Users</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="enterpriseUserFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="total-enterprise-user-table" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-enterprise-user-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>

<!--- Modal For Enterprise Users report --->
<div class="modal fade" id="total-failed-mfa-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">Failed MFA</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="failedMFAFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="total-failed-mfa-table" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-failed-mfa-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>

<!--- Modal For Revenue report --->
<div class="modal fade" id="total-revenue-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">Total Revenue</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="total-revenue-table" class="table table-striped table-responsive">
                                        <thead>
                                            <th>Channel</th>
                                            <th>Revenue</th>
                                            <th>Credit</th>
                                            <th>Total Revenue</th>
                                        </thead>
                                        <tr>
                                            <td class="show-Com-payment-revenue">.COM revenue</td>
                                            <td id="dotComRevenue">$ 0</td>
                                            <td id="dotComRevenueCredit">$ 0</td>
                                            <td id="dotComRevenueTotal">$ 0</td>
                                        </tr>
                                        <tr>
                                            <td class="show-enterprise-revenue">Enterprise revenue</td>
                                            <td id="EnterpriseRevenue">$ 0</td>
                                            <td id="EnterpriseRevenueCredit">$ 0.00</td>
                                            <td id="EnterpriseRevenueTotal">$ 0</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--- <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-enterprise-user-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div> --->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="total-message-in-this-month" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">Total Message In This Month</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="total-revenue-table" class="table table-striped table-responsive">
                                        <thead>
                                            <th>Message</th>
                                            <th>Count Value</th>                                           
                                        </thead>
                                        <tr>
                                            <td class="">Total Message Sent</td>
                                            <td id="mtCounttxt">0</td>                                           
                                        </tr>
                                        <tr>
                                            <td class="">Total Message Predicted</td>
                                            <td id="predictedCounttxt">0</td>                                           
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>                  
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>               
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="total-enterprise-user-detail-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">Enterprise User Details</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="totalEnterpriseUserDetailFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="total-enterprise-user-detail-table" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="total-com-payment-detail-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">.COM Payment Details</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="totalComPaymentDetailFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="total-com-payment-detail-table" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="acquistion-cost-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">Total Marketing Costs</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="acquistion-cost-table" class="table table-striped table-responsive">
                                        <thead>
                                            <th>Channel</th>
                                            <th>Cost</th>
                                        </thead>
                                        <tr>
                                            <td>Google</td>
                                            <td id="gaac">$ 0</td>
                                        </tr>
                                        <tr>
                                            <td>Bing</td>
                                            <td id="babs">$ 0</td>
                                        </tr>
                                        <tr>
                                            <td>Facebook</td>
                                            <td id="fbabs">$ 0</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--- <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-enterprise-user-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div> --->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>

<!--- MODAL POPUP PREVIEW--->
<div class="bootbox modal fade" id="previewCampaignModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Preview Campaign</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <!--- <button type="button" class="btn btn-primary btn-success-custom" id="btn-rename-campaign">Save</button> --->
                <button type="button" class="btn btn-default btn-back-custom" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--- modal user with source and type--->
<!-- Modal For User source-->
<div class="modal fade" id="user-source-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="user-source-modal-title"></strong></h4>                
            </div>
            <div class="modal-body">                 
                
                <div class="row">
                    <div class="col-xs-12">
                        <div id="userSourceFilterBox">
                            
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="re-table">
                            <div class="table-responsive">
                                <table id="userSourceTable" class="table table-striped table-responsive">
                    
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-1 pull-right">
                        <button class="btn-re-dowload user-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>                
            </div>
        </div>
    </div>
</div>

<!-- Modal For User type-->
<div class="modal fade" id="user-type-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="user-type-modal-title"></strong></h4>                
            </div>
            <div class="modal-body">                 
                
                <div class="row">
                    <div class="col-xs-12">
                        <div id="userTypeFilterBox">
                            
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="re-table">
                            <div class="table-responsive">
                                <table id="userTypeTable" class="table table-striped table-responsive">
                    
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-1 pull-right">
                        <button class="btn-re-dowload user-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>                
            </div>
        </div>
    </div>
</div>
<!--- Modal Enterprise billing detail --->
<div class="modal fade" id="enterprise-billing-detail-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">Enterprise Billing Details</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="EnterpriseBillingDetailFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="enterprise-billing-detail-table" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="user-id" value="">
<input type="hidden" id="listUserids" value="">

<cfparam name="variables._title" default="Admin CRM Report - Sire">
<cfinclude template="../views/layouts/master.cfm">
 
