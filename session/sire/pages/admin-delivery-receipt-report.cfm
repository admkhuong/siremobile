<cfscript>
    createObject("component", "public.sire.models.helpers.layout")
       
        .addJs('/session/sire/assets/global/scripts/filter-bar-generator.js', true)       
        .addCss("../assets/global/plugins/daterangepicker/daterangepicker.css", true)        
        .addCss("/session/sire/css/filter_table.css", true)
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)
        
        .addJs("../assets/global/plugins/daterangepicker/moment.min.js", true)
        .addJs("../assets/global/plugins/daterangepicker/daterangepicker.js", true)
        .addJs("/public/sire/js/select2.min.js")
         
        .addJs("/session/sire/assets/pages/scripts/admin-delivery-receipt-report.js");        
</cfscript>


<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<div class="portlet light bordered">
    <form id="frSearch">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="page-title">Search</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3 col-md-3">
                <div class="form-group">
                    <select class="form-control validate[required] select2" id="search-by">
                        <option value="u.EmailAddress_vch">Search By User Email</option>
                        <option value="u.UserId_int">Search By User ID</option>
                        <option value="b.BatchId_bi">Search By Campaign ID</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-3 col-md-3">
                <div class="form-group">
                    <input class="form-control validate[required]" id="search-value" placeholder="Search value">
                </div>
            </div>
            <div class="col-sm-3 col-md-3">
                <button type="submit" class="btn green-gd" id="search-now">Search</button>
            </div>
            <div class="col-sm-3 col-md-3">
                
            </div>
        </div>
    </form>
</div>

<div class="portlet light bordered hidden" id="report-section">
    <!--- Filter bar --->
    <div class="row">
        <div class="col-sm-3 col-md-4">
            <h2 class="page-title">List Campaigns</h2>
        </div>
        <div class="col-sm-9 col-md-8">
            <div id="box-filter" class="form-gd">

            </div>
        </div>
    </div>
    <!--- Listing --->
    <div class="row">
        <div class="col-md-12">
            <div class="re-table">
                <div class="table-responsive">
                    <table id="tblListBlast" class="table-responsive table-striped dataTables_wrapper dataTable">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="bootbox modal fade in" id="mdCampaignReport" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleModalLabel">Preview Campaign</h4>
            </div>
            <div class="modal-body">
                <div class="row">    
                    
                    <div class="col-xs-12 col-sm-12">
                        <h4 class="portlet-heading2">Campaign Information</h4>                        
                        <div class="form-group clearfix">
                            <div class="col-sm-12">Campaign Id: <b><span id="campaign-id"></span></b></div>
                            <div class="col-sm-12">Campaign Name: <b><span id="campaign-name"></span></b></div>
                            <div class="col-sm-12">User Id: <b><span id="user-id"></span></b></div>
                            <div class="col-sm-12">User Email: <b><span id="user-mail"></span></b></div>
                        </div> 
                        <div class="clearfix"></div>
                        <hr class="hr0">                        
                    </div>
                </div>                
                <div class="row">                        
                    <div class="col-xs-12 col-sm-12">
                        <h4 class="portlet-heading2">Schedule Option</h4>    
                        <div id="schedule-options-detail">

                        </div>
                        
                        <div class="clearfix"></div>
                        <hr class="hr0">                    
                    </div>
                </div>
                <div class="row">                        
                    <div class="col-xs-12 col-sm-12">
                        <h4 class="portlet-heading2">Summary Data</h4>                        
                        <div class="form-group clearfix">
                            <div class="col-xs-9 col-sm-6">Number of Subscribers</div>
                            <div class="col-xs-3 col-sm-6" id="number-subscribers">0</div>
                        </div> 
                        <div class="form-group clearfix">
                            <div class="col-xs-9 col-sm-6">Send to mblox successfully</div>
                            <div class="col-xs-3 col-sm-6" id="mblox-number-sent">0</div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="col-xs-9 col-sm-6">Send to mblox failure</div>
                            <div class="col-xs-3 col-sm-6" id="mblox-number-fail">0</div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="col-xs-9 col-sm-6">In progress</div>
                            <div class="col-xs-3 col-sm-6" id="number-progress">0</div>
                        </div>  
                        <div class="form-group clearfix">
                            <div class="col-xs-9 col-sm-6">Delivered</div>
                            <div class="col-xs-3 col-sm-6" id="delivered">0</div>
                        </div>  
                        <div class="form-group clearfix">
                            <div class="col-xs-9 col-sm-6">Lost Notification</div>
                            <div class="col-xs-3 col-sm-6" id="lost-noti">0</div>
                        </div>  
                        <div class="form-group clearfix">
                            <div class="col-xs-9 col-sm-6">Non Delivered</div>
                            <div class="col-xs-3 col-sm-6" id="non-delivered">0</div>
                        </div>  
                                                                  
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-back-custom" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<cfparam name="variables._title" default="Admin Delivery Receipt Report">
<cfinclude template="../views/layouts/master.cfm">

