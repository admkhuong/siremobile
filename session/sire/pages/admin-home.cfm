
<cfparam name="currTab" default="">
<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfoutput>

<div class="portlet light bordered adminsection">
	<div class="portlet-body" id="section-it">
		<h4 class="portlet-heading">IT</h4>
		<div class="row  content-simon-new">
			<div class="content-body "id="sectionIt" >
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-system-usage-report-today">System Usage Report (Today: #LSDateFormat(Now(), 'full')#)</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-system-usage-report">System Usage Report (Full)</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-site-errors">Site Errors</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-noc-settings">NOC Settings</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-noc-reports">NOC Reports</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-shortcode-manage">ShortCode Manage</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="live-chat-setting">Settings</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-setting-system">Payment Settings</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-nagios-report">Nagios Report</a>
				</div>
			</div>
		</div>
	</div>
	<div class="portlet-body" id="section-campign">
		<h4 class="portlet-heading">Campaign</h4>
		<div class="row  content-simon-new">
			<div class="content-body "id="sectionCampaign" >
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-mlp-template">CPP Template</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-template-category-management">Template Category management</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="/session/sire/pages/campaign-template-picker?adv=1">Build custom campaign using existing templates</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="campaign-template-picker?adv=1&inpTemplateFlag=1">Edit an Existing Campaign Template</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="keyword-lookup">Keyword Lookup</a>
				</div>
			</div>
		</div>
	</div>
	<div class="portlet-body" id="section-support">
		<h4 class="portlet-heading">Support</h4>
		<div class="row  content-simon-new">
			<div class="content-body "id="sectionSupport" >
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-account-update">Account management</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-plan-management">Plan management</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-user-support">User Support</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-recurring-report">Recurring Report</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-payment-transactions">Payment Transactions</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-copy-batch">Copy Batch</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="trouble-ticket">Trouble Ticket</a>
				</div>
				<!---<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-contactstring-history-report">Contact String History Report</a>
				</div>--->
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-import-contact">Import Contact</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-verify-user-import-contact">Customer Import Approval</a>
				</div>
				<!--- <div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-master-user-coupon">Master user coupon assignment</a>
				</div> --->
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-coupon-manage">Coupon Management</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="/session/sire/pages/admin-tool-message">SMS Communication Tool</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-block-phone-number">Block Phone Number</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-whilelist-phonenumber">White List Phone Number</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-block-words">Black List Words</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="/session/sire/pages/verify">Verify</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-block-ip-address">Block IP Address List</a>
				</div>

				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-payment-transaction-suspicious">Verify Suspicious Transaction</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin_verify_payment_transaction">Verify Payment Transactions</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-payment-transaction-exceeded-limit">Payment Transactions Exceeded Limit</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin_search_MTs">Search MTs for special keywords</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="blast_management">Blast Management</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="/session/sire/pages/admin-report-paid-users">Report Paid Users</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-delivery-receipt-report">Admin Delivery Receipt Report</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-management-user-cc">Admin Management User CC</a>
				</div>
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="admin-management-ip-address">Admin Management IP Address</a>
				</div>				
			</div>
		</div>
	</div>

	<!---
	<div class="portlet-body">
		<h4 class="portlet-heading">Admin</h4>

    	<div class="row  content-simon-new">

			<div class="content-body "id="WelcomePageTutorial" >

			</div>
		</div>
	</div>
	--->
	<div class="portlet-body" id="section-crm">
		<h4 class="portlet-heading">CRM</h4>
		<div class="row  content-simon-new">
			<div class="content-body "id="sectionCRM">
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_self" href="admin-crm-reports">Admin CRM Reports</a>
				</div>
			</div>
		</div>
	</div>
</cfoutput>

<cfparam name="variables._title" default="Admin Home">

<cfinclude template="../views/layouts/master.cfm">
<cfoutput>
	<script type="text/javascript">
		$( document ).ready(function() {
			if(window.location.pathname==="/session/sire/pages/admin-home")
			{
				var currTab= "#currTab#";
				$("##"+currTab).show();
			}
		});
	</script>
</cfoutput>
