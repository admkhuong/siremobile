<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
<!--- <cflocation url="/session/sire/pages/admin-home" addtoken="false"/> --->

<cfinclude template="/public/sire/configs/bulk_insert_constants.cfm">

<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfparam name="uploadid" default="0"/>

<cfinvoke method="GetUploadInfo" component="session.sire.models.cfc.import-contact" returnvariable="RetVarGetUploadInfo">
	<cfinvokeargument name="inpUploadId" value="#uploadid#"/>
</cfinvoke>

<cfif RetVarGetUploadInfo.RXRESULTCODE LT 1 OR RetVarGetUploadInfo.UPLOAD.SendToQueue_ti NEQ 0>
	<cflocation url="/session/sire/pages/admin-import-contact" addtoken="false"/>
</cfif>

<cfinvoke method="GetCampaignDetail" component="session.sire.models.cfc.campaign" returnvariable="RetVarGetCampaign">
	<cfinvokeargument name="INPCAMPAIGNID" value="#RetVarGetUploadInfo.UPLOAD.BatchId_bi#"/>
</cfinvoke>

<cfset CDFS = listToArray(RetVarGetUploadInfo.UPLOAD.ColumnNames_vch)/>

<cfinvoke method="GetBalance" component="session.sire.models.cfc.billing" returnvariable="RetVarUserBalance">
	<cfinvokeargument name="inpUserId" value="#RetVarGetUploadInfo.UPLOAD.UserId_int#"/>
</cfinvoke>

<cfset userBalance = "Cant get user balance info"/>
<cfset isUnlimitedAccount = "No"/>
<cfif RetVarUserBalance.RXRESULTCODE GT 0>
	<cfset userBalance = NumberFormat(RetVarUserBalance.BALANCE, ",")/>
	<cfset isUnlimitedAccount = yesNoFormat(RetVarUserBalance.UNLIMITEDBALANCE)/>
</cfif>

<cfinvoke method="GetCampaignListById" component="session.sire.models.cfc.campaign" returnvariable="RetVarGetUserCampaignList">
	<cfinvokeargument name="inpSourceUserId" value="#RetVarGetUploadInfo.UPLOAD.UserId_int#"/>
</cfinvoke>

<cfinvoke method="GetEligibleCount" component="session.sire.models.cfc.import-contact" returnvariable="RetVarGetEligibleCount">
	<cfinvokeargument name="inpUploadId" value="#uploadid#"/>
</cfinvoke>

<cfset RetVarGetEligibleCount = deserializeJSON(RetVarGetEligibleCount)/>
<cfset recordSent = 0/>
<cfset recordEligible = 0/>
<cfset recordSending = 0/>
<cfset recordOther = 0/>
<cfif RetVarGetEligibleCount.RXRESULTCODE GT 0>
	<cfset recordSent = RetVarGetEligibleCount.SENT/>
	<cfset recordEligible = RetVarGetEligibleCount.ELIGIBLE/>
	<cfset recordSending = RetVarGetEligibleCount.SENDING/>
	<cfset recordOther = RetVarGetEligibleCount.OTHER/>
</cfif>

<cfinvoke component="session.sire.models.cfc.import-contact" method="getTZList" returnvariable="tzList"></cfinvoke>

<cfscript>
	CreateObject("component","public.sire.models.helpers.layout")
		.addCss("/public/sire/css/select2.min.css")
		.addCss("/public/sire/css/select2-bootstrap.min.css")
		.addCss("/public/sire/js/icheck/square/blue.css")
		.addCss("/public/sire/css/daterangepicker.css")
		.addCss("/public/css/campaign/mycampaign/schedule.css")
		.addCss("/public/js/jquery-ui-1.11.4.custom/jquery-ui.css")
		.addCss("/session/sire/css/message.css")
		.addCss("/session/sire/css/preview.css")
		.addCss("/session/sire/css/schedule.css")
		.addCss("/public/js/jquery-ui-1.11.4.custom/jquery-ui.css")
		.addJs("/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js")
		.addJs("/public/sire/js/select2.min.js")
		.addJs("/session/sire/js/vendors/bootstrap-filestyle.min.js")
		.addJs("/public/sire/js/icheck/icheck.min.js")
		.addJs("/public/sire/js/moment.min.js")
		.addJs("/public/sire/js/daterangepicker.js")
		.addJS("/session/sire/js/schedule-import.js")
		.addJs("/session/sire/js/admin-upload-detail.js")
		.addJS("/session/sire/js/import-contact-schedule.js")
		.addJS("/public/js/jquery.tmpl.min.js")
		.addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true);
        // .addJs("../assets/pages/scripts/data-field-filter.js");
</cfscript>



<cfinclude template="/session/sire/configs/paths.cfm">	
<cfinclude template="/public/sire/configs/bulk_insert_constants.cfm"/>

<script type="text/javascript">
	var uploadStatus = [];
	<cfoutput>
		<cfloop collection="#UPLOADS_STATUS_LANG#" item="key">
			uploadStatus[#key#] = "#UPLOADS_STATUS_LANG[key]#"
		</cfloop>
	</cfoutput>
</script>

<cfset INPBATCHID = 0/>

	<cfoutput>
			<!--- Set some globally avaialble javascript vars based on current data --->
	<script TYPE="text/javascript">
		
		var INPBATCHID = 0;
		var schedule_data = "";
		
		var date_txt_1 = '#LSDateFormat(DateAdd("d", 30, Now()), "m-d-yyyy")#';
		var date_txt_2 = '#LSDateFormat(NOW(), "m-d-yyyy")#';

		var ScheduleTimeData;

		var CDFS = "#RetVarGetUploadInfo.UPLOAD.ColumnNames_vch#";
		CDFS = CDFS.split(',');

		var uploadid = #uploadid#;

		var totalEligible = #RetVarGetEligibleCount.ELIGIBLE#;
	</script>
	
	<input name="SCHEDULEDATA" id="SCHEDULEDATA" type="hidden" value=""/>
	<br/>
	<!--- BEGIN FILTER --->
	<div class="portlet light bordered #iIf(recordEligible LT 1, DE("hidden"), DE(""))#">
		<div class="portlet-body">
			<div class="uk-grid uk-grid-upsmall" uk-grid>
				<div class="uk-width-1-2">
					<h4 class="portlet-heading master2-heading mb-0">Data Field Filters</h4>
				</div>
				<div class="uk-width-1-2 pull-right">
					<a href="/session/sire/pages/admin-import-contact"><h4 class=" mb-0 pull-right">Back to list</h4></a>
				</div>
			</div>
			<div class="uk-grid uk-grid-upsmall <!--- uk-margin-medium-top --->" uk-grid>
				<div class="uk-width-1-2@xl uk-width-4-5@l">
					<div class="uk-grid " uk-margin>
						<div class="uk-width-1-2 uk-width-1-4@s">
							<ul class="uk-nav nav-label">
								<li><b>Total Import:</b> #RetVarGetUploadInfo.UPLOAD.Total_int#</li>
							</ul>
						</div>
						<div class="uk-width-1-2 uk-width-1-4@s">
							<ul class="uk-nav nav-label">
								<li><b>Total Valid:</b> #RetVarGetUploadInfo.UPLOAD.Valid_int#</li>
							</ul>
						</div>
						<div class="uk-width-1-2 uk-width-1-4@s">
							<ul class="uk-nav nav-label">
								<li><b>Total Sent:</b> #recordSent#</li>
							</ul>
						</div>
						<div class="uk-width-1-2 uk-width-1-4@s">
							<ul class="uk-nav nav-label">
								<li><b>Total Remaining: <span style="color: red;font-size: 16px">#recordEligible#</span></b></li>
							</ul>
						</div>
<!--- 						<div class="uk-width-1-2 uk-width-1-3@s">
							<ul class="uk-nav nav-label">
								<li><b>Batch ID:</b> 125</li>
							</ul>
						</div>
						<div class="uk-width-1-2 uk-width-1-3@s">
							<ul class="uk-nav nav-label">
								<li><b>Campaign Type:</b> Campaign Blast</li>
							</ul>
						</div>
						<div class="uk-width-1-2 uk-width-1-3@s">
							<ul class="uk-nav nav-label">
								<li><b>Campaign Name:</b> Campaign_3</li>
							</ul>
						</div> --->
					</div>
				</div>
			</div>

			<div class="uk-grid uk-grid-upsmall <!--- uk-margin-medium-top --->" uk-grid>
				<div class="uk-width-1-2@xl uk-width-4-5@l">
					<div class="uk-grid " uk-margin>
						<div class="uk-width-1-2 uk-width-1-4@s">
							<ul class="uk-nav nav-label">
								<li><b>UserID:</b> #RetVarGetUploadInfo.UPLOAD.UserId_int#</li>
							</ul>
						</div>
						<div class="uk-width-1-2 uk-width-1-4@s">
							<ul class="uk-nav nav-label">
								<li><b>User Total Balance:</b> #userBalance#</li>
							</ul>
						</div>
						<div class="uk-width-1-2 uk-width-1-4@s">
							<ul class="uk-nav nav-label">
								<li><b>Is Unlimited Account:</b> #isUnlimitedAccount#</li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="uk-grid uk-grid-upsmall <!--- uk-margin-medium-top --->" uk-grid>
				<div class="uk-width-4-6@xl uk-width-4-5@l filter-section">
	                <div class="uk-grid uk-grid-upsmall uk-flex-middle filter-body filter-row " uk-margin>
	                	<div class="uk-width-auto">
	                		<button type="button" class="btn green-cancel btn-remove-filter"><i class="fa fa-minus"></i></button>
	                	</div>
	                	<div class="uk-width-1-5@s">
	                		<select class="form-control filter-field">
								<option value="State_vch">State</option>
								<option value="City_vch">City</option>
								<!--- <option value="Zipcode_vch">Zipcode</option> --->
								<option value="TimeZone_int">TimeZone</option>
								<cfoutput>
									<cfloop array="#CDFS#" index="field">
										<option value="#field#">#field#</option>
									</cfloop>
								</cfoutput>
							</select>
	                	</div>
	                	<div class="uk-width-1-5@s">
	                		<select class="form-control filter-action">
								<option value="=">Is</option>
								<option value="!=">Is Not</option>
								<option value="like">Similar To</option>
								<option value="not like">Not Similar To</option>
							</select>
	                	</div>
	                	<div class="uk-width-1-6@s">
	                		<input type="text" class="form-control filter-value">
	                	</div>
	                	<!--- <div class="uk-width-expand">
	                		<label for="" class="color-5c"><b>Total left eligible to send: </b>1,000</label>
	                	</div> --->
	                </div>

	                <div class="uk-grid uk-grid-upsmall uk-flex-middle" uk-margin>
	                	<div class="uk-width-auto">
	                		<button type="button" class="btn green-cancel btn-add-filter"><i class="fa fa-plus"></i></button>
	                	</div>
	                </div>
				</div>

				<div class="uk-width-4-6@xl uk-width-4-5@l <!--- uk-margin-medium-top ---> uk-margin-small-bottom">
					<div class="uk-grid uk-grid-upsmall uk-flex-middle" uk-margin>
						<div class="uk-width-auto">
							<button type="button" class="btn green-cancel btn-apply-filter mr-15">APPLY FILTER</button>
						</div>	
					</div>	
				</div>

				<div class="uk-width-4-6@xl uk-width-4-5@l <!--- uk-margin-medium-top ---> uk-margin-small-bottom">
					<div class="uk-grid uk-grid-upsmall uk-flex-middle" uk-margin>
						<form class="send-partial-form form-inline">
							<div class="form-group">
								<label><b>Select Campaign</b></label>&nbsp;&nbsp;
								<select name="campaign-id" class="form-control validate[required] select2" id="CampaignId">
									<cfoutput>
										<cfloop array="#RetVarGetUserCampaignList.CampaignList#" index="campaign">
											<option value="#campaign.id#">#campaign.text#</option>
										</cfloop>
									</cfoutput>
								</select>
							</div>
						</form>
	                	<div class="uk-width-auto">
	                		<label for=""><b>Total to be send</b></label>
	                	</div>
	                	<div class="uk-width-1-5@s">
		                	<form class="send-partial-form">
		                		<input type="text" class="form-control validate[required,custom[integer,min[1]]]" id="send-number" value="" />
		                	</form>
	                	</div>
	                	<div class="uk-width-expand">
	                		<button type="button" class="btn green-cancel mr-15 btn-send">send</button>
	                		<!--- <button type="button" class="btn green-cancel green-cancel-trash">delete</button> --->
	                	</div>
					</div>		
				</div>
			</div>
			<div class="uk-width-1-5@s">
				<a href="" class="btn-show-scheduler hidden"> <span class="text-create-hidden hidden">Set</span> Schedule </a>
			</div>
			<br/>
		</div>
	</div>

	<!--- END FILTER --->

	<!--- BEGIN FILE HISTORY --->
	<div class="portlet light bordered">
		<div class="portlet-body">
			<div class="uk-grid uk-grid-upsmall" uk-grid>
				<div class="uk-width-1-2">
					<h4 class="portlet-heading master2-heading mb-0">File Allocation History</h4>
				</div>
				<cfif recordEligible LT 1>
					<div class="uk-width-1-2 pull-right">
						<a href="/session/sire/pages/admin-import-contact"><h4 class=" mb-0 pull-right">Back to list</h4></a>
					</div>
				</cfif>
			</div>
			<div class="uk-grid uk-grid-upsmall uk-margin-medium-top" uk-grid>
				<div class="uk-width-3-5@l">
					<div class="uk-grid " uk-margin>
						<div class="uk-width-2-5">
							<ul class="uk-nav nav-label">
								<li><b>File Name:</b> #RetVarGetUploadInfo.UPLOAD.OriginFileName_vch#</li>
							</ul>
						</div>
						<div class="uk-width-3-5">
							<ul class="uk-nav nav-label">
								<li><b>Import Date:</b> #DateFormat(RetVarGetUploadInfo.UPLOAD.Created_dt,"mm-dd-yyyy")# #TimeFormat(RetVarGetUploadInfo.UPLOAD.Created_dt, "HH:mm:ss")#</li>
							</ul>
						</div>
						<div class="uk-width-2-5">
							<ul class="uk-nav nav-label">
								<li><b>Org Cnt:</b> #RetVarGetUploadInfo.UPLOAD.Total_int#</li>
							</ul>
						</div>
						<div class="uk-width-2-5">
							<ul class="uk-nav nav-label">
								<li><b>Import Cnt:</b> #RetVarGetUploadInfo.UPLOAD.Total_int#</li>
							</ul>
						</div>
						<div class="uk-width-1-5">
							<ul class="uk-nav nav-label">
								<li><b>Valid Cnt:</b> #RetVarGetUploadInfo.UPLOAD.Valid_int#</li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="uk-grid">
				<div class="uk-grid-width-1-1">
					<div class="re-table">  
						<table id="report-upload-blast" class="table table-responsive table-bordered">
						
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--- END FILE HISTORY --->

	<!--- <div class="row bg-white">
		<div class="col-lg-12">
			<div class="content-header" style="min-height: 0px; padding-bottom: 0px;">
				<div class="row">
					<div class="col-sm-12 heading-title">File Allocation History</div>
				</div>				
			</div>
		</div>
		<div class="col-lg-12 col-sm-12">
			<div class="panel-body">
				<cfoutput>
					<div class="row">
						<div class="col-md-4 col-lg-4 col-sm-4 col-sx-12">
							<b>File Name:</b> #RetVarGetUploadInfo.UPLOAD.OriginFileName_vch#
						</div>
						<div class="col-md-4 col-lg-4 col-sm-4 col-sx-12">
							<b>Org Cnt:</b> #RetVarGetUploadInfo.UPLOAD.Total_int#
						</div>
					</div>
					<div class="row">
						<div class="col-md-4 col-lg-4 col-sm-4 col-sx-12">
							<b>Import Date:</b> #DateFormat(RetVarGetUploadInfo.UPLOAD.Created_dt,"mm-dd-yyyy")# #TimeFormat(RetVarGetUploadInfo.UPLOAD.Created_dt, "HH:mm:ss")#
						</div>
						<div class="col-md-4 col-lg-4 col-sm-4 col-sx-12">
							<b>Import Cnt:</b> #RetVarGetUploadInfo.UPLOAD.Total_int#
						</div>
					</div>
				</cfoutput>
				<div class="table-responsive">  
						<table id="report-upload-blast" class="table table-responsive table-bordered">
						
					</table>
				</div>
			</div>
		</div>
	</div> --->


</cfoutput>

<style type="text/css">
table .btn {
	margin: 3px 6px 3px 0;
}

#report-file-upload td p {
	margin: 0px;
}

td.updated-by p.lbl {
	font-size: 13px;
}

.reportStat{
	font-weight: bold;
}

.table-bordered > thead:first-child > tr:first-child > th{
	font-size: 15px;
}

a.editable{
	text-decoration: underline;
}

td.col-center {
	text-align: center;
}
.newbtn {
	margin-left: 20px;
}
.eligible-count span {
	vertical-align: middle;
}
</style>

<!-- Modal -->
<div class="bootbox modal fade" id="scheduler" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				 <h4 class="modal-title">Campaign Blast Scheduler</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<fieldset>
							<legend>Date Range</legend>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label>Start date:</label>
										<input type="text" class="form-control date-picker" name="start-date">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>End date:</label>
										<input type="text" class="form-control date-picker" name="end-date">
									</div>
								</div>
							</div>
						</fieldset>
					</div>
					<div class="col-md-6">
						<fieldset>
							<legend>Time Range</legend>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label>Start time:</label>
										<div class="input-group bootstrap-timepicker timepicker">
								            <input  type="text" class="form-control input-small time-picker" name="start-time">
								            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
								        </div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>End time:</label>
										<div class="input-group bootstrap-timepicker timepicker">
								            <input  type="text" class="form-control input-small time-picker" name="end-time">
								            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
								        </div>
									</div>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#advance-schedule">Advance</button>
						<div id="advance-schedule" class="collapse ">
						   	<div class="row">
						   		<div class="col-sm-6">
						   			<div class="well well-sm">
						   				<div class="row">
							   				<div class="col-xs-12">Sunday</div>
							   				<div class="col-xs-12">Monday</div>
							   				<div class="col-xs-12">Tuesday</div>
							   				<div class="col-xs-12">Webnesday</div>
							   				<div class="col-xs-12">Thursday</div>
							   				<div class="col-xs-12">Friday</div>
							   				<div class="col-xs-12">Saturday</div>	
						   				</div>
					   				</div>
						   		</div>
						   		<div class="col-sm-6">
						   			<div class="well well-sm">Small Well</div>
						   		</div>
						   	</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" value="" name="campaign-id">
				<button type="button" class="btn btn-primary btn-success-custom visible" >Save</button>
				<button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="ScheduleOptionsModal" tabindex="-1" role="dialog" aria-labelledby="ScheduleOptionsModal" aria-hidden="true">
    <div class="modal-dialog" style="width:auto; max-width:900px;">
        <div class="modal-content">
        	<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>            
				<h4 class="modal-title title">Schedule Options</h4>
            </div>
            <cfset campaignid = 1 />
            <cfinclude template="dsp_advance_schedule.cfm">
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<cfparam name="variables._title" default="Admin - Upload details">
<cfinclude template="../views/layouts/master2.cfm">
