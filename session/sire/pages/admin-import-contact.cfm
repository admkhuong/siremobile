<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />
<cfinvoke component="session.sire.models.cfc.import-contact" method="getTZList" returnvariable="tzList"></cfinvoke>

<cfscript>
	CreateObject("component","public.sire.models.helpers.layout")
		.addCss("/public/sire/css/select2.min.css")
		.addCss("/public/sire/css/select2-bootstrap.min.css")
		.addCss("/public/sire/js/icheck/square/blue.css")
		.addCss("/public/sire/css/daterangepicker.css")
		.addCss("/public/css/campaign/mycampaign/schedule.css")
		.addCss("/public/js/jquery-ui-1.11.4.custom/jquery-ui.css")
		.addCss("/session/sire/css/message.css")
		.addCss("/session/sire/css/preview.css")
		.addCss("/session/sire/css/schedule.css")
		.addCss("/public/js/jquery-ui-1.11.4.custom/jquery-ui.css")
		.addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
		.addJs("/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js")
		.addJs("/public/sire/js/select2.min.js")
		.addJs("/session/sire/js/vendors/bootstrap-filestyle.min.js")
		.addJs("/public/sire/js/icheck/icheck.min.js")
		.addJs("/public/sire/js/moment.min.js")
		.addJs("/public/sire/js/daterangepicker.js")
		.addJs("/session/sire/js/admin-import-contact.js")
		.addJS("/session/sire/js/import-contact-schedule.js")
		.addJS("/public/js/jquery.tmpl.min.js")
		.addJS("/session/sire/js/schedule-import.js")
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)
        .addJs("../assets/pages/scripts/import-contact-form.js");
</cfscript>



<cfinclude template="/session/sire/configs/paths.cfm">	
<cfinclude template="/public/sire/configs/bulk_insert_constants.cfm"/>

<script type="text/javascript">
	var uploadStatus = [];
	<cfoutput>
		<cfloop collection="#UPLOADS_STATUS_LANG#" item="key">
			uploadStatus[#key#] = "#UPLOADS_STATUS_LANG[key]#"
		</cfloop>
	</cfoutput>
</script>

<cfset INPBATCHID = 0/>

<cfoutput>
		<!--- Set some globally avaialble javascript vars based on current data --->
<script TYPE="text/javascript">
	
	var INPBATCHID = 0;
	var schedule_data = "";
	
	var date_txt_1 = '#LSDateFormat(DateAdd("d", 30, Now()), "m-d-yyyy")#';
	var date_txt_2 = '#LSDateFormat(NOW(), "m-d-yyyy")#';

	var ScheduleTimeData;

</script>
</cfoutput>


<div class="portlet">
	<input name="SCHEDULEDATA" id="SCHEDULEDATA" type="hidden" value=""/>

	<div class="uk-grid uk-grid-upsmall" uk-margin>
		<div class="uk-width-1-4@l uk-width-2-5@m">
			<div class="portlet light bordered">
				<div class="portlet-body">
					<form id="import-contact" name="import-contact" enctype="multipart/form-data">
					<h4 class="portlet-heading master2-heading mb-0">Import Contact</h4>

					<div class="input-group image-preview uk-margin-top">
		                <input type="text" class="form-control image-preview-filename"> <!-- don't give a name === doesn't send on POST/GET -->
		                <span class="input-group-btn">
		                    <!--- <button type="button" class="btn green-cancel-trash image-preview-clear" style="display:none;">Clear</button> --->
		                    <div class="btn green-cancel image-preview-input">
		                        <!--- <span class="image-preview-input-title">Choose csv</span>
		                        <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/> <!-- rename it --> --->
		                        <span class="image-preview-input-title" id="preview-button">Choose file</span>
		                        <input type="file" class="validate[required]" data-buttonText="Choose CSV" name="" accept=".csv" id="csv-file">
		                    </div>
		                </span>
		            </div>
		            <i>Only support file with UTF-8 encoding</i>

		            <div class="uk-grid uk-grid-upsmall uk-margin-top uk-margin-bottom" uk-margin>
		            	<div class="uk-width-1-2">
		            		<div class="ad-radio">

                    			<input type="radio" class="css-checkbox" checked id="rd-subcriber-list" value="1" data-control="subcriber-list" name="option-to">
								<label class="radGroup1" for="rd-subcriber-list">Subscribers List</label>

		            		</div>
		            	</div>
		            	<div class="uk-width-1-2">
		            		<div class="ad-radio">
	                			<input type="radio" class="css-checkbox" id="rd-campaign-blast" value="2" data-control="campaign-blast" name="option-to">
								<label  class="radGroup1" for="rd-campaign-blast">Campaign Blast</label>

		            		</div>
		            	</div>
		            </div>

		            <div class="wrap-switch-content">



			            <div class="uk-grid uk-grid-upsmall hidden form-group uk-margin" data-control="campaign-blast">
							<div class="uk-width-1-2">
								<div class="ad-radio">
									<input type="radio" class="css-checkbox" checked id="rd-queue-now" value="1" name="inpSendToQueue">
									<label class="radGroup1 radGroup2" for="rd-queue-now">Send All</label>
								</div>	
							</div>
									
							<div class="uk-width-1-2">
								<div class="ad-radio">
									<input type="radio" class="css-checkbox" id="rd-queue-later" value="0" name="inpSendToQueue">
									<label  class="radGroup1 radGroup2" for="rd-queue-later">Send Partial</label>
								</div>	
							</div>
						</div>



						<div class="form-gd uk-margin-top">
							<div class="form-group">
				            	<label for="">Select User</label>
				            	<select name="user-id" class="form-control select2 validate[required] uk-width-4-5@l" id="user-id">
				            	</select>	
				            </div>

				            <div class="form-group" data-control="subcriber-list">
								<label>Select Subscribers List</label>
								<select name="subcriber-id" class="form-control select2 non-ds validate[required]"  data-modal-id="new-subcriber-list">
								</select>
								
							</div>

				            <div class="form-group" data-control="subcriber-list">
				            	<p><a href="javascript:void(0)" class="create-new-subcriberlist hidden"> Create New Subscribers List</a> </p>
				            </div>


				            <div class="form-group hidden campaign-select" data-control="campaign-blast">
								<label>Select Campaign</label>
								<select name="campaign-id" class="form-control select2 non-ds" data-modal-id="new-campaign">
									
								</select>
								<br/>
								<a href="" class="btn-show-scheduler"> <span class="text-create-hidden hidden">Set</span> Schedule </a>
							</div>


				            <div id="section-advance" style="display: none">
								<div class="form-group">
									<label>Total number of columns used in your file</label>
									<input class="form-control" type="text" name="inpColumnNumber" id="inpColumnNumber" value="1">
								</div>
								<div class="form-group">
									<label>Column number for phone on your list</label>
									<select class="form-control" name="inpContactStringCol" id="inpContactStringCol">
										<option>1</option>
									</select>
								</div>
								<div class="form-group">
									<label>Column Names (separate by enter)</label>
									<textarea style="resize: vertical;" class="form-control" name="inpColumnNames" id="inpColumnNames">ContactString_vch</textarea>
								</div>
								<div class="form-group">
									<label>Field delimiters</label>
									<input class="form-control" type="text" name="inpSeparator" id="inpSeparator" value="," maxlength="1" size="1">
								</div>
								<div class="form-group">
									<label>Total line ignore when import</label>
									<input class="form-control" type="text" name="inpSkipLine" id="inpSkipLine" value="0">
								</div>
								<div class="form-group">
									<label>Number of errors allowed</label>
									<input class="form-control" type="text" name="inpErrorsIgnore" id="inpErrorsIgnore" value="0">
								</div>
							</div>


				            <div class="form-action">
				            	<button type="submit" class="btn green-gd mr-15 btn-import">Import</button>
								<input type="hidden" name="guid">
								<button class="btn green-cancel btn-advance " id="btn-advance">Advance</button>
				            </div>
						</div>

		            </div>
		        </form>    
				</div>
			</div>
		</div>

		<div class="uk-width-3-4@l uk-width-3-5@m">
			<div class="portlet light bordered">
				<div class="portlet-body">
					<h4 class="portlet-heading master2-heading mb-0">Report</h4>
					<form name="report-filter" id="report-filter">
					<div class="uk-grid uk-grid-upsmall uk-margin-top">
						<div class="uk-width-3-5@xl">
							<div class="uk-grid uk-grid-upsmall" uk-grid>
								<div class="uk-width-1-3@s">
									<select name="select-user-id-filter" data-placeholder="Select an user by id or email" class="select2 form-control">
									</select>
								</div>
								<div class="uk-width-1-3@s">
									<input type="text" data-type="daterange" data-control="start-date" class="form-control">
									<cfoutput>
										<input type="hidden" name="date-start" value="#DateFormat(NOW(), "yyyy-mm-dd")#" class="form-control" data-value="#DateFormat(NOW(), "mm/dd/yyyy")#">
										<input type="hidden" name="date-end" value="#DateFormat(NOW(), "yyyy-mm-dd")#" class="form-control" data-value="#DateFormat(NOW(), "mm/dd/yyyy")#">
									</cfoutput>
								</div>
								<div class="uk-width-auto">
									<button type="submit" class="btn green-cancel">Apply Filter</button>
				  					<button type="button" id="btn-clear-filter" name="clear-filter" class="btn green-cancel">Clear Filter</button>
								</div>
							</div>	
						</div>
					</div>	
					</form>			
				</div>
			</div>

			<div class="portlet light bordered">
				<div class="portlet-body">
					<h4 class="portlet-heading master2-heading mb-0">File Summary</h4>
					<div class="uk-margin-top">
						<div class="re-table table-responsive">  
								<table id="report-file-upload" class="table table-responsive table-bordered table-striped dataTables_wrapper">
								
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>	




<style type="text/css">
.ui-widget {
    font-family: Trebuchet MS,Tahoma,Verdana,Arial,sans-serif;
    font-size: 13px;
}

#MyscheduleTabs {
	border: none;
}

.ui-widget-header {
     border: none!important; 
     background: #f6a828 url(images/ui-bg_gloss-wave_35_f6a828_500x100.png) 50% 50% repeat-x; 
    /* color: #ffffff; */
    /* font-weight: bold; */
}
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}

.value-rs {
	text-align: center;
}

.lbl-rs {
	text-align: right;
	width: 40%;
}
.progress-bar {
	background-color: #274e60;
}

.label-as-badge {
    border-radius: 1em;
}
.rd-label {
	font-size: 15px;
}
/* Latest compiled and minified CSS included as External Resource*/

/* Optional theme */

/*@import url('//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css');*/

.stepwizard-step p {
    margin-top: 0px;
    color:#666;
}
.stepwizard-row {
    display: table-row;
}
.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}
.stepwizard-step button[disabled] {
    /*opacity: 1 !important;
    filter: alpha(opacity=100) !important;*/
}
.stepwizard .btn.disabled, .stepwizard .btn[disabled], .stepwizard fieldset[disabled] .btn {
    opacity:1 !important;
    color:#bbb;
}
.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content:" ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-index: 0;
}
.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
.btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}

table .btn {
	margin: 3px 6px 3px 0;
}

#report-file-upload td p {
	margin: 0px;
}

td.updated-by p.lbl {
	font-size: 13px;
}

.reportStat{
	font-weight: bold;
}

.table-bordered > thead:first-child > tr:first-child > th{
	font-size: 15px;
}

a.editable{
	text-decoration: underline;
}

td.col-center {
	text-align: center;
}

.col-xl {
	width: 350px !important;
}
.col-lg {
	width: 200px !important;
}
.col-md {
	width: 150px !important;
}
.col-sm {
	width: 100px !important;
}
.id {
    white-space:nowrap !important;
}
.select {
	width: 8%;
}
</style>

<!-- Modal -->
<div class="bootbox modal fade" id="new-subcriber-list" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<form id="frm-new-subcriber-list">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				  <h4 class="modal-title">New Subscribers List</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<input type="text" class="form-control validate[required,custom[onlyLetterNumberSp]] txt-add-subcriber-list" name="subcriber-list">
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success btn-success-custom"> Save </button>
				&nbsp;
				<button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal">Cancel</button>
			</div>
			</form>
		</div>
	<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<!-- Modal -->
<div class="bootbox modal fade" id="show-file-history" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				 <h4 class="modal-title">File Update history</h4>
			</div>
			<div class="modal-body">
				<form name="file-update-history">
					<div class="row">
						<div class="col-sm-4 col-xs-12">
							<div class="form-group">
								<label>File Date</label>
								<input type="text" value="" data-type="date-single" data-control="start-date" name="file-date" class="form-control">
								<cfoutput>
									<input type="hidden" name="file-date-start" value="#DateFormat(NOW().add("d", -30), "yyyy-mm-dd")#" class="form-control" data-value="#DateFormat(NOW().add("d", -30), "mm/dd/yyyy")#">
									<input type="hidden" name="file-date-end" value="#DateFormat(NOW(), "yyyy-mm-dd")#" class="form-control" data-value="#DateFormat(NOW(), "mm/dd/yyyy")#">
								</cfoutput>
							</div>
						</div>
						<!--- <div class="col-sm-4 col-xs-12">
							<div class="form-group">
								<label>File Name</label>
								<select name="file-name" class="form-control">
								</select>
							</div>
						</div> --->
						<div class="col-sm-2 col-xs-12">
							<div class="form-group"> 
								<label class="hidden-xs">&nbsp;</label>
								<button type="button" name="view" class="btn btn-success form-control">View</button>
							</div>
						</div>
						<div class="col-sm-2 col-xs-12">
							<div class="form-group"> 
								<label class="hidden-xs">&nbsp;</label>
								<button type="button" name="clear" class="btn btn-primary form-control">Clear</button>
							</div>
						</div>
						<div class="col-sm-4 col-xs-12"></div>
					</div>
				</form>
				<div class="re-table">  
					<table id="show-file-history-ds" class="table table-responsive table-bordered table-striped">
						
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" value="" name="id">
				<button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal">Close</button>
			</div>
		</div>
	<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<!-- Modal -->
<div class="bootbox modal fade" id="update-contact" style="z-index: 10000" tabindex="-2" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				 <h4 class="modal-title">Update contact</h4>
			</div>
			<div class="modal-body">
				<div class="alert alert-danger hidden"></div>
				<div class="form-group">
					<label>Contact</label>
					<input type="text" class="form-control" name="contact">
				</div>
				<div class="form-group">
					<label>Timezone</label>
					<select class="form-control" name="contact-tz">
						<cfloop array="#tzList#" index="item">
							<option value="<cfoutput>#item.VALUE#</cfoutput>"><cfoutput>#item.NAME#</cfoutput></option>
						</cfloop>
					</select>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" value="" name="id">
				<input type="hidden" value="" name="fileID">
				<input type="hidden" value="" name="type">
				<button type="button" name="save" class="btn btn-primary btn-success-custom">Save</button>
				<button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal">Close</button>
			</div>
		</div>
	<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<!-- Modal -->
<div class="bootbox modal fade" id="show-contact" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				 <h4 class="modal-title">Contacts</h4>
			</div>
			<div class="modal-body">
				<div class="re-table">
					<table id="show-contact-ds" class="table table-responsive table-bordered table-striped">
						
					</table>
				</div>	
			</div>
			<div class="modal-footer">
				<input type="hidden" value="" name="id">
				<input type="hidden" value="" name="type">
				<input type="hidden" value='<cfoutput>#SerializeJSON(tzList)#</cfoutput>' name="tzlist">
				<button type="button" class="btn btn-primary btn-success-custom" name="deleteSelectedItem" data-delete-all="1">Delete all</button>
				<button type="button" class="btn btn-primary btn-success-custom" name="deleteSelectedItem" data-delete-all="0">Delete selected</button>
				<button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary btn-success-custom" data-dismiss="modal">Reload</button>
				<button type="button" class="btn btn-primary btn-success-custom visible btn-print">Print</button>
			</div>
		</div>
	<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<div class="bootbox modal fade" id="send-now-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				 <h4 class="modal-title">Send now</h4>
			</div>
			<div class="modal-body">
				<h4><span id="totalSent"></span> Sent / <span id="totalRemain"></span> Remain</h4>
				<div class="re-table">
					<table id="send-contact-table" class="table table-responsive table-bordered table-striped">
						
					</table>
				</div>	
			</div>

				<form class="form-horizontal" style="padding: 0 2% 0 2%">
					<div class="form-group">
						<label class="control-label col-sm-6" style="text-align: right"><b>Amount of phone number to send</b></label>
						<div class="col-sm-6">
							<input class="form-control" type="text" name="inpContactNumberToSend" id="inpContactNumberToSend">
						</div>
					</div>
				</form>

			<div class="modal-footer">
				<input type="hidden" value="" name="id">
				<button type="button" class="btn btn-medium btn-success-custom" id="btn-send-all-confirm">Send to all</button>
				<button type="button" class="btn btn-medium btn-success-custom" id="btn-send-confirm" name="">Send</button>
				<button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- Modal -->
<div class="bootbox modal fade" id="show-report" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h3 class=""><b>File Status Details</b></h3>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 long-text-overflow">
						<b>File date:</b> <span id="header_file_date"></span>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 long-text-overflow">
						<b>File name:</b> <span title="" id="header_file_name"></span>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 long-text-overflow for_batch_report">
						<b>Campaign:</b> <span title="" id="header_campaign_name"></span>
					</div>
				</div>
				<div class="row for_batch_report"><br/>
					<div class="col-xs-12 ol-md-4 col-lg-4 col-sm-4 long-text-overflow">
						Total In Contact Queue: <span id="reportContactQueue" class="hidden reportStat"></span>
					</div>
					<div class="col-xs-12 col-md-4 col-lg-4 col-sm-4 long-text-overflow">
						Total In IreResult: <span id="reportIreResult" class="hidden reportStat"></span>
					</div>
					<div class="col-xs-12 col-md-4 col-lg-4 col-sm-4 long-text-overflow">
						Total In Contact Result: <span id="reportContactResult" class="hidden  reportStat"></span>
					</div>
				</div>

				<div class="row for_batch_report">
					<div class="col-xs-12 col-md-4 col-lg-4 col-sm-4 long-text-overflow">
						Total Blocked: <span id="reportBlocked" class="hidden reportStat"></span>
					</div>
					<div class="col-xs-12 col-md-4 col-lg-4 col-sm-4 long-text-overflow">
						Total Duplicated In Queue : <span id="reportDuplicatedInQueue" class="hidden  reportStat"></span> 
					</div>
				</div>
				
				<div class="row"><br/>
					<div class="col-xs-12 col-md-4 col-lg-4 col-sm-4 long-text-overflow">
						Total Load Success: <span id="reportLoadSuccess" class="reportStat"></span> 
					</div>
					<div class="col-xs-12 col-md-4 col-lg-4 col-sm-4 long-text-overflow">
						Total Load Error: <span id="reportLoadError" class="reportStat"></span>
					</div>
					<div class="col-xs-12 col-md-4 col-lg-4 col-sm-4 long-text-overflow for_batch_report">
						Overall In Contact Queue : <span id="reportContactQueueOverall" class="hidden reportStat"></span>
					</div>
				</div>
				<br/>
				<div class="re-table table-responsive">
					<table id="show-report-ds" class="table table-responsive table-bordered table-striped" style="min-width: 800px">
						
					</table>
				</div>	
			</div>

			<div class="modal-footer">
			</div>
		</div>
	<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- Modal -->
<div class="bootbox modal fade" id="scheduler" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				 <h4 class="modal-title">Campaign Blast Scheduler</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<fieldset>
							<legend>Date Rang</legend>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label>Start date:</label>
										<input type="text" class="form-control date-picker" name="start-date">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>End date:</label>
										<input type="text" class="form-control date-picker" name="end-date">
									</div>
								</div>
							</div>
						</fieldset>
					</div>
					<div class="col-md-6">
						<fieldset>
							<legend>Time Rang</legend>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label>Start time:</label>
										<div class="input-group bootstrap-timepicker timepicker">
								            <input  type="text" class="form-control input-small time-picker" name="start-time">
								            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
								        </div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>End time:</label>
										<div class="input-group bootstrap-timepicker timepicker">
								            <input  type="text" class="form-control input-small time-picker" name="end-time">
								            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
								        </div>
									</div>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#advance-schedule">Advance</button>
						<div id="advance-schedule" class="collapse ">
						   	<div class="row">
						   		<div class="col-sm-6">
						   			<div class="well well-sm">
						   				<div class="row">
							   				<div class="col-xs-12">Sunday</div>
							   				<div class="col-xs-12">Monday</div>
							   				<div class="col-xs-12">Tuesday</div>
							   				<div class="col-xs-12">Webnesday</div>
							   				<div class="col-xs-12">Thursday</div>
							   				<div class="col-xs-12">Friday</div>
							   				<div class="col-xs-12">Saturday</div>	
						   				</div>
					   				</div>
						   		</div>
						   		<div class="col-sm-6">
						   			<div class="well well-sm">Small Well</div>
						   		</div>
						   	</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" value="" name="campaign-id">
				<button type="button" class="btn btn-primary btn-success-custom visible" >Save</button>
				<button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- Modal -->
<div class="modal fade" id="ScheduleOptionsModal" tabindex="-1" role="dialog" aria-labelledby="ScheduleOptionsModal" aria-hidden="true">
    <div class="modal-dialog" style="width:auto; max-width:900px;">
        <div class="modal-content">
        	<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>            
				<h4 class="modal-title title">Schedule Options</h4>
            </div>
            <cfset campaignid = 1 />
            <cfinclude template="dsp_advance_schedule.cfm">
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->





<cfparam name="variables._title" default="Admin - Import contact">
<cfinclude template="../views/layouts/master2.cfm">
