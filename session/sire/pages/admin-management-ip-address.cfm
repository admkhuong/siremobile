<cfscript>
    createObject("component", "public.sire.models.helpers.layout")
        .addJs("/public/js/jquery.tmpl.min.js", true)
        .addJs("/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js")
        .addCss("/public/js/jquery-ui-1.11.4.custom/jquery-ui.css", true)
        
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", false)

        .addCss("/session/sire/assets/global/plugins/uniform/css/uniform.default.css", true)
        .addJs('/session/sire/assets/global/scripts/filter-bar-generator.js', true)
        .addJs('/session/sire/assets/global/plugins/uniform/jquery.uniform.min.js', true)        

        .addJs("/public/sire/js/select2.min.js", true)
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)

        .addJs("/session/sire/assets/pages/scripts/admin_management_ip_address.js");        
</cfscript>


<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfparam name="isDialog" default="0">
<cfparam name="isUpdateSuccess" default="">

<div class="portlet light bordered">
    <!--- Filter bar --->
    <div class="row">
        <div class="col-sm-3 col-md-4">
            <h2 class="page-title">Admin Management IP Address</h2>
        </div>
        <div class="col-sm-9 col-md-8">
            <div id="box-filter" class="form-gd">
            </div>
        </div>       
    </div>   
    <!--- Listing --->
    <div class="row">
        <div class="col-md-12">
            <div class="re-table">
                <div class="table-responsive">
                    <table id="tblListEMS" class="table-striped dataTables_wrapper">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!--- View details IP Address --->
<div class="modal" id="modal-ip-details-sections" role="dialog">
    <div class="modal-dialog modal-details">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">IP Address Details</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                          <cfoutput>
                            <div class="update_cardholder_info">
                                <ul class="nav list-newinfo">
                                    <li>IP: <label id="txtIP"></label></li>
                                    <li>Country Code: <label id="txtCountryCode"></label></li>
                                    <li>Country Name: <label id="txtCountryName"></label></li>
                                    <li>Region Code: <label id="txtRegionCode"></label></li>
                                    <li>Region Name: <label id="txtRegionName"></label></li>
                                    <li>City: <label id="txtCity"></label></li>
                                    <li>Zip Code: <label id="txtZipCode"></label></li>
                                    <li>Time Zone: <label id="txtTimeZone"></label></li>                                    
                                    <li>Latitude: <label id="txtLatitude"></label></li>                                    
                                    <li>Longitude: <label id="txtLongitude"></label></li>                                    
                                    <li>Metro Code: <label id="txtMetroCode"></label></li>    
                                </ul>
                            </div>                                                          
                        </cfoutput> 
                        </div>
                    </div>                  
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>
<!--- End details IP Address --->
<!--- Top10 IP Address --->
<div class="modal fade" id="modal-top10-ip-sections" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">Top 10 IP Address</strong></h4>
            </div>
            <div class="modal-body">
                 <div class="row">
                            <div class="col-md-12">
                                <div class="re-table">
                                    <div class="table-responsive">
                                        <table id="tbTop10IPAddress" class="table-striped dataTables_wrapper">
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>     
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>
<!--- End Top10 IP Address --->


<cfparam name="variables._title" default="Admin Management IP Address">
<cfinclude template="../views/layouts/master.cfm">

