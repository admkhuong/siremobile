<cfscript>
    createObject("component", "public.sire.models.helpers.layout")
        .addJs("/public/js/jquery.tmpl.min.js", true)
        .addJs("/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js")
        .addCss("/public/js/jquery-ui-1.11.4.custom/jquery-ui.css", true)
        
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", false)

        .addCss("/session/sire/assets/global/plugins/uniform/css/uniform.default.css", true)
        .addJs('/session/sire/assets/global/scripts/filter-bar-generator.js', true)
        .addJs('/session/sire/assets/global/plugins/uniform/jquery.uniform.min.js', true)        

        .addJs("/public/sire/js/select2.min.js", true)
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)

        .addJs("/session/sire/assets/pages/scripts/admin_management_user_cc.js");        
</cfscript>


<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfparam name="isDialog" default="0">
<cfparam name="isUpdateSuccess" default="">

<div class="portlet light bordered">
    <!--- Filter bar --->
    <div class="row">
        <div class="col-sm-3 col-md-4">
            <h2 class="page-title">Admin Management User CC</h2>
        </div>
        <div class="col-sm-9 col-md-8">
            <div id="box-filter" class="form-gd">
            </div>
        </div>       
    </div>
    <div class="form-action">
        <a href="/session/sire/pages/admin-add-new-user-cc" class="btn green-gd mr-15 btn-add-new-user-cc">Add New CC</a>          
    </div>
    <br>    
    <!--- Listing --->
    <div class="row">
        <div class="col-md-12">
            <div class="re-table">
                <div class="table-responsive">
                    <table id="tblListEMS" class="table-striped dataTables_wrapper">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>




<cfset RetCustomerInfo = ''>

<cfparam name="expirationDate" default="">
<cfparam name="firstName" default="">
<cfparam name="lastName" default="">
<cfparam name="line1" default="">
<cfparam name="city" default="">
<cfparam name="state" default="">
<cfparam name="zip" default="">
<cfparam name="country" default="US">
<cfparam name="phone" default="">
<cfparam name="emailAddress" default="">

<!--- Change CC info before charge --->
<div id="buyaddon-modal-sections" class="uk-new-modal uk-modal modal-cardholder" uk-modal>
    <div class="uk-modal-dialog addon-modal-dialog">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <form id="addon-form">        
            <input type="hidden" name="authorizedUserId" id="authorizedUserId" value="">
            <input type="hidden" name="selected-user-id" id="selected-user-id" value=""/>   
            <input type="hidden" name="action" id="action" value="update-cc"/>   
            <input type="hidden" name="paymentGateway" id="paymentGateway" value="">
            <div class="uk-modal-body form-gd">
                <div class="inner-update-cardholder">
                    <h4 class="uk-new-modal-title">Admin Update User CC</h4>
                        <cfoutput>
                            <div class="update_cardholder_info">
                                <ul class="nav list-newinfo">
                                    <li id='vl_maskedNumber'>Card Number: <label id="txtMaskedNumber"></label></li>
                                    <li id='vl_expirationDate'>Expiration Date: <label id="txtExpirationDate"></label></li>
                                    <li id='vl_fullname'>Cardholder Name: <label id="txtCardholderName"></label></li>
                                    <li id='vl_line1'>Address: <label id="txtAddress"></label></li>
                                    <li id='vl_city'>City: <label id="txtCity"></label></li>
                                    <li id='vl_state'>State: <label id="txtState"></label></li>
                                    <li id='vl_zip'>Zip code: <label id="txtZipCode"></label></li>
                                    <li id='vl_country'>Country: <label id="txtCountry"></label></li>
                                    <li id='vl_emailAddress'>Email: <label id="txtEmail"></label></li>
                                </ul>
                                <div class="form-group select-gateway-row">                                    
                                    <div class="radio-inline" style="padding-left: 22px !important;">
                                        <label>
                                            <input type="radio" name="select_gw" class="select_gw" value="1">
                                            Worldpay
                                        </label>
                                    </div>
                                    <div class="radio-inline" style="padding-left: 22px !important;">
                                        <label>
                                            <input type="radio" name="select_gw" class="select_gw" value="2">
                                            Paypal
                                        </label>
                                    </div>
                                    <div class="radio-inline" style="padding-left: 22px !important;">
                                        <label>
                                            <input type="radio" name="select_gw" class="select_gw"  value="3">
                                            Mojo
                                        </label>
                                    </div>
                                    <div class="radio-inline" style="padding-left: 22px !important;">
                                        <label>
                                            <input type="radio" name="select_gw" class="select_gw"  value="4">
                                            Total Apps
                                        </label>
                                    </div>
                                </div>
                                <div class="row row-small amount-row">
                                    <div class="col-lg-8 col-md-6">
                                        <div class="form-group">
                                            <label>Amount <span>*</span></label>
                                            <input type="text" class="form-control validate[required,min[1],custom[number]]" maxlength="10" id="amount" name="amount"  value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group select-user-card-row">                                    
                                    <div class="radio-inline" id="rad-currentcard" style="padding-left: 22px !important;">
                                        <label>
                                            <input type="radio" name="select_used_card" class="select_used_card" id="optionsRadios1" value="1">
                                            Current Card
                                        </label>
                                    </div>
                                    <div class="radio-inline" id="rad-newcard" style="padding-left: 22px !important;">
                                        <label>
                                            <input type="radio" name="select_used_card" class="select_used_card" id="optionsRadios2" value="2" checked>
                                            New Card
                                        </label>
                                    </div>
                                </div>

                                <div class="fset_cardholder_info" >                                
                                <div class="form-group">
                                    <div class="card-support-type">
                                        <img class="img-responsive" src="../assets/layouts/layout4/img/supported-payment-types.png" alt="">
                                    </div>
                                </div>

                                <div class="row row-small">
                                    <div class="col-lg-8 col-md-6">
                                        <div class="form-group">
                                            <label>Card Number <span>*</span></label>
                                            <input type="text" class="form-control validate[required,custom[creditCardFunc]] card_number" maxlength="32" id="card_number" name="number" value='' placeholder="---- ---- ---- 1234">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label>Security Code</label>
                                            <input type="text" class="form-control validate[custom[onlyNumber]] security_code" maxlength="4" id="security_code" name="cvv" data-errormessage-custom-error="* Invalid security code">
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-small">
                                    <div class="col-lg-8 col-md-6">
                                        <div class="form-group">
                                            <label>Expiration Date <span>*</span></label>
                                            <input type="hidden" id="expiration_date_upgrade" name="expirationDate" value="">
                                            <select class="form-control validate[required] expiration_date_month expiration_date_month_upgrade" id="expiration_date_month_upgrade" class="expiration_date_month">
                                                <option value="" disabled selected>Month</option>
                                                <option value="01">January</option>
                                                <option value="02">February</option>
                                                <option value="03">March</option>
                                                <option value="04">April</option>
                                                <option value="05">May</option>
                                                <option value="06">June</option>
                                                <option value="07">July</option>
                                                <option value="08">August</option>
                                                <option value="09">September</option>
                                                <option value="10">October</option>
                                                <option value="11">November</option>
                                                <option value="12">December</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <select class="form-control validate[required]" id="expiration_date_year_upgrade" class="expiration_date_year">
                                                <option value="" disabled selected>Year</option>
                                                <cfloop from="#year(now())#" to="#year(now())+50#" index="i">
                                                    <option value="#i#">#i#</option>
                                                </cfloop>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row row-small">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>Cardholder Name <span>*</span></label>
                                            <input type="text" class="form-control validate[required,custom[onlyLetterNumberSp]] first_name" maxlength="50" id="first_name" name="firstName" placeholder="First Name" value="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <input type="text" class="form-control validate[required,custom[onlyLetterNumberSp]] last_name" maxlength="50" id="last_name" name="lastName" placeholder="Last Name" value="">
                                        </div>
                                    </div>
                                </div>
                               <!---  <div class="form-group">
                                    <label>Address:<span>*</span></label>
                                    <input type="text" class="form-control validate[required,custom[noHTML]] cardholder line1" maxlength="60" id="line1" name="line1" data-value='#line1#'>
                                </div>
                                <div class="form-group">
                                    <label>City:<span>*</span></label>
                                    <input type="text" class="form-control validate[required,custom[noHTML]] cardholder city" maxlength="40" id="city" name="city" data-value='#city#'>
                                </div> --->
                                 <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Address:<span>*</span></label>
                                                     <input type="text" class="form-control validate[required,custom[noHTML], custom[validateAddrAndCity]] cardholder line1" maxlength="60" id="line1" name="line1" data-value='#line1#'>
                                                </div>
                                            </div>                                         
                                </div>
                                 <div class="row">                          
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>City:<span>*</span></label>
                                                    <input type="text" class="form-control validate[required,custom[noHTML], custom[validateAddrAndCity]] cardholder city" maxlength="40" id="city" name="city" data-value='#city#'>
                                                </div>
                                            </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>State:<span>*</span></label>
                                            <input type="text" class="form-control validate[required,custom[noHTML], custom[onlyLetterNumberSp]] cardholder state" maxlength="40" id="state" name="state" data-value="#state#">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Zip Code:<span>*</span></label>
                                            <input type="text" class="form-control validate[required,custom[onlyNumber]] cardholder zip" maxlength="5" id="zip" name="zip" data-value="#zip#" data-errormessage-custom-error="* Invalid zip code">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Country:<span>*</span></label>
                                    <select class="form-control cardholder validate[required] country" id="country" name="country" data-value="#country#" >
                                        <cfinclude template="../views/commons/payment/country.cfm">
                                    </select>
                                </div>                               
                                <div class="form-group">
                                    <label>Email:</label>
                                    <input type="text" class="form-control validate[custom[email]] cardholder email" maxlength="128" id="email" name="email" data-value="#emailAddress#">
                                </div>
                                <div class="form-group save-cc-row">
                                    <label>
                                        <input type="checkbox" id="save_cc_info" name="save_cc_info"> 
                                        Save card information
                                    </label>
                                </div>                             
                                <hr>
                            </div>
                        </cfoutput>
                    
                    </div>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right buyaddon-footer-modal">
                <button type="submit" class="btn newbtn green-gd btn-update-user-cc">Save</button>
                <button type="button" class="btn newbtn1 green-cancel uk-modal-close btn-cancel-cc">Cancel</button>
            </div>
        </form>
    </div>
</div>

<div class="modal fade in" id="mdTransaction" tabindex="-1" role="dialog" aria-hidden="true">    
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <b>
                    <h4 class="modal-title" id="modal-tit">Transaction List</h4>
                </b>
            </div>
            <div class="modal-body">
                <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="re-table">
                        <div class="table-responsive">
                            <table id="transactions-list" class="table table-striped table-bordered table-hover">
                                
                            </table>                            
                        </div>
                    </div>	
                </div>                        
                </div> 
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>    
</div>

<div id="modal-full-cc-sections" class="uk-new-modal uk-modal modal-cardholder" uk-modal>
    <div class="uk-modal-dialog addon-modal-dialog">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <form id="addon-form">        
            <div class="uk-modal-body form-gd">
                <div class="inner-update-cardholder">
                    <h4 class="uk-new-modal-title">Full User CC Information</h4>
                        <cfoutput>
                            <div class="update_cardholder_info">
                                <ul class="nav list-newinfo">
                                    <li id='vl_maskedNumber'>Card Number: <label id="txtMaskedNumber1"></label></li>
                                    <li id='vl_cardcvv'>Card CVV: <label id="txtCardCVV1"></label></li>
                                    <li id='vl_expirationDate'>Expiration Date: <label id="txtExpirationDate1"></label></li>
                                    <li id='vl_fullname'>Cardholder Name: <label id="txtCardholderName1"></label></li>
                                    <li id='vl_line1'>Address: <label id="txtAddress1"></label></li>
                                    <li id='vl_city'>City: <label id="txtCity1"></label></li>
                                    <li id='vl_state'>State: <label id="txtState1"></label></li>
                                    <li id='vl_zip'>Zip code: <label id="txtZipCode1"></label></li>
                                    <li id='vl_country'>Country: <label id="txtCountry1"></label></li>
                                    <li id='vl_emailAddress'>Email: <label id="txtEmail1"></label></li>
                                    <li id='vl_zip'>Phone: <label id="txtPhone1"></label></li>                 
                                </ul>
                            </div>
                        </cfoutput>
                    
                    </div>
                </div>
            </div>        
        </form>
    </div>
</div>

<!--- End change CC info before charge --->
<cfparam name="variables._title" default="Admin Management User CC">
<cfinclude template="../views/layouts/master.cfm">

