<cfabort>
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />
<cfinvoke method="GetAllMasterCouponPlan" component="session.sire.models.cfc.plan" returnvariable="GetAllMasterCouponPlan"></cfinvoke>

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("../assets/global/plugins/daterangepicker/daterangepicker.css", true)        
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)
        .addJs("../assets/global/plugins/daterangepicker/moment.min.js", true)
        .addJs("../assets/global/plugins/daterangepicker/daterangepicker.js", true)
        .addJs('/session/sire/assets/global/scripts/filter-bar-generator.js', true)  
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)      
        
        .addJs("/public/sire/js/select2.min.js")
        
        .addJs("../assets/pages/scripts/admin-master-user-coupon.js");
</cfscript>


<cfoutput>
<div class="portlet light bordered">
    <div class="portlet-body form">          
        <div class="row">
            <div class="col-sm-6">
                <h2 class="page-title">Master user coupon assignment</h2>
            </div>
            <div class="col-sm-6">                
                <div class="pull-right">
                    <h2 class="page-title">                        
                        <button type="button" class="btn green-gd" data-toggle="modal" data-target="##mdNewCoupon">NEW COUPON</button>
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="portlet light bordered">
        <div class="portlet-body form">  
            
            <div class="row">
                <div class="col-sm-3 col-md-4">
                    <h2 class="page-title">Coupon List</h2>
                </div>
                <div class="col-sm-9 col-md-8">
                    <div id="box-filter" class="form-gd">

                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class="re-table table-data">
                        <div class="table-responsive">
                            <table id="coupon-list" class="table table-striped table-responsive">
                
                            </table>
                        </div>
                    </div>                
                </div>
            </div>                   
            
        </div>
    </div>
</div>
<div class="modal fade" id="mdNewCoupon" role="dialog">
    <form name="frNewCoupon" class="col-sm-12" id="frNewCoupon">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <b><h4 class="modal-title" id="modal-tit">
                NEW COUPON
                </h4></b>	
            </div>
            <div class="modal-body">	
                <div class="modal-body-inner"> 
                    <div class="form-group">
                        <label>Coupon code</label>
                        <input type="text" class="form-control validate[required]" id="couponCode" value="">                        
                    </div>
                    <div class="form-group">
                        <label>Master User</label>                        
                        <select name="masterUser" class="form-control select2 validate[required]" id="masterUser">
				        </select>                    
                    </div>
                    <div class="form-group">
                        <label>Plan</label>
                        <select id="plan" name="plan" class="form-control select2 validate[required]"  tabindex="7">							                            
							<cfloop query="GetAllMasterCouponPlan.MASTERCOUPONPLANLIST">
                                <option value="#PlanId_int#" >#PlanName_vch#</option> 
                            </cfloop>
						</select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green-cancel" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn green-gd" id="btnUpdateMessage">Save</button>
            </div>
        </div>
    </div>
    </form>
</div>
</cfoutput>

<cfparam name="variables._title" default="Master user coupon assignment">
<cfinclude template="../views/layouts/master.cfm">

