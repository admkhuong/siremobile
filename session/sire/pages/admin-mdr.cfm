<!--- Messaging Detail Records --->


<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<!--- Load Setting Defaults --->
<cfset DBSourceManagementRead = "Bishop" />

<cfset ReportUserId = "522" />
<cfset inpStart = "#dateformat('2016-04-01','yyyy-mm-dd')#" & " " & "00:00:00">
<cfset inpEnd = "#dateformat('2016-04-30','yyyy-mm-dd')#" & " " & "23:59:59">
    
<!--- Get details from simplexresults.ireresults --->
<cfquery name="GetMDRDetails" datasource="#DBSourceManagementRead#">
    
    SELECT 
	    IREResultsId_bi AS ID,
    	BatchId_bi,
    	UserId_int,
    	CPId_int,
    	QID_int,
    	IREType_int,
    	Created_dt,
    	ContactString_vch,
    	Response_vch,
    	ShortCode_vch
    FROM 
    	simplexresults.ireresults
	WHERE
		UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ReportUserId#"> 
	AND	
		Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
    AND
       	Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">	
   	AND
		LEFT(ContactString_vch, 9) <> '900000001'	<!--- Remove demo stuff  --->		
	ORDER BY IREResultsId_bi DESC		
                                      
</cfquery>   


 <cfinvoke method="QueryToCSV" component="session.sire.models.cfc.admin" returnvariable="RetVarQueryToCSV">
    <cfinvokeargument name="Query" value="#GetMDRDetails#">
    <cfinvokeargument name="CreateHeaderRow" value="true">
    <cfinvokeargument name="Delimiter" value=",">
</cfinvoke>



<cfcontent type="application/msexcel">
<cfheader 
    name="Content-Disposition" 
    value="filename=MDR_#ReportUserId#_At_#DateFormat(inpStart, 'yyyy_mm_dd')#_#LSTimeFormat(inpEnd,'hh_mm_ss')#.csv" 
    charset="utf-8">  
    
<cfoutput>#RetVarQueryToCSV#</cfoutput>
