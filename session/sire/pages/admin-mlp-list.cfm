<cfinclude template="../../sire/configs/paths.cfm">

<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />



<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("/session/sire/css/mlp.css")
    	.addJs("/session/sire/js/site/admin_mlp_list.js");
</cfscript>



<div class="portlet light bordered subscriber">
    <div class="portlet-body form">
    	 <h2 class="page-title">Create template from MLP</h2>
        <div class="row">
        	<div class="col-xs-12">
		        <div class="row">
		        	<div class="col-lg-12 col-md-12">
				        <div class="subscriber-list">
	        	        	<div class="table-responsive re-table">
	        	                <table id="tblListEMS" class="table-responsive table-striped dataTables_wrapper dataTable">
	        	                    
	        	                </table>
	        	            </div>
				        </div>
		        	</div>
		        </div>
        	</div>
        </div>
    </div>
</div>


<cfparam name="variables._title" default="Admin MLP List - Sire">
<cfinclude template="../views/layouts/master.cfm">


