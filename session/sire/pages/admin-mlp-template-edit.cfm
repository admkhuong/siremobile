<cfparam name="ccpxDataId" default="0">

<cfparam name="action" default="">

<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />


<cfif action EQ 'edit'>
	<cfinvoke component="session.sire.models.cfc.mlp" method="ReadCPPTemplate" returnvariable="RetCPPXData">
		 <cfinvokeargument name="ccpxDataId" value="#ccpxDataId#">
	</cfinvoke> 
<cfelse>
	<cfinvoke component="session.sire.models.cfc.mlp" method="AdminReadCPP" returnvariable="RetCPPXData">
		 <cfinvokeargument name="ccpxDataId" value="#ccpxDataId#">
	</cfinvoke>
</cfif>



<cfset xmlControl ={}/>
<cfset termOfService = ''>
<cfset checkMultiSubList = 0>


<cfif RetCPPXData.RXRESULTCODE GT 0>

	<cfinvoke component="session.sire.models.cfc.mlp" method="ParseCPPXXmlData" returnvariable="xmlControl">
		<cfinvokeargument name="xmlData" value="#RetCPPXData.DATA#">
	</cfinvoke> 	


	<cfloop array="#xmlControl#" index="indexXml">
		<cfif structKeyExists(indexXml, "term-of-service")>
			<cfset termOfService = indexXml['term-of-service']>
		</cfif>
		<cfif structKeyExists(indexXml, "multi-sub-list")>
			<cfset checkMultiSubList = indexXml['multi-sub-list']>
		</cfif>	
	</cfloop>
	
</cfif>

<cfset objectList = {}>
<cfset objectList['HTML']  = {'LABEL':'Text Object','CONTENT':'<textarea class="normal textHtml cpp-object" name="text-content">%VALUE%</textarea>','OBJTYPE':'text-content'}>
<cfset objectList['SMS']   = {'LABEL':'SMS Number Object','CONTENT':'<input type="text" class="form-control sms-number cpp-object" name="sms-number" value="%VALUE%" placeholder="SMS Number" maxlength="255">','OBJTYPE':'sms-number'}>
<cfset objectList['EMAIL'] = {'LABEL':'Email address Object','CONTENT':'<input type="text" class="form-control email-address cpp-object" name="email-address" value="%VALUE%" placeholder="Email Address" maxlength="255">','OBJTYPE':'email-address'}>
<cfset objectList['VOICE'] = {'LABEL':'Voice Number Object','CONTENT':'<input type="text" class="form-control voice-number cpp-object" name="voice-number" value="%VALUE%" placeholder="Voice Number" maxlength="255">','OBJTYPE':'voice-number'}>
<cfset objectList['SUBCRIBERLIST'] = {'LABEL':'Subscriber List Object','CONTENT':'<div class="cpp-object subcriber-list-group"><div class="row"><div class="col-sm-6"><input type="text" name="subcriber-name" class="form-control subcriber-name" value="%VALUE%" placeholder="Subcriber Name" maxlength="255"></div><div class="col-sm-6"><select class="form-control subcriber-list" name="subcriber-list" data-pre_val="%LISTID%"></select></div></div></div>
','OBJTYPE':'subcriber-name'}>

<main class="container-fluid page my-plan-page">
	<cfinclude template="../views/commons/credits_available.cfm">
		<section class="row bg-white">
			<div class="content-header">
				<div class="row">
					<div class="col-sm-5 content-title">
                    	<cfinclude template="../views/commons/welcome.cfm">
                	</div>
				</div>	
			</div>
			<!--- END : content-header --->
			<hr class="hrt0">
			<div class="content-body">
			<div class="container-fluid">
                <div class="row heading">
                	<div class="col-sm-12 heading-title">MLP Template<hr></div>
                </div>

				

                <div class="row">
                	<form id="edit_cpp_frm"  autocomplete="off">
                		<cfoutput>
                		<div class="col-md-6 form-horizontal">	
							 <div class="form-group">
	                            <label class="control-label col-md-3">Template Name:<span class="text-danger">*</span></label>
	                            <div class="col-md-7">
	                               <input type="text" class="form-control validate[required,custom[noHTML]]" id="templateName" name="templateName" maxlength="" value="#RetCPPXData.TITLE#" data-prompt-position="topLeft:100">
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="control-label col-md-3">Description</label>
	                            <div class="col-md-7">
	                               <textarea class="form-control custom-message" id="Description" name="Description" maxlength="160" data-prompt-position="topLeft:150" rows="3">#RetCPPXData.DESCRIPTION#</textarea>
	                            </div>
	                        </div>
                        </div>
                        
                        <!--- Display term of service --->
						 <div class="objectItem clearfix objectItemTerm">
							<div class="col-md-6">
								<div class="checkbox">
								    <label>
								      <input type="checkbox" name='checkTerms' id="checkTerms" value='1' <cfif termOfService NEQ ''> checked</cfif> >Require customers need to accept your terms of service ?
								    </label>
								</div>
							</div>	
							<div class="col-md-6">
			                	<div class="form-group wrapper-terms_of_service" style=<cfif termOfService EQ ''>"display:none"</cfif>>
				                	<textarea name="term-of-service" id="term-of-service" class="normal cpp-object terms_of_service">#termOfService#</textarea>
				                </div>	
							</div>
		                </div>	
                        
                        <br>
                        <div class="col-md-12">
		                	<hr class="">
		                </div>
		                <ul class="col-lg-10 col-lg-offset-1 sortable" id="listObj">
		                	<cfif action EQ 'Create'>
			                	<li class="groupObjectItem" id="obj_1">
				                	<div class="btn-group pull-right">
					               		<button type="button" class="btn btn-medium btn-success-custom btn-modal-add-obj pull-left">Add New Object</button>	
					               	</div>	
					            </li>
					        </cfif>
					        
					        <cfset num = 1>
					        <cfset num_sub_list = 0>       	
				            <cfloop array="#xmlControl#" index="index">
				            	<cfset htmlId = "obj_"&NOW().getTime()&"_"&num>
				            	<cfif structKeyExists(index, "text-content")>
				            		<cfset content = replace( objectList['HTML']['CONTENT'],"%VALUE%", index['text-content'])>
				            		<li class="groupObjectItem" id="#htmlId#">
					               		<div class="btn-group pull-right">
					               			<button type="button" class="btn btn-medium btn-success-custom btn-modal-add-obj pull-left">Add New Object</button>	
					               		</div>	
					               		<div class="clearfix"></div>
					               		<div class="objectItem clearfix">
					               			<div class="col-sm-12">
			    								<label class="control-point-label"> #objectList['HTML']['LABEL']#  </label><a data-object-type="text-content" class="obj-cpp-preview">Preview</a>
			    								<button type="button" class="btn btn-link pull-right cpp-object-btn-delete">
			    									<img src="/session/sire/images/close.png">
												</button>
			    							</div>
			    							<div class="col-sm-12 content-area">
			    								#content#
			    							</div>	
					               		</div>
					               	</li>
				            	</cfif>

				            	<cfif structKeyExists(index, "sms-number")>
				            		<cfset content = replace( objectList['SMS']['CONTENT'],"%VALUE%", index['sms-number'])>
				            		<li class="groupObjectItem" id="#htmlId#">
					               		<div class="btn-group pull-right">
					               			<button type="button" class="btn btn-medium btn-success-custom btn-modal-add-obj pull-left">Add New Object</button>	
					               		</div>	
					               		<div class="clearfix"></div>
					               		<div class="objectItem clearfix">
					               			<div class="col-sm-12">
			    								<label class="control-point-label"> #objectList['SMS']['LABEL']#  </label><a data-object-type="sms-number" class="obj-cpp-preview">Preview</a>
			    								<button type="button" class="btn btn-link pull-right cpp-object-btn-delete">
			    									<img src="/session/sire/images/close.png">
												</button>
			    							</div>
			    							<div class="col-sm-12 content-area">
			    								#content#
			    							</div>	
					               		</div>
					               	</li>
				            	</cfif>

				            	<cfif structKeyExists(index, "email-address")>
				            		<cfset content = replace( objectList['EMAIL']['CONTENT'],"%VALUE%", index['email-address'])>
				            		<li class="groupObjectItem" id="#htmlId#">
					               		<div class="btn-group pull-right">
					               			<button type="button" class="btn btn-medium btn-success-custom btn-modal-add-obj pull-left">Add New Object</button>	
					               		</div>	
					               		<div class="clearfix"></div>
					               		<div class="objectItem clearfix">
					               			<div class="col-sm-12">
			    								<label class="control-point-label"> #objectList['EMAIL']['LABEL']#  </label><a data-object-type="email-address" class="obj-cpp-preview">Preview</a>
			    								<button type="button" class="btn btn-link pull-right cpp-object-btn-delete">
			    									<img src="/session/sire/images/close.png">
												</button>
			    							</div>
			    							<div class="col-sm-12 content-area">
			    								#content#
			    							</div>	
					               		</div>
					               	</li>
				            	</cfif>

				            	<cfif structKeyExists(index, "voice-number")>
				            		<cfset content = replace( objectList['VOICE']['CONTENT'],"%VALUE%", index['voice-number'])>
				            		<li class="groupObjectItem" id="#htmlId#">
					               		<div class="btn-group pull-right">
					               			<button type="button" class="btn btn-medium btn-success-custom btn-modal-add-obj pull-left">Add New Object</button>	
					               		</div>	
					               		<div class="clearfix"></div>
					               		<div class="objectItem clearfix">
					               			<div class="col-sm-12">
			    								<label class="control-point-label"> #objectList['VOICE']['LABEL']#  </label><a data-object-type="voice-number" class="obj-cpp-preview">Preview</a>
			    								<button type="button" class="btn btn-link pull-right cpp-object-btn-delete">
			    									<img src="/session/sire/images/close.png">
												</button>
			    							</div>
			    							<div class="col-sm-12 content-area">
			    								#content#
			    							</div>	
					               		</div>
					               	</li>
				            	</cfif>

				          		<cfif structKeyExists(index, "subcriber-list-group")>
				          			<cfset num_sub_list ++ >
				            		<cfset content = replace( objectList['SUBCRIBERLIST']['CONTENT'],"%VALUE%", index['subcriber-list-group']['NAME'])>
				            		<cfset content  = replace( content,"%LISTID%", index['subcriber-list-group']['LISTID'])>
				            		<li class="groupObjectItem" id="#htmlId#">
					               		<div class="btn-group pull-right">
					               			<button type="button" class="btn btn-medium btn-success-custom btn-modal-add-obj pull-left">Add New Object</button>	
					               		</div>	
					               		<div class="clearfix"></div>
					               		<div class="objectItem clearfix">
					               			<div class="col-sm-12">
			    								<label class="control-point-label"> Subscriber List Object  </label><a data-object-type="subcriber-name" class="obj-cpp-preview">Preview</a>
			    								<button type="button" class="btn btn-link pull-right cpp-object-btn-delete" data-control-point-id="1" data-control-point-type="SHORTANSWER" data-control-point-af="NOFORMAT" data-control-point-oig="0" data-control-point-scdf="" data-control-point-one-selection-no-branch="0">
			    									<img src="/session/sire/images/close.png">
												</button>
			    							</div>
			    							<div class="col-sm-12 content-area">
			    								#content#
			    							</div>	
					               		</div>
					               	</li>
				            	</cfif>

				            	<cfset num ++>
				            </cfloop>

			                	<li class="groupObjectItem" id="obj_1">
				                	<div class="btn-group pull-right">
					               		<button type="button" class="btn btn-medium btn-success-custom btn-modal-add-obj pull-left">Add New Object</button>	
					               	</div>
					               	<div class="clearfix"></div>	
					            </li>
					      
			               	<li class="groupObjectItem checkMultiSubList"  style="<cfif num_sub_list LT 2> display:none </cfif>">
				           		<div class="checkbox">
								    <label>
								      <input type="checkbox" class="cpp-object multi_subcriber_list" name="multi_subcriber_list" id="multi_subcriber_list" <cfif checkMultiSubList EQ '1'> checked</cfif> > Enable multiple subcriberlist selection.
								    </label>
								</div>
					        </li>
		               </ul>
		                <div class="clearfix"></div>

		                <div class="col-md-12">
		                	<hr class="hrt0">
		                </div>
		                
		                
			        	<div class="form-footer text-center pull-right">
			        		<button type="submit" class="btn btn-success-custom btn-save-cpp pull-left">Save</button>
			        		<a href="/session/sire/pages/admin-mlp-template-preview-page?ccpxDataId=#ccpxDataId#" class="btn btn-success-custom btn-preview-cpp-template pull-left" target="_blank">Preview</a>
			        		<a href="/session/sire/pages/admin-mlp-template" type="button" class="btn btn-success-custom btn-create-cpp pull-left">Back to list</a>
			        		<!---
			            	<a type="button" class="btn btn-medium btn-back-custom pull-left"  href="javascript:history.go(-1)">Back</a>
			            	--->
			        	</div>
			        </cfoutput>	
	                </form>		
	            </div>    
            </div>
            </div>    
            <!--- END CONTENT BODY --->
		</section>	
</main>


<!-- Modal -->
<div class="modal fade  bootbox" id="addObjectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add New Object</h4>
      </div>
      <div class="modal-body text-center">
      		<input type="hidden" id="current_obj_id" value="">
			<select class="form-control" id="select_object">
				<option value='HTML'>Text Object</option>
				<option value="SMS">SMS Number Object</option>
				<option value="VOICE">Voice Number Object</option>
				<option value="EMAIL">Email Address Object</option>
				<option value="SUBCRIBERLIST">Subscriber List Object</option>
			</select>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-medium btn-success-custom" id="btn-add-object" data-dismiss="modal">Add</button>
        <button type="button" class="btn btn-medium btn-back-custom" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!--- MODAL POPUP --->
<div class="bootbox modal fade" id="previewObject" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel">Preview</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <!--- <button type="button" class="btn btn-primary btn-success-custom" id="btn-rename-campaign">Save</button> --->
        <button type="button" class="btn btn-default btn-back-custom" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/js/tinymce_4.3.8/tinymce.min.js">
</cfinvoke>


<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/js/jquery-ui-1.11.4.custom/jquery-ui.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/public/js/jquery-ui-1.11.4.custom/jquery-ui.theme.css">
</cfinvoke>
<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/public/sire/css/font-awesome.min.css">
</cfinvoke>
<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/mlp.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/ajaxupload.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/site/admin_mlp_template_edit.js">
</cfinvoke>

<cfparam name="variables._title" default="Adminc MLP Template - Sire">

<script type="text/javascript">
	var objectList = <cfoutput>#serializeJSON(objectList)#</cfoutput>
	var ccpxDataId = <cfoutput>#ccpxDataId#</cfoutput>
</script>

<cfinclude template="../views/layouts/main.cfm">