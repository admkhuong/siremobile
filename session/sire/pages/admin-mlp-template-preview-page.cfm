<cfparam name="ccpxDataId" default="">

<cfparam name="variables._title" default="Marketing Landing Pages - Sire">

<div class="portlet light bordered">
	<div class="row">
		<div class="col-xs-12">
	        <h4 class="form-heading">Administration - Preview a MLP Template</h4>
		</div>  
    </div>
    <hr class="hrt0">
    <div class="row">
        <div class="col-xs-12">
			<div class="content-body">
                <div id="resizable" class="ui-widget-content" style="width:100%">
                	<iframe src="/mlp-x/lz/index.cfm?inpURL=<cfoutput>#ccpxDataId#</cfoutput>&action=template" scrolling="auto" width="100%" height="100%"></iframe>
	            </div>    
	        </div>    
		</div>
    </div>
</div>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/js/jquery-ui-1.11.4.custom/jquery-ui.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/public/js/jquery-ui-1.11.4.custom/jquery-ui.theme.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/mlp.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/site/mlp-preview.js">
</cfinvoke>

<cfinclude template="../views/layouts/master.cfm">
