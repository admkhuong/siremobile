<cfparam name="ccpxDataId" default="0">
<cfparam name="isPreview" default="0">
<cfparam name="checkTerm" default="0">
<cfparam name="showSubmitBtn" default="0">
<cfparam name="checkTotalSubList" default="0">

<cfif isPreview EQ 1>
	<cfset inpActive = 0>
<cfelse>
	<cfset inpActive = 1>
</cfif>



<cfinvoke component="public.sire.models.cfc.mlp" method="ReadCPPTemplateByFilter" returnvariable="parseXML">
	<cfinvokeargument name="inpTemplateID" value="#ccpxDataId#">
</cfinvoke>

<cfoutput>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>MLP</title>
	<meta name="robots" content="index, follow">
	<meta name="description" content="">
	<meta name="viewport" id="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=10.0,initial-scale=1.0">
	<link href="/public/sire/css/bootstrap.css" rel="stylesheet">
	<link href="/public/sire/css/validationEngine.jquery.css" rel="stylesheet">
    <link href="/public/sire/css/bootstrap-tour.min.css" rel="stylesheet">
		
	<link href="/cppx/css/style.css" rel="stylesheet">

	<link rel="stylesheet" href="/cppx/css/font-awesome-4.5.0/css/font-awesome.min.css">

    <!--- http://realfavicongenerator.net/favicon?file_id=p1a4qpcvhn1jltrkukhpvas1f206#.VlNZzHunXvA  --->
    <link rel="apple-touch-icon" sizes="57x57" href="/public/sire/images/sireicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/public/sire/images/sireicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/public/sire/images/sireicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/public/sire/images/sireicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/public/sire/images/sireicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/public/sire/images/sireicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/public/sire/images/sireicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/public/sire/images/sireicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/public/sire/images/sireicons/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/public/sire/images/sireicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/public/sire/images/sireicons/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/public/sire/images/sireicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/public/sire/images/sireicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/public/sire/images/sireicons/manifest.json">
    <link rel="mask-icon" href="/public/sire/images/sireicons/safari-pinned-tab.svg" color="##5bbad5">
    <meta name="msapplication-TileColor" content="##da532c">
    <meta name="msapplication-TileImage" content="/public/sire/images/sireicons/mstile-144x144.png">
    <meta name="theme-color" content="##ffffff">
    <cfif parseXML.URLCSS NEQ ''>
    	<link rel="stylesheet" href="#parseXML.URLCSS#">
    </cfif>
</head>
<body>
	
	<!-- BODY SECTION
	=============================================================-->
	<div id="main">
		<div class="container-fluid page">
			<div class="row">

			   

			   	<cfif parseXML.RXRESULTCODE EQ 1>

			   		<!--- LOOP TO CHECK TOTAL  subcriber list group --->
			   		<cfset xmlObject = parseXML.DATA>
				   	<cfset checkTerm = 0>
				   	<cfset checkMultiSubList = xmlObject[1]['multi-sub-list']>
				   	<cfloop array="#xmlObject#" index="cppObject">
				   		<cfif structKeyExists(cppObject, "subcriber-list-group")>
				   			<cfset checkTotalSubList++ >
				   		</cfif>	
				   	</cfloop>	

			   		<form id="cppForm" action="##">
			   			<input type="hidden" name="inpCPPUUID" id="inpCPPUUID" value="#parseXML.CPPID#">
				   		
				   		<cfloop array="#xmlObject#" index="cppObject">
				   			
				   			<cfif structKeyExists(cppObject, "term-of-service")>
				   				<cfset checkTerm = 1>
				   				<cfset checkTermTxt = cppObject['term-of-service']>
				   			</cfif>

				   			<cfif structKeyExists(cppObject, "text-content")>
				   				<div class="col-md-12">
				   					<div class="stepNote">#cppObject['text-content']#</div>
				   				</div>	
				   			</cfif>

						   	<cfif structKeyExists(cppObject, "sms-number")>
						   		<cfset showSubmitBtn = 1>
				   				<div class="col-md-12">
				   					<div class="form-group">
									    <label for="sms-number">#cppObject['sms-number']#</label>
									    <div class="input-group">
										    <input type="input" class="form-control validate[required,custom[usPhoneNumber]] sms-number cpp-object" name="sms-number" placeholder="SMS Number" maxlength="255">
										    <span class="input-group-addon remove-input-object" style="display:none"><i class="fa fa-minus"></i></span>
										    <span class="input-group-addon add-input-object"><i class="fa fa-plus-circle fa-lg"></i></span>

										</div>    
									</div>
				   				</div>	
				   			</cfif>

				   			<cfif structKeyExists(cppObject, "voice-number")>
				   				<cfset showSubmitBtn = 1>
				   				<div class="col-md-12">
				   					<div class="form-group">
									    <label for="sms-number">#cppObject['voice-number']#</label>
									    <div class="input-group">
										    <input type="input" class="form-control validate[required,custom[usPhoneNumber]] voice-number cpp-object" name="voice-number" placeholder="Voice Number" maxlength="255">
										    <span class="input-group-addon remove-input-object" style="display:none"><i class="fa fa-minus"></i></span>
										    <span class="input-group-addon add-input-object"><i class="fa fa-plus-circle fa-lg"></i></span>

										</div>    
									</div>
				   				</div>	
				   			</cfif>

						   	<cfif structKeyExists(cppObject, "email-address")>
						   		<cfset showSubmitBtn = 1>
				   				<div class="col-md-12">
				   					<div class="form-group">
									    <label for="email-address">#cppObject['email-address']#</label>
									    <div class="input-group">
										    <input type="input" class="validate[required,custom[email]] form-control email-address cpp-object" name="email-address" placeholder="Email Address" maxlength="255">
										    <span class="input-group-addon remove-input-object" style="display:none"><i class="fa fa-minus"></i></span>
										    <span class="input-group-addon add-input-object"><i class="fa fa-plus-circle fa-lg"></i></span>

										</div>    
									</div>
				   				</div>	
				   			</cfif>

				   			<cfif structKeyExists(cppObject, "subcriber-list-group")>
				   				<cfset showSubmitBtn = 1>
				   				<cfif checkTotalSubList GT 1>
					   				<div class="col-md-12 col-lg-6">
					   					<cfif checkMultiSubList EQ 1>
								   			<div class="checkbox">
											    <label>
											    	<input type="checkbox" class="validate[minCheckbox[1]] subcriber-list cpp-object" name="subcriber-list" value="#cppObject['subcriber-list-group']['LISTID']#"> #cppObject['subcriber-list-group']['NAME']#		
											    </label>
											</div>
										<cfelse>
											<div class="radio">
											    <label>
											    	<input type="radio" class="validate[minCheckbox[1]] subcriber-list cpp-object" name="subcriber-list" value="#cppObject['subcriber-list-group']['LISTID']#"> #cppObject['subcriber-list-group']['NAME']#		
											    </label>
											</div>
										</cfif>
									</div>	
								<cfelse>
									<input type="checkbox" class="validate[minCheckbox[1]] subcriber-list cpp-object hidden" name="subcriber-list" value="#cppObject['subcriber-list-group']['LISTID']#" checked>
								</cfif>
				   			</cfif>

				   		</cfloop>

				   		<!---
					   	<cfif checkTerm EQ 1> <!--- DISPLAY TERM --->
						   	<div class="col-md-12">
						   		<div class="checkbox">
								    <label>
								      <input type="checkbox" class="validate[required]" name="check-terms"> #checkTermTxt#
								    </label>
								</div>
							</div>	
					   	</cfif>
					   	--->
					   	<cfif showSubmitBtn EQ 1> 
						   	<div class="form-group">
								<div class="col-md-12">
								   	<button type="button" class="btn btn-primary" id="btn-submit-cpp">Submit</button>
								</div>
							</div>	
						</cfif>	
					   	<!--- END DISPLAY OBJECT --->	
				   	</form>	
			   	<cfelse> <!--- NOT FOUND CPP DATA --->
				   	<div class="col-md-12">
				   		<p class="text-center"> #parseXML.MESSAGE# </p>
				   	</div>		   	
		   		</cfif>
		   </div>	
		</div> <!--- END : container-fluid --->   

	</div> <!--- END : main ---> 

	<cfif checkTerm EQ 1>
		<div id="checkTermModal" class="modal fade" tabindex="-1" role="dialog">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title">Terms of service</h4>
		      </div>
		      <div class="modal-body">
		        #checkTermTxt#
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-primary">Agree</button>
		      </div>
		    </div><!-- /.modal-content -->
		  </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	</cfif>
	
	<script src="/public/sire/js/jquery-2.1.4.js"></script>
	<script src="/public/sire/js/bootstrap.js"></script>
	<!---
	<script src="/public/sire/js/retina.js"></script>
	--->
	<script src="/public/sire/js/jquery.validationEngine.en.js"></script>
	<script src="/public/sire/js/jquery.validationEngine.js"></script>
	<script src="/public/sire/js/bootbox.min.js"></script>
	<script src="/public/sire/js/jquery.mask.min.js"></script>

	<script type="text/javascript">
		var isPreview = <cfoutput>#isPreview#</cfoutput>;
		var checkTerm = <cfoutput>#checkTerm#</cfoutput>;
	</script>

</body>
</html>
</cfoutput>
