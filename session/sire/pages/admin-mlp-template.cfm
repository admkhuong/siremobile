<cfinclude template="../../sire/configs/paths.cfm">

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/js/site/admin_mlp_template.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/mlp.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/mlp-admin.css">
</cfinvoke>

<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<div class="portlet light bordered">
	<div class="row heading">
		<div class="col-sm-6">
	        <h2 class="page-title">Administration - MLP Template</h2>
		</div>  
		<div class="col-sm-6 heading-button text-right">
		<!--- <a class="btn btn-success-custom" href="/session/sire/pages/mlp-edit?action=template">Create new template</a>&nbsp --->
		<a class="btn btn-success-custom" href="/session/sire/pages/admin-mlp-list">Create template from MLP</a></div>
    </div>
    <div class="row">
        <div class="col-xs-12">
			<div class="content-body "id="MainCampaignConsole" >
				<div id="CampaignListSection">
					<div class="re-table">
						<div class="table-responsive">
							<table id="tblListEMS" class="<!--- table-bordered ---> table-responsive <!--- table-hover ---> table-striped dataTables_wrapper">
				           
				            </table>
				        </div>
					</div>
				</div>
			</div>
        </div>
    </div>
</div>

<cfparam name="variables._title" default="Admin MLP Teampate">
<cfinclude template="../views/layouts/master.cfm">
