<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
    .addCss("../assets/global/plugins/daterangepicker/daterangepicker.css", true)
    .addCss("../assets/pages/styles/font-awesome-4.7.0/css/font-awesome.min.css")
    .addCss("/session/sire/css/filter_table.css", true)
    .addJs("../assets/global/plugins/highchart/highcharts.js", true)
    .addJs("../assets/global/plugins/highchart/exporting.js", true)
    .addJs("../assets/pages/scripts/jquery.textfill.min.js")
    .addJs("../assets/global/plugins/daterangepicker/moment.min.js", true)
    .addJs("../assets/global/plugins/daterangepicker/daterangepicker.js", true)
    .addJs("/session/sire/assets/global/scripts/filter-bar-generator.js")
    .addJs("../assets/pages/scripts/admin_new_coupon.js");
</cfscript>


<div class="portlet light bordered subscriber">
    <form id="add-new-coupon-form">
        <input type="hidden" name="origin-coupon-id" id="origin-coupon-id">
        <input type="hidden" name="coupon-id" id="coupon-id">
        <div class="row">
            <div class="col-lg-10 col-md-10">
                <h2 class="page-title">New Coupon</h2>
            </div>
            <div class="col-lg-2 col-md-2">
                <h2><button class="btn green-gd btn-re pull-right" type="submit" id="create-new-coupon">Save</button></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-title">Basic Information</h2>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    <label for="coupon-name">Name<span class="red">*</span></label>
                    <label class="pull-right hidden" id="coupon-name-countdown-label"><a id="coupon-name-countdown" class="hidden">256</a> left</label>
                    <input type="text" class="form-control validate[required,custom[noHTML]]" id="coupon-name"/>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    <label for="coupon-code">Coupon Code<span class="red">*</span></label>
                    <label class="pull-right hidden" id="coupon-code-countdown-label"><a id="coupon-code-countdown"  class="hidden">16</a> left</label>
                    <input type="text" class="form-control validate[required,custom[noHTML],minSize[3],custom[onlyLetterNumber]]" id="coupon-code"/>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    <label for="coupon-desc">Description<span class="red">*</span></label>
                    <label class="pull-right hidden" id="coupon-desc-countdown-label"><a id="coupon-desc-countdown"  class="hidden">1000</a> left</label>
                    <textarea type="text" class="form-control validate[required]" id="coupon-desc" rows="5" /></textarea>
                </div>
            </div>
            <div class="col-lg-6 col-md-6" style="padding:0px">
                <div class="col-lg-12 col-md-12">
                    <a href="admin-coupon-manage" class="btn green-gd"><b>MANAGE ADDITION CODES</b></a>
                    <a id="delete-coupon" class="btn blue-gd"><b>DELETE COUPON</b></a>
                </div>
                <div class="col-lg-12 col-md-12 coupon-availability">
                    <b>Availability</b>
                </div>
                <div class="col-lg-12 col-md-12">
                    <div class="custom-checkbox">
                        <input type="checkbox" value="" id="coupon-available" checked />
                        <label for="coupon-available">If this box is checked, the coupon is available for use.</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <h2 class="page-title">Options</h2>
            </div>
            <div class="col-lg-12 col-md-12">
                <ul class="nav nav-tabs coupon">
                    <li class="active">
                        <a data-toggle="tab" href="#coupon-op-discounts">Discounts</a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#coupon-op-limitations">Limitations</a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#coupon-op-recurring">Recurring</a>
                    </li>
                </ul>
                <div class="tab-content coupon">
                    <div id="coupon-op-discounts" class="tab-pane fade in active">
                        <div class="coupon-tab-desc">Choose the discount type(s) that you want to apply to this coupon.</div>
                        <div class="row">
                            <div class="col-lg-12">
                                <input type="radio" class="css-checkbox discount-type" id="discount-no" value="0" name="op-discount" checked>
                                <label class="radGroup1" for="discount-no">No Discount</label>
                            </div>
                            <div class="col-lg-12 plan-price-discount">
                                <input type="radio" class="css-checkbox discount-type" id="discount-plan" value="1" name="op-discount" data-control="op-discount-plan">
                                <label class="radGroup1" for="discount-plan">Plan Price Discount</label>

                                <div class="discount-detail" data-control="op-discount-plan">
                                    <div class="col-lg-11 col-lg-offset-1">
                                        <div class="row">
                                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                                <div class="custom-checkbox">
                                                    <input type="radio" class="css-checkbox discount-option-detail1 discount-option-checkbox" id="discount-plan-percentage" value="1" name="op-discount-plan"/>
                                                    <label for="discount-plan-percentage" class="radGroup1">Percentage</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-5 col-sm-5 col-xs-12">
                                                <div class="form-group has-feedback">
                                                    <input type="text" class="form-control validate[custom[number]] discount-option-detail1 discount-option-value" id="discount-plan-percentage-value" placeholder="0%" maxlength="3" />
                                                    <i class="form-control-feedback daterange-inline-icon fa fa-percent" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                                <div class="custom-checkbox">
                                                    <input type="radio" class="discount-option-detail1 discount-option-checkbox" id="discount-plan-flat-rate" value="2" name="op-discount-plan"/>
                                                    <label for="discount-plan-flat-rate" class="radGroup1">Flat Rate</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-5 col-sm-5 col-xs-12">
                                                <div class="form-group has-feedback">
                                                    <input type="text" class="form-control validate[custom[number],custom[flatRate]] discount-option-detail1 discount-option-value" id="discount-plan-flat-rate-value" placeholder="0.00" maxlength="6" />                                               
                                                    <i class="form-control-feedback daterange-inline-icon fa fa-usd" aria-hidden="true"></i>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12 other-discount">
                                <input type="radio" class="css-checkbox discount-type" id="discount-other" value="2" name="op-discount" data-control="op-discount-other">
                                <label class="radGroup1" for="discount-other">Other Discount</label>

                                <div class="discount-detail" data-control="op-discount-plan">
                                    <div class="col-lg-11 col-lg-offset-1">
                                        <div class="row">
                                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                                <div class="custom-checkbox">
                                                    <input type="checkbox" class="css-checkbox discount-option-detail2 discount-option-checkbox" id="discount-plan-credit" value="1" name="op-discount-plan1"/>
                                                    <label for="discount-plan-credit" class="radGroup1">Credit</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-5 col-sm-5 col-xs-12">
                                                <div class="form-group has-feedback">
                                                    <input type="text" class="form-control validate[custom[number]] discount-option-detail2 discount-option-value" id="discount-plan-credit-value" placeholder="0" maxlength="11"/>
                                                    <i class="form-control-feedback daterange-inline-icon fa fa-hashtag" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                                <div class="custom-checkbox">
                                                    <input type="checkbox" class="css-checkbox discount-option-detail2 discount-option-checkbox" id="discount-plan-keyword" value="2" name="op-discount-plan1"/>
                                                    <label for="discount-plan-keyword" class="radGroup1">Keyword</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-5 col-sm-5 col-xs-12">
                                                <div class="form-group has-feedback">
                                                    <input type="text" class="form-control validate[custom[integer]] discount-option-detail2 discount-option-value" id="discount-plan-keyword-value" placeholder="0" maxlength="9" />
                                                    <i class="form-control-feedback daterange-inline-icon fa fa-hashtag" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                                <div class="custom-checkbox">
                                                    <input type="checkbox" class="css-checkbox discount-option-detail2 discount-option-checkbox" id="discount-plan-mlp" value="3" name="op-discount-plan1"/>
                                                    <label for="discount-plan-mlp" class="radGroup1">MLP</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-5 col-sm-5 col-xs-12">
                                                <div class="form-group has-feedback">
                                                    <input type="text" class="form-control validate[custom[integer]] discount-option-detail2 discount-option-value" id="discount-plan-mlp-value" placeholder="0" maxlength="9" />
                                                    <i class="form-control-feedback daterange-inline-icon fa fa-hashtag" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                                <div class="custom-checkbox">
                                                    <input type="checkbox" class="css-checkbox discount-option-detail2 discount-option-checkbox" id="discount-plan-shorturl" value="4" name="op-discount-plan1"/>
                                                    <label for="discount-plan-shorturl" class="radGroup1">Short URL</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-5 col-sm-5 col-xs-12">
                                                <div class="form-group has-feedback">
                                                    <input type="text" class="form-control validate[custom[integer]] discount-option-detail2 discount-option-value" id="discount-plan-shorturl-value" placeholder="0" maxlength="9" />
                                                    <i class="form-control-feedback daterange-inline-icon fa fa-hashtag" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="coupon-op-limitations" class="tab-pane fade">
                        <div class="coupon-tab-desc">Choose the lifetime of the coupon and limitations that should be applied.</div>
                        <div class="row">
                            <div class="col-lg-5 col-md-5">
                                <div class="form-group has-feedback">
                                    <label for="promo-new-startdate">Activation Date</label>
                                    <input type="text" class="form-control" id="promo-new-startdate" value="" placeholder="Click to select date">
                                    <i class="form-control-feedback daterange-inline-icon fa fa-calendar"></i>
                                    <div class="start-date-coupon-desc"><i>If no date is selected the coupon will be activated immediately.</i></div>
                                </div>
                            </div>

                            <div class="col-lg-5 col-md-5">
                                <div class="form-group has-feedback">
                                    <label for="promo-new-enddate">Expiration Date</label>
                                    <input type="text" class="form-control" id="promo-new-enddate" value="" placeholder="Click to select date">
                                    <i class="form-control-feedback daterange-inline-icon fa fa-calendar"></i>
                                    <div class="expire-date-coupon-desc"><i>If no date is selected the coupon will not expire.</i></div>
                                </div>
                            </div>

                            <div class="col-lg-5 col-md-5">
                                <div class="form-group">
                                    <label for="promo-new-redemptions">Max Redemptions</label>
                                    <input type="text" class="form-control validate[custom[onlyNumber]]" id="promo-new-redemptions" value="" placeholder="Unlimited">
                                    <div class="expire-date-coupon-desc">
                                        <i>The number of times a coupon code can be used. Set to zero or empty for unlimited redemptions.</i>
                                    </div>
                                    <div class="expire-date-coupon-desc">
                                        <i><span class="note-title">Note:</span> This number applies to each coupon code in this coupon.</i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-5 col-md-5">
                                <div class="custom-checkbox mt-10">
                                    <input type="checkbox" value="" id="check-combined" />
                                    <label for="check-combined">Cannot be combined with other offers.</label>
                                </div>

                                <div class="custom-checkbox mt-10">
                                    <input type="checkbox" value="" id="check-individual" />
                                    <label for="check-individual">Cannot be used by the same individual more than once.</label>
                                </div>

                                <div class="custom-checkbox mt-10">
                                    <input type="checkbox" value="" id="check-invitation" />
                                    <label for="check-invitation">By Invitation Only</label>
                                </div>

                                <div class="mt-10">
                                    <textarea rows="6" type="text" name="" id="invitation-list" class="form-control invitation-ml" place="Email" data-prompt-position="topLeft:100" placeholder="Emails separated by comma."></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="coupon-op-recurring" class="tab-pane fade">
                        <div class="coupon-tab-desc">Choose when the coupon will be applied.</div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="custom-checkbox">
                                        <input type="radio" class="css-checkbox recurring-type" id="recurring-type-1" value="1" name="coupon-recurring" checked/>
                                        <label for="recurring-type-1" class="radGroup1">No recurring</label><br>
                                        <span><i>The coupon will only be applied once to your customer's subscription.</i></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="custom-checkbox">
                                        <input type="radio" class="css-checkbox recurring-type" id="recurring-type-2" value="2" name="coupon-recurring"/>
                                        <label for="recurring-type-2" class="radGroup1">Apply on every renewal</label><br>
                                        <span><i>The coupon will be applied every time the customer renews his/her subscription.</i></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="custom-checkbox">
                                        <input type="radio" class="css-checkbox recurring-type" id="recurring-type-3" value="3" name="coupon-recurring"/>
                                        <label for="recurring-type-3" class="radGroup1">Fixed number of renewals</label><br>
                                        <span><i>The coupon will be applied for a limited amount of subscription renewals (after the inital redemption).</i></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <input type="text" name="" id="number-of-renewals" class="form-control" val="0">
                                        <span class="help-block color-5c">Enter the number of renewals</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<style>
    .red {
        color: red;
    }
    label {
        font-weight: bold;
    }
    ul.coupon li {
        font-weight: bold;
    }
    ul.coupon > li > a {
        color: #5c5c5c !important;
    }
    ul.coupon > li.active > a {
        color: #74c37f !important;
    }
    ul.coupon > li.active > a:hover{
        color: #74c37f !important;
    }
    .tab-content.coupon {
        border: 1px solid #ddd;
        border-top: none;
        padding: 1.5rem;
    }
    ul.nav-tabs.coupon {
        margin-bottom: 0px;
    }
    .note-title{
        color: #74c37f;
        font-weight: 700;
    }
    .mt-10 > label{
        font-weight: normal !important;
    }
    @media (min-width: 992px) {
        .invitation-ml{
            margin-left: 25px;
        }
    }
</style>

<cfparam name="variables._title" default="Coupons Detail">
<cfinclude template="../views/layouts/master.cfm">
