<cfparam name="nocid" default="0"/>

<cfscript>
	createObject("component", "public.sire.models.helpers.layout")
	.addJs("/public/js/jquery.tmpl.min.js", true)
	.addJs("/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js")
	.addCss("/public/js/jquery-ui-1.11.4.custom/jquery-ui.css", true)
	.addCss("/session/sire/assets/global/plugins/uniform/css/uniform.default.css", true)
	.addJs('/session/sire/assets/global/scripts/filter-bar-generator.js', true)
	.addJs('/session/sire/assets/global/plugins/uniform/jquery.uniform.min.js', true)
	.addCss("/session/sire/assets/global/plugins/Chartist/chartist.min.css", true)
	.addJs("/session/sire/assets/pages/scripts/admin-noc-reports.js");
</cfscript>

<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfinclude template="/session/sire/configs/noc_constants.cfm"/>

<cfoutput>

<div class="portlet light bordered">
	<!--- Filter bar --->
	<div class="row">
		<div class="col-sm-3 col-md-4">
			<h2 class="page-title">NOC Reports</h2>
		</div>
		<div class="col-sm-9 col-md-8">
			<div id="box-filter" class="form-gd">

			</div>
		</div>
	</div>
	<!--- Listing --->
	<div class="row">
		<div class="col-md-12">
			<div class="re-table">
				<div class="table-responsive">
					<table id="NOCReportsList" class="table table-striped table-responsive">
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				<a title="Open settings" target="_blank" href="/session/sire/pages/admin-noc-settings"><i class="fa fa-cog" aria-hidden="true" style="font-size: 20px"></i></a>
			</div>
		</div>
	</div>
</div>

<style type="text/css">
	input.right {
		text-align: right;
	}
	th.th-center {
		text-align: center;
	}
</style>

<script type="text/javascript">
	nocid = #nocid#;
</script>

</cfoutput>

<cfparam name="variables._title" default="Admin NOC Reports">
<cfinclude template="../views/layouts/master.cfm">