<cfscript>
	createObject("component", "public.sire.models.helpers.layout")
	.addCss("../assets/global/plugins/daterangepicker/daterangepicker.css", true)
	.addCss("/session/sire/assets/global/plugins/Chartist/chartist.min.css", true)
	.addCss("/session/sire/assets/global/plugins/Chartist/plugins/tooltips/chartist-plugin-tooltip.css", true)
	.addJs("/public/js/jquery.tmpl.min.js", true)
	.addJs("/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js")
	.addCss("/public/js/jquery-ui-1.11.4.custom/jquery-ui.css", true)
	.addCss("/session/sire/assets/global/plugins/uniform/css/uniform.default.css", true)
	.addJs('/session/sire/assets/global/scripts/filter-bar-generator.js', true)
	.addJs('/session/sire/assets/global/plugins/uniform/jquery.uniform.min.js', true)
	.addJs("../assets/global/plugins/daterangepicker/moment.min.js", true)
	.addJs("../assets/global/plugins/daterangepicker/daterangepicker.js", true)
	.addJs("/session/sire/assets/pages/scripts/admin-noc-settings.js")
	.addJs("/session/sire/assets/global/plugins/Chartist/chartist.min.js", true)
	.addJs("/session/sire/assets/global/plugins/Chartist/plugins/tooltips/chartist-plugin-tooltip.js", true);
</cfscript>

<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfinclude template="/session/sire/configs/noc_constants.cfm"/>

<cfoutput>

<div class="portlet light bordered">
	<!--- Filter bar --->
	<div class="row">
		<div class="col-sm-3 col-md-4">
			<h2 class="page-title">NOC Settings</h2>
		</div>
		<div class="col-sm-9 col-md-8">
			<div id="box-filter" class="form-gd">

			</div>
		</div>
	</div>
	<!--- Listing --->
	<div class="row">
		<div class="col-md-12">
			<div class="re-table">
				<div class="table-responsive">
					<table id="NOCList" class="table table-striped table-responsive">
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6">
			<!--- <button class="btn bulk-noc-function" data-action="execute" id="execute-selected-noc">Execute</button>&nbsp; --->
			<button class="btn bulk-noc-function" data-action="delete" id="delete-selected-noc">Delete</button>&nbsp;
			<button class="btn bulk-noc-function" data-action="pause" id="pause-selected-noc">Pause</button>&nbsp;
			<button class="btn bulk-noc-function" data-action="resume" id="resume-selected-noc">Resume</button>
		</div>
		<div class="col-xs-6">
			<div class="pull-right">
				<button class="btn btn-block green-gd" data-toggle="modal" data-target="##CreateNewNOCModal">Add New NOC</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="CreateNewNOCModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="width:auto; max-width:900px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Create New NOC</h4>
			</div>
			<div class="modal-body">
				<form name="create-new-noc-form" id="create-new-noc-form" class="form-horizontal">
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Name:</label>
						<div class="col-sm-9">
							<input type="text" class="form-control validate[required]" id="new-name" placeholder="NOC Name">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">URL:</label>
						<div class="col-sm-9">
							<input type="text" class="form-control validate[required, custom[url]]" id="new-url" placeholder="NOC URL">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Description:</label>
						<div class="col-sm-9">
							 <textarea class="form-control" id="new-description" maxlength="250" data-prompt-position="topLeft:150" rows="3"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Interval Type:</label>
						<div class="col-sm-5">
							<select class="form-control" id="new-interval-type">
								<cfloop array="#_NOC_INTERVAL_TYPE#" index="type">
									<option value="#type#">#type#</option>
								</cfloop>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Start Date :</label>
						<div class="col-sm-10">
							<div class="row">
								<div class="col-sm-2">
									<input id="new-start-date-day" class="form-control right day validate[required,custom[onlyNumber]]" title="Day" type="text" placeholder="Day" value="#(day(now()) LT 10 ? "0"&day(now()): day(now()))#">
								</div>
								<div class="col-sm-2">
									<input id="new-start-date-month" class="form-control right month validate[required,custom[onlyNumber]]" title="Month" type="text" placeholder="Month" value="#(month(now()) LT 10 ? "0"&month(now()) : month(now()))#">
								</div>
								<div class="col-sm-2">
									<input id="new-start-date-year" class="form-control right year validate[required,custom[onlyNumber]]" title="Year" type="text" placeholder="Year" value="#year(now())#">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Start Time :</label>
						<div class="col-sm-10">
							<div class="row">
								<div class="col-sm-2">
									<input id="new-start-time-hour" class="form-control right hour validate[required,custom[onlyNumber]]" title="Hour" type="text" placeholder="Hour" value="00">
								</div>
								<div class="col-sm-2">
									<input id="new-start-time-minute" class="form-control right minute validate[required,custom[onlyNumber]]" title="Minute" type="text" placeholder="Minute" value="00">
								</div>
								<div class="col-sm-2">
									<input id="new-start-time-second" class="form-control right second validate[required,custom[onlyNumber]]" title="Second" type="text" placeholder="Second" value="00">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">End Date :</label>
						<div class="col-sm-10">
							<div class="row">
								<div class="col-sm-2">
									<input id="new-end-date-day" class="form-control right day validate[custom[onlyNumber]]" title="Day" type="text" placeholder="Day" value="">
								</div>
								<div class="col-sm-2">
									<input id="new-end-date-month" class="form-control right month validate[custom[onlyNumber]]" title="Month" type="text" placeholder="Month" value="">
								</div>
								<div class="col-sm-2">
									<input id="new-end-date-year" class="form-control right year validate[custom[onlyNumber]]" title="Year" type="text" placeholder="Year" value="">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">End Time :</label>
						<div class="col-sm-10">
							<div class="row">
								<div class="col-sm-2">
									<input id="new-end-time-hour" class="form-control right hour validate[custom[onlyNumber]]" title="Hour" type="text" placeholder="Hour">
								</div>
								<div class="col-sm-2">
									<input id="new-end-time-minute" class="form-control right minute validate[custom[onlyNumber]]" title="Minute" type="text" placeholder="Minute">
								</div>
								<div class="col-sm-2">
									<input id="new-end-time-second" class="form-control right second validate[custom[onlyNumber]]" title="Second" type="text" placeholder="Second">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Interval :</label>
						<div class="col-sm-10">
							<div class="row">
								<div class="col-sm-2">
									<input id="new-interval-hour" class="form-control right hour validate[custom[onlyNumber]]" title="Hour" type="text" placeholder="Hour" value="00">
								</div>
								<div class="col-sm-2">
									<input id="new-interval-minute" class="form-control right minute validate[custom[onlyNumber]]" title="Minute" type="text" placeholder="Minute" value="00">
								</div>
								<div class="col-sm-2">
									<input id="new-interval-second" class="form-control right second validate[custom[onlyNumber]]" title="Second" type="text" placeholder="Second" value="00">
								</div>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Additions :</label>
						<div class="col-sm-10">
							<div class="row">
								<div class="col-sm-2">
									<input id="new-total-fail" class="form-control right validate[custom[onlyNumber]]" title="Total Fail" type="text" placeholder="Total Fail" value="">
								</div>
								<div class="col-sm-2">
									<input id="new-avg-connecttime" class="form-control right validate[custom[onlyNumber]]" title="Avg connect time" type="text" placeholder="Avg times" value="">
								</div>
								<div class="col-sm-2">
									
								</div>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Paused :</label>
						<div class="col-sm-10" style="margin-top:8px">
							<div class="custom-checkbox">
								<input class="css-checkbox" type="checkbox" value="" id="new-paused" />
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Port:</label>
						<div class="col-sm-5">
							<input type="text" class="form-control validate[required,custom[onlyNumber]]" id="new-port" placeholder="Port" value="80"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Time out(s):</label>
						<div class="col-sm-5">
							<input type="text" class="form-control validate[required,custom[onlyNumber]]" id="new-time-out" placeholder="Time out" value="3600"/>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success-custom btn-save-group" id="create-new-noc-btn"> Save </button>
				&nbsp; &nbsp; &nbsp; 
				<button type="button" class="btn btn-back-custom" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="EditNOCModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="width:auto; max-width:900px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Edit NOC</h4>
			</div>
			<div class="modal-body">
				<form name="edit-noc-form" id="edit-noc-form" class="form-horizontal">
					<input type="hidden" id="edit-noc-id" value="">
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Name:</label>
						<div class="col-sm-9">
							<input type="text" class="form-control validate[required]" id="edit-name" placeholder="NOC Name">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">URL:</label>
						<div class="col-sm-9">
							<input type="text" class="form-control validate[required, custom[url]]" id="edit-url" placeholder="NOC URL">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Description:</label>
						<div class="col-sm-9">
							 <textarea class="form-control" id="edit-description" maxlength="250" data-prompt-position="topLeft:150" rows="3"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Interval Type:</label>
						<div class="col-sm-5">
							<select class="form-control" id="edit-interval-type">
								<cfloop array="#_NOC_INTERVAL_TYPE#" index="type">
									<option value="#type#">#type#</option>
								</cfloop>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Start Date :</label>
						<div class="col-sm-10">
							<div class="row">
								<div class="col-sm-2">
									<input id="edit-start-date-day" class="form-control right day validate[required,custom[onlyNumber]]" title="Day" type="text" placeholder="Day" value="">
								</div>
								<div class="col-sm-2">
									<input id="edit-start-date-month" class="form-control right month validate[required,custom[onlyNumber]]" title="Month" type="text" placeholder="Month" value="">
								</div>
								<div class="col-sm-2">
									<input id="edit-start-date-year" class="form-control right year validate[required,custom[onlyNumber]]" title="Year" type="text" placeholder="Year" value="">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Start Time :</label>
						<div class="col-sm-10">
							<div class="row">
								<div class="col-sm-2">
									<input id="edit-start-time-hour" class="form-control right hour validate[required,custom[onlyNumber]]" title="Hour" type="text" placeholder="Hour" value="">
								</div>
								<div class="col-sm-2">
									<input id="edit-start-time-minute" class="form-control right minute validate[required,custom[onlyNumber]]" title="Minute" type="text" placeholder="Minute" value="">
								</div>
								<div class="col-sm-2">
									<input id="edit-start-time-second" class="form-control right second validate[required,custom[onlyNumber]]" title="Second" type="text" placeholder="Second" value="">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">End Date :</label>
						<div class="col-sm-10">
							<div class="row">
								<div class="col-sm-2">
									<input id="edit-end-date-day" class="form-control right day validate[custom[onlyNumber]]" title="Day" type="text" placeholder="Day" value="">
								</div>
								<div class="col-sm-2">
									<input id="edit-end-date-month" class="form-control right month validate[custom[onlyNumber]]" title="Month" type="text" placeholder="Month" value="">
								</div>
								<div class="col-sm-2">
									<input id="edit-end-date-year" class="form-control right year validate[custom[onlyNumber]]" title="Year" type="text" placeholder="Year" value="">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">End Time :</label>
						<div class="col-sm-10">
							<div class="row">
								<div class="col-sm-2">
									<input id="edit-end-time-hour" class="form-control right hour validate[custom[onlyNumber]]" title="Hour" type="text" placeholder="Hour">
								</div>
								<div class="col-sm-2">
									<input id="edit-end-time-minute" class="form-control right minute validate[custom[onlyNumber]]" title="Minute" type="text" placeholder="Minute">
								</div>
								<div class="col-sm-2">
									<input id="edit-end-time-second" class="form-control right second validate[custom[onlyNumber]]" title="Second" type="text" placeholder="Second">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Interval :</label>
						<div class="col-sm-10">
							<div class="row">
								<div class="col-sm-2">
									<input id="edit-interval-hour" class="form-control right hour validate[custom[onlyNumber]]" title="Hour" type="text" placeholder="Hour" value="">
								</div>
								<div class="col-sm-2">
									<input id="edit-interval-minute" class="form-control right minute validate[custom[onlyNumber]]" title="Minute" type="text" placeholder="Minute" value="">
								</div>
								<div class="col-sm-2">
									<input id="edit-interval-second" class="form-control right second validate[custom[onlyNumber]]" title="Second" type="text" placeholder="Second" value="">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Additions :</label>
						<div class="col-sm-10">
							<div class="row">
								<div class="col-sm-2">
									<input id="edit-total-fail" class="form-control right validate[custom[onlyNumber]]" title="Total Fail" type="text" placeholder="Total Fail" value="">
								</div>
								<div class="col-sm-2">
									<input id="edit-avg-connecttime" class="form-control right validate[custom[onlyNumber]]" title="Avg connect time" type="text" placeholder="Avg times" value="">
								</div>
								<div class="col-sm-2">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Paused :</label>
						<div class="col-sm-10" style="margin-top:8px">
							<div class="custom-checkbox">
								<input class="css-checkbox" type="checkbox" value="" id="edit-paused" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Port:</label>
						<div class="col-sm-5">
							<input type="text" class="form-control validate[required,custom[onlyNumber]]" id="edit-port" placeholder="Port">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Time out(s):</label>
						<div class="col-sm-5">
							<input type="text" class="form-control validate[required,custom[onlyNumber]]" id="edit-time-out" placeholder="Time out">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success-custom btn-save-group" id="edit-noc-btn"> Save </button>
				&nbsp; &nbsp; &nbsp; 
				<button type="button" class="btn btn-back-custom" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="ChatSessionReport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="width:auto; max-width:900px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><b>Chat Session Reports</b></h4>
			</div>
			<div class="modal-body">
				<!--- Date range --->
				<div class="row subscriber">
					<div class="col-xs-12">
						<div class="options">
							<div class="row">
								<div class="col-xs-12 col-sm-6">
									<b>Total session number: <span id="total-session"></span></b>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="date-range">
										<p style="margin-top:0"><b>Selected Date Range</b></p>
										<div class="reportrange pull-right">
											<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
											<span></span> <b class="caret"></b>
											<input type="hidden" name="dateStart" value="">
											<input type="hidden" name="dateEnd" value="">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div id="highchart" class="chat-report-detail">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="re-table">
							<div class="table-responsive">
								<table id="CountChatSession" class="table table-striped table-responsive">
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<style type="text/css">
	input.right {
		text-align: right;
	}
</style>

<script>
	var _CCSS_NOC_ID = #_COUNT_CHAT_SESSION_NOC_ID#
</script>

</cfoutput>

<cfparam name="variables._title" default="Admin NOC Settings">
<cfinclude template="../views/layouts/master.cfm">