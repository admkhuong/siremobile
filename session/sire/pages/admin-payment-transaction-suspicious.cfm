<cfscript>
    createObject("component", "public.sire.models.helpers.layout")
        .addJs("/public/js/jquery.tmpl.min.js", true)
        .addJs("/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js")
        .addCss("/public/js/jquery-ui-1.11.4.custom/jquery-ui.css", true)
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)
        
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", false)
        .addJs("/public/sire/js/select2.min.js")

        .addCss("/session/sire/assets/global/plugins/uniform/css/uniform.default.css", true)
        .addJs('/session/sire/assets/global/scripts/filter-bar-generator.js', true)
        .addJs('/session/sire/assets/global/plugins/uniform/jquery.uniform.min.js', true)
        .addJs("/session/sire/assets/pages/scripts/admin-payment-transaction-suspicious.js");        
</cfscript>

<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfparam name="isDialog" default="0">
<cfparam name="isUpdateSuccess" default="">


<div class="portlet light bordered">
    <!--- Filter bar --->
    <div class="row">
        <div class="col-sm-3 col-md-4">
            <h2 class="page-title">Suspicious Transaction</h2>
        </div>
        <div class="col-sm-9 col-md-8">
            <div id="box-filter" class="form-gd">
            </div>
        </div>
    </div>
    <!--- Listing --->
    <div class="row">
        <div class="col-md-12">
            <div class="re-table">
                <div class="table-responsive">
                    <table id="tblList" class="table-striped dataTables_wrapper table-responsive">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="mdCCMatch" tabindex="-1" role="dialog" aria-hidden="true">
    <form name="frCCMatch" id="frCCMatch">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <b>
                        <h4 class="modal-title" id="modal-tit">CC Matching Detail</h4>
                    </b>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <strong>Last Profile update (days)</strong>
                        </div>
                        <div class="col-xs-12 col-sm-5">
                            <span class="profile-update-text"></span>
                        </div>
                        <div class="col-xs-12 col-sm-1">
                            <span class="profile-update-match"></span>
                        </div>                        
                    </div>  
                    <hr/> 
                    <div class="row">
                        <div class="col-xs-12 col-sm-3">
                            <strong>IP Address</strong>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <span class="ip-text"></span>
                        </div>
                        <div class="col-xs-12 col-sm-1">
                            <span class="ip-match"></span>
                        </div>
                    </div>  
                    <hr/> 
                    
                    <div class="row">
                        <div class="col-xs-12 col-sm-3">
                            <strong>AVS</strong>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <span class="avs-text"></span>
                        </div>
                        <div class="col-xs-12 col-sm-1">
                            <span class="avs-match"></span>
                        </div>
                    </div>  
                    <hr/> 
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <strong>Last 4 Digits Card Number</strong>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <span class="card-number-text"></span>
                        </div>                      
                        
                    </div> 
                    <hr/>    
                    <div class="row">
                        <div class="col-xs-12 col-sm-3">
                            
                        </div>
                        <div class="col-xs-5 col-sm-4">
                            <strong>CC INFO</strong>
                        </div>
                        <div class="col-xs-5 col-sm-4">
                            <strong>PROFILE INFO</strong>
                        </div>
                        <div class="col-xs-2 col-sm-1">
                            
                        </div>
                    </div>
                    <hr/> 
                    <div class="row">
                        <div class="col-xs-12 col-sm-3">
                            <strong>First Name</strong>
                        </div>
                        <div class="col-xs-5 col-sm-4">
                            <span class="fname-text"></span>
                        </div>
                        <div class="col-xs-5 col-sm-4">
                            <span class="fname-profile"></span>
                        </div>
                        <div class="col-xs-2 col-sm-1">
                            <span class="fname-match"></span>
                        </div>
                        
                    </div> 
                    <hr/>                   
                    <div class="row">
                        <div class="col-xs-12 col-sm-3">
                            <strong>Last Name</strong>
                        </div>
                        <div class="col-xs-5 col-sm-4">
                            <span class="lname-text"></span>
                        </div>
                        <div class="col-xs-5 col-sm-4">
                            <span class="lname-profile"></span>
                        </div>
                        <div class="col-xs-2 col-sm-1">
                            <span class="lname-match"></span>
                        </div>
                    </div>
                    <!---
                    <hr/>  
                    <div class="row">
                        <div class="col-xs-12 col-sm-3">
                            <strong>Address</strong>
                        </div>
                        <div class="col-xs-5 col-sm-4">
                            <span class="addr-text"></span>
                        </div>
                        <div class="col-xs-5 col-sm-4">
                            <span class="addr-profile"></span>
                        </div>
                        <div class="col-xs-2 col-sm-1">
                            <span class="addr-match"></span>
                        </div>
                    </div>
                    <hr/> 
                    <div class="row">
                        <div class="col-xs-12 col-sm-3">
                            <strong>City</strong>
                        </div>
                        <div class="col-xs-5 col-sm-4">
                            <span class="city-text"></span>
                        </div>
                        <div class="col-xs-5 col-sm-4">
                            <span class="city-profile"></span>
                        </div>
                        <div class="col-xs-2 col-sm-1">
                            <span class="city-match"></span>
                        </div>
                    </div>
                    <hr/> 
                    <div class="row">
                        <div class="col-xs-12 col-sm-3">
                            <strong>State</strong>
                        </div>
                        <div class="col-xs-5 col-sm-4">
                            <span class="state-text"></span>
                        </div>
                        <div class="col-xs-5 col-sm-4">
                            <span class="state-profile"></span>
                        </div>
                        <div class="col-xs-2 col-sm-1">
                            <span class="state-match"></span>
                        </div>
                    </div>
                    <hr/> 
                    <div class="row">
                        <div class="col-xs-12 col-sm-3">
                            <strong>Country</strong>
                        </div>
                        <div class="col-xs-5 col-sm-4">
                            <span class="country-text"></span>
                        </div>
                        <div class="col-xs-5 col-sm-4">
                            <span class="country-profile"></span>
                        </div>
                        <div class="col-xs-2 col-sm-1">
                            <span class="country-match"></span>
                        </div>
                    </div>
                    <hr/> 
                    <div class="row">
                        <div class="col-xs-12 col-sm-3">
                            <strong>Zip Code</strong>
                        </div>
                        <div class="col-xs-5 col-sm-4">
                            <span class="zip-text"></span>
                        </div>
                        <div class="col-xs-5 col-sm-4">
                            <span class="zip-profile"></span>
                        </div>
                        <div class="col-xs-2 col-sm-1">
                            <span class="zip-match"></span>
                        </div>
                    </div>
                    --->
                    <hr/> 
                    <div class="row">
                        <div class="col-xs-12 col-sm-3">
                            <strong>Email</strong>
                        </div>
                        <div class="col-xs-5 col-sm-4">
                            <span class="email-text"></span>
                        </div>
                        <div class="col-xs-5 col-sm-4">
                            <span class="email-profile"></span>
                        </div>
                        <div class="col-xs-2 col-sm-1">
                            <span class="email-match"></span>
                        </div>
                    </div>
                    <hr/> 
                    <div class="row">
                        <div class="col-xs-12 col-sm-3">
                            <strong>Phone Carrier</strong>
                        </div>
                        <div class="col-xs-5 col-sm-4">
                            <span class="phone-text"></span>
                        </div>
                        <div class="col-xs-5 col-sm-4">
                            <span class="phone-profile"></span>
                        </div>
                        <div class="col-xs-2 col-sm-1">
                            <span class="phone-match"></span>
                        </div>
                    </div>
                    <hr/> 
                    
                    <div class="row status-rows">                          
                        <div class="form-gd">
                            <div class="form-group">                                   
                                <div class="col-xs-8">                   
                                    <select class="form-control validate[required, min[1]] select2 submit-action" id="submit-action" data-errormessage="Please select action">
                                        <option value="0">Select Option</option>
                                        <option value="1">Verified this time only</option>
                                        <option value="2">Verified forever</option>
                                        <option value="3">Reject this time only</option>
                                        <option value="4">Blocked forever</option>
                                    </select>  
                                </div>
                                <div class="col-xs-4">
                                    <input type="hidden" name="transaction-id" id="transaction-id" value="0">
                                    <button type="button" class="btn green-gd form-control" id="btnSubmit">Submit</button>                                                                     
                                </div>                                
                            </div>                            
                        </div>   
                    </div>  
                    <div class="row admin-action">
                                             
                    </div>       
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </form>
</div>

<div class="modal fade in" id="mdAVSExplanation" tabindex="-1" role="dialog" aria-hidden="true">    
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <b>
                    <h4 class="modal-title" id="modal-tit">AVS Code explanation</h4>
                </b>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-2">
                        00
                    </div>
                    <div class="col-xs-10">
                        5-Digit zip and address match   D, M, Y
                    </div>                                          
                </div>  
                <hr/> 
                <div class="row">
                    <div class="col-xs-2">
                        01
                    </div>
                    <div class="col-xs-10">
                        9-Digit zip and address match   X
                    </div>                                          
                </div> 
                <hr/> 
                <div class="row">
                    <div class="col-xs-2">
                        02
                    </div>
                    <div class="col-xs-10">
                        Postal code and address match   Y  
                    </div>                                          
                </div> 
                <hr/> 
                <div class="row">
                    <div class="col-xs-2">
                        10
                    </div>
                    <div class="col-xs-10">
                        5-Digit zip matches, address does not match L, P, Z
                    </div>                                          
                </div> 
                <hr/> 
                <div class="row">
                    <div class="col-xs-2">
                        11
                    </div>
                    <div class="col-xs-10">
                        9-Digit zip matches, address does not match W
                    </div>                                          
                </div> 
                <hr/> 
                <div class="row">
                    <div class="col-xs-2">
                        12
                    </div>
                    <div class="col-xs-10">
                        Zip does not match, address matches  A
                    </div>                                          
                </div> 
                <hr/> 
                <div class="row">
                    <div class="col-xs-2">
                        13
                    </div>
                    <div class="col-xs-10">
                        Postal code does not match, address matches A  
                    </div>                                          
                </div> 
                <hr/> 
                <div class="row">
                    <div class="col-xs-2">
                        14
                    </div>
                    <div class="col-xs-10">
                        Postal code matches, address not verified  L, P, Z
                    </div>                                          
                </div> 
                <hr/> 
                <div class="row">
                    <div class="col-xs-2">
                        20
                    </div>
                    <div class="col-xs-10">
                        Neither zip nor address match   C, N
                    </div>                                          
                </div> 
                <hr/> 
                <div class="row">
                    <div class="col-xs-2">
                        30
                    </div>
                    <div class="col-xs-10">
                        AVS service not supported by issuer  G, I, S
                    </div>                                          
                </div> 
                <hr/> 
                <div class="row">
                    <div class="col-xs-2">
                        31
                    </div>
                    <div class="col-xs-10">
                        AVS system not available    B, O, R
                    </div>                                          
                </div> 
                <hr/> 
                <div class="row">
                    <div class="col-xs-2">
                        32
                    </div>
                    <div class="col-xs-10">
                        Address unavailable    U
                    </div>                                          
                </div> 
                <hr/> 
                <div class="row">
                    <div class="col-xs-2">
                        33
                    </div>
                    <div class="col-xs-10">
                        General error
                    </div>                                          
                </div>
                <hr/>  
                <div class="row">
                    <div class="col-xs-2">
                        34
                    </div>
                    <div class="col-xs-10">
                        AVS not performed    B, O, R, S
                    </div>                                          
                </div> 
                <hr/> 
                <div class="row">
                    <div class="col-xs-2">
                        40
                    </div>
                    <div class="col-xs-10">
                        Address failed internal checks
                    </div>                                          
                </div> 
            </div>
        </div>
    </div>
</div>
                
<!--- End change CC info before charge --->
<cfparam name="variables._title" default="Suspicious Transaction">
<cfinclude template="../views/layouts/master.cfm">

