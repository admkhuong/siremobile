<cfscript>
	createObject("component", "public.sire.models.helpers.layout")
		.addJs("/public/sire/js/select2.min.js", true)
		.addCss("/public/sire/css/select2.min.css", true)
		.addCss("/public/sire/css/select2-bootstrap.min.css", true)
		.addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)
		.addJs("/session/sire/assets/pages/scripts/admin-user-support.js");
</cfscript>
<cfparam name="url.transId" default="0">

<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfinvoke method="GetPaymentTransactionById" component="session.sire.models.cfc.reports" returnvariable="RetVarGetPaymentTransactionById">
	<cfinvokeargument name="inpTransId" value="#url.transId#"/>
</cfinvoke>

<cfif RetVarGetPaymentTransactionById.RXRESULTCODE NEQ 1>
	<cflocation url='/session/sire/pages/admin-home' addtoken="false"/>
<cfelse>
	<cfset data = RetVarGetPaymentTransactionById.DATALIST[1]>
</cfif>

<cfoutput>
<div class="portlet light bordered subscriber">
	<div class="row">
		<div class="col-md-12">
			<h2 class="page-title">Admin Report Billing Detail</h2>
		</div>
	</div>
	<!--- Listing --->
	
	<form class="form-gd">	
			
			<div class="row" style="background: rgb(222,222,222); margin: 0em; padding:10px; font-weight:bold"> Transaction Id: #data.TRANSID# </div>
			<br/>
			<!--- BEGIN : CUSTOMER DETAIL --->
			<div class="uk-grid">

				<div class="uk-width-6-6@l">
					<div class="uk-grid uk-grid-small" uk-grid>
						<div class="uk-width-1-1@s uk-first-column"><h3 class="form-heading">Customer Detail:</h3> </div>	

						<div class="uk-width-1-2@s uk-first-column">
							<div class="form-group">
								<label>CustomerId: #data.USERID# </label>
							</div>
						</div>

						<div class="uk-width-1-2@s">
							<div class="form-group">
								<label>Company Name: #data.ORGNAME# </label>
							</div>
						</div>

						<div class="uk-width-1-2@s uk-first-column">
							<div class="form-group">
								<label>First Name: #data.FIRSTNAME# </label>
							</div>
						</div>

						<div class="uk-width-1-2@s">
							<div class="form-group">
								<label>Address: #data.ADDRESS# </label>
							</div>
						</div>

						<div class="uk-width-1-2@s uk-first-column">
							<div class="form-group">
								<label>Last Name: #data.LASTNAME# </label>
							</div>
						</div>

						<div class="uk-width-1-2@s">
							<div class="form-group">
								<label>Zip Code: #data.ZIPCODE# </label>
							</div>
						</div>

						<div class="uk-width-1-2@s uk-first-column">
							<div class="form-group">
								<label>Email: #data.EMAIL# </label>
							</div>
						</div>

						<div class="uk-width-1-2@s">
							<div class="form-group">
								<label>Website: #data.WEBSITE# </label>
							</div>
						</div>

						<div class="uk-width-1-2@s uk-first-column">
							<div class="form-group">
								<label>Phone Number: #data.PHONENUMBER# </label>
							</div>
						</div>
					</div>	
				</div>	
			</div>
			<!--- END :CUSTOMER DETAIL --->

			<!--- BEGIN : BILLING DETAIL --->
			<div class="uk-grid">
				<div class="uk-width-6-6@l">
					<div class="uk-grid uk-grid-small" uk-grid>
						<div class="uk-width-1-1@s uk-first-column"><h3 class="form-heading">Billing Detail:</h3> </div>	

						<div class="uk-width-1-2@s uk-first-column">
							<div class="form-group">
								<label>TransactionId: #data.TRANSID# </label>
							</div>
						</div>

						<div class="uk-width-1-2@s">
							<div class="form-group">
								<label>First Name: #data.CCFIRSTNAME#</label>
							</div>
						</div>

						<div class="uk-width-1-2@s uk-first-column">
							<div class="form-group">
								<label>Created Date: #data.CREATED#</label>
							</div>
						</div>

						<div class="uk-width-1-2@s">
							<div class="form-group">
								<label>Last Name: #data.CCLASTNAME#</label>
							</div>
						</div>

						<div class="uk-width-1-2@s uk-first-column">
							<div class="form-group">
								<label>Amount:  $#numberFormat(replace(data.AMOUNT, "$", "","all"),'.99')#</label>
							</div>
						</div>

						<div class="uk-width-1-2@s">
							<div class="form-group">
								<label>Card Number: #data.CCNUMBER#</label>
							</div>
						</div>

						<div class="uk-width-1-2@s uk-first-column">
							<div class="form-group">
								<label>Email:  #data.CCEMAIL#</label>
							</div>
						</div>

						<div class="uk-width-1-2@s">
							<div class="form-group">
								<label>Bill Address: #data.CCADDRESS#</label>
							</div>
						</div>

						<div class="uk-width-1-2@s uk-first-column">
							<div class="form-group">
								<label>Result: #data.CCRESULT#</label>
							</div>
						</div>

						<div class="uk-width-1-2@s">
							<div class="form-group">
								<label>AVS Code: #data.CCAVSCODE# </label>
							</div>
						</div>

						<div class="uk-width-1-2@s uk-first-column">
							<div class="form-group">
								<label>Message: #data.CCMESSAGE# </label>
							</div>
						</div>

						<div class="uk-width-1-2@s">
							<div class="form-group">
								<label>AVS Result: #data.CCAVSRESULT# </label>
							</div>
						</div>

					</div>	
				</div>	
			</div>
			<!--- END :BILLING DETAIL --->
	</form>
</div>	

</cfoutput>

<cfparam name="variables._title" default="Admin User Support">
<cfinclude template="../views/layouts/master.cfm">

