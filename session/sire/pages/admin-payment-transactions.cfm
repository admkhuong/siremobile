<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />
<cfinclude template="/session/sire/configs/paths.cfm">

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addJs("/public/js/jquery.tmpl.min.js", true)
		.addJs("/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js")
		.addCss("/public/js/jquery-ui-1.11.4.custom/jquery-ui.css", true)
		.addCss("/session/sire/assets/global/plugins/uniform/css/uniform.default.css", true)
		.addJs('/session/sire/assets/global/scripts/filter-bar-generator.js', true)
		.addJs('/session/sire/assets/global/plugins/uniform/jquery.uniform.min.js', true)
		.addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)
        .addJs("/session/sire/assets/pages/scripts/admin_payment_transactions.js");
</cfscript>

<div class="portlet light bordered ">
	<!--- Filter bar --->
	<div class="row">
		<div class="col-sm-3 col-md-4">
			<h2 class="page-title">Filter</h2>
		</div>
		<div class="col-sm-9 col-md-8">
			<div id="box-filter" class="form-gd">

			</div>
		</div>
	</div>
</div>

<div class="portlet light bordered ">
	<div class="portlet-body row">
		<div class="col-sm-12 col-md-12">
			<div class="content-body">
				<div class="container-fluid">
					<div class="re-table">
						<div class="table-responsive">
							<table id="transactions-list" class="table table-striped table-bordered table-hover">
								
							</table>
							<div id="transactions-detail" class="hidden"></div>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>

<cfparam name="variables._title" default="Admin - Payment transactions report">
<cfinclude template="../views/layouts/master.cfm">