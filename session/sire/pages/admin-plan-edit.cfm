<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfparam name="url.inpPlanId" default="0" />

<cfif isNumeric(url.inpPlanId) AND url.inpPlanId GT 0>
	<cfinvoke method="GetPlanById" component="session.sire.models.cfc.plan" returnvariable="RetVarGetPlanById">
		<cfinvokeargument name="inpPlanId" value="#url.inpPlanId#"/>
	</cfinvoke>
	
	<cfif RetVarGetPlanById.RXRESULTCODE EQ 1 AND RetVarGetPlanById.PLAN.RECORDCOUNT GT 0>
		<cfset RetVarGetPlan = RetVarGetPlanById.PLAN/>
		<cfset _Act = 'Edit'/>
		<cfinclude template="admin-plan-form.cfm"/>
	<cfelse>
		<cflocation url="/session/sire/pages/admin-plan-management" addtoken="false"/>
	</cfif>
<cfelse>
	<cflocation url="/session/sire/pages/admin-plan-management" addtoken="false"/>
</cfif>

