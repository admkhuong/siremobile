<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfparam name="_Act" default="New" />
<cfparam name="RetVarGetPlan" default="#{
	PlanId_int = 0,
	PlanName_vch = '',
	Status_int = 1,
	Amount_dec = 0,
	YearlyAmount_dec = 0,
	UserAccountNumber_int = 1,
	Controls_int = '',
	KeywordsLimitNumber_int = 3,
	KeywordMinCharLimit_int = 8,
	FirstSMSIncluded_int = 25,
	CreditsAddAmount_int = 0,
	PriceMsgAfter_dec = 2.7,
	PriceKeywordAfter_dec = 5,
	ByMonthNumber_int = 0,
	ExtraConfigs = '',
	Order_int = 100,
	MlpsLimitNumber_int = 2,
	ShortUrlsLimitNumber_int = 2,
	MlpsImageCapacityLimit_bi = 52428800
}#" />


<cfoutput>
	<div class="portlet light bordered">
		<form id="plan-form" method="post" autocomplete="off" action="">
			<input type="hidden" name="PlanId_int" id="PlanId_int" value="#RetVarGetPlan.PlanId_int#">
			<div class="row">
				<div class="col-lg-8 col-md-8">
					<h2 class="page-title">#_Act# Plan</h2>
				</div>
				<div class="col-lg-4 col-md-4">
					<h2>
						<button class="btn green-gd btn-re pull-right save-plan" type="submit">Save</button>
						<a class="pull-right"> &nbsp; </a>
						<a href="/session/sire/pages/admin-plan-management" class="btn btn-back-custom btn-re pull-right">Cancel</a>
					</h2>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-6 col-md-6">
					<div class="form-group">
						<label for="PlanName_vch">Name<span class="red">*</span></label>
						<input type="text" class="form-control validate[required,maxSize[50],custom[noHTML]]" id="PlanName_vch" name="PlanName_vch" value="#RetVarGetPlan.PlanName_vch#" maxlength="50" tabindex="0" />
					</div>
				</div>
				<div class="col-sm-6 col-md-6">
					<div class="form-group">
						<label for="Status_int">Status</label>
						<select id="Status_int" name="Status_int" class="form-control"  tabindex="7">
							<option value="-8" <cfif RetVarGetPlan.Status_int EQ -8> selected</cfif>>Pending</option>							
							<option value="8"<cfif RetVarGetPlan.Status_int EQ 8> selected</cfif>>Enterprise</option>
							<option value="4" <cfif RetVarGetPlan.Status_int EQ 4> selected</cfif>>Enterprise Postpaid</option>							
							<option value="0" <cfif RetVarGetPlan.Status_int EQ 0> selected</cfif>>Inactive</option>
						</select>
					</div>
				</div>				
				<div class="col-lg-6 col-md-6">
					<div class="form-group">
						<label for="Amount_dec">Monthly Amount ($/month)<span class="red">*</span></label>
						<input type="text" class="form-control validate[required,min[0],custom[number]]" id="Amount_dec" name="Amount_dec" value="#RetVarGetPlan.Amount_dec#" maxlength="12"  tabindex="1"/>
					</div>
				</div>
				<div class="col-lg-6 col-md-6">
					<div class="form-group">
						<label for="Order_int">Order<span class="red">*</span></label>
						<input type="text" class="form-control validate[required,min[1],custom[integer]]" id="Order_int" name="Order_int" value="#RetVarGetPlan.Order_int#" maxlength="12"  tabindex="7" />
					</div>
					
				</div>
				<div class="col-lg-6 col-md-6">
					<div class="form-group">
						<label for="YearlyAmount_dec">Yearly Amount ($/month)<span class="red">*</span></label>
						<input type="text" class="form-control validate[required,min[0],custom[number]]" id="YearlyAmount_dec" name="YearlyAmount_dec" value="#RetVarGetPlan.YearlyAmount_dec#" maxlength="12"  tabindex="2"/>
					</div>					
				</div>
				<div class="col-lg-6 col-md-6">		
					<div class="form-group">
						<label for="KeywordMinCharLimit_int">Keyword MinChar Limit<span class="red">*</span></label>
						<input type="text" class="form-control validate[required,min[1],custom[integer]]" id="KeywordMinCharLimit_int" name="KeywordMinCharLimit_int" value="#RetVarGetPlan.KeywordMinCharLimit_int#" maxlength="12"  tabindex="8"/>
					</div>							
				</div>
				<div class="col-lg-6 col-md-6">
					<div class="form-group">
						<label for="FirstSMSIncluded_int">SMS Included<span class="red">*</span></label>
						<input type="text" class="form-control validate[required,min[1],custom[integer]]" id="FirstSMSIncluded_int" name="FirstSMSIncluded_int" value="#RetVarGetPlan.FirstSMSIncluded_int#" maxlength="12"  tabindex="3" />
					</div>
				</div>
				<div class="col-lg-6 col-md-6">
					<div class="form-group">
						<label for="PriceKeywordAfter_dec">Keyword Price ($)<span class="red">*</span></label>
						<input type="text" class="form-control validate[required,min[0],custom[number]]" id="PriceKeywordAfter_dec" name="PriceKeywordAfter_dec" value="#RetVarGetPlan.PriceKeywordAfter_dec#" maxlength="12"  tabindex="9" />
					</div>					
				</div>
				<div class="col-lg-6 col-md-6">
					<div class="form-group">
						<label for="KeywordsLimitNumber_int">Keywords Limit Number<span class="red">*</span></label>
						<input type="text" class="form-control validate[required,min[1],custom[integer]]" id="KeywordsLimitNumber_int" name="KeywordsLimitNumber_int" value="#RetVarGetPlan.KeywordsLimitNumber_int#" maxlength="12"  tabindex="4" />
					</div>					
				</div>
				<div class="col-lg-6 col-md-6">
					<div class="form-group">
						<label for="PriceMsgAfter_dec">SMS Price (Cent)<span class="red">*</span></label>
						<input type="text" class="form-control validate[required,min[0],custom[number]]" id="PriceMsgAfter_dec" name="PriceMsgAfter_dec" value="#RetVarGetPlan.PriceMsgAfter_dec#" maxlength="12"  tabindex="10"/>
					</div>					
				</div>
				<div class="col-lg-6 col-md-6">
					<div class="form-group">
						<label for="ShortUrlsLimitNumber_int">ShortURLs Limit Number<span class="red">*</span></label>
						<input type="text" class="form-control validate[required,min[0],custom[integer]]" id="ShortUrlsLimitNumber_int" name="ShortUrlsLimitNumber_int" value="#RetVarGetPlan.ShortUrlsLimitNumber_int#" maxlength="12"  tabindex="5" />
					</div>
				</div>
				<div class="col-lg-6 col-md-6">
					<div class="form-group">
						<label for="MlpsImageCapacityLimit_bi">MLPs Image Capacity Limit<span class="red">*</span></label>
						<input type="text" class="form-control validate[required,min[0],custom[number]]" id="MlpsImageCapacityLimit_bi" name="MlpsImageCapacityLimit_bi" value="#RetVarGetPlan.MlpsImageCapacityLimit_bi#" maxlength="12"  tabindex="11" />
					</div>
				</div>
				<div class="col-lg-6 col-md-6">
					<div class="form-group">
						<label for="MlpsLimitNumber_int">MLPs Limit Number<span class="red">*</span></label>
						<input type="text" class="form-control validate[required,min[0],custom[integer]]" id="MlpsLimitNumber_int" name="MlpsLimitNumber_int" value="#RetVarGetPlan.MlpsLimitNumber_int#" maxlength="12"  tabindex="6" />
					</div>
				</div>
			</div>

			<!--- <div class="row">
				<div class="col-lg-10 col-md-10">
					<h2 class="page-title">#_Act# Plan</h2>
				</div>
				<div class="col-lg-2 col-md-2">
					<h2><button class="btn green-gd btn-re pull-right save-plan" type="submit">Save</button></h2>
				</div>
			</div> --->
		</form>
	</div>
</cfoutput>

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
    .addJs("/session/sire/assets/pages/scripts/admin_plan_form.js")
	.addJs("/public/sire/js/select2.min.js")
	.addCss("/public/sire/css/select2.min.css", true)
    .addCss("/public/sire/css/select2-bootstrap.min.css", true);
</cfscript>

<cfparam name="variables._title" default="Admin - Plan Form"/>
<cfinclude template="../views/layouts/master.cfm"/>