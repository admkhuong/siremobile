<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<div class="portlet light bordered ">
	<div class="portlet-body row">
		<div class="col-sm-12 col-md-4">
			<h2 class="page-title">Admin - Plans management</h2>
		</div>
		<div class="col-sm-12 col-md-8">
			<div id="box-filter">
				
			</div>
		</div>
		<div class="col-sm-12">
			<a href="/session/sire/pages/admin-plan-new" class="btn green-gd">ADD NEW PLAN</a>
			<br/><br/>
		</div>
		<div class="content-body">
			<div class="container-fluid">
				<div class="re-table">
					<div class="table-responsive">
						<table id="tblListPlan" class="table table-striped table-bordered table-hover">
							
						</table>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
    .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)  
    .addCss("/public/js/jquery-ui-1.11.4.custom/jquery-ui.css") 
    .addCss("/session/sire/css/filter_table.css ") 
    .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)
    .addJs("/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js")
    .addJs("/public/js/jquery.tmpl.min.js")
    .addJs("/public/sire/js/moment.min.js")
    .addJs("/session/sire/assets/global/scripts/filter-bar-generator.js")
    .addJs("/session/sire/assets/pages/scripts/admin_plan_management.js")
    ;
</cfscript>

<cfparam name="variables._title" default="Admin - Plans management"/>
<cfinclude template="../views/layouts/master.cfm"/>