<cfscript>
    createObject("component", "public.sire.models.helpers.layout")
        .addJs("/public/js/jquery.tmpl.min.js", true)
        .addJs("/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js")
        .addCss("/public/js/jquery-ui-1.11.4.custom/jquery-ui.css", true)
        .addCss("/session/sire/assets/global/plugins/uniform/css/uniform.default.css", true)
        .addJs('/session/sire/assets/global/scripts/filter-bar-generator.js', true)
        .addJs('/session/sire/assets/global/plugins/uniform/jquery.uniform.min.js', true)
        .addJs("/session/sire/assets/pages/scripts/admin-recuring-report.js");        
</cfscript>


<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfparam name="isDialog" default="0">
<cfparam name="isUpdateSuccess" default="">


<div class="portlet light bordered">
    <!--- Filter bar --->
    <div class="row">
        <div class="col-sm-3 col-md-4">
            <h2 class="page-title">Recurring Report</h2>
        </div>
        <div class="col-sm-9 col-md-8">
            <div id="box-filter" class="form-gd">

            </div>
        </div>
    </div>
    <!--- Listing --->
    <div class="row">
        <div class="col-md-12">
            <div class="re-table">
                <div class="table-responsive">
                    <table id="tblListEMS" class="table-striped dataTables_wrapper">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="RecurringLogModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:auto; max-width:900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Update Log</h4>

            </div>
            <div class="modal-body">

                <!---<div id="AdvanceScheduleContent">
                    <cfinclude template="../../ems/dsp_advance_schedule.cfm">
                </div>--->

			</div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<cfparam name="variables._title" default="Admin Recurring Report">
<cfinclude template="../views/layouts/master.cfm">

