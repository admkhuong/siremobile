<cfscript>
	createObject("component", "public.sire.models.helpers.layout")
		.addJs("/public/sire/js/select2.min.js", true)
		.addCss("/public/sire/css/select2.min.css", true)
		.addCss("/public/sire/css/select2-bootstrap.min.css", true)
		.addJs("/session/sire/assets/pages/scripts/admin-setting-system.js");
</cfscript>

<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfinvoke component="session.sire.models.cfc.billing" method="GetPaymentMethodSetting" returnvariable="RetGetPaymentMethodSetting"></cfinvoke>



<div class="portlet light bordered subscriber">
	<div class="row">
		<div class="col-md-12">
			<h2 class="page-title">Payment Settings</h2>
		</div>
	</div>
	<!--- Listing --->
	<div class="row">
		<div class="col-xs-12">
			<div class="form-gd">
				<form name="admin-sett-sys" id="admin-sett-sys">		
					<div class="row">
						<div class="col-xs-12 col-sm-4">
							<div class="form-group" style="padding-bottom: 25px;">
								<div class="radio-inline" id="rad-currentcard">
									<input type="radio" id="pm_worldpay" name="payment_method" value="1" <cfif RetGetPaymentMethodSetting.RESULT EQ 1>checked</cfif>/>
									<label for="pm_worldpay">WorldPay</label>
								</div>
								<div class="card-support-type">
									<img class="img-responsive" src="../assets/layouts/layout4/img/supported-payment-types.png" alt="">
								</div>						
							</div>
						</div>
						<div class="col-xs-8 col-sm-3">
							<b>Worldpay Transactions limit ($)</b></br>
							<input type="text" class="form-control transaction-limit validate[custom[number]]" placeholder="Worldpay Transactions limit" id="transaction-limit-1" name="transaction-limit-1">		
						</div>
						<div class="col-xs-4 col-sm-1">
							</br>
							<input type="buttion" class="btn green-gd form-control save-transaction-limit" data-payment-method="1" value="Apply"/>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-4">
							<div class="form-group" style="padding-bottom: 25px;">
								<div class="radio-inline" id="rad-currentcard">
									<input type="radio" id="pm_paypal" name="payment_method" value="2"<cfif RetGetPaymentMethodSetting.RESULT EQ 2>checked</cfif>/>
									<label for="pm_paypal">Paypal</label>
								</div>
								<div class="card-support-type">
									<img class="img-responsive" src="../assets/layouts/layout4/img/supported-payment-types.png" alt="">
								</div>
							</div>
						</div>
						<div class="col-xs-8 col-sm-3">
							<b>Paypal Transactions limit ($)</b></br>
							<input type="text" class="form-control transaction-limit validate[custom[number]]" placeholder="Paypal Transactions limit" id="transaction-limit-2" name="transaction-limit-2">		
						</div>
						<div class="col-xs-4 col-sm-1">
							</br>
							<input type="buttion" class="btn green-gd form-control save-transaction-limit" data-payment-method="2" value="Apply"/>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-4">
							<div class="form-group" style="padding-bottom: 25px;">
								<div class="radio-inline" id="rad-currentcard">
									<input type="radio" id="pm_mojo" name="payment_method" value="3"<cfif RetGetPaymentMethodSetting.RESULT EQ 3>checked</cfif>/>
									<label for="pm_paypal">Mojo</label>
								</div>
								<div class="card-support-type">
									<img class="img-responsive" src="../assets/layouts/layout4/img/supported-payment-types.png" alt="">
								</div>
							</div>
						</div>
						<div class="col-xs-8 col-sm-3">
							<b>Mojo Transactions limit ($)</b></br>
							<input type="text" class="form-control transaction-limit validate[custom[number]]" placeholder="Mojo Transactions limit" id="transaction-limit-3" name="transaction-limit-3">		
						</div>
						<div class="col-xs-4 col-sm-1">
							</br>
							<input type="buttion" class="btn green-gd form-control save-transaction-limit" data-payment-method="3" value="Apply"/>
						</div>
					</div>		
					<div class="row">
						<div class="col-xs-12 col-sm-4">
							<div class="form-group" style="padding-bottom: 25px;">
								<div class="radio-inline" id="rad-currentcard">
									<input type="radio" id="pm_ttapp" name="payment_method" value="4"<cfif RetGetPaymentMethodSetting.RESULT EQ 4>checked</cfif>/>
									<label for="pm_ttapp">Total Apps</label>
								</div>
								<div class="card-support-type">
									<img class="img-responsive" src="../assets/layouts/layout4/img/supported-payment-types.png" alt="">
								</div>
							</div>
						</div>
						<div class="col-xs-8 col-sm-3">
							<b>Total Apps Transactions limit ($)</b></br>
							<input type="text" class="form-control transaction-limit validate[custom[number]]" placeholder="Total Apps Transactions limit" id="transaction-limit-4" name="transaction-limit-4">		
						</div>
						<div class="col-xs-4 col-sm-1">
							</br>
							<input type="buttion" class="btn green-gd form-control save-transaction-limit" data-payment-method="4" value="Apply"/>
						</div>
					</div>				
				</form>
			</div>
		</div>	
	</div>
</div>


<cfparam name="variables._title" default="Payment Settings">
<cfinclude template="../views/layouts/master.cfm">

