<cfscript>
	createObject("component", "public.sire.models.helpers.layout")
	.addJs("/public/js/jquery.tmpl.min.js", true)
	.addJs("/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js")
	.addCss("/public/js/jquery-ui-1.11.4.custom/jquery-ui.css", true)
	.addCss("/session/sire/assets/global/plugins/uniform/css/uniform.default.css", true)
	.addJs('/session/sire/assets/global/scripts/filter-bar-generator.js', true)
	.addJs('/session/sire/assets/global/plugins/uniform/jquery.uniform.min.js', true)
	.addCss("/session/sire/assets/global/plugins/Chartist/chartist.min.css", true)
	.addJs("/session/sire/assets/pages/scripts/admin-shortcode-manage.js");
</cfscript>

<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfinvoke method="GetSystemDefaultShortCode" component="public.sire.models.cfc.shortcode" returnvariable="SHORTCODE"/>

<cfoutput>

<div class="portlet light bordered">
	<div class="row">
		<div class="col-sm-5 col-md-5 col-xs-12">
			<h2 class="page-title">ShortCode Manage</h2>
		</div>
		<div class="col-md-7 col-sm-7 col-xs-12">
			<div id="box-filter" class="form-gd">

			</div>
		</div>
	</div>
	<!--- Listing --->
	<div class="row">
		<div class="col-md-12">
			<div class="re-table">
				<div class="table-responsive">
					<table id="ShortCodeList" class="table table-striped table-responsive">
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<style type="text/css">
	input.right {
		text-align: right;
	}
	th.th-center {
		text-align: center;
	}
</style>

</cfoutput>

<cfparam name="variables._title" default="Admin ShortCode Manage">
<cfinclude template="../views/layouts/master.cfm">