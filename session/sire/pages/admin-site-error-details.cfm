<cfparam name="isDialog" default="0">
<cfparam name="errid">


<cfset rootUrl = ""/>
<cfset SessionPath = "session"/>
<cfset PublicPath = "public"/>




<cfset errorLogData = {} >
<cfinvoke component="public.sire.models.cfc.error_logs" method="getErrorLogDetailById" returnvariable="errorLogData">
     <cfinvokeargument name="errid" value="#errid#">
</cfinvoke>


<cfif errorLogData.RXRESULTCODE NEQ 1>
    <cfcookie name="ERROR_LOG_MESSAGE" value="#errorLogData.MESSAGE#">
    <cflocation url = "/session/sire/pages/admin-site-errors?existflag=0" addtoken="false" >
</cfif>

<cfif NOT Session.USERID GT 0>
    <cfexit><!---exit if session expired --->
</cfif>


<cfoutput>
    <script src="#rootUrl#/#SessionPath#/campaign/mcid/js/jquery.numbericbox.js" type="text/javascript" ></script>
<!---   <script language="javascript" src="#rootUrl#/#publicPath#/js/sms/ui.spinner.js"></script>
    <link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/sms/ui.spinner.css"/>
    <script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.selectmenu.js"></script>--->

    <script src="#rootUrl#/#PublicPath#/js/jquery.jplayer.min.js" type="text/javascript"></script>
    <script src="#rootUrl#/#PublicPath#/js/jquery.jplayer.inspector.js" type="text/javascript"></script>

    <script src="#rootUrl#/#PublicPath#/js/jquery.transform.js" type="text/javascript"></script>
    <script src="#rootUrl#/#PublicPath#/js/jquery.grab.js" type="text/javascript"></script>
    <script src="#rootUrl#/#PublicPath#/js/mod.csstransforms.min.js" type="text/javascript"></script>
    <script src="#rootUrl#/#PublicPath#/js/circle.player.js" type="text/javascript"></script>
    <link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.selectmenu.css" />
    <link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/ems/emergencymessages.css"/>
    <link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/circle.skin/circle.player4.css"/>
</cfoutput>

<div class="bootbox">
<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Error log detail</h4>     
</div>


<!---wrap the page content do not style this--->
<div class="modal-body">

<cfset errorDetail = errorLogData.DATA>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="fname" class="col-sm-3 control-label">Error message</label>
                    <div class="col-sm-9">
                        <input type="text" value="<cfoutput>#errorDetail.ErrorMessage_txt#</cfoutput>" disabled="disabled" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="fname" class="col-sm-3 control-label">Request URL</label>
                    <div class="col-sm-9">
                        <input type="text" value="<cfoutput>#errorDetail.RequestUrl_txt#</cfoutput>" disabled="disabled" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="fname" class="col-sm-3 control-label">User Id</label>
                    <div class="col-sm-9">
                        <input type="text" value="<cfoutput>#errorDetail.UserId_int#</cfoutput>" disabled="disabled" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="fname" class="col-sm-3 control-label">User IP</label>
                    <div class="col-sm-9">
                        <input type="text" value="<cfoutput>#errorDetail.UserIp_vch#</cfoutput>" disabled="disabled" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="fname" class="col-sm-3 control-label">Error detail</label>
                    <div class="col-sm-9" style="margin-bottom: 10px;">
                        <textarea disabled="disabled" rows="4" cols="120" class="form-control"><cfoutput>#errorDetail.ErrorDetail_txt#</cfoutput></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="fname" class="col-sm-3 control-label">HTTP User Agent</label>
                    <div class="col-sm-9">
                        <input type="text" value="<cfoutput>#errorDetail.HttpUserAgent_txt#</cfoutput>" disabled="disabled" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="fname" class="col-sm-3 control-label">HTTP Referer</label>
                    <div class="col-sm-9">
                        <input type="text" value="<cfoutput>#errorDetail.HttpReferer_txt#</cfoutput>" disabled="disabled" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="fname" class="col-sm-3 control-label">Status</label>
                    <div class="col-sm-9" style="margin-bottom: 10px;">
                        <select id="input-status" name="status" class="form-control">
                            <option value="0" <cfif errorDetail.Status_int EQ 0>selected="selected"</cfif>>Open</option>
                            <!--- <option value="1" <cfif errorDetail.Status_int EQ 1>selected="selected"</cfif>>In Progess</option>
                            <option value="2" <cfif errorDetail.Status_int EQ 2>selected="selected"</cfif>>Tech Review</option>
                            <option value="3" <cfif errorDetail.Status_int EQ 3>selected="selected"</cfif>>QA Review</option> --->
                            <option value="4" <cfif errorDetail.Status_int EQ 4>selected="selected"</cfif>>Close</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="fname" class="col-sm-3 control-label">Created date</label>
                    <div class="col-sm-9">
                        <input type="text" value="<cfoutput>#LSDateFormat(errorDetail.Created_dt, 'yyyy-mm-dd')# #LSTimeFormat(errorDetail.Created_dt, 'HH:mm:ss')#</cfoutput>" disabled="disabled" class="form-control"/>
                    </div>
                </div>
            </div>
        </div>
       

</div>
<div class="modal-footer">

    <button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal" >Cancel</button>
     
    <button type="button" class="btn btn-success btn-success-custom" id="inpSubmit" onclick="updateErrorStatus()">Save</button>
   
</div>  
</div>
<script type="text/javascript">
$(document).ready(function(){
});
function updateErrorStatus() {
    $.ajax({
        type: "POST",
        url: '<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/sire/models/cfc/error_logs.cfc?method=updateErrorStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
        dataType: 'json',
        data: {
            status : $('#input-status').val(),
            errid: <cfoutput>#errid#</cfoutput>
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        },
        success: function(data){
            if (data.SUCCESS) {
                //window.location.href="/session/sire/pages/admin-site-errors?isupdatesuccess=1";
                bootbox.dialog({
                    message: 'Save "error log" successfully!',
                    title: "Admin Site Error log",
                    buttons: {
                        success: {
                            label: "Ok",
                            className: "btn btn-medium btn-success-custom",
                            callback: function() {
                                $('#AdminErrorModal').modal('hide');
                                window.location.href="/session/sire/pages/admin-site-errors?isupdatesuccess=1";
                            }
                        }
                    }
                });
                
            } else {
                //location.reload();
                bootbox.dialog({
                    message: d.MESSAGE,
                    title: "Admin Site Error log",
                    buttons: {
                        success: {
                            label: "Ok",
                            className: "btn btn-medium btn-success-custom",
                            callback: function() {
                            }
                        }
                    }
                });
            }
        }
    });

    return false;
}
</script>