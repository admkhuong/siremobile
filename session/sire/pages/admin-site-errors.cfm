<cfscript>
    createObject("component", "public.sire.models.helpers.layout")
        .addJs("/session/sire/assets/pages/scripts/admin-site-errors.js")
        .addJs("/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js", true)
        .addJs("../assets/global/plugins/daterangepicker/moment.min.js", true)
        .addJs("../assets/global/plugins/daterangepicker/moment-timezone-with-data.js", true)
        .addJs("/session/sire/assets/global/scripts/filter-bar-generator.js", true)
        .addCss("/public/js/jquery-ui-1.11.4.custom/jquery-ui.css", true);
</cfscript>

<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfparam name="isDialog" default="0">
<cfparam name="isUpdateSuccess" default="">


<div class="portlet light bordered">
    <!--- Filter bar --->
    <div class="row">
        <div class="col-sm-3 col-md-4">
            <h2 class="page-title">Administration - Site errors</h2>
        </div>
        <div class="col-sm-9 col-md-8">
            <div id="box-filter" class="form-gd">

            </div>
        </div>
    </div>
    <!--- Listing --->
    <div class="row">
        <div class="col-md-12">
            <div class="re-table">
                <div class="table-responsive">
                    <table id="tblSiteErrorList" class="table table-responsive table-striped">
                      
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Modal -->
<div class="modal fade" id="AdminErrorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:auto; max-width:900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Error log detail</h4>

            </div>
            <div class="modal-body">



            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<cfparam name="variables._title" default="Admin Site Errors">
<cfinclude template="../views/layouts/master.cfm">

