<cfparam name="inpDate" default="#Dateformat(NOW(),'YYYY-MM-DD')#">

<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfinclude template="/session/cfc/csc/constants.cfm">

<!--- Load Setting Defaults --->
<cfset DBSourceManagementRead = "Bishop" />


<!---
	MO Counts
	MT Counts
	Batch Counts
	Sessions
	
	
	Error Counts
	Opt Out Counts
	Opt In Counts
	Blast Queue
	Hold Queue
		
--->

<div class="portlet light bordered ">
	<div class="portlet-body">
		<div class="row">
			<div class="col-md-6 col-lg-6 col-xs-12">
				<h4 class="portlet-heading">Admin - System Usage Report</h4>
			</div>
			<div class="col-md-6 col-lg-6 col-xs-12" style="padding-top: 10px">
				<div class="form-group">
					<div class="col-sm-4 col-lg-4">
						<label for="ShortCodeId" class="control-label pull-right" style="font-size: 18px">Short Code <span class="text-danger">*</span></label>
					</div>
					<div class="col-sm-8 col-lg-8">
						<!--- Get Select Box of Short Code --->
						<cftry>
							<!----- used when login using username/email and password ---->
							<CFQUERY name="getShortcode" datasource="#Session.DBSourceREAD#">
								
								SELECT 
									ShortCode_vch,
									ShortCodeId_int,
									IsDefault_ti
								FROM 
									sms.shortcode													
							</CFQUERY>

							<select name="ShortCodeId" id="ShortCodeId" size="5" style="width: 100%;" class="form-control validate[required,custom[noHTML]]">
								<cfif getShortcode.RecordCount GT 0>
									<option value="0" selected>All</option>
									<cfloop query="getShortcode">
										<cfoutput><option value="#getShortcode.ShortCodeId_int#" style="">#getShortcode.ShortCodeId_int# - #getShortcode.ShortCode_vch#</option></cfoutput>
									</cfloop>
								
								<cfelse>
								
									<option value="0" style="">0 - No Short Code Found!</option>
								
								</cfif>	 
					
							</select>
 
						<cfcatch TYPE="any">
							
						</cfcatch>
						</cftry>
					</div>
				</div>
			</div>
		</div>
		
		<cfoutput>
		<div class="row content-simon-new">

			<div class="col-lg-12 col-md-12">
				<p class="mlp-list-count title-simon-row"><b class="mlp-numb">Summary Info User Accounts</b></p>
				<!--- Get New Users Last 1 days --->
				<cfquery name="GetTotalUsersLast1" datasource="#Session.DBSourceEBM#">
					SELECT
						COUNT(UserId_int) AS UserCount
					FROM
						simpleobjects.useraccount    
					WHERE
						Created_dt > CURDATE()                    
				</cfquery>  
						
				<div class="col-lg-12 col-md-12">
					Total new Sire System Users (Today: #LSDateFormat(Now(), 'full')#): <b id="total-user">#GetTotalUsersLast1.UserCount#</b>	            
				</div>
			</div>
									 

			
			
			<div class="col-lg-12 col-md-12">
				<p class="mlp-list-count title-simon-row"><b class="mlp-numb">Summary Info Batches</b></p>
				<!--- Get New Batches Last 1 days --->
				<cfquery name="GetTotalBatchesLast1" datasource="#Session.DBSourceEBM#">
					SELECT
						COUNT(BatchId_bi) AS BatchCount
					FROM
						simpleobjects.batch    
					WHERE
						Created_dt > CURDATE()                     
				</cfquery>  
						
				<div class="col-lg-12 col-md-12">
					Total new Sire System Batches (Today: #LSDateFormat(Now(), 'full')#): <b id="total-batch">#GetTotalBatchesLast1.BatchCount#</b>	            
				</div>
			</div>
						   

			<div class="col-lg-12 col-md-12">
				<p class="mlp-list-count title-simon-row"><b class="mlp-numb">Summary Info MOs</b></p>
				<!--- Get New MOs Last 1 days --->
				<cfquery name="GetTotalMOsLast1" datasource="#Session.DBSourceEBM#">
					SELECT
						COUNT(moInboundQueueId_bi) AS MOCount
					FROM
						simplequeue.moinboundqueue  
					WHERE
						Created_dt > CURDATE()                     
				</cfquery>  
						
				<div class="col-lg-12 col-md-12">
					Total new Sire System MOs (Today: #LSDateFormat(Now(), 'full')#): <b id="total-mos">#GetTotalMOsLast1.MOCount#</b>	            
				</div>
			</div>
			

			<div class="col-lg-12 col-md-12">
				<p class="mlp-list-count title-simon-row"><b class="mlp-numb">Summary Info MTs</b></p>
				<!--- Get New MTs Last 1 days --->
				<cfquery name="GetTotalMTsLast1" datasource="#Session.DBSourceEBM#">
					SELECT
						COUNT(IREResultsId_bi) AS MTCount
					FROM
						simplexresults.ireresults 
					WHERE
						Created_dt > CURDATE()
					AND	
						IREType_int = 1	                    
				</cfquery>  
						
				<div class="col-lg-12 col-md-12">
					Total new Sire System MTs (Today: #LSDateFormat(Now(), 'full')#): <b id="total-mts">#GetTotalMTsLast1.MTCount#</b>	            
				</div>
			</div>

			<div class="col-lg-12 col-md-12">
				<p class="mlp-list-count title-simon-row"><b class="mlp-numb">Summary Info Sessions</b></p>
								
				<!--- Get New Sessions Last 1 days --->
				<cfquery name="GetTotalSessionsLast1" datasource="#Session.DBSourceEBM#">
					SELECT
						COUNT(SessionId_bi) AS SessionCount
					FROM
						simplequeue.sessionire 
					WHERE
						Created_dt > CURDATE() 
				</cfquery>  
						
				<div class="col-lg-12 col-md-12">
					Total new Sire System Sessions (Today: #LSDateFormat(Now(), 'full')#): <b id="total-session">#GetTotalSessionsLast1.SessionCount#</b>	            
				</div>
			</div>

			<div class="col-lg-12 col-md-12">
				<p class="mlp-list-count title-simon-row"><b class="mlp-numb">Summary Info Active Sessions</b></p>
									
				<!--- Get New Sessions Last 1 days --->
				<cfquery name="GetTotalActiveSessionsLast1" datasource="#Session.DBSourceEBM#">
					SELECT
						COUNT(SessionId_bi) AS SessionCount
					FROM
						simplequeue.sessionire 
					WHERE
						Created_dt > CURDATE()
					AND	
						SessionState_int < 4 
				</cfquery>  
						
				<div class="col-lg-12 col-md-12">
					Total new Sire System Active Sessions (Today: #LSDateFormat(Now(), 'full')#): <b id="total-active-session">#GetTotalActiveSessionsLast1.SessionCount#</b>	            
				</div>
			</div>			

			<div class="col-lg-12 col-md-12">
				<p class="mlp-list-count title-simon-row"><b class="mlp-numb">Summary Info Web Site Error Log</b></p>
				<!--- Get New SiteErrorLogs Last 1 days --->
				<cfquery name="GetTotalSiteErrorLogsLast1" datasource="#Session.DBSourceEBM#">
					SELECT
						COUNT(SireErrorLogsId_int) AS SiteErrorLogCount
					FROM
						simpleobjects.sire_error_logs  
					WHERE
						Created_dt > CURDATE()    
					AND
						Status_int = 0	                 
				</cfquery>  
						
				<div class="col-lg-12 col-md-12">
					Total new Sire System SiteErrorLogs (Today: #LSDateFormat(Now(), 'full')#): <b>#GetTotalSiteErrorLogsLast1.SiteErrorLogCount#</b>	            
				</div>
			</div>
  
			</div>
		</div>
	</div>

<div class="portlet light bordered ">
	<div class="portlet-body">
		<h4 class="portlet-heading">Usage Details</h4>
		
		<div class="row content-simon-new">            
				<!--- Get total usage by type Last 1 days - top 100 --->
			<cfquery name="GetTotalUsageLast1" datasource="#DBSourceManagementRead#">
				SELECT
					UserId_int,
					COUNT(*) AS TotalCount,
					IreType_int
				FROM 
					simplexresults.ireresults
				WHERE
					Created_dt > <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#inpDate#">
				GROUP BY
					UserId_int,IreType_int
				ORDER BY
					TotalCount DESC   
				LIMIT 100	
					
			</cfquery>  
			
			
			<style>
				
				td, th
				{
					padding: 1em;
					
				}
			</style>	
					
			<div class="col-lg-12 col-md-12">
											
				<h4>Top 100 system usages by type and user (Today: #LSDateFormat(inpDate, 'full')#): <b>#GetTotalBatchesLast1.BatchCount#</b></h4>
				
				<h4>Legend</h4>
				<table border="1">
					<tr>
						<td>IREMESSAGETYPE_MT</td>
						<td>#IREMESSAGETYPE_MT#</td>
					</tr>
					<tr>
						<td>IREMESSAGETYPE_MO</td>
						<td>#IREMESSAGETYPE_MO#</td>
					</tr>	
					<tr>
						<td>IREMESSAGETYPE_INTERVAL_TIMEOUT</td>
						<td>#IREMESSAGETYPE_INTERVAL_TIMEOUT#</td>
					</tr>	
					<tr>
						<td>IREMESSAGETYPE_API_TRIGGERED</td>
						<td>#IREMESSAGETYPE_API_TRIGGERED#</td>
					</tr>	
					<tr>
						<td>IREMESSAGETYPE_IRE_PROCESSING</td>
						<td>#IREMESSAGETYPE_MO#</td>
					</tr>	
					<tr>
						<td>IREMESSAGETYPE_IRE_PROCESSING</td>
						<td>#IREMESSAGETYPE_MO#</td>
					</tr>	
					<tr>
						<td>IREMESSAGETYPE_IRE_CP_API_CALL</td>
						<td>#IREMESSAGETYPE_IRE_CP_API_CALL#</td>
					</tr>	
				</table>	

				<div class="col-lg-12 col-md-12" style="margin-top: 2em; margin-left: -15px; margin-bottom: 2em;">
					<div class="row">
						<div class="form-group">
							<label for="inpBatchId" class="col-sm-3 col-lg-3 control-label">List Users <span class="text-danger">*</span></label>
							<div class="col-sm-9 col-lg-6">
							
								<!--- Get Select Box of Active Batches --->
								<cftry>      
																	
									<!----- used when login using username/email and password ---->
									<CFQUERY name="getUser" datasource="#Session.DBSourceREAD#">
										
										SELECT 
											UserId_int,
											PrimaryPhoneStr_vch,
											EmailAddress_vch,
											FirstName_vch,
											LastName_vch
										FROM 
											simpleobjects.useraccount
										WHERE
											Active_int = 1
																																
									</CFQUERY>

									<select name="IUserId" id="IUserId" size="5" style="width: 100%;" class="form-control validate[required,custom[noHTML]]">
										<cfif getUser.RecordCount GT 0>
										
											<cfloop query="getUser">
												<cfoutput><option value="#getUser.UserId_int#" style="" <cfif getUser.UserId_int EQ Session.USERID>selected</cfif>   >#getUser.UserId_int# - #getUser.EmailAddress_vch#</option></cfoutput>
											</cfloop>
										
										<cfelse>
										
											<option value="0" style="">0 - No Users Found!</option>
										
										</cfif>	 
							
									</select>
		 
								<cfcatch TYPE="any">
									
								</cfcatch>
								</cftry>     
							</div>
						</div>
					</div>						
				</div>

				<table border="1" class="table-simon-add">
					<thead>
						<tr>
							<th>User ID</th>
							<th>IRE Type</th>
							<th>Count</th>			            
						</tr>
					</thead>
					<tbody id="total-usage">
						<cfloop query="GetTotalUsageLast1">
							<tr>
								<td>#GetTotalUsageLast1.UserId_int#</td>
								<td>#GetTotalUsageLast1.IreType_int#</td>
								<td>#GetTotalUsageLast1.TotalCount#</td>
							</tr>
						</cfloop>
					</tbody>
				</table>
									
			</div>

			
		
												
		</div>
		
		
		
			<!--- Get total usage by type Last 1 days - top 100 --->
			<cfquery name="GetContactQueueStatus1" datasource="#DBSourceManagementRead#">
				
				SELECT 
					COUNT(*) AS TOTALCOUNT, 
					UserId_int, 
					DTSStatusType_ti  
				FROM 
					simplequeue.contactqueue
				WHERE
					Scheduled_dt > <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#inpDate#">
				GROUP BY
					UserId_int, DTSStatusType_ti
				ORDER BY
					UserId_int, DTSStatusType_ti ASC   
				LIMIT 100	
					
			</cfquery>  
			

			<div class="col-lg-8 col-md-8">
											
			 <h4>Contact Queue Status</h4>
				
				 <table border="1">
						<thead>
							<tr>
								<th>User ID</th>
								<th>IRE Type</th>
								<th>Count</th>			            
							</tr>	
						</thead>
						<tbody id="contact-queue-status">
							<cfloop query="GetContactQueueStatus1">
								<tr>
									<td>#GetContactQueueStatus1.UserId_int#</td>
									<td>#GetContactQueueStatus1.DTSStatusType_ti#</td>
									<td>#GetContactQueueStatus1.TOTALCOUNT#</td>
								</tr>
							</cfloop>
						</tbody>
				 </table>

			</div>

		</cfoutput>
	<div class="clearfix">	</div>
	</div>

</div>
		


<!--- https://select2.github.io/  --->
<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/public/sire/css/select2.min.css">
</cfinvoke>

<!--- https://select2.github.io/  --->
<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/public/sire/css/select2-bootstrap.min.css">
</cfinvoke>

<!--- https://select2.github.io/  --->
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/select2.min.js">
</cfinvoke>
		

<cfparam name="variables._title" default="System Usage">
<cfinclude template="../views/layouts/master.cfm">



<script type="text/javascript">
		
	$(document).ready(function()
	{							
		// Make all select boxes select2 style
		$("SELECT").select2( { theme: "bootstrap"} );
												

		$("body").on('change', '#ShortCodeId', function(event) {
			event.preventDefault();
			var id = $(this).val();
			$.ajax({
				url: '/session/sire/models/cfc/admin.cfc?method=SystemUsageReportTodayWithShortcode&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				data: {inpShortCodeId: id},
			})
			.done(function(data) {
				if (parseInt(data.RXRESULTCODE) == 1) {
					$("#total-user").text(data.TOTALUSERLAST1);
					$("#total-batch").text(data.TOTALBATCHESLAST1);
					$("#total-mos").text(data.TOTALMOSLAST1);
					$("#total-mts").text(data.TOTALMTSLAST1);
					$("#total-session").text(data.TOTALSESSIONSLAST1);
					$("#total-active-session").text(data.TOTALACTIVESESSIONLAST1);
					
					$("#total-usage").html('');
					for (var i = 0; i < data.TOTALUSAGELAST1.length; i++) {
						var html = '<tr>' +
									'<td>'+data.TOTALUSAGELAST1[i].USERID+'</td>'+
									'<td>'+data.TOTALUSAGELAST1[i].IRETYPE+'</td>'+
									'<td>'+data.TOTALUSAGELAST1[i].TOTALCOUNT+'</td>'+
								'</tr>';
						$("#total-usage").append(html);
					}

					$("#contact-queue-status").html('');
					for (var i = 0; i < data.CONTACTQUEUESTATUS1.length; i++) {
						var html = '<tr>' +
									'<td>'+data.CONTACTQUEUESTATUS1[i].USERID+'</td>'+
									'<td>'+data.CONTACTQUEUESTATUS1[i].STATUS+'</td>'+
									'<td>'+data.CONTACTQUEUESTATUS1[i].TOTALCOUNT+'</td>'+
								'</tr>';
						$("#contact-queue-status").append(html);
					}
				} else {
					alertBox("Get report failed!");
				}
			})
			.fail(function(e, msg) {
				console.log("error: "+msg);
			});
			
		});
	});




</script>
