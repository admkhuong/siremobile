

<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfinclude template="/session/cfc/csc/constants.cfm">

<!--- Load Setting Defaults --->
<cfset DBSourceManagementRead = "Bishop" />

<!---
	MO Counts
	MT Counts
	Batch Counts
	Sessions
	
	
	Error Counts
	Opt Out Counts
	Opt In Counts
	Blast Queue
	Hold Queue
		
--->


<div class="portlet light bordered ">
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-xs-12">
                <h4 class="portlet-heading">Admin - System Usage Report</h4>
            </div>
            <div class="col-md-6 col-lg-6 col-xs-12" style="padding-top: 10px">
                <div class="form-group">
                    <div class="col-sm-4 col-lg-4">
                        <label for="ShortCodeId" class="control-label pull-right" style="font-size: 18px">Short Code <span class="text-danger">*</span></label>
                    </div>
                    <div class="col-sm-8 col-lg-8">
                        <cftry>
                            <CFQUERY name="getShortcode" datasource="#Session.DBSourceREAD#">
                                
                                SELECT 
                                    ShortCode_vch,
                                    ShortCodeId_int,
                                    IsDefault_ti
                                FROM 
                                    sms.shortcode                                                
                            </CFQUERY>

                            <select name="ShortCodeId" id="ShortCodeId" size="5" style="width: 100%;" class="form-control validate[required,custom[noHTML]]">
                                <cfif getShortcode.RecordCount GT 0>
                                    <option value="0" selected>All</option>
                                    <cfloop query="getShortcode">
                                        <cfoutput><option value="#getShortcode.ShortCodeId_int#" style="">#getShortcode.ShortCodeId_int# - #getShortcode.ShortCode_vch#</option></cfoutput>
                                    </cfloop>
                                
                                <cfelse>
                                
                                    <option value="0" style="">0 - No Short Code Found!</option>
                                
                                </cfif>  
                    
                            </select>
 
                        <cfcatch TYPE="any">
                            
                        </cfcatch>
                        </cftry>
                    </div>
                </div>
            </div>
        </div>
		<cfoutput>
		<div class="row content-simon-new">
            
			<div class="col-lg-12 col-md-12">
                <!--- QUERY  --->
        			<!--- Get Total User Counts--->
                    <cfquery name="GetTotalUsers" datasource="#DBSourceManagementRead#">
                        SELECT
                            COUNT(UserId_int) AS UserCount
                        FROM
                            simpleobjects.useraccount                                                 
                    </cfquery>  
                    	            
        			<!--- Get New Users Last 30 days --->
                    <cfquery name="GetTotalUsersLast30" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(UserId_int) AS UserCount
                        FROM
                            simpleobjects.useraccount    
                        WHERE
                        	Created_dt > '#LSDateFormat(DATEADD('d', -30, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -30, NOW()), 'HH:mm:ss')#'                     
                    </cfquery>  

                    <!--- Get New Users Last 7 days --->
                    <cfquery name="GetTotalUsersLast7" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(UserId_int) AS UserCount
                        FROM
                            simpleobjects.useraccount    
                        WHERE
                        	Created_dt > '#LSDateFormat(DATEADD('d', -7, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -7, NOW()), 'HH:mm:ss')#'                     
                    </cfquery>  
                       
                    <!--- Get New Users Last 1 days --->
                    <cfquery name="GetTotalUsersLast1" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(UserId_int) AS UserCount
                        FROM
                            simpleobjects.useraccount    
                        WHERE
                        	Created_dt > '#LSDateFormat(DATEADD('d', -1, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -1, NOW()), 'HH:mm:ss')#'                     
                    </cfquery> 
                <!--- END QUERY  --->
                <p class="mlp-list-count title-simon-row"><b class="mlp-numb">Summary Info User Accounts</b></p>

                <div class="col-lg-12 col-md-12 content-simon-detail">
                    <p>Total number of Sire System Users: <b id="total-users">#GetTotalUsers.UserCount#</b></p>
                    <p>Total new Sire System Users (Last 30 Days): <b id="total-users-last-30">#GetTotalUsersLast30.UserCount#</b></p>  
                    <p>Total new Sire System Users (Last 7 Days): <b id="total-users-last-7">#GetTotalUsersLast7.UserCount#</b></p>  
                    <p>Total new Sire System Users (Last 24 hours): <b id="total-users-last-1">#GetTotalUsersLast1.UserCount#</b>   </p>     
                </div>
            </div>

                    
        
            <div class="col-lg-12 col-md-12">

    			<!--- QUERY  --->
        			<!--- Get Total Batch Counts--->
                    <cfquery name="GetTotalBatches" datasource="#DBSourceManagementRead#">
                        SELECT
                            COUNT(BatchId_bi) AS BatchCount
                        FROM
                            simpleobjects.batch                                              
                    </cfquery>  
                    	           
                    
                    <!--- Get New Batches Last 30 days --->
                    <cfquery name="GetTotalBatchesLast30" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(BatchId_bi) AS BatchCount
                        FROM
                            simpleobjects.batch    
                        WHERE
                        	Created_dt > '#LSDateFormat(DATEADD('d', -30, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -30, NOW()), 'HH:mm:ss')#'                     
                    </cfquery>  

                                
                    <!--- Get New Batches Last 7 days --->
                    <cfquery name="GetTotalBatchesLast7" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(BatchId_bi) AS BatchCount
                        FROM
                            simpleobjects.batch    
                        WHERE
                        	Created_dt > '#LSDateFormat(DATEADD('d', -7, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -7, NOW()), 'HH:mm:ss')#'                     
                    </cfquery>  
                                
                    <!--- Get New Batches Last 1 days --->
                    <cfquery name="GetTotalBatchesLast1" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(BatchId_bi) AS BatchCount
                        FROM
                            simpleobjects.batch    
                        WHERE
                        	Created_dt > '#LSDateFormat(DATEADD('d', -1, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -1, NOW()), 'HH:mm:ss')#'                     
                    </cfquery>  
                <!--- END QUERY  --->
                <p class="mlp-list-count title-simon-row"><b class="mlp-numb">Summary Info Batches</b></p>

                <div class="col-lg-12 col-md-12 content-simon-detail">
                    <p>Total number of Sire System Batches: <b id="total-batches">#GetTotalBatches.BatchCount#</b></p>  
                    <p>Total new Sire System Batches (Last 30 Days): <b id="total-batches-last-30">#GetTotalBatchesLast30.BatchCount#</b></p>  
                    <p>Total new Sire System Batches (Last 7 Days): <b id="total-batches-last-7">#GetTotalBatchesLast7.BatchCount#</b></p>  
                    <p>Total new Sire System Batches (Last 24 hours): <b id="total-batches-last-1">#GetTotalBatchesLast1.BatchCount#</b>  </p>  
                </div>

            </div>
                    
            <div class="col-lg-12 col-md-12">

                <!--- QUERY --->
        			<!--- Get Total MOs Counts--->
                    <cfquery name="GetTotalMOs" datasource="#DBSourceManagementRead#">
                        SELECT
                            COUNT(moInboundQueueId_bi) AS MOCount
                        FROM
                            simplequeue.moinboundqueue                                              
                    </cfquery>  

                    <!--- Get New MOs Last 30 days --->
                    <cfquery name="GetTotalMOsLast30" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(moInboundQueueId_bi) AS MOCount
                        FROM
                            simplequeue.moinboundqueue    
                        WHERE
                        	Created_dt > '#LSDateFormat(DATEADD('d', -30, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -30, NOW()), 'HH:mm:ss')#'                     
                    </cfquery>  

                    <!--- Get New MOs Last 7 days --->
                    <cfquery name="GetTotalMOsLast7" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(moInboundQueueId_bi) AS MOCount
                        FROM
                            simplequeue.moinboundqueue    
                        WHERE
                        	Created_dt > '#LSDateFormat(DATEADD('d', -7, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -7, NOW()), 'HH:mm:ss')#'                     
                    </cfquery>  
                                
                    <!--- Get New MOs Last 1 days --->
                    <cfquery name="GetTotalMOsLast1" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(moInboundQueueId_bi) AS MOCount
                        FROM
                            simplequeue.moinboundqueue  
                        WHERE
                        	Created_dt > '#LSDateFormat(DATEADD('d', -1, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -1, NOW()), 'HH:mm:ss')#'                     
                    </cfquery>  
                <!--- END QUERY  --->

                <p class="mlp-list-count title-simon-row"><b class="mlp-numb">Summary Info MOs</b></p>
                <div class="col-lg-12 col-md-12 content-simon-detail">
                    <p>Total number of Sire System MOs: <b id="total-mos">#GetTotalMOs.MOCount#</b></p>  
                    <p>Total new Sire System MOs (Last 30 Days): <b id="total-mos-last-30">#GetTotalMOsLast30.MOCount#</b></p>  
                    <p>Total new Sire System MOs (Last 7 Days): <b id="total-mos-last-7">#GetTotalMOsLast7.MOCount#</b></p>  
                    <p>Total new Sire System MOs (Last 24 hours): <b id="total-mos-last-1">#GetTotalMOsLast1.MOCount#</b></p>  
                </div>
            </div>
            

			<div class="col-lg-12 col-md-12">
                <!--- QUERY  --->
        			<!--- Get Total MTs Counts--->
                    <cfquery name="GetTotalMTs" datasource="#DBSourceManagementRead#">
                        SELECT
                            COUNT(IREResultsId_bi) AS MTCount
                        FROM
                            simplexresults.ireresults   
                        WHERE
                        	IREType_int = 1	                                           
                    </cfquery>  

                    <!--- Get New MTs Last 30 days --->
                    <cfquery name="GetTotalMTsLast30" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(IREResultsId_bi) AS MTCount
                        FROM
                            simplexresults.ireresults   
                        WHERE
        	               	Created_dt > '#LSDateFormat(DATEADD('d', -30, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -30, NOW()), 'HH:mm:ss')#'  
                       	AND	
                        	IREType_int = 1	                    
                    </cfquery>  
            
                    <!--- Get New MTs Last 7 days --->
                    <cfquery name="GetTotalMTsLast7" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(IREResultsId_bi) AS MTCount
                        FROM
                            simplexresults.ireresults   
                        WHERE
                        	Created_dt > '#LSDateFormat(DATEADD('d', -7, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -7, NOW()), 'HH:mm:ss')#' 
                       	AND	
                        	IREType_int = 1	                     
                    </cfquery>  
           
                    <!--- Get New MTs Last 1 days --->
                    <cfquery name="GetTotalMTsLast1" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(IREResultsId_bi) AS MTCount
                        FROM
                            simplexresults.ireresults 
                        WHERE
                        	Created_dt > '#LSDateFormat(DATEADD('d', -1, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -1, NOW()), 'HH:mm:ss')#' 
                        AND	
                        	IREType_int = 1	                    
                    </cfquery>  
                <!--- END QUERY  --->
                <p class="mlp-list-count title-simon-row"><b class="mlp-numb">Summary Info MTs</b></p>
                <div class="col-lg-12 col-md-12 content-simon-detail">
                    <p>Total number of Sire System MTs: <b id="total-mts">#GetTotalMTs.MTCount#</b></p>  
                    <p>Total new Sire System MTs (Last 30 Days): <b id="total-mts-last-30">#GetTotalMTsLast30.MTCount#</b></p>  
                    <p>Total new Sire System MTs (Last 7 Days): <b id="total-mts-last-7">#GetTotalMTsLast7.MTCount#</b>     </p>  
                    <p>Total new Sire System MTs (Last 24 hours): <b id="total-mts-last-1">#GetTotalMTsLast1.MTCount#</b></p>  
                </div>
            </div>
            

			<div class="col-lg-12 col-md-12">
                <!--- QUERY  --->
        			<!--- Get Total Sessions Counts--->
                    <cfquery name="GetTotalSessions" datasource="#DBSourceManagementRead#">
                        SELECT
                            COUNT(SessionId_bi) AS SessionCount
                        FROM
                            simplequeue.sessionire                                                        
                    </cfquery>  
                    
                    <!--- Get New Sessions Last 30 days --->
                    <cfquery name="GetTotalSessionsLast30" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(SessionId_bi) AS SessionCount
                        FROM
                            simplequeue.sessionire
                        WHERE
        	               	Created_dt > '#LSDateFormat(DATEADD('d', -30, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -30, NOW()), 'HH:mm:ss')#'  
                    </cfquery>  
            
                    <!--- Get New Sessions Last 7 days --->
                    <cfquery name="GetTotalSessionsLast7" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(SessionId_bi) AS SessionCount
                        FROM
                            simplequeue.sessionire 
                        WHERE
                        	Created_dt > '#LSDateFormat(DATEADD('d', -7, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -7, NOW()), 'HH:mm:ss')#' 
                     </cfquery>  

                    <!--- Get New Sessions Last 1 days --->
                    <cfquery name="GetTotalSessionsLast1" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(SessionId_bi) AS SessionCount
                        FROM
                            simplequeue.sessionire 
                        WHERE
                        	Created_dt > '#LSDateFormat(DATEADD('d', -1, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -1, NOW()), 'HH:mm:ss')#' 
                    </cfquery>  
                <!--- END QUERY  --->
                <p class="mlp-list-count title-simon-row"><b class="mlp-numb">Summary Info Sessions</b></p>
                <div class="col-lg-12 col-md-12 content-simon-detail">
                    <p>Total number of Sire System Sessions: <b id="total-sessions">#GetTotalSessions.SessionCount#</b></p>  
                    <p>Total new Sire System Sessions (Last 30 Days): <b id="total-sessions-last-30">#GetTotalSessionsLast30.SessionCount#</b>  </p>  
                    <p>Total new Sire System Sessions (Last 7 Days): <b id="total-sessions-last-7">#GetTotalSessionsLast7.SessionCount#</b></p>  
                    <p>Total new Sire System Sessions (Last 24 hours): <b id="total-sessions-last-1">#GetTotalSessionsLast1.SessionCount#</b>  </p>  
                </div>
            </div>
            
			<div class="col-lg-12 col-md-12">
                <!--- QUERY  --->
        			<!--- Get Total Sessions Counts--->
                    <cfquery name="GetTotalActiveSessions" datasource="#DBSourceManagementRead#">
                        SELECT
                            COUNT(SessionId_bi) AS SessionCount
                        FROM
                            simplequeue.sessionire   
                        WHERE	
                        	SessionState_int < 4                                                         
                    </cfquery>  

                    <!--- Get New Sessions Last 30 days --->
                    <cfquery name="GetTotalActiveSessionsLast30" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(SessionId_bi) AS SessionCount
                        FROM
                            simplequeue.sessionire
                        WHERE
        	               	Created_dt > '#LSDateFormat(DATEADD('d', -30, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -30, NOW()), 'HH:mm:ss')#'  
        	            AND	
        		            SessionState_int < 4    	
                    </cfquery>  
                                
                    <!--- Get New Sessions Last 7 days --->
                    <cfquery name="GetTotalActiveSessionsLast7" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(SessionId_bi) AS SessionCount
                        FROM
                            simplequeue.sessionire 
                        WHERE
                        	Created_dt > '#LSDateFormat(DATEADD('d', -7, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -7, NOW()), 'HH:mm:ss')#'
                        AND	
        		            SessionState_int < 4 	 
                     </cfquery>  
            
                    <!--- Get New Sessions Last 1 days --->
                    <cfquery name="GetTotalActiveSessionsLast1" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(SessionId_bi) AS SessionCount
                        FROM
                            simplequeue.sessionire 
                        WHERE
                        	Created_dt > '#LSDateFormat(DATEADD('d', -1, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -1, NOW()), 'HH:mm:ss')#' 
                       	AND	
        		            SessionState_int < 4 
                    </cfquery>  

                <!--- END QUERY  --->
                <p class="mlp-list-count title-simon-row"><b class="mlp-numb">Summary Info Active Sessions</b></p>
                <div class="col-lg-12 col-md-12 content-simon-detail">
                    <p>Total number of Sire System Sessions: <b id="total-active-sessions">#GetTotalActiveSessions.SessionCount#</b>    </p>  
                    <p>Total new Sire System Active Sessions (Last 30 Days): <b id="total-active-sessions-last-30">#GetTotalActiveSessionsLast30.SessionCount#</b></p>  
                    <p>Total new Sire System Active Sessions (Last 7 Days): <b id="total-active-sessions-last-7">#GetTotalActiveSessionsLast7.SessionCount#</b></p>  
                    <p> Total new Sire System Active Sessions (Last 24 hours): <b id="total-active-sessions-last-1">#GetTotalActiveSessionsLast1.SessionCount#</b></p>  

                </div>
            </div>
            
			<div class="col-lg-12 col-md-12 content-simon-detail">
                <!--- QUERY  --->
        			<!--- Get Total MOs Counts--->
                    <cfquery name="GetTotalSiteErrorLogs" datasource="#DBSourceManagementRead#">
                        SELECT
                            COUNT(SireErrorLogsId_int) AS SiteErrorLogCount
                        FROM
                            simpleobjects.sire_error_logs  
                        WHERE
                        	Status_int = 0	                                                
                    </cfquery>  
                    
                    <!--- Get New SiteErrorLogs Last 30 days --->
                    <cfquery name="GetTotalSiteErrorLogsLast30" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(SireErrorLogsId_int) AS SiteErrorLogCount
                        FROM
                            simpleobjects.sire_error_logs    
                        WHERE
                        	Created_dt > '#LSDateFormat(DATEADD('d', -30, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -30, NOW()), 'HH:mm:ss')#'     
                       	AND
                        	Status_int = 0	                
                    </cfquery>  
                                
                    <!--- Get New SiteErrorLogs Last 7 days --->
                    <cfquery name="GetTotalSiteErrorLogsLast7" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(SireErrorLogsId_int) AS SiteErrorLogCount
                        FROM
                            simpleobjects.sire_error_logs    
                        WHERE
                        	Created_dt > '#LSDateFormat(DATEADD('d', -7, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -7, NOW()), 'HH:mm:ss')#'   
                        AND
                        	Status_int = 0	  
                    </cfquery>  
                                
                    <!--- Get New SiteErrorLogs Last 1 days --->
                    <cfquery name="GetTotalSiteErrorLogsLast1" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(SireErrorLogsId_int) AS SiteErrorLogCount
                        FROM
                            simpleobjects.sire_error_logs  
                        WHERE
                        	Created_dt > '#LSDateFormat(DATEADD('d', -1, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -1, NOW()), 'HH:mm:ss')#'    
                        AND
                        	Status_int = 0	                 
                    </cfquery>  
                <!--- END QUERY  --->
                <p class="mlp-list-count title-simon-row"><b class="mlp-numb">Summary Info Web Site Error Log</b></p>
                <div class="col-lg-12 col-md-12 content-simon-detail">
                    <p> Total number of Sire System SiteErrorLogs: <b>#GetTotalSiteErrorLogs.SiteErrorLogCount#</b></p>  
                    <p>Total new Sire System SiteErrorLogs (Last 30 Days): <b>#GetTotalSiteErrorLogsLast30.SiteErrorLogCount#</b></p>  
                    <p>Total new Sire System SiteErrorLogs (Last 7 Days): <b>#GetTotalSiteErrorLogsLast7.SiteErrorLogCount#</b></p>  
                    <p>Total new Sire System SiteErrorLogs (Last 24 hours): <b>#GetTotalSiteErrorLogsLast1.SiteErrorLogCount#</b></p>  

                </div>
            </div>

            </div>
        </div>
    </div>

<div class="portlet light bordered ">
    <div class="portlet-body">
        <h4 class="portlet-heading">Usage Details</h4>
        
        <div class="row content-simon-new">


            	<!--- Get total usage by type Last 1 days - top 100 --->
            <cfquery name="GetTotalUsageLast1" datasource="#DBSourceManagementRead#">
				SELECT
					UserId_int,
					COUNT(*) AS TotalCount,
					IreType_int
				FROM 
					simplexresults.ireresults
				WHERE
					Created_dt > DATE_ADD(NOW(), INTERVAL -1 DAY)
				GROUP BY
					UserId_int,IreType_int
	            ORDER BY
					TotalCount DESC   
				LIMIT 100	
					
            </cfquery>  
            
            
			<style>
				
				td, th
				{
					padding: 1em;
					
				}
			</style>	
                    
            <div class="col-lg-12 col-md-12">
	        		        		        
	            <h4>Top 100 system usages by type and user (Last 24 hours): <b>#GetTotalBatchesLast1.BatchCount#</b></h4>
	            
	            <h4>Legend</h4>
	        	<table border="1">
		        	<tr>
			        	<td>IREMESSAGETYPE_MT</td>
			        	<td>#IREMESSAGETYPE_MT#</td>
		        	</tr>
		        	<tr>
			        	<td>IREMESSAGETYPE_MO</td>
			        	<td>#IREMESSAGETYPE_MO#</td>
		        	</tr>	
		        	<tr>
			        	<td>IREMESSAGETYPE_INTERVAL_TIMEOUT</td>
			        	<td>#IREMESSAGETYPE_INTERVAL_TIMEOUT#</td>
		        	</tr>	
		        	<tr>
			        	<td>IREMESSAGETYPE_API_TRIGGERED</td>
			        	<td>#IREMESSAGETYPE_API_TRIGGERED#</td>
		        	</tr>	
		        	<tr>
			        	<td>IREMESSAGETYPE_IRE_PROCESSING</td>
			        	<td>#IREMESSAGETYPE_MO#</td>
		        	</tr>	
		        	<tr>
			        	<td>IREMESSAGETYPE_IRE_PROCESSING</td>
			        	<td>#IREMESSAGETYPE_MO#</td>
		        	</tr>	
		        	<tr>
			        	<td>IREMESSAGETYPE_IRE_CP_API_CALL</td>
			        	<td>#IREMESSAGETYPE_IRE_CP_API_CALL#</td>
		        	</tr>	
	        	</table>	

	            <table border="1"  class="table-simon-add">
		            <thead>
    		            <tr>
    			            <th>User ID</th>
    			            <th>IRE Type</th>
    			            <th>Count</th>			            
    		            </tr>
                    </thead>
                    <tbody id="total-usage">
        	            <cfloop query="GetTotalUsageLast1">
        		            <tr>
        			            <td>#GetTotalUsageLast1.UserId_int#</td>
        			            <td>#GetTotalUsageLast1.IreType_int#</td>
        			            <td>#GetTotalUsageLast1.TotalCount#</td>
        		            </tr>
        	            </cfloop>
                    </tbody>
	            </table>
	                	            
            </div>

            
		<!--- List of admin filter, lookup tools and reports  --->
		
		<!--- Form for selecting User Id, Date Range --->

		<!--- Auto Populate Drop Box with users based on filters as they are typed  --->



												
		</div>
		</cfoutput>
	
	</div>

</div>

<!--- https://select2.github.io/  --->
<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/sire/css/select2.min.css">
</cfinvoke>

<!--- https://select2.github.io/  --->
<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/sire/css/select2-bootstrap.min.css">
</cfinvoke>

<!--- https://select2.github.io/  --->
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/sire/js/select2.min.js">
</cfinvoke>

<cfparam name="variables._title" default="System Usage">
<cfinclude template="../views/layouts/master.cfm">

<script type="text/javascript">
        
    $(document).ready(function()
    {                           
        // Make all select boxes select2 style
        $("SELECT").select2( { theme: "bootstrap"} );
                                                

        $("body").on('change', '#ShortCodeId', function(event) {
            event.preventDefault();
            var id = $(this).val();
            $.ajax({
                url: '/session/sire/models/cfc/admin.cfc?method=SystemUsageReportWithShortcode&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                type: 'POST',
                data: {inpShortCodeId: id},
            })
            .done(function(data) {
                if (parseInt(data.RXRESULTCODE) == 1) {
                    $("#total-users").text(data.TOTALUSERS);
                    $("#total-users-last-30").text(data.TOTALUSERSLAST30);
                    $("#total-users-last-7").text(data.TOTALUSERSLAST7);
                    $("#total-users-last-1").text(data.TOTALUSERSLAST1);

                    $("#total-batches").text(data.TOTALBATCHES);
                    $("#total-batches-last-30").text(data.TOTALBATCHESLAST30);
                    $("#total-batches-last-7").text(data.TOTALBATCHESLAST7);
                    $("#total-batches-last-1").text(data.TOTALBATCHESLAST1);

                    $("#total-mos").text(data.TOTALMOS);
                    $("#total-mos-last-30").text(data.TOTALMOSLAST30);
                    $("#total-mos-last-7").text(data.TOTALMOSLAST7);
                    $("#total-mos-last-1").text(data.TOTALMOSLAST1);

                    $("#total-mts").text(data.TOTALMTS);
                    $("#total-mts-last-30").text(data.TOTALMTSLAST30);
                    $("#total-mts-last-7").text(data.TOTALMTSLAST7);
                    $("#total-mts-last-1").text(data.TOTALMTSLAST1);

                    $("#total-sessions").text(data.TOTALSESSIONS);
                    $("#total-sessions-last-30").text(data.TOTALSESSIONSLAST30);
                    $("#total-sessions-last-7").text(data.TOTALSESSIONSLAST7);
                    $("#total-sessions-last-1").text(data.TOTALSESSIONSLAST1);

                    $("#total-active-sessions").text(data.TOTALACTIVESESSIONS);                    
                    $("#total-active-sessions-last-30").text(data.TOTALACTIVESESSIONSLAST30);                    
                    $("#total-active-sessions-last-7").text(data.TOTALACTIVESESSIONSLAST7);                    
                    $("#total-active-sessions-last-1").text(data.TOTALACTIVESESSIONSLAST1);
                    
                    $("#total-usage").html('');
                    for (var i = 0; i < data.TOTALUSAGE.length; i++) {
                        var html = '<tr>' +
                                    '<td>'+data.TOTALUSAGE[i].USERID+'</td>'+
                                    '<td>'+data.TOTALUSAGE[i].IRETYPE+'</td>'+
                                    '<td>'+data.TOTALUSAGE[i].TOTALCOUNT+'</td>'+
                                '</tr>';
                        $("#total-usage").append(html);
                    }
                } else {
                    alertBox("Get report failed!");
                }
            })
            .fail(function(e, msg) {
                console.log("error: "+msg);
            });
            
        });
    });
</script>
