
<cfscript>
	createObject("component", "public.sire.models.helpers.layout")
		.addJs("/public/sire/js/select2.min.js", true)
		.addJs("/session/sire/js/site/admin_template_update.js")
		.addCss("/public/sire/css/select2.min.css", true)
		.addCss("/public/sire/css/select2-bootstrap.min.css", true);
</cfscript>

<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />



<cfinvoke component="session.sire.models.cfc.category" method="GetCategoryGroup" returnvariable="CatGroups">

<cfinvoke component="session.sire.models.cfc.category" method="GetAllTemplate" returnvariable="Templates">

<div class="portlet light bordered">
	<div class="row">
		<div class="col-xs-12">
	        <h2 class="page-title">Template Category Management</h2>
		</div>  
    </div>
    <div class="row">
        <div class="col-xs-12">
        	
		<!--- END : content-header --->
		<div class="content-body">
			<div class="row">
				<div class="col-lg-2 col-md-12">
					<div class="form-group">
						<label>Category Name:</label>
						<select name="tcg.CGID_int" id="CategoryGroup" class="form-control filter-control">
							<option value="0">All</option>
							<cfloop array="#CatGroups.DATALIST#" index="option">
								<cfoutput><option value="#option.ID#">#option.NAME#</option></cfoutput>
							</cfloop>
						</select>
					</div>
				</div>
				<div class="col-lg-2 col-md-12">
					<div class="form-group">
						<label>Sub-category:</label>
						<select name="tc.CID_int" id="Category"  class="form-control filter-control">
							<option value="0">All</option>
						</select>
					</div>
				</div>
				<div class="col-lg-2 col-md-12">
					<div class="form-group">
						<label>Template Name:</label>
						<select name="tx.TID_int" id="TemplateID" class="form-control filter-control">
							<option value="0">All</option>
						</select>
					</div>
				</div>
				<div class="col-lg-6 col-md-12">
					<div class="form-group">
						<label style="display: block;">&nbsp;</label>
						<a target="_blank" title="Add a new sub-category" href="admin-category-template" id="btnAddNewCat" class="btn btn-success-custom pull-right" name="btnAddNewCat">Sub Category management</a>
						<button id="btnAddNew" title="Add a template to a sub-category" class="btn btn-success-custom pull-right" name="btnAddNew">Add to Category</button>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<br>
		<div class="re-table">
			<div class="table-responsive">
				<table id="tblListEMS" class="<!--- table-bordered ---> table-responsive <!--- table-hover ---> table-striped dataTables_wrapper">
					<thead>
						<tr>
							<th>Category Name</th><th>Sub-category Name</th><th>Template Name</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
		
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="category-template-update" role="dialog">
	<div class="modal-dialog">
		<form id="category-template-info" name="category-template-info">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong>Template Category Update</strong></h4>
				</div>
				<div class="modal-body">

					<div class="form-group">
						<label for="hCategoryGroup">Category:</label>
						<select id="hCategoryGroup" name="" class="form-control select2 validate[required]" >
							<cfloop array="#CatGroups.DATALIST#" index="CatGroupItem">
								<cfoutput><option value="#CatGroupItem.ID#">#CatGroupItem.NAME#</option></cfoutput>
							</cfloop>
						</select>
					</div>
					<div class="form-group"> 
						<label for="hCategory">Sub-category:</label>
						<select id="hCategory" name="Category" class="form-control select2 validate[required]">
							<option value="0">All</option>
						</select>
					</div>
					<div class="form-group">
						<label for="hTemplateID">Template:</label>

						<select id="hTemplateID" name="TemplateID" class="form-control select2 validate[required]">
							<cfloop array="#Templates.DATALIST#" index="TemplateItem">
								<cfoutput><option value="#TemplateItem.ID#">#TemplateItem.NAME#</option></cfoutput>
							</cfloop>
						</select>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn-save btn btn-success-custom" >Save</button>
					<button type="button" class="btn btn-back-custom" id="cancel-button" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="category-template-add" role="dialog">
	<div class="modal-dialog">
		<form id="category-template-addnew" name="category-template-add">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong>Add a new template to category</strong></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="aCategoryGroup">Category:</label>
						<select id="aCategoryGroup" name="" class="add-form-control form-control select2 validate[required]" >
							<option value="0">--Select a group--</option>
							<cfloop array="#CatGroups.DATALIST#" index="CatGroupItem">
								<cfoutput><option value="#CatGroupItem.ID#">#CatGroupItem.NAME#</option></cfoutput>
							</cfloop>
						</select>
					</div>
					<div class="form-group"> 
						<label for="aCategory">Sub-category:</label>
						<select id="aCategory" name="Category" class="add-form-control form-control select2 validate[required]">
							<option value="0">--Select a sub-category--</option>
						</select>
					</div>
					<div class="form-group">
						<label for="aTemplateID">Template:</label>
						<select id="aTemplateID" name="TemplateID" class="add-form-control form-control select2 validate[required, custom[integer,min[1]]]">
							<option value="0">--Select a template--</option>
							<cfloop array="#Templates.DATALIST#" index="TemplateItem">
								<cfoutput><option value="#TemplateItem.ID#">#TemplateItem.NAME#</option></cfoutput>
							</cfloop>
						</select>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn-save btn btn-success-custom" >Save</button>
					<button type="button" class="btn btn-back-custom" id="cancel-button" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>

<!-- Modal rename template -->
<div class="modal fade" id="rename-template" role="dialog">
	<div class="modal-dialog">
		<form id="rename-template-form" name="rename-template-form">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong>RENAME TEMPLATE</strong></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>New template name:</label>
						<input type="hidden" id="template-id">
						<input type="text" name="new-template-name" class="form-control validate[required]" id="new-template-name">
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn-save btn btn-success-custom" >Save</button>
					<button type="button" class="btn btn-back-custom" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>

<cfparam name="variables._title" default="Admin - Template Category Management">
<cfinclude template="../views/layouts/master.cfm">

<style type="text/css">
@media (min-width: 1200px) {
	div.lbl-filter  {
		text-align: right;
	}
}

#tblListEMS a.btn-success-custom {
	margin: 0px 0px 5px 5px;
}

.btn-success-custom {
	margin: 0 0 0 5px;
}


ul.dropdown-menu {
	padding: 5px;
	min-width: 80px;
	left: -25px ;
}
ul.dropdown-menu li a{
	margin-left: 0!important;
}

a.btn-change-order {
	color: #FFFFFF!important;
}

button.btn-success-custom {
	border-color: rgb( 92, 180, 105 )!important;
}

td.template a {
	cursor: pointer;
}


</style>


