<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfparam name="url.campaignid" default="0"/>


<cfoutput>
	<div class="portlet light bordered">
		<cfinclude template="admin-tool.cfm"/>
	</div>
	<div class="portlet light bordered">
		<div class="portlet-body">
			<div class="new-inner-body-portlet2">
				<div class="form-gd">
					<div class="row">
						<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
							<h4 class="portlet-heading2">Campaign</h4>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="inner-addon left-addon">
								<i class="glyphicon glyphicon-search"></i>
								<input type="search" class="form-control" id="searchbox" placeholder="Search Campaign">
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="pull-right">
								<a href="/session/sire/pages/admin-tool-campaign-flowedit" class="btn btn-block green-gd pop-message">Add New Campaign</a>
							</div>
						</div>
					</div>

					<div class="re-table" style="margin-top: 10px">
						<div class="table-responsive">
							<table id="admin-campaign-list" class="table table-striped table-bordered table-hover dataTable" aria-describedby="">

							</table>
						</div>
					</div>
				</div>
		    </div>
		</div>
	</div>
</cfoutput>
		
<cfscript>
	CreateObject("component","public.sire.models.helpers.layout")
	.addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
	.addCss("../assets/layouts/layout4/css/custom3.css") 
	.addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)
	.addJs("../assets/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js", true)
	.addJs("../assets/pages/scripts/admin-tool-campaign-flow.js");
</cfscript>

<cfparam name="variables._title" default="Admin Tools - Alert Campaign">
<cfinclude template="../views/layouts/master.cfm">
