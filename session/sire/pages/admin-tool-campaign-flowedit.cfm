<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfparam name="url.campaignid" default="0"/>

<cfinclude template="/session/sire/configs/alert_constants.cfm"/>

<cfif isNumeric(url.campaignid) AND url.campaignid GT 0>
	<cfinvoke method="getCampaign" component="session.sire.models.cfc.admin-tool" returnvariable="RetVarCampaign">
		<cfinvokeargument name="inpCampaignId" value="#url.campaignid#"/>
	</cfinvoke>
	<cfset RetVarCampaign = RetVarCampaign.Campaign/>
	<cfif RetVarCampaign.Status_int EQ 1>
		<cflocation url="/session/sire/pages/admin-tool-campaign-flow" addtoken="false"/>
	</cfif>
</cfif>

<cfinvoke method="getMessages" component="session.sire.models.cfc.admin-tool" returnvariable="RetVarMessages">
	<cfinvokeargument name="inpGetAll" value="0"/>
</cfinvoke>

<cfinvoke method="getSegments" component="session.sire.models.cfc.admin-tool" returnvariable="RetVarSegments">
	<cfinvokeargument name="inpGetAll" value="0"/>
</cfinvoke>

<cfif !isDefined("RetVarCampaign")>
	<cfset RetVarCampaign = {
		RECORDCOUNT = 0,
		PKId_bi = 0,
		CampaignName_vch = "",
		MessageId_bi = 0,
		SegmentId_bi = 0,
		ReceiverId_int = 0,
		WebAlertSentCount_int = 0,
		EmailSentCount_int = 0,
		SMSSentCount_int = 0,
		ListCount_int = 0,
		SentTo_int = 0,
		Trigger_bi = 1,
		UserId_int = 0,
		Created_dt = javacast("null",""),
		LastModifierId_int = 0,
		LastEdited_dt = javacast("null",""),
		LastModifyDesc_vch = "",
		Status_int = 1,
		Expire_dt = javacast("null","")
	} />
</cfif>

<cfoutput>
	<div class="portlet light bordered">
		<cfinclude template="admin-tool.cfm"/>
	</div>
	<div class="portlet light bordered">
		<div class="portlet-body">
			<div class="new-inner-body-portlet2">
				<h4 class="portlet-heading2">
					<cfif RetVarCampaign.PKId_bi GT 0>
						Edit
					<cfelse>
						New
					</cfif>
					Campaign
				</h4>
				<form id="AlertCampaignForm" method="post" accept-charset="utf-8" autocomplete="off">
					<input type="hidden" name="PKId_bi" value="#RetVarCampaign.PKId_bi#">
					<div class="row">
						<div class="col-md-6 col-lg-4 col-sm-12 col-xs-12">
							<div class="form-group">
								<label><b>Campaign:</b></label>
								<input type="text" name="CampaignName_vch" value="#RetVarCampaign.CampaignName_vch#" placeholder="Campaign's Name" class="form-control validate[required,custom[noHTML],maxSize[255]]" maxlength="255" />
							</div>
						</div>
						<div class="col-md-6 col-lg-4 col-sm-12 col-xs-12">
							<div class="form-group">
								<label><b>Message:</b></label>
								<select id="MessageId_bi" name="MessageId_bi" class="form-control validate[required]">
									<option value="">- Select Message -</option>
									<cfloop query="#RetVarMessages.Messages#">
										<cfset messagesNotEmpty = 0 />
										<cfset messagesNotEmpty += (trim(RetVarMessages.Messages.Web_Alert_Content_vch) EQ "" ? 0 : ALERT_SEND_TO_WEB_ALERT) />
										<cfset messagesNotEmpty += (trim(RetVarMessages.Messages.Email_Content_vch) EQ "" ? 0 : ALERT_SEND_TO_EMAIL) />
										<cfset messagesNotEmpty += (trim(RetVarMessages.Messages.SMS_Content_vch) EQ "" ? 0 : ALERT_SEND_TO_SMS) />
										<option id="MessageId_bi#RetVarMessages.Messages.PKId_bi#" value="#RetVarMessages.Messages.PKId_bi#" #(RetVarMessages.Messages.PKId_bi EQ RetVarCampaign.MessageId_bi ? 'selected' : "")# data-messagesnotempty="#messagesNotEmpty#">#RetVarMessages.Messages.Subject_vch#</option>
									</cfloop>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-lg-4 col-sm-12 col-xs-12">
							<div class="form-group">
								<label><b>Segment:</b></label>
								<div class="radio">
									<label>
										<input type="radio" name="SegmentId_bi_opt" value="0" #(RetVarCampaign.SegmentId_bi EQ 0 AND RetVarCampaign.ReceiverId_int EQ 0 ? 'checked=""' : "")# class="validate[required]" id="SegmentId_bi_opt0" />
										Everyone
									</label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="SegmentId_bi_opt" value="1" #(RetVarCampaign.SegmentId_bi GT 0 AND RetVarCampaign.ReceiverId_int EQ 0 ? 'checked=""' : "")# class="validate[required]" id="SegmentId_bi_opt1" />
										Segment list:
									</label>
								</div>
								<select id="SegmentId_bi" name="SegmentId_bi" class="form-control validate[funcCall[checkSegmentId_bi[##SegmentId_bi_opt1]]]">
									<option value="0">- Select Segment -</option>
									<cfloop query="#RetVarSegments.Segments#">
										<option value="#RetVarSegments.Segments.PKId_bi#" #(RetVarSegments.Segments.PKId_bi EQ RetVarCampaign.SegmentId_bi ? 'selected' : "")#>#RetVarSegments.Segments.SegmentName_vch#</option>
									</cfloop>
								</select>
								<div class="radio">
									<label>
										<input type="radio" name="SegmentId_bi_opt" value="2" #(RetVarCampaign.ReceiverId_int GT 0 ? 'checked=""' : "")# class="validate[required]" id="SegmentId_bi_opt2" />
										Single user:
									</label>
								</div>
								<select id="ReceiverId_int" name="ReceiverId_int" class="form-control validate[funcCall[checkSegmentId_bi[##SegmentId_bi_opt2]]]">
									<option value="0">- Select Receiver -</option>
									<cfif RetVarCampaign.ReceiverId_int GT 0>
										<cfinvoke method="getUserById" component="session.sire.models.cfc.admin-tool" returnvariable="RetVarUser">
											<cfinvokeargument name="inpUserId" value="#RetVarCampaign.ReceiverId_int#"/>
										</cfinvoke>
										<cfif RetVarUser.RXRESULTCODE EQ 1>
											<option value="#RetVarUser.USER.UserId_int#" selected>#RetVarUser.USER.UserId_int# - #RetVarUser.USER.EmailAddress_vch#</option>
										</cfif>
									</cfif>
								</select>
							</div>
							<div class="form-group">
								<label><b>Send to:</b></label>
								<div style="margin-left: 20px">
									<div class="checkbox">
										<label>
											<input type="checkbox" name="SentTo_int" value="#ALERT_SEND_TO_WEB_ALERT#" #(BitAnd(RetVarCampaign.SentTo_int, ALERT_SEND_TO_WEB_ALERT) ? 'checked=""' : "")# class="validate[required,funcCall[checkSentTo_int[##MessageId_bi]]]" data-errormessage-custom-error="Web  alert message is empty" />
											Web Alert
										</label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="SentTo_int" value="#ALERT_SEND_TO_EMAIL#" #(BitAnd(RetVarCampaign.SentTo_int, ALERT_SEND_TO_EMAIL) ? 'checked=""' : "")# class="validate[required,funcCall[checkSentTo_int[##MessageId_bi]]]" data-errormessage-custom-error="Email  message is empty" />
											Email
										</label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name="SentTo_int" value="#ALERT_SEND_TO_SMS#" #(BitAnd(RetVarCampaign.SentTo_int, ALERT_SEND_TO_SMS) ? 'checked=""' : "")# class="validate[required,funcCall[checkSentTo_int[##MessageId_bi]]]" data-errormessage-custom-error="SMS  message is empty" />
											SMS
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-lg-4 col-sm-12 col-xs-12">
							<div class="form-group">
								<label><b>Trigger:</b></label>
								<div class="radio">
									<label>
										<input type="radio" id="auto-trigger" disabled="" #(RetVarCampaign.Trigger_bi NEQ ALERT_TRIGGER_SEND_NOW ? 'checked=""' : "")# />
										Triggered automatically
									</label>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="radio col-xs-offset-1 col-xs-11">
											<label>
												<input type="radio" name="Trigger_bi" value="#ALERT_TRIGGER_SIGN_UP#" #(BitAnd(RetVarCampaign.Trigger_bi, ALERT_TRIGGER_SIGN_UP) GT 0 ? 'checked=""' : "")#/>
												Sign up
											</label>
										</div>
										<div class="radio col-xs-offset-1 col-xs-11">
											<label>
												<cfset ALERT_TRIGGER_SIGN_IN_ALL = ALERT_TRIGGER_SIGN_IN + ALERT_TRIGGER_LOW_CREDIT + ALERT_TRIGGER_NO_CREDIT + ALERT_TRIGGER_DECLINED_CREDIT/>
												<input type="radio" name="Trigger_bi" value="#ALERT_TRIGGER_SIGN_IN#" #(BitAnd(RetVarCampaign.Trigger_bi, ALERT_TRIGGER_SIGN_IN_ALL) GT 0 ? 'checked=""' : "")#/>
												Sign in
											</label>
										</div>
										<div class="col-lg-12">
											<div class="col-xs-offset-1 col-xs-11">
												<div class="checkbox">
													<label>
														<input type="checkbox" name="Trigger_bi" value="#ALERT_TRIGGER_LOW_CREDIT#" #(BitAnd(RetVarCampaign.Trigger_bi, ALERT_TRIGGER_LOW_CREDIT) GT 0 ? 'checked=""' : "")#/>
														Low credit
													</label>
												</div>
												<div class="checkbox">
													<label>
														<input type="checkbox" name="Trigger_bi" value="#ALERT_TRIGGER_NO_CREDIT#" #(BitAnd(RetVarCampaign.Trigger_bi, ALERT_TRIGGER_NO_CREDIT) GT 0 ? 'checked=""' : "")#/>
														No credit
													</label>
												</div>
												<div class="checkbox">
													<label>
														<input type="checkbox" name="Trigger_bi" value="#ALERT_TRIGGER_DECLINED_CREDIT#" #(BitAnd(RetVarCampaign.Trigger_bi, ALERT_TRIGGER_DECLINED_CREDIT) GT 0 ? 'checked=""' : "")#/>
														Declined credit
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="radio">
									<label>
										<input type="radio" name="Trigger_bi" value="#ALERT_TRIGGER_SEND_NOW#" #(BitAnd(RetVarCampaign.Trigger_bi,ALERT_TRIGGER_SEND_NOW) GT 0 ? 'checked=""' : "")# id="Trigger_bi1"/>
										Send NOW!
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-lg-4 col-sm-12 col-xs-12">
							<label><b>Status:</b></label>
							<div class="form-group">
								<label class="radio-inline">
									<input type="radio" name="Status_int" value="1" #(RetVarCampaign.Status_int EQ 1 ? 'checked=""' : "")# id="Status_int1" />
									Active
								</label>
								<label class="radio-inline">
									<input type="radio" name="Status_int" value="0" #(RetVarCampaign.Status_int NEQ 1 ? 'checked=""' : "")#/>
									Inactive
								</label>
							</div>
						</div>
						<div class="col-md-6 col-lg-4 col-sm-12 col-xs-12">
							<div class="form-group">
								<a href="/session/sire/pages/admin-tool-campaign-flow" class="btn btn-back-custom">Cancel</a>
								&nbsp; &nbsp; &nbsp;
								<button type="submit" class="btn btn-success-custom btn-save-alert-campaign"> Save </button>
							</div>
						</div>
					</div>	
				</form>
			</div>
		</div>
	</div>
</cfoutput>		

<cfscript>
	CreateObject("component","public.sire.models.helpers.layout")
	.addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
	.addCss("/public/sire/css/select2.min.css", true)
	.addCss("/public/sire/css/select2-bootstrap.min.css", true)
	.addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)	   	    
	.addJs("../assets/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js", true)
	.addJs("/public/sire/js/select2.min.js", true)
	.addJs("../assets/pages/scripts/admin-tool-campaign-flowedit.js");
</cfscript>

<cfparam name="variables._title" default="Admin Tools - Alert Campaign">
<cfinclude template="../views/layouts/master.cfm">
