<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfif isNumeric(url.campaignid) AND url.campaignid GT 0>
	<cfinvoke method="getCampaignReport" component="session.sire.models.cfc.admin-tool" returnvariable="RetVarCampaign">
		<cfinvokeargument name="inpCampaignId" value="#url.campaignid#"/>
	</cfinvoke>
	<cfset RetVarCampaign = RetVarCampaign.Campaign/>
<cfelse>
	<cflocation url="/session/sire/pages/admin-tool-campaign-flow" addtoken="false"/>
</cfif>

<cfinclude template="/session/sire/configs/alert_constants.cfm"/>

<cfset ListCount_str = BitAnd(RetVarCampaign.Trigger_bi, ALERT_TRIGGER_SIGN_UP + ALERT_TRIGGER_LOW_CREDIT + ALERT_TRIGGER_NO_CREDIT + ALERT_TRIGGER_DECLINED_CREDIT) GT 0 ? "" : "/" & RetVarCampaign.ListCount_int/>

<cfset SentTo_vch = ""/>
<cfif BitAnd(RetVarCampaign.SentTo_int, ALERT_SEND_TO_WEB_ALERT) GT 0>
	<cfset SentTo_vch = "Web Alert"/>
</cfif>
<cfif BitAnd(RetVarCampaign.SentTo_int, ALERT_SEND_TO_EMAIL) GT 0>
	<cfset SentTo_vch = listAppend(SentTo_vch, "Email", ", ")/>
</cfif>
<cfif BitAnd(RetVarCampaign.SentTo_int, ALERT_SEND_TO_SMS) GT 0>
	<cfset SentTo_vch = listAppend(SentTo_vch, "SMS", ", ")/>
</cfif>

<cfset Trigger_vch = ""/>
<cfif BitAnd(RetVarCampaign.Trigger_bi, ALERT_TRIGGER_SEND_NOW) GT 0>
	<cfset Trigger_vch = "Send NOW!"/>
<cfelseif BitAnd(RetVarCampaign.Trigger_bi, ALERT_TRIGGER_SIGN_UP) GT 0>
	<cfset Trigger_vch = "Sign up"/>
<cfelseif BitAnd(RetVarCampaign.Trigger_bi, ALERT_TRIGGER_SIGN_IN) GT 0>
	<cfset Trigger_vch = "Sign in"/>
</cfif>
<cfif BitAnd(RetVarCampaign.Trigger_bi, ALERT_TRIGGER_LOW_CREDIT) GT 0>
	<cfset Trigger_vch = "Low credit"/>
</cfif>
<cfif BitAnd(RetVarCampaign.Trigger_bi, ALERT_TRIGGER_NO_CREDIT) GT 0>
	<cfset Trigger_vch = listAppend(Trigger_vch, "No credit", ", ")/>
</cfif>
<cfif BitAnd(RetVarCampaign.Trigger_bi, ALERT_TRIGGER_DECLINED_CREDIT) GT 0>
	<cfset Trigger_vch = listAppend(Trigger_vch, "Declined credit", ", ")/>
</cfif>

<cfoutput>
	<div class="portlet light bordered">
		<cfinclude template="admin-tool.cfm"/>
	</div>
	<div class="portlet light bordered">
		<div class="portlet-body">
			<div class="new-inner-body-portlet2">
				<div class="form-gd">
					<div class="clearfix">
						<h3 class="form-heading">SMS Campaign Report</h3>
						<div class="pull-right">
							<a href="/session/sire/pages/admin-tool-campaign-flow" class="btn btn-block green-gd"> Back </a>
						</div>
					</div>
					<hr/>
					<div class="row">
						<div class="col-md-6 col-lg-4 col-sm-12 col-xs-12">
							<div class="form-group">
								<h4>
									<b>Campaign Name:</b> #HTMLEditFormat(RetVarCampaign.CampaignName_vch)#
								</h4>
								<i>
									Created #dateFormat(RetVarCampaign.Created_dt, "mm/dd/yyyy")#
								</i>
							</div>
						</div>
						<div class="col-md-6 col-lg-4 col-sm-12 col-xs-12">
							<div class="form-group">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-lg-4 col-sm-12 col-xs-12">
							<div class="form-group">
								<h4>
									<b>Message:</b> #HTMLEditFormat(RetVarCampaign.MessageSubject_vch)#
								</h4>
								<i>
									Created #DateFormat(RetVarCampaign.MessageCreated_dt, "mm/dd/yyyy")#
								</i>
							</div>
						</div>
						<div class="col-md-6 col-lg-4 col-sm-12 col-xs-12">
							<div class="form-group">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-lg-4 col-sm-12 col-xs-12">
							<div class="form-group">
								<h4>
									<cfif RetVarCampaign.SegmentId_bi GT 0>
									<b>Segment List:</b> #(RetVarCampaign.SegmentName_vch EQ "" ? "n/a" : RetVarCampaign.SegmentName_vch)#
									<cfelseif RetVarCampaign.ReceiverId_int GT 0>
									<b>Segment:</b> #(RetVarCampaign.ReceiverEmailAddress_vch EQ "" ? "n/a" : RetVarCampaign.ReceiverEmailAddress_vch)#
									<cfelse>
									<b>Segment:</b> Every one
									</cfif>
								</h4>
								<cfif RetVarCampaign.SegmentName_vch NEQ "">
								<i>
									Created: #DateFormat(RetVarCampaign.SegmentCreated_dt, "mm/dd/yyyy")#
								</i>
								</cfif>
							</div>
						</div>
						<cfif RetVarCampaign.SegmentName_vch NEQ "">
						<div class="col-md-6 col-lg-4 col-sm-12 col-xs-12">
							<div class="form-group">
							</div>
						</div>
						</cfif>
					</div>
					<div class="row">
						<div class="col-md-6 col-lg-4 col-sm-12 col-xs-12">
							<div class="form-group">
								<h4>
									<b>Send to:</b> #replace(SentTo_vch, ',', ', ', 'all')#
								</h4>
								<i>
									Trigger: #replace(Trigger_vch, ',', ', ', 'all')#
								</i>
							</div>
						</div>
						<div class="col-md-6 col-lg-4 col-sm-12 col-xs-12">
							<div class="form-group">
							</div>
						</div>
					</div>
					<!--- <div class="row">
						<div class="col-md-6 col-lg-4 col-sm-12 col-xs-12">
							<div class="form-group">
								<h4>
									Status: #RetVarCampaign.Status_int EQ 1 ? "Active" : "Inactive"#
								</h4>
							</div>
						</div>
					</div> --->

					<!--- <div class="row">
						<div class="col-md-6 col-lg-4 col-sm-12 col-xs-12">
							<div class="form-group">
								<h3>
									Email Sent: #(BitAnd(RetVarCampaign.SentTo_int, ALERT_SEND_TO_EMAIL) EQ 0 ? "n/a" : RetVarCampaign.EmailSentCount_int & ListCount_str )#
								</h3>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-lg-4 col-sm-12 col-xs-12">
							<div class="form-group">
								<h3>
									SMS Sent: #(BitAnd(RetVarCampaign.SentTo_int, ALERT_SEND_TO_SMS) EQ 0 ? "n/a" : RetVarCampaign.SMSSentCount_int & ListCount_str )#
								</h3>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-lg-4 col-sm-12 col-xs-12">
							<div class="form-group">
								<h3>
									Web Alerted: #(BitAnd(RetVarCampaign.SentTo_int, ALERT_SEND_TO_WEB_ALERT) EQ 0 ? "n/a" : RetVarCampaign.WebAlertSentCount_int & ListCount_str )#
								</h3>
							</div>
						</div>
					</div> --->
				</div>
		    </div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-4 col-xs-4">
			<div class="portlet light bordered NumSent">
				<h1 class="EmailSent">
					<b<cfif len(ListCount_str) GT 4> style="font-size:26px"</cfif>>
						#(BitAnd(RetVarCampaign.SentTo_int, ALERT_SEND_TO_EMAIL) EQ 0 ? "n/a" : RetVarCampaign.EmailSentCount_int & ListCount_str )#
					</b>
				</h1>
				<h3 class="titleSent">
					Email Sent
				</h3>
			</div>
		</div>
		<div class="col-sm-4 col-xs-4">
			<div class="portlet light bordered NumSent">
				<h1 class="SMSSent">
					<b<cfif len(ListCount_str) GT 4> style="font-size:26px"</cfif>>
						#(BitAnd(RetVarCampaign.SentTo_int, ALERT_SEND_TO_SMS) EQ 0 ? "n/a" : RetVarCampaign.SMSSentCount_int & ListCount_str )#
					</b>
				</h1>
				<h3 class="titleSent">
					SMS Sent
				</h3>
			</div>
		</div>
		<div class="col-sm-4 col-xs-4">
			<div class="portlet light bordered NumSent">
				<h1 class="WebAlerted">
					<b<cfif len(ListCount_str) GT 4> style="font-size:26px"</cfif>>
						#(BitAnd(RetVarCampaign.SentTo_int, ALERT_SEND_TO_WEB_ALERT) EQ 0 ? "n/a" : RetVarCampaign.WebAlertSentCount_int & ListCount_str )#
					</b>
				</h1>
				<h3 class="titleSent">
					Web Alerted
				</h3>
			</div>
		</div>
	</div>
</cfoutput>
		
<cfscript>
	CreateObject("component","public.sire.models.helpers.layout")
	.addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
	.addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)
	.addJs("../assets/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js", true);
</cfscript>

<cfparam name="variables._title" default="Admin Tools - Alert Campaign Report">
<cfinclude template="../views/layouts/master.cfm">
