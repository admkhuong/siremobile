

<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />
<cfoutput>
	<cfscript>
	    CreateObject("component","public.sire.models.helpers.layout")
	    .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
	    .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)	 
	    .addCss("../assets/layouts/layout4/css/custom3.css") 	     	 
	    .addJs("../assets/pages/scripts/admin-tool-message.js");  	    	    
	</cfscript>
</cfoutput>
<div class="portlet light bordered">
	<cfinclude template="admin-tool.cfm"/>
</div>
<div class="portlet light bordered">
	<div class="portlet-body">
		<div class="new-inner-body-portlet2">
			<div class="form-gd">
				<div class="row">
					<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
						<h4 class="portlet-heading2">Message List</h4>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div class="inner-addon left-addon">
							<i class="glyphicon glyphicon-search"></i>
							<input type="search" class="form-control" id="searchbox" placeholder="Search Message">
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div class="pull-right">
							<a href="#" class="btn btn-block green-gd pop-message" data-id="0" data-toggle="modal" data-message-action="new" data-target="#edit-message">Add New Message</a>
						</div>
					</div>
				</div>
				<div class="" style="margin-top: 10px">
			        <div class="row">
			            <div class="col-xs-12">
			                <div class="re-table">
			                    <div class="table-responsive">
			                        <table id="adm-message-list" class="table table-striped table-responsive">
			            
			                        </table>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>			
			</div>
	    </div>
	</div>
</div>
<!--- modal--->
 <!--- edit message--->
 <div class="modal fade" id="edit-message" tabindex="-1" role="dialog" aria-hidden="true">
	<form name="form-edit-message" class="col-sm-12" id="form-edit-message">
	    <div class="modal-dialog">
	        <div class="modal-content">
	        	<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<b><h4 class="modal-title" id="modal-tit">
						NEW MESSAGE
					</h4></b>			
					<input type="hidden" name="messageId" id="messageId" value="0" autocomplete="off"/>		
				</div>
	            <div class="modal-body">	               
	               <div class="modal-body-inner">                  
	                    <div class="form-group">
	                       <input type="text" class="form-control validate[required, custom[noHTML], maxSize[255]]" maxlength="255" id="message-name" name="message-name" placeholder="Subject" autocomplete="off">	                       
	                    </div>
	                    <div class="form-group">
	                       <textarea  name="" data-control="text-message" cols="30" rows="3" class="form-control text validate[maxSize[459]]" id="message-message" placeholder="SMS Content" autocomplete="off" maxlength="459"></textarea>
	                    </div>
	                    <div class="form-group">
	                       <textarea  name="" data-control="text-message" cols="30" rows="3" class="form-control text validate[maxSize[1000]]" id="message-web" placeholder="Web Alert Content" autocomplete="off" maxlength="1000"></textarea>
	                    </div>
	                    <div class="form-group">
	                       <textarea  name="" data-control="text-message" cols="30" rows="3" class="form-control text validate[maxSize[4000]]" id="message-mail" placeholder="Mail Content" autocomplete="off" maxlength="4000"></textarea>
	                    </div>
	                    <div id="amw" class="form-group" style="display:none">
	                    	<div class="alert alert-warning" role="alert" style="background-color:#fcf8e3;color:#8a6d3b">
	                    		Your message is incomplete, you must have at least one message.
	                    	</div>
	                    </div>
	             	</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn green-cancel" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn green-gd"  id="btnUpdateMessage">Save</button>
				</div>
			</div>
		</div>
	</form>
 </div>
<cfparam name="variables._title" default="Admin Tools">
<cfinclude template="../views/layouts/master.cfm">
