

<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />
<cfoutput>
	<cfscript>
	    CreateObject("component","public.sire.models.helpers.layout")
	    .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
	    .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)	 
	    .addCss("../assets/layouts/layout4/css/custom3.css") 	     	 
	    .addJs("../assets/pages/scripts/admin-tool-segment.js");  	    	    
	</cfscript>
	<cfparam name="action" default="list">
	<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	    <cfinvokeargument name="path" value="/session/sire/assets/global/scripts/filter-bar-generator.js">
	</cfinvoke>
</cfoutput>
<cfif action is "list">
	<div class="portlet light bordered">
		<cfinclude template="admin-tool.cfm"/>
	</div>
	<div class="portlet light bordered">
		<div class="portlet-body">
			<div class="new-inner-body-portlet2">
				<div class="form-gd">
					<div class="row">
						<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
							<h4 class="portlet-heading2">Segment List</h4>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="inner-addon left-addon">
								<i class="glyphicon glyphicon-search"></i>
								<input type="search" class="form-control" id="searchbox" placeholder="Search Segment">
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="pull-right">
								<a href="/session/sire/pages/admin-tool-segment?action=new" class="btn btn-block green-gd pop-segment">Add New Segment List</a>
							</div>
						</div>
					</div>
					<div class="" style="margin-top: 10px">
				        <div class="row">
				            <div class="col-xs-12">
				                <div class="re-table">
				                    <div class="table-responsive">
				                        <table id="adm-segment-list" class="table table-striped table-responsive">
				            
				                        </table>
				                    </div>
				                </div>
				            </div>
				        </div>
				    </div>			
				</div>
		    </div>
		</div>
	</div>
<cfelse>
	<cfparam name="segmentId" default="0">
	
	<cfset listUserSelected = ""/>
	<cfif segmentId GT 0>
		<cfinvoke method="GetSegmentListInfo" component="session.sire.models.cfc.admin-tool" returnvariable="SegmentListInfo">
			<cfinvokeargument name="inpSegmentID" value="#segmentId#"/>
		</cfinvoke>
		<cfif SegmentListInfo.RXRESULTCODE EQ 1>
			<cfset listUserSelected = SegmentListInfo.ListUserBySegment/>
		</cfif>
	</cfif>

	<div class="portlet light bordered">
		<cfinclude template="admin-tool.cfm"/>
	</div>
	<div class="portlet light bordered">
		<div class="portlet-body">
			<div class="new-inner-body-portlet2">
				<form name="form-edit-segment" id="form-edit-segment">
					<div class="row">
						<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
							<h4 class="portlet-heading2">
								<cfif action is "new">
									Define New Segment List
								<cfelse>
									Edit Segment List
								</cfif>
							</h4>
							<input type="hidden" name="segmentId" value="<cfoutput>#segmentId#</cfoutput>" id="segmentId">
							<div class="form-group">
						      <input type="text" class="form-control validate[required, custom[noHTML], maxSize[255]]" id="seg-list-name" placeholder="Enter Segment Name" name="seg-list-name">
						    </div>
						    <div class="form-group">      
						      <input type="text" class="form-control validate[custom[noHTML]]" id="seg-desc" placeholder="Enter Segment Description" name="seg-desc">
						    </div>
							<a href="/session/sire/pages/admin-tool-segment" class="btn btn-back-custom">Cancel</a>
						    &nbsp; &nbsp; &nbsp;
						    <button type="submit" class="btn btn-success-custom" id="submitSegment"> Save </button>
						</div>
					</div>
				</form>	
				<div class="" style="margin-top: 10px">
			        <div class="row" style="margin-top: 10px">
			        	<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
							<h4 class="portlet-heading2">Select User</h4>
						</div>
						<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
							<div id="box-filter">
								
							</div>
						</div>							
					</div>
					<div class="row"  style="margin-top: 10px">
			            <div class="col-xs-12">
			                <div class="re-table">
			                    <div class="table-responsive">
			                    	<cfoutput>
			                    	<input type="hidden" name="list-user-selected" id="list-user-selected" size="100" value="#listUserSelected#" />
			                    	</cfoutput>
			                        <table id="adm-user-list" class="table table-striped table-responsive">
			            
			                        </table>
			                    </div>
			                </div>
			            </div>				            
			        </div>
			    </div>			    
		    </div>
		</div>
	</div>
</cfif>
<cfparam name="variables._title" default="Admin Tools">
<cfinclude template="../views/layouts/master.cfm">
