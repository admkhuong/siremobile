
<cfparam name="inpAccountEMail" default="" />
<cfparam name="inpLimit" default="100" />
<cfheader name="expires" value="#now()#">
<cfheader name="pragma" value="no-cache">
<cfheader name="cache-control" value="no-cache, no-store, must-revalidate">

<!--- Dont allow any exports that take more than a hour to run.... --->
<cfsetting requestTimeout="3600" />
<cfparam name="prefixFileName" default="DispositionExport">

<cfparam name="inprel1" default="summary.summarytable">
<cfparam name="inprel2" default="CSV">
<cfparam name="inpStart" default="#dateformat(DateAdd('d', -30, NOW()),'yyyy-mm-dd')#">
<cfparam name="inpEnd" default="#dateformat(NOW(),'yyyy-mm-dd')#">
<cfparam name="inpBatchIdList" default="0000">
<cfparam name="campaignId" default="0">
<cfparam name="inpcustomdata1" default="">
<cfparam name="inpcustomdata2" default="">
<cfparam name="inpcustomdata3" default="">
<cfparam name="inpcustomdata4" default="">
<cfparam name="inpcustomdata5" default="">
<cfparam name="iDisplayLength" default="5000">
<cfparam name="iDisplayStart"  default="0">

<!--- used for report download naming convention --->
<cfif TRIM(inpcustomdata1) EQ "" >
	<cfset ReportUserId = session.UserId/>
<cfelse>
	<cfset ReportUserId = inpcustomdata1/>
</cfif>

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("/session/sire/assets/global/plugins/daterangepicker/daterangepicker.css", true)
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)
        .addCss("/session/sire/assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("/session/sire/assets/global/plugins/daterangepicker/moment.min.js", true)
        .addJs("/session/sire/assets/global/plugins/daterangepicker/daterangepicker.js", true)
        .addJs("/session/sire/assets/global/plugins/uikit/js/uikit.min.js", true)
        .addJs("/public/sire/js/select2.min.js")
        .addJs("https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js");
</cfscript>


<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />


<style>

     /* DivTable.com */
     .divTable{
     	display: table;
     	width: 100%;
     }
     .divTableRow {
     	display: table-row;
     }
     .divTableHeading {
     	background-color: #EEE;
     	display: table-header-group;
     }
     .divTableCell, .divTableHead {
     	border: 1px solid #999999;
     	display: table-cell;
     	padding: 3px 10px;
     }
     .divTableHeading {
     	background-color: #EEE;
     	display: table-header-group;
     	font-weight: bold;
     }
     .divTableFoot {
     	background-color: #EEE;
     	display: table-footer-group;
     	font-weight: bold;
     }
     .divTableBody {
     	display: table-row-group;
     }

     .divTableHead {
     	border: 1px solid #999999;
     	display: table-cell;
     	padding: 3px 10px;
          font-weight: bold;
     }

</style>


<cfset inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
<cfset inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">


<!--- BUQ me (Big Ugly Query) --->
<!--- Simple join won't work for multiple reasons , but need to avoid (lazy and dumb answer) running back to DB 1000's of times to get user info --->
<!--- Problem 1: is simple joins try to join across all unique IRE results - the answer is left-join-after-group-by --->
<!--- Solution: left-join-after-group-by  https://stackoverflow.com/questions/9390679/left-join-after-group-by ---->
<!--- Problem 2: Billing plans are duplicated each month - latest plan wins - can be active or inactive based on Status --->
<!--- Solution: Get max entry and group by User Id--->
<!--- Problem 3: Need counts for both MO and MT seperately --->
<!--- Solution: Use COUNT and combine in seperate CASE statements --->
<cfquery name="GetUserListQuery" datasource="#Session.DBSourceEBM#">
     SELECT
          ire.MO,
          ire.MT,
          ire.TOTALMESSAGES,
          CASE WHEN ire.UserId_int IS NULL THEN 0 ELSE ire.UserId_int END AS UserId_int,
          CASE WHEN ua.EmailAddress_vch IS NULL THEN 'SYSTEM' ELSE ua.EmailAddress_vch END AS EmailAddress_vch,
          CASE WHEN p.PlanName_vch IS NULL THEN 'INACTIVE' ELSE p.PlanName_vch END AS PlanName_vch
     FROM
     (
          SELECT
               UserId_int,
               COUNT(CASE WHEN IREType_int=1 THEN 1 END) as MT,
               COUNT(CASE WHEN IREType_int=2 THEN 1 END) as MO,
               (COUNT(CASE WHEN IREType_int=1 THEN 1 END) + COUNT(CASE WHEN IREType_int=2 THEN 1 END) ) AS TOTALMESSAGES
          FROM
               simplexresults.ireresults ire
          WHERE
               Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
          AND
              Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
          AND
               (IREType_int = 1 OR IREType_int = 2)
          GROUP BY
               UserId_int
          ORDER BY
               TOTALMESSAGES DESC
     ) AS  ire
     LEFT OUTER JOIN
          simpleobjects.useraccount ua  ON (ua.UserId_int = ire.UserId_int)
     LEFT OUTER JOIN
          (SELECT MAX(UserPlanId_bi), UserId_int, PlanId_int FROM simplebilling.userplans WHERE Status_int = 1 GROUP BY UserId_int ) up ON (up.UserId_int = ua.UserId_int)
     LEFT OUTER JOIN
          simplebilling.plans p ON (p.PlanId_int = up.PlanId_int)
</cfquery>


<style type="text/css">
    table td.center, table th.center{
        text-align: center;
    }
</style>

<div id="ReportSection" class="portlet light bordered subscriber">
    <div class="portlet-body form">


        <div class="row" style="margin-bottom:3em;">
            <div class="col-xs-12 col-sm-3">
				<h2 class="page-title" style="margin-top: 0;">Summary Report: <div class='pdfIconPrint no-print' onclick='OutputObjAsPDF($(this).parents("#ReportSection"), false)'></div></h2>
            </div>

	   		<div class="col-xs-12 col-sm-6 col-sm-offset-3">

		   		<!--- use manual styles to make sure pdf output lines up as expected - not all of the bootstrap stuff will show up correctly --->
			    <div class="date-range pull-right" style="text-align: right;">
			        <b>Selected Date Range</b><br/>
			        <div class="reportrange pull-right" id="reportrange">
			            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
			            <span></span> <b class="caret"></b>
			            <cfoutput>
			                <input type="hidden" name="inpStartDate" id="inpStartDate" value="#inpStart#">
			                <input type="hidden" name="inpEndDate" id="inpEndDate" value="#inpEnd#">
		                </cfoutput>
			        </div>
			    </div>
			</div>

        </div>

          <cfset MasterMessageTotal = 0 />
          <cfset MTTotal = 0 />
          <cfset MOTotal = 0 />

		<cfset LoopCounter = 0 />

         <!--- Filter bar --->
         <div class="row">

              <div class="col-xs-12">

                    <div class="divTable" style="width: 100%;border: 1px solid #000;" >
                         <div class="divTableBody">
                              <div class="divTableRow">
							<div class="divTableHead">Counter</div>
							<div class="divTableHead">User Id</div>
                                   <div class="divTableHead">eMail</div>
                                   <div class="divTableHead">MT Count</div>
                                   <div class="divTableHead">MO Count</div>
                                   <div class="divTableHead">Total Count</div>
                                   <div class="divTableHead">Plan</div>
                              </div>

                         <cfloop query="GetUserListQuery" >

						<cfset LoopCounter = LoopCounter + 1 />

                              <cfoutput>
                                   <div class="divTableRow">
								<div class="divTableCell">#LoopCounter#</div>
                                        <div class="divTableCell">#GetUserListQuery.UserId_int#</div>
                                        <div class="divTableCell">#GetUserListQuery.EmailAddress_vch#</div>
                                        <div class="divTableCell">#NumberFormat(GetUserListQuery.MT, ',')#</div>
                                        <div class="divTableCell">#NumberFormat(GetUserListQuery.MO, ',')#</div>
                                        <div class="divTableCell">#NumberFormat(GetUserListQuery.TOTALMESSAGES, ',')#</div>
                                        <div class="divTableCell">#GetUserListQuery.PlanName_vch#</div>
                                   </div>

                              </cfoutput>

                              <cfset MasterMessageTotal = MasterMessageTotal + GetUserListQuery.TOTALMESSAGES />
                              <cfset MTTotal = MTTotal + GetUserListQuery.MT />
                              <cfset MOTotal = MOTotal + GetUserListQuery.MO />
     <!---
                              <cfflush/>

                               <!--- <cfset sleep(2000)> ---> --->

                         </cfloop>

                         <cfoutput>
                              <div class="divTableRow">
							<div class="divTableCell">#LoopCounter#</div>
                                   <div class="divTableHead"></div>
                                   <div class="divTableHead"></div>
                                   <div class="divTableHead">#NumberFormat(MTTotal, ',')#</div>
                                   <div class="divTableHead">#NumberFormat(MOTotal, ',')#</div>
                                   <div class="divTableHead">#NumberFormat(MasterMessageTotal, ',')#</div>
                                   <div class="divTableHead"></div>
                              </div>
                         </cfoutput>

                    </div>
               </div>




          </div>
    </div>


</div>



<cfparam name="variables._title" default="Admin Testing">
<cfinclude template="../views/layouts/master.cfm">




     <script type="text/javascript">
      	$(function()
         {

		     var start = moment($("#inpStartDate").val(), "YYYY-MM-DD"); // moment().subtract(30, 'days');
     		var end =  moment($("#inpEndDate").val(), "YYYY-MM-DD");
     		var minDate = moment().subtract(365, 'days');

     	    if(start.diff(minDate, 'days') < 0){
     	    	start = minDate;
     	    }

     	    $('.reportrange').daterangepicker({
     	        startDate: start,
     	        endDate: end,
     	        "dateLimit": {
     	        	"years": 1
     	    	},
     	        ranges: {
     	           'Today': [moment(), moment()],
     	           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
     	           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
     	           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
     	           'This Month': [moment().startOf('month'), moment().endOf('month')],
     	           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
     	        },
     	        minDate: minDate
         	}, cb);

              function cb(start, end) {

     	        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

     	        //var dateStart = JSON.stringify({day: start.format('DD'), month: start.format('MM'), year: start.format('YYYY') });
     	        //var dateEnd = JSON.stringify({day: end.format('DD'), month: end.format('MM'), year: end.format('YYYY') });

     	        var dateStart = start.format('YYYY-MM-DD');
     	        var dateEnd =   end.format('YYYY-MM-DD');

     	        $('input[name="dateStart"]').data('date-start', start);
     	        $('input[name="dateEnd"]').data('date-end', end);
     	        var inpSubID = $('select[name="subcriber-list"]').val();

     			<cfoutput>
     				location.href = '/session/sire/pages/admin-user-report?inpStart=' + dateStart + '&inpEnd=' + dateEnd  <cfif inpcustomdata1 NEQ "">+ '&inpcustomdata1=#inpcustomdata1#'</cfif>;
      	        </cfoutput>
     	    }

     		<!--- Update hidden inputs with new values so reports can access them easily --->
     		$("#inpStartDate").val(start.format('YYYY-MM-DD'));
     		$("#inpEndDate").val(end.format('YYYY-MM-DD'));

     		$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));


         });

     </script>
