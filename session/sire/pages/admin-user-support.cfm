<cfscript>
	createObject("component", "public.sire.models.helpers.layout")
		.addJs("/public/sire/js/select2.min.js", true)
		.addCss("/public/sire/css/select2.min.css", true)
		.addCss("/public/sire/css/select2-bootstrap.min.css", true)
		.addJs("/session/sire/assets/pages/scripts/admin-user-support.js");
</cfscript>

<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<!--- 
********************************************************************************************	
************************************** DANGER!!!!!!!! **************************************	
********************************************************************************************	
--->

<cfparam name="AddKey" default="0">
<cfparam name="RemoveKey" default="0">
<cfparam name="IUserId" default="0">

<cfset AdminKeySet = 0 />
<cfset IUserIdSet = 0 />

<cfif AddKey EQ 1>

	<!--- Create new string for Cookie---> 
    <cfset strAdminKey = (
            CreateUUID() & ":" &
            CreateUUID() & ":" &
            #Session.USERID# & ":" &
            7 & ":" &  <!--- 9 is not on --- 7 is on --->
            CreateUUID() & ":" &                 
            CreateUUID()  
    ) />

	<cfset  strAdminKey = Encrypt(strAdminKey,APPLICATION.EncryptionKey_Cookies,"cfmx_compat","hex") />

	<!--- Store the cookie such that it will expire after 30 days. --->
    <cfcookie
        name="_ga_105"
        value="#strAdminKey#"
        expires="365"
    />

	<!--- Add cookie to this device so admins can impersonate users --->
	<cfset AdminKeySet = 1 />
	
</cfif>	

<cfif RemoveKey EQ 1>
	
	<cfcookie
        name="_ga_105"
        value="000000"
        expires="365"
    />
	
</cfif>

<cfif IUserId GT 0>
	<!----- used when login using username/email and password ---->
    <CFQUERY name="getUser" datasource="#Session.DBSourceREAD#">
        SELECT 
            Password_vch,
            UserId_int,
            PrimaryPhoneStr_vch,
            SOCIALMEDIAID_BI,
            EmailAddressVerified_vch,
            EmailAddress_vch,
            CompanyAccountId_int,
            Active_int,
            MFAContactString_vch,
            MFAContactType_ti,
            MFAEnabled_ti,
            MFAEXT_vch,
            FirstName_vch,
            LastName_vch
        FROM 
            simpleobjects.useraccount
        WHERE
        	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IUserId#">    
        					                        
    </CFQUERY>
	
	
	<cfif getUser.RECORDCOUNT GT 0>
	
		<cfset validated = true/>
	
		<cfset Session.MFAISON = getUser.MFAEnabled_ti /> 
		<cfset Session.MFALENGTH = LEN(TRIM(getUser.MFAContactString_vch) ) /> 
		<cfset Session.USERID = getUser.UserId_int>
		<cfset Session.FULLNAME = getUser.FirstName_vch & ' ' & getUser.LastName_vch>
		<cfset Session.FIRSTNAME = getUser.FirstName_vch>
		<cfset Session.USERNAME = getUser.EmailAddress_vch>
		<cfset Session.COMPANYID = 0>
		<cfset Session.CompanyUserId = 0>
		<cfset Session.isAdmin = 0>
		<cfset Session.PrimaryPhone = getUser.PrimaryPhoneStr_vch>
		<cfset Session.EmailAddress = getUser.EmailAddress_vch>
				
		<!--- SET SHORTCODE --->
		
		<!--- remove old method: 
		<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="getShortCode"></cfinvoke>				
		--->
		<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="getShortCode"></cfinvoke>
		
		<cfset Session.SHORTCODE = getShortCode.SHORTCODE>

		<cfset IUserIdSet = 1 />

	</cfif>	

</cfif>

<cfset AdminCookieData_ga_105 = "" />

<cftry>
	<!--- Read and decrypt admin cookie --->
	<cfset AdminCookieData_ga_105 = Decrypt(evaluate("COOKIE._ga_105"),APPLICATION.EncryptionKey_Cookies,"cfmx_compat","hex") />

	<cfcatch  type="any">

	</cfcatch>

</cftry>

<!--- See if admin cookie is set --->
<cfif ListLen(AdminCookieData_ga_105, ":") EQ 6>
	<!--- See if admin cookie is proper value --->
	<cfif ListGetAt(AdminCookieData_ga_105, 4, ":") EQ 7>
		<cfset AdminKeySet = 1 />
	</cfif>	
</cfif>	 


<div class="portlet light bordered subscriber">
	<div class="row">
		<div class="col-md-12">
			<h2 class="page-title">User Administration and Support</h2>
		</div>
	</div>
	<!--- Listing --->
	<div class="row">
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="form-gd">
			<cfif IUserIdSet EQ 1>
				<h4>The current logged in user has been changed to impersonate <cfoutput>UserId=(<strong>#Session.USERID#</strong>) UserName=(<strong>#Session.USERNAME#</strong>)</cfoutput></h4>
				<h4>You need to log out and re log in as administrator to impersonate someone else.</h4>
				<p><a class="active" href="campaign-manage">Campaigns</a></p>
			<cfelseif AdminKeySet EQ 1>
				<form name="frm-impersonate" method="POST">
					<div class="form-group">
					    <label for="IUserId" >Impersonate User Id <span class="text-danger">*</span></label>
					    <select name="user-id" id="user-id" class="form-control validate[required]"></select>
					    <input type="hidden" name="cur-user-id" value="<cfoutput>#Session.USERID#</cfoutput>">
					</div>
					<div class="form-group">
						<button type="submit" class="btn green-gd btn-re form-control" id="btn_SendNow">Impersonate Now</button>
					</div>
					<div class="form-group">
						<a class="active" href="admin-user-support?RemoveKey=1">Remove Admin Key from this device</a>
					</div>
				</form>
			<cfelse>
				<div class="form-group">
			 		<a class="active" href="admin-user-support?AddKey=1">Add Admin Key to this device</a>
	        	</div>
			</cfif>
			</div>
		</div>
		<div class="col-md-8 col-sm-6">
			
		</div>
	</div>
</div>


<cfparam name="variables._title" default="Admin User Support">
<cfinclude template="../views/layouts/master.cfm">

