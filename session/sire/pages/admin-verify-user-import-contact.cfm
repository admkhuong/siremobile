<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />
<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("../assets/global/plugins/daterangepicker/daterangepicker.css", true)        
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)
        .addJs("../assets/global/plugins/daterangepicker/moment.min.js", true)
        .addJs("../assets/global/plugins/daterangepicker/daterangepicker.js", true)
        .addJs('/session/sire/assets/global/scripts/filter-bar-generator.js', true)  
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)      
        
        .addJs("/public/sire/js/select2.min.js")
        
        .addJs("../assets/pages/scripts/admin-verify-user-import-contact.js");
</cfscript>


<cfoutput>
<cfinclude template="/public/sire/configs/bulk_insert_constants.cfm"/>
<script type="text/javascript">
    var uploadStatus = [];
    <cfoutput>
        <cfloop collection="#UPLOADS_STATUS_LANG#" item="key">
            uploadStatus[#key#] = "#UPLOADS_STATUS_LANG[key]#"
        </cfloop>
    </cfoutput>
</script>
<div class="portlet light bordered">
    <div class="portlet-body form">
    <ul class="uk-tab session-tab" uk-tab>
        <li><a href="##">Import Contacts</a></li>
        <li><a href="##">Individual Import</a></li>
    </ul>

    <ul class="uk-switcher uk-margin content-tab-pricing">
        <li>
            <div class="row">
                <div class="col-sm-3 col-md-4">
                    <h2 class="page-title">Import Contacts</h2>
                </div>
                <div class="col-sm-9 col-md-8">
                    <div id="box-filter" class="form-gd">

                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class="re-table table-data">
                        <div class="table-responsive">
                            <table id="upload-list" class="table table-striped table-responsive">
                
                            </table>
                        </div>
                    </div>                
                </div>
            </div>                   
        </li>
        <li> 
            <div class="row">
                <div class="col-sm-3 col-md-4">
                    <h2 class="page-title">Individual Import</h2>
                </div>
                <div class="col-sm-9 col-md-8">
                    <div id="box-filter1" class="form-gd">

                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class="re-table table-data">
                        <div class="table-responsive">
                            <table id="individual-list" class="table table-striped table-responsive">
                
                            </table>
                        </div>
                    </div>                
                </div>
            </div> 
        </li>  
    </ul>
    </div>
</div> 

</cfoutput>
<div class="modal fade" id="md-import-view-data" tabindex="-1" role="dialog" aria-hidden="true">	
    <div class="modal-dialog">
        <div class="modal-content set-width-350">        
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <b><h4>Data Preview</h4></b>
            </div>
            <div class="modal-body"> 
                <div class="row ">
                    <div class="col-xs-12">
                        <div class="re-table table-data">
                            <div class="table-responsive">
                                <table id="tbl-import-view-data" class="table table-striped table-responsive">
                    
                                </table>
                            </div>
                        </div>                
                    </div>
                </div>
            </div>        
        </div>
    </div>
</div>
<div class="modal fade" id="md-indi-view-data" tabindex="-1" role="dialog" aria-hidden="true">	
    <div class="modal-dialog">
        <div class="modal-content set-width-350">        
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <b><h4>Data Preview</h4></b>
            </div>
            <div class="modal-body"> 
                <div class="row ">
                    <div class="col-xs-12">
                        <div class="re-table table-data">
                            <div class="table-responsive">
                                <table id="tbl-indi-view-data" class="table table-striped table-responsive">
                    
                                </table>
                            </div>
                        </div>                
                    </div>
                </div>
            </div>        
        </div>
    </div>
</div>
<style>
    .action-box
    {
        min-width:170px !important;
    }
    .input-box{
         width: calc(100% - 170px) !important;
    }
</style>

<cfparam name="variables._title" default="Customer Import Approval">
<cfinclude template="../views/layouts/master.cfm">

