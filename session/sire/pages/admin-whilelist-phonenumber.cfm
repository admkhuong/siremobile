

<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />
<cfoutput>
	<cfscript>
	    CreateObject("component","public.sire.models.helpers.layout")
	    .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
	    .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)	 
	    .addCss("../assets/layouts/layout4/css/custom3.css") 	     	 
	    .addJs("../assets/pages/scripts/admin-whilelist-phonenumber.js");  	    	    
	</cfscript>
</cfoutput>

<div class="portlet light bordered">
	<div class="portlet-body">
		<div class="new-inner-body-portlet2">
			<div class="form-gd">
				<div class="row">
					<div>
						<div class="col-sm-6 col-xs-12">
							<h4 class="portlet-heading2">White Phone Number List</h4>
						</div>
					</div>
					<div>
						<div class="col-sm-3 col-xs-12">
							<div class="inner-addon left-addon">
								<i class="glyphicon glyphicon-search"></i>
								<input type="search" class="form-control" id="searchbox" placeholder="Search Phone Number">
							</div>
						</div>
						<div class="col-sm-3 col-xs-12">
							<div class="pull-right">
								<a href="#" class="btn btn-block green-gd pop-phone" data-id="0" data-toggle="modal" data-phone-action="new" data-target="#form-new-wphone-number">Add New White Phone Number</a>
							</div>
						</div>
					</div>
				</div>
				<div class="" style="margin-top: 10px">
			        <div class="row">
			            <div class="col-xs-12">
			                <div class="re-table">
			                    <div class="table-responsive">
			                        <table id="adm-phone-white-list" class="table table-striped table-responsive">
			            
			                        </table>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>			
			</div>
	    </div>
	</div>
</div>
<!--- modal--->
 <!--- edit message--->
 <div class="modal fade" id="form-new-wphone-number" tabindex="-1" role="dialog" aria-hidden="true">
	<form name="form-new-wphone-number" class="col-sm-12" id="form-new-wphone-number">
	    <div class="modal-dialog">
	        <div class="modal-content">
	        	<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<b><h4 class="modal-title" id="modal-tit">
						NEW WHITE PHONE NUMBER
					</h4></b>	
				</div>
	            <div class="modal-body">	               
	               <div class="modal-body-inner">  
	                   <div class="form-group">
                            <label><b>Phone Number</b></label>
                            <input type="text" class ="form-control validate[required,custom[usPhoneNumber]]" id="user_phone" value="">
                            <p id ="invalidphone" style="color: red; display:none; margin: 2px 0;">* Invalid phone number</p>
                        </div>
	             	</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn green-cancel" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn green-gd"  id="btnUpdateWPhone">Save</button>
				</div>
			</div>
		</div>
	</form>
 </div>
<cfparam name="variables._title" default="Admin - White Phone Number">
<cfinclude template="../views/layouts/master.cfm">
