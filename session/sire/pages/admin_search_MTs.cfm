<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("../assets/global/plugins/daterangepicker/daterangepicker.css", true)
        .addCss("/session/sire/assets/global/plugins/Chartist/chartist.min.css", true)
        .addCss("/session/sire/css/filter_table.css", true)
        .addJs("../assets/global/plugins/highchart/highcharts.js", true)
        .addJs("../assets/global/plugins/highchart/exporting.js", true)
        .addJs("../assets/pages/scripts/jquery.textfill.min.js")
        .addJs("/session/sire/assets/global/plugins/Chartist/chartist.min.js", true)
        .addJs("../assets/global/plugins/daterangepicker/moment.min.js", true)
        .addJs("../assets/global/plugins/daterangepicker/daterangepicker.js", true)
        .addJs("/session/sire/assets/global/scripts/filter-bar-generator.js")
        .addJs("/session/sire/assets/pages/scripts/admin_search_mts.js");  
</cfscript>


<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<div class="portlet light bordered">
    <!--- Filter bar --->
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-lg-12">
            <h2 class="page-title">Search MTs</h2>
        </div>
        <div class="col-xs-12 col-sm-12 col-lg-12">
            <!--- <div id="box-filter" class="form-gd">
            </div> --->

            <form id="search-mt-word-from">
                <div class="col-sm-6 col-xs-12 col-md-3 col-lg-3 mtsearchbox">
                    <input class="form-control validate[required]" id="searchbox" placeholder="Search Word">
                </div>

                <div class="col-sm-6 col-xs-12 col-md-3 col-lg-3 mtsearchbox">
                    <a type="button" class="btn green-gd" id="search-word">Search</a>
                </div>
            </form>
            

            <div class="col-sm-6 col-xs-12 col-md-6 col-lg-6 select-date-range-search-mt">
                <div class="admin-dashboard-block-title">Selected Date Range</div>
                <div class="" style="padding-left: 0px;margin-bottom: 30px">
                    <div class="date-range">
                        <div class="reportrange">
                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                            <span class="time"></span> <span class="pull-right" style="margin-right: 0"><b class="caret"></b></span>
                            <input type="hidden" name="dateStart" value="">
                            <input type="hidden" name="dateEnd" value="">
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <!--- Listing --->
    <div class="row">
        <div class="col-md-12">
            <div class="re-table">
                <div class="table-responsive">
                    <table id="tableDataList" class="table-striped dataTables_wrapper">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<cfparam name="variables._title" default="Admin Search MTs">
<cfinclude template="../views/layouts/master.cfm">