<cfscript>
    createObject("component", "public.sire.models.helpers.layout")
        .addJs("/public/js/jquery.tmpl.min.js", true)
        .addJs("/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js")
        .addCss("/public/js/jquery-ui-1.11.4.custom/jquery-ui.css", true)
        
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", false)

        .addCss("/session/sire/assets/global/plugins/uniform/css/uniform.default.css", true)
        .addJs('/session/sire/assets/global/scripts/filter-bar-generator.js', true)
        .addJs('/session/sire/assets/global/plugins/uniform/jquery.uniform.min.js', true)
        .addJs("/session/sire/assets/pages/scripts/admin_verify_payment_transaction.js");        
</cfscript>


<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfparam name="isDialog" default="0">
<cfparam name="isUpdateSuccess" default="">


<div class="portlet light bordered">
    <!--- Filter bar --->
    <div class="row">
        <div class="col-sm-3 col-md-4">
            <h2 class="page-title">Verify Payment Transaction</h2>
        </div>
        <div class="col-sm-9 col-md-8">
            <div id="box-filter" class="form-gd">
            </div>
        </div>
    </div>
    <!--- Listing --->
    <div class="row">
        <div class="col-md-12">
            <div class="re-table">
                <div class="table-responsive">
                    <table id="tblListEMS" class="table-striped dataTables_wrapper">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="RecurringLogModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:auto; max-width:900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Update Log</h4>

            </div>
            <div class="modal-body">

                <!---<div id="AdvanceScheduleContent">
                    <cfinclude template="../../ems/dsp_advance_schedule.cfm">
                </div>--->

			</div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<cfset RetCustomerInfo = ''>
<cfparam name="maskedNumber" default="">
<cfparam name="UserID" default="">
<cfparam name="expirationDate" default="">
<cfparam name="firstName" default="">
<cfparam name="lastName" default="">
<cfparam name="line1" default="">
<cfparam name="city" default="">
<cfparam name="state" default="">
<cfparam name="zip" default="">
<cfparam name="country" default="">
<cfparam name="phone" default="">
<cfparam name="emailAddress" default="">
<cfparam name="hidden_card_form" default="">

<!--- Change CC info before charge --->
<div id="buyaddon-modal-sections" class="uk-new-modal uk-modal modal-cardholder" uk-modal>
    <div class="uk-modal-dialog addon-modal-dialog">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <form id="addon-form">
            <!--- <input type="hidden" name="upgradePlanAmount" id="upgradePlanAmount" value="0">
            <input type="hidden" name="listKeyword" id="listKeyword" value="">
            <input type="hidden" name="listMLP" id="listMLP" value="">
            <input type="hidden" name="listShortUrl" id="listShortUrl" value="">
            <input type="hidden" name="currentPlan" id="currentPlan" value=""><!---#RetUserPlan.PLANID#--->
            <input type="hidden" name="planId" id="planId" value="0">
            <input type="hidden" name="monthYearSwitch" id="monthYearSwitch" value=""><!---#RetUserPlan.BILLINGTYPE#---> --->
            <input type="hidden" name="paymentId" id="paymentId" value="">
            <input type="hidden" name="paymentGateway" id="paymentGateway" value="1">
            <input type="hidden" name="userId" id="userId" value="">

            <div class="uk-modal-body form-gd">
                <div class="inner-update-cardholder">
                    <h4 class="uk-new-modal-title">Buy Credits - Keywords</h4>
                        <cfoutput>
                            <div class="update_cardholder_info">
                                <ul class="nav list-newinfo">
                                    <li id='vl_maskedNumber'>Card Number: </li>
                                    <li id='vl_expirationDate'>Expiration Date: </li>
                                    <li id='vl_fullname'>Cardholder Name: </li>
                                    <li id='vl_line1'>Address: </li>
                                    <li id='vl_city'>City: </li>
                                    <li id='vl_state'>State: </li>
                                    <li id='vl_zip'>Zip code: </li>
                                    <li id='vl_country'>Country: </li>
                                    <li id='vl_emailAddress'>Email: </li>
                                </ul>
                                <div class="form-group">
                                    <div class="radio-inline" id="rad-currentcard">
                                        <label>
                                            <input type="radio" name="select_used_card" class="select_used_card" id="optionsRadios1" value="1" checked>
                                            Current Card
                                        </label>
                                    </div>
                                    <div class="radio-inline" id="rad-newcard" style="padding-left: 22px !important;">
                                        <label>
                                            <input type="radio" name="select_used_card" class="select_used_card" id="optionsRadios2" value="2">
                                            New Card
                                        </label>
                                    </div>
                                </div>

                               <!---  <input type="hidden" id="amount-sms-to-send" name="" value="0">
                                <input type="hidden" id="amount-keyword-to-send" name="" value="0"> --->
                                <input type="hidden" id="amount" name="amount" value="0">
                                <input type="hidden" id="numberSMSToSend" name="numberSMSToSend" value="0">
                                <input type="hidden" id="numberKeywordToSend" name="numberKeywordToSend" value="0">
                                <input type="hidden" id="pricemsgafter" value=""><!---#RetUserPlan.PRICEMSGAFTER#--->
                                <input type="hidden" id="actionStatus" value="">
                                <input type="hidden" value="" id="h_email" name="h_email"><!---#userInfo.FULLEMAIL#--->
                                <input type="hidden" name="h_email_save" value="#emailAddress#">

                                <div class="fset_cardholder_info" style="<cfif hidden_card_form NEQ ''>display: none</cfif>">
                                <input type="radio" class="hidden" name="payment_method" class="check_payment_method" id="payment_method_0" value="0" checked/>
                                <div class="form-group">
                                    <div class="card-support-type">
                                        <img class="img-responsive" src="../assets/layouts/layout4/img/supported-payment-types.png" alt="">
                                    </div>
                                </div>

                                <div class="row row-small">
                                    <div class="col-lg-8 col-md-6">
                                        <div class="form-group">
                                            <label>Card Number <span>*</span></label>
                                            <input type="text" class="form-control validate[required,custom[creditCardFunc]] card_number" maxlength="32" id="card_number" name="number" value='' placeholder="---- ---- ---- 1234">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label>Security Code <span>*</span></label>
                                            <input type="text" class="form-control validate[required,custom[onlyNumber]] security_code" maxlength="4" id="security_code" name="cvv" data-errormessage-custom-error="* Invalid security code">
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-small">
                                    <div class="col-lg-8 col-md-6">
                                        <div class="form-group">
                                            <label>Expiration Date <span>*</span></label>
                                            <input type="hidden" id="expiration_date_upgrade" name="expirationDate" value="">
                                            <select class="form-control validate[required] expiration_date_month expiration_date_month_upgrade" id="expiration_date_month_upgrade" name="expiration_date_month_upgrade" class="expiration_date_month">
                                                <option value="" disabled selected>Month</option>
                                                <option value="01">January</option>
                                                <option value="02">February</option>
                                                <option value="03">March</option>
                                                <option value="04">April</option>
                                                <option value="05">May</option>
                                                <option value="06">June</option>
                                                <option value="07">July</option>
                                                <option value="08">August</option>
                                                <option value="09">September</option>
                                                <option value="10">October</option>
                                                <option value="11">November</option>
                                                <option value="12">December</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <select class="form-control validate[required]" id="expiration_date_year_upgrade" class="expiration_date_year" name="expiration_date_year_upgrade">
                                                <option value="" disabled selected>Year</option>
                                                <cfloop from="#year(now())#" to="#year(now())+50#" index="i">
                                                    <option value="#i#">#i#</option>
                                                </cfloop>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row row-small">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>Cardholder Name <span>*</span></label>
                                            <input type="text" class="form-control validate[required,custom[onlyLetterNumberSp]] first_name" maxlength="50" id="first_name" name="firstName" placeholder="First Name" value="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <input type="text" class="form-control validate[required,custom[onlyLetterNumberSp]] last_name" maxlength="50" id="last_name" name="lastName" placeholder="Last Name" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Address:<span>*</span></label>
                                    <input type="text" class="form-control validate[required,custom[noHTML]] cardholder line1" maxlength="60" id="line1" name="line1" data-value='#line1#'>
                                </div>
                                <div class="form-group">
                                    <label>City:<span>*</span></label>
                                    <input type="text" class="form-control validate[required,custom[noHTML]] cardholder city" maxlength="40" id="city" name="city" data-value='#city#'>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>State:<span>*</span></label>
                                            <input type="text" class="form-control validate[required,custom[noHTML]] cardholder state" maxlength="2" id="state" name="state" data-value="#state#">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Zip Code:<span>*</span></label>
                                            <input type="text" class="form-control validate[required,custom[onlyNumber]] cardholder zip" maxlength="5" id="zip" name="zip" data-value="#zip#" data-errormessage-custom-error="* Invalid zip code">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Country:<span>*</span></label>
                                    <select class="form-control cardholder validate[required] country" id="country" name="country" data-value="#country#" >
                                        <cfinclude template="../views/commons/payment/country.cfm">
                                    </select>
                                </div>
                                <input type="hidden" class="form-control validate[custom[phone]] cardholder phone" maxlength="30" id="phone" name="phone" data-value="#phone#">
                                <div class="form-group">
                                    <label>Email:</label>
                                    <input type="text" class="form-control validate[custom[email]] cardholder email" maxlength="128" id="email" name="email" data-value="#emailAddress#">
                                </div>
                                <div class="form-group">
                                    <label>
                                        <input type="checkbox" id="save_cc_info" name="save_cc_info" value="1" > 
                                        Save card information
                                    </label>
                                </div>
                                <hr>
                            </div>
                        </cfoutput>
                    
                    </div>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right buyaddon-footer-modal">
                <button type="submit" class="btn newbtn green-gd btn-purchase-cc">Purchase</button>
                <button type="button" class="btn newbtn1 green-cancel uk-modal-close btn-cancel-cc">Cancel</button>
            </div>
        </form>
    </div>
</div>
<!--- End change CC info before charge --->
<cfparam name="variables._title" default="Admin Verify Payment Transaction">
<cfinclude template="../views/layouts/master.cfm">

