<cfparam name="campaignId" default="0">

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("/session/sire/assets/global/plugins/daterangepicker/daterangepicker.css", true)
        .addCss("/session/sire/assets/global/plugins/Chartist/chartist.min.css", true)
        .addCss("/session/sire/assets/global/plugins/Chartist/plugins/tooltips/chartist-plugin-tooltip.css", true)
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)
        .addCss("/session/sire/assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("/session/sire/assets/global/plugins/daterangepicker/moment.min.js", true)
        .addJs("/session/sire/assets/global/plugins/daterangepicker/daterangepicker.js", true)
        .addJs("/session/sire/assets/pages/scripts/script-04.js")
        .addJs("/session/sire/assets/pages/scripts/campaign-reports.js")
        .addJs("/session/sire/assets/global/plugins/Chartist/chartist.min.js", true)
        .addJs("/session/sire/assets/global/plugins/Chartist/plugins/tooltips/chartist-plugin-tooltip.js", true)
        .addJs("/session/sire/assets/global/plugins/uikit/js/uikit.min.js", true)
        .addJs("/session/sire/assets/global/plugins/chartjs/Chart.bundle.js", true)
        .addJs("/session/sire/assets/global/scripts/filter-bar-generator.js")
        .addJs("/public/sire/js/select2.min.js");

</cfscript>

<cfinvoke method="GetBatchDetails" component="session.sire.models.cfc.control-point" returnvariable="RetVarGetBatchDetails">
    <cfinvokeargument name="inpBatchID" value="#campaignId#">
</cfinvoke>

<cfinvoke method="GetCampaignDetail" component="session.sire.models.cfc.campaign" returnvariable="RetVarBatchDetails">
    <cfinvokeargument name="INPCAMPAIGNID" value="#campaignId#">
</cfinvoke>

<cfinvoke method="GetTotalOptOutByBatch" component="session.sire.models.cfc.campaign" returnvariable="RetVarGetTotalOptOutByBatch">
    <cfinvokeargument name="inpBatchId" value="#campaignId#">
</cfinvoke>

<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="RetVarUserOrgInfo">
</cfinvoke>

<cfinvoke component="session.sire.models.cfc.reports" method="GetDataForSurveyReport" returnvariable="RetVarGetSurveyData">
    <cfinvokeargument name="inpBatchId" value="#campaignId#"/>
</cfinvoke>

<cfinvoke component="session.sire.models.cfc.reports" method="GetAllCustomersReponsedABatch" returnvariable="RetVarGetAllCustomersData">
    <cfinvokeargument name="inpBatchId" value="#campaignId#"/>
</cfinvoke>

<cfinvoke component="session.sire.models.cfc.campaign" method="GetCampaignBlastScheduleInfo" returnvariable="RetVarGetCampaignBlastScheduleInfoData">
    <cfinvokeargument name="inpBatchId" value="#campaignId#"/>
</cfinvoke>

<cfif RetVarGetBatchDetails.RECORDCOUNT LE 0>
    <cflocation url="/session/sire/pages/campaign-manage" addtoken="false">
</cfif>

<cfset campaignData.BatchId_bi = RetVarGetBatchDetails.BATCHID>
<cfset campaignData.Desc_vch = RetVarGetBatchDetails.DESC>
<cfset campaignData.EMS_Flag_int = RetVarGetBatchDetails.EMSFLAG>
<cfset campaignData.GroupId_int = RetVarGetBatchDetails.GROUPID>
<cfset campaignData.MLPID = RetVarGetBatchDetails.MLPID>
<cfset campaignData.TEMPLATEID = RetVarGetBatchDetails.TEMPLATEID>
<cfset campaignData.TemplateType = RetVarGetBatchDetails.TemplateType>

<input type="hidden" name="inpBatchId" id="inpBatchId" value="<cfoutput>#campaignData.BatchId_bi#</cfoutput>"/>
<input type="hidden" name="EMS_Flag" id="EMS_Flag" value="<cfoutput>#campaignData.EMS_Flag_int#</cfoutput>">
<input type="hidden" name="templateType" id="templateType" value="<cfoutput>#campaignData.TemplateType#</cfoutput>">
<input type="hidden" name="templateId" id="templateId" value="<cfoutput>#campaignData.TEMPLATEID#</cfoutput>">
<input type="hidden" name="campaignName" id="campaignName" value="<cfoutput>#campaignData.Desc_vch#</cfoutput>"/>
<input type="hidden" name="companyName" id="companyName" value="<cfoutput>#RetVarUserOrgInfo.ORGINFO.ORGANIZATIONNAME_VCH#</cfoutput>"/>
<input type="hidden" name="batchCreatedDate" id="batchCreatedDate" value="<cfoutput>#DateFormat(RetVarBatchDetails.CAMPAIGNOBJECT.CREATED_DT, "yyyy-mm-dd")#</cfoutput>"/>
<input type="hidden" name="minCustomerOrder" id="minCustomerOrder" value="1">
<input type="hidden" name="maxCustomerOrder" id="maxCustomerOrder" value="<cfoutput>#ArrayLen(RetVarGetAllCustomersData["DATALIST"])#</cfoutput>">

<style type="text/css">
    table td.center, table th.center{
        text-align: center;
    }
</style>

<div class="portlet light bordered subscriber">
    <div class="portlet-body form">
       
            <h2 class="page-title" style="margin-top: 0;">Report: <cfoutput>#campaignData.Desc_vch#</cfoutput></h2>
            <h4><cfoutput>#RetVarGetCampaignBlastScheduleInfoData.campaignBlastScheduleInfo#</cfoutput></h4>
            <h4><b>Total Opted-out: <a id="totalOptout" href="javascript:;"><cfoutput>#RetVarGetTotalOptOutByBatch.OPTOUT#</cfoutput></a></b></h4>
       
<!---         <h4 class="optin-display"><b>&nbsp;</b></h4> --->
        <div class="row">
            <div class="col-xs-12">
                
                <div class="options">
                        <div class="row">
                            <div class="col-xs-12 col-sm-3">
                                <cfif campaignData.TEMPLATEID NEQ 9>
                                    <div class="select-list">
                                        <p><b>Selected List</b></p>
                                        <select name="subcriber-list" class="form-control Select2">
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                </cfif>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                <div class="date-range">
                                    <p><b>Selected Date Range</b></p>
                                    <div class="reportrange pull-right">
                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                        <span></span> <b class="caret"></b>
                                        <input type="hidden" name="dateStart" value="">
                                        <input type="hidden" name="dateEnd" value="">
                                        <input type="hidden" name="inpBatchID" value="<cfoutput>#campaignId#</cfoutput>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row optin-display">
                    <!---   <div class="col-xs-12">
                                <button class="btn-re-dowload download-subcriber pull-right">
                                    <i class="fa fa-download"></i>
                                </button>
                            </div> --->
                    </div>
                    <!--- options select --->

                 <!---    <cfif campaignData.TEMPLATEID EQ 9>
                        <div class="options">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-sm-offset-6">
                                    <div class="date-range">
                                        <p><b>Selected Date Range</b></p>
                                        <div class="reportrange pull-right">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                            <span></span> <b class="caret"></b>
                                            <input type="hidden" name="dateStart" value="">
                                            <input type="hidden" name="dateEnd" value="">
                                            <input type="hidden" name="inpBatchID" value="<cfoutput>#campaignId#</cfoutput>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </cfif> --->

                    <div class="row">
                        <div class="col-xs-12">
                            <div id="highchart" class="campaign-report-detail">
                       
                            </div>
                        </div>
                    </div>
                    <!--- charts --->

                    <div class="row col-md-12 export-data-div">
                        <button class="btn green-gd export-data" id="export-multiple-choice"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORT</button>
                    </div>

                    <div class="row">
                        <div class="col-md-12 wrapper-tblcCampaignReportData">
                            <div class="subscriber-list">
                                <div class="re-table">
                                    <div class="table-responsive">
                                        <table id="tblcCampaignReportData" class="table-responsive table-striped dataTables_wrapper dataTable">
                                        </table>
                                    </div>
                                </div>    
                            </div>
                            <a href="##" class="expand-btn hidden">+ Expand</a>
                        </div>
                    </div>
             
<!---
             
               <!--- BEGIN: SUBCRIBER LIST --->
                        <div class="row optin-display sub-list-section">
                            <div class="col-lg-8 col-md-12 left-group-contact">
                                <div class="row">
                                                                        
                                </div>
                                
                                <div class="subscriber-list">
                                    <div class="re-table">
                                        <div class="table-responsive">
                                            <table id="tblSubcriberList" class="table table-bordered table-striped table-report">
                                                
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <div class="search-contact">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-4 col-xs-12 col-lg-4 col-sm-4" id="subscribered-countdiv">
                                                <label class="label-search-subscriber pull-left" id="subscribered-count"><strong>Subscribed: <span id="total-subcriber"></span></strong></label>
                                            </div>
                                            <div class="col-md-8 col-xs-12 col-lg-8 col-sm-8">
                                                <div class="search-subscriber">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="text" name="contact-keyword" class="form-control" placeholder="Search Contacts">
                                                            <input type="hidden" name="sub-id" value="">
                                                            <span class="input-group-addon btn-search-contact">
                                                                <i class="fa fa-search" aria-hidden="true"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="subscriber-phone-list">
                                    <div class="re-table">
                                        <div class="table-responsive">
                                            <table id="subcriberListDetail" class="table table-bordered table-hover">
                                                
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--- END :SUBCRIBER LIST ---> 
                        
--->
                        
            </div>
        </div>
    </div>
</div>



        

<!-- Modal rename template -->
<div class="modal fade" id="rename-subscriber-list" role="dialog">
    <div class="modal-dialog">
        <form id="rename-subscriber-list-form" name="rename-subscriber-list-form">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><strong>RENAME SUBSCRIBER LIST</strong></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>New subscriber list name:</label>
                        <input type="hidden" id="contact-group-id">
                        <input type="text" name="new-subscriber-list-name" class="form-control validate[required]" id="new-subscriber-list-name">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn-save btn btn-success-custom" >Save</button>
                    <button type="button" class="btn btn-back-custom" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>
    </div>
</div>


<!-- Modal -->
<div class="bootbox modal fade" id="AddNewSubscriberList" tabindex="-1" role="dialog" aria-labelledby="AddNewSubscriberList" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form name="frm-add-new">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title title">Add New Subscriber List</h4>            
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="subscriber_list_name">Subscriber List Name:<span class="text-danger"> *</span></label>
                        <input id="subscriber_list_name" class="form-control validate[required, custom[onlyLetterNumberSp]]" maxlength="255">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success-custom btn-save-group"> Save </button>
                    &nbsp; &nbsp; &nbsp; 
                    <button type="button" class="btn btn-back-custom" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal For Blast list-->
<div class="modal fade" id="messages-sent-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="">Messages Sent</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="blastMessageSentFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="message-blast-sent-table" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--- <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-keyword-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div> --->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>
<!--- end modal --->

<!-- Modal For Queued list-->
<div class="modal fade" id="messages-queued-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="">Messages Queued</strong></h4>
            </div>
            <div class="modal-body">
                <div id="cbghfghrfh">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="blastMessageQueuedFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="message-blast-queued-table" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--- <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-keyword-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div> --->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>
<!--- end modal --->

<!-- Modal For Blast list-->
<div class="modal fade" id="messages-wait-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="">Messages Waiting</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="blastMessageWaitFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="message-blast-wait-table" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--- <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-keyword-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div> --->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>
<!--- end modal --->

<!-- Modal For Blast list-->
<div class="modal fade" id="response-answer-detail-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <input type="hidden" name="" id="answer-cpid" value="">
            <input type="hidden" name="" id="answer-qid" value="">
            <input type="hidden" name="" id="answer-format" value="">
            <input type="hidden" name="" id="answer-text" value="">
            <input type="hidden" name="" id="answer-regex" value="">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="">Response detail</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="responseAnswerFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="response-answer-detail" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-response-answer-detail"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>
<!--- end modal --->
<!-- Modal Optout list-->
<div class="modal fade" id="mdOptOutList" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="">Opted-out List</strong></h4>
            </div>
            <div class="modal-body">
                <div id="cbghfghrfh">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="optFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="tblOptOutList" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload dowload-optout-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>
<!--- end modal --->

<cfparam name="variables._title" default="Campaign Reports #campaignData.Desc_vch# - Sire">
<cfinclude template="/session/sire/views/layouts/master.cfm">
