<cfheader name="expires" value="#now()#">
<cfheader name="pragma" value="no-cache">
<cfheader name="cache-control" value="no-cache, no-store, must-revalidate">

<!--- Dont allow any exports that take more than a hour to run.... --->
<cfsetting requestTimeout="3600" />
<cfparam name="prefixFileName" default="DispositionExport">

<cfparam name="inprel1" default="summary.summarytable">
<cfparam name="inprel2" default="CSV">
<cfparam name="inpStart" default="#dateformat(DateAdd('d', -30, NOW()),'yyyy-mm-dd')#">
<cfparam name="inpEnd" default="#dateformat(NOW(),'yyyy-mm-dd')#">
<cfparam name="inpBatchIdList" default="0000">
<cfparam name="campaignId" default="0">
<cfparam name="inpcustomdata1" default="">
<cfparam name="inpcustomdata2" default="">
<cfparam name="inpcustomdata3" default="">
<cfparam name="inpcustomdata4" default="">
<cfparam name="inpcustomdata5" default="">
<cfparam name="iDisplayLength" default="5000">
<cfparam name="iDisplayStart"  default="0">

<!--- used for report download naming convention --->
<cfif TRIM(inpcustomdata1) EQ "" >
	<cfset ReportUserId = session.UserId/>
<cfelse>
	<cfset ReportUserId = inpcustomdata1/>
</cfif>

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("/session/sire/assets/global/plugins/daterangepicker/daterangepicker.css", true)
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)
        .addCss("/session/sire/assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("/session/sire/assets/global/plugins/daterangepicker/moment.min.js", true)
        .addJs("/session/sire/assets/global/plugins/daterangepicker/daterangepicker.js", true)
        .addJs("/session/sire/assets/global/plugins/uikit/js/uikit.min.js", true)
        .addJs("/public/sire/js/select2.min.js")
        .addJs("https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js");
</cfscript>

<!---
<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("/session/sire/assets/global/plugins/daterangepicker/daterangepicker.css", true)
        .addCss("/session/sire/assets/global/plugins/Chartist/chartist.min.css", true)
        .addCss("/session/sire/assets/global/plugins/Chartist/plugins/tooltips/chartist-plugin-tooltip.css", true)
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)
        .addCss("/session/sire/assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("/session/sire/assets/global/plugins/daterangepicker/moment.min.js", true)
        .addJs("/session/sire/assets/global/plugins/daterangepicker/daterangepicker.js", true)
        .addJs("/session/sire/assets/pages/scripts/script-04.js")
        .addJs("/session/sire/assets/pages/scripts/campaign-reports.js")
        .addJs("/session/sire/assets/global/plugins/Chartist/chartist.min.js", true)
        .addJs("/session/sire/assets/global/plugins/Chartist/plugins/tooltips/chartist-plugin-tooltip.js", true)
        .addJs("/session/sire/assets/global/plugins/uikit/js/uikit.min.js", true)
        .addJs("/session/sire/assets/global/plugins/chartjs/Chart.bundle.js", true)
        .addJs("/session/sire/assets/global/scripts/filter-bar-generator.js")
        .addJs("/public/sire/js/select2.min.js");

</cfscript>
--->

<style>


	.pdfIconPrint {
	    background: url(/public/sire/images/grid_pdf_over.png) no-repeat;
	    height: 16px;
	    width: 16px;
	    text-decoration: none !important;
	    margin: 5px 0 0 8px;
	}

	@media print {

		.ranges{
			display: none;
		}
	}


</style>

<!---  See sample query logic at bottom of the page --->
<!--- New reporting requirement - keep all reports in a cfc and method in the session.sire.models.cfc.reports dir --->
<!--- component="session.sire.models.cfc.reports.#TargetCFC#" method="#TargetReportName#" --->
<!--- Queries used here need to be able to return limited data - same paging mechanism used in DataTables - LIMIT (START) (END) --->
<cfset TargetCFC = "" />
<cfset TargetReportName = "" />

<cfif ListLen(inprel1, '.') EQ 2>

	<cfset TargetCFC = ListGetAt(inprel1, 1, '.') />
	<cfset TargetReportName = ListGetAt(inprel1, 2, '.') />

</cfif>



<!--- Get query specified --->
	    <cfinvoke component="session.sire.models.cfc.reports.#TargetCFC#" method="#TargetReportName#" returnvariable="RetVarinprel1">
	        <cfinvokeargument name="inpBatchIdList" value="#inpBatchIdList#">
	        <cfinvokeargument name="inpStart" value="#inpStart#">
	        <cfinvokeargument name="inpEnd" value="#inpEnd#">
	        <cfinvokeargument name="OutQueryResults" value="1">
	        <cfinvokeargument name="inpcustomdata1" value="#inpcustomdata1#"/>
	        <cfinvokeargument name="inpcustomdata2" value="#inpcustomdata2#"/>
	        <cfinvokeargument name="inpcustomdata3" value="#inpcustomdata3#"/>
	        <cfinvokeargument name="inpcustomdata4" value="#inpcustomdata4#"/>
	        <cfinvokeargument name="inpcustomdata5" value="#inpcustomdata5#"/>
	        <cfinvokeargument name="iDisplayLength" value="#iDisplayLength#"/>
	        <cfinvokeargument name="iDisplayStart" value="#iDisplayStart#"/>
	    </cfinvoke>

<cfset RunningTotal = 0 />
<cfset RunningTotalMT = 0 />
<cfset RunningTotalMO = 0 />

<cfset LastBatchId = 0 />

<style type="text/css">
    table td.center, table th.center{
        text-align: center;
    }
</style>

<div id="ReportSection" class="portlet light bordered subscriber">
    <div class="portlet-body form">


        <div class="row" style="margin-bottom:3em;">
            <div class="col-xs-12 col-sm-3">
				<h2 class="page-title" style="margin-top: 0;">Summary Report: <div class='pdfIconPrint no-print' onclick='OutputObjAsPDF($(this).parents("#ReportSection"), false)'></div></h2>
            </div>

	   		<div class="col-xs-12 col-sm-6 col-sm-offset-3">

		   		<!--- use manual styles to make sure pdf output lines up as expected - not all of the bootstrap stuff will show up correctly --->
			    <div class="date-range pull-right" style="text-align: right;">
			        <b>Selected Date Range</b><br/>
			        <div class="reportrange pull-right" id="reportrange">
			            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
			            <span></span> <b class="caret"></b>
			            <cfoutput>
			                <input type="hidden" name="inpStartDate" id="inpStartDate" value="#inpStart#">
			                <input type="hidden" name="inpEndDate" id="inpEndDate" value="#inpEnd#">
		                </cfoutput>
			        </div>
			    </div>
			</div>

        </div>

        <cfloop query="#RetVarinprel1.QUERYRES#">

	        <cfset RunningTotal = RunningTotal + RetVarinprel1.QUERYRES.TOTALCOUNT />

	        <div class="row">
	            <div class="col-xs-12">

		            <cfoutput>

		        		<cfif LastBatchId NEQ RetVarinprel1.QUERYRES.BatchId_bi >

			        		<cfset LastBatchId = RetVarinprel1.QUERYRES.BatchId_bi />

							<cfinvoke method="GetBatchDesc" component="session.sire.models.cfc.control-point" returnvariable="RetVarGetBatchDesc">
							    <cfinvokeargument name="inpBatchID" value="#RetVarinprel1.QUERYRES.BatchId_bi#">
							</cfinvoke>

							<div class="col-xs-12"><h3>(#RetVarinprel1.QUERYRES.BatchId_bi#) #RetVarGetBatchDesc.DESC#</h3></div>

		            	</cfif>

		        		<div class="col-xs-12">
			        		<cfif RetVarinprel1.QUERYRES.IREType_int EQ 1>

				        		<cfset RunningTotalMT = RunningTotalMT + RetVarinprel1.QUERYRES.TOTALCOUNT />
				        		<h4>Outbound #NumberFormat(RetVarinprel1.QUERYRES.TOTALCOUNT, ',')#</h4>

				        	<cfelseif RetVarinprel1.QUERYRES.IREType_int EQ 2>

				        		<cfset RunningTotalMO = RunningTotalMO + RetVarinprel1.QUERYRES.TOTALCOUNT />
				        		<h4>Inbound #NumberFormat(RetVarinprel1.QUERYRES.TOTALCOUNT, ',')#</h4>

			        		</cfif>
		        		</div>

		            </cfoutput>

	            </div>

	        </div>

        </cfloop>

        <div class="row" style="margin-top: 3em;">
	        <div class="col-xs-12">
				<cfoutput>
					<h3>Total Outbound #NumberFormat(RunningTotalMT, ',')#</h3>
					<h3>Total Inbound #NumberFormat(RunningTotalMO, ',')#</h3>
					<h3>Total #NumberFormat(RunningTotal, ',')#</h3>
				</cfoutput>
		    </div>
	    </div>


    </div>
</div>



<cfparam name="variables._title" default="Campaign Usage Summary Report">
<cfinclude template="/session/sire/views/layouts/master.cfm">


<script type="text/javascript">
 	$(function()
    {

console.log($("#inpStartDate").val());

		var start = moment($("#inpStartDate").val(), "YYYY-MM-DD"); // moment().subtract(30, 'days');
		var end =  moment($("#inpEndDate").val(), "YYYY-MM-DD");
		var minDate = moment().subtract(90, 'days');

	    if(start.diff(minDate, 'days') < 0){
	    	start = minDate;
	    }

	    $('.reportrange').daterangepicker({
	        startDate: start,
	        endDate: end,
	        "dateLimit": {
	        	"years": 1
	    	},
	        ranges: {
	           'Today': [moment(), moment()],
	           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
	           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
	           'This Month': [moment().startOf('month'), moment().endOf('month')],
	           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	        },
	        minDate: minDate
    	}, cb);

         function cb(start, end) {

	        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

	        //var dateStart = JSON.stringify({day: start.format('DD'), month: start.format('MM'), year: start.format('YYYY') });
	        //var dateEnd = JSON.stringify({day: end.format('DD'), month: end.format('MM'), year: end.format('YYYY') });

	        var dateStart = start.format('YYYY-MM-DD');
	        var dateEnd =   end.format('YYYY-MM-DD');

	        $('input[name="dateStart"]').data('date-start', start);
	        $('input[name="dateEnd"]').data('date-end', end);
	        var inpSubID = $('select[name="subcriber-list"]').val();

			<cfoutput>
				location.href = '/session/sire/pages/advanced-templates/campaign-report-summary?inpStart=' + dateStart + '&inpEnd=' + dateEnd  <cfif inpcustomdata1 NEQ "">+ '&inpcustomdata1=#inpcustomdata1#'</cfif>;
 	        </cfoutput>
	    }

		<!--- Update hidden inputs with new values so reports can access them easily --->
		$("#inpStartDate").val(start.format('YYYY-MM-DD'));
		$("#inpEndDate").val(end.format('YYYY-MM-DD'));

		$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));


    });


    function OutputObjAsPDF(inpObj, FindPDFPrintClassFlag)
	{
		var html = "";


		if(FindPDFPrintClassFlag)
		{

			html = $(inpObj);

			html2canvas($(html), {

				allowTaint: false,
			    onrendered: function(canvas) {
			        // canvas is the final rendered <canvas> element

			        console.log(canvas);

			        var img = canvas.toDataURL();
			        console.log(img);

			        // window.open(img);

			        var HTMLForPDF = $('<div></div>');

			        console.log(HTMLForPDF);
			        console.log($(HTMLForPDF));

			        var image = new Image();
					image.id = "pic"
					image.src = canvas.toDataURL();
					$(HTMLForPDF).append(image);

					console.log($(HTMLForPDF).html());

			        $('body').append($('<form/>')
						  .attr({'action': '/session/sire/pages/advanced-templates/reports-print-pdf', 'method': 'post', 'id': 'replacer', 'target': '_blank'})
						  .append($('<input/>').attr({'type': 'hidden', 'name': 'inpHTML', 'value': html}))
						  .append($('<input/>').attr({'type': 'hidden', 'name': 'inpFileName', 'value': <cfoutput>'#TargetCFC#_#ReportUserId#_#inpStart#_#inpEnd#.pdf'</cfoutput> }))
						).find('#replacer').submit();

			    }
			});


			return;



		}
		else
		{
			html = $(inpObj).clone();

			html.find('.no-print').remove();
			html = html.html();
		}

		$('body').append($('<form/>')
		  .attr({'action': '/session/sire/pages/advanced-templates/reports-print-pdf', 'method': 'post', 'id': 'replacer', 'target': '_blank'})
		  .append($('<input/>').attr({'type': 'hidden', 'name': 'inpHTML', 'value': html}))
		  .append($('<input/>').attr({'type': 'hidden', 'name': 'inpFileName', 'value': <cfoutput>'#TargetCFC#_#ReportUserId#_#inpStart#_#inpEnd#.pdf'</cfoutput> })		  )
		).find('#replacer').submit();



	}

</script>
