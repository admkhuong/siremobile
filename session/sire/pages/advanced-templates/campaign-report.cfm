

<cfheader name="expires" value="#now()#">
<cfheader name="pragma" value="no-cache">
<cfheader name="cache-control" value="no-cache, no-store, must-revalidate">

<!--- Dont allow any exports that take more than a hour to run.... --->
<cfsetting requestTimeout="3600" />
<cfparam name="prefixFileName" default="DispositionExport">

<cfparam name="inprel1" default="summary.summarytable">
<cfparam name="inprel2" default="CSV">
<cfparam name="inpStart" default="#dateformat(DateAdd('d', -30, NOW()),'yyyy-mm-dd')#">
<cfparam name="inpEnd" default="#dateformat(NOW(),'yyyy-mm-dd')#">
<cfparam name="inpBatchIdList" default="0000">
<cfparam name="campaignId" default="0">
<cfparam name="inpcustomdata1" default="">
<cfparam name="inpcustomdata2" default="">
<cfparam name="inpcustomdata3" default="">
<cfparam name="inpcustomdata4" default="">
<cfparam name="inpcustomdata5" default="">
<cfparam name="iDisplayLength" default="5000">
<cfparam name="iDisplayStart"  default="0">

<style>

<!---
	table.dataTable thead .sorting_desc, table.dataTable thead .sorting_asc {
    	background-position: left;
    }
--->
	<!--- override the multiple custom.css and other css files - latest data table 1.10 and up uses :after css --->
	table.dataTable thead .sorting_asc,  table.dataTable thead .sorting_desc
	{
		background-image:none !important;

	}

	<!--- override the multiple custom.css and other css files --->
	table .fa, table .fa:hover {
    	color: inherit !important;
	}


	.dataTables_length
	{
		text-align: right;
	}

	<!--- If the col width numbers dont add up to 100% - force the last column to take up the remaining width - this helps keep smaller columns as specified --->
	table td:last-child {
	    width: 100%;
	}


    	td.details-control
    	{
        text-align:center;
        color:#4a798f;
	   cursor: pointer;
	}

	tr.shown td.details-control
	{
	    text-align:center;
	    color:#337ab7;
	}

	td.no-match-control
     {
    		text-align:center;
    		color:#4a798f;
    	 	cursor: pointer;

     }

      tr.shown td.no-match-control
      {
    	 	text-align:center;
    	 	color:#337ab7;
		cursor: pointer;
      }

	.answer-container
	{
		background-color: #f6fbfe;
	}

<!---
	Move the sort selection to the left ...maybe ... might not be worth it

	table.dataTable thead .sorting_desc:before {
    	content: "\e156";
	}

	table.dataTable thead .sorting_asc:before {
    	content: "\e155";
	}

	table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before
	{
	    position: absolute;
	    bottom: 8px;
	    left: 8px;
	    display: block;
	    font-family: 'Glyphicons Halflings';
	    opacity: 0.5;
	}
--->

.campaign-survey-report tbody tr:last-child {
     background: inherit !important;
}

// .campaign-survey-report tr:last-child {
//     background: #dedede !important;
// }


/* .campaign-survey-report-bucket-mover tbody tr:last-child {
    background: inherit !important;
} */

/* .even {
   background-color: #f6fbfe !important;
} */

.btn-add-bucket {
    color: #FFF;
    background-color: #74c37f;
    border-color: #74c37f;
    text-transform: uppercase;
    font-weight: 400;
}

</style>


<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("/session/sire/assets/global/plugins/daterangepicker/daterangepicker.css", true)
        .addCss("/session/sire/assets/global/plugins/Chartist/chartist.min.css", true)
        .addCss("/session/sire/assets/global/plugins/Chartist/plugins/tooltips/chartist-plugin-tooltip.css", true)
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)
        .addCss("/session/sire/assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("/session/sire/assets/global/plugins/daterangepicker/moment.min.js", true)
        .addJs("/session/sire/assets/global/plugins/daterangepicker/daterangepicker.js", true)
<!---         .addJs("/session/sire/assets/pages/scripts/campaign-reports.js") --->
        .addJs("/session/sire/assets/global/plugins/Chartist/chartist.min.js", true)
        .addJs("/session/sire/assets/global/plugins/Chartist/plugins/tooltips/chartist-plugin-tooltip.js", true)
        .addJs("/session/sire/assets/global/plugins/uikit/js/uikit.min.js", true)
        .addJs("/session/sire/assets/global/plugins/chartjs/Chart.bundle.js", true)
        .addJs("/session/sire/assets/global/scripts/filter-bar-generator.js")
        .addJs("/public/sire/js/select2.min.js")
	   .addCss("/session/sire/assets/global/plugins/bootstrap-select/css/bootstrap-select.css", true);

</cfscript>

<cfinvoke method="GetBatchDetails" component="session.sire.models.cfc.control-point" returnvariable="RetVarGetBatchDetails">
    <cfinvokeargument name="inpBatchID" value="#campaignId#">
</cfinvoke>

<cfinvoke method="GetCampaignDetail" component="session.sire.models.cfc.campaign" returnvariable="RetVarBatchDetails">
    <cfinvokeargument name="INPCAMPAIGNID" value="#campaignId#">
</cfinvoke>

<cfinvoke method="GetTotalOptOutByBatch" component="session.sire.models.cfc.campaign" returnvariable="RetVarGetTotalOptOutByBatch">
    <cfinvokeargument name="inpBatchId" value="#campaignId#">
</cfinvoke>

<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="RetVarUserOrgInfo"></cfinvoke>

<cfinvoke component="session.sire.models.cfc.reports" method="GetDataForSurveyReport" returnvariable="RetVarGetSurveyData">
    <cfinvokeargument name="inpBatchId" value="#campaignId#"/>
    <cfinvokeargument name="inpStartDate" value="#inpStart#">
  	<cfinvokeargument name="inpEndDate" value="#inpEnd#">
</cfinvoke>

<cfinvoke component="session.sire.models.cfc.reports" method="GetAllCustomersReponsedABatch" returnvariable="RetVarGetAllCustomersData">
    <cfinvokeargument name="inpBatchId" value="#campaignId#"/>
</cfinvoke>

<cfinvoke component="session.sire.models.cfc.campaign" method="GetCampaignBlastScheduleInfo" returnvariable="RetVarGetCampaignBlastScheduleInfoData">
    <cfinvokeargument name="inpBatchId" value="#campaignId#"/>
</cfinvoke>

<cfset campaignData.BatchId_bi = RetVarGetBatchDetails.BATCHID>
<cfset campaignData.Desc_vch = RetVarGetBatchDetails.DESC>
<cfset campaignData.EMS_Flag_int = RetVarGetBatchDetails.EMSFLAG>
<cfset campaignData.GroupId_int = RetVarGetBatchDetails.GROUPID>
<cfset campaignData.MLPID = RetVarGetBatchDetails.MLPID>
<cfset campaignData.TEMPLATEID = RetVarGetBatchDetails.TEMPLATEID>
<cfset campaignData.TemplateType = RetVarGetBatchDetails.TemplateType>
<cfoutput>
	<input type="hidden" name="inpBatchId" id="inpBatchId" value="#campaignData.BatchId_bi#"/>
	<input type="hidden" name="EMS_Flag" id="EMS_Flag" value="#campaignData.EMS_Flag_int#">
	<input type="hidden" name="templateType" id="templateType" value="#campaignData.TemplateType#">
	<input type="hidden" name="templateId" id="templateId" value="#campaignData.TEMPLATEID#">
	<input type="hidden" name="campaignName" id="campaignName" value="#campaignData.Desc_vch#"/>
	<input type="hidden" name="companyName" id="companyName" value="#RetVarUserOrgInfo.ORGINFO.ORGANIZATIONNAME_VCH#"/>
	<input type="hidden" name="batchCreatedDate" id="batchCreatedDate" value="#DateFormat(RetVarBatchDetails.CAMPAIGNOBJECT.CREATED_DT, "yyyy-mm-dd")#"/>
	<input type="hidden" name="minCustomerOrder" id="minCustomerOrder" value="1">
	<input type="hidden" name="maxCustomerOrder" id="maxCustomerOrder" value="#ArrayLen(RetVarGetAllCustomersData["DATALIST"])#">
</cfoutput>

<style type="text/css">
    table td.center, table th.center{
        text-align: center;
    }
</style>

<div class="portlet light bordered subscriber">
    <div class="">

	    <div class="row" style="margin-bottom:3em;">
            <div class="col-xs-12 col-sm-8">
				<h2 class="page-title" style="margin-top: 0;">Summary Report: <cfoutput>#campaignData.Desc_vch#</cfoutput> <div class='pdfIconPrint no-print' onclick='OutputObjAsPDF($(this).parents("#ReportSection"), false)'></div></h2>
            </div>

	   		<div class="col-xs-12 col-sm-4 xcol-sm-offset-3">

		   		<!--- use manual styles to make sure pdf output lines up as expected - not all of the bootstrap stuff will show up correctly --->
			    <div class="date-range pull-right" style="text-align: right;">
			        <b>Selected Date Range</b><br/>
			        <div class="reportrange pull-right" id="reportrange">
			            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
			            <span></span> <b class="caret"></b>
			            <cfoutput>
			                <input type="hidden" name="inpStartDate" value="#inpStart#">
			                <input type="hidden" name="inpEndDate" value="#inpEnd#">
		                </cfoutput>
			        </div>
			    </div>
			</div>

        </div>

        <h4><b>Total Opted-out: <a id="totalOptout" href="javascript:;"><cfoutput>#RetVarGetTotalOptOutByBatch.OPTOUT#</cfoutput></a></b></h4>

<!---         <h4 class="optin-display"><b>&nbsp;</b></h4> --->

    </div>
</div>

<!--- new survey --->

    <div class="portlet light bordered">
        <ul uk-tab="connect: .campaign-report-content" class="cp-report-tab">
            <li class="uk-active">
                <a href="#">Question Summaries</a>
            </li>
            <li>
                <a href="#">Session Responses</a>
            </li>
		  <li>
                <a href="#">After Session</a>
            </li>
        </ul>
    </div>

    <div class="portlet light bordered">
        <ul class="uk-switcher uk-margin campaign-report-content">
            <li>

            	<!---  --->
				<div class="portlet light">

					<cfinvoke component="session.sire.models.cfc.control-point" method="ReadXMLQuestions" returnvariable="RetVarReadXMLQuestions">
						<cfinvokeargument name="inpBatchId" value="#campaignId#">
						<cfinvokeargument name="inpDefaultOption" value="">
						<cfinvokeargument name="inpTypeFilter" value="ONESELECTION,SHORTANSWER">
					</cfinvoke>


					<cfloop array="#RetVarReadXMLQuestions.QUESTIONS#" index="CPOBJ">

			            <cfoutput>

							 <div class="portlet light">
			                    <div class="ques-item">
			                        <h4 class="title">Q#CPOBJ.RQ#: #CPOBJ.TEXT#</h4>

			                        <ul uk-tab class="cp-report-tab">
			                            <li><a href="##">Data Table</a></li>
			                            <cfif CPOBJ.TYPE EQ "ONESELECTION">
			                                <li><a href="##">Graph</a></li>
			                            </cfif>
			                        </ul>

			                        <ul class="uk-switcher uk-margin ques-list">
			                            <li>


					                        <cfif CPOBJ.TYPE EQ "ONESELECTION">

												<div class="re-table">
									                <div class="table-responsive ">
									                    <table id="q-results-table-#CPOBJ.ID#" class="table table-bordered table-striped q-results-table campaign-survey-report2" QID="#CPOBJ.ID#">

									                    </table>
									                </div>
									            </div>

									        <cfelse>

									        	<div class="re-table">
									                <div class="table-responsive ">
									                    <table id="q-results-open-table-#CPOBJ.ID#" class="table table-bordered table-striped q-results-open-table campaign-survey-report2" QID="#CPOBJ.ID#">

									                    </table>
									                </div>
									            </div>

					                        </cfif>

					                    </li>

			                            <cfif CPOBJ.TYPE EQ "ONESELECTION">
			                                <li>
			                                    <canvas id="q-graph-canvas-#CPOBJ.ID#"></canvas>
			                                </li>
			                            </cfif>
			                        </ul>
			                    </div>
							 </div>

			            </cfoutput>

					</cfloop>

				</div>

            </li>

            <li>

            	<div class="portlet light">
					<div class="re-table">
		                <div class="table-responsive ">
		                    <table id="session-table" class="table table-bordered table-striped session-table campaign-survey-report">

		                    </table>
		                </div>
		            </div>

		            <div id="question-details-container">


		            </div>
            	</div>

            </li>

		  <li>

			  <!--- <h4>User response this blast</h4>                     --->
			  <div class="row">
				 <div class="col-md-10">
					<div id="tblSubcriberListResponseBlastFilterBox">
					</div>
				 </div>
				 <div class="col-md-2">
					<button class="btn-response-dowload response-blast-list"><i class="fa fa-download" aria-hidden="true"></i></button>
				 </div>
			  </div>
			  <div class="row sub-list-section">
				 <div class="col-md-12">
					<div class="table-responsive re-table">
					    <table id="tblSubcriberListResponseBlast" class="table-responsive table-striped dataTables_wrapper dataTable">
					    </table>
					</div>
				 </div>
			  </div>

		  </li>

        </ul>
    </div>
<!--- end new survey --->


<cfparam name="variables._title" default="Campaign Reports #campaignData.Desc_vch# - Sire">
<cfinclude template="/session/sire/views/layouts/master3.cfm">


<script type="text/javascript">
 	$(function()
    {

    });

</script>

<script type="application/javascript">

	var inpBatchId = $('input[name="inpBatchID"]').val();
	var strAjaxQuery = "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true";
	var templateType = $('input[name="templateType"]').val();
	var templateId = $('input[name="templateId"]').val();
	var campaignName = $('input[name="campaignName"]').val();
	var filter = '';
	var sortDir = 'DESC';

	var start = moment().subtract(30, 'days');
    var end = moment();
    var minDate = moment().subtract(90, 'days');

	<cfif trim(inpStart) NEQ "">
		<cfoutput>
			start = moment('#inpStart#', 'YYYY-MM-DD')
		</cfoutput>
	</cfif>

	<cfif trim(inpEnd) NEQ "">
		<cfoutput>
			end = moment('#inpEnd#', 'YYYY-MM-DD')
		</cfoutput>
	</cfif>

	$(function()
    	{
	    <!---Date Range Stuff --->

	    if(start.diff(minDate, 'days') < 0){
	    	start = minDate;
	    }

	    $('.reportrange').daterangepicker({
		        startDate: start,
		        endDate: end,
		        "dateLimit": {
		        	"years": 1
		    	},
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        },
		        minDate: minDate
	    	}, cb);

         function cb(start, end) {

	        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

	        //var dateStart = JSON.stringify({day: start.format('DD'), month: start.format('MM'), year: start.format('YYYY') });
	        //var dateEnd = JSON.stringify({day: end.format('DD'), month: end.format('MM'), year: end.format('YYYY') });

	        var dateStart = start.format('YYYY-MM-DD');
	        var dateEnd =   end.format('YYYY-MM-DD');

	        $('input[name="dateStart"]').data('date-start', start);
	        $('input[name="dateEnd"]').data('date-end', end);
	        var inpSubID = $('select[name="subcriber-list"]').val();

			<cfoutput>
				location.href = '/session/sire/pages/advanced-templates/campaign-report?campaignId=<cfoutput>#campaignid#</cfoutput>&inpStart=' + dateStart + '&inpEnd=' + dateEnd  <cfif inpcustomdata1 NEQ "">+ '&inpcustomdata1=#inpcustomdata1#'</cfif>;
 	        </cfoutput>
	    }

		<!--- Update hidden inputs with new values so reports can access them easily --->
		$("#inpStartDate").val(start.format('YYYY-MM-DD'));
		$("#inpEndDate").val(end.format('YYYY-MM-DD'));

		$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));


		$(".Select2").select2(
        	{ theme: "bootstrap", width: 'auto'}
        );


	    $('[data-toggle="popover"]').popover({
            placement : 'top',
            trigger : 'hover'
		});

		$(document).on('click','#make-a-chat',function(){
			var phone= $(this).closest('li').find('#customer-respondent-phone').text();
			var alertCheckExitChatSession=CheckExistChatSession(phone);
			if(alertCheckExitChatSession !=="")
			{
				alertBox(alertCheckExitChatSession,'Oops!','');
				return false;
			}

			bootbox.dialog({
				message: '<h4 class="be-modal-title">Confirmation</h4><p>You are about to launch a chat session with this subscriber, please confirm!</p>',
				title: '&nbsp;',
				className: "be-modal",
				buttons: {
					success: {
						label: "Continue",
						className: "btn btn-medium green-gd",
						callback: function(result) {
							//make new chat
							$.ajax({
								url: '/session/sire/models/cfc/smschat.cfc?method=CheckExistChatCampaign' + strAjaxQuery,
								type: 'POST',
								data: {

								},
								beforeSend: function () {
									$("#processingPayment").show();
								}
							})
							.done(function(data) {
								if (parseInt(data.RXRESULTCODE) == 1) {
									$("#processingPayment").hide();
									StartChatSession(phone,data.KEYWORD);
								} else {
									alertBox('There is no chat campaign available, please create one','Oops !','');
								}
							})
							.fail(function(e, msg) {

							})
							.always(function() {
								$("#processingPayment").hide();
							});
						}
					},
					cancel: {
						label: "Cancel",
						className: "green-cancel"
					},
				}
			});


		});

		function CheckExistChatSession(phone)
		{
			var strReturn='';
			$.ajax({
				url: '/session/sire/models/cfc/smschat.cfc?method=CheckExistChatSession' + strAjaxQuery,
				type: 'POST',
				async: false,
				data: {
					inpContactString: phone
				},
				beforeSend: function () {
					$("#processingPayment").show();
				}
			})
			.done(function(data) {
				if (parseInt(data.RXRESULTCODE) == 1) {
					if(data.AVAILABLESESSION ==1)
					{
						strReturn=data.ALERTRETURN;
					}
				} else {
					alertBox(data.MESSAGE, 'Oops!');
				}
			})
			.fail(function(e, msg) {

			})
			.always(function() {
				$("#processingPayment").hide();
			});
			return strReturn;
		}
		function StartChatSession(phone,keyword){
			//make new chat
			$.ajax({
				url: '/session/sire/models/cfc/smschat.cfc?method=MakeNewChatSessionForContactString' + strAjaxQuery,
				type: 'POST',
				data: {
					inpKeyword: keyword,
					inpContactString: phone,
					inpSkipCheckOPT:1
				},
				beforeSend: function () {
					$("#processingPayment").show();
				}
			})
			.done(function(data) {
				data = JSON.parse(data);
				if (parseInt(data.RXRESULTCODE) > 0) {
					location.href="/session/sire/pages/sms-response?keyword="+keyword+"&ssid="+data.NEWSESSIONID;
				} else {
					alertBox(data.MESSAGE, 'Oops!');
				}
			})
			.fail(function(e, msg) {

			})
			.always(function() {
				$("#processingPayment").hide();
			});
			//
		}

	});





</script>



<!--- Load Tables --->
<script type="application/javascript">

    $(function()
    {

	    <!--- Look for all q-results-table and then load them --->
	    $.each($('.q-results-table'), function() {
		   LoadSelectionTableData($(this).attr('QID'));
		});

		 <!--- Look for all q-results-table and then load them --->
	    $.each($('.q-results-open-table'), function() {
		   LoadOpenEndedTableData($(this), $(this).attr('QID'));
		});


	});

	<!--- Load table for all possible --->
	function LoadSelectionTableData(inpQId)
	{
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/reports/response.cfc?method=ResponseTable&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			async: true,
			data:
			{
				inpBatchId : "<cfoutput>#campaignId#</cfoutput>",
				inpStart: "<cfoutput>#inpStart#</cfoutput>",
				inpEnd: "<cfoutput>#inpEnd#</cfoutput>",
				inpQId : inpQId
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},
			success:
			<!--- Default return function for Async call back --->
			function(d)
			{
				<!--- for some reason manually created JSON returned in d is not being retunred here as clean JSON - so parse it--->
				// var dObj = JSON.parse(d)
				if (parseInt(d.RXRESULTCODE) == 1)
				{

					<!--- ***TODO: better off making whole td clickable - just add a class and then make class clickable --->
					<!--- Dynamically format data for columns as links as needed--->

					var FormatAnswerDataCol = function(inpObj)
					{
						if(inpObj[0] == "No Match" || inpObj[0] == "NO MATCH")
						{
							return inpObj[1];
							<!--- This was moved to sub table at the row level
							return '<a class="response-no-match-details" data-inpQId="' + inpQId +'">' + inpObj[1] + '</a>';
							--->
						}
						else
						{
							return inpObj[1];
						}
					}

					var inpTopParentTable = $('#q-results-table-' + inpQId).DataTable({
						"aaData": d.aaData,
				        "bStateSave": false,
						"iStateDuration": -1,
						"bProcessing": true,
						"bFilter": false,
						"bServerSide":false,
						"bDestroy":true,
						"sPaginationType": "simple_numbers",
					    "bLengthChange": false,
						"iDisplayLength":20,
						"bAutoWidth": false,
						"aaSorting": [],
					    "aoColumns": [
							// { "sClass": 'details-control', "bSortable": false, "mData": null, "sDefaultContent": 'qqq', "sWidth": "10px"},
							{"mData": null, "sClass": 'details-control', "sDefaultContent": '', "sWidth": "10px", "bSortable": false,

								"mRender": function (data, type, full) {

									if(data[0] == "No Match" || data[0] == "NO MATCH")
									{

										 return '<i class="fa fa-plus-square" aria-hidden="true"></i>';
									}
									else
									{
										return '';
									}

			                     }


							},
							{"mData": [0], "sTitle": 'Answer Choices', "bSortable": false, "sWidth": "30%" },
					        {"mData":FormatAnswerDataCol, "sTitle": 'Responses', "bSortable": false, "sWidth": "30%" },
					        {"mData": [2], "sTitle": 'Percentage', "bSortable": false, "sWidth": "30%" }
					   ],

					   	"rowCallback": function( row, data )
			   			{

			   				/* dont let rows wrap ... for now... */
			   			 	$('td', row).attr('nowrap','nowrap');
			   			 	return row;
			   		 	},

						"drawCallback": function( oSettings )
						{
							/* Update CSS for last row color */
							var api = this.api();

							$(api.rows(':last').nodes()).css('background', '#dedede');
							// api.rows(':last').nodes().css('background', '#dedede');
						}

					});

					/* Formatting function for row details - modify as you need */
					function format ( d2 ) {
					    // `d2` is the original data object for the row

					    return '<table class="table table-bordered table-striped q-results-open-table campaign-survey-report-bucket-mover"></table>';

					}

					<!--- Add event listener for opening and closing details --->
				    $('#q-results-table-' + inpQId + ' tbody').on('click', 'td.details-control', function () {

					   var tr = $(this).closest('tr');
				        var row = inpTopParentTable.row( tr );
				        var tdi = tr.find("i.fa");

				        if ( row.child.isShown() )
				        {
				            <!--- This row is already open - close it --->
				            row.child.hide();
				            tr.removeClass('shown');
				            tdi.first().removeClass('fa-minus-square');
			                tdi.first().addClass('fa-plus-square');
				        }
				        else
				        {
				            <!--- Open this row --->

				            <!--- create the new table - do optional formatting based on row data --->
				            var inpSubTable = format(row.data());

				            <!--- Add the new table to the row --->
				            row.child( inpSubTable ).show();

				            <!--- initialize and load the new table --->
				            InitSubDataTable( $( row.child().find('table') ), inpQId, inpTopParentTable);

						  <!--- Make the sub table distinctive from top level table --->
				            row.child().css("background-color", "#f6fbfe");

						  <!--- row.child().find('label').css("color", "rgb(255, 255, 255)"); --->

				            tr.addClass('shown');
							tdi.first().removeClass('fa-plus-square');
							tdi.first().addClass('fa-minus-square');

				        }
				    });

					<!--- Use same data from Table to load chart --->
					LoadSelectionChartData(d.aaData, inpQId);

				}
				else
				{
					<!--- No result returned --->
					bootbox.alert("General Error processing your request.");
				}
			}
		});

	}


	function LoadSelectionChartData(aaData, inpQId)
	{
		<!---
			aaData is an array of arrays

				aaData[j][0] = Answer text
				aaData[j][1] = Total Count
				aaData[j][2] = Percentage
		--->

		var chartLabels = [];
		var chartData = [];

		var tempLabel;

		<!--- Dont show summary row from table in Graph - use  (aaData.length - 1) --->
		for (var j = 0; j < aaData.length - 1; j++)
		{
			if (aaData[j][0].toString().length > 20){
                tempLabel = aaData[j][0].toString().substring(0, 20)+'...';
            }
            else{
                tempLabel = aaData[j][0].toString();
            }

			chartLabels.push((j+1) + '-' + tempLabel);

			chartData.push(aaData[j][2]);

		}

		<!--- handle graph chart --->
    	var horizontalBar1Data = {
    		labels: chartLabels,
    		datasets: [
    			{
    				label: "",
    				backgroundColor: "#578ca5",
		    		borderColor: "#578ca5",
		    		borderWidth: 1,
		    		data: chartData
    			}
    		]
    	}

		var options = {
			responsive: true,
			legend: {
				display: false
			},
		    scales: {
		        xAxes: [{
		            gridLines: {
		                display: true
		            },
		            ticks: {
	                	min: 0,
	                	max: 100,
	                	callback: function(value){return value+ "%"}
	                }
		        }],
		        yAxes: [{
		            gridLines: {
		                display: false
		            },
		            categoryPercentage: 1,
            		barPercentage: 0.4
		        }]
		    },
		}

    	var ctx = document.getElementById("q-graph-canvas-" + inpQId).getContext("2d");
    	window.horizontalBar1 = new Chart(ctx, {
    		type: 'horizontalBar',
    		data: horizontalBar1Data,
    		options: options
    	});

	}


	<!--- Load dynamic tables for open ended question answer counts --->
	function LoadOpenEndedTableData(inpObj, inpQId)
	{
		inpObj.dataTable( {

			"bProcessing": true,
			"iDisplayLength": 5,
			"aLengthMenu": [[5, 10, 25, 50, 100, 1000], [5, 10, 25, 50, 100, 1000]],
			"bLengthChange": true,
			"aaSorting": [[ 1, "desc" ]],
			"bServerSide": true,
			"pagingType": "simple_numbers",
			"bAutoWidth": false,
			"responsive": true,
			"sAjaxSource": '/session/sire/models/cfc/reports/response.cfc?method=ResponseOpenEndedTable&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			 "aoColumns": [
						        {"sTitle": 'Responses', "bSortable": false, "sWidth": "50%" },
						        {"sTitle": 'Count', "bSortable": true, "sWidth": "50%" }
						    ],
			"fnServerData": function ( sSource, aoData, fnCallback ) {

					<!--- These value pairs need to be on a seperate line each --->
					aoData.push({"name": "inpBatchId", "value": encodeURI("<cfoutput>#campaignId#</cfoutput>")});
					aoData.push({"name": "inpStart", "value": "<cfoutput>#inpStart#</cfoutput>"});
					aoData.push({"name": "inpEnd", "value": "<cfoutput>#inpEnd#</cfoutput>"});
					aoData.push({"name": "inpQId", "value": inpQId});

					$.getJSON( sSource, aoData, function (json) {
						fnCallback(json);
                    });
              },
			  "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                    $('td', nRow).attr('nowrap','nowrap');
                    return nRow;
               },
			   "fnInitComplete":function(oSettings, json){

					inpObj.find('thead tr th').each(function(index){

						$this = $(this);
						if($this.hasClass("nosort"))
						{
							<!--- disable sorting on column "1" --->
							oSettings.aoColumns[$this.index()].bSortable = false;
							<!--- hide arrows --->
							$this.css("background","none");
						}

					  });

				},
			   "fnDrawCallback": function( oSettings ) {

					<!--- For tables that are hidden overflow - add full text to title --->
					inpObj.find('tbody tr td').each(function(index){

						$this = $(this);
						var titleVal = $this.text();
						if (titleVal != '') {
						  $this.attr('title', titleVal);
						}
					  });

				}


    	} );
	}

	function InitSubDataTable(inpObj, inpQId, inpTopParentTable)
	{
		var NewSubTable = inpObj.DataTable( {

			"bProcessing": true,
			"iDisplayLength": 5,
			"aLengthMenu": [[5, 10, 25, 50, 100, 1000], [5, 10, 25, 50, 100, 1000]],
			"bLengthChange": true,
			"aaSorting": [[ 2, "desc" ]],
			"bServerSide": true,
			"pagingType": "simple_numbers",
			"bAutoWidth": false,
			"responsive": true,
			"sAjaxSource": '/session/sire/models/cfc/reports/response.cfc?method=LoadNoMatchesTable&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			 "aoColumns": [



							{"mData": null, "sClass": 'no-match-control', "sDefaultContent": '', "sWidth": "6%", "bSortable": false,

							 "mRender": function (data, type, full) {

								 if(parseInt(data[1]) > 0)
								 {

									  return '<i class="fa fa-plus-square" aria-hidden="true"></i>';
								 }
								 else
								 {
									 return '';
								 }

								}
							},


					        	{"mData": [0],"sTitle": 'Responses', "bSortable": false, "sWidth": "47%", "sClass": 'nosort' },
					        	{"mData": [1],"sTitle": 'Count', "bSortable": true, "sWidth": "47%" }
						],
			"fnServerData": function ( sSource, aoData, fnCallback ) {

					<!--- These value pairs need to be on a seperate line each --->
					aoData.push({"name": "inpBatchId", "value": encodeURI("<cfoutput>#campaignId#</cfoutput>")});
					aoData.push({"name": "inpStart", "value": "<cfoutput>#inpStart#</cfoutput>"});
					aoData.push({"name": "inpEnd", "value": "<cfoutput>#inpEnd#</cfoutput>"});
					aoData.push({"name": "inpQId", "value": inpQId});

					$.getJSON( sSource, aoData, function (json) {
						fnCallback(json);
	                });
	          },
			  "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
	                $('td', nRow).attr('nowrap','nowrap');
	                return nRow;
	           },
			   "fnInitComplete":function(oSettings, json){

					inpObj.find('thead tr th').each(function(index){

						/* Manually adding the nosort class will help  hide the inactive sorting arrows -  set an "sClass": 'nosort' in the column defs above for this to work */
						$this = $(this);
						if($this.hasClass("nosort"))
						{
							<!--- disable sorting on column "1" --->
							oSettings.aoColumns[$this.index()].bSortable = false;
							<!--- hide arrows --->
							$this.css("background","none");
						}

					  });

				},
			   "fnDrawCallback": function( oSettings ) {

					<!--- For tables that are hidden overflow - add full text to title --->
					inpObj.find('tbody tr td').each(function(index){

						$this = $(this);
						var titleVal = $this.text();
						if (titleVal != '') {
						  $this.attr('title', titleVal);
						}
					  });

				}


		});

		/* Formatting function for row details - modify as you need */
		function format3 ( d3 ) {
		    // `d3` is the original data object for the row

		    return '<table class="table table-bordered table-striped q-results-open-table campaign-survey-report-bucket-mover"></table>';

		}

		<!--- Add event listener for opening and closing details --->

		 $(NewSubTable.table().body()).on('click', 'td.no-match-control', function () {

		   var tr = $(this).closest('tr');
		   var row = NewSubTable.row( tr );
		   var tdi = tr.find("i.fa");

		   if ( row.child.isShown() )
		   {
			  	<!--- This row is already open - close it --->
				row.child.hide();
				tr.removeClass('shown');
				tdi.first().removeClass('fa-minus-square');
				tdi.first().addClass('fa-plus-square');
		   }
		   else
		   {
			  <!--- Open this row --->

			  <!--- create the new table - do optional formatting based on row data --->
			  var inpSubTable = format3(row.data());

			  <!--- Add the new table to the row --->
			  row.child( inpSubTable ).show();

			  <!--- initialize and load the new table --->
			  InitSubDataTableAnswerBuckets( $( row.child().find('table') ), inpQId, row.data()[0], inpTopParentTable);

			  <!--- Make the sub table distinctive from top level table --->
			  row.child().css("background-color", "#f6fbfe");

			  <!--- row.child().find('label').css("color", "rgb(255, 255, 255)"); --->

			  tr.addClass('shown');
				tdi.first().removeClass('fa-plus-square');
				tdi.first().addClass('fa-minus-square');

		   }
	    });

	}

	function InitSubDataTableAnswerBuckets(inpObj, inpQId, inpResponseRaw, inpTopParentTable)
	{
		var NewSubTable = inpObj.DataTable( {

			"bProcessing": true,
			"lengthChange": false,
			"paging": false,
			"bSort" : false,
			"bLengthChange": true,
			"bServerSide": true,
			"bAutoWidth": false,
			"responsive": true,
			"sAjaxSource": '/session/sire/models/cfc/reports/response.cfc?method=ResponseOptionListTable&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"aoColumns": [
							/* First 2 rows stores AVAL and TEXT value of raw response to update */
							{"mData": null, "bVisible": false },
							{"mData": null, "bVisible": false },
						   	{"mData": null, "sTitle": 'Response Buckets for {' + inpResponseRaw + '}', "bSortable": false, "sWidth": "75%",
								"mRender": function (data, type, full)
								{
									if (data[0] != data[1] && data[0].length != 0 )
									{
										return data[0] + ' ' + data[1];
									}
									else
									{
										return data[1];
									}
								}
							},
						   	{"mData": null, "sWidth": "25%", "bSortable": false,
						   		"mRender": function (data, type, full)
								{

									return '<button type="button" class="btn btn-success btn-add-bucket">Add to This Bucket</button>';
								}
							},
						],

			"fnServerData": function ( sSource, aoData, fnCallback ) {

				<!--- These value pairs need to be on a seperate line each --->
				aoData.push({"name": "inpBatchId", "value": encodeURI("<cfoutput>#campaignId#</cfoutput>")});
				aoData.push({"name": "inpStart", "value": "<cfoutput>#inpStart#</cfoutput>"});
				aoData.push({"name": "inpEnd", "value": "<cfoutput>#inpEnd#</cfoutput>"});
				aoData.push({"name": "inpQId", "value": inpQId});

				$.getJSON( sSource, aoData, function (json) {
					fnCallback(json);
			 	});
			},

			/* called after each TR is created and added to the DOM */
			"createdRow": function( row, data, dataIndex )
			{

				/* for each row look for the btn-add-bucket button */
			    	$(row).find('.btn-add-bucket').each(function(id, el)
			    	{
					/* Setup a click function call for each row */
				    	$(this).click(function()
				    	{
					    	if (data[0] != data[1] && data[0].length != 0 )
					    	{
					    		UpdateResponseBucket($(this), data[0], inpQId, inpResponseRaw, inpTopParentTable);
				    		}
					    	else
					    	{
					    		UpdateResponseBucket($(this), data[1], inpQId, inpResponseRaw, inpTopParentTable);
					    	}
				    	});
			    });
			},

		  	"rowCallback": function( row, data )
			{

				/* dont let rows wrap ... for now... */
			 	$('td', row).attr('nowrap','nowrap');
			 	return row;
		 	},

			"fnInitComplete":function(oSettings, json){

				inpObj.find('thead tr th').each(function(index){

					$this = $(this);
					if($this.hasClass("nosort"))
					{
						<!--- disable sorting on column "1" --->
						oSettings.aoColumns[$this.index()].bSortable = false;
						<!--- hide arrows --->
						$this.css("background","none");
					}

				  });

			},

		   	"drawCallback": function( oSettings )
		   	{
				<!--- For tables data that might have hidden overflow - add full text to title --->
				inpObj.find('tbody tr td').each(function(index)
				{
					$this = $(this);
					var titleVal = $this.text();
					if (titleVal != '') {
					  $this.attr('title', titleVal);
					}
				});

			}


		});

	}



</script>




<!--- Browse session for responses --->
<script type="application/javascript">

    $(function()
    {
	    <!--- Start session Table --->
	    LoadSessionsTableData($('#session-table'));


	});


	<!--- Load dynamic tables for open ended question answer counts --->
	function LoadSessionsTableData(inpObj)
	{
		var oTable = inpObj.DataTable( {

			"bProcessing": true,
			"iDisplayLength": 5,
			"aLengthMenu": [[1, 5, 10, 25, 50, 100], [1, 5, 10, 25, 50, 100]],
			"bLengthChange": true,
			"aaSorting": [[ 0, "desc" ]],
			"bServerSide": true,
			"pagingType": "simple_numbers",
			"bAutoWidth": false,
			"responsive": true,
			"sAjaxSource": '/session/sire/models/cfc/reports/response.cfc?method=GetSessionsTableData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"createdRow": function( row, data, dataIndex ) {
				<!-- as we create each row - store the hidden SessionId as an attribute --->


				if(data[1].toString().length > 1)
				{
					 <!--- data must be an ISO formatted string in UTC time zone --->
					 var m = moment.utc(data[1].toString()).toDate();
					 $(row).attr('inpDateString', moment(m).local().format('ddd MMM DD, YYYY hh:mm A'));
				}
				else
				{
					$(row).attr('inpDateString', data[1]);
				}

				 $(row).attr('SessionId', data[2]);
				 $(row).attr('inpContactString', data[3]);
			},
			"aoColumns": [
						        {"sTitle": 'Sequence', "bSortable": true, "sWidth": "20%" },
						        {"sTitle": 'Date', "bSortable": true, "sWidth": "20%",

							        "mRender": function (data, type, full) {

										if(data.toString().length > 1)
										{
											 <!--- data must be an ISO formatted string in UTC time zone --->
											 var m = moment.utc(data.toString()).toDate()
											 return moment(m).local().format('ddd MMM DD, YYYY hh:mm A')
										}
										else
										{
											return '';
										}
			                     	}
			                    },
			                    {"mData": null, "bVisible": false },
						        {"sTitle": 'Device', "bSortable": true, "sWidth": "100%" }
						    ],
			"fnServerData": function ( sSource, aoData, fnCallback ) {

					<!--- These value pairs need to be on a seperate line each --->
					aoData.push({"name": "inpBatchId", "value": encodeURI("<cfoutput>#campaignId#</cfoutput>")});
					aoData.push({"name": "inpStart", "value": "<cfoutput>#inpStart#</cfoutput>"});
					aoData.push({"name": "inpEnd", "value": "<cfoutput>#inpEnd#</cfoutput>"});

					$.getJSON( sSource, aoData, function (json) {
						fnCallback(json);
                    });
              },
			  "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                    $('td', nRow).attr('nowrap','nowrap');
                    return nRow;
               },
			   "fnInitComplete":function(oSettings, json){

					inpObj.find('thead tr th').each(function(index){

						$this = $(this);
						if($this.hasClass("nosort"))
						{
							<!--- disable sorting on column "1" --->
							oSettings.aoColumns[$this.index()].bSortable = false;
							<!--- hide arrows --->
							$this.css("background","none");
						}

					  });

					  inpObj.find('tbody').on( 'click', 'tr', function () {

				        if ( $(this).hasClass('selected') ) {
				            $(this).removeClass('selected');
				        }
				        else {
				            inpObj.find('tr.selected').removeClass('selected');
				            $(this).addClass('selected');

				            LoadSessionDetails($(this).attr('SessionId'), $(this).attr('inpContactString'), $(this).attr('inpDateString'));

				        }
				    } );

				},
			   "fnDrawCallback": function( oSettings ) {

					<!-- auto select the top record on load --->
					inpObj.find('tr.selected').removeClass('selected');
				    inpObj.find('tbody tr').first().addClass('selected');

				    <!-- alternate way to get data for current row instead of storing at row level--->
				    <!--- var oData = oTable.rows('.selected').data();
				    --->

				    LoadSessionDetails(inpObj.find('tbody tr').first().attr('SessionId'), inpObj.find('tbody tr').first().attr('inpContactString'), inpObj.find('tbody tr').first().attr('inpDateString'));

					<!--- For tables that are hidden overflow - add full text to title --->
					inpObj.find('tbody tr td').each(function(index){

						$this = $(this);
						var titleVal = $this.text();
						if (titleVal != '') {
						  $this.attr('title', titleVal);
						}
					  });



				}

    	} );

	}


	<!--- Load table for all possible --->
	function LoadSessionDetails(inpSessionId, inpContactString, inpDateString)
	{
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/reports/response.cfc?method=GetSessionDetails&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			async: true,
			data:
			{
				inpBatchId : "<cfoutput>#campaignId#</cfoutput>",
				inpSessionId : inpSessionId,
				inpContactString : inpContactString,
				inpDateString : inpDateString
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},
			success:
			<!--- Default return function for Async call back --->
			function(d)
			{
				if (parseInt(d.RXRESULTCODE) == 1)
				{
					$('#question-details-container').html(d.DETAILS_HTML);
				}
				else
				{
					$('#question-details-container').empty();

					<!--- No result returned --->
					bootbox.alert("General Error processing your request.");
				}
			}
		});

	}

	function UpdateResponseBucket(inpSourceObj, inpResponse, inpQId, inpResponseRaw, inpTopParentTable)
	{
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/reports/response.cfc?method=UpdateResponseBucket&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			async: false,
			data:  {
					INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>",
					inpQId: inpQId,
					inpResponse: inpResponse,
					inpResponseRaw : inpResponseRaw
				   },
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},
			success:
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d)
				{
					if (d.RXRESULTCODE == 1)
					{
						/*
							ajx reload wont work because this table loads with page - not direct ajax
							Call the LoadSelectionTableData function instead
							// inpTopParentTable.ajax.reload();
						 */

						LoadSelectionTableData(inpQId);
					}
					else
					{

					}
				}

			});

	}


	</script>


<!--- Load After Session Data --->
<script type="application/javascript">

	$(function()
	{
		InitFilterResponseBlastTable();
		GetResponseBlastListByCampaignId();
	});

	// function InitFilterResponseBlastTable ()
	// {
	// 	$("#tblSubcriberListResponseBlastFilterBox").html('');
	//
	// 	//Filter bar initialize
	// 	$('#tblSubcriberListResponseBlastFilterBox').sireFilterBar({
	// 		fields: [
	// 			{DISPLAY_NAME: 'Phone Number', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'IS', SQL_FIELD_NAME: ' ContactString_vch '},
	// 			{DISPLAY_NAME: 'Created', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'DATE', SQL_FIELD_NAME: ' Created_dt '},
	// 		],
	// 		clearButton: true,
	// 		error: function(ele) { },
	// 		clearCallback: function()
	// 		{
	// 			GetResponseBlastListByCampaignId();
	// 		},
	// 		function(filterData){
	// 			GetResponseBlastListByCampaignId(filterData);
	// 		}
	// 	});
	// }

	$('button.btn-response-dowload.response-blast-list').on("click", function(event){
    	 	window.location.href="/session/sire/models/cfm/admin/admin-export-response-blast-list?inpBatchId="+$("#inpBatchId").val()+ "&sortDir="+sortDir +"&filter="+filter;
      });

	function InitFilterResponseBlastTable () {
		$("#tblSubcriberListResponseBlastFilterBox").html('');

		//Filter bar initialize
		$('#tblSubcriberListResponseBlastFilterBox').sireFilterBar({
			fields: [
				{DISPLAY_NAME: 'Phone Number', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'IS', SQL_FIELD_NAME: ' ContactString_vch '},
				{DISPLAY_NAME: 'Created', CF_SQL_TYPE: 'CF_SQL_VARCHAR', TYPE: 'DATE', SQL_FIELD_NAME: ' Created_dt '},
			],
			clearButton: true,
			error: function(ele){

			},
			clearCallback: function(){
				GetResponseBlastListByCampaignId();
			}
			}, function(filterData){
			GetResponseBlastListByCampaignId(filterData);
		});
	}

	// Get response blast list by campaign id
	 function GetResponseBlastListByCampaignId(strFilter){
	    $("#tblSubcriberListResponseBlast").html('');
	    var customFilterData = typeof(strFilter)!='undefined'?JSON.stringify(strFilter):"";

	    var formatContactString = function(object){
			 return '<span class="format-phone-number">'+object.PHONE+'</span>';
		 }


	    var table = $('#tblSubcriberListResponseBlast').dataTable({
		   "bStateSave": false,
		   "iStateDuration": -1,
		   "bProcessing": true,
		   "bFilter": false,
		   "bServerSide":true,
		   "bDestroy":true,
		   "sPaginationType": "simple_numbers",
		   "bLengthChange": false,
		   "iDisplayLength": 20,
		   "bAutoWidth": false,
		   "aoColumns": [
			  {"mData": "BATCHID", "sName": '', "sTitle": 'Batch Id', "bSortable": false, "sClass": "", "sWidth": "10%"},
			  {"mData": formatContactString, "sName": '', "sTitle": 'Phone Number', "bSortable": false, "sClass": "", "sWidth": "25%"},
			  {"mData": "RESPONSE", "sName": '', "sTitle": 'Response', "bSortable": false, "sClass": "", "sWidth": "50%"},
			  {"mData": "CREATED", "sName": '', "sTitle": 'Created', "bSortable": true, "sClass": "", "sWidth": "15%"}
		   ],
		   "sAjaxDataProp": "DATALIST",
		   "sAjaxSource": '/session/sire/models/cfc/campaign.cfc?method=GetResponseBlastListByCampaignId'+strAjaxQuery,
		   "fnDrawCallback": function( oSettings ) {
			  if (oSettings._iDisplayStart < 0){
				 oSettings._iDisplayStart = 0;
				 $('input.paginate_text').val("1");
			  }
		   },
		   "fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data

			  aoData.push(
				 { "name": "inpBatchId", "value": $("#inpBatchId").val()}
			  );
			  aoData.push(
				 { "name": "customFilter", "value": customFilterData}
			  );
			  // this is set sortDir for export response blast data
			  var hasSortDir = $("#tblSubcriberListResponseBlast th").hasClass("sorting_desc");
			  if(hasSortDir){
				 sortDir = "DESC";
			  }
			  else{
				 sortDir = "ASC";
			  }
			  filter = customFilterData;

			  oSettings.jqXHR = $.ajax( {
				 "dataType": 'json',
				 "type": "POST",
				 "url": sSource,
				 "data": aoData,
				 "success": function(data) {
					if(data.DATALIST.length == 0) {
						 $('#tblSubcriberListResponseBlast_paginate').hide();
					 }
					 fnCallback(data);
				 }
			  });
		   },
		   "fnInitComplete":function(oSettings){
				 $('.format-phone-number').mask('(000) 000-0000');
			 }
	    });
	}

</script>
