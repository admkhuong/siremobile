
<cfparam name="inpBatchId" default="">
<cfparam name="inpAPIUserName" default="SampleName">
<cfparam name="inpAPIPassword" default="SamplePassword">

<cfinvoke component="session.sire.models.cfc.security-credential" method="GetDefaultActiveAPICredentials" returnvariable="RetVarGetDefaultActiveAPICredentials">
</cfinvoke>

<cfif RetVarGetDefaultActiveAPICredentials.ACCESSKEY NEQ "">
	<cfset inpAPIUserName = RetVarGetDefaultActiveAPICredentials.ACCESSKEY />
</cfif>

<cfif RetVarGetDefaultActiveAPICredentials.SECRETKEY NEQ "">
	<cfset inpAPIPassword = RetVarGetDefaultActiveAPICredentials.SECRETKEY />
</cfif>

<cfoutput>

	<!--- BEGIN : CHOOSE KEYWORD --->
	<div class="portlet cpedit">
	    <div class="portlet-body">
	        <div class="row">
	            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                <h4 class="portlet-heading-new">API  <a tabindex="0" class="info-popover" data-placement="right" data-container="body" data-trigger="focus" role="button" data-html="true"   title="" data-content="To trigger a Transactional SMS, you need to send information via an HTTPS request to the URL address <b>https://api.siremobile.com/ire/secure/triggerSMS</b>. At a minimum of you need four pieces of information to send along with this request. To see how the information can be used to generate a REST API request click the BUILD URL button below.</br></br><a href='https://www.siremobile.com/cs-transactional-api' target='_blank'> Learn More. </a>" ><img  src='../assets/layouts/layout4/img/info.png'/></a></h4>

	                <div class="form-gd">
	                    <div class="form-group">

	                        <div class="form-group">
	                            <div class="row">

	                                <div class="col-sm-12 col-lg-8">
		                                <label for="inpContactString">Target SMS Phone Number</label>
	                                    <input type="text" class="form-control validate[required,custom[usPhoneNumberII]]" id="inpContactString" name="inpContactString" maxlength="15" value="9495551212" data-prompt-position="topLeft:100">
	                                </div>

	                            </div>
	                        </div>

	                        <input type="hidden" id="APIBatchId" value="#inpBatchId#" />
	                        <input type="hidden" id="APIUserName" value="#inpAPIUserName#" />
	                        <input type="hidden" id="APIPassword" value="#inpAPIPassword#" />

	                        <div class="row row-small">
								<div class="col-xs-12">
									<a href="javascript:;" id="btn_build_URL" class="btn grey-mint add-more-answer">Build Your Own Trigger URL</a>
								</div>
							</div>

						</div>

	                </div>

					<div class="form-gd">
	                    <div class="form-group">

                            <h4>The URL you need to call the REST API <a tabindex="0" class="info-popover" data-placement="right" data-trigger="focus" role="button" data-html="true"   title="" data-content="This REST URL will work with either GET (like from your Browser) or POST from any application that supports web REST calls. This REST URL is using basic user name / password over SSL security. Optionally, if you want to learn to use full Pub/Private key with your REST API calls click here SIRE REST API Pub/Private Key Option" ><img  src='../assets/layouts/layout4/img/info.png'/></a></h4>

                            <b><textarea id="HTTPSAPIURL" name="HTTPSAPIURL" rows="5" style="width:100%;" class="form-control"></textarea></b>


						</div>
					</div>

	            </div>

	        </div>
			<!--- <a href="javascript:;" class="btn green-gd campaign-next check-kw btn-adj-pos #hiddenClass#">Next <i class="fa fa-caret-down"></i></a> --->
	    </div>
	</div>
	<!--- END : CHOOSE KEYWORD --->

</cfoutput>
