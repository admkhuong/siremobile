<!--- This section is for specifying "Blast" values --->

<cfparam name="RetVarGetAssignedShortCode" default="{}" type="struct" />

<cfset subcriberArray = []>

<cfinvoke component="session.sire.models.cfc.subscribers" method="GetSubcriberList" returnvariable="groupList">
	<cfinvokeargument name="inpShortCode" value="#RetVarGetAssignedShortCode.SHORTCODE#">
	<cfinvokeargument name="inpShortCodeId" value="#RetVarGetAssignedShortCode.SHORTCODEID#">
</cfinvoke>
<cfif groupList.RXRESULTCODE EQ 1 AND groupList.iTotalRecords GT 0>
	<cfset subcriberList = groupList.listData>
	<cfloop array ="#subcriberList#" index="item">
	    <cfset subcriberArray[item[1]] = item[2]>
	</cfloop>
<cfelse>
	<cfset subcriberList = []>
</cfif>

<cfoutput>

	<!--- BEGIN : CHOOSE BLAST --->
	<div class="portlet cpedit">
	    <div class="portlet-body">
	        <div class="row">
	            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                <h4 class="portlet-heading-new">Blast  <a tabindex="0" class="info-popover" data-placement="right" data-trigger="focus" data-container="body" role="button" data-html="true"   title="" data-content="A SMS blast allows you to send a message to, or start a conversation with, your entire subscriber list without using a keyword.</br></br><img width='30px' src='../assets/layouts/layout4/img/Icon_Tip.png'/> If you want to break through the noise and stand out, you can’t just send a generic blast.  One-on-one hyper-personalized interactions are key to getting your customers to convert." ><img  src='../assets/layouts/layout4/img/info.png'/></a></h4>

	                <div class="col-sm-12" style="" id="PickAList">
											<label for="disabledSelect" class="">Select the Subscriber List <span>* <i>required</i></span></label>
											<div class="warning-block">

												<select id="SubscriberList" data-type="old-blast" class="Select2" style="width:100%;">
													<option value="0" selected >Select Subscriber List</option>
													<cfloop array ="#subcriberList#" index="item">
														<option value="#item[1]#" data-total-subcribers="#item[3]#">#item[2]# (#item[3]#)</option>
									            	</cfloop>
												</select>

										    </div>
									    <br>
									    <!--- <div class="box-subcriber-filter hidden" class="warning-info">
										    <label id="lbl-show-filter-bar" for="subcriber-filter-bar" class="btn btn-success-custom showed">Show filter</label>

										    <div id="subcriber-filter-bar" class="hidden">
										    	<cfoutput>
										            <!---set up column --->
										            <cfset datatable_ColumnModel = arrayNew(1)>

										            <cfloop array="#retValCustomDataField.CDFARRAY#" index="item">
											            <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = item.VARIABLENAME, CF_SQL_TYPE = 'CF_SQL_VARCHAR', TYPE='TEXT', SQL_FIELD_NAME = item.CDFID})>
										            </cfloop>

										            <!---we must define javascript function name to be called from filter later--->
										            <cfset datatable_jsCallback = "InitControl">
										            <br>
										            <input class="form-control" placeholder="Contact String" name="inpContactStringFilter" id="inpContactStringFilter">
										            <cfinclude template="/session/sire/pages/campaign-edit-blast-filter.cfm" >
										            <div class="table-responsive">
											            <table id="tblListEMS" class="table table-striped table-bordered table-hover dataTable">
											            	<thead>
											            		<tr>
											            			<th>Contact ID</th><th>Contact</th>
											            		</tr>
											            	</thead>
											            </table>
										            </div>
									            </cfoutput>
										    </div>
									    </div> --->
										</div>

										<div class="col-sm-12">
											<hr class="hrb10">
										    <label for="opt-out-txt">Schedule</label>
										    <div class="warning-block">
										        <div id="warning-content"><span class="warning-text">Note:</span> Messages are by default scheduled between 9am - 7pm (PST).</div>
										        <a data-toggle="modal" data-target="##ScheduleOptionsModal" href="##">
										            Click here to enable alternate delivery times.
										        </a>
										    </div>

										</div>

										<div class="col-sm-12">
											<hr class="hrb10">
										    <label for="opt-out-txt">Advanced Option - Specific Time Zone - Ignore Device Location</label>
										    <div class="warning-block">
										        <div id="warning-content"><span class="warning-text">Note:</span> Messages are by default scheduled relative to local device time zone.</div>
											   <select id="inpTimeZone" name="inpTimeZone" class="form-control Select2">
												   <option value="0" <cfif 999 EQ '0'>selected</cfif>>Local to Device</option>
												   <option value="34" <cfif 999 EQ '34'>selected</cfif>>Hawaii</option>
												   <option value="32" <cfif 999 EQ '32'>selected</cfif>>Alaska</option>
												   <option value="31" <cfif 999 EQ '31'>selected</cfif>>Pacific</option>
												   <option value="30" <cfif 999 EQ '30'>selected</cfif>>Mountain</option>
												   <option value="29" <cfif 999 EQ '29'>selected</cfif>>Central</option>
												   <option value="28" <cfif 999 EQ '28'>selected</cfif>>Eastern</option>
											   </select>
										    </div>
										</div>

						    			<div class="col-sm-12">
							    			<div class="modal-footer">
												<button id="btn-save-send" type="button" class="btn btn-success btn-success-custom btn-save-send" data-dismiss="modal"> Send </button>
											</div>
						    			</div>

	            </div>

	        </div>

	    </div>
	</div>
	<!--- END : CHOOSE BLAST --->

</cfoutput>
