

<cfoutput>

	<!--- BEGIN : CHOOSE KEYWORD --->
	<div class="portlet cpedit">
	    <div class="portlet-body">
	        <div class="row">
	            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
	                <h4 class="portlet-heading-new">#UIL_ChooseAKeyword#  <a tabindex="0" class="info-popover" data-placement="right" data-trigger="focus" role="button" data-html="true"   title="" data-container="body" data-content="#UIL_KeywordInfo#</br></br><img width='30px' src='../assets/layouts/layout4/img/Icon_Tip.png'/> #UIL_KeywordInfoTip#" ><img  src='../assets/layouts/layout4/img/info.png'/></a></h4>

	                <div class="form-gd">
	                    <div class="form-group">
	                        <input value="#campaignData.Keyword_txt#" class="form-control" id="Keyword" type="text" name="Keyword" maxlength="160" style="width:100%;" />
	                    </div>
	                    <div class="form-group">
	                        <span class="has-error KeywordStatus" style="display: none;" id="KeywordStatus"></span>
	                    </div>
	                </div>
	            </div>
	            <div class="col-lg-7 col-md-7 hidden-sm hidden-xs preview-keyword">

	                <div class="movi-short">
						<div class="movi-short-code">
							<button class=" movi__btn">#UIL_ShortCode#</button>
							<div class="line"></div>
							<span class="dot"></span>
						</div>
						<div class="movi-keyword">
							<button class=" movi__btn">#UIL_Keyword#</button>
							<div class="line"></div>
							<span class="dot"></span>
						</div>

	                    <div class="movi-heading">
	                        <h3 class="movi-heading__number">#session.Shortcode#</h3>
	                    </div>
	                    <div class="movi-body">
	                        <div class="media chat__item chat--sent">
	                            <div class="media-body media-bottom">
	                                <div class="chat__text">
	                                    <span class="span_keyword">Localdinner</span>
	                                </div>
	                            </div>
	                            <div class="media-right media-bottom">
	                                <div class="chat__user">
	                                    <img src="/session/sire/assets/layouts/layout4/img/avatar-women.png" />
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div><!--End Mobile View -->
	            </div>
	        </div>
			<!--- <a href="javascript:;" class="btn green-gd campaign-next check-kw btn-adj-pos #hiddenClass#">Next <i class="fa fa-caret-down"></i></a> --->
	    </div>
	</div>
	<!--- END : CHOOSE KEYWORD --->

</cfoutput>
