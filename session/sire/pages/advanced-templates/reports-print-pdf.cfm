<cfparam name="inpHTML" default="This is a test" />
<cfparam name="inpFileName" default="ReportOutput.pdf" />

<!---  https://www.prepressure.com/library/paper-size --->
<!--- 
http://dnando.github.io/blog/2015/07/01/generating-accurate-pdfs-using-cfdocument/

A4
210mm x 297mm
	
US Letter in mm
Letter (ANSI A)	
215.9mm x 279.4mm
		
--->
<cfheader name="content-disposition" value="attachment; filename=#inpFileName#"/>

<cfdocument format="pdf" pagetype="A4" orientation="portrait" unit="cm" margintop="1" marginleft="1" marginright="1" marginbottom="1" saveasname="#inpFileName#" name="output_doc">
<html>
	<head>
		
		<cfheader name="expires" value="#now()#">
		<cfheader name="pragma" value="no-cache">
		<cfheader name="cache-control" value="no-cache, no-store, must-revalidate">
		
	    <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <cfinclude template="/session/sire/views/layouts/components/assets-global.cfm">        
        <!-- END GLOBAL MANDATORY STYLES -->
                
		<style type="text/css">
			html, body { width:190mm; padding:0; margin: 0; background-color: #FFF; font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; }
			.block1{ width: 80mm; height: 80mm; margin: 0; padding: 0; background-color: #ddd;  }
			.block2{ width: 80mm; height: 80mm; margin-left: 10mm; margin-top: 10mm; background-color: #ddd;  }
			.block3{
				position: absolute;
				top: 10mm;
				left: 10mm;
				width: 80mm;
				height: 40mm;
				z-index: 2;
				background-color: #aaa
			}
			.NoPrint {display: none;}
		</style>
	</head>
	<body>
		
		<div style="position:relative; top:0mm; left:0mm; width:210mm;">
			
			<cfoutput>#inpHTML#</cfoutput>
						
		</div>	
		
	</body>
</html>
</cfdocument>

<cfcontent type="application/pdf" variable="#output_doc#" />


<!--- better as CFC?
	
	
	<!---code starts--->
<cfcomponent restpath="pdf" >
 <cffunction name="converttopdf" access="remote" returntype="binary" output="false" httpmethod="post" produces="application/xml" >
  <cfargument name="data" type="string" >
  <cfset pdffile = "" >
  <cfdocument format="PDF" overwrite="yes" name="pdffile" >
   <cfoutput>#data#</cfoutput>
  </cfdocument>
 
  <cfreturn #pdffile#>
 </cffunction>
</cfcomponent>
<!---code ends--->

	
	
	 --->