<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission"></cfinvoke>

<cfoutput>
<cfparam name="currTab" default="">
<div class="portlet light bordered">
	<div class="portlet-body" >
		<h4 class="portlet-heading">Affiliate</h4>
		<div class="row  content-simon-new">				
			<div class="content-body ">				
				<div class="col-lg-8 col-md-8">			
					<a class="active" target="_blank" href="/session/sire/pages/affiliates-dashboard?type=admin">Dashboard</a>			
				</div>	                
				<div class="col-lg-8 col-md-8">
					<a class="active" target="_blank" href="/session/sire/pages/affiliates-dashboard?type=report">Affiliate Report</a>
				</div>                
				<div class="col-lg-8 col-md-8">				
					<a class="active" target="_blank" href="/session/sire/pages/affiliates-commission-report">Affiliate Commission Report</a>			
				</div>
                				
			</div>
		</div>
	</div>
</div>
</cfoutput>		
		

<cfparam name="variables._title" default="Admin Affiliate">
<cfinclude template="../views/layouts/master.cfm">

