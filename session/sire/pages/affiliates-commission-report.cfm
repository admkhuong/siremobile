<cfscript>    

    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("../assets/global/plugins/daterangepicker/daterangepicker.css", true)        
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)
        
        .addJs("../assets/global/plugins/daterangepicker/moment.min.js", true)
        .addJs("../assets/global/plugins/daterangepicker/daterangepicker.js", true)                        
                
        .addJs("/session/sire/assets/global/scripts/filter-bar-generator.js")        
        .addJs("../assets/pages/scripts/affiliate-commission-report.js")
        .addJs("/public/sire/js/select2.min.js");
</cfscript>
<cfoutput>
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />
<div class="portlet light bordered ">
	<div class="portlet-body">
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <h2 class="page-title">Affiliate Commission Report</h2>
            </div>
            <div class="col-sm-12 col-md-8">
                <div id="box-filter">
                    
                </div>
            </div>  
        </div>  
        <div class="row">
            <div class="col-sm-12">
                <div class="re-table">
                    <div class="table-responsive">
                        <table id="listReport" class="table table-striped table-bordered table-hover">
                            
                        </table>
                    </div>
                </div>				
            </div>
        </div>
        <div class="div-commission-detail row hidden">
            
            <div class="col-xs-12 col-md-12">
                <h4 class="portlet-subheading"><b id="commission-detail-tit">Affiliate Commission Detail for</b></h4>
                <div class="re-table">
                    <div class="table-responsive">
                        <table id="listDetail" class="table table-striped table-bordered table-hover">
                            
                        </table>
                    </div>
                </div>				
            </div>                
        </div>
	</div>
</div>
</cfoutput>		
<div class="modal fade in" id="mdPaymentDetail" tabindex="-1" role="dialog" aria-hidden="true">	
    <div class="modal-dialog">
        <div class="modal-content">
       
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <b><h4>Payment Detail</h4></b>                
            </div>
            <div class="modal-body"> 
                <div class="row">
                    <div class="col-sm-4 col-xs-12">
                        Email receive:
                    </div>                
                    <div class="col-sm-8 col-xs-12">
                        <span id="email-receive"></span>
                    </div>                
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-4 col-xs-12">
                        Amount:
                    </div>                
                    <div class="col-sm-8 col-xs-12">
                        <span id="amount"></span>
                    </div>                
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-4 col-xs-12">
                        Status:
                    </div>                
                    <div class="col-sm-8 col-xs-12">
                        <span id="status"></span>
                    </div>                
                </div>
            </div>
        
        </div>
    </div>
</div>
		

<cfparam name="variables._title" default="Affiliate Commission Report">
<cfinclude template="../views/layouts/master.cfm">

