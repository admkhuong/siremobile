<cfparam  name="actionType" default="4"> <!--- 1: sign-up , 2: sign-up-request-affiliate, 3: sign-up-apply-affiliate, 4: request-affiliate --->
<cfparam  name="affiliatetype" default="1">
<cfparam  name="ami" default="">

<cfset variables.menuToUse = 2 />
<cfparam name="url.type" default=""/>
<cfif type EQ "admin" OR type EQ "report">
    <cfset variables.menuToUse = 1 />
</cfif>

<cfscript>
    

    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("../assets/global/plugins/daterangepicker/daterangepicker.css", true)
        .addCss("/session/sire/assets/global/plugins/Chartist/chartist.min.css", true)
        .addCss("/session/sire/assets/global/plugins/Chartist/plugins/tooltips/chartist-plugin-tooltip.css", true)
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)
        .addCss("/session/sire/assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/daterangepicker/moment.min.js", true)
        .addJs("../assets/global/plugins/daterangepicker/daterangepicker.js", true)                
        .addJs("/session/sire/assets/global/plugins/Chartist/chartist.min.js", true)
        .addJs("/session/sire/assets/global/plugins/Chartist/plugins/tooltips/chartist-plugin-tooltip.js", true)
        .addJs("/session/sire/assets/global/plugins/uikit/js/uikit.min.js", true)
        .addJs("/session/sire/assets/global/plugins/chartjs/Chart.bundle.js", true)
        .addJs("/session/sire/assets/global/scripts/filter-bar-generator.js")
        .addCss("/public/sire/css/affiliate.css")	
        .addJs("../assets/pages/scripts/affiliate-dashboard.js")
        .addJs("/public/sire/assets/plugins/mobile-detect/mobile-detect.js")
        .addJs("/public/sire/js/select2.min.js");
</cfscript>
<cfset randomCode=Chr(RandRange(65, 90)) & Chr(RandRange(65, 90)) & Chr(RandRange(65, 90))>  
<cfif type EQ "report" OR  type EQ "admin">
    <cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />
</cfif>
<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="userOrgInfo"></cfinvoke>
<cfinvoke method="GetAffiliateCode" component="public.sire.models.cfc.affiliate" returnvariable="GetAffiliateCode"></cfinvoke>
<cfinvoke method="GetCommissionSetting" component="public.sire.models.cfc.affiliate" returnvariable="commissionSetting">
    <cfinvokeargument  name="inpUserId"  value="#Session.UserID#">
    <cfinvokeargument  name="inpOrder"  value="Condition_int ASC,Value_dec ASC">
    
</cfinvoke>


<cfinvoke method="GetTotalActiveReferrals" component="public.sire.models.cfc.affiliate" returnvariable="GetTotalActiveReferrals"></cfinvoke>

<cfset ReferredPercent=0>	

<cfset TotalUserPaid=GetTotalActiveReferrals.TOTAL_USER>

<cfif CommissionSetting.RXRESULTCODE EQ 1 AND TotalUserPaid GT 0>
    <cfloop index = "index" array = "#CommissionSetting.SETTING#" >
        <cfif index.VALUE LTE TotalUserPaid AND index.CONDITION EQ 1>
            <cfset ReferredPercent=index.PERCENT>					
        <cfelseif index.VALUE GT TotalUserPaid AND index.CONDITION EQ 0>
            <cfset ReferredPercent=index.PERCENT>					
                <cfbreak>
        </cfif>					
    </cfloop>
</cfif>


<cfset AffiliateInfomation = {
    AffiliateCode = "",
	AffiliateUserEmail= "" ,
	AffiliateUserID= 0	
}>

<cfset UserInfomation= {
    FirstName = "",
    LastName = "",
    SMSNumber  = "",   
    EmailAddress = "",
    Address = "",
    City = "",
    State = "",
    Zip = "",
    SSN_TaxID = "", 
    AffiliateCode= randomCode,
    PaypalEmailAddress=""
}>
<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke>
<cfset actionType= 4>
<cfset UserInfomation= {
    FirstName = userInfo.FIRSTNAME,
    LastName = userInfo.LASTNAME,
    SMSNumber  = userInfo.MFAPHONE,   
    EmailAddress = userInfo.FULLEMAIL,
    
    Address = userInfo.ADDRESS,
    City = userInfo.CITY,
    State = userInfo.STATE,
    Zip = userInfo.POSTALCODE,
    SSN_TaxID = userInfo.SSNTAXID, 
    AffiliateCode= randomCode,
    PaypalEmailAddress=userInfo.PaypalEmail
}>
<cfif GetAffiliateCode.RXRESULTCODE EQ 1 AND GetAffiliateCode.AFFILIATECODE NEQ "">  
    <cfset UserInfomation.AffiliateCode= GetAffiliateCode.AFFILIATECODE>
</cfif>

<cfoutput>
<input type="hidden" id="dashboardType" value="#type#"/>
<div id="affiliate-page">        
    <cfif type EQ "report">
        <div class="row dashboard affiliate-dashboard">           
            <div class="col-xs-12 col-sm-12 col-lg-12">
                <div class="portlet light bordered">
                    <div class="portlet-body form">		
                            <div class="row">
                            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">                        
                                <h2 class="dashboard-block-title">Select User</h2>   
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                <div id="box-filter">
                                    
                                </div>
                            </div>	
                            </div>						
                            <div class="row">
                            <div class="col-xs-12">
                                <div class="re-table">
                                    <div class="table-responsive">
                                        <cfoutput>
                                        <input type="text" class="hidden" name="list-user-selected" id="list-user-selected" size="100" value="" />
                                        </cfoutput>
                                        <table id="adm-user-list" class="table table-striped table-responsive">
                            
                                        </table>
                                    </div>
                                </div>
                            </div>				            
                            </div>
                    </div>        
                </div>
            </div>
        </div>
    </cfif>
    <div class="all-dashboard #type EQ "report"? 'hidden':''#">
        <div class="row dashboard affiliate-dashboard">
            <div class="col-xs-12 col-sm-6 col-lg-6">
                <div class="portlet light bordered admin-dashboard-mid-card">
                    <div class="portlet-body form">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">                                
                                <div class="admin-dashboard-block-title">
                                    <cfif type EQ "admin">
                                        Affiliate Total:
                                    <cfelse>
                                        Total Users:
                                    </cfif>
                                </div>
                                <div class="col-md-8 col-lg-8 col-sm-8" style="padding-left: 0px">
                                    <div class="admin-dashboard-value">
                                        <cfif type EQ "admin">
                                            <div><span id="total-affiliate-all" class="span-total"></span></div>                                
                                        <cfelse>
                                            <div><span id="total-acct-all" class="span-total"></span></div>                                
                                        </cfif>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="admin-dashboard-block-title">                                    
                                    <cfif type EQ "admin">
                                        New Affiliate Total:
                                    <cfelse>
                                        Total Of New Users:
                                    </cfif>
                                </div>
                                <div class="col-md-8 col-lg-8 col-sm-8" style="padding-left: 0px">
                                    <div class="admin-dashboard-value">
                                        <cfif type EQ "admin">
                                            <div><span id="new-affiliate" class="span-total">0</span></div>
                                        <cfelse>
                                            <div><span id="new-user" class="span-total">0</span></div>
                                        </cfif>
                                    </div>
                                </div>
                            </div>                    
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-6">
                <div class="portlet light bordered admin-dashboard-mid-card">
                    <div class="portlet-body form">
                        <div class="admin-dashboard-block-title">Selected Date Range</div>
                        <div class="col-xs-12 col-sm-12 col-lg-7 col-md-10" style="padding-left: 0px;margin-top: 20px">
                            <div class="date-range">                            
                                <div class="reportrange">
                                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                    <span></span> <b class="caret"></b>
                                    <input type="hidden" name="dateStart" value="">
                                    <input type="hidden" name="dateEnd" value="">                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row dashboard affiliate-dashboard">
            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                <div class="portlet light bordered admin-dashboard-mid-card">
                    <div class="portlet-body form">
                        <div class="admin-dashboard-block-title blue-text">Total Revenue</div>
                        <div class="admin-dashboard-value">                    
                            <div><span class="span-total" id="total-revenue">$ 0.00</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                <div class="portlet light bordered admin-dashboard-mid-card">
                    <div class="portlet-body form">
                        <div class="admin-dashboard-block-title blue-text">Total Commissions</div>
                        <div class="admin-dashboard-value">
                            <div><span class="span-total" id="earning-value">$ 0.00</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                <div class="portlet light bordered admin-dashboard-mid-card">
                    <div class="portlet-body form">
                        <div class="admin-dashboard-block-title blue-text">Total Free Accounts</div>
                        <div class="admin-dashboard-value">
                            <div><span id="total-acct-free" class="span-total">0</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                <div class="portlet light bordered admin-dashboard-mid-card">
                    <div class="portlet-body form">
                        <div class="admin-dashboard-block-title blue-text">Total Paid Accounts</div>
                        <div class="admin-dashboard-value">
                            <div><span id="total-acct-paid" class="span-total">0</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <cfif type EQ "">
        <div class="row dashboard affiliate-dashboard">   
            <div class="col-xs-12 col-sm-6 col-lg-6">
                <div class="portlet light bordered">
                    <div class="portlet-body form div-affiliate-link">    
                        <h2 class="dashboard-block-title">Affiliate Link</h2>                                     
                        <div class="row" style="padding-top:7px">                
                            <cfif GetAffiliateCode.RXRESULTCODE EQ 1 AND GetAffiliateCode.AFFILIATECODE NEQ "">                                                                                
                                <div class="col-xs-12" >
                                    CODE:          
                                </div>
                                <div class="col-xs-8 col-sm-8" >                                                                                                                                    
                                    <input class="form-control affiliate-code" id="affiliate-code" value="#GetAffiliateCode.AFFILIATECODE#"/>                                     
                                    <input type="hidden"  id="affiliateId" value="#GetAffiliateCode.AFFILIATEID#">
                                </div> 
                                <div class="col-xs-4 col-sm-4" >   
                                    <a href="##" class="btn green-gd affiliate-code-update">Update Code</a>
                                </div> 
                                <div class="col-xs-12" >
                                    LINK:          
                                </div>
                                <div class="col-xs-12 col-sm-12" >                                                                                                                                    
                                    <input class="affiliate-link" id="affiliate-link" value="https://#cgi.http_host#/signup-affiliates?ami=#GetAffiliateCode.AFFILIATECODE#" readonly/>                        
                                </div>                
                                <div class="col-xs-12 col-sm-12" >                        
                                    <a href="##" class="btn green-gd affiliate-link-copy">Copy Link</a>
                                    <p class="portlet-subheading text-color-gray">
                                        This is your unique Affiliate link.  All Sire users who sign up with this link will be credited to your account.  
                                        We recommend you share it on social media, add it to your email signature and include it on marketing material to earn more revenue.  
                                        Note that any traffic attributed to your link will be cookied for 180 days to ensure you receive credit for the referral.  
                                        Click <a href='https://sire-mobile.appointlet.com/b/sire-support' target='_blank'>here</a> to contact us with any questions you may have.
                                    </p>   
                                </div> 
                                
                            <cfelse>
                                <div class="col-xs-12 col-sm-12" >  
                                    <p class="portlet-subheading text-color-gray">
                                        You have no Affiliate Code, Click <a href="##" class="" style="text-decoration:underline" data-toggle="modal" data-target="##mdSignup">here</a> to request a Affiliate Code.</br></br>                                                                        
                                    </p>                            
                                </div>         
                            </cfif>
                        </div>            
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-6">             
                <div class="portlet light bordered">
                    <div class="portlet-body form div-affiliate-commission">   
                        <h2 class="dashboard-block-title">Commission Rate: #ReferredPercent# %</h2>
                                    
                        <div class="row" style="padding-top:7px">     
                            <div class="col-xs-12 col-sm-12" >  
                                <cfif GetAffiliateCode.RXRESULTCODE EQ 1 AND GetAffiliateCode.AFFILIATECODE NEQ "">                                                                                                                    
                                <table class="affiliate-rate">
                                    <tr>
                                        <th>Total</th>
                                        <th>Commission Rate</th>
                                    </tr>
                                    <cfif commissionSetting.RXRESULTCODE EQ 1>
                                        <cfset startPaid=1>
                                        <cfloop index = "index" array = "#commissionSetting.SETTING#" >
                                            
                                            <tr>
                                                <td>
                                                    <cfif index.Condition EQ 1 >
                                                        #index.value +1#  + Users
                                                    <cfelseif startPaid EQ 1>
                                                        < &nbsp; #index.value # Users
                                                    <cfelse>
                                                        #startPaid# -#index.value # Users
                                                    </cfif>
                                                    
                                                </td>
                                                <td>#index.percent# %</td>
                                            </tr>
                                            <cfset startPaid=index.value+1>
                                        </cfloop>
                                    </cfif>
                                </table>
                                </cfif>
                            </div>  
                        </div>                                  
                    </div>
                </div>                                       
            </div>   
                
        </div>
        </cfif>
        <div class="row dashboard affiliate-dashboard">    
            <div class="col-xs-12 col-sm-12 col-lg-12">
                <div class="portlet light bordered">
                    <div class="portlet-body form">    
                        <div class="row">
                            <div class="col-xs-12 col-sm-7 col-lg-7">
                                <h2 class="dashboard-block-title">Period Overview</h2>                    
                            </div>
                            <div class="col-xs-12 col-sm-5 col-lg-5">
                                
                            </div>
                        </div>
                        
                        <hr class="dashboard-hr">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-lg-6 set-chart">   
                                <div class="portlet light bordered">                                
                                    <h2 class="dashboard-block-title text-center"><span id="active-acct-ref" class="span-total">0</span>Active Accounts Referred</h2>
                                    <div class="chart-active-referred" id="highchart" >
                                        
                                    </div>                                  
                                </div>                                
                            </div>
                            <div class="col-xs-12 col-sm-12 col-lg-6 set-chart">   
                                <div class="portlet light bordered">                                
                                    <h2 class="dashboard-block-title text-center"><span id="new-acct-ref" class="span-total">0</span>New Accounts Referred</h2>
                                    <div class="chart-new-referred" id="highchart" >
                                        
                                    </div>                                  
                                </div>                                
                            </div>
                            <div class="col-xs-12 col-sm-12 col-lg-6 set-chart">    
                                <div class="portlet light bordered">  
                                    <h2 class="dashboard-block-title text-center"><span id="upgrade-acct-ref" class="span-total">0</span>Upgrade Accounts Referred</h2>
                                    <div class="chart-upgrade-referred" id="highchart" >
                                        
                                    </div> 
                                </div>                                     
                            </div>
                            <div class="col-xs-12 col-sm-12 col-lg-6 set-chart">
                                <div class="portlet light bordered"> 
                                    <h2 class="dashboard-block-title text-center"><span id="downgrade-acct-ref" class="span-total">0</span>Downgrade Accounts Referred</h2>
                                    <div class="chart-downgrade-referred" id="highchart" >
                                        
                                    </div>  
                                </div>
                            </div>
                        </div>   
                    
                    </div>
                </div>  
            </div>   
        </div>
    </div>
</div>

<div class="modal fade" id="mdSignup" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">              
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>  
                <h4 class="modal-title gray-text"><strong >Affiliate Sign-up</strong></h4>
            </div>
            <div class="modal-body">
                <div class="portlet-body form">
                    <div class="row">
                        <div class="col-xs-12">                             
                            <form name="signup_form" id="signup_form" autocomplete="off">
                                                    
                                <input type="hidden" name="" id="actionType" value="#actionType#">
                                <input type="hidden" name="" id="select-plan-2" value="1">
                                <input type="hidden" name="" id="affiliatetype" value="#affiliatetype#">
                                <input type="hidden" name="" id="AMI" value="#AffiliateInfomation.AffiliateUserID#">
                                            
                                <div class="uk-grid uk-grid-small" uk-grid>
                                    <div class="uk-width-1-2 ">
                                        <div class="form-group">
                                            <label for="">First name</label>
                                            <input type="text" class="form-control validate[required,custom[noHTML]]" id="fname" placeholder="" maxlength="50" value="#UserInfomation.FirstName#" #actionType EQ 4 ? 'readonly': ''#>
                                        </div>
                                    </div>

                                    <div class="uk-width-1-2">
                                        <div class="form-group">
                                            <label for="">Last name</label>
                                            <input type="text" class="form-control validate[required,custom[noHTML]]" id="lname" placeholder="" maxlength="50" value="#UserInfomation.LastName #" #actionType EQ 4 ? 'readonly': ''#>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="uk-width-1-1 ">
                                        <div class="form-group">                                        
                                            <label for="">Address</label>
                                            <input type="text" class="form-control validate[required]" id="address" placeholder="" value="#UserInfomation.Address #">
                                        </div>
                                    </div>
                                    <div class="uk-width-1-2">
                                        <div class="form-group">
                                            <label for="">City</label>
                                            <input type="text" class="form-control validate[required]" id="city" placeholder="" value="#UserInfomation.City #">
                                        </div>
                                    </div>
                                    <div class="uk-width-1-2">
                                        <div class="form-group">
                                            <label for="">State</label>
                                            <input type="text" class="form-control validate[required]" id="state" placeholder="" value="#UserInfomation.State #">
                                        </div>
                                    </div>
                                    <div class="uk-width-1-2">
                                        <div class="form-group">
                                            <label for="">Zip</label>
                                            <input type="text" class="form-control validate[required]" id="zip" placeholder="" value="#UserInfomation.Zip #">
                                        </div>
                                    </div>
                                    <div class="uk-width-1-2">
                                        <div class="form-group">
                                            <label for="">SSN/Tax ID</label>
                                            <input type="text" class="form-control validate[required]" id="taxid" placeholder="" value="#UserInfomation.SSN_TaxID #">
                                        </div>
                                    </div>
                                    <div class="uk-width-1-1">
                                        <div class="form-group">
                                            <label for="">Paypal E-mail Address</label>&nbsp;<span>(Commission payments will be sent to this account)</span>
                                            <input type="text" class="form-control validate[required,custom[email]]" id="paypalemailadd" placeholder="" value="#UserInfomation.PaypalEmailAddress #">
                                        </div>
                                    </div>
                                    <div class="uk-width-1-2">
                                        <div class="form-group">
                                            <label for="">Affiliate Code</label>
                                            <input type="text" class="form-control validate[required]" id="affiliatecode" value="#UserInfomation.affiliatecode#" #ami NEQ '' ? 'readonly': ''# placeholder="">
                                        </div>
                                    </div>                
                                    <div class="uk-width-1-1 last-box-newsignup">
                                        <div class="">
                                            <label for="agree" class="uk-flex uk-flex-middle conditioner uk-position-relative">
                                                <input name="agree" class="validate[required]" id="agree" type="checkbox"> 
                                                <span>I’ve read and agree to the <a href="/affiliate-term-of-use" target="_blank">Terms and Conditions of Use.</a></span>
                                            </label>
                                        </div>                                    
                                    </div>

                                    <div class="uk-width-1-1">
                                        <button class="btn newbtn green-gd" id="btn-new-signup">get started!</button>
                                    </div>
                                </div>
                            </form>   
                        </div>   
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>
</cfoutput>

<!--- Modal For Enterprise Users report --->
<div class="modal fade" id="mdListAccReferred" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="referred-modal-title">List Account Referred</strong></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="re-table">
                            <div class="table-responsive">                                
                                <table id="list-acc-referred" class="table table-striped table-bordered table-hover">

                                </table>
                            </div>
                        </div>
                    </div>				            
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>






<cfif trim(type) EQ "admin">
    <cfparam name="variables._title" default="Admin Affiliate Dashboard">
<cfelseif trim(type) EQ "report">
    <cfparam name="variables._title" default="Admin Affiliate Report">
<cfelse>
    <cfparam name="variables._title" default="Affiliate Dashboard">
</cfif>
<cfinclude template="../views/layouts/master.cfm">
<cfinclude template="inc_dashboard_slide_integrate.cfm">
<cfif GetAffiliateCode.RXRESULTCODE EQ 1 AND GetAffiliateCode.AFFILIATECODE EQ "" AND type NEQ "report" AND type NEQ "admin"> 
    <script>
        
        //$('#mdSignup').modal({backdrop: 'static', keyboard: false}) 
        $("#mdSignup").modal('show');        
        var md = new MobileDetect(window.navigator.userAgent);
    
        if(md.phone() !== null || md.tablet() !== null){
            if(!$("body").hasClass('startFreeOpen1')){
                $("body").addClass('startFreeOpen1')
            }
        }
    </script>
<cfelse>
    <script>
         // Load slide when new user login   
        /* fix body shrink issue after a message model disappear */
        if(firstLogin == 1){
                $(document).on('shown.bs.modal', function() { $("body.modal-open").removeAttr("style"); });        
                // integrate popup
                GetSlideShowStatus().then(function(data){       
                    if(data.RXRESULTCODE==1 && data.STATUS==1){
                        $("#mdIntegrate-0").modal("show");
                    }            
                }).catch(function(data){
            
            });
        }    
        // Update when click do not show again
        $(document).on("click",".btn-itegrate",function(){            
            var id= $(this).data("id") ;
            var nextId= id +1;            
            $("#mdIntegrate-"+id).modal("hide");
            $("#mdIntegrate-"+nextId).modal("show");
        });
        $(document).on("click",".md-chk-remember",function(){            
            var status=1;
            if($(this).is(":checked")){
                status=0;                
            }
            else
            {
                status=1;               
            }
            UpdateSlideShowStatus(status).then(function(data){
                if(data.RXRESULTCODE==1){                    
                    if(status==1){
                        $(".md-chk-remember").prop('checked', false);                 
                        
                    }
                    else
                    {                        
                        $(".md-chk-remember").prop('checked', true);                 
                    }
                }
            });
        });
        // End of update/show modal slide intergrate
    </script>
</cfif>