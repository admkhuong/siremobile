


<!--- Make sure current user is an Agency or do not allow this --->
<cfinvoke method="GetAgencyPermission" component="public.sire.models.cfc.userstools" returnvariable="RetVarGetAdminPermission" />

<cfscript>
	createObject("component", "public.sire.models.helpers.layout")
		.addJs("/public/sire/js/select2.min.js", true)
		.addCss("/public/sire/css/select2.min.css", true)
		.addCss("/public/sire/css/select2-bootstrap.min.css", true)
		.addJs("/session/sire/assets/pages/scripts/agency-impersonate-client.js");
</cfscript>


<cfparam name="IUserId" default="0">
<cfset IUserIdSet = 0 />


<cfif IUserId GT 0>
	<!--- Establish Session - set values as found --->
	<cfinvoke component="public.sire.models.cfc.userstools" method="ImpersonateAgencyClient" returnvariable="RetVarImpersonateAgencyClient">
		<cfinvokeargument name="inpUserId" value="#IUserId#">
	</cfinvoke>

	<!--- <cfdump var="#RetVarImpersonateAgencyClient#" /> --->

</cfif>

<cfset AdminCookieData_ga_105 = "" />

<cftry>
	<!--- Read and decrypt admin cookie --->
	<cfset AdminCookieData_ga_105 = Decrypt(evaluate("COOKIE._ga_105"),APPLICATION.EncryptionKey_Cookies,"cfmx_compat","hex") />

	<cfcatch  type="any">

	</cfcatch>

</cftry>

<!--- See if admin cookie is set --->
<cfif ListLen(AdminCookieData_ga_105, ":") EQ 6>
	<!--- See if admin cookie is proper value --->
	<cfif ListGetAt(AdminCookieData_ga_105, 4, ":") EQ 7>
		<cfset AdminKeySet = 1 />
	</cfif>
</cfif>


<div class="portlet light bordered subscriber">
	<div class="row">
		<div class="col-md-12">
			<h2 class="page-title">Agency Client Impersonation</h2>
		</div>
	</div>
	<!--- Listing --->
	<div class="row">
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="form-gd">
			<cfif IUserIdSet EQ 1>
				<h4>The current logged in user has been changed to impersonate <cfoutput>UserId=(<strong>#Session.USERID#</strong>) UserName=(<strong>#Session.USERNAME#</strong>)</cfoutput></h4>
				<h4>You need to log out and re log in as administrator to impersonate someone else.</h4>
				<p><a class="active" href="campaign-manage">Campaigns</a></p>
			<cfelse>
				<form name="frm-impersonate" method="POST">
					<div class="form-group">
					    <label for="IUserId" >Impersonate User Id <span class="text-danger">*</span></label>
					    <select name="user-id" id="user-id" class="form-control validate[required]"></select>
					    <input type="hidden" name="cur-user-id" value="<cfoutput>#Session.USERID#</cfoutput>">
					</div>
					<div class="form-group">
						<button type="submit" class="btn green-gd btn-re form-control" id="btn_SendNow">Impersonate Now</button>
					</div>
				</form>
			</cfif>
			</div>
		</div>
		<div class="col-md-8 col-sm-6">

		</div>
	</div>
</div>


<cfparam name="variables._title" default="Agency Impersonate User">
<cfinclude template="/session/sire/views/layouts/master3.cfm">
