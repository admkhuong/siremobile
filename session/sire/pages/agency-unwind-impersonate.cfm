
<cfinvoke component="public.sire.models.cfc.userstools" method="UnwindImpersonateAgencyClient" returnvariable="RetVarUnwindImpersonateAgencyClient">
</cfinvoke>

<cfdump var="#RetVarUnwindImpersonateAgencyClient#" />

<div class="portlet light bordered subscriber">
	<div class="row">
		<div class="col-md-12">
			<h2 class="page-title">Unwind Agency Client Impersonation</h2>
		</div>

          <div class="col-md-12">
               <cfoutput><a href="/session/sire/pages/agency-impersonate-client" id="UnwindImpersonateLink"><b>Impersonating from</b> <i>#Session.ImpersonateClientAsAgencyId#</i> <i>#Session.ImpersonateClientAsAgencyManagerUserId#</i></a></cfoutput>
          </div>

	</div>

</div>


<cfparam name="variables._title" default="Agency Impersonate User">
<cfinclude template="/session/sire/views/layouts/master3.cfm">
