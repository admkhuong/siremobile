<cfinvoke method="CheckAgencyPermission" component="session.sire.models.cfc.agency"/>
<cfscript>

    CreateObject("component","public.sire.models.helpers.layout")
     // .addCss("/session/sire/assets/global/plugins/daterangepicker/daterangepicker.css", true)
     // .addCss("/public/sire/css/select2.min.css", true)
     // .addCss("/public/sire/css/select2-bootstrap.min.css", true)
     // .addJs("/session/sire/assets/global/plugins/daterangepicker/moment.min.js", true)
     // .addJs("/session/sire/assets/global/plugins/daterangepicker/daterangepicker.js", true)
     // .addJs("/public/sire/js/select2.min.js")
     .addJs("/session/sire/assets/pages/scripts/agency-client-list.js");
</cfscript>

<div class="portlet light bordered">
    <div class="portlet-body form">
        <h2 class="page-title">Clients</h2>
        <div class="row">
               <div class="col-xs-12">
                    <div>
                         <div id="new-user" class="btn pull-right btn-success"> New User </div>

                         <div id="semi-signup-modal" class="modal" role="dialog">
                         <div class="modal-dialog">
                         <div class="modal-content">
                              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                   <br>
                                   <div class="col-md-6">
                                        <div>
                                             <label for="first-name">First Name*</label>
                                             <input id="first-name-semi" type="text" class="form-control" required="required" maxlength="50" >
                                             <div class="help-block"></div>
                                        </div>
                                   </div>
                                   <div class="col-md-6">
                                        <div>
                                             <label for="last-name">Last Name*</label>
                                             <input id="last-name-semi" type="text"  class="form-control" required="required" maxlength="50" >
                                             <div class="help-block"></div>
                                        </div>
                                   </div>
                                   <div class="col-md-6">
                                        <div>
                                             <label for="phone-number">SMS Enabled Phone Number*</label>
                                             <input id="phone-number" type="text" class="form-control" required="required" maxlength="50" >
                                             <div class="help-block"></div>
                                        </div>
                                   </div>
                                   <div class="col-md-6">
                                        <div>
                                             <label for="email-address" class="control-label">E-mail Address*</label>
                                             <input id="email-address" type="email" class="form-control" maxlength="100" required="required" >
                                             <div class="help-block"></div>
                                        </div>
                                   </div>
                                   <div class=" col-md-6 password-generator-switch">Generate a password for me
                                        <label class="switch">
                                             <input id="password-input-semi" type="checkbox">
                                             <span class="slider round"></span>
                                        </label>
                                        <div>
                                             <input id="password-semi" type="text" class="form-control" required="required" minlength="8" maxlength="50" >
                                        </div>
                                   </div>
                                   <div class="validation-message"></div>
                                   <button type="button" disabled="true" class="btn btn-success add-client-button-semi">Submit</button>
                         </div>
                         </div>
                         </div>

                         <div id="fully-signup-modal" class="modal" role="dialog">
                         <div class="modal-dialog">
                         <div class="modal-content">
                              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                   <br>
                                   <div class="col-lg-12">
                                        <div>
                                             <label for="first-name-fully">First Name*</label>
                                             <input id="first-name-fully" type="text" class="form-control" required="required" maxlength="50">
                                             <div class="help-block"></div>
                                        </div>
                                   </div>
                                   <div class="col-lg-12">
                                        <div>
                                             <label for="last-name-fully">Last Name*</label>
                                             <input id="last-name-fully" type="text"  class="form-control" required="required" maxlength="50">
                                             <div class="help-block"></div>
                                        </div>
                                   </div>
                                   <div class="col-lg-12">
                                        <div>
                                             <label for="company-name-fully">Company Name</label>
                                             <input id="company-name-fully" type="text" class="form-control" maxlength="50">
                                        </div>
                                   </div>
                                   <div class="col-lg-12 password-generator-switch">
                                        Generate a password for me
                                        <label class="switch">
                                             <input id="password-input-fully" type="checkbox">
                                             <span class="slider round"></span>
                                        </label>
                                        <div>
                                             <input id="password-fully"  type="text" class="form-control" required="required" minlength="8" maxlength="50">
                                        </div>
                                   </div>
                                   <div class="validation-message"></div>
                                   <button type="button" disabled="true" class="btn btn-success add-client-button-fully">Submit</button>
                         </div>
                         </div>
                         </div>

                         <div id="edit-modal" class="modal" role="dialog">
                         <div class="modal-dialog">
                         <div class="modal-content">
                              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                   <br>
                                   <div class="col-md-6">
                                        <div class="form-group">
                                             <label for="first-name-edit">First Name*</label>
                                             <input id="first-name-edit" type="text" class="form-control" required="required" maxlength="50">
                                             <div class="help-block"></div>
                                        </div>
                                   </div>
                                   <div class="col-md-6">
                                        <div class="form-group">
                                             <label for="last-name-edit">Last Name*</label>
                                             <input id="last-name-edit" type="text" class="form-control" required="required" maxlength="50">
                                             <div class="help-block"></div>
                                        </div>
                                   </div>
                                   <div class="col-md-6">
                                        <div class="form-group">
                                             <label for="phone-number-edit">SMS Enabled Phone Number*</label>
                                             <input id="phone-number-edit" type="text" class="form-control" required="required" maxlength="50">
                                             <div class="help-block"></div>
                                        </div>
                                   </div>
                                   <div class="col-md-6">
                                        <div class="form-group">
                                             <label for="company-name-edit">Company Name</label>
                                             <input id="company-name-edit" type="text" class="form-control" required="required" maxlength="50">
                                             <div class="help-block"></div>
                                        </div>
                                   </div>
                                   <!-- email no longer editable to prevent potential account conflicts -->
                                   <!--- <div class="col-md-6">
                                        <div class="form-group">
                                             <label for="email-address-edit" class="control-label">E-mail Address*</label>
                                             <input id="email-address-edit" type="email" class="form-control" maxlength="100" required="required">
                                             <div class="help-block"></div>
                                        </div>
                                   </div> --->
                                   <button type="button" disabled="true" class="btn btn-success update-client-button">Submit</button>
                         </div>
                         </div>
                         </div>
                    </div>

                    <div>
                         <div id="password-reset-modal" class="modal" role="dialog">
                         <div class="modal-dialog">
                         <div class="modal-content">
                              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                              <div>
                                   <div class="form-group">
                                        <label for="reset-email">Please enter a valid email to send the reset link:</label>
                                        <input id="reset-email" type="text" class="form-control" required="required" maxlength="150">
                                        <div class="help-block"></div>
                                   </div>
                                   <button type="button" data-dismiss="modal" class="btn btn-danger cancel-reset-button">Cancel</button>
                                   <button type="button" class="btn pull-right btn-success send-reset-button">Reset</button>
                              </div>
                         </div>
                         </div>
                         </div>
                    </div>

                    <div class="client-list">
                         <div class="re-table">
                              <div class="table-responsive">
                                   <table id="client-list-detail" class="table table-bordered table-striped table-responsive"></table>
                              </div>
                         </div>
                    </div>

               </div>
          </div>
     </div>
</div>

<cfparam name="variables._title" default="Agency View - Sire">
<cfinclude template="/session/sire/views/layouts/master3.cfm">

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>

<style>
.validation-message
{
     display: inline-block;
     margin-left: 18px;
     margin-top: 10px;
     margin-bottom: 0px;
     color: red;
}

.modal-dialog
{
     font-size: 0.9rem;
}

.modal-content
{
     padding: 30px;
}

.add-client-button-semi,.add-client-button-fully,.update-client-button
{
     margin-left: 83%;
     margin-top: 15px;
}

.dataTables_wrapper .dataTables_paginate .paginate_button
{
     margin: 10px;
     display: inline;
     font-size: 1.5rem;
     padding-left: 8px;
     padding-right: 8px;
     padding-top: 3px;
     padding-bottom: 3px;
}

#new-user
{
     margin-bottom: 20px;
}

#action-dropdown
{
     border-radius: 4px !important;
}

label
{
     font-size: 1.5rem;
}

.bootbox-body
{
     font-size: 1.5rem;
}

.password-generator-switch
{
     position: relative;
     font-size: 1.5rem;
     padding-top: 40px;
}

.switch
{
     position: absolute;
     display: inline-block;
     width: 60px;
     height: 30px;
     margin-left: 12px;
}

.switch input
{
     display:none;
}

.slider
{
     position: absolute;
     cursor: pointer;
     top: 0;
     left: 0;
     right: 0;
     bottom: 0;
     background-color: #ccc;
     -webkit-transition: .4s;
     transition: .4s;
}

.slider:before
{
     position: absolute;
     content: "";
     height: 24px;
     width: 24px;
     left: 5px;
     bottom: 3px;
     background-color: white;
     -webkit-transition: .4s;
     transition: .4s;
}

input:checked + .slider
{
     background-color: #2196F3;
}

input:focus + .slider
{
     box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before
{
     -webkit-transform: translateX(26px);
     -ms-transform: translateX(26px);
     transform: translateX(26px);
}

/* Rounded sliders */
.slider.round
{
     border-radius: 34px;
}

.slider.round:before
{
     border-radius: 50%;
}

#password-fully, #password-semi
{
     margin-top: 25px;
}

.validation-message
{
     font-size: 16px;
}

.table-responsive
{
     display: inline;
}

@media only screen and (max-width: 750px)
{
     .table-responsive
     {
          overflow: scroll;
          display: block;
     }
}
