<!--- RETRIVE CUSTOMER DATA --->
<cfset firstName =''>
<cfset lastName =''>
<cfset emailAddress =''>
<cfset primaryPaymentMethodId = -1>
<cfset expirationDate =''>
<cfset maskedNumber =''>
<cfset line1 =''>
<cfset city =''>
<cfset state =''>
<cfset zip =''>
<cfset country =''> 
<cfset phone =''>
<cfset hidden_card_form =''>
<cfset disabled_field =''>
<cfset cvv =''>
<cfset paymentType = 'card'>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/credit-pages.css">
</cfinvoke>

<!---
<cfinvoke component="session.sire.models.cfc.billing" method="getUserAuthenInfo" returnvariable="GetUserAuthen"></cfinvoke> 
--->

<!--- <cfif GetUserAuthen.RXRESULTCODE EQ 1> --->
	<cfinvoke component="session.sire.models.cfc.billing" method="RetrieveCustomer" returnvariable="RetCustomerInfo"></cfinvoke> 	

	<cfif RetCustomerInfo.RXRESULTCODE EQ 1>
		<cfset firstName = RetCustomerInfo.CUSTOMERINFO.firstName>
		<cfset lastName = RetCustomerInfo.CUSTOMERINFO.lastName>
		<cfset emailAddress = RetCustomerInfo.CUSTOMERINFO.emailAddress>
		<cfset primaryPaymentMethodId = 0> <!--- do not print --->
		<cfset expirationDate = RetCustomerInfo.CUSTOMERINFO.expirationDate>
		<cfset maskedNumber = RetCustomerInfo.CUSTOMERINFO.maskedNumber>
		<cfset line1 = RetCustomerInfo.CUSTOMERINFO.line1>
		<cfset city = RetCustomerInfo.CUSTOMERINFO.city>
		<cfset state = RetCustomerInfo.CUSTOMERINFO.state>
		<cfset zip = RetCustomerInfo.CUSTOMERINFO.zip>
		<cfset country = RetCustomerInfo.CUSTOMERINFO.country> 
		<cfset phone = RetCustomerInfo.CUSTOMERINFO.phone>
		<cfset hidden_card_form = 'display:none'>
		<cfset disabled_field ='disabled'>
		<cfset paymentType = RetCustomerInfo.CUSTOMERINFO.paymentType>
		<cfset cvv =RetCustomerInfo.CUSTOMERINFO.cvv>
	<cfelseif RetCustomerInfo.RXRESULTCODE EQ -1>
		<cfoutput> Connection to Secunet Time Out</cfoutput>
		<cfexit>
	</cfif>
<!---
</cfif>
--->
<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke>

<cfinvoke method="GetUserPlan" component="session/sire/models/cfc/order_plan" returnvariable="getUserPlan">
</cfinvoke>
<main class="container-fluid page credit-pages">
    <cfinclude template="../views/commons/credits_available.cfm">

    <cfinvoke component="session.sire.models.cfc.order_plan" method="GetNextPlanUpgrade" returnvariable="nextPlanUpgrade">
        <cfinvokeargument name="inpPlanOrder" value="#RetUserPlan.PLANORDER#">
    </cfinvoke>

    <section class="row bg-white">
        <div class="content-header">
            <div class="row">
                <div class="col-sm-4 col-lg-3 col-md-4 col-xs-12 content-title">
                    <cfinclude template="../views/commons/welcome.cfm">
                </div>
                <div class="col-sm-8 col-lg-9 col-md-8 col-xs-12 content-menu">
                    <a href="/session/sire/pages/my-plan"><span class="icon-my-plan-unactive"></span><span>My Plan</span></a>

                    <cfif RetUserPlan.PLANEXPIRED EQ 1 >
                        <a href="javascript:void(0)" class="icon-disable" title="Please extend your plan."><span class="icon icon-upgrade-plan-unactive"></span><span>Upgrade Plan</span></a>   
                    <cfelseif RetUserPlan.PLANEXPIRED GTE 2 && RetUserPlan.KEYWORDBUYAFTEREXPRIED GT 0  >   
                        <a href="javascript:void(0)" class="icon-disable" title="Please pay for your keywords"><span class="icon icon-upgrade-plan-unactive"></span><span>Upgrade Plan</span></a>   
                    <cfelse>
                        <cfif nextPlanUpgrade.PlanId_int GT 0>
                            <cfset next_plan_id = nextPlanUpgrade.PlanId_int>
                            <a href="/session/sire/pages/order-plan?plan=<cfoutput>#next_plan_id#</cfoutput>" title="" class=""><span class="icon-upgrade-plan-unactive"></span><span>Upgrade Plan</span></a>   
                        <cfelse>
                            <a href="javascript:void(0)" class="icon-disable" title=""><span class="icon icon-upgrade-plan-unactive"></span><span>Upgrade Plan</span></a>   
                        </cfif>
                    </cfif>

                    <a class="active"><span class="icon-buy-keyword"></span><span>Buy Credit</span></a>

                    <cfif RetUserPlan.PLANEXPIRED GTE 2 && RetUserPlan.KEYWORDBUYAFTEREXPRIED GT 0  >
                        <a href="javascript:void(0)" class="icon-disable" title="Please pay for your keywords"><span class="icon icon-buy-keyword-unactive"></span><span>Buy Keyword</span></a> 
                    <cfelse>    
                        <a href="/session/sire/pages/buy-keyword" title="" class=""><span class="icon-buy-keyword-unactive"></span><span>Buy Keyword</span></a>
                    </cfif>
                    <a href="/session/sire/pages/invite-friend" class="unactive"><span class="icon-invite-friends-unactive"></span><span>Invite a friend</span></a>
                </div>
            </div>
        </div>
        <hr class="hrt0">

    	<cfinclude template="../configs/credits.cfm">
    	<cfoutput>
        <form id="buy_credits" class="container-fluid content-body" action="/session/sire/models/cfm/buy-sms.cfm">
	    	<input type="hidden" id="amount" name="amount" value="0">
	    	<input type="hidden" id="pricemsgafter" value="#getUserPlan.PRICEMSGAFTER#">
	    	<input type="hidden" id="actionStatus" value="">
	    	<input type="hidden" value="#userInfo.FULLEMAIL#" id="h_email" name="h_email">
    		
    		<div id ="step1">
    		<div class="row">
            <div class="col-md-6">
                <div class="row heading">
                	<div class="col-sm-12 heading-title">Secure Payment<hr></div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 secure-payment-plan">
                    	<div class="pull-left">Currency:</div>
                    	<div class="pull-right active">$ US Dollars</div>
                    </div>
                    <div class="col-xs-12 col-sm-12 secure-payment-plan">
                    	<div class="row">
                    		<div class="col-sm-5 col-md-4 col-lg-5 plan-info">Number of Credit:<span class="text-danger">*</span></div>
                    		<div class="col-sm-3 col-md-3 col-lg-2"><input type="text" class="form-control validate[required,custom[onlyNumberNotZero]]" style="text-align:right" id="numberSMS" name="numberSMS"></div>
                    		<div class="col-sm-2 col-md-3 col-lg-3 pricemsg_after plan-info"> x <b><cfoutput>#NumberFormat(getUserPlan.PRICEMSGAFTER,'._')#</cfoutput> Cents </b></div>
                    		<div class="col-sm-2 col-md-2 col-lg-2 pull-right"><a class="btn btn-primary-custom pull-right" id="keyword_calculate">Calculate</a></div>
                    	</div>
                    </div>
                    <div class="col-xs-12 col-sm-12 secure-payment-plan">
                    	<div class="pull-left">Amount:</div>
                    	<div class="pull-right amount active" id="Amount">$0</div>
                    </div>
                    <!---
                    <div class="col-xs-12 col-sm-12 secure-payment-plan">
                    	<div class="pull-left">Number of Keyword:</div>
                    	<div class="pull-right amount active" id="">#getUserPlan.TOTALKEYWORD#</div>
                    </div>
                    <div class="col-xs-12 col-sm-12 secure-payment-plan">
                    	<div class="pull-left">Your Plan:</div>
                    	<div class="pull-right amount text-uppercase active" id="">#getUserPlan.PLANNAME#</div>
                    </div>--->
                    <div class="col-xs-12 col-sm-12 small-info">
                    	Please review your purchase details, then select a payment method to continue
                    </div>
                </div>
            <cfinclude template="../views/commons/payment/step1.cfm">
            </div>   
			<cfinclude template="../views/commons/payment/step2.cfm">        	
        	<cfinclude template="../views/commons/payment/step3_buy_credit.cfm">
    	</form>
    	<div class="row">
    		<div class="col-md-6">
    			<div class="row">
            		<div class="col-sm-12">
                    	<div class="pull-left worldpay-logo"><img src="../images/powered-worldpay.png"></div>
                    	<div class="pull-right worldpay-help">For help with your payment visit: <a target="_blank" href="http://www.worldpay.com/uk/support">WorldPay Help</a></div>
            		</div>
			     </div>         		
			</div>            	
		</div>
    	</cfoutput>
    </section>
</main>

<!-- Modal -->
<div class="modal fade" id="scModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Security Code</h4>
      </div>
      <div class="modal-body text-center">
			<img src="/session/sire/images/securecode.gif"/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/buy_credits_payment.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/site/buy_sms.js">
</cfinvoke>

<script type="text/javascript">
 	var country = '<cfoutput>#country#</cfoutput>';
</script>


<cfparam name="variables._title" default="Buy Credits - Sire">
<cfinclude template="../views/layouts/main.cfm">