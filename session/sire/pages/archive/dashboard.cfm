
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/site/dashboard.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/dashboard.css">
</cfinvoke>

<!--- <cfinvoke component="public.sire.models.helpers.layout" method="addJs">
  <cfinvokeargument name="path" value="/session/sire/js/vendors/morris/raphael-min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
  <cfinvokeargument name="path" value="/session/sire/js/vendors/morris/morris.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
  <cfinvokeargument name="path" value="/session/sire/js/vendors/morris/morris.min.js">
</cfinvoke> --->
<cfquery name="WaitlistDashboard" datasource="#Session.DBSourceEBM#">
    SELECT 
        Value_txt
    FROM
        simpleobjects.sire_setting
    WHERE
        VariableName_txt LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="DashboardWistlist">
    LIMIT 
        1 
</cfquery>




<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUserPlan" returnvariable="RetUserPlan"></cfinvoke> 

<!--- GET USER REFERRAL INFO --->
<cfinvoke component="session.sire.models.cfc.referral" method="GetUserReferInfo" returnvariable="RetValUserReferInfo">
  <cfinvokeargument name="inpUserId" value="#SESSION.USERID#">
</cfinvoke>

<cfinvoke component="session.sire.models.cfc.reports.mlp" method="DashboardMLPSummary" returnvariable="MLPSummary">
</cfinvoke>

<cfif RetUserPlan.PLANEXPIRED GT 0> 
  <cfset monthlyAmount = RetUserPlan.AMOUNT + (RetUserPlan.KEYWORDPURCHASED + RetUserPlan.KEYWORDBUYAFTEREXPRIED)*RetUserPlan.PRICEKEYWORDAFTER >
<cfelse>  
  <cfset monthlyAmount = RetUserPlan.AMOUNT + RetUserPlan.KEYWORDPURCHASED*RetUserPlan.PRICEKEYWORDAFTER >
</cfif> 

<!--- CHECK FIRST LOGIN --->
<cfinvoke component="public.sire.models.cfc.userstools" method="CheckFirstLogin" returnvariable="checkFirstLoginResult">
  <cfinvokeargument name="InpUserId" value="#Session.USERID#">
</cfinvoke>

<cfif checkFirstLoginResult.RXRESULTCODE EQ 1>
  <!-- Button to Trigger Live Demo Video Modal -->
  <a href="#LiveDemoVideoModal" id="LiveDemoVideo" class="btn btn-lg btn-primary hidden" data-toggle="modal">Launch Live Demo Video Modal</a>

  <cfinvoke component="public.sire.models.cfc.userstools" method="UpdateFirstLogedIn" returnvariable="updateFirstLogedInResult">
    <cfinvokeargument name="InpUserId" value="#Session.USERID#">
  </cfinvoke>
</cfif>

<cfinclude template="/session/sire/configs/paths.cfm">

<main class="container-fluid page my-plan-page">
    <cfinclude template="../views/commons/credits_available.cfm">
    <section class="row bg-white si-dashboard si-content">
  		<div class="row">
        <!--- <cfif WaitlistDashboard.Value_txt EQ  'On'>
          <div class="col-md-6 col-xs-12">
            <div class="apps top-box">
              <div class="heading">
                <h2><a class="link" href="wait-list-manage">My Apps</a></h2>
              </div>
              <div class="content">
                <div class="round-corner">
                  <div class="row">
                    <div class="col-md-12">
                      <h3>Total Waitlist</h3>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4 col-xs-12">
                      <p class="num summary-value" data-prop="waitlisted" >0</p>
                      <p class="title">Waitlisted</p>
                    </div>
                    <div class="col-md-4 col-xs-12">
                      <p class="num summary-value" data-prop="served" >0</p>
                      <p class="title">Served</p>
                    </div>
                    <div class="col-md-4 col-xs-12">
                      <p class="num summary-value" data-prop="noshow" >0</p>
                      <p class="title">No Shows</p>
                    </div>
                  </div>
                  <div class="row box-item box-detail" style="display: none;">
                    <div class="col-md-12">
                      <table id="tblWaitlist" class="table table-striped table-bordered table-hover dataTable">
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <cfelse>
          <div class="col-md-6 col-xs-12 pull-right"></div>
        </cfif> --->

        <div class="col-md-6 col-xs-12">
          <div class="apps top-box mlp hyper-link" data-href="/session/sire/pages/mlp-list">
            <div class="heading">
              <h2><a class="link">My Apps</a></h2>
            </div>
            <div class="content">
              <div class="round-corner">
                <div class="row">
                  <div class="col-md-12">
                    <h3>My Landing Pages</h3>
                    <cfif MLPSummary.MLPVIEWS EQ 0 AND MLPSummary.MLPACTIVE EQ 0 AND MLPSummary.MLPDEACTIVE EQ 0>
                      <p id="link-to-mlp" style="text-align: center; margin-left: 1%; margin-right: 1%">You don't have any Marketing Landing Page, click <a href="mlp-list" target="_blank">here</a> to create new.</p>
                    </cfif>
                  </div>
                </div>
                <cfif MLPSummary.MLPVIEWS NEQ 0 OR MLPSummary.MLPACTIVE NEQ 0 OR MLPSummary.MLPDEACTIVE NEQ 0>
                  <div class="row" id="mlp-detail">
                    <div class="col-md-4 col-xs-12">
                      <p class="num" data-prop="mlp-views" id="mlp-views" >0</p>
                      <p class="title">MLP Views</p>
                      <p class="detail">(Last 30 Days)</p>
                    </div>
                    <div class="col-md-4 col-xs-12">
                      <p class="num" data-prop="mlp-active" id="mlp-active" >0</p>
                      <p class="mlpactivetitle">Active MLP #</p>
                    </div>
                    <div class="col-md-4 col-xs-12">
                      <p class="num" data-prop="mlp-deactive" id="mlp-deactive">0</p>
                      <p class="mlpdeactivetitle">Deactive MLP #</p>
                    </div>
                  </div>
                </cfif>
              </div>
            </div>

          </div>
        </div>
        
        <!--- / --->
        <div class="col-md-6 col-xs-12">
          <div class="campaigns top-box hyper-link" data-href="/session/sire/pages/campaign-manage">
            <div class="heading">
              <h2><a class="link" href="campaign-manage">My Campaigns</a></h2>
            </div>
            <div class="content">
              <div class="subsciber">
                <div class="round-corner">
                  <div class="row">
                    <div class="col-md-12">
                      <h3>Campaigns</h3>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4 col-xs-12">
                      <p class="num campaign-info" data-prop="TOTALCAMPAIGN">0</p>
                      <p class="title">Total Campaigns</p>
                    </div>
                    <div class="col-md-4 col-xs-12">
                      <p class="num campaign-info" data-prop="TOTALKEYWORD">0</p>
                      <p class="title">Total Keywords</p>
                    </div>
                    <div class="col-md-4 col-xs-12">
                      <p class="num campaign-info" data-prop="TOTALSMS">0</p>
                      <p class="title">Total Sent/Received SMS</p>
                       <p class="detail">(Last 30 days)</p>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-md-12">
                      <h3>Subscribers</h3>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4 col-xs-12">
                      <p class="num subsciber-count" data-prop="OPTINTOTALCOUNT">0</p>
                      <p class="title">Total Subscribers</p>
                      <p class="detail">(All Lists)</p>
                    </div>
                    <div class="col-md-4 col-xs-12">
                      <p class="num subsciber-count" data-prop="OPTINCOUNT">0</p>
                      <p class="title">New Subscribers</p>
                      <p class="detail">(Last 30 Days)</p>
                    </div>
                    <div class="col-md-4 col-xs-12">
                      <p class="num subsciber-count" data-prop="OPTOUTCOUNT">0</p>
                      <p class="title">Subscriber Opt Outs</p>
                      <p class="detail">(Last 30 Days)</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-xs-12">
  				<div class="feed-activity box-bottom hyper-link" data-href="/session/sire/pages/activity-feed">
  					<div class="heading">
  						<a class="link" href="/session/sire/pages/activity-feed"><h2>My Activity Feed</h2></a>
  					</div>
  					<div class="content">
  						<div class="round-corner">
  							<ul id="has-scroll" class="act-list-box">
  								
  							</ul>
  						</div>
  					</div>
  				</div>
  			</div>
  			<!--- / --->
  			<div class="col-md-6 col-xs-12">
				<div class="account box-bottom hyper-link" data-href="/session/sire/pages/my-plan">
					<div class="heading">
						<h2><a class="link" href="my-plan">My Account</a></h2>
					</div>
					<div class="content">
						<div class="round-corner">
							<div class="col-md-7 col-xs-12">
								<h3>My Plan</h3>
								<div class="row">
									<div class="col-xs-12">
										<div class="my-plan">
                        <div class="row">
                          <div class="col-xs-9"><p>Plan Type:</p></div>
                          <div class="col-xs-3">
                            <p class="label-value">
                              <cfoutput>#userPlanName#</cfoutput>
                            </p>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-xs-9"><p>Monthly Amount:</p></div>
                          <div class="col-xs-3">
                            <p class="label-value">
                              $<cfoutput>#NumberFormat(monthlyAmount,',')#</cfoutput>
                            </p>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-xs-9"><p># of Credits Available:</p></div>
                          <div class="col-xs-3">
                            <p class="label-value">
                              <cfif RetValBillingData.Balance GT 0>
                                <cfoutput>#NumberFormat(RetValBillingData.Balance,',')#</cfoutput>
                              <cfelse>  
                                0
                              </cfif>
                            </p>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-xs-9"><p># of Keywords Available:</p></div>
                          <div class="col-xs-3">
                            <p class="label-value">
                              <cfif RetUserPlan.KEYWORDAVAILABLE GT 0>
                                <cfoutput>#RetUserPlan.KEYWORDAVAILABLE#</cfoutput>
                              <cfelse>  
                                0
                              </cfif>
                            </p>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-xs-9"><p># of Keywords Purchased:</p></div>
                          <div class="col-xs-3">
                            <p class="label-value">
                              <cfset keywordsPurchased = RetUserPlan.KEYWORDPURCHASED + RetUserPlan.KEYWORDBUYAFTEREXPRIED>
                              <cfif keywordsPurchased GT 0>
                                <cfoutput>#keywordsPurchased#</cfoutput>
                              <cfelse>  
                                0
                              </cfif>
                            </p>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-xs-9"><p># of Friends Referred:</p></div>
                          <div class="col-xs-3">
                            <p class="label-value">
                              <cfif RetValUserReferInfo.FriendsReferred GT 0>
                                <cfoutput>#RetValUserReferInfo.FriendsReferred#</cfoutput>
                              <cfelse>
                                0
                              </cfif>
                            </p>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-xs-9"><p># of </span> Referral Credits Received:</p></div>
                          <div class="col-xs-3">
                              <p class="label-value">
                                <cfif RetValUserReferInfo.PromoCreditsReceived GT 0>
                                  <cfoutput>#RetValUserReferInfo.PromoCreditsReceived#</cfoutput>
                                <cfelse>
                                  0
                                </cfif>
                              </p>
                            </div>
                        </div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-5 col-xs-12">	
								<div class="free-items">
									<h3><cfoutput>#userPlanName#</cfoutput></h3>
									<ul>
										<li><cfoutput>#RetUserPlan.USERACCOUNTNUMBER#</cfoutput> User Account</li>
										<li><cfoutput>#RetUserPlan.KEYWORDSLIMITNUMBER#</cfoutput> Keywords</li>
										<li>First <cfoutput>#NumberFormat(RetUserPlan.USERPLANFIRSTSMSINCLUDED,',')#</cfoutput> SMS Included</li>
										<li><cfoutput>#NumberFormat(RetUserPlan.PRICEMSGAFTER,'._')#</cfoutput> Cents per Msg after</li>
										<li><cfoutput>#((IsNumeric(RetUserPlan.MLPSLIMITNUMBER) AND RetUserPlan.MLPSLIMITNUMBER GT 0) ? NumberFormat(RetUserPlan.MLPSLIMITNUMBER,',') : "Unlimited")#</cfoutput> MLPs</li>
										<li><cfoutput>#((IsNumeric(RetUserPlan.SHORTURLSLIMITNUMBER) AND RetUserPlan.SHORTURLSLIMITNUMBER GT 0) ? NumberFormat(RetUserPlan.SHORTURLSLIMITNUMBER,',') : "Unlimited")#</cfoutput> Short URLs</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
  			</div>
  			<!--- / --->
  		</div>
	</section>
  <cfif checkFirstLoginResult.RXRESULTCODE EQ 1>
    <!--- Live Demo Video Modal --->
    <div id="LiveDemoVideoModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">How to Create Your First Campaign</h4>
                </div>
                <div class="modal-body">
                  <div class="embed-responsive embed-responsive-16by9">
              <iframe id="ifrmVideo" class="embed-responsive-item" src="https://www.youtube.com/embed/0cQVhYLBqoA?rel=0&modestbranding=1" frameborder="0" allowfullscreen></iframe>
                  </div>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-success-custom" data-dismiss="modal">Close</button>
              </div>
            </div>
        </div>
    </div>
  </cfif>
</main>

<cfparam name="variables._title" default="Dashboard - Sire">
<cfinclude template="../views/layouts/main.cfm">


