<cfinclude template="../../sire/configs/paths.cfm">

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/js/site/mlp_picker.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/mlp-template-picker.css">
</cfinvoke>

<!---
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/sire/js/jquery.ui.touch-punch.min.js">
</cfinvoke>
--->

<!--- TEMPLATE PICKER --->
<cfinvoke component="session.sire.models.cfc.mlp" method="getCPPTemplateList" returnvariable="listTemplate"></cfinvoke>




<main class="container-fluid page">
  <cfinclude template="../views/commons/credits_available.cfm">
  <section class="row bg-white">
    <div class="content-header">
      <div class="row">
          <div class="col-sm-6 col-md-6 col-lg-6 content-title">
            
            <p class="title">Select a MLP Template<span class="hidden-xs hidden-sm"> to get started!</span></p>
            
            <!--- remove old method: 
			<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>				
			--->
			<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>
            <cfif shortCode.SHORTCODE NEQ ''>
                <p class="sub-title">Your Short Code is <span class="active"><cfoutput>#shortCode.SHORTCODE#</cfoutput></span></p>  
            <cfelse>
                <p class="sub-title"><a class="active" href="##">Set Keywords</a></p>
            </cfif>
            
          </div>
          
      </div>
    </div>
    <hr class="hrt0">
    <div class="content-body">
        <div class="col-md-12">

	      <div id="createCampaignModal">
          <cfif listTemplate.RXRESULTCODE GT 0>  
            <cfset listData = listTemplate.DATA>
            <form action="##" name="form-create-campaign" id="form-create-campaign" autocomplete="off">
                  
                <div class="container-fluid TemplatePickerContainer">
                   <div class="row">
                        
                          <cfloop array="#listData#" index="item">
                              <cfif Len(item.NAME) GT 150>
                                <cfset displayName = Left(item.NAME, 150) & ' ...'>
                                <cfelse>
                                <cfset displayName = item.NAME>                              
                              </cfif>
                             
                              <cfoutput>
                              <!---
                              <div class="col-sm-6 col-md-4 col-lg-3 item-grid cat-class-all">
                                <div id="templateid-#item.ID#" href='/session/sire/pages/mlp-add?templateid=#item.ID#' class="content TemplatePickerImage" <cfif item.IMAGE NEQ ''>style="background-image: url(../images/template-picker/#item.IMAGE#);"</cfif>> 
                            
                                  <p class="template-name"><cfoutput>#displayName#</cfoutput></p>                                  
                                  
                                  <div class="item_action">
                                    <p class="template-name"><cfoutput>#item.NAME#</cfoutput></p>
                                    <div class="select_group_btn">
                                      <button type="button" class="btn btn-success-custom btn-lg btn-select-cpp" >Select</button>
                                      <button type="button" class="btn btn-success-custom btn-lg btn-preview-cpp-template" data-template-id = '<cfoutput>#item.ID#</cfoutput>' >Preview</button>
                                    </div>  
                                  </div>
                                </div>
                                <p class="template-name">  <cfoutput><cfoutput>#item.NAME#</cfoutput></cfoutput></p>
                                --->

                                <div class="col-sm-6 col-md-6 col-lg-4 item-grid cat-class-all">
                                  <div id="templateid-#item.ID#" href='/session/sire/pages/mlp-edit?action=add&ccpxDataIdSource=#item.ID#' class="content mlp-tmp-content">
                                    <iframe src="/mlp-x/lz/index.cfm?inpURL=<cfoutput>#item.ID#</cfoutput>&isPreview=1&action=template" scrolling="auto" frameborder="0" width="100%" height="100%"></iframe>
                                  <div class="item_action">
                                    <div class="select_group_btn">
                                      <button type="button" class="btn btn-success-custom btn-lg btn-select-cpp" >Select</button>
                                    </div>  
                                  </div>

                                  </div>
                                  <a href="##"><p class="template-name"><strong> <cfoutput>#item.NAME#</cfoutput> </strong></p></a>
                                </div>
                              </cfoutput>
                          </cfloop>
                </div>
           </div> 
              
            </form>
          <cfelse>  
            <form action="##" name="form-create-campaign" id="form-create-campaign" autocomplete="off">
              <p class="title">No template!</p>
            </form>  
          </cfif>  
          </div>
        </div>
      </div>
  </section>

</main>

<!--- MODAL POPUP --->
<div class="bootbox modal fade" id="previewCampaignModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel">Preview Campaign</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <!--- <button type="button" class="btn btn-primary btn-success-custom" id="btn-rename-campaign">Save</button> --->
        <button type="button" class="btn btn-default btn-back-custom" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!--- Default bootstrap modal example --->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<cfparam name="variables._title" default="MLP Manage - Sire">
<cfinclude template="../views/layouts/main.cfm">


<script type="application/javascript">


</script>