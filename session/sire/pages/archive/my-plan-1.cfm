<!--- RETRIVE CUSTOMER DATA --->
<cfset firstName =''>
<cfset lastName =''>
<cfset emailAddress =''>
<cfset primaryPaymentMethodId = -1>
<cfset expirationDate =''>
<cfset maskedNumber =''>
<cfset line1 =''>
<cfset city =''>
<cfset state =''>
<cfset zip =''>
<cfset country =''> 
<cfset phone =''>
<cfset hidden_card_form =''>
<cfset disabled_field =''>
<cfset cvv =''>
<cfset paymentType = 'card'>
<cfset RetCustomerInfo = ''>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/my-plan.css">
</cfinvoke>

<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke>
<!---
<cfinvoke component="session.sire.models.cfc.billing" method="getUserAuthenInfo" returnvariable="GetUserAuthen"></cfinvoke> 
--->

<!--- <cfif GetUserAuthen.RXRESULTCODE EQ 1> --->
	<cfinvoke component="session.sire.models.cfc.billing" method="RetrieveCustomer" returnvariable="RetCustomerInfo"></cfinvoke> 	

	<cfif RetCustomerInfo.RXRESULTCODE EQ 1>	
		<cfset firstName = RetCustomerInfo.CUSTOMERINFO.firstName>
		<cfset lastName = RetCustomerInfo.CUSTOMERINFO.lastName>
		<cfset emailAddress = RetCustomerInfo.CUSTOMERINFO.emailAddress>
		<cfset primaryPaymentMethodId = 0> <!--- do not print --->
		<cfset expirationDate = RetCustomerInfo.CUSTOMERINFO.expirationDate>
		<cfset maskedNumber = RetCustomerInfo.CUSTOMERINFO.maskedNumber>
		<cfset line1 = RetCustomerInfo.CUSTOMERINFO.line1>
		<cfset city = RetCustomerInfo.CUSTOMERINFO.city>
		<cfset state = RetCustomerInfo.CUSTOMERINFO.state>
		<cfset zip = RetCustomerInfo.CUSTOMERINFO.zip>
		<cfset country = RetCustomerInfo.CUSTOMERINFO.country> 
		<cfset phone = RetCustomerInfo.CUSTOMERINFO.phone>
		<cfset hidden_card_form = 'display:none'>
		<cfset disabled_field ='disabled'>
		<cfset paymentType = RetCustomerInfo.CUSTOMERINFO.paymentType>
		<cfset cvv =RetCustomerInfo.CUSTOMERINFO.cvv>
	<cfelseif RetCustomerInfo.RXRESULTCODE EQ -1>
		<main class="container-fluid page my-plan-page">
			<cfinclude template="../views/commons/credits_available.cfm">
    			<section class="row bg-white">
    				<div class="content-header">
    					<div class="col-md-12">
    						<cfparam name="variables._title" default="My Plan - Sire">
							<p class="text-danger text-center "><cfoutput>#RetCustomerInfo.MESSAGE#</cfoutput></p>
						</div>	
					</div>
				</section>	
		</main>
		<cfinclude template="../views/layouts/main.cfm">
		<cfexit>
	</cfif>
<!---
</cfif>
--->

 <!--- GET USER REFERRAL DATA --->
<cfinvoke component="session.sire.models.cfc.referral" method="GetUserMonthFree" returnvariable="ResultGetUserReferral">
    <cfinvokeargument name="inpUserId" value="#SESSION.USERID#">
</cfinvoke>

<!--- GET USER REFERRAL INFO --->
<cfinvoke component="session.sire.models.cfc.referral" method="GetUserReferInfo" returnvariable="RetValUserReferInfo">
	<cfinvokeargument name="inpUserId" value="#SESSION.USERID#">
</cfinvoke>


<main class="container-fluid page my-plan-page">
    <cfinclude template="../views/commons/credits_available.cfm">

    <cfinvoke component="session.sire.models.cfc.order_plan" method="GetNextPlanUpgrade" returnvariable="nextPlanUpgrade">
		<cfinvokeargument name="inpPlanOrder" value="#RetUserPlan.PLANORDER#">
	</cfinvoke>
	
    <section class="row bg-white">
        <div class="content-header">
            <div class="row">
                <div class="col-sm-4 col-lg-3 col-md-4 col-xs-12 content-title">
                    <cfinclude template="../views/commons/welcome.cfm">
                </div>
                <div class="col-sm-8 col-lg-9 col-md-8 col-xs-12 content-menu">
                    <a class="active"><span class="icon-my-plan"></span><span>My Plan</span></a>
                    
                    <cfif RetUserPlan.PLANEXPIRED EQ 1 >
                    	<a href="javascript:void(0)" class="icon-disable" title="Please extend your plan."><span class="icon icon-upgrade-plan-unactive"></span><span>Upgrade Plan</span></a>	
                    <cfelseif RetUserPlan.PLANEXPIRED GTE 2 && RetUserPlan.KEYWORDBUYAFTEREXPRIED GT 0  >	
                    	<a href="javascript:void(0)" class="icon-disable" title="Please pay for your keywords"><span class="icon icon-upgrade-plan-unactive"></span><span>Upgrade Plan</span></a>	
                    <cfelse>
                		<cfif nextPlanUpgrade.PlanId_int GT 0>
	                    	<cfset next_plan_id = nextPlanUpgrade.PlanId_int>
	                    	<a href="/session/sire/pages/order-plan?plan=<cfoutput>#next_plan_id#</cfoutput>" title="" class=""><span class="icon-upgrade-plan-unactive"></span><span>Upgrade Plan</span></a>	
	                    <cfelse>
	                    	<a href="javascript:void(0)" class="icon-disable" title=""><span class="icon icon-upgrade-plan-unactive"></span><span>Upgrade Plan</span></a>	
	                    </cfif>
                    </cfif>	
                    
                    <a href="/session/sire/pages/buy-sms" title="" class=""><span class="icon-buy-credits-unactive"></span><span>Buy Credit</span></a>

                    <cfif RetUserPlan.PLANEXPIRED GTE 2 && RetUserPlan.KEYWORDBUYAFTEREXPRIED GT 0  >
                    	<a href="javascript:void(0)" class="icon-disable" title="Please pay for your keywords"><span class="icon icon-buy-keyword-unactive"></span><span>Buy Keyword</span></a>	
                    <cfelse>	
                    	<a href="/session/sire/pages/buy-keyword" title="" class=""><span class="icon-buy-keyword-unactive"></span><span>Buy Keyword</span></a>
                    </cfif>
                    <a href="/session/sire/pages/invite-friend" class="unactive"><span class="icon-invite-friends-unactive"></span><span>Invite a friend</span></a>	
	            </div>    
            </div>
        </div>
        <hr class="hrt0">

        <cfif RetUserPlan.PLANEXPIRED GT 0> 
        	<cfset monthlyAmount = RetUserPlan.AMOUNT + (RetUserPlan.KEYWORDPURCHASED + RetUserPlan.KEYWORDBUYAFTEREXPRIED)*RetUserPlan.PRICEKEYWORDAFTER >
        <cfelse>	
        	<cfset monthlyAmount = RetUserPlan.AMOUNT + RetUserPlan.KEYWORDPURCHASED*RetUserPlan.PRICEKEYWORDAFTER >
        </cfif>	

    	<cfinclude template="../configs/credits.cfm">

    	<!--- GET MAX KEYWORD CAN EXPIRED --->
		<cfset UnsubscribeNumberMax = 0>
		<cfif RetUserPlan.PLANEXPIRED EQ 1>
		    <cfif RetUserPlan.KEYWORDAVAILABLE GT RetUserPlan.KEYWORDBUYAFTEREXPRIED>
		        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDBUYAFTEREXPRIED>
		    <cfelse>
		        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDAVAILABLE>
		    </cfif>
		<cfelseif RetUserPlan.PLANEXPIRED EQ 2> <!--- IF USER PLAN HAS DOWNGRADE IT CAN NOT UNSCRIBE KEYWORD --->
			<!---
		    <cfif RetUserPlan.KEYWORDAVAILABLE GT RetUserPlan.KEYWORDBUYAFTEREXPRIED>
		        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDBUYAFTEREXPRIED>
		    <cfelse>
		        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDAVAILABLE>
		    </cfif>    
			--->
		    <cfset UnsubscribeNumberMax = 0>
		<cfelse>
		    <cfif RetUserPlan.KEYWORDAVAILABLE GT RetUserPlan.KEYWORDPURCHASED>
		        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDPURCHASED>
		    <cfelse>
		        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDAVAILABLE>
		    </cfif>
		</cfif>

    	<cfoutput>
        <form id="my_plan_form" class="container-fluid content-body" action="##" autocomplete="off">
        	<input type="hidden" name="primaryPaymentMethodId" value="#primaryPaymentMethodId#">
        	<input type="hidden" value="#userInfo.FULLEMAIL#" id="h_email">
    		<div class="row">
	            <div class="col-md-6">
	                <div class="row heading">
	                	<div class="col-sm-12 heading-title">My Plan<hr></div>
	                </div>
	                <div class="row">
	                    <div class="col-xs-11 col-sm-11 secure-payment-plan">
	                    	<div class="pull-left">Plan Type:</div>
	                    	<div class="pull-right active"><cfoutput>#userPlanName#</cfoutput></div>
	                    </div>

	                    <div class="col-xs-11 col-sm-11 secure-payment-plan">
	                    	<div class="pull-left">Monthly Amount:</div>
	                    	<div class="pull-right active">$ #NumberFormat(monthlyAmount,',')#</div>
	                    </div>

	                    <div class="col-xs-11 col-sm-11 secure-payment-plan">
	                    	<div class="pull-left">## of Credits Available:</div>
	                    	<div class="pull-right active">
	                    		<cfif RetValBillingData.Balance GT 0>
		                    		#NumberFormat(RetValBillingData.Balance,',')#
		                    	<cfelse>	
		                    		0
		                    	</cfif>
	                    	</div>
	                    </div>

	                    <div class="col-xs-11 col-sm-11 secure-payment-plan">
	                    	<div class="pull-left">## of Keywords Available:</div>
	                    	<div class="pull-right active">
	                    		<cfif RetUserPlan.KEYWORDAVAILABLE GT 0>
		                    		#RetUserPlan.KEYWORDAVAILABLE#
		                    	<cfelse>	
		                    		0
		                    	</cfif>
	                    	</div>
	                    </div>

	                    <div class="col-xs-11 col-sm-11 secure-payment-plan">
	                    	<div class="pull-left">## of Keywords Purchased:
		                    	<cfif UnsubscribeNumberMax GT 0>
		                    		<a href="##" data-toggle="modal" data-target="##UnscriberKeywordModal"><span class="glyphicon glyphicon-arrow-down" title="Unsubscribe keyword"></span></a>
		                    	<cfelse>
		                    		<a class="unsubscribe_disable"><span class="glyphicon glyphicon-arrow-down" title="Unsubscribe keyword"></span></a>
		                    	</cfif>	
	                    	</div>
	                    	<div class="pull-right active">
	                    		<cfset keywordsPurchased = RetUserPlan.KEYWORDPURCHASED + RetUserPlan.KEYWORDBUYAFTEREXPRIED>
		                    	<cfif keywordsPurchased GT 0>
		                    		#keywordsPurchased#
		                    	<cfelse>	
		                    		0
		                    	</cfif>

	                    	</div>
	                    </div>
	                    
						<div class="col-xs-11 col-sm-11 secure-payment-plan">
							<div class="pull-left">##  of Friends Referred:</div>
							<div class="pull-right active">
								<cfif RetValUserReferInfo.FriendsReferred GT 0>
									#RetValUserReferInfo.FriendsReferred#
								<cfelse>
									0
								</cfif>
							</div>
						</div>

						<div class="col-xs-11 col-sm-11 secure-payment-plan">
							<div class="pull-left">##  of Referral Credits Received:</div>
							<div class="pull-right active">
								<cfif RetValUserReferInfo.PromoCreditsReceived GT 0>
									#RetValUserReferInfo.PromoCreditsReceived#
								<cfelse>
									0
								</cfif>
							</div>
						</div>

	                    <div class="col-xs-11 col-sm-11 secure-payment-plan">
	                    	<div class="pull-left">Plan Details Include:</div>
	                    </div>

	                   <div class="col-sm-5 col-md-8 col-lg-5 col-xs-12">
	                        <div class="sire-pricing" id="price1">
	                            <div class="price-title">
	                                <h4><cfoutput>#userPlanName#</cfoutput></h4>
	                            </div>
	                            <div class="price-content">
	                                <p><cfoutput>#RetUserPlan.USERACCOUNTNUMBER#</cfoutput> User Account</p>
	                                <p><cfoutput>#RetUserPlan.KEYWORDSLIMITNUMBER#</cfoutput> Keywords</p>
	                                <p>First <cfoutput>#NumberFormat(RetUserPlan.USERPLANFIRSTSMSINCLUDED,',')#</cfoutput> SMS Included</p>
	                                <p><cfoutput>#NumberFormat(RetUserPlan.PRICEMSGAFTER,'._')#</cfoutput> Cents per Msg after</p>
	                                <p><cfoutput>#((IsNumeric(RetUserPlan.MLPSLIMITNUMBER) AND RetUserPlan.MLPSLIMITNUMBER GT 0) ? NumberFormat(RetUserPlan.MLPSLIMITNUMBER,',') : "Unlimited")#</cfoutput> MLPs</p>
	                                <p><cfoutput>#((IsNumeric(RetUserPlan.SHORTURLSLIMITNUMBER) AND RetUserPlan.SHORTURLSLIMITNUMBER GT 0) ? NumberFormat(RetUserPlan.SHORTURLSLIMITNUMBER,',') : "Unlimited")#</cfoutput> Short URLs</p>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                
	                <cfif hidden_card_form NEQ ''>
	            		<hr>
	            		<cfif paymentType EQ 'card'>
		        			<div class="row heading">
		            			<div class="col-sm-12 heading-title">Card Information<hr></div>
		            			<div class="col-sm-11 form-horizontal">
		            				<div class="form-group">
		            					<label for="card_number" class="col-sm-11 control-label">Card Number: #maskedNumber#</label>
		            				</div>		
		            				<div class="form-group">
		            					<label for="card_number" class="col-sm-11 control-label">Expiration Date: #expirationDate#</label>
		            				</div>		
		            				<div class="form-group">
		            					<label for="card_number" class="col-sm-11 control-label">Cardholder Name: #firstName# #lastName# </label>
		            				</div>
		            				<div class="form-group">
										<button type="button" id="remove-payment-btn" title="Remove Card Infomation" class="btn btn-success-custom">Remove</button>
									</div>
		            				<div class="radio-inline">
									 <label>
									    <input type="radio" name="optionsRadios" class="select_used_card" id="optionsRadios1" value="1" checked>
									    	Current Card
									  </label>
									</div>
									<div class="radio-inline">
									  	<label>
									    	<input type="radio" name="optionsRadios" class="select_used_card" id="optionsRadios2" value="2">
									    	New Card
									  	</label>
									</div>
								</div>	
		            		</div>
		            	<cfelseif paymentType EQ 'check'>
		            		<div class="row heading">
		            			<div class="col-sm-12 heading-title">ECheck Information<hr></div>
		            			<div class="col-sm-11 form-horizontal">
		            				<div class="form-group">
		            					<label for="card_number" class="col-sm-11 control-label">Routing Number: ****#Right(maskedNumber, 4)#</label>
		            				</div>		
		            				<div class="form-group">
		            					<label for="card_number" class="col-sm-11 control-label">Account Number: ****#Right(cvv, 4)#</label>
		            				</div>		
		            				<div class="form-group">
		            					<label for="card_number" class="col-sm-11 control-label">Customer Name: #firstName# #lastName# </label>
		            				</div>
		            				<div class="radio-inline">
									 <label>
									    <input type="radio" name="optionsRadios" class="select_used_card" id="optionsRadios1" value="1" checked>
									    	Current Card
									  </label>
									</div>
									<div class="radio-inline">
									  	<label>
									    	<input type="radio" name="optionsRadios" class="select_used_card" id="optionsRadios2" value="2">
									    	New Card
									  	</label>
									</div>
								</div>	
		            		</div>
	            		</cfif>
	            	</cfif>

	                <hr/>
	                <div class="update_card_info" style="#hidden_card_form#">
			        	<div class="row heading">
			        		<!---
		        			<div class="col-sm-12 heading-title">
			        			Select Your Payment Method:
		        			</div>
		        			--->
		        			<div class="col-sm-12">
		        				<input type="radio" name="payment_method" class="check_payment_method" id="payment_method_0" value="0" checked/>
			        			<label for="payment_method_0" class="payment_method_card payment_method_card_0">
			        				&nbsp;
			        			</label>
		        				<!---<input type="radio" name="payment_method" class="check_payment_method" id="payment_method_0_1" value="0"/>
			        			<label for="payment_method_0_1" class="payment_method_card payment_method_card_1">
			        				&nbsp;
			        			</label>
		        				<input type="radio" name="payment_method" class="check_payment_method" id="payment_method_0_2" value="0"/>
			        			<label for="payment_method_0_2" class="payment_method_card payment_method_card_2">
			        				&nbsp;
			        			</label>
		        				<input type="radio" name="payment_method" class="check_payment_method" id="payment_method_0_3" value="0"/>
			        			<label for="payment_method_0_3" class="payment_method_card payment_method_card_3">
			        				&nbsp;
			        			</label>--->
			        			<!---
		        				<input type="radio" name="payment_method" class="check_payment_method" id="payment_method_1" value="1"/>
			        			<label for="payment_method_1" class="payment_method_echeck">
			        				&nbsp;
			        			</label>
			        			--->
		        			</div>
		        			<div class="col-sm-12">
		    					<hr style="margin-top: 10px;"> 
		        			</div>
			        	</div>

		                <div class="row">
		                	<div class="col-sm-11 form-horizontal">
							  <div class="form-group">
							    <label for="card_number" class="col-sm-4 control-label">Card Number:<span class="text-danger">*</span></label>
							    <div class="col-sm-8">
							      <input type="text" class="form-control validate[required,custom[creditCardFunc]]" maxlength="32" id="card_number" name="number" value=''>
							    </div>
							  </div>
							  <div class="form-group">
							    <label for="security_code" class="col-sm-4 control-label">Security Code:<span class="text-danger">*</span>
							    	<a href="##" data-toggle="modal" data-target="##scModal"><img src="/session/sire/images/help-small.png"/></a>
							    </label>
							    <div class="col-sm-8">
							      <input type="text" class="form-control validate[required,custom[onlyNumber]]" maxlength="4" id="security_code" name="cvv" data-errormessage-custom-error="* Invalid security code">
							    </div>
							  </div>
							  <div class="form-group" id="ExpirationDate">
							    <label for="" class="col-sm-4 control-label">Expiration Date:<span class="text-danger">*</span></label>
						        <input type="hidden" id="expiration_date" name="expirationDate" value="">
							    <div class="col-sm-4">
							      <select class="form-control validate[required]" id="expiration_date_month">
							      	<option value="01">January</option>
							      	<option value="02">February</option>
							      	<option value="03">March</option>
							      	<option value="04">April</option>
							      	<option value="05">May</option>
							      	<option value="06">June</option>
							      	<option value="07">July</option>
							      	<option value="08">August</option>
							      	<option value="09">September</option>
							      	<option value="10">October</option>
							      	<option value="11">November</option>
							      	<option value="12">December</option>
							      </select>
							    </div>
							    <div class="col-sm-4">
							    	<select class="form-control validate[required]" id="expiration_date_year">
							    		<cfloop from="#year(now())#" to="#year(now())+50#" index="i">
							    			<option value="#i#">#i#</option>
							    		</cfloop>
							    	</select>
							    </div>
							  </div>
							  <div class="form-group">
							    <label for="first_name" class="col-sm-4 control-label" id="card_name_label">Cardholder Name:<span class="text-danger">*</span></label>
							    <div class="col-sm-4">
							      <input type="text" class="form-control validate[required,custom[onlyLetterNumberSp]]" maxlength="50" id="first_name" name="firstName" placeholder="First Name" value="">
							    </div>
							    <div class="col-sm-4">
							      <input type="text" class="form-control validate[required,custom[onlyLetterNumberSp]]" maxlength="50" id="last_name" name="lastName" placeholder="Last Name" value="">
							    </div>
							  </div>
							</div>
		            	</div>
		            </div>
	        	</div>
	        	<div class="col-md-6">
	                <div class="row heading">
	                    <div class="col-sm-12 heading-title">Cardholder Details<hr></div>
	                </div>
	                <div class="update_cardholder_info">

	                	<fieldset name='fset_cardholder_info' #disabled_field#>
		                <div class="row">
		                	<div class="col-sm-11 form-horizontal">
							  <div class="form-group">
							    <label for="line1" class="col-sm-4 control-label">Address:<span class="text-danger">*</span></label>
							    <div class="col-sm-8">
							      <input type="text" class="form-control validate[required,custom[noHTML]] cardholder" maxlength="60" id="line1" name="line1" value="#line1#" data-value='#line1#'>
							    </div>
							  </div>
							  <div class="form-group">
							    <label for="city" class="col-sm-4 control-label">City:<span class="text-danger">*</span></label>
							    <div class="col-sm-8">
							      <input type="text" class="form-control validate[required,custom[noHTML]] cardholder" maxlength="40" id="city" name="city" value="#city#" data-value='#city#'>
							    </div>
							  </div>
							  <div class="form-group">
							    <label for="state" class="col-sm-4 control-label">State:<span class="text-danger">*</span></label>
							    <div class="col-sm-8">
							      <input type="text" class="form-control validate[required,custom[noHTML]] cardholder" maxlength="40" id="state" name="state" value="#state#" data-value="#state#">
							    </div>
							  </div>
							  <div class="form-group">
							    <label for="zip" class="col-sm-4 control-label">Zip Code:<span class="text-danger">*</span></label>
							    <div class="col-sm-8">
							      <input type="text" class="form-control validate[required,custom[onlyNumber]] cardholder" maxlength="5" id="zip" name="zip" value="#zip#" data-value="#zip#" data-errormessage-custom-error="* Invalid zip code">
							    </div>
							  </div>
							  <div class="form-group">
							    <label for="country" class="col-sm-4 control-label">Country:<span class="text-danger">*</span></label>
							    <div class="col-sm-8">
									<select class="form-control cardholder validate[required]" id="country" name="country" data-value="#country#" >
										<cfinclude template="../views/commons/payment/country.cfm">
									</select>
							    </div>
							  </div>
							  <!---
							  <div class="form-group">
							    <label for="phone" class="col-sm-4 control-label">Telephone:</label>
							    <div class="col-sm-8">
							      <input type="text" class="form-control validate[custom[phone]] cardholder" maxlength="30" id="phone" name="phone" value="#phone#" data-value="#phone#">
							    </div>
							  </div>--->
							  <input type="hidden" class="form-control validate[custom[phone]] cardholder" maxlength="30" id="phone" name="phone" value="#phone#" data-value="#phone#">
							  <div class="form-group">
							    <label for="email" class="col-sm-4 control-label">Email:</label>
							    <div class="col-sm-8">
							      <input type="text" class="form-control validate[custom[email]] cardholder" maxlength="128" id="email" name="email" value="#emailAddress#" data-value="#emailAddress#">
							    </div>
							  </div>
							  <br>
				            	<hr>
				            	<div class="row table-footer">
				            		<div class="col-sm-12">
				                    	<div class="pull-left"><img src="../images/powered-worldpay.png"></div>
				                    	<div class="pull-right worldpay-help">For help with your payment visit: <a target="_blank" href="http://www.worldpay.com/uk/support">WorldPay Help</a></div>
				            		</div>
				            	</div>
						    </div>
		                </div>
		                </fieldset>
	                </div>
	            </div>
	        </div>
            <div class="row">
	        	<div class="col-sm-12 text-right">
	        		<button type="submit" class="btn btn-medium btn-success-custom btn-update-profile " #disabled_field# >Update Payment Info</button>
	        		<a style="margin-left: 10px;" type="button" class="btn btn-medium btn-back-custom"  href="javascript:history.go(-1)">Cancel</a>
	        	</div>
        	</div>
    	</form>
    	</cfoutput>
    </section>
</main>

<!-- Modal -->
<div class="modal fade" id="scModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Security Code</h4>
      </div>
      <div class="modal-body text-center">
			<img src="/session/sire/images/securecode.gif" style="width:100%"/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="bootbox modal fade" id="UnscriberKeywordModal" tabindex="-1" role="dialog" aria-labelledby="signOutLabel">
  
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title title">Unsubscribe Keyword</h4>            
            </div>
      <div class="modal-body">
        		<form id="unscriber_keyword_form" class="row">
        			<label for="subscriber_list_name" class="col-sm-4">
        				Number Keyword:*
        			</label>
        			<div class="col-sm-8">
        				<input type="hidden" id="unsubscribe_number_max" name="unsubscribe_number_max" value="<cfoutput>#UnsubscribeNumberMax#</cfoutput>">
        				<input id="numberKeywordUnscriber" class="form-control validate[required,custom[onlyNumber],min[1],max[<cfoutput>#UnsubscribeNumberMax#</cfoutput>]]" maxlength="255">
        				<div style="color:red">You can Unsubscribe Max <cfoutput>#UnsubscribeNumberMax#</cfoutput> keywords</div>
        			</div>
        		</form>
       </div>
        <div class="modal-footer">
                <button type="button" class="btn btn btn-medium btn-success-custom" id="Unsubscribe"> Unsubscribe </button>
                <button type="button" class="btn btn btn-medium btn-back-custom" data-dismiss="modal">Cancel</button>
            </div>
    </div>
  </div>
</div>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/my_plan.js">
</cfinvoke>

<script type="text/javascript">
 	var country = '<cfoutput>#country#</cfoutput>';
</script>

<cfparam name="variables._title" default="My Plan - Sire">

<cfinclude template="../views/layouts/main.cfm">