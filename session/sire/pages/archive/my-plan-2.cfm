<cfset variables.menuToUse = 2 />
<cfset variables.urlpage = 'myplan' />
<!--- RETRIVE CUSTOMER DATA --->
<cfset firstName =''>
<cfset lastName =''>
<cfset emailAddress =''>
<cfset primaryPaymentMethodId = -1>
<cfset expirationDate =''>
<cfset maskedNumber =''>
<cfset line1 =''>
<cfset city =''>
<cfset state =''>
<cfset zip =''>
<cfset country =''> 
<cfset phone =''>
<cfset hidden_card_form =''>
<cfset disabled_field =''>
<cfset cvv =''>
<cfset paymentType = 'card'>
<cfset RetCustomerInfo = ''>

<cfscript>
CreateObject("component","public.sire.models.helpers.layout")
.addCss("../assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css", true)
.addJs("../assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js", true)
.addJs("../assets/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js", true)
.addJs("../assets/pages/scripts/myplan.js")
.addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
.addJs("../assets/global/plugins/uikit/js/uikit.min.js", true);
</cfscript>

<!--- GET USER BILLING DATA --->
<cfinvoke component="session.sire.models.cfc.billing"  method="GetBalanceCurrentUser"  returnvariable="RetValBillingData"></cfinvoke>



<!--- GET USER PLAN DETAIL --->
<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUserPlan" returnvariable="RetUserPlan"></cfinvoke>

<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetOrderPlan"  returnvariable="RetPlan">
    <cfinvokeargument name="plan" value="#RetUserPlan.PLANID#">
</cfinvoke>

<!--- GET NUMBER OF USED MLPS BY USER --->
<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUsedMLPsByUserId" returnvariable="RetUsedMLP"></cfinvoke>

<!--- GET NUMBER OF USED SHORT URLS BY USER --->
<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUsedShortURLsByUserId" returnvariable="RetUsedShortURL"></cfinvoke>

<!--- GET USER REFERRAL INFO --->
<cfinvoke component="session.sire.models.cfc.referral" method="GetUserReferInfo" returnvariable="RetValUserReferInfo">
<cfinvokeargument name="inpUserId" value="#SESSION.USERID#">
</cfinvoke>

<!--- GET USER INFO --->
<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke>

<!--- GET NEXT PLAN UPGRADE --->
<cfinvoke component="session.sire.models.cfc.order_plan" method="GetNextPlanUpgrade" returnvariable="nextPlanUpgrade">
<cfinvokeargument name="inpPlanOrder" value="#RetUserPlan.PLANORDER#">
</cfinvoke>

<cfinvoke component="session.sire.models.cfc.billing" method="RetrieveCustomer" returnvariable="RetCustomerInfo"></cfinvoke>    

<cfif RetCustomerInfo.RXRESULTCODE EQ 1>    
    <cfset firstName = RetCustomerInfo.CUSTOMERINFO.firstName>
    <cfset lastName = RetCustomerInfo.CUSTOMERINFO.lastName>
    <cfset emailAddress = RetCustomerInfo.CUSTOMERINFO.emailAddress>
    <cfset primaryPaymentMethodId = 0> <!--- do not print --->
    <cfset expirationDate = RetCustomerInfo.CUSTOMERINFO.expirationDate>
    <cfset maskedNumber = RetCustomerInfo.CUSTOMERINFO.maskedNumber>
    <cfset line1 = RetCustomerInfo.CUSTOMERINFO.line1>
    <cfset city = RetCustomerInfo.CUSTOMERINFO.city>
    <cfset state = RetCustomerInfo.CUSTOMERINFO.state>
    <cfset zip = RetCustomerInfo.CUSTOMERINFO.zip>
    <cfset country = RetCustomerInfo.CUSTOMERINFO.country> 
    <cfset phone = RetCustomerInfo.CUSTOMERINFO.phone>
    <cfset hidden_card_form = 'display:none'>
    <cfset disabled_field ='display: none'>
    <cfset paymentType = RetCustomerInfo.CUSTOMERINFO.paymentType>
    <cfset cvv =RetCustomerInfo.CUSTOMERINFO.cvv>
    <cfelseif RetCustomerInfo.RXRESULTCODE EQ -1>
        <main class="container-fluid page my-plan-page">
            <cfinclude template="../views/commons/credits_available.cfm">
            <section class="row bg-white">
                <div class="content-header">
                    <div class="col-md-12">
                        <cfparam name="variables._title" default="My Plan - Sire">
                        <p class="text-danger text-center "><cfoutput>#RetCustomerInfo.MESSAGE#</cfoutput></p>
                    </div>  
                </div>
            </section>  
        </main>
        <cfinclude template="../views/layouts/master.cfm">
        <cfexit>
        </cfif>

        <cfset userPlanName = 'Free'>
        <cfif RetUserPlan.RXRESULTCODE EQ 1>
            <cfset userPlanName =   RetUserPlan.PLANNAME>
        </cfif>

        <cfif RetUserPlan.PLANEXPIRED GT 0> 
            <cfset monthlyAmount = RetUserPlan.AMOUNT + (RetUserPlan.KEYWORDPURCHASED + RetUserPlan.KEYWORDBUYAFTEREXPRIED)*RetUserPlan.PRICEKEYWORDAFTER >
            <cfelse>    
                <cfset monthlyAmount = RetUserPlan.AMOUNT + RetUserPlan.KEYWORDPURCHASED*RetUserPlan.PRICEKEYWORDAFTER >
            </cfif>

            <cfset planMonthlyAmount = RetUserPlan.AMOUNT />

            <!--- GET MAX KEYWORD CAN EXPIRED --->
            <cfset UnsubscribeNumberMax = 0>
            <cfif RetUserPlan.PLANEXPIRED EQ 1>
                <cfif RetUserPlan.KEYWORDAVAILABLE GT RetUserPlan.KEYWORDBUYAFTEREXPRIED>
                    <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDBUYAFTEREXPRIED>
                    <cfelse>
                        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDAVAILABLE>
                    </cfif>
                    <cfelseif RetUserPlan.PLANEXPIRED EQ 2> <!--- IF USER PLAN HAS DOWNGRADE IT CAN NOT UNSCRIBE KEYWORD --->
    <!---
    <cfif RetUserPlan.KEYWORDAVAILABLE GT RetUserPlan.KEYWORDBUYAFTEREXPRIED>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDBUYAFTEREXPRIED>
    <cfelse>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDAVAILABLE>
    </cfif>    
--->
<cfset UnsubscribeNumberMax = 0>
<cfelse>
    <cfif RetUserPlan.KEYWORDAVAILABLE GT RetUserPlan.KEYWORDPURCHASED>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDPURCHASED>
        <cfelse>
            <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDAVAILABLE>
        </cfif>
    </cfif>

    <div class="portlet light bordered">
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form id="my_plan_form" class="form-gd" action="##" autocomplete="off" role="form">
                <input type="hidden" name="primaryPaymentMethodId" value="<cfoutput>#primaryPaymentMethodId#</cfoutput>">
                <input type="hidden" value="<cfoutput>#userInfo.FULLEMAIL#</cfoutput>" id="h_email">
                <input type="hidden" name="h_email_save" value="<cfoutput>#emailAddress#</cfoutput>">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="clearfix">
                                <h3 class="form-heading">Plan Details</h3>
                            </div>
                            <div class="statistical">
                                <!--- Plan Details included by plan_details.cfm --->
                                <cfinclude template="../views/commons/plan_details.cfm">
                            </div>

                            <div class="tab-invite-1 boundary-align uk-panel myplan-tabbable">
                                <a href="##" id="trigger-myplan-tab" class="btn green-gd newbtn uk-width-1-1 uk-margin-small-bottom visible-xs"><i class="fa fa-th" aria-hidden="true"></i> Toggle My Plan Tab</a>

                                <ul id="toggle-xs" class="uk-child-width-1-3 uk-child-width-1-5@s invite-ul uk-tab" uk-margin>
                                    <li class="uk-active">
                                        <a href="##plantab1">
                                            <span class="tab-img i-1"></span>
                                            <span>My Plan</span>
                                        </a>
                                    </li>
                                    <li>
                                        <cfif RetUserPlan.PLANEXPIRED EQ 1 > 
                                            <a class="plan-disabled" title="Please extend your plan.">
                                                <span class="tab-img i-2"></span>
                                                <span>Upgrade Plan</span>
                                            </a>   
                                            <cfelseif RetUserPlan.PLANEXPIRED GTE 2 && RetUserPlan.KEYWORDBUYAFTEREXPRIED GT 0  >   
                                                <a class="plan-disabled" title="Please pay for your keywords">
                                                    <span class="tab-img i-2"></span>
                                                    <span>Upgrade Plan</span>
                                                </a>  
                                                <cfelse>
                                                    <cfif nextPlanUpgrade.PlanId_int GT 0>
                                                        <cfset next_plan_id = nextPlanUpgrade.PlanId_int>
                                                        <a href="/session/sire/pages/order-plan?plan=<cfoutput>#next_plan_id#</cfoutput>" title="" class="link-myplan">
                                                        <span class="tab-img i-2"></span>
                                                        <span>Upgrade Plan</span>
                                                    </a>   
                                                    <cfelse>
                                                        <a class="plan-disabled" title="">
                                                            <span class="tab-img i-2"></span>
                                                            <span>Upgrade Plan</span>
                                                        </a>   
                                                    </cfif>
                                                </cfif> 
                                            </li>
                                            <li>
                                                <a href="/session/sire/pages/buy-sms" title="" class="link-myplan">
                                                    <span class="tab-img i-3"></span>
                                                    <span>Buy Credit</span>
                                                </a>
                                            </li>
                                            <li>
                                                <cfif RetUserPlan.PLANEXPIRED GTE 2 && RetUserPlan.KEYWORDBUYAFTEREXPRIED GT 0  >
                                                    <a class="plan-disabled" title="Please pay for your keywords">
                                                        <span class="tab-img i-4"></span>
                                                        <span>Buy Keyword</span>
                                                    </a> 
                                                    <cfelse>  
                                                        <a href="/session/sire/pages/buy-keyword" title="" class="link-myplan">
                                                            <span class="tab-img i-4"></span>
                                                            <span>Buy Keyword</span>
                                                        </a>
                                                    </cfif>
                                                </li>
                                                <li>
                                                    <a href="/session/sire/pages/invite-friend" class="link-myplan">
                                                        <span class="tab-img i-5"></span>
                                                        <span>Invite a friend</span>
                                                    </a>
                                                </li>
                                            </ul>

                                            <!--- <ul class="uk-switcher uk-margin invite-content">
                                                <li></li>
                                                <li>
                                                    <div class="wrap-upgrade">
                                                        <div class="form-group">
                                                            <label>Select Your Plan</label>
                                                            <select class="bs-select form-control">
                                                                <option>Individual</option>
                                                                <option>Individual 1</option>
                                                                <option>Individual 2</option>
                                                                <option>Individual 3</option>
                                                            </select>
                                                        </div>

                                                        <div class="statistical">
                                                            <ul class="nav list-plan-statistical">
                                                                <li>Monthly Amount: <span>$24</span>
                                                                <em>Your billing date will be the same day every month.</em>
                                                            </li>
                                                            <li>Credits Available:  <span>1000</span></li>
                                                            <li>Keywords Available: <span>5</span></li>
                                                            <li>MLPs Available: <span>5</span></li>
                                                            <li>Short URLs Available <span>5</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="wrap-buycredit">
                                                    <div class="row">
                                                        <div class="col-lg-8">
                                                            <div class="row">
                                                                <div class="col-xs-7">
                                                                    <div class="form-group">
                                                                        <label>Number of Credits <span>*</span></label>
                                                                        <input type="text" class="form-control amount-input" placeholder="">
                                                                        <label class="amount">Amount: <span class="text-green">$ 5.04</span></label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-5">
                                                                    <span class="amountx">x 2.7 Cents</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <a href="#" class="btn newbtn green-gd btt-calculate">calculate</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="wrap-buycredit">
                                                    <div class="row">
                                                        <div class="col-lg-8">
                                                            <div class="row">
                                                                <div class="col-xs-7">
                                                                    <div class="form-group">
                                                                        <label>Number of Keywords <span>*</span></label>
                                                                        <input type="text" class="form-control amount-input" placeholder="">
                                                                        <label class="amount">Amount: <span class="text-green">$ 25</span></label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-5">
                                                                    <span class="amountx">x 5$</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <a href="##" class="btn newbtn green-gd btt-calculate">calculate</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="tab-invite tabbable tabbable-tabdrop">
                                                    <ul class="nav nav-tabs tab-invite-ul">
                                                        <li class="active">
                                                            <a href="##invitetab1" data-toggle="tab">Refer via E-mail</a>
                                                        </li>
                                                        <li>
                                                            <a href="##invitetab2" data-toggle="tab">Refer via Social Media</a>
                                                        </li>
                                                    </ul>
                                                    <div class="tab-content tab-invite-content">
                                                        <div class="tab-pane active" id="invitetab1">
                                                            <div class="form-group">
                                                                <label for="">Email</label>
                                                                <div class="row row-small">
                                                                    <div class="col-xs-9 col-lg-10">
                                                                        <input type="text" class="form-control" />
                                                                    </div>
                                                                    <div class="col-xs-3 col-lg-2">
                                                                        <a href="##" class="btn btn-block green-gd">add</a>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="block">Import Contacts</label>
                                                                <a href="##" class="btt-invite-email active">
                                                                    <img class="img-responsive" src="../assets/layouts/layout4/img/icon-gmail.png" alt="">
                                                                </a>
                                                                <a href="##" class="btt-invite-email">
                                                                    <img class="img-responsive" src="../assets/layouts/layout4/img/icon-outlook.png" alt="">
                                                                </a>
                                                                <a href="##" class="btt-invite-email">
                                                                    <img class="img-responsive" src="../assets/layouts/layout4/img/icon-yahoo.png" alt="">
                                                                </a>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="block">Selected Emails</label>
                                                            </div>

                                                            <div class="holder-friend"></div>

                                                            <div class="dv-send-invite">
                                                                <button type="submit" class="btn newbtn green-gd">Send Invite</button>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="invitetab2">
                                                            <div class="form-group">
                                                                <label class="block">Share</label>
                                                                <a href="##" class="invite-social-ic fb">
                                                                    <i class="fa fa-facebook-square"></i>
                                                                </a>
                                                                <a href="##" class="invite-social-ic tw">
                                                                    <i class="fa fa-facebook-square"></i>
                                                                </a>
                                                                <a href="##" class="invite-social-ic gp">
                                                                    <i class="fa fa-google-plus-square"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul> --->
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <!--- Cardholder details included by cardholder_details.cfm --->
                                    <cfinclude template="../views/commons/cardholder_details.cfm">
                                    <div class="form-actions">
                                        <button type="submit" class="btn newbtn green-gd btn-update-profile simon-btn-plan mlp-action">UPDATE PAYMENT INFO</button>
                                        <button type="button" class="btn newbtn green-cancel  simon-btn-plan mlp-action" href="javascript:history.go(-1)">CANCEL</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>

            <cfinclude template="../views/commons/sireplandetails.cfm">

            <cfparam name="variables._title" default="My Plan - Sire">

            <cfinclude template="../views/layouts/master.cfm">

            <cfinclude template="../views/commons/what-mlp-keyword.cfm">

            <script type="text/javascript">
            jQuery(document).ready(function($) {
                function loadCountry(){
                    if(country == '') country = "US";
                    $('#country').val(country);
                }
                loadCountry();
            });

            </script>
