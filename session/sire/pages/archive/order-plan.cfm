<cfparam name="url.plan" default="0">

<cfquery name="checkExistPlan" datasource="#Session.DBSourceREAD#">
	SELECT PlanId_int,Order_int FROM simplebilling.plans WHERE Status_int = 1 AND PlanId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.plan#"> 
</cfquery>
<cfif checkExistPlan.RecordCount EQ 0>
	<cflocation url="/plans-pricing" addtoken="false">
</cfif>

<cfinvoke method="GetUserPlan" component="session/sire/models/cfc/order_plan" returnvariable="getUserPlan">
</cfinvoke>

<cfif getUserPlan.PLANORDER GTE checkExistPlan.Order_int >
	<cflocation url="/plans-pricing" addtoken="false">
</cfif>



<cfquery name="getPlans" datasource="#Session.DBSourceREAD#">
	SELECT * FROM simplebilling.plans 
	WHERE 
		Status_int = 1 AND  Order_int > <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getUserPlan.PLANORDER#"> 
	ORDER BY Order_int  
</cfquery>


<!---<cfinvoke component="public.sire.models.cfc.order_plan" method="GetOrderPlan" returnvariable="price2">
	<cfinvokeargument name="plan" value="2">
</cfinvoke>
<cfinvoke component="public.sire.models.cfc.order_plan" method="GetOrderPlan" returnvariable="price3">
	<cfinvokeargument name="plan" value="3">
</cfinvoke>
--->

<cfinvoke component="public.sire.models.cfc.order_plan" method="GetOrderPlan" returnvariable="price">
	<cfinvokeargument name="plan" value="#url.plan#">
</cfinvoke>

<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke>

<cfset BuyKeywordNumber = 0>
<!--- When upgrate plan get plan keyword limit number https://setaintl2008.atlassian.net/browse/SIRE-965 --->

<cfset TotalKeyword = getUserPlan.PLANKEYWORDSLIMITNUMBER + getUserPlan.KEYWORDPURCHASED > 

<cfif TotalKeyword GT price.KEYWORDSLIMITNUMBER>
  <cfset BuyKeywordNumber = (TotalKeyword - price.KEYWORDSLIMITNUMBER)>
</cfif>


<cfset priceBuyKeywordNumber = (BuyKeywordNumber * getUserPlan.PRICEKEYWORDAFTER)>
<cfset totalAMOUNT = (price.AMOUNT + BuyKeywordNumber * getUserPlan.PRICEKEYWORDAFTER)>

<!--- RETRIVE CUSTOMER DATA --->
<cfset firstName =''>
<cfset lastName =''>
<cfset emailAddress =''>
<cfset primaryPaymentMethodId = -1>
<cfset expirationDate =''>
<cfset maskedNumber =''>
<cfset line1 =''>
<cfset city =''>
<cfset state =''>
<cfset zip =''>
<cfset country =''> 
<cfset phone =''>
<cfset hidden_card_form =''>
<cfset disabled_field =''>
<cfset cvv =''>
<cfset paymentType = 'card'>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/credit-pages.css">
</cfinvoke>


<main class="container-fluid page credit-pages">
    <cfinclude template="../views/commons/credits_available.cfm">
    <!--- IF USER HAS DOWNGRADE -> must pay before buy new keyword --->
    <cfif getUserPlan.PLANEXPIRED EQ 2 >
        <cflocation url="#extendPlanUrl#" addtoken="false">
    </cfif>
    
    <section class="row bg-white">
        <div class="content-header">
            <div class="row">

                <div class="col-sm-4 col-lg-3 col-md-4 col-xs-12 content-title">
                    <cfinclude template="../views/commons/welcome.cfm">
                </div>
                
                <div class="col-sm-8 col-lg-9 col-md-8 col-xs-12 content-menu">
                    <a href="/session/sire/pages/my-plan"><span class="icon-my-plan-unactive"></span><span>My Plan</span></a>
                    <a class="active"><span class="icon-upgrade-plan"></span><span>Upgrade Plan</span></a>
                    <a href="/session/sire/pages/buy-sms" title="" class=""><span class="icon-buy-credits-unactive"></span><span>Buy Credit</span></a>

                    <cfif RetUserPlan.PLANEXPIRED GTE 2 && RetUserPlan.KEYWORDBUYAFTEREXPRIED GT 0  >
                      <a href="javascript:void(0)" class="icon-disable" title="Please pay for your keywords"><span class="icon icon-buy-keyword-unactive"></span><span>Buy Keyword</span></a> 
                    <cfelse>  
                      <a href="/session/sire/pages/buy-keyword" title="" class=""><span class="icon-buy-keyword-unactive"></span><span>Buy Keyword</span></a>
                    </cfif>

                    <a href="/session/sire/pages/invite-friend" class="unactive"><span class="icon-invite-friends-unactive"></span><span>Invite a friend</span></a>
                </div>
            </div>
        </div>
        <hr class="hrt0">

    	<cfinclude template="../configs/credits.cfm">

      <cfinvoke component="session.sire.models.cfc.order_plan" method="checkUserPlanExpried" returnvariable="checkUserPlan">
        <cfinvokeargument name="userId" value="#SESSION.USERID#">
      </cfinvoke>

      <cfif checkUserPlan.RXRESULTCODE GT 0> <!--- IF USER PLAN EXPIRED REDIRECT USER TO MY PLAN PAGE --->
        <cflocation url="/session/sire/pages/my-plan" addtoken="false">
      <cfelse>
        <!---
          <cfinvoke component="session.sire.models.cfc.billing" method="getUserAuthenInfo" returnvariable="GetUserAuthen"></cfinvoke> 
          --->

          <!--- <cfif GetUserAuthen.RXRESULTCODE EQ 1> --->
           <cfinvoke component="session.sire.models.cfc.billing" method="RetrieveCustomer" returnvariable="RetCustomerInfo"></cfinvoke>  

            <cfif RetCustomerInfo.RXRESULTCODE EQ 1>
              <cfset firstName = RetCustomerInfo.CUSTOMERINFO.firstName>
              <cfset lastName = RetCustomerInfo.CUSTOMERINFO.lastName>
              <cfset emailAddress = RetCustomerInfo.CUSTOMERINFO.emailAddress>
              <cfset primaryPaymentMethodId = 0> <!--- do not print --->
              <cfset expirationDate = RetCustomerInfo.CUSTOMERINFO.expirationDate>
              <cfset maskedNumber = RetCustomerInfo.CUSTOMERINFO.maskedNumber>
              <cfset line1 = RetCustomerInfo.CUSTOMERINFO.line1>
              <cfset city = RetCustomerInfo.CUSTOMERINFO.city>
              <cfset state = RetCustomerInfo.CUSTOMERINFO.state>
              <cfset zip = RetCustomerInfo.CUSTOMERINFO.zip>
              <cfset country = RetCustomerInfo.CUSTOMERINFO.country> 
              <cfset phone = RetCustomerInfo.CUSTOMERINFO.phone>
              <cfset hidden_card_form = 'display:none'>
              <cfset disabled_field ='disabled'>
              <cfset paymentType = RetCustomerInfo.CUSTOMERINFO.paymentType>
              <cfset cvv =RetCustomerInfo.CUSTOMERINFO.cvv>
            <cfelseif RetCustomerInfo.RXRESULTCODE EQ -1>
              <cfoutput> Connection to Secunet Time Out</cfoutput>
              <cfexit>
            </cfif>
          <!---
          </cfif>
          --->
        <cfoutput>
        <form id="buy_credits" class="container-fluid content-body" action="/session/sire/models/cfm/order-plan.cfm">
        <input type="hidden" id="plan" name="plan" value="#url.plan#">
        <input type="hidden" id="actionStatus" value="">
        <input type="hidden" id="amount" name="amount" value="#totalAMOUNT#">
        <input type="hidden" value="#userInfo.FULLEMAIL#" id="h_email" name="h_email">
          
        <div id ="step1">
        <div class="row">
            <div class="col-md-6">
                <div class="row heading">
                  <div class="col-sm-12 heading-title">Secure Payment<hr></div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 secure-payment">
                      <div class="pull-left">Currency:</div>
                      <div class="pull-right">$ US Dollars</div>
                    </div>
                    <div class="col-xs-12 col-sm-12 secure-payment">
                      <div class="pull-left">Select Your Plan:</div>
                      <div class="pull-right number-credits">
                        <select class="form-control" id="order-plan">
                          <cfloop query="getPlans">
                          	<option value="#getPlans.PlanId_int#" <cfif url.plan EQUAL getPlans.PlanId_int>selected</cfif>>#getPlans.PlanName_vch#</option>
                          </cfloop> 
                        </select>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 secure-payment">
                      <div class="pull-left">First Credits Include:</div>
                      <div class="pull-right amount" id="FirstSMSInclude"><cfoutput>#NumberFormat(price.FIRSTSMSINCLUDED,',')#</cfoutput></div>
                    </div>
                    <div class="col-xs-12 col-sm-12 secure-payment">
                      <div class="pull-left">Number of Plan Keyword:</div>
                      <div class="pull-right amount" id="NumberKeyword"><cfoutput>#price.KEYWORDSLIMITNUMBER#</cfoutput></div>
                    </div>
                    <div class="col-xs-12 col-sm-12 secure-payment">
                      <div class="pull-left">Monthly Keyword Fee: <a href="##" data-toggle="modal" data-target="##MonthlyKeywordFeeModal"><img src="/session/sire/images/help-small.png"/></a></div>
                      <div class="pull-right amount" id="BuyKeywordNumber"><cfoutput>$#priceBuyKeywordNumber#</cfoutput></div>
                    </div>
                    
                    <div class="col-xs-12 col-sm-12 secure-payment">
                      <div class="pull-left">Monthly Plan Price:</div>
                      <div class="pull-right amount" id="MonthlyAmount">$<cfoutput>#price.AMOUNT#</cfoutput></div>
                    </div>
                    
                    <div class="col-xs-12 col-sm-12 secure-payment">
                      <div class="pull-left">Monthly Amount: <a href="##" data-toggle="modal" data-target="##MonthlyAmountModal"><img src="/session/sire/images/help-small.png"/></a></div>
                      <div class="pull-right amount" id="MonthlyAmountTotal" style="color:red">$<cfoutput>#totalAMOUNT#</cfoutput></div>
                    </div>
                    
                    
                    <div class="col-xs-12 col-sm-12 small-info" style="color:red">
                      For consistency, your billing date will be the same day every month.  If you start your plan on the 29th, 30th or 31st then your monthly billing date will fall on the 28th.
                    </div>
                    <div class="col-xs-12 col-sm-12 small-info">
                      Please review your purchase details, then select a payment method to continue
                    </div>
                  </div>
                    <cfinclude template="../views/commons/payment/step1.cfm">
                </div>
                <cfinclude template="../views/commons/payment/step2.cfm">         
            <cfinclude template="../views/commons/payment/step3_order_plan.cfm">
          </form>
          <div class="row">
            <div class="col-md-6">
              <div class="row">
                    <div class="col-sm-12">
                          <div class="pull-left worldpay-logo"><img src="../images/powered-worldpay.png"></div>
                          <div class="pull-right worldpay-help">For help with your payment visit: <a target="_blank" href="http://www.worldpay.com/uk/support">WorldPay Help</a></div>
                    </div>
               </div>             
          </div>              
        </div>
      </cfoutput>
      </cfif>
 
    </section>
</main>

<!-- Modal -->
<div class="modal fade" id="scModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Security Code</h4>
      </div>
      <div class="modal-body text-center">
			<img src="/session/sire/images/securecode.gif"/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="MonthlyKeywordFeeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Monthly Keyword Fee</h4>
      </div>
      <div class="modal-body">
			<b>Monthly Keyword Fee = Buy Keyword x 5 (Keyword Price)</b>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="MonthlyAmountModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Monthly Amount</h4>
      </div>
      <div class="modal-body">
			<b>Monthly Amount = Monthly Plan Price + Monthly Keyword Fee</b>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/buy_credits_payment.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/site/order_plan.js">
</cfinvoke>

<script type="text/javascript">
 	var country = '<cfoutput>#country#</cfoutput>';
</script>

<cfparam name="variables._title" default="Upgrade Plan - Sire">
<cfinclude template="../views/layouts/main.cfm">