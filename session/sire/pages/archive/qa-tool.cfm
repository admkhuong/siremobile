<cfparam name="shortCode" default="1244">
<cfparam name="carrier" default="Sprint">
<cfparam name="sessionId" default="1" >

<cfset SERVERSOCKETON = 0>

<cfinvoke component="session.sire.models.cfc.control-point" method="GetAssignedShortCode" returnvariable="RetVarGetAssignedShortCode"></cfinvoke>
<cfset DefaultShortCode = RetVarGetAssignedShortCode.SHORTCODE />	
							

<cfif serverSocketOn EQ 1>
	<script src="<cfoutput>#serverSocket#:#serverSocketPort#</cfoutput>/socket.io/socket.io.js"></script>
</cfif>
<script>
	var client = client || {};
</script>
<cfinclude template="/session/cfc/csc/constants.cfm">
<cfinclude template="/public/paths.cfm" >


<!---  ***JLP This broke on my local server - I turned off for now--->
<cfif serverSocketOn EQ 1>
	<cfinclude template="inc_client.cfm" >
</cfif>
<!--- Locally used javascript only --->
<cfoutput>

		<script src="/public/sire/js/jquery-2.1.4.js"></script>
        
         <!---Tool tip tool - http://qtip2.com/guides --->
		<!---<link type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/js/qtip/jquery.qtip.css" />
		<scrit type="text/javascript" src="#rootUrl#/#PublicPath#/js/qtip/jquery.qtip.js"></script>--->    
         
        <style>
        	/*@import url('#rootUrl#/#PublicPath#/css/default/default.css') all;
			@import url('#rootUrl#/#PublicPath#/css/administrator/popup.css') all;
			@import url('#rootUrl#/#PublicPath#/css/mcid/campaignicons.css') all;
			@import url('#rootUrl#/#PublicPath#/css/mcid/override.css') all;
			@import url('#rootUrl#/#PublicPath#/css/mcid/scriptbuilder.css') all;
			@import url('#rootUrl#/#PublicPath#/css/mcid/poolballs/ivr.css') all;
			@import url('#rootUrl#/#PublicPath#/css/jquery.jgrowl.css') all;
			@import url('#rootUrl#/#PublicPath#/css/jquery.selectbox.css') all;
			@import url('#rootUrl#/#PublicPath#/css/metro.css') all;
			@import url('#rootUrl#/#PublicPath#/css/surveyprint.css') print;*/
			@import url('/session/sire/css/qa-tool.css') all;
		
			
       </style>     

 		        
  	
</cfoutput>

<style>

.bubble
	{
		word-break: break-word;
	}



</style>

<!---
<script language="javascript">
	$('#mainTitleText').html('<cfoutput>SMS <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> QA SMS</cfoutput>');
	$('#subTitleText').html('QA Testing Tool');	
</script>--->

<!--- https://github.com/javierjulio/textarea-auto-expand/blob/master/examples/jquery.textarea_auto_expand.js --->
<!--- http://jsfiddle.net/ThinkingStiff/mek5Z/ --->

<script src="/public/js/common.js"></script>
<script type="text/javascript" language="javascript">
var dialogTitle = "QA Tool";
<!--- ***JLP This broke on my local server - I turned off for now--->
<cfif serverSocketOn EQ 1>
	!function(window, $, client) {
				client.data.username = "User";
				client.data.contactstring = $("#inpContactString").val();
				client.data.userid = "";
				client.data.usertype = 2;
				client.data.shortcode = $("#inpSMSToAddress").val();
				client.data.sessionid = "";
				client.data.carrier ='';
			}
	(window, window.jQuery, window.client);
</cfif>
	
		
	<!--- Track time traveled so far so display gives user an semi-accurate feedback of which message when. --->
	var inpTimeTraveledSoFarInMinutes = 0;
	var AllowBlanks = 0;
	var LastResponseTypeLocal = '';
	
	$(function() {	

		<cfset inpFormData = StructNew() />
		<cfset inpFormData.inpFirstName = "Lee" />
		<cfset inpFormData.inpLastName = "Peterson" />
		<cfset inpFormData.inpRepName = "Mary" />
		<cfset inpFormData.inpOneTimePassword = "85490527" />
		<cfset inpFormData.inpBatchIdAppt = "7451284" />
		<cfset inpFormData.inpContactStringAppt = "9493945710" />
		<cfset inpFormData.inpContactTypeIdAppt = "1" />
		<cfset inpFormData.inpStart = "2016-10-06 13:00" />
		<cfset inpFormData.inpConfirm = "1" />
		<cfset inpFormData.inpCurrEMail = "someone@somewhere.com" />
		<cfset inpFormData.inpAsOfDate = "#LSDateFormat(now(), 'yyyy-mm-dd')#" />
		<cfset inpFormData.inpDeductibleInMet = "$350" />
		<cfset inpFormData.inpDeductibleIn = "$500" />
		<cfset inpFormData.inpDeductibleOutMet = "$1350" />
		<cfset inpFormData.inpDeductibleOut = "$1500" />
		<cfset inpFormData.inpA = "Scottsdale Golf: To authorize transaction reply with: 244990, Total $9.00 " />
		<cfset inpFormData.inpB = "Msg freq depends on user. HELP for help or STOP to opt-out. Msg&Data rates may apply." />
		<cfset inpFormData.inpId = "351674c74846919ec735074" />
		<cfset inpFormData.inpDomain = "ebmui.com" />
		<cfset inpFormData.inpPath = "ebmresponse/capture/samples/logapiresponse" />
		
		
		<cfoutput> 
      		var inpFormDataJSON = '#SerializeJSON(inpFormData)#';
		</cfoutput> 
		 
		$('#inpJSON').html(inpFormDataJSON);
		
		<!--- Turns off caching of AJAX requests --->
		$.ajaxSetup({ cache: false });
		
		//$('#SMSTextInputArea').autogrow();
		
		<!--- Resize history screen based on size of text input --->
		$('#SMSTextInputArea').resize(function() {
			
		  	$("#SMSHistoryScreenArea").height($('#PhoneScreenArea').height() - $('#SMSTextInputAreaContainer').height() - 31 - $('#SMSToAddress').height());
			$("#SMSHistoryScreenArea").animate({ scrollTop: $('#SMSHistoryScreenArea')[0].scrollHeight}, 1000);
			
		});

		<!--- force resize on page load --->
		$('#SMSTextInputArea').resize();

		$('#STARTCP').click(function(){
			SetStartCP();
		});
		
		<!--- Mirror Responses to physical device --->
		$('#STARTPhysicalDevice').click(function(){
			
			if($('#inpContactString').val().length == 0 )
			{				
				//bootbox.alert(" Device Address: Address can not be blank.", function(result) { } );
				bootbox.dialog({
					title: dialogTitle,
			        message: "Device Address: Address can not be blank.",
			        buttons: {
			            success: {
			                label: "Ok",
			                className: "btn btn-medium btn-success-custom",
			                callback: function() {
			
			                }
			            }
			        }
			    });		
				return false;						
			}else {
				
				validatePhone = 0;
				$.ajax({
					async: false,
					type: "POST",
					url: '/public/sire/models/cfc/validate.cfc?method=ValidatePhoneNumber&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					data: {PhoneNumber:$('#inpContactString').val()},
					beforeSend: function( xhr ) {
							
					},					  
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						
					},					  
					success: function(data)	{
						if(data.RXRESULTCODE == -1) {
							 validatePhone = 1;
						}
					}
						
				});
				if (validatePhone == 1) {
					bootbox.dialog({
								title: dialogTitle,
						        message: "Phone Number is not a valid US telephone number",
						        buttons: {
						            success: {
						                label: "Ok",
						                className: "btn btn-medium btn-success-custom",
						                callback: function() {
						
						                }
						            }
						        }
						    });
					return false;
				}
			}	
			
			var MSTextInputAreaValue = $('#SMSTextInputArea').val()
			<!--- Warn if Blank ?--->	
			if($('#SMSTextInputArea').val().length == 0 )
					MSTextInputAreaValue = "Interval Trigger";
								
			if($('#inpSMSToAddress').val().length == 0 )
			{				
				//bootbox.alert("SMS TO: address has not been entered.\n"  + "SMS TO: address can not be blank." + "\n", function(result) { } );
				bootbox.dialog({
					title: dialogTitle,
			        message: "SMS TO: address has not been entered.\n"  + "SMS TO: address can not be blank." + "\n",
			        buttons: {
			            success: {
			                label: "Ok",
			                className: "btn btn-medium btn-success-custom",
			                callback: function() {
			
			                }
			            }
			        }
			    });												
				return false;						
			}
							
			<!--- Post text that is sent from me class --->
			
			$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble me">' + MSTextInputAreaValue + '</div>');
			$("#SMSHistoryScreenArea").stop(true, true).animate({ scrollTop: $('#SMSHistoryScreenArea')[0].scrollHeight}, 1000);
			
			
		<!---	if($('#SMSTextInputArea').val().length == 0 )						
				SendToPhysicalDevice($('#inpSMSToAddress').val(), 'IRE - INTERVAL - Time Machine', $('#inpContactString').val(), 1 );
			else
				SendToPhysicalDevice($('#inpSMSToAddress').val(), $('#SMSTextInputArea').val(), $('#inpContactString').val(), 1 );--->
			
			
			SendToPhysicalDevice($('#inpSMSToAddress').val(), $('#SMSTextInputArea').val(), $('#inpContactString').val(), 1 );
		});
		
		$("#CPOptionsLink").click(function(){
			
			/*$('#StartCPContainer').toggle('slow', function() {
				<!--- Animation complete. --->
			  });*/
			  bootbox.dialog({
					title: dialogTitle,
			        message: "Coming soon...",
			        buttons: {
			            success: {
			                label: "Ok",
			                className: "btn btn-medium btn-success-custom",
			                callback: function() {
			
			                }
			            }
			        }
			    });
  
		});	
		
		$("#JSONOptionsLink").click(function(){
			
			 $('#JSONOptionsContainer').toggle('slow', function() {
				<!--- Animation complete. --->
			  });
			  /*
			 bootbox.dialog({
					title: dialogTitle,
			        message: "Coming soon...",
			        buttons: {
			            success: {
			                label: "Ok",
			                className: "btn btn-medium btn-success-custom",
			                callback: function() {
			
			                }
			            }
			        }
			    });*/
  
		});		
		
		$("#PhysicalDeviceOptionsLink").click(function(){
			
			$('#StartPhysicalDeviceContainer').toggle('slow', function() {
				<!--- Animation complete. --->
			  });
			  /*
			 bootbox.dialog({
					title: dialogTitle,
			        message: "Coming soon...",
			        buttons: {
			            success: {
			                label: "Ok",
			                className: "btn btn-medium btn-success-custom",
			                callback: function() {
			
			                }
			            }
			        }
			    }); */
  
		});		
						
		
		$("#TimeMachineOptionsLink").click(function(){
			
			$('#TimeMachineContainer').toggle('slow', function() {
				<!--- Animation complete. --->
			  });
			/*
			bootbox.dialog({
					title: dialogTitle,
			        message: "Coming soon...",
			        buttons: {
			            success: {
			                label: "Ok",
			                className: "btn btn-medium btn-success-custom",
			                callback: function() {
			
			                }
			            }
			        }
			    });*/
  
		});		
		

		<!--- Action to perform on SMS send--->
		$('#SMSSend').click(function(){

			var inpContactString = $('#inpContactString').val(); 
			var letters = /^[0-9]+$/;
			var validatePhone = 0;
			if(inpContactString.length == 0 )
			{
				
				//bootbox.alert("Device Address: Address can not be blank." + "\n", function(result) { } );	
				bootbox.dialog({
					title: dialogTitle,
			        message: "Device Address: Address can not be blank." + "\n",
			        buttons: {
			            success: {
			                label: "Ok",
			                className: "btn btn-medium btn-success-custom",
			                callback: function() {
			
			                }
			            }
			        }
			    });									
				return false;						
			} else {
				
				validatePhone = 0;
				$.ajax({
					async: false,
					type: "POST",
					url: '/public/sire/models/cfc/validate.cfc?method=ValidatePhoneNumber&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					data: {PhoneNumber:$('#inpContactString').val()},
					beforeSend: function( xhr ) {
							
					},					  
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						
					},					  
					success: function(data)	{
						if(data.RXRESULTCODE == -1) {
							 validatePhone = 1;
						}
					}
						
				});
				if (validatePhone == 1) {
					bootbox.dialog({
								title: dialogTitle,
						        message: "Phone Number is not a valid US telephone number",
						        buttons: {
						            success: {
						                label: "Ok",
						                className: "btn btn-medium btn-success-custom",
						                callback: function() {
						
						                }
						            }
						        }
						    });
					return false;
				}
			}	
			
			var SMSTextInputArea = $('#SMSTextInputArea').val();
			
			if(AllowBlanks == 0)
				if(SMSTextInputArea.length == 0 )
				{
					
					/*bootbox.confirm("Text to send has not been entered.\n"  + "Text to send can not normally be blank. Hit OK if you want to allow future blanks to be sent in this session." + "\n", function(result) { 
						if(result)
						{
							AllowBlanks = 1
							return;
						}
						else
						{
							return;
						}					
						
					});	*/
					
					bootbox.dialog({
				        message: "Text to send has not been entered.\n"  + "Text to send can not normally be blank. Hit OK if you want to allow future blanks to be sent in this session." + "\n",
				        title: dialogTitle,
				        buttons: {
				            success: {
				                label: "OK",
				                className: "btn btn-medium btn-success-custom",
				                callback: function(result) {
				                    if(result)
									{
										AllowBlanks = 1
										return;
									}
									else
									{
										return;
									}					
									
				                }
				            },
				            cancel: {
				                label: "Cancel",
				                className: "btn btn-medium btn-back-custom",
				                callback: function() {
				
				                }
				            },
				        }
				    });
					
					
														
					return false;						
				}
			
			var inpSMSToAddress = $('#inpSMSToAddress').val();		
			if(inpSMSToAddress.length == 0 )
			{
				
				//bootbox.alert("SMS TO: address has not been entered.\n"  + "SMS TO: address can not be blank." + "\n", function(result) { } );
				bootbox.dialog({
					title: dialogTitle,
			        message: "SMS TO: address has not been entered.\n"  + "SMS TO: address can not be blank." + "\n",
			        buttons: {
			            success: {
			                label: "Ok",
			                className: "btn btn-medium btn-success-custom",
			                callback: function() {
			
			                }
			            }
			        }
			    });														
				return false;						
			}			
			<!--- Post text that is sent from me class --->
			if(SMSTextInputArea == "")
				$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble me">' + '&nbsp;' + '</div>');
			else
				$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble me">' + SMSTextInputArea + '</div>');
					
			$("#SMSHistoryScreenArea").stop(true, true).animate({ scrollTop: $('#SMSHistoryScreenArea')[0].scrollHeight}, 1000);
						
			//get response and call socket: send message to AAU system			
			GetResponseSimple(inpSMSToAddress, SMSTextInputArea, inpContactString, 0 );
			
		<!---	
			var t=setTimeout(function(){
											<!--- Go get keyword response if any...--->
											$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble you">' + 'Keyword response goes here!' + '</div>');
											$("#SMSHistoryScreenArea").animate({ scrollTop: $('#SMSHistoryScreenArea')[0].scrollHeight}, 1000);
			
										},1500)--->
			
			
		});	
		
		$('#TIMEMACHINE').click(function(){			
				
			if($('#inpContactString').val().length == 0 )
			{
				
				//bootbox.alert("Device Address: Address can not be blank." + "\n", function(result) { } );	
				bootbox.dialog({
					title: dialogTitle,
			        message: "Device Address: Address can not be blank." + "\n",
			        buttons: {
			            success: {
			                label: "Ok",
			                className: "btn btn-medium btn-success-custom",
			                callback: function() {
			
			                }
			            }
			        }
			    });										
				return false;						
			}	else {
				
				validatePhone = 0;
				$.ajax({
					async: false,
					type: "POST",
					url: '/public/sire/models/cfc/validate.cfc?method=ValidatePhoneNumber&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					data: {PhoneNumber:$('#inpContactString').val()},
					beforeSend: function( xhr ) {
							
					},					  
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						
					},					  
					success: function(data)	{
						if(data.RXRESULTCODE == -1) {
							 validatePhone = 1;
						}
					}
						
				});
				if (validatePhone == 1) {
					bootbox.dialog({
								title: dialogTitle,
						        message: "Phone Number is not a valid US telephone number",
						        buttons: {
						            success: {
						                label: "Ok",
						                className: "btn btn-medium btn-success-custom",
						                callback: function() {
						
						                }
						            }
						        }
						    });
					return false;
				}
			}		
				
				
			if($('#inpSMSToAddress').val().length == 0 )
			{
				
				//bootbox.alert("SMS TO: address has not been entered.\n"  + "SMS TO: address can not be blank." + "\n", function(result) { } );
				bootbox.dialog({
					title: dialogTitle,
			        message: "SMS TO: address has not been entered.\n"  + "SMS TO: address can not be blank.",
			        buttons: {
			            success: {
			                label: "Ok",
			                className: "btn btn-medium btn-success-custom",
			                callback: function() {
			
			                }
			            }
			        }
			    });										
				return false;						
			}	
			
			TimeMachineProcess($('#inpSMSToAddress').val(), '', $('#inpContactString').val());
			
			
		});	
		
		$('#TIMEMACHINECHECK').click(function(){			
				
			if($('#inpContactString').val().length == 0 )
			{
				
				//bootbox.alert("Device Address: Address can not be blank." + "\n", function(result) { } );
				bootbox.dialog({
					title: dialogTitle,
			        message:"Device Address: Address can not be blank." ,
			        buttons: {
			            success: {
			                label: "Ok",
			                className: "btn btn-medium btn-success-custom",
			                callback: function() {
			
			                }
			            }
			        }
			    });											
				return false;						
			}	else {
				
				validatePhone = 0;
				$.ajax({
					async: false,
					type: "POST",
					url: '/public/sire/models/cfc/validate.cfc?method=ValidatePhoneNumber&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					data: {PhoneNumber:$('#inpContactString').val()},
					beforeSend: function( xhr ) {
							
					},					  
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						
					},					  
					success: function(data)	{
						if(data.RXRESULTCODE == -1) {
							 validatePhone = 1;
						}
					}
						
				});
				if (validatePhone == 1) {
					bootbox.dialog({
								title: dialogTitle,
						        message: "Phone Number is not a valid US telephone number",
						        buttons: {
						            success: {
						                label: "Ok",
						                className: "btn btn-medium btn-success-custom",
						                callback: function() {
						
						                }
						            }
						        }
						    });
					return false;
				}
			}	
				
				
			if($('#inpSMSToAddress').val().length == 0 )
			{
				
				//bootbox.alert("SMS TO: address has not been entered.\n"  + "SMS TO: address can not be blank." + "\n", function(result) { } );
				bootbox.dialog({
					title: dialogTitle,
			        message: "SMS TO: address has not been entered.\n"  + "SMS TO: address can not be blank." + "\n" ,
			        buttons: {
			            success: {
			                label: "Ok",
			                className: "btn btn-medium btn-success-custom",
			                callback: function() {
			
			                }
			            }
			        }
			    });									
				return false;						
			}	
			
			CheckNextResponse($('#inpSMSToAddress').val(), '', $('#inpContactString').val());
						
		});	
		
		<!---$('.showToolTip').each(function() {
			 $(this).qtip({
				 content: {
					 text: $(this).next('.tooltiptext')
				 },
				  style: {
						classes: 'qtip-bootstrap'
					}
			 });
		 });	
			 --->

		 CheckNextResponse($('#inpSMSToAddress').val(), '', $('#inpContactString').val());
					
	});
	
	function MidStr(str, start, len)
        /***
                IN: str - the string we are LEFTing
                    start - our string's starting position (0 based!!)
                    len - how many characters from start we want to get

                RETVAL: The substring from start to start+len
        ***/
        {
                // Make sure start and len are within proper bounds
                if (start < 0 || len < 0) return "";

                var iEnd, iLen = String(str).length;
                if (start + len > iLen)
                        iEnd = iLen;
                else
                        iEnd = start + len;

                return String(str).substring(start,iEnd);
        }
	
	<!---Browser is caching identical ajax request	force unique id for keyword on statement re-push --->
	var inpflag = 0;
	var MaxStatements = 20;
	var TIMEZONEOFFSETPST = 0;
	
	function GetResponseSimple(inpShortCode, inpKeyword, inpContactString, inpOverRideInterval) {
			
							
		$.ajax({
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=GetSMSResponse&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		type: 'POST',
		dataType: 'json',
		data:  	{ 
					inpKeyword : inpKeyword,
					inpShortCode : inpShortCode,
					inpContactString : inpContactString,
					inpNewLine : "<BR>",
					inpOverRideInterval : inpOverRideInterval,
					inpFormDataJSON : $('#inpJSON').val(),
					inpQATool : 1					
				},					  
		error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
		success:		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
				<!--- Get row 1 of results if exisits--->
									
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(d.RXRESULTCODE) != "undefined")
					{							
						CurrRXResultCode = d.RXRESULTCODE;	
						
						if(CurrRXResultCode > 0)
						{
							
							if(d.RESPONSE.length > 0)
							{	
							
								var ConcatenateOffFlag = 0;
							
								if(ConcatenateOffFlag == 1)	
								{						
									var CurrPos = 0;
									var AmountToGrab = 160;								
									var BuffStr = d.RESPONSE;		
									
									<!---console.log('{' + BuffStr.substring(AmountToGrab-2,AmountToGrab-1) + '}');--->
									<!--- Break message on space if within last 60 characters --->
									while(BuffStr.substring(AmountToGrab-2,AmountToGrab-1) != " " && AmountToGrab > 100 && BuffStr.substring(AmountToGrab-2,AmountToGrab-1) != "") 
									{
										AmountToGrab = AmountToGrab - 1;
										<!---console.log('{' + BuffStr.substring(AmountToGrab-2,AmountToGrab-1) + '}');	--->								
									}
									
									<!--- Move back to last space--->
									AmountToGrab = AmountToGrab - 1;
									
									<!---console.log('AmountToGrab=' + AmountToGrab);--->
									var BuffStrOut = MidStr(BuffStr, CurrPos, AmountToGrab);
																		
									<!---console.log(BuffStrOut);--->
									
									$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble you">' + escapeHtml(BuffStrOut) + '</div>');								
									
									CurrPos = CurrPos + AmountToGrab;
									
									while( BuffStr.length > CurrPos)
									{					
									
										AmountToGrab = 160;
										
										<!---console.log('{' + BuffStr.substring(CurrPos+AmountToGrab-2,CurrPos+AmountToGrab-1) + '}');--->
										<!--- Break message on space if within last 60 characters --->
										while(BuffStr.substring(CurrPos+AmountToGrab-2,CurrPos+AmountToGrab-1) != " " && AmountToGrab > 100 && BuffStr.substring(CurrPos+AmountToGrab-2,CurrPos+AmountToGrab-1) != "")
										{
											AmountToGrab = AmountToGrab - 1;
											<!---console.log('{' + BuffStr.substring(CurrPos+AmountToGrab-2,CurrPos+AmountToGrab-1) + '}');--->
										}
										
										<!--- Move back to last space--->
										AmountToGrab = AmountToGrab -1;
													
										<!---console.log('AmountToGrab=' + AmountToGrab);--->
										BuffStrOut = MidStr(BuffStr, CurrPos, AmountToGrab);
										<!---console.log(BuffStrOut);--->
										CurrPos = CurrPos + AmountToGrab;
										
										$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble you">' + escapeHtml(BuffStrOut) + '</div>');									
									}
								
								}
								else
								{
									$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble you" rel="' + d.RESPONSELENGTH + '">' + escapeHtml(d.RESPONSE) + '</div>');									
									<cfif serverSocketOn EQ 1>
										client.socket.emit('add_notification', 
										{
											member : 'System generated',
											shortcode: $("#inpSMSToAddress").val(),
											userid : client.data.userid,
											usertype : client.data.usertype,
											sessionid : client.data.sessionid,
											carrier: client.data.carrier,
											contactstring : $("#inpContactString").val(),
											message : d.RESPONSE,
											keyword: inpKeyword,
											Campaign: d.BATCHID
										});
									</cfif>
								}
																									
								<!--- Scroll to bottom on new data --->							
								$("#SMSHistoryScreenArea").animate({ scrollTop: $('#SMSHistoryScreenArea')[0].scrollHeight}, 1000);
							}
							
							<!--- Get next Response--->
							if(d.RESPONSETYPE == 'STATEMENT' || d.RESPONSETYPE == 'OPTIN' || d.RESPONSETYPE == 'API' || d.RESPONSETYPE == 'CDF' || d.RESPONSETYPE == 'OTTPOST' || d.RESPONSETYPE == 'RESET')
							{
								EBMText = 'IRE - ' +  d.RESPONSETYPE;
								d.RESPONSETYPE = '';
								
								inpflag++;
								
								if(inpflag < MaxStatements)
									GetResponseSimple(inpShortCode, EBMText, inpContactString, 1); 
								
								<!---var t=setTimeout(function(){
										GetResponseSimple(inpShortCode, inpKeyword, inpContactString, 0);
									},1500)--->
																		
							}
							else if(d.RESPONSETYPE == 'AGENT')
							{
								d.RESPONSETYPE = '';
								
								inpflag++;
								
								<!--- 
									Now what? How do we make this page list for AAU message to the current device address?
									How to ignore messages aau message if device address is changed?
								--->
															
							}
							else if(d.RESPONSETYPE == 'INTERVAL')
							{	
							
								if(d.INTERVALTYPE == 'SECONDS' && parseInt(d.INTERVALVALUE) <= 30 )
								{
								
									if(parseInt(d.INTERVALVALUE) < 5)
										d.INTERVALVALUE = 3;
									
											
									<!---console.log('Settimeout interval');	--->
									setTimeout(function(){ GetResponseSimple(inpShortCode, 'IRE - INTERVAL', inpContactString, 1); }, parseInt(d.INTERVALVALUE)*1000);									
								}
								else
								{										
									QueueNextResponse(inpShortCode, 'IRE - INTERVAL', inpContactString, d.INTERVALSCHEDULED, d.INTERVALEXPIREDNEXTQID, d.IRESESSIONID)
								}
								
								<!--- Update optional interval processing and display --->
								$('#INTERVALSCHEDULED').html(d.INTERVALSCHEDULED + '<BR/>Device Address Time Zone offset from PST is ' + d.TIMEZONEOFFSETPST);
								$('#IntervalOptions').show();
							}
							else
							{
								
								LastResponseTypeLocal = d.RESPONSETYPE;
								
								<!--- Non repeat question type --->
								MaxStatements = inpflag + 20; 
								
							}							
						}
						else
						{
							
						}
					}
					else
					{<!--- Invalid structure returned --->	
						
					}
						   var isTestKeyword = d.ISTESTKEYWORD;
								
								if(isTestKeyword == 1)
								{
									$("#isTestKeyword").val("0");
									$("#isEmitChatContent").val("0");
									$("#Keyword").val(inpKeyword);
								}
								else if(isTestKeyword == -1)
								{
									$("#isTestKeyword").val("1");
								}	
								else
								{
									$("#isTestKeyword").val("");
									$("#isEmitChatContent").val("");
								}
								
								var isEmitChatContent = $("#isEmitChatContent").val();
								isTestKeyword = $("#isTestKeyword").val();
								if(isEmitChatContent == 0 && isTestKeyword == 1 && inpKeyword != "")
								{
									$("#isEmitChatContent").val("1");
									<cfif serverSocketOn EQ 1>
										client.socket.emit('add_notification', 
											{
												member : client.data.username,
												shortcode: inpShortCode,
												userid : client.data.userid,
												usertype : client.data.usertype,
												sessionid : client.data.sessionid,
												carrier: client.data.carrier,
												contactstring : inpContactString,
												message : inpKeyword,
												keyword: $("#Keyword").val(),
												Campaign: d.BATCHID
											});
									</cfif>
											
									$('#SMSTextInputArea').resize();
								}	 		
					$('#SMSTextInputArea').val('');							
			} 		
			
		});
		return false;
	}
	
	function TimeMachineProcess(inpShortCode, inpKeyword, inpContactString) {
	
		MaxStatements = inpflag + 20; 
	
		$.ajax({
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=TimeMachineGetNextResponseQA&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		dataType: 'json',
		data:  	{ 					
					inpContactString : inpContactString,
					inpShortCode : inpShortCode,					
					inpNewLine : "<BR>",
					inpTimeTraveledSoFarInMinutes : inpTimeTraveledSoFarInMinutes
				},					  
		error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
		success:		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
				<!--- Get row 1 of results if exisits--->
									
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(d.RXRESULTCODE) != "undefined")
					{							
						CurrRXResultCode = d.RXRESULTCODE;	
											
						if(CurrRXResultCode > 0 && typeof(d.GETRESPONSE.RESPONSE) != "undefined" )
						{
							if(d.GETRESPONSE.RESPONSE.length > 0 )
							{	
								var ConcatenateOffFlag = 0;
							
								inpTimeTraveledSoFarInMinutes = d.INPTIMETRAVELEDSOFARINMINUTES;
							
								if(ConcatenateOffFlag == 1)	
								{						
									var CurrPos = 0;
									var AmountToGrab = 160;								
									var BuffStr = d.GETRESPONSE.RESPONSE;		
									
									<!---console.log('{' + BuffStr.substring(AmountToGrab-2,AmountToGrab-1) + '}');--->
									<!--- Break message on space if within last 60 characters --->
									while(BuffStr.substring(AmountToGrab-2,AmountToGrab-1) != " " && AmountToGrab > 100 && BuffStr.substring(AmountToGrab-2,AmountToGrab-1) != "") 
									{
										AmountToGrab = AmountToGrab - 1;
										<!---console.log('{' + BuffStr.substring(AmountToGrab-2,AmountToGrab-1) + '}');	--->								
									}
									
									<!--- Move back to last space--->
									AmountToGrab = AmountToGrab - 1;
									
									<!---console.log('AmountToGrab=' + AmountToGrab);--->
									var BuffStrOut = MidStr(BuffStr, CurrPos, AmountToGrab);
																		
									<!---console.log(BuffStrOut);--->
									
									$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="you" style="margin-bottom:0; color:#999; font-size:10px;">' + d.INTERVALSCHEDULED + '</div>');								
									$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble you">' + escapeHtml(BuffStrOut) + '</div>');								
									
									CurrPos = CurrPos + AmountToGrab;
									
									while( BuffStr.length > CurrPos)
									{					
									
										AmountToGrab = 160;
										
										<!---console.log('{' + BuffStr.substring(CurrPos+AmountToGrab-2,CurrPos+AmountToGrab-1) + '}');--->
										<!--- Break message on space if within last 60 characters --->
										while(BuffStr.substring(CurrPos+AmountToGrab-2,CurrPos+AmountToGrab-1) != " " && AmountToGrab > 100 && BuffStr.substring(CurrPos+AmountToGrab-2,CurrPos+AmountToGrab-1) != "")
										{
											AmountToGrab = AmountToGrab - 1;
											<!---console.log('{' + BuffStr.substring(CurrPos+AmountToGrab-2,CurrPos+AmountToGrab-1) + '}');--->
										}
										
										<!--- Move back to last space--->
										AmountToGrab = AmountToGrab -1;
													
										<!---console.log('AmountToGrab=' + AmountToGrab);--->
										BuffStrOut = MidStr(BuffStr, CurrPos, AmountToGrab);
										<!---console.log(BuffStrOut);--->
										CurrPos = CurrPos + AmountToGrab;
										
										$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble you">' + escapeHtml(BuffStrOut) + '</div>');									
									}
								
								}
								else
								{
										$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="you" style="margin-bottom:0; color:#999; font-size:10px;">' + d.INTERVALSCHEDULED + '</div>');
										$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble you" rel="' + d.GETRESPONSE.RESPONSELENGTH + '">' + escapeHtml(d.GETRESPONSE.RESPONSE) + '</div>');
								}
							
								<!--- Scroll to bottom on new data --->							
								$("#SMSHistoryScreenArea").animate({ scrollTop: $('#SMSHistoryScreenArea')[0].scrollHeight}, 1000);
						
							}
														
														
							if(typeof(d.GETRESPONSE.RESPONSETYPE) != "undefined")
							{		
								<!--- Get next Response--->
								if(d.GETRESPONSE.RESPONSETYPE == 'STATEMENT' || d.GETRESPONSE.RESPONSETYPE == 'OPTIN' || d.GETRESPONSE.RESPONSETYPE == 'API' || d.GETRESPONSE.RESPONSETYPE == 'CDF' || d.GETRESPONSE.RESPONSETYPE == 'OTTPOST' || d.GETRESPONSE.RESPONSETYPE == 'RESET')
								{
									
									EBMText = 'IRE - ' +  d.GETRESPONSE.RESPONSETYPE;
									d.GETRESPONSE.RESPONSETYPE = '';
									
																		
									inpflag++;
									
									if(inpflag < MaxStatements)
										GetResponseSimple(inpShortCode, EBMText, inpContactString, 1); 
									
																		
									<!---var t=setTimeout(function(){
											GetResponseSimple(inpShortCode, inpKeyword, inpContactString, 1);
										},1500)--->
																			
								}
								else if(d.RESPONSETYPE == 'AGENT')
								{
									d.RESPONSETYPE = '';
									
									inpflag++;
									
									<!--- 
										Now what? How do we make this page list for AAU message to the current device address?
										How to ignore messages aau message if device address is changed?
									--->
																			
								}
								else if(d.GETRESPONSE.RESPONSETYPE == 'INTERVAL')
								{						
								
									if(d.GETRESPONSE.INTERVALTYPE == 'SECONDS' && parseInt(d.GETRESPONSE.INTERVALVALUE) <= 30 )
									{
										
										
										if(parseInt(d.GETRESPONSE.INTERVALVALUE) < 5)
											d.GETRESPONSE.INTERVALVALUE = 3;
										
																					
										<!---console.log('Settimeout interval');	--->
										setTimeout(function(){ GetResponseSimple(inpShortCode, 'IRE - INTERVAL', inpContactString, 1); }, parseInt(d.GETRESPONSE.INTERVALVALUE)*1000);									
									}
									else
									{										
										QueueNextResponse(inpShortCode, 'IRE - INTERVAL', inpContactString, d.GETRESPONSE.INTERVALSCHEDULED, d.GETRESPONSE.INTERVALEXPIREDNEXTQID, d.GETRESPONSE.IRESESSIONID);
										
										CheckNextResponse(inpShortCode, inpKeyword, inpContactString);
									}									
									
									<!--- Update optional interval processing and display --->
									<!---$('#INTERVALSCHEDULED').html(d.GETRESPONSE.INTERVALSCHEDULED + '<BR/>Device Address Time Zone offset from PST is ' + d.GETRESPONSE.TIMEZONEOFFSETPST);
									$('#IntervalOptions').show();--->
									
									<!--- force resize on empty --->
									$('#SMSTextInputArea').val('');
									$('#SMSTextInputArea').resize();
								}
								else
								{
									
									LastResponseTypeLocal = d.RESPONSETYPE;
									
									<!--- force resize on empty --->
									$('#SMSTextInputArea').val('');
									$('#SMSTextInputArea').resize();									
									
								}
							}
							
							
							
						}
						else
						{
							
						}
					}
					else
					{<!--- Invalid structure returned --->	
						
					}
											
			} 		
			
		});
		return false;
	}
	
	
	function QueueNextResponse(inpShortCode, inpKeyword, inpContactString, inpScheduled, inpTimeOutNextQID, inpIRESessionId)
	{		
		var data = 
		{ 
			inpContactString : inpContactString,
			inpShortCode : inpShortCode,
			inpQueueState : '<cfoutput>#SMSQCODE_QA_TOOL_READYTOPROCESS#</cfoutput>',
			inpScheduled : inpScheduled,
			inpTimeOutNextQID : inpTimeOutNextQID,
			inpIRESessionId : inpIRESessionId,
			inpKeyword : inpKeyword
		};
				
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'QueueNextResponseSMS', data, "Error: Next Interval response has not been set!", function(d ) {
			CheckNextResponse(inpShortCode, inpKeyword, inpContactString);
		});	
	
	}
	
	function CheckNextResponse(inpShortCode, inpKeyword, inpContactString)
	{		
		var data = 
		{ 
			inpContactString : inpContactString,
			inpShortCode : inpShortCode
		};
				
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'TimeMachineCheckNextResponseQA', data, "Error: Next Interval response can not be found!", function(d ) {
			
			if(typeof(d.RXRESULTCODE) != "undefined")
			{							
				CurrRXResultCode = d.RXRESULTCODE;	
									
				if(CurrRXResultCode > 0 && typeof(d.NEXTEVENT) != "undefined" )
				{
					$('#INTERVALSCHEDULED').html(d.NEXTEVENT);	
					
					<!---<!--- Update optional interval processing and display --->
								$('#INTERVALSCHEDULED').html(d.INTERVALSCHEDULED + '<BR/>Device Address Time Zone offset from PST is ' + d.TIMEZONEOFFSETPST);--->
								
								
					$('#IntervalOptions').show();						
				}
				else
				{
					$('#INTERVALSCHEDULED').html('NA - MM:DD:YYYY');					
				}
			}
			else
			{
				$('#INTERVALSCHEDULED').html('NA - MM:DD:YYYY');				
			}
							
		});	
	
	}
	
	
	function SetStartCP()
	{		
		var data = 
		{ 
			inpContactString : $('#inpContactString').val(),
			inpShortCode : $('#inpSMSToAddress').val(),
			inpKeyword : $('#inpCPKeyword').val(),
			inpCP : $('#inpCP').val()
			
		};
				
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'SetSurveyStateToCP', data, "Error: Starting Control Point has not been set!", function(d ) {
			
											
			if(typeof(d.RXRESULTCODE) != "undefined")
			{		
				CurrRXResultCode = d.RXRESULTCODE;	
									
				if(CurrRXResultCode > 0 && d.PROGRAMSETCPOK > 0 )
				{
					inpTimeTraveledSoFarInMinutes = 0;
					
					MaxStatements = inpflag + 20; 
			
					inpflag++;
			
					if(inpflag < MaxStatements)
						GetResponseSimple($('#inpSMSToAddress').val(), inpflag, $('#inpContactString').val(), 1); 					
				}
			}
		});	
	
	}
	
	function SendToPhysicalDevice(inpShortCode, inpKeyword, inpContactString, inpOverRideInterval) 
	{				
		<!--- If last response is a answer to a question then do not override interval --->
		if(LastResponseTypeLocal == 'ONESELECTION' || LastResponseTypeLocal == 'SHORTANSWER')
			inpOverRideInterval = 0;		
			
		<!--- Reset to blank so next SendToPhysicalDevice will be based on response type --->
		LastResponseTypeLocal = '';
			
		$.ajax({
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=ProcessNextResponseSMSRemote&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		dataType: 'json',
		data:  	{ 
					inpKeyword : inpKeyword,
					inpShortCode : inpShortCode,
					inpContactString : inpContactString,
					inpNewLine : "<BR>",
					inpOverRideInterval : inpOverRideInterval,
					inpScrubContactString : 0
				},					  
		error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
		success:		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
				<!--- Get row 1 of results if exisits--->
									
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(d.RXRESULTCODE) != "undefined")
					{							
						CurrRXResultCode = d.RXRESULTCODE;	
						
						if(parseInt(d.PROKF) == 0)
						{
							
							bootbox.alert("Platform error delivering To physical device.\n"  + d.PRM + "\n", function(result) { } );																	
						}
						
						if(CurrRXResultCode > 0)
						{
							
							if(d.RESPONSE.length > 0)
							{	
							
								var ConcatenateOffFlag = 0;
							
								if(ConcatenateOffFlag == 1)	
								{						
									var CurrPos = 0;
									var AmountToGrab = 160;								
									var BuffStr = d.RESPONSE;		
									
									<!---console.log('{' + BuffStr.substring(AmountToGrab-2,AmountToGrab-1) + '}');--->
									<!--- Break message on space if within last 60 characters --->
									while(BuffStr.substring(AmountToGrab-2,AmountToGrab-1) != " " && AmountToGrab > 100 && BuffStr.substring(AmountToGrab-2,AmountToGrab-1) != "") 
									{
										AmountToGrab = AmountToGrab - 1;
										<!---console.log('{' + BuffStr.substring(AmountToGrab-2,AmountToGrab-1) + '}');	--->								
									}
									
									<!--- Move back to last space--->
									AmountToGrab = AmountToGrab - 1;
									
									<!---console.log('AmountToGrab=' + AmountToGrab);--->
									var BuffStrOut = MidStr(BuffStr, CurrPos, AmountToGrab);
																		
									<!---console.log(BuffStrOut);--->
									
									$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble you">' + escapeHtml(BuffStrOut) + '</div>');								
									
									CurrPos = CurrPos + AmountToGrab;
									
									while( BuffStr.length > CurrPos)
									{					
									
										AmountToGrab = 160;
										
										<!---console.log('{' + BuffStr.substring(CurrPos+AmountToGrab-2,CurrPos+AmountToGrab-1) + '}');--->
										<!--- Break message on space if within last 60 characters --->
										while(BuffStr.substring(CurrPos+AmountToGrab-2,CurrPos+AmountToGrab-1) != " " && AmountToGrab > 100 && BuffStr.substring(CurrPos+AmountToGrab-2,CurrPos+AmountToGrab-1) != "")
										{
											AmountToGrab = AmountToGrab - 1;
											<!---console.log('{' + BuffStr.substring(CurrPos+AmountToGrab-2,CurrPos+AmountToGrab-1) + '}');--->
										}
										
										<!--- Move back to last space--->
										AmountToGrab = AmountToGrab -1;
													
										<!---console.log('AmountToGrab=' + AmountToGrab);--->
										BuffStrOut = MidStr(BuffStr, CurrPos, AmountToGrab);
										<!---console.log(BuffStrOut);--->
										CurrPos = CurrPos + AmountToGrab;
										
										$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble you">' + escapeHtml(BuffStrOut) + '</div>');									
									}
								
								}
								else
								{
									$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble you" rel="' + d.RESPONSELENGTH + '">' + escapeHtml(d.RESPONSE) + '</div>');																		
								}
																									
								<!--- Scroll to bottom on new data --->							
								$("#SMSHistoryScreenArea").animate({ scrollTop: $('#SMSHistoryScreenArea')[0].scrollHeight}, 1000);
							}
							
							<!--- Get next Response--->
							if(d.RESPONSETYPE == 'STATEMENT' || d.RESPONSETYPE == 'OPTIN' || d.RESPONSETYPE == 'API' || d.RESPONSETYPE == 'CDF' || d.RESPONSETYPE == 'OTTPOST' || d.RESPONSETYPE == 'RESET')
							{
								
								EBMText = 'IRE - ' +  d.RESPONSETYPE;
								d.RESPONSETYPE = '';
								
								inpflag++;
								
								if(inpflag < MaxStatements)
									SendToPhysicalDevice(inpShortCode, EBMText, inpContactString, 1); 
								
								<!---var t=setTimeout(function(){
										GetResponseSimple(inpShortCode, inpKeyword, inpContactString, 0);
									},1500)--->
																		
							}
							else if(d.RESPONSETYPE == 'AGENT')
							{
								d.RESPONSETYPE = '';
								
								inpflag++;
								
								<!--- 
									Now what? How do we make this page list for AAU message to the current device address?
									How to ignore messages aau message if device address is changed?
								--->
																		
							}
							else if(d.RESPONSETYPE == 'INTERVAL')
							{			
								if(d.INTERVALTYPE == 'SECONDS' && parseInt(d.INTERVALVALUE) <= 30 )
								{
									
									if(parseInt(d.INTERVALVALUE) < 5)
										d.INTERVALVALUE = 3;
									
										
									<!---console.log('Settimeout interval');	--->
									setTimeout(function(){ SendToPhysicalDevice(inpShortCode, 'IRE - INTERVAL', inpContactString, 1); }, parseInt(d.INTERVALVALUE)*1000);									
								}
								else
								{		
									QueueNextResponse(inpShortCode, 'IRE - INTERVAL', inpContactString, d.INTERVALSCHEDULED, d.INTERVALEXPIREDNEXTQID, d.IRESESSIONID);	
								}
															
								<!--- Update optional interval processing and display --->
								$('#INTERVALSCHEDULED').html(d.INTERVALSCHEDULED + '<BR/>Device Address Time Zone offset from PST is ' + d.TIMEZONEOFFSETPST);
								$('#IntervalOptions').show();
								
								<!--- force resize on empty --->
								$('#SMSTextInputArea').val('');
								$('#SMSTextInputArea').resize();
			
							}
							else
							{
								LastResponseTypeLocal = d.RESPONSETYPE;
								
								<!--- Non repeat question type --->
								MaxStatements = inpflag + 20; 
								
								<!--- force resize on empty --->
								$('#SMSTextInputArea').val('');
								$('#SMSTextInputArea').resize();
													
							}							
						}
						else
						{
							
						}
					}
					else
					{<!--- Invalid structure returned --->	
						
					}
											
			} 		
			
		});
		return false;
	}
		
	function escapeHtml(text) {
	
	  // Preserve newlines		
	  text = text.replace(/<br>/gi, 'nexlinex');
	 
	  var map = {
	    '&': '&amp;',
	    '<': '&lt;',
	    '>': '&gt;',
	    '"': '&quot;',
	    "'": '&#039;'
	  };
	
	  text = text.replace(/[&<>"']/g, function(m) { return map[m]; });	  
	  text = text.replace(/nexlinex/gi, '<br>');
	  
	  return text;
	}
		
</script>
   

   
<cfoutput>
<main class="container-fluid page">
    <cfinclude template="../views/commons/credits_available.cfm">
    <section class="row bg-white">
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-6 content-title">
                    <cfinclude template="../views/commons/welcome.cfm">
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6 content-menu">
					<a class="active" href="javascript:void(0)" style="cursor:default"><span class="icon-setting"></span><span>QA Tool</span></a>
                </div>
            </div>
        </div>
        <hr class="hrt0">
        <div class="container-fluid content-body">
<!---wrap the page content do not style this--->
<div id="page-content">
   
  <div class="container" >
  	
        <div class="EBMDialog row">
                              
            <form method="POST" id="qa_tools_form">
                               
                   <!--- <div class="form-left-portal">--->
                   <div class="col-xs-12 col-sm-12 col-md-6 OTTDemoContainer" style="">
  
                      	                             
                       <div id="QAScreen">
                       		<div class="iiner">
                       			<div class="iiner-relative">
                       				<img class="img-responsive ScreenBackground" src="#rootUrl#/#PublicPath#/images/sms/iphone6base.png" />  
		                                                       
		                            <!--- Phone screen display area --->
		                            <div id="PhoneScreenArea">	 	
		                                                      
		                                                      
		                                <div id="SMSToAddress"><input id="inpSMSToAddress" name="inpSMSToAddress" placeholder="To:" size="20" autofocus="autofocus" value="#DefaultShortCode#"/></div>                          
		                                
										<!--- SMS text history display area - append new SMS messages here - cookie history storage with clear button --->                          
		                             	<div id="SMSHistoryScreenArea">
		                                 
		                                  <!---  <div class="bubble me">Hello there! Can you meet at Houston's for lunch today at 12:30?</div>
		                                    <div style="clear:both"></div>
		                                    <div class="bubble you">Hi. Can I meet you there at 1:00? I'm stuck in a meeting.</div>
		                                    <div style="clear:both"></div>
		                                    <div class="bubble me">Great! See you there.</div>
		                                    <div style="clear:both"></div>
		                                    <div class="bubble you">Are you gonna grab a table?</div>
		                                    <div style="clear:both"></div>
		                                    <div class="bubble me">Yes.</div>
		                                    <div style="clear:both"></div>
		                                    <div class="bubble you">Awesome.</div>  --->                                                                       
		                                </div>   
		                                                          	                                
		                                <div id="SMSTextInputAreaContainer">
		                                 	<div class="iiner-relative">
		                                 		 <!--- SMS Text input area  --->                                                                  	
			                                    <textarea id="SMSTextInputArea" name="SMSTextInputArea" maxlength="160"></textarea>
			                                    <a class="btn btn-medium btn-primary-custom SMSSendButton" id="SMSSend" >Send</a>	
		                                 	</div>
		                                </div>
		                                 
		                                

		                            </div>
                       			</div>
                       		</div>
                            <img class="ScreenBackgroundGlare" src="#rootUrl#/#PublicPath#/images/sms/iphonebaseglareonly.png" />  
                        
                            
                              
                       </div>
                       
                                                                                
                    </div>
              
                             
                          
             
             <!---<div class="form-right-portal" style="float:right; width:470px;">--->
                    <div class="col-xs-12 col-sm-12 col-md-6">
	                        <div class="inputbox-container">
	                            <label for="inpContactString">Device Address <span class="small">Phone Number</span></label>
	                            <!--- Do not validate this... validate[custom[usPhoneNumberIII]] --->
	                            <input id="inpContactString" class="" name="inpContactString" placeholder="Enter Contact String Here" size="23" maxlength="25"/>
	                        </div>
	                    
	                    	<div class="clear" id="TimeMachineOptions" style="padding-top:20px; padding-bottom:0px;" title="System Time #LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'full')#">
	                        	<label class="qe-label-a">Advanced - <span style="text-decoration:underline; cursor:pointer;" id="TimeMachineOptionsLink">Time Machine</span></label>
	                        </div> 
	                        
	                         <div id="TimeMachineContainer" style="display:none;">
	                        	                              
	                            <div id="IntervalOptions" class="inputbox-container">
	                                <label>The next response has been queued up for delivery after:</label>
	                                <div id="INTERVALSCHEDULED" class="QASmall">NA - MM:DD:YYYY</div>
	                                 
	                                <a class="bluebuttonAuto small2" id="TIMEMACHINECHECK">                              
	                                    <div class="hide-info showToolTip">Check Queue</div>
	                                   <!--- <div class="tooltiptext">
	                                        <em>Check Queue</em>
	                                         Check when next response is scheduled to run - if there is one. Use Time Machine button separately to force any queued responses to run now.
	                                    </div>--->
	                                </a>
	                                    
	                                <a class="bluebuttonAuto small2" id="TIMEMACHINE">
	                                	<div class="hide-info showToolTip">Run Time Machine</div>
	                                    <!---<div class="tooltiptext">
	                                         <em>Run Time Machine</em>
	                                          Force any queued responses to run now.
	                                    </div>    --->                               
	                                </a>
	                            </div>
	                        
	                        </div>
                        <!---
                        <div class="clear" id="CPOptions" style="padding-top:20px; padding-bottom:0px;">
                        	<label class="qe-label-a">Advanced - <span style="text-decoration:underline; cursor:pointer;" id="CPOptionsLink">Starting Control Point</span></label>
                        </div> 
                        
                        <div id="StartCPContainer" style="display:none;">
                        	 	
                             <div class="inputbox-container">
                                <label for="inpCP">Starting Control Point</label>
                                <input id="inpCP" name="inpCP" placeholder="Enter CP" size="10" value=""/>
                                
                                <label for="inpCPKeyword">Program Start Keyword</label>
                                <input id="inpCPKeyword" name="inpCPKeyword" placeholder="Enter Keyword" size="20" value=""/>
                                
                             	<a class="bluebuttonAuto small2" id="STARTCP">                               
                                	<div class="hide-info showToolTip">Start IC at Control Point</div>
                                   <!--- <div class="tooltiptext">
                                         <em>Start IC at Control Point</em>
                                         This will start a new Interactive Campaign begining at the specified Control Point. All running campaigns on this device address will be stopped.
                                    </div> --->  
                                </a>
            
                            </div>
                        </div>--->
                        
                        <div class="clear" id="PhysicalDeviceOptions" style="padding-top:20px; padding-bottom:0px;">
                        	<label class="qe-label-a">Advanced - <span style="text-decoration:underline; cursor:pointer;" id="PhysicalDeviceOptionsLink">Physical Device Options</span></label>
                        </div> 
                        
                        <div id="StartPhysicalDeviceContainer" style="display:none;">
                        	 	
                             <div class="inputbox-container">
                                
                             	<a class="bluebuttonAuto small2" id="STARTPhysicalDevice">
                                    <div class="hide-info showToolTip">Send Next Response To Physical Device</div>
                                    <!---<div class="tooltiptext">
                                        <em>Send Next Response To Physical Device</em>
                                        This will send the next response to the physical device address specified. This will overide any waiting intervals. Initial messages will display locally on QA but if responses are sent from Physical Device, then messages will be sent to Physical Device only.
                                    </div> --->                                     
                                </a>
            
                            </div>
                        </div>
                        
                        <div class="clear" id="JSONOptions" style="padding-top:20px; padding-bottom:0px;">
                        	<label class="qe-label-a">Advanced - <span style="text-decoration:underline; cursor:pointer;" id="JSONOptionsLink">JSON CDFs</span></label>
                        </div> 
                        
                        <div id="JSONOptionsContainer" style="display:none;">
                        	 	
                            <div class="inputbox-container">
                                <label for="inpJSON">JSON list of Custom Data Fields</label>
                                <textarea id="inpJSON" name="inpJSON" placeholder="Enter Valid JSON" value="" style="padding:5px; width:280px; height:250px;"></textarea>
                            
                               
                                               
                            </div>
                        </div>
                        
                	</div>
                            
                   
                     <div style="clear:both"></div>
	                <div style="clear:both"></div>
               
            </form>
               
                                          
                
        </div>
		<input type="hidden" id="isTestKeyword" value="0"> 
		<input type="hidden" id="isEmitChatContent" value="0">
		<input type="hidden" id="Keyword" value="">     

    
       </div>
  <!--- /.container --->
  
</div>
<!--- /#page-content --->
</div>
    </section>
</main>


<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/js/jquery.autogrow-textarea.js">
</cfinvoke>

<cfparam name="variables._title" default="QA Tool - Sire">
<cfinclude template="../views/layouts/main-qatool.cfm">
</cfoutput>
