<div class="portlet light bordered shorturl">
    <div class="portlet-body form">
        <h2 class="page-title">Short URL</h2>
        <p>Easy to type, shortened web links (URLs) that can be useful in many places for Marketing and CRM messaging. The shortened URL will redirect to a longer URL that you specify. 
		In SMS this will help you save characters in your In SMS messages. You can use the default random codes for the short URL key or reserve your own custom named keys if 
		it is not used by someone else already. You also get a quick view of how many people clicked (Hits) on your links. <a href="">Click here to see an Example.</a></p>
		<div class="text-right">
			<button type="submit" class="btn green-gd btn-re">ADD SHORT URL</button>
		</div>
		<div class="re-table">
			<div class="table-scrollable">
		        <table class="table table-bordered">
		            <thead>
		                <tr>
		                    <th>Name</th>
		                    <th>Short URL</th>
		                    <th>Actual</th>
		                    <th>Clicks</th>
		                    <th>Action</th>
		                </tr>
		            </thead>
		            <tbody>
		                <tr>
		                    <td>Single Question <br> 2017-01-25 17:35:46</td>
		                    <td>http://mlp-x.com/fashion</td>
		                    <td>https://www.google.com/maps/place/South+Coast+Plaza/@33.6911652,117.8911167,17z/data=!3m1!4b1!4m5!3m4!1s0x80dcdf2525c7200b:0xd1d9f268d34c30d8!8m2!3d33.6911652!4d-117.888922778</td>
		                    <td>12</td>
		                    <td>
		                        <div class="btn-re-edit"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </div>
		                        <div class="btn-re-dowload"> <i class="fa fa-download" aria-hidden="true"></i> </div>
		                        <div class="btn-re-delete"> <i class="fa fa-times" aria-hidden="true"></i> </div>
		                    </td>
		                </tr>
		                <tr>
		                    <td>Single Question <br> 2017-01-25 17:35:46</td>
		                    <td>http://mlp-x.com/fashion</td>
		                    <td>https://www.google.com/maps/place/South+Coast+Plaza/@33.6911652,117.8911167,17z/data=!3m1!4b1!4m5!3m4!1s0x80dcdf2525c7200b:0xd1d9f268d34c30d8!8m2!3d33.6911652!4d-117.888922778</td>
		                    <td>12</td>
		                    <td>
		                        <div class="btn-re-edit"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </div>
		                        <div class="btn-re-dowload"> <i class="fa fa-download" aria-hidden="true"></i> </div>
		                        <div class="btn-re-delete"> <i class="fa fa-times" aria-hidden="true"></i> </div>
		                    </td>
		                </tr>
		                <tr>
		                    <td>Single Question <br> 2017-01-25 17:35:46</td>
		                    <td>http://mlp-x.com/fashion</td>
		                    <td>https://www.google.com/maps/place/South+Coast+Plaza/@33.6911652,117.8911167,17z/data=!3m1!4b1!4m5!3m4!1s0x80dcdf2525c7200b:0xd1d9f268d34c30d8!8m2!3d33.6911652!4d-117.888922778</td>
		                    <td>12</td>
		                    <td>
		                        <div class="btn-re-edit"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </div>
		                        <div class="btn-re-dowload"> <i class="fa fa-download" aria-hidden="true"></i> </div>
		                        <div class="btn-re-delete"> <i class="fa fa-times" aria-hidden="true"></i> </div>
		                    </td>
		                </tr>
		            </tbody>
		        </table>
		    </div>
		</div>
	    <!--- table --->
	    <div class="dataTables_paginate paging_input text-center">
	    	<span class="paginate_button first">
	    		<img class="paginate-arrow" src="/session/sire/images/double-left.png">
	    	</span>
	    	<span class="paginate_button previous" id="tblListShortURL_previous">
	    		<img class="paginate-arrow" src="/session/sire/images/left.png">
	    	</span>
	    	<span class="paginate_page">Page </span>
	    	<input type="text" class="paginate_text" style="display: inline;" value="1">
	    	<span class="paginate_of"> of 1</span>
	    	<span class="paginate_button next" id="tblListShortURL_next">
	    		<img class="paginate-arrow" src="/session/sire/images/right.png">
	    	</span>
	    	<span class="paginate_button last" id="tblListShortURL_last">
	    		<img class="paginate-arrow" src="/session/sire/images/double-right.png">
	    	</span>
	    </div>
	    <!--- pagination --->
    </div>
</div>
<cfinclude template="../views/layouts/master.cfm">