<cfparam name="keyword" default=""/>
<cfparam name="NXX" default=""/>
<cfparam name="NPA" default=""/>

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addJs("https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js")
        .addCss("https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css")
        .addJs("https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.3/socket.io.js")
        .addJs("../assets/pages/scripts/sms_keyword.js")
        .addCss("../assets/layouts/layout4/css/custom3.css")
        .addJs("../assets/pages/scripts/sms_ws.js");
</cfscript>

<cfinclude template="/public/sire/configs/paths.cfm"/>

<cfif keyword EQ "">
    <cflocation url="/session/sire/pages/dashboard" addtoken="false"/>
</cfif>

<cfinvoke component="session.sire.models.cfc.smschat" method="GetKeywordDetails" returnvariable="RetVarKeywordInfo">
    <cfinvokeargument name="inpKeyword" value="#keyword#"/>
</cfinvoke>

<cfif RetVarKeywordInfo.RXRESULTCODE LT 1>
    <cflocation url="/session/sire/pages/dashboard" addtoken="false"/>
</cfif>

<cfset city = ""/>

<cfif NPA NEQ "" AND NXX NEQ "">
    <cfinvoke component="session.sire.models.cfc.smschat" method="GetCityByPhoneOrAreaCode" returnvariable="RetVarCity">
        <cfinvokeargument name="inpNPA" value="#NPA#"/>
        <cfinvokeargument name="inpNXX" value="#NXX#"/>
    </cfinvoke>
    <cfif RetVarCity.RXRESULTCODE GT 0>
        <cfset city = RetVarCity.CITY/>
    </cfif>
</cfif>

<cfset keyword = HTMLEditFormat(keyword)/>

<cfoutput>

<div class="portlet light bordered">
    <div class="row">
        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
            <h4 class="portlet-heading2"><cfif city NEQ "">#city# </cfif>#keyword#</h4>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
            <h4 class="portlet-heading2 pull-right"><a href="/session/sire/pages/sms-chat">Back to list</a></h4>
        </div>
    </div>

    <div class="portlet light" style="padding-bottom: 0px">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 pull-right">
                <div class="pull-right div-keyword">
                    <button class="btn btn-block green-gd hidden" data-toggle="modal" id="sendMultipleBtn">Send Multiple</button>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 pull-left">
                <div class="inner-addon left-addon">
                    <i class="glyphicon glyphicon-search"></i>
                    <input type="search" class="form-control" id="searchbox" placeholder="Phone" />
                </div>
            </div>
        </div>
    </div>
    <div class="portlet light hidden" id="refreshAlert" style="padding-bottom: 0px;margin-bottom: 0px">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12">
                <div class="alert alert-info">
                    <span id="alertMessage"></span> Click <a class="alert-link" id="refreshTable">here</a> to update.
                </div>
            </div>
        </div>
    </div>
    <div class="portlet light">
        <div class="row">
            <div class="col-xs-12">
                <div class="re-table">
                    <div class="table-responsive">
                        <table id="sessionList" class="table table-striped table-responsive">
            
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--- User socket channel --->
<cfset us_chn = left(hash("#EnvPrefix#"&"SIRE_CHAT_USER_"&"#session.UserId#"), 6)/>

<script type="text/javascript">
    var keyword = "#keyword#";
    var NPA = "#NPA#";
    var NXX = "#NXX#";
    var city = "#city#";
    var batchId = #RetVarKeywordInfo.INFO.BATCHID#;
    var us_chn = "#us_chn#";
</script>

<style type="text/css">
    .col-center {
        text-align: center;
    }
    @media (max-width: 767px) {
        .div-keyword {
            margin-bottom: 10px;
        }
    }
</style>

</cfoutput>

<cfparam name="variables._title" default="SMS Keyword - Sire"> 

<cfinclude template="../views/layouts/master.cfm">