<cfinclude template="/session/sire/configs/credits.cfm">
<cfset variables.menuToUse = 2 />

<cfparam name="activeTab" default=""/>
<cfparam name="action" default=""/>
<cfparam name="downgradePlanName" default=""/>
<cfparam name="downgradePlanId" default="0"/>
<cfparam name="displayDowngrade" default="0"/>
<cfparam name="listKeyword" default="#arrayNew(1)#"/>
<cfparam name="listKeywordName" default="#arrayNew(1)#"/>
<cfparam name="listMLP" default="#arrayNew(1)#"/>
<cfparam name="listMLPName" default="#arrayNew(1)#"/>
<cfparam name="listShortUrl" default="#arrayNew(1)#"/>
<cfparam name="listShortUrlName" default="#arrayNew(1)#"/>

<cfif structKeyExists(url, 'active')>
    <cfset activeTab =  url.active >
</cfif>


<!--- RETRIVE CUSTOMER DATA --->
<cfset firstName =''>
<cfset lastName =''>
<cfset emailAddress =''>
<cfset primaryPaymentMethodId = -1>
<cfset expirationDate =''>
<cfset maskedNumber =''>
<cfset line1 =''>
<cfset city =''>
<cfset state =''>
<cfset zip =''>
<cfset country =''> 
<cfset phone =''>
<cfset hidden_card_form =''>
<cfset disabled_field =''>
<cfset cvv =''>
<cfset paymentType = 'card'>
<cfset RetCustomerInfo = ''>

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)
        .addJs("../assets/global/plugins/crypto-js/crypto-js.js", true)
        .addJs("../assets/pages/scripts/billing-update-card.js")
        .addJs("../assets/pages/scripts/billing_history.js")
        .addJs("../assets/pages/scripts/my-plan.js");
</cfscript>

<!--- GET USER INFO --->
<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke>
<cfif userInfo.USERLEVEL EQ 3 AND userInfo.USERTYPE EQ 2>
    <cfabort>    
</cfif>


<!--- GET USER BILLING DATA --->
<cfinvoke component="session.sire.models.cfc.billing"  method="GetBalanceCurrentUser"  returnvariable="RetValBillingData"></cfinvoke>

<!--- GET USER PLAN DETAIL --->
<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUserPlan" returnvariable="RetUserPlan"></cfinvoke>

<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetOrderPlan"  returnvariable="RetPlan">
    <cfinvokeargument name="plan" value="#RetUserPlan.PLANID#">
</cfinvoke>

<!--- GET NUMBER OF USED MLPS BY USER --->
<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUsedMLPsByUserId" returnvariable="RetUsedMLP"></cfinvoke>

<!--- GET NUMBER OF USED SHORT URLS BY USER --->
<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUsedShortURLsByUserId" returnvariable="RetUsedShortURL"></cfinvoke>

<!--- GET USER REFERRAL INFO --->
<cfinvoke component="session.sire.models.cfc.referral" method="GetUserReferInfo" returnvariable="RetValUserReferInfo">
<cfinvokeargument name="inpUserId" value="#SESSION.USERID#">
</cfinvoke>



<!--- GET NEXT PLAN UPGRADE --->
<cfinvoke component="session.sire.models.cfc.order_plan" method="GetNextPlanUpgrade" returnvariable="nextPlanUpgrade">
<cfinvokeargument name="inpPlanOrder" value="#RetUserPlan.PLANORDER#">
</cfinvoke>

<cfinvoke component="session.sire.models.cfc.billing" method="CheckExistsPaymentMethod" returnvariable="CheckExistsPaymentMethod"></cfinvoke>

<cfinvoke component="public.sire.models.cfc.plan" method="GetActivedPlans" returnvariable="plans">
</cfinvoke>
<!--- <cfif Session.loggedIn EQ 1>
    <cfinvoke method="GetUserPlan" component="session/sire/models/cfc/order_plan" returnvariable="getUserPlan">
    </cfinvoke>
</cfif>   --->  

<!--- <cfdump var="#RetCustomerInfo.RXRESULTCODE#" abort="true"> --->
<!--- Get Payment method setting --->
<cfinvoke component="session.sire.models.cfc.billing" method="GetPaymentMethodSettingByUserId" returnvariable="RetGetPaymentMethodSetting"></cfinvoke>

<!--- 
<cfif Session.loggedIn EQ 1>
    <cfinvoke method="GetUserPlan" component="session/sire/models/cfc/order_plan" returnvariable="getUserPlan">
    </cfinvoke>
</cfif>  
--->   
<cfif RetGetPaymentMethodSetting.RESULT EQ 1 OR RetGetPaymentMethodSetting.RESULT EQ 0 > <!--- Worldpay --->
    <cfinvoke component="session.sire.models.cfc.billing" method="RetrieveCustomer" returnvariable="RetCustomerInfo"></cfinvoke>    
    <cfset display_billing_info = 1/>
<cfelseif RetGetPaymentMethodSetting.RESULT EQ 2><!--- Paypal --->
    <cfinvoke component="session.sire.models.cfc.vault-paypal" method="getDetailCreditCardInVault" returnvariable="RetCustomerInfo"></cfinvoke>    
    <cfset display_billing_info = 1/>
    
<cfelseif RetGetPaymentMethodSetting.RESULT EQ 3><!--- Mojo --->
    <cfinvoke component="session.sire.models.cfc.vault-mojo" method="getDetailCreditCardInVault" returnvariable="RetCustomerInfo">
    </cfinvoke>    
    <cfset display_billing_info = 1/>
<cfelseif RetGetPaymentMethodSetting.RESULT EQ 4><!--- Total Apps --->
    <cfinvoke component="session.sire.models.cfc.vault-totalapps" method="getDetailCreditCardInVault" returnvariable="RetCustomerInfo"></cfinvoke>    
    <cfset display_billing_info = 1/>
<cfelse>
    <cfset RetCustomerInfo= {}/>
    <cfset RetCustomerInfo.RXRESULTCODE= 0/>
    <cfset display_billing_info = 0/>
</cfif>

<cfset creditCardType = ''/>

<cfif RetCustomerInfo.RXRESULTCODE EQ 1>    
    <cfset firstName = RetCustomerInfo.CUSTOMERINFO.firstName>
    <cfset lastName = RetCustomerInfo.CUSTOMERINFO.lastName>
    <cfset emailAddress = RetCustomerInfo.CUSTOMERINFO.emailAddress>
    <cfset primaryPaymentMethodId = 0> <!--- do not print --->
    <cfset expirationDate = RetCustomerInfo.CUSTOMERINFO.expirationDate>
    <cfset maskedNumber = RetCustomerInfo.CUSTOMERINFO.maskedNumber>
    <cfset line1 = RetCustomerInfo.CUSTOMERINFO.line1>
    <cfset city = RetCustomerInfo.CUSTOMERINFO.city>
    <cfset state = RetCustomerInfo.CUSTOMERINFO.state>
    <cfset zip = RetCustomerInfo.CUSTOMERINFO.zip>
    <cfset country = RetCustomerInfo.CUSTOMERINFO.country> 
    <cfset phone = RetCustomerInfo.CUSTOMERINFO.phone>
    <cfset hidden_card_form = 'display:none'>
    <cfset disabled_field ='display: none'>
    <cfset paymentType = RetCustomerInfo.CUSTOMERINFO.paymentType>
    <cfset cvv =RetCustomerInfo.CUSTOMERINFO.cvv>
    <cfset creditCardType =RetCustomerInfo.CUSTOMERINFO.creditCardType>
    <!--- <cfset lastFourDigits = RetCustomerInfo.CUSTOMERINFO.lastFourDigits> --->
    <cfset lastFourDigits = right(RetCustomerInfo.CUSTOMERINFO.maskedNumber, 4)>
<cfelseif RetCustomerInfo.RXRESULTCODE EQ -1>
    <main class="container-fluid page my-plan-page">
        <cfinclude template="../views/commons/credits_available.cfm">
        <section class="row bg-white">
            <div class="content-header">
                <div class="col-md-12">
                    <cfparam name="variables._title" default="My Plan - Sire">
                    <p class="text-danger text-center "><cfoutput>#RetCustomerInfo.MESSAGE#</cfoutput></p>
                </div>  
            </div>
        </section>  
    </main>
    <cfinclude template="../views/layouts/master.cfm">
    <cfexit>
</cfif>
<cfset userPlanName = 'FREE'>
<cfif RetUserPlan.RXRESULTCODE EQ 1>
    <cfset userPlanName = UCase(RetUserPlan.PLANNAME)>
</cfif>

<cfif RetUserPlan.PLANEXPIRED GT 0> 
    <cfset monthlyAmount = RetUserPlan.AMOUNT + (RetUserPlan.KEYWORDPURCHASED + RetUserPlan.KEYWORDBUYAFTEREXPRIED)*RetUserPlan.PRICEKEYWORDAFTER >
    <cfelse>    
        <cfset monthlyAmount = RetUserPlan.AMOUNT + RetUserPlan.KEYWORDPURCHASED*RetUserPlan.PRICEKEYWORDAFTER >
    </cfif>

    <cfset planMonthlyAmount = RetUserPlan.AMOUNT />

    <!--- GET MAX KEYWORD CAN EXPIRED --->
    <cfset UnsubscribeNumberMax = 0>
    <cfif RetUserPlan.PLANEXPIRED EQ 1>
        <cfif RetUserPlan.KEYWORDAVAILABLE GT RetUserPlan.KEYWORDBUYAFTEREXPRIED>
            <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDBUYAFTEREXPRIED>
            <cfelse>
                <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDAVAILABLE>
            </cfif>
            <cfelseif RetUserPlan.PLANEXPIRED EQ 2> <!--- IF USER PLAN HAS DOWNGRADE IT CAN NOT UNSCRIBE KEYWORD --->
    <!---
    <cfif RetUserPlan.KEYWORDAVAILABLE GT RetUserPlan.KEYWORDBUYAFTEREXPRIED>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDBUYAFTEREXPRIED>
    <cfelse>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDAVAILABLE>
    </cfif>    
--->
    <cfset UnsubscribeNumberMax = 0>
<cfelse>
    <cfif RetUserPlan.KEYWORDAVAILABLE GT RetUserPlan.KEYWORDPURCHASED>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDPURCHASED>
    <cfelse>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDAVAILABLE>
    </cfif>
</cfif>


<!--- GET INFOR DISPLAY DOWNGRADE --->
<cfif RetUserPlan.USERDOWNGRADEPLAN GT 0>
    <cfset listKeyword = deserializeJSON(RetUserPlan.USERDOWNGRADEKEYWORD)/>

    <cfset listMLP = deserializeJSON(RetUserPlan.USERDOWNGRADEMLP)/>
    <cfset listShortUrl = deserializeJSON(RetUserPlan.USERDOWNGRADESHORTURL)/>

    <cfset downgradePlanId = RetUserPlan.USERDOWNGRADEPLAN/>
    <cfloop query="#plans.DATA#">
        <cfif id EQ RetUserPlan.USERDOWNGRADEPLAN>
            <cfset downgradePlanName = name/>    
        </cfif>
    </cfloop>

    <!--- GET LIST KEYWORD NAME --->
    <cfinvoke component="session.sire.models.cfc.keywords" method="GetKeywordByListId" returnvariable="RetKeywordsInfo">
        <cfinvokeargument name="inpListKeywordId" value="#listKeyword#"/>
    </cfinvoke>
  
    <cfif RetKeywordsInfo.RXRESULTCODE GT 0>
        <cfif arrayLen(RetKeywordsInfo.NAMELIST) GT 0>
            <cfset listKeywordName = RetKeywordsInfo.NAMELIST/>  
        </cfif>
    </cfif>

    <!--- GET LIST MLP NAME --->
    <cfinvoke component="session.sire.models.cfc.mlp" method="GetMLPByListId" returnvariable="RetMLPInfo">
        <cfinvokeargument name="inpListMLPId" value="#listMLP#"/>
    </cfinvoke>
  
    <cfif RetMLPInfo.RXRESULTCODE GT 0>
        <cfif arrayLen(RetMLPInfo.NAMELIST) GT 0>
            <cfset listMLPName = RetMLPInfo.NAMELIST/>  
        </cfif>
    </cfif>

    <!--- GET LIST SHORT URL NAME --->
    <cfinvoke component="session.sire.models.cfc.urltools" method="GetShortUrlByListId" returnvariable="RetShortUrlInfo">
        <cfinvokeargument name="inpListShortUrlId" value="#listShortUrl#"/>
    </cfinvoke>

    <cfif RetShortUrlInfo.RXRESULTCODE GT 0>
        <cfif arrayLen(RetShortUrlInfo.NAMELIST) GT 0>
            <cfset listShortUrlName = RetShortUrlInfo.NAMELIST/>  
        </cfif>
    </cfif>



    <cfif action EQ 'downgradeDetail'>
        <cfset displayDowngrade = 1/>
    </cfif>

</cfif>



<div class="portlet light bordered">
	<div class="portlet-body">
		<div class="new-inner-body-portlet2">
			<ul class="uk-tab session-tab" uk-tab>
                <li><a href="#">Billing Info</a></li>    
                <cfif userInfo.USERTYPE EQ 1 OR (userInfo.USERTYPE EQ 2 AND userInfo.USERLEVEL EQ 1) >
			        <li><a href="#">Upgrade Plan</a></li>
                </cfif>
			    <li><a href="#">Billing History</a></li>
			    <li><a href="#">Transactions</a></li>
                <li><a href="#">Transactions exceeded limit</a></li>
			</ul>

			<ul class="uk-switcher uk-margin content-tab-pricing">
                 
			    <li>
                    <cfif RetGetPaymentMethodSetting.RESULT EQ 0 || RetGetPaymentMethodSetting.RESULT EQ 1 || RetGetPaymentMethodSetting.RESULT EQ 2 || RetGetPaymentMethodSetting.RESULT EQ 3 || RetGetPaymentMethodSetting.RESULT EQ 4>
			    	<div class="uk-grid uk-grid-medium">
			    		<div class="uk-width-1-1">
			    			<div class="form-gd">
								<div class="clearfix">
									<h3 class="form-heading">Cardholder Details</h3>
								</div>
							</div>
			    		</div>
						
				    		<div class="uk-width-1-1">
				    			<div class="uk-grid uk-grid-small cardholder-detail" uk-grid>
									<div class="uk-width-3-4@m uk-width-1-2@xl">

										<!--- <cfdump var="#RetCustomerInfo.CUSTOMERINFO#" abort="true"> --->
										<div class="uk-grid uk-grid-small uk-grid-match" uk-grid>
											<div class="uk-width-1-4@m uk-flex-middle">
												<span class="uk-text-middle">Card Information:</span>
											</div>
                                            
                                            <cfif creditCardType NEQ '' >
    											<div class="uk-width-expand">
    												<div>                                                    
    													<cfswitch expression="#UCASE(creditCardType)#">  
    													    <cfcase value="MASTERCARD">
    													    	<img src="../assets/layouts/layout4/img/card/mastercard.png" alt="">
    													    </cfcase>  
    													    <cfcase value="DISCOVER">
    													    	<img src="../assets/layouts/layout4/img/card/discover.png" alt="">
    													    </cfcase> 
    													    <cfcase value="AMERICANEXPRESS">
    													    	<img src="../assets/layouts/layout4/img/card/americanexpress.png" alt="">
    													    </cfcase> 
                                                            <cfcase value="AMEX">
    													    	<img src="../assets/layouts/layout4/img/card/americanexpress.png" alt="">
    													    </cfcase> 
    													    <cfcase value="ECHECK">
    													    	<img src="../assets/layouts/layout4/img/card/echeck.png" alt="">
    													    </cfcase>
    													    <cfcase value="VISA">
    													    	<img src="../assets/layouts/layout4/img/card/visa.png" alt="">
    													    </cfcase>
    													    <cfdefaultcase>  
    													        <img src="../assets/layouts/layout4/img/CC-1.jpg" alt="">
    													    </cfdefaultcase>  
    													</cfswitch>  
    											        <span class="uk-text-middle"><cfoutput>#creditCardType#</cfoutput> ending in <cfoutput>#lastFourDigits#</cfoutput> expires <cfoutput>#expirationDate#</cfoutput></span>
    											    </div>
    											</div>
                                             </cfif>   
										</div>

										<div class="uk-grid uk-grid-small" uk-grid>
											<div class="uk-width-1-4@m">
												<span>Billing Address:</span>
											</div>
                                            <cfif creditCardType NEQ '' >
    											<div class="uk-width-expand">
    												<ul class="uk-list">
    													<li><cfoutput>#firstName# #lastName#</cfoutput></li>
    													<li><cfoutput>#line1#</cfoutput></li>
    													<li><cfoutput>#city#, #state# #zip#</cfoutput></li>
    													<li id="country-code" data-country='<cfoutput>#country#</cfoutput>'></li>
    												</ul>
    											</div>
                                            </cfif>    
										</div>
									</div>
									<div class="uk-width-expand">
										<a class="btn newbtn green-gd"  data-toggle="modal" data-target="#modal-sections">Update</a>
									</div>
								</div>
				    		</div>

			    	</div>
                    </cfif>
                    <!--- Paypal Checkout 
                        <cfif RetGetPaymentMethodSetting.RESULT EQ 0 || RetGetPaymentMethodSetting.RESULT EQ 2>
                        <hr>    
                        <div class="uk-grid uk-grid-medium">
                            <div class="uk-width-1-1">
                                <div class="form-gd">
                                    <div class="clearfix">
                                        <h3 class="form-heading">Paypal Billing Agreement</h3>
                                    </div>
                                </div>
                            </div>
                            
                                <div class="uk-width-1-1">
                                    <div class="uk-grid uk-grid-small cardholder-detail" uk-grid>
                                        <div class="uk-width-expand">
                                            <cfinvoke component="session.sire.models.cfc.billing" method="getUserAuthenInfo" returnvariable="RetUserAuthenInfo">
                                                <cfinvokeargument name="userId" value="#SESSION.USERID#">
                                                <cfinvokeargument name="intPaymentGateway" value="2">
                                            </cfinvoke>
                
                                            <cfif RetUserAuthenInfo.RXRESULTCODE EQ 1>
                                                <a class="btn newbtn green-gd" id="pp_create_billing_agreement">Update Paypal billing agreement</a>
                                            <cfelse>    
                                                <a class="btn newbtn green-gd" id="pp_create_billing_agreement">Create Paypal billing agreement</a>
                                            </cfif>    
                                        </div>
                                    </div>
                                </div>

                        </div>
                        </cfif>  
                    --->  
			    </li>
                <cfif userInfo.USERTYPE EQ 1 OR (userInfo.USERTYPE EQ 2 AND userInfo.USERLEVEL EQ 1) >

			    <li>
			    	<cfinclude template="inc_upgrade_plan.cfm">
			    </li>
                </cfif>

			    <li>
			    	<div class="uk-grid uk-grid-medium">
			    		<div class="uk-width-1-1">
			    			<div class="form-gd">
								<div class="clearfix">
									<h3 class="form-heading">Billing History</h3>
								</div>
							</div>
			    		</div>

			    		<div class="uk-width-1-1">
			    			<div class="re-table">
							    <div class="table-responsive">
							        <table id="billing-history-list" class="table table-striped table-bordered table-hover">
							            
							        </table>
							    </div>
							</div>
			    		</div>
			    	</div>
			    </li>
			    <li>
			    	<div class="uk-grid uk-grid-medium">
			    		<div class="uk-width-1-1">
			    			<div class="form-gd">
								<div class="clearfix">
									<h3 class="form-heading">Transactions</h3>
								</div>
							</div>
			    		</div>

			    		<div class="uk-width-1-1">
			    			<div class="re-table">
							    <div class="table-responsive">
							        <table id="transaction-list" class="table table-striped table-bordered table-hover">
							            
							        </table>
							    </div>
							</div>
			    		</div>
			    	</div>
			    </li>

                <li>
			    	<div class="uk-grid uk-grid-medium">
			    		<div class="uk-width-1-1">
			    			<div class="form-gd">
								<div class="clearfix">
									<h3 class="form-heading">Transactions exceeded limit</h3>
								</div>
							</div>
			    		</div>

			    		<div class="uk-width-1-1">
			    			<div class="re-table">
							    <div class="table-responsive">
							        <table id="transaction-exceed-list" class="table table-striped table-bordered table-hover">
							            
							        </table>
							    </div>
							</div>
			    		</div>
			    	</div>
			    </li>
			</ul>

		</div>
	</div>
</div>
<!--- --->
<div id="modal-sections" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Payment Method</h4>
            </div>
            
            <form id="my_plan_form">            
            <div class="inner-update-cardholder">                
                    <cfoutput>
                        <div class="update_cardholder_info">
                        <input type="hidden" name="primaryPaymentMethodId" value="#primaryPaymentMethodId#">

                            <div id="fset_cardholder_info" >

                            <input type="radio" class="hidden" name="payment_method" class="check_payment_method" id="payment_method_0" value="0" checked/>


                            <div class="row row-small">
                                <div class="col-lg-8 col-md-6">
                                    <div class="form-group">
                                        <label>Card Number <span>*</span></label>
                                        <input type="text" class="form-control validate[required,custom[creditCardFunc]]" maxlength="32" id="card_number" name="number" value='' placeholder="---- ---- ---- 1234">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="form-group">
                                        <label>Security Code <span>*</span></label>
                                        <input type="text" class="form-control validate[required,custom[onlyNumber],minSize[3]" maxlength="4" id="security_code" name="cvv" data-errormessage-custom-error="* Invalid security code" data-errormessage-range-underflow = "* Security code should be 3 or 4 characters">
                                    </div>
                                </div>
                            </div>

                            <div class="row row-small">
                                <div class="col-lg-8 col-md-6">
                                    <div class="form-group">
                                        <label>Expiration Date <span>*</span></label>
                                        <input type="hidden" id="expiration_date" name="expirationDate" value="">
                                        <select class="form-control validate[required]" id="expiration_date_month" class="expiration_date_month">
                                            <option value="" disabled selected>Month</option>
                                            <option value="01">January</option>
                                            <option value="02">February</option>
                                            <option value="03">March</option>
                                            <option value="04">April</option>
                                            <option value="05">May</option>
                                            <option value="06">June</option>
                                            <option value="07">July</option>
                                            <option value="08">August</option>
                                            <option value="09">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6">
                                    <div class="form-group">
                                        <label>&nbsp;</label>
                                        <select class="form-control validate[required]" id="expiration_date_year" class="expiration_date_year">
                                            <option value="" disabled selected>Year</option>
                                            <cfloop from="#year(now())#" to="#year(now())+50#" index="i">
                                                <option value="#i#">#i#</option>
                                            </cfloop>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row row-small">
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label>Cardholder Name <span>*</span></label>
                                        <input type="text" class="form-control validate[required,custom[onlyLetterNumberSp]]" maxlength="50" id="first_name" name="firstName" placeholder="First Name" value="">
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label>&nbsp;</label>
                                        <input type="text" class="form-control validate[required,custom[onlyLetterNumberSp]]" maxlength="50" id="last_name" name="lastName" placeholder="Last Name" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Address:<span>*</span></label>
                                <input type="text" class="form-control validate[required,custom[noHTML], custom[validateAddrAndCity]] cardholder" maxlength="60" id="line1" name="line1" data-value='#line1#'>
                            </div>

                            <div class="form-group">
                                <label>City:<span>*</span></label>
                                <input type="text" class="form-control validate[required,custom[noHTML], custom[validateAddrAndCity]] cardholder" maxlength="40" id="city" name="city" data-value='#city#'>
                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>State:<span>*</span></label>
                                        <input type="text" class="form-control validate[required,custom[noHTML]] cardholder" maxlength="2" id="state" name="state" data-value="#state#">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Zip Code:<span>*</span></label>
                                        <input type="text" class="form-control validate[required,custom[onlyNumber]] cardholder" maxlength="5" id="zip" name="zip" data-value="#zip#" data-errormessage-custom-error="* Invalid zip code">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Country:<span>*</span></label>
                                <select class="form-control cardholder validate[required]" id="country" name="country" data-value="#country#" >
                                    <cfinclude template="../views/commons/payment/country.cfm">
                                </select>
                            </div>

                            <input type="hidden" class="form-control validate[custom[phone]] cardholder" maxlength="30" id="phone" name="phone" data-value="#phone#">

                            <div class="form-group">
                                <label>Email:</label>
                                <input type="text" class="form-control validate[custom[email]] cardholder" maxlength="128" id="email" name="email" data-value="#emailAddress#">
                            </div>                            
                        </div>
                    </cfoutput>
                
                </div>
            </div>            
            <div class="modal-footer uk-text-right buyaddon-footer-modal">
                <!--- <button class="btn newbtn green-cancel uk-modal-close">Cancel</button> --->
                
                <button type="button" class="btn newbtn1 green-cancel" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn newbtn green-gd">Save</button>
            </div>
            </form>
            
        </div>  
    </div>  
</div>
<!--- --->

<!-- Modal -->
<div id="InfoModalKeyword" class="modal fade" role="dialog">
  <div class="modal-dialog new-modal">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What is a Keyword?</h4>
      </div>
      <div class="modal-body">
      	<p class="text-center">
      		Imagine if Joe’s Coffee Shop offers this promotion: Text “FreeCoffee” to 39492 and receive a free coffee on your next visit.  “FreeCoffee” is the Keyword.  It’s the unique word or code you create so your customers can respond to your campaign.  
      	</p>
      </div>
      <!--- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> --->
    </div>

  </div>
</div>

<!-- Modal -->
<div id="InfoModalMLP" class="modal fade" role="dialog">
  <div class="modal-dialog new-modal">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What is a MLP?</h4>
      </div>
      <div class="modal-body">
      	<p class="text-center"> 
      		Let’s say Pete’s Gym offered a promotion: Buy 5 group fitness classes and get 3 free! Click here to redeem coupon.  Once customers clicks on that link they will be sent to your own Marketing Landing Page (MLP) displaying the coupon.  It’s a standalone web page promoting your offer and the best part is it’s included for free!
      	</p>
      </div>
      <!--- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> --->
    </div>

  </div>
</div>

    <script type="text/javascript">
        var activeTab = <cfoutput>"#activeTab#"</cfoutput>;
        var showPopupDowngrade = <cfoutput>"#displayDowngrade#"</cfoutput>;
        var downgradePlanId = <cfoutput>#downgradePlanId#</cfoutput>;
        var downgradePlanName = "<cfoutput>#downgradePlanName#</cfoutput>";

        var listKeyword = <cfoutput>#SerializeJSON(listKeyword)#</cfoutput>;
        var listKeywordName = <cfoutput>#SerializeJSON(listKeywordName)#</cfoutput>;
        var listMLP = <cfoutput>#SerializeJSON(listMLP)#</cfoutput>;
        var listMLPName= <cfoutput>#SerializeJSON(listMLPName)#</cfoutput>;
        var listShortUrl = <cfoutput>#SerializeJSON(listShortUrl)#</cfoutput>;
        var listShortUrlName = <cfoutput>#SerializeJSON(listShortUrlName)#</cfoutput>;
        var paypalCheckoutUrl = <cfoutput>'#_PaypalCheckOutUrl#'</cfoutput>;
    </script>


<cfinclude template="inc_upgrade_plan_popup.cfm"/>

<cfinclude template="../views/commons/sireplandetails.cfm">

<cfparam name="variables._title" default="My Account - Sire">

<cfinclude template="../views/layouts/master.cfm">
<cfif CheckExistsPaymentMethod.RXRESULTCODE EQ -1 AND userInfo.USERTYPE EQ 2>
    <script>                
        bootbox.dialog({
            message: "<p>Please take a minute to update your credit card info.</br>Don't worry, you won't be charged anything right now. All TheRestaurantExpert.com clients receive a low discounted rate of 1.4 cents per message which will be billed at the end of each billing cycle. Feel free to reach out with any questions: <a href='mailto:hello@siremobile.com'>hello@siremobile.com</a>.</p>",
            title: '<h4 class="be-modal-title">Before you get started</h4>',
            className: "",
            buttons: {
                success: {
                    label: "Continue",
                    className: "btn btn-medium green-gd",
                    callback: function(result) {
                        $("#modal-sections").modal('show');               
                    }
                }
            }
        });
        
    </script>
</cfif>