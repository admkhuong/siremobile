<main class="container-fluid page">
    <cfinclude template="../views/commons/credits_available.cfm">
    <section class="row bg-white">
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-6 content-title">
                    <cfinclude template="../views/commons/welcome.cfm">
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6 content-menu">
					<a class="active" href="javascript:void(0)"><span class="icon-buy-credits"></span><span>Buy Credits</span></a>
                </div>
            </div>
        </div>
        <hr class="hrt0">
        <div class="container-fluid content-body">
        	<div class="row">
            <div class="col-sm-5">
                <div class="row heading">
                    <div class="col-sm-12">The more credits you buy up front, the less you pay per credit!</div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-credits">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Credits</th>
                            <th>Price $USD</th>
                            <th>Select</th>
                        </tr>
                        </thead>
                        <tbody>
                    	<cfset i = 1>
                    	<cfinclude template="../configs/credits.cfm">
                    	<cfoutput>
                    	<cfloop array="#credits#" index="credit">
                        <tr>
                        	<td>#i#</td>
                            <td>#NumberFormat(credit.credits)#</td>
                            <td>$#NumberFormat(credit.price)#</td>
                            <td>
                            	<label class="purchase-amount-label">
                            		<cfif structKeyExists(credit, 'df') && credit.df EQ 1>
                                	<input type="radio" name="PurchaseAmount" class="purchase-amount" data-index="#i++#" data-credits="#NumberFormat(credit.credits)#" data-price="$#NumberFormat(credit.price)#" checked="checked">
                                	<cfelse>
                                	<input type="radio" name="PurchaseAmount" class="purchase-amount" data-index="#i++#" data-credits="#NumberFormat(credit.credits)#" data-price="$#NumberFormat(credit.price)#">
                                	</cfif>
                            	</label>
                            </td>
                        </tr>
                    	</cfloop>
                    	</cfoutput>
                        </tbody>
                    </table>
                    2 Credits for one SMS Message - up to 160 characters
                </div>
            </div>
            <div class="col-sm-6 col-sm-offset-1">
                <div class="row heading">
                	<div class="col-sm-12 heading-title">Secure Payment<hr></div>
                </div>
                <div class="row secure-payment">
                    <div class="col-xs-12 col-sm-12">
                    	<div class="pull-left">Currency:</div>
                    	<div class="pull-right">$ US Dollars</div>
                    </div>
                    <div class="col-xs-12 col-sm-12">
                    	<div class="pull-left">Number of Credits:</div>
                    	<div class="pull-right number-credits"></div>
                    </div>
                    <div class="col-xs-12 col-sm-12">
                    	<div class="pull-left">Amount:</div>
                    	<div class="pull-right amount"></div>
                    </div>
                    <div class="col-sm-12 small-info">
                    	Please review your purchase details, then select a payment method to continue
                    </div>
                </div>
                <hr>
                <div class="row heading">
                	<div class="col-sm-12 heading-title">Supported Payment Types</div>
                </div>
            	<div class="row supported-payment-types">
            		<div class="col-sm-12">
                    	<img src="/session/sire/images/supported-payment-types.png">
            		</div>
            	</div>
            	<hr>
            	<div class="row table-footer">
            		<div class="col-sm-12">
                    	<div class="pull-left"><img src="/session/sire/images/powered-worldpay.png"></div>
                    	<div class="pull-right worldpay-help">For help with your payment visit: <a href="http://www.securenet.com/contact-us" target="_blank">WorldPay Help</a></div>
            		</div>
            	</div>
        	</div>
        	<div class="col-sm-11 form-footer text-right">
        		<button class="btn btn-medium btn-success-custom btn-buy-credits">Continue</button>
        		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
            	<a class="btn btn-medium btn-primary-custom" href="/session/sire/pages/campaign-manage">Cancel</a>
        	</div>
    	</div>
    	</div>
    </section>
</main>
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/buy_credits.js">
</cfinvoke>
<cfinclude template="../views/layouts/main.cfm">