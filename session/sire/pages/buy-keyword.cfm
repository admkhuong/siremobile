<cfparam name="url.plan" default="0">
<cfset variables.urlpage = 'buykeyword' />


<cfinvoke method="GetUserPlan" component="session/sire/models/cfc/order_plan" returnvariable="getUserPlan">
</cfinvoke>

<cfset variables.menuToUse = 2 />

<!--- RETRIVE CUSTOMER DATA --->
<cfset firstName =''>
<cfset lastName =''>
<cfset emailAddress =''>
<cfset primaryPaymentMethodId = -1>
<cfset expirationDate =''>
<cfset maskedNumber =''>
<cfset line1 =''>
<cfset city =''>
<cfset state =''>
<cfset zip =''>
<cfset country =''> 
<cfset phone =''>
<cfset hidden_card_form =''>
<cfset disabled_field =''>
<cfset cvv =''>
<cfset paymentType = 'card'>
<cfset RetCustomerInfo = ''>

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("../assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css", true)
        .addJs("../assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js", true)
        .addJs("../assets/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js", true)
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)
        .addJs("../assets/pages/scripts/buykeyword.js");
</cfscript>

<!--- GET USER BILLING DATA --->
<cfinvoke component="session.sire.models.cfc.billing"  method="GetBalanceCurrentUser"  returnvariable="RetValBillingData"></cfinvoke>

<!--- GET USER PLAN DETAIL --->
<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUserPlan" returnvariable="RetUserPlan"></cfinvoke>

<!--- GET NUMBER OF USED MLPS BY USER --->
<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUsedMLPsByUserId" returnvariable="RetUsedMLP"></cfinvoke>

<!--- GET NUMBER OF USED SHORT URLS BY USER --->
<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUsedShortURLsByUserId" returnvariable="RetUsedShortURL"></cfinvoke>

<!--- GET USER REFERRAL INFO --->
<cfinvoke component="session.sire.models.cfc.referral" method="GetUserReferInfo" returnvariable="RetValUserReferInfo">
    <cfinvokeargument name="inpUserId" value="#SESSION.USERID#">
</cfinvoke>

<!--- GET USER INFO --->
<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke>

<!--- GET NEXT PLAN UPGRADE --->
<cfinvoke component="session.sire.models.cfc.order_plan" method="GetNextPlanUpgrade" returnvariable="nextPlanUpgrade">
    <cfinvokeargument name="inpPlanOrder" value="#RetUserPlan.PLANORDER#">
</cfinvoke>

<cfinvoke component="session.sire.models.cfc.billing" method="RetrieveCustomer" returnvariable="RetCustomerInfo"></cfinvoke>   

<cfif RetCustomerInfo.RXRESULTCODE EQ 1>    
    <cfset firstName = RetCustomerInfo.CUSTOMERINFO.firstName>
    <cfset lastName = RetCustomerInfo.CUSTOMERINFO.lastName>
    <cfset emailAddress = RetCustomerInfo.CUSTOMERINFO.emailAddress>
    <cfset primaryPaymentMethodId = 0> <!--- do not print --->
    <cfset expirationDate = RetCustomerInfo.CUSTOMERINFO.expirationDate>
    <cfset maskedNumber = RetCustomerInfo.CUSTOMERINFO.maskedNumber>
    <cfset line1 = RetCustomerInfo.CUSTOMERINFO.line1>
    <cfset city = RetCustomerInfo.CUSTOMERINFO.city>
    <cfset state = RetCustomerInfo.CUSTOMERINFO.state>
    <cfset zip = RetCustomerInfo.CUSTOMERINFO.zip>
    <cfset country = RetCustomerInfo.CUSTOMERINFO.country> 
    <cfset phone = RetCustomerInfo.CUSTOMERINFO.phone>
    <cfset hidden_card_form = 'display:none'>
    <cfset disabled_field ='display: none'>
    <cfset paymentType = RetCustomerInfo.CUSTOMERINFO.paymentType>
    <cfset cvv =RetCustomerInfo.CUSTOMERINFO.cvv>
<cfelseif RetCustomerInfo.RXRESULTCODE EQ -1>
    <main class="container-fluid page my-plan-page">
        <cfinclude template="../views/commons/credits_available.cfm">
            <section class="row bg-white">
                <div class="content-header">
                    <div class="col-md-12">
                        <cfparam name="variables._title" default="My Plan - Sire">
                        <p class="text-danger text-center "><cfoutput>#RetCustomerInfo.MESSAGE#</cfoutput></p>
                    </div>  
                </div>
            </section>  
    </main>
    <cfinclude template="../views/layouts/master.cfm">
    <cfexit>
</cfif>

<cfset userPlanName = 'Free'>
<cfif RetUserPlan.RXRESULTCODE EQ 1>
    <cfset userPlanName =   RetUserPlan.PLANNAME>
</cfif>

<cfif RetUserPlan.PLANEXPIRED GT 0> 
    <cfset monthlyAmount = RetUserPlan.AMOUNT + (RetUserPlan.KEYWORDPURCHASED + RetUserPlan.KEYWORDBUYAFTEREXPRIED)*RetUserPlan.PRICEKEYWORDAFTER >
<cfelse>    
    <cfset monthlyAmount = RetUserPlan.AMOUNT + RetUserPlan.KEYWORDPURCHASED*RetUserPlan.PRICEKEYWORDAFTER >
</cfif>

<cfset planMonthlyAmount = RetUserPlan.AMOUNT />


<!--- GET MAX KEYWORD CAN EXPIRED --->
<cfset UnsubscribeNumberMax = 0>
<cfif RetUserPlan.PLANEXPIRED EQ 1>
    <cfif RetUserPlan.KEYWORDAVAILABLE GT RetUserPlan.KEYWORDBUYAFTEREXPRIED>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDBUYAFTEREXPRIED>
    <cfelse>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDAVAILABLE>
    </cfif>
<cfelseif RetUserPlan.PLANEXPIRED EQ 2> <!--- IF USER PLAN HAS DOWNGRADE IT CAN NOT UNSCRIBE KEYWORD --->
    <!---
    <cfif RetUserPlan.KEYWORDAVAILABLE GT RetUserPlan.KEYWORDBUYAFTEREXPRIED>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDBUYAFTEREXPRIED>
    <cfelse>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDAVAILABLE>
    </cfif>    
    --->
    <cfset UnsubscribeNumberMax = 0>
<cfelse>
    <cfif RetUserPlan.KEYWORDAVAILABLE GT RetUserPlan.KEYWORDPURCHASED>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDPURCHASED>
    <cfelse>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDAVAILABLE>
    </cfif>
</cfif>

<div class="portlet light bordered">
    <div class="portlet-body form">
        <form id="buy_credits" class="content-body" action="/session/sire/models/cfm/buy-sms.cfm">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6 form-gd">
                        <div class="clearfix">
                            <h3 class="form-heading">Plan Details</h3>
                        </div>
                        <div class="statistical">
                            <!--- Plan Details included by plan_details.cfm --->
                            <cfinclude template="../views/commons/plan_details.cfm">
                        </div>

                        <div class="tabbable tabbable-tabdrop tab-invite-1">
                            <button class="btn green-gd uk-width-1-1 visible-xs uk-margin-small-bottom" type="button" uk-toggle="target: #toggle-xs; cls: uk-hidden"><i class="fa fa-th" aria-hidden="true"></i> Toggle My Plan Tab</button>
                            <ul  id="toggle-xs" class="uk-child-width-1-3 uk-child-width-1-5@s invite-ul uk-tab" uk-margin>
                                <li>
                                    <a href="/session/sire/pages/my-plan">
                                        <span class="tab-img i-1"></span>
                                        <span>My Plan</span>
                                    </a>
                                </li>
                                <li>
                                    <cfif RetUserPlan.PLANEXPIRED EQ 1 >
                                        <a href="javascript:void(0)" class="plan-disabled" title="Please extend your plan."><span class="tab-img i-2"></span><span>Upgrade Plan</span></a>   
                                    <cfelseif RetUserPlan.PLANEXPIRED GTE 2 && RetUserPlan.KEYWORDBUYAFTEREXPRIED GT 0  >   
                                        <a href="javascript:void(0)" class="plan-disabled" title="Please pay for your keywords"><span class="tab-img i-2"></span><span>Upgrade Plan</span></a>   
                                    <cfelse>
                                        <cfif nextPlanUpgrade.PlanId_int GT 0>
                                            <cfset next_plan_id = nextPlanUpgrade.PlanId_int>
                                            <a href="/session/sire/pages/order-plan?plan=<cfoutput>#next_plan_id#</cfoutput>" title="" class=""><span class="tab-img i-2"></span><span>Upgrade Plan</span></a>   
                                        <cfelse>
                                            <a href="javascript:void(0)" title="" class="plan-disabled" ><span class="tab-img i-2"></span><span>Upgrade Plan</span></a>   
                                        </cfif>
                                    </cfif>
                                </li>
                                <li>
                                    <a href="/session/sire/pages/buy-sms">
                                        <span class="tab-img i-3"></span>
                                        <span>Buy Credit</span>
                                    </a>
                                </li>
                                <li class="uk-active"> 
                                    <a href="/session/sire/pages/buy-keyword" title="" class="active"><span class="tab-img i-4"></span><span>Buy Keyword</span></a>
                                </li>
                                <li>
                                    <a href="/session/sire/pages/invite-friend"><span class="tab-img i-5"></span><span>Invite a friend</span></a>
                                </li>
                            </ul>

                            <div class="wrap-buycredit">
                                <cfoutput>
                                    <input type="hidden" id="amount" name="amount" value="0">
                                    <input type="hidden" id="pricekeywordafter" value="#getUserPlan.PRICEKEYWORDAFTER#">
                                    <input type="hidden" id="actionStatus" value="">
                                    <input type="hidden" value="#userInfo.FULLEMAIL#" id="h_email" name="h_email">
                                    <input type="hidden" name="h_email_save" value="#emailAddress#">
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="row row-small">
                                                <div class="col-xs-12">
                                                    <div class="form-group mb-0">
                                                        <label>Number of Keywords <span>*</span></label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-7">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control validate[required,custom[onlyNumberNotZero]]" style="text-align:right" id="numberKeyword" name="numberKeyword">
                                                        <label class="amount">Amount: <span class="text-green" id="Amount">$0</span></label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-5">
                                                    <span class="amountx pricemsg_after plan-info" > x <b><cfoutput>$#NumberFormat(getUserPlan.PRICEKEYWORDAFTER)#</cfoutput></b></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <a class="btn newbtn green-gd btt-calculate" id="keyword_calculate">calculate</a>
                                        </div>
                                    </div>
                                </cfoutput>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 form-gd">
                        <!--- Cardholder details included by cardholder_details.cfm --->
                        <cfinclude template="../views/commons/cardholder_details.cfm">
                        
                        <button type="submit" class="btn newbtn green-gd btn-payment-credits">Purchase</button>
                        <a type="button" class="btn newbtn green-cancel"  href="javascript:history.go(-1)">Cancel</a>

                    </div>
                </div>
        
            </div>
        </form>
    </div>
</div>

<cfinclude template="../views/commons/sireplandetails.cfm">

<cfparam name="variables._title" default="Buy Keywords - Sire">

<cfinclude template="../views/layouts/master.cfm">

<cfinclude template="../views/commons/what-mlp-keyword.cfm">

<script type="text/javascript">
    var country = '<cfoutput>#country#</cfoutput>';
</script>
