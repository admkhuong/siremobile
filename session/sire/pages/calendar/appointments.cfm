<cfparam name="cid" default="1" />
<cfparam name="inpcontactstring" default="888"/>
<cfparam name="inpbatchId" default="1"/>
<cfparam name="inpEnterToSend" default="0"/>
<cfparam name="inpShortCode" default=""/>

<cfinclude template="/session/cfc/csc/constants.cfm">

<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>
<!---  SHORTCODEID, SHORTCODE--->
<cfset inpShortCode = shortCode.SHORTCODE>
<!---
<cfinvoke component="session.sire.models.cfc.optin-out" method="checkContactStringOptout" returnvariable="RetValCheckContactStringOptout">
	<cfinvokeargument name="inpContactString" value="8888888800">
	<cfinvokeargument name="inpShortCode" value="QA2Nowhere">
</cfinvoke>
<cfdump var = "#RetValCheckContactStringOptout#"/>
--->
<!--- auto create a camapign for user first uses --->
<cfinvoke component="session.sire.models.cfc.calendar" method="SetCalendarConfigurationDefault" returnvariable="RetValSetCalendarConfigurationDefault"></cfinvoke>
<cfif RetValSetCalendarConfigurationDefault.RXRESULTCODE GT 0>
	<cfset cid = RetValSetCalendarConfigurationDefault.CALENDARID>
	<cfset inpbatchId = RetValSetCalendarConfigurationDefault.BATCHID>
</cfif>

<cfinvoke component="session.sire.models.cfc.calendar" method="GetCalendarConfiguration" returnvariable="RetValGetCalendarConfiguration">
	<cfinvokeargument name="inpCalendarId" value="#cid#">
</cfinvoke>

<cfinvoke method="GetChatSetting" component="session.sire.models.cfc.smschat" returnvariable="RetVarSetting">
</cfinvoke>

<cfif RetVarSetting.RXRESULTCODE GT 0>
	<cfif RetVarSetting.EnterToSend EQ 1>
		<cfset inpEnterToSend = 1>
	</cfif>
</cfif>

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("/session/sire/css/calendar.css")
</cfscript>

<style>

	#Calendar_collapse
	{
		display: none;
	}
</style>

<!---
	Notes:
		Start with only one calendar per account - makes it simple

	CREATE SCHEMA `calendar` DEFAULT CHARACTER SET latin1 ;



	https://fullcalendar.io/docs/event_data/events_json_feed/

	Requires user to be logged in to Sire edit - secured by Session.UserId




	ConfirmationFlag States

	0 - No reminder sent (Hide)
	1 - Scheduled
	2 - Accepted
	3 - Declined
	4 - Change Request
	5 - Reminder Error - Bad SMS, eMail or other error (Hide)
	6 - Off
	7 - Sent
	8 - Chat Active
	9 - Opt Out
	10 - Chat Close



	Tasks:

	Look at Apple calendar app on iphone to see how they do UI small


	More Bulletproof - verify no schedule already set in case event "loses" (ajax timeout / error /etc) the events DTSId

	Need to work on verify MAX sessions - see logic where IRESESSIONSTATE_TERMINATED_TOO_MANY_SESSIONS_IN_PROGRESS in inc_smsdelivery

	Update status when/if reminder sent -
	Update UI result

	Need to be sure to set a default schedule for reminders - when batch is picked / defined
	Make sure end date is next century

	Look for Distirbution Id in results to see if campaing was scheduled OK
	Also look for DNC or other errors

	If first message confirms - adjust next messge - Delete? BR just say time?


	UI FEEDBACK WHILE LOADING APPT AND SETTING UP REMINDER


	Atuo switch to popup on small screen vs popover

	Setup default reminder campaign
	Log queued id in event - use this to update or change dates
	Setup time to remind - before event
	Option to remind if event is scheduled past reminder window
	New duplicate logic for reminders - no more than one per day? what about multiple pre-scheduled appt reminders? Both greater than and less than a certain limit? Pre-check before attempt on queue? Better to check first?


	Mulitple Remiders Option - use advanced flows if needed
	Flow Option
	BR - Check if confirmed or denied yet - if not confirm again later?


	Verify batch owner on scheduling of reminder - make sure no hacks


	Option to auto remind
	Option to confirm each reminder

	New green fro reminders that cant go out

	NOC monitor for automated processes
	look for abuse
	look for not running
	look for too slow
	look for counts in use

	Multithreaded cron for processing quickly - every hour 6AM PST til 11PM PST - covers Hawaii?
	Reminder Schedule is 9-6 local - not directly user adjustable?


		Setting panel -
		Choose time ranges and increments
		Choose colors?
		Choose Campaing to verify
		OR
		Choose Campaign to Notify
		OR
		Just choose reminder campaign
		Option to create remider campaign from hidden template

		New CDF - Event Reminder ID - Store in JSON data and use in API call to update calendar event


		More detailed event editor
		Notes
		Name
		Use data from event in CDF JSON to message




		Asyncornous load?
		Only load current page?

		Sync to external calander option
		cfc
		   Add Event
		   Read Event
		   Update Event
		   Reminder Campaing - coming, reschedule, yes / no NLP - pop-response in calendar automatically
		   Expose as Sire secure API
		   Expose to public?

		Check warn DNC on add appointment
			Tool for user to opt back in once they opt out

		Name
		Number
		First
		Last


		*When an event is deleted - any scheduled reminders in queue need to be removed

		Complicance on appotinment reminders


		On add appointment - check rules


		email through a service
		opt out rules etc



		Allow chat with appointment session
			Cant make it? When is a good time?


		Reporting and Stats
		Canceled Counts?
		Changed Counts?
		Daily overview?


		Cron:

		Search the events DB by date range and schedule that days calander items - starts at midnight




 --->



<div class="portlet light bordered">
	<div class="row">
		<div class ="reminder-calendar-title col-xs-12">
			<a class="title_calendar" title="Refresh Calendar">APPOINTMENT CALENDAR <a/><a title="Hide/show filter" data-toggle="toggle" data-target="#Calendar_collapse"><i class="fa fa-bars icon_calendar" aria-hidden="true"></i></a>
		</div>
	</div>
	<div class="row">
		<div id='Calendar_collapse' class="col-xs-2 in"> <!---collapse in--->
			<!--- Calendar left --->
			<div id='calendar_left'>
				<label class="container1"><p class="status-name"> Scheduled </p> <!--- 1 --->
					<input type="checkbox" value=<cfoutput>#APPT_REMINDER_QUEUED#</cfoutput> <cfif listFind(RetValGetCalendarConfiguration.FILTERCALENDAR, '#APPT_REMINDER_QUEUED#') GT 0> checked="checked"<cfelse></cfif>>
					<span class="checkmark remind-inqueue"></span>
				</label>
				<label class="container1"><p class="status-name"> Off </p> <!--- 6 --->
					<input type="checkbox" value=<cfoutput>#APPT_REMINDER_DO_NOT_SEND#</cfoutput> <cfif listFind(RetValGetCalendarConfiguration.FILTERCALENDAR, '#APPT_REMINDER_DO_NOT_SEND#') GT 0> checked="checked"<cfelse></cfif>>
					<span class="checkmark remind-dont-send"></span>
				</label>
				<label class="container1"><p class="status-name"> Sent </p> <!--- 7 --->
					<input type="checkbox" value=<cfoutput>#APPT_REMINDER_SENT#</cfoutput> <cfif listFind(RetValGetCalendarConfiguration.FILTERCALENDAR, '#APPT_REMINDER_SENT#') GT 0> checked="checked"<cfelse></cfif>>
					<span class="checkmark remind-sent"></span>
				</label>

				<!--- <label class="container1"><p class="status-name"> Chat Active</p> <!--- 8 --->
					<input type="checkbox" value=<cfoutput>#APPT_REMINDER_CHAT_ACTIVE#</cfoutput> <cfif listFind(RetValGetCalendarConfiguration.FILTERCALENDAR, '#APPT_REMINDER_CHAT_ACTIVE#') GT 0> checked="checked"<cfelse></cfif>>
					<span class="checkmark remind-chat-active"></span>
				</label>
				--->

				<label class="container1"><p class="status-name"> Opt Out </p> <!--- 9 --->
					<input type="checkbox" value=<cfoutput>#APPT_REMINDER_OPT_OUT#</cfoutput> <cfif listFind(RetValGetCalendarConfiguration.FILTERCALENDAR, '#APPT_REMINDER_OPT_OUT#') GT 0> checked="checked"<cfelse></cfif>>
					<span class="checkmark remind-opt-out"></span>
				</label>
				<label class="container1"><p class="status-name"> Accepted </p> <!--- 2 --->
					<input type="checkbox" value=<cfoutput>#APPT_REMINDER_ACCEPTED#</cfoutput> <cfif listFind(RetValGetCalendarConfiguration.FILTERCALENDAR, '#APPT_REMINDER_ACCEPTED#') GT 0> checked="checked"<cfelse></cfif>>
					<span class="checkmark remind-accepted"></span>
				</label>
				<label class="container1"><p class="status-name"> Change Request </p> <!--- 4 --->
					<input type="checkbox" value=<cfoutput>#APPT_REMINDER_CHANGE_REQUEST#</cfoutput> <cfif listFind(RetValGetCalendarConfiguration.FILTERCALENDAR, '#APPT_REMINDER_CHANGE_REQUEST#') GT 0> checked="checked"<cfelse></cfif>>
					<span class="checkmark remind-chang-request"></span>
				</label>
				<label class="container1"><p class="status-name"> Declined</p> <!--- 3 --->
					<input type="checkbox" value=<cfoutput>#APPT_REMINDER_DECLINED#</cfoutput> <cfif listFind(RetValGetCalendarConfiguration.FILTERCALENDAR, '#APPT_REMINDER_DECLINED#') GT 0> checked="checked"<cfelse></cfif>>
					<span class="checkmark remind-decline"></span>
				</label>

				<!--- <label class="container1"><p class="status-name"> Chat Closed</p> <!--- 10 --->
					<input type="checkbox" value=<cfoutput>#APPT_REMINDER_CHAT_CLOSE#</cfoutput> <cfif listFind(RetValGetCalendarConfiguration.FILTERCALENDAR, '#APPT_REMINDER_CHAT_CLOSE#') GT 0> checked="checked"<cfelse></cfif>>
					<span class="checkmark remind-chat-close"></span>
				</label>
				--->
			</div>

		</div>

       <div id ="box_calendar" class="col-xs-12">
	   		<div id='calendar'></div>
       </div>
   </div>
</div>
<!--- Use the content froma popup in the popovers - default to popover on smaller screens --->
<!--- Set a js variable to track screen size on load and redraw --->

<div class="bootbox modal fade" id="Calendar-Event-Modal" tabindex="-1" role="dialog" aria-labelledby="Calendar-Event-Modal" aria-hidden="true" style="">

    <div class="modal-dialog" role="document" style="">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Calendar Event</h4>
            </div>
<!---
<div id="popover-markup" class="popover-markup">
    <div class="head hide">Calendar Event</div>
    <div class="content hide">

	<div class="event-bubble-close close" tabindex="0" style="z-index: 1000;" role="button" aria-label="close">X</div>

--->
	    	<!--- Popover section without header --->
		    <div class="popover-section modal-body popover-content">

			    <div class="event-bubble">

				    <h5><span class="event-link event-link-active"><!---Event</span> | <span class="reminder-link">Reminder</span> | <span class="chat-link">Chat</span> | <span class="respone"><a>Respone History</a></span>---></h5>
				    <div class="event-group">
				        <div class="form-group">
							<label>Event Name</label>
				            <input
				                type="text"
				                class="form-control inpTitle"
				                placeholder="Notes…">
				        </div>
				        <div class="form-group">
					        <label><i class="glyphicon glyphicon-phone"></i> Mobile Phone Number (SMS Capable)</label>
				            <input
				                type="text"
				                class="form-control inpSMSNumber"
				                placeholder="Mobile Phone Number">
				        </div>
						 <div class="form-group">
						 	<label><span class="glyphicon glyphicon-calendar"></span> Event Date</label>
							<div class="input-group datetimepicker date set-z-index" id="datetimestart">
								<input type='text' class="form-control datetimestart"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>
						 <div class="form-group hide">
						 	<label><span class="glyphicon glyphicon-calendar"></span> End Date</label>
							<div class="input-group datetimepicker date set-z-index" id="datetimeend">
								<input type='text' class="form-control datetimeend"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>

				        <div class="form-group">
					        <table cellpadding="5" cellspacing="5" border="0" style="">
					        	<tr>
						        	<td><label><span class="glyphicon glyphicon-time"></span> Start Time</label></td>
						        	<td><label><span class="glyphicon glyphicon-time"></span> End Time</label></td>
					        	</tr>
								<tr>
							        <td><input type="text" class="time ui-timepicker-input TimeStepStart form-control" autocomplete="off" style="width: 80%; display: inline;"></td>
									<td><input type="text" class="time ui-timepicker-input TimeStepEnd form-control" autocomplete="off" style="width: 80%; display: inline;"></td>
								</tr>
					        </table>
				        </div>
						<div class="form-group event-status">

					       <label>Status:</label>
				            <select id="inpConfirmationFlag" class="Select2">
								<option value=<cfoutput>#APPT_REMINDER_QUEUED#</cfoutput> >Scheduled</option>
								<option value=<cfoutput>#APPT_REMINDER_ACCEPTED#</cfoutput> >Accepted</option>
								<option value=<cfoutput>#APPT_REMINDER_DECLINED#</cfoutput> >Declined</option>
								<option value=<cfoutput>#APPT_REMINDER_CHANGE_REQUEST#</cfoutput> >Change Request</option>
 								<option value=<cfoutput>#APPT_REMINDER_DO_NOT_SEND#</cfoutput> >Off</option>
 								<option value=<cfoutput>#APPT_REMINDER_SENT#</cfoutput> >Sent</option>
								<!--- <option value=<cfoutput>#APPT_REMINDER_CHAT_ACTIVE#</cfoutput> >Chat Active</option> --->
								<option value=<cfoutput>#APPT_REMINDER_OPT_OUT#</cfoutput> >Opt Out</option>
								<!--- <option value=<cfoutput>#APPT_REMINDER_CHAT_CLOSE#</cfoutput> >Chat Close</option> --->
							</select>
						</div>
						<!--- <div class="form-group tab-chat-calendar">
							 <table style="margin-top: 10px; margin-bottom: 0px;">
					        	<tr>
						        	<td><button type="button" class="button-chat-calendar"><i class="fa fa-comment-o" style ="color:white;" aria-hidden="true"></i> <b>Chat</b></button></td>
					        	</tr>
					        </table>
				        </div> --->
				    </div>
			    </div>

			    <div class="modal-footer">

			        <button type="button" class="fc-button fc-state-default fc-sire-event-button xmlp-btn btn-delete-event" data-dismiss="modal" style="float: left;">Delete</button>
					<button type="button" class="fc-button fc-state-default fc-sire-event-button xmlp-btn btn-resend-event" data-dismiss="modal" style="float: left; margin-left: 8px; display:none;">Resend</button>
			        <button type="button" class="fc-button fc-state-default fc-sire-event-button xmlp-btn btn-save-event" data-dismiss="modal">Save</button>
			        <button type="button" class="fc-button fc-state-default fc-sire-event-button xmlp-btn btn-cancel-event" data-dismiss="modal">Cancel</button>

			    </div>

		    </div>
        </div>
    </div>

</div>

<!--- Modal chat --->
<div class="modal fade" id="chat-modal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
      	<button type="button" class="closeChat" data-dismiss="modal">&times;</button>
        <div class="modal-header">
			<div class="col-md-12">
				<div class="row" id="event_phonenumber">
					<div class="col-md-3">
						<label class="control-label" id ="event_phonenumber_title"><b>Contact:</b></label>
					</div>
					<div class="col-md-9">
						<label class="control-label" id ="event_phonenumber_value"></label>
					</div>
				</div>
				<div class="row" id="event_first_name" style="display:none">
					<div class="col-md-3">
						<label class="control-label" id="event_first_name_title"><b>First name:</b></label>
					</div>
					<div class="col-md-9">
						<label class="control-label" id="event_first_name_value"></label>
					</div>
				</div>
				<div class="row" id="event_last_name" style="display:none">
					<div class="col-md-3">
						<label class="control-label" id="event_last_name_title"><b>Last name:</b></label>
					</div>
					<div class="col-md-9">
						<label class="control-label" id="event_last_name_value"></label>
					</div>
				</div>
				<div class="row" id="event_name" style="display:none">
					<div class="col-md-3">
						<label class="control-label" id="event_name_title"><b>Event name:</b></label>
					</div>
					<div class="col-md-9">
						<label class="control-label" id="event_name_value"></label>
					</div>
				</div>
				<div class="row" id="event_start" style="display:none">
					<div class="col-md-3">
						<label class="control-label" id="event_start_title"><b>Start date:</b></label>
					</div>
					<div class="col-md-9">
						<label class="control-label" id="event_start_value"></label>
					</div>
				</div>
				<div class="row" id="event_end" style="display:none">
					<div class="col-md-3">
						<label class="control-label" id="event_end_title"><b>End date:</b></label>
					</div>
					<div class="col-md-9">
						<label class="control-label" id="event_end_value"></label>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label><b>Notes</b></label>
						<textarea type="text" class="form-control event_chat_notes" onmouseout ="UpdateNoteschat()"  placeholder="Notes…"></textarea>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-md-12">
						<label>Status:</label>
						<select id="inpConfirmationFlag" class="Select2 form-control">
							<option value=<cfoutput>#APPT_REMINDER_QUEUED#</cfoutput> >Scheduled</option>
							<option value=<cfoutput>#APPT_REMINDER_ACCEPTED#</cfoutput> >Accepted</option>
							<option value=<cfoutput>#APPT_REMINDER_DECLINED#</cfoutput> >Declined</option>
							<option value=<cfoutput>#APPT_REMINDER_CHANGE_REQUEST#</cfoutput> >Change Request</option>
							<option value=<cfoutput>#APPT_REMINDER_DO_NOT_SEND#</cfoutput> >Off</option>
							<option value=<cfoutput>#APPT_REMINDER_SENT#</cfoutput> >Sent</option>
							<option value=<cfoutput>#APPT_REMINDER_CHAT_ACTIVE#</cfoutput> >Chat Active</option>
							<option value=<cfoutput>#APPT_REMINDER_OPT_OUT#</cfoutput> >Opt Out</option>
							<option value=<cfoutput>#APPT_REMINDER_CHAT_CLOSE#</cfoutput> >Chat Close</option>
						</select>
					</div>
				</div>
			</div>
        </div>
        <div class="modal-body">
          	<div class="inner-phone-screen clearfix mCustomScrollbar _mCS_1 mCS_no_scrollbar" style ="overflow-y:auto;height:400px;" id="SMSHistoryScreenArea">

			</div>
			<div class="part-type-mess">
				<div class="uk-grid uk-grid-small">
					<div class="uk-width-expand">
						<form id="text-form">
							<div class="form-group form-gd">
								<!--- <input id="SMSTextInputArea" type="text" class="form-control validate[required]" placeholder="Type your message here"/> --->
								<textarea style="height: 34px; resize: none; width:99%" name="" id="SMSTextInputArea" class="form-control validate[required]" placeholder="Type your message here" ></textarea><!---disabled--->
							</div>
						</form>
					</div>
					<!---
					<div class="uk-width-auto">
						<div style="text-align: left; padding: 0px; margin: 0px; width: 50%; valign: left;"><button type="button" class="btn green-gd btn-endchat">End chat</button></div>
						<div style="text-align: right; padding: 0px; margin: 0px;width: 50%; valign: right;"><button id="SMSSend" type="submit" class="btn green-gd">Send</button></div>
					</div>
					--->
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6" style="text-align: left;">
					<button type="button" class="btn new-blue-gd btn-endchat">End chat</button>
				</div>
				<div class="col-xs-6" style="text-align: right; padding-right:25px;">
					<button id="SMSSend" type="submit" class="btn green-gd">Send</button>
				</div>
			</div>
        </div>
        <div class="modal-footer">
			<div class="col-xs-12">
				<span class="custom-checkbox">
					<input type="checkbox" id="enter-to-send" value="1" <cfif inpEnterToSend EQ 1>Checked</cfif>>
					<label for="enter-to-send" style="margin-right: 25px">
						Enter to send
					</label>
				</span>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
        </div>
      </div>

    </div>
</div>
<!--- Modal Customer respone --->
<!--- MODAL POPUP PREVIEW--->
    <div class="bootbox modal fade" id="previewCampaignModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Respone History</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <!--- <button type="button" class="btn btn-primary btn-success-custom" id="btn-rename-campaign">Save</button> --->
                    <button type="button" class="btn btn-default btn-back-custom" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


<cfparam name="variables._title" default="Sire Calendar">
<cfinclude template="/session/sire/views/layouts/master.cfm">


<!--- https://fullcalendar.io --->
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js'></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.css"/>
<!--- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.print.css"/>  --->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>


<!--- http://jonthornton.github.io/jquery-timepicker/ --->
<!--- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script> --->
<link rel="stylesheet" href="/session/sire/assets/global/css/timepicker/jquery.timepicker.css" />
<script src="/session/sire/assets/global/plugins/timepicker/jquery.timepicker.js"></script>

<!--- http://jonthornton.github.io/jquery-datepicker/ --->
<!--- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script> --->
<script src="/session/sire/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min-4.7.14.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" />
<link rel="stylesheet" href="/public/sire/css/select2-bootstrap.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.3/socket.io.js"></script>

<cfscript>
     CreateObject("component","public.sire.models.helpers.layout")
          .addJs("https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js")
          .addCss("https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css")
          .addCss("/session/sire/assets/global/plugins/jquery-mcustomscrollbar/jquery.mCustomScrollbar.css", true)
          .addCss("/session/sire/assets/global/plugins/uikit/css/uikit3.css", true)
          .addJs("/session/sire/assets/global/plugins/jquery-mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js", true)
          .addJs("/session/sire/assets/global/plugins/uikit/js/uikit.min.js", true)
          .addCss("/session/sire/assets/layouts/layout4/css/custom3.css", true)
          .addCss("/session/sire/assets/layouts/layout4/css/custom.css", true)
          .addJs("/session/sire/assets/pages/scripts/sms_ws.js");

          // .addJs("/session/sire/assets/global/css/timepicker/jquery.timepicker.css", true)
          // .addJs("/session/sire/assets/global/plugins/timepicker/jquery.timepicker.js", true);
</cfscript>

<!---
	sms_ws.js - web socket stuff

--->


<cfinclude template="/public/sire/configs/paths.cfm"/>
<!---


	$('#yourCalendar').fullCalendar('unselect');

	unselect: function(){
    $('#submitButton').unbind();
},


	--->

<!--- documented javascript inlcuded from this page --->
<cfinclude template="/session/sire/pages/calendar/inc_appointments.cfm"/>
