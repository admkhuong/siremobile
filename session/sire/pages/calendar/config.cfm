<cfparam name="cid" default="1" />

<cfparam name="inpcontactstring" default="888"/>
<cfparam name="inpbatchId" default="1"/>
<cfparam name="emai" default=""/>
<cfparam name="phone" default=""/>

<!--- auto create a camapign for user first uses --->
<cfinvoke component="session.sire.models.cfc.calendar" method="SetCalendarConfigurationDefault" returnvariable="RetValSetCalendarConfigurationDefault"></cfinvoke>
<cfif RetValSetCalendarConfigurationDefault.RXRESULTCODE GT 0>
	<cfset cid = RetValSetCalendarConfigurationDefault.CALENDARID>
	<cfset inpbatchId = RetValSetCalendarConfigurationDefault.BATCHID>
</cfif>

<cfinvoke component="session.sire.models.cfc.calendar" method="GetCalendarConfiguration" returnvariable="RetValGetCalendarConfiguration">
	<cfinvokeargument name="inpCalendarId" value="#cid#">
</cfinvoke>

<!--- <cfinvoke method="GetSchedule" component="session.cfc.schedule" returnvariable="ScheduleByBatchId">
	<cfinvokeargument name="INPBATCHID" value="#RetValGetCalendarConfiguration.BATCHID#">
</cfinvoke>

<cfdump var='#RetValGetCalendarConfiguration#'/>
<cfdump var='#ScheduleByBatchId#'/> --->

<cfinvoke component="session.sire.models.cfc.calendar" method="GetUserInfo" returnvariable="RetValGetUserInfo">
	<cfinvokeargument name="inpUserId" value="#session.UserId#">
</cfinvoke>

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addJs("/public/sire/js/moment.min.js", true)
        .addCss("/session/sire/css/calendar_config.css")
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)
        .addJs("/public/sire/js/select2.min.js", true)
        // .addJs("/session/sire/assets/global/plugins/bootstrap-datetimepicker/js/jquery.timepicker.min.js ", true)
        // .addCss("/public/sire/css/jquery.timepicker.min.css", true)
        .addJs("/session/sire/assets/pages/scripts/calendar-config.js")
        // .addCss("/public/sire/css/jquery.timepicker.min.css", true)
		.addCss("/session/sire/assets/layouts/layout4/css/custom.css", true);
</cfscript>

<!---
	Notes:
	Tasks:

	Test on Distribution Id 9 so production does not get confused
	http://dev.siremobile.com/management/processing/act_sire_processcontactqueue.cfm?inpDistributionProcessId=9&inpNumberOfDialsToRead=1&inpMod=1&DoModValue=0&inpVerboseDebug=1

--->

<!--- fix for Font Awesome 5 Free --->
<style>

	.webfont_scrolltime
	{
		margin-left: 1em;
	}

	.webfont_scrolltime:before {
	    font-family: "Font Awesome 5 Free";
	}

	.divTable{
		display: table;
		width: 100%;
	}
	.divTableRow {
		display: table-row;
	}
	.divTableHeading {
		background-color: #EEE;
		display: table-header-group;
	}
	.divTableCell, .divTableHead {
		border: none;
		display: table-cell;
		padding: 3px 10px;
	}
	.divTableCellRight
	{
		text-align: right;
	}

	.divTableHeading {
		background-color: #EEE;
		display: table-header-group;
		font-weight: bold;
	}
	.divTableFoot {
		background-color: #EEE;
		display: table-footer-group;
		font-weight: bold;
	}
	.divTableBody {
		display: table-row-group;
	}

	#CalendarConfigBatchSelect
	{
		margin: .5em 0;
	}

	.Nav2Redir {
	    line-height: 34px;
	    float: right;
	}

	.AddNewNav2Redir {
	    line-height: 34px;
	    float: right;
	    margin-left: .5em;
	}

	.RefreshNav2Redir {
	    line-height: 34px;
	    float: right;
	    margin-left: .5em;
	}

	.RefreshNav2Redir:hover, .AddNewNav2Redir:hover, .Nav2Redir:hover
	{
	     color: #000;
	     cursor:pointer;;
	}

	.BatchSelectContainer .select2
	{
		margin-left: .5em;
		margin-right: -1em;
		float: right;
	}

</style>


<div class="portlet light bordered">

	<div class="row">
       <div class="col-xs-12 col-md-12">
	   <h2 style="float: right; margin: 8px;">
			<i class="fa fa-cogs" aria-hidden="true"></i>
	   </h2>
	       <h2>Calendar Configuration</h2>
       </div>
   </div>
</div>

<div class="portlet">
	<div class="row">
		<div class="col-xs-12 col-md-6">
			<div style="display: table; padding: 0 .5em; margin: 0 auto; height: 34px; background-color: #fff; width: 100%; border-radius: 3px;">
				<div class="col-xs-6 config-title">
					Auto Schedule Reminder Message
				</div>
				<div class="col-xs-6" style="text-align: right; display: table-cell; vertical-align: middle;">
					<label class="toggle-switch">
						<input id="ASR" type="checkbox" <cfif RetValGetCalendarConfiguration.ASR GT 0>checked</cfif>>
						<span class="toggle-slider round"></span>
					</label>
				</div>
			</div>
		</div>
	</div>

	<div class="row" style="margin-top: 1em;">

		<div class="col-xs-12 col-md-6">
			<div style="display: table; padding: .3em .5em 0 .5em; margin: 0 auto; height: 43px; background-color: #fff; width: 100%; border-radius: 3px;">
				<div class="col-xs-6 config-title">
					Calendar Time Zone
				</div>

				<div style="text-align: right; display: table-cell; vertical-align: middle; color: #000; font-weight: 500; width: 100px;">
					<select id="inpTimeZone" name="inpTimeZone" class ="form-control Select2">
						<option value="34" <cfif RetValGetCalendarConfiguration.TIMEZONE EQ '34'>selected</cfif>>Hawaii</option>
						<option value="32" <cfif RetValGetCalendarConfiguration.TIMEZONE EQ '32'>selected</cfif>>Alaska</option>
						<option value="31" <cfif RetValGetCalendarConfiguration.TIMEZONE EQ '31'>selected</cfif>>Pacific</option>
						<option value="30" <cfif RetValGetCalendarConfiguration.TIMEZONE EQ '30'>selected</cfif>>Mountain</option>
						<option value="29" <cfif RetValGetCalendarConfiguration.TIMEZONE EQ '29'>selected</cfif>>Central</option>
						<option value="28" <cfif RetValGetCalendarConfiguration.TIMEZONE EQ '28'>selected</cfif>>Eastern</option>
					</select>
				</div>

			</div>

		</div>
	</div>

	<div class="row" style="margin-top: 1em;">

		<div class="col-xs-12 col-md-6">
			<div style="display: table; padding: .3em .5em 0 .5em; margin: 0 auto; height: 43px; background-color: #fff; width: 100%; border-radius: 3px;">
				<div class="col-xs-6 config-title">
					Calendar Reminder Campaign
				</div>

				<div class="col-xs-6 BatchSelectContainer" style="text-align: right;">

					<select id="CalendarConfigBatchSelect" class="form-control Select2 BatchSelect BatchList" data-width="80%">
							<!--- Handle case where there is no data yet --->
							<option value="0" selected>-- Loading Data ... --</option>
					</select>

					<span class="RefreshNav2Redir" id="RefreshNav2Redir">
						<i class="fas fa-sync-alt"></i>
					</span>

					<span class="AddNewNav2Redir" id="AddNewNav2Redir">
						<i class="fas fa-plus"></i>
					</span>

					<span class="Nav2Redir" id="Nav2Redir">
						<i class="fas fa-link"></i>
					</span>



				</div>
			</div>

		</div>
	</div>

	<div class="row" style="margin-top: 1em;">
		<div class="col-xs-12 col-md-6">

			<div style="display: table; padding: .3em .5em 0 .5em; margin: 0 auto; height: 34px; background-color: #fff; width: 100%; border-radius: 3px;">
				<div class="col-xs-6 config-title">
					Calendar Default Display
				</div>

				<div class="divTable">
					<div class="divTableBody">
						<div class="divTableCell divTableCellRight">
							Default Time Display
							<label class="webfont_scrolltime">
								<input type="text" id="TimeStepScroll" class="time ui-timepicker-input TimeStepScroll form-control" autocomplete="off" style=" width: 103px; display: inline; border: 1px solid #e5e5e5; text-align: right;">
							</label>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

	<div class="row" style="margin-top: 1em;">
		<div class="col-xs-12 col-md-6">
			<div style="display: table; padding: .3em .5em 0 .5em; margin: 0 auto; height: 34px; background-color: #fff; width: 100%; border-radius: 3px;">
				<div class="col-xs-6 config-title">
					Reminder Schedule Window
				</div>

				<div class="divTable">
					<div class="divTableBody">
						<div class="divTableRow">

							<div class="divTableCell divTableCellRight">
								Start Time
								<label class="webfont_scrolltime">
									<input type="text" id="StartTimeStepScroll" class="time ui-timepicker-input StartTimeStepScroll form-control" autocomplete="off" style=" width: 103px; display: inline; border: 1px solid #e5e5e5; text-align: right;">
								</label>
							</div>
						</div>

						<div class="divTableRow">

							<div class="divTableCell divTableCellRight">
								End Time
								<label class="webfont_scrolltime">
									<input type="text" id="EndTimeStepScroll" class="time ui-timepicker-input EndTimeStepScroll form-control" autocomplete="off" style=" width: 103px; display: inline; border: 1px solid #e5e5e5; text-align: right;">
								</label>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>

	</div>

	<div class="row" style="margin-top: 1em;">

		<div class="col-xs-12 col-md-6">
			<div style="display: table; padding: .3em .5em 0 .5em; margin: 0 auto; height: 43px; background-color: #fff; width: 100%; border-radius: 3px;">
				<div class="col-xs-6 config-title">
					Reminder Sent Before Scheduled Appointment
				</div>

				<div id="IntervalValue" style="text-align: right; display: table-cell; vertical-align: middle; color: #000; font-weight: 500; padding-right: 1em; width: 100px;">
					<select name="VALUE" class ="form-control Select2">
						<option value="1" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '1'>selected</cfif>>1</option>
						<option value="2" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '2'>selected</cfif>>2</option>
						<option value="3" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '3'>selected</cfif>>3</option>
						<option value="4" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '4'>selected</cfif>>4</option>
						<option value="5" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '5'>selected</cfif>>5</option>
						<option value="6" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '6'>selected</cfif>>6</option>
						<option value="7" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '7'>selected</cfif>>7</option>
						<option value="8" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '8'>selected</cfif>>8</option>
						<option value="9" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '9'>selected</cfif>>9</option>
						<option value="10" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '10'>selected</cfif>>10</option>
						<option value="11" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '11'>selected</cfif>>11</option>
						<option value="12" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '12'>selected</cfif>>12</option>
						<option value="13" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '13'>selected</cfif>>13</option>
						<option value="14" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '14'>selected</cfif>>14</option>
						<option value="15" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '15'>selected</cfif>>15</option>
						<option value="16" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '16'>selected</cfif>>16</option>
						<option value="17" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '17'>selected</cfif>>17</option>
						<option value="18" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '18'>selected</cfif>>18</option>
						<option value="19" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '19'>selected</cfif>>19</option>
						<option value="20" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '20'>selected</cfif>>20</option>
						<option value="21" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '21'>selected</cfif>>21</option>
						<option value="22" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '22'>selected</cfif>>22</option>
						<option value="23" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '23'>selected</cfif>>23</option>
						<option value="24" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '24'>selected</cfif>>24</option>
					</select>
				</div>

				<div id="IntervalType" style="text-align: right; display: table-cell; vertical-align: middle; color: #000; font-weight: 500; width: 100px;">
					<select name="ITYPE" class ="form-control Select2">
						<option value="HOUR" <cfif RetValGetCalendarConfiguration.INTERVALTYPE EQ 'HOUR'>selected</cfif>>Hour(s)</option>
						<option value="DAY" <cfif RetValGetCalendarConfiguration.INTERVALTYPE EQ 'DAY'>selected</cfif>>Day(s)</option>
					</select>
				</div>

			</div>

		</div>
	</div>

	<div class="row" style="margin-top: 1em;">
		<div class="col-xs-12 col-md-6">
		</div>
		<div class="col-xs-12 col-md-6">
			<!--- <div style="display: table; padding: .3em .5em 0 .5em; margin: 0 auto; height: 45px; background-color: #fff; width: 100%; border-radius: 3px;">
			    <div class="col-xs-7 config-title">
			        Sync Google Calendar
			    </div>
			    <div class="col-xs-5" style="text-align: right; display: table-cell; vertical-align: middle;">
			    	 <a id="signin-button" class="btn green-gd btn-re">Sync</a>
			    </div>
			</div>	 --->
       </div>
	</div>
	<!--- Back to Sire Calendar --->
	<div class="row" style="margin-top: 1em;">
       <div class="col-xs-12 col-md-6">
			<a href="/session/sire/pages/calendar/appointments<cfif cid NEQ 1>?cid=<cfoutput>#cid#</cfoutput></cfif>" class="btn green-gd btn-re">Back to Sire Calendar</a>
       </div>
	</div>
</div>

<input type="text" id="nextPageToken" style ="display:none;">

<cfparam name="variables._title" default="Sire Calendar Configuration">
<cfinclude template="/session/sire/views/layouts/master3.cfm">



<!--- http://jonthornton.github.io/jquery-timepicker/ --->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script>


<!--- Include javascript here for better cf documentation --->
<script type="application/javascript">

	var appBatchId = '<cfoutput>#RetValGetCalendarConfiguration.BATCHID#</cfoutput>';

	var clientId = '20348249418-cpq9052uguml07j3evagt0f2mv7fejb6.apps.googleusercontent.com';
	var apiKey = 'AIzaSyC4Hel1V9G57UDYm7068M2xlRkcPwlPb4c';
	var eventObj =  { 'eventName':"", 'startDate':"", 'endDate':"", 'status':""}
	var listEvent = new Array();
	var nextPageToken = '';
	var listGoogleId = [];
	var CalendarConfigurationInterval = 'INTERVAL 1 DAY';
	var CalendarConfigurationIntervalResend = '';

	var today = new Date();
	var nextday = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;
	var mmnext = mm + 1;
	var yyyy = today.getFullYear();
	var yyyynext = today.getFullYear();

	if(dd<10) {
		dd = '0'+dd;
	}
	if(mm<10) {
		mm = '0'+mm;
	}

	if(mm==13){
		mm = '01';
		yyyy = yyyy + 1;
	}

	if(mmnext<10) {
		mmnext = '0'+mmnext;
	}

	if(mmnext==13){
		mmnext = '01';
		yyyynext = yyyynext + 1;
	}

	if(mmnext==14){
		mmnext = '02';
		yyyynext = yyyynext + 1;
	}
	today = yyyy+'-'+mm + '-' + dd + 'T00:00:00Z';
	nextday = yyyynext+'-'+mmnext + '-' + dd + 'T00:00:00Z';
	var options = {
	'calendarId': 'primary',
	'singleEvents': true,
	'maxResults':100,
	//'pageToken': 'CigKGjMzNzFyZGwwNGEyNGt1cDY4bG9jdjgybnN0GAEggICAit_724QWGg0IABIAGMDZw_3cqdgC',
	'timeMin': today,
	'timeMax': nextday,
	}
	var noti_method = '<cfoutput>#RetValGetCalendarConfiguration.NOTIFIMETHOD#</cfoutput>';

	<!--- On page loaded --->
	$(function() {

		// INTERVAL RESEND
		$("#saveInterval").on('click',function(){
			saveInterval(1);
		})

		function saveInterval(auto){
			if( parseInt(appBatchId) > 0)
			{
				<!--- update interval only --->
				if(auto === 1){
					ControlPointData.IMRNR = (inpCPE.find('#IMRNR').val() == null) ? "0" : inpCPE.find('#IMRNR').val();
					ControlPointData.ITYPE = (inpCPE.find('#ITYPE').val() == null) ? "0" : inpCPE.find('#ITYPE').val();
					ControlPointData.IVALUE = (inpCPE.find('#IVALUE').val() == null) ? "0" : inpCPE.find('#IVALUE').val();

					CalendarConfigurationIntervalResend = "INTERVAL " + ControlPointData.IVALUE + " " + ControlPointData.ITYPE + "; " + ControlPointData.IMRNR;
					SaveIntervalResend();
				}
				else{
					ControlPointData.IMRNR = 0;
					ControlPointData.ITYPE = 0
					ControlPointData.IVALUE = 0;

					CalendarConfigurationIntervalResend = "";
					SaveIntervalResend();
				}

				<!--- Save to XMLControlString --->
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url: '/session/sire/models/cfc/control-point.cfc?method=CP_Save&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					dataType: 'json',
					async: false,
					data:
					{
						INPBATCHID : appBatchId,
						controlPoint : JSON.stringify(ControlPointData),
						inpSimpleViewFlag : 1,
						inpTemplateFlag : 0
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						<!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},
					success:
						<!--- Default return function for Async call back --->
					function(d)
					{
						<!--- RXRESULTCODE is 1 if everything is OK --->
						if (d.RXRESULTCODE == 1)
						{
							bootbox.dialog({
							    message: "Update auto schedule success.",
							    title: "SIRE",
							    buttons: {
							        success: {
							            label: "Ok",
							            className: "btn btn-medium btn-success-custom",
							            callback: function() {
							            }
							        }
							    }
							});
						}
						else
						{
							<!--- No result returned --->
							bootbox.alert("General Error processing your request.");
						}

					}

				});
			}
		}

		$(".Select2").select2( { theme: "bootstrap"} );

		<!--- inplace editing of description  see http://jsfiddle.net/egstudio/aFMWg/1/ --->
		var replaceWith = $('<input name="temp" type="text" style="color: #000; display: inline; width:100%; box-sizing: border-box; text-align:left; padding-left:2em;" />'),
		connectWith = $('input[name="DefaultEventTitle"]');

		$('#DefaultEventTitleDisplay').inlineEdit(replaceWith, connectWith);

		<!--- Bind checkbox for ASR - auto save changes--->
		$('#ASR').change(function(){
			// if($("#ASR").is(':checked')){
			// 	$(".setInterval").removeClass('hidden');
			// }
			// else{
			// 	$(".setInterval").addClass('hidden');
			// 	saveInterval(0);
			// }

			SaveASR();
		});

		$('#IntervalResend').change(function(){
			if($("#IntervalResend").is(':checked')){
				$(".setInterval").removeClass('hidden');
			}
			else{
				$(".setInterval").addClass('hidden');
				saveInterval(0);
			}

			//SaveASR();
		});


		if('<cfoutput>#TRIM(RetValGetCalendarConfiguration.SCROLLTIME)#</cfoutput>' != '')
		{
			var inpScrollMoment = moment('<cfoutput>#RetValGetCalendarConfiguration.SCROLLTIME#</cfoutput>', 'HH:mm:ss');
			$('#TimeStepScroll').val(inpScrollMoment.format("HH:mm A"));

		}

		$('#TimeStepScroll').timepicker({
			'step': 15,
			'scrollDefault': '9:00 AM',
			'timeFormat': 'h:i A'
		});

		$('#TimeStepScroll').change(function(){


			if($('#TimeStepScroll').val() != "")
			{

				//console.log($('#TimeStepScroll').val());
				<!--- Convert to Moments for formating option--->
				var s = moment($('#TimeStepScroll').val(), 'HH:mm A');

			  	SaveScrollTime(s.format("HH:mm:ss"));
			}

		});

		$('#inpTimeZone').change(function(){

			var s = $('#inpTimeZone').val();

			if(s == "")
				s = 31;

		  	SaveTimeZone(s);

		});

		var inpStartMoment = moment('<cfoutput>#RetValGetCalendarConfiguration.BSTART#</cfoutput>', 'HH:mm:ss');
		$('#StartTimeStepScroll').val(inpStartMoment.format("HH:mm A"));

		var inpEndMoment = moment('<cfoutput>#RetValGetCalendarConfiguration.BEND#</cfoutput>', 'HH:mm:ss');
		$('#EndTimeStepScroll').val(inpEndMoment.format("HH:mm A"));


		// Setting for start/end time
		$('#StartTimeStepScroll').timepicker({
			'step': 60,
			'scrollDefault': inpStartMoment.format("HH:mm A"),
			'timeFormat': 'h:i A',
			'minTime': '8:00 AM',
			'maxTime': '9:00 PM'
		});

		$('#EndTimeStepScroll').timepicker({
			'step': 60,
			'scrollDefault': inpEndMoment.format("HH:mm A"),
			'timeFormat': 'h:i A',
			'minTime': '8:00 AM',
			'maxTime': '9:00 PM'
		});



		$('#StartTimeStepScroll').change(function(){

			if($('#StartTimeStepScroll').val() != "")
			{
				var s = moment($('#StartTimeStepScroll').val(), 'HH:mm A');
			  	var res = s.format("HH:mm:ss").split(":");
				var hourInput = parseInt(res[0]);

				// Check with start time
				var sEnd = moment($('#EndTimeStepScroll').val(), 'HH:mm A');
			  	var resEnd = sEnd.format("HH:mm:ss").split(":");
				var endInput = parseInt(resEnd[0]);

				// if(hourInput == 0)									{
				// 	hourInput =  12;
				// }
				// else if(hourInput == 12)									{
				// 	hourInput =  24;
				// }
				if(hourInput < 8 || hourInput > 21){
					alertBox('Start time must be between 08:00 AM and 09:00 PM. End Time will be reset by the Start time!','Invalid!');
					// reSetStartTime();
					// SaveBatchScheduleTime(sEnd.format("HH:mm:ss"), 0);
					return;
				}
				if(parseInt(endInput) < hourInput){
					alertBox('Warning Start time must be less than End time. ','Invalid!');

					// reSetStartTime();

					// SaveBatchScheduleTime(sEnd.format("HH:mm:ss"), 0);
					return;
				}

			  	SaveBatchScheduleTime(s.format("HH:mm:ss"), 0);
			}

		});
		$('#EndTimeStepScroll').change(function(e){
			if($('#EndTimeStepScroll').val() != "")
			{
				//validate for EndTime
				var s = moment($('#EndTimeStepScroll').val(), 'HH:mm A');
			  	var res = s.format("HH:mm:ss").split(":");
				var hourInput = parseInt(res[0]);
				// Check with start time
				var sStart = moment($('#StartTimeStepScroll').val(), 'HH:mm A');
			  	var resStart = sStart.format("HH:mm:ss").split(":");
				var startInput = parseInt(resStart[0]);

				// if(hourInput == 0)									{
				// 	hourInput =  12;
				// }
				// else if(hourInput == 12)									{
				// 	hourInput =  24;
				// }

				if(hourInput < 9 || hourInput > 21){
					alertBox('End time must be between 09:00 AM and 09:00 PM. End Time will be reset by the Start time!','Invalid!');
					reSetEndTime();
					SaveBatchScheduleTime(sStart.format("HH:mm:ss"), 1);
					return;
				}
				if(parseInt(startInput) > hourInput){

					alertBox('Warning End time must be greater than Start time.','Invalid!');
					// reSetEndTime();
					// SaveBatchScheduleTime(sStart.format("HH:mm:ss"), 1);
					return;
				}

			  	SaveBatchScheduleTime(s.format("HH:mm:ss"), 1);
			}

		});
		function reSetEndTime(){
			var startTime = moment($('#StartTimeStepScroll').val(), 'HH:mm A');
			var res = startTime.format("HH:mm:ss").split(":");
			var showTime = parseInt(res[0]);

			var showTimeType = "AM";
			if(parseInt(showTime) > 12){
				showTime = parseInt(showTime) - 12;
				showTimeType = "PM";
			}
			else if (parseInt(showTime) == 0){
				showTime = 12;
			}
			var inpScrollMoment = moment(showTime, 'HH:mm:ss');
			$('#EndTimeStepScroll').val(inpScrollMoment.format('HH:mm ') + showTimeType);
		}
		function reSetStartTime(){
			var endTime = moment($('#EndTimeStepScroll').val(), 'HH:mm A');
			var res = endTime.format("HH:mm:ss").split(":");
			var showTime = parseInt(res[0]);

			var showTimeType = "AM";
			if(parseInt(showTime) > 12){
				showTime = parseInt(showTime) - 12;
				showTimeType = "PM";
			}
			else if (parseInt(showTime) == 0){
				showTime = 12;
			}
			var inpScrollMoment = moment(showTime, 'HH:mm:ss');
			$('#StartTimeStepScroll').val(inpScrollMoment.format('HH:mm ') + showTimeType);
		}


		// End setting for start/end time

	});

	<!--- http://jsfiddle.net/egstudio/aFMWg/1/ --->
	$.fn.inlineEdit = function(replaceWith, connectWith) {

	    $(this).hover(function() {
	        $(this).addClass('hover');
	    }, function() {
	        $(this).removeClass('hover');
	    });

	    $('#DefaultEventTitleDisplay, #edit-default-title').on("click touchstart", function(e){

	        var elem = $('#DefaultEventTitleDisplay');

	        elem.hide();
	        $('#edit-default-title').hide();
	        elem.after(replaceWith);

	        if(elem.text() != 'blank')
	        	replaceWith.val(elem.text());
	        else
	        	replaceWith.val('');

	        replaceWith.focus();

	        replaceWith.blur(function() {

	            if ($(this).val() != "")
	            {
	                connectWith.val($(this).val()).change();
	                elem.text($(this).val());
	            }
	            else
	            {
		            if($(this).val() == 'blank')
		            	$(this).val('');

		            connectWith.val($(this).val()).change();
	                elem.text('blank');
	            }

	            $(this).remove();
	            elem.show();
	            $('#edit-default-title').show();
	        });

	        <!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();

	    });
	};
	<!--- CalendarConfig --->

	$(document).on("change", "#IntervalType, #IntervalValue" , function(){
		CalendarConfigurationInterval = "INTERVAL " + $('select[name=VALUE]').val() + " " + $('select[name=ITYPE]').val();
		SaveInterval();
	});

	function SaveInterval()
	{
		<!--- Save interval time to send reminder --->
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/calendar.cfc?method=SaveConfigInterval&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			async: true,
			data:
			{
				inpCalendarId : '<cfoutput>#cid#</cfoutput>',
				inpInterval : CalendarConfigurationInterval

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },
			success:
			<!--- Default return function for call back --->
			function(d)
			{
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1)
				{
				}
				else
				{
					<!--- No result returned --->
					if(d.ERRMESSAGE != "")
						alertBox("Error while saving changes to Notification Reminder setting","ERROR");
				}
			}

		});

	}

	$(document).on("change", "#ResendIntervalType, #ResendIntervalValue" , function(){
		CalendarConfigurationIntervalResend = "INTERVAL " + $('select[name=RESENDVALUE]').val() + " " + $('select[name=RESENDTYPE]').val();
		SaveIntervalResend();
	});

	function SaveIntervalResend()
	{
		<!--- Save interval time to send reminder --->
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/calendar.cfc?method=SaveConfigIntervalResend&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			async: true,
			data:
			{
				inpCalendarId : '<cfoutput>#cid#</cfoutput>',
				inpInterval : CalendarConfigurationIntervalResend

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },
			success:
			<!--- Default return function for call back --->
			function(d)
			{
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1)
				{
				}
				else
				{
					<!--- No result returned --->
					if(d.ERRMESSAGE != "")
						alertBox("Error while saving changes to Interval Resend Reminder setting","ERROR");
				}
			}

		});

	}


	function SaveASR()
	{
		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/calendar.cfc?method=SaveConfigAutoRemind&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			async: true,
			data:
			{
				inpCalendarId : '<cfoutput>#cid#</cfoutput>',
				inpAutoRemind : $('#ASR').is(':checked')

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },
			success:
			<!--- Default return function for call back --->
			function(d)
			{
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1)
				{


				}
				else
				{
					<!--- No result returned --->
					if(d.ERRMESSAGE != "")
						alertBox("Error while saving changes to Auto Schedule Reminder setting","ERROR");
				}
			}

		});

	}

	function SaveScrollTime(inpScrollTime)
	{
		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/calendar.cfc?method=SaveConfigScrollTime&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			async: true,
			data:
			{
				inpCalendarId : '<cfoutput>#cid#</cfoutput>',
				inpScrollTime : inpScrollTime

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> alertBox("No Response from the remote server. Check your connection and try again.","ERROR!"); },
			success:
			<!--- Default return function for call back --->
			function(d)
			{
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1)
				{


				}
				else
				{
					<!--- No result returned --->
					if(d.ERRMESSAGE != "")
						bootbox.alert("Error while saving changes to defualt scroll time setting");
				}
			}

		});
	}

	function SaveTimeZone(inpTimeZone)
	{
		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/calendar.cfc?method=SaveConfigTimeZone&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			async: true,
			data:
			{
				inpCalendarId : '<cfoutput>#cid#</cfoutput>',
				inpTimeZone : inpTimeZone

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> alertBox("No Response from the remote server. Check your connection and try again.","ERROR!"); },
			success:
			<!--- Default return function for call back --->
			function(d)
			{
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1)
				{


				}
				else
				{
					<!--- No result returned --->
					if(d.ERRMESSAGE != "")
						bootbox.alert("Error while saving changes to defualt scroll time setting");
				}
			}

		});
	}

	function SaveConfigBatchId(inpBatchId, inpStartHour, inpEndHour)
	{
		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/calendar.cfc?method=SaveConfigBatchId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			async: true,
			data:
			{
				inpCalendarId : '<cfoutput>#cid#</cfoutput>',
				inpBatchId : inpBatchId,
				inpStartHour : inpStartHour,
				inpEndHour : inpEndHour
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> alertBox("No Response from the remote server. Check your connection and try again.","ERROR!"); },
			success:
			<!--- Default return function for call back --->
			function(d)
			{
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1)
				{
					appBatchId = inpBatchId;
				}
				else
				{
					<!--- No result returned --->
					if(d.ERRMESSAGE != "")
						bootbox.alert("Error while saving changes to defualt scroll time setting");
				}
			}

		});
	}

	function SaveBatchScheduleTime(inpInputTime, type)
	{
		if(inpInputTime != ""){
			var res = inpInputTime.split(":");
			var hourInput = parseInt(res[0]);
			<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/calendar.cfc?method=SaveConfigBatchScheduleTime&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType: 'json',
				async: true,
				data:
				{
					inpBatchId : appBatchId,
					inpBatchScheduleTime : hourInput,
					inpType: type

				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> alertBox("No Response from the remote server. Check your connection and try again.","ERROR!"); },
				success:
				<!--- Default return function for call back --->
				function(d)
				{
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1)
					{
					}
					else
					{
						<!--- No result returned --->
						if(d.ERRMESSAGE != "")
							bootbox.alert("Error while saving changes to defualt scroll time setting");
					}
				}
			});
		}
	}
</script>



<!--- Final page load actions - do this stuff last so less F.O.U.C. problems--->
<script type="application/javascript">

	var BatchListBox = null;


	$(function()
     {

		<!--- Less trips to server and DB - requires send array of select boxes to populate --->
		var	BatchSelectObjs = [];

		<!--- Less trips to server and DB - requires send array of select boxes to populate --->
		BatchSelectObjs.push($('#CalendarConfigBatchSelect'));

		PopulateBatchSelectBox(BatchSelectObjs);

		$('#CalendarConfigBatchSelect').val('<cfoutput>#RetValGetCalendarConfiguration.BATCHID#</cfoutput>').change

		<!--- Section Settings Items --->
		$('#CalendarConfigBatchSelect').change(function() {

			<!--- Get start time --->
			var s = moment($('#StartTimeStepScroll').val(), 'HH:mm A');
			var res = s.format("HH:mm:ss").split(":");
			var inpStartHour = parseInt(res[0]);

			<!--- Get end time --->
			var sEnd = moment($('#EndTimeStepScroll').val(), 'HH:mm A');
			var resEnd = sEnd.format("HH:mm:ss").split(":");
			var inpEndHour = parseInt(resEnd[0]);

			SaveConfigBatchId($('#CalendarConfigBatchSelect').val(), inpStartHour, inpEndHour);

		});


		$("#Nav2Redir").click(function()
		{
			<!--- Read in Batchid from the target --->
			var targetBatch = $(this).parent().find('#CalendarConfigBatchSelect').val();

			if(targetBatch > 0)
			{

				<!--- Verify Batch existis and is active --->
				<!--- Look for new Batch Option --->
				<!--- Allow to navigate back - Switch to Parent tab? --->
				var TargetWindow = 	window.open('', 'CampaignEdit-' + targetBatch);

				<!--- Only load URL once incase work is in progress --->
				if(TargetWindow.location.href.search('campaignid=' + targetBatch) == -1 )
					window.open('/session/sire/pages/campaign-edit?campaignid=' + targetBatch, 'CampaignEdit-' + targetBatch);
			}
		});


		$("#AddNewNav2Redir").click(function()
		{
			// window.open('/session/sire/pages/campaign-template-picker', '');
			window.open('/session/sire/pages/campaign-edit?templateid=169', '')

			<!--- Wait 10 seconds and then auto refresh campaign list to pick up any new campaign options --->
			setTimeout(function()
				{
					BatchListBox = null;
					<!--- Less trips to server and DB - requires send array of select boxes to populate --->
					var	BatchSelectObjs = [];

					<!--- Less trips to server and DB - requires send array of select boxes to populate --->
					BatchSelectObjs.push($('#CalendarConfigBatchSelect'));

					PopulateBatchSelectBox(BatchSelectObjs);
				}
				, 5000);

		});


		$("#RefreshNav2Redir").click(function()
		{
			BatchListBox = null;
			<!--- Less trips to server and DB - requires send array of select boxes to populate --->
			var	BatchSelectObjs = [];

			<!--- Less trips to server and DB - requires send array of select boxes to populate --->
			BatchSelectObjs.push($('#CalendarConfigBatchSelect'));

			PopulateBatchSelectBox(BatchSelectObjs);
		});




	});

	<!--- Becuase these buttons can be dynamically added - set click method here each time they are added --->
	<!--- BOQ should only load real questions with answers - everthing else should load all actions/CPs --->
	function PopulateBatchSelectBox(inpObjs)
	{
		if(BatchListBox != null)
		{
			$.each(inpObjs , function(i, val)
			{

				var PrevSelectVal = inpObjs[i].val();

				<!--- Remove current Questions from Box --->
				inpObjs[i].empty();

				<!--- Only hit DOM once to add all of the options --->
				inpObjs[i].html(BatchListBox.join(''));

				if(PrevSelectVal == "" || PrevSelectVal == 'undefined' || PrevSelectVal == undefined)
					inpObjs[i].val(0);
				else
					inpObjs[i].val(PrevSelectVal);

				inpObjs[i].find('option[value="0"]').text("Enter or Select a Campaign Id");


			});
		}
		else
		{
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/control-point.cfc?method=GetBatchFilteredList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType: 'json',
				async: false,
				data:
				{

				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},
				success:
				<!--- Default return function for Async call back --->
				function(d)
				{
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1)
					{
						<!--- update list send to methods --->
						inpCPListjsVar = d;

						<!--- Store Questions for Select box here --->
						var output = [];

						output.push('<option value="'+ 0 +'">'+ 'Enter or Select a Campaign Id' +'</option>');

						$.each(d.BATCHARRAY.DATA.BATCHID_BI , function(i, val)
						{
							<!--- HTML Encode Text for Display - needs to be 'quote safe' --->
							<!--- Use the Physical - non-changing ID for each reference --->
							output.push('<option value="'+ d.BATCHARRAY.DATA.BATCHID_BI[i] +'">'+ '' + d.BATCHARRAY.DATA.BATCHID_BI[i] + ' ' + d.BATCHARRAY.DATA.DESC_VCH[i] +'</option>');

						});

						<!--- Store here to minimize trips to DB  --->
						BatchListBox = output;

						$.each(inpObjs , function(i, val)
						{
							var PrevSelectVal = inpObjs[i].val();

							<!--- Remove current Questions from Box --->
							inpObjs[i].empty();

							<!--- Only hit DOM once to add all of the options --->
							inpObjs[i].html(output.join(''));

							if(PrevSelectVal == "" || PrevSelectVal == 'undefined' || PrevSelectVal == undefined)
								inpObjs[i].val(0);
							else
								inpObjs[i].val(PrevSelectVal);

							<!--- AI in the UI - Make sure selected value still existis --->
							if(inpObjs[i].val() != PrevSelectVal)
							{
								inpObjs[i].val(0);
							}

							inpObjs[i].find('option[value="0"]').text("Enter or Select a Campaign Id");

						});

					}
					else
					{
						<!--- No result returned --->
						bootbox.alert("General Error processing your request.");
					}

				}

			});
		}
	}
</script>
