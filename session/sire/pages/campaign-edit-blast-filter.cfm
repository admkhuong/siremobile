<cfparam name="datatable_columnModel" default="#Arraynew(1)#">
﻿<cfparam name="datatable_filterId" default="">
﻿<cfparam name="datatable_jsCallback" default="">


<!---validate messages --->
<cfset ISNOTNUMBER = "Is not a number">
<cfset ISNOTDATE = "Is not a date">
<cfset CANNOTBEEMPTY = "Cannot be empty">
<cfset FIELDREQUIRED = "This field is required">

<!---list of data type --->
<cfset LIST = "LIST">
<cfset TEXT = "TEXT">
<cfset DATE = "DATE">
<cfset INTEGER = "INTEGER">
<cfset STATUS = "STATUS">

<!---list of operator --->
<!--- {VALUE='NOT LIKE', DISPLAY='NOT Similar To', INLIST='#DATE#'}, --->
<cfset
	operatorKeys = [
		{VALUE='=', DISPLAY='Is', INLIST='#TEXT#,#INTEGER#,#LIST#,#STATUS#'},
		{VALUE='<>', DISPLAY='Is Not', INLIST='#TEXT#,#INTEGER#,#LIST#'},
		{VALUE='LIKE', DISPLAY='Similar To', INLIST='#TEXT#'},
		{VALUE='>', DISPLAY='Greater Than', INLIST='#TEXT#'},
		{VALUE='<', DISPLAY='Less Than', INLIST='#TEXT#'}
	]>
	
<!---show list of data filter--->
 
<!---list of cf_sql_type --->
<cfset validFieldType = [
			'CF_SQL_BIGINT',
			'CF_SQL_BIT',
			'CF_SQL_CHAR',
			'CF_SQL_BLOB',
			'CF_SQL_CLOB',
			'CF_SQL_DATE',
			'CF_SQL_DECIMAL',
			'CF_SQL_DOUBLE',
			'CF_SQL_FLOAT',
			'CF_SQL_IDSTAMP',
			'CF_SQL_INTEGER',
			'CF_SQL_LONGVARCHAR',
			'CF_SQL_MONEY',
			'CF_SQL_MONEY4',
			'CF_SQL_NUMERIC',
			'CF_SQL_REAL',
			'CF_SQL_REFCURSOR',
			'CF_SQL_SMALLINT',
			'CF_SQL_TIME',
			'CF_SQL_TIMESTAMP',
			'CF_SQL_TINYINT',
			'CF_SQL_VARCHAR'
		]>
<!---build template for filter rows --->
<cfoutput>
	<script id="filter_row_template_#datatable_filterId#" type="text/x-jquery-tmpl"> 
		<div class="datatable_filter_row row filter_row">
			<select class="datatable_filter_dropdownlist" rel="column" onchange="UpdateOperator_#datatable_filterId#(this);">
				<cfloop array="#datatable_ColumnModel#" index="columnItem">
					<option value="#columnItem.SQL_FIELD_NAME#" filterType="#columnItem.TYPE#" sqlType="#columnItem.CF_SQL_TYPE#">
						#columnItem.DISPLAY_NAME#
					</option>
				</cfloop>
			</select>
			<select class="datatable_filter_dropdownlist" rel="operator">
				<cfloop array="#operatorKeys#" index="operatorItem">
					<option value="#operatorItem.VALUE#" >
						#operatorItem.DISPLAY#
					</option>
				</cfloop>
			</select>
			
			<input type="text"  rel="datatable_filter_text" class="filter_val" value="">
			<select class="filter_status" name="filter_status" style="display:none">
				<option value= "-1" selected>All</option>
				<option value="0">Open</option>
				<option value="1">In Progess</option>
				<option value="2">Tech Review</option>
				<option value="3">QA Review</option>
				<option value="4">Closed</option>
			</select>

			<span style="color:red;display:none;" rel="datatable_filter_validate"></span>
			<div class="action">
				<!---<a class="delete_filter left" onclick="RemoveFilterRow_#datatable_filterId#(this);return false;" href="javascript:void(0);">--->
				<a class="delete_filter left" href="javascript:void(0);" onclick="RemoveFilterRow_#datatable_filterId#(this);return false;">
					<img class="filter_add_button ListIconLinks img24_24 delete_filter_18_18 img32_32" title="Delete Filter" src="/public/images/dock/blank.gif">
				</a>
				<a class="add_filter left"  href="javascript:void(0);" onclick="AddFilterRow_#datatable_filterId#(this);return false;">
					<img class="filter_add_button ListIconLinks img24_24 add_filter_18_18 img32_32" title="Add Filter" src="/public/images/dock/blank.gif">
				</a>
			</div>
		</div>
	</script>	
</cfoutput>

<cfoutput>

    <div id="DataTableFilterContainer">
    <form autocomplete="off">
    <div class="filter_box" id="filter_box_#datatable_filterId#">
        <!---this div contain caption for filter --->
        <div class="actions_filter_box">
            <span class="filter_title">Filters</span>
            <span style="display:none;" id="show_action_#datatable_filterId#" class="filter_action">
                <a onclick="ShowFilterDatatable_#datatable_filterId#(); return false;" href="javascript:void(0);">
                    <img width='25' height='25' alt='Show Filter Fields' src="/public/images/up_btn.jpg">
                </a>
            </span>
            <span style="display:block;" id="hide_action_#datatable_filterId#" class="filter_action">
                <a onclick="HideFilterDatatable_#datatable_filterId#(); return false;" href="javascript:void(0);">
                    <img width="25" height="25" alt="Hide Filter Fields" src="/public/images/down_btn.jpg">
                </a>
            </span>	
        </div>
        
        <!---this is filter rows --->
        <div id="filter_rows" style="display:none">
            <div class="datatable_filter_row row filter_row">
                <select class="datatable_filter_dropdownlist"  rel="column" onchange="UpdateOperator_#datatable_filterId#(this);">
                    <cfloop array="#datatable_ColumnModel#" index="columnItem">
                        <option value="#columnItem.SQL_FIELD_NAME#" filterType="#columnItem.TYPE#" sqlType="#columnItem.CF_SQL_TYPE#">
                            #columnItem.DISPLAY_NAME#
                        </option>
                    </cfloop>
                </select>
                <select class="datatable_filter_dropdownlist" rel="operator">
                    <cfloop array="#operatorKeys#" index="operatorItem">
                        <option value="#operatorItem.VALUE#" >
                            #operatorItem.DISPLAY#
                        </option>
                    </cfloop>
                </select>
    
                <input type="text"  rel="datatable_filter_text" class="filter_val" value="">
                <select class="filter_status datatable_filter_dropdownlist" name="filter_status" style="display:none">
                	<option value="-1" selected>All</option>
					<option value="0">Open</option>
					<option value="1">In Progess</option>
					<option value="2">Tech Review</option>
					<option value="3">QA Review</option>
					<option value="4">Closed</option>
				</select>
                <span style="color:red;display:none;" rel="datatable_filter_validate"></span>
                <div class="action">
                    <!---<a class="delete_filter left" onclick="RemoveFilterRow_#datatable_filterId#(this);return false;" style="display:none;" href="javascript:void(0);">--->
                    <a class="delete_filter left" style="display:none;" href="javascript:void(0);" onclick="RemoveFilterRow_#datatable_filterId#(this);return false;">
                        <img class="filter_add_button ListIconLinks img24_24 delete_filter_18_18 img32_32" title="Delete Filter" src="/public/images/dock/blank.gif">
                    </a>
                    <a class="add_filter left" href="javascript:void(0);" onclick="AddFilterRow_#datatable_filterId#(this);return false;">
                        <img class="filter_add_button ListIconLinks img24_24 add_filter_18_18 img32_32" title="Add Filter" src="/public/images/dock/blank.gif">
                    </a>
                </div>
            </div>
        </div>

        <!---button for submit filter --->
        <div style="background-image:url('/public/images/backgroud_bottom_table.jpg');display: none" id="filter_submits">
            <div style="float:right; margin-right: 1em; margin-top: 0.7em;" class="row" value="3" id="filter_row_3">
                <a class="filter-btn btn-primary dropdown-toggle clear-filter-btn" >Clear</a>
                <a class="filter-btn btn-primary dropdown-toggle apply-filter-btn">Apply Filter</a>
            </div>
            <div style="clear:both;"></div>
        </div>
    </div>
    </form>
    <div style="margin-top:10px">
    </div>

</div>

</cfoutput>
<cfoutput>
<script src="/public/sire/js/jquery-2.1.4.js"></script>


<script type="text/javascript">
	
	var addressFormatting_#datatable_filterId# = function(text, opt){
		var newText = text;
		//array of find replaces
		var findreps = [
			{find:/^([^\-]+) \- /g, rep: '<span class="ui-selectmenu-item-header">$1</span>'},
			{find:/([^\|><]+) \| /g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
			{find:/([^\|><\(\)]+) (\()/g, rep: '<span class="ui-selectmenu-item-content">$1</span>$2'},
			{find:/([^\|><\(\)]+)$/g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
			{find:/(\([^\|><]+\))$/g, rep: '<span class="ui-selectmenu-item-footer">$1</span>'}
		];

		for(var i in findreps){
			newText = newText.replace(findreps[i].find, findreps[i].rep);
		}
		return newText;
	}
	
	//this func will init controls in filter box like dropdown, etc
	function InitDataTableFilterControl_#datatable_filterId#(){
		
		UpdateOperator_#datatable_filterId#($("##filter_box_#datatable_filterId# .datatable_filter_dropdownlist:first"));
	}
	
	//this function will remove a filter row
	//callerObject is button triggering this function
	$( document ).ready(function(){		
		
	});
	
	//add new filter row 
	//callerObject is button triggering this function
	function AddFilterRow_#datatable_filterId#(callerObject){
		//use jquery template to add html right after container div of callerObject
		$(callerObject).parent().parent().after($("##filter_row_template_").tmpl());
		
		//update operator for recent added filter row
		UpdateOperator_#datatable_filterId#($(callerObject).parent().parent().next().find(".datatable_filter_dropdownlist:first"));
		
		
		//display all delete button
		that_parent = $(this).parent().parent().parent().parent();
		$(that_parent).find(".delete_filter").show();
		
		//UpdateOperator(that_parent);
		return false;
		
		
	}		
	
	//this function will remove a filter row
	//callerObject is button triggering this function
	function RemoveFilterRow_#datatable_filterId#(callerObject){
		
		var numberOfFilterRow = $(".datatable_filter_row").size();
		//if there is only one filter row left, just hide its delete button
		if(numberOfFilterRow <=2){
			$(".delete_filter").hide();
		}
		//remove html of container div of this button
		$(callerObject).parent().parent().remove();
			
	}
	
	//show filter box when triggered
	function ShowFilterDatatable_#datatable_filterId#(){
		$("##filter_box_#datatable_filterId# ##hide_action_#datatable_filterId#").show();
		$("##filter_box_#datatable_filterId# ##filter_rows").toggle("fast");
		$("##filter_box_#datatable_filterId# ##show_action_#datatable_filterId#").hide();
		$("##filter_submits").toggle("fast");
	}
	
	//hide filter box when triggered
	function HideFilterDatatable_#datatable_filterId#(){
		$("##filter_box_#datatable_filterId# ##show_action_#datatable_filterId#").show();
		$("##filter_box_#datatable_filterId# ##filter_rows").toggle("fast");
		$("##filter_box_#datatable_filterId# ##hide_action_#datatable_filterId#").hide();
		$("##filter_submits").toggle("fast");
	}
	
	//clear filter
	function ClearFilter_#datatable_filterId#(){
		$("##filter_box_#datatable_filterId# ##filter_rows").html("");
		$("##filter_box_#datatable_filterId# ##filter_rows").append($("##filter_row_template_#datatable_filterId#").tmpl());
		//use select menu for dropdowns
		InitDataTableFilterControl_#datatable_filterId#();
		
		var numberOfFilterRow = $("##filter_box_#datatable_filterId# ##filter_rows .datatable_filter_row").size();
		//if there is only one filter row left, just hide its delete button
		if(numberOfFilterRow <=1){
			$("##filter_box_#datatable_filterId# .delete_filter").hide();
		}

	   
		if('#datatable_jsCallback#' != ''){
			#datatable_jsCallback#();
		}
	}
	
	//apply filter 	
	function ApplyFilter_#datatable_filterId#(){
		//validate first, if false, return
		if(!Validate_#datatable_filterId#()) return false;
		//if true, call js call back function
		var filterData = new Array();
		var filterRows = $("##filter_box_#datatable_filterId# ##filter_rows .datatable_filter_row");
		for(var i=0;i<filterRows.size();i++){
			var filterRowItem = filterRows[i];
			var fieldName = $(filterRowItem).find("select[rel=column]").find(":selected").val();
			var operator = $(filterRowItem).find("select[rel=operator]").find(":selected").val();
			var fieldType = $(filterRowItem).find("select[rel=column]").find(":selected").attr('sqlType');
			var filtertype = $(filterRowItem).find("select[rel=column]").find(":selected").attr('filtertype');
			var inputValue =  $(filterRowItem).find("input[rel=datatable_filter_text]").val()!=""?$(filterRowItem).find("input[rel=datatable_filter_text]").val():$(filterRowItem).find("select[rel=listDataFilter]").val();
			
			//we need to custom data for date field
			if(fieldType == "CF_SQL_TIMESTAMP"){
				var tempDateTime = inputValue.split("/")[2] +"-"+inputValue.split("/")[0]+"-"+inputValue.split("/")[1];
				inputValue = tempDateTime;
				if(operator ==">"){
					inputValue += " 00:00:00";
				}
				if(operator =="<"){
					inputValue += " 23:59:59";
				}
			}

			if(filtertype == "STATUS"){
				inputValue = $('.filter_status').val();
			}
		
			filterData.push({
				NAME: fieldName,
				OPERATOR: operator,
				TYPE: fieldType,
				VALUE:inputValue
			});
		}
		if('#datatable_jsCallback#' != ''){
			#datatable_jsCallback#(filterData);
		}
	}
	
	//validate data of filter rows
	function Validate_#datatable_filterId#(){
		var result = true;
		var filterRows = $("##filter_box_#datatable_filterId# ##filter_rows .datatable_filter_row");
		//loop over filter rows to validate
		for(var i = 0;i<filterRows.size();i++){
			var filterRowItem = filterRows[i];
			var errorSpan = $(filterRowItem).find("span[rel=datatable_filter_validate]");
			var filterType = $(filterRowItem).find("select[rel=column]").find(":selected").attr('filterType');
			var inputValue =  $(filterRowItem).find("input[rel=datatable_filter_text]").val();
			
			//validate empty
			if (filterType == '#INTEGER#' || filterType == '#TEXT#'||filterType == '#DATE#') {
				if (inputValue == "") {
					$(errorSpan).html("#FIELDREQUIRED#");
					$(errorSpan).show();
					result = false;
				}else{
					$(errorSpan).html("");
					$(errorSpan).hide();
				}
			}
			//validate number
			if (filterType =='#INTEGER#' && inputValue != '') {
				if (!isNumber(inputValue)) {
					$(errorSpan).html("#ISNOTNUMBER#");
					$(errorSpan).show();
					result = false;
				}
				else{
					$(errorSpan).html("");
					$(errorSpan).hide();
				}
			}

			if(filterType == '#STATUS#')
			{
				inputValue = $(filterRowItem).find('.ilter_status datatable_filter_dropdownlist').val();
				if (inputValue == "") {
					$(errorSpan).html("#FIELDREQUIRED#");
					$(errorSpan).show();
					result = false;
				}else{
					$(errorSpan).html("");
					$(errorSpan).hide();
				}
			}
			//validate date
			if (filterType == '#DATE#') {
				if(!IsValidDate_#datatable_filterId#(inputValue)){
					$(errorSpan).html("#ISNOTDATE#");
					$(errorSpan).show();
					result = false;
				}else{
					$(errorSpan).html("");
					$(errorSpan).hide();
				}
			}
		}	
		return result;
	}
	
	//we need to regenerate operator dropdown options whenever user reselect column filter name 
	function UpdateOperator_#datatable_filterId#(callerObject){
		//get field type of column
		var filterType = $(callerObject).find(":selected").attr('filterType');

		// get object select value		
		var objectSelectValue = $(callerObject).find(":selected").val();
		// check filterType is List or not		
		if(filterType == 'LIST'){
			// if true to hide filter_val textbox
			$(callerObject).parent().find('.filter_val').hide();
			$(callerObject).parent().find('.filter_status').hide();
			var dataFilterList = '';
			// remove a listDataFilter because onchange event of selectmenu class is triggered 2 time 
			$(callerObject).parent().find('.listDataFilter').remove();
			// fill data to selectmenu
			<cfoutput>
				dataFilterList += '<select class="datatable_filter_dropdownlist listDataFilter" rel="listDataFilter">';
					<cfloop array="#datatable_ColumnModel#" index="columnItem">
						<cfif #columnItem.TYPE# eq "LIST">
							if("#columnItem.SQL_FIELD_NAME#" == objectSelectValue){
								<cfloop array="#columnItem.LISTOFVALUES#" index="listItem">
									dataFilterList += '<option value="#listItem.VALUE#" >';
										dataFilterList += '#listItem.DISPLAY#';
									dataFilterList += '</option>';
								</cfloop>
							}
						</cfif>
					</cfloop>
				dataFilterList += '</select>';
			</cfoutput>
			// append to page
			$(callerObject).parent().append(dataFilterList);
			
		}
		else if(filterType == 'STATUS'){
			$(callerObject).parent().find('.listDataFilter').hide();
			$(callerObject).parent().find('.filter_val').hide();
			$(callerObject).parent().find('.filter_status').show();
		}
		else{
		
			$(callerObject).parent().find('.filter_status').hide();
			// show filter_val textbox
			$(callerObject).parent().find('.filter_val').show();
			// remove listData
			$(callerObject).parent().find('.listDataFilter').remove();
		}
		
		var operators = new Array();//this is operator to be used to generate dropdown options below
		var operatorKeys = <cfoutput>#serializeJSON(operatorKeys)#</cfoutput>;
		for (var i = 0; i < operatorKeys.length; i++) {
			if (operatorKeys[i].INLIST.indexOf(filterType) > -1){
				//if operatorKeys item has this datatype, add this operator
				operators.push(operatorKeys[i])
			}
		}
		//now we have list of operator
		var operatorDropDown = $(callerObject).parent().find("select[rel=operator]");
		operatorDropDown.find('option').remove();
			
		for (var i = 0; i < operators.length; i++) {
			operatorDropDown.append(new Option(operators[i].DISPLAY, operators[i].VALUE));
		}
			
		//reset text of input field 
		var inputField = $(callerObject).nextAll(".filter_val");
		inputField.val('');
		//if filterType is DATE, init datepicker in input field
		if(filterType == '<cfoutput>#DATE#</cfoutput>'){
			inputField.datepicker({
				inline: true,
				calendars: 4,
				extraWidth: 100,
				dateFormat: 'mm/dd/yy' 
			}).attr('readonly','true');
		}else if(inputField.hasClass('hasDatepicker')){
			inputField.datepicker("destroy").removeAttr('readonly');
		}
		
		// check content of filter name 
		// if filter name too long then cut filter name 20 characters
		var allContent = $('span .ui-selectmenu-item-content');
		allContent.each(function(index) {
				$(this).text($.trim($(this).text()));
				if($(this).text().length > 16){
					$(this).text($(this).text().substr(0,16) + '...');
				}
				
				$(this).parent().parent().css('min-width','188px');
				<!---$(this).parent().parent().css('float','left');--->
				
			});
		}
	
	
	function IsValidDate_#datatable_filterId#(input){
		var monthfield=input.split("/")[0];
		var dayfield=input.split("/")[1];
		var yearfield=input.split("/")[2];
		var dayobj = new Date(yearfield, monthfield-1, dayfield)
		if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield)){
			return false;
		}
		return true;
	}
	
	
	function ComboBatchListReporting_#datatable_filterId#()
	{	
		var filterData = new Array();
		
		//if true, call js call back function
		var filterRows = $("##filter_box_#datatable_filterId# ##filter_rows .datatable_filter_row");
		for(var i=0;i<filterRows.size();i++){
			var filterRowItem = filterRows[i];
			var fieldName = $(filterRowItem).find("select[rel=column]").find(":selected").val();
			var operator = $(filterRowItem).find("select[rel=operator]").find(":selected").val();
			var fieldType = $(filterRowItem).find("select[rel=column]").find(":selected").attr('sqlType');
			var inputValue =  $(filterRowItem).find("input[rel=datatable_filter_text]").val()!=""?$(filterRowItem).find("input[rel=datatable_filter_text]").val():$(filterRowItem).find("select[rel=listDataFilter]").val();
			
			//we need to custom data for date field
			if(fieldType == "CF_SQL_TIMESTAMP"){
				var tempDateTime = inputValue.split("/")[2] +"-"+inputValue.split("/")[0]+"-"+inputValue.split("/")[1];
				inputValue = tempDateTime;
				if(operator ==">"){
					inputValue += " 00:00:00";
				}
				if(operator =="<"){
					inputValue += " 23:59:59";
				}
			}
			
			if(typeof(inputValue) != "undefined")
			{
				if(inputValue.trim != "")
				{
				
					filterData.push({
						NAME: fieldName,
						OPERATOR: operator,
						TYPE: fieldType,
						VALUE:inputValue
					});
	
				}
			}
		}
		
		
	}
			function BuildCDFFilterArray(filterRows){
				var dataReturn = new Array();
				for(var i = 0; i < filterRows.size(); i++){
					var curRow = $(filterRows[i]);

					var CDFID = curRow.find('select.datatable_filter_dropdownlist[rel="column"]').val();
					var OPERATOR = curRow.find('select.datatable_filter_dropdownlist[rel="operator"]').val();
					var VALUE = encodeURIComponent(curRow.find('input[rel="datatable_filter_text"]').val());

					var tmpRow = {"OPERATOR": OPERATOR, "VALUE": VALUE, "CDFID": CDFID};



					// check CDFID exists 
					var exists = false;
					var dataReturnIndexChecking = 0;
					for(var j in dataReturn){
						var dataReturnItem = dataReturn[j];
						if(dataReturnItem.CDFID == tmpRow.CDFID){
							exists = true;
							dataReturnIndexChecking = j;
						}
					}

					
					if(exists === false ){
						dataReturn.push({
							"CDFID": tmpRow.CDFID,
							"ROWS":[{"OPERATOR":tmpRow.OPERATOR,"VALUE":tmpRow.VALUE}]
						});
					} else {
						dataReturn[dataReturnIndexChecking].ROWS.push({
							"OPERATOR": tmpRow.OPERATOR, "VALUE": tmpRow.VALUE
						});
					}
				}

				return dataReturn;
			}


		

			function InitControl(subCrbl, contactString, filterRows){
				var cdfData = new Array();
				if(typeof(contactString) == "undefined"){
					contactString = "";
				}
				if(typeof(filterRows) !== "undefined"){
					cdfData = BuildCDFFilterArray(filterRows);
				}
				
				var dataValue = {
					CDFData: JSON.stringify(cdfData),
					CustomString: contactString,
					INPGROUPID:subCrbl
				};


				$.ajax({
				    type: "POST",
				    url: '/session/cfc/emergencymessages.cfc?method=GetContactCountByCDFAndCustomString&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				    data: dataValue,
				    async: false,
					dataType:"json",
				    success: function(dStruct ) {
				    	 var inputDataString = ($('input.data-contact-string').length ? $('input.data-contact-string') : $('<input type="hidden" class="data-contact-string">'));
				  			 inputDataString.data('contactStringList', dStruct.SMSLIST).appendTo('body');
				  		 var inputDataGroup = ($('input.data-contact-group').length ? $('input.data-contact-group') : $('<input type="hidden" class="data-contact-group">'));
				  		     inputDataGroup.data('contactStringListGroup', subCrbl).appendTo('body');
						 var inputDataCdfFilter = ($('input.data-cdf-filter').length ? $('input.data-cdf-filter') : $('<input type="hidden" class="data-cdf-filter">'));
				  		     inputDataCdfFilter.data('contactStringCdfFilter', JSON.stringify(cdfData)).appendTo('body');



				  		var table = $('##tblListEMS').dataTable({
							"bStateSave": false,
							"iStateDuration": -1,
							"fnStateLoadParams": function (oSettings, oData) {

							},
							"fnStateSaveParams": function (oSettings, oData) {

							},
							"bProcessing": true,
							"bFilter": false,
							"bServerSide":true,
							"bDestroy":true,
							"sPaginationType": "input",
						    "bLengthChange": false,
							"iDisplayLength": 20,
							"bAutoWidth": false,
						    "aoColumns": [
								{"sName": 'id', "sTitle": 'Contact ID',"bSortable": false,"sClass":"contact-id"},
								{"sName": 'name', "sTitle": 'Contact',"bSortable": false,"sClass":"contact-name"},
								
							],
							"sAjaxDataProp": "aaData",
							"sAjaxSource": '/session/cfc/emergencymessages.cfc?method=GetListContactById&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
							"fnRowCallback": function(nRow, aData, iDisplayIndex) {

					       	},
						   	"fnDrawCallback": function( oSettings ) {
						  		
						    },
							"fnServerData": function (sSource, aoData, fnCallback, oSettings) {//this fn is used for filtering data
								aoData.push({
									"name": "INPGROUPID", "value": $('input.data-contact-group').data('contactStringListGroup').toString()
								});

								aoData.push({"name": "ContactType", "value" : 3});
								if($('input.data-contact-string').data('contactStringList').length){
									aoData.push(
										{"name": "ContactId", "value": $('input.data-contact-string').data('contactStringList').toString()}
									);
								} else {
									aoData.push(
										{"name": "ContactId", "value": "-1"}
									);

								}
							
						        oSettings.jqXHR = $.ajax( {
							        "dataType": 'json',
							        "type": "POST",
							        "url": sSource,
							        "data": aoData,
							        "success": function(rspData){
							        	fnCallback(rspData);
							        	if(rspData.aaData.length == 0){
							        		$("##tblListEMS_paginate").hide();
							        	} else {
							        		$("##tblListEMS_paginate").show();
							        	}
							        }
						      	});
					        },
							"fnInitComplete":function(oSettings){
								// console.log(oSettings);
							}
						});

				    }
				});
			}


			function validateFilter(filterRows){
				
				for(var i = 0; i < filterRows.size(); i++){
					var thisInput = $(filterRows[i]).find('input[rel="datatable_filter_text"]');
					if(thisInput.val() == ""){
						thisInput.focus();
						return false;
					}
				}
				return true;
			}



	
	
</script>
</cfoutput>