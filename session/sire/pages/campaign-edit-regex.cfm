

<cfoutput>
	
<main class="container-fluid page">
	<section class="row bg-white">
		
		
		
		<!--- 
			
			<input id="AVALREG" value="" class="form-control" maxlength="1000" style="width:100%;">
			
			
			  <cfscript>
	            // Write the dump to an output buffer.
			        savecontent variable="XMLDATA" {
			            writeDump( var = LineData, format = "text" );
			        }
	            </cfscript>


			 --->
		
		
		<cfset JSONDATA =  '{"events":[{"type":"message","replyToken":"a66347b344714475adf84ee5a3add7b6","source":{"userId":"U115f7114aaa8dc4a88a335f9b3252b7f","type":"user"},"timestamp":1488339435212,"message":{"type":"text","id":"5716003010993","text":"Hello"}}]}' />
		
		<cfset LineData = deserializeJSON(JSONDATA) />
		<cfdump var="#LineData#">
		
		<cfdump var="#LineData.events#">
		
		LineData.events[1].type = #LineData.events[1].type#
		LineData.events[1].message.text = #LineData.events[1].message.text#
		LineData.events[1].source.userId = #LineData.events[1].source.userId#
		
		<br />
								
		<div id="AnswerValueRegExSection" class="row">
			
			<div class="col-xs-12 mb15">
				<div class="col-xs-12"><lable class="bc-title mbTight">Answer Option:</lable></div>
				<div class="col-xs-12 mb15"></div>
			</div>	
			
			
			<div class="col-xs-12">						 																		
				<div class="col-xs-12"><lable class="bc-title mbTight">Regular Expression:</lable></div>
				<div class="col-xs-12 mb15"><div class="" style="height: auto;"><textarea class="form-control" id="AVALREGEdit" maxlength="1000" rows="6" style="border-radius: 5px;"></textarea></div></div>
			</div>
			
			
			<div class="col-xs-12">	
			
				<div class="aval-regex-footer modal-footer" style="border: none;">
	                <button type="button" class="btn btn-success btn-success-custom btn-save-aval-regex"> Save </button>
	                &nbsp; &nbsp; &nbsp; 
	                <button type="button" class="btn btn-primary btn-back-custom btn-cancel-aval-regex">Cancel</button>
	            </div>	
				
			</div>											
		</div>	
		
		
		
		<div class="row">
			<div class="col-xs-12">	
				
				<div id="quickref" class="col-xs-12">
				
				  <div class="col-xs-12 col-md-4">
				
					    <table>
					      <tbody><tr>
					        <td style="min-width: 6em;"><code>[abc]</code></td>
					        <td>A single character of: a, b, or c</td>
					      </tr>
					      <tr>
					        <td><code>[^abc]</code></td>
					        <td>Any single character except: a, b, or c</td>
					      </tr>
					      <tr>
					        <td><code>[a-z]</code></td>
					        <td>Any single character in the range a-z</td>
					      </tr>
					      <tr>
					        <td><code>[a-zA-Z]</code></td>
					        <td>Any single character in the range a-z or A-Z</td>
					      </tr>
					      <tr>
					        <td><code>^</code></td>
					        <td>Start of line</td>
					      </tr>
					      <tr>
					        <td><code>$</code></td>
					        <td>End of line</td>
					      </tr>
					      <tr>
					        <td><code>\A</code></td>
					        <td>Start of string</td>
					      </tr>
					      <tr>
					        <td><code>\z</code></td>
					        <td>End of string</td>
					      </tr>
					    </tbody></table>
					  </div>
					
					  <div class="col-xs-12 col-md-4">
					    <table>
					      <tbody><tr>
					        <td style="min-width: 6em;"><code>.</code></td>
					        <td>Any single character</td>
					      </tr>
					    <tr>
					      <td><code>\s</code></td>
					      <td>Any whitespace character</td>
					    </tr>
					    <tr>
					      <td><code>\S</code></td>
					      <td>Any non-whitespace character</td>
					    </tr>
					    <tr>
					      <td><code>\d</code></td>
					      <td>Any digit</td>
					    </tr>
					    <tr>
					      <td><code>\D</code></td>
					      <td>Any non-digit</td>
					    </tr>
					    <tr>
					      <td><code>\w</code></td>
					      <td>Any word character (letter, number, underscore)</td>
					    </tr>
					    <tr>
					      <td><code>\W</code></td>
					      <td>Any non-word character</td>
					    </tr>
					    <tr>
					      <td><code>\b</code></td>
					      <td>Any word boundary</td>
					    </tr>
					  </tbody></table>
					</div>
					
					<div class="col-xs-12 col-md-4">
					<table>
					  <tbody><tr>
					    <td style="min-width: 6em;"><code>(...)</code></td>
					    <td>Capture everything enclosed</td>
					  </tr>
					  <tr>
					    <td><code>(a|b)</code></td>
					    <td>a or b</td>
					  </tr>
					  <tr>
					    <td><code>a?</code></td>
					    <td>Zero or one of a</td>
					  </tr>
					  <tr>
					    <td><code>a*</code></td>
					    <td>Zero or more of a</td>
					  </tr>
					  <tr>
					    <td><code>a+</code></td>
					    <td>One or more of a</td>
					  </tr>
					  <tr>
					    <td><code>a{3}</code></td>
					    <td>Exactly 3 of a</td>
					  </tr>
					  <tr>
					    <td><code>a{3,}</code></td>
					    <td>3 or more of a</td>
					  </tr>
					  <tr>
					    <td><code>a{3,6}</code></td>
					    <td>Between 3 and 6 of a</td>
					  </tr>
					</tbody></table>
					
					</div>
					
<!---
					<div id="regex_options" style="text-align:center">
						<p>
							options:
							<code>i</code> case insensitive
							<code>m</code> make dot match newlines
							<code>x</code> ignore whitespace in regex
							<code>o</code> perform ##{...} substitutions only once
						</p>
					</div>
--->
				
				</div>
			</div>	
		</div>

	</section>
</main>

</cfoutput>



<cfparam name="variables._title" default="Answer Match Editor - Sire">

<cfinclude template="../views/layouts/main.cfm">



<script type="application/javascript">
	
	
	$(function() {
	
		
		
	});
	
	
</script>		