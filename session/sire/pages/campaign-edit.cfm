<cfparam name="templateid" default="">
<cfparam name="campaignid" default="">
<cfparam name="adv" default="1">
<cfparam name="Tour" default="">
<cfparam name="inpTemplateFlag" default="0">
<cfparam name="swt" default="0"> <!--- Simplified Walk Through (swt) style - only ask one question at a time. Rely on template descriptions to guide the user through --->
<cfparam name="swt_desc" default="1">
<cfparam name="swt_keyword" default="1">
<cfparam name="swt_qa" default="1">
<cfparam name="blast" default="0">
<cfparam name="swt_aal" default="0">
<cfparam name="XMLCSEditable" default="false">
<cfparam name="src" default="0">

<cfparam name="hfe" default="0"> <!-- hide flow editor - Flag to enabel or disable flow editor options-->
<cfparam name="afe" default="0"> <!-- hide advanced flow editor - Flag to enabel or disable advanced flow editor options-->

<!--- Language check - Experimental --->
<cfparam name="UIL" default="ENGLISH">

<cfif LEN(TRIM(UIL)) GT 0 >
	<cfset Session.UILanguage = UIL />
</cfif>

<cfinclude template="/session/sire/models/cfc/control-point/control-point-ui-constants.cfm">

<!--- Make sure to load assets in head of final display logic public.sire.models.helpers.layout is a custom tool to help with this --->
<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("/session/sire/css/filter_table.css")
	   .addCss("/session/sire/css/campaign-edit/main.css")
	   .addCss("/session/sire/css/campaign-edit/gears.css")
	   .addCss("/session/sire/css/campaign-edit/media-hacks.css")
	   .addCss("/session/sire/css/campaign-edit/on-off-slider.css")
</cfscript>

<cfinvoke method="GetUserInfor" component="public.sire.models.users" returnvariable="RevalGetUserInfor">
	 <cfinvokeargument name="inpiduser" value="#Session.USERID#">
</cfinvoke>

<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="GetAdminPermission">
	 <cfinvokeargument name="inpSkipRedirect" value="1">
</cfinvoke>

<cfif GetAdminPermission.ISADMINOK EQ "1">
	<cfset XMLCSEditable = true />
<cfelse>
	<cfif RevalGetUserInfor.ALLEDITXMLCS EQ "0">
		<cfset XMLCSEditable = false />
	<cfelse>
		<cfset XMLCSEditable = true />
	</cfif>
</cfif>

<!--- Verify System Admin only can do this for now - may add ability for user to edit their own custom templates later--->
<cfif inpTemplateFlag NEQ 0>

	<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
	<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />
</cfif>

<cfinvoke component="session.cfc.emergencymessages" method="GetCustomDataFields" returnvariable="retValCustomDataField" />

<!--- Use this to pass in control flag to CP display options --->
<cfset inpSimpleViewFlag = "#adv#" />


<!--- multi-short code support - Enterprise accounts -
	  Look if Batch is being used by alternate short code - look in keyword - if it is use this short code
--->
<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="RetVarGetAssignedShortCode">

	<!--- dont use batch id if this is a new campaign request based off of template --->
	<cfif isNumeric(templateid) AND inpTemplateFlag EQ 0>

	<cfelse>

		<cfinvokeargument name="inpBatchId" value="#campaignid#">
		<!--- Use session default if none found associated with current Batch Id--->
		<cfinvokeargument name="inpShortCode" value="#session.Shortcode#">
	</cfif>


</cfinvoke>

<cfset campaignData = {
	BatchId_bi = '',
	KeywordId_int = '',
	Keyword_vch = '',
	Keyword_txt = '',
	Desc_vch = '',
	EMS_Flag_int = 0,
	GroupId_int = 0,
	Schedule_arr = [],
	ControlPoint_arr = [],
	CustomHelpMessage_vch = '',
	CustomStopMessage_vch = '',
	MLPID = 0
}>

<cfif isNumeric(campaignid) AND campaignid GT 0 AND inpTemplateFlag EQ 0>

	<cfinvoke method="GetBatchDetails" component="session.sire.models.cfc.control-point" returnvariable="RetVarGetBatchDetails">
		<cfinvokeargument name="inpBatchID" value="#campaignid#">
	</cfinvoke>

    <cfif RetVarGetBatchDetails.RECORDCOUNT LE 0>
    	<cflocation url="/session/sire/pages/campaign-manage" addtoken="false">
    </cfif>

	<cfif RetVarGetBatchDetails.RXRESULTCODE GT 0>
		<cfif RetVarGetBatchDetails.SHORTCODEID NEQ RetVarGetAssignedShortCode.SHORTCODEID>
			<cflocation url="/session/sire/pages/campaign-manage" addtoken="false"/>
		</cfif>
	</cfif>

    <cfset campaignData.BatchId_bi = RetVarGetBatchDetails.BATCHID>
    <cfset campaignData.Desc_vch = RetVarGetBatchDetails.DESC>
    <cfset campaignData.EMS_Flag_int = RetVarGetBatchDetails.EMSFLAG>
    <cfset campaignData.GroupId_int = RetVarGetBatchDetails.GROUPID>
    <cfset campaignData.MLPID = RetVarGetBatchDetails.MLPID>

    <!--- Dont display MLP if it is no longer available or online --->
    <cfif campaignData.MLPID GT 0>
    	<cfinvoke component="session.sire.models.cfc.mlp" method="ReadCPP" returnvariable="RetCPPXData">
			 <cfinvokeargument name="ccpxDataId" value="#campaignData.MLPID#">
		</cfinvoke>

		<cfif RetCPPXData.STATUS EQ 0>
			<cfset campaignData.MLPID = 0 />
		</cfif>

	</cfif>

	<cfinclude template="/session/cfc/csc/constants.cfm">
	<cfquery name="keywordQuery" datasource="#session.DBSourceREAD#">
		SELECT KeywordId_int, Keyword_vch, Created_dt, Active_int, EMSFlag_int
		FROM sms.keyword WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RetVarGetBatchDetails.BATCHID#">
			AND IsDefault_bit = 0
		ORDER BY Created_dt DESC, KeywordId_int DESC
	</cfquery>

	<cfif keywordQuery.RecordCount GT 0 && keywordQuery.Active_int[1] EQ 1>
		<cfset campaignData.KeywordId_int = keywordQuery.KeywordId_int[1]>
		<cfset campaignData.Keyword_vch = keywordQuery.Keyword_vch[1]>
		<cfif keywordQuery.EMSFlag_int[1] EQ 0>
			<cfset campaignData.Keyword_txt = campaignData.Keyword_vch>
		</cfif>
	</cfif>

	<!--- --->
	<cfinvoke method="GetSchedule" component="session.cfc.schedule" returnvariable="ScheduleByBatchId">
		<cfinvokeargument name="INPBATCHID" value="#campaignid#">
	</cfinvoke>
	<cfif ScheduleByBatchId.RXRESULTCODE GT 0>
		<cfloop array="#ScheduleByBatchId.ROWS#" index="ROW">
			<cfset Arrayappend(campaignData.Schedule_arr, ROW)>
		</cfloop>
	</cfif>

	<cfparam name="SCHEDULE" default="#campaignData.Schedule_arr#" >
	<!--- --->

	<cfquery name="customMessageQuery" datasource="#session.DBSourceREAD#">
		SELECT * FROM sms.smsshare WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#campaignid#">
	</cfquery>

	<cfloop query="#customMessageQuery#">
		<cfswitch expression="#Keyword_vch#" >
		<cfcase value="HELP">
			<cfset campaignData.CustomHelpMessage_vch = Content_vch>
		</cfcase>
		<cfcase value="STOP">
			<cfset campaignData.CustomStopMessage_vch = Content_vch>
		</cfcase>
		</cfswitch>
	</cfloop>

<!--- If campaignid is specified and template edit mode is enbabled by the inpTemplateFlag=1, then assume campaignid is the templateId--->
<cfelseif isNumeric(campaignid) AND campaignid GT 0 AND inpTemplateFlag EQ 1>

	<cfset campaignData = {
		BatchId_bi = '',
		KeywordId_int = '',
		Keyword_vch = '',
		Keyword_txt = '',
		Desc_vch = '',
		EMS_Flag_int = 0,
		GroupId_int = 0,
		Schedule_arr = [],
		ControlPoint_arr = [],
		CustomHelpMessage_vch = '',
		CustomStopMessage_vch = '',
		MLPID = 0
	}>

	<cfinvoke method="GetBatchDetails" component="session.sire.models.cfc.control-point" returnvariable="RetVarGetBatchDetails">
		<cfinvokeargument name="inpBatchID" value="#campaignid#">
		<cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
	</cfinvoke>

    <cfif RetVarGetBatchDetails.RECORDCOUNT LE 0>
    	<cflocation url="/session/sire/pages/campaign-manage" addtoken="false">
    </cfif>

    <cfset campaignData.BatchId_bi = RetVarGetBatchDetails.BATCHID>
    <cfset campaignData.Desc_vch = RetVarGetBatchDetails.DESC>
    <cfset campaignData.EMS_Flag_int = RetVarGetBatchDetails.EMSFLAG>
    <cfset campaignData.GroupId_int = RetVarGetBatchDetails.GROUPID>
    <cfset campaignData.MLPID = RetVarGetBatchDetails.MLPID>

<!--- Template was selected and template edit mode is enbabled by the inpTemplateFlag=1 --->
<cfelseif isNumeric(templateid) AND inpTemplateFlag EQ 1>
    <cfset campaignData = {
		BatchId_bi = '',
		KeywordId_int = '',
		Keyword_vch = '',
		Keyword_txt = '',
		Desc_vch = '',
		EMS_Flag_int = 0,
		GroupId_int = 0,
		Schedule_arr = [],
		ControlPoint_arr = [],
		CustomHelpMessage_vch = '',
		CustomStopMessage_vch = '',
		MLPID = 0
	}>

	<cfset campaignData.BatchId_bi = templateid>
	<cfset campaignid = templateid>

	<cfinvoke method="GetBatchDetails" component="session.sire.models.cfc.control-point" returnvariable="RetVarGetBatchDetails">
		<cfinvokeargument name="inpBatchID" value="#campaignid#">
		<cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
	</cfinvoke>

    <cfif RetVarGetBatchDetails.RECORDCOUNT LE 0>
    	<cflocation url="/session/sire/pages/campaign-manage" addtoken="false">
    </cfif>

    <cfset campaignData.BatchId_bi = RetVarGetBatchDetails.BATCHID>
    <cfset campaignData.Desc_vch = RetVarGetBatchDetails.DESC>
    <cfset campaignData.EMS_Flag_int = RetVarGetBatchDetails.EMSFLAG>
    <cfset campaignData.GroupId_int = RetVarGetBatchDetails.GROUPID>
    <cfset campaignData.MLPID = RetVarGetBatchDetails.MLPID>

<cfelseif isNumeric(templateid) AND inpTemplateFlag EQ 0>

	<!--- By default - create a batch to store --->

	<!--- Set default keyword so preview works too --->

	<!--- Look for top-menu.js menu selected short code first if it exists --->

	<!--- Create new Batch --->
	<cfinvoke method="AddNewBatchFromTemplate" component="session.sire.models.cfc.control-point" returnvariable="RetVarAddNewBatchFromTemplate">
		<cfinvokeargument name="inpTemplateId" value="#templateid#">
		<cfinvokeargument name="inpDesc" value="">
		<cfinvokeargument name="inpKeyword" value="">
		<cfinvokeargument name="inpShortCode" value="#RetVarGetAssignedShortCode.SHORTCODE#">
		<cfinvokeargument name="inpAddList" value="#swt_aal#">
	</cfinvoke>

	<!--- <cfdump var="#RetVarAddNewBatchFromTemplate#">	 <cfabort/> --->

   <!--- Skip the middle man and just load page after template is copied --->

    <cfif ADV EQ 1>
	    <cfset ADVBuff = "&ADV=1" />
	<cfelse>
		<cfset ADVBuff = "" />
	</cfif>

    <cfif RetVarAddNewBatchFromTemplate.BATCHID GT 0>

	    <cflocation url="/session/sire/pages/campaign-edit?campaignid=#RetVarAddNewBatchFromTemplate.BATCHID#&hfe=#hfe#&afe=#afe##ADVBuff#&src=#src#" addToken="false" />

    </cfif>
<cfelse>
	<cflocation url="/session/sire/pages/campaign-manage" addtoken="false">
</cfif>

<cfset cpOneSelectionNoBranchs = []>
<cfset cpOneSelectionInBranchs = []>


<!--- Lookup whether this BatchId has any blasts associated with it. Modify display options accordiningly --->
<cfinvoke component="session.sire.models.cfc.control-point" method="GetBatchBlasts" returnvariable="RetVarGetBatchBlasts">
	<cfinvokeargument name="inpBatchId" value="#campaignid#">
</cfinvoke>





<!--- MINHHTN HOT FIX --->
<cfparam name="Session.AdditionalDNC" default="">

<!---<cfinclude template="/public/sire/configs/paths.cfm">

<cfinclude template="/public/sire/configs/userConstants.cfm">--->

<cfset totalSubcriber = 0>

<cfparam name="batchid" default="#campaignid#">
<cfparam name="group_id" default="#campaignData.GroupId_int#">
<cfset disableClass="">

<!--- for cfincludes --->
<cfset inpBatchId = campaignid />


<cfoutput>
	<div class="portlet xlight bordered subscriber">

<!---
		<div class="content-header">

			<cfif inpTemplateFlag NEQ 0>
				<!--- Verify System Admin only can do this for now - may add ability for user to edit their own custom templates later--->
				<div class="row">
	                <div class="col-sm-12 col-md-12 col-lg-12 content-title">
	            		<h1>Template Edit Mode</h1>
	            	</div>
				</div>
			</cfif>

--->

		<div id="campaign_edit" class="clearfix" autocomplete="off">
			<input value="#campaignid#" id="campaign_id" type="hidden" name="BatchId_bi">
			<input value="#templateid#" id="template_id" type="hidden" name="TID_int">
			<input value="#RetVarGetAssignedShortCode.SHORTCODE#" id="ShortCode" type="hidden" name="ShortCode">
			<input value="#campaignData.KeywordId_int#" id="keywordId" type="hidden" name="keywordId">

			<div class="content-body">

    			<div id="SWTDescSection" class="form-group clearfix">

					<div class="col-xs-12 col-sm-12 hidden-md hidden-lg ">
						<div class="row">
						 	&nbsp;
				 		</div>
					</div>

					<!--- This section is for dispalying "Blast" values --->
					<!--- Do this here to avoid FOUC flicker --->
					<cfif RetVarGetBatchBlasts.BLASTS.RECORDCOUNT GT 0>
						<!--- Only show if there has been a blast --->
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="BlastStats">
					<cfelse>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="BlastStats" style="display:none;">
					</cfif>

	    				<div id="ProcessingBlastStats" style="min-height: 60px; float: left;">
							<img src="/public/sire/images/loading.gif" class="ajax-loader">
						</div>

						<div class="col-xs-12 col-sm-12">
							<label id="BlastQueue" style="display: block;">Total Messages Still In Blast Queue: &nbsp;</label>
							<label id="BlastComplete" style="display: block;">Total Blast Messages Completed: &nbsp;</label>
							<h4 id="btn-reload-blast-stats"><a href="javascript:;" style="color:##568ca5;">Refresh Stats</a></h4>
						</div>

	    			</div>

				</div>

				<!--- BEGIN : CAMPAIGN NAME --->

    			<!--- Preserve possible newlines in display but format all other tags --->
				<!--- Expected HTML output is different for straight div vs text area - Preview, Edit, SWT etc --->
                <!--- Allow newline in text desc messages - reformat for display --->
				<!--- Regular HTML --->
				<cfset CDescHTML = ReplaceNoCase(campaignData.Desc_vch, "newlinex", "<br>", "ALL")>
				<cfset CDescHTML = Replace(CDescHTML, "\n", "<br>", "ALL")>
				<cfset CDescHTML = Replace(CDescHTML, "#chr(10)#", "<br>", "ALL")>
				<cfset CDescHTML = Replace(CDescHTML, "{defaultShortcode}", "#RetVarGetAssignedShortCode.SHORTCODE#", "ALL")>

			    <div class="portlet light bordered top-section">
			        <div class="portlet-body-1">

			            <div class="row">
			                <div class="col-xs-12">
			                    <div class="form-gd form-lb-large">
			                        <div class="form-group">
			                            <h4 class="portlet-heading-new">#UIL_CampaignName# <span id="ParentLink"></span></h4>

			                            <span id="CampaignName" class="text-color-gray campaign-name-input" data-prompt-position="topLeft:100" data-container="body" data-toggle="popover-hover" data-placement="bottom" data-content="Rename">#CDescHTML#</span>

			                        </div>
			                    </div>
			                </div>
			            </div>



			        </div>
			    </div>
			    <!--- END : CAMPAIGN NAME --->




    			<div class="row mb15 CPObjContainer" id="CPObjContainer">

	    			<div id="CPObjStage" class="droppable sortable">

		    			<!-- col-md-12 col-lg-12 <div class="col-md-6" id="CPContainer"> Remove for NBE integrate-->
		    			<div class="CPContainer" id="CPContainer">

						<cfset MaxCPCount = 0 />

						<cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPs" returnvariable="RetVarReadCPs">
							<cfinvokeargument name="inpBatchId" value="#campaignid#">
							<cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
						</cfinvoke>

						<!--- Read all the CP items once so they can be used for selection boxes without having to re-query for each control point --->
						<cfinvoke component="session.sire.models.cfc.control-point" method="ReadXMLQuestions" returnvariable="RetVarReadXMLQuestions">
							<cfinvokeargument name="inpBatchId" value="#campaignid#">
							<cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
	                         </cfinvoke>

				    			<cfloop array="#RetVarReadCPs.CPOBJ#" index="CPOBJ">

					    		    <cfset MaxCPCount = MaxCPCount + 1 />

					    			<cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPDataById" returnvariable="RetVarReadCPDataById">
									<cfinvokeargument name="inpBatchId" value="#campaignid#">
									<cfinvokeargument name="inpQID" value="#CPOBJ.RQ#">
									<cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
									<cfinvokeargument name="RenderHTML" value="1">
								</cfinvoke>

								<!--- Hide anything with SWT = 0 --->
								<cfif RetVarReadCPDataById.CPOBJ.SWT NEQ "0" OR hfe EQ 0  >

						    			<cfinvoke method="RenderCP" component="session.sire.models.cfc.control-point" returnvariable="RetVarRenderCP">
										<cfinvokeargument name="controlPoint" value="#RetVarReadCPDataById.CPOBJ#">
										<cfinvokeargument name="inpBatchId" value="#campaignid#">
										<cfinvokeargument name="inpCPList" value="#RetVarReadXMLQuestions#">
										<cfinvokeargument name="inpSimpleViewFlag" value="#inpSimpleViewFlag#">
										<cfinvokeargument name="inpHFE" value="#hfe#">
										<cfinvokeargument name="inpAFE" value="#afe#">
										<cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
									</cfinvoke>

									<!--- <cfdump var="#RetVarReadCPDataById.CPOBJ#" /> --->

									<!--- This is the HTML for a CP opject - HTML is maintained in RenderCP function --->
									#RetVarRenderCP.HTML#
							    	</cfif>
				    			</cfloop>

						    	<!--- MLP Section --->
								<cfif campaignData.MLPID GT 0>

							   		<div id="SWTMLPSection" class="form-group clearfix" style="margin-top: 0;">
						    			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

						   			 		<cfinvoke component="session.sire.models.cfc.mlp" method="ReadCPP" returnvariable="RetCPPXData">
												 <cfinvokeargument name="ccpxDataId" value="#campaignData.MLPID#">
											</cfinvoke>

						   			 		<div class="" style="overflow: hidden;">
												<div><b>Linked MLP Preview:</b> <a class="" href="/session/sire/pages/mlp-edit?ccpxDataId=#campaignData.MLPID#&action=Edit">Edit</a> </div>
							                	<div class="Frame-Wrapper"><iframe id="MLPIFrame" class="scaled-frame" src="/mlp-x/lz/#RetCPPXData.CPPUUID#" scrolling="auto"></iframe></div>
								            </div>

										</div>
									</div>

			   			 		</cfif>

			   			 		<!--- Add a default drop zone for blank campaigns --->
			   			 		<div style="height: 100px; min-height: 100px; border: 1px dashed rgba(0, 0, 0, 0.1)" class="cpobj-drop-zone default-drop-zone">
				   			 		<div class="xcol-sm-12">
				   			 			<div class="control-point-border clearfix" style="text-align: center; line-height: 100px; background-color:transparent; color: rgba(0, 0, 0, 0.1);">
					   			 			Drag a Step or Action to insert into conversation flow here to get started
				   			 			</div>
				   			 		</div>
			   			 		</div>

						</div> <!--- CP Container --->

					</div> <!--- End CPObjStage --->

				</div>

	    	</div><!--- content-body --->

		</div>

	</div>

<!--- Set some globally avaialble javascript vars based on current data --->
<script TYPE="text/javascript">

	var INPBATCHID = '#campaignid#';
	var schedule_data = "";
	var SHORTCODE = "#RetVarGetAssignedShortCode.SHORTCODE#";

	var date_txt_1 = '#LSDateFormat(DateAdd("d", 30, Now()), "m-d-yyyy")#';
	var date_txt_2 = '#LSDateFormat(NOW(), "m-d-yyyy")#';

	var ScheduleTimeData;

	<!--- Set Global var for inpBatchId for Preview .js --->
	var inpBatchId = '<cfoutput>#campaignData.BatchId_bi#</cfoutput>'

</script>
</cfoutput>


	<!--- <span class="AnswerItem-list-style">Answer List Style</span>    '+ getOptionListStyle(format_answer, answers_list.find('.AnswerItem').length) + ' --->
	<!--- Store the structure of a Answer Items here and use ToScript to output it in later javascript function --->
	<!--- Cleaner and easier to maintain as straight HTML with syntax highlighting intact--->
	<!--- Use jQuery to initialize any values --->
	<cfsavecontent variable="AnswerListItemTemplate">
		<cfoutput>

		<div class="AnswerItem row row-small">
			<div class="col-xs-12">
				<div class="form-group AnswerItemInput">

					<i class="fa fa-angle-down toggle-ai-opt"></i>

					<span class="AVALREGLINK" id="AVALREGLINK">

						<i class="fas fa-bars"></i>

						<input type="hidden" id="AVAL" class="form-control readonly-val text-center" value="" />
						<input type="hidden" id="AVALREG" class="form-control readonly-val text-center" value="" />
						<input type="hidden" id="AVALRESP" class="form-control readonly-val text-center" value="" />
						<input type="hidden" id="AVALREDIR" class="form-control readonly-val text-center" value="" />
						<input type="hidden" id="AVALNEXT" class="form-control readonly-val text-center" value="" />

					</span>

					<span  class="IntentInput">
						<span><input id="OPTION" type="text" value="" class="form-control" /></span>

						<!--- Keep the display clean - hover delete option --->
						<span class="glyphicon glyphicon-remove-sign cp-edit-icon delete-intent hover-delete-intent" aria-hidden="true"></span>
					</span>
				</div>
			</div>

			<div class="wrapper-training-text col-xs-12">
				<div class="form-gd">
					<div class="training-text-list">

					</div>
				</div>

				<div class="row row-small" style="margin-bottom: 2em;">
					<div class="col-xs-12 new-training add-more-training">
						<span class="training-plus">+</span> Add Training Phrase
					</div>
				</div>
			</div>

		</div>

		</cfoutput>
	</cfsavecontent>

	<!--- Store the structure of a Text Item here and use ToScript to output it in later javascript function --->
	<!--- Cleaner and easier to maintain as straight HTML with syntax highlighting intact--->
	<!--- Use jQuery to initialize any values --->
	<cfsavecontent variable="TextListItemTemplate">
		<cfoutput>

		<div class="RandomTextArrayItem row row-small">
			<div class="col-xs-12">
				<div class="form-group">

					<input type="hidden" id="TID" class="form-control readonly-val text-center" value="" />
					<input type="hidden" id="T64" class="form-control readonly-val text-center" value="" />

					<span  class="RandomInput">

						<span style="display:block; overflow:hidden;"><input data-emojiable="true" data-type="input" type="text" value="" class="form-control RTEXT" /></span>

						<!--- Keep the display clean - hover delete option --->
						<span class="glyphicon glyphicon-remove-sign cp-edit-icon delete-text hover-delete-text" aria-hidden="true"></span>

						<div class="col-xs-12" style="margin-bottom: 1em;">
							<h5 class="text-right control-point-char text-color-gray">
								Character Count {N/A}
							</h5>
							<span>
								<a tabindex="0" class="info-popover pull-right see-unicode-mess " data-trigger="focus" role="button" data-placement="right"  data-html="true" title="" data-content='#UnicodePopupInfo#'><img  src='/session/sire/assets/layouts/layout4/img/info.png '/></a>
								<span class="see-unicode-mess see-unicode-mess-edit pull-right">#UnicodeCPWarning# &nbsp</span>
							</span>
						</div>
					</span>

				</div>
			</div>

		</div>

		</cfoutput>
	</cfsavecontent>

	<!--- Store the structure of a Text Item here and use ToScript to output it in later javascript function --->
	<!--- Cleaner and easier to maintain as straight HTML with syntax highlighting intact--->
	<!--- Use jQuery to initialize any values --->
	<cfsavecontent variable="TrainingItemTemplate">
		<cfoutput>

		<div class="TrainingItem row row-small">
			<div class="col-xs-12">
				<div class="form-group">

					<input type="hidden" id="TID" class="form-control readonly-val text-center" value="" />
					<input type="hidden" id="T64" class="form-control readonly-val text-center" value="" />

					<span style="display:block; overflow:hidden;"><input type="text" value="" class="form-control TEXT" /></span>

					<!--- Keep the display clean - hover delete option --->
					<span class="glyphicon glyphicon-remove-sign cp-edit-icon delete-training hover-delete-training" aria-hidden="true"></span>

				</div>
			</div>

		</div>

		</cfoutput>
	</cfsavecontent>

	<cfsavecontent variable="ExpertSystemListItemTemplate">
		<cfoutput>


			<!--- https://github.com/VerbalExpressions/JSVerbalExpressions --->
			<!--- http://buildregex.com/  --->
			<div class="ESItem" style="position:relative;">

			 	<div class="col-sm-12 mb15">

					<input type="hidden" id="ESREG" value="" class="form-control" maxlength="1000" style="width:100%;">

<!--- 				<a iwd="AVALREGLINK" data-toggle="modal" href="javascript:;" data-target="##AVALEditModal">RegEx</a> --->
					<a id="ESREGLINK" href="##a">Reg Exp Editor</a>

			 	</div>

					<div id="ESTextSection" class="row">

					<div class="col-xs-12"><lable for="ESTEXT" class="bc-title mbTight">Text to send:</lable></div>
					<div class="col-xs-12 mb15"><div class="Preview-Me Preview-Bubble" style="width: 90%; height: auto;"><textarea class="form-control" id="ESTEXT" maxlength="1000" rows="6" style="border-radius: 5px;"></textarea></div></div>

					<div class="col-xs-12 mb15" id="TDESCPLAIN"></div>

				</div>


				<div class="col-xs-2 col-sm-2 mb15">
			 		<button type="button" class="btn btn-default es_remove">
						<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
					</button>
			 	</div>
			</div>

		</cfoutput>
	</cfsavecontent>



<!--- Answer Match RegEx Editor --->
<div class="bootbox modal fade" id="AVALEditModal" role="dialog" aria-labelledby="AVALEditModal" aria-hidden="true">
    <form action="javascript:;" name="form-aval-regedit-campaign" id="form-aval-regedit-campaign" autocomplete="off">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Answer Match Editor</h4>
                </div>
                <div class="modal-body">

                	<div class="col-xs-12 mb15">

					<div class="col-xs-12 col-sm-12">
						<div class="form-group">
							<lable class="bc-title mbTight">Display Choice:</lable>
							<input id="AVALREGEditAnswerText" type="text" value="" class="form-control" />
						</div>
					</div>

				</div>

				<input type="hidden" id="AVALREGEditREG" />
				<!---
					<div class="col-xs-12">
						<div class="col-xs-12"><lable class="bc-title mbTight">Regular Expression:</lable></div>
						<div class="col-xs-12 mbTight f-eight">
							Case Insensitive, add (?) prefix.<br/>
							Case Insensitive all alternatives, add (?i) prefix.
						</div>
						<div class="col-xs-12 mb15"><div class="" style="height: auto;"><textarea class="form-control" id="AVALREGEditREG" maxlength="1000" rows="6" style="border-radius: 5px;"></textarea></div></div>
					</div>
				--->

				<div class="col-xs-12">

					<div class="col-xs-12 col-sm-12">
						<div class="form-group">
							<lable class="bc-title mbTight">Bucket Value:</lable>
							<input id="AVALREGEditAnswerBucket" type="text" value="" class="form-control" />
						</div>
					</div>

				</div>

				<div class="col-xs-12">

					<div class="col-xs-12 col-sm-12">
						<div class="form-group">
							<lable class="bc-title mbTight">Simple Response</lable>
							<input id="AVALREGEditRESP" type="text" value="" class="form-control" />
						</div>
					</div>

				</div>

				<div class="col-xs-12">

					<div class="col-xs-12 col-sm-12">
						<div class="form-group">

							<!---  <input id="AVALREGEditREDIR" type="text" value="" class="form-control" /> --->

							<lable class="bc-title mbTight">Sub Session:</lable>

							<div>

								<span class="AddNewNav2Redir" id="AddNewNav2Redir">
									<i class="fas fa-plus"></i>
								</span>

								<span class="Nav2Redir" id="Nav2Redir">
									<i class="fas fa-link"></i>
								</span>

								<span style="display:block; overflow:hidden;padding-right:10px;">

									<select id="AVALREGEditREDIR" class="Select2 BatchSelect BatchList" data-width="100%">
											<!--- Handle case where there is no data yet --->
											<option value="0" selected>-- Loading Data ... --</option>
									</select>

								</span>
							</div>



						</div>
					</div>

				</div>

				<div class="col-xs-12">

					<div class="col-xs-12 col-sm-12">
						<div class="form-group">
							<lable class="bc-title mbTight">Next CP</lable>

							<select id="AVALREGEditNEXT" class="Select2 StepQuestionList" data-width="100%">
									<!--- Handle case where there is no data yet --->
									<option value="0" selected>-- Loading Data ... --</option>
							</select>

						</div>
					</div>

				</div>

                </div>
                <div class="modal-footer">
                     <button type="button" id="btn-save-aval-regex" class="btn btn-success btn-success-custom btn-save-aval-regex"> Save </button>
	                &nbsp; &nbsp; &nbsp;
	                <button type="button" class="btn btn-primary btn-back-custom btn-cancel-aval-regex" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </form>
</div>



<!--- Expert System Match RegEx Editor --->
<div class="bootbox modal fade" id="ESREGEditModal" tabindex="-1" role="dialog" aria-labelledby="ESREGEditModal" aria-hidden="true">
    <form action="javascript:;" name="form-aval-regedit-campaign" id="form-aval-regedit-campaign" autocomplete="off">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Expert System (REGEX) Editor</h4>
                </div>
                <div class="modal-body">




					<div class="col-xs-12">
						<div class="col-xs-12"><lable class="bc-title mbTight">Regular Expression:</lable></div>
						<div class="col-xs-12 mb15"><div class="" style="height: auto;"><textarea class="form-control" id="ESREGEdit" maxlength="1000" rows="6" style="border-radius: 5px;"></textarea></div></div>
					</div>

                </div>
                <div class="modal-footer">
                     <button type="button" id="btn-save-es-regex" class="btn btn-success btn-success-custom btn-save-es-regex"> Save </button>
	                &nbsp; &nbsp; &nbsp;
	                <button type="button" class="btn btn-primary btn-back-custom btn-cancel-es-regex" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </form>
</div>


<!--- CLONE CAMPAIGN --->
<div class="bootbox modal fade" id="cloneCampaignModal" tabindex="-1" role="dialog" aria-labelledby="cloneCampaignModal" aria-hidden="true">
    <form action="javascript:;" name="form-clone-campaign" id="form-clone-campaign" autocomplete="off">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Clone Campaign</h4>
                </div>
                <div class="modal-body">

                    <div class="col-sm-12" style="margin-top: 1em;">
					    <p class="col-sm-12 alert-message">Are you sure?</p>
					</div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success btn-success-custom" id="btn-clone-campaign">Clone</button>
                    &nbsp; &nbsp; &nbsp;
                    <button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </form>
</div>

<!-- Modal -->
<div class="bootbox modal fade" id="AddNewSubscriberList" tabindex="-1" role="dialog" aria-labelledby="AddNewSubscriberList" aria-hidden="true">

    <div class="modal-dialog">
    	<form id="subscriber_list_form_add" class="row">
	        <div class="modal-content" id="add-sub-list-modal-content">
	        	<div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
	                <h4 class="modal-title title">Add New Subscriber List</h4>
	            </div>
	        	<div class="modal-body">
	        		<div class="row">
		        		<div class="col-sm-4">
		        			<label for="subscriber_list_name">
		        				Subscriber List Name:*
		        			</label>
		        		</div>
	        			<div class="col-sm-8">
	        				<input id="subscriber_list_name" class="form-control validate[required, custom[onlyLetterNumberSp]]" maxlength="255">
	        			</div>
        			</div>
	           	</div>
	            <div class="modal-footer">
	                <button type="submit" id="btn-save-group" class="btn btn-success btn-success-custom"> Save </button>
	                &nbsp; &nbsp; &nbsp;
	                <button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal">Cancel</button>
	            </div>
	        </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="bootbox modal fade" id="AddNewCDFModal" tabindex="-1" role="dialog" aria-labelledby="AddNewCDFModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        	<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title title">Add New Custom Data Field (CDF) to List</h4>
            </div>
        	<div class="modal-body">
        		<form id="subscriber_list_form" class="row">
        			<label for="subscriber_list_name" class="col-sm-4">
        				CDF Name:*
        			</label>
        			<div class="col-sm-8">
        				<input id="inpCDFName" class="form-control" maxlength="255">
        			</div>
        		</form>
           	</div>
            <div class="modal-footer">
                <button type="button" id="btn-save-cdf" class="btn btn-success btn-success-custom"> Save </button>
                &nbsp; &nbsp; &nbsp;
                <button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="ScheduleOptionsModal" tabindex="-1" role="dialog" aria-labelledby="ScheduleOptionsModal" aria-hidden="true">
    <div class="modal-dialog" style="width:auto; max-width:900px;">
        <div class="modal-content">
        	<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title title">Schedule Options</h4>
            </div>
            <cfinclude template="dsp_advance_schedule.cfm">
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="ConfirmLaunchModal" tabindex="-1" role="dialog" aria-labelledby="ConfirmLaunchModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        	<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Confirm Launch Campaign</h4>
            </div>
			<div class="modal-body clearfix">

				<div id="ProcessingBlast" style="display: none; min-height: 150px;">
					<h4>Your blast is loading into queue now:</h4>
					<img src="/public/sire/images/loading.gif" class="ajax-loader">
				</div>

				<!--- Confirmation Display --->
				<div class="ConfirmationSummary">
					<div class="form-group clearfix">
						<div class="col-xs-9 col-sm-6">Number of Subscribers:</div>
						<div class="col-xs-3 col-sm-6" id="confirm-number-subscribers">0</div>
					</div>

					<div class="form-group clearfix">
						<div class="col-xs-9 col-sm-6">Already Blasted this Campaign:</div>
						<div class="col-xs-3 col-sm-6" id="confirm-list-duplicates">0</div>
					</div>

					<div class="form-group clearfix">
						<div class="col-xs-9 col-sm-6">Total Eligible:</div>
						<div class="col-xs-3 col-sm-6" id="confirm-total-elligable">0</div>
					</div>

					<div class="form-group clearfix">
						<div class="col-xs-9 col-sm-6">Credits Needed:</div>
						<div class="col-xs-3 col-sm-6" id="confirm-credits-needed">0</div>
					</div>

					<div class="form-group clearfix">
						<div class="col-xs-9 col-sm-6">Credits Available:</div>
						<div class="col-xs-3 col-sm-6" id="confirm-credits-available">0</div>
					</div>

					<div id="confirm-launch-alert-text" class="clearfix" style="display: none;">
						<div class="col-xs-12 col-sm-12">
							<!--- /session/sire/pages/buy-sms --->
							<div class="alert alert-info" role="alert">
								You do not have enough credits to launch this campaign.<br>
								Please <a id="confirm-launch-buy-credits" href="/session/sire/pages/my-plan?active=addon" target="_blank">click here</a> to buy more credits.
							</div>
						</div>
					</div>

				</div>

			</div>
			<div class="modal-footer ConfirmationSummary">
				<button type="button" class="btn btn-success btn-success-custom" id="btn-confirm-save-and-send">Confirm Blast & Send Now!</button>
                &nbsp;
                <button type="button" class="btn btn-primary btn-back-custom" id="btn-cancel-send" data-dismiss="modal">Cancel</button>
			</div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- Modal -->
<div id="InfoModalRegExp" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What is a Regular Expression?</h4>
      </div>
      <div class="modal-body">

	      <p class="MedFont"><b>Regular Expression</b> (Optional) If the user's answer matches the supplied regular expression, the answer value will be stored along with the actual answer. The order of comparison is done with the first possible answer and stops when a match is found.</p>

		  <p>A regular expression, often called a pattern, is an expression used to specify a set of strings required for a particular purpose.</p>

		  <p>You are not required to use or know regular expression, but if you want to use use your own you will need to be familiar with the concepts. Google is your friend.</p>

		  <p>You should always test your regular expression to make sure they behave as you expect.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="InfoModalAVal" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What is a Stored Value (S-Val)?</h4>
      </div>
      <div class="modal-body">

	      <p class="MedFont"><b>Stored Values</b> Additional values associated with possible user inputs can be stored in the results. You can assign numerical values for later use in analytical processing, or you can report on best match based on a range of possible user inputs.</p>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- Modal -->
<div id="InfoModalKeyword" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What is a Keyword?</h4>
      </div>
      <div class="modal-body">

	      <p class="MedFont"><b>Keywords</b> are one of the fastest ways you can begin to communicate with your customers. <b>Keywords</b> are special messages sent to a Short Code. <b>Keywords</b> are used to trigger new Sire SMS sessions. Sessions can include simple response messages, list building via opt in confirmation, surveys, meet-ups, drip marketing, and additional two-way data exchange and capture.</p>

		  <img src="/public/sire/images/learning/keyword-special-simple.png" style="width: 100%; max-width:533px; height: auto;" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="InfoModalAPI" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What is an API Trigger?</h4>
      </div>
      <div class="modal-body">

	   		<h4>Advanced users and/or Developers can trigger SMS message flows with dynamic variables via API calls. The Sire engine is then used to deliver personalized interactive content, while the Sire Campaigns can be used as an SMS Content Management System.</h4>
		    <p>To trigger a Transactional SMS, you need to send information via an HTTPS request to the URL address <b>https://api.siremobile.com/ire/secure/triggerSMS</b>.  See the Transactional API tutorial under Learning and Support for more information.</p>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="InfoModalExpertSystem" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What is an Expert System ("Chat Bot") Trigger?</h4>
      </div>
      <div class="modal-body">

	   		<h4>The Expert System ("Chat Bot") is supposed to exchange data between the the Sire server and the Expert System control points, resulting in responses based on a set of possible questions, or statements. </h4>
		    <p>In artificial intelligence, an expert system is a computer system that emulates the decision-making ability of a human expert. Expert systems are designed to solve complex problems by reasoning about knowledge, represented mainly as if–then rules rather than through conventional procedural code.

</p>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="InfoModalCampaignName" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What makes a good Campaign Name?</h4>
      </div>
      <div class="modal-body">


			Coming up with a name for your campaign isn’t that difficult, but you can future-proof and organize your campaigns by utilizing clear naming conventions… just like a pro.

			<h3>Benefits of Using a Naming Convention</h3>
			<p>Being disciplined about how you name your campaigns has several notable benefits:</p>

			<ul>
				<li><h4>Future-proof Your Account</h4>Even if your account grows to include hundreds of campaigns, a consistent naming convention ensures the it remains easy to manage. You’ll be able to find existing campaigns easily and identify opportunities for new campaigns quickly.</li>

				<li><h4>Shareable</h4>Using a clear naming structure also makes your account structure easily understandable by others that may work on your account in the future. Collaboration becomes easy because colleagues will know exactly what a campaign is meant to do.</li>

				<li><h4>Filtering</h4>You can also search for, or filter by, parts of the campaign name (e.g., You can filter by campaigns that target the US by search for campaign names that contain “CA -“). </li>

				<li><h4>Group Similar Campaigns</h4>Once your naming convention is in place, you can group similar campaigns together quickly and easily. All it takes is a quick click on the campaign name header to sort the names alphabetically.
				<br><i>Pro tip: Sorting similar campaigns works best if you start your naming structure with the most common settings first and then move on to the more unique settings (e.g., start with “target audience type” and end with the “main keyword”).</i></li>

				<li><h4>High-level Reporting</h4>When similar campaigns are easily sorted and filtered by name, spotting trends becomes easier</li>
			</ul>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="InfoModalSubscriber" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What is a Subscriber List?</h4>
      </div>
      <div class="modal-body">

	      <p class="MedFont">A <b>Subscriber List</b> is a list of subscribers that have opted into your campaign by texting your keyword to your short code. Example:“39492”. Sire automatically captures all of your subscribers phone numbers and saves them in your subscriber list. This is also to help keep non-compliant spammers at bay.</p>

		  <img src="/public/sire/images/learning/opt-in.jpg" style="width: 100%; max-width:600px; height: auto;" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!--- Custom Message --->
<!-- Modal -->
<div class="bootbox modal fade" id="CustomMessageModal" tabindex="-1" role="dialog" aria-labelledby="CustomMessageModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        	<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title title">Help/Stop Message</h4>
<!---                 <h5 class="">Tools for Compliance</h5>  --->
            </div>
        	<div class="modal-body">
    			<div class="row">
    				<cfoutput>
    				<div class="col-sm-12">
    					<label for="CustomHelpMessage_vch" class="col-sm-4 control-label">Help Message</label>
    					<div class="col-sm-8">
							<textarea class="form-control" id="CustomHelpMessage_vch" name="CustomHelpMessage_vch" maxlength="160" rows="3">#campaignData.CustomHelpMessage_vch#</textarea>
						</div>
					</div>

					<div class="col-xs-12 col-sm-12 cbic">
    					<div class="row">
						 	&nbsp;
				 		</div>
					</div>

    				<div class="col-sm-12">
    					<label for="CustomStopMessage_vch" class="col-sm-4 control-label">Stop Message</label>
    					<div class="col-sm-8">
							<textarea class="form-control" id="CustomStopMessage_vch" name="CustomStopMessage_vch" maxlength="160" rows="3">#campaignData.CustomStopMessage_vch#</textarea>
						</div>
    				</div>
    				</cfoutput>

    				<div class="col-xs-12 col-sm-12 cbic">
    					<div class="row">
						 	&nbsp;
				 		</div>
						<hr class="hrt0 hrb10">
					</div>

    				<div class="col-sm-12">
					<!---<p>All campaigns must respond with messages for users who send <b>HELP</b> and <b>STOP</b> keywords.</p><p>By default we will use the messages that you  setup in your organizational profile on your <a href="my-account">MY ACCOUNT->SETTINGS</a> page.</p><p>You may also specify custom <b>HELP</b> and <b>STOP</b> response messages for each campaign.</p> --->
    					<p>All campaigns must respond with messages for users who send <b>HELP</b> and <b>STOP</b> keywords.</p><p>By default we will use the messages that you  setup in your organizational profile on your <a href="profile" target="_blank">MY ACCOUNT->SETTINGS</a> page.</p><p>You may also specify custom <b>HELP</b> and <b>STOP</b> response messages for each campaign.</p>
    				</div>

    			</div>
           	</div>
            <div class="modal-footer">

	            <button type="button" class="btn btn-success btn-success-custom" id="btn-save-custom-responses">Save</button>
	        &nbsp; &nbsp; &nbsp;
	        	<button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal">Cancel</button>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!--- Alert popup --->
<div class="modal fade" id="bootstrapAlert" tabindex="-1" role="dialog" aria-labelledby="alertLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-body-inner row">
        	<h4 class="col-sm-10 modal-title"></h4>
        	<p class="col-sm-12 alert-message">
        	</p>
        </div>
      </div>
    </div>
  </div>
</div>



<!-- Modal -->
<div id="InfoModalCDF" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What is a Custom Data Field (CDF)?</h4>
      </div>
      <div class="modal-body">
      	<p>Tools to manage your custom data. Your account. Your data. No Limits! </p>
      	<p>Used within an Action as part of the flow of a conversation, you can store a users response to the previous question and link it to the current phone number.</p>
      	<p>Your company's unique strengths are its competitive advantage. Why should you compromise with vanilla application deployments or other vendor limits? With Sire's Custom Data Fields, you don't have too.</p>
      	<p>Any CDFs in the message flow will be replaced with the data you send as part of the triggerSMS API call. Send these additional CDF values as part of the URL query string in GET and POST requests or as part of the JSON data in the POST requests. </p>

	  	<ul>
		  	<li><b>Personalization:</b> Personalize different parts of each message for each individual recipient.</li>
		  	<li><b>Customization:</b> Customize parts of the message to relate to each individual recipient's transaction history. The knowledge that a company has about a customer needs to be put into practice and the information held has to be taken into account in order to be able to give the client exactly what he wants</li>
		  	<li><b>Rules Engine:</b> You can change the flow of a conversation based on previously captured input.</li>
	  	</ul>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<cfif XMLCSEditable EQ true >
	<!-- Modal Edit XMLControlSTring-->
	<div id="EditControlString"  tabindex="-1" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <!-- Modal content-->
	    <form id="frmEditXMLCString">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Edit XMLControlString</h4>
		      </div>
		      <div class="modal-body">

			      	<div class="form-group">
			      		<textarea rows="6" class="form-control validate[required]" form="frmEditXMLCString" name="inpRawXMLControlString" id="inpRawXMLControlString">
			      		</textarea>
			      		<input type="hidden" name="campaignid" value="<cfoutput>#campaignid#</cfoutput>">
			      	</div>

		      </div>
		      <div class="modal-footer">
		      	<button type="submit" id="btnSaveXMLCS" class="btn btn-success-custom" >Save</button>
		        <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
		      </div>
		    </div>
	    </form>
	  </div>
	</div>
</cfif>

<!--- Save as new template modal --->
<div class="modal fade" id="AdminSaveNewTemplateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
            	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><strong>SAVE AS NEW TEMPLATE</strong></h4>
	            <div class="modal-body-inner row">
		            <form name="save-as-new-template" class="col-sm-12" autocomplete="off" id="save-as-new-template-form">

            			<div class="form-group">
						  	<label for="templatename" id="template-name-label">Template name</label>
						  	<label class="pull-right"><a id="template-name-countdown">255</a> left</label>
						  	<input type="text" class="form-control validate[required, custom[noHTML], maxSize[255]]" id="template-name" name="template-name">
						</div>

						<div class="form-group">
							<label for="templatecategory" id="template-category-label">Template category</label>
							<select class="form-control" id="select-template-category">
								<!---  --->
							</select>
						</div>

						<div class="form-group">
							<label for="templatedescription" id="template-description-label">Template Description</label>
							<label class="pull-right"><a id="template-description-countdown">1000</a> left</label>
							<textarea class="form-control validate[maxSize[1000], required]" id="template-description" name="template-description" placeholder="" rows="6"></textarea>
						</div>

						<div class="form-group">
							<label for="templateimage" id="template-image-label">Template Image</label>
							<select class="form-control" id="select-template-image">
								<!---  --->
							</select>
						</div>

						<div class="form-group">
							<label for="inpTemplateType" class="bc-title mbTight">Template Type</label>
							<select id="inpTemplateType" class="Select2" data-width="100%">
	                            <option value="0" selected="selected">Default Keyword OR Blast</option>
								<option value="1">Keyword Only</option>
								<option value="2">Blast Only</option>
	                        </select>
						</div>

						<div class="row hidden">
							<div class="form-group col-sm-3 col-xs-6 col-md-4 col-lg-4">
								<label for="templateorder" id="template-order-label">Template order</label>
								<select class="form-control" id="select-template-order">
									<!---  --->
								</select>
							</div>
						</div>


						<div class="form-group">
							<label for="templatexml" id="template-xml-label">Template XML String</label>
							<textarea class="form-control validate[required]" id="template-xml" name="template-xml" placeholder="" rows="6" data-prompt-position="topLeft:100"></textarea>
						</div>

		            </form>
		            <div class="save-new-template-btn-group">
					  	<button class="btn btn-back-custom" id="close-save-new-template-btn" data-dismiss="modal">Cancel</button>
					  	<button class="btn btn-success-custom" id="confirm-save-new-template-btn" type="submit">Save</button>
					</div>
				</div>
        	</div>
        </div>
    </div>
</div>


<!--- Only load this if there is a linked MLP - load after jquery has been loaded --->
<cfif campaignData.MLPID GT 0>
	<!-- Modal SWTMLPSection-->
	<div id="InfoModalEditMLP" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Marketing Landing Portal Editor</h4>
	      </div>
	      <div class="modal-body">

		      	<cfset inpCPPID= "">
		      	<cfset ccpxDataId= campaignData.MLPID>
		      	<cfset ccpxDataIdSource= "0">
		      	<cfset templateid= "0">
		      	<cfset action= "edit">
		      	<cfset ModalMode = 1 />

				<cfinclude template="mlp-edit.cfm" />

	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>

	  </div>
	</div>
</cfif>


<!--- Read Current Profile data and put into form --->
<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="RetVarGetUserOrganization"></cfinvoke>

<!--- Save as new template modal --->
<div class="modal fade" id="UpdateCompanyNameModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
            	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><strong>Set Your Company Name</strong></h4>
	            <div class="modal-body-inner row">

		            	<div style="text-align: center; background-color: #578ca5; width: 70px; height: 70px; float: left; margin-right: 1em; margin-bottom: 2em; border-radius: 5px;"><img src="/public/sire/images/ic_profile.png" id="console-ic-profile" class="" style="width: 64px; height: 64px; margin: 3px 3px;"></div>


						<input type="hidden" name="OrganizationId_int" id="OrganizationId_int" value="<cfoutput>#RetVarGetUserOrganization.ORGINFO.OrganizationId_int#</cfoutput>">

						<p>This template requires a Company Name. You should provide the name of your company now.</p>

<!--- 						<p><div style="text-align: center; margin-top: 1em;"><a href="/session/sire/pages/my-account" style="display: inline;" class="ConsoleLinks">Manage My Account Info</a></div></p> --->



					    <div class="col-sm-12 col-lg-10">
						    <label for="OrganizationName_vch" class="control-label">Company Name</label>
					    	<input type="text" class="form-control" name="OrganizationName_vch" id="OrganizationName_vch" maxlength="250" data-prompt-position="topLeft:100"
					    	value="<cfoutput>#RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH #</cfoutput>">
					    </div>

						<div class="col-sm-12 col-lg-10">
							<p>You can always change your default settings later at <span style="white-space: nowrap">MY ACCOUNT<b style="font-size: .8em;" class="glyphicon glyphicon-arrow-right"></b>MY PROFILE</span></p>
					 	</div>

				</div>

				<div class="modal-footer">
				  	<button class="btn btn-back-custom" id="close-save-company-info-btn" data-dismiss="modal">Cancel</button>
				  	<button type="button" class="btn btn-success btn-success-custom" id="confirm-save-company-info-btn">Save</button>
				</div>
        	</div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="bootbox modal fade" id="RulesEngineBOCDVPicker" tabindex="-1" role="dialog" aria-labelledby="RulesEngineBOCDVPicker" aria-hidden="true">

    <div class="modal-dialog">
    	<form id="subscriber_list_form_add" class="row">
	        <div class="modal-content" id="add-sub-list-modal-content">
	        	<div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
	                <h4 class="modal-title title">Choose the data condition value</h4>
	            </div>
	        	<div class="modal-body">



	        		<ul uk-tab="connect: .BOCDV-options-menu" class="cp-report-tab">
			            <li class="uk-active">
			                <a href="##">Answer</a>
			            </li>
			            <li>
			                <a href="##">CDF</a>
			            </li>
			            <li>
			                <a href="##">Custom CDF</a>
			            </li>
			            <li>
			                <a href="##">Custom</a>
			            </li>
			        </ul>

					<ul class="uk-switcher uk-margin BOCDV-options-menu">
			            <li>

							<div class="portlet">
							    <div class="portlet-body portlet-body-small" >
							        <div class="row">

							            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<label>
						        				Look at result from the this step
						        			</label>

											<select id="QuestionSelection" class="form-control Select2" style="width:100%;" QuestionsOnly="1">
												<option value="0">Select a step</option>
											</select>

										</div>

										 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

											<label>
						        				Which result
						        			</label>

											<select id="QuestionSelectionMatchType" class="form-control Select2" style="width:100%;" QuestionsOnly="1">
												<option value="0">Select an option</option>
												<option value="1">Compare Raw Answer</option>
												<option value="2">Compare Answer Bucket</option>
											</select>

										</div>

						        	</div>
						        </div>
		        			</div>
			            </li>

			           	<li>
							<div class="portlet">
							    <div class="portlet-body portlet-body-small">
							        <div class="row">

										 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

											<label>
						        				Choose a Standard Custom Data Field (CDF)
						        			</label>

											<select id="BOCDV_STDCDF_Picker" class="form-control Select2" style="width:100%;">
												<option value="0">Select a Standard CDF</option>
												<option value="CONTACTSTRING">CONTACTSTRING</option>
												<option value="SHORTCODE">SHORTCODE</option>
												<option value="BATCHID">BATCHID</option>
												<option value="CAMPAIGNID">CAMPAIGNID</option>
												<option value="KEYWORD">KEYWORD</option>
												<option value="INPCX">INPCX</option>
												<option value="MLPCX=X">MLPCX=X</option>
												<option value="ISOPT2X">ISOPT2X</option>
												<option value="INPPA=JSON:KEYNAME:X">INPPA=JSON:KEYNAME:X</option>
												<option value="NOW+X">NOW+X</option>
											</select>

										</div>

						        	</div>
						        </div>
		        			</div>

			           	</li>

			           	<li>
							<div class="portlet">
							    <div class="portlet-body portlet-body-small">
							        <div class="row">

										 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

											<label>
						        				Choose a user defined Custom Data Field (CDF)
						        			</label>

											<select id="BOCDV_CDF_Picker" class="form-control Select2" style="width:100%;">
												<option value="0">Select a user defined CDF</option>
											</select>

										</div>

						        	</div>
						        </div>
		        			</div>
			           	</li>

			           	<li>

			           		<div class="portlet">
							    <div class="portlet-body portlet-body-small">
							        <div class="row">

					        			<div class="col-xs-12">

						        			<label>
						        				Final Condition data Value
						        			</label>

					        				<input id="BOCDV_Picker" class="form-control" maxlength="255">
					        			</div>

						        	</div>
						        </div>
		        			</div>

			           	</li>

					</ul>










	           	</div>
	            <div class="modal-footer">
		            <button class="btn btn-back-custom" data-dismiss="modal">Cancel</button>
	                <button type="button" class="btn btn-success btn-success-custom" id="btn-save-bocdv" data-dismiss="modal">Save</button>
	            </div>
	        </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js">
</cfinvoke>

<!--- Used in Schedule for now.... --->
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/js/jquery.tmpl.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/js/schedule.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/js/campaign_edit.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/sire/css/legacy-schedule.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/sire/js/jquery-ui-1.11.4.custom/jquery-ui.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/message.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/schedule.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/style.css">
</cfinvoke>

<!---
	//   .addJs("/session/sire/assets/pages/scripts/campaign.js")
	//   .addCss("/public/sire/css/jquery.timepicker.min.css", true)
	//   .addCss("/session/sire/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css", true)
	//   .addJs("/session/sire/assets/global/plugins/bootstrap-datetimepicker/js/moment-with-locales.js", true)
    //   .addJs("/session/sire/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js", true)
    //   .addJs("/session/sire/assets/global/plugins/bootstrap-datetimepicker/js/jquery.timepicker.min.js", true)

	jquery.ui.touch-punch.min.js
	http://touchpunch.furf.com/ - Make jquery ui work on touch devices - hack for now but works until better option comes along

	jquery.ddslick.js
	http://designwithpc.com/Plugins/ddSlick
	A free light weight jQuery plugin that allows you to create a custom drop down with images and description.

	emoji-picker
	https://github.com/OneSignal/emoji-picker
	😍 😜 😂 😛 Instantly add Emoji support to input fields on your website! 💥 ✨ 👍 🤘

	select2.min.js
	https://select2.org/
	https://github.com/select2/select2-bootstrap-theme
	https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js
	https://cdnjs.com/libraries/select2-bootstrap-theme



	ejs_production.js


--->

<cfscript>
   CreateObject("component","public.sire.models.helpers.layout", true)
	   .addJs("/public/js/jquery.ddslick.js", true)
	   .addJs("/session/sire/assets/pages/scripts/ejs_production.js", true)
	   .addCss("https://cdnjs.cloudflare.com/ajax/libs/emoji-picker/1.1.5/css/emoji.min.css", false)
	   .addJs("https://cdnjs.cloudflare.com/ajax/libs/emoji-picker/1.1.5/js/config.min.js", false)
	   .addJs("https://cdnjs.cloudflare.com/ajax/libs/emoji-picker/1.1.5/js/util.min.js", false)
	   .addJs("https://cdnjs.cloudflare.com/ajax/libs/emoji-picker/1.1.5/js/jquery.emojiarea.min.js", false)
	   .addJs("https://cdnjs.cloudflare.com/ajax/libs/emoji-picker/1.1.5/js/emoji-picker.min.js", false)
	   .addCss("https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css", false)
	   .addCss("https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css", false)
	   .addJs("https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js", false)
	   .addJs("https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js", false)
	   .addCss("https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.16/css/uikit.css", true)
	   .addJs("https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.16/js/uikit.min.js", true)
</cfscript>


<cfparam name="variables._title" default="Campaign Edit - Sire">

<!--- Special view for campaing edit sections - allows better dedicated menu for draggables --->
<cfinclude template="../views/layouts/campaign-edit-view.cfm">


<!--- These counts belong in a dedicated file script type="application/javascript --->
<cfinclude template="/session/sire/pages/advanced-templates/inc-counts.cfm" />

<script type="application/javascript">

	var action = 'Edit';
	var selectorCP = "data-control-point-type='ONESELECTION'";

	<!--- Page Global to store looked up options for SELECT boxes --- null will force reload on next read --->
	var QuestionListBox = null;
	var QuestionsOnlyListBox = null;
	var ChooserActive = false;


	<!--- This outputs a previously defined Coldfusion variable as a javascript variable --->
	<!--- Page global so can be read in beginning and modified each time add - remove CPS--->
	<cfoutput>
		var #toScript(RetVarReadXMLQuestions, "inpCPListjsVar")#;
	</cfoutput>

	function isAsciiOnly(str) {
	    for (var i = 0; i < str.length; i++)
	        if (str.charCodeAt(i) > 127)
	            return false;
	    return true;
	}

	function hasUnicodeChar(str) {
		var UnicodeRegex = new RegExp('[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]');
		if(UnicodeRegex.test(str)){
			return true;
		}
		return false;
	}

	//UTF8 decode for messages that have Unicode characters
	function utf8_decode (strData) {

		var tmpArr = []
		var i = 0
		var c1 = 0
		var seqlen = 0

		strData += ''

		while (i < strData.length) {
			c1 = strData.charCodeAt(i) & 0xFF
			seqlen = 0

			// http://en.wikipedia.org/wiki/UTF-8#Codepage_layout
			if (c1 <= 0xBF) {
				c1 = (c1 & 0x7F)
				seqlen = 1
			} else if (c1 <= 0xDF) {
				c1 = (c1 & 0x1F)
				seqlen = 2
			} else if (c1 <= 0xEF) {
				c1 = (c1 & 0x0F)
				seqlen = 3
			} else {
				c1 = (c1 & 0x07)
				seqlen = 4
			}

		for (var ai = 1; ai < seqlen; ++ai) {
			c1 = ((c1 << 0x06) | (strData.charCodeAt(ai + i) & 0x3F))
		}

		if (seqlen === 4) {
			c1 -= 0x10000
			tmpArr.push(String.fromCharCode(0xD800 | ((c1 >> 10) & 0x3FF)))
			tmpArr.push(String.fromCharCode(0xDC00 | (c1 & 0x3FF)))
		} else {
			tmpArr.push(String.fromCharCode(c1))
			}

			i += seqlen
		}

		return tmpArr.join('')
	}


	$(function() {


		<!--- inplace editing of Campaign Name with save on close .campaign-name-input --->
		var replaceWithCampaignName = $('<input name="tempCampaignName" type="text" class="campaign-name-input-temp" />'),
		connectWithCampaignName = $('#CampaignName');

		$('#CampaignName').inlineEditCampaignName(replaceWithCampaignName, connectWithCampaignName);

		$('#CampaignName').popover({trigger:'hover', template:'<div class="popover campaign-name-input-pop" role="tooltip"><div class="arrow"></div><div class="popover-body">Rename</div></div>'});


		$('#page-logo').popover({trigger:'hover', template:'<div class="popover page-logo-input-pop" role="tooltip"><div class="arrow"></div><div class="popover-body">Dashboard</div></div>'});

		<!--- Becuase these buttons can be dynamically added - set click method here for each obj--->
		$.each($('.cp-desc-input'), function (index, item) {

			<!--- inplace editing of CP Desc without save on close .cp-desc-input --->
			replaceWithCPDesc = $('<input name="tempCPDesc" type="text" class="cp-desc-input-temp" />');

		   	$(this).inlineEditCPDesc(replaceWithCPDesc, $(this));
		   	$(this).popover({trigger:'hover', template:'<div class="popover cp-desc-input-pop" role="tooltip"><div class="arrow"></div><div class="popover-body">Step Description</div></div>'});
		});


		<cfif campaignData.MLPID GT 0>
			InitMLPEditor();
		</cfif>

		<!--- Set up all the select2 boxes --->
		$(".Select2").select2( { theme: "bootstrap"} );

		$(".Select2BatchSelect").select2( { theme: "bootstrap", tags: true} );

		<!--- Becuase these buttons can be dynamically added - set click method here for each obj--->
		$.each($('.control-point-btn-edit'), function (index, item) {
		   	BindEditCPInline($(this));
		});

		<!--- Setup sortables --->
		BindSortableCP($("#CPObjContainer"));

		<!--- set click method(s) here for each CP obj on inital page load --->
		$.each($('.control-point'), function (index, item) {
		   	<!--- Setup save buttons --->
		   	BindSaveCPInline($(this));

			$(this).find('.add-more-text').on('click', function(event)
			{
				var content = $(this).parents('.control-point').find('.control-point-body');
				AddRandomTextToList('', content);
			});

			$(this).find('.RandomTextArrayItem').each(function(index, item) {
				BindRandomTextOptions($(item));
			});

			$(this).find('.TrainingItem').each(function(index, item) {
				BindTrainingTextOptions($(item));
			});

		});

		<!--- Becuase these buttons can be dynamically added - set click method here for each obj--->
		$.each($('.control-point-btn-delete'), function (index, item) {
		   	BindDeleteCPInline($(this));
		});

		<!--- Becuase these buttons can be dynamically added - set click method here for each obj--->
		$.each($('.copy-cp'), function (index, item) {
			BindCopyCPInline($(this));
		});

		<!--- Becuase these buttons can be dynamically added - set click method here for each obj--->
		$.each($('.control-point-btn-add'), function (index, item) {
		   BindAddCPInline($(this));
		});

		<!--- Becuase these Control Points can be dynamically added - set cancel method here for each obj--->
		$.each($('.btn-cancel-cpe'), function (index, item) {
		   	BindCPCancel($(this));
		});

		<!--- Extra way to get to editor - just click on text body for users who cant see the edit icon... --->
		$(".control-point-body").click(function()
		{
			$(this).parents('.control-point').find('.control-point-btn-edit').click();
		});

		$('#InfoModalEditMLP').on('hidden.bs.modal', function () {

			<!--- Refresh iFram on modal edit close --->
			var _theframe = document.getElementById("MLPIFrame");
			_theframe.contentWindow.location.href = _theframe.src;

		})

		<!---  --->
		$(".console-link-box").click(function()
		{
			location.href = $(this).attr('rel1');
		});

		<!---  --->
		$(".short-code-btn-edit").click(function()
		{
			$('#ShortCodeDisplaySection').hide();
			$('#ShortCodeDefineSection').show();

		});

		<!--- Revert display and revert to current keyword --->
		$(".btn-cancel-short-code-data").click(function()
		{
			<!--- revert to current display value  --->
			$('#ShortCodeSelect').val($("#ShortCodeDisplayValue").html());
			$('#select2-ShortCodeSelect-container').html($("#ShortCodeDisplayValue").html());
			$('#select2-ShortCodeSelect-container').attr('title', $("#ShortCodeDisplayValue").html());

			$('#ShortCodeDisplaySection').show();
			$('#ShortCodeDefineSection').hide();
		});


		<!--- Save AVAL REGEXE changes --->
		$("#btn-save-aval-regex").click(function()
		{
			<!--- Save changes to RegEx to current answer item --->
			$('#AVALEditModal').data('inpAnswerItem').find('#OPTION').val($('#AVALEditModal').find('#AVALREGEditAnswerText').val());
			$('#AVALEditModal').data('inpAnswerItem').find('#AVAL').val($('#AVALEditModal').find('#AVALREGEditAnswerBucket').val());
			$('#AVALEditModal').data('inpAnswerItem').find('#AVALREG').val($('#AVALEditModal').find('#AVALREGEditREG').val());
			$('#AVALEditModal').data('inpAnswerItem').find('#AVALRESP').val($('#AVALEditModal').find('#AVALREGEditRESP').val());
			$('#AVALEditModal').data('inpAnswerItem').find('#AVALREDIR').val($('#AVALEditModal').find('#AVALREGEditREDIR').val());
			$('#AVALEditModal').data('inpAnswerItem').find('#AVALNEXT').val($('#AVALEditModal').find('#AVALREGEditNEXT').val());

			<!--- Show CP Editor Save options if this changes --->
			$('#AVALEditModal').data('inpAnswerItem').parents('.QuestionContainer').parent().find('.CPEFooter').css("visibility", "visible");

			<!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
			$('#AVALEditModal').data('inpAnswerItem').parents('.QuestionContainer').parent().find('.btn-save-cp').prop("disabled",false);

			<!--- Remove current answer item reference - safe so that future modals dont change wrong data --->
			$('#AVALEditModal').data('inpAnswerItem', null)

			$('#AVALEditModal').modal('hide');

		});

		<!--- Save Expert System REGEX changes --->
		$("#btn-save-es-regex").click(function()
		{
			<!--- Save changes to RegEx to current answer item --->
			$('#ESREGEditModal').data('inpESItem').find('#ESREG').val($('#ESREGEditModal').find('#ESREGEdit').val());

			<!--- Remove current answer item reference - safe so that future modals dont change wrong data --->
			$('#ESREGEditModal').data('inpESItem', null)

			$('#ESREGEditModal').modal('hide');
		});



		<!--- Clone Campaign --->
		$("#confirm-save-company-info-btn").click(function()
		{
			<!--- Allow blank to erase responses --->
			$("#confirm-save-company-info-btn").prop("disabled",true);

			$.ajax({
			type: "POST",
			url: '/session/sire/models/cfc/control-point.cfc?method=UpdateCompanyName&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			async: true,
			data: {
					OrganizationId_int : $("#OrganizationId_int").val(),
					OrganizationName_vch : $("#OrganizationName_vch").val()
				},
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); $("#confirm-save-company-info-btn").prop("disabled",false);	},
			success:
				function(d) {
					$('#processingPayment').hide();

					if(d.RXRESULTCODE == 1)
					{
						$('#confirm-save-company-info-btn').prop('disabled',false);
						$('#UpdateCompanyNameModal').modal('hide');

						if($("#OrganizationName_vch").val().length > 0)
						{
							var str = $('#CPText').val();
							var res = str.replace('{%CompanyName%}', $("#OrganizationName_vch").val());

							$('#CPText').val(res);
						}

						$('html,body').animate({scrollTop:0},0);


						return;
					}
					else
					{
						//bootbox.alert(d.MESSAGE, function() {});
						bootbox.dialog({
					    message: " " + d.MESSAGE,
					    title: "Save Company Name Failed",
						    buttons: {
						        success: {
						            label: "Ok",
						            className: "btn btn-medium btn-success-custom",
						            callback: function() {}
						        }
						    }
						});

						$("#confirm-save-company-info-btn").prop("disabled",false);
					}
				}
			});


		});


		<!--- Clone Campaign --->
		$("#btn-clone-campaign").click(function()
		{
			<!--- Allow blank to erase responses --->
			$("#btn-clone-campaign").prop("disabled",true);

			$.ajax({
			type: "POST",
			url: '/session/sire/models/cfc/control-point.cfc?method=CloneCampaign&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			async: true,
			data: {
				inpBatchId : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>",
				inpShortCode : $("#ShortCode").val()
				},
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); $("#btn-save-custom-responses").prop("disabled",false);	},
			success:
				function(d) {
					$('#processingPayment').hide();
					$('#cloneCampaignModal').modal('hide');
					if(d.RXRESULTCODE == 1){
						$('#btn-clone-campaign').prop('disabled',false);

						bootbox.dialog({
						    message: " " + d.MESSAGE,
						    title: "Campaign Cloned Success",
						    closeButton: false,
						    buttons: {
						        success: {
						            label: "Ok",
						            className: "btn btn-medium btn-success-custom",
						            callback: function() {
						            	location.href = '/session/sire/pages/campaign-edit?campaignid=' + d.NEXTCAMPAINID + '<cfif ADV EQ 1>&ADV=1</cfif>';
						            }
						        }
						    }
						});
						return;
					}
					else
					{
						//bootbox.alert(d.MESSAGE, function() {});
						bootbox.dialog({
					    message: " " + d.MESSAGE,
					    title: "Clone Campaigns",
						    buttons: {
						        success: {
						            label: "Ok",
						            className: "btn btn-medium btn-success-custom",
						            callback: function() {}
						        }
						    }
						});

						$("#btn-clone-campaign").prop("disabled",false);
					}
				}
			});


		});



		<!--- If page is refrshed with a selected list already set - go ahead and enable button. --->
		if($('#SubscriberList').val() > 0 )
			$(".btn-save-send").prop("disabled",false);

		<!--- Prepare to send blast - give user chance to confirm --->
		$(".btn-save-send").click(function()
		{
			BlastNow();
		});

		<!--- Confirm to send blast - Blast will now be sent --->
		$("#btn-confirm-save-and-send").click(function()
		{
			$("#btn-confirm-save-and-send").prop("disabled",true);

			<!--- Change to loading icon --->
			$('#ProcessingBlast').show();
			$('.ConfirmationSummary').hide();

			<!--- Save Custom Responses - validation on server side - will return error message if not valid --->
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/control-point.cfc?method=BlastToList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType: 'json',
				async: true,
				data:
				{
					inpBatchId : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>",
					inpGroupId : $('#SubscriberList').val(),
					inpShortCode : $("#ShortCode").val(),
					inpTimeZone : $("#inpTimeZone").val(),
					inpScheduledTimeData : JSON.stringify( GetScheduleTimeList() ),
					inpCdffilter: $('input.data-cdf-filter').data('contactStringCdfFilter'),
					inpContactFilter: $('#inpContactStringFilter').val()
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); $("#btn-confirm-save-and-send").prop("disabled",false);	},
				success:
				<!--- Default return function for Async call back --->
				function(d)
				{
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1)
					{
						<!--- Kill loading icon --->

						$('#ConfirmLaunchModal').modal('hide');

						<!--- Change to loading icon --->
						$('#ProcessingBlast').hide();
						$('.ConfirmationSummary').show();

						<!--- Re-enable button to send for next time --->
						$("#btn-confirm-save-and-send").prop("disabled",false);

						if (d.QUEUEDCOUNT != "") {

							bootbox.alert('<h3>Your blast has been loaded and will start going out shortly subject to your scheduled times.</h3 <h4>Count loaded into send queue = (' + d.QUEUEDCOUNT + ')</h4>', function(){  });

						} else {

							bootbox.alert(d.MESSAGE);

						}

					}
					else
					{
						<!--- No result returned --->
						if(d.ERRMESSAGE != "")
						{
							bootbox.alert(d.ERRMESSAGE);

							$('#ConfirmLaunchModal').modal('hide');

							<!--- Change to loading icon --->
							$('#ProcessingBlast').hide();
							$('.ConfirmationSummary').show();

							<!--- Re-enable button to send for next time --->
							$("#btn-confirm-save-and-send").prop("disabled",false);
						}
					}
				}

			});

		});

		<!--- Update/Set Custom Response --->
		$("#btn-save-custom-responses").click(function()
		{
			<!--- Allow blank to erase responses --->
			$("#btn-save-custom-responses").prop("disabled",true);

			<!--- Save Custom Responses - validation on server side - will return error message if not valid --->
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/control-point.cfc?method=SetCustomStandardReplyHELPSTOP&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType: 'json',
				async: true,
				data:
				{
					INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>",
					inpKeyword : "HELP",
					inpShortCode : $("#ShortCode").val(),
					inpHELPResponse : $("#CustomHelpMessage_vch").val(),
					inpSTOPResponse : $("#CustomStopMessage_vch").val()
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); $("#btn-save-custom-responses").prop("disabled",false);	},
				success:
				<!--- Default return function for Async call back --->
				function(d)
				{
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1)
					{
						$('#CustomMessageModal').modal('hide');
						$("#btn-save-custom-responses").prop("disabled",false);
					}
					else
					{
						<!--- No result returned --->
						if(d.ERRMESSAGE != "")
							bootbox.alert(d.ERRMESSAGE);

						$("#btn-save-custom-responses").prop("disabled",false);
					}
				}

			});

		});


		$('#processingPayment').hide();

		<!--- User Refreshable Stats --->
		$("#btn-reload-blast-stats").click(function()
		{
			<!--- Async load data on previous blasts - helps UI load faster for user --->
			CheckForPreviousBlasts();
		});

		<cfif RetVarGetBatchBlasts.BLASTS.RECORDCOUNT GT 0>
			<!--- Only show if there has been a blast --->
			<!--- Async load data on previous blasts - helps UI load faster for user --->
			CheckForPreviousBlasts();
		</cfif>

	});

	<!--- Becuase these buttons can be dynamically added - set click method here each time they are added --->
	function BindLearnMoreInline(inpObj)
	{
		<!--- Unbind any events already bound to this object - re-render will need to rebind objects --->
		inpObj.off();

		inpObj.click(function(){

			<!--- Extra way to get to more information - just click on text body or Learn More to toggle --->
			$(this).find('div').toggle();

		});
	}

	<!--- Becuase these buttons can be dynamically added - set click method here each time they are added --->
	function BindDeleteCPInline(inpObj)
	{
		<!--- Unbind any events already bound to this object - re-render will need to rebing objects --->
		inpObj.off();

		inpObj.click(function(){
			inpCPObj = $('#cp' + inpObj.attr('data-control-point-id'))	;

			bootbox.confirm('Are you sure you want to delete this Action?', function(r)
			{
				<!--- Only complete if user comfirms it --->
				if (r == true)
				{
					$.ajax({
						type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
						url: '/session/sire/models/cfc/control-point.cfc?method=DeleteControlPoint&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
						dataType: 'json',
						async: false,
						data:
						{
							INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>",
							INPPID : inpObj.attr("data-control-point-physical-id"),
							inpTemplateFlag : "<cfoutput>#inpTemplateFlag#</cfoutput>"
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},
						success:
						<!--- Default return function for Async call back --->
						function(d)
						{
							<!--- RXRESULTCODE is 1 if everything is OK --->
							if (d.RXRESULTCODE == 1)
							{
								<!--- Remove current Action from page --->
								inpCPObj.remove();

								<!--- Re-Index CPs on HTML page - Physical Ids remain the same. --->
								reIndexQuestions();

							}
							else
							{
								<!--- No result returned --->
								bootbox.alert("General Error processing your request. ");
							}

						}

					});

				}

				<!--- Return true to close bootbox when complete --->
				return true;
			});

		});
	}

	function BindCopyCPInline(inpObj)
	{
		<!--- Unbind any events already bound to this object - re-render will need to rebing objects --->
		inpObj.off();

		inpObj.click(function(){
			inpCPObj = $(this).parents('.control-point');

			var ControlPointData = BuildControlPointData(inpCPObj);
			var OutToClipboard = JSON.stringify(ControlPointData);

			// Create a dummy input to copy the string array inside it
			var dummy = document.createElement("input");

			// Add it to the document
			document.body.appendChild(dummy);

			// Set its ID
			dummy.setAttribute("id", "dummy_id");

			// Output the array into it
			document.getElementById("dummy_id").value=OutToClipboard;

			// Select it
			dummy.select();

			// Copy its contents
			document.execCommand("copy");

			// Remove it as its not needed anymore
			document.body.removeChild(dummy);

			return false;

		});


	}


	<!--- If an item is added to the page and or removed - re-order the CPs to Compare display order --->
	function reIndexQuestions(){

		var position = 1;

		<!--- The RQ is used to identify the top level CP object in the HTML - Delete uses PID -BUT- HTML is removed by RQ  --->
		position = 1;
		$.each($('.control-point'), function(index, value) {

			<!--- Warning: Dont mess with physical RQ location when saving simple view which can hide complex CPs from the end user --->

			$(this).attr('id', 'cp' + position);
			$(this).attr('data-control-rq-id', position);
			$(this).find('#RQ').val(position);

		    position = position + 1;
		});

		<!--- Reset all the CP identifiers in order shown --->
		position = 1;
		$.each($('.CPNumberOnPage:visible'), function(index, value) {

			$(this).html("" + position);
			position = position + 1;

		});

		<!--- Reset all the delete buttons RQs in order shown --->
		position = 1;
		$.each($('.control-point-btn-delete'), function(index, value) {
		   $(this).attr('data-control-point-id', position);
		   position = position + 1;

		});

		<!--- provide for a default drop zone if there are no objects yet --->
		if($('.control-point').length  > 0)
		{
			$('.default-drop-zone').hide();
		}
		else
		{
			$('.default-drop-zone').show();
		}

		<!--- Reset Question SELECTs to re lookup next time they are needed --->
		ForceReloadQuestionLists();

	}

	<!--- If an item is added to the page and or removed - re-order the CPs to Compare display order --->
	function reIndexConditions(inpCPE){

		var position = 1;
		<!--- Reset all the CID identifiers in order shown --->
		position = 1;
		$.each(inpCPE.find('.ConditionItem'), function(index, value) {

			$(this).find('#CID').val(position);
			$(this).find('#CIDLabel').html(" # " + position);

			position = position + 1;

		});

	}


	<!--- Render / Add each item to the list from Template --->
	function AddOptionToList(inpOptionText, inpOptionAval, inpOptionAvalReg, inpOptionAvalResp, inpOptionAvalRedir, inpNextCP, inpCPE)
	{
		<!--- The HTML for Answer and CPEditor are stored in coldfusion variables  AnswerListItemTemplate --->
		<!--- This outputs a previously defined Coldfusion variable as a javascript variable --->
		<cfoutput>
			var #toScript(AnswerListItemTemplate, "AnswerItemjsVar")#;
		</cfoutput>

		<!--- Create a jquery reference to the returned HTML and store it in 'inpAnswerItem' --->
		var inpAnswerItem = $(AnswerItemjsVar);

		var max = 0;
		inpCPE.find('.answers_list').find('.AnswerItem').each(function() {
		    max = Math.max(parseInt($(this).attr('option-id')), max);
		});
		max++;

		<!--- Set a unnique Id for each item --->
		inpAnswerItem.attr('option-id', max )

		<!--- Set OPTION text --->
		inpAnswerItem.find("#OPTION").val(inpOptionText);
		inpAnswerItem.find("#AVAL").val(inpOptionAval);
		inpAnswerItem.find("#AVALREG").val(inpOptionAvalReg);
		inpAnswerItem.find("#AVALRESP").val(inpOptionAvalResp);
		inpAnswerItem.find("#AVALREDIR").val(inpOptionAvalRedir);
		inpAnswerItem.find("#AVALNEXT").val(inpNextCP);

		BindSetupAnswerOptions(inpAnswerItem);

		<!--- Apend to list --->
		inpCPE.find('.answers_list').append(inpAnswerItem);

		<!--- Show CP Editor Save options if this changes --->
        inpCPE.parent().find('.CPEFooter').css("visibility", "visible");

        <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
        inpCPE.parent().find('.btn-save-cp').prop("disabled",false);

	}

	<!--- Render / Add each item to the list from Template --->
	function AddESOptionToList(inpOptionId, inpOptionText, inpOptionReg, inpCPE)
	{
		<!--- The HTML for Answer and CPEditor are stored in coldfusion variables  AnswerListItemTemplate --->
		<!--- This outputs a previously defined Coldfusion variable as a javascript variable --->
		<cfoutput>
			var #toScript(ExpertSystemListItemTemplate, "ESItemjsVar")#;
		</cfoutput>

		<!--- Create a jquery reference to the returned HTML and store it in 'inpESItem' --->
		var inpESItem = $(ESItemjsVar);

		var max = 0;
		inpCPE.find('#es_list').find('.ESItem').each(function() {
		    max = Math.max(parseInt($(this).attr('option-id')), max);
		});
		max++;

		<!--- Set a unnique Id for each item --->
		inpESItem.attr('option-id', max )

		<!--- Set OPTION text --->
		inpESItem.find("#ESID").val(inpOptionId);
		inpESItem.find("#ESTEXT").val(inpOptionText);
		inpESItem.find("#ESREG").val(inpOptionReg);


		inpESItem.find("#ESREGLINK").click(function()
		{
			<!--- Store a refence to this AVAL item so it can be updated by modal --->
			$('#ESREGEditModal').data('inpESItem', inpESItem);

			$('#ESREGEditModal #ESREGEdit').val(inpESItem.find("#ESREG").val());

			$('#ESREGEditModal').modal('show');

		});


		<!--- Apend to list --->
		inpCPE.find('#es_list').append(inpESItem);
	}


	function getOptionListStyle(AF, id)
	{
		switch(AF) {
			case 'NUMERICPAR':
				return id + 1 + ')';
				break;
			case 'NUMERIC':
				return id + 1 + ' for';
				break;
			case 'ALPHAPAR':
				return String.fromCharCode(65+id) + ')';
				break;
			case 'ALPHA':
				return String.fromCharCode(65+id) + ' for';
				break;
			case 'HIDDEN':
				return '';
				break;
			default:
				return '';
		}

	}



	<!--- Becuase these buttons can be dynamically added - set click method here each time they are added --->
	<!--- BOQ should only load real questions with answers - everthing else should load all actions/CPs --->
	function PopulateQuestionSelectBox(inpObjs)
	{
		if(QuestionListBox != null)
		{
			$.each(inpObjs , function(i, val)
			{

				var PrevSelectVal = inpObjs[i].val();

				<!--- Remove current Questions from Box --->
				inpObjs[i].empty();

				<!--- Set default message for Question selecter vs default Action director --->
				if(inpObjs[i].attr('id') == "BOQ" || inpObjs[i].attr('QuestionsOnly') == 1)
				{
					<!--- Only hit DOM once to add all of the options --->
					inpObjs[i].html(QuestionsOnlyListBox.join(''));

					if(PrevSelectVal == "" || PrevSelectVal == 'undefined' || PrevSelectVal == undefined)
						inpObjs[i].val(0);
					else
						inpObjs[i].val(PrevSelectVal);

					<!--- AI in the UI - Make sure selected value still existis --->
					if(inpObjs[i].val() != PrevSelectVal)
					{
						inpObjs[i].val(0);

						<!--- Notify UI that parent has changed and might need saving --->

						<!--- Show CP Editor Save options if this changes --->
			            inpObjs[i].parents('.control-point').find('.CPEFooter').css("visibility", "visible");

			            <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
			            inpObjs[i].parents('.control-point').find('.btn-save-cp').prop("disabled",false);
					}

					inpObjs[i].find('option[value="0"]').text("Please Select an Existing Step with a Result");
				}
				else
				{
					<!--- Only hit DOM once to add all of the options --->
					inpObjs[i].html(QuestionListBox.join(''));

					if(PrevSelectVal == "" || PrevSelectVal == 'undefined' || PrevSelectVal == undefined)
						inpObjs[i].val(0);
					else
						inpObjs[i].val(PrevSelectVal);

					inpObjs[i].find('option[value="0"]').text("Default to next Control Point");

				}
			});
		}
		else
		{
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/control-point.cfc?method=ReadXMLQuestions&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType: 'json',
				async: false,
				data:
				{
					INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>",
					inpTemplateFlag : "<cfoutput>#inpTemplateFlag#</cfoutput>"
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},
				success:
				<!--- Default return function for Async call back --->
				function(d)
				{
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1)
					{
						<!--- update list send to methods --->
						inpCPListjsVar = d;

						<!--- Store Questions for Select box here --->
						var output = [];
						var outputQuestionsOnly = [];

						outputQuestionsOnly.push('<option value="'+ 0 +'">'+ 'Please Select an Existing Step with a Result' +'</option>');

						$.each(d.QUESTIONS , function(i, val)
						{
						  	<!--- HTML Encode Text for Display - needs to be 'quote safe' --->
						  	<!--- Use the Physical - non-changing ID for each reference --->

						  	<!--- Only show questions with answers non-questions --->
						  	if(d.QUESTIONS[i].TYPE == "ONESELECTION" || d.QUESTIONS[i].TYPE == "SHORTANSWER" || d.QUESTIONS[i].TYPE == "API")
						  		if(d.QUESTIONS[i].TDESC != '')
									outputQuestionsOnly.push('<option value="'+ d.QUESTIONS[i].ID +'">'+ 'Step #' + d.QUESTIONS[i].RQ + ' ' + d.QUESTIONS[i].TDESC +'</option>');
								else if(d.QUESTIONS[i].TEXT != '')
								  	outputQuestionsOnly.push('<option value="'+ d.QUESTIONS[i].ID +'">'+ 'Step #' + d.QUESTIONS[i].RQ + ' ' + d.QUESTIONS[i].TEXT +'</option>');
							  	else
								  	outputQuestionsOnly.push('<option value="'+ d.QUESTIONS[i].ID +'">'+ 'Step #' + d.QUESTIONS[i].RQ + ' ' + d.QUESTIONS[i].TYPE +'</option>');

						});


						$.each(d.QUESTIONS , function(i, val)
						{
						  	<!--- HTML Encode Text for Display - needs to be 'quote safe' --->
						  	<!--- Use the Physical - non-changing ID for each reference --->

						  	if(d.QUESTIONS[i].TDESC != '')
								output.push('<option value="'+ d.QUESTIONS[i].ID +'">'+ 'Step #' + d.QUESTIONS[i].RQ + ' ' + d.QUESTIONS[i].TDESC +'</option>');
							else if(d.QUESTIONS[i].TEXT != '')
							  	output.push('<option value="'+ d.QUESTIONS[i].ID +'">'+ 'Step #' + d.QUESTIONS[i].RQ + ' ' + d.QUESTIONS[i].TEXT +'</option>');
						  	else
							  	output.push('<option value="'+ d.QUESTIONS[i].ID +'">'+ 'Step #' + d.QUESTIONS[i].RQ + ' ' + d.QUESTIONS[i].TYPE +'</option>');

						});

						<!--- Store here to minimize trips to DB  --->
						QuestionListBox = output;
						QuestionsOnlyListBox = outputQuestionsOnly;

						$.each(inpObjs , function(i, val)
						{
							var PrevSelectVal = inpObjs[i].val();

							<!--- Remove current Questions from Box --->
							inpObjs[i].empty();

							<!--- Set default message for Question selecter vs default Action director --->
							if(inpObjs[i].attr('id') == "BOQ" || inpObjs[i].attr('QuestionsOnly') == 1 )
							{
								<!--- Only hit DOM once to add all of the options --->
								inpObjs[i].html(outputQuestionsOnly.join(''));

								if(PrevSelectVal == "" || PrevSelectVal == 'undefined' || PrevSelectVal == undefined)
									inpObjs[i].val(0);
								else
									inpObjs[i].val(PrevSelectVal);

								<!--- AI in the UI - Make sure selected value still existis --->
								if(inpObjs[i].val() != PrevSelectVal)
								{
									inpObjs[i].val(0);

									<!--- Notify UI that parent has changed and might need saving --->

									<!--- Show CP Editor Save options if this changes --->
						            inpObjs[i].parents('.control-point').find('.CPEFooter').css("visibility", "visible");

						            <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
						            inpObjs[i].parents('.control-point').find('.btn-save-cp').prop("disabled",false);
								}

								inpObjs[i].find('option[value="0"]').text("Please Select an Existing Step with a Result")
							}
							else
							{
								<!--- Only hit DOM once to add all of the options --->
								inpObjs[i].html(output.join(''));

								if(PrevSelectVal == "" || PrevSelectVal == 'undefined' || PrevSelectVal == undefined)
									inpObjs[i].val(0);
								else
									inpObjs[i].val(PrevSelectVal);

								<!--- AI in the UI - Make sure selected value still existis --->
								if(inpObjs[i].val() != PrevSelectVal)
								{
									inpObjs[i].val(0);

									<!--- Notify UI that parent has changed and might need saving --->

									<!--- Show CP Editor Save options if this changes --->
						            inpObjs[i].parents('.control-point').find('.CPEFooter').css("visibility", "visible");

						            <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
						            inpObjs[i].parents('.control-point').find('.btn-save-cp').prop("disabled",false);
								}

								inpObjs[i].find('option[value="0"]').text("Default to next Control Point");

							}
						});

					}
					else
					{
						<!--- No result returned --->
						bootbox.alert("General Error processing your request.");
					}

				}

			});
		}
	}

	<!--- Only display answers Compared to single selection question --->
	function onChangeQuestionSelection(inpObj, inpBOV)
	{
		<!--- Read parant ConditionItem Obj for BOQ and BOV  needed--->
		if(inpObj.parents('.ConditionItem').find('#BOQ').val() > 0)
		{
			PopulateQuestionAnswersSelectBox(inpObj.parents('.ConditionItem').find('#BOV'), inpObj.parents('.ConditionItem').find('#BOQ').val(), inpBOV);
		}
	}

	<!--- Becuase these items can be dynamically added - set click method here each time they are added --->
	function PopulateQuestionAnswersSelectBox(inpObj, inpPID, inpBOV)
	{
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/control-point.cfc?method=ReadXMLQuestionAnswers&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			async: false,
			data:
			{
				INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>",
				inpQID : inpPID,
				inpIDKey : "ID",
				inpTemplateFlag : "<cfoutput>#inpTemplateFlag#</cfoutput>"
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},
			success:
			<!--- Default return function for Async call back --->
			function(d)
			{
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1)
				{
					<!--- Remove current Questions from Box --->
					inpObj.empty();

					<!--- Store Questions for Select box here --->
					var output = [];

					$.each(d.CPOBJ.OPTIONS , function(i, val)
					{
					  <!--- HTML Encode Text for Display - needs to be 'quote safe' --->
					  <!--- Use the Physical - non-changing ID for each reference --->
					  output.push('<option value="'+ d.CPOBJ.OPTIONS[i].ID +'">'+ d.CPOBJ.OPTIONS[i].TEXT +'</option>');

					});

					<!--- Only hit DOM once to add all of the options --->
					inpObj.html(output.join(''));

					<!--- Be sure to select store value if there is one --->
					inpObj.val(inpBOV);

				}
				else
				{
					<!--- No result returned --->
					bootbox.alert("General Error processing your request.");
				}

			}

		});

	}


	<!--- Validate Contorl Point Actions - make sure they are minimally completed and saved --->
	function ValidateCPActionsCompleted()
	{
		var LocalReturnValue = true;

		<!--- Loop over each CP --->
		$.each($('.control-point'), function(index, value) {


			<!--- Check to see if any editors are open - let the user finsih saving chages first --->
			<!--- This looks to see if the current control point has a CP type changer option - closed editors wont have this --->
			if( $(this).find('#ChangeCPType').length )
			{

				<!--- Scroll to this position --->
				$("body").scrollTop($(this).offset().top - 100);

				bootbox.alert('You still have an editor open: <b>' + $(this).find('.control-point-label').html() + '</b>. Be sure to save your changes before continuing.');

				<!--- Set the return value for the main validation function --->
				LocalReturnValue = false;

				<!--- Returning false here just breaks out of the jQuery each loop --->
				return false;
			}

			<!--- Check if this is a OPTIN type --->
			if($(this).attr('data-control-point-type') == 'OPTIN')
			{
				<!--- Check if Subscriber List has been set --->
				if(parseInt( $(this).find('.control-point-optin').attr('data-control-point-OIG')) == 0)
				{

					<!--- Scroll to this position --->
					$("body").scrollTop($(this).offset().top - 100);

					bootbox.alert('<b>Subscriber List</b> has not yet been set on action item <b>' + $(this).find('.control-point-label').html() + '</b>. You will need a <b>Subscriber List</b> to be able captured and store for future access your Opt In requests.');

					<!--- Open this editor --->
					$(this).find('.control-point-btn-edit').click();

					<!--- Set the return value for the main validation function --->
					LocalReturnValue = false;

					<!--- Returning false here just breaks out of the jQuery each loop --->
					return false;
				}

			}

		});

		return LocalReturnValue;
	}


	<!--- Check for blast stats asyncronously --->
	function CheckForPreviousBlasts()
	{
		$('#ProcessingBlastStats').show();

		$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/control-point.cfc?method=GetPreviousBlasts&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType: 'json',
				async: true,
				data:
				{
					inpBatchId : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>"
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {<!--- SQUASH ERRORS No result returned  bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); ---> },
				success:
				<!--- Default return function for Async call back --->
				function(d)
				{
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1)
					{
						<!--- Remove current Questions from Box --->


						$('#BlastQueue').html('Total Messages Still In Blast Queue: ' + parseInt(d.QUEUED));
						$('#BlastComplete').html('Total Blast Messages Completed: ' + parseInt(d.COMPLETED));


						$('#ProcessingBlastStats').hide();

					}
					else
					{
						<!--- No result returned - SQUASH ERRORS--->
						<!--- bootbox.alert("General Error processing your request."); --->
					}

				}

			});
	}


	<!--- Write Batch Desc to DB --->
	function SaveCampaignName()
	{
		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/control-point.cfc?method=UpdateBatchDesc&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			async: true,
			data:
			{
				INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>",
				inpDesc : $('#CampaignName').html(),
				inpTemplateFlag : "<cfoutput>#inpTemplateFlag#</cfoutput>"

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },
			success:
			<!--- Default return function for call back --->
			function(d)
			{
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1)
				{
					$('#KeywordStatus').html('');
				}
				else
				{
					<!--- No result returned --->
					if(d.ERRMESSAGE != "")
						bootbox.alert(d.ERRMESSAGE);
				}
			}

		});

	}




	$('#lbl-show-filter-bar').click(function(e){
		$('#subcriber-filter-bar').toggleClass('hidden');
		if($(this).hasClass('showed')){
			$(this).text('hide filter');
			$(this).removeClass('showed');
		} else {
			$(this).addClass('showed');
			$(this).text('show filter');
		}
	});


	$('a.apply-filter-btn').click(function(e){
		var contactString  = $('#inpContactStringFilter').val(),
		    subCrbl = $('#SubscriberList[data-type="old-blast"]').val(),
		    CFDInclude = true;
		var filterRows = $('#filter_rows > div.filter_row');
		    if(validateFilter(filterRows)){

		    	InitControl(subCrbl, contactString, filterRows);
		    }
	});


	$('body').on('change', '#inpContactStringFilter', function(e){

		var contactString  = $(this).val(),
		    subCrbl = $('#SubscriberList[data-type="old-blast"]').val();
		    ClearFilter_();
		    InitControl(subCrbl, contactString);

	});

	$('a.clear-filter-btn').click(function(event){
		var subCrbl = $('#SubscriberList[data-type="old-blast"]').val();
	  	$('#inpContactStringFilter').val("");
	    ClearFilter_();
		InitControl(subCrbl);

	});


	$('#SubscriberList[data-type="old-blast"]').change(function(e){

		subCrbl = $(this).val();
		// InitControl(subCrbl);
		$('#inpContactStringFilter').val("");

		if ($(this).val() > 0)
		{
			$('#btn-save-send').prop('disabled', false);
			$('div.box-subcriber-filter').removeClass('hidden');
		}
		else
		{
			$('#btn-save-send').prop('disabled', true);
			$('div.box-subcriber-filter').addClass('hidden');
		}
	});


	function BlastNow()
	{

		$(".btn-save-send").prop("disabled",true);

			<!--- Check for basic validation of Control Point data before continuing - give user warning and allow them to fix --->
			if(!ValidateCPActionsCompleted())
			{
				$(".btn-save-send").prop("disabled",false);
				return;

			}


			<!--- Save Custom Responses - validation on server side - will return error message if not valid --->
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/control-point.cfc?method=GetBlastToListStats&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType: 'json',
				async: true,
				data:
				{
					inpBatchId : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>",
					inpGroupId : $('#SubscriberList').val(),
					inpShortCode : $("#ShortCode").val(),
					inpTimeZone : $("#inpTimeZone").val(),
					inpContactFilter: $("#inpContactStringFilter").val(),
					inpCdffilter: $('input.data-cdf-filter').data('contactStringCdfFilter')

				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); $(".btn-save-send").prop("disabled",false);	},
				success:
				<!--- Default return function for Async call back --->
				function(d)
				{
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1)
					{
						$('#confirm-number-subscribers').text(d.LISTCOUNT);
						$('#confirm-credits-in-queue').text(d.QUEUECOUNT);
						$('#confirm-credits-available').text(d.BALANCE);

						$('#confirm-list-duplicates').text(d.LISTCOUNTDUPLICATE);

						var TotalElligable = parseInt(d.LISTCOUNT) - parseInt(d.LISTCOUNTDUPLICATE);
							TotalElligable = TotalElligable > 0 ? TotalElligable : 0;
						$('#confirm-total-elligable').text(TotalElligable);
						$('#confirm-credits-needed').text(TotalElligable);

						/* Check if this batch and subscriber list already in queue together */

						if (parseInt(d.HASACTIVEQUEUE) > 0) {
							bootbox.alert('This campaign already has processed a blast with this subscribers list.<br>Please choose another list.');
						} else {
							<!--- Disable send button and warn if credits not enough --->
							if (parseInt(d.BALANCENUMBER) >= parseInt(d.LISTCOUNT))
							{
								$('#btn-confirm-save-and-send').prop('disabled', false);
								$('#confirm-launch-alert-text').hide();
							}
							else
							{
								$('#btn-confirm-save-and-send').prop('disabled', true);
								$('#confirm-launch-alert-text').show();
							}


							$('#ConfirmLaunchModal').modal('show');
						}

						<!--- Re-enable button to send for next time --->
						$(".btn-save-send").prop("disabled",false);

					}
					else
					{
						<!--- No result returned --->
						if(d.ERRMESSAGE != "")
						{
							bootbox.alert(d.ERRMESSAGE);
							$(".btn-save-send").prop("disabled",false);
						}


					}
				}

			});



	}


	<!--- Auto create subscriber list if none is selected --->
	function UpdateMLPWithKeywordAndShortCode()
	{
		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/control-point.cfc?method=UpdateMLPWithKeywordAndShortCode&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			async: false,
			data:
			{
				INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>",
				inpShortCode : $("#ShortCode").val(),
				inpKeyword : $("#Keyword").val()

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },
			success:
			<!--- Default return function for call back --->
			function(d)
			{
				<!--- Refresh iFram on content changed --->
				var _theframe = document.getElementById("MLPIFrame");
				_theframe.contentWindow.location.href = _theframe.src;
			}

		});

	}


	<!--- .campaign-name-input inline edit specifically for Campaign Name --->
	$.fn.inlineEditCampaignName = function(replaceWith, connectWith) {

	    $(this).hover(function() {
	        $(this).addClass('hover');
	    }, function() {
	        $(this).removeClass('hover');
	    });

	    $(this).on("click touchstart", function(e){

	        var elem = $(this);

	        elem.hide();
	        elem.after(replaceWith);
	        replaceWith.val(elem.text());
	        replaceWith.focus();

	        replaceWith.blur(function() {

	            if ($(this).val() != "") {
	                connectWith.html($(this).val()).change();
	                elem.html($(this).val());
	            }

	            $(this).remove();
	            elem.show();

	            SaveCampaignName();

	        });

	        <!--- If check for special key presses to imporve functionality --->
			replaceWith.on('keyup', function(e){

		        <!--- If 'ESC' key is pressed undo changes --->
		        if (e.which == 27) {

		            $(this).remove();
					elem.show();
		        }

				<!--- If Enter is pressed save changes --->
		        if (e.which == 13) {
		            replaceWith.blur();
		        }


			});

	        <!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();

	    });
	};

	<!--- .CPDesc inline edit specifically for CP Desc with auto gro text areas --->
	$.fn.inlineEditCPDesc = function(replaceWith, connectWith) {

	    $(this).hover(function() {
	        $(this).addClass('hover');
	    }, function() {
	        $(this).removeClass('hover');
	    });

	    $(this).on("click touchstart", function(e){

	        var elem = $(this);

	        elem.hide();
	        elem.after(replaceWith);
	        replaceWith.val(elem.text());
	        replaceWith.focus();

	         replaceWith.blur(function() {


			 	if($(this).val() != elem.text())
				{
					<!--- Show CP Editor Save options if this changes --->
	                elem.parents('.control-point').find('.CPEFooter').css("visibility", "visible");
	                <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
	                elem.parents('.control-point').find('.btn-save-cp').prop("disabled",false);
	            }

	            //if ($(this).val() != "") {
	                connectWith.val($(this).val()).change();
	                elem.text($(this).val());
	            //}

	            $(this).remove();
	            elem.show();


	        });

	        <!--- If check for special key presses to imporve functionality --->
			replaceWith.on('keyup input', function(e){

		        <!--- If 'ESC' key is pressed undo changes --->
		        if (e.which == 27) {

		            $(this).remove();
					elem.show();
		        }

				<!--- If Enter is pressed save changes --->
		        if (e.which == 13) {
		            replaceWith.blur();
		        }

			});


	        <!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();

	    });
	};

	<!--- Becuase these buttons can be dynamically added - set click method here each time they are added --->
	function BindSaveCPInline(inpCPE)
	{
		<!--- Unbind any events already bound to this object - re-render will need to rebind objects --->
		inpCPE.find('.btn-save-cp').off();
		inpCPE.find('.btn-save-cp').unbind();

		<!--- This will save changes to XMLControlString --->
		inpCPE.find('.btn-save-cp').click(function(){
			<!--- Keeping it simple for now - Read every property of the current CP HTML - no ugly code that is difficult to maintain for all the checking what changed (less chance of bugs / easier to understand flow for updates) --->

			<!--- Prevent slow connections from letting user click save button multiple times --->
			inpCPE.find('.btn-save-cp').prop("disabled",true);

			var ControlPointData = BuildControlPointData(inpCPE);

			inpCPE.find('.CPEFooter').css("visibility", "hidden");

			// Disable actual save
			// return;

			<!--- Save to XMLControlString --->
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/control-point.cfc?method=CP_Save&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType: 'json',
				async: false,
				data:
				{
					INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>",
					controlPoint : JSON.stringify(ControlPointData),
					inpSimpleViewFlag : "<cfoutput>#inpSimpleViewFlag#</cfoutput>",
					inpTemplateFlag : "<cfoutput>#inpTemplateFlag#</cfoutput>"
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},
				success:
				<!--- Default return function for Async call back --->
				function(d)
				{
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1)
					{

						<!--- Show CP Editor Save options if this changes --->
		                inpCPE.find('.CPEFooter').css("visibility", "hidden");

		                <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
		                inpCPE.find('.btn-save-cp').prop("enabled",false);


					}
					else
					{
						<!--- No result returned --->
						bootbox.alert("<b>" + d.MESSAGE + "</b><br/>" + d.ERRMESSAGE);

						<!--- Show CP Editor Save options if this changes --->
	  	                	inpCPE.find('.CPEFooter').css("visibility", "visible");

	  	                	<!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
	  	                	inpCPE.find('.btn-save-cp').prop("disabled",false);

					}

				}

			});

		});
	}

	function BuildControlPointData(inpCPE)
	{

		var Options = [];
		var ESOptions = [];
		var Conditions = [];
		var RandomTextArray = [];

		<!--- Loop over specifed OPTIONS --->
		var OptionCounter = 1;
		inpCPE.find('.AnswerItem').each(function(id, el){

			var OptionItem = {

				'AVAL': $(el).find("#AVAL").val(),
				'AVALREG': $(el).find("#AVALREG").val(),
				'AVALRESP': $(el).find("#AVALRESP").val(),
				'AVALREDIR': $(el).find("#AVALREDIR").val(),
				'AVALNEXT': $(el).find("#AVALNEXT").val(),
				'ID' : OptionCounter,
				'TEXT' : $(el).find("#OPTION").val()
			}

			OptionItem.TRN = [];
			var TrainingArrayCounter = 1;

			var TrainingArrayRegEx = "";

			$(el).find('.TrainingItem').each(function(id, el2){

				var TrainingItem = {

					'TID':  TrainingArrayCounter,
					'TEXT' : ($(el2).find('.TEXT').val() == null) ? "" : $(el2).find('.TEXT').val(),
					'T64' : 0
				}

				OptionItem.TRN.push(TrainingItem);

				if(TrainingArrayRegEx !== "")
					TrainingArrayRegEx = TrainingArrayRegEx + "|" + TrainingItem.TEXT;
				else
					TrainingArrayRegEx = TrainingItem.TEXT;

				TrainingArrayCounter++;
			});

			<!--- Training items override regex --->
			if(TrainingArrayRegEx !== "")
			{
				<!--- default global case insensitive command (?i)--->
				OptionItem.AVALREG = "(?i)" + TrainingArrayRegEx;

				<!--- update local GUI too --->
				$(el).find("#AVALREG").val(OptionItem.AVALREG);
			}

			Options.push(OptionItem);

			OptionCounter++;

		});

		<!--- Loop over specifed Expert System OPTIONS --->
		var OptionCounter = 1;
		inpCPE.find('.ESItem').each(function(id, el){

			var ESOptionItem = {

				'ESREG': ($(el).find('#ESREG').val() == null) ? "" : $(el).find('#ESREG').val(),
				'ESTEXT': ($(el).find('#ESTEXT').val() == null) ? "" : $(el).find('#ESTEXT').val(),
				'ESID' : OptionCounter
			}

			ESOptions.push(ESOptionItem);

			OptionCounter++;

		});

		<!--- Hide the complexity of differing question types from the end user - in UI all are ONESELECTION but do not require answer list --->
		if(Options.length > 0 && inpCPE.find('#TYPE').val() == "SHORTANSWER")
		{
			inpCPE.find('#TYPE').val("ONESELECTION");
		}

		<!--- Hide the complexity of differing question types from the end user - in UI all are ONESELECTION but do not require answer list --->
		if(Options.length == 0 && inpCPE.find('#TYPE').val() == "ONESELECTION")
		{
			inpCPE.find('#TYPE').val("SHORTANSWER");
		}

		<!--- Loop over specifed CONDITIONS --->
		var ConditionsCounter = 1;
		inpCPE.find('.ConditionItem').each(function(id, el){

			<!--- Overwite RESPONSE2 with Valid RESPONSE --->
			var CurrType = ($(el).find('#CTYPE').val() == null) ? "CDF" : $(el).find('#CTYPE').val();
			// CurrType = (CurrType == 'RESPONSE2') ? "RESPONSE" : CurrType;

			<!--- Also show default BOVA text - this text is automatically searched for on single seltions as well as any answer format options --->
			if(($(el).find('#BOAV').val() == null || $(el).find('#BOAV').val() == '') && ($(el).find('#BOV').val() != null && $(el).find('#BOV').val() != ''))
			{
				$(el).find('#BOAV').val($(el).find('#BOV :selected').text());
			}

			var ConditionItem = {
				'CID':  ConditionsCounter,
				'TYPE' : CurrType,
				'BOQ' : ($(el).find('#BOQ').val() == null) ? "0" : $(el).find('#BOQ').val(),
				'BOC' : ($(el).find('#BOC').val() == null) ? "=" : $(el).find('#BOC').val(),
				'BOV' : ($(el).find('#BOV').val() == null) ? "" : $(el).find('#BOV').val(),
				'BOAV' : ($(el).find('#BOAV').val() == null) ? "" : $(el).find('#BOAV').val(),
				'BOCDV' : ($(el).find('#BOCDV').val() == null) ? "" : $(el).find('#BOCDV').val(),
				'BOTNQ' : ($(el).find('#BOTNQ').val() == null) ? "0" : $(el).find('#BOTNQ').val(),
				'DESC' : 'DESC'
			}

			Conditions.push(ConditionItem);

			ConditionsCounter++;

		});

		<!--- Loop over specifed RandomTextArray --->
		var RandomTextArrayCounter = 1;
		inpCPE.find('.RandomTextArrayItem').each(function(id, el){

			var RandomTextArrayItem = {
				'TID':  RandomTextArrayCounter,
				'TEXT' : ($(el).find('.RTEXT').val() == null) ? "" : $(el).find('.RTEXT').val(),
				'T64' : 0
			}

			RandomTextArray.push(RandomTextArrayItem);

			RandomTextArrayCounter++;

		});


		<!--- If this is not API type - delete default API values  - makes string smaller in DB--->
		if(inpCPE.find('#TYPE').val() != "API")
		{
			inpCPE.find('#API_ACT').val("");
			inpCPE.find('#API_DATA').val("");
			inpCPE.find('#API_DIR').val("");
			inpCPE.find('#API_DOM').val("");
			inpCPE.find('#API_PORT').val("");
			inpCPE.find('#API_HEAD').val("");
			inpCPE.find('#API_FORM').val("");
			inpCPE.find('#API_RA').val("");
			inpCPE.find('#API_TYPE').val("");
		}

		<!--- Cleanup Nulls in this structure - the value from key [XXX] is NULL, which is the same as not existing in CFML --->
		var ControlPointData = {

			'AF' : (inpCPE.find('#AF').val() == null) ? "" : inpCPE.find('#AF').val(),
			'API_ACT' : (inpCPE.find('#API_ACT').val() == null) ? "" : inpCPE.find('#API_ACT').val(),
			'API_DATA' : (inpCPE.find('#API_DATA').val() == null) ? "" : inpCPE.find('#API_DATA').val(),
			'API_DIR' : (inpCPE.find('#API_DIR').val() == null) ? "" : inpCPE.find('#API_DIR').val(),
			'API_DOM' : (inpCPE.find('#API_DOM').val() == null) ? "" : inpCPE.find('#API_DOM').val(),
			'API_PORT' : (inpCPE.find('#API_PORT').val() == null) ? "" : inpCPE.find('#API_PORT').val(),
			'API_HEAD' : (inpCPE.find('#API_HEAD').val() == null) ? "" : inpCPE.find('#API_HEAD').val(),
			'API_FORM' : (inpCPE.find('#API_FORM').val() == null) ? "" : inpCPE.find('#API_FORM').val(),
			'API_RA' : (inpCPE.find('#API_RA').val() == null) ? "" : inpCPE.find('#API_RA').val(),
			'API_TYPE' : (inpCPE.find('#API_TYPE').val() == null) ? "" : inpCPE.find('#API_TYPE').val(),
			'BOFNQ' : (inpCPE.find('#BOFNQ').val() == null) ? "0" : inpCPE.find('#BOFNQ').val(),
			'CONDITIONS' : Conditions,
			'ERRMSGTXT' : "",
			'GID' : "1",
			'ID' : inpCPE.find('#ID').val(),
			'IENQID' :  (inpCPE.find('#IENQID').val() == null) ? "0" : inpCPE.find('#IENQID').val(),
			'IHOUR' :  (inpCPE.find('#IHOUR').val() == null) ? "10" : inpCPE.find('#IHOUR').val(),
			'IMIN' :  (inpCPE.find('#IMIN').val() == null) ? "00" : inpCPE.find('#IMIN').val(),
			'IMRNR' :  (inpCPE.find('#IMRNR').val() == null) ? "0" : inpCPE.find('#IMRNR').val(),
			'INOON' :  (inpCPE.find('#INOON').val() == null) ? "01" : inpCPE.find('#INOON').val(),
			'INRMO' :  (inpCPE.find('#INRMO').val() == null) ? "NEXT" : inpCPE.find('#INRMO').val(),
			'ITYPE' :  (inpCPE.find('#ITYPE').val() == null) ? "HOURS" : inpCPE.find('#ITYPE').val(),
			'IVALUE' :  (inpCPE.find('#IVALUE').val() == null) ? "24" : inpCPE.find('#IVALUE').val(),
			'LMAX' :  (inpCPE.find('#LMAX').val() == null) ? "0" : inpCPE.find('#LMAX').val(),
			'LDIR' :  (inpCPE.find('#LDIR').val() == null) ? "-1" : inpCPE.find('#LDIR').val(),
			'LMSG' :  (inpCPE.find('#LMSG').val() == null) ? "" : inpCPE.find('#LMSG').val(),
			'OIG' : (inpCPE.find('#OIG').val() == null) ? "0" : inpCPE.find('#OIG').val(),
			'OPTIONS' : Options,
			'ESOPTIONS' : ESOptions,
			'REQUIREDANS' : "undefined",
			'RQ' : inpCPE.find('#RQ').val(),
			'SCDF' : (inpCPE.find('#SCDF').val() == null) ? "0" : inpCPE.find('#SCDF').val(),
			'TDESC' : (inpCPE.find('#TDESC').val() == null) ? "" : inpCPE.find('#TDESC').val(),
			'TEXT' : (inpCPE.find('#CPText').val() == null) ? "" : inpCPE.find('#CPText').val(),
			'TYPE' : inpCPE.find('#TYPE').val(),
			'SWT' : (inpCPE.find('#SWT').val() == null) ? "0" : inpCPE.find('#SWT').val(),
			'ANSTEMP' : (inpCPE.find('#ANSTEMP').val() == null) ? "" : inpCPE.find('#ANSTEMP').val(),
			'T64' : "0",
			'RTF' : (inpCPE.find('#RTF').is(':checked') == true) ? "1" : "0",
			'RTEXT' : RandomTextArray
		};


		return ControlPointData;

	}

	function BindCPCancel(inpObj)
	{
		<!--- Revert display and revert to current saved CP data   LoadingCPs --->

		inpObj.click(function()
		{
			var CurrentCPObj = $(this).parents('.control-point');

			CurrentCPObj.find('.LoadingCPs').show();

			<!--- Read Current Data From XMLControlString --->
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/control-point.cfc?method=ReadCPDataById&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType: 'json',
				async: false,
				data:
				{
					INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>",
					inpQID : CurrentCPObj.attr('data-control-rq-id'),
					inpTemplateFlag : "<cfoutput>#inpTemplateFlag#</cfoutput>"
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("*Error. No Response from the remote server. Check your connection and try again.");},
				success:
				<!--- Default return function for Async call back --->
				function(d)
				{
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1)
					{
						<!--- Declare as var so it is available asyncronously to the next AJax call --->
						var CurrCPObj = d.CPOBJ;

						<!--- Read Current Data From XMLControlString --->
						$.ajax({
							type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
							url: '/session/sire/models/cfc/control-point.cfc?method=RenderCP&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
							dataType: 'json',
							async: false,
							data:
							{
								INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>",
								controlPoint : JSON.stringify(d.CPOBJ),
								inpCPList: JSON.stringify(inpCPListjsVar),
								inpSimpleViewFlag : "<cfoutput>#inpSimpleViewFlag#</cfoutput>",
								inpTemplateFlag : "<cfoutput>#inpTemplateFlag#</cfoutput>",
								inpHFE : "<cfoutput>#hfe#</cfoutput>",
								inpAFE : "<cfoutput>#afe#</cfoutput>"
							},
							error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("*Error. No Response from the remote server. Check your connection and try again.");},
							success:
							<!--- Default return function for Async call back --->
							function(d2)
							{
								<!--- RXRESULTCODE is 1 if everything is OK --->
								if (d2.RXRESULTCODE == 1)
								{
									RenderCPToPage(d2.HTML, CurrCPObj.RQ, CurrCPObj.TYPE, CurrentCPObj, "REPLACE");
								}
								else
								{
									<!--- No result returned --->
									bootbox.alert("General Error processing your request. ");
								}

							}

						});

					}
					else
					{
						<!--- No result returned --->
						bootbox.alert("General Error processing your request. ");
					}

				}

			});


		});

	}


	<!--- Becuase these buttons can be dynamically added - set click method here each time they are added --->
	function RenderNewCP(inpRQ, inpType, inpTarget, inpLocation)
	{
		// inpCPObj = $('#cp' + inpObj.attr('data-control-point-id'))	;

		var NewCPID = 0;


		if(inpTarget.attr('data-control-rq-id') > 0)
		{
			if(inpLocation == 'BEFORE')
			{
				NewCPID = parseInt( inpTarget.attr('data-control-rq-id') );
			}
	        else
	        {
		        NewCPID = parseInt( inpTarget.attr('data-control-rq-id') ) + 1;
			}
		}
		else
		{
			<!--- Get max control point index --->
			NewCPID = $('.control-point').length + 1;

			<!--- Force location to before no matter what --->
			inpLocation = 'BEFORE';

		}


		<!--- Set this value as the new CPs RQ POSITION --->
		// $("#ChooseCPToAdd .btn-add-cp").attr('data-control-point-id', $(this).attr('data-control-point-id'));

		<!--- Store the CP this was click on - used for Render Editor --->
		// $("#ChooseCPToAdd .btn-add-cp").data('inpCP', $(this).parents('.control-point'));


		<!--- Build an empty Action DataObj --->
		var Options = [];
		var ESOptions = [];
		var Conditions = [];
		var RandomTextArray = [];

		var ControlPointData = {

				'AF' : "HIDDEN",
				'API_ACT' : "",
				'API_DATA' : "",
				'API_DIR' : "",
				'API_DOM' : "URL Goes Here",
				'API_PORT' : "80",
				'API_HEAD' : "",
				'API_FORM' : "",
				'API_RA' : "",
				'API_TYPE' : "GET",
				'BOFNQ' : "0",
				'CONDITIONS' : Conditions,
				'ERRMSGTXT' : "",
				'GID' : "1",
				'ID' : "0",  <!--- NextPID = RetVarGetValueAggregate + 1 --->
				'IENQID' : "0",
				'IHOUR' : "0",
				'IMIN' : "0",
				'IMRNR' : "0",
				'INOON' : "01",
				'INRMO' : "END",
				'ITYPE' : "HOURS",
				'IVALUE' : "2",
				'LMAX' : "0",
				'LDIR' : "-1",
				'LMSG' : "",
				'OIG' : "0",
				'OPTIONS' : Options,
				'ESOPTIONS' : ESOptions,
				'REQUIREDANS' : "undefined",
				'RQ' : NewCPID,
				'SCDF' : "0",
				'TDESC' : "",
				'TEXT' : "",
				'TYPE' : inpType,
				'SWT' : "1",
				'ANSTEMP' : "0",
				'T64' : "0",
				'TGUIDE' : "",
				'RTF' :  "0" ,
				'RTEXT' : RandomTextArray
			};

		<!--- Read the CP from the component --->
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/control-point.cfc?method=Add_ControlPoint&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			async: false,
			data:
			{
				INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>",
				controlPoint : JSON.stringify(ControlPointData),
				inpSimpleViewFlag : "<cfoutput>#inpSimpleViewFlag#</cfoutput>",
				inpTemplateFlag : "<cfoutput>#inpTemplateFlag#</cfoutput>",
				inpHFE : "<cfoutput>#hfe#</cfoutput>",
				inpAFE : "<cfoutput>#afe#</cfoutput>"
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},
			success:
			<!--- Default return function for Async call back --->
			function(d)
			{
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1)
				{
					RenderCPToPage(d.HTML, ControlPointData.RQ, inpType, inpTarget, inpLocation);
				}
				else
				{
					var el = $('');

					<!--- No result returned --->
					bootbox.alert("General Error processing your request. ");
				}

			}

		});




	}

	<!--- Render / Add each item to the list from Template --->
	function AddRandomTextToList(inpText, inpCPE)
	{
		<!--- The HTML for Answer and CPEditor are stored in coldfusion variables  AnswerListItemTemplate --->
		<!--- This outputs a previously defined Coldfusion variable as a javascript variable --->
		<cfoutput>
			var #toScript(TextListItemTemplate, "TextListItemjsVar")#;
		</cfoutput>

		<!--- Create a jquery reference to the returned HTML and store it in 'inpAnswerItem' --->
		var inpTextItem = $(TextListItemjsVar);

		var max = 0;
		$('.control-point-char').each(function() {
		    max = Math.max(parseInt($(this).attr('id').replace('cp-char-count-', '')), max);
		});
		max++;

		<!--- Set a unnique Id for each item --->
		inpTextItem.find(".control-point-char").attr('id', 'cp-char-count-' + max);

		<!--- Set OPTION text --->
		inpTextItem.find("#TID").val(0);
		inpTextItem.find("#T64").val(0);
		inpTextItem.find(".RTEXT").val(inpText);

		BindRandomTextOptions(inpTextItem);

		<!--- Apend to list, then find text input and set focus --->
		inpCPE.find('.rtext_list').append(inpTextItem);

		<!--- Show CP Editor Save options if this changes --->
		inpCPE.parent().find('.CPEFooter').css("visibility", "visible");

		<!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
		inpCPE.parent().find('.btn-save-cp').prop("disabled",false);

		<!--- Wait unil the new object is appended before trying to do this --->
		// Re-Initializes and creates emoji set from sprite sheet
		var emojiPicker = new EmojiPicker({
		emojiable_selector: '[data-emojiable=true]',
		assetsPath: 'https://cdnjs.cloudflare.com/ajax/libs/emoji-picker/1.1.5/img',
		popupButtonClasses: 'far fa-smile'
		});
		// Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
		// You may want to delay this step if you have dynamically created input fields that appear later in the loading process
		// It can be called as many times as necessary; previously converted input fields will not be converted again
		emojiPicker.discover();

		BindRandomTextCharacterCounts(inpTextItem);

		<!--- After append to list and init emoji editor, then find text input and set focus --->
		inpCPE.find('.rtext_list').find('.RandomTextArrayItem:last').find('div .emoji-wysiwyg-editor').focus();
	}

	function BindRandomTextOptions (inpObj)
	{
	   <!--- On Answer Change --->
	    inpObj.find(".RTEXT").change(function(e) {

		    <!--- Show CP Editor Save options if this changes --->
		   $(this).parents('.control-point-body').find('.CPEFooter').css("visibility", "visible");

		   <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
		   $(this).parents('.control-point-body').find('.btn-save-cp').prop("disabled",false);

	    });

	    inpObj.find('.delete-text').on('click', function(event) {

		   <!--- Show CP Editor Save options if this changes --->
		   $(this).parents('.control-point-body').find('.CPEFooter').css("visibility", "visible");

		   <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
		   $(this).parents('.control-point-body').find('.btn-save-cp').prop("disabled",false);

		   $(this).parents('.row.row-small').remove();

	    });

	}

	<!--- Render / Add each item to the list from Template --->
	function AddTrainingTextToList(inpText, inpCPE)
	{
		<!--- The HTML for Answer and CPEditor are stored in coldfusion variables  AnswerListItemTemplate --->
		<!--- This outputs a previously defined Coldfusion variable as a javascript variable --->
		<cfoutput>
			var #toScript(TrainingItemTemplate, "TrainingTextListItemjsVar")#;
		</cfoutput>

		<!--- Create a jquery reference to the returned HTML and store it in 'inpAnswerItem' --->
		var inpTextItem = $(TrainingTextListItemjsVar);

		inpTextItem.find("#TID").val(0);

		<!--- Set OPTION text --->
		inpTextItem.find("#T64").val(0);
		inpTextItem.find(".TEXT").val(inpText);

		BindTrainingTextOptions(inpTextItem);

		<!--- Apend to list, then find text input and set focus --->
		inpCPE.find('.training-text-list').append(inpTextItem);

		<!--- Show CP Editor Save options if this changes --->
		inpCPE.parent().find('.CPEFooter').css("visibility", "visible");

		<!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
		inpCPE.parent().find('.btn-save-cp').prop("disabled",false);

		<!--- After append to list and init emoji editor, then find text input and set focus --->
		inpCPE.find('.training-text-list').find('.TrainingItem:last').find('.TEXT').focus();
	}

	function BindTrainingTextOptions (inpObj)
	{
	   <!--- On Answer Change --->
	    inpObj.find(".TEXT").change(function(e) {

		    <!--- Show CP Editor Save options if this changes --->
		   $(this).parents('.control-point-body').find('.CPEFooter').css("visibility", "visible");

		   <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
		   $(this).parents('.control-point-body').find('.btn-save-cp').prop("disabled",false);

	    });

	    inpObj.find('.delete-training').on('click', function(event) {

		   <!--- Show CP Editor Save options if this changes --->
		   $(this).parents('.control-point-body').find('.CPEFooter').css("visibility", "visible");

		   <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
		   $(this).parents('.control-point-body').find('.btn-save-cp').prop("disabled",false);

		   $(this).parents('.TrainingItem').remove();

	    });

	}

	<!--- Can be called either on new object or revert on Cancel --->
	function RenderCPToPage(inpHTML, inpRQ, inpType, inpTarget, inpLocation)
	{
		var el = $(inpHTML);

		<!---  RE-intitialize any newly added SELECT2 boxes --->
		el.find(".Select2").select2( { theme: "bootstrap"} );

		<!--- Bind delete button action to newly created object --->
		BindDeleteCPInline(el.find('.control-point-btn-delete'));

		<!--- Bind save button action to newly created object --->
		BindSaveCPInline(el);

		<!--- Bind the cancel option - revert to last saved values --->
		BindCPCancel(el.find('.btn-cancel-cpe'));

		<!--- Make sure the new object is droppable and accepts new CP objects --->
		SetupDroppables(el);

		<!--- inplace editing of CP Desc without save on close .cp-desc-input --->
		replaceWithCPDesc = $('<input name="tempCPDesc" type="text" class="cp-desc-input-temp" />');

		<!--- Becuase these buttons can be dynamically added - set click method here for each obj--->
		el.find('.cp-desc-input').inlineEditCPDesc(replaceWithCPDesc, el.find('.cp-desc-input'));
		el.find('.cp-desc-input').popover({trigger:'hover', template:'<div class="popover cp-desc-input-pop" role="tooltip"><div class="arrow"></div><div class="popover-body">Step Description</div></div>'});

		el.find('.RandomTextArrayItem').each(function(index, item) {
			BindRandomTextOptions($(item));
		});

		el.find('.TrainingItem').each(function(index, item) {
			BindTrainingTextOptions($(item));
		});

		// el.find('.toggle-ai-opt').on('click', function(event)
		// {
		// 	$(this).parents('.AnswerItem').find('.wrapper-training-text').slideToggle();
		// 	$(this).toggleClass('fa-angle-down');
		// 	$(this).toggleClass('fa-angle-up');
		// });

		<!--- Initialize Special Rules Engine Stuff Here--->
		if( inpType == 'BRANCH')
		{
			<!--- Load the add event for each Rules condition list --->
			el.find('.AddCondition').click(function()
			{
				<!--- Search up for the BrachContainer then down for the ConditionsList container and then pass it into the function --->
				AddConditionToList($(this).parents('#BranchOptionsContainer').find('#ConditionsList'));

				<!--- Show CP Editor Save options if this changes --->
                	$(this).parents('.control-point').find('.CPEFooter').css("visibility", "visible");
                	<!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
                	$(this).parents('.control-point').find('.btn-save-cp').prop("disabled",false);

			});

			$.each(el.find('.ConditionItem'), function (index, item) {
			   	BindSetupRulesEngineBOCDVPicker($(this));

			   	BindBranchSelectionChange($(this).find('#BOTNQ'));
				BindBranchSelectionChange($(this).find('#BOQ'));
				BindBranchSelectionChange($(this).find('#BOC'));
				BindBranchSelectionChange($(this).find('#BOCDV'));
				BindBranchSelectionChange($(this).find('#BOAV'));

				<!--- Allow user to remove conditions --->
  			   	$(this).find('.RemoveCondition').on('click', function()
  				{
  					<!--- Remove the XML - next save will not include this --->
  					$(this).parents('.ConditionItem').remove();

  					<!--- Conditions are applied in order shown - first Compare wins --->
  					reIndexConditions(el.find('#ConditionsList'));

  					<!--- Show CP Editor Save options if this changes --->
  	                	el.find('.CPEFooter').css("visibility", "visible");

  	                	<!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
  	                	el.find('.btn-save-cp').prop("disabled",false);

  				});


			});

			BindBranchSelectionChange(el.find('#BOFNQ'));

			<!--- Setup sortables for Conditions --->
			BindSortableCond(el.find('#ConditionsList'));
		}

		el.find('.add-more-text').on('click', function(event)
		{

			var content = $(this).parents('.control-point').find('.control-point-body');
			AddRandomTextToList('', content);
		});

		if( inpType == 'ONESELECTION' || inpType == 'SHORTANSWER')
		{
			// BindQuestionTypeChange(el.find('.cp_type'));

			el.find('.AnswerItem').each(function(){
	               BindSetupAnswerOptions ($(this));
	            });

			el.find('.add-more-answer').on('click', function(event)
		    {
			    var content = $(this).parents('.control-point').find('.cp-content');
	            AddOptionToList('', '', '', '', '', '', content);
	        });

	        <!--- On Answer Format Change --->
			el.find("#AF,#ITYPE,#IVALUE, #IMRNR, #INRMO").change(function(e) {

				<!--- Show CP Editor Save options if this changes --->
	            $(this).parents('.QuestionContainer').parent().find('.CPEFooter').css("visibility", "visible");

	            <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
	            $(this).parents('.QuestionContainer').parent().find('.btn-save-cp').prop("disabled",false);

			});

			//show/hide question timeout inteval
		    el.find(".toggle-inteval-question").on("click",function(e)
		    {
		        var toggle_div = $(this).parent().find('.div-toggle-inteval');

				$(this).find('i').toggleClass('fa-angle-up');
				$(this).find('i').toggleClass('fa-angle-down');

				$(this).parent().find('.div-toggle-inteval').slideToggle();

		    });

		    el.find(".toggle-answer-intents").on("click",function(e)
		    {
			    $(this).parents('.QuestionContainer').find('.wrapper-answer-text').slideToggle();
 			   $(this).find('i').toggleClass('fa-angle-up');
 			   $(this).find('i').toggleClass('fa-angle-down');

		    });

		}

		<!--- Initialize Special API Stuff Here--->
		if( inpType == 'API')
		{
			<!--- On Answer Format Change --->
			el.find("#API_RA, #API_TYPE, #API_DOM, #API_DIR, #API_PORT, #API_HEAD, #API_FORM, #API_ACT, #API_DATA").change(function(e) {

				<!--- Show CP Editor Save options if this changes --->
	            $(this).parents('.control-point').find('.CPEFooter').css("visibility", "visible");

	            <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
	            $(this).parents('.control-point').find('.btn-save-cp').prop("disabled",false);

			});

		}

		<!--- Initialize Special API Stuff Here--->
		if( inpType == 'CDF')
		{
			<!--- On various selection Changes --->
			el.find("#SCDF").change(function(e) {

				<!--- Show CP Editor Save options if this changes --->
	            $(this).parents('.control-point').find('.CPEFooter').css("visibility", "visible");

	            <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
	            $(this).parents('.control-point').find('.btn-save-cp').prop("disabled",false);

			});


			el.find("#btn-add-cdf").click(function()
			{
				$('#btn-save-cdf').data('inpCPE', el);
				$('#inpCDFName').val('');
				$('#AddNewCDFModal').modal('show');
			});

			PopulateCDFSelectBox(el.find('#SCDF'), el.find('#CDF_SCDF').val());

		}

		<!--- Initialize Special API Stuff Here--->
		if( inpType == 'INTERVAL')
		{
			<!--- On Answer Format Change --->
			el.find("#API_RA, #API_TYPE, #API_DOM, #API_DIR, #API_PORT, #API_HEAD, #API_FORM, #API_ACT, #API_DATA").change(function(e) {

				<!--- Show CP Editor Save options if this changes --->
	            $(this).parents('.control-point').find('.CPEFooter').css("visibility", "visible");

	            <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
	            $(this).parents('.control-point').find('.btn-save-cp').prop("disabled",false);

			});

			<!--- On various selection Changes --->
			el.find("#ITYPE, #IVALUE, #IHOUR, #IMIN, #INOON, #IENQID, #IMRNR, #INRMO").change(function(e) {

				<!--- Show CP Editor Save options if this changes --->
	            $(this).parents('.control-point').find('.CPEFooter').css("visibility", "visible");

	            <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
	            $(this).parents('.control-point').find('.btn-save-cp').prop("disabled",false);

				<!--- Reset these every time they change --->
				onChangeIntervalType($(this).parents('.control-point'));
			});

			<!--- Set this on load --->
			onChangeIntervalType(el);

		}


		<!--- Initialize Special OPTIN Stuff Here--->
		if( inpType == 'OPTIN')
		{
			<!--- On various selection Changes --->
			el.find("#OIG").change(function(e) {

				<!--- Show CP Editor Save options if this changes --->
	            $(this).parents('.control-point').find('.CPEFooter').css("visibility", "visible");

	            <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
	            $(this).parents('.control-point').find('.btn-save-cp').prop("disabled",false);

			});


			el.find("#btn-add-group").click(function()
			{
				$('#btn-save-group').data('inpCPE', el);
				$('#subscriber_list_name').val('');
				$('#AddNewSubscriberList').modal('show');
			});

			PopulateSubscriberListSelectBox(el.find('#OIG'), el.find('#OPTIN_OIG').val());

		}

		if(inpLocation == 'BEFORE')
		{
			el.insertBefore(inpTarget);
		}
        else if(inpLocation == 'AFTER')
        {
            el.insertAfter(inpTarget);
		}
		else if(inpLocation == 'REPLACE')
		{
			inpTarget.replaceWith(el);
		}

		<!--- Do this stuff AFTER been added to the page --->

		<!--- Assign based on global variable - these are reset on each load so it does not matter if they grow high numbers --->
		$(".control-point-char").each(function(index, el2) {
	        $(el2).attr('id', 'cp-char-count-'+(NextCPCID++));
	    });

		<!--- bind the char counter to this new obj if it has one --->
		BindCharacterCounts(el);

		<!--- bind the char counters to this new obj if it has random text sections --->
		el.find(".RandomTextArrayItem").each(function(index, el2) {
		   BindRandomTextCharacterCounts($(el2));
		});

		el.find('.RTF').each(function(index, el2) {
			BindRTFClicks($(el2));
		});

		el.find('.info-popover').popover({
			html:true,
			placement: function(tip, ele) {
				var width = $(window).width();

				var iconPos  = $(ele).offset(),
				toLeft  = iconPos.left,
				toRight = $(window).width() - ($(ele).width() + toLeft);
				if(width < 500)
				{
					return 'top';
				}
				return toRight > 500 ? 'right' : 'bottom' ;
			}
		});

		<!--- Wait unil the new object is appended before trying to do this --->
		// Re-Initializes and creates emoji set from sprite sheet
        var emojiPicker = new EmojiPicker({
          emojiable_selector: '[data-emojiable=true]',
          assetsPath: 'https://cdnjs.cloudflare.com/ajax/libs/emoji-picker/1.1.5/img',
          popupButtonClasses: 'far fa-smile'
        });
        // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
        // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
        // It can be called as many times as necessary; previously converted input fields will not be converted again
        emojiPicker.discover();

		<!--- Re-Index CPs on HTML page - Physical Ids remain the same. --->
		reIndexQuestions();

		el.find('.LoadingCPs').hide();



	}

</script>




<!--- Advanced Template Editor CP Editor UI Scripts --->

<script type="application/javascript">

	<!--- Global variable to track when inti is complete --->
	var NextCPCID = 1;

	$(function() {


		<!--- https://cdnjs.cloudflare.com/ajax/libs/emoji-picker/1.1.5/img
		/public/sire/assets/emoji-picker/lib/img/
		--->

		 // Initializes and creates emoji set from sprite sheet
        var emojiPicker = new EmojiPicker({
          emojiable_selector: '[data-emojiable=true]',
          assetsPath: 'https://cdnjs.cloudflare.com/ajax/libs/emoji-picker/1.1.5/img',
          popupButtonClasses: 'far fa-smile'
        });
        // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
        // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
        // It can be called as many times as necessary; previously converted input fields will not be converted again
       emojiPicker.discover();


		<!--- Need to re-do this so it can be called next plus 1 or max on page so new ones can be added--->
	    $(".control-point-char").each(function(index, el) {
	        $(el).attr('id', 'cp-char-count-'+(NextCPCID++));
	    });

		<!--- Becuase these buttons can be dynamically added - set click method here for each obj--->
		$.each($('.control-point'), function (index, item) {
		   	BindCharacterCounts($(this));
		});

		$.each($('.RandomTextArrayItem'), function (index, item) {
			BindRandomTextCharacterCounts($(this));
		});

		$.each($('.RTF'), function (index, item) {
			BindRTFClicks($(item));
		});

	});

	function BindRTFClicks(inpObj)
	{
		inpObj.on('click', function(event) {

			if( $(this).is(':checked') )
			{
				$(this).parents('.control-point-body').find('.wrapper-random-text').show();
			}
			else
			{
				$(this).parents('.control-point-body').find('.wrapper-random-text').hide();
			}

			<!--- Show CP Editor Save options if this changes --->
			inpObj.parents('.control-point').find('.CPEFooter').css("visibility", "visible");
			<!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
			inpObj.parents('.control-point').find('.btn-save-cp').prop("disabled",false);

		});
	}

	<!--- Because these buttons can be dynamically added - set click method here each time they are added --->
	function BindRandomTextCharacterCounts(inpObj)
	{
		var FirstPassSetupComplete = false;

		 if (inpObj.find('.RTEXT').length > 0) {

		   var counterId = inpObj.find(".control-point-char").attr('id');;

		   var UnicodeWarningObj = inpObj.find('.see-unicode-mess');

		   var text = inpObj.find(".RTEXT")[0];
		   var opttext_add="";

		   var str = $(text).val();

		   //str = str.replace(new RegExp("\n", 'gi'),"23");
		   //str = str.replace(/(?:\r\n|\r|\n)/g, '<br />');

		   AdvCountText("#"+counterId, str, UnicodeWarningObj);

		  // $(text).unbind('input change');
		  // $(text).bind('input change', function(e) {

		    $(text).off('input change');
		   $(text).on('input change', function(e) {

			  AdvCountText("#"+counterId, $(text).val(), UnicodeWarningObj);

			  $(text).val($(text).val().replace("’", "'"));
			  $(text).val($(text).val().replace("‘", "'"));

			  var firstMessageContent = $('.msg-content textarea:first-child').val();

			  var text1 =  $(this).next('.emoji-wysiwyg-editor').html();
			  var text2 =  $(text).val();

			  if(text1)
			  {
				 var divText = AdvConvertString(text1);
				 //$(this).next('.emoji-wysiwyg-editor').html(divText);
			  }

			  text2 = AdvConvertString(text2);

			  var fultext= text2;

			  if(fultext.length > 160)
			  {
				 $(this).closest(".control-point-body").find(".span_text").html(fultext.substring(0,160).replace(/\n/g, "</br>") + '...');
			  }
			  else
			  {
				 $(this).closest(".control-point-body").find(".span_text").html(fultext.replace(/\n/g, "</br>"));
			  }

			  <!--- Wait until intialization has been done before enabling these buttone  --->
			  if(FirstPassSetupComplete)
			  {
				  <!--- Show CP Editor Save options if this changes --->
				  inpObj.parents('.control-point').find('.CPEFooter').css("visibility", "visible");
				  <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
				  inpObj.parents('.control-point').find('.btn-save-cp').prop("disabled",false);
				 }

		   });

			 <!--- Initialization complete - changes will now trigger save option --->
			 FirstPassSetupComplete = true;

		}

	}

	<!--- Because these buttons can be dynamically added - set click method here each time they are added --->
	function BindCharacterCounts(inpObj)
	{

	    var FirstPassSetupComplete = false;

        	if (inpObj.find('textarea').length > 0) {

            var counterId = inpObj.find(".control-point-char").attr('id');;

            var UnicodeWarningObj = inpObj.find("textarea").first().parent().find('.see-unicode-mess');

            var text = inpObj.find("textarea")[0];
            var opttext_add="";
            if(inpObj.find('#select-select-val').val() != undefined && inpObj.find('#select-select-type').val() != undefined)
            {
                opttext_add="Reply YES to confirm, HELP for help or STOP to cancel.Up to "+ inpObj.find('#select-select-val').val()+ " msg/ "+ inpObj.find('#select-select-type').val()+". Msg&Data rates may apply.";
            }

            var str = $(text).val();

            //str = str.replace(new RegExp("\n", 'gi'),"23");
            //str = str.replace(/(?:\r\n|\r|\n)/g, '<br />');

            AdvCountText("#"+counterId, str+ opttext_add, UnicodeWarningObj);

           // $(text).unbind('input change');
           // $(text).bind('input change', function(e) {

	        $(text).off('input change');
            $(text).on('input change', function(e) {

                if($(this).closest(".cp-content").find('#select-select-val').val() != undefined && $(this).closest(".cp-content").find('#select-select-type').val() != undefined)
                {
                    opttext_add="Reply YES to confirm, HELP for help or STOP to cancel.Up to "+$(this).closest(".cp-content").find('#select-select-val').val()+ " msg/ "+$(this).closest(".cp-content").find('#select-select-type').val()+". Msg&Data rates may apply.";
                }

                AdvCountText("#"+counterId, $(text).val()+ opttext_add, UnicodeWarningObj);

                $(text).val($(text).val().replace("’", "'"));
                $(text).val($(text).val().replace("‘", "'"));
                var firstMessageContent = $('.msg-content textarea:first-child').val();

                if($("#preview_message").length > 0){
                    $("#preview_message").text(AdvConvertString(firstMessageContent));
                }

                var text1 =  $(this).next('.emoji-wysiwyg-editor').html();
                var text2 =  $(text).val();

                if(text1)
                {
                    var divText = AdvConvertString(text1);
                    //$(this).next('.emoji-wysiwyg-editor').html(divText);
                }

                text2 = AdvConvertString(text2);

                var fultext= text2+ opttext_add;

                if(fultext.length > 160)
                {
                    $(this).closest(".control-point-body").find(".span_text").html(fultext.substring(0,160).replace(/\n/g, "</br>") + '...');
                }
                else
                {
                    $(this).closest(".control-point-body").find(".span_text").html(fultext.replace(/\n/g, "</br>"));
                }

                <!--- Wait until intialization has been done before enabling these buttone  --->
                if(FirstPassSetupComplete)
                {
	                <!--- Show CP Editor Save options if this changes --->
	                inpObj.find('.CPEFooter').css("visibility", "visible");
	                <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
	                inpObj.find('.btn-save-cp').prop("disabled",false);
				}

            });

			<!--- Initialization complete - changes will now trigger save option --->
			FirstPassSetupComplete = true;

        }

	}


	<!--- When the emoji editor is being clicked on, use the following to detect changes in the contenteditable div and trigger the change event on the element above it - usually the textarea--->
	<!--- https://stackoverflow.com/questions/1391278/contenteditable-change-events --->
	$('body').on('focus', '[contenteditable]', function() {
	    var $this = $(this);
	    $this.data('before', $this.html());
	    return $this;
	}).on('blur keyup paste input', '[contenteditable]', function() {
	    var $this = $(this);
	    if ($this.data('before') !== $this.html()) {
	        $this.data('before', $this.html());
	        $this.trigger('change');
	    }
	    return $this;
	});


</script>

<!--- CPObj drag and drop --->
<script type="application/javascript">

	var StartSortIndex = -1;

    $(function() {

	    <!-- hide flow editor functionality - Flag to enabel or disable flow editor options-->
		<cfif hfe EQ 1>
			return;
		</cfif>


        $('.draggable').each(function( index ) {

			<!--- For tile-item menu items - use clone. Everyhting else use special indicators --->
			if( $(this).hasClass("cpobj-item")   )
			{
				$(this).draggable(
				{
					appendTo: '#CPObjStage',
					revert: "invalid", // true, false "invalid"
					// containment: "body", // #CPObjContainer #page-container
					stack: "#CPObjStage",
					opacity: .9,
					helper: "clone",
					zIndex: 300,
					cursor: "move",
					drag: function(e, ui )
					{
						var $droppable = $(this).data("current-droppable");

						if ($droppable)
						{
							updateHighlight(ui, $droppable);
						}
					}
				});
			}

		});


		<!--- Bind the droppable handeling to each control point section    control-point-border --->
		$('.cpobj-drop-zone').each(function( index ) {

           	SetupDroppables($(this));

		});


	});

	function SetupDroppables(inpObj)
	{

		<!-- hide flow editor functionality - Flag to enabel or disable flow editor options-->
		<cfif hfe EQ 1>
			return;
		</cfif>

		inpObj.droppable({
		  greedy: true,
		  accept: ".draggable",
		  tolerance: "touch",  // pointer touch
	      classes: {
	       "ui-droppable-active": "ui-state-default"

	      },
	      <!--- Visual feedback cues --->
			over: function(event, ui)
			{

				var $this = $(this);
                updateHighlight(ui, $this);
                ui.draggable.data("current-droppable", $this);

			},
			deactivate: function( event, ui )
			{
				<!---
					Do this here so all drobbable3 objects are cleaned up
					tolerance: "touch" is used so there are no issues with no-mans land and margins
					draggable obj is always big enough to be "touch"ing a drop zone
					deactivate loops over all droppable objects
				--->
				cleanupHighlight(ui, $(this));

			},
			<!--- Visual feedback cues --->
			out: function(event, ui)
			{

				cleanupHighlight(ui, $(this));

			},
			drop: function( event, ui ) {

	            <!--- Get where obj was dropped --->
	            var $newPosX = ui.offset.left - $(this).offset().left;
				var $newPosY = ui.offset.top - $(this).offset().top;

				var $this = $(this);
	            cleanupHighlight(ui, $this);

	            <!--- Check what is being dropped --->
	            if(ui.draggable.hasClass("cpobj-statement"))
	            {
		            if (isInUpperHalf(ui, $this))
	                {
	                   RenderNewCP(0, 'STATEMENT', $this, 'BEFORE');
	                }
	                else
	                {
	                   RenderNewCP(0, 'STATEMENT', $this, 'AFTER');
	                }

	            }

 	            if(ui.draggable.hasClass("cpobj-trailer"))
	            {
		            if (isInUpperHalf(ui, $this))
	                {
	                   RenderNewCP(0, 'TRAILER', $this, 'BEFORE');
	                }
	                else
	                {
	                   RenderNewCP(0, 'TRAILER', $this, 'AFTER');
	                }

	            }

				if(ui.draggable.hasClass("cpobj-rules-engine"))
	            {
		            if (isInUpperHalf(ui, $this))
	                {
	                   RenderNewCP(0, 'BRANCH', $this, 'BEFORE');
	                }
	                else
	                {
	                   RenderNewCP(0, 'BRANCH', $this, 'AFTER');
	                }

	            }

				if(ui.draggable.hasClass("cpobj-question"))
	            {
	                if (isInUpperHalf(ui, $this))
	                {
	                   RenderNewCP(0, 'ONESELECTION', $this, 'BEFORE');
	                }
	                else
	                {
	                   RenderNewCP(0, 'ONESELECTION', $this, 'AFTER');
	                }

	            }

	            if(ui.draggable.hasClass("cpobj-api"))
	            {
		            if (isInUpperHalf(ui, $this))
	                {
	                   RenderNewCP(0, 'API', $this, 'BEFORE');
	                }
	                else
	                {
	                   RenderNewCP(0, 'API', $this, 'AFTER');
	                }

	            }

	            if(ui.draggable.hasClass("cpobj-cdf"))
	            {
		            if (isInUpperHalf(ui, $this))
	                {
	                   RenderNewCP(0, 'CDF', $this, 'BEFORE');
	                }
	                else
	                {
	                   RenderNewCP(0, 'CDF', $this, 'AFTER');
	                }

	            }

				if(ui.draggable.hasClass("cpobj-interval"))
	            {
		            if (isInUpperHalf(ui, $this))
	                {
	                   RenderNewCP(0, 'INTERVAL', $this, 'BEFORE');
	                }
	                else
	                {
	                   RenderNewCP(0, 'INTERVAL', $this, 'AFTER');
	                }

	            }

			  if(ui.draggable.hasClass("cpobj-opt-in"))
	            {
		            if (isInUpperHalf(ui, $this))
	                {
	                   RenderNewCP(0, 'OPTIN', $this, 'BEFORE');
	                }
	                else
	                {
	                   RenderNewCP(0, 'OPTIN', $this, 'AFTER');
	                }

	            }

			  if(ui.draggable.hasClass("cpobj-batch-cp"))
			  {
			  	if (isInUpperHalf(ui, $this))
			      {
			  	  RenderNewCP(0, 'BATCHCP', $this, 'BEFORE');
			      }
			      else
			      {
			  	  RenderNewCP(0, 'BATCHCP', $this, 'AFTER');
			      }

			  }

			  if(ui.draggable.hasClass("cpobj-whff"))
			  {
			  	if (isInUpperHalf(ui, $this))
			      {
			  	  RenderNewCP(0, 'WFHH', $this, 'BEFORE');
			      }
			      else
			      {
			  	  RenderNewCP(0, 'WFHH', $this, 'AFTER');
			      }

			  }

			  if(ui.draggable.hasClass("cpobj-reset"))
			  {
			  	if (isInUpperHalf(ui, $this))
			      {
			  	  RenderNewCP(0, 'RESET', $this, 'BEFORE');
			      }
			      else
			      {
			  	  RenderNewCP(0, 'RESET', $this, 'AFTER');
			      }

			  }


	      	}
	    });


	}


	function isInUpperHalf(ui, $droppable)
    {
        var $draggable = ui.draggable || ui.helper;
        return (ui.offset.top + $draggable.outerHeight() / 2
                <= $droppable.offset().top + $droppable.outerHeight() / 2);
    }

    function updateHighlight(ui, $droppable)
    {
        if (isInUpperHalf(ui, $droppable) || $droppable.hasClass('default-drop-zone')) {
            $droppable.removeClass("droppable-below")
                      .addClass("droppable-above");
        } else {
            $droppable.removeClass("droppable-above")
                      .addClass("droppable-below");
        }
    }

    function cleanupHighlight(ui, $droppable)
    {
        ui.draggable.removeData("current-droppable");
        $droppable.removeClass("droppable-above droppable-below");
    }


	<!--- Need to bind sortable to new section objects --->
	function BindSortableCP(inpObj, inpHelperText)
	{
		<!-- hide flow editor functionality - Flag to enabel or disable flow editor options-->
		<cfif hfe EQ 1>
			return;
		</cfif>

		if(typeof inpHelperText == 'undefined')
			inpHelperText = '';

		if(typeof inpObj.attr("data-helper-text") !== 'undefined')
			inpHelperText = inpObj.attr("data-helper-text");


		inpObj.sortable({

				containment: "#CPContainer",
				items: '.control-point',
				handle: ".handle",
				    start: function(e, ui ){
					     ui.placeholder.height(ui.helper.outerHeight());
					},
					start: function(event, ui)
					{
<!---
						console.log('Start Sort');
						console.log('ID' + ui.item.attr('id'));
						console.log('PID' + ui.item.attr('data-control-point-physical-id'));
						console.log('data-control-rq-id ' + ui.item.attr('data-control-rq-id'));
						console.log(ui.item.index());
--->

						<!--- Track where sort object started from--->
						StartSortIndex = ui.item.index();

						//$(this).find('.SurveyQuestionBorder').css('border-style','dotted');
						ui.item.find('.CPDisplaySection').css('border-style','dotted');

			 		},
					stop: function(event, ui)
					{

<!---
						console.log('Stop Sort');
						console.log('ID' + ui.item.attr('id'));
						console.log('PID' + ui.item.attr('data-control-point-physical-id'));
						console.log('data-control-rq-id ' + ui.item.attr('data-control-rq-id'));
						console.log(ui.item.index());
						console.log(ui.item);
--->

						<!--- Only update if position actually changes--->
						if(StartSortIndex != ui.item.index())
						{
							<!--- javascript and Sort index values are zero (0) based - CF is one based - careful --->
							updateXMLCPPosition('<cfoutput>#INPBATCHID#</cfoutput>', ui.item.attr('data-control-point-physical-id'), (ui.item.index() + 1)  );
						}

						<!--- Reset so we know what the start index was --->
						StartSortIndex = -1;

						//$(this).find('.SurveyQuestionBorder').css('border-style','none');
						ui.item.find('.CPDisplaySection').css('border-style','none');

				    },
				    change: function( event, ui ) {

					}

		});


			    		// .selectable({ filter: ".q_contents", cancel: ".handle,.nosel" });
	}

	function updateXMLCPPosition(BatchID, INPPID, INPNEWPOS)
	{
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/control-point.cfc?method=MoveQuestion&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			async: false,
			data:  {
					INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>",
					INPPID: INPPID,
					INPNEWPOS: INPNEWPOS
				   },
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},
			success:
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d)
				{
					if (d.RXRESULTCODE == 1)
					{
						reIndexQuestions();
					}
					else
					{

					}
				}

			});

	}

</script>



<!--- Question CP objects --->
<script type="application/javascript">

    $(function() {


		$('.QuestionContainer').each(function() {

		    // BindQuestionTypeChange($(this).find('.cp_type'));

		    $(this).find('.AnswerItem').each(function(){
               BindSetupAnswerOptions ($(this));
            });

		    $(this).find('.add-more-answer').on('click', function(event)
		    {
			    var content = $(this).parents('.control-point').find('.cp-content');
	            AddOptionToList('', '', '', '', '', '', content);
	        });

			//show/hide question timeout inteval
		    $(this).parent().find(".toggle-inteval-question").on("click",function(e)
		    {
			   $(this).find('i').toggleClass('fa-angle-up');
			   $(this).find('i').toggleClass('fa-angle-down');

			   $(this).parent().find('.div-toggle-inteval').slideToggle();

		    });

		    $(this).parent().find('.toggle-answer-intents').on('click', function(event)
	    		{
	    			$(this).parents('.QuestionContainer').find('.wrapper-answer-text').slideToggle();
				$(this).find('i').toggleClass('fa-angle-up');
  			     $(this).find('i').toggleClass('fa-angle-down');
	    		});



		    <!--- On various selection Changes --->
			$(this).parents('.control-point').find("#AF,#ITYPE,#IVALUE, #IMRNR, #INRMO").change(function(e) {

				<!--- Show CP Editor Save options if this changes --->
	            $(this).parents('.control-point').find('.CPEFooter').css("visibility", "visible");

	            <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
	            $(this).parents('.control-point').find('.btn-save-cp').prop("disabled",false);

			});


		});

    });


    // function BindQuestionTypeChange(inpObj)
	// {
	// 	<!--- Section Settings Items --->
	// 	inpObj.change(function(e) {
    //
	// 		e.preventDefault();
    //
    //         $(this).closest('.control-point').find('.see-unicode-mess').addClass('hidden');
    //
    //         var parents = $(this).parents('.control-point');
    //         var idquestion = parents.attr('id');
    //         var valueoldtemplate = $('#'+idquestion+' .control-point textarea:first-child').val();
    //
    //         var oldSelected = $(this).attr('data-cp-type');
    //         var selectValue = this.value;
    //         $(this).attr('data-cp-type',selectValue);
    //
    //         changeCPType($(this),selectValue);
    //
    //         //  displayQuestionNum();
    //
    //
	// 		<!--- Show CP Editor Save options if this changes --->
    //         $(this).parents('.QuestionContainer').parent().find('.CPEFooter').css("visibility", "visible");
    //
    //         <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
    //         $(this).parents('.QuestionContainer').parent().find('.btn-save-cp').prop("disabled",false);
    //
	// 	});
    //
	// }


    function changeCPType(obj,type)
    {
        var content = obj.parents('.control-point').find('.cp-content');
        var currentContentHere= $(obj).closest('.control-point').find("textarea").val();

        var selected_answer_template = $(obj).find('option:selected').data('anstemp');
        obj.parents('.control-point').find('#ANSTEMP').val(selected_answer_template);


        if(type == 'SHORTANSWER')
        {
            obj.parents('.control-point').find('#TYPE').val('SHORTANSWER');

            var answers_list = content.find('.answers_list');
            answers_list.html('');

			content.find('.AnswersRow').hide();
        }
        else if(type == 'ONESELECTION')
        {
            obj.parents('.control-point').find('#TYPE').val('ONESELECTION');

            var answers_list = content.find('.answers_list');
            answers_list.html('');

            content.find('.AnswersRow').show();

            $(obj).attr('data-anstemp',selected_answer_template);
            switch(selected_answer_template)
            {
                // Yes or No
                case 1:
                    AddOptionToList('Yes', 'Yes', '(?i)^y$|yes|si|ya|yup|yea|yeah|sure|ok|okay|all right|very well|of course|by all means|sure|certainly|absolutely|indeed|right|affirmative|agreed|roger|aye|uh-huh|okey|aye|positive|^1$|confirm|in|ye|yas', '', '', '', content);
                    AddOptionToList('No', 'No', '(?i)^n$|no|non|ne|nyet|no way|negative|^0$|out', '', '', '', content);
                break;

                // True or False
                case 2:
                    AddOptionToList('True', '1', '', '', '', '', content);
                    AddOptionToList('False', '0', '', '', '', '', content);
                break;

                // Agree or Disagree
                case 3:
                    AddOptionToList('Strong Disagree', '1', '', '', '', '', content);
                    AddOptionToList('Disagree', '2', '', '', '', '', content);
                    AddOptionToList('Neither Disagree or Agree', '3', '', '', '', '', content);
                    AddOptionToList('Agree', '4', '', '', '', '', content);
                    AddOptionToList('Strong Agree', '5', '', '', '', '', content);
                break;

                // Useful or Not
                case 4:
                    AddOptionToList('Not useful', '1', '', '', '', '', content);
                    AddOptionToList('Somewhat useful', '2', '', '', '', '', content);
                    AddOptionToList('Useful', '3', '', '', '', '', content);
                    AddOptionToList('Mostly useful', '4', '', '', '', '', content);
                    AddOptionToList('Very useful', '5', '', '', '', '', content);
                break;

                // Easy or Not
                case 5:
                    AddOptionToList('Not easy', '1', '', '', '', '', content);
                    AddOptionToList('Somewhat easy', '2', '', '', '', '', content);
                    AddOptionToList('Easy', '3', '', '', '', '', content);
                    AddOptionToList('Mostly easy', '4', '', '', '', '', content);
                    AddOptionToList('Extremely easy', '5', '', '', '', '', content);
                break;

                // Likely or Not
                case 6:
                    AddOptionToList('Extremely likely', '1', '', '', '', '', content);
                    AddOptionToList('Very likely', '2', '', '', '', '', content);
                    AddOptionToList('Moderately likely', '3', '', '', '', '', content);
                    AddOptionToList('Slightly likely', '4', '', '', '', '', content);
                    AddOptionToList('Not at all likely', '5', '', '', '', '', content);
                break;

                // Important or Not
                case 7:
                    AddOptionToList('Extremely important', '1', '', '', '', '', content);
                    AddOptionToList('Very important', '2', '', '', '', '', content);
                    AddOptionToList('Moderately important', '3', '', '', '', '', content);
                    AddOptionToList('Slightly important', '4', '', '', '', '', content);
                    AddOptionToList('Not at all important', '5', '', '', '', '', content);
                break;

                // Satisfied or Not
                case 8:
                    AddOptionToList('Extremely satisfied', '1', '', '', '', '', content);
                    AddOptionToList('Very satisfied', '2', '', '', '', '', content);
                    AddOptionToList('Moderately satisfied', '3', '', '', '', '', content);
                    AddOptionToList('Slightly satisfied', '4', '', '', '', '', content);
                    AddOptionToList('Not at all satisfied', '5', '', '', '', '', content);
                break;

                // Net Promoter Score
                case 9:
                    AddOptionToList('0 Not at all satisfied', '0', '', '', '', '', content);
                    AddOptionToList('1', '1', '', '', '', '', content);
                    AddOptionToList('2', '2', '', '', '', '', content);
                    AddOptionToList('3', '3', '', '', '', '', content);
                    AddOptionToList('4', '4', '', '', '', '', content);
                    AddOptionToList('5', '5', '', '', '', '', content);
                    AddOptionToList('6', '6', '', '', '', '', content);
                    AddOptionToList('7', '7', '', '', '', '', content);
                    AddOptionToList('8', '8', '', '', '', '', content);
                    AddOptionToList('9', '9', '', '', '', '', content);
                    AddOptionToList('10 Extremely satisfied', '10', '', '', '', '', content);
                break;
                // custom
                 case 0:
                    AddOptionToList('', '1', '', '', '', '', content);
                    AddOptionToList('', '2', '', '', '', '', content);
                    AddOptionToList('', '3', '', '', '', '', content);
                    AddOptionToList('', '4', '', '', '', '', content);
                    AddOptionToList('', '5', '', '', '', '', content);

                break;

                default:


                break;

            }
            //

			<!--- ***TODO What the heck is this?!? *** --->
            content.find('.ans-group').each(function(id,el){
                var id = $(el).attr('id');
                var ran = Math.floor((Math.random() * 100) + 1);
                id = id+'-'+ran;
                $(el).attr('id',id);
                $(el).next('.lbl-ans-group').attr('id',id);
                $(el).next('.lbl-ans-group').attr('for',id);
            });

        }

        content.find("textarea").val(currentContentHere);
        content.find(".span_text").html(currentContentHere);


    }

    function BindSetupAnswerOptions (inpObj)
    {
	    <!--- On Answer Change --->
		inpObj.find("#OPTION").change(function(e) {

			<!--- Show CP Editor Save options if this changes --->
            $(this).parents('.QuestionContainer').parent().find('.CPEFooter').css("visibility", "visible");

            <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
            $(this).parents('.QuestionContainer').parent().find('.btn-save-cp').prop("disabled",false);

		});

		inpObj.find("#AVALREGLINK").click(function()
		{
			<!--- Store a refence to this AVAL item so it can be updated by modal --->
			$('#AVALEditModal').data('inpAnswerItem', inpObj);

			$('#AVALEditModal #AVALREGEditAnswerText').val(inpObj.find("#OPTION").val());
			$('#AVALEditModal #AVALREGEditAnswerBucket').val(inpObj.find("#AVAL").val());
			$('#AVALEditModal #AVALREGEditREG').val(inpObj.find("#AVALREG").val());
			$('#AVALEditModal #AVALREGEditRESP').val(inpObj.find("#AVALRESP").val());

			<!--- Look for current value in select list - if not there add it and notate it as added - let user specify their own batch--->
			// Set the value, creating a new option if necessary
			if ($('#AVALREGEditREDIR').find("option[value='" + inpObj.find("#AVALREDIR").val() + "']").length)
			{
				$('#AVALREGEditREDIR').val(inpObj.find("#AVALREDIR").val()).trigger('change');
			}
			else
			{
				// Create a DOM Option and pre-select by default
				var newOption = new Option(inpObj.find("#AVALREDIR").val(), inpObj.find("#AVALREDIR").val(), true, true);
				// Append it to the select
				$('#AVALREGEditREDIR').append(newOption).trigger('change');
			}

			// $('#AVALEditModal #AVALREGEditREDIR').val(inpObj.find("#AVALREDIR").val()).change();

			<!--- run change so select2 boxes get set after any reloads of the question data --->
			$('#AVALEditModal #AVALREGEditNEXT').val(inpObj.find("#AVALNEXT").val()).change();

			$('#AVALEditModal').modal('show');

		});

		inpObj.find('.delete-intent').on('click', function(event) {

            <!--- Show CP Editor Save options if this changes --->
            $(this).parents('.QuestionContainer').parent().find('.CPEFooter').css("visibility", "visible");

            <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
            $(this).parents('.QuestionContainer').parent().find('.btn-save-cp').prop("disabled",false);

            $(this).parents('.row.row-small').remove();


        });

	   inpObj.find('.add-more-training').on('click', function(event)
	   {
		   var content = $(this).parents('.wrapper-training-text');
		   AddTrainingTextToList('', content);

		   <!--- Show CP Editor Save options if this changes --->
		   $(this).parents('.control-point').find('.CPEFooter').css("visibility", "visible");

		   <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
		   $(this).parents('.control-point').find('.btn-save-cp').prop("disabled",false);

	   });

		inpObj.find('.toggle-ai-opt').on('click', function(event)
		{
			$(this).parents('.AnswerItem').find('.wrapper-training-text').slideToggle();
			$(this).toggleClass('fa-angle-down');
			$(this).toggleClass('fa-angle-up');
		});

    }


</script>

<!--- AP CP objects --->
<script type="application/javascript">

    $(function()
    {

		$('.APIContainer').each(function() {

			<!--- On various selection Changes --->
			$(this).parents('.control-point').find("#API_RA, #API_TYPE, #API_DOM, #API_DIR, #API_PORT, #API_HEAD, #API_FORM, #API_ACT, #API_DATA").change(function(e) {

				<!--- Show CP Editor Save options if this changes --->
	            $(this).parents('.control-point').find('.CPEFooter').css("visibility", "visible");

	            <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
	            $(this).parents('.control-point').find('.btn-save-cp').prop("disabled",false);

			});

		});

	});


</script>


<!--- CDF CP objects --->
<script type="application/javascript">

    var CDFListBox = null;

    $(function()
    {

		$('.CDFContainer').each(function() {

			var inpCPE = $(this).parents('.control-point');

			<!--- On various selection Changes --->
			$(this).parents('.control-point').find("#SCDF").change(function(e) {

				<!--- Show CP Editor Save options if this changes --->
	            $(this).parents('.control-point').find('.CPEFooter').css("visibility", "visible");

	            <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
	            $(this).parents('.control-point').find('.btn-save-cp').prop("disabled",false);

			});


			inpCPE.find("#btn-add-cdf").click(function()
			{
				$('#btn-save-cdf').data('inpCPE', inpCPE);
				$('#inpCDFName').val('');
				$('#AddNewCDFModal').modal('show');
			});

			PopulateCDFSelectBox(inpCPE.find('#SCDF'), inpCPE.find('#CDF_SCDF').val());

		});


		<!--- Allow user to add a CDF (Custom Data Field) --->
		$('#AddNewCDFModal').find("#btn-save-cdf").click(function()
		{
			$("#btn-save-cdf").prop("disabled",true);

			var inpCPE = $('#btn-save-cdf').data('inpCPE');

			$.ajax({
				url: '/session/sire/models/cfc/control-point.cfc?method=AddCDF&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'post',
				dataType: 'json',
				data: {
						inpCDFName: $('#inpCDFName').val(),
						inpOptions : {},
						inpCheckOption : 0,
						inpDefaultValue : ''

				},
				beforeSend: function()
				{
					$("#btn-save-cdf").prop("disabled",true);

				},
				complete: function()
				{
					$("#btn-save-cdf").prop("disabled",false);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); $("#btn-save-cdf").prop("disabled",false);	},
				success: function(d) {

					if (d.RXRESULTCODE == 1)
					{
						<!--- Clear the current CDF box --->
						CDFListBox = null;

						<!--- Repopulate values with current list --->
						PopulateCDFSelectBox(inpCPE.find('#SCDF'), d.CDFNAME)
						$('#AddNewCDFModal').modal('hide');
					}
					else
					{
						bootbox.alert('Error: Add New CDF: <br/>' + d.MESSAGE);
					}

					$("#btn-save-cd").prop("disabled",false);
				}
			});
		});

	});


	<!--- Becuase these items can be dynamically added - set here each time they are added inpObj here is the SELECT box --->
	function PopulateCDFSelectBox(inpObj, inpCDF)
	{
		if(inpCDF == "")
			inpCDF = 0;

		if(CDFListBox != null)
		{
			<!--- Remove current Questions from Box --->
			inpObj.empty();

			<!--- Only hit DOM once to add all of the options --->
			inpObj.html(CDFListBox.join(''));

			<!--- Now set the selection here --->
			inpObj.val(inpCDF);
		}
		else
		{
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/control-point.cfc?method=GetCDFs&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType: 'json',
				async: false, <!--- Do this Syncronously so it only pulls once or when changed --->
				data:
				{

				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},
				success:
				<!--- Default return function for Async call back --->
				function(d)
				{
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (parseInt(d.RXRESULTCODE) === 1)
					{
						<!--- Remove current Questions from Box --->
						inpObj.empty();

						<!--- Store Questions for Select box here --->
						var output = [];

						<!--- Default 0 --->
						output.push('<option value="0">Select CDF To Use</option>');


						$.each($(d.CDFS.DATA.CDFID_INT), function(i, val)
						{
						  <!--- HTML Encode Text for Display - needs to be 'quote safe' --->
						  <!--- Use the Physical - non-changing ID for each reference --->
						  output.push('<option value="'+ d.CDFS.DATA.CDFNAME_VCH[i] +'">'+ d.CDFS.DATA.CDFNAME_VCH[i] +'</option>');

						});

						<!--- Store here to minimize trips to DB  --->
						CDFListBox = output;

						<!--- Only hit DOM once to add all of the options --->
						inpObj.html(output.join(''));

						<!--- Be sure to select stored value if there is one --->
						inpObj.val(inpCDF);

					}
					else
					{
						<!--- No result returned --->
						bootbox.alert("General Error processing your request.");
					}

				}

			});

		}

	}

</script>


<!--- Interval CP objects --->
<script type="application/javascript">

    $(function()
    {

		$('.IntervalContainer').each(function() {

			<!--- On various selection Changes --->
			$(this).parents('.control-point').find("#ITYPE, #IVALUE, #IHOUR, #IMIN, #INOON, #IENQID, #IMRNR, #INRMO").change(function(e) {

				<!--- Show CP Editor Save options if this changes --->
	            $(this).parents('.control-point').find('.CPEFooter').css("visibility", "visible");

	            <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
	            $(this).parents('.control-point').find('.btn-save-cp').prop("disabled",false);

				<!--- Reset these every time they change --->
				onChangeIntervalType($(this).parents('.control-point'));

			});

			onChangeIntervalType($(this));

		});

	});


	<!--- Only display Interval Options that make sense - hide the ones not used --->
	function onChangeIntervalType(inpCPE)
	{
		switch(inpCPE.find('#ITYPE').val())
		{

			case "SECONDS":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Seconds(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Hide the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').hide();

			break;

			case "MINUTES":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Minute(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Hide the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').hide();

			break;

			case "HOURS":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Hour(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Hide the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').hide();

			break;

			case "DAYS":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Day(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Show the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').show();

			break;

			case "WEEKS":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Week(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Show the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').show();

			break;

			case "MONTHS":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Month(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Show the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').show();

			break;

			case "TODAY":
				<!--- Hide the IVALUE Container	--->
				inpCPE.find('#IVALUE_Container').hide();
				<!--- Show the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').show();

			break;

			case "WEEKDAY":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Weekdays to Skip? ');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Show the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').show();

			break;

			case "DATE":
				<!--- Hide the IVALUE Container	--->
				inpCPE.find('#IVALUE_Container').hide();
				<!--- Show the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').show();

			break;

			case "SUN":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Sunday(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Show the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').show();

			break;

			case "MON":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Monday(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Show the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').show();

			break;

			case "TUE":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Tuesday(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Show the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').show();

			break;

			case "WED":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Wednesday(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Show the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').show();

			break;

			case "THU":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Thursday(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Show the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').show();

			break;

			case "FRI":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Friday(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Show the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').show();

			break;

			case "SAT":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Saturday(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Show the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').show();

			break;

		}
	}


</script>



<!--- OptIn CP objects --->
<script type="application/javascript">

    var GroupsListBox = null;

    $(function()
    {

		$('.OptInContainer').each(function() {

			var inpCPE = $(this).parents('.control-point');

			<!--- On various selection Changes --->
			$(this).parents('.control-point').find("#OIG").change(function(e) {

				<!--- Show CP Editor Save options if this changes --->
	            $(this).parents('.control-point').find('.CPEFooter').css("visibility", "visible");

	            <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
	            $(this).parents('.control-point').find('.btn-save-cp').prop("disabled",false);

			});

			inpCPE.find("#btn-add-group").click(function()
			{
				$('#btn-save-group').data('inpCPE', inpCPE);
				$('#subscriber_list_name').val('');
				$('#AddNewSubscriberList').modal('show');
			});

			PopulateSubscriberListSelectBox(inpCPE.find('#OIG'), inpCPE.find('#OPTIN_OIG').val());

		});

		<!--- Allow user to add a group (Subscriber List) --->
		$('#subscriber_list_form_add').validationEngine('attach', {
			promptPosition : "topLeft",
			autoPositionUpdate: true,
			showArrow: false,
			scroll: false,
			focusFirstField : false
		});

		$('#subscriber_list_form_add').on('submit', function(event)
		{
			event.preventDefault();

			var inpCPE = $('#btn-save-group').data('inpCPE');
			if($(this).validationEngine('validate')){
				$.ajax({
					url: '/session/sire/models/cfc/control-point.cfc?method=AddGroup&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					type: 'post',
					dataType: 'json',
					data: {INPGROUPDESC: $('#subscriber_list_name').val()},
					beforeSend: function()
					{
						$("#btn-save-group").prop("disabled",true);

					},
					complete: function()
					{
						$("#btn-save-group").prop("disabled",false);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); $("#btn-save-group").prop("disabled",false);	},
					success: function(d) {

						if (d.RXRESULTCODE == 1)
						{
							<!--- Make sure to add and select the new Subcriber list in the SELECT box --->
							GroupsListBox = null;
							PopulateSubscriberListSelectBox(inpCPE.find('#OIG'), d.INPGROUPID)
							$('#AddNewSubscriberList').modal('hide');

							<!--- trigger change method --->
							inpCPE.find('#OIG').change();
						}
						else
						{
							bootbox.alert('Error: Add New Subscriber List: <br/>' + d.MESSAGE);
						}

						$("#btn-save-group").prop("disabled",false);
					}
				});
			}

		});

	});


	<!--- Auto create subscriber list if none is selected --->
	function AutoCreateSubscriberList()
	{
		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/control-point.cfc?method=CreateAndLinkSubscriberList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			async: false,
			data:
			{
				INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>"
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },
			success:
			<!--- Default return function for call back --->
			function(d)
			{
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1)
				{
					<!--- !important - Force reload of list box on next display call --->
					GroupsListBox = null;

					<!--- Append new list to existing OIG - reload causes it to not be visible to CP_Save mehtod --->
					$('#WalkThroughStepEditor').find('#CPEBody').find('#OIG').append($('<option/>', {
				        value: d.NEWLISTID,
				        text : d.NEWName
				    }));

					<!--- Add the native list item --->
				    $('#WalkThroughStepEditor').find('#CPEBody').find('#OIG').append($('<option>', {value: d.NEWLISTID, text: d.NEWNAME}));

				    <!--- Add the data to the select2 object --->
				    $('#WalkThroughStepEditor').find('#CPEBody').find('#OIG').select2('data', {id: d.NEWLISTID, text: d.NEWNAME});

				    <!--- Select the new list by default --->
					$('#WalkThroughStepEditor').find('#CPEBody').find('#OIG').val(parseInt(d.NEWLISTID));

				}
				else
				{
					<!--- No result returned --->
					if(d.ERRMESSAGE != "")
						bootbox.alert(d.ERRMESSAGE);
				}
			}

		});

	}


	<!--- Becuase these items can be dynamically added - set here each time they are added inpObj here is the SELECT box --->
	function PopulateSubscriberListSelectBox(inpObj, inpOIG)
	{
		if(GroupsListBox != null)
		{
			<!--- Remove current Questions from Box --->
			inpObj.empty();

			<!--- Only hit DOM once to add all of the options --->
			inpObj.html(GroupsListBox.join(''));

			<!--- Now set the selection here --->
			inpObj.val(inpOIG);
		}
		else
		{

			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/control-point.cfc?method=GetGroups&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType: 'json',
				async: true,
				data:
				{

				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},
				success:
				<!--- Default return function for Async call back --->
				function(d)
				{
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1)
					{
						<!--- Remove current Questions from Box --->
						inpObj.empty();

						<!--- Store Questions for Select box here --->
						var output = [];

						<!--- Default 0 --->
						output.push('<option value="0">Select Subscriber List</option>');

						$.each($(d.GROUPS.DATA.GROUPID_BI), function(i, val)
						{
						  <!--- HTML Encode Text for Display - needs to be 'quote safe' --->
						  <!--- Use the Physical - non-changing ID for each reference --->
						  if(parseInt(inpOIG) == parseInt(d.GROUPS.DATA.GROUPID_BI[i]))
						  	output.push('<option value="'+ d.GROUPS.DATA.GROUPID_BI[i] +'" selected>'+ d.GROUPS.DATA.GROUPNAME_VCH[i] +'</option>');
						  else
						  	output.push('<option value="'+ d.GROUPS.DATA.GROUPID_BI[i] +'">'+ d.GROUPS.DATA.GROUPNAME_VCH[i] +'</option>');

						});

						<!--- Store here to minimize trips to DB  --->
						GroupsListBox = output;

						<!--- Only hit DOM once to add all of the options --->
						inpObj.html(output.join(''));

						<!--- Be sure to select stored value if there is one --->
						inpObj.val(parseInt(inpOIG));

					}
					else
					{
						<!--- No result returned --->
						bootbox.alert("General Error processing your request.");
					}

				}

			});

		}

	}

</script>

<!--- Keyword javascript --->
<script type="application/javascript">
    $(function() {

		<!--- Initialize current keyword --->
		if($("#Keyword").val() != undefined)
		{
			if($("#Keyword").val().length > 20)
			{
				$(".movi-body").find(".span_keyword").text($("#Keyword").val().substring(1,20) + '...');
			}
			else
			{
				$(".movi-body").find(".span_keyword").text($("#Keyword").val());
			}
		}

		// Validate keyword in real time - ajax with warnings
		$("#Keyword").on("keydown",function(){
			$('#KeywordStatus').removeClass('has-error');
			$('#KeywordStatus').addClass('not-has-error');
			$('#KeywordStatus').show();
			$("#KeywordStatus").text("Checking keyword availability…");
		});

		$("#Keyword").delayOn("input keyup blur", 800, function(element, event) {
			$('#KeywordStatus').removeClass('has-error');
			$('#KeywordStatus').removeClass('not-has-error');
			if(element.value.length > 20)
			{
				$(".movi-body").find(".span_keyword").text(element.value.substring(1,20) + '...');
			}
			else
			{
				$(".movi-body").find(".span_keyword").text(element.value);
			}
			$("#span-mlp-kw").text(element.value);

			if(element.value.length == 0)
			{
				<!--- Auto Save Keyword blank to erase --->
				SaveKeyword();
			}
			else
			{
				$('#KeywordStatus').html('<img src="/public/sire/images/loading.gif" class="ajax-loader-inline" style="width:2em; height:2em;"> ');

				/*<!--- Save Custom Responses - validation on server side - will return error message if not valid --->*/
				$.ajax({
					type: "POST", /*<!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->*/
					url: '/session/sire/models/cfc/control-point.cfc?method=ValidateKeyword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					dataType: 'json',
					async: false,
					data:
					{
						inpBatchId : inpBatchId,
						inpKeyword : element.value,
						inpShortCode : $("#ShortCode").val()

					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { /*<!--- No result returned --->*/
						alertBox("Error. No Response from the remote server. Check your connection and try again.");
					},
					success:
					/*<!--- Default return function for Async call back --->*/
					function(d)
					{

						/*<!--- RXRESULTCODE is 1 if everything is OK --->*/
						if (parseInt(d.RXRESULTCODE) == 1)
						{
							//$('#KeywordStatus').css('color', 'green');
							$('#KeywordStatus').addClass('not-has-error');
							$('#KeywordStatus').html(d.MESSAGE);

							<!--- Auto Save Keyword --->
							SaveKeyword();
						}
						else
						{
							/*<!--- No result returned --->	*/
							if(d.ERRMESSAGE != "")
							{
								//$('#KeywordStatus').css('color', 'red');
								$('#KeywordStatus').addClass('has-error');
								$('#KeywordStatus').html('<i class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>   '+d.ERRMESSAGE);


								setTimeout(function(){
									$("#Keyword").focus();
								}, 1000);
							}
						}
						$('#KeywordStatus').show();
					}
				});
			}
		});



    });


    <!--- Write keyword to DB --->
	function SaveKeyword()
	{
		if($("#Keyword").val().length == 0)
		{
			<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/control-point.cfc?method=ClearKeyword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType: 'json',
				async: true,
				data:
				{
					INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>",
					inpKeyword : $("#Keyword").val(),
					inpShortCode : $("#ShortCode").val(),
					inpTemplateFlag : "<cfoutput>#inpTemplateFlag#</cfoutput>"

				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },
				success:
				<!--- Default return function for call back --->
				function(d)
				{
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1)
					{
						$('#KeywordStatus').removeClass('has-error');
						$('#KeywordStatus').removeClass('not-has-error');
						$('#KeywordStatus').html("");
					}
					else
					{
						<!--- No result returned --->
						if(d.ERRMESSAGE != "")
							bootbox.alert(d.ERRMESSAGE);
					}
				}

			});

		}
		else
		{
			if($("#Keyword").val().length > 0 )
			{
				<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url: '/session/sire/models/cfc/control-point.cfc?method=SaveKeyword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					dataType: 'json',
					async: true,
					data:
					{
						INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>",
						inpKeyword : $("#Keyword").val(),
						inpShortCode : $("#ShortCode").val(),
						inpTemplateFlag : "<cfoutput>#inpTemplateFlag#</cfoutput>"

					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },
					success:
					<!--- Default return function for call back --->
					function(d)
					{
						<!--- RXRESULTCODE is 1 if everything is OK --->
						if (d.RXRESULTCODE == 1)
						{
							$('#KeywordStatus').removeClass('has-error');
							$('#KeywordStatus').removeClass('not-has-error');
							$('#KeywordStatus').addClass('not-has-error');
							$('#KeywordStatus').html("Reserved OK");
						}
						else
						{
							<!--- No result returned --->
							if(d.ERRMESSAGE != "")
								bootbox.alert(d.ERRMESSAGE);
						}
					}

				});
			}
		}
	}

</script>

<!--- API Sample --->
<script type="application/javascript">

    $(function()
    {
         $("#btn_build_URL").click(function() {

			var LocalHTTPSAPIURL = 'https://api.siremobile.com/ire/secure/triggerSMS?inpContactString=' + encodeURIComponent($("#inpContactString").val()) + '&inpBatchId=' + encodeURIComponent($("#APIBatchId").val()) + '&inpUserName=' + encodeURIComponent($("#APIUserName").val()) + '&inpPassword=' + encodeURIComponent($("#APIPassword").val()) ;

			$("#HTTPSAPIURL").html(LocalHTTPSAPIURL);

		});
	});

</script>


<!--- Branch Rules Engine Stuff  --->
<script type="application/javascript">

    var StartSortIndexConditions = -1;

    $(function()
    {
        <!--- Initialize Special Rules Engine Stuff Here--->
		<!--- Load the click event for each Rules condition list --->

		$.each($('.AddCondition'), function() {

		   $(this).click(function()
			{
				<!--- Search up for the BrachContainer then down for the ConditionsList container and then pass it into the function --->
				AddConditionToList($(this).parents('#BranchOptionsContainer').find('#ConditionsList'));

				<!--- Show CP Editor Save options if this changes --->
                $(this).parents('.control-point').find('.CPEFooter').css("visibility", "visible");
                <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
                $(this).parents('.control-point').find('.btn-save-cp').prop("disabled",false);

			});

		});


		$('#btn-save-bocdv').on('click', function()
		{

			<!--- Set the value to the one selected --->
			$(this).data('inpCond').find('#BOCDV').val($('#BOCDV_Picker').val());
			$(this).data('inpCond').find('#BOCDV').trigger( "change" );

			ReloadContition($(this).data('inpCond'));

			$('#RulesEngineBOCDVPicker').modal('hide');

		});

		<!--- Allow user to remove conditions --->
		$.each($('.BranchOptionsContainer'), function(index, item) {

		   var BranchOptionsContainer = $(this);

		   $(this).find('#ConditionsList').on('click', '.RemoveCondition', function()
			{
				<!--- Remove the XML - next save will not include this --->
				$(this).parents('.ConditionItem').remove();

				<!--- Conditions are applied in order shown - first Compare wins --->
				reIndexConditions(BranchOptionsContainer.find('#ConditionsList'));

				<!--- Show CP Editor Save options if this changes --->
                BranchOptionsContainer.parents('.control-point').find('.CPEFooter').css("visibility", "visible");

                <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
                BranchOptionsContainer.parents('.control-point').find('.btn-save-cp').prop("disabled",false);

			});

			BindBranchSelectionChange(BranchOptionsContainer.find('#BOFNQ'));

			<!--- Because these Select Boxes can be dynamically added - set click method here for each obj--->
			$.each(BranchOptionsContainer.find('.ConditionItem'), function (index, item) {
			   	BindSetupRulesEngineBOCDVPicker($(this));

			   	BindBranchSelectionChange($(this).find('#BOTNQ'));
				BindBranchSelectionChange($(this).find('#BOQ'));
				BindBranchSelectionChange($(this).find('#BOC'));
				BindBranchSelectionChange($(this).find('#BOCDV'));
				BindBranchSelectionChange($(this).find('#BOAV'));
			});

			<!--- Setup sortables for Conditions --->
			BindSortableCond(BranchOptionsContainer.find('#ConditionsList'));


		});

        <!--- QuestionSelectionMatchType --->
        $('#QuestionSelectionMatchType').change(function(e) {

			<!--- only update final value if anser is available --->
			if($('#QuestionSelection').val() > 0)
			{
				if(	$('#QuestionSelectionMatchType').val() == 1 )
			    {
				    $('#BOCDV_Picker').val("{%INPPA=" +  $('#QuestionSelection').val() + "%}");
				    $('#BOCDV_CDF_Picker').val('0').trigger('change');
				    $('#BOCDV_STDCDF_Picker').val('0').trigger('change');
			    }
			    else if ( $('#QuestionSelectionMatchType').val() == 2 )
			    {
				    $('#BOCDV_Picker').val("{%INPPAV=" +  $('#QuestionSelection').val() + "%}");
				    $('#BOCDV_CDF_Picker').val('0').trigger('change');
				    $('#BOCDV_STDCDF_Picker').val('0').trigger('change');
			    }
		    }

		});

		<!--- BOCDV_CDF_Picker --->
        $('#BOCDV_CDF_Picker').change(function(e) {

			<!--- only update final value if anser is available --->
			if($('#BOCDV_CDF_Picker').val() != "0")
			{
				$('#BOCDV_Picker').val("{%" +  $('#BOCDV_CDF_Picker').val() + "%}");

				$('#BOCDV_STDCDF_Picker').val('0').trigger('change');
				$('#QuestionSelection').val('0').trigger('change');
				$('#QuestionSelectionMatchType').val('0').trigger('change');

		    }

		});

		<!--- BOCDV_STDCDF_Picker --->
        $('#BOCDV_STDCDF_Picker').change(function(e) {

			<!--- only update final value if anser is available --->
			if($('#BOCDV_STDCDF_Picker').val() != "0")
			{
				$('#BOCDV_Picker').val("{%" +  $('#BOCDV_STDCDF_Picker').val() + "%}");

				$('#BOCDV_CDF_Picker').val('0').trigger('change');
				$('#QuestionSelection').val('0').trigger('change');
				$('#QuestionSelectionMatchType').val('0').trigger('change');
		    }

		});

	});

	<!--- Becuase these items can be dynamically added - set click method here each time they are added --->
	function BindSetupRulesEngineBOCDVPicker(inpCond)
	{
		<!--- Unbind any events already bound to this object - re-render will need to rebind objects --->
		inpCond.off();

		var QuestionSelectObjs = [];

		inpCond.find(".BOCDVBox").click(function()
		{
			$('#btn-save-bocdv').data('inpCond', inpCond);
			$('#BOCDV_Picker').val(inpCond.find('#BOCDV').val());

			<!--- Less trips to server and DB - requires send array of select boxes to populate --->
			QuestionSelectObjs = [];
			QuestionSelectObjs.push($('#RulesEngineBOCDVPicker').find("#QuestionSelection"));
			// inpCPE.find('#IENQID').attr("inpVal", inpCPObj.IENQID);
			PopulateQuestionSelectBox(QuestionSelectObjs);

			PopulateCDFSelectBox($('#BOCDV_CDF_Picker'), 0);

			$('#BOCDV_CDF_Picker').val('0').trigger('change');
			$('#BOCDV_STDCDF_Picker').val('0').trigger('change');
			$('#QuestionSelection').val('0').trigger('change');
			$('#QuestionSelectionMatchType').val('0').trigger('change');


			$('#RulesEngineBOCDVPicker').modal('show');
		});

	}

	function BindBranchSelectionChange(inpObj)
	{
		<!--- Do not unbind selec2 boxes or shit will break; Unbind any events already bound to this object - re-render will need to rebind objects --->
		// inpObj.off();

		<!--- Section Settings Items --->
		inpObj.change(function() {

			<!--- Show CP Editor Save options if this changes --->
            $(this).parents('.control-point').find('.CPEFooter').css("visibility", "visible");

            <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
            $(this).parents('.control-point').find('.btn-save-cp').prop("disabled",false);

		});

		<!--- If text area get on input also--->
		if(inpObj.is("textarea"))
		{
			inpObj.on('keyup paste input', function() {
			    <!--- Show CP Editor Save options if this changes --->
	            $(this).parents('.control-point').find('.CPEFooter').css("visibility", "visible");

	            <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
	            $(this).parents('.control-point').find('.btn-save-cp').prop("disabled",false);
            });
		}


	}

	<!--- Need to bind sortable to new section objects --->
	function BindSortableCond(inpObj, inpHelperText)
	{

		if(typeof inpHelperText == 'undefined')
			inpHelperText = '';

		if(typeof inpObj.attr("data-helper-text") !== 'undefined')
			inpHelperText = inpObj.attr("data-helper-text");


		inpObj.sortable({

				containment: "#ConditionsList",
				items: '.ConditionItem',
				handle: ".HandleCond",
				    start: function(e, ui ){
					     ui.placeholder.height(ui.helper.outerHeight());
					},
					start: function(event, ui)
					{

						<!--- Track where sort object started from--->
						StartSortIndexConditions = ui.item.index();

						ui.item.addClass('DrageConditionItem');

			 		},
					stop: function(event, ui)
					{

						<!--- Only update if position actually changes--->
						if(StartSortIndexConditions != ui.item.index())
						{
							reIndexConditions(ui.item.parents('#BranchOptionsContainer'));

							<!--- Show CP Editor Save options if this changes --->
				            ui.item.parents('.control-point').find('.CPEFooter').css("visibility", "visible");

				            <!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
				            ui.item.parents('.control-point').find('.btn-save-cp').prop("disabled",false);

						}

						<!--- Reset so we know what the start index was --->
						StartSortIndexConditions = -1;

						//$(this).find('.SurveyQuestionBorder').css('border-style','none');
						ui.item.removeClass('DrageConditionItem');

				    },
				    change: function( event, ui ) {

					}

		});
	}

	function AddConditionToList(inpCondContainer)
	{
		<!--- Get the next condition CID - max of current list + 1 --->
		var NextCID = inpCondContainer.find('.ConditionItem').length + 1;


		<!--- SAMPLE XML we are trying to populate
			<COND BOAV='' BOC='=' BOCDV='undefined' BOQ='2' BOTNQ='0' BOV='1' CID='1' TYPE='RESPONSE'></COND>
			<COND BOAV='Test' BOC='=' BOCDV='{%%INPPA2}' BOQ='undefined' BOTNQ='4' BOV='' CID='2' TYPE='CDF'></COND>
		 --->


		<!--- Default blank item --->
		var inpConditionObj = {

			'CID':  NextCID,
			'TYPE' : 'CDF',
			'BOQ' : '0',
			'BOC' : '=',
			'BOV' : '',
			'BOAV' : '',
			'BOCDV' : '',
			'BOTNQ' : '0',
			'DESC' : 'DESC'
		}

//session.sire.models.cfc.control-point.rulesengine

		$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/control-point/rulesengine.cfc?method=GetAdvEditUIConditionRemote&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType: 'json',
				async: false,
				data:
				{
					CONDITIONITEM: JSON.stringify(inpConditionObj),
					inpBatchId : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>",
					inpCPList: JSON.stringify(inpCPListjsVar),
					CIIndex : NextCID
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},
				success:
				<!--- Default return function for Async call back --->
				function(d)
				{
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1)
					{

						var el = $(d.COND_HTML);

						<!---  RE-intitialize any newly added SELECT2 boxes --->
						el.find(".Select2").select2( { theme: "bootstrap"} );

						<!--- Allow user to remove conditions --->
						el.on('click', '.RemoveCondition', function()
						{
							<!--- Remove the XML - next save will not include this --->
							el.remove();

							<!--- Conditions are applied in order shown - first Compare wins --->
							reIndexConditions(inpCondContainer);

							<!--- Show CP Editor Save options if this changes --->
							$(this).parents('.control-point').find('.CPEFooter').css("visibility", "visible");

							<!--- Renable button -> This helps to prevent slow connections from letting user click save button multiple times --->
							$(this).parents('.control-point').find('.btn-save-cp').prop("disabled",false);

						});

						BindBranchSelectionChange(el.find('#BOTNQ'));
						BindBranchSelectionChange(el.find('#BOQ'));
						BindBranchSelectionChange(el.find('#BOC'));
						BindBranchSelectionChange(el.find('#BOCDV'));
						BindBranchSelectionChange(el.find('#BOAV'));

						BindSetupRulesEngineBOCDVPicker(el);

						inpCondContainer.append(el);

					}
					else
					{
						<!--- No result returned --->
						bootbox.alert("General Error processing your request.");
					}

				}

		});

	}

	function ReloadContition(inpCond)
	{

		var ConditionItem = {
			'CID':  inpCond.find('#CID').val(),
			'TYPE' : (inpCond.find('#TYPE').val() == null) ? "CDF" : inpCond.find('#TYPE').val(),
			'BOQ' : (inpCond.find('#BOQ').val() == null) ? "0" : inpCond.find('#BOQ').val(),
			'BOC' : (inpCond.find('#BOC').val() == null) ? "=" : inpCond.find('#BOC').val(),
			'BOV' : (inpCond.find('#BOV').val() == null) ? "" : inpCond.find('#BOV').val(),
			'BOAV' : (inpCond.find('#BOAV').val() == null) ? "" : inpCond.find('#BOAV').val(),
			'BOCDV' : (inpCond.find('#BOCDV').val() == null) ? "" : inpCond.find('#BOCDV').val(),
			'BOTNQ' : (inpCond.find('#BOTNQ').val() == null) ? "0" : inpCond.find('#BOTNQ').val(),
			'DESC' : 'DESC'
		}

		$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/control-point/rulesengine.cfc?method=GetAdvEditUIConditionRemote&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType: 'json',
				async: false,
				data:
				{
					CONDITIONITEM: JSON.stringify(ConditionItem),
					inpBatchId : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>",
					inpCPList: JSON.stringify(inpCPListjsVar),
					CIIndex : inpCond.find('#CID').val()
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},
				success:
				<!--- Default return function for Async call back --->
				function(d)
				{
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1)
					{

						var el = $(d.COND_HTML);

						<!---  RE-intitialize any newly added SELECT2 boxes --->
						el.find(".Select2").select2( { theme: "bootstrap"} );


						<!--- Allow user to remove conditions --->
						el.on('click', '.RemoveCondition', function()
						{
							<!--- Remove the XML - next save will not include this --->
							$(this).parents('.ConditionItem').remove();

							<!--- Conditions are applied in order shown - first Compare wins --->
							reIndexConditions(inpCondContainer.parents('#BranchOptionsContainer'));
						});

						BindSetupRulesEngineBOCDVPicker(el);

						inpCond.replaceWith(el);

					}
					else
					{
						<!--- No result returned --->
						bootbox.alert("General Error processing your request.");
					}

				}

		});

	}

	function ForceReloadQuestionLists()
	{
		<!--- Anytime a question list is re-indexed--->

		<!--- Reset Question SELECTs to re lookup next time they are needed --->
		QuestionListBox = null;
		QuestionsOnlyListBox= null;

		<!--- Less trips to server and DB - requires send array of select boxes to populate --->
		var	QuestionSelectObjs = [];


		$.each($('.StepQuestionList'), function (index, item) {

		   	<!--- Less trips to server and DB - requires send array of select boxes to populate --->
			QuestionSelectObjs.push($(this));

		});

		PopulateQuestionSelectBox(QuestionSelectObjs);

	}

</script>

<!--- Fixed menu for CP objects --->
<script type="application/javascript">

	<!---
		Careful! This is making UI more complex... need to handle intialization changes as well as activation changes
	--->

    $(function() {

	    <!-- hide flow editor functionality - Flag to enabel or disable flow editor options-->
		<cfif hfe EQ 1>
			return;
		</cfif>

    });
</script>

<!--- Final page load actions - do this stuff last so less F.O.U.C. problems--->
<script type="application/javascript">

	var BatchListBox = null;

	<!--- Becuase these buttons can be dynamically added - set click method here each time they are added --->
	<!--- BOQ should only load real questions with answers - everthing else should load all actions/CPs --->
	function PopulateBatchSelectBox(inpObjs)
	{
		if(BatchListBox != null)
		{
			$.each(inpObjs , function(i, val)
			{

				var PrevSelectVal = inpObjs[i].val();

				<!--- Remove current Questions from Box --->
				inpObjs[i].empty();

				<!--- Only hit DOM once to add all of the options --->
				inpObjs[i].html(BatchListBox.join(''));

				if(PrevSelectVal == "" || PrevSelectVal == 'undefined' || PrevSelectVal == undefined)
					inpObjs[i].val(0);
				else
					inpObjs[i].val(PrevSelectVal);

				inpObjs[i].find('option[value="0"]').text("Enter or Select a Campaign Id");


			});
		}
		else
		{
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/control-point.cfc?method=GetBatchFilteredList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType: 'json',
				async: false,
				data:
				{

				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},
				success:
				<!--- Default return function for Async call back --->
				function(d)
				{
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1)
					{
						<!--- update list send to methods --->
						inpCPListjsVar = d;

						<!--- Store Questions for Select box here --->
						var output = [];

						output.push('<option value="'+ 0 +'">'+ 'Enter or Select a Campaign Id' +'</option>');

						$.each(d.BATCHARRAY.DATA.BATCHID_BI , function(i, val)
						{
							<!--- HTML Encode Text for Display - needs to be 'quote safe' --->
							<!--- Use the Physical - non-changing ID for each reference --->
							output.push('<option value="'+ d.BATCHARRAY.DATA.BATCHID_BI[i] +'">'+ '' + d.BATCHARRAY.DATA.BATCHID_BI[i] + ' ' + d.BATCHARRAY.DATA.DESC_VCH[i] +'</option>');

						});

						<!--- Store here to minimize trips to DB  --->
						BatchListBox = output;

						$.each(inpObjs , function(i, val)
						{
							var PrevSelectVal = inpObjs[i].val();

							<!--- Remove current Questions from Box --->
							inpObjs[i].empty();

							<!--- Only hit DOM once to add all of the options --->
							inpObjs[i].html(output.join(''));

							if(PrevSelectVal == "" || PrevSelectVal == 'undefined' || PrevSelectVal == undefined)
								inpObjs[i].val(0);
							else
								inpObjs[i].val(PrevSelectVal);

							<!--- AI in the UI - Make sure selected value still existis --->
							if(inpObjs[i].val() != PrevSelectVal)
							{
								inpObjs[i].val(0);
							}

							inpObjs[i].find('option[value="0"]').text("Enter or Select a Campaign Id");

						});

					}
					else
					{
						<!--- No result returned --->
						bootbox.alert("General Error processing your request.");
					}

				}

			});
		}
	}
</script>
<!--- Final page load actions - do this stuff last so less F.O.U.C. problems--->
<script type="application/javascript">

    $(function()
    {

 		$( "#CampaignTriggerMenu" ).css("visibility", "visible");
<!---
	    <!--- Open keyword section with default visible transition --->
	    if($("#Keyword").val().length > 0)
	    {
		    <!--- Sections are 0 based index - open first "keyword" section --->
		    $("#CampaignTriggerMenu").accordion('option', 'active' , 0);
	    }
--->
		<!--- Less trips to server and DB - requires send array of select boxes to populate --->
		var	QuestionSelectObjs = [];

		$.each($('#AVALREGEditNEXT'), function (index, item) {

			<!--- Less trips to server and DB - requires send array of select boxes to populate --->
			QuestionSelectObjs.push($(this));

		});

		PopulateQuestionSelectBox(QuestionSelectObjs);

		<!--- Less trips to server and DB - requires send array of select boxes to populate --->
		var	BatchSelectObjs = [];

		$.each($('#AVALREGEditREDIR'), function (index, item) {

			<!--- Less trips to server and DB - requires send array of select boxes to populate --->
			BatchSelectObjs.push($(this));

		});

		PopulateBatchSelectBox(BatchSelectObjs);

	    $('.info-popover').popover({
		     html:true,
			placement: function(tip, ele) {
				var width = $(window).width();

				var iconPos  = $(ele).offset(),
				toLeft  = iconPos.left,
				toRight = $(window).width() - ($(ele).width() + toLeft);
				if(width < 500)
				{
					return 'top';
				}
				return toRight > 500 ? 'right' : 'bottom' ;
			}
		});

		<!--- provide for a default drop zone if there are no objects yet --->
		if($('.control-point').length  > 0)
		{
			$('.default-drop-zone').hide();
		}
		else
		{
			$('.default-drop-zone').show();
		}

		<!--- hack so select2 will work in bootstrap modals https://github.com/select2/select2/issues/600#issuecomment-102857595 AND https://github.com/select2/select2/issues/1436#issuecomment-21028474 --->
//		$.fn.modal.Constructor.prototype.enforceFocus = function() {};
//		$.fn.modal.Constructor.prototype.enforceFocus = $.noop;

		<cfif HFE EQ 1>
		   	reIndexQuestions();
		</cfif>

		$("#Nav2Redir").click(function()
		{
			<!--- Read in Batchid from the target --->
			var targetBatch = $(this).parent().find('#AVALREGEditREDIR').val();

			if(targetBatch > 0)
			{

				<!--- Verify Batch existis and is active --->
				<!--- Look for new Batch Option --->
				<!--- Allow to navigate back - Switch to Parent tab? --->
				var TargetWindow = 	window.open('', 'CampaignEdit-' + targetBatch);

				<!--- Only load URL once incase work is in progress --->
				if(TargetWindow.location.href.search('campaignid=' + targetBatch) == -1 )
					window.open('/session/sire/pages/campaign-edit?campaignid=' + targetBatch + '&afe=<cfoutput>#afe#</cfoutput>&src=<cfoutput>#campaignData.BatchId_bi#</cfoutput>', 'CampaignEdit-' + targetBatch);
			}
		});

		window.name = 'CampaignEdit-' + '<cfoutput>#campaignData.BatchId_bi#</cfoutput>';

		if(window.opener !== null && '<cfoutput>#src#</cfoutput>' !=  '<cfoutput>#campaignData.BatchId_bi#</cfoutput>' && '<cfoutput>#src#</cfoutput>' != '0')
		{
			$('#ParentLink').html('<i class="fas fa-arrow-left"></i> Parent' );

			$('#ParentLink').click(function()
			{
				var TargetWindow = 	window.open('', 'CampaignEdit-' + <cfoutput>#src#</cfoutput>);
				console.log('window.opener.focus()')

			});

			<!--- if this an add new template --->
			<!--- window.opener.$("#serverMsg") Add an option if it does not exist yet and then select it --->
		}

		$("#AddNewNav2Redir").click(function()
		{
			window.open('/session/sire/pages/campaign-edit?templateid=116&adv=1&customCampaign=1&afe=<cfoutput>#afe#</cfoutput>&src=<cfoutput>#campaignData.BatchId_bi#</cfoutput>', '');
		});

		<!--- Copy and paste events --->
		$(document).bind({
			copy : function(e){
				console.log('Copy Event');
			},
			paste : function(e){

				try
				{
					var ControlPointData = JSON.parse(e.originalEvent.clipboardData.getData('text'));

					if(parseInt(ControlPointData.RQ) > 0 )
					{
						console.log("CP Paste detected");

						bootbox.confirm('CP Paste Detected - Are you sure you want to paste to flow?', function(r)
						{
							<!--- Only complete if user comfirms it --->
							if (r == true)
							{
								<!---Add to beginning of flow --->
								ControlPointData.RQ = 0;
								ControlPointData.TGUIDE = "";

								<!--- Read the CP from the component --->
								$.ajax({
									type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
									url: '/session/sire/models/cfc/control-point.cfc?method=Add_ControlPoint&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
									dataType: 'json',
									async: false,
									data:
									{
										INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>",
										controlPoint : JSON.stringify(ControlPointData),
										inpSimpleViewFlag : "<cfoutput>#inpSimpleViewFlag#</cfoutput>",
										inpTemplateFlag : "<cfoutput>#inpTemplateFlag#</cfoutput>",
										inpHFE : "<cfoutput>#hfe#</cfoutput>",
										inpAFE : "<cfoutput>#afe#</cfoutput>"
									},
									error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},
									success:
									<!--- Default return function for Async call back --->
									function(d)
									{
										<!--- RXRESULTCODE is 1 if everything is OK --->
										if (d.RXRESULTCODE == 1)
										{
											ControlPointData.TGUIDE = "";

											RenderCPToPage(d.HTML, ControlPointData.RQ, ControlPointData.TYPE, $('.cpobj-drop-zone').first(), 'BEFORE');
										}
										else
										{
											var el = $('');

											<!--- No result returned --->
											bootbox.alert("General Error processing your request. ");
										}

									}

								});

							}

							<!--- Return true to close bootbox when complete --->
							return true;
						});

					}

				}
				catch(err)
				{
					console.log(err);

				}

			},
			cut : function(){
				console.log('Cut Event');
			}
		});

		<!--- Done loading everthing - hide any overlays --->
		$('.LoadingCPs').hide();

	});

</script>



<!---



--->
