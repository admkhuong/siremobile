
<cfparam name="cid" default=""> <!--- Batch Id for the where the expert system CP is part of --->
<cfparam name="cpid" default=""> <!--- The Contorl Point the Expert System is located at --->

<cfparam name="variables._title" default="Campaign - Expert System Editor">



<!--- Notes and Sources --->
<!--- 
	
	https://en.wikipedia.org/wiki/Expert_system
	
	
	http://listjs.com/
	http://listjs.com/examples/data-attributes-custom-attributes/
	
	https://googlechrome.github.io/samples/css-custom-properties/
	
	
	
	http://buildregex.com/	
	https://github.com/VerbalExpressions/JSVerbalExpressions
	
	
--->



<!--- Tasks
	
	
	tool to import AIML - at least the input/ouput portions
	tool to export AIML - at least the input/output portions
	
	Logical filtering on MOs to add -
		no keywords
		length greater than 5?10? characters
		number of words more than 3
	
	
	<!--- CP flow - where does this fit? branch logic oiptions on each response --->
			
	
--->






<!--- Make sure user really owns this cid? --->


<cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPDataById" returnvariable="RetVarReadCPDataById">
	<cfinvokeargument name="inpBatchId" value="#cid#">
	<cfinvokeargument name="inpQID" value="#cpid#">
	<cfinvokeargument name="inpTemplateFlag" value="0">
</cfinvoke>


<!--- <cfdump var="#RetVarReadCPDataById#" /> --->
								

<!---
OR link to a remote site
<link rel="shortcut icon" type="image/x-icon" href="https://www.bbc.co.uk/favicon.ico?_=3.18.29" />
--->


<!--- Possible Todo: Minfy js output on load - so data send to browser is smaller/lighter/faster - Test: Will server side delays offset client side delays of extra data--->
<!--- <cfinclude template="../assets/pages/styles/genric-responsive.cfm" />  --->

<style>
	
	:root 
	{
		/*************************** Page-wide variables ****************************/
		
		  /* Base spacing unit. */
		  --header-height: 100px;
		  --footer-height: 70px;
		  
		  --full-tile-height: calc(90vh - (var(--header-height) + var(--footer-height)) );
		  --half-tile-height: calc( (var(--full-tile-height) / 2 ) - .5em );
		  
	}


	
	.TestBorder
	{
		 border: dashed 1px rgba(177,186,204,.5);
		 min-height: 35vh;
	}
	
	.dashboard-item
	{
		padding: 1em;		
	}
	
	.ResponseListSection
	{		
		background: #fff;
		border-radius: 8px;
	}
	
	#ESOptionContainer
	{	
		
		padding-top: 1em;	
		position: relative;	
	}
	
	.TileItem
	{
		background: #fff;
		position: relative;
		padding: 1em;
	}
	
	.FullTileHeight
	{
		max-height: var(--full-tile-height);
		min-height: var(--full-tile-height);		
	}
	
	.HalfTileHeight
	{
		max-height: var(--half-tile-height);
		min-height: var(--half-tile-height);	
		overflow-y: auto;		
	}
		
	#ItemsContainer
	{
		overflow-y: auto;
		max-height: calc( var(--full-tile-height) - (68px + 1em) );
		min-height: calc( var(--full-tile-height) - (68px + 1em) );						
	}	
	
	#ESOptionItems
	{		
		list-style: none;	
		padding: 0 .5em;	
	}
		
	.ESItem  
	{
		border: dashed 1px rgba(177,186,204,.5);
		margin-top: 1em;	
		border-radius: 8px;
		padding: 0 .5em;	
		position: relative;
	}
	

	.annotated-list { box-sizing: border-box; }
	.annotated-list .list { padding-top: 20px; }
	.annotated-list .list > div { padding: 10px 0; border-top: solid 1px rgba(255, 255, 255, 0.1); }
	.annotated-list .avatar { max-width: 150px; }
	.annotated-list .avatar img { max-width: 100%; }
	.annotated-list h3 { font-size: 16px; font-size: 1.6rem; margin: 0 0 3px; font-weight: bold; }
	.annotated-list p { margin: 0; }
	.annotated-list input { border-radius: 25px; padding: 7px 14px; background-color: transparent; border: solid 1px rgba(0, 0, 0, 0.2); width: 200px; box-sizing: border-box; color: #2e2e2e; margin-bottom: 5px; }
	.annotated-list input:focus { outline: none; border-color: #aaa; }
	.annotated-list .sort { font-size: 12px; font-size: 1.2rem; padding: 5px 15px; border-radius: 25px; border: none; display: inline-block; color: #2e2e2e; text-decoration: none; background-color: rgba(0, 0, 0, 0.05); margin: 2px 0; }
	.annotated-list .sort:hover { text-decoration: none; background-color: rgba(0, 0, 0, 0.1); }
	.annotated-list .sort:focus { outline: none; }
	.annotated-list .sort:after { width: 0; height: 0; border-left: 4px solid transparent; border-right: 4px solid transparent; border-bottom: 4px solid transparent; content: ""; position: relative; top: -10px; right: -4px; }
	.annotated-list .sort.asc:after { width: 0; height: 0; border-left: 4px solid transparent; border-right: 4px solid transparent; border-top: 4px solid #2e2e2e; content: ""; position: relative; top: 11px; right: -4px; }
	.annotated-list .sort.desc:after { width: 0; height: 0; border-left: 4px solid transparent; border-right: 4px solid transparent; border-bottom: 4px solid #2e2e2e; content: ""; position: relative; top: -9px; right: -4px; }


	/** 1. Correct font family not being inherited in all browsers. 2. Correct font size not being inherited in all browsers. 3. Address margins set differently in Firefox 4+, Safari 5, and Chrome. */
	.annotated-list button, .annotated-list input, .annotated-list select, .annotated-list textarea { font-family: inherit; /* 1 */ font-size: 100%; /* 2 */ margin: 0; /* 3 */ }
	
	/** Address Firefox 4+ setting `line-height` on `input` using `!important` in the UA stylesheet. */
	.annotated-list button, .annotated-list input { line-height: normal; }
	
	/** Address inconsistent `text-transform` inheritance for `button` and `select`. All other form control elements do not inherit `text-transform` values. Correct `button` style inheritance in Chrome, Safari 5+, and IE 8+. Correct `select` style inheritance in Firefox 4+ and Opera. */
	.annotated-list button, .annotated-list select { text-transform: none; }
	
	/** 1. Avoid the WebKit bug in Android 4.0.* where (2) destroys native `audio` and `video` controls. 2. Correct inability to style clickable `input` types in iOS. 3. Improve usability and consistency of cursor style between image-type `input` and others. */
	.annotated-list button, .annotated-list html input[type="button"], .annotated-list input[type="reset"], .annotated-list input[type="submit"] { -webkit-appearance: button; /* 2 */ cursor: pointer; /* 3 */ }
	
	/** Re-set default cursor for disabled elements. */
	.annotated-list button[disabled], .annotated-list html input[disabled] { cursor: default; }
	
	
	.Preview-Me {
	    margin: 5px 45px 5px 20px;
	    background-color: #66ccff;
	    color: #ffffff;
	    text-shadow: 1px 1px #AAAAAA;
	}

	.Preview-Bubble {
	    background-color: #28CBEF;
	    border-radius: 5px;
	    box-shadow: 0 0 6px #B2B2B2;
	    display: inline-block;
	    padding: 10px 18px;
	    position: relative;
	    vertical-align: top;
	    font-family: Helvetica, sans-serif;
	    font-size: 15px;
	    text-align: left;
	}

	.Preview-Me::before {
	    box-shadow: -2px 2px 2px 0 rgba( 178, 178, 178, .4 );
	    left: -7px;
	    background-color: #66ccff;
	}
	
	.Preview-Bubble::before {
	    background-color: #28CBEF;
	    content: "\00a0";
	    display: block;
	    height: 12px;
	    position: absolute;
	    top: 10px;
	    transform: rotate( 29deg ) skew( -35deg );
	    -moz-transform: rotate( 29deg ) skew( -35deg );
	    -ms-transform: rotate( 29deg ) skew( -35deg );
	    -o-transform: rotate( 29deg ) skew( -35deg );
	    -webkit-transform: rotate( 29deg ) skew( -35deg );
	    width: 16px;
	}
	
	.es-btn
    {
 	    display: inline-block; 
	    vertical-align: middle;
	    padding: 5px 12px;
	    border-radius: 15em;
	    border: 1px solid;
<!--- 	    margin: 2px; --->
	    min-width: 60px;
	    font-size: .875rem;
	    text-align: center;
	    outline: 0;
	    line-height: 1;
	    cursor: pointer;	
	    color: #FFF;
	    background-color: #74c37f;
	    border-color: #74c37f;
	    text-transform: uppercase;
	    font-weight: 700;    
    }	
    
    .es-btn:hover
    {
	    background-color: #83dd8f;
	}	
	
	div.MLPEditMenu
	{
		left: 0;
	    right:0;
		top: 0; 
		margin-top: -0.75em;  
	    position: absolute;
	    display: none;  
		text-align: center;  		
		border: 5px none rgba(0, 0, 0, 0);
	}
	
	.ESItem:hover {
       outline:none;
       box-shadow: 0 0 3pt 2pt rgba(96, 109, 188, 0.68);
       
	}

	.MLPEditMenu .mlp-icon, .MLPMasterSectionEdit .mlp-icon {
	   /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#a7cfdf+0,23538a+100;Blue+3d+%238 */
		background: #a7cfdf; /* Old browsers */
		background: -moz-linear-gradient(top, #a7cfdf 0%, #23538a 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top, #a7cfdf 0%,#23538a 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom, #a7cfdf 0%,#23538a 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a7cfdf', endColorstr='#23538a',GradientType=0 ); /* IE6-9 */	    border-radius: 50%;
	    color: #fff;
	    font-size: 1.125rem;
	    height: 27px;
	    margin: 3px 3px;
	    padding: 3px 3px;
	    transition: background-color .2s ease,border .2s ease;
	    width: 27px;
	    line-height: 18px;
	}
	
	.MLPEditMenu .mlp-icon:hover, .MLPMasterSectionEdit .mlp-icon:hover {
	    /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#c5deea+0,8abbd7+31,066dab+100;Web+2.0+Blue+3D+%231 */
		background: #c5deea; /* Old browsers */
		background: -moz-linear-gradient(top, #c5deea 0%, #8abbd7 31%, #066dab 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top, #c5deea 0%,#8abbd7 31%,#066dab 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom, #c5deea 0%,#8abbd7 31%,#066dab 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c5deea', endColorstr='#066dab',GradientType=0 ); /* IE6-9 */
		-webkit-box-shadow: -1px 2px 5px 0px rgba(50, 50, 50, 0.4);
		-moz-box-shadow:    -1px 2px 5px 0px rgba(50, 50, 50, 0.4);
		box-shadow:         -1px 2px 5px 0px rgba(50, 50, 50, 0.4);
		cursor: pointer;
	}
	
	.MLPEditMenu .mlp-icon-alert {
	    /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#ff3019+0,cf0404+100;Red+3D */
		background: #ff3019; /* Old browsers */
		background: -moz-linear-gradient(top, #ff3019 0%, #cf0404 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top, #ff3019 0%,#cf0404 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom, #ff3019 0%,#cf0404 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff3019', endColorstr='#cf0404',GradientType=0 ); /* IE6-9 */
	    border-radius: 50%;
	    color: #fff;
	    font-size: 1.125rem;
	    height: 27px;
	    margin: 3px 3px;
	    padding: 3px 3px;
	    transition: background-color .2s ease,border .2s ease;
	    width: 27px;
        line-height: 18px;
	}

	.MLPEditMenu .mlp-icon-alert:hover {
	   /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#ff1a00+0,ff1a00+100;Red+Flat+%231 */
		background: #ff1a00; /* Old browsers */
		background: -moz-linear-gradient(top, #ff1a00 0%, #ff1a00 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top, #ff1a00 0%,#ff1a00 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom, #ff1a00 0%,#ff1a00 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff1a00', endColorstr='#ff1a00',GradientType=0 ); /* IE6-9 */
	    -webkit-box-shadow: -1px 2px 5px 0px rgba(50, 50, 50, 0.4);
		-moz-box-shadow:    -1px 2px 5px 0px rgba(50, 50, 50, 0.4);
		box-shadow:         -1px 2px 5px 0px rgba(50, 50, 50, 0.4);
	    
	}
	
	
	
	<!--- Verbal Expressions Builder --->

	#ESREG {
	    border-left: 1px solid #ddd;
	    padding: 8px 15px;
	}
	
	.match-options {
	    display: inline-block!important;
	
	}
	
	#vx-list li {
       margin: .5em;
	}

 	#vx-list
	{		
		list-style: none;	
		padding: 0 .5em;	
	}
	
	
	li [class*=" fa-"], li [class*=" glyphicon-"], li [class*=" icon-"], li [class^=fa-], li [class^=glyphicon-], li [class^=icon-] {
	    display: inline-block;
	    text-align: center;
	    width: auto;
	}
	
	.btn-default {
	    color: #fff;
	    background-color: #474949;
	    border-color: #474949;
	}

</style>	


<div class="portlet">
	
	
	<div class="row">
	
		<!--- List of input and response --->
		<div class="col-xs-12 col-sm-4 dashboard-item">			
			
			<div id="ResponseListSectionContainer" class="ResponseListSection TestBorder">
			
				<div id="ESOptionContainer" class="annotated-list TileItem FullTileHeight">
					<!--- xstyle="position: absolute; background: #fff; top: .5em; left:.5em;" --->
					
					<div style="padding-left: .5em; height: 34px;">
						<!-- class="search" automagically makes an input a search field. -->
						<!--- <input class="fuzzy-search" placeholder="Search" /> --->
						<!--- <input class="search" placeholder="Search" /> --->
						<input class="search" placeholder="Search" />
						
						<!-- class="sort" automagically makes an element a sort buttons. The date-sort value decides what to sort by. -->
						<button class="sort" data-sort="ESTEXT">Sort by response</button>
					</div>
					
					<div id="ItemsContainer" class="">
						<!-- Child elements of container with class="list" becomes list items -->
						<ul id="ESOptionItems" class="list">		
							<cfoutput>	
								<cfloop array ="#RetVarReadCPDataById.CPOBJ.ESOPTIONS#" index="item">
									<li class="ESItem" data-ESID="#item.ESID#">							
										
										<h5 class="ESREG">#item.ESREG#</h5>
										<h5 class="ESTEXT">#item.ESTEXT#</h5>
																			
										<div class="MLPEditMenu">
			
											<span style="min-width:120px; font-size: 1em;" contenteditable="false"> 
												<i class="fa fa-gear mlp-icon mlp-icon-settings es-settings"></i> 												
												<i class="fa fa-remove mlp-icon-alert es-delete" data-tooltip="Are you sure?" style=""></i>												
											</span>
										
										</div>
																															
									</li>
				            	</cfloop>  
							</cfoutput>
						</ul>
					</div>

					<div style="padding-left: .5em; height: 34px;">
						
						<button type="button" id="AddItem" class="es-btn" style="margin-right: 1em;">Add</button>
						
					</div>
					
				</div>						                   
			
			</div>
			
		</div>	
	
		<!--- response builder --->
		<div class="col-xs-12 col-sm-8 dashboard-item">
			
			
			<div id="SingleItemEditor" class="TestBorder TileItem HalfTileHeight">
							 				 				 	
			 	<div class="col-sm-12 mb15">
									
					<div class="row">
						
						<button id="new-condition" class="btn btn-primary xes-btn col-sm-2" style="white-space: normal;">
		                    New Condition <i class="fa fa-plus"></i>
		                </button>
		                
		                <div class="col-sm-10">
		                    <div class="" id="ESREG">
		                        <strong>Expression: </strong><span>/(?:red)/g</span>
		                    </div>
		                </div>
					</div>	
	                
					<ul id="vx-list">
						
						
					</ul>	
										
			 	</div>

					<div id="ESTextSection" class="row">
								 																		
					<div class="col-xs-12"><lable for="ESTEXT" class="bc-title mbTight">Response</lable></div>
					<div class="col-xs-12 mb15"><div class="Preview-Me Preview-Bubble" style="width: 90%; height: auto;"><textarea class="form-control" id="ESTEXT" maxlength="1000" rows="6" style="border-radius: 5px;"></textarea></div></div>
											
					<div class="col-xs-12 mb15" id="TDESCPLAIN"></div> 
					
				</div>	
				
			</div>
			
			<div id="SingleItemEditor" class="TestBorder TileItem HalfTileHeight" style="margin-top: 1em;">
				
				<h3>Learning and Training Tools</h3>
				
			</div>
				
		</div>
		
	</div>
	
	
	<div class="row">
	
		<!--- Learning-training tools - Report of common questions --->
		<!--- Last x number of MOs that where not the keyword --->
		<div class="col-xs-12 col-sm-4 TestBorder dashboard-item">
			yo 3
		</div>	
	
		<!--- online testing - input-response --->
		<div class="col-xs-12 col-sm-8 TestBorder dashboard-item">
			yo 4
		</div>
		
	</div>
	
		
</div>


<cfinclude template="../views/layouts/master.cfm"> 

<!---
<link rel="stylesheet" href="/session/sire/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/session/sire/js/vendors/darktooltip/darktooltip.css">
--->


<!--- https://select2.github.io/  --->
<!---
<link rel="stylesheet" href="/public/sire/css/select2-bootstrap.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> 
--->

<!---
<!--- http://touchpunch.furf.com/ - Make jquery ui work on tough devices - hack for now but works until better option comes along --->
<script type="text/javascript" src="/public/sire/js/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="/session/sire/js/vendors/darktooltip/jquery.darktooltip.js"></script>
--->

<link rel="stylesheet" href="/session/sire/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/session/sire/js/vendors/darktooltip/darktooltip.css">

<link rel="stylesheet" href="/public/sire/css/select2.min.css">
<link rel="stylesheet" href="/public/sire/css/select2-bootstrap.min.css">

<!--- http://listjs.com/ --->
<script src="https://cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script> 
<script type="text/javascript" src="/session/sire/js/vendors/darktooltip/jquery.darktooltip.js"></script>

<!--- https://select2.github.io/  --->
<script type="text/javascript" src="/public/sire/js/select2.min.js"></script>

<script type="text/javascript">

	<!--- Expose list globally so other functions can access it --->
	var ESOptionContainerList = null;
	
	$(function() {
		
		var options = {
		  valueNames: [ 'ESTEXT', 'ESREG', { data: ['ESID'] } ]
		};
		
		ESOptionContainerList = new List('ESOptionContainer', options);

		ReadCPInline();
		
		$('#AddItem').click(function()
		{	
			AddNewESOptionToList();
		});	
		
		$('#new-condition').click(function()
		{	
			AddVXListItem();
		});	
		 
	});
	
	
	<!--- Load on update --->
	function ReadCPInline()
	{		
		<!--- Read Current Data From XMLControlString --->				
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/control-point.cfc?method=ReadCPDataById&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: false,
			data:  
			{ 
				INPBATCHID : "<cfoutput>#cid#</cfoutput>", 
				inpQID : "<cfoutput>#cpid#</cfoutput>"
								
							
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("*Error. No Response from the remote server. Check your connection and try again.");},					  
			success:		
			<!--- Default return function for Async call back --->
			function(d) 
			{																						
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1) 
				{								
					RenderExpertSystemCP(d.CPOBJ);
				}
				else
				{
					<!--- No result returned --->		
					bootbox.alert("General Error processing your request. ");
				}			
								
			} 		
				
		});	
			
	}
	
	
	function RenderExpertSystemCP(inpCPObj)
	{
		
		<!--- erase any data that is there --->
		$('#ESOptionItems').empty();
		
		<!--- http://listjs.com/api/#clear - make sure to clear the in memory list --->
		ESOptionContainerList.clear();
				
		<!--- Store the rest of the CP obj here for later retrieval --->
		$('#ResponseListSectionContainer').data('data-attr-cpobj', inpCPObj);
			
		<!--- Read options from inpCPObj --->
		var arrayLength = inpCPObj.ESOPTIONS.length;
		for (var i = 0; i < arrayLength; i++) 
		{			
	
			ESOptionContainerList.add({ ESTEXT: inpCPObj.ESOPTIONS[i].ESTEXT, ESREG: inpCPObj.ESOPTIONS[i].ESREG, ESID: inpCPObj.ESOPTIONS[i].ESID });
		}
		
		BindResponseSectionOption();				
				
		<!--- http://listjs.com/api/#reindex - make sure new items are visible to list tools --->
	//	ESOptionContainerList.update();
		
	}
	
	<!--- Render / Add each item to the list from Template --->
	function AddNewESOptionToList()
	{			

		
		<!--- Default to 1 if none found --->
		var maximum = 0;

		$('#ESOptionItems').find('.ESItem').each(function() {
		  var value = parseInt($(this).attr('data-ESID'));
		  maximum = (value > maximum) ? value : maximum;
		});
		
		<!--- new value is one more than max --->
		maximum++;
		
		ESOptionContainerList.add({ ESTEXT: "New Response", ESREG: "Placeholder", ESID: maximum });
		
		BindResponseSectionOption();
					
	}

	function BindResponseSectionOption()
	{
		
		<!--- list add removes any bound click events - readd to everything in list --->
		$(".es-settings").click(function()
		{					
			$('#SingleItemEditor').attr('data-ESID', $(this).closest('.ESItem').attr('data-ESID') );
			$('#SingleItemEditor #ESREG').html($(this).closest('.ESItem').find('.ESREG').html() );
			$('#SingleItemEditor #ESTEXT').val($(this).closest('.ESItem').find('.ESTEXT').html() );				
			
		});	
				
		<!--- Add hover menu options delete menu --->
		$(".ESItem").on('mouseout', function(e) {
				
			<!--- Dont hide current selection / hover menu if tool tip is open --->
			if(!$(".dark-tooltip:visible").length)
			{	
			 	$(this).find('.MLPEditMenu').hide();
		 	}
		});
		
		$(".ESItem").on('mouseover', function(e) {		
						  			  	
		  	$(this).children('.MLPEditMenu').show();
		  			  		  	
		  	// <!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();
		  		  	
		});		
		
		$(".es-delete").off();
		
		$(".es-delete").click(function()
		{					
			<!--- http://listjs.com/api/#remove - --->
			ESOptionContainerList.remove('ESID', $(this).closest('.ESItem').attr('data-ESID'));
			
			RemoveESItem();
		});			
		
	}
	
	
	function RemoveESItem()
	{
		var ESOptions = [];
		
		<!--- Loop over specifed Expert System OPTIONS --->
		var OptionCounter = 1;
		
		var arrayLength = ESOptionContainerList.items.length;
		
		for (var i = 0; i < arrayLength; i++) {
		    			
			var ESOptionItem = { 
				
				'ESREG': ($(ESOptionContainerList.items[i]).find('#ESREG').val() == null) ? "" : $(ESOptionContainerList.items[i]).find('#ESREG').val(),
				'ESTEXT': ($(ESOptionContainerList.items[i]).find('#ESTEXT').val() == null) ? "" : $(ESOptionContainerList.items[i]).find('#ESTEXT').val(),
				'ESID' : OptionCounter				
			}
			
			ESOptions.push(ESOptionItem);
			
			OptionCounter++;
					
		};
		
	}
	
	
	function AddVXListItem(inpExp, inpText)
	{
		<!--- Default to 1 if none found --->
		var maximum = 0;

		$('#vx-list').find('.VXItem').each(function() {
		  var value = parseInt($(this).attr('data-VXID'));
		  maximum = (value > maximum) ? value : maximum;
		});
		
		<!--- new value is one more than max --->
		maximum++;
		
		
		var template = $($("#vx-template").html()); 

		template.attr('data-VXID', maximum);
		template.find('.VXTEXT').val(inpText);
		
		<!--- Set up all the select2 boxes --->
		template.find('.VXEXP').select2( { theme: "bootstrap"} );
			
		$('#vx-list').append(template);
		
	}
		
</script>	

<!--- Store an easy to read/modify HTML of item as a  text/template --->
<script type="text/template" id="item-template">
   <li class="ESItem" data-ESID="0">
		<h5 class="ESREG">0</h5>
		<h5 class="ESTEXT">0</h5>										
	</li>
</script>

<!--- Store an easy to read/modify HTML of item as a text/template --->
<script type="text/template" id="vx-template">
   <li class="VXItem row" data-VXID="0">
		
		<div class="col-sm-4 col-md-2">		
			<select class="VXEXP select2 match-options form-control"><option value="add">Add</option><option value="any">Any</option><option value="anyOf">Any Of</option><option value="anything">Anything</option><option value="anythingBut">Anything But</option><option value="endOfLine">End of Line</option><option value="find">Find</option><option value="lineBreak">Line Break</option><option value="maybe">Maybe</option><option value="or">Or</option><option value="range">Range</option><option value="something">Something</option><option value="somethingBut">Something But</option><option value="startOfLine">Start of Line</option><option value="tab">Tab</option><option value="then">Then</option><option value="word">Word</option></select>
		</div>
		
		<div class="col-sm-6 col-md-8">
            <input type="text" class="form-control match-param VXTEXT" placeholder="Match Text">
        </div>				
        
        <div class="col-xs-1 col-sm-1" style="text-align: right; padding: 0 0;">
	        <button class="remove-match-option btn btn-danger" style="xwidth: 100%; xfloat: right;">
	        	<i class="fa fa-close"></i>
	        </button>
        </div>
        
        <div class="col-xs-1 col-sm-1" style="text-align: left; padding: 0 .8em;">
	        <button class="move-match-option btn btn-default" style="xwidth: 100%; xfloat: left;">
	            <i class="fa fa-arrows-v"></i>
	        </button>
        </div>
            						
	</li>
</script>


