<cfscript>
	createObject("component", "public.sire.models.helpers.layout")
		.addJs("/public/js/jquery.tmpl.min.js", true)
		.addJs("/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js")
		.addCss("/public/js/jquery-ui-1.11.4.custom/jquery-ui.css", true)
		.addCss("/session/sire/assets/global/plugins/uniform/css/uniform.default.css", true)
        .addCss("/session/sire/js/vendors/spectrum/spectrum.css", true)
        .addCss("/session/sire/js/vendors/popline/themes/popclip.css", true)
        .addCss("/session/sire/js/vendors/dropzone/dropzone.css", true)
		.addJs('/session/sire/assets/global/scripts/filter-bar-generator.js', true)
		.addJs('/session/sire/assets/global/plugins/uniform/jquery.uniform.min.js', true)
		// .addJs("/session/sire/js/ckeditor/ckeditor.js", true)
        .addJs("/session/sire/js/vendors/spectrum/spectrum.js", true)
        .addJs("/session/sire/js/vendors/popline/scripts/jquery.popline.js", true)

        <!--- https://github.com/kenshin54/popline --->
        .addJs("/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.link.js", true)
        .addJs("/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.decoration.js", true)
        .addJs("/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.list.js", true)
        .addJs("/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.justify.js", true)
        .addJs("/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.blockformat.js", true)
        .addJs("/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.social.js", true)
        .addJs("/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.textcolor-spectrum.js", true)
        .addJs("/session/sire/js/vendors/dropzone/min/dropzone.min.js", true)
		.addJs("/session/sire/assets/pages/scripts/campaign-manage.js");		
</cfscript>
    <cfinvoke component="session.sire.models.cfc.mlp" method="GetCampaignMLPTemplate" returnvariable="TemplatResult">
    </cfinvoke>

	<cfinvoke component="session.sire.models.cfc.mlp" method="ReadMLPTemplate" returnvariable="RetCPPXData">
		<cfinvokeargument name="ccpxDataId" value="#TemplatResult.TEMPLATEID#">
	</cfinvoke>

<style type="text/css">
    table .checked {
        color: green!important;
    }

    table td:last-child, table th:last-child {
    	text-align: center;
    }

	.getlink-mlp, .edit-mlp {
		font-size: 22px;
	}

    /*.popline{
        width: 320px;
    }*/

    .campaign-mlp{
        min-width: 273px !important;
    }


    @media screen and (max-width: 375px){
            .campaign-mlp{
            width: 273px !important;
        }
    }

    @media screen and (min-width: 376px) and (max-width: 480px){
        .campaign-mlp{
            width: 312px !important;
        }
    }

    .popline li{
        padding-right: 12px;
    }

    .popline .popline-color2-button{
        padding-right: 0px;
    }

    .popline .popline-blockFormat-button{
        padding-right: 0px;
    }

    .popline-textfield{
        color: black;
    }

    #campaign-mlp-logo{
        cursor: pointer;
    }

    .mlp-btn {
        display: inline-block;
        vertical-align: middle;
        padding: 10px 24px;
        border-radius: 15em;
        border: 1px solid;
        margin: 2px;
        min-width: 60px;
        font-size: .875rem;
        text-align: center;
        outline: 0;
        line-height: 1;
        cursor: pointer;
        color: #FFF;
        background-color: #74c37f;
        border-color: #74c37f;
        text-transform: uppercase;
        font-weight: 700;
    }

    .dz-success-mark{
        display: none;
    }

    .dz-error-mark{
        display: none;
    }
	<cfoutput>#RetCPPXData.CUSTOMCSS#</cfoutput>
</style>
	
<div class="portlet light bordered subscriber">
	<!--- Filter bar --->
	<div class="row">
		<div class="col-sm-3 col-md-4">
			<h2 class="page-title">Campaigns</h2>
		</div>
		<div class="col-sm-9 col-md-8">
			<div id="box-filter" class="form-gd">

			</div>
		</div>
	</div>
	<!--- Listing --->
	<div class="row">
		<div class="col-md-12">
			<div class="compaign">
				<div class="re-table">
					<div class="<!--- table-scrollable --->table-responsive">
						<table id="tblCampaignList" class="<!--- table-bordered ---> table-responsive <!--- table-hover ---> table-striped dataTables_wrapper">
							<!--- <tfoot>
								<tr>
									
								</tr>
							</tfoot> --->
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="row hidden">
    	<div class="col-xs-12 text-right">
    		<!--- <div class="btn-re-dowload"> <i class="fa fa-download" aria-hidden="true"></i> </div> --->
    		<div class="btn-re-remove delete-selected-item"> <i class="fa fa-trash" aria-hidden="true"></i> </div>
    	</div>
    </div>
</div>


<div class="bootbox modal fade" id="loadQueueProcess" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Percents of Load Process</h4>
            </div>
            <div class="modal-body">

                <div class="progress">
                    <div class="progress-bar progress-bar-striped active" id="progressouter" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" >
                    </div>
                </div>

                <p id="timecounter" class="text-primary"></p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-back-custom" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

 <!--- Campaign Template MLP Modal --->
    <div class="modal fade" id="campaign-mlp-modal" role="dialog">
	<!---<cfoutput>#GetMLPResult.CPMLPID#</cfoutput>--->
		<input type="hidden" name="" id="cpMLPId" value="">
		<input type="hidden" name="" id="campaign-mlp-custom-css" value="<cfoutput>#RetCPPXData.CUSTOMCSS#</cfoutput>">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">Customize Your Marketing Landing Page</strong></h4>
                </div>
                <div class="modal-body campaign-mlp-modal-body">
                    <!---<cfoutput>#RAWCONTENTTOSHOW#</cfoutput>--->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success-custom" id="save-campaign-mlp">Save</button>
                    <!--- <input type="hidden" name="userID" value=""> --->
                </div>
            </div>
        </div>
    </div>

<!--- Image Chooser --->
<div class="bootbox modal fade" id="ImageChooserModal" tabindex="-1" role="dialog" aria-labelledby="ImageChooserModal" aria-hidden="true" style="">
    
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Choose Image</h4>
            </div>
            <div class="modal-body">
                    
                <div class="tabbable">
                    <ul class="nav nav-tabs" id="myTabs">    
                        <li class="active"><a href="#MLPUploadImage"  class="active" data-toggle="tab">Upload New Image</a></li>
                        <li><a href="/session/sire/models/cfm/imagebrowser-mlp1.cfm" data-toggle="MLPImagePicker" data-target="#MLPImagePicker">Your Image Library</a></li>  
                        <li><a href="#MLPImageLink"  class="active" data-toggle="tab3">External Link</a></li>                  
                    </ul>
                 
                    <div class="tab-content">
                        
                        <div class="tab-pane active" id="MLPUploadImage" style="min-height: 10em;">     
                            
                            <div class="row>">                   
                                <div class="MLPDropZone" style="xmin-height:2em; text-align:center; line-height:6em; border:dashed 1px #ccc; padding-bottom: 3em;" contenteditable="false">Drop files here or click to upload.
                                    
                                    
                                    
                                </div>      
                            </div>
                            
                            <cfinvoke method="GetUserStorageInfo" component="session.sire.models.cfc.image" returnvariable="retValUserUsage">
                            </cfinvoke>
    
                            <cfif retValUserUsage.RXRESULTCODE GT 0>
                
                                <cfset usagePercent = DecimalFormat((retValUserUsage.USERUSAGE/retValUserUsage.USERSTORAGEPLAN)*100) />
                
                                <cfset UsageMBSize = retValUserUsage.USERUSAGE / 1048576>
                                <cfset StoragePlanMBSize = retValUserUsage.USERSTORAGEPLAN / 1048576>
                
                                <cfif UsageMBSize GT 1024>
                                    <cfset UsageMBSize = DecimalFormat(UsageMBSize / 1024)>
                                    <cfset UsageMBSize = UsageMBSize&"GB">
                                <cfelse>
                                    <cfset UsageMBSize = DecimalFormat(UsageMBSize)&"MB">
                                </cfif>
                
                                <cfif StoragePlanMBSize GT 1024>
                                    <cfset StoragePlanMBSize = DecimalFormat(StoragePlanMBSize / 1024)>
                                    <cfset StoragePlanMBSize = StoragePlanMBSize&"GB">
                                <cfelse>
                                    <cfset StoragePlanMBSize = DecimalFormat(StoragePlanMBSize)&"MB">
                                </cfif>
                
                                <cfoutput>
                
                                    <input type="hidden" id="inpUserUsage" value="#retValUserUsage.USERUSAGE#">
                                    <input type="hidden" id="inpUserStoragePlan" value="#retValUserUsage.USERSTORAGEPLAN#">
                
                                    <div class="row" style="padding-top:20px">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="">
                                                Account Storage Usage
                                            </div>  
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                <b><span id="userUsageSpan">#UsageMBSize#</span> / <span id="userStoragePlanSpan">#StoragePlanMBSize#</span></b>
                                            </div>
                                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-striped progress-bar-success" role="progressbar" id="usageProgress" 
                                                        aria-valuemin="0" aria-valuemax="100" style="width:#usagePercent#%">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                
                                </cfoutput>
                            </cfif>
                    
                        </div>
                        
                        <div class="tab-pane" id="MLPImagePicker"></div>
                        
                        <div class="tab-pane" id="MLPImageLink"  style="min-height: 10em;">
                            
                            <div class="MLPDropZone" style="min-height:2em; text-align:left; line-height:1em;" contenteditable="false">Image Link (URL)</div>
                            <input type="text" id="MLPImageURL" value="" style="width: 100%; height: 1.5em;" />                         
                            
                        </div>
                       
                    </div>
                </div>
                                    
            </div>
            
            <div class="modal-footer">
                <button type="button" class="mlp-btn" id="btn-upload-image">Use This Image</button>
                <button type="button" class="mlp-btn" id="btn-use-image-link">Use Specified Link</button>
                &nbsp; &nbsp; &nbsp;
                <button type="button" class="mlp-btn" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
   
</div>
    
<cfparam name="variables._title" default="Manage Campaigns - Sire">
<cfinclude template="../views/layouts/master.cfm">

<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#campaign-mlp-logo')
                    .attr('src', e.target.result);
                console.log('test');
            };

            reader.readAsDataURL(input.files[0]);
            
        }
    }
</script>