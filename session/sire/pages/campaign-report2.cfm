<cfparam name="url.campaignid" default="0">
<cfparam name="url.id" default="#url.campaignid#">
<cfif !isNumeric(url.id) OR url.id LT 1>
	<cflocation url="/session/sire/pages/campaign-manage" addtoken="false">
</cfif>

<cfinvoke method="GetCampaign" component="session.sire.models.cfc.campaign" returnvariable="campaignDetail">
    <cfinvokeargument name="campaignId" value="#url.id#">
    <cfinvokeargument name="userId" value="#Session.UserId#">
</cfinvoke>

<cfif campaignDetail.RXRESULTCODE NEQ 1 OR trim(campaignDetail.BatchObject.Desc_vch) EQ ''>
	<cflocation url="/session/sire/pages/campaign-manage" addtoken="false">
</cfif>

<cfinclude template="../../../public/paths.cfm">


<!--- remove old method: 
<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>								
--->
<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode">
	<cfinvokeargument name="inpBatchId" value="#url.campaignid#">
</cfinvoke>

<cfinvoke component="session.sire.models.cfc.campaign" method="getKeywordQueryByBatchId" returnvariable="keywordQuery">
	<cfinvokeargument name="inpBatchId" value="#url.id#"/>
</cfinvoke>

<cfquery name="totalSubscribers" datasource="#Session.DBSourceREAD#">
	SELECT COUNT( DISTINCT CL.ContactId_bi ) AS total, OP.OptIn_dt, DATE_FORMAT( OP.OptIn_dt, '%m%Y' ) AS month
	FROM simplelists.contactlist CL
	INNER JOIN simplelists.contactstring AS CS
		ON CL.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
			AND CS.OptInSourceBatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.id#"> 
			AND CS.ContactType_int = 3 
			AND CL.ContactId_bi = CS.ContactId_bi
	INNER JOIN simplelists.optinout OP
		ON OP.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.id#">
			AND OP.OptIn_dt IS NOT NULL
			AND LENGTH(CS.ContactString_vch) < 14
			AND CS.ContactString_vch = OP.ContactString_vch	
	GROUP BY month
	ORDER BY OptIn_dt
</cfquery>

<cfquery name="totalUnsubscribers" datasource="#Session.DBSourceREAD#">
	SELECT COUNT( DISTINCT CL.ContactId_bi ) AS total, OP.OptOut_dt, DATE_FORMAT( OP.OptOut_dt, '%m%Y' ) AS month
	FROM simplelists.contactlist CL
	INNER JOIN simplelists.contactstring AS CS
		ON CL.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
			AND CS.OptInSourceBatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.id#"> 
			AND CS.ContactType_int = 3 
			AND CL.ContactId_bi = CS.ContactId_bi
	INNER JOIN simplelists.optinout OP
		ON OP.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.id#">
			AND OP.OptOut_dt IS NOT NULL
			AND LENGTH(CS.ContactString_vch) < 14
			AND CS.ContactString_vch = OP.ContactString_vch
	GROUP BY month
	ORDER BY OptOut_dt
</cfquery>

<!---<cfquery name="totalDeliveries" datasource="#Session.DBSourceREAD#">
	SELECT COUNT(CR.MasterRXCallDetailId_int) total, CR.Created_dt, 
		DATE_FORMAT( CR.Created_dt, '%m%Y' ) AS month, IR.InitialSendResult_int AS Delivery
	FROM simplexresults.contactresults AS CR
		INNER JOIN simplexresults.ireresults AS IR ON CR.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.id#">
			AND CR.MasterRXCallDetailId_int = IR.MasterRXCallDetailId_bi
	GROUP BY month, Delivery
</cfquery>--->

<cfset cm_as = 0>
<cfset cm_asr = 0>
<cfset firstMonth1 = now()>
<cfset lastMonth1 = 0>
<cfset subscribersTotal = {}>
<!---<cfset monthNumber = DateDiff('d', campaignDetail.BatchObject.created_dt, now()) / (365/12)>--->
<cfset monthNumber = (dateFormat(now(), 'yyyy') - dateFormat(campaignDetail.BatchObject.created_dt, 'yyyy')) * 12
					 + (dateFormat(now(), 'm') - dateFormat(campaignDetail.BatchObject.created_dt, 'm') + 1)>
<cfif totalSubscribers.RecordCount GT 0>
	<cfloop query="totalSubscribers">
		<cfset cm_as += totalSubscribers.total>
		<cfif totalSubscribers.OptIn_dt LT firstMonth1>
			<cfset firstMonth1 = totalSubscribers.OptIn_dt>
		</cfif>
		<cfif totalSubscribers.OptIn_dt GT lastMonth1>
			<cfset lastMonth1 = totalSubscribers.OptIn_dt>
		</cfif>
		<cfset subscribersTotal['X' & dateFormat(totalSubscribers.OptIn_dt, 'myyyy')] = totalSubscribers.total>
	</cfloop>
	<cfset cm_asr = cm_as / monthNumber>
</cfif>

<cfset cm_aur = 0>
<cfset firstMonth2 = now()>
<cfset lastMonth2 = 0>
<cfset unsubscribersTotal = {}>
<cfif totalUnsubscribers.RecordCount GT 0>
	<cfloop query="totalUnsubscribers">
		<cfset cm_aur += totalUnsubscribers.total>
		<cfif totalUnsubscribers.OptOut_dt LT firstMonth2>
			<cfset firstMonth2 = totalUnsubscribers.OptOut_dt>
		</cfif>
		<cfif totalUnsubscribers.OptOut_dt GT lastMonth2>
			<cfset lastMonth2 = totalUnsubscribers.OptOut_dt>
		</cfif>
		<cfset unsubscribersTotal['X' & dateFormat(totalUnsubscribers.OptOut_dt, 'myyyy')] = totalUnsubscribers.total>
	</cfloop>
	<cfset cm_aur = cm_aur / monthNumber>
</cfif>

<!---<cfset cm_ms = 0>
<cfset cm_adr = 0>
<cfset lastMonth3 = 0>
<cfset deliveriesTotal = {}>
<cfif totalDeliveries.RecordCount GT 0>
	<cfloop query="totalDeliveries">
		<cfset cm_ms += totalDeliveries.total>
		<cfif totalDeliveries.Delivery EQ 1>
			<cfset cm_adr += totalDeliveries.total>
		</cfif>
		<cfif totalDeliveries.Created_dt GT lastMonth2>
			<cfset lastMonth3 = totalDeliveries.Created_dt>
		</cfif>
		<cfset deliveriesTotal['X' & dateFormat(totalDeliveries.Created_dt, 'myyyy')] = totalDeliveries.total>
	</cfloop>
	<cfset cm_adr = cm_adr * 100 / cm_ms>
</cfif>--->

<cfquery name="totalSMS" datasource="#Session.DBSourceREAD#">
	SELECT IREType_int, COUNT(*) AS TOTALCOUNT
	FROM simplexresults.ireresults
	WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.id#">
		AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
		AND IREType_int IN (1,2)
		AND LENGTH(ContactString_vch) < 14
	GROUP BY IREType_int
</cfquery>

<cfset messageNumber = [0,0]>
<cfloop query="totalSMS">
	<cfset messageNumber[IREType_int] = TOTALCOUNT>
</cfloop>

<cfoutput>
<main class="container-fluid page">
    <cfinclude template="../views/commons/credits_available.cfm">
    <section class="row bg-white">
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-6 content-title">
                    <p class="title">#htmleditformat(campaignDetail.BatchObject.Desc_vch)#</p>
                    	<p class="sub-title">
	                    <cfif keywordQuery.RecordCount EQ 1 AND keywordQuery.EMSFlag_int EQ 0>
                    		Keyword: <span class="active">#keywordQuery.Keyword_vch#</span> 
                    		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
	                    </cfif> 
                    		Short code: <span class="active">#shortCode.SHORTCODE#</span>
                		</p>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6 content-menu">
				    <a class="active" href="##"><span class="icon-report"></span><span>Report</span></a>
				    <a href="/session/sire/pages/reports-dashboard?inpBatchIdList=#url.campaignid#"><span class="icon-report4"></span><span>Advanced</span></a>
				    <a href="/session/sire/pages/campaign-edit?campaignid=#url.id#"><span class="icon-edit"></span><span>Edit</span></a>
                </div>
            </div>
        </div>
        <hr>
        <div class="container-fluid content-body">
        	<div class="row">
        	<div class="col-lg-8">
				<table class="table table-striped table-bordered table-hover">
				    <thead>
				        <tr>
				            <th>Active Subscribers</th>
				            <th>Avg Sub Rate</th>
				            <th>Avg Unsub Rate</th>
				            <!---<th>Avg Delivery Rate</th>--->
				        </tr>
				    </thead>
				    <tbody>
				        <tr>
				            <td>#cm_as - cm_aur#</td>
				            <td>#NumberFormat(cm_asr, '9.99')# (per month)</td>
				            <td>#NumberFormat(cm_aur, '9.99')# (per month)</td>
				            <!---<td>#cm_adr EQ 0 ? '(Not enough data yet)' : NumberFormat(cm_adr, '9.99') & '%'#</td>--->
				        </tr>
				    </tbody>
				</table>
				<br>
				<div class="row heading">
					<div class="col-sm-12 heading-title">Subscriber Growth</div>
					<div class="col-sm-12">
						<div class="canvas-holder">
    						<canvas id="subscriber_growth_chart" width="818" height="409"></canvas>
    					</div>
    					<div id="subscriber_growth_legend">
    					</div>
					</div>
				</div>    		
        	</div>
        	<div class="col-lg-4">
				<table class="table table-striped table-bordered table-hover">
				    <thead>
				        <tr>
				            <th>Message Sent</th>
				            <th>Message Received</th>
				        </tr>
				    </thead>
				    <tbody>
				        <tr>
				            <td class="text-center">#messageNumber[1]#</td>
				            <td class="text-center">#messageNumber[2]#</td>
				        </tr>
				    </tbody>
				</table>
				<a class="download-subscribers" href="##" style="display: inline;" data-campaign-id = "#url.campaignid#">Download Activity Feed</a>				
                <cfinclude template="../views/commons/active_feed.cfm">    
        	</div>
        	</div>
    	</div>
    </section>
</main>

<cfset firstMonth  = arrayMin([firstMonth1, firstMonth2])>
<cfset lastMonth  = arrayMax([lastMonth1, lastMonth2])>
<cfset allSubscribers = {}>

<cfif lastMonth LE 0>
	<cfset lastMonth = now()>
	<cfset lastYear = dateFormat(lastMonth, 'yyyy')>
	<cfset lastMonth = dateFormat(lastMonth, 'm')>
<cfelse>
	<cfset firstYear = dateFormat(firstMonth, 'yyyy')>
	<cfset firstMonth = dateFormat(firstMonth, 'm')>
	<cfset lastYear = dateFormat(lastMonth, 'yyyy')>
	<cfset lastMonth = dateFormat(lastMonth, 'm')>
	<cfset cm_as = 0>
	<cfloop condition=true>
		<cfset cm_key = 'X' & firstMonth & firstYear>
		<cfif structKeyExists(subscribersTotal, cm_key)>
			<cfset cm_as += subscribersTotal[cm_key]>
		</cfif>
		<cfif structKeyExists(unsubscribersTotal, cm_key)>
			<cfset cm_as -= unsubscribersTotal[cm_key]>
		</cfif>
		<cfset allSubscribers[cm_key] = cm_as>
		<cfif firstYear EQ lastYear AND firstMonth EQ lastMonth>
			<cfbreak>
		</cfif>
		<cfset firstMonth++>
		<cfif firstMonth GT 12>
			<cfset firstMonth = 1>
			<cfset firstYear++>
		</cfif>
	</cfloop>
</cfif>
<script type="text/javascript">
	var subscriber_growth_chart_data = {
		labels: [],
		datasets: [
			{
				label: "Subscribers",
	            fillColor: "rgba(86,140,165,0.5)",
	            strokeColor: "rgba(86,140,165,0.8)",
	            highlightFill: "rgba(86,140,165,0.75)",
	            highlightStroke: "rgba(86,140,165,1)",
	            data: []
			}/*,
			{
				label: "Unsubscribers",
	            fillColor: "rgba(220,220,220,0.5)",
				strokeColor: "rgba(220,220,220,0.8)",
				highlightFill: "rgba(220,220,220,0.75)",
				highlightStroke: "rgba(220,220,220,1)",
	            data: []
			},
			{
				label: "Delivery Message",
	            fillColor: "rgba(116,195,127,0.5)",
	            strokeColor: "rgba(116,195,127,0.8)",
	            highlightFill: "rgba(116,195,127,0.75)",
	            highlightStroke: "rgba(116,195,127,1)",
	            data: []
			}*/
		]
	};
	<cfloop index="i" from="1" to="6">
		subscriber_growth_chart_data.labels.unshift('#MonthAsString(lastMonth)#');
		
		<cfset cm_key = 'X' & lastMonth & lastYear>
		<cfif structKeyExists(subscribersTotal, cm_key)>
			subscriber_growth_chart_data.datasets[0].data.unshift(#allSubscribers[cm_key]#);
		<cfelse>
			subscriber_growth_chart_data.datasets[0].data.unshift(0);
		</cfif>
		<!---<cfif structKeyExists(subscribersTotal, cm_key)>
			subscriber_growth_chart_data.datasets[1].data.unshift(#subscribersTotal[cm_key]#);
		<cfelse>
			subscriber_growth_chart_data.datasets[1].data.unshift(0);
		</cfif>
		<cfif structKeyExists(unsubscribersTotal, cm_key)>
			subscriber_growth_chart_data.datasets[2].data.unshift(#unsubscribersTotal[cm_key]#);
		<cfelse>
			subscriber_growth_chart_data.datasets[2].data.unshift(0);
		</cfif>--->
		<!---<cfif structKeyExists(deliveriesTotal, cm_key)>
			subscriber_growth_chart_data.datasets[2].data.unshift(#deliveriesTotal[cm_key]#);
		<cfelse>
			subscriber_growth_chart_data.datasets[2].data.unshift(0);
		</cfif>--->
		<cfset lastMonth-->
		<cfif lastMonth LT 1>
			<cfset lastMonth = 12>
			<cfset lastYear-->
		</cfif>
	</cfloop>
</script>
</cfoutput>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/js/jquery-ui-1.11.4.custom/jquery-ui.css">
</cfinvoke>
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js">
</cfinvoke>
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/vendors/Chart.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/opt_in_report.js">
</cfinvoke>

<cfparam name="variables._title" default="Campaign Report - Sire">
<cfinclude template="../views/layouts/main.cfm">