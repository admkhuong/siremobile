<cfparam name="campaignId" default="0">

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("../assets/global/plugins/daterangepicker/daterangepicker.css", true)
        .addCss("/session/sire/assets/global/plugins/Chartist/chartist.min.css", true)
        .addCss("/session/sire/assets/global/plugins/Chartist/plugins/tooltips/chartist-plugin-tooltip.css", true)
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)
        .addCss("/session/sire/assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/daterangepicker/moment.min.js", true)
        .addJs("../assets/global/plugins/daterangepicker/daterangepicker.js", true)
        .addJs("../assets/pages/scripts/script-04.js")
        .addJs("../assets/pages/scripts/campaign-reports.js")
        .addJs("/session/sire/assets/global/plugins/Chartist/chartist.min.js", true)
        .addJs("/session/sire/assets/global/plugins/Chartist/plugins/tooltips/chartist-plugin-tooltip.js", true)
        .addJs("/session/sire/assets/global/plugins/uikit/js/uikit.min.js", true)
        .addJs("/session/sire/assets/global/plugins/chartjs/Chart.bundle.js", true)
        .addJs("/session/sire/assets/global/scripts/filter-bar-generator.js")
        .addJs("/public/sire/js/select2.min.js");

</cfscript>

<cfinvoke method="GetBatchDetails" component="session.sire.models.cfc.control-point" returnvariable="RetVarGetBatchDetails">
    <cfinvokeargument name="inpBatchID" value="#campaignId#">
</cfinvoke>

<cfinvoke method="GetCampaignDetail" component="session.sire.models.cfc.campaign" returnvariable="RetVarBatchDetails">
    <cfinvokeargument name="INPCAMPAIGNID" value="#campaignId#">
</cfinvoke>
<cfinvoke method="GetTotalOptOutByBatch" component="session.sire.models.cfc.campaign" returnvariable="rtGetTotalOptOutByBatch">
    <cfinvokeargument name="inpBatchId" value="#campaignId#">
</cfinvoke>

<cfinvoke method="GetMultipleChoiceBySurveyBatch" component="session.sire.models.cfc.reports" returnvariable="RetReportVal">
    <cfinvokeargument name="inpBatchId" value="#campaignId#">
</cfinvoke>

<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="userOrgInfo">
</cfinvoke>

<cfinvoke component="session.sire.models.cfc.reports" method="GetDataForSurveyReport" returnvariable="GetSurveyData">
    <cfinvokeargument name="inpBatchId" value="#campaignId#"/>
</cfinvoke>

<cfinvoke component="session.sire.models.cfc.reports" method="GetAllCustomersReponsedABatch" returnvariable="GetAllCustomersData">
    <cfinvokeargument name="inpBatchId" value="#campaignId#"/>
</cfinvoke>

<cfinvoke component="session.sire.models.cfc.campaign" method="GetCampaignBlastScheduleInfo" returnvariable="GetCampaignBlastScheduleInfoData">
    <cfinvokeargument name="inpBatchId" value="#campaignId#"/>
</cfinvoke>

<cfif RetVarGetBatchDetails.RECORDCOUNT LE 0>
    <cflocation url="/session/sire/pages/campaign-manage" addtoken="false">
</cfif>

<cfset campaignData.BatchId_bi = RetVarGetBatchDetails.BATCHID>
<cfset campaignData.Desc_vch = RetVarGetBatchDetails.DESC>
<cfset campaignData.EMS_Flag_int = RetVarGetBatchDetails.EMSFLAG>
<cfset campaignData.GroupId_int = RetVarGetBatchDetails.GROUPID>
<cfset campaignData.MLPID = RetVarGetBatchDetails.MLPID>
<cfset campaignData.TEMPLATEID = RetVarGetBatchDetails.TEMPLATEID>
<cfset campaignData.TemplateType = RetVarGetBatchDetails.TemplateType>

<input type="hidden" name="inpBatchId" id="inpBatchId" value="<cfoutput>#campaignData.BatchId_bi#</cfoutput>"/>
<input type="hidden" name="EMS_Flag" id="EMS_Flag" value="<cfoutput>#campaignData.EMS_Flag_int#</cfoutput>">
<input type="hidden" name="templateType" id="templateType" value="<cfoutput>#campaignData.TemplateType#</cfoutput>">
<input type="hidden" name="templateId" id="templateId" value="<cfoutput>#campaignData.TEMPLATEID#</cfoutput>">
<input type="hidden" name="campaignName" id="campaignName" value="<cfoutput>#campaignData.Desc_vch#</cfoutput>"/>
<input type="hidden" name="companyName" id="companyName" value="<cfoutput>#userOrgInfo.ORGINFO.ORGANIZATIONNAME_VCH#</cfoutput>"/>
<input type="hidden" name="batchCreatedDate" id="batchCreatedDate" value="<cfoutput>#DateFormat(RetVarBatchDetails.CAMPAIGNOBJECT.CREATED_DT, "yyyy-mm-dd")#</cfoutput>"/>
<input type="hidden" name="minCustomerOrder" id="minCustomerOrder" value="1">
<input type="hidden" name="maxCustomerOrder" id="maxCustomerOrder" value="<cfoutput>#ArrayLen(GetAllCustomersData["DATALIST"])#</cfoutput>">

<style type="text/css">
    table td.center, table th.center{
        text-align: center;
    }
</style>

<div class="portlet light bordered subscriber">
    <div class="portlet-body form">
        <cfif campaignData.TemplateType NEQ 1>
            <h2 class="page-title" style="margin-top: 0;">Report: <cfoutput>#campaignData.Desc_vch#</cfoutput></h2>
        <cfelse>
            <h2 class="page-title" style="margin-top: 0;">Report: <cfoutput>#campaignData.Desc_vch#</cfoutput></h2>
            <h4><cfoutput>#GetCampaignBlastScheduleInfoData.campaignBlastScheduleInfo#</cfoutput></h4>
            <h4><b>Total Opted-out: <a id="totalOptout" href="javascript:;"><cfoutput>#rtGetTotalOptOutByBatch.OPTOUT#</cfoutput></a></b></h4>
        </cfif>

<!---         <h4 class="optin-display"><b>&nbsp;</b></h4> --->
        <div class="row">
            <div class="col-xs-12">
                <cfif campaignData.TEMPLATEID NEQ 11 AND campaignData.TemplateType NEQ 1>
    		        <div class="options">
                        <div class="row">
                            <div class="col-xs-12 col-sm-3">
                                <cfif campaignData.TEMPLATEID NEQ 9>
                                    <div class="select-list">
                                        <p><b>Selected List</b></p>
                                        <select name="subcriber-list" class="form-control Select2">
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                </cfif>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                <div class="date-range">
                                    <p><b>Selected Date Range</b></p>
                                    <div class="reportrange pull-right">
                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                        <span></span> <b class="caret"></b>
                                        <input type="hidden" name="dateStart" value="">
                                        <input type="hidden" name="dateEnd" value="">
                                        <input type="hidden" name="inpBatchID" value="<cfoutput>#campaignId#</cfoutput>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row optin-display">
                    <!---   <div class="col-xs-12">
                                <button class="btn-re-dowload download-subcriber pull-right">
                                    <i class="fa fa-download"></i>
                                </button>
                            </div> --->
                    </div>
                    <!--- options select --->

                 <!---    <cfif campaignData.TEMPLATEID EQ 9>
                        <div class="options">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-sm-offset-6">
                                    <div class="date-range">
                                        <p><b>Selected Date Range</b></p>
                                        <div class="reportrange pull-right">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                            <span></span> <b class="caret"></b>
                                            <input type="hidden" name="dateStart" value="">
                                            <input type="hidden" name="dateEnd" value="">
                                            <input type="hidden" name="inpBatchID" value="<cfoutput>#campaignId#</cfoutput>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </cfif> --->

                    <div class="row">
                        <div class="col-xs-12">
                            <div id="highchart" class="campaign-report-detail">
                       
                            </div>
                        </div>
                    </div>
                    <!--- charts --->

                    <div class="row col-md-12 export-data-div">
                        <button class="btn green-gd export-data" id="export-multiple-choice"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORT</button>
                    </div>

                    <div class="row">
                        <div class="col-md-12 wrapper-tblcCampaignReportData">
                            <div class="subscriber-list">
                                <div class="re-table">
                                    <div class="table-responsive">
                                        <table id="tblcCampaignReportData" class="table-responsive table-striped dataTables_wrapper dataTable">
                                        </table>
                                    </div>
                                </div>    
                            </div>
                            <a href="##" class="expand-btn hidden">+ Expand</a>
                        </div>
                    </div>
                </cfif>

                <!--- single yes/no question --->
                <cfif campaignData.TEMPLATEID EQ 5>
                    <div class="row single-yesno-section" style="margin-top:60px">
                        <div class="col-md-6">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="tblSingleYesNoList" class="table table-bordered table-striped table-report dataTable">
                                        
                                    </table>
                                </div>
                            </div>                 
                        </div>
                        <div class="col-md-6">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="tblSingleYesNoDetailsList" class="table table-bordered table-hover">
                                        
                                    </table>
                                </div>
                            </div>                 
                        </div>
                    </div>
                    <!--- single yes/no question --->
                </cfif>
                
                <!--- multiplechoicelist --->
                <cfif campaignData.TEMPLATEID EQ 4>
                    <div class="row multiple-choice-section">
                        <div class="col-md-6">
                          
                                <div class="table-responsive re-table">
                                    <table id="tblMultiplechoiceList" class="table-responsive table-striped dataTables_wrapper dataTable">
                                        
                                    </table>
                                </div>
                                           
                        </div>
                        <div class="col-md-6">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="tblMultiplechoiceDetailsList" class="table table-bordered table-hover">
                                        
                                    </table>
                                </div>
                            </div>                 
                        </div>
                    </div>
                </cfif>
                <!--- multiplechoicelist --->

                <cfif campaignData.TEMPLATEID EQ 7>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="tblMultiplechoiceSurveyList" class="table table-bordered table-hover">
                                        
                                    </table>
                                </div>
                            </div>                 
                        </div>

                        <div class="col-md-12">
                            <div class="table-responsive re-table">
                                <table id="tblMultiplechoiceSurveyDetails" class="table-responsive table-striped dataTables_wrapper dataTable">
                                </table>
                            </div>
                        </div>
                    </div>
                </cfif>

                
                <cfif campaignData.TEMPLATEID NEQ 11>
                    <cfif campaignData.TemplateType NEQ 1>
                        <hr class="sub-list-section" />
                    </cfif>
                    <div class="row sub-list-section">
                        <div class="col-md-6">
                            <div class="table-responsive re-table">
                                <table id="tblSubcriberListBlast" class="table-responsive table-striped dataTables_wrapper dataTable">
                                    
                                </table>
                            </div>
                        </div>
                    </div>               
                    <br>
                    <br>
                    <br>                    
                    <!--- <h4>User response this blast</h4>                     --->
                    <div class="row">
                        <div class="col-md-10">
                            <div id="tblSubcriberListResponseBlastFilterBox">                                
                            </div>                        
                        </div>
                        <div class="col-md-2">                            
                            <button class="btn-response-dowload response-blast-list"><i class="fa fa-download" aria-hidden="true"></i></button> 
                        </div>
                    </div>         
                    <div class="row sub-list-section">
                        <div class="col-md-12">
                            <div class="table-responsive re-table">
                                <table id="tblSubcriberListResponseBlast" class="table-responsive table-striped dataTables_wrapper dataTable">   
                                </table>
                            </div>
                        </div>
                    </div>

                    <cfif campaignData.TemplateType NEQ 1>
                        <!--- BEGIN: SUBCRIBER LIST --->
                        <div class="row optin-display sub-list-section">
                            <div class="col-lg-8 col-md-12 left-group-contact">
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 col-lg-6 col-sm-6" id="add-newsldiv">
                                        <button type="button" id="btn-add-new" class="btn green-gd btn-re">ADD NEW LIST</button>
                                    </div>
                                    <!--- <div class="col-md-6 col-xs-6 col-lg-6 col-sm-6" id="subscribered-countdiv">
                                        <label class="label-search-subscriber" id="subscribered-count"><strong>Subscribed: <span id="total-subcriber"></span></strong></label>
                                    </div> --->
                                    
                                </div>
                                <h4><b>Total Opted-out: <a id="totalOptout" href="javascript:;"><cfoutput>#rtGetTotalOptOutByBatch.OPTOUT#</cfoutput></a></b></h4>
                                <div class="subscriber-list">
                                    <div class="re-table">
                                        <div class="table-responsive">
                                            <table id="tblSubcriberList" class="table table-bordered table-striped table-report">
                                                
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <div class="search-contact">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-4 col-xs-12 col-lg-4 col-sm-4" id="subscribered-countdiv">
                                                <label class="label-search-subscriber pull-left" id="subscribered-count"><strong>Subscribed: <span id="total-subcriber"></span></strong></label>
                                            </div>
                                            <div class="col-md-8 col-xs-12 col-lg-8 col-sm-8">
                                                <div class="search-subscriber">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="text" name="contact-keyword" class="form-control" placeholder="Search Contacts">
                                                            <input type="hidden" name="sub-id" value="">
                                                            <span class="input-group-addon btn-search-contact">
                                                                <i class="fa fa-search" aria-hidden="true"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="subscriber-phone-list">
                                    <div class="re-table">
                                        <div class="table-responsive">
                                            <table id="subcriberListDetail" class="table table-bordered table-hover">
                                                
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--- END :SUBCRIBER LIST --->

                        <!--- BEGIN : --->
                        <div class="row sub-list-section">
                            <div class="col-lg-12 col-md-12">
                                <div class="subscriber-list">
                                    <div class="table-responsive re-table">
                                        <table id="tblSubcriberListOldReport" class="table-responsive table-striped dataTables_wrapper dataTable">
                                            
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--- END : --->
                    </cfif>
                </cfif>
            </div>
        </div>
    </div>
</div>

<!--- new survey --->
<cfif campaignData.TEMPLATEID EQ 11>
    <div class="portlet light bordered">
        <ul uk-tab="connect: .campaign-report-content" class="cp-report-tab">
            <li class="uk-active">
                <a href="#">Question Summaries</a>
            </li>
            <li>
                <a href="#">Individual Responses</a>
            </li>
        </ul>
    </div>

    <div class="portlet light bordered">
        <ul class="uk-switcher uk-margin campaign-report-content">
            <li>
                <cfset order = 0/>
                <cfloop array="#GetSurveyData["DATALIST"]#" index="i">
                    <div class="portlet light">
                        <div class="ques-item">
                            <h4 class="title"><cfoutput>#i.NAME#</cfoutput></h4>
                           
                                <ul uk-tab class="cp-report-tab">
                                    <li><a href="#">Data Table</a></li>
                                    <cfif i.TYPE EQ "ONESELECTION">
                                        <li><a href="#">Graph</a></li>
                                    </cfif>
                                </ul>

                                <ul class="uk-switcher uk-margin ques-list">
                                    <li>
                                        <div class="re-table">
                                            <div class="table-responsive ">
                                                <table id="tblNewSurvey<cfoutput>#order#</cfoutput>" class="table table-bordered table-striped">
                                                </table>
                                            </div>
                                        </div>
                                        <button style="float: right" class="btn green-gd export-data export-question-report" data-id="<cfoutput>#order#</cfoutput>" id="export-question-report-<cfoutput>#order#</cfoutput>"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXPORT</button>
                                    </li>
                                    <cfif i.TYPE EQ "ONESELECTION">
                                        <li>
                                            <canvas id="graph-canvas-<cfoutput>#order#</cfoutput>"></canvas>
                                        </li>
                                    </cfif>
                                </ul>
                               
                        </div>
                    </div>
                    <cfset order = order + 1/>
                </cfloop>
            </li>

            <li>
                <div class="portlet light">                
                    <cfif ArrayLen(GetAllCustomersData["DATALIST"]) EQ 0>
                        <span style="font-size: 16px">No response found!</span>
                    <cfelse>
                        <div class="uk-grid uk-grid-small" uk-grid>
                            <div class="uk-width-1-5@l uk-width-1-2@m">
                                <select class="form-control Select2" id="respondent-select">
                                    <!--- <option value="0">Select a respondent</option> --->
                                    <cfloop array="#GetAllCustomersData["DATALIST"]#" index="index1">
                                        <option value="<cfoutput>#index1.ORDER#</cfoutput>">Respondent #<cfoutput>#index1.ORDER#</cfoutput></option>
                                    </cfloop>
                                </select>
                            </div>
                            <div class="uk-width-expand@s">
                                <a type="button" class="btn green-gd" id="previous-customer"><i class="fa fa-chevron-left"></i></a>
                                <a type="button" class="btn green-gd" id="next-customer"><i class="fa fa-chevron-right"></i></a>
                            </div>

                            <div class="uk-width-1-1">
                                <ul class="uk-list stt-head-individual" style="display: none">
                                    <li>
                                        <h4 id="customer-respondent-status"></h4>
                                    </li>
                                    <li>
                                        <b>Date:</b> <span id="customer-respondent-date"></span>
                                    </li>
                                    <li>
                                        <b>Phone Number:</b> <span id="customer-respondent-phone"></span><a id="make-a-chat" title="Create new chat session to this customer" class="btn-chat-xs"><i class="fa fa-commenting-o" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </div>

                            <div class="uk-width-1-1">
                                <hr class="line-space">
                            </div>

                            <div class="uk-width-1-1" id="respondent-answer-list">
                                <!--- <div class="anws-item"><h4 class="title">Q1:  How old are you?</h4><ul class="uk-list"><li>31-35</li></ul></div> --->
                            </div>
                        </div>
                    </cfif>        
                </div>
            </li>
        </ul>
    </div>
</cfif>
<!--- end new survey --->


        

<!-- Modal rename template -->
<div class="modal fade" id="rename-subscriber-list" role="dialog">
    <div class="modal-dialog">
        <form id="rename-subscriber-list-form" name="rename-subscriber-list-form">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><strong>RENAME SUBSCRIBER LIST</strong></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>New subscriber list name:</label>
                        <input type="hidden" id="contact-group-id">
                        <input type="text" name="new-subscriber-list-name" class="form-control validate[required]" id="new-subscriber-list-name">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn-save btn btn-success-custom" >Save</button>
                    <button type="button" class="btn btn-back-custom" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>
    </div>
</div>


<!-- Modal -->
<div class="bootbox modal fade" id="AddNewSubscriberList" tabindex="-1" role="dialog" aria-labelledby="AddNewSubscriberList" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form name="frm-add-new">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title title">Add New Subscriber List</h4>            
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="subscriber_list_name">Subscriber List Name:<span class="text-danger"> *</span></label>
                        <input id="subscriber_list_name" class="form-control validate[required, custom[onlyLetterNumberSp]]" maxlength="255">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success-custom btn-save-group"> Save </button>
                    &nbsp; &nbsp; &nbsp; 
                    <button type="button" class="btn btn-back-custom" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal For Blast list-->
<div class="modal fade" id="messages-sent-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="">Messages Sent</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="blastMessageSentFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="message-blast-sent-table" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--- <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-keyword-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div> --->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>
<!--- end modal --->

<!-- Modal For Queued list-->
<div class="modal fade" id="messages-queued-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="">Messages Queued</strong></h4>
            </div>
            <div class="modal-body">
                <div id="cbghfghrfh">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="blastMessageQueuedFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="message-blast-queued-table" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--- <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-keyword-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div> --->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>
<!--- end modal --->

<!-- Modal For Blast list-->
<div class="modal fade" id="messages-wait-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="">Messages Waiting</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="blastMessageWaitFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="message-blast-wait-table" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--- <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-keyword-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div> --->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>
<!--- end modal --->

<!-- Modal For Blast list-->
<div class="modal fade" id="response-answer-detail-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <input type="hidden" name="" id="answer-cpid" value="">
            <input type="hidden" name="" id="answer-qid" value="">
            <input type="hidden" name="" id="answer-format" value="">
            <input type="hidden" name="" id="answer-text" value="">
            <input type="hidden" name="" id="answer-regex" value="">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="">Response detail</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="responseAnswerFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="response-answer-detail" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-response-answer-detail"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>
<!--- end modal --->
<!-- Modal Optout list-->
<div class="modal fade" id="mdOptOutList" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="">Opted-out List</strong></h4>
            </div>
            <div class="modal-body">
                <div id="cbghfghrfh">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="optFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="tblOptOutList" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload dowload-optout-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>
<!--- end modal --->

<cfparam name="variables._title" default="Campaign Reports #campaignData.Desc_vch# - Sire">
<cfinclude template="../views/layouts/master.cfm">

