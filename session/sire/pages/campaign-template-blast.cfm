<cfparam name="templateId" default="0">
<cfparam name="campaignId" default="0">
<cfparam name="inpTemplateFlag" default="0">
<cfparam name="adv" default="0">
<cfparam name="action" default="CreateNew">
<cfparam name="hiddenTemplatePickerClass" default="">
<cfparam name="hiddenClass" default="">
<cfparam name="templateType" default="0">
<cfparam name="selectedTemplateType" default="0">
<cfparam name="displayOptin" default="0">
<cfparam name="displayIntervalType" default="0">
<cfparam name="countCPOneSelection" default="0">
<cfparam name="displaySubcriberList" default="0">


<cfset inpSimpleViewFlag = "#adv#" />
<cfset optinDetectString = "Reply YES to confirm, HELP for help or STOP to cancel"/>

<cfset variables.menuToUse = 1 />
<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addJs("../assets/global/plugins/jquery-knob/js/jquery.knob.js", true)
        .addJs("../assets/global/plugins/chartjs/Chart.bundle.js", true)
        .addJs("/public/sire/js/select2.min.js")
        .addJs("../assets/pages/scripts/ejs_production.js", true)
        .addJs("../assets/pages/scripts/campaign.js")
        .addJs("/public/js/jquery.tmpl.min.js", true)
        .addJs("/public/js/jquery.ddslick.js", true)
        //.addJs("../assets/pages/scripts/another-message.ejs")        
        .addJs("../assets/pages/scripts/campaign-template-blast.js")
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addCss("../assets/global/plugins/uikit2/css/uikit.css", true)
        .addCss("../assets/global/plugins/uikit2/css/components/slidenav.min.css", true)

        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/jquery.timepicker.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)
        .addCss("/session/sire/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css", true)
        .addJs("/session/sire/assets/global/plugins/bootstrap-datetimepicker/js/moment-with-locales.js", true)
        .addJs("/session/sire/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js", true)
        .addJs("/session/sire/assets/global/plugins/bootstrap-datetimepicker/js/jquery.timepicker.min.js", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)
        .addJs("../assets/global/plugins/uikit2/js/uikit.min.js", true)
        .addJs("../assets/global/plugins/uikit2/js/components/slideset.min.js", true)
        .addJs("/public/js/jquery.ddslick.js", true)
        .addJs("/session/sire/js/schedule-campaign-blast.js", true)

        .addCss("/session/sire/css/screen.css", true)
        .addCss("/session/sire/css/style.css", true)
        .addCss("/session/sire/css/message.css", true)
        .addCss("/session/sire/css/schedule.css", true)
        .addCss("/public/css/campaign/mycampaign/schedule.css", true)
        
    </cfscript>

    <style type="text/css">
        .help-block {
            font-style: italic;
            font-size: 1.5rem;
            /*color: #8f8f8f;*/
            color: rgb(143,143,143);
        }
        .row-subscriber .uk-active .subscriber-item.greenbox:hover {
            background-color: #74c37f !important;
        }
        .row-subscriber .uk-active .subscriber-item.bluebox:hover {
            background-color: #578ca5 !important;
        }

        .campaign-simon-cancel{
            margin-top: 1rem;
            margin-bottom: 3rem;
            padding-left: 2rem;
            padding-right: 2rem;
        }

        .increments-indicator {
            display: block;
        }

        #cppNameDisplay:hover
        {
            color: #000000;
            cursor: pointer;	
            text-decoration: underline;	
        }
    </style>

    <cfset monthNames = ['January', 'February', 'March', 'April', 'May', 'June ', 'July', 'August', 'September', 'October', 'November', 'December']/>    
    <cfinvoke method="getTemplateList" component="session.sire.models.cfc.campaign" returnvariable="rxTemplateList">
    </cfinvoke>

    <cfset campaignBlastScheduleInfo= {}>
    <cfinvoke method="GetCampaignBlastScheduleInfo" component="session.sire.models.cfc.campaign" returnvariable="campaignBlastScheduleInfo">
        <cfinvokeargument name="inpBatchId" value="#campaignId#">
    </cfinvoke>    

    <!--- Read Current Profile data and put into form --->
    <cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="RetVarGetUserOrganization"></cfinvoke>    
    <!---<cfset cpNameSectionShow = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH EQ '' ? 0 : 1/>--->
    <cfset cpNameSectionShow = 0/> <!---new logic , not show company name section --->

    <cfset campaignData = {
        BatchId_bi = '',
        KeywordId_int = '',
        Keyword_vch = '',
        Keyword_txt = '',
        Desc_vch = '',
        EMS_Flag_int = 0,
        GroupId_int = 0,
        Schedule_arr = [],
        ControlPoint_arr = [],
        CustomHelpMessage_vch = '',
        CustomStopMessage_vch = '',
        TEMPLATEID = 0,
        OPTIN_GROUPID = 0,
        BLAST_GROUPID = 0,
        ScheduleOption=0
    }>

    <cfset scheduleData = '' />
    <cfset scheduleDateTime = ''/>
    <cfset scheduleTime = ''/>

    <cfset btnSendNowText="Send now">
    <cfset btnBlastNowText="Schedule for Later">
    <cfif campaignBlastScheduleInfo.campaignBlastScheduleInfo GT 0>        
        <cfset btnSendNowText="Send Another now">
        <cfset btnBlastNowText="Schedule Another for Later">
    </cfif> 

    <!--- MINHHTN HOT FIX --->
    <cfparam name="Session.AdditionalDNC" default="">

<!--- remove old method: 
<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>            
--->
<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode">   
    <cfinvokeargument name="inpBatchId" value="#campaignId#">
    </cfinvoke>

    <cfif campaignId GT 0>

        <cfif templateId EQ 0>
            <cfset hiddenTemplatePickerClass = ""/>    
            <cfset hiddenClass = "hidden"/>
            <cfset action = "Edit"/>
            <cfset actionHeader = "Edit Campaign"/>
            <cfset btnSaveLabel = "Save"/>
        <cfelse>
            <cfset actionHeader = "Create Campaign"/>
            <cfset btnSaveLabel = "Finish"/>
        </cfif>


        <cfinvoke method="GetBatchDetails" component="session.sire.models.cfc.control-point" returnvariable="RetVarGetBatchDetails">
            <cfinvokeargument name="inpBatchID" value="#campaignId#">
    </cfinvoke>           
    
            <cfif RetVarGetBatchDetails.RECORDCOUNT LE 0>
                <cflocation url="/session/sire/pages/campaign-manage" addtoken="false">
            </cfif>

            <cfif RetVarGetBatchDetails.TEMPLATEID EQ 0>
                <cflocation url="/session/sire/pages/campaign-edit?campaignId=#campaignId#" addtoken="false">
            </cfif>

            <cfif RetVarGetBatchDetails.RXRESULTCODE GT 0>
                <cfif RetVarGetBatchDetails.SHORTCODEID NEQ shortCode.SHORTCODEID>
                    <cflocation url="/session/sire/pages/campaign-manage" addtoken="false"/>
                </cfif>
            </cfif>

            <cfset campaignData.BatchId_bi = RetVarGetBatchDetails.BATCHID>
            <cfset campaignData.Desc_vch = RetVarGetBatchDetails.DESC>
            <cfset campaignData.EMS_Flag_int = RetVarGetBatchDetails.EMSFLAG>
            <cfset campaignData.GroupId_int = RetVarGetBatchDetails.GROUPID>
            <cfset campaignData.MLPID = RetVarGetBatchDetails.MLPID>
            <cfset campaignData.TEMPLATEID = RetVarGetBatchDetails.TEMPLATEID>
            <cfset campaignData.TemplateType_ti =  RetVarGetBatchDetails.TemplateType>
            <cfset campaignData.ScheduleOption =  RetVarGetBatchDetails.ScheduleOption>
            

            <!--- Preserve possible newlines in display but format all other tags --->
            <!--- Expected HTML output is different for straight div vs text area - Preview, Edit, SWT etc --->                    
            <!--- Allow newline in text desc messages - reformat for display --->
            <!--- Regular HTML --->
            <cfset CDescHTML = ReplaceNoCase(campaignData.Desc_vch, "newlinex", "<br>", "ALL")>
            <cfset CDescHTML = Replace(CDescHTML, "\n", "<br>", "ALL")>
            <cfset CDescHTML = Replace(CDescHTML, "#chr(10)#", "<br>", "ALL")>
            <cfset CDescHTML = Replace(CDescHTML, "{defaultShortcode}", "#shortCode.SHORTCODE#", "ALL")>
            <!--- Text Area version --->                
            <cfset CDescTA = Replace(campaignData.Desc_vch, "\n", "#chr(10)#", "ALL")>
            <cfset CDescTA = Replace(CDescTA, "<br>", "#chr(10)#", "ALL")>
            <cfset CDescTA = Replace(CDescTA, "{defaultShortcode}", "#shortCode.SHORTCODE#", "ALL")>

    <!--- Detect Optin Type --->
    <cfset optinDetect = Find(optinDetectString, RetVarGetBatchDetails.XMLCONTROLSTRING)/>

            <cfinclude template="/session/cfc/csc/constants.cfm">
            <cfquery name="keywordQuery" datasource="#session.DBSourceREAD#">
                SELECT KeywordId_int, Keyword_vch, Created_dt, Active_int, EMSFlag_int
                FROM sms.keyword WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RetVarGetBatchDetails.BATCHID#">
                AND IsDefault_bit = 0
                ORDER BY Created_dt DESC, KeywordId_int DESC
            </cfquery>

            <cfif keywordQuery.RecordCount GT 0 && keywordQuery.Active_int[1] EQ 1>
                <cfset campaignData.KeywordId_int = keywordQuery.KeywordId_int[1]>
                <cfset campaignData.Keyword_vch = keywordQuery.Keyword_vch[1]>
                <cfif keywordQuery.EMSFlag_int[1] EQ 0>
                    <cfset campaignData.Keyword_txt = campaignData.Keyword_vch>
                </cfif>
            </cfif>

            <!--- --->
            <cfinvoke method="GetSchedule" component="session.cfc.schedule" returnvariable="ScheduleByBatchId">
                <cfinvokeargument name="INPBATCHID" value="#campaignId#">
                </cfinvoke>
                <cfif ScheduleByBatchId.RXRESULTCODE GT 0>
                    <cfloop array="#ScheduleByBatchId.ROWS#" index="ROW">                       
                        <cfset Arrayappend(campaignData.Schedule_arr, ROW)>
                    </cfloop>
                    <cfset scheduleData = listToArray(campaignData.Schedule_arr[1].START_DT, '-')/>

                    <cfset Arrayappend(scheduleData,"#campaignData.Schedule_arr[1].STARTHOUR_TI#")/>
                    <cfset Arrayappend(scheduleData,"#campaignData.Schedule_arr[1].STARTMINUTE_TI#")/>
                    <cfset Arrayappend(scheduleData,"#campaignData.Schedule_arr[1].ENDHOUR_TI#")/>
                    <cfset Arrayappend(scheduleData,"#campaignData.Schedule_arr[1].ENDMINUTE_TI#")/>

                    <cfset scheduleDate = dateFormat(campaignData.Schedule_arr[1].START_DT, 'mm/dd/yyyy')>
                    <cfset scheduleTime = LSTimeFormat(campaignData.Schedule_arr[1].STARTHOUR_TI&":"&campaignData.Schedule_arr[1].STARTMINUTE_TI, 'hh:mm tt')>
                    <cfset scheduleDateTime = scheduleDate&" "& scheduleTime/>

                </cfif>

                <cfparam name="SCHEDULE" default="#campaignData.Schedule_arr#" >




                <!--- GET CUSTOME STOP/HELP --->
                <cfquery name="customMessageQuery" datasource="#session.DBSourceREAD#">
                    SELECT * FROM sms.smsshare WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#campaignId#">
                </cfquery>

                <cfloop query="#customMessageQuery#">
                    <cfswitch expression="#Keyword_vch#" >
                        <cfcase value="HELP">
                            <cfset campaignData.CustomHelpMessage_vch = Content_vch>
                        </cfcase>
                        <cfcase value="STOP">
                            <cfset campaignData.CustomStopMessage_vch = Content_vch>
                        </cfcase>
                    </cfswitch>
                </cfloop>

                <!--- GET SUBCRIBER LIST --->
                <!--- <cfset subcriberArray = []> --->
                <cfinvoke component="session.sire.models.cfc.subscribers" method="GetSubcriberList" returnvariable="groupList">
                    <cfinvokeargument name="inpShortCodeId" value="#shortCode.SHORTCODEID#">
                    </cfinvoke>
                    <cfif groupList.RXRESULTCODE EQ 1 AND groupList.iTotalRecords GT 0>
                        <cfset subcriberList = groupList.listData>
                    <cfelse>
                        <cfset subcriberList = []/>    
                    </cfif>

                    <cfinvoke method="GetLatestBlastGroup" component="session.sire.models.cfc.campaign" returnvariable="blastList">
                        <cfinvokeargument name="inpBatchId" value="#campaignId#"/>
                    </cfinvoke>

                    <cfif blastList.RXRESULTCODE GT 0 AND blastList.GROUPID GT 0>
                        <cfset campaignData.BLAST_GROUPID = blastList.GROUPID/>
                    </cfif>

                    <cfinvoke method="getTemplateList" component="session.sire.models.cfc.campaign" returnvariable="rxTemplateDetail">
                        <cfinvokeargument name="inpTemplateId" value="#campaignData.TEMPLATEID#"/>
                        <cfinvokeargument name="inpCheckActive" value="0"/>
                    </cfinvoke>

                    <cfloop query="#rxTemplateDetail.TEMPLATELIST#">
                        <cfset displayOptin = #DisplayOptin_ti#/>    
                        <cfset displayIntervalType = #DisplayInteval_ti#/>
                        <cfset displaySubcriberList = #DisplaySubcriberList_ti#/>
                        <cfset templateId = #TID_int#>
                        <cfset templateName = replaceNoCase(#Name_vch#, "<Br/>", " ","ALL")>
                        <cfset templateType = #Type_ti#/>
                        <cfset displayPreview = #DisplayPreview_ti#/>
                    </cfloop>

                <cfelseif isNumeric(templateId) AND inpTemplateFlag EQ 0>
                    <!--- Create new Batch --->
                    <cfinvoke method="AddNewBatchFromTemplateNew" component="session.sire.models.cfc.control-point" returnvariable="RetVarAddNewBatchFromTemplate">
                        <cfinvokeargument name="inpTemplateId" value="#templateId#">
                        <cfinvokeargument name="inpTemplateType" value="#selectedTemplateType#">
                        <cfinvokeargument name="inpDesc" value="">
                        <cfinvokeargument name="inpKeyword" value="">
                        <cfinvokeargument name="inpShortCode" value="#shortCode.SHORTCODE#">
                    </cfinvoke>
                    <!--- Skip the middle man and just load page after template is copied --->       
                    <cfif RetVarAddNewBatchFromTemplate.BATCHID GT 0>
                        <cflocation url="campaign-template-blast?campaignId=#RetVarAddNewBatchFromTemplate.BATCHID#&templateType=#RetVarAddNewBatchFromTemplate.TEMPLATETYPE#&templateID=#templateId#&selectedTemplateType=#selectedTemplateType#" addToken="false" />
                    <cfelse>
                        <cflocation url="campaign-template-choice" addToken="false" />
                    </cfif>
                <cfelse>
                    <cflocation url="/session/sire/pages/campaign-manage" addtoken="false">
                </cfif>


<cfoutput>

    <!--- campaign template list choice --->
    <cfinclude template="campaign-template-list.cfm">    
    <!--- END campaign template list choice --->


    <input value="#shortCode.SHORTCODE#" id="ShortCode" type="hidden" name="ShortCode">
    <input id="optin-detect" value="#optinDetect#" type="hidden">
    <div>
    <form name="frm-campaign" id="frm-campaign"> 
    <!-- Start For first campaign -->
    <div class="content-for-first-campaign">

    <!--- SELECT TEMPLATE TYPE --->
    <cfif templateType EQ 2>
        <div class="portlet light bordered">
            <div class="portlet-body">
                <h4 class="portlet-heading template-name">#templateName#</h4>
                <cfif action EQ 'CreateNew' OR action EQ 'SelectTemplate'>
                    <h4 class="portlet-heading2 mb-20">Who do you want to send this message to?</h4>
                </cfif>     
                <div class="uk-grid uk-grid-medium row-subscriber" uk-grid>
                    <div class="uk-width-1-2@s #(campaignData.TemplateType_ti EQ 0) ? "uk-active" : "uk-unactive"#">
                        <div class="subscriber-item greenbox" data-type='0'>
                            <div class="uk-grid uk-grid-small uk-flex-middle">
                                <div class="uk-width-2-5 text-center">
                                    <img class="img-responsive" src="../assets/layouts/layout4/img/subscriber-keyword.png" alt="">
                                </div>
                                <div class="uk-width-3-5">
                                    <h4 class="titleh4">Subscribers who send your</h4> 
                                    <h4 class="titleh4"> Keyword to your short code </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-1-2@s #(campaignData.TemplateType_ti EQ 1) ? "uk-active" : "uk-unactive"#">
                        <div class="subscriber-item bluebox" data-type='1'>
                            <div class="uk-grid uk-grid-small uk-flex-middle">
                                <div class="uk-width-2-5 text-center">
                                    <img class="img-responsive" src="../assets/layouts/layout4/img/subscriber-sound.png" alt="">
                                </div>
                                <div class="uk-width-3-5">
                                    <h4 class="titleh4">Subscribers on an</h4>
                                    <h4 class="titleh4"> existing list </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    

                <div class="mb-20"></div>                    
            </div>
        </div>
    </cfif>
    <!--- END SELECT TEMPLATE TYPE --->
        
     <!--- BEGIN : CAMPAIGN NAME --->                                                    
    <div class="portlet light bordered">
        <div class="portlet-body">
            
            <div class="row">
                <div class="col-md-6">
                    <div class="form-gd form-lb-large">
                        <div class="form-group">
                            <label class="hidden">Give Your Campaign a Name</label><br>
                            <cfif action eq "CreateNew">
                                <input type="text" style="display: none" class="form-control validate[required,custom[noHTML]]" placeholder="" name="Desc_vch" id="Desc_vch" value="Blast to Subscribers on #LSDateFormat(now(), 'yyyy-mm-dd')#"> 
                                <span id="cppNameDisplay">Blast to Subscribers on #LSDateFormat(now(), 'yyyy-mm-dd')#</span>                               
                                <cfif cpNameSectionShow EQ 0><input type="hidden" name="OrganizationName_vch" id="OrganizationName_vch" value="#RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH#"></cfif> 
                            <cfelse>
                                <input type="text" class="form-control validate[required,custom[noHTML]]" placeholder="" name="Desc_vch" id="Desc_vch" value="#CDescHTML#">                                    
                                <span class="help-block hidden">ex: Summer Happy Hour Promo – 20% off</span>  
                                <cfif cpNameSectionShow EQ 0><input type="hidden" name="OrganizationName_vch" id="OrganizationName_vch" value="#RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH#"></cfif>      
                            </cfif>                             
                        </div>
                    </div>
                </div>
                <div class="col-md-12 hidden">                                
                    <div class="form-group">
                        <a href="javascript:;" class="btn green-gd campaign-next #hiddenClass#">Next <i class="fa fa-caret-down"></i></a>

                        <!--- Button Cancel When wan't create a Campaign --->
                        <cfif campaignData.BatchId_bi GT 0>
                            <a href="##" onclick="openCampaignSimon()" class="btn green-cancel campaign-simon-cancel pull-right #hiddenClass#">Cancel</a>       
                        </cfif>                   
                    </div>
                </div>
            </div>    
        </div>
    </div>        
    <!--- END : CAMPAIGN NAME --->

    <!--- CP RENDER WRAPPER --->
    <div class="portlet light bordered cpedit msg-content">
        <div class="portlet-body">

            <!--- RENDER CP --->
            <cfset MaxCPCount = 0 />
            <cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPs" returnvariable="RetVarReadCPs">
                <cfinvokeargument name="inpBatchId" value="#campaignid#">
                <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
            </cfinvoke>

            <h4 class="portlet-heading2">Write Your Message</h4>

            <cfloop array="#RetVarReadCPs.CPOBJ#" index="CPOBJ">                   
                <cfset MaxCPCount++ />

                <cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPDataById" returnvariable="RetVarReadCPDataById">
                    <cfinvokeargument name="inpBatchId" value="#campaignid#">
                        <cfinvokeargument name="inpQID" value="#CPOBJ.RQ#">
                            <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
                            </cfinvoke>

             <!---    <pre>
                    <cfdump var="#RetVarReadCPDataById.CPOBJ.SWT#"/>
                </pre> --->
                
                <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'OPTIN'>
                    <cfset campaignData.OPTIN_GROUPID = RetVarReadCPDataById.CPOBJ.OIG />
                </cfif>

                <cfif templateId EQ 11>
                    <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'ONESELECTION' || RetVarReadCPDataById.CPOBJ.TYPE EQ 'SHORTANSWER' >
                        <cfset countCPOneSelection ++ />
                    </cfif>    
                <cfelse>
                    <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'ONESELECTION'>
                        <cfset countCPOneSelection ++ />
                    </cfif>    
                </cfif>

                <!--- <cfif RetVarReadCPDataById.CPOBJ.TYPE NEQ 'BRANCH' AND RetVarReadCPDataById.CPOBJ.TYPE NEQ 'OPTIN' > --->
                <cfif RetVarReadCPDataById.CPOBJ.TYPE NEQ 'BRANCH' OR (templateId EQ 7) >
                    <cfif RetVarReadCPDataById.CPOBJ.TYPE NEQ 'OPTIN' OR (RetVarReadCPDataById.CPOBJ.TYPE EQ 'OPTIN' AND displayOptin EQ 1)>

                        <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'TRAILER' AND RetVarReadCPDataById.CPOBJ.SWT EQ 0>
                            <!--- do nothing --->
                        <cfelse>    
                            <cfinvoke method="RenderCPNew" component="session.sire.models.cfc.control-point" returnvariable="RetVarRenderCP">
                                <cfinvokeargument name="controlPoint" value="#RetVarReadCPDataById.CPOBJ#">
                                <cfinvokeargument name="inpBatchId" value="#campaignid#">   
                                <cfinvokeargument name="inpSimpleViewFlag" value="#inpSimpleViewFlag#"> 
                                <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">                     
                                <cfinvokeargument name="inpdisplayIntervalType" value="#displayIntervalType#">
                                <cfinvokeargument name="inpTemplateId" value="#templateId#">                     
                            </cfinvoke> 
                                <!--- This is the HTML for a CP opject - HTML is maintained in RenderCP function --->   
                                #RetVarRenderCP.HTML#
                        </cfif>    

                    </cfif>
                </cfif>
            </cfloop>

            <cfif displayPreview EQ 1>
                <a href="javascript:;" class="btn green-gd campaign-next nextCpeditCheckRule #hiddenClass#">Next <i class="fa fa-caret-down"></i></a>
            <cfelse>        
                <a href="javascript:;" class="btn green-gd campaign-next #hiddenClass#">Next <i class="fa fa-caret-down"></i></a>
            </cfif>
            
        </div>
    </div>
    <!--- END CP RENDER WRAPPER --->

    <!--- BEGIN SELECT SUBCRIBER --->
    <div class="portlet light bordered cpedit">
        <div class="portlet-body">
            <h4 class="portlet-heading2">Select a Subscriber List</h4>
            <div class="row row-small">
                <div class="col-md-12 col-xs-12">
                    <p class="portlet-subheading">
                        A Subscriber List contains the phone numbers of everyone who opted into your campaign by texting your keyword to your short code. 
                    </p>
                </div>
            </div>

            <div class="row row-small">
                <div class="col-md-6 col-xs-6">
                    <div class="form-gd form-lb-large">
                        <div class="form-group">
                            <select class="form-control validate[required] Select2" id="SubscriberList" multiple="multiple">
                                <cfif arrayLen(subcriberList) GT 0>
                                    <cfloop array="#subcriberList#" index="array_index">
                                     <!---  <option #campaignData.BLAST_GROUPID EQ array_index[1] ? "selected" : ""# value="#array_index[1]#">#array_index[2]# (#array_index[3]#)</option> --->
                                     <option value="#array_index[1]#">#array_index[2]# (#array_index[3]#)</option>
                                 </cfloop>
                             </cfif>
                         </select>
                        </div>
                         <div class="help-block pull-right">Total subcribers: <span class="totalSubcriberSelected">0</span> 
                            <span> <i class="fa fa-question-circle" title="When sending to multiple lists: if a contact appears on more than one list, duplicates will be automatically removed so each contact will only receive 1 message." uk-tooltip pos="right"></i> </span>
                        </div>
                    </div>
                </div>
            </div>

            <a href="javascript:;" class="btn green-gd campaign-next #hiddenClass#">Next <i class="fa fa-caret-down"></i></a>                                  
        </div>
    </div>
     <!--- END SELECT SUBCRIBER --->

 
    <div class="portlet light bordered cpedit confirm-block">
        <div class="portlet-body">
            <h4 class="portlet-heading2">Confirm & Send</h4>
            <p class="portlet-subheading">
                You are about to send the message
            </p>
            <div class="row">
                <div class="col-lg-6">
                    <div class="guess light-blue" id="preview_message"> 
                        
                    </div>
                </div>
                <div class="col-lg-6">
                <p>                
                    #campaignBlastScheduleInfo.campaignBlastScheduleInfo#
                 </p>               
                </div>
            </div>

            <p class="portlet-subheading">
                To <span class="totalSubcriberSelected portlet-heading2">0</span> people
            </p>    

            <!---<h4 class="portlet-heading2"> Now you have a choice: </h4>--->
            <p>
                <a href="javascript:;" class="btn green-gd btn-blast-now">#btnSendNowText#</a>                
                <a href="javascript:;" class="btn green-gd campaign-next btn-schedule-later">#btnBlastNowText#</a>
                <a href="javascript:;" class="btn green-gd btn-save-blast btn-confirm-section">Save & Exit</a>
            </p>    
        </div>
    </div> 

    <!--- BEGIN SCHEDULE --->
    <cfif campaignData.TemplateType_ti EQ 1>

        <div class="portlet light bordered cpedit schedule-section #hiddenClass#">
            <div class="portlet-body">
                <h4 class="portlet-heading2">Schedule Your Blast</h4>  
                <input type="hidden" id="schedule-option" value="#campaignData.ScheduleOption#"/>
                <p class="portlet-subheading">
                    The scheduled time can be changed before the campaign is started and while it is paused. Scheduled times are PST.
                </p>              
                <!--- simple schedule --->
                <div id ="simple_schedule" <cfif campaignData.ScheduleOption is 1>style="display:none"</cfif>>                    
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="row row-small">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input-group datetimepicker date set-z-index" id="datetimepicker">
                                            <input type='text' class="form-control" value="#scheduleDateTime#" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    Start time
                    <div class="row row-small">                                                       
                        <div class="col-sm-2">
                            <div class="form-group">
                                <input id="schedule-time-start-hour" type='text' class="form-control schedule-time-start-hour" value="#scheduleTime#"/>
                            </div>
                        </div>
                    </div>
                </div>
                <!--- simple schedule --->
                <div id ="advance_schedule" <cfif campaignData.ScheduleOption is 0>style="display:none"</cfif>>
                         <cfinclude template="cpp_advance_schedule.cfm">
                </div>
                
                <a href="javascript:;" Id = "btn_advance_schedule" class="" <cfif campaignData.ScheduleOption is 1>style="display:none"</cfif>>Advanced Schedule </a>                
                <a href="javascript:;" Id = "btn_simple_schedule" class="" <cfif campaignData.ScheduleOption is 0>style="display:none"</cfif>>Simple Schedule </a>
                
                <!---<a href="javascript:;" class="btn green-gd #hiddenClass# campaign-next">Next <i class="fa fa-caret-down"></i></a>--->
                <div> 
                    <br>                 
                    <a href="javascript:;" class="btn green-gd btn-save-schedule">Save & Send scheduled blast</a>
                    <a href="javascript:;" class="btn green-gd btn-cancel-schedule">Cancel Schedule</a>
                </div>
            </div>
        </div> 
    </cfif>
    <div class="portlet light bordered cpedit hidden">
    </div>    


    </form>
    </div>
<!-- End For first campaign -->
<!--- <cfinclude template="template_example.cfm"> --->
</div>

<!-- START MODAL -->
<div class="modal be-modal fade" id="modalCompanyUpdate" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="frCompanyUpdate">
                <div class="modal-header">                                            
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>
                <div class="modal-body">
                    <h4 class="be-modal-title">Update Company Name</h4>
                    <div class="form-gd">
                        <div class="form-group">
                           <p>
                            Phone carriers require all business messages to be easily identifiable to the company who sends them. Your company name will appear before your message.
                           </p>
                           <input type="text" class="form-control validate[required,custom[noHTML]]" name="txtCompanyUpdate" id="txtCompanyUpdate" maxlength="250" data-prompt-position="topLeft:100" placeholder="Enter Company Name" value="#RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH#">
                           <br>
                        </div>
                    </div>
                    <div class="text-right">
                        <button type="submit" id="btn-save-company" class="btn green-gd">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div> 
<!-- END MODAL -->


<cfsavecontent variable="AnswerListItemTemplate">
    <cfoutput>

        <div class="AnswerItem row row-small">
            <div class="col-xs-8 col-sm-10">
                <div class="form-group">
                    <input id="OPTION" type="text" value="Not useful" class="form-control">
                </div>
            </div>

            <div class="col-xs-2 col-sm-1">
                <div class="form-group">
                    <input type="text" id="AVAL" class="form-control readonly-val text-center" value="1">
                    <input type="hidden" id="AVALREG" class="form-control readonly-val text-center" value="">
                </div>
            </div>
            <div class="col-xs-2 col-sm-1">
                <div class="form-group">
                    <button class="form-control delete-answer">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
        </div>
    </cfoutput>             
</cfsavecontent>  



<script type="text/javascript">
    var OPTIN_GROUPID = #campaignData.OPTIN_GROUPID#;
    var BLAST_GROUPID = #campaignData.BLAST_GROUPID#;
    var GroupsListBox = null;

    <!--- Set some globally avaialble javascript vars based on current data --->
    var schedule_data = "";
    var SHORTCODE = "#shortCode.SHORTCODE#";
    
    var date_txt_1 = '#LSDateFormat(DateAdd("d", 30, Now()), "m-d-yyyy")#';
    var date_txt_2 = '#LSDateFormat(NOW(), "m-d-yyyy")#';

    var ScheduleTimeData;
    
    <!--- Set Global var for inpBatchId for Preview .js --->
    var inpBatchId = #campaignData.BatchId_bi#;
    var inpBatchName = '#campaignData.Desc_vch#';

    var action = '#action#';
    
    var #toScript(AnswerListItemTemplate, "AnswerItemjsVar")#; 

    var inpCampaignType = #campaignData.TemplateType_ti#;
    var inpTemplateId = #templateId#;      
    var inpCampaignId = #campaignId#;

    var countCPOneSelection = #countCPOneSelection#;
    var currentCPOneSelection = 1;

    var intListDeleteRID = [];

    var checkClickPreview = 0;

    <cfif templateId EQ 11>
        var selectorCP = "data-template-id="+inpTemplateId+"";
    <cfelse>
        var selectorCP = "data-control-point-type='ONESELECTION'";
    </cfif>

    var subcriberList = #SerializeJSON(subcriberList)#

    var displayPreview = #displayPreview#

</script>

</cfoutput>

<cfif action EQ "CreateNew">
    <input type="hidden" id="currentStep" value="1">
    <input type="hidden" id="totalStep" value="1">
    <cfsavecontent variable="variables.portleft">
        <div class="portlet light bordered box-widget-sidebar">
            <div class="portlet-body non-pt">
                <div class="wrap-dial-step">
                    <h4 class="stt-step">Progress</h4>
                    <div class="step-holder">
                        <canvas id="myStep" class="mauto" width="200" height="200"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </cfsavecontent>
</cfif>

<!-- Modal -->
<div class="modal fade" id="ConfirmLaunchModal" tabindex="-1" role="dialog" aria-labelledby="ConfirmLaunchModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Confirm Launch Campaign</h4>            
            </div>
            <div class="modal-body clearfix">

                <div id="ProcessingBlast" style="display: none; min-height: 150px;">
                    <h4>Your blast is loading into queue now:</h4>
                    <img src="/public/sire/images/loading.gif" class="ajax-loader">
                </div>

                <!--- Confirmation Display --->
                <div class="ConfirmationSummary">
                    <div class="form-group clearfix">
                        <div class="col-xs-9 col-sm-6">Number of Subscribers:</div>
                        <div class="col-xs-3 col-sm-6" id="confirm-number-subscribers">0</div>
                    </div>
                    
                    <div class="form-group clearfix">
                        <div class="col-xs-9 col-sm-6">Already Blasted this Campaign:</div>
                        <div class="col-xs-3 col-sm-6" id="confirm-list-duplicates">0</div>
                    </div>
                    
                    <div class="form-group clearfix">
                        <div class="col-xs-9 col-sm-6">Total Eligible:</div>
                        <div class="col-xs-3 col-sm-6" id="confirm-total-elligable">0</div>
                    </div>
                    
                    <div class="form-group clearfix">
                        <div class="col-xs-9 col-sm-6">Credits Needed:</div>
                        <div class="col-xs-3 col-sm-6" id="confirm-credits-needed">0</div>
                    </div>
                    
                    <div class="form-group clearfix">
                        <div class="col-xs-9 col-sm-6">Credits Available:</div>
                        <div class="col-xs-3 col-sm-6" id="confirm-credits-available">0</div>
                    </div>

                    <div id="confirm-launch-alert-text" class="clearfix" style="display: none;">
                        <div class="col-xs-12 col-sm-12">
                            <!--- /session/sire/pages/buy-sms --->
                            <div class="alert alert-info" role="alert">
                                You do not have enough credits to launch this campaign.<br>
                                Please <a id="confirm-launch-buy-credits" href="/session/sire/pages/my-plan?active=addon" target="_blank">click here</a> to buy more credits.
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </div>
            <div class="modal-footer ConfirmationSummary">
                <button type="button" class="btn green-gd btn-finished" id="btn-confirm-save-and-send">Confirm Blast</button>
                &nbsp;
                <button type="button" class="btn green-cancel" id="btn-cancel-send" data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<!--- <div class="modal be-modal fade sr-modal" id="CompleteModal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            </div>
                <div class="modal-body">
                    <div class="portlet-body">
                        <h3 class="modal-title" style="margin-bottom: 0">High Five! You're done!</h3>
                        <p class="text-center">Let's test it out. Grab your phone and text your keyword to the short<br>
                            phone number <cfoutput>#shortCode.SHORTCODE#</cfoutput>. If it looks good, you're ready to go. If you see<br>
                                something you want to change, <a href="/session/sire/pages/campaign-template-blast?campaignid=<cfoutput>#campaignId#</cfoutput>">edit your campaign here.</a></p>
                    </div>
                    <div class="image3">
                        <img class="img-responsive campaign-finished" src="../assets/layouts/layout4/img/create-campaigns-finished.png" alt="">
                        <h2>
                            <cfoutput>#shortcode.SHORTCODE#</cfoutput>           
                        </h2> 
                    </div>        
                </div>
        </div>
    </div>
</div> --->



<!-- Modal rename template -->
<div class="modal be-modal fade" id="edit-cp-text" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="edit-cp-text-form" name="edit-cp-text-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <h3 class="modal-title">EDIT MESSAGE</h3>
                    <div class="form-group">
                        <label>Message:</label>
                        <textarea class="form-control no-resize validate[required,maxSize[1000]]" id="message-content" name="message-content" rows="10" data-prompt-position="topLeft:100"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn-save btn btn-success-custom" >Save</button>
                    <button type="button" class="btn btn-back-custom" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!--- Modal Cancel When wan't create a Campaign --->
<cfif campaignData.BatchId_bi GT 0>
    <div class="modal be-modal fade" id="cancelCampaignTemplate" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="cancel-campaign-template" name="cancel-campaign-template">
                    <div class="modal-header">
                        <a href="##" onclick="dismissCampaignSimon()" class="close">&times;</a>
                    </div>
                    <div class="modal-body">
                        <h3 class="modal-title"></h3>
                        <div class="form-group">
                            <label>If you cancel, your current campaign draft will be discarded. Are you sure you would like to continue?</label>
                            <input type="hidden" value="<cfoutput>#campaignData.BatchId_bi#</cfoutput>" id="idCampaignSimon">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="##" onclick="removeCampaignSimon()"  class="btn-save btn btn-success-custom" >Yes</a>
                        <a href="##" onclick="dismissCampaignSimon()" class="btn btn-back-custom">No</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</cfif>

<!-- START MODAL -->
<div class="modal be-modal fade sr-modal" id="makeAGoodCampaign" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="frm_subscriber_list">
                <div class="modal-header">                                            
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>
                <div class="modal-body">
                    <h3 class="modal-title">Benefits of Using a Naming Convention</h3>
                    <p>Being disciplined about how you name your campaigns has several notable benefits:</p>
                    <div class="inner-modal">
                        <p class="strong">Shareable</p>
                        <p>Using a clear naming structure also makes your account structure easily understandable by others that may work on your account in the future. Collaboration becomes easy because colleagues will know exactly what a campaign is meant to do.</p>
                        <p class="strong">Filtering</p>
                        <p>You can also search for, or filter by, parts of the campaign name (e.g., You can filter by campaigns that target the US by search for campaign names that contain “CA -“).</p>
                        <p class="strong">Group Similar Campaigns</p>
                        <p>Once your naming convention is in place, you can group similar campaigns together quickly and easily. All it takes is a quick click on the campaign name header to sort the names alphabetically. <br> Pro tip: Sorting similar campaigns works best if you start your naming structure with the most common settings first and then move on to the more unique settings (e.g., start with “target audience type” and end with the “main keyword”).</p>
                        <p class="strong">High-level Reporting</p>
                        <p>When similar campaigns are easily sorted and filtered by name, spotting trends becomes easier</p>
                        <br/>
                        <br/>
                    </div>    
                </div>
            </form>
        </div>
    </div>
</div>  
<!-- END MODAL -->

<!--- Save as new template modal --->
<div class="modal fade" id="AdminSaveNewTemplateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
               <h4 class="modal-title"><strong>SAVE AS NEW TEMPLATE</strong></h4>
               <div class="modal-body-inner row">
                  <form name="save-as-new-template" class="col-sm-12" autocomplete="off" id="save-as-new-template-form">

                     <div class="form-group">
                       <label for="templatename" id="template-name-label">Template name</label>
                       <label class="pull-right"><a id="template-name-countdown">255</a> left</label>
                       <input type="text" class="form-control validate[required, custom[noHTML], maxSize[255]]" id="template-name" name="template-name">
                   </div>

                   <div class="form-group">
                     <label for="templatecategory" id="template-category-label">Template category</label>
                     <select class="form-control" id="select-template-category">
                        <!---  --->
                    </select>
                </div>

                <div class="form-group">
                 <label for="templatedescription" id="template-description-label">Template Description</label>
                 <label class="pull-right"><a id="template-description-countdown">1000</a> left</label>
                 <textarea class="form-control validate[maxSize[1000], required]" id="template-description" name="template-description" placeholder="" rows="6"></textarea>
             </div>

             <div class="form-group">
                 <label for="templateimage" id="template-image-label">Template Image</label>
                 <select class="form-control" id="select-template-image">
                    <!---  --->
                </select>
            </div>

            <div class="form-group">
             <label for="inpTemplateType" class="bc-title mbTight">Template Type</label>										
             <select id="inpTemplateType" class="Select2" data-width="100%">	
               <option value="0" selected="selected">Default Keyword OR Blast</option>
               <option value="1">Keyword Only</option> 
               <option value="2">Blast Only</option>                              
           </select>			            
       </div>

       <div class="row hidden">
         <div class="form-group col-sm-3 col-xs-6 col-md-4 col-lg-4">
            <label for="templateorder" id="template-order-label">Template order</label>
            <select class="form-control" id="select-template-order">
               <!---  --->
           </select>
       </div>
   </div>


   <div class="form-group">
     <label for="templatexml" id="template-xml-label">Template XML String</label>
     <textarea class="form-control validate[required]" id="template-xml" name="template-xml" placeholder="" rows="6" data-prompt-position="topLeft:100"></textarea>
 </div>

 </form>
 <div class="save-new-template-btn-group">
    <button class="btn btn-back-custom" id="close-save-new-template-btn" data-dismiss="modal">Cancel</button>
    <button class="btn btn-success-custom" id="confirm-save-new-template-btn" type="submit">Save</button>
</div>
</div>
</div>
</div>
</div>
</div>

<cfinclude template="../views/commons/inc_carousel.cfm">

<cfif action EQ 'Edit'>
    <cfparam name="variables._title" default="Edit Campaign - Sire">
<cfelse>
    <cfparam name="variables._title" default="Campaign Create - Sire">        
</cfif>

<style type="text/css">
    .image { 
        position: relative;
    }

    .image > h2 { 
        position: absolute; 
        top: 121px; 
        left: 0px;
        right: 0px;
        margin-left: auto;
        margin-right: auto;
        font-size: 12px;
        color: black;
        font-weight: bold;
    }

    @media screen and (max-width: 480px) {
        .small-hide {
            display: none;
        }
    }

    .image2 { 
        position: relative;
    }

    .image2 > h2 { 
        position: absolute; 
        top: 25%; 
        left: 52%;
        right: 0px;
        margin-left: auto;
        margin-right: auto;
        font-size: 10px;
        color: black;
        font-weight: bold;
    }

    @media screen and (min-width: 1921px) {
        .image2 > h2 {
            top: 99px; 
            left: 196px;
        }
    }

    @media screen and (min-width: 1700px) and (max-width: 1921px) {
        .image2 > h2 {
            top: 99px;
            left: 196px;
        }
    }

    @media screen and (min-width: 1600px) and (max-width: 1700px) {
        .image2 > h2 {
            top: 99px;
            left: 12vw;
        }
    }

    @media screen and (min-width: 1456px) and (max-width: 1599px) {
        .image2 > h2 {
            top: 6.3vw;
            left: 12.4vw;
        }
    }

    @media screen and (min-width: 1400px) and (max-width: 1455px) {
        .image2 > h2 {
            top: 6vw;
            left: 12.4vw;
        }
    }

    @media screen and (min-width: 1360px) and (max-width: 1399px) {
        .image2 > h2 {
            top: 5.8vw;
            left: 12vw;
        }
    }

    @media screen and (min-width: 1280px) and (max-width: 1359px) {
        .image2 > h2 {
            top: 5.5vw;
            left: 11.8vw;
        }
    }

    @media screen and (min-width: 1248px) and (max-width: 1280px) {
        .image2 > h2 {
            top: 67px;; 
            left: 11.5vw;
        }
    }

    @media screen and (max-width: 1240px) {
        .image2 > h2 { 
            position: absolute; 
            top: 25%; 
            left: 52%;
            right: 0px;
            margin-left: auto;
            margin-right: auto;
            font-size: 0.7vw;
            color: black;
            font-weight: bold;
        }
    }

    @media screen and (max-width: 1140px) {
        .image2 > h2 { 
            position: absolute; 
            top: 24%; 
            left: 52%;
            right: 0px;
            margin-left: auto;
            margin-right: auto;
            font-size: 0.7vw;
            color: black;
            font-weight: bold;
        }
    }

    @media screen and (max-width: 1070px) {
        .image2 > h2 { 
            position: absolute; 
            top: 23%; 
            left: 52%;
            right: 0px;
            margin-left: auto;
            margin-right: auto;
            font-size: 0.7vw;
            color: black;
            font-weight: bold;
        }
    }

    @media screen and (max-width: 991px) {
        .image2 > h2 { 
            position: absolute; 
            top: 98px; 
            left: 194px;
            right: 0px;
            margin-left: auto;
            margin-right: auto;
            font-size: 10px;
            color: black;
            font-weight: bold;
        }
    }

    @media screen and (max-width: 484px) {
        .image2 > h2 { 
            position: absolute; 
            top: 27%; 
            left: 194px;
            right: 0px;
            margin-left: auto;
            margin-right: auto;
            font-size: 10px;
            color: black;
            font-weight: bold;
        }
    }

    @media screen and (max-width: 452px) {
        .image2 > h2 { 
            position: absolute; 
            top: 26.2%; 
            left: 53%;
            right: 0px;
            margin-left: auto;
            margin-right: auto;
            font-size: 10px;
            color: black;
            font-weight: bold;
        }
    }

/*////////////////////////////////////*/

    .image3 { 
        position: relative;
    }

    .image3 > h2 { 
        position: absolute; 
        top: 23%; 
        left: 24%;
        right: 0px;
        margin-left: auto;
        margin-right: auto;
        font-size: 10px;
        color: black;
        font-weight: bold;
    }

    @media screen and (max-width: 768px) {
        .image3 > h2 { 
            top: 23%; 
            left: 34%;
            font-size: 10px;
        }
    }

    @media screen and (max-width: 720px) {
        .image3 > h2 { 
            top: 22%; 
            left: 21%;
            font-size: 9px;
        }
    }

    @media screen and (max-width: 320px) {
        .image3 > h2 { 
            top: 20%; 
            left: 21%;
            font-size: 8px;
        }
    }

</style>

<cfinclude template="../views/layouts/master.cfm">