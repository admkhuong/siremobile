<cfparam name="templateid" default="1">
<cfparam name="campaignid" default="">
<cfparam name="inpTemplateFlag" default="0">
<cfparam name="adv" default="0">
<cfparam name="action" default="SelectTemplate">
<cfparam name="hiddenTemplatePickerClass" default="">
<cfparam name="templateType" default="0">
<cfparam name="selectedTemplateType" default="0">
<cfparam name="countCPOneSelection" default="0">

<cfoutput>
    
<cfset inpSimpleViewFlag = "#adv#" />

<cfset variables.menuToUse = 1 />
<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addJs("../assets/global/plugins/jquery-knob/js/jquery.knob.js", true)
        .addJs("../assets/global/plugins/chartjs/Chart.bundle.js", true)
        //.addJs("../assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js", true)
        // .addJs("../assets/pages/scripts/core-uikit.min.js")
        // .addJs("../assets/pages/scripts/switcher.js")
        .addJs("../assets/pages/scripts/campaign-template-choice.js")
        .addJs("../assets/pages/scripts/preview.js")
        //.addJs("../assets/pages/scripts/another-message.ejs")
        .addJs("../assets/pages/scripts/ejs_production.js")
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addCss("../assets/global/plugins/uikit2/css/uikit.css", true)
        .addCss("../assets/global/plugins/uikit2/css/components/slidenav.min.css", true)
        .addCss("../assets/global/plugins/flexslider/flexslider.css", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)
        .addJs("../assets/global/plugins/uikit2/js/uikit.min.js", true)
        .addJs("../assets/global/plugins/uikit2/js/components/slideset.min.js", true)
        .addJs("../assets/global/plugins/flexslider/jquery.flexslider-min.js", true);
        // .addCss("../assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css");

</cfscript>
<cfinvoke method="CountTotalCampaign" component="session.sire.models.cfc.campaign" returnvariable="countTotalCampaign">
<cfif countTotalCampaign.TOTALCAMPAIGN EQ 0>
    <cflocation url="/session/sire/pages/campaign-template-new?templateid=1&selectedTemplateType=0&firstlogin=1" addtoken="false"/>
</cfif>
<cfinvoke method="getTemplateList" component="session.sire.models.cfc.campaign" returnvariable="rxTemplateList">
</cfinvoke>

<cfset hiddenClass = ""/>
<cfset actionHeader = "Create Campaign"/>

<!-- Modal Carousel -->
<div id="modal-introduce" class="uk-new-modal uk-modal modal-cardholder" uk-modal>
    <div class="uk-modal-dialog uk-modal-body addon-modal-dialog">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="my-text-center head-intro">
            <h4><b>High Five! You're done!</b></h4>
            <p>Here are some recommended next steps for you.</p>
        </div>
        <div class="wrap-set-introduce" data-my-slideset="{default: 1, small: 2}">
            <div class="my-slidenav-position">
                <ul class="my-grid my-slideset">
                    <li class="intro-item my-text-center">
                        <div class="inner-intro-item">
                            <div class="img uk-flex uk-flex-middle uk-flex-center">
                                <img src="\session\sire\images\introduce-img\intro-phone.png" alt="">
                            </div>
                            <div class="content">
                                <h4><b>Test your Campaign</b></h4>
                                <p>
                                    Test it out. Grab your phone and text your keyword to the short phone number 39492.  If it looks good, you're ready to go.
                                </p>
                            </div>
                        </div>
                    </li>
                    <li class="intro-item my-text-center">
                        <div class="inner-intro-item">
                            <div class="img uk-flex uk-flex-middle uk-flex-center">
                                <img src="\session\sire\images\introduce-img\intro-edit.png" alt="">
                            </div>
                            <div class="content">
                                <h4><b>Edit your Campaign</b></h4>
                                <p>
                                    Make changes to the campaign by navigating to <span>Manage Campaigns</span> in the left menu and clicking on the "edit" button
                                </p>
                            </div>
                        </div>
                    </li>
                    <li class="intro-item my-text-center">
                        <div class="inner-intro-item">
                            <div class="img uk-flex uk-flex-middle uk-flex-center">
                                <img src="\session\sire\images\introduce-img\intro-forum.png" alt="">
                            </div>
                            <div class="content">
                                <h4><b>Spread the Word</b></h4>
                                <p>
                                    Promote your keyword & short code by adding them to signs, flyers, mailers and coupons to start building your marketing list.
                                </p>
                            </div>
                        </div>
                    </li>
                    <li class="intro-item my-text-center">
                        <div class="inner-intro-item">
                            <div class="img uk-flex uk-flex-middle uk-flex-center">
                                <img src="\session\sire\images\introduce-img\intro-talk.png" alt="">
                            </div>
                            <div class="content">
                                <h4><b>We're here to help</b></h4>
                                <p>
                                    Our Client Support team is here to help you succeed.  If you have any questions or need help, email us at <span>hello@siremobile.com</span> or book an appointment by <a href="##">clicking here.</a>
                                </p>
                            </div>
                        </div>
                    </li>
                </ul>

                <a href="" class="my-slidenav my-slidenav-previous" data-my-slideset-item="previous"></a>
                <a href="" class="my-slidenav my-slidenav-next" data-my-slideset-item="next"></a>
            </div>
        </div>
    </div>
</div>

<cfinclude template="campaign-template-list.cfm">        



<script type="text/javascript">
var action = "#action#"
var inpBatchId = 0;
var countCPOneSelection = #countCPOneSelection#;
var currentCPOneSelection = 1;
var inpTemplateId = #templateid#;



</script>

</cfoutput>
    
<cfsavecontent variable="variables.portleft">
<!--- <div class="portlet light bordered box-widget-sidebar">
    <div class="portlet-body non-pt">
        <div class="wrap-dial-step">
            <h4 class="stt-step">Progress</h4>
            <div class="step-holder">
                <canvas id="myStep" class="mauto" width="200" height="200"></canvas>
            </div>
        </div>
    </div>
</div> --->
</cfsavecontent>

<cfparam name="variables._title" default="Campaign Template Choice - Sire">

<cfinclude template="../views/layouts/master.cfm">