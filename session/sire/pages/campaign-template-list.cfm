<!--- USE IN campaign-template.cfm --->
<cfif action EQ 'Edit'>
    <style type="text/css">
        .wrap-campaign-template .campaign-template.active .retangle-1:before{
            border-bottom: none;
        }
    </style>
</cfif>
 <cfinvoke method="CountTotalCampaign" component="session.sire.models.cfc.campaign" returnvariable="countTotalCampaign">
</cfinvoke>

<cfoutput>
<div class="portlet light bordered #hiddenTemplatePickerClass# ">
    <div class="portlet-body">
        <h4 class="campaign-heading-new">#actionHeader#</h4>
        <p class="portlet-subheading text-color-gray">
            <cfif action EQ 'Edit'>
                Edit your campaign, preview your changes and click 'Save'.
            <cfelse>
                We've collected the most popular text templates used by our clients. </br> Select the type of text message campaign you would like to create and we'll walk you through every step along the way!
            </cfif>
        </p>
       
            <!--- BEGIN TEMPLATE PICKER --->
            <div class="wrap-campaign-template">
                <div class="uk-grid uk-grid-upsmall for-col-nonrelative "  <cfif action EQ 'CreateNew' OR action EQ 'SelectTemplate'> uk-switcher="connect: ##campaign-switcher" </cfif> >
                    <cfloop query="#rxTemplateList.TEMPLATELIST#">
                        <cfif TID_int EQ templateid>
                            <cfset templateActive=1/>
                         <cfelse>
                            <cfset templateActive=0/>   
                        </cfif>
                        <!--- TEMPLATE DETAIL --->
                        <div class="uk-width-1-5@l uk-width-1-4@m uk-width-1-3@s uk-width-1-2"> <!--- col-lg-15 col-md-4 col-xs-6 --->
                            <div class="campaign-template <cfif trim(Description_vch) EQ ''>non-template</cfif> <cfif templateActive EQ 1>active</cfif>">

                                <div class="retangle-1">
                                    <div class="corner" data-template-id="#TID_int#"><a href="##" class="retangle__learnmore" data-toggle="modal" >Learn<br>More</a></div>
                                    <div class="inner-retangle"  data-template-id="#TID_int#" data-template-type="#Type_ti#">
                                        <cfif TID_int EQ 1 AND countTotalCampaign.TOTALCAMPAIGN GT 0>
                                            <span class="template-name">Opt-In Campaign</span>
                                        <cfelse>                                        
                                            <span class="template-name">#Name_vch#</span>
                                        </cfif>
                                    </div>
                                </div>
                                <cfif action EQ 'CreateNew' OR action EQ 'SelectTemplate'>
                                    <cfif trim(Description_vch) NEQ ''>
                                        <div class="campaign-template-content"> 
                                             #Description_vch#  
                                        </div>
                                    </cfif>    
                                </cfif>    
                            </div>                                    
                        </div>
                        <!--- TEMPLATE DETAIL --->
                    </cfloop>
                </div>    
            </div>    
            <!--- END TEMPLATE PICKER --->
            <!---<a href="javascript:;" class="btn green-gd campaign-next #hiddenClass# start-edit">Next <i class="fa fa-caret-down"></i></a>--->

    </div>
</div>
</cfoutput>
<cfinclude template="inc_learnmore_popup.cfm">    