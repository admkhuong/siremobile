<!--- <cfset text1= "\"/>
<cfset text2= "/"/>
<cfset regexUnicode  = '[^A-Za-z0-9 \u00A0\\r\\n\\t@£$¥èéùìòÇØøÅå\u0394_\u03A6\u0393\u039B\u03A9\u03A0\u03A8\u03A3\u0398\u039EÆæßÉ!\"$%&()*+,\\\-./:;<=>?¡ÄÖÑÜ§¿äöñüà^{}[~\\]|\u20AC\u2013\u2014õç`’´‘’′″“–‑−—一«»”！：•®οκ'&"'##]">

<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", regexUnicode) ) />
<cfset UniSearchMatcher1 = UniSearchPattern.Matcher(JavaCast( "string", text1 ) ) />
<cfset UniSearchMatcher2 = UniSearchPattern.Matcher(JavaCast( "string", text2 ) ) />

<cfdump var="#UniSearchMatcher1.find()#"/>
<cfdump var="#UniSearchMatcher2.find()#"/>
<cfabort/> --->

<cfparam name="templateId" default="0">
<cfparam name="campaignId" default="0">
<cfparam name="inpTemplateFlag" default="0">
<cfparam name="adv" default="0">
<cfparam name="action" default="CreateNew">
<cfparam name="hiddenTemplatePickerClass" default="">
<cfparam name="hiddenClass" default="">
<cfparam name="templateType" default="0">
<cfparam name="selectedTemplateType" default="0">
<cfparam name="displayOptin" default="0">
<cfparam name="displayIntervalType" default="0">
<cfparam name="countCPOneSelection" default="0">
<cfparam name="displaySubcriberList" default="0">
<cfparam name="displayPreview" default="0">
<cfparam name="RAWCONTENTTOSHOW" default="">
<cfparam name="MLPLINK" default="">
<cfparam name="arrCpBranch" default="#[]#">
<cfparam name="arrCpOptIn" default="#[]#">
<cfparam name="arrCpShortAns" default="#[]#">
<cfparam name="arrCpTrailer" default="#[]#">
<cfparam name="arrCpInterval" default="#[]#">
<cfparam name="arrCpStatement" default="#[]#">

<cfparam name="cpOptIn" default="0">

<cfset inpSimpleViewFlag = "#adv#" />
<div id="processingPayment" style="display: block;">
<img src="/public/sire/images/loading.gif" class="ajax-loader">
</div>

<cfset variables.menuToUse = 1 />

<cfinclude template="/session/sire/configs/paths.cfm">
<!---
//.addJs("/session/sire/assets/global/plugins/popper/popper.js", true)
 // .addJs("/session/sire/js/ckeditor/ckeditor.js", true)
 //.addJs("/session/sire/js/html2canvas.min.js", true)
 <!--- https://github.com/kenshin54/popline --->
--->
<cfscript>
   CreateObject("component","public.sire.models.helpers.layout")
        .addJs("../assets/global/plugins/jquery-knob/js/jquery.knob.js", true)
        .addJs("../assets/global/plugins/chartjs/Chart.bundle.js", true)

        .addJs("/public/sire/js/select2.min.js")
        .addJs("../assets/pages/scripts/ejs_production.js", true)
        .addJs("../assets/pages/scripts/campaign.js")
        .addJs("../assets/pages/scripts/preview.js")


        .addJs("../assets/pages/scripts/campaign-template-new.js")
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)
        .addCss("../assets/global/plugins/uikit2/css/uikit.css", true)

        .addCss("../assets/global/plugins/uikit2/css/components/slidenav.min.css", true)


        .addCss("/public/sire/css/jquery.timepicker.min.css", true)
        .addCss("/session/sire/js/vendors/dropzone/dropzone.css", true)



        .addCss("/session/sire/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css", true)
        .addCss("../assets/global/plugins/emojipicker/lib/css/emoji.css", true)
        .addCss("/session/sire/js/vendors/spectrum/spectrum.css", true)
        .addCss("/session/sire/js/vendors/popline/themes/popclip.css", true)


        .addJs("/session/sire/assets/global/plugins/bootstrap-datetimepicker/js/moment-with-locales.js", true)
        .addJs("/session/sire/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js", true)
        .addJs("/session/sire/assets/global/plugins/bootstrap-datetimepicker/js/jquery.timepicker.min.js", true)



        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)
        .addJs("../assets/global/plugins/uikit2/js/uikit.min.js", true)
        .addJs("../assets/global/plugins/uikit2/js/components/slideset.min.js", true)
        .addJs("../assets/global/plugins/emojipicker/lib/js/config.js", true)
        .addJs("../assets/global/plugins/emojipicker/lib/js/util.js", true)
        .addJs("/session/sire/js/jquery.sirecustom-emojipicker.js", true)

        .addJs("/session/sire/js/es6-promise.auto.js", true)
        .addJs("/session/sire/js/jspdf.min.js", true)

        .addJs("/session/sire/js/html2canvas.min.js", true)
        .addJs("../assets/global/plugins/emojipicker/lib/js/emoji-picker.js", true)
        .addJs("/session/sire/js/html2pdf.min.js", true)
        .addJs("/session/sire/js/vendors/list.min.js", true)
        .addJs("/session/sire/js/vendors/spectrum/spectrum.js", true)
        .addJs("/session/sire/js/vendors/popline/scripts/jquery.popline.js", true)


        .addJs("/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.link.js", true)
        .addJs("/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.decoration.js", true)
        .addJs("/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.list.js", true)
        .addJs("/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.justify.js", true)
        .addJs("/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.blockformat.js", true)
        .addJs("/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.social.js", true)
        .addJs("/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.textcolor-spectrum.js", true)
        .addJs("/session/sire/js/vendors/dropzone/min/dropzone.min.js", true)

        .addJs("/public/js/jquery.ddslick.js", true);

    </cfscript>

    <cfset monthNames = ['January', 'February', 'March', 'April', 'May', 'June ', 'July', 'August', 'September', 'October', 'November', 'December']/>

    <cfinvoke method="CountTotalCampaign" component="session.sire.models.cfc.campaign" returnvariable="countTotalCampaign">

    <cfinvoke method="getTemplateList" component="session.sire.models.cfc.campaign" returnvariable="rxTemplateList">
    </cfinvoke>

    <cfset campaignBlastScheduleInfo= {}>
    <cfinvoke method="GetCampaignBlastScheduleInfo" component="session.sire.models.cfc.campaign" returnvariable="campaignBlastScheduleInfo">
        <cfinvokeargument name="inpBatchId" value="#campaignId#">
    </cfinvoke>

    <cfinvoke component="session.sire.models.cfc.mlp" method="GetCampaignMLPTemplate" returnvariable="TemplatResult">
    </cfinvoke>

    <cfinvoke component="session.sire.models.cfc.mlp" method="ReadMLPTemplate" returnvariable="RetCPPXData">
        <cfinvokeargument name="ccpxDataId" value="#TemplatResult.TEMPLATEID#">
    </cfinvoke>


    <cfinvoke component="session.sire.models.cfc.mlp" method="GetMLPByCampaignId" returnvariable="GetMLPResult">
        <cfinvokeargument name="inpBatchId" value="#campaignId#">
    </cfinvoke>

    <!--- <cfdump var="#GetMLPResult.CPMLPID#" abort="true"/> --->

    <cfif campaignId GT 0 AND GetMLPResult.CPMLPID GT 0>
        <cfinvoke component="session.sire.models.cfc.mlp" method="ReadCPP" returnvariable="ReadCPPResult">
            <cfinvokeargument name="ccpxDataId" value="#GetMLPResult.CPMLPID#">
        </cfinvoke>

        <cfset RAWCONTENTTOSHOW = ReadCPPResult.RAWCONTENT/>
        <cfset MLPLINK = ReadCPPResult.CPPURL/>
    <cfelse>
        <cfset RAWCONTENTTOSHOW = RetCPPXData.RAWCONTENT/>
        <cfset MLPLINK = ''/>
    </cfif>


    <cfset MLPDOMAINPATH = cgi.HTTP_HOST EQ 'www.siremobile.com' ? 'https://mlp-x.com/' : 'https://#cgi.HTTP_HOST#/mlp-x/lz/index.cfm?inpURL='/>


    <input type="hidden" name="" id="cpMLPId" value="<cfoutput>#GetMLPResult.CPMLPID#</cfoutput>">
    <input type="hidden" name="" id="campaign-mlp-custom-css" value="<cfoutput>#RetCPPXData.CUSTOMCSS#</cfoutput>">

    <!--- Read Current Profile data and put into form --->
    <cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="RetVarGetUserOrganization"></cfinvoke>

    <cfset _has_Org = false>
    <cfif RetVarGetUserOrganization.RXRESULTCODE EQ 1>
        <cfset _has_Org = true>
        <cfset orgInfoData = RetVarGetUserOrganization.ORGINFO>
        <cfset logoURL = orgInfoData.OrganizationLogo_vch NEQ '' ? '#_S3IMAGEURL##orgInfoData.OrganizationLogo_vch#' : 'https://s3-us-west-1.amazonaws.com/siremobile/mlp_image/727D1D902034DF3AAA7B0BF758E5A476/cpg-mlp-default-logo.jpg'/>
    <cfelse>
        <cfset logoURL = 'https://s3-us-west-1.amazonaws.com/siremobile/mlp_image/727D1D902034DF3AAA7B0BF758E5A476/cpg-mlp-default-logo.jpg'/>
    </cfif>

    <!--- <cfset hahaha = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", '(<img style="max-height: 100%;" id="campaign-mlp-logo"([^>]+)>)' ) ) />

    <cfset UniSearchMatcher = hahaha.Matcher(JavaCast( "string", RAWCONTENTTOSHOW ) ) /> --->
    <!--- <cfdump var="#RAWCONTENTTOSHOW#" abort="true"/> --->

    <cfset findLogoURL = REFind(logoURL,RAWCONTENTTOSHOW)/>

    <cfif !findLogoURL AND campaignId GT 0 AND GetMLPResult.CPMLPID GT 0>
        <cfset findMLPImageURL = REFind('(<img style="max-height: 100%;" id="campaign-mlp-logo"([^>]+)>)',RAWCONTENTTOSHOW,1,"TRUE")/>
        <!--- <cfdump var="#REFind('(<img style="max-height: 100%;" id="campaign-mlp-logo"([^>]+)>)',RAWCONTENTTOSHOW,1,"TRUE")#" abort="true"/> --->
        <!--- src="([^>]+)"> --->

        <cfset findMLPImageURL = REFind('src="([^>]+)"',RAWCONTENTTOSHOW,1,"TRUE")/>

        <cfset MLPImageURL = Mid(RAWCONTENTTOSHOW, findMLPImageURL.pos[1], findMLPImageURL.len[1]).replace('src="', '')/>
        <cfset MLPImageURL = MLPImageURL.replace('"', '')/>

        <cfif isValid("URL",MLPImageURL)>
            <cfhttp url="#MLPImageURL#" method="get" timeout="3" result="resp">
            </cfhttp>
            <cfset JsonResult="data:image/png;base64,"&ToBase64(resp.fileContent) />
        <cfelse>
            <cfset JsonResult = MLPImageURL />
        </cfif>

        <input id="preload-mlp-img" type="hidden" data-url="<cfoutput>#MLPImageURL#</cfoutput>" name="<cfoutput>#JsonResult#</cfoutput>">

        <!--- <cfdump var="#MLPImageURL#" abort="true"/> --->

    <cfelse>
        <cfhttp  url="#logoURL#" method="get" timeout="3" result="resp">
        </cfhttp>

        <cfscript>
            JsonResult=ToBase64(resp.fileContent);
        </cfscript>

        <input id="preload-mlp-img" type="hidden" data-url="<cfoutput>#logoURL#</cfoutput>" name="data:image/png;base64,<cfoutput>#JsonResult#</cfoutput>">

        <!--- <cfdump var="#MLPImageURL#" abort="true"/> --->
    </cfif>

    <input type="hidden" id="maximagesize" name="" value="<cfoutput>#_IMGMAXSIZE#</cfoutput>">

    <!---<cfset cpNameSectionShow = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH EQ '' ? 0 : 1/>--->
    <cfset cpNameSectionShow = 0/> <!---new logic , not show company name section --->

    <cfset campaignData = {
        BatchId_bi = '',
        KeywordId_int = '',
        Keyword_vch = '',
        Keyword_txt = '',
        Desc_vch = '',
        EMS_Flag_int = 0,
        GroupId_int = 0,
        Schedule_arr = [],
        ControlPoint_arr = [],
        CustomHelpMessage_vch = '',
        CustomStopMessage_vch = '',
        TEMPLATEID = 0,
        OPTIN_GROUPID = 0,
        BLAST_GROUPID = 0,
        ScheduleOption=0,
        TEMPLATETYPE_TI=0
    }>

    <cfset scheduleData = '' />
    <cfset scheduleDateTime = ''/>
    <cfset scheduleTime = ''/>
    <cfset btnSendNowText="Send now">
    <cfset btnBlastNowText="Schedule for Later">
    <cfif campaignBlastScheduleInfo.campaignBlastScheduleInfo GT 0>
        <cfset btnSendNowText="Send Another now">
        <cfset btnBlastNowText="Schedule Another for Later">
    </cfif>

    <!--- MINHHTN HOT FIX --->
    <cfparam name="Session.AdditionalDNC" default="">
    <cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode">
        <cfinvokeargument name="inpBatchId" value="#campaignId#">
    </cfinvoke>

    <input type="hidden" name="" id="ShortcodeForCPMLP" value="<cfoutput>#shortCode.SHORTCODE#</cfoutput>">
    <!--- GET SUBCRIBER LIST --->

    <cfinvoke component="session.sire.models.cfc.subscribers" method="GetSubcriberList" returnvariable="groupList">
        <cfinvokeargument name="inpShortCodeId" value="#shortCode.SHORTCODEID#">
    </cfinvoke>

    <cfif groupList.RXRESULTCODE EQ 1 AND groupList.iTotalRecords GT 0>
        <cfset subcriberList = groupList.listData>
    <cfelse>
        <cfset subcriberList = []/>
    </cfif>
    <cfinvoke method="getTemplateList" component="session.sire.models.cfc.campaign" returnvariable="rxTemplateDetail">
        <cfinvokeargument name="inpTemplateId" value="#TEMPLATEID#"/>
        <cfinvokeargument name="inpCheckActive" value="0"/>
    </cfinvoke>

    <cfloop query="#rxTemplateDetail.TEMPLATELIST#">
        <cfset templateName = replaceNoCase(#Name_vch#, "<Br/>", " ","ALL")>
    </cfloop>
    <cfif selectedTemplateType EQ 1>
        <cfset campaignNameDefault = "Blast to Subscribers on "&LSDateFormat(NOW(), "yyyy-mm-dd")>
    <cfelse>
        <cfset campaignNameDefault = #templateName#&" "&LSDateFormat(NOW(), "yyyy-mm-dd")>
        <cfif countTotalCampaign.TOTALCAMPAIGN GT 0 AND templateid EQ 1>
            <cfset campaignNameDefault = "Opt-In Campaign"&" "&LSDateFormat(NOW(), "yyyy-mm-dd")>
        </cfif>
    </cfif>
    <cfset CDESCHTML=campaignNameDefault>

    <cfif campaignId GT 0>

        <cfif templateId EQ 0>
            <cfset hiddenTemplatePickerClass = ""/>
            <cfset hiddenClass = "hidden"/>
            <cfset action = "Edit"/>
            <cfset actionHeader = "Edit Campaign"/>
            <cfset btnSaveLabel = "FINISHED"/>
        <cfelse>
            <cfset actionHeader = "Create Campaign"/>
            <cfset btnSaveLabel = "FINISHED"/>
        </cfif>


        <cfinvoke method="GetBatchDetails" component="session.sire.models.cfc.control-point" returnvariable="RetVarGetBatchDetails">
            <cfinvokeargument name="inpBatchID" value="#campaignId#">
        </cfinvoke>

        <cfif RetVarGetBatchDetails.RECORDCOUNT LE 0>
            <cflocation url="/session/sire/pages/campaign-manage" addtoken="false">
        </cfif>

        <cfif RetVarGetBatchDetails.TEMPLATEID EQ 0>
            <cflocation url="/session/sire/pages/campaign-edit?campaignId=#campaignId#" addtoken="false">
        </cfif>

        <cfif RetVarGetBatchDetails.RXRESULTCODE GT 0>
            <cfif RetVarGetBatchDetails.SHORTCODEID NEQ shortCode.SHORTCODEID>
                <cflocation url="/session/sire/pages/campaign-manage" addtoken="false"/>
            </cfif>
        </cfif>

        <cfset campaignData.BatchId_bi = RetVarGetBatchDetails.BATCHID>
        <cfset campaignData.Desc_vch = RetVarGetBatchDetails.DESC>
        <cfset campaignData.EMS_Flag_int = RetVarGetBatchDetails.EMSFLAG>
        <cfset campaignData.GroupId_int = RetVarGetBatchDetails.GROUPID>
        <cfset campaignData.MLPID = RetVarGetBatchDetails.MLPID>
        <cfset campaignData.TEMPLATEID = RetVarGetBatchDetails.TEMPLATEID>
        <cfset campaignData.TemplateType_ti =  RetVarGetBatchDetails.TemplateType>
            <cfset campaignData.ScheduleOption =  RetVarGetBatchDetails.ScheduleOption>

        <!--- Preserve possible newlines in display but format all other tags --->
        <!--- Expected HTML output is different for straight div vs text area - Preview, Edit, SWT etc --->
        <!--- Allow newline in text desc messages - reformat for display --->
        <!--- Regular HTML --->
        <cfset CDescHTML = ReplaceNoCase(campaignData.Desc_vch, "newlinex", "<br>", "ALL")>
        <cfset CDescHTML = Replace(CDescHTML, "\n", "<br>", "ALL")>
        <cfset CDescHTML = Replace(CDescHTML, "#chr(10)#", "<br>", "ALL")>
        <cfset CDescHTML = Replace(CDescHTML, "{defaultShortcode}", "#shortCode.SHORTCODE#", "ALL")>
        <!--- Text Area version --->
        <cfset CDescTA = Replace(campaignData.Desc_vch, "\n", "#chr(10)#", "ALL")>
        <cfset CDescTA = Replace(CDescTA, "<br>", "#chr(10)#", "ALL")>
        <cfset CDescTA = Replace(CDescTA, "{defaultShortcode}", "#shortCode.SHORTCODE#", "ALL")>



        <cfinclude template="/session/cfc/csc/constants.cfm">
        <cfquery name="keywordQuery" datasource="#session.DBSourceREAD#">
            SELECT KeywordId_int, Keyword_vch, Created_dt, Active_int, EMSFlag_int
            FROM sms.keyword WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RetVarGetBatchDetails.BATCHID#">
            AND IsDefault_bit = 0
            ORDER BY Created_dt DESC, KeywordId_int DESC
        </cfquery>

        <cfif keywordQuery.RecordCount GT 0 && keywordQuery.Active_int[1] EQ 1>
            <cfset campaignData.KeywordId_int = keywordQuery.KeywordId_int[1]>
            <cfset campaignData.Keyword_vch = keywordQuery.Keyword_vch[1]>
            <cfif keywordQuery.EMSFlag_int[1] EQ 0>
                <cfset campaignData.Keyword_txt = campaignData.Keyword_vch>
            </cfif>
            <cfset campaignData.Keyword_txt =REReplace(HTMLCodeFormat(campaignData.Keyword_txt),'<pre>|</pre>','','all')>
        </cfif>

        <!--- --->
        <cfinvoke method="GetSchedule" component="session.cfc.schedule" returnvariable="ScheduleByBatchId">
            <cfinvokeargument name="INPBATCHID" value="#campaignId#">
        </cfinvoke>

        <cfif ScheduleByBatchId.RXRESULTCODE GT 0>
            <cfloop array="#ScheduleByBatchId.ROWS#" index="ROW">
                <cfset Arrayappend(campaignData.Schedule_arr, ROW)>
            </cfloop>
            <cfset scheduleData = listToArray(campaignData.Schedule_arr[1].START_DT, '-')/>

            <cfset Arrayappend(scheduleData,"#campaignData.Schedule_arr[1].STARTHOUR_TI#")/>
            <cfset Arrayappend(scheduleData,"#campaignData.Schedule_arr[1].STARTMINUTE_TI#")/>
            <cfset Arrayappend(scheduleData,"#campaignData.Schedule_arr[1].ENDHOUR_TI#")/>
            <cfset Arrayappend(scheduleData,"#campaignData.Schedule_arr[1].ENDMINUTE_TI#")/>

            <cfset scheduleDate = dateFormat(campaignData.Schedule_arr[1].START_DT, 'mm/dd/yyyy')>
            <cfset scheduleTime = LSTimeFormat(campaignData.Schedule_arr[1].STARTHOUR_TI&":"&campaignData.Schedule_arr[1].STARTMINUTE_TI, 'hh:mm tt')>
            <cfset scheduleDateTime = scheduleDate&" "& scheduleTime/>

        </cfif>

        <cfparam name="SCHEDULE" default="#campaignData.Schedule_arr#" >
        <!--- GET CUSTOME STOP/HELP --->
        <cfquery name="customMessageQuery" datasource="#session.DBSourceREAD#">
            SELECT * FROM sms.smsshare WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#campaignId#">
        </cfquery>

        <cfloop query="#customMessageQuery#">
            <cfswitch expression="#Keyword_vch#" >
                <cfcase value="HELP">
                    <cfset campaignData.CustomHelpMessage_vch = Content_vch>
                </cfcase>
                <cfcase value="STOP">
                    <cfset campaignData.CustomStopMessage_vch = Content_vch>
                </cfcase>
            </cfswitch>
        </cfloop>



        <cfinvoke method="GetLatestBlastGroup" component="session.sire.models.cfc.campaign" returnvariable="blastList">
            <cfinvokeargument name="inpBatchId" value="#campaignId#"/>
        </cfinvoke>

        <cfif blastList.RXRESULTCODE GT 0 AND blastList.GROUPID GT 0>
            <cfset campaignData.BLAST_GROUPID = blastList.GROUPID/>
        </cfif>

        <cfinvoke method="getTemplateList" component="session.sire.models.cfc.campaign" returnvariable="rxTemplateDetail">
            <cfinvokeargument name="inpTemplateId" value="#campaignData.TEMPLATEID#"/>
            <cfinvokeargument name="inpCheckActive" value="0"/>
        </cfinvoke>

        <cfloop query="#rxTemplateDetail.TEMPLATELIST#">
            <cfset displayOptin = #DisplayOptin_ti#/>
            <cfset displayIntervalType = #DisplayInteval_ti#/>
            <cfset displaySubcriberList = #DisplaySubcriberList_ti#/>
            <cfset templateId = #TID_int#>
            <cfset templateName = replaceNoCase(#Name_vch#, "<Br/>", " ","ALL")>
            <cfset templateType = #Type_ti#/>
            <cfset displayPreview = #DisplayPreview_ti#/>
        </cfloop>

    <cfelseif isNumeric(templateId) AND inpTemplateFlag EQ 0>

        <cfinvoke method="getTemplateList" component="session.sire.models.cfc.campaign" returnvariable="rxTemplateDetail">
            <cfinvokeargument name="inpTemplateId" value="#templateId#"/>
            <cfinvokeargument name="inpCheckActive" value="0"/>
        </cfinvoke>

        <cfloop query="#rxTemplateDetail.TEMPLATELIST#">
            <cfset templateName = replaceNoCase(#Name_vch#, "<Br/>", " ","ALL")>
            <cfset displayOptin = #DisplayOptin_ti#/>
            <cfset displayIntervalType = #DisplayInteval_ti#/>
            <cfset displaySubcriberList = #DisplaySubcriberList_ti#/>
            <cfset templateId = #TID_int#>
            <cfset templateName = replaceNoCase(#Name_vch#, "<Br/>", " ","ALL")>
            <cfset templateType = #Type_ti#/>
            <cfset displayPreview = #DisplayPreview_ti#/>
            <cfset xmlControlString = #XMLControlString_vch#/>

        </cfloop>

        <!--- Create new Batch --->
        <cfif selectedTemplateType EQ 1>
            <cfset campaignNameDefault = "Blast to Subscribers on "&LSDateFormat(NOW(), "yyyy-mm-dd")>
        <cfelse>
            <cfset campaignNameDefault = #templateName#&" "&LSDateFormat(NOW(), "yyyy-mm-dd")>
            <cfif countTotalCampaign.TOTALCAMPAIGN GT 0 AND templateid EQ 1>
                <cfset campaignNameDefault = "Opt-In Campaign"&" "&LSDateFormat(NOW(), "yyyy-mm-dd")>
            </cfif>
        </cfif>

        <cfset campaignData.BatchId_bi = 0>
        <cfset campaignData.Desc_vch = campaignNameDefault>
        <cfset campaignData.TEMPLATEID = templateId>
        <cfset campaignData.TemplateType_ti =  selectedTemplateType>
        <cfset campaignData.ScheduleOption =  "">

        <!--- Preserve possible newlines in display but format all other tags --->
        <!--- Expected HTML output is different for straight div vs text area - Preview, Edit, SWT etc --->
        <!--- Allow newline in text desc messages - reformat for display --->
        <!--- Regular HTML --->
        <cfset CDescHTML = ReplaceNoCase(campaignData.Desc_vch, "newlinex", "<br>", "ALL")>
        <cfset CDescHTML = Replace(CDescHTML, "\n", "<br>", "ALL")>
        <cfset CDescHTML = Replace(CDescHTML, "#chr(10)#", "<br>", "ALL")>
        <cfset CDescHTML = Replace(CDescHTML, "{defaultShortcode}", "#shortCode.SHORTCODE#", "ALL")>
        <!--- Text Area version --->
        <cfset CDescTA = Replace(campaignData.Desc_vch, "\n", "#chr(10)#", "ALL")>
        <cfset CDescTA = Replace(CDescTA, "<br>", "#chr(10)#", "ALL")>
        <cfset CDescTA = Replace(CDescTA, "{defaultShortcode}", "#shortCode.SHORTCODE#", "ALL")>


        <cfset actionHeader = "Create Campaign"/>
        <cfset btnSaveLabel = "Finished"/>
    <cfelse>
        <cflocation url="/session/sire/pages/campaign-manage" addtoken="false">
    </cfif>

<cfoutput>

<!--- campaign template list choice --->
<cfinclude template="campaign-template-list.cfm">
<!--- END campaign template list choice --->
<input value="#shortCode.SHORTCODE#" id="ShortCode" type="hidden" name="ShortCode">

<div>
    <form name="frm-campaign" id="frm-campaign">
    <!---
    <div id="processingPaymentCampaign">
        <img src="/public/sire/images/loading.gif" class="ajax-loader">
    </div>
    --->

    <div class="content-for-first-campaign">
        <cfif templateId EQ 1>
            <cfinclude template="inc_campaign-edit-template-1.cfm">
        <cfelseif  templateId EQ 3>
            <cfinclude template="inc_campaign-edit-template-3.cfm">
        <cfelseif templateId EQ 8>
            <cfinclude template="inc_campaign-edit-template-8.cfm">
        <cfelseif templateId EQ 9>
            <cfinclude template="inc_campaign-edit-template-9.cfm">
        <cfelseif  templateId EQ 11>
            <cfinclude template="inc_campaign-edit-template-11.cfm">
        <cfelse>
            <cfinclude template="inc_campaign-edit-other-0.cfm">
        </cfif>


    </div>
    </form>
<!-- End For first campaign -->
</div>

<!-- START MODAL -->
<div class="modal be-modal fade" id="AddNewSubscriberList" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="frm_subscriber_list">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>
                <div class="modal-body">
                    <h4 class="be-modal-title">Add new subcriber list</h4>
                    <div class="form-gd">
                        <div class="form-group">
                            <label class="label-small-modal" for="">Subscriber List Name: </label>
                            <input id="subscriber_list_name" class="form-control validate[required, custom[onlyLetterNumberSp]]" maxlength="255">
                            <span class="help-block">ex: Happy Hour Promo List</span>
                        </div>
                    </div>
                    <div class="text-right">
                        <button type="button" id="btn-save-group" class="btn green-gd">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal be-modal fade" id="modalCompanyUpdate" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="frCompanyUpdate">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>
                <div class="modal-body">
                    <h4 class="be-modal-title">Update Company Name</h4>
                    <div class="form-gd">
                        <div class="form-group">
                           <p>
                            Phone carriers require all business messages to be easily identifiable to the company who sends them. Your company name will appear before your message.
                           </p>
                           <input type="text" class="form-control validate[required,custom[noHTML]]" name="txtCompanyUpdate" id="txtCompanyUpdate" maxlength="250" data-prompt-position="topLeft:100" placeholder="Enter Company Name" value="#RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH#">
                           <br>
                        </div>
                    </div>
                    <div class="text-right">
                        <button type="submit" id="btn-save-company" class="btn green-gd">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal rename template -->
<div class="modal be-modal fade" id="edit-cp-text" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="edit-cp-text-form" name="edit-cp-text-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <h3 class="modal-title">EDIT MESSAGE</h3>
                    <div class="form-group">
                        <label>Message:</label>
                        <textarea class="form-control no-resize validate[required,maxSize[1000]]" id="message-content" name="message-content" rows="10" data-prompt-position="topLeft:100"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn-save btn btn-success-custom" >Save</button>
                    <button type="button" class="btn btn-back-custom" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END MODAL -->
<!-- Modal -->
<div class="modal fade" id="ConfirmLaunchModal" tabindex="-1" role="dialog" aria-labelledby="ConfirmLaunchModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Confirm Launch Campaign</h4>
            </div>
            <div class="modal-body clearfix">

                <div id="ProcessingBlast" style="display: none; min-height: 150px;">
                    <h4>Your blast is loading into queue now:</h4>
                    <img src="/public/sire/images/loading.gif" class="ajax-loader">
                </div>

                <!--- Confirmation Display --->
                <div class="ConfirmationSummary">
                    <div class="form-group clearfix">
                        <div class="col-xs-9 col-sm-6">Number of Subscribers:</div>
                        <div class="col-xs-3 col-sm-6" id="confirm-number-subscribers">0</div>
                    </div>

                    <div class="form-group clearfix">
                        <div class="col-xs-9 col-sm-6">Already Blasted this Campaign:</div>
                        <div class="col-xs-3 col-sm-6" id="confirm-list-duplicates">0</div>
                    </div>

                    <div class="form-group clearfix">
                        <div class="col-xs-9 col-sm-6">Total Eligible:</div>
                        <div class="col-xs-3 col-sm-6" id="confirm-total-elligable">0</div>
                    </div>

                    <div class="form-group clearfix">
                        <div class="col-xs-9 col-sm-6">Estimated Credits:</div> <!---Credits Needed--->
                        <div class="col-xs-3 col-sm-6" id="confirm-credits-needed">0</div>
                    </div>

                    <div class="form-group clearfix">
                        <div class="col-xs-9 col-sm-6">Credits Available:</div>
                        <div class="col-xs-3 col-sm-6" id="confirm-credits-available">0</div>
                    </div>

                    <div id="confirm-launch-alert-text" class="clearfix" style="display: none;">
                        <div class="col-xs-12 col-sm-12">
                            <!--- /session/sire/pages/buy-sms --->
                            <div class="alert alert-info" role="alert">
                                You do not have enough credits to launch this campaign.<br>
                                Please <a id="confirm-launch-buy-credits" href="/session/sire/pages/my-plan?active=addon" target="_blank">click here</a> to buy more credits.
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="modal-footer ConfirmationSummary">
                <button type="button" class="btn green-gd btn-finished" id="btn-confirm-save-and-send">Confirm Blast</button>
                &nbsp;
                <button type="button" class="btn green-cancel" id="btn-cancel-send" data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!--- Image Chooser --->
<div class="bootbox modal fade" id="ImageChooserModal" tabindex="-1" role="dialog" aria-labelledby="ImageChooserModal" aria-hidden="true" style="">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Choose Image</h4>
            </div>
            <div class="modal-body">

                <div class="tabbable">
                    <ul class="nav nav-tabs" id="myTabs">
                        <li class="active"><a href="##MLPUploadImage"  class="active" data-toggle="tab">Upload New Image</a></li>
                        <li><a href="/session/sire/models/cfm/imagebrowser-mlp1.cfm" data-toggle="MLPImagePicker" data-target="##MLPImagePicker">Your Image Library</a></li>
                        <li><a href="##MLPImageLink"  class="active" data-toggle="tab3">External Link</a></li>
                    </ul>

                    <div class="tab-content">

                        <div class="tab-pane active" id="MLPUploadImage" style="min-height: 10em;">

                            <div class="row>">
                                <div class="MLPDropZone" style="xmin-height:2em; text-align:center; line-height:6em; border:dashed 1px ##ccc; padding-bottom: 3em;" contenteditable="false">Drop files here or click to upload.



                                </div>
                            </div>

                            <cfinvoke method="GetUserStorageInfo" component="session.sire.models.cfc.image" returnvariable="retValUserUsage">
                            </cfinvoke>

                            <cfif retValUserUsage.RXRESULTCODE GT 0>

                                <cfset usagePercent = DecimalFormat((retValUserUsage.USERUSAGE/retValUserUsage.USERSTORAGEPLAN)*100) />

                                <cfset UsageMBSize = retValUserUsage.USERUSAGE / 1048576>
                                <cfset StoragePlanMBSize = retValUserUsage.USERSTORAGEPLAN / 1048576>

                                <cfif UsageMBSize GT 1024>
                                    <cfset UsageMBSize = DecimalFormat(UsageMBSize / 1024)>
                                    <cfset UsageMBSize = UsageMBSize&"GB">
                                <cfelse>
                                    <cfset UsageMBSize = DecimalFormat(UsageMBSize)&"MB">
                                </cfif>

                                <cfif StoragePlanMBSize GT 1024>
                                    <cfset StoragePlanMBSize = DecimalFormat(StoragePlanMBSize / 1024)>
                                    <cfset StoragePlanMBSize = StoragePlanMBSize&"GB">
                                <cfelse>
                                    <cfset StoragePlanMBSize = DecimalFormat(StoragePlanMBSize)&"MB">
                                </cfif>

                                <cfoutput>

                                    <input type="hidden" id="inpUserUsage" value="#retValUserUsage.USERUSAGE#">
                                    <input type="hidden" id="inpUserStoragePlan" value="#retValUserUsage.USERSTORAGEPLAN#">

                                    <div class="row" style="padding-top:20px">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="">
                                                Account Storage Usage
                                            </div>
                                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                <b><span id="userUsageSpan">#UsageMBSize#</span> / <span id="userStoragePlanSpan">#StoragePlanMBSize#</span></b>
                                            </div>
                                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-striped progress-bar-success" role="progressbar" id="usageProgress"
                                                        aria-valuemin="0" aria-valuemax="100" style="width:#usagePercent#%">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </cfoutput>
                            </cfif>

                        </div>

                        <div class="tab-pane" id="MLPImagePicker"></div>

                        <div class="tab-pane" id="MLPImageLink"  style="min-height: 10em;">

                            <div class="MLPDropZone" style="min-height:2em; text-align:left; line-height:1em;" contenteditable="false">Image Link (URL)</div>
                            <input type="text" id="MLPImageURL" value="" style="width: 100%; height: 1.5em;" />

                        </div>

                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="mlp-btn" id="btn-upload-image">Use This Image</button>
                <button type="button" class="mlp-btn" id="btn-use-image-link">Use Specified Link</button>
                &nbsp; &nbsp; &nbsp;
                <button type="button" class="mlp-btn" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>

</div>

<cfsavecontent variable="AnswerListItemTemplate">
    <cfoutput>

        <div class="AnswerItem row row-small">
            <div class="col-xs-8 col-sm-10">
                <div class="form-group">
                    <input id="OPTION" type="text" value="Not useful" class="form-control">
                </div>
            </div>

            <div class="col-xs-2 col-sm-1">
                <div class="form-group">
                    <input type="text" id="AVAL" class="form-control readonly-val text-center" value="1">
                    <input type="hidden" id="AVALREG" class="form-control readonly-val text-center" value="">
                </div>
            </div>
            <div class="col-xs-2 col-sm-1">
                <div class="form-group">
                    <button class="form-control delete-answer">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
        </div>
    </cfoutput>
</cfsavecontent>

<cfinclude template="inc_dashboard_slide_integrate.cfm">

<script type="text/javascript">
    var OPTIN_GROUPID = "#campaignData.OPTIN_GROUPID#";
    var BLAST_GROUPID = "#campaignData.BLAST_GROUPID#";
    var GroupsListBox = null;

    <!--- Set some globally avaialble javascript vars based on current data --->
    var schedule_data = "";
    var SHORTCODE = "#shortCode.SHORTCODE#";

    var date_txt_1 = '#LSDateFormat(DateAdd("d", 30, Now()), "m-d-yyyy")#';
    var date_txt_2 = '#LSDateFormat(NOW(), "m-d-yyyy")#';

    var ScheduleTimeData;

    <!--- Set Global var for inpBatchId for Preview .js --->
    var inpBatchId = #campaignData.BatchId_bi#;
    var inpBatchName = '#campaignData.Desc_vch#';

    var action = '#action#';
    var #toScript(AnswerListItemTemplate, "AnswerItemjsVar")#;


    var inpCampaignType = #campaignData.TemplateType_ti#;
    var inpTemplateId = #templateId#;
    var inpCampaignId = #campaignId#;
    var inpTemplateType=#selectedTemplateType#;
    var countCPOneSelection = #countCPOneSelection#;
    var currentCPOneSelection = 1;

    var intListDeleteRID = [];
    var tempListDeleteRID = [];

    var checkClickPreview = 0;

    <cfif templateId EQ 11>
        var selectorCP = "data-template-id="+inpTemplateId+"";
    <cfelse>
        var selectorCP = "data-control-point-type='ONESELECTION'";
    </cfif>

    var subcriberList = #SerializeJSON(subcriberList)#

    var displayPreview = #displayPreview#

    var arrCpBranch = #SerializeJSON(arrCpBranch)#
    var arrCpOptIn = #SerializeJSON(arrCpOptIn)#

    var arrCpShortAns = #SerializeJSON(arrCpShortAns)#
    var arrCpTrailer = #SerializeJSON(arrCpTrailer)#

    var arrCpInterval = #SerializeJSON(arrCpInterval)#
    var arrCpStatement = #SerializeJSON(arrCpStatement)#


    var cpOptIn = #cpOptIn#

    var mlpLink =   "";
    var shareUrl =  "";
    var hostname = ("https://"+window.location.hostname);

    var clientId = '#ClientId#'; // Is  App ID of facebook Graph
    var clientSecret = '#ClientSecret#'; // Is App Secret of facebook Graph

</script>

</cfoutput>

<!--- <cfif action EQ "CreateNew">
    <input type="hidden" id="currentStep" value="1">
    <input type="hidden" id="totalStep" value="1">
    <cfsavecontent variable="variables.portleft">
        <div class="portlet light bordered box-widget-sidebar">
            <div class="portlet-body non-pt">
                <div class="wrap-dial-step">
                    <h4 class="stt-step">Progress</h4>
                    <div class="step-holder">
                        <canvas id="myStep" class="mauto" width="200" height="200"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </cfsavecontent>
</cfif> --->

<cfinclude template="../views/commons/inc_carousel.cfm">

<cfif action EQ 'Edit'>
    <cfparam name="variables._title" default="Edit Campaign - Sire">
<cfelse>
    <cfparam name="variables._title" default="Campaign Create - Sire">
</cfif>

<style type="text/css">
    .campaign-mlp{
        margin-left: 0px !important;
    }

    .campaign-mlp{
        min-width: 273px !important;
    }


    @media screen and (max-width: 375px){
            .campaign-mlp{
            width: 273px !important;
        }
    }

    @media screen and (min-width: 376px) and (max-width: 480px){
        .campaign-mlp{
            width: 312px !important;
        }
    }


    .campaign-mlp-header img{
        max-height: 100% !important;
    }

    /*.popline{
        width: 320px;
    }*/

    .popline li{
        padding-right: 12px;
    }

    .popline .popline-color2-button{
        padding-right: 0px;
    }

    .popline .popline-blockFormat-button{
        padding-right: 0px;
    }

    .popline-textfield{
        color: black;
    }

    #campaign-mlp-logo{
        cursor: pointer;
    }

    .mlp-btn {
        display: inline-block;
        vertical-align: middle;
        padding: 10px 24px;
        border-radius: 15em;
        border: 1px solid;
        margin: 2px;
        min-width: 60px;
        font-size: .875rem;
        text-align: center;
        outline: 0;
        line-height: 1;
        cursor: pointer;
        color: #FFF;
        background-color: #74c37f;
        border-color: #74c37f;
        text-transform: uppercase;
        font-weight: 700;
    }

    .dz-success-mark{
        display: none;
    }

    .dz-error-mark{
        display: none;
    }
    <cfoutput>#RetCPPXData.CUSTOMCSS#</cfoutput>

</style>

<cfinclude template="../views/layouts/master-blast.cfm">
