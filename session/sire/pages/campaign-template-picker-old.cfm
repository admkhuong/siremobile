<!--- Advanced user and Admin options --->
<cfparam name="adv" default="0">
<cfparam name="inpTemplateFlag" default="0">
<cfparam name="createSubcriber" default="0">
<cfparam name="selectTemplate" default="0">

<!--- Used to keep code in synce but make available for public browsing of samples --->
<cfparam name="PreviewOnly" default="0">

<!--- Allow showing everthing by default --->
<cfparam name="ShowAll" default="0">

<cfinclude template="../../sire/configs/paths.cfm">

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/campaign.css">
</cfinvoke>


<style>
	
	<!--- Hide more advanced features from users in preview mode --->	
	.BuilderOption
	{
		<cfif adv EQ 0>
			display: none;  
		</cfif>   
	}  
  
	.cp-obj-header
	{
		 background-color: #578ca5;
		 color: #fafafa; 
		 border-radius: 6px 6px 0 0; 
	}
	
	.btn-success-custom, .btn-success-custom:hover, .btn-success-custom:focus, .btn-success-custom:active, .btn-success-custom.active, .btn-success-custom[disabled], .btn-group.open .btn-success-custom.dropdown-toggle 
	{
		font-family: 'Source Sans Pro', sans-serif;
		border-style: solid;
		border-width: 1px;
		border-color: rgb( 92, 180, 105 );
		/* border-radius: 5px; */
		background-color: #74c37f !important;
		background-image: inherit !important;
		background-position: inherit !important;
		box-shadow: 0.418px 2.971px 0px 0px rgb( 61, 152, 74 );
		color: #fff;
		text-transform: none;
		font-size: .8em;
	}
	
	.btn-success-custom:hover
	{
		background-color: #eeeeee !important;
		color: #000000;
		border-color: #ffffff;
		box-shadow: 0.418px 2.971px 0px 0px #cccccc;
		
	}
  
  	.btn-lg, .btn-group-lg > .btn 
  	{
	    padding: 8px 6px;
	    font-size: .8em;
	    line-height: .2em;
	    border-radius: 6px;
	}
	
	.item_action .template-desc {
	    text-align: left;
	    font-size: .8em;
	    margin-left: .8em;
	}
	
	.top-folder .content {
	    position: relative;
	    overflow: hidden;	    
		background-repeat: no-repeat;
		background-position: 50% 90%;
	}

	.top-folder .content {
	    background-color: #274e60 !important;
	    padding: 25px 15px 15px 15px !important;
	    border-radius: 10px;
	    border: 3px solid #eee;
	    height: 90px !important;
	    word-wrap: break-word;
	    display: block;
	    text-decoration: none;
	    color: #fff !important;
	}

	.top-folder .content:hover 
	{
		cursor: pointer;	
	}
	
	.top-folder .border-active
	{	   

	}
	
	.top-folder .border-active:hover
	{	    

	}
	
	.ShowMore
	{
		position: absolute;
		right: .5em;
		top: .5em;
		border: 1px solid #274e60;
		color: #274e60;
		height: 1em;
		line-height: .9em;
		text-align: center;
		padding: 0 .2em;	
		border-radius: 3px;	
		background-color: #fff !important;
	}
	
	.ShowLess
	{
		position: absolute;
		right: .5em;
		top: .5em;
		border: 1px solid #274e60;
		color: #274e60;
		height: 1em;
		line-height: .8em;
		text-align: center;
		padding: 0 .3em;	
		border-radius: 3px;	
		background-color: #fff !important;
	}


	.border-active:hover .ShowMore, .border-active:hover .ShowLess
	{		
		border: 1px solid #777;
		font-weight: bold;		
	}
	
	.top-folder .content:hover  .ShowMore, .top-folder .content:hover  .ShowLess
	{		
		border: 1px solid #777;		
		font-weight: bold;
	}
	
	.content-header
	{
		min-height:auto;
	}
	
	.TemplatePickerContainer
	{
		padding-right: 2em;
	}
		
	.template-slideout
	{
		position: fixed;
		padding: 12px 0;
	    text-align: center;
	    background: #74c37f;
	    -webkit-transition-duration: 0.3s;
	    -moz-transition-duration: 0.3s;
	    transition-duration: 0.3s;
	    -o-transition-duration: 0.3s;
	    -webkit-border-radius: 5px 0 0 5px;
	    -moz-border-radius: 5px 0 0 5px;
	    border-radius: 5px 0 0 5px;
        color: #fff;
        z-index: 1000;
	    text-align: center;
	    		
	}
	.template-slideout-inner
	{
		position: fixed;	    
	    background:#ffffff;
	    border: solid 2px #74c37f;	    
	    -webkit-transition-duration: 0.3s;
	    -moz-transition-duration: 0.3s;
	    transition-duration: 0.3s;
	    -o-transition-duration: 0.3s;
	    text-align: left;
	    -webkit-border-radius: 0 0 0 5px;
	    -moz-border-radius: 0 0 0 5px;
	    border-radius: 0 0 0 5px;
		z-index: 1000;		
		color: #5c5c5c;
	}
	
	.template-slideout:hover
	{
		cursor: pointer;			
	}
	
	.template-slideout-inner:hover
	{
		cursor:default;			
	}
	
	<!--- Position Social --->
	#template-slideout 
	{	    
	    top: 143px;
	    right: 0;
	    width: 35px;	 
	    line-height: 30px;   
	}
			
	#template-slideout-inner 
	{
	    top: 143px;
	    right: -350px;
	    width: 350px;
	    padding: 0;
	    padding-bottom:2em;
	    line-height: 1.4em;
	}
		
	.rotated-text {
	    display: inline-block;
	    overflow: hidden;
	    width: 30px;
	    font-size: 18px;
	}
	.rotated-text-inner {
	    display: inline-block;
	    white-space: nowrap;
	 
	    -webkit-transform: rotate(-90deg);
	       -moz-transform: rotate(-90deg);
	         -o-transform: rotate(-90deg);
	            transform: rotate(-90deg);

	   /* IE9+ */
	   -ms-transform: none;
	   -ms-transform-origin: none;
	   /* IE8+ */
	   -ms-writing-mode: tb-lr;
	   /* IE7 and below */
	   *writing-mode: tb-lr;

	}
	.rotated-text-inner:before {
	    content: "";
	    float: left;
	    margin-top: 100%;
	}
	
	
	.template-picker-sub-menu
	{	
		background-color: #578ca5;
		border-color: #578ca5;  
		border-radius:0;   
		color: #fafafa;
		border-bottom-color: #fff;	
		margin-bottom: 10px;	
    }
    
    .template-picker-sub-menu .nav > li > a:hover, .template-picker-sub-menu .nav > li > a:focus {
	    background-color: #274e60;
	    border-color: #337ab7;
	    border-radius: 3px 3px 0 0;
	 }
	    
    .template-picker-sub-menu .nav .open > a, .template-picker-sub-menu .nav .open > a:hover, .template-picker-sub-menu .nav .open > a:focus {
	    background-color: #274e60;
	    border-color: #337ab7;
	    border-radius: 3px 3px 0 0;
	}
		

	.createCampaignModal .img-responsive
	{
		height: 25px;
		display: inline;
		margin-right: .5em;		
	}
	
	<!--- This makes the meu close and re-open the next on IOS touch - need to verify on android. --->
	<!--- http://stackoverflow.com/questions/13902734/bootstrap-cant-click-dropdown-child-menu-items-on-mobile-and-first-click-not  --->
	.dropdown-backdrop 
	{
	  position: static;
	}
	
	.content-header 
	{    	
		padding: 0 15px 0 15px;
	}

	.content-body
	{
		padding: 0 15px;
	}
	
	.page
	{
		padding-top: 0;
		padding-bottom: 0;
	}

	@media only screen and (max-width: 480px){
   .template-cat{
     font-size:14px !important; 
     letter-spacing: 1px !important; 
     font-weight: 200 !important; 
     padding-left: 5px !important;
     padding-right: 5px !important;
   }

   .template-picker-sub-menu{
     padding-left: 32px;
   }

   .template-cat .dropdown-menu{
     width: 300px !important;
   }
 }

 @media only screen and (max-width: 768px){
  #ShowStarterTemplates{
   text-align: center !important;
  }

  #ShowAllTemplates{
   text-align: center !important;
  }
 }

 @media only screen and (min-width: 769px){
  #ShowStarterTemplates-div{
   text-align: right !important;
  }

  #ShowAllTemplates-div{
   text-align: left !important;
  }
 }

 @media only screen and (max-width: 480px){
	  #developer-tab{
	   left: -13em !important;
	   right: -15px !important;
	  }

    #template-slideout-inner{
	   right: -270px;
	      width: 270px;
	  }

    .template-picker-sub-menu{
	    padding-left: 15px !important;
	  }
	  
 }

		
</style>

<!--- Only allow template selectiong for editing by Admins  --->
<cfif inpTemplateFlag EQ 1>
  <cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />
</cfif>

<cfoutput>
		    		    
<main class="container-fluid page template-container" style="padding-top: 0;">	
		
	<cfif PreviewOnly EQ 0>

		<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
		    <cfinvokeargument name="path" value="/session/sire/js/template_request.js">
		</cfinvoke>
		
		<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfoById" returnvariable="userInfo">
		  	<cfinvokeargument name="inpUserId" value="#Session.USERID#">
		</cfinvoke>
		
		<div id="template-slideout" class="template-slideout"> 
	
		    <div class="rotated-text"><span class="rotated-text-inner">Request Custom Template</div>
		
		</div>
		    
	    <div id="template-slideout-inner" class="template-slideout-inner">
	        
	        <div class="container-fluid">
		        
		        <div class="row">
		        
		        	<!--- Offer user ability to request a custom template --->			            	
					
					  <div class="modal-body">
		                
		                <h4 class="modal-title" id="template-request-form-title">Custom Order a Template</></h4>
		                <form id="template-request-form">
		                  <div class="form-group">
			                  
			                <h5>Don't see a template that you like? <br/>Let us build one for you!</h5>
			                  
		                    <input type="hidden" name="user-id" id="user-id" value="<cfoutput>#Session.USERID#</cfoutput>">
		                    <input type="hidden" name="user-contactstring" id="user-contactstring" value="<cfoutput>#userInfo.MFAPHONE#</cfoutput>">
		                    <input type="hidden" name="user-firstname" id="user-firstname" value="<cfoutput>#Session.FIRSTNAME#</cfoutput>">
		                    <input type="hidden" name="user-email" id="user-email" value="<cfoutput>#Session.USERNAME#</cfoutput>">
		                    <input type="hidden" name="user-fullname" id="user-fullname" value="<cfoutput>#Session.FULLNAME#</cfoutput>">
		                    <h5 style="">What is the purpose of the template? Please describe what you are looking for.</h5>
		                    <textarea class="form-control validate[required,maxSize[130],custom[noHTML]]" id="template-purpose" rows="4" placeholder="" data-prompt-position="topLeft:100" style="width: 90%"></textarea>
		                    </div>
		                    
		                    <div class="row">
		                      <button type="submit" class="btn btn-success-custom" id="submit-template-request" style="float:right">Submit</button>
		                      <button type="button" class="btn btn-back-custom" id="cancel-template-request" data-dismiss="modal" style="float:right">Cancel</button>
		                    </div>
		                    
		                  </div>
		                  <!--- <div class="form-group" style="float:right">
		                    <button type="button" class="btn btn-primary-custom" id="cancel-template-request" data-dismiss="modal">Cancel</button>
		                    <button type="submit" class="btn btn-success-custom" id="submit-template-request">Submit</button>
		                    
		                  </div> --->
		                
		                </form>
		              </div>
	
								
		        </div>				
	                
	        </div>
	        
	    </div>

    </cfif>
    
	<section class="bg-white">
	    
	    <div class="content-header">
	    	<div class="row"> 
		        <div class="navbar template-picker-sub-menu col-sm-12 col-md-12 col-lg-12">
			        <div class="container-fluid"><!-- nav /.container-fluid -->
						<div class="navbar-header pull-left">
<!--- 							<a class="navbar-brand hidden-xs ShowStarterTemplates" href="##" style="color:##fafafa;">Template Categories</a> --->
								
							<!--- Place holder to save all sub category templates content in --->
							<cfsavecontent variable="AllSubCategoriesListTemplates"></cfsavecontent>
		
						 	<!--- Read in each Category besides Basic --->
							<cfinvoke component="session.sire.models.cfc.control-point" method="GetTemplateCategoriesList" returnvariable="RetVarGetTemplateCategoriesList"></cfinvoke>
							
							<div class="btn-group">
								
								<cfloop query="RetVarGetTemplateCategoriesList.QUERYRES">
									
									<ul class="nav pull-left">
										
										<!--- Pull the last menu to from the right so it does not go off display --->
										<cfif RetVarGetTemplateCategoriesList.QUERYRES.CGID_int EQ 3>
											<li class="dropdown pull-right">
										<cfelse>
											<li class="dropdown pull-left">
										</cfif>							
										
											<a href="##" data-toggle="dropdown" style="color:##fafafa; margin-top: 5px; text-transform:uppercase; font-size:1em; letter-spacing: 2px; font-weight: 500;" class="dropdown-toggle template-cat">#RetVarGetTemplateCategoriesList.QUERYRES.CGNAME_vch#<b class="caret"></b></a>
										 						
											<cfif RetVarGetTemplateCategoriesList.QUERYRES.CGID_int EQ 3>     
									            <ul class="dropdown-menu" id="developer-tab">
									           <cfelse>
									            <ul class="dropdown-menu">
									           </cfif>
												
												<!--- Read in the current sub categroies --->	
												<cfinvoke component="session.sire.models.cfc.control-point" method="GetTemplateCategoriesSubCategoriesList" returnvariable="RetVarGetTemplateCategoriesSubCategoriesList">
													 <cfinvokeargument name="inpCategoryId" value="#RetVarGetTemplateCategoriesList.QUERYRES.CGID_int#">
												</cfinvoke>
																	                            	                   
												<cfloop query="RetVarGetTemplateCategoriesSubCategoriesList.QUERYRES">
													<li>
														<a class="dropdown-item SubCategory" href="##" rel="#RetVarGetTemplateCategoriesSubCategoriesList.QUERYRES.CID_int#" rel2="#RetVarGetTemplateCategoriesList.QUERYRES.CGID_int#" style="letter-spacing: 1px;">#RetVarGetTemplateCategoriesSubCategoriesList.QUERYRES.DisplayName_vch#</a>
													</li>											
													
													<!--- Generate content here while we build the menu so we dont need to loop or run queries twice - store results in a variable and then append to display variable for use later on --->
													<cfsavecontent variable="SubCategoriesListTemplate">												
														
														<!--- Initially displayed as hidden - will use same rel and rel2 to match to selected sub category menu option ---> 
														<div class="createCampaignModal" rel="#RetVarGetTemplateCategoriesSubCategoriesList.QUERYRES.CID_int#" rel2="#RetVarGetTemplateCategoriesList.QUERYRES.CGID_int#" style="display: none;">
														
															<div class="col-md-12">
																			
																<div class="container-fluid TemplatePickerContainer">
																						
												            		<div class="row">	
						            	
																		<!--- Read in al the templates for this sub category --->
																		<cfinvoke component="session.sire.models.cfc.control-point" method="GetCampaignSubCategoryTemplateList" returnvariable="RetVarGetCampaignSubCategoryTemplateList">
																			 <cfinvokeargument name="inpCategoryId" value="#RetVarGetTemplateCategoriesList.QUERYRES.CGID_int#">
																			 <cfinvokeargument name="inpSubCategoryId" value="#RetVarGetTemplateCategoriesSubCategoriesList.QUERYRES.CID_int#">
																		</cfinvoke>
						 					                            
						 					                            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8" style="padding-top: 5px; padding-left: 25px;" >
													                            <h4><img class="img-responsive" src="#RetVarGetTemplateCategoriesSubCategoriesList.QUERYRES.ImgSource_vch#" alt="" /> <b>#RetVarGetTemplateCategoriesList.QUERYRES.CGNAME_vch# <b style="font-size: .8em;" class="glyphicon glyphicon-arrow-right"></b> #RetVarGetTemplateCategoriesSubCategoriesList.QUERYRES.DisplayName_vch#</b></h4>
<!--- 													                            #RetVarGetTemplateCategoriesSubCategoriesList.QUERYRES.Description_txt# --->
													                    </div>    
											                            
											                            <!--- Only show this version of the div on xs screens - adds extra padding on small devices --->
											                            <div style="clear: both; padding-bottom: 1em;" class="visible-xs"></div>									                            	                            
																		<div style="clear: both;" class="hidden-xs"></div> 		
																			
												                    	<cfloop query="RetVarGetCampaignSubCategoryTemplateList.QUERYRES">
												                        	<cfif Len(RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Name_vch) GT 150>
												                                <cfset displayName = Left(RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Name_vch, 150) & ' ...'>
												                                <cfelse>
												                                <cfset displayName = RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Name_vch>                              
												                            </cfif>
												
												
																			<cfset RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Description_vch = Replace(RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Description_vch, "\n", "<br>", "ALL")>
																			<cfset RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Description_vch = Replace(RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Description_vch, "#chr(10)#", "<br>", "ALL")>
								
												                            <cfif Len(RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Description_vch) GT 80>
												                                <cfset displayDesc = Left(RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Description_vch, 80) & '<i title= "Read more..." data-description="' & htmlEditFormat(RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Description_vch) & '" class="glyphicon glyphicon-plus read-more-description"></i>'>
												                                <cfelse>
												                                <cfset displayDesc = htmlEditFormat(RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Description_vch)>                              
												                            </cfif>
												                             
												                            <cfset arrCatID = listToArray(RetVarGetCampaignSubCategoryTemplateList.QUERYRES.CatListID, ',') />
												                            
												                            <div class="col-sm-4 col-md-3 col-lg-2 <cfloop array="#arrCatID#" index="catID" > cat-id-#catID#</cfloop> item-grid cat-class-#RetVarGetCampaignSubCategoryTemplateList.QUERYRES.CatListID# cat-class-all cat-group-#RetVarGetCampaignSubCategoryTemplateList.QUERYRES.GroupID_int#">
												                              
												                            	<div id="templateid-#RetVarGetCampaignSubCategoryTemplateList.QUERYRES.TID_int#" href='/session/sire/pages/campaign-edit?templateid=#RetVarGetCampaignSubCategoryTemplateList.QUERYRES.TID_int#&templateType=#RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Type_int#<cfif ADV EQ 1>&ADV=1</cfif><cfif inpTemplateFlag EQ 1>&inpTemplateFlag=1</cfif>' class="content TemplatePickerImage" <cfif RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Image_vch NEQ ''>style="background-image: url(../../../session/sire/images/template-picker/#RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Image_vch#);"</cfif>> 
												                            
												                                	<p class="template-name">#displayName#</p>                                  
												                                  
																					<div class="item_action">
																						<p class="template-name">#RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Name_vch#</p>
													                                    <div class="select_group_btn">
													                                      <cfif PreviewOnly EQ 0><button type="button" class="btn btn-success-custom btn-lg btn-select-campaign" >Select</button></cfif>
													                                      <button type="button" class="btn btn-success-custom btn-lg btn-preview-campaign" data-template-id = '#RetVarGetCampaignSubCategoryTemplateList.QUERYRES.TID_int#' >Preview</button>
													                                    </div>  
													                                    <p class="template-desc"><a href="##"> <cfoutput>#displayDesc#</cfoutput> </a> </p>
																					</div>
												
												                            	</div>
												                            </div>
												
												                        </cfloop>
									                        		
									                        		</div>
									                          
									                        	</div>
									                        	
															</div>
														
															<div style="clear: both; padding-bottom: 1em;" class="Xvisible-xs"></div>	
														
														</div>
																																																	
													</cfsavecontent>											
													
													<!--- Append to final display variable  --->
													<cfset AllSubCategoriesListTemplates = AllSubCategoriesListTemplates & SubCategoriesListTemplate />
													
												</cfloop>									
											
											</ul>
										
										</li>
									</ul>
																
								</cfloop>
							</div>
				        </div>
	        		</div><!-- nav /.container-fluid -->
	        	</div>    
			</div>
	    </div>
    
	    <div class="content-body">
		    				
	        <div class="col-md-12">		
							
				<!--- Read in the Basic Catagries --->
				<cfinvoke component="session.sire.models.cfc.control-point" method="GetBasicCampaignTemplateList" returnvariable="RetVarBasicTemplates"></cfinvoke>
															
				<div class="createCampaignModal StarterTemplates" style=" display: none;">
					<!--- Top Section --->
					<!--- New common starter templates - top section 3? 4? DB controlled --->
													
					<div class="container-fluid TemplatePickerContainer">
											                            
		            	<div class="row">		                			         		         								                
						
							<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8" style="padding-top: 5px; padding-left: 25px;" >
	                           <h4><img class="img-responsive" src="/public/sire/images/learning/ReadyToUse.png" alt="" /> <b>Starter Templates</b></h4>
							   Basic Templates for new users. You may also select a template category from the menus above to see even more templates.
							</div>    
							
                            <!--- Only show this version of the div on xs screens - adds extra padding on small devices --->
                            <div style="clear: both; padding-bottom: 1em;" class="visible-xs"></div>									                            	                            
							<div style="clear: both;" class="hidden-xs"></div> 	

							 
	                    	<cfloop query="RetVarBasicTemplates.QUERYRES">
	                        	<cfif Len(RetVarBasicTemplates.QUERYRES.Name_vch) GT 150>
	                                <cfset displayName = Left(RetVarBasicTemplates.QUERYRES.Name_vch, 150) & ' ...'>
	                                <cfelse>
	                                <cfset displayName = RetVarBasicTemplates.QUERYRES.Name_vch>                              
	                            </cfif>
	                            	                            
								<cfset RetVarBasicTemplates.QUERYRES.Description_vch = Replace(RetVarBasicTemplates.QUERYRES.Description_vch, "\n", "<br>", "ALL")>
								<cfset RetVarBasicTemplates.QUERYRES.Description_vch = Replace(RetVarBasicTemplates.QUERYRES.Description_vch, "#chr(10)#", "<br>", "ALL")>
								
	                            <cfif Len(RetVarBasicTemplates.QUERYRES.Description_vch) GT 80>
	                                <cfset displayDesc = Left(RetVarBasicTemplates.QUERYRES.Description_vch, 80) & '<i title= "Read more..." data-description="' & htmlEditFormat(RetVarBasicTemplates.QUERYRES.Description_vch) & '" class="glyphicon glyphicon-plus read-more-description"></i>'>
	                                <cfelse>
	                                <cfset displayDesc = htmlEditFormat(RetVarBasicTemplates.QUERYRES.Description_vch)>                              
	                            </cfif>
	                             
	                            <cfset arrCatID = listToArray(RetVarBasicTemplates.QUERYRES.CatListID, ',') />
	                              
	                            <div class="col-sm-4 col-md-3 col-lg-2 <cfloop array="#arrCatID#" index="catID" > cat-id-#catID#</cfloop> item-grid cat-class-#RetVarBasicTemplates.QUERYRES.CatListID# cat-class-all cat-group-#RetVarBasicTemplates.QUERYRES.GroupID_int#">
	                              
	                            	<div id="templateid-#RetVarBasicTemplates.QUERYRES.TID_int#" href='/session/sire/pages/campaign-edit?templateid=#RetVarBasicTemplates.QUERYRES.TID_int#&templateType=#RetVarBasicTemplates.QUERYRES.Type_int#<cfif ADV EQ 1>&ADV=1</cfif><cfif inpTemplateFlag EQ 1>&inpTemplateFlag=1</cfif>' class="content TemplatePickerImage" <cfif RetVarBasicTemplates.QUERYRES.Image_vch NEQ ''>style="background-image: url(../../../session/sire/images/template-picker/#RetVarBasicTemplates.QUERYRES.Image_vch#);"</cfif>> 
	                            
	                                	<p class="template-name" style="margin: 0 0;">#displayName#</p>     
	                                  
										<div class="item_action">
											<p class="template-name">#RetVarBasicTemplates.QUERYRES.Name_vch#</p>
		                                    <div class="select_group_btn">
			                                  <cfif PreviewOnly EQ 0><button type="button" class="btn btn-success-custom btn-lg btn-select-campaign" >Select</button></cfif>
		                                      <button type="button" class="btn btn-success-custom btn-lg btn-preview-campaign" data-template-id = '#RetVarBasicTemplates.QUERYRES.TID_int#' >Preview</button>
		                                    </div>  
		                                    <p class="template-desc"><a href="##"> <cfoutput>#displayDesc#</cfoutput> </a> </p>
										</div>
	
	                            	</div>
	                            </div>
	
	                        </cfloop>
				
						</div>
				
					</div>
			
				</div>
			
			</div>	
			
			<!--- Write out all of the categories by section - only display current section --->
			#AllSubCategoriesListTemplates#				
					
			<div class="text-center">
		    <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12" id="ShowStarterTemplates-div" style="margin-left: 0px">
		     <a href="##" id="ShowStarterTemplates" style="display: none;">Show Starter Templates</a> 
		    </div>
		    <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12" id="ShowAllTemplates-div" style="margin-left: 0px">
		     <a href="##" id="ShowAllTemplates" style="width: 100%">Show All Templates</a>
		    </div>
		   </div>
			
			<div style="clear: both; padding-bottom: 1em;"></div>
				        	        	        
	    </div>
	    
	</section>        
</main>

</cfoutput>

<!--- MODAL POPUP --->
<div class="bootbox modal fade" id="previewCampaignModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel">Preview Campaign</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <!--- <button type="button" class="btn btn-primary btn-success-custom" id="btn-rename-campaign">Save</button> --->
        <button type="button" class="btn btn-default btn-back-custom" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/sire/js/js.cookie.min.js">
</cfinvoke>

<cfparam name="variables._title" default="Campaign Templates - Sire">
<cfif PreviewOnly EQ 0>
	<cfinclude template="../views/layouts/main.cfm">
<cfelse>
	<cfinclude template="/public/sire/views/layouts/learning-and-support-layout.cfm">
</cfif>

<!--- Include javascript here for better cf documentation --->
<script type="application/javascript">
			
	<!--- On page loaded --->
	$(function() {
						
		<!--- Slide out menu items --->				
				
		$('#template-slideout').click(function(){			
			
			if(parseInt($('#template-slideout').css('right')) == 0)
		   {
		    <!--- Close any other open slide outs --->
		    CloseTemplateSlideOuts($('#template-slideout'));
		    if($(window).width() <= 480){
		     $('#template-slideout').css('right', '270px')
		    }
		    else{
		     $('#template-slideout').css('right', '350px')
		    }
		    $('#template-slideout-inner').css('right', '0')
		    
		   }
		   else
		   {
		    $('#template-slideout').css('right', '0')
		    if($(window).width() <= 480){
		     $('#template-slideout-inner').css('right', '-270px')
		    }
		    else{
		     $('#template-slideout-inner').css('right', '-350px')
		    }
		     
		    
		   }
						
		});
							
		<!--- When user mouses over somthing that is no longer selected hide it --->
		$( document ).on( "mouseleave", ".item-grid .content", function(event) {
	
			// Keep the selection sticky if the preview option is still active		
			if(  !($("#previewCampaignModal").data('bs.modal') || {isShown: false}).isShown)
			{
				event.preventDefault();
				$('.item_action').hide();
				$(".item-grid .content").removeClass('selected');
			}
		});

		// TEMPLATE PICKER
		<!--- For mobile device handle onclick as well as mouse over --->
		$('.item-grid .content').click(function(){
		
			// Keep the selection sticky if the preview option is still active		
			if(  !($("#previewCampaignModal").data('bs.modal') || {isShown: false}).isShown)
			{
				event.preventDefault();
				$('.item_action').hide();
				$(".item-grid .content").removeClass('selected');
			}
			
			event.preventDefault();
			$('.item_action').hide();
			$(".item-grid .content").removeClass('selected');
			$(this).addClass('selected');
			campaign_url = $(this).attr("href");
			$('#btn-select-campaign').prop('disabled',false);
	
			$(this).find('.item_action').show();
			
		});	
			
		$( document ).on( "mouseenter", ".item-grid .content", function(event) {
			event.preventDefault();
			$('.item_action').hide();
			$(".item-grid .content").removeClass('selected');
			$(this).addClass('selected');
			campaign_url = $(this).attr("href");
			$('#btn-select-campaign').prop('disabled',false);
	
			$(this).find('.item_action').show();
	
		});
		
		$( document ).on( "mouseleave", ".item-grid .content", function(event) {
		
			// Keep the selection sticky if the preview option is still active		
			if(  !($("#previewCampaignModal").data('bs.modal') || {isShown: false}).isShown)
			{
				event.preventDefault();
				$('.item_action').hide();
				$(".item-grid .content").removeClass('selected');
			}
		});
		
	
		$( document ).on( "click", "#btn-select-campaign, .btn-select-campaign", function(event) {
			if(typeof campaign_url != 'undefined' && campaign_url != '')
			window.location.href = campaign_url;
		});

		$('.createCampaignModal a[data-toggle="tab"]').on('hide.bs.tab', function (e) {
		  e.target // newly activated tab
		  e.relatedTarget // previous active tab
		  $(".item-grid .content").removeClass('selected');
		  campaign_url = '';
		  $('#btn-select-campaign').prop('disabled',true);
		});
	
		<!--- This is moved here so I can override a public section version that does not redirect when not logged in --->
	    $( document ).on( "click", ".btn-preview-campaign", function(event) {
	      var templateid = $(this).data('template-id');
	      
	      try{
	        $.ajax({
	          type: "GET",
	          
	          <cfif PreviewOnly EQ 0>
	          url: '/session/sire/models/cfm/template_preview.cfm?templateid='+templateid,   
	          <cfelse>
	           url: '/public/sire/models/cfm/template_preview.cfm?templateid='+templateid, 
	          </cfif>
	          beforeSend: function( xhr ) {
	            $('#processingPayment').show();
	          },            
	          error: function(XMLHttpRequest, textStatus, errorThrown) {
	            $('#processingPayment').hide();
	            bootbox.dialog({
	                message: "Get preview Fail",
	                title: "Campaigns",
	                buttons: {
	                    success: {
	                        label: "Ok",
	                        className: "btn btn-medium btn-success-custom",
	                        callback: function() {}
	                    }
	                }
	            });
	          },            
	          success:function(d){
	
	            if(d.indexOf('class="home_page"') > -1){
	              return false;
	            }
	            
	            $('#processingPayment').hide();
	            $('#previewCampaignModal .modal-body').html(d);
	            $('#previewCampaignModal').modal('show');
	          }
	        });
	      }catch(ex){
	        $('#processingPayment').hide();
	        bootbox.dialog({
	            message: "Get preview Fail",
	            title: "Campaigns",
	            buttons: {
	                success: {
	                    label: "Ok",
	                    className: "btn btn-medium btn-success-custom",
	                    callback: function() {}
	                }
	            }
	        });
	      }
	    });
	    
	    <!--- Show longer description sin pop-up --->
		$('i.read-more-description').click(function(event){
		      var description = $(this).data('description');
		      var templateName = $(this).parent().parent().parent().find("p.template-name").html();
		      bootbox.dialog({
		          message: description,
		          title: templateName,
		          buttons: {
		              success: {
		                  label: "Close",
		                  className: "btn btn-medium btn-success-custom",
		                  callback: function() {}
		              }
		          }
			  });
		});				
				
		<!--- Toggle '.SubCategoryTemplates' --->
		$('.SubCategory').click(function(){

			<!--- Close the slideotu if it is open --->				
			CloseTemplateSlideOuts($('#template-slideout'));			
							
			$('.createCampaignModal[rel!="' + $(this).attr('rel') + '"]').hide();
			$('.createCampaignModal[rel="' + $(this).attr('rel') + '"]').show();
			
			$('#ShowAllTemplates').show();
			$('#ShowStarterTemplates').show();
			
			<!--- Only set a cookie if user is logged in --->
			<cfif PreviewOnly EQ 0>
				Cookies.set('TemplateCategoryDefault_<cfoutput>#Session.USERID#</cfoutput>', $(this).attr('rel'));
			</cfif>
		}); 
		
		<!--- Session not active, Show all, look for cookie, or default to Starter Template set --->
		<cfif ShowAll EQ 1>
			$('.createCampaignModal').show();
			$('#ShowAllTemplates').show();
			$('#ShowStarterTemplates').hide();
		<cfelse>
						
			<cfif PreviewOnly EQ 0>
				<!--- If a cookie has been previously set on this device use it --->
				if(Cookies.get('TemplateCategoryDefault_<cfoutput>#Session.USERID#</cfoutput>'))
				{
					$('.createCampaignModal[rel!="' + Cookies.get('TemplateCategoryDefault_<cfoutput>#Session.USERID#</cfoutput>') + '"]').hide();
					$('.createCampaignModal[rel="' + Cookies.get('TemplateCategoryDefault_<cfoutput>#Session.USERID#</cfoutput>') + '"]').show();
										
					$('#ShowAllTemplates').show();	
					$('#ShowStarterTemplates').show();				
				}
				else
				{
					<!--- Show starter templates by default --->
					$('.StarterTemplates').show();
					$('#ShowAllTemplates').show();	
					$('#ShowStarterTemplates').hide();				
				}
			<cfelse>
				$('.createCampaignModal').show();	
				$('#ShowAllTemplates').hide();	
				$('#ShowStarterTemplates').hide();		
			</cfif>
				
		</cfif>		
		
		<!--- Clear preference and show all templates --->
		$('#ShowAllTemplates').click(function(){
			
			<!--- Close the slideotu if it is open --->				
			CloseTemplateSlideOuts($('#template-slideout'));
			
			$('#ShowAllTemplates').hide();
							
			$('.createCampaignModal').show();
			$('#ShowStarterTemplates').show();
			
			<!--- Only set a cookie if user is logged in --->
			<cfif PreviewOnly EQ 0>			
				Cookies.remove('TemplateCategoryDefault_<cfoutput>#Session.USERID#</cfoutput>');				
			</cfif>
		}); 


		<!--- Clear preference and show all templates --->
		$('#ShowStarterTemplates').click(function(){
			
			<!--- Close the slideout if it is open --->				
			CloseTemplateSlideOuts($('#template-slideout'));
						
			$('.createCampaignModal').hide();				
			$('.StarterTemplates').show();
			$('#ShowAllTemplates').show();
			$('#ShowStarterTemplates').hide();
			
			<!--- Only set a cookie if user is logged in --->
			<cfif PreviewOnly EQ 0>			
				Cookies.remove('TemplateCategoryDefault_<cfoutput>#Session.USERID#</cfoutput>');				
			</cfif>
		}); 
		
		$('#cancel-template-request').click(function(){
				
 			$('.template-slideout').click(); 							
			
		}); 
 
		 
	});	
	
	function CloseTemplateSlideOuts(inbObj)
	 {  
	  $('.template-slideout').css('right', '0');
	  if($(window).width() <= 480){
	   $('.template-slideout-inner').css('right', '-270px'); 
	  }
	  else{
	   $('.template-slideout-inner').css('right', '-350px'); 
	  }
	    
	  
	 }
	
</script>	
