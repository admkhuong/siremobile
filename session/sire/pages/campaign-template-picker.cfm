<!--- Advanced user and Admin options --->
<cfparam name="adv" default="0">
<cfparam name="inpTemplateFlag" default="0">
<cfparam name="createSubcriber" default="0">
<cfparam name="selectTemplate" default="0">

<!--- Used to keep code in synce but make available for public browsing of samples --->
<cfparam name="PreviewOnly" default="0">

<!--- Allow showing everthing by default --->
<cfparam name="ShowAll" default="0">

<cfinclude template="../../sire/configs/paths.cfm">

<!--- Only allow template selectiong for editing by Admins  --->
<cfif inpTemplateFlag EQ 1>
  <cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />
</cfif>



<cfset variables.menuToUse = 1 />
<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)
        .addJs("../assets/pages/scripts/campaign-template-picker.js");

</cfscript>

<style>

     .template-item
     {
          height: 300px;
     }

</style>

<!--- Nav --->
<nav class="uk-navbar-container uk-margin-bottom template-navbar" uk-navbar="mode: hover; delay-hide: 100;">
    <div class="uk-navbar">

		<!--- Place holder to save all sub category templates content in --->
		<cfsavecontent variable="AllSubCategoriesListTemplates"></cfsavecontent>
		<!--- Read in each Category besides Basic --->
		<cfinvoke component="session.sire.models.cfc.control-point" method="GetTemplateCategoriesList" returnvariable="RetVarGetTemplateCategoriesList"></cfinvoke>

         <ul class="uk-navbar-nav">

			<!--- Category --->
			<cfloop query="RetVarGetTemplateCategoriesList.QUERYRES">
            <li>
            	<a href="#"><cfoutput>#RetVarGetTemplateCategoriesList.QUERYRES.CGNAME_vch# </cfoutput><i class="fa fa-caret-down"></i></a>

            	<div class="uk-navbar-dropdown">
                    <ul class="uk-nav uk-navbar-dropdown-nav">
						<!--- Read in the current sub categroies --->
						<cfinvoke component="session.sire.models.cfc.control-point" method="GetTemplateCategoriesSubCategoriesList" returnvariable="RetVarGetTemplateCategoriesSubCategoriesList">
							 <cfinvokeargument name="inpCategoryId" value="#RetVarGetTemplateCategoriesList.QUERYRES.CGID_int#">
						</cfinvoke>

						<!--- Subcategory --->
	                    <cfloop query="RetVarGetTemplateCategoriesSubCategoriesList.QUERYRES">
	                    	<cfoutput>
	                        	<li><a class="dropdown-item SubCategory" href="##" rel="#RetVarGetTemplateCategoriesSubCategoriesList.QUERYRES.CID_int#" rel2="#RetVarGetTemplateCategoriesList.QUERYRES.CGID_int#">#RetVarGetTemplateCategoriesSubCategoriesList.QUERYRES.DisplayName_vch#</a></li>
	                        </cfoutput>

							<!--- Generate content here while we build the menu so we dont need to loop or run queries twice - store results in a variable and then append to display variable for use later on --->
								<cfsavecontent variable="SubCategoriesListTemplate">
								<cfoutput>
								<!--- Initially displayed as hidden - will use same rel and rel2 to match to selected sub category menu option --->
								<div class="createCampaignModal" rel="#RetVarGetTemplateCategoriesSubCategoriesList.QUERYRES.CID_int#" rel2="#RetVarGetTemplateCategoriesList.QUERYRES.CGID_int#" style="display: none;">

									<div class="portlet light bordered">
										<div class="portlet-body">

											<!--- Read in al the templates for this sub category --->
											<cfinvoke component="session.sire.models.cfc.control-point" method="GetCampaignSubCategoryTemplateList" returnvariable="RetVarGetCampaignSubCategoryTemplateList">
												 <cfinvokeargument name="inpCategoryId" value="#RetVarGetTemplateCategoriesList.QUERYRES.CGID_int#">
												 <cfinvokeargument name="inpSubCategoryId" value="#RetVarGetTemplateCategoriesSubCategoriesList.QUERYRES.CID_int#">
											</cfinvoke>

											<h4 class="portlet-heading uk-margin-bottom">#RetVarGetTemplateCategoriesList.QUERYRES.CGNAME_vch# <b style="font-size: .8em;" class="glyphicon glyphicon-arrow-right"></b> #RetVarGetTemplateCategoriesSubCategoriesList.QUERYRES.DisplayName_vch#</h4>


											<div class="uk-grid uk-grid-upsmall uk-grid-match">

										    	<cfloop query="RetVarGetCampaignSubCategoryTemplateList.QUERYRES">
										        	<cfif Len(RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Name_vch) GT 150>

										                <cfset displayName = Left(RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Name_vch, 150) & ' ...'>
										                <cfelse>
										                <cfset displayName = RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Name_vch>
										            </cfif>
									                <cfset displayName = Replace(displayName, "\n", " ", "ALL")>

													<cfset RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Description_vch = Replace(RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Description_vch, "\n", "<br>", "ALL")>
													<cfset RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Description_vch = Replace(RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Description_vch, "#chr(10)#", "<br>", "ALL")>

										            <cfif Len(RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Description_vch) GT 80>
										                <cfset displayDesc = Left(RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Description_vch, 80) & '<i title= "Read more..." data-description="' & htmlEditFormat(RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Description_vch) & '" class="glyphicon glyphicon-plus read-more-description"></i>'>
										                <cfelse>
										                <cfset displayDesc = htmlEditFormat(RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Description_vch)>
										            </cfif>

										            <cfset arrCatID = listToArray(RetVarGetCampaignSubCategoryTemplateList.QUERYRES.CatListID, ',') />

														<div class="uk-width-1-1 uk-width-1-3@s uk-width-1-4@l uk-width-1-5@xl uk-margin-bottom <cfloop array="#arrCatID#" index="catID" > cat-id-#catID#</cfloop> item-grid cat-class-#RetVarGetCampaignSubCategoryTemplateList.QUERYRES.CatListID# cat-class-all cat-group-#RetVarGetCampaignSubCategoryTemplateList.QUERYRES.GroupID_int#">

															<div class="uk-inline uk-width-1-1 template-item"  id="templateid-#RetVarGetCampaignSubCategoryTemplateList.QUERYRES.TID_int#" href='/session/sire/pages/campaign-edit?templateid=#RetVarGetCampaignSubCategoryTemplateList.QUERYRES.TID_int#&templateType=#RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Type_int#<cfif ADV EQ 1>&ADV=1</cfif><cfif inpTemplateFlag EQ 1>&inpTemplateFlag=1</cfif>' >
																<h4 class="uk-text-truncate">#displayName#</h4>

																<div class="img-template">
																	<img src="../../../session/sire/images/template-picker/#RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Image_vch#" alt="">
																</div>

																<div class="uk-overlay uk-overlay-default uk-position-cover content-template">
																	<h4 class="uk-text-truncate">#displayName#</h4>
																	<div class="uk-grid uk-grid-small grid-button-preview" uk-grid>

																		<div class="uk-width-1-2 uk-text-right">
																			<cfif PreviewOnly EQ 0><a class="preview btn green-gd  btn-select-campaign"  href='/session/sire/pages/campaign-edit?templateid=#RetVarGetCampaignSubCategoryTemplateList.QUERYRES.TID_int#&templateType=#RetVarGetCampaignSubCategoryTemplateList.QUERYRES.Type_int#<cfif ADV EQ 1>&ADV=1</cfif><cfif inpTemplateFlag EQ 1>&inpTemplateFlag=1</cfif>'>Select</a></cfif>
																		</div>
																		<div class="uk-width-1-2 uk-text-left">
																			<a href="##" class="preview btn green-gd btn-preview-campaign"  data-template-id = '#RetVarGetCampaignSubCategoryTemplateList.QUERYRES.TID_int#' >Preview</a>
																		</div>
																	</div>

																	<a href="##" class="short-des">
																		#displayDesc#
																	</a>

																</div>

															</div>

														</div>
												</cfloop>

											</div>

										</div>

									</div>

								</div>
								</cfoutput>
								</cfsavecontent>
							<!--- Append to final display variable  --->
							<cfset AllSubCategoriesListTemplates = AllSubCategoriesListTemplates & SubCategoriesListTemplate />

	                    </cfloop>

                    </ul>
                </div>
            </li>
            </cfloop>

        </ul>

    </div>
</nav>
<!--- END Nav --->



<div id="wrap-all-template">

	<!--- Write out all of the categories by section - only display current section --->
	<cfoutput>#AllSubCategoriesListTemplates#</cfoutput>

	<!--- <div class="uk-text-center uk-margin-top uk-margin-bottom">
		<a href="##" class="btn green-gd" id="ShowStarterTemplates">Show Starter Templates</a>
	</div> --->
</div>


<!--- MODAL POPUP PREVIEW--->
<div class="bootbox modal fade" id="previewCampaignModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel">Preview Campaign</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <!--- <button type="button" class="btn btn-primary btn-success-custom" id="btn-rename-campaign">Save</button> --->
        <button type="button" class="btn btn-default btn-back-custom" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!--- END MODAL POPUP --->


<cfparam name="variables._title" default="Campaign Template Picker - Sire">

<cfinclude template="../views/layouts/campaign-template-view.cfm">

<!--- Include javascript here for better cf documentation --->
<script type="application/javascript">

	<!--- On page loaded --->
	$(function() {


          $('#previewCampaignModal').on('show.bs.modal', function () {
                 $(this).find('.modal-dialog').css({
                        width:'60%', //probably not needed
                        height:'auto', //probably not needed
                        'max-height':'100%'
                 });

                 $(this).find('input[type="text"], textarea').attr('readonly','readonly');
          });



		<!--- Slide out menu items --->


		<!--- When user mouses over somthing that is no longer selected hide it --->
		$( document ).on( "mouseleave", ".item-grid .content", function(event) {

			// Keep the selection sticky if the preview option is still active
			if(  !($("#previewCampaignModal").data('bs.modal') || {isShown: false}).isShown)
			{
				event.preventDefault();
				$('.item_action').hide();
				$(".item-grid .content").removeClass('selected');
			}
		});

		// TEMPLATE PICKER
		<!--- For mobile device handle onclick as well as mouse over --->
		$('.item-grid .content').click(function(){

			// Keep the selection sticky if the preview option is still active
			if(  !($("#previewCampaignModal").data('bs.modal') || {isShown: false}).isShown)
			{
				event.preventDefault();
				$('.item_action').hide();
				$(".item-grid .content").removeClass('selected');
			}

			event.preventDefault();
			$('.item_action').hide();
			$(".item-grid .content").removeClass('selected');
			$(this).addClass('selected');
			campaign_url = $(this).attr("href");
			$('#btn-select-campaign').prop('disabled',false);

			$(this).find('.item_action').show();

		});

		$( document ).on( "mouseenter", ".item-grid .content", function(event) {
			event.preventDefault();
			$('.item_action').hide();
			$(".item-grid .content").removeClass('selected');
			$(this).addClass('selected');
			campaign_url = $(this).attr("href");
			$('#btn-select-campaign').prop('disabled',false);

			$(this).find('.item_action').show();

		});

		$( document ).on( "mouseleave", ".item-grid .content", function(event) {

			// Keep the selection sticky if the preview option is still active
			if(  !($("#previewCampaignModal").data('bs.modal') || {isShown: false}).isShown)
			{
				event.preventDefault();
				$('.item_action').hide();
				$(".item-grid .content").removeClass('selected');
			}
		});


		$( document ).on( "click", "#btn-select-campaign, .btn-select-campaign", function(event) {
			campaign_url = $(this).attr("href");
			if(typeof campaign_url != 'undefined' && campaign_url != '')
			window.location.href = campaign_url;
		});

		$('.createCampaignModal a[data-toggle="tab"]').on('hide.bs.tab', function (e) {
		  e.target // newly activated tab
		  e.relatedTarget // previous active tab
		  $(".item-grid .content").removeClass('selected');
		  campaign_url = '';
		  $('#btn-select-campaign').prop('disabled',true);
		});

		<!--- This is moved here so I can override a public section version that does not redirect when not logged in --->
	    $( document ).on( "click", ".btn-preview-campaign", function(event) {
	      var templateid = $(this).data('template-id');

	      try{
	        $.ajax({
	          type: "GET",

	          <cfif PreviewOnly EQ 0>
	          url: '/session/sire/models/cfm/template_preview.cfm?templateid='+templateid,
	          <cfelse>
	           url: '/public/sire/models/cfm/template_preview.cfm?templateid='+templateid,
	          </cfif>
	          beforeSend: function( xhr ) {
	            $('#processingPayment').show();
	          },
	          error: function(XMLHttpRequest, textStatus, errorThrown) {
	            $('#processingPayment').hide();
	            bootbox.dialog({
	                message: "Get preview Fail",
	                title: "Campaigns",
	                buttons: {
	                    success: {
	                        label: "Ok",
	                        className: "btn btn-medium btn-success-custom",
	                        callback: function() {}
	                    }
	                }
	            });
	          },
	          success:function(d){

	            if(d.indexOf('class="home_page"') > -1){
	              return false;
	            }

	            $('#processingPayment').hide();
	            $('#previewCampaignModal .modal-body').html(d);
	            $('#previewCampaignModal').modal('show');
	          }
	        });
	      }catch(ex){
	        $('#processingPayment').hide();
	        bootbox.dialog({
	            message: "Get preview Fail",
	            title: "Campaigns",
	            buttons: {
	                success: {
	                    label: "Ok",
	                    className: "btn btn-medium btn-success-custom",
	                    callback: function() {}
	                }
	            }
	        });
	      }
	    });

	    <!--- Show longer description sin pop-up --->
		$('i.read-more-description').click(function(event){
		      var description = $(this).data('description');
		      var templateName = $(this).parent().parent().parent().find("p.template-name").html();
		      bootbox.dialog({
		          message: description,
		          title: templateName,
		          buttons: {
		              success: {
		                  label: "Close",
		                  className: "btn btn-medium btn-success-custom",
		                  callback: function() {}
		              }
		          }
			  });
		});

		<!--- Toggle '.SubCategoryTemplates' --->
		$('.SubCategory').click(function(){

			<!--- Close the slideotu if it is open --->
			CloseTemplateSlideOuts($('#template-slideout'));

			$('.createCampaignModal[rel!="' + $(this).attr('rel') + '"]').hide();
			$('.createCampaignModal[rel="' + $(this).attr('rel') + '"]').show();

			$('#ShowAllTemplates').show();
			$('#ShowStarterTemplates').show();

			<!--- Only set a cookie if user is logged in --->
			<cfif PreviewOnly EQ 0>
				Cookies.set('TemplateCategoryDefault_<cfoutput>#Session.USERID#</cfoutput>', $(this).attr('rel'));
			</cfif>
		});

		<!--- Session not active, Show all, look for cookie, or default to Starter Template set --->
		<cfif ShowAll EQ 1>
			$('.createCampaignModal').show();
			$('#ShowAllTemplates').show();
			$('#ShowStarterTemplates').hide();
		<cfelse>

			<cfif PreviewOnly EQ 0>
				<!--- If a cookie has been previously set on this device use it --->
				if(Cookies.get('TemplateCategoryDefault_<cfoutput>#Session.USERID#</cfoutput>'))
				{
					$('.createCampaignModal[rel!="' + Cookies.get('TemplateCategoryDefault_<cfoutput>#Session.USERID#</cfoutput>') + '"]').hide();
					$('.createCampaignModal[rel="' + Cookies.get('TemplateCategoryDefault_<cfoutput>#Session.USERID#</cfoutput>') + '"]').show();

					$('#ShowAllTemplates').show();
					$('#ShowStarterTemplates').show();
				}
				else
				{
					<!--- Show starter templates by default --->
					$('.StarterTemplates').show();
					$('#ShowAllTemplates').show();
					$('#ShowStarterTemplates').hide();
				}
			<cfelse>
				$('.createCampaignModal').show();
				$('#ShowAllTemplates').hide();
				$('#ShowStarterTemplates').hide();
			</cfif>

		</cfif>

		<!--- Clear preference and show all templates --->
		$('#ShowAllTemplates').click(function(){

			<!--- Close the slideotu if it is open --->
			CloseTemplateSlideOuts($('#template-slideout'));

			$('#ShowAllTemplates').hide();

			$('.createCampaignModal').show();
			$('#ShowStarterTemplates').show();

			<!--- Only set a cookie if user is logged in --->
			<cfif PreviewOnly EQ 0>
				Cookies.remove('TemplateCategoryDefault_<cfoutput>#Session.USERID#</cfoutput>');
			</cfif>
		});


		<!--- Clear preference and show all templates --->
		$('#ShowStarterTemplates').click(function(){

			<!--- Close the slideout if it is open --->
			CloseTemplateSlideOuts($('#template-slideout'));

			$('.createCampaignModal').hide();
			$('.StarterTemplates').show();
			$('#ShowAllTemplates').show();
			$('#ShowStarterTemplates').hide();

			<!--- Only set a cookie if user is logged in --->
			<cfif PreviewOnly EQ 0>
				Cookies.remove('TemplateCategoryDefault_<cfoutput>#Session.USERID#</cfoutput>');
			</cfif>
		});

		$('#cancel-template-request').click(function(){

 			$('.template-slideout').click();

		});


	});

	function CloseTemplateSlideOuts(inbObj)
	 {
	  $('.template-slideout').css('right', '0');
	  if($(window).width() <= 480){
	   $('.template-slideout-inner').css('right', '-270px');
	  }
	  else{
	   $('.template-slideout-inner').css('right', '-350px');
	  }


	 }

</script>
