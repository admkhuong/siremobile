<cfparam name="templateId" default="0">
<cfparam name="campaignId" default="0">
<cfparam name="inpTemplateFlag" default="0">
<cfparam name="adv" default="0">
<cfparam name="action" default="Edit">
<cfparam name="hiddenTemplatePickerClass" default="">
<cfparam name="hiddenClass" default="">
<cfparam name="templateType" default="0">
<cfparam name="selectedTemplateType" default="0">
<cfparam name="displayOptin" default="0">
<cfparam name="displayIntervalType" default="0">
<cfparam name="countCPOneSelection" default="0">
<cfparam name="displaySubcriberList" default="0">
<cfparam name="displayPreview" default="0">
<cfparam name="RAWCONTENTTOSHOW" default="">
<cfparam name="MLPLINK" default="">
<cfparam name="arrCpBranch" default="#[]#">
<cfparam name="arrCpOptIn" default="#[]#">
<cfparam name="arrCpShortAns" default="#[]#">
<cfparam name="arrCpTrailer" default="#[]#">
<cfparam name="arrCpInterval" default="#[]#">
<cfparam name="arrCpStatement" default="#[]#">

<cfparam name="cpOptIn" default="0">

<cfset inpSimpleViewFlag = "#adv#" />

<cfset variables.menuToUse = 1 />

<cfinclude template="/session/sire/configs/paths.cfm">
<cfinclude template="/session/cfc/csc/constants.cfm">

<cfscript>
   CreateObject("component","public.sire.models.helpers.layout")
        .addJs("../assets/global/plugins/jquery-knob/js/jquery.knob.js", true)
        .addJs("../assets/global/plugins/chartjs/Chart.bundle.js", true)
        
        .addJs("/public/sire/js/select2.min.js")
        .addJs("../assets/pages/scripts/ejs_production.js", true)
        .addJs("../assets/pages/scripts/campaign.js")
        .addJs("../assets/pages/scripts/preview.js")
        
        .addJs("../assets/pages/scripts/campaign-template-new.js")        
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)
        .addCss("../assets/global/plugins/uikit2/css/uikit.css", true)
        
        .addCss("../assets/global/plugins/uikit2/css/components/slidenav.min.css", true)

        .addCss("/public/sire/css/jquery.timepicker.min.css", true)
        .addCss("/session/sire/js/vendors/dropzone/dropzone.css", true)

        .addCss("/session/sire/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css", true)
        .addCss("../assets/global/plugins/emojipicker/lib/css/emoji.css", true)
        .addCss("/session/sire/js/vendors/spectrum/spectrum.css", true)
        .addCss("/session/sire/js/vendors/popline/themes/popclip.css", true)
        
        .addJs("/session/sire/assets/global/plugins/bootstrap-datetimepicker/js/moment-with-locales.js", true)
        .addJs("/session/sire/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js", true)
        .addJs("/session/sire/assets/global/plugins/bootstrap-datetimepicker/js/jquery.timepicker.min.js", true)

        //.addJs("/session/sire/assets/global/plugins/popper/popper.js", true)

        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)
        .addJs("../assets/global/plugins/uikit2/js/uikit.min.js", true)
        .addJs("../assets/global/plugins/uikit2/js/components/slideset.min.js", true)
        .addJs("../assets/global/plugins/emojipicker/lib/js/config.js", true)
        .addJs("../assets/global/plugins/emojipicker/lib/js/util.js", true)
        .addJs("/session/sire/js/jquery.sirecustom-emojipicker.js", true)
        // .addJs("/session/sire/js/ckeditor/ckeditor.js", true)
        .addJs("/session/sire/js/es6-promise.auto.js", true)
        .addJs("/session/sire/js/jspdf.min.js", true)
        .addJs("/session/sire/js/html2canvas.min.js", true)
        .addJs("../assets/global/plugins/emojipicker/lib/js/emoji-picker.js", true)
        .addJs("/session/sire/js/html2pdf.min.js", true)
        .addJs("/session/sire/js/vendors/list.min.js", true)
        .addJs("/session/sire/js/vendors/spectrum/spectrum.js", true)
        .addJs("/session/sire/js/vendors/popline/scripts/jquery.popline.js", true)

        <!--- https://github.com/kenshin54/popline --->
        .addJs("/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.link.js", true)
        .addJs("/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.decoration.js", true)
        .addJs("/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.list.js", true)
        .addJs("/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.justify.js", true)
        .addJs("/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.blockformat.js", true)
        .addJs("/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.social.js", true)
        .addJs("/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.textcolor-spectrum.js", true)
        .addJs("/session/sire/js/vendors/dropzone/min/dropzone.min.js", true)
        
        .addJs("/public/js/jquery.ddslick.js", true);          

    </cfscript>    
    
    <!--- Read Current Profile data and put into form --->
    <cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="RetVarGetUserOrganization"></cfinvoke>

    <cfset cpNameSectionShow = 0/> <!---new logic , not show company name section --->

    <cfset campaignData = {
        BatchId_bi = '',
        KeywordId_int = '',
        Keyword_vch = '',
        Keyword_txt = '',
        Desc_vch = '',
        EMS_Flag_int = 0,
        GroupId_int = 0,
        Schedule_arr = [],
        ControlPoint_arr = [],
        CustomHelpMessage_vch = '',
        CustomStopMessage_vch = '',
        TEMPLATEID = 0,
        OPTIN_GROUPID = 0,
        BLAST_GROUPID = 0,
        ScheduleOption=0,
        TEMPLATETYPE_TI=0
    }>

    <!--- MINHHTN HOT FIX --->
    <cfparam name="Session.AdditionalDNC" default="">
    <cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="RetvarShortCode">   
        <cfinvokeargument name="inpBatchId" value="#campaignId#">
    </cfinvoke>
    <!--- GET SUBCRIBER LIST campaignNameDefault---> 

    <cfset CDESCHTML="">
    <!--- Validate trouble ticket batch --->
    <cfif campaignId EQ 0>
         <cfinvoke method="CreateDefaultBatch" component="session.sire.models.cfc.troubleTicket.trouble-ticket" returnvariable="RetVarGetBatchID"></cfinvoke>
        <cfset campaignId = RetVarGetBatchID.BATCHID />
    </cfif>
    <cfif campaignId GT 0>

        <cfset hiddenTemplatePickerClass = ""/>    
        <cfset hiddenClass = "hidden"/>
        <cfset actionHeader = "Edit Campaign"/>
        <cfset btnSaveLabel = "SAVE"/>
        
        <cfinvoke method="GetBatchDetails" component="session.sire.models.cfc.control-point" returnvariable="RetVarGetBatchDetails">
            <cfinvokeargument name="inpBatchID" value="#campaignId#">
        </cfinvoke>           
    
        <cfif RetVarGetBatchDetails.RECORDCOUNT LE 0>
            <cflocation url="/session/sire/pages/campaign-manage" addtoken="false">
        </cfif>

        <cfif RetVarGetBatchDetails.RXRESULTCODE GT 0>
            <cfif RetVarGetBatchDetails.SHORTCODEID NEQ RetvarShortCode.SHORTCODEID>
                <cfinvoke method="CreateDefaultBatch" component="session.sire.models.cfc.troubleTicket.trouble-ticket" returnvariable="RetVarGetBatchID">
                    <cfinvokeargument name="inpTroubleTicketConfig" value= "1">
                </cfinvoke>
                <cfset campaignId = RetVarGetBatchID.BATCHID />
                <!--- <cflocation url="/session/sire/pages/campaign-manage" addtoken="false"/> --->
            </cfif>
        </cfif>

        <cfset campaignData.BatchId_bi = RetVarGetBatchDetails.BATCHID>
        <cfset campaignData.Desc_vch = RetVarGetBatchDetails.DESC>
        <cfset campaignData.EMS_Flag_int = RetVarGetBatchDetails.EMSFLAG>
        <cfset campaignData.GroupId_int = RetVarGetBatchDetails.GROUPID>
        <cfset campaignData.MLPID = RetVarGetBatchDetails.MLPID>
        <cfset campaignData.TEMPLATEID = RetVarGetBatchDetails.TEMPLATEID>
        <cfset campaignData.TemplateType_ti =  RetVarGetBatchDetails.TemplateType>
            <cfset campaignData.ScheduleOption =  RetVarGetBatchDetails.ScheduleOption>

        <!--- Preserve possible newlines in display but format all other tags --->
        <!--- Expected HTML output is different for straight div vs text area - Preview, Edit, SWT etc --->                    
        <!--- Allow newline in text desc messages - reformat for display --->
        <!--- Regular HTML --->
        <cfset CDescHTML = ReplaceNoCase(campaignData.Desc_vch, "newlinex", "<br>", "ALL")>
        <cfset CDescHTML = Replace(CDescHTML, "\n", "<br>", "ALL")>
        <cfset CDescHTML = Replace(CDescHTML, "#chr(10)#", "<br>", "ALL")>
        <cfset CDescHTML = Replace(CDescHTML, "{defaultShortcode}", "#RetvarShortCode.SHORTCODE#", "ALL")>
        <!--- Text Area version --->                
        <cfset CDescTA = Replace(campaignData.Desc_vch, "\n", "#chr(10)#", "ALL")>
        <cfset CDescTA = Replace(CDescTA, "<br>", "#chr(10)#", "ALL")>
        <cfset CDescTA = Replace(CDescTA, "{defaultShortcode}", "#RetvarShortCode.SHORTCODE#", "ALL")>

        <cfquery name="KeywordQuery" datasource="#session.DBSourceREAD#">
            SELECT KeywordId_int, Keyword_vch, Created_dt, Active_int, EMSFlag_int
            FROM sms.keyword WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RetVarGetBatchDetails.BATCHID#">
            AND IsDefault_bit = 0
            ORDER BY Created_dt DESC, KeywordId_int DESC
        </cfquery>

        <cfif KeywordQuery.RecordCount GT 0 && KeywordQuery.Active_int[1] EQ 1>
            <cfset campaignData.KeywordId_int = KeywordQuery.KeywordId_int[1]>
            <cfset campaignData.Keyword_vch = KeywordQuery.Keyword_vch[1]>
            <cfif KeywordQuery.EMSFlag_int[1] EQ 0>
                <cfset campaignData.Keyword_txt = campaignData.Keyword_vch>
            </cfif>
            <cfset campaignData.Keyword_txt =REReplace(HTMLCodeFormat(campaignData.Keyword_txt),'<pre>|</pre>','','all')>
        </cfif>

        <cfparam name="SCHEDULE" default="#campaignData.Schedule_arr#" >
        <!--- GET CUSTOME STOP/HELP --->
        <cfquery name="CustomMessageQuery" datasource="#session.DBSourceREAD#">
            SELECT * FROM sms.smsshare WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#campaignId#">
        </cfquery>

        <cfloop query="#CustomMessageQuery#">
            <cfswitch expression="#Keyword_vch#" >
                <cfcase value="HELP">
                    <cfset campaignData.CustomHelpMessage_vch = Content_vch>
                </cfcase>
                <cfcase value="STOP">
                    <cfset campaignData.CustomStopMessage_vch = Content_vch>
                </cfcase>
            </cfswitch>
        </cfloop>
    </cfif>

<cfoutput>

<!--- campaign template list choice --->
<!--- <cfinclude template="campaign-template-list.cfm">     --->
<!--- END campaign template list choice --->
<input value="#RetvarShortCode.SHORTCODE#" id="ShortCode" type="hidden" name="ShortCode">

<div>
    <form name="frm-campaign" id="frm-campaign"> 
    <div class="content-for-first-campaign">  
        <cfinclude template="inc_campaign-edit-template-troubleticket.cfm">  
    </div>
    </form>
<!-- End For first campaign -->
</div>
</div>  

<cfinclude template="inc_dashboard_slide_integrate.cfm">

<script type="text/javascript">
    var OPTIN_GROUPID = "#campaignData.OPTIN_GROUPID#";
    var BLAST_GROUPID = "#campaignData.BLAST_GROUPID#";
    var GroupsListBox = null;

    <!--- Set some globally avaialble javascript vars based on current data --->
    var schedule_data = "";
    var SHORTCODE = "#RetvarShortCode.SHORTCODE#";
    
    var date_txt_1 = '#LSDateFormat(DateAdd("d", 30, Now()), "m-d-yyyy")#';
    var date_txt_2 = '#LSDateFormat(NOW(), "m-d-yyyy")#';

    var ScheduleTimeData;
    
    <!--- Set Global var for inpBatchId for Preview .js --->
    var inpBatchId = #campaignData.BatchId_bi#;
    var inpBatchName = '#campaignData.Desc_vch#';

    var action = '#action#';

    var inpCampaignType = #campaignData.TemplateType_ti#;
    var inpTemplateId = #templateId#;      
    var inpCampaignId = #campaignId#;
    var inpTemplateType=#selectedTemplateType#;
    var countCPOneSelection = #countCPOneSelection#;
    var currentCPOneSelection = 1;

    var intListDeleteRID = [];
    var tempListDeleteRID = [];

    var checkClickPreview = 0;

    <cfif templateId EQ 11>
        var selectorCP = "data-template-id="+inpTemplateId+"";
    <cfelse>
        var selectorCP = "data-control-point-type='ONESELECTION'";
    </cfif>

    var subcriberList = "";

    var displayPreview = #displayPreview#;

    var arrCpBranch = #SerializeJSON(arrCpBranch)#;
    var arrCpOptIn = #SerializeJSON(arrCpOptIn)#;

    var arrCpShortAns = #SerializeJSON(arrCpShortAns)#;
    var arrCpTrailer = #SerializeJSON(arrCpTrailer)#;

    var arrCpInterval = #SerializeJSON(arrCpInterval)#;
    var arrCpStatement = #SerializeJSON(arrCpStatement)#;  

    var cpOptIn = #cpOptIn#;

    var mlpLink =   "";
    var shareUrl =  "";
    var hostname = ("https://"+window.location.hostname);

</script>

</cfoutput>

<!--- <cfinclude template="../views/commons/inc_carousel.cfm"> --->

<cfif action EQ 'Edit'>
    <cfparam name="variables._title" default="Edit Campaign - Sire">
<cfelse>
    <cfparam name="variables._title" default="Campaign Create - Sire">        
</cfif>

<style type="text/css">
    .campaign-mlp{
        margin-left: 0px !important;
    }

    .campaign-mlp{
        min-width: 273px !important;
    }


    @media screen and (max-width: 375px){
            .campaign-mlp{
            width: 273px !important;
        }
    }

    @media screen and (min-width: 376px) and (max-width: 480px){
        .campaign-mlp{
            width: 312px !important;
        }
    }

    
    .campaign-mlp-header img{
        max-height: 100% !important;
    }

    /*.popline{
        width: 320px;
    }*/

    .popline li{
        padding-right: 12px;
    }

    .popline .popline-color2-button{
        padding-right: 0px;
    }

    .popline .popline-blockFormat-button{
        padding-right: 0px;
    }

    .popline-textfield{
        color: black;
    }

    #campaign-mlp-logo{
        cursor: pointer;
    }

    .mlp-btn {
        display: inline-block;
        vertical-align: middle;
        padding: 10px 24px;
        border-radius: 15em;
        border: 1px solid;
        margin: 2px;
        min-width: 60px;
        font-size: .875rem;
        text-align: center;
        outline: 0;
        line-height: 1;
        cursor: pointer;
        color: #FFF;
        background-color: #74c37f;
        border-color: #74c37f;
        text-transform: uppercase;
        font-weight: 700;
    }

    .dz-success-mark{
        display: none;
    }

    .dz-error-mark{
        display: none;
    }

</style>

<cfinclude template="../views/layouts/master-blast.cfm">


