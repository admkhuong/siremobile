<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/site/change_password.js">
</cfinvoke>

<main class="container-fluid page" id="change-password-page">
    <cfinclude template="../views/commons/credits_available.cfm">
        <section class="row bg-white">
            <div class="content-header">
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6 content-title">
                        <cfinclude template="../views/commons/welcome.cfm">
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-6 content-menu">
                        <a class="active"><span class="icon-change-password"></span><span>Change Password</span></a>
                    </div>
                </div>
            </div>
            <hr class="hrt0">
            <div class="content-body">
            	
            	<form action="##" method="POST" name="change_password_form" id="change_password_form" class="form-horizontal">
                    <div class="col-lg-7">
                    <div class="row heading">
                        <div class="col-sm-6 heading-title">Change Password</div>
                        <div class="col-sm-6 heading-button"></div>
                    </div>
                    <div class="row">
                    	
                    	<div class="form-group">
                            <label class="control-label col-md-3">Email</label>
                            <div class="col-md-7">
                                <p><cfoutput>#Session.EMAILADDRESS#</cfoutput></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Old Password<span class="text-danger">*</span></label>
                            <div class="col-md-7">
                                <input class="form-control validate[required]" id="currentPassword" name="currentPassword" type="password" placeholder="" maxlength="50" data-prompt-position="topLeft:50">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">New Password<span class="text-danger">*</span></label>
                            <div class="col-md-7">
                                <input type="password" class="form-control validate[required,funcCall[isValidPassword]]" id="inpPasswordSignup" name="inpPasswordSignup" placeholder="" maxlength="50" data-prompt-position="topLeft:50">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Confirm New Password<span class="text-danger">*</span></label>
                            <div class="col-md-7">
                                <input type="password" class="form-control validate[required,equals[inpPasswordSignup]]" id="inpConfirmPassword" name="inpConfirmPassword" placeholder="" maxlength="50" data-prompt-position="topLeft:50"/>
                            </div>
                        </div>
                        
                    </div>

                </div>
                <div class="col-lg-5 guide">
                    <h4><strong>Secure Password Requirements</strong></h4>
                    <ul>
                        <li>must be at least 8 characters</li>
                        <li>must have at least 1 number</li>
                        <li>must have at least 1 uppercase letter</li>
                        <li>must have at least 1 lower case letter</li>
                        <li>must have at least 1 special character</li>
                    </ul>
                </div>
                	<div class="col-md-10 col-xs-12 button-content-footer">
                		<button class="btn btn-medium btn-success-custom" id="change-password">Save</button>
                        <a href="/session/sire/pages/" class="btn btn-medium btn-back-custom">Cancel</a>
                	</div>
                            	</form>

            </div>
        </section>

</main>
<cfparam name="variables._title" default="Change Password - Sire">
<cfinclude template="../views/layouts/main.cfm">