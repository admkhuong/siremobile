<!DOCTYPE html>
<html>
    <head>
        <title>A Simple Image File Upload with CKEditor</title>
        <!-- Make sure the path to CKEditor is correct. -->
        <script src="/session/sire/js/vendors/bootstrapeditor/plugins/editor/ckeditor/ckeditor.js"></script>
    </head>
    <body>
        <form>
            <textarea name="editor1" id="editor1" rows="10" cols="80">
                This is my textarea to be replaced with CKEditor.
            </textarea>
            <script>
                        CKEDITOR.replace( 'editor1', {
                              filebrowserImageBrowseUrl : 'fileBrowser.cfm',
                              filebrowserUploadUrl: "upload.cfm"
                        } );
            </script>
        </form>
    </body>
</html>