<cfscript>
	CreateObject("component", "public.sire.models.helpers.layout")
	.addJs("/session/sire/assets/global/scripts/filter-bar-generator.js")
	.addJs("/public/sire/js/select2.min.js", true)
	.addJs("/session/sire/js/trouble-ticket.js")
	.addCss("/public/sire/css/select2.min.css", true)
	.addCss("/public/sire/css/select2-bootstrap.min.css", true);

</cfscript>

<!--- <cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/js/trouble-ticket.js">
</cfinvoke> --->

<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfparam name="isDialog" default="0">
<cfparam name="isUpdateSuccess" default="">

<div class="portlet light bordered">
	<!--- Filter bar --->
	<div class="row">
		<div class="col-sm-3 col-md-4">
			<h2 class="page-title">Admin - Trouble Ticket</h2>
		</div>
		<div class="col-sm-9 col-md-8">
			<div id="box-filter" class="form-gd">

			</div>
		</div>
	</div>
	<!--- Listing --->
	<div class="row">
		<div class="col-md-12">
			<div class="compaign">
				<div class="re-table">
					<div class="table-responsive">
						<table id="tblListEMS" class="table-responsive table-striped dataTables_wrapper">
						
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>






<!-- Modal -->
<div class="modal fade" id="AdminTroubleTicketModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:auto; max-width:900px;">
        <div class="modal-content">
            <div class="modal-body">
            	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title"><strong>TROUBLE TICKET DETAILS</strong></h4>
	            <div class="modal-body-inner row">
		            <form name="edit-trouble-ticket" class="col-sm-12" autocomplete="off" id="edit-trouble-ticket-form">
		            	<input type="hidden" name="" id="ticket-id" value="">
		            	<div class="row">
		            		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
		            			<div class="form-group">
								  	<label for="userphone" id="user-phone-label">Phone</label>
								  	<input type="text" class="form-control" id="user-phone" name="user-phone">
								</div>
		            		</div>

		            		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
		            			<div class="form-group">
									<label for="status">Status</label>
									<select  id="ticket-status" class="form-control detail-control">
										<option value="0">Open</option>
										<option value="1">Assigned</option>
										<option value="2">In Progress</option>
										<option value="3">On Hold</option>
										<option value="4">Closed</option>
									</select>
								</div>
		            		</div>
		            	</div>

		            	<div class="row">
		            		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
		            			<div class="form-group">
									<label for="userid">User Id</label>
									<select  id="user-id-select" class="form-control detail-control select2">
									</select>
								</div>
		            		</div>

		            		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
		            			<div class="form-group">
									<label for="batchid">Batch Id</label>
									<select  id="batch-id-select" class="form-control detail-control select2">
									</select>
								</div>
		            		</div>
		            	</div>

						<div class="form-group">
						  	<label for="userrequestdetails">Details</label>
						  	<textarea class="form-control no-resize validate[required,maxSize[255]] disabled" id="user-request-details" name="user-request-details" rows="6" data-prompt-position="topLeft:100"></textarea>
						</div>

						<div class="form-group">
						  	<label for="notes" id="notes-label">Notes</label>
						  	<textarea class="form-control no-resize validate[maxSize[255]] disabled" id="notes-for-request" name="notes-for-request" rows="6" data-prompt-position="topLeft:100"></textarea>
						</div>
						<div class="form-group">
							<div class="edit-trouble-ticket-btn-group">
							  	<button class="btn btn-back-custom" id="close-edit-ticket-btn" data-dismiss="modal">Cancel</button>
							  	<button class="btn btn-success-custom" id="confirm-edit-ticket-btn" type="submit">Save</button>
							</div>
						</div>	
		            </form>
				</div>
        	</div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="AdminTroubleTicketNotifyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:auto; max-width:900px;">
        <div class="modal-content">
            <div class="modal-body">
            	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title"><strong>NOTIFY TROUBLE TICKET</strong></h4>
	            <div class="modal-body-inner row">
		            <form name="notify-trouble-ticket" class="col-sm-12" autocomplete="off" id="notify-trouble-ticket-form">
		            	<input type="hidden" name="" id="notify-ticket-id" value="">
		            	<div class="row">

		            		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
				            	<div class="form-group">
									<label for="notify-method">How do you want to send notify message?</label>
									<select  id="notify-method" class="form-control">
										<option value="0">SMS</option>
										<option value="1">Email</option>
									</select>
								</div>
							</div>

		            		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
		            			<div class="form-group">
								  	<label for="notify-phone" id="notify-phone-label">Phone</label>
								  	<input type="text" class="form-control" id="notify-phone" name="notify-phone">
								</div>
		            		</div>

		            		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-6" style="display: none;">
		            			<div class="form-group">
									<label for="notify-email-address">Email</label>
									<input type="text" class="form-control validate[custom[email]]" id="notify-email-address" name="notify-email-address">
								</div>
		            		</div>
		            	</div>

		            	<div class="form-group">
							<label for="notifysms">Message</label>
							<label class="pull-right" id="notify-sms-mess-countdown-label"><a id="notify-sms-mess-countdown">160</a> left</label>
							<textarea class="form-control no-resize validate[maxSize[1000]]" id="notify-sms-message" name="notify-sms-message" placeholder="" rows="6" data-prompt-position="topLeft:100"></textarea>
						</div>
						<div class="form-group" style="display: none;">
							<label for="notifyemailsubject">Subject</label>
							<label class="pull-right" id="notify-email-sub-countdown-label"><a id="notify-email-sub-countdown">255</a> left</label>
							<input type="text" class="form-control validate[maxSize[255]]" id="notify-email-subject" name="notify-email-subject" placeholder="" data-prompt-position="topLeft:100"/>
						</div>
						<div class="form-group" style="display: none;">
							<label for="notifyemail">Message</label>
							<label class="pull-right" id="notify-email-mess-countdown-label"><a id="notify-email-mess-countdown">1000</a> left</label>
							<textarea class="form-control no-resize validate[maxSize[1000]]" id="notify-email-message" name="notify-email-message" placeholder="" rows="6" data-prompt-position="topLeft:100"></textarea>
						</div>
						<div class="form-group">
							<div class="notify-trouble-ticket-btn-group">
							  	<button class="btn btn-back-custom" id="close-nofity-ticket-btn" data-dismiss="modal">Cancel</button>
							  	<button class="btn btn-success-custom" id="confirm-notify-ticket-btn" type="submit">Send</button>
							</div>
						</div>
		            </form>
		           
				</div>
        	</div>
        </div>
    </div>
</div>

<cfparam name="variables._title" default="Trouble Ticket">
<cfinclude template="../views/layouts/master.cfm">

<style type="text/css">

	.edit-trouble-ticket-btn-group, .notify-trouble-ticket-btn-group{
		float: right;
	}

	#notes-for-request, #notify-email-message, #notify-sms-message{
		resize: none;
	}

	#user-request-details{
		resize: none;
	}

	table#tblListEMS th{
		text-align: center;
	}

	.ticket-action-th {
		width: 85px !important;
	}

	td.ticket-action-th, td.ticket-id-th, td.ticket-batchid-th, td.ticket-userid-th,
	td.ticket-status-th, td.ticket-date-th, td.ticket-phone-th{
		text-align: center;
	}

	.ticket-id-th {
		width: 105px !important;
	}

	.ticket-batchid-th {
		width: 105px !important;
	}

	.ticket-userid-th {
		width: 105px !important;
	}

	.ticket-status-th {
		width: 105px !important;
	}

	.ticket-phone-th {
		width: 180px !important;
	}

	.ticket-date-th {
		width: 180px !important;
	}

	.ticket-note-th {
		width: 200px !important;
	}

	.ticket-detail-th {
		width: 300px !important;
	}

	@media only screen and (max-width: 768px){
		.ticket-detail-th {
			width: 200px !important;
		}
		.ticket-note-th {
			width: 200px !important;
		}

		.ticket-phone-th {
			width: 130px !important;
		}

		.ticket-date-th {
			width: 130px !important;
		}
	}

	.disabled-notify-trouble-ticket{
		cursor: not-allowed;
	}

	.notify-trouble-ticket, .edit-trouble-ticket {
		cursor: pointer;
	}
	#notify-email-sub-countdown, #notify-email-mess-countdown, #notify-sms-mess-countdown{
		text-decoration: none;
		color: #71c37b;
	}
</style>