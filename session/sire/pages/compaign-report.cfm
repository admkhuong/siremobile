<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
    	.addCss("../assets/global/plugins/daterangepicker/daterangepicker.css", true)
    	.addJs("../assets/global/plugins/daterangepicker/moment.min.js", true)
        .addJs("../assets/global/plugins/daterangepicker/daterangepicker.js", true)
        .addJs("../assets/pages/scripts/script-04.js");
</cfscript>
<div class="portlet light bordered compaign-report">
    <div class="portlet-body form">
        <h2 class="page-title">Report: Simple Subscribers (Opt In) 2017-01-23 18:08:13</h2>
        <h3 class="compaign-report-subtitle">Subscriber Growth</h3>
        <div class="options">
	        <div class="row">
	        	<div class="col-xs-12 col-sm-3">
	        		<div class="select-list">
	        			<p><b>Selected List</b></p>
		                <select class="form-control">
			                <option>All</option>
			                <option>Email</option>
		                </select>
		            </div>
	        	</div>
	        	<div class="col-xs-12 col-sm-6 col-sm-offset-3">
	        		<div class="date-range">
	        			<p><b>Selected Date Range</b></p>
						<div class="reportrange pull-right">
						    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
						    <span></span> <b class="caret"></b>
						</div>
	        		</div>
	        	</div>
	        </div>
        </div>
        <!--- options select --->
        <div id="highchart_1" class="compaign-report-chart" style="height:500px;" data-highcharts-chart="0">
            <div class="highcharts-container" id="highcharts-0" style="position: relative; overflow: hidden; width: 100%; height: 500px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); font-family: &quot;Open Sans&quot;;">
                <svg version="1.1" style="font-family:Open Sans;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="100%" height="500">
                    <desc>Created with Highcharts 4.1.9</desc>
                    <defs>
                        <clipPath id="highcharts-1">
                            <rect x="0" y="0" width="1403" height="396">
                            </rect>
                        </clipPath>
                    </defs>
                    <rect x="0" y="0" width="100%" height="500" strokeWidth="0" fill="#FFFFFF" class=" highcharts-background"></rect>
                    <path fill="none" d="M 65 405.5 L 1468 405.5" stroke="#808080" stroke-width="1"></path>
                    <g class="highcharts-grid" zIndex="1"></g>
                    <g class="highcharts-grid" zIndex="1">
                        <path fill="none" d="M 65 462.5 L 1468 462.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path>
                        <path fill="none" d="M 65 405.5 L 1468 405.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path>
                        <path fill="none" d="M 65 349.5 L 1468 349.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path>
                        <path fill="none" d="M 65 292.5 L 1468 292.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path>
                        <path fill="none" d="M 65 236.5 L 1468 236.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path>
                        <path fill="none" d="M 65 179.5 L 1468 179.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path>
                        <path fill="none" d="M 65 123.5 L 1468 123.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path>
                        <path fill="none" d="M 65 65.5 L 1468 65.5" stroke="#D8D8D8" stroke-width="1" zIndex="1" opacity="1"></path>
                    </g>
                    <g class="highcharts-axis" zIndex="2">
                        <path fill="none" d="M 181.5 462 L 181.5 472" stroke="#C0D0E0" stroke-width="1" opacity="1"></path>
                        <path fill="none" d="M 298.5 462 L 298.5 472" stroke="#C0D0E0" stroke-width="1" opacity="1"></path>
                        <path fill="none" d="M 415.5 462 L 415.5 472" stroke="#C0D0E0" stroke-width="1" opacity="1"></path>
                        <path fill="none" d="M 532.5 462 L 532.5 472" stroke="#C0D0E0" stroke-width="1" opacity="1"></path>
                        <path fill="none" d="M 649.5 462 L 649.5 472" stroke="#C0D0E0" stroke-width="1" opacity="1"></path>
                        <path fill="none" d="M 766.5 462 L 766.5 472" stroke="#C0D0E0" stroke-width="1" opacity="1"></path>
                    </g>
                    <g class="highcharts-series-group" zIndex="3">
                        <path fill="rgba(124,181,236,0.25)" d="M 0 0"></path>
                        <path fill="rgba(247,163,92,0.25)" d="M 0 0"></path>
                        <path fill="rgba(144,237,125,0.25)" d="M 0 0"></path>
                        <path fill="rgba(67,67,72,0.25)" d="M 0 0"></path>
                        <g class="highcharts-series highcharts-series-2" visibility="visible" zIndex="0.1" transform="translate(65,66) scale(1 1)" clip-path="url(#highcharts-1)">
                            <path fill="none" d="M 58.458333333333336 349.6114285714286 L 175.375 332.64 L 292.2916666666667 299.8285714285714 L 409.2083333333333 244.38857142857142 L 526.125 186.68571428571428 L 643.0416666666667 147.08571428571426 L 759.9583333333334 128.98285714285709 L 876.8750000000001 136.90285714285716 L 993.7916666666667 177.63428571428568 L 1110.7083333333333 237.6 L 1227.625 295.30285714285714 L 1344.5416666666667 328.1142857142857" stroke="#90ed7d" stroke-width="2" zIndex="1" stroke-linejoin="round" stroke-linecap="round"></path>
                            <path fill="none" d="M 48.458333333333336 349.6114285714286 L 58.458333333333336 349.6114285714286 L 175.375 332.64 L 292.2916666666667 299.8285714285714 L 409.2083333333333 244.38857142857142 L 526.125 186.68571428571428 L 643.0416666666667 147.08571428571426 L 759.9583333333334 128.98285714285709 L 876.8750000000001 136.90285714285716 L 993.7916666666667 177.63428571428568 L 1110.7083333333333 237.6 L 1227.625 295.30285714285714 L 1344.5416666666667 328.1142857142857 L 1354.5416666666667 328.1142857142857" stroke-linejoin="round" visibility="visible" stroke="rgba(192,192,192,0.0001)" stroke-width="22" zIndex="2" class=" highcharts-tracker" style=""></path>
                        </g>
                        <g class="highcharts-markers highcharts-series-2 highcharts-tracker" visibility="visible" zIndex="0.1" transform="translate(65,66) scale(1 1)" clip-path="url(#highcharts-2)" style="">
                            <path fill="#90ed7d" d="M 1340 324.1142857142857 L 1348 324.1142857142857 1348 332.1142857142857 1340 332.1142857142857 Z"></path>
                            <path fill="#90ed7d" d="M 1223 291.30285714285714 L 1231 291.30285714285714 1231 299.30285714285714 1223 299.30285714285714 Z"></path>
                            <path fill="#90ed7d" d="M 1106 233.6 L 1114 233.6 1114 241.6 1106 241.6 Z"></path>
                            <path fill="#90ed7d" d="M 989 173.63428571428568 L 997 173.63428571428568 997 181.63428571428568 989 181.63428571428568 Z"></path>
                            <path fill="#90ed7d" d="M 872 132.90285714285716 L 880 132.90285714285716 880 140.90285714285716 872 140.90285714285716 Z"></path>
                            <path fill="#90ed7d" d="M 755 124.98285714285709 L 763 124.98285714285709 763 132.98285714285709 755 132.98285714285709 Z" stroke-width="1"></path>
                            <path fill="#90ed7d" d="M 639 143.08571428571426 L 647 143.08571428571426 647 151.08571428571426 639 151.08571428571426 Z" stroke-width="1"></path>
                            <path fill="#90ed7d" d="M 522 182.68571428571428 L 530 182.68571428571428 530 190.68571428571428 522 190.68571428571428 Z" stroke-width="1"></path>
                            <path fill="#90ed7d" d="M 405 240.38857142857142 L 413 240.38857142857142 413 248.38857142857142 405 248.38857142857142 Z" stroke-width="1"></path>
                            <path fill="#90ed7d" d="M 288 295.8285714285714 L 296 295.8285714285714 296 303.8285714285714 288 303.8285714285714 Z" stroke-width="1"></path>
                            <path fill="#90ed7d" d="M 171 328.64 L 179 328.64 179 336.64 171 336.64 Z" stroke-width="1"></path>
                            <path fill="#90ed7d" d="M 54 345.6114285714286 L 62 345.6114285714286 62 353.6114285714286 54 353.6114285714286 Z" stroke-width="1"></path>
                        </g>
                    </g>
                    <g class="highcharts-legend" zIndex="7" transform="translate(1480,208)">
                        <g zIndex="1">
                            <g>
                                <g class="highcharts-legend-item" zIndex="1" transform="translate(8,3)">
                                    <path fill="none" d="M 0 11 L 16 11" stroke="#7cb5ec" stroke-width="2"></path>
                                    <path fill="#7cb5ec" d="M 8 7 C 13.328 7 13.328 15 8 15 C 2.6719999999999997 15 2.6719999999999997 7 8 7 Z"></path>
                                    <text x="21" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start" zIndex="2" y="15">Tokyo</text>
                                </g>
                                <g class="highcharts-legend-item" zIndex="1" transform="translate(8,20)">
                                    <path fill="none" d="M 0 11 L 16 11" stroke="#434348" stroke-width="2"></path>
                                    <path fill="#434348" d="M 8 7 L 12 11 8 15 4 11 Z"></path>
                                    <text x="21" y="15" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start" zIndex="2">
                                        <tspan>New York</tspan>
                                    </text>
                                </g>
                                <g class="highcharts-legend-item" zIndex="1" transform="translate(8,37)">
                                    <path fill="none" d="M 0 11 L 16 11" stroke="#90ed7d" stroke-width="2"></path>
                                    <path fill="#90ed7d" d="M 4 7 L 12 7 12 15 4 15 Z"></path>
                                    <text x="21" y="15" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start" zIndex="2">Berlin</text>
                                </g>
                                <g class="highcharts-legend-item" zIndex="1" transform="translate(8,54)">
                                    <path fill="none" d="M 0 11 L 16 11" stroke="#f7a35c" stroke-width="2"></path>
                                    <path fill="#f7a35c" d="M 8 7 L 12 15 4 15 Z"></path>
                                    <text x="21" y="15" style="color:#333333;font-size:12px;font-weight:bold;cursor:pointer;fill:#333333;" text-anchor="start" zIndex="2">London</text>
                                </g>
                            </g>
                        </g>
                    </g>
                    <g class="highcharts-axis-labels highcharts-xaxis-labels" zIndex="7">
                        <text x="123.45833333333334" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:112px;text-overflow:clip;" text-anchor="middle" transform="translate(0,0)" y="481" opacity="1">August</text>
                        <text x="240.37500000000003" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:112px;text-overflow:clip;" text-anchor="middle" transform="translate(0,0)" y="481" opacity="1">September</text>
                        <text x="357.2916666666667" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:112px;text-overflow:clip;" text-anchor="middle" transform="translate(0,0)" y="481" opacity="1">October</text>
                        <text x="474.2083333333334" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:112px;text-overflow:clip;" text-anchor="middle" transform="translate(0,0)" y="481" opacity="1">November</text>
                        <text x="591.125" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:112px;text-overflow:clip;" text-anchor="middle" transform="translate(0,0)" y="481" opacity="1">December</text>
                        <text x="708.0416666666667" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:112px;text-overflow:clip;" text-anchor="middle" transform="translate(0,0)" y="481" opacity="1">January</text>
                    </g>
                    <g class="highcharts-axis-labels highcharts-yaxis-labels" zIndex="7">
                        <text x="50" style="color:#606060;cursor:default;font-size:11px;fill:#606060;width:513px;text-overflow:clip;" text-anchor="end" transform="translate(0,0)" y="466" opacity="1">0</text>
                    </g>
                    <g class="highcharts-tooltip" zIndex="8" style="cursor:default;padding:0;pointer-events:none;white-space:nowrap;" transform="translate(72,-9999)" opacity="0" visibility="visible">
                        <path fill="none" d="M 3.5 0.5 L 44.5 0.5 50.5 -5.5 56.5 0.5 98 0.5 C 101.5 0.5 101.5 0.5 101.5 3.5 L 101.5 43.5 C 101.5 46.5 101.5 46.5 98.5 46.5 L 3.5 46.5 C 0.5 46.5 0.5 46.5 0.5 43.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="black" stroke-opacity="0.049999999999999996" stroke-width="5" transform="translate(1, 1)" width="101" height="46"></path>
                        <path fill="none" d="M 3.5 0.5 L 44.5 0.5 50.5 -5.5 56.5 0.5 98 0.5 C 101.5 0.5 101.5 0.5 101.5 3.5 L 101.5 43.5 C 101.5 46.5 101.5 46.5 98.5 46.5 L 3.5 46.5 C 0.5 46.5 0.5 46.5 0.5 43.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="black" stroke-opacity="0.09999999999999999" stroke-width="3" transform="translate(1, 1)" width="101" height="46"></path>
                        <path fill="none" d="M 3.5 0.5 L 44.5 0.5 50.5 -5.5 56.5 0.5 98 0.5 C 101.5 0.5 101.5 0.5 101.5 3.5 L 101.5 43.5 C 101.5 46.5 101.5 46.5 98.5 46.5 L 3.5 46.5 C 0.5 46.5 0.5 46.5 0.5 43.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="black" stroke-opacity="0.15" stroke-width="1" transform="translate(1, 1)" width="101" height="46"></path>
                        <path fill="rgba(249, 249, 249, .85)" d="M 3.5 0.5 L 44.5 0.5 50.5 -5.5 56.5 0.5 98 0.5 C 101.5 0.5 101.5 0.5 101.5 3.5 L 101.5 43.5 C 101.5 46.5 101.5 46.5 98.5 46.5 L 3.5 46.5 C 0.5 46.5 0.5 46.5 0.5 43.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" stroke="#90ed7d" stroke-width="1"></path>
                        <text x="8" zIndex="1" style="font-size:12px;color:#333333;fill:#333333;" y="20">
                            <tspan style="font-size: 10px">Jan</tspan>
                            <tspan style="fill:#90ed7d" x="8" dy="15">●</tspan>
                            <tspan dx="0"> Berlin: </tspan>
                            <tspan style="font-weight:bold" dx="0">-0.9°C</tspan>
                        </text>
                    </g>
                    <text x="1576" text-anchor="end" zIndex="8" style="cursor:pointer;color:#909090;font-size:9px;fill:#909090;" y="495">Highcharts.com</text>
                </svg>
            </div>
        </div>
        <!--- charts --->
        <div class="row">
        	<div class="col-xs-12 col-lg-10 col-lg-offset-1">
		        <div class="compaign-report-table">
        	        <div class="re-table">
        	        	<div class="table-scrollable">
        	                <table class="table table-bordered">
        	                    <thead>
        	                        <tr>
        	                            <th>Subscribers</th>
        	                            <th>Number of Messages Sent</th>
        	                            <th>Opt In’s</th>
        	                            <th>Opt Out’s</th>
                                        <th>Response Percentage</th>
        	                        </tr>
        	                    </thead>
        	                    <tbody>
        	                        <tr>
        	                            <td>xxxxxxx</td>
        	                            <td>50</td>
        	                            <td>21</td>
                                        <td>7</td>
                                        <td>80%</td>
        	                        </tr>
                                    <!---  --->
                                    <tr>
                                        <td>xxxxxxx</td>
                                        <td>50</td>
                                        <td>21</td>
                                        <td>7</td>
                                        <td>80%</td>
                                    </tr>
                                    <!---  --->
                                    <tr>
                                        <td>xxxxxxx</td>
                                        <td>50</td>
                                        <td>21</td>
                                        <td>7</td>
                                        <td>80%</td>
                                    </tr>
                                    <!---  --->
                                    <tr>
                                        <td>xxxxxxx</td>
                                        <td>50</td>
                                        <td>21</td>
                                        <td>7</td>
                                        <td>80%</td>
                                    </tr>
                                    <!---  --->
                                    <tr>
                                        <td>xxxxxxx</td>
                                        <td>50</td>
                                        <td>21</td>
                                        <td>7</td>
                                        <td>80%</td>
                                    </tr>
        	                    </tbody>
        	                </table>
        	            </div>
        	        </div>
		        </div>
        	</div>
        </div>
        <!--- table compaign report --->
        <div class="row">
            <div class="col-xs-12">
                <div class="dataTables_paginate paging_input text-center">
                    <span class="paginate_button first">
                        <img class="paginate-arrow" src="/session/sire/images/double-left.png">
                    </span>
                    <span class="paginate_button previous" id="tblListShortURL_previous">
                        <img class="paginate-arrow" src="/session/sire/images/left.png">
                    </span>
                    <span class="paginate_page">Page </span>
                    <input type="text" class="paginate_text" style="display: inline;" value="1">
                    <span class="paginate_of"> of 1</span>
                    <span class="paginate_button next" id="tblListShortURL_next">
                        <img class="paginate-arrow" src="/session/sire/images/right.png">
                    </span>
                    <span class="paginate_button last" id="tblListShortURL_last">
                        <img class="paginate-arrow" src="/session/sire/images/double-right.png">
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<cfinclude template="../views/layouts/master.cfm">