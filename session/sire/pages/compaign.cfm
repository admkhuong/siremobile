<cfset variables.menuToUse = 2 />
<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addJs("../assets/pages/scripts/compaign.js");
</cfscript>
<div class="portlet light bordered compaign">
    <div class="portlet-body form">
        <div class="row">
        	<div class="col-xs-12 col-sm-6">
        		<h2 class="page-title">Campaigns</h2>
        	</div>
        	<div class="col-xs-12 col-sm-6">
        		<div class="compaign-filter">
	        		<div class="row">
	        			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
	        				<div class="select-compaign">
		        				<div class="select-list">
					                <select class="form-control">
						                <option>All</option>
						                <option>Email</option>
					                </select>
					            </div>
	        				</div>
	        			</div>
	        			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
	        				<div class="select-fillter">
		        				<div class="select-list">
					                <select class="form-control">
						                <option>is</option>
						                <option>is</option>
					                </select>
					            </div>
	        				</div>
	        			</div>
	        			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
	        				<div class="compaign-search">
		        				<input type="" name="" class="form-control">
		        				<i class="fa fa-plus" aria-hidden="true"></i>
		        				<i class="fa fa-search" aria-hidden="true"></i>
	        				</div>
	        			</div>
	        		</div>
        		</div>
        	</div>
        </div>
        <!---  --->
        <div class="row">
        	<div class="col-xs-12">
        		<div class="compaign-table">
        	        <div class="re-table">
        	        	<div class="table-scrollable">
        	                <table class="table table-bordered">
        	                    <thead>
        	                        <tr>
        	                            <th>ID</th>
        	                            <th>Name</th>
        	                            <th>Keyword</th>
        	                            <th>Opt In</th>
        	                            <th>Out</th>
        	                            <th>List</th>
        	                            <th>Template Type</th>
        	                            <th>Action</th>
        	                            <th><i class="fa fa-check" aria-hidden="true"></i></th>
        	                        </tr>
        	                    </thead>
        	                    <tbody>
        	                        <tr>
        	                            <td>163188</td>
        	                            <td>Simple Subscribers (Opt In) <br> 2017-01-23 18:09:13</td>
        	                            <td>GALLERYDAY</td>
        	                            <td>20</td>
        	                            <td>3</td>
        	                            <td>list 1</td>
        	                            <td>build your first campaign</td>
        	                            <td>
        	                            	<div class="btn-re-edit"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </div>
        	                            	<div class="btn-re-xs"><a href="">REPORT</a></div>
        	                            </td>
        	                            <td>
        	                            	<div class="tick-select"><i class="fa fa-square-o" aria-hidden="true"></i></div>
        	                            </td>
        	                        </tr>
        	                        <!---  --->
        	                        <tr>
        	                            <td>163188</td>
        	                            <td>Simple Subscribers (Opt In) <br> 2017-01-23 18:09:13</td>
        	                            <td>GALLERYDAY</td>
        	                            <td>20</td>
        	                            <td>3</td>
        	                            <td>list 1</td>
        	                            <td>build your first campaign</td>
        	                            <td>
        	                            	<div class="btn-re-edit"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </div>
        	                            	<div class="btn-re-xs"><a href="">REPORT</a></div>
        	                            </td>
        	                            <td>
        	                            	<div class="tick-select"><i class="fa fa-square-o" aria-hidden="true"></i></div>
        	                            </td>
        	                        </tr>
        	                        <!---  --->
        	                        <tr>
        	                            <td>163188</td>
        	                            <td>Simple Subscribers (Opt In) <br> 2017-01-23 18:09:13</td>
        	                            <td>GALLERYDAY</td>
        	                            <td>20</td>
        	                            <td>3</td>
        	                            <td>list 1</td>
        	                            <td>build your first campaign</td>
        	                            <td>
        	                            	<div class="btn-re-edit"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </div>
        	                            	<div class="btn-re-xs"><a href="">REPORT</a></div>
        	                            </td>
        	                            <td>
        	                            	<div class="tick-select"><i class="fa fa-square-o" aria-hidden="true"></i></div>
        	                            </td>
        	                        </tr>
        	                        <!---  --->
        	                        <tr>
        	                            <td>163188</td>
        	                            <td>Simple Subscribers (Opt In) <br> 2017-01-23 18:09:13</td>
        	                            <td>GALLERYDAY</td>
        	                            <td>20</td>
        	                            <td>3</td>
        	                            <td>list 1</td>
        	                            <td>build your first campaign</td>
        	                            <td>
        	                            	<div class="btn-re-edit"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </div>
        	                            	<div class="btn-re-xs"><a href="">REPORT</a></div>
        	                            </td>
        	                            <td>
        	                            	<div class="tick-select"><i class="fa fa-square-o" aria-hidden="true"></i></div>
        	                            </td>
        	                        </tr>
        	                        <!---  --->
        	                        <tr>
        	                            <td>163188</td>
        	                            <td>Simple Subscribers (Opt In) <br> 2017-01-23 18:09:13</td>
        	                            <td>GALLERYDAY</td>
        	                            <td>20</td>
        	                            <td>3</td>
        	                            <td>list 1</td>
        	                            <td>build your first campaign</td>
        	                            <td>
        	                            	<div class="btn-re-edit"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </div>
        	                            	<div class="btn-re-xs"><a href="">REPORT</a></div>
        	                            </td>
        	                            <td>
        	                            	<div class="tick-select"><i class="fa fa-square-o" aria-hidden="true"></i></div>
        	                            </td>
        	                        </tr>
        	                    </tbody>
        	                </table>
        	            </div>
        	        </div>
        		</div>
        	</div>
        </div>
        <!---  --->
        <div class="row">
        	<div class="col-xs-12 text-right">
        		<div class="btn-re-dowload"> <i class="fa fa-download" aria-hidden="true"></i> </div>
        		<div class="btn-re-remove"> <i class="fa fa-trash" aria-hidden="true"></i> </div>
        	</div>
        </div>
        <!---  --->
        <div class="row">
        	<div class="col-xs-12">
        		<div class="dataTables_paginate paging_input text-center">
			    	<span class="paginate_button first">
			    		<img class="paginate-arrow" src="/session/sire/images/double-left.png">
			    	</span>
			    	<span class="paginate_button previous" id="tblListShortURL_previous">
			    		<img class="paginate-arrow" src="/session/sire/images/left.png">
			    	</span>
			    	<span class="paginate_page">Page </span>
			    	<input type="text" class="paginate_text" style="display: inline;" value="1">
			    	<span class="paginate_of"> of 1</span>
			    	<span class="paginate_button next" id="tblListShortURL_next">
			    		<img class="paginate-arrow" src="/session/sire/images/right.png">
			    	</span>
			    	<span class="paginate_button last" id="tblListShortURL_last">
			    		<img class="paginate-arrow" src="/session/sire/images/double-right.png">
			    	</span>
			    </div>
        	</div>
        </div>
    </div>
</div>
<cfinclude template="../views/layouts/master.cfm">