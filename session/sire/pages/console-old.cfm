<cfinclude template="/public/paths.cfm" >
<cfinclude template="/session/sire/configs/paths.cfm">
<cfparam name="QA" default="0">


<cfinclude template="../../sire/configs/paths.cfm">

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/campaign.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/sire/css/select2.min.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/sire/css/select2-bootstrap.min.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/sire/js/select2.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/vendors/chartjs/Chart.min.js">
</cfinvoke>

<!--- color scheme for use in chart.js --->
<cfset variables.colorArr= [
	 
   "##77bb77", "##ccdddd", "##eeeeee", "##5599aa",			     
   "##44ee66", "##eeffee", "##cceeee", "##33aacc", "##B36C79", "##FFFC73",
   "##F18922", "##8EC050", "##785A3A", "##1A90C9", "##681D2A", "##FFFA00",
   "##B57A40", "##749051", "##5A4937", "##347797", "##4E242B", "##BFBC30",
   "##9D530B", "##507D1A", "##4E3113", "##085B83", "##440914", "##A6A300",
   "##F8A858", "##B4E07E", "##BC9873", "##4FB3E4", "##B35264", "##FFFB40" 
]>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/public/sire/js/jquery-ui-1.12.1.custom/jquery-ui.min.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/public/sire/js/jquery-ui-1.12.1.custom/jquery-ui.theme.min.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery-ui-1.12.1.custom/jquery-ui.min.js">
</cfinvoke>
			
<style>
	
	.console-box
	{
		position: relative;
		min-height: 4em;
		padding: 1em;
		border: rgba(0, 0, 0, 0.1) solid 1px;
		border-radius: 1em;
	
		margin-bottom: 2em;
		-webkit-box-shadow: 6px 6px 5px 0px rgba(0,0,0,0.55);
		-moz-box-shadow: 6px 6px 5px 0px rgba(0,0,0,0.55);
		box-shadow: 6px 6px 5px 0px rgba(0,0,0,0.55);
	}
	
	.content-body {
    	padding: 2em;
	}
	
	.console-link-box
	{
		color:inherit;
		text-decoration: none;
		
	}
	
	.console-link-box:hover
	{
		color:inherit;
		text-decoration: none;
		
	}
	
	.chart-legend li span{
	    display: inline-block;
	    padding: .2em;
	    margin-bottom: .2em;
	    margin-right: 1em;
	    text-shadow: 1px 1px 2px rgba(255, 255, 255, 1);
	    border-radius: 1em;
	}
	
	.chart-legend ul{
		font-size: .9em;
	    list-style:none;
	    position: absolute;
		top: 50%;
		left: 50%;
		/* bring your own prefixes */
		transform: translate(-60%, -50%);	
		padding-left: 1em;     
	}
	
	
	.center-vertical{
		position: absolute;
		top: 50%;
		left: 50%;
		/* bring your own prefixes */
		transform: translate(-50%, -50%);	     
	}
	
	
	#NoActiveKeywords
	{
		display: none;		
	}
	
	
	.ConsoleLinks
	{
		font-weight: bold;
		display: block;
		font-size: .8em;
	}
	
	.ConsoleLinks:hover
	{
		cursor: pointer;		
	}
	
	
	div.slimview h1, div.slimview h2, div.slimview h3, div.slimview h4, div.slimview h5 
	{
    	margin-top: 0px;
		margin-bottom: 0px;
	}

	.SlimTop
	{
		margin-top: 0;
	}

	.MedFont
	{
		font-size: .8em;
	}
	
	.AlignBottom
	{
		position: absolute;
		bottom: 1em;
		left: 1em;
	}
	
	.OrgInfo
	{
		background-color: rgba(200, 200, 200, 0.1); 
		border-radius: .5em;
		padding: .3em .3em .3em 1em;
		margin: .3em .3em .3em 0;	
		height: 100%;	
	}

	.helper {
	    display: inline-block;
	    height: 100%;
	    vertical-align: middle;
	}

</style>


<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="RetVarGetUserOrganization"></cfinvoke>
			


<cfif RetVarGetUserOrganization.RXRESULTCODE EQ 1>
	<cfset _has_Org = true>
	<cfset orgInfoData = RetVarGetUserOrganization.ORGINFO>
</cfif>

<!--- CHECK FIRST LOGIN --->
<cfinvoke component="public.sire.models.cfc.userstools" method="CheckFirstLogin" returnvariable="checkFirstLoginResult">
	<cfinvokeargument name="InpUserId" value="#Session.USERID#">
</cfinvoke>			
			
<main class="container-fluid page">
	
	<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="RetVarUserInfo"></cfinvoke>

	<cfinclude template="../views/commons/credits_available.cfm">
	
	
	<section class="row bg-white">
		
		<div class="content-body" id="" >

			<cfif checkFirstLoginResult.RXRESULTCODE EQ 1>
				<!-- Button to Trigger Live Demo Video Modal -->
				<a href="#LiveDemoVideoModal" id="LiveDemoVideo" class="btn btn-lg btn-primary hidden" data-toggle="modal">Launch Live Demo Video Modal</a>

				<cfinvoke component="public.sire.models.cfc.userstools" method="UpdateFirstLogedIn" returnvariable="updateFirstLogedInResult">
					<cfinvokeargument name="InpUserId" value="#Session.USERID#">
				</cfinvoke>
			</cfif>
							
			<!--- Keywords --->
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
								
				<h4>Keywords</h4>
				
				<a class="console-link-box" href="campaign-manage">	
									
				<div id="KeywordsContainer" style="display: none;">
					
					<div class="console-box" style="text-align: center; height: 160px;">						
												
							<div id="ProcessingKeywordCounts" style="min-height: 60px; float: left;">
								<img src="/public/sire/images/loading.gif" class="ajax-loader">
								<h4>Loading...</h4>
							</div>
						
							<canvas id="myChartKeywords" width="300" height="300"></canvas>
						
							<div id="Chart-Legend-Keywords" class="chart-legend" style="display: none;"></div>
							
							<!--- Initially styled as hidden but will display if there are no active keywords --->
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" id="NoActiveKeywords">
								Learn More
							</div>	
												
					</div>
					
					
										
					<div class="console-box" style="text-align: center; height: 198px;">						
														
						<!--- Initially styled as hidden but will display if there are no active keywords --->
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" id="NoActiveKeywords">
							Learn More
						</div>	
					
					
						<h3 class="SlimTop">Most Active Keywords</h3>
						<h5>(Last 30 Days)</h5>
							
						<div id="MostActiveKeywords" style="text-align: center;">
								...
						</div>
					
					</div>
															
				</div>	

				</a>
			</div>	
			
			

<!---
				<cfif _has_Org AND orgInfoData.OrganizationLogo_vch NEQ ''>
					
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
						<div class="" style="text-align: center; height: 360px; overflow: hidden;">						
							 <span class="helper"></span><img id="OrgLogo" src="<cfoutput>#_S3IMAGEURL##orgInfoData.OrganizationLogo_vch#</cfoutput>"  />
						</div>
					</div>
				</cfif>
--->
				
			<!--- Subscriber Section --->
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
				
				<a class="console-link-box" href="/session/sire/pages/subscribers">				
				
					<h4>Subscriber</h4>
					
					<div id="ProcessingOptCounts" style="min-height: 400px; width: 100%; float: left;" class="console-box">
						<img src="/public/sire/images/loading.gif" class="ajax-loader">
						<h4>Loading...</h4>
					</div>
										
					<div id="SubscriberActivityContainer" style="display: none;">
					
						<div class="console-box" style="text-align: center; height: 108px; overflow-y: hidden;">						
																					
							<div class="slimview">
								<h1 id="OptInTotal">...</h1>
								<h3>Total Subscribers</h3>
								<h5>(All Lists)</h5>
							</div>
										
						</div>
						
						<div class="console-box" style="text-align: center; height: 108px; overflow-y: hidden;">						
							
							<div class="slimview">
								<h1 id="OptCount">...</h1>
								<h3>New Subscribers</h3>
								<h5>(Last 30 Days)</h5>
							</div>
										
						</div>
						
						<div class="console-box" style="text-align: center; height: 110px; overflow-y: hidden;">						
							
							<div class="slimview">
								<h1 id="OptOutCount">...</h1>
								<h3>Subscriber Opt Outs</h3>
								<h5>(Last 30 Days)</h5>
							</div>
										
						</div>
						
					</div>
				
				</a>
					
			</div>	
			
			<!--- MO/MT Summary Report --->
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
				
				<h4>Activity Counter (Last 30 Days)</h4>
				
				<div class="console-box" style="height: 390px; padding-top: .5em; padding-bottom: .5em;" >
					
					<div id="ProcessingEventCounts" style="min-height: 60px; float: left;">
						<img src="/public/sire/images/loading.gif" class="ajax-loader">
						<h4>Loading...</h4>
					</div>
					
					
					<canvas id="myChartActivity" width="300" height="300"></canvas>
					
					<div id="Chart-Legend-Activity" class="chart-legend" style="display: none;"></div>
					
				</div>
			</div>			
							
					
			<!--- Top three MLP hit counts --->
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4" id="MLPSection">
				
				<a class="console-link-box" href="mlp-list">
					<h4>Marketing Landing Pages (MLP)</h4>
					<div class="console-box" style="height: 400px;">
												
						<div id="ProcessingMLPLoadCounts" style="min-height: 60px; float: left;">
							<img src="/public/sire/images/loading.gif" class="ajax-loader">
							<h4>Loading...</h4>
						</div>
						
						<div id="" class="slimview" style="text-align: center;">
							<h1 id="MLPLoadCount">...</h1>
							<h3>MLP Views</h3>
							<h5>(Last 30 Days)</h5>
						</div>
						
						<BR>
						
						<!--- Initially styled as hidden but will display if there are no active keywords --->
						<div id="NoActiveMLPs">
							A Marketing Landing Page (MLP) is a standalone web page dedicated to a single product, service or campaign. Start by using one of our mobile first design templates or build from scratch. Customize your page elements on the fly. Coding not required.				
						</div>		
						
					</div>
				</a>
				
			</div>	

			

			
			<!--- Org Info --->
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
				
<!--- Read Profile data and put into form --->
				<a class="console-link-box" href="/session/sire/pages/my-account">	
											
				<h4>Organization Profile Information</h4>			
				                    						
					<div id="OrgContainer" style="">
						
						<div class="console-box row" style="text-align: left; background-color: #274e60; height: 400px; color: #ffffff">						
							
							<!--- Calculate percentage complete --->
							<cfset TotalOrgSteps = 13 />
							
							<!--- Assume user name and password are already set from sign up --->
							<cfset TotalStepsComplete = 2 />
							
							<!--- Check for first name --->
							<cfif LEN(TRIM(RetVarUserInfo.USERNAME)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>
	
							<!--- Check for last name --->
							<cfif LEN(TRIM(RetVarUserInfo.LASTNAME)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>
							
							<!--- Check for SMS number --->
							<cfif LEN(TRIM(RetVarUserInfo.FULLMFAPHONE)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>
	
							<!--- Check for ADDRESSe --->
							<cfif LEN(TRIM(RetVarUserInfo.ADDRESS)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>
	
							<!--- Check for POSTALCODE --->
							<cfif LEN(TRIM(RetVarUserInfo.POSTALCODE)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>
							
							<!--- Check for ORGANIZATIONBUSINESSNAME_VCH --->
							<cfif LEN(TRIM(RetVarGetUserOrganization.ORGINFO.ORGANIZATIONBUSINESSNAME_VCH)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>
	
							<!--- Check for ORGANIZATIONNAME_VCH --->
							<cfif LEN(TRIM(RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>
	
							<!--- Check for ORGANIZATIONWEBSITE_VCH --->
							<cfif LEN(TRIM(RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITE_VCH)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>
	
							<!--- Check for ORGANIZATIONWEBSITETINY_VCH --->
							<cfif LEN(TRIM(RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITETINY_VCH)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>
	
							<!--- Check for ORGANIZATIONPHONE_VCH --->
							<cfif LEN(TRIM(RetVarGetUserOrganization.ORGINFO.ORGANIZATIONPHONE_VCH)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>
	
							<!--- Check for ORGANIZATIONEMAIL_VCH --->
							<cfif LEN(TRIM(RetVarGetUserOrganization.ORGINFO.ORGANIZATIONEMAIL_VCH)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>
							
							<div style="text-align: center;"><img src="/public/sire/images/ic_profile.png" id="console-ic-profile" class="" style=""></div>
							<div style="margin-top: 1em;"><cfoutput>#Round((TotalStepsComplete/TotalOrgSteps)*100)#</cfoutput> % Complete</div>
							<div style="margin-top: .5em;" id="ProfileProgressBar"></div>						
									
							<div style="text-align: center; margin-top: 1em;"><a href="/session/sire/pages/my-account" style="color: #ffffff; display: inline;" class="ConsoleLinks">Manage My Account Info</a></div>
													
	<!---						
		
							<cfif (TRIM(RetVarGetUserOrganization.ORGINFO.ORGANIZATIONBUSINESSNAME_VCH) EQ "" OR TRIM(RetVarGetUserOrganization.ORGINFO.ORGANIZATIONBUSINESSNAME_VCH) EQ "") OR QA NEQ 0>			
								<div><span style="color: rgba(226, 110, 110, 0.78)">Warning:</span> <i>Just a little more information is needed to finish configuring your account. We noticed that you have not set up you organization information yet. This information is used by the templates to automatically put in information personalized to your business.</i></div>	
							</cfif>
									
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="">
								
								<cfoutput>								
									
									<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" id=""><b>Profile Option</b></div>
									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" id=""><b>Current Setting</b></div>
									<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" id=""><a data-toggle="modal" href="##" data-target="##InfoModalTemplateCDF"><b>Template CDF Tag</b></a></div>								
									
									<div class="col-xs-12 col-sm-12 hidden-md hidden-lg"><hr></div>
									
									<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" id="" style=""><h5 class="OrgInfo">Full Business Name</h5></div>
									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" id="" style=""><h5 class="OrgInfo">#RetVarGetUserOrganization.ORGINFO.ORGANIZATIONBUSINESSNAME_VCH#&nbsp;</h5></div>
									<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" id="" style=""><h5 class="OrgInfo">{%CompanyName%}</h5></div>
									
									<div class="col-xs-12 col-sm-12 hidden-md hidden-lg"><hr></div>
									
									<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" id="" style=""><h5 class="OrgInfo">Bus. Name Abbr.</h5></div>
									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" id="" style=""><h5 class="OrgInfo">#RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH#&nbsp;</h5></div>
									<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" id="" style=""><h5 class="OrgInfo">{%CompanyNameShort%}</h5></div>
									
									<div class="col-xs-12 col-sm-12 hidden-md hidden-lg"><hr></div>
									
									<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" id="" style=""><h5 class="OrgInfo">Full Business Website</h5></div>
									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" id="" style=""><h5 class="OrgInfo">#RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITE_VCH#&nbsp;</h5></div>
									<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" id="" style=""><h5 class="OrgInfo">{%CompanyWebSite%}</h5></div>
									
									<div class="col-xs-12 col-sm-12 hidden-md hidden-lg"><hr></div>
									
									<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" id="" style=""><h5 class="OrgInfo">Short/Tiny URL Website</h5></div>
									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" id="" style=""><h5 class="OrgInfo">#RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITETINY_VCH#&nbsp;</h5></div>
									<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" id="" style=""><h5 class="OrgInfo">{%CompanyWebSiteShort%}</h5></div>
									
									<div class="col-xs-12 col-sm-12 hidden-md hidden-lg"><hr></div>
									
									<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" id="" style=""><h5 class="OrgInfo">Business Phone Number</h5></div>
									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" id="" style=""><h5 class="OrgInfo">#RetVarGetUserOrganization.ORGINFO.ORGANIZATIONPHONE_VCH#&nbsp;</h5></div>
									<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" id="" style=""><h5 class="OrgInfo">{%CompanyPhoneNumber%}</h5></div>
									
									<div class="col-xs-12 col-sm-12 hidden-md hidden-lg"><hr></div>
									
									<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" id="" style=""><h5 class="OrgInfo">Business eMail</h5></div>
									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" id="" style=""><h5 class="OrgInfo">#RetVarGetUserOrganization.ORGINFO.ORGANIZATIONEMAIL_VCH#&nbsp;</h5></div>
									<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" id="" style=""><h5 class="OrgInfo">{%CompanyEMailAddress%}</h5></div>
									
								</cfoutput>	
							</div>	
											
	--->		
						</div>										
																					
					</div>	
					
				</a>				
			</div>	

			<cfif _has_Org AND orgInfoData.OrganizationLogo_vch NEQ ''>
				
				
					
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
				
				<h4>&nbsp;</h4>
					
					<div class="console-box" style="text-align: center; height: 400px; padding-top: .5em; padding-bottom: .5em;" >
						<span class="helper"></span><img id="OrgLogo" src="<cfoutput>#_S3IMAGEURL##orgInfoData.OrganizationLogo_vch#</cfoutput>"  />
					</div>
				</div>
			</cfif>
			
				
		</div>
	
	</section>
	
</main>


<!--- 
	
	
<!---
				<!--- No subscriber list yet version --->
				<div id="NoSubscriberActivityContainer" style="display: none;">
					
					<div class="console-box" style="min-height: 390px;">
						<h4>You have no Subscriber Lists yet.</h4>
						
						<div class="">
							<h4>Quick Links - Subscribers</h4>
							<a data-toggle="modal" href="##" data-target="#InfoModalSubscriber" class="ConsoleLinks" style="padding-left:2em;">What is a Subscriber?</a>
							<a href="/session/sire/pages/subscribers" style="padding-left:2em;" class="ConsoleLinks">Manage Subscriber Lists</a></li>
							<a href="/session/sire/pages/campaign-manage?tour=list" style="padding-left:2em;" class="ConsoleLinks">Guided Tour - My First Subscriber List</a></li>
							<a href="/public/sire/pages/how-to-use?vplay=3" style="padding-left:2em;" class="ConsoleLinks">Video - How To Create Your First Subscriber List</a></li>
						</div>
						
						<img src="/public/sire/images/learning/opt-in.jpg" style="width: 100%; max-width:400px; max-height: 200px; height: auto;  position: absolute; bottom: 0; right: 0; border-radius: 1em;" />
						
			
<!---
						<p class="MedFont AlignBottom" style="padding-right: 1em;">A <b>Subscriber list</b> is a list of subscribers that have opted into your campaign by texting your keyword to your short code. Sire automatically captures all of your subscribers phone numbers and saves them in your subscriber list. This also is to help keep non-compliant spammers at bay.							
						Subscribers need an easy way to join your lists. Create a new Subscriber List now.</p>
	
--->
						
								
					</div>
				</div>	
--->
		
		
		
						<!--- No keywords yet version --->
				
<!---
				<div id="NoKeywordsContainer" style="display: none;">
					
					<div class="console-box" style="height: 390px; overflow: hidden;">
						<h4>You have not setup any Keywords yet.</h4>
						
	<!---
						<p class="MedFont"><b>Keywords</b> are one of the fastest ways you can begin to communicate with your customers. <b>Keywords</b> are special messages sent to a Short Code. <b>Keywords</b> are used to trigger new Sire SMS sessions. Sessions can include simple response messages, list building via opt in confirmation, surveys, meet-ups, drip marketing, and additional two-way data exchange and capture.</p>
					
	--->	
						
						<h4>Quick Links - Campaigns</h4>
						<a data-toggle="modal" href="##" data-target="#InfoModalKeyword" class="ConsoleLinks" style="padding-left:2em;">What is a Keyword?</a>
						<a href="/session/sire/pages/campaign-template-picker" style="padding-left:2em;" class="ConsoleLinks">Create New Campaign From Template</a></li>
						<a href="/session/sire/pages/campaign-manage?tour=keyword" style="padding-left:2em;" class="ConsoleLinks">Guided Tour - My First Keyword</a></li>
						<a href="/public/sire/pages/how-to-use?vplay=2" style="padding-left:2em;" class="ConsoleLinks">Video - How to create your first campaign</a></li>
						
						<img src="/public/sire/images/learning/sire-special.png" style="float:right; margin-top: 2em;" />
						
											
						
					</div>
					
				</div>	
--->
				<!--- Link to tutorial on what they are - video? Why do you need them? --->
				
	
--->
<cfif checkFirstLoginResult.RXRESULTCODE EQ 1>
	<!--- Live Demo Video Modal --->
	<div id="LiveDemoVideoModal" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                <h4 class="modal-title">How to Create Your First Campaign</h4>
	            </div>
	            <div class="modal-body">
	            	<div class="embed-responsive embed-responsive-16by9">
						<iframe id="ifrmVideo" class="embed-responsive-item" src="https://www.youtube.com/embed/A_tV18dQtSw?rel=0&modestbranding=1" frameborder="0" allowfullscreen></iframe>
	            	</div>
	            </div>
	            <div class="modal-footer">
			        <button type="button" class="btn btn-success-custom" data-dismiss="modal">Close</button>
			      </div>
	        </div>
	    </div>
	</div>
</cfif>
<!-- Modal -->
<div id="InfoModalKeyword" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;"> 
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What is a Keyword?</h4>
      </div>
      <div class="modal-body">
	      
	      <p class="MedFont"><b>Keywords</b> are one of the fastest ways you can begin to communicate with your customers. <b>Keywords</b> are special messages sent to a Short Code. <b>Keywords</b> are used to trigger new Sire SMS sessions. Sessions can include simple response messages, list building via opt in confirmation, surveys, meet-ups, drip marketing, and additional two-way data exchange and capture.</p>
					
		  <img src="/public/sire/images/learning/keyword-special-simple.png" style="width: 100%; max-width:533px; height: auto;" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="InfoModalSubscriber" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;"> 
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What is a Subscriber List?</h4>
      </div>
      <div class="modal-body">
	      
	      <p class="MedFont">A <b>Subscriber List</b> is a list of subscribers that have opted into your campaign by texting your keyword to your short code. Example:“39492”. Sire automatically captures all of your subscribers phone numbers and saves them in your subscriber list. This is also to help keep non-compliant spammers at bay.</p>
					
		  <img src="/public/sire/images/learning/opt-in.jpg" style="width: 100%; max-width:600px; height: auto;" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="InfoModalCDF" data-backdrop-limit="1" tabindex="-1" class="modal fade" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;"> 
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What is a Custom Data Field (CDF)?</h4>
      </div>
      <div class="modal-body">
      	<p>Tools to manage your custom data. Your account. Your data. No Limits! </p>
      	<p>Used within an Action as part of the flow of a conversation, you can store a users response to the previous question and link it to the current phone number.</p>
      	<p>Your company's unique strengths are its competitive advantage. Why should you compromise with vanilla application deployments or other vendor limits? With Sire's Custom Data Fields, you don't have too.</p>
      	<p>Any CDFs in the message flow will be replaced with the data you send as part of the triggerSMS API call. Send these additional CDF values as part of the URL query string in GET and POST requests or as part of the JSON data in the POST requests. </p>
      
	  	<ul>
		  	<li><b>Personalization:</b> Personalize different parts of each message for each individual recipient.</li>
		  	<li><b>Customization:</b> Customize parts of the message to relate to each individual recipient's transaction history. The knowledge that a company has about a customer needs to be put into practice and the information held has to be taken into account in order to be able to give the client exactly what he wants</li>
		  	<li><b>Business Rules and Branching:</b> You can change the flow of a conversation based on previously captured input.</li>
	  	</ul>  	
		  	
		  	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="InfoModalTemplateCDF" data-backdrop-limit="1" tabindex="-1" class="modal fade" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;"> 
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What is a Template Custom Data Field (CDF)?</h4>
      </div>
      <div class="modal-body">
      	<p>When you see these special tags in a Template, the data from your Organization Profile will be auto populated at the time of campaign creation. </p>
      	<p>Enter you Organizational Profile information once,  and it can then be used to standardize your message flows.</p>
      	<p>Most messages you send are REQUIRED to have the business name who is sending them included.</p>
      		
		  	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!---
				<!--- Top three campaigns --->
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
					<h4>WIP</h4>
					<div class="console-box" style="text-align: center; height: 400px;">Work in progress.....</div>
				</div>	
				
				<!--- Top three campaigns --->
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
					<h4>WIP</h4>
					<div class="console-box">Work in progress.....</div>
				</div>			
							
				<!--- Tutorials --->
		
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
					<h4>WIP</h4>
					<div class="console-box">Work in progress....</div>
				</div>			
				
				<!--- Account Summary - balnace - credits fire power --->
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
					<h4>WIP</h4>
					<div class="console-box" style="height: 400px; overflow: hidden; position: relative;">
															
						<img src="/public/sire/images/learning/keyword-special-simple.png" style="height: 100%; width: 100%; max-width: 533px; position: absolute; bottom: 0; right: 0;" />
							
					</div>
				</div>	
				
				
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
					<h4>WIP</h4>
					<div class="console-box" style="height: 400px; overflow: hidden; position: relative;">
															
						<img src="/public/sire/images/learning/outdoor-chalkboard-special.png" style="width: auto; height: 400px; position: absolute; bottom: 0; right: 0;" />
							
					</div>
				</div>	
				
--->


<cfparam name="variables._title" default="Console">
<cfinclude template="../views/layouts/main.cfm">


     
            
            

<!--- Page Tours using bootstrp tour - see http://http://bootstraptour.com/api/ --->
<script type="application/javascript">

	<cfoutput> 
		var #toScript(colorArr, "colorArr")#; 
	</cfoutput> 
		
	<!--- Set up page tour for new users --->
	$(function() {		
		
		$( "#ProfileProgressBar" ).progressbar({
	      value: <cfoutput>#(TotalStepsComplete/TotalOrgSteps)*100#</cfoutput>
	     
	  	      
	    });
    
		LoadSMSEventCounts();
		GetKeywordsCountInUse();
		GetOptInCounts();
		GetMLPLoadCounts();
			
	});
			
	<!--- Check for stats asyncronously --->
	function LoadSMSEventCounts()
	{								
		$('#ProcessingEventCounts').show();
				
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/reports/sms.cfc?method=ConsoleSummaryIRECounts&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: true,
			data:  
			{ 					
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { $('#ProcessingEventCounts').hide(); <!--- SQUASH ERRORS No result returned  bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); ---> },					  
			success:		
			<!--- Default return function for Async call back --->
			function(d) 
			{			
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1) 
				{	
					
					$('#ProcessingEventCounts').hide();
																														
					var ctx = $("#myChartActivity");
	
					var myChartActivity = new Chart(ctx, {
					    type: 'doughnut',
					    animation:{
							animateRotate:true
			    		},
			    		data: {
						   
					        labels: ["MO(s)","MT(s)","API <span class='hidden-xs'>Call(s)</span>"],						        
					        datasets: [{
					            data: [d.MOCOUNT, d.MTCOUNT, d.APICOUNT],  
					            backgroundColor: colorArr, 
					            borderColor: [
					                'rgba(255,255,255,1)',
					                'rgba(255,255,255,1)',
					                'rgba(255,255,255,1)',
					                'rgba(255,255,255,1)',
					                'rgba(255,255,255,1)',
					                'rgba(255,255,255,1)'						              
					            ],
					            borderWidth: 3
					        }]
					    },
					    options: {
						   		responsive: true,
						   		maintainAspectRatio: false,
						   		cutoutPercentage: 80,
						   		legend: {
									display: false,
								},
								tooltips: {
									enabled: false,
								},
								<!--- Legend callback string --->
								legendCallback: function(chart) {
		
									var text = [];
					                text.push('<ul>');
					                for (var i=0; i<chart.data.datasets[0].data.length; i++) {
					                    text.push('<li>');
					                    text.push('<span style="background-color:' + chart.data.datasets[0].backgroundColor[i] + '">' + chart.data.datasets[0].data[i] + '</span>');
					                    if (chart.data.labels[i]) {
					                        text.push(chart.data.labels[i]);
					                    }
					                    text.push('</li>');
					                }
					                text.push('</ul>');
					                return text.join("");
								   
								}	

							}
					        
					        
						});			
						
						<!--- Special chart.js method to generate legend where you want it. Style is with your own CSS to ctrol flow. --->
						$('#Chart-Legend-Activity').html(myChartActivity.generateLegend());
						$('#Chart-Legend-Activity').fadeIn(400);
						
						
						
						
				}
				else
				{
					<!--- No result returned - SQUASH ERRORS--->		
					<!--- bootbox.alert("General Error processing your request."); --->
					$('#ProcessingEventCounts').hide();
				}			
								
			} 		
				
		});		
	}


	<!--- Check for stats asyncronously --->
	function GetKeywordsCountInUse()
	{	
					
		$('#ProcessingKeywordCounts').show();
				
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/control-point.cfc?method=ConsoleSummaryKeywords&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: true,
			data:  
			{ 	
				inpLimit: 5				
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { $('#ProcessingKeywordCounts').hide(); <!--- SQUASH ERRORS No result returned  bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); ---> },					  
			success:		
			<!--- Default return function for Async call back --->
			function(d) 
			{			
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1) 
				{	
					$('#ProcessingKeywordCounts').hide();
					
					$('#KeywordsContainer').show();
											
					var ctx = $("#myChartKeywords");

					var myChartKeywords = new Chart(ctx, {
					    type: 'doughnut',
					    animation:{
							animateRotate:true
			    		},
			    		data: {
						   
					        labels: ["Active","Limit"],						        
					        datasets: [{
					            data: [d.KEYWORDSINUSE, d.KEYWORDLIMIT],  
					            backgroundColor: ['#D46A6A','#ccdddd' ], 
					            borderColor: [
					                'rgba(255,255,255,1)',
					                'rgba(255,255,255,1)',
					                'rgba(255,255,255,1)',
					                'rgba(255,255,255,1)',
					                'rgba(255,255,255,1)',
					                'rgba(255,255,255,1)'						              
					            ],
					            borderWidth: 3
					        }]
					    },
					    options: {
						   		responsive: true,
						   		maintainAspectRatio: false,
						   		cutoutPercentage: 80,
						   		legend: {
									display: false,
								},
								tooltips: {
									enabled: false,
								},
								<!--- Legend callback string --->
								legendCallback: function(chart) {
		
									var text = [];
					                text.push('<ul>');
					                for (var i=0; i<chart.data.datasets[0].data.length; i++) {
					                    text.push('<li>');
					                    text.push('<span style="background-color:' + chart.data.datasets[0].backgroundColor[i] + '">' + chart.data.datasets[0].data[i] + '</span>');
					                    if (chart.data.labels[i]) {
					                        text.push(chart.data.labels[i]);
					                    }
					                    text.push('</li>');
					                }
					                text.push('</ul>');
					                return text.join("");
								   
								}	

							}
					        
					        
						});			
					
					<!--- Special chart.js method to generate legend where you want it. Style is with your own CSS to ctrol flow. --->
					$('#Chart-Legend-Keywords').html(myChartKeywords.generateLegend());
					$('#Chart-Legend-Keywords').fadeIn(400);
						
					<!--- Loop over top keywords here --->
					var output = [];   
					
   
					$.each(d.TOPACTIVE , function(i, val) 
					{ 
					  <!--- HTML Encode Text for Display - needs to be 'quote safe' --->
					  <!--- Use the Physical - non-changing ID for each reference --->
					  
					  if(parseInt(d.TOPACTIVE[i].BATCHID) > 0)
					  {
							output.push('<div><a class="ConsoleLinks" title="'+ d.TOPACTIVE[i].DESC +'" href="campaign-edit?campaignid=' + d.TOPACTIVE[i].BATCHID +'">'+ d.TOPACTIVE[i].KEYWORD +'</a></div>');		  
					  }
					  else
					  {
						  output.push('<div>'+ d.TOPACTIVE[i].DESC + '</div>');						  
					  }
					
					});
					
					<!--- Only hit DOM once to add all of the options --->
					$("#MostActiveKeywords").html(output.join(''));
					
				}
				else
				{
					<!--- No result returned - SQUASH ERRORS--->		
					<!--- bootbox.alert("General Error processing your request."); --->
					$('#ProcessingKeywordCounts').hide();
				}			
								
			} 		
				
		});		
	}


	<!--- Check for stats asyncronously --->
	function GetOptInCounts()
	{						
		$('#ProcessingOptCounts').show();
				
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/reports/opt.cfc?method=ConsoleSummaryOptCounts&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: true,
			data:  
			{ 					
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { $('#ProcessingOptCounts').hide(); <!--- SQUASH ERRORS No result returned  bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); ---> },					  
			success:		
			<!--- Default return function for Async call back --->
			function(d) 
			{			
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1) 
				{	
					$('#ProcessingOptCounts').hide();
																							
					$("#OptCount").html(d.OPTINCOUNT);	
					$("#OptOutCount").html(d.OPTOUTCOUNT);
					$("#OptInTotal").html(d.OPTINTOTALCOUNT);
					
					$('#SubscriberActivityContainer').show();
				}
				else
				{
					<!--- No result returned - SQUASH ERRORS--->		
					<!--- bootbox.alert("General Error processing your request."); --->
					$('#ProcessingOptCounts').hide();
				}			
								
			} 		
				
		});		
	}


	<!--- Check for stats asyncronously --->
	function GetMLPLoadCounts()
	{	
		$('#MLPSection').hide();
							
		$('#ProcessingMLPLoadCounts').show();
				
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/reports/mlp.cfc?method=ConsoleSummaryMLPCounts&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: true,
			data:  
			{ 					
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { $('#ProcessingMLPLoadCounts').hide(); <!--- SQUASH ERRORS No result returned  bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); ---> },					  
			success:		
			<!--- Default return function for Async call back --->
			function(d) 
			{			
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1) 
				{	
					
					if(parseInt(d.MLPACTIVECOUNT) > 0  && '<cfoutput>#QA#</cfoutput>' == '0')
					{
					
						$('#MLPSection').show();
					
						$('#ProcessingMLPLoadCounts').hide();
																								
						$("#MLPLoadCount").html(d.MLPLOADCOUNT);	
											
						$('#MLPLoadCountContainer').show();
						
					}
					else
					{
						$('#MLPSection').hide();
						
						
					}
						
				}
				else
				{
					<!--- No result returned - SQUASH ERRORS--->		
					<!--- bootbox.alert("General Error processing your request."); --->
					$('#ProcessingMLPLoadCounts').hide();
				}			
								
			} 		
				
		});		
	}

	$(document).ready(function($) {

		if($("#LiveDemoVideo").length){
			$( "#LiveDemoVideo" ).trigger( "click" );
		}
		<!--- JS for Live Demo Video modal --->
		/* Get iframe src attribute value i.e. YouTube video url
	    and store it in a variable */
	    var videoFrame = $("#ifrmVideo");
	    
	    /* Assign empty url value to the iframe src attribute when
	    modal hide, which stop the video playing */
	    $("#LiveDemoVideoModal").on('hide.bs.modal', function(){
	        videoFrame.attr('src', '');
	    });


	});


</script>

