
<cfparam name="action" default="" />
<cfparam name="INPBATCHID" default="#campaignId#" />
<cfif action EQ "edit">	
	<input type="hidden" value='<cfoutput>#serializeJSON(SCHEDULE)#</cfoutput>' id="SCHEDULEDATA">
</cfif>

<input type="hidden" value='<cfoutput>#INPBATCHID#</cfoutput>' id="INPBATCHID" name="INPBATCHID">

<!---<div class="modal-body">--->
    
<div id='SetScheduleSBDiv' class="RXFormXXX">
    <div id="RightStage">
        <div class="myScheduleType">
            <div class="clearfix">                                       
                <div class="row">
                    <div class="col-md-5">
                        <div class="form">
                            <fieldset  class="scheduler-border">
                            <legend class="scheduler-border">Date Range:</legend>
                            <div class="form-group col-xs-11">
                                <label >Start Date (mm/dd/yy):</label>                               
                                <div class="input-group datetimepicker date set-z-index" id="pkAcrossMultipleDaysStartDate">
                                    <input type="text" class="form-control" value="" id="AcrossMultipleDaysStartDate">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>                                  
                            </div>
                                                        
                            <div class="form-group col-xs-11">
                                <label >End Date (mm/dd/yy):</label>
                                <div class="input-group datetimepicker date  set-z-index" id="pkAcrossMultipleDaysEndDate">
                                    <input type="text" class="form-control" value="" id="AcrossMultipleDaysEndDate">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>   
                            </div>
                                </fieldset>
                        </div>
                    </div>
                    <div class="col-md-5">
                        
                        <fieldset class="scheduler-border">
                        <legend class="scheduler-border">Time Range:</legend>
                        <div class="form-inline">
                            <label >Start Time</label>
                            <div>
                                <select id="FollowingTimeStart_hour" class="hour form-control " customfield="start">
                            
                                </select>                            
                                <select id="FollowingTimeStart_minute" class="minute form-control ">
                            
                                </select>
                                <select id="FollowingTimeStart_noon_option" class="noon_list form-control" customfield="start">
                                
                                </select>
                            </div>
                        </div>
                        <div class="form-inline" style="margin-top: 15px;margin-bottom: 15px;">
                            <label >End Time</label>
                            <div>
                                <select id="FollowingTimeEnd_hour" class="hour form-control " customfield="end">
                                
                                </select>
                                <select id="FollowingTimeEnd_minute" class="minute form-control">
                            
                                </select>
                                <select id="FollowingTimeEnd_noon_option" class="noon_list form-control " customfield="end">
                                
                                </select>
                            </div>
                        </div>
                        </fieldset>
                    </div>


                    <div class="clearfix col-md-12">
                        <a href="##" class="" id="showAdvanceSchedule"> Show more</a>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div id="divDaySelector" style="display:none">
                            <ul id="MyscheduleTabs" class="MyscheduleTabs nav nav-tabs" style="background: none;">
                            </ul>
                            <div class="row_label">
                                <label>
                                    * The campaign will run on <img alt="" width="13" height="13"src="/public/images/mb/check_box.png"/> days of the
                                    week
                                </label>
                            </div>
                        </div>
                    </div>                       
                </div>
            </div>                    

        </div>
    </div>
</div>        
    
    



<script id="tmplScheduleDay" type="text/x-jquery-tmpl">
	<li class="DSSunday">
	    <input type="checkbox" id="chkAcrossMultipleDays_${i}" value="${i}"  {{if i == 0}} checked {{/if}} class="schedule_day_checkbox {{if i == 0}} check_all {{else}} check {{/if}} onclick="SelectScheduleTab(${i})">
	    <a href="#tabs-${i}" class="tab_label">${dayName}</a>
    </li>
</script>

<script id="tmplScheduleDayTime" type="text/x-jquery-tmpl">
    <li id="tabs-${i}">
	    <div class="hourRow" >		
		    <div class="row">
			    <label title="Calls will not start until the time LOCALOUTPUT to the phone number is the start time or later." style="display:block;">Start Time</label>         
			    <div class="padding_left5">
				    <select id="cboStartTimeHour_AcrossMultipleDays_${i}" class="hour form-control" customfield="start"></select>
			    </div>
			    <div class="padding_left5">
				    <select id="cboStartTimeMinute_AcrossMultipleDays_${i}" class="minute form-control"></select>
			    </div>
			    <div class="padding_left5">
				    <select id="cboNoonOption_StartTime_${i}" class="noon_list form-control" customfield="start">
				    </select>
			    </div>
		    </div>
									                    
		    <div class="row"> 
			    <label title="Calls still in queue after this time will carry over until tommorow's start time." style="display:block;">End Time</label>            
			    <div class="padding_left5">
				    <select id="cboEndTimeHour_AcrossMultipleDays_${i}" class="hour form-control" customfield="end"></select>
			    </div>
			    <div class="padding_left5">
				    <select id="cboEndTimeMinute_AcrossMultipleDays_${i}" class="minute form-control"></select>
			    </div>
			    <div class="padding_left5">
				    <select id="cboNoonOption_EndTime_${i}" class="noon_list form-control" customfield="end">
				    </select>
			    </div>
		    </div>
	    </div>                         
	    
		<div class="hourRow">
			<div class="row last_row">
				<input type="checkbox" id="chkEnableBlachout_${i}" name="chkEnableBlachout" onclick="VisibleBlackout(${i})">
				<label for="chkEnableBlachout">Enable Blackout Start and End Time</label>
			</div>
			<div id="divBlackoutTime_${i}" style="display: none; clear: both;">
				<div class="row">
					<label style="display:block;">Blackout Start Time</label>
					<div class="padding_left5">
						<select id="cboBlackoutStartTimeHour_AcrossMultipleDays_${i}" class="hour form-control" customfield="blackout start"></select>
					</div>
					<div class="padding_left5">
						<select id="cboNoonOption_BlackOutStartTime_${i}" class="noon_list form-control" customfield="start">
						</select>
					</div>
				</div>
				<div class="row">
					<label style="display:block;">Blackout End Time</label>
					<div class="padding_left5">
						<select id="cboBlackoutEndTimeHour_AcrossMultipleDays_${i}" class="hour form-control" customfield="blackout end"></select>
					</div>
					<div class="padding_left5">
						<select id="cboNoonOption_BlackOutEndTime_${i}" class="noon_list form-control" customfield="end">
						</select>
					</div>
				</div>                          
			</div>
		</div>
    </li>	
</script>
