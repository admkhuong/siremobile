<cfparam name="url.action" default=""/>
<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("/session/sire/assets/global/plugins/Chartist/chartist.min.css", true)
        .addCss("../assets/global/plugins/daterangepicker/daterangepicker.css", true)
        .addJs("../assets/global/plugins/highchart/highcharts.js", true)
        .addJs("../assets/global/plugins/highchart/exporting.js", true)
        .addJs("../assets/global/plugins/mobile-detect/mobile-detect.js", true)
        .addJs("../assets/pages/scripts/jquery.textfill.min.js")
        .addJs("/session/sire/assets/global/plugins/Chartist/chartist.min.js", true)
        .addJs("/public/sire/js/js.cookie.js")
        .addJs("/session/sire/assets/global/scripts/filter-bar-generator.js")
        .addJs("../assets/global/plugins/daterangepicker/moment.min.js", true)
        .addJs("../assets/global/plugins/daterangepicker/daterangepicker.js", true)
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)
        .addJs("/public/sire/js/select2.min.js")
        .addJs("../assets/pages/scripts/dashboard.js");
</cfscript>

<cfinvoke component="session.sire.models.cfc.reports.dashboard" method="GetSentMessageForDashboard" returnvariable="getSentMessageResutl"></cfinvoke>

<cfinvoke component="session.sire.models.cfc.reports.dashboard" method="GetMLPShortURL" returnvariable="getMLPShortURL"></cfinvoke>

<cfinvoke component="session.sire.models.cfc.reports.opt" method="ConsoleSummaryOptCounts" returnvariable="getOpt"></cfinvoke>

<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="userOrgInfo"></cfinvoke>
<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke>
<!--- for integrate user --->

<cfset PopUpAskUpdatePayment=0>
<cfif userInfo.USERLEVEL EQ 3 AND userInfo.USERTYPE EQ 2>
    <cfset SireHigherLevelUserID=userInfo.HIGHERUSERID>

    <cfinvoke component="session.sire.models.cfc.billing" method="CheckExistsPaymentMethod" returnvariable="RetCustomerInfo">
        <cfinvokeargument name="userId" value="#SireHigherLevelUserID#">
    </cfinvoke>
    <cfif RetCustomerInfo.RXRESULTCODE EQ -1>
        <cfset PopUpAskUpdatePayment=1>
    </cfif>
</cfif>
<!--- end: for integrate user --->

<cfset _has_Org = false>
<cfif userOrgInfo.RXRESULTCODE EQ 1>
    <cfset _has_Org = true>
    <cfset orgInfoData = userOrgInfo.ORGINFO>
</cfif>

<input type="hidden" id="custom-stop-message" name="CustomStopMessage_vch" value="<cfif _has_Org><cfoutput>#orgInfoData.CustomStopMessage_vch#</cfoutput></cfif>">
<input type="hidden" id="custom-help-message" name="CustomStopMessage_vch" value="<cfif _has_Org><cfoutput>#orgInfoData.CustomHelpMessage_vch#</cfoutput></cfif>">
<input type="hidden" name="" id="company-name" value="<cfoutput>#TRIM(userOrgInfo.ORGINFO.ORGANIZATIONNAME_VCH)#</cfoutput>">
<cfif url.action EQ 'checkUserCampaign'>
    <cfinvoke component="session.sire.models.cfc.campaign" method="GetCampaignList" returnvariable="reGetCampaignList"></cfinvoke>
    <cfif reGetCampaignList.RXRESULTCODE GT 0>
        <cfif reGetCampaignList.iTotalRecords EQ 0>
            <cflocation url="/session/sire/pages/campaign-template-choice" addtoken="false"/>
        </cfif>
    </cfif>
</cfif>
<!--- <cfif (userInfo.USERTYPE EQ 2 AND userInfo.USERLEVEL LT 3) >
    <cfinclude template="inc_management_dashboard.cfm"/>
</cfif> --->
<div class="row dashboard">
    <div class="col-xs-12 col-sm-6 col-lg-3" id="dashboard-total-message">
        <div class="portlet light bordered">
            <div class="portlet-body form">
                <!--- <div class=""><div id="total-sent" class="total-sent"><span><cfoutput>9999999</cfoutput></span></div></div>
                <img class="img-responsive img-extend" src="/session/sire/assets/layouts/layout4/img/bkg-send-mail.png"> --->
                <div class="total-sent-message">
                    <!--- <div class="total-message-number counter-element-box"><span class="counter-element-value"><cfoutput>#getSentMessageResutl.TOTALMESSAGE#</cfoutput></span></div> --->
                    <div class="total-message-number">
                        <div id="total-sent" class="total-sent">
                            <!--- <Cfset getSentMessageResutl.TOTALMESSAGE = 1000000/> --->
                            <cfif len(getSentMessageResutl.TOTALMESSAGE) GTE 1 AND len(getSentMessageResutl.TOTALMESSAGE) LTE 2>
                                <span style="font-size: 5vw"><cfoutput>#getSentMessageResutl.TOTALMESSAGE#</cfoutput></span>
                             <cfelseif len(getSentMessageResutl.TOTALMESSAGE) EQ 3>
                                <span style="font-size: 4vw"><cfoutput>#getSentMessageResutl.TOTALMESSAGE#</cfoutput></span>
                            <cfelseif len(getSentMessageResutl.TOTALMESSAGE) GT 3 AND len(getSentMessageResutl.TOTALMESSAGE) LTE 5>
                                <span style="font-size: 2.8vw"><cfoutput>#getSentMessageResutl.TOTALMESSAGE#</cfoutput></span>
                            <cfelse>
                                <span style="font-size: 2vw"><cfoutput>#getSentMessageResutl.TOTALMESSAGE#</cfoutput></span>
                            </cfif>
                        </div>
                    </div>
                    <!--- <div class="total-message-number"><div id="total-sent" class="total-sent"><span><cfoutput>999999</cfoutput></span></div></div> --->
                </div>
                <div class="tit-send">
                    <h2 class="dashboard-block-title">Total Messages Sent</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg-3" id="dashboard-total-opt">
        <div class="portlet light bordered">
            <div class="portlet-body form">
                <div class="opts-rate">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="opt-rate" id="opt-rate" style="height: 100px; width: 100%">
                                <!--- <div class="opt-rate-ins"></div>
                                <div class="sep"></div>
                                <div class="opt-rate-outs"></div> --->
                            </div>
                        </div>
                        <div class="col-xs-6" id="optinout-sec">
                            <div class="opt-detail">
                                <section>
                                    <div class="opt-number" id="opt-in-number"><span><cfoutput>#getOpt.OPTINTOTALCOUNT#</cfoutput></span></div>
                                    <div class="opt-text">Opt Ins</div>
                                </section>
                                <section>
                                    <div class="opt-number" id="opt-out-number"><span><cfoutput>#getOpt.OPTOUTTOTALCOUNT#</cfoutput></span></div>
                                    <div class="opt-text">Opt Outs</div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-lg-6" id="dashboard-total-subscriber">
        <div class="portlet light bordered">
            <div class="portlet-body form">
                <div class="dashboard-chart-number">
                    <h2 class="dashboard-block-title">Total Number of Subscribers: <span id="all-sub"></span></h2>
                    <div class="chart-number-subscriber">
                        <div id="chart_1" class="">
                            <div class="amcharts-main-div">
                                <div class="amcharts-chart-div" id="subscriber-detail-chart" style="position: relative; text-align: left; width: 100%; padding: 0px;">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row dashboard">
    <div class="col-xs-12 col-sm-12 col-lg-12">
        <div class="portlet light bordered" id="activity-feed-block">
            <div class="portlet-body form">
                <div class="activity-feed">
                    <h2 class="dashboard-block-title">Activity Feed</h2>
                    <div class="tbl-activity-feed">
                        <div class="re-table">
                            <div class="table-scrollable">
                                <table id="activityList" class="table table-striped table-bordered table-responsive">
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="text-right">
                        <div class="see-more">
                            <span>See More</span> <i class="fa fa-caret-down" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row dashboard">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
        <div class="portlet light bordered" id="dashboard-campaign-block">
            <div class="portlet-body form">
                <div class="compaigns">
                    <h2 class="dashboard-block-title">Campaigns</h2>
                    <div class="compaigns-table">
                        <div class="re-table">
                            <div class="table-scrollable">

                                <table id="tblListEMS" class="table table-responsive table-striped table-bordered" style="table-layout:fixed">

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
        <div class="portlet light bordered dashboard">
            <div class="portlet-body form">
                <div class="archive mlp-shorturl-dashboard">
                    <div class="row" id="mlp-shorturl-details">
                        <div class="col-xs-12 col-sm-12 shorturlchart-section" style="height: 290px">
                            <h4 class="dashboard-block-title text-center mlp-url-title">Short URLs: <span id="short-url-total"><cfoutput>#getMLPShortURL.TOTALSHORTURL#</cfoutput></span></h4>
                            <div class="archive-shorturl">
                                <!--- <input type="hidden" name="" id="shorturl-percent-complete" value="6"> --->
                                <div class="row">
                                    <div class="mlp-shorturl-chart" id="shorturl-chart">

                                    </div>
                                </div>

                                <div class="archive-shorturl-content">
                                    <div class="archive-shorturl-figure">
                                        <div class="number pie_value" id="short-url-click"><span><cfoutput>#getMLPShortURL.TOTALCLICKSHORTURL#</cfoutput></span></div>
                                        <div class="text">clicks</div>
                                    </div>
                                    <!--- <input type="text" id="archive-shorturl" class="dial"> --->
                                </div>


                            </div>

                        </div>
                        <div class="col-xs-12 col-sm-12 mlpchart-section" style="height: 290px">
                            <hr style="margin-top: 35px; margin-bottom: -2px">
                            <h4 class="dashboard-block-title text-center mlp-url-title">MLPs: <span id="mlp-total"><cfoutput>#getMLPShortURL.MLPACTIVE#</cfoutput></span></h4>
                            <div class="mlps">
                                <!--- <input type="hidden" name="" id="mlps-percent-complete" value="11"> --->
                                <div class="row">
                                    <div class="mlp-shorturl-chart" id="mlp-chart">

                                    </div>
                                </div>

                                <div class="mlps-content">
                                    <div class="mlps-figure">
                                        <div class="number pie_value" id="mlp-views"><span><cfoutput>#getMLPShortURL.MLPVIEWS#</cfoutput></span></div>
                                        <div class="text">views</div>
                                    </div>
                                    <!--- <input type="text" id="mlps" class="dial"> --->
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
        <div class="portlet light bordered" id="dashboard-keyword-block">
            <div class="portlet-body form">
                <div class="dashboard-keyword">
                    <h2 class="dashboard-block-title">Keywords</h2>
                    <div class="dashboard-keyword-thumb">
                        <img src="../assets/layouts/layout4/img/bkg-keyword.png">
                    </div>
                    <div class="keyword-table">
                        <div class="re-table">
                            <div class="table-scrollable">
                                <table id="keywordListDetail" class="table table-bordered table-striped">

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--- Modal For Enterprise Users report --->
<div class="modal fade" id="total-short-url-click-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">Short URLs clicks details</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="shortURLClickFilterBox">

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="total-short-url-click-table" class="table table-striped table-responsive">

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-shorturl-click-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>

<!--- Modal For Enterprise Users report --->
<div class="modal fade" id="total-mlp-view-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">MLP views details</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="mlpViewFilterBox">

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="total-mlp-view-table" class="table table-striped table-responsive">

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-mlp-view-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>

<cfinclude template="inc_dashboard_slide_integrate.cfm">


<cfinclude template="../views/commons/complete_profile_modal.cfm">
<script>
    userId= <cfoutput>#Session.UserId#</cfoutput>
</script>

<cfparam name="variables._title" default="Dashboard - Sire">
<cfinclude template="/session/sire/views/layouts/master3.cfm">
<cfif PopUpAskUpdatePayment EQ 1>
    <script>
        bootbox.dialog({
            message: '<p>Your manage payment method is either incomplete or not valid. Please contact your manage to get detail info.</p>',
            title: '<h4 class="be-modal-title">Oops!</h4>',
            className: "",
            buttons: {
                success: {
                    label: "OK",
                    className: "btn btn-medium green-gd",
                    callback: function(result) {
                        //
                    }
                }
            }
        });

    </script>
</cfif>
