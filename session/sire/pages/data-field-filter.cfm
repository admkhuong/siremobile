<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)
        .addJs("../assets/pages/scripts/data-field-filter.js");

</cfscript>

<div class="portlet light bordered">
	<div class="portlet-body">
		<h4 class="portlet-heading master2-heading mb-0">Data Field Filters</h4>
		
		<div class="uk-grid uk-grid-upsmall uk-margin-medium-top" uk-grid>
			<div class="uk-width-1-2@xl uk-width-3-5@l">
				<div class="uk-grid " uk-margin>
					<div class="uk-width-1-2 uk-width-1-3@s">
						<ul class="uk-nav nav-label">
							<li><b>Total Import:</b> 65,000</li>
						</ul>
					</div>
					<div class="uk-width-1-2 uk-width-1-3@s">
						<ul class="uk-nav nav-label">
							<li><b>Total Send:</b> 60,00</li>
						</ul>
					</div>
					<div class="uk-width-1-2 uk-width-1-3@s">
						<ul class="uk-nav nav-label">
							<li><b>Total Remaining:</b> 5,000</li>
						</ul>
					</div>
					<div class="uk-width-1-2 uk-width-1-3@s">
						<ul class="uk-nav nav-label">
							<li><b>Batch ID:</b> 125</li>
						</ul>
					</div>
					<div class="uk-width-1-2 uk-width-1-3@s">
						<ul class="uk-nav nav-label">
							<li><b>Campaign Type:</b> Campaign Blast</li>
						</ul>
					</div>
					<div class="uk-width-1-2 uk-width-1-3@s">
						<ul class="uk-nav nav-label">
							<li><b>Campaign Name:</b> Campaign_3</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="uk-grid uk-grid-upsmall uk-margin-medium-top" uk-grid>
			<div class="uk-width-4-6@xl uk-width-4-5@l">
                <div class="uk-grid uk-grid-upsmall uk-flex-middle" uk-margin>
                	<div class="uk-width-auto">
                		<button type="button" class="btn green-cancel"><i class="fa fa-minus"></i></button>
                	</div>
                	<div class="uk-width-1-5@s">
                		<select name="" id="" class="form-control">
                    		<option value="">State</option>
                    		<option value="">State 1</option>
                    		<option value="">State 2</option>
                    	</select>
                	</div>
                	<div class="uk-width-1-5@s">
                		<select name="" id="" class="form-control">
                    		<option value="">Equal</option>
                    		<option value="">Equal 1</option>
                    		<option value="">Equal 2</option>
                    	</select>
                	</div>
                	<div class="uk-width-1-6@s">
                		<input type="text" class="form-control" placeholder="CA" />
                	</div>
                	<div class="uk-width-expand">
                		<label for="" class="color-5c"><b>Total left eligible to send: </b>1,000</label>
                	</div>
                </div>

                <div class="uk-grid uk-grid-upsmall uk-flex-middle" uk-margin>
                	<div class="uk-width-auto">
                		<button type="button" class="btn green-cancel"><i class="fa fa-plus"></i></button>
                	</div>
                </div>
			</div>

			<div class="uk-width-4-6@xl uk-width-4-5@l uk-margin-medium-top uk-margin-small-bottom">
				<div class="uk-grid uk-grid-upsmall uk-flex-middle" uk-margin>
                	<div class="uk-width-auto">
                		<label for=""><b>Send Partial</b></label>
                	</div>
                	<div class="uk-width-1-5@s">
                		<input type="text" class="form-control" placeholder="5,000" />
                	</div>
                	<div class="uk-width-expand">
                		<button type="button" class="btn green-cancel mr-15">send all</button>
                		<button type="button" class="btn green-cancel green-cancel-trash">delete</button>
                	</div>
                </div>
			</div>
		</div>
	</div>
</div>

<div class="portlet light bordered">
	<div class="portlet-body">
		<h4 class="portlet-heading master2-heading mb-0">File Allocation History</h4>
		
		<div class="uk-grid uk-grid-upsmall uk-margin-medium-top" uk-grid>
			<div class="uk-width-2-5@l">
				<div class="uk-grid " uk-margin>
					<div class="uk-width-1-2">
						<ul class="uk-nav nav-label">
							<li><b>File Name:</b> Marketing_2017.csv</li>
						</ul>
					</div>
					<div class="uk-width-1-2">
						<ul class="uk-nav nav-label">
							<li><b>Import Date:</b> 01/01/2017 datetime</li>
						</ul>
					</div>
					<div class="uk-width-1-2">
						<ul class="uk-nav nav-label">
							<li><b>Org Cnt:</b> 70,000</li>
						</ul>
					</div>
					<div class="uk-width-1-2">
						<ul class="uk-nav nav-label">
							<li><b>Import Cnt:</b> 65,000</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="uk-grid">
			<div class="uk-grid-width-1-1">
				<i>Put Table Here</i>
			</div>
		</div>
	</div>
</div>


<cfparam name="variables._title" default="Admin Data Field Filter - Sire">

<cfinclude template="../views/layouts/master2.cfm">