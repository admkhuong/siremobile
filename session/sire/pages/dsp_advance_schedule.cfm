
<cfparam name="mode" default="" />
<cfparam name="INPBATCHID" default="#campaignid#" />

<cfif mode EQ "edit">	
	<input type="hidden" value='<cfoutput>#serializeJSON(SCHEDULE)#</cfoutput>' id="SCHEDULEDATA">
</cfif>

<input type="hidden" value='<cfoutput>#INPBATCHID#</cfoutput>' id="INPBATCHID" name="INPBATCHID">

<div class="modal-body">
    <div id="AdvanceScheduleContent">
        <div id='SetScheduleSBDiv' class="RXFormXXX">
            <div id="RightStage">
                <div class="myScheduleType">
                    <div class="clearfix">
                        <!---
                        <div class="clearfix">
                            <input id="rdoSchedultType_1" style="width: 15px;" name="SCHEDULETYPE" type="radio" value="1" checked="checked">
                            <label for="rdoSchedultType_1">Deliver starting on this date</label>
                        </div>
                        --->
                        <div class="row_label">
                            <p class="help-block">
                            You can always change the scheduled time before the campaign is started and while it is
                            paused. All date times in PST.
                            </p>
                        </div>
                        <div class="row row_label">
                            <div class="col-md-5">
                                <div class="form">
                                	<fieldset  class="scheduler-border">
  									<legend class="scheduler-border">Date Range:</legend>
                                    <div class="form-group form-inline">
                                        <span class="following_time_label">Start Date (mm/dd/yy)</span>
                                        <select id="AcrossMultipleDaysStartDate_Month"
                                                onchange="setValForDatePicker('AcrossMultipleDaysStartDate');"
                                                class="month_list month hidden_date_picker_btn"
                                                style="width:70px;"></select>

                                        <select id="AcrossMultipleDaysStartDate_Day"
                                                onchange="setValForDatePicker('AcrossMultipleDaysStartDate');"
                                                class="day_list margin_left5 day hidden_date_picker_btn"></select>

                                        <select id="AcrossMultipleDaysStartDate_Year"
                                                onchange="setValForDatePicker('AcrossMultipleDaysStartDate');"
                                                class="year_list margin_left5 year hidden_date_picker_btn"></select>
                                        <input type="text" onchange="SelectDate(this)" class="form-control input-sm" value=""
                                               id="AcrossMultipleDaysStartDate" readonly>
                                    </div>
                                    
                                    <div class="form-group form-inline">
                                        <span class="following_time_label">End Date (mm/dd/yy)</span>

                                        <select id="AcrossMultipleDaysEndDate_Month"
                                                onchange="setValForDatePicker('AcrossMultipleDaysEndDate');"
                                                class="month_list month hidden_date_picker_btn" style="width:70px;"
                                                disabled></select>

                                        <select id="AcrossMultipleDaysEndDate_Day"
                                                onchange="setValForDatePicker('AcrossMultipleDaysEndDate');"
                                                class="day_list margin_left5 day hidden_date_picker_btn"
                                                disabled></select>

                                        <select id="AcrossMultipleDaysEndDate_Year"
                                                onchange="setValForDatePicker('AcrossMultipleDaysEndDate');"
                                                class="year_list margin_left5 year hidden_date_picker_btn"
                                                disabled></select>
                                        <input type="text" onchange="SelectDate(this)" class="form-control input-sm" value=""
                                               id="AcrossMultipleDaysEndDate" readonly>
                                    </div>
                                     </fieldset>
                                </div>
                            </div>
                            <div class="col-md-5 start-end-time">
                            	
                                <fieldset class="scheduler-border">
								<legend class="scheduler-border">Time Range:</legend>
                                <div class="form-inline">
                                    <label style="display:block;">Start Time</label>
                                    <select id="FollowingTimeStart_hour" class="hour form-control input-sm" customfield="start"></select>
                                    <select id="FollowingTimeStart_minute" class="minute form-control input-sm"></select>
                                    <select id="FollowingTimeStart_noon_option" class="noon_list form-control input-sm"
                                            customfield="start"></select>
                                </div>
                                <div class="form-inline" style="margin-top: 10px;margin-bottom: 10px;">
                                    <label style="display:block;">End Time</label>
                                    <select id="FollowingTimeEnd_hour" class="hour form-control input-sm" customfield="end"></select>
                                    <select id="FollowingTimeEnd_minute" class="minute form-control input-sm"></select>
                                    <select id="FollowingTimeEnd_noon_option" class="noon_list form-control input-sm"
                                            customfield="end"></select>
                                </div>
                                </fieldset>
                            </div>


                            <div class="clearfix col-md-12">
                                <a href="##" class="" id="showAdvanceSchedule"> Show advance schedule </a>
                            </div>

                            <div class="clearfix"></div>
                            <div class="col-md-12">
                                <div id="divDaySelector" style="display:none">
                                    <ul id="MyscheduleTabs" class="MyscheduleTabs nav nav-tabs" style="background: none;">
                                    </ul>
                                    <div class="row_label">
                                        <label>
                                            * The campaign will run on <img alt="" width="13" height="13"src="/public/images/mb/check_box.png"/> days of the
                                            week
                                        </label>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>
                    <!---
                    <div class="clearfix">
                        <div class="clearfix">
                            <input id="rdoSchedultType_2" style="width: 15px;" name="SCHEDULETYPE" type="radio" value="2">
                            <label for="rdoSchedultType_2">Deliver on a custom schedule</label>
                        </div>
                        <div class="row_label">
                            You can always change the scheduled time before the campaign is started and while it is
                            paused
                        </div>
                        <div class="row row_label">
                            <div class="col-md-5 form-inline">
                                <span class="across_multiple_days_label">Start Date (mm/dd/yy)</span>
                                <select id="AcrossMultipleDaysStartDate_Month"
                                        onchange="setValForDatePicker('AcrossMultipleDaysStartDate');"
                                        class="month_list month multiple_days hidden_date_picker_btn"></select>
                                <select id="AcrossMultipleDaysStartDate_Day"
                                        onchange="setValForDatePicker('AcrossMultipleDaysStartDate');"
                                        class="day_list day multiple_days hidden_date_picker_btn"></select>
                                <select id="AcrossMultipleDaysStartDate_Year"
                                        onchange="setValForDatePicker('AcrossMultipleDaysStartDate');"
                                        class="year_list year multiple_days hidden_date_picker_btn"></select>
                                <input type="text" onchange="SelectDate(this); DoCheckAll();" class="form-control input-sm" value=""
                                       id="AcrossMultipleDaysStartDate" readonly>
                            </div>
                            <div class="col-md-5 form-inline">
                                <span class="across_multiple_days_label">End Date (mm/dd/yy)</span>
                                <select id="AcrossMultipleDaysEndDate_Month"
                                        onchange="setValForDatePicker('AcrossMultipleDaysEndDate');"
                                        class="month_list month multiple_days hidden_date_picker_btn"></select>
                                <select id="AcrossMultipleDaysEndDate_Day"
                                        onchange="setValForDatePicker('AcrossMultipleDaysEndDate');"
                                        class="day_list day multiple_days hidden_date_picker_btn"></select>
                                <select id="AcrossMultipleDaysEndDate_Year"
                                        onchange="setValForDatePicker('AcrossMultipleDaysEndDate');"
                                        class="year_list year multiple_days hidden_date_picker_btn"></select>
                                <input type="text" onchange="SelectDate(this); DoCheckAll();" class="form-control input-sm" value=""
                                       id="AcrossMultipleDaysEndDate" readonly>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div id="divDaySelector">
                            <ul id="MyscheduleTabs" class="MyscheduleTabs nav nav-tabs" style="background: none;">
                            </ul>
                            <div class="row_label">
                                <label>
                                    * The campaign will run on <img alt="" width="13" height="13"
                                                                    src="/public/images/mb/check_box.png"/> days of the
                                    week
                                </label>
                            </div>
                        </div>
                    </div>
                    --->

                </div>
            </div>
        </div>

    </div>

</div>


<cfoutput>
<div class="modal-footer">
    <button type="button" class="btn btn-success btn-success-custom" onclick="<cfif INPBATCHID GT 0> updateScheduleEdit(); <cfelse>  updateScheduleCreate(); </cfif>">Save changes</button>
    &nbsp; &nbsp; &nbsp; 
    <button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal">Close</button>
</div>
</cfoutput>



<script id="tmplScheduleDay" type="text/x-jquery-tmpl">
	<li class="DSSunday">
	    <input type="checkbox" id="chkAcrossMultipleDays_${i}" value="${i}"  {{if i == 0}} checked {{/if}} class="schedule_day_checkbox {{if i == 0}} check_all {{else}} check {{/if}} onclick="SelectScheduleTab(${i})">
	    <a href="#tabs-${i}" class="tab_label">${dayName}</a>
    </li>
</script>

<script id="tmplScheduleDayTime" type="text/x-jquery-tmpl">
    <li id="tabs-${i}">
	    <div class="hourRow" >		
		    <div class="row">
			    <label title="Calls will not start until the time LOCALOUTPUT to the phone number is the start time or later." style="display:block;">Start Time</label>         
			    <div class="padding_left5">
				    <select id="cboStartTimeHour_AcrossMultipleDays_${i}" class="hour" customfield="start"></select>
			    </div>
			    <div class="padding_left5">
				    <select id="cboStartTimeMinute_AcrossMultipleDays_${i}" class="minute"></select>
			    </div>
			    <div class="padding_left5">
				    <select id="cboNoonOption_StartTime_${i}" class="noon_list" customfield="start">
				    </select>
			    </div>
		    </div>
									                    
		    <div class="row"> 
			    <label title="Calls still in queue after this time will carry over until tommorow's start time." style="display:block;">End Time</label>            
			    <div class="padding_left5">
				    <select id="cboEndTimeHour_AcrossMultipleDays_${i}" class="hour" customfield="end"></select>
			    </div>
			    <div class="padding_left5">
				    <select id="cboEndTimeMinute_AcrossMultipleDays_${i}" class="minute"></select>
			    </div>
			    <div class="padding_left5">
				    <select id="cboNoonOption_EndTime_${i}" class="noon_list" customfield="end">
				    </select>
			    </div>
		    </div>
	    </div>                         
	    
		<div class="hourRow">
			<div class="row last_row">
				<input type="checkbox" id="chkEnableBlachout_${i}" name="chkEnableBlachout" onclick="VisibleBlackout(${i})">
				<label for="chkEnableBlachout">Enable Blackout Start and End Time</label>
			</div>
			<div id="divBlackoutTime_${i}" style="display: none; clear: both;">
				<div class="row">
					<label style="display:block;">Blackout Start Time</label>
					<div class="padding_left5">
						<select id="cboBlackoutStartTimeHour_AcrossMultipleDays_${i}" class="hour" customfield="blackout start"></select>
					</div>
					<div class="padding_left5">
						<select id="cboNoonOption_BlackOutStartTime_${i}" class="noon_list" customfield="start">
						</select>
					</div>
				</div>
				<div class="row">
					<label style="display:block;">Blackout End Time</label>
					<div class="padding_left5">
						<select id="cboBlackoutEndTimeHour_AcrossMultipleDays_${i}" class="hour" customfield="blackout end"></select>
					</div>
					<div class="padding_left5">
						<select id="cboNoonOption_BlackOutEndTime_${i}" class="noon_list" customfield="end">
						</select>
					</div>
				</div>                          
			</div>
		</div>
    </li>	
</script>
