<cfparam name="INPGROUPID" default="0">
<cfparam name="INPSHORTCODE" default="0">
<cfparam name="CONTACTTYPES" default="">
<cfparam name="PRICEPERMESS" default="1">
<!---params below are used in campaigncontrolconsole page when we launching ems --->
<cfparam name="ISINLISTFORM" default="0">
<cfparam name="INPSCRIPTID" default="0">
<cfparam name="INPLIBID" default="0">
<cfparam name="INPELEID" default="0">
<cfparam name="INPBATCHID" default="0">
<cfparam name="CDFDATA" default="">
<cfparam name="CUSTOMSTRING" default="">
<cfparam name="NOTE" default="">
<cfparam name="ISAPPLYFILTER" default="1">

<cfinvoke component="session.cfc.emergencymessages" method="GetConfirmationData" returnvariable="retValGETEMSConfirmationData">
	<cfinvokeargument name="INPGROUPID" value="#INPGROUPID#" >
	<cfinvokeargument name="INPSHORTCODE" value="#INPSHORTCODE#">
	<cfinvokeargument name="CONTACTTYPES" value="#CONTACTTYPES#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
	<cfinvokeargument name="CDFDATA" value="#CDFDATA#">
	<cfinvokeargument name="CUSTOMSTRING" value="#CUSTOMSTRING#">
</cfinvoke>

<cfinvoke component="session.sire.models.cfc.subscribers" method="GetSubcriberList" returnvariable="groupList"></cfinvoke>

<cfset subcriberList = groupList.listData>
<cfloop array ="#subcriberList#" index="item">
	<cfif INPGROUPID EQ item[1]>
		<cfset UNITCOUNT = item[3]>
		<cfbreak>
	</cfif>
</cfloop>

<cfif retValGETEMSConfirmationData.RXRESULTCODE NEQ 1>
	<div ><h4>Error getting preview data - Please try again. If problem persists, please contact your system administrator.</h4></div>
	<cfexit><!---do not generate anything if error --->
</cfif>

<cfset color_txt = ''>
<cfset subcriber_count = retValGETEMSConfirmationData.ELIGIBLECOUNT>

<cfif subcriber_count EQ 0>
	<cfset color_txt = 'warning'>
</cfif>	

<cfoutput>

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Confirm Launch SMS</h4>     
    </div>
    <div class="modal-body" id="divConfirm">
    	<div class="row">
    		<div class="col-lg-12"> <h4>Are you sure you want to launch this sms?</h4> </div>
    	</div>
    	<div class="row">
			<div class="m_top_10 col-lg-12">
				<div class="left-input col-sm-6 col-xs-8">
	        		<span class="em-lbl" >Eligible contact</span>
				</div>
				<div class="right-input col-sm-6 col-xs-4">		        		
				<!--- <input type="text" readonly="readonly" value="#retValGETEMSConfirmationData.ELIGIBLECOUNT#" name="ELIGIBLECOUNT" id="ELIGIBLECOUNT"> --->
				<input type="text" readonly="readonly" value="#UNITCOUNT#" name="ELIGIBLECOUNT" id="ELIGIBLECOUNT">
				</div>
			</div>	
    	</div>
    	<div class="row">
	    	<!---do not contact --->
			<div class="m_top_10 col-lg-12">
		        <div class="pull-left left-input col-sm-6 col-xs-8">
		        	<span class="em-lbl">Total Credits Needed</span>
				</div>
		        <div class="pull-left right-input col-sm-6 col-xs-4">		        		
				<!--- <input type="text" readonly="readonly" value="#retValGETEMSConfirmationData.UNITCOUNT * TOTALMESSAGE#" name="UNITCOUNT" id="UNITCOUNT"> --->
				<input type="text" readonly="readonly" value="#UNITCOUNT * PRICEPERMESS#" name="UNITCOUNT" id="UNITCOUNT">
				</div>				
		    </div>
		</div>

		<div class="row">
			<!---current balance --->
			<div class="m_top_10 col-lg-12">
		        <div class="pull-left left-input col-sm-6 col-xs-8">
		        	<span class="em-lbl">Current balance</span>
				</div>
		        <div class="pull-left right-input col-sm-6 col-xs-4">		        		
					<input type="text" readonly="readonly" value="#retValGETEMSConfirmationData.CURRENTBALANCE#" name="CURRENTBALANCE" id="CURRENTBALANCE">
				</div>				
		    </div>
		</div>	
		    
    	<!---buttons --->
		<div class="modal-footer">
			<cfoutput>	
			<cfif INPBATCHID EQ 0>
					
				<div class="row ">
					<div class="pull-left">               
		        		<button type="button" class="btn btn-medium btn-success-custom btn-save-for-review" data-dismiss="modal">
		        			Save For Review
		        		</button>
		        		<cfif subcriber_count GT 0>
		        			<button type="button" class="btn btn-medium btn-success-custom"  data-dismiss="modal" onclick ="AddNewEmergency(false);">Launch Now</button>	
		        		</cfif>
	                	
	                </div>
	                <div class="pull-right">               	
	                	<button type="button" class="btn btn-medium btn-back-custom" data-dismiss="modal">Cancel</button>
	                </div>	
        		</div>
			<cfelse>
				<!---INPBATCHID NEQ 0 means this popup is invoked from emergency list when user click button launch --->
				<!---LaunchEMS is js function defined in campaigncontrolconsole --->
                
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        		<button type="button" class="btn btn-primary" onclick ="LauchEmergency(<cfoutput>#inpscriptid#,#inplibid#,#inpeleid#,#inpbatchid#,#inpgroupid#,'#contacttypes#','#note#',#isapplyfilter#</cfoutput>);" <!---data-dismiss="modal"--->>Launch Now</button>
             
			</cfif>
			</cfoutput>	   
		</div>	
		
    </div>    
</cfoutput>

<cfif inpbatchid GT 0>
	<script type="text/javascript">
		//Lauch emergency
		function LauchEmergency(inpscriptid, inplibid, inpeleid, inpbatchid, inpgroupid, contacttypes, note, contactfilter, isapplyfilter){
				
			var currPage = $(this).attr('pageRedirect');
			var data = {
				INPSCRIPTID: '<cfoutput>#retValGETEMSConfirmationData.INPSCRIPTID#</cfoutput>',
				INPLIBID: '<cfoutput>#retValGETEMSConfirmationData.INPLIBID#</cfoutput>',
				INPELEID: '<cfoutput>#retValGETEMSConfirmationData.INPELEID#</cfoutput>',
				INPBATCHID: '<cfoutput>#retValGETEMSConfirmationData.INPBATCHID#</cfoutput>',
				INPBATCHDESC: '<cfoutput>#retValGETEMSConfirmationData.INPBATCHID#</cfoutput>',
				INPGROUPID: '<cfoutput>#retValGETEMSConfirmationData.INPGROUPID#</cfoutput>',
				CONTACTTYPES: '<cfoutput>#retValGETEMSConfirmationData.CONTACTTYPES#</cfoutput>',
				CONTACTFILTER: '<cfoutput>#retValGETEMSConfirmationData.CONTACTFILTER#</cfoutput>',
				NOTE: '<cfoutput>#retValGETEMSConfirmationData.NOTE#</cfoutput>',
				ISAPPLYFILTER:'<cfoutput>#retValGETEMSConfirmationData.ISAPPLYFILTER#</cfoutput>' 
			};
				
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: 'session/cfc/distribution.cfc?method=AddFiltersToQueue&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				async:false,
				data:  data,					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					$.alerts.okButton = '&nbsp;OK&nbsp;';					
					bootbox.alert("No Response from the remote server. Check your connection and try again.");
				},					  
				success:		
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d) 
					{
						<!--- Alert if failure --->
						<!--- Get row 1 of results if exisits--->
						<!--- Check if variable is part of JSON result string --->								
						CurrRXResultCode = d.DATA.RXRESULTCODE;	
						if(CurrRXResultCode > 0)
						{
							bootbox.alert('You have successfully launched the campaign.', function(result) { 
								location.reload(); 
							});
							return false;
						}
						else
						{							
							bootbox.alert("Campaign has NOT been launch.\n" + d.DATA.MESSAGE + "\n" + d.DATA.ERRMESSAGE, function(result) { } );
						}
					} 		
			});
			return false;																	
		}
	</script>
</cfif>
