<cfset rootUrl = ""/>
<cfset SessionPath = "session"/>
<cfset PublicPath = "public"/>

<cfparam name="INPBATCHID" default="0">

<script type="text/javascript">


	$(function() 
	{		
							

		$("#inpSubmit").click(function() { 


			try{
				$.ajax({
		            type: "POST",
		            url: "/public/sire/models/cfc/reportings.cfc?method=UpdateRecurringLog&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
		    		data:$('#form-create-template').serialize(),
		            dataType: "json", 
		            success: function(d) {
		            	
         				if(d.RXRESULTCODE == 1){

							bootbox.dialog({
									        message: "Save Log Success",
									        title: "Recurring Report",
									        buttons: {
									            success: {
									                label: "Ok",
									                className: "btn btn-medium btn-success-custom",
									                callback: function() {
									                	$('#RecurringLogModal').modal('hide');
														window.location.href = '/session/sire/pages/admin-recurring-report';
									                }
									            }
									        }
							});
							
							
						}
						else
						{
							<!--- No result returned --->							
							//bootbox.alert(d.MESSAGE);
							bootbox.dialog({
									        message: d.MESSAGE,
									        title: "Recurring Report",
									        buttons: {
									            success: {
									                label: "Ok",
									                className: "btn btn-medium btn-success-custom",
									                callback: function() {
									                }
									            }
									        }
							});
						}
		         	}
		   		});
			}catch(ex){
				bootbox.alert('Update Fail', function() {});
			}
		});

	});



	

</script>	


<cfoutput>

<!--- GET MAX ORDER IN ORDER --->
<div class="bootbox">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Update Status Log</h4>        
</div>


<cfquery name="getLog" datasource="#Session.DBSourceEBM#">
					SELECT 
	        			l.RecurringLogId_bi,
	        			l.PaymentDate_dt,
	        			l.Status_ti,
	        			l.SentEmail_ti,
	        			l.UserPlanId_bi,
	        			l.UserId_int,
	        			l.PlanId_int,
	        			l.FirstName_vch,
	        			l.EmailAddress_vch,
	        			l.Amount_dec,
	        			l.PaymentData_txt,
	        			l.PaymentResponse_txt,
	        			l.LogFile_vch,
	        			p.PlanName_vch,
	        			up.BuyKeywordNumber_int
	        		FROM `simplebilling`.recurringlog l 
	        				INNER JOIN `simplebilling`.plans p ON l.PlanId_int = p.PlanId_int
	        				INNER JOIN  `simplebilling`.userplans up ON l.UserPlanId_bi = up.UserPlanId_bi
	        		WHERE RecurringLogId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#"> 
</cfquery>

<div class="modal-body">


    <div id="MC_Stage_XMLControlString_Master_Template" class="EBMDialog">
                          
        <form method="POST" id="form-create-template">
                                      
            <div class="inner-txt-box">
   
            
                <div class="form-left-portal">
            
                    <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>" />
	
					<label for="INPSTATUS">Status</label>
                	<select id="" name="INPSTATUS">
                		<option <cfif getLog.Status_ti EQ 0>selected</cfif> value="0">Open</option>
                		<option <cfif getLog.Status_ti EQ -1>selected</cfif> value="-1">In Progess</option>
                		<option <cfif getLog.Status_ti EQ -2>selected</cfif> value="-2">Tech Review</option>
                		<option <cfif getLog.Status_ti EQ -3>selected</cfif> value="-3">QA Review</option>
                		<option <cfif getLog.Status_ti EQ 1>selected</cfif> value="1">Close</option>
				    </select>

				                                          
                </div>                            
                                       
          
          
            </div>
                       
        </form>
           
    </div>     
    
</div> <!--- modal-body --->
    
<div class="modal-footer">

	<button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal" >Cancel</button>
     
	<button type="button" class="btn btn-success btn-success-custom" id="inpSubmit">Save</button>
   
</div>	
</div>	
    
</cfoutput>
