<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/site/common.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/my-account.js">
</cfinvoke>

<cfinclude template="/session/sire/configs/paths.cfm">



<!---

Now what can I do?




Create end user intiated responses and interactive campaigns via Keywords
Learn how effective inbound marketing strategies vis SMS work 

Browse our templates to help you get started

Easy to use first timers
Loyalty Program
Tell the Manager - Instant Feedback
Is you Bathroom clean?
List building - Group building
	dont enter a list yourself - let your targets join

	



FAQS
	Dont have time to be creative - use our templates
	
	
	



Fact: About 20 percent of your customers produce 80 percent of your sales.

Create a unique style of messaging and even unique offers that are only sent to your SMS subscribers. Make them never want to leave because they won’t see this content anywhere else.

Most brands and retailers sending SMS messages send at most one message per week.





Coupons
Send Blasts to lists - no keyword required
Is Tues going a little slow? Offer a Bogo
Special Events
Change of plans
Outage alerts




Trigger interactive campaign to end user via API Call - need a keyword even if the end user does not know about it

Your order is recieved/ready/shipped/deliverd/
Your bill is ready
Multi-factor authentication


Advanced Drip Marketing 
What to expect when you are expecting
Manage your money tips
Group Events - Who is in









--->


<main class="container-fluid page">
	<cfinclude template="../views/commons/credits_available.cfm">
	<section class="row bg-white">
		<div class="content-header">
			<div class="row">
                <div class="col-sm-6 col-md-6 col-lg-6 content-title">
                	<!---<cfinclude template="../views/commons/welcome.cfm">--->
                    <h2>Guided Tour</h2>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6 content-menu">
				    <a class="active" href="##"><span class="icon-setting"></span><span>Setting</span></a>
                </div>
            </div>
		</div>
		<hr class="hrt0">
		<div class="content-body">
			<div class="container-fluid">
				
                <div class="col-sm-10 col-md-4">
                	
                    <div class="headerTile">
                    
                        <p class="sub-title">Keywords</p>
                        
                        <p>Don't already have a list of customers to market too? Help your customers come to you. Build lists or offer other useful interactions via Keywords.</p>
                        
                        <a class="headerTileLearnMore" href="#">Learn More...</a>
                    
                    </div>
                    
                </div>

                <div class="col-sm-10 col-md-4">
               		
                    <div class="headerTile">
                     
                        <p class="sub-title">Blasts</p>
                        
                        <p>Your users have told you they want to hear from you. Now they can. Send a message or trigger an interactive message to a list. </p>
                        
                        <a class="headerTileLearnMore" href="#">Learn More...</a>
                    
                    </div>
                    
                </div>	                
                
                <div class="col-sm-10 col-md-4">
                
	            	<div class="headerTile">
                        <p class="sub-title">Triggered Events</p>
                        
                        <p>Hook Sire into your other systems. Trigger customized messaging based on events via our simple to use REST API</p>
                                            
                        <a class="headerTileLearnMore" href="#">Learn More...</a>
                    
                    </div>
                    
                </div>	
				
			</div>	
		</div>
	</section>
</main>
<cfparam name="variables._title" default="New Campaign - Sire">
<cfinclude template="../views/layouts/main.cfm">