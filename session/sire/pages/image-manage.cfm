<cfinclude template="/session/sire/configs/paths.cfm">

<cfparam name="variables._title" default="Manage Images - Sire">

<main class="container-fluid page my-plan-page">

	<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUserPlan" returnvariable="RetUserPlan"></cfinvoke> 
	<cfinvoke component="session.sire.models.cfc.order_plan" method="GetNextPlanUpgrade" returnvariable="nextPlanUpgrade">
		<cfinvokeargument name="inpPlanOrder" value="#RetUserPlan.PLANORDER#">
	</cfinvoke>

    <section class="row bg-white">
        <div class="content-header">
        	<div class="row">
            </div>
        </div>
		<div class="content-body">
			<div class="upload-container" id="uploadArea">

				<div id="actions" class="row">
				<div class="col-sm-12 page-title">Manage your Images</div>
					<div class="add-image-desc">
						<div class="col-lg-7">
							<span class="btn btn-success-custom fileinput-button">
								<i class="glyphicon glyphicon-plus"></i>
								<span>Add image...</span>
							</span>
						</div>
						<div class="col-lg-12" style="margin-top:5px">
							Click Add image button or drag and drop image in this page. Accepted format: <cfoutput><cfloop array="#_ACCEPTEDIMAGETYPE#" index="type">#type#&nbsp;&nbsp;</cfloop></cfoutput><br>Max file size: 10MB.<br>Filename format: Alphanumeric characters [0-9a-zA-Z] and Special characters -, _, (, )
						</div>
						<div class="col-lg-5">
							<!-- The global file processing state -->
							<span class="fileupload-process">
								<div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
									<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
								</div>
							</span>
						</div>
					</div>
				</div>

				<div class="table table-striped" class="files" id="previews">

				  <div id="template" class="file-row">
				    <!-- This is used as the file preview template -->
				    <div>
				        <span class="preview"><img data-dz-thumbnail /></span>
				    </div>
				    <div>
				        <p class="name" data-dz-name></p>
				        <strong class="error text-danger" data-dz-errormessage></strong>
				    </div>
				    <div>
				        <p class="size" data-dz-size></p>
				        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
				          <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
				        </div>
				    </div>
				    <div>
				      <button class="btn btn-primary start">
				          <i class="glyphicon glyphicon-upload"></i>
				          <span>Start</span>
				      </button>
				      <button data-dz-remove class="btn btn-warning cancel">
				          <i class="glyphicon glyphicon-ban-circle"></i>
				          <span>Cancel</span>
				      </button>
				      <button data-dz-remove class="btn btn-danger delete">
				        <i class="glyphicon glyphicon-trash"></i>
				        <span>Delete</span>
				      </button>
				    </div>
				  </div>
				</div>
			</div>

			<cfinvoke method="GetUserStorageInfo" component="session.sire.models.cfc.image" returnvariable="retValUserUsage">
			</cfinvoke>

			<cfif retValUserUsage.RXRESULTCODE GT 0>

				<cfset usagePercent = DecimalFormat((retValUserUsage.USERUSAGE/retValUserUsage.USERSTORAGEPLAN)*100) />

				<cfset UsageMBSize = retValUserUsage.USERUSAGE / 1048576>
				<cfset StoragePlanMBSize = retValUserUsage.USERSTORAGEPLAN / 1048576>

				<cfif UsageMBSize GT 1024>
					<cfset UsageMBSize = DecimalFormat(UsageMBSize / 1024)>
					<cfset UsageMBSize = UsageMBSize&"GB">
				<cfelse>
					<cfset UsageMBSize = DecimalFormat(UsageMBSize)&"MB">
				</cfif>

				<cfif StoragePlanMBSize GT 1024>
					<cfset StoragePlanMBSize = DecimalFormat(StoragePlanMBSize / 1024)>
					<cfset StoragePlanMBSize = StoragePlanMBSize&"GB">
				<cfelse>
					<cfset StoragePlanMBSize = DecimalFormat(StoragePlanMBSize)&"MB">
				</cfif>

				<cfoutput>

					<input type="hidden" id="inpUserUsage" value="#retValUserUsage.USERUSAGE#">
					<input type="hidden" id="inpUserStoragePlan" value="#retValUserUsage.USERSTORAGEPLAN#">

					<div class="upload-container" style="padding-top:20px">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
								<b>Storage usage: <span id="userUsageSpan">#UsageMBSize#</span> / <span id="userStoragePlanSpan">#StoragePlanMBSize#</span></b>
							</div>
							<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
								<div class="progress">
									<div class="progress-bar progress-bar-striped progress-bar-success" role="progressbar" id="usageProgress" 
										aria-valuemin="0" aria-valuemax="100" style="width:#usagePercent#%">
									</div>
								</div>
							</div>
						</div>
					</div>

				</cfoutput>
			</cfif>

			<cfinclude template="/session/sire/views/images/imagebrowse.cfm">

		</div>
	</section>
</main>

<script type="text/javascript">
	var maxFilesize = "<cfoutput>#_IMGMAXSIZE#</cfoutput>";
</script>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/dropzone.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/image-manage.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/vendors/dropzone.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/site/image-manage.js">
</cfinvoke>

<cfinclude template="../views/layouts/master.cfm">
