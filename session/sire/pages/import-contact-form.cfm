<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)
        .addJs("../assets/pages/scripts/import-contact-form.js");

</cfscript>

<div class="portlet">
	<div class="uk-grid uk-grid-upsmall" uk-margin>
		<div class="uk-width-1-4@l uk-width-2-5@m">
			<div class="portlet light bordered">
				<div class="portlet-body">
					<h4 class="portlet-heading master2-heading mb-0">Import Contact</h4>

					<div class="input-group image-preview uk-margin-top">
		                <input type="text" class="form-control image-preview-filename"> <!-- don't give a name === doesn't send on POST/GET -->
		                <span class="input-group-btn">
		                    <button type="button" class="btn green-cancel-trash image-preview-clear" style="display:none;">Clear</button>
		                    <div class="btn green-cancel image-preview-input">
		                        <span class="image-preview-input-title">Choose csv</span>
		                        <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/> <!-- rename it -->
		                    </div>
		                </span>		                
		            </div>	

		            <div class="uk-grid uk-grid-upsmall uk-margin-top uk-margin-bottom" uk-margin>
		            	<div class="uk-width-1-2">
		            		<div class="ad-radio">
		            			<input type="radio" name="radiog_dark" id="radio1" class="css-checkbox" />
                    			<label for="radio1" class="css-label radGroup1 radGroup2">Subscribers List</label>
		            		</div>
		            	</div>
		            	<div class="uk-width-1-2">
		            		<div class="ad-radio">
		            			<input type="radio" name="radiog_dark" id="radio2" class="css-checkbox" />
	                			<label for="radio2" class="css-label radGroup1 radGroup2">Campaign Blast</label>
		            		</div>
		            	</div>
		            </div>

		            <div class="wrap-switch-content">
						<div class="uk-grid uk-grid-upsmall" uk-margin>
			            	<div class="uk-width-1-2">
			            		<div class="ad-radio">
			            			<input type="radio" name="radiog_dark2" id="radio3" class="css-checkbox" />
	                    			<label for="radio3" class="css-label radGroup1 radGroup2">Send All</label>
			            		</div>
			            	</div>
			            	<div class="uk-width-1-2">
			            		<div class="ad-radio">
			            			<input type="radio" name="radiog_dark2" id="radio4" class="css-checkbox" />
		                			<label for="radio4" class="css-label radGroup1 radGroup2">Send Partial</label>
			            		</div>
			            	</div>
			            </div>	
						
						<div class="form-gd uk-margin-top">
							<div class="form-group">
				            	<label for="">Select User</label>
				            	<select name="" id="" class="form-control uk-width-4-5@l">
				            		<option value="">Search by email or ID</option>
				            		<option value="">Search by email or ID</option>
				            		<option value="">Search by email or ID</option>
				            	</select>
				            </div>

				            <div class="form-group">
				            	<label for="">Select Campaign</label>
				            	<select name="" id="" class="form-control uk-width-4-5@l">
				            		<option value="">Weather Emergency 2017-02-1</option>
				            		<option value="">Search by email or ID</option>
				            		<option value="">Search by email or ID</option>
				            	</select>
				            </div>

				            <div class="form-group">
				            	<a href="##" class="">Schedule</a>
				            </div>

				            <div class="form-action">
				            	<button class="btn green-gd mr-15">import</button>
				            	<button class="btn green-cancel">advanced</button>
				            </div>
						</div>
		            </div>
				</div>
			</div>
		</div>

		<div class="uk-width-3-4@l uk-width-3-5@m">
			<div class="portlet light bordered">
				<div class="portlet-body">
					<h4 class="portlet-heading master2-heading mb-0">Report</h4>

					<div class="uk-grid uk-grid-upsmall uk-margin-top">
						<div class="uk-width-3-5@xl">
							<div class="uk-grid uk-grid-upsmall" uk-grid>
								<div class="uk-width-1-3@s">
									<select name="" id="" class="form-control">
					            		<option value="">Weather Emergency 2017-02-1</option>
					            		<option value="">Search by email or ID</option>
					            		<option value="">Search by email or ID</option>
					            	</select>
								</div>
								<div class="uk-width-1-3@s">
									<select name="" id="" class="form-control">
					            		<option value="">Weather Emergency 2017-02-1</option>
					            		<option value="">Search by email or ID</option>
					            		<option value="">Search by email or ID</option>
					            	</select>
								</div>
								<div class="uk-width-auto">
									<button class="btn green-cancel">apply filter</button>
								</div>
							</div>	
						</div>
					</div>				
				</div>
			</div>

			<div class="portlet light bordered">
				<div class="portlet-body">
					<h4 class="portlet-heading master2-heading mb-0">File Summary</h4>
					<div class="uk-margin-top">
						Table put here
					</div>
				</div>
			</div>
		</div>
	</div>
</div>	


<cfparam name="variables._title" default="Admin Data Field Filter - Sire">

<cfinclude template="../views/layouts/master2.cfm">