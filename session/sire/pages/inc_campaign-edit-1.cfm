<cfoutput>  

 <!--- BEGIN : CAMPAIGN NAME --->                                                    
<div class="portlet light bordered">
    <div class="portlet-body">
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-gd form-lb-large">
                    <div class="form-group">
                        <label class="hidden">Give Your Campaign a Name</label><br>
                        <cfif action eq "CreateNew">
                            <input type="text" style="display: none" class="form-control validate[required,custom[noHTML]]" placeholder="" name="Desc_vch" id="Desc_vch" value="#CDescHTML#"> 
                            <span id="cppNameDisplay">#CDescHTML#</span>                               
                            <cfif cpNameSectionShow EQ 0><input type="hidden" name="OrganizationName_vch" id="OrganizationName_vch" value="#RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH#"></cfif> 
                        <cfelse>
                            <input type="text" class="form-control validate[required,custom[noHTML]]" placeholder="" name="Desc_vch" id="Desc_vch" value="#CDescHTML#">                                    
                            <span class="help-block hidden">ex: Summer Happy Hour Promo – 20% off</span>  
                            <cfif cpNameSectionShow EQ 0><input type="hidden" name="OrganizationName_vch" id="OrganizationName_vch" value="#RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH#"></cfif>      
                        </cfif>                             
                    </div>
                </div>
            </div>
            <div class="col-md-12 hidden">                                
                <div class="form-group">
                        <a href="javascript:;" class="btn green-gd campaign-next #hiddenClass#">Next <i class="fa fa-caret-down"></i></a>&nbsp;&nbsp;
                        <!---<a data-toggle="modal" data-target="##ScheduleOptionsModal" href="##" class="btn green-gd campaign-schedule #hiddenClass#">Schedule Blast For Later </a>--->
                        <!--- <a data-toggle="modal" data-target="##ScheduleOptionsModal" href="##" class="btn green-gd">Schedule </a>--->
                    <!--- Button Cancel When wan't create a Campaign --->
                    <cfif campaignData.BatchId_bi GT 0>
                        <a href="##" onclick="openCampaignSimon()" class="btn green-cancel campaign-simon-cancel pull-right #hiddenClass#">Cancel</a>       
                    </cfif>                   
                </div>
            </div>
        </div>    
    </div>
</div>        
<!--- END : CAMPAIGN NAME ---> 


<!--- BEGIN : Company NAME --->    
<cfif cpNameSectionShow EQ 1>
    <div class="portlet light bordered cpedit">
        <div class="portlet-body">
            <h4 class="portlet-heading2">Company Name</h4>
            <p class="portlet-subheading">
                Phone carriers require all business messages to be easily identifiable to the company who sends them. Your company name will appear before your message.
            </p>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-gd form-lb-large">
                        <div class="form-group">
                            <input type="hidden" name="OrganizationId_int" id="OrganizationId_int" value="<cfoutput>#RetVarGetUserOrganization.ORGINFO.OrganizationId_int#</cfoutput>">
                            <input type="text" class="form-control validate[required,custom[noHTML]]" name="OrganizationName_vch" id="OrganizationName_vch" maxlength="250" data-prompt-position="topLeft:100" placeholder="" value="#RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH#">
                        </div>
                    </div>
                </div>
            </div>    

            <a href="javascript:;" class="btn green-gd campaign-next #hiddenClass#">Next <i class="fa fa-caret-down"></i></a>                                
        </div>
    </div>       
</cfif>
<!--- END : Company NAME --->  

<!--- BEGIN CP RENDER WRAPPER --->
<div class="portlet light bordered cpedit">
    <div class="portlet-body">
        <!--- RENDER CP --->
        <cfset MaxCPCount = 0 />
        <cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPs" returnvariable="RetVarReadCPs">
            <cfinvokeargument name="inpBatchId" value="#campaignid#">
                <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
                </cfinvoke>

                <!---  <cfif campaignData.TEMPLATEID EQ 1> --->
                <h4 class="portlet-heading2">Write Your Message</h4>
                <!--- </cfif> --->    

                <cfloop array="#RetVarReadCPs.CPOBJ#" index="CPOBJ">                   
                    <cfset MaxCPCount++ />

                    <cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPDataById" returnvariable="RetVarReadCPDataById">
                        <cfinvokeargument name="inpBatchId" value="#campaignid#">
                            <cfinvokeargument name="inpQID" value="#CPOBJ.RQ#">
                                <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
                                </cfinvoke>

                 <!---    <pre>
                        <cfdump var="#RetVarReadCPDataById.CPOBJ.SWT#"/>
                    </pre> --->
                    
                    <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'OPTIN'>
                        <cfset campaignData.OPTIN_GROUPID = RetVarReadCPDataById.CPOBJ.OIG />
                    </cfif>

                    <cfif templateId EQ 11>
                        <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'ONESELECTION' || RetVarReadCPDataById.CPOBJ.TYPE EQ 'SHORTANSWER' >
                            <cfset countCPOneSelection ++ />
                        </cfif>    
                    <cfelse>
                        <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'ONESELECTION'>
                            <cfset countCPOneSelection ++ />
                        </cfif>    
                    </cfif>

                    <!--- <cfif RetVarReadCPDataById.CPOBJ.TYPE NEQ 'BRANCH' AND RetVarReadCPDataById.CPOBJ.TYPE NEQ 'OPTIN' > --->
                    <cfif RetVarReadCPDataById.CPOBJ.TYPE NEQ 'BRANCH' OR (templateId EQ 7) >
                        <cfif RetVarReadCPDataById.CPOBJ.TYPE NEQ 'OPTIN' OR (RetVarReadCPDataById.CPOBJ.TYPE EQ 'OPTIN' AND displayOptin EQ 1)>

                            <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'TRAILER' AND RetVarReadCPDataById.CPOBJ.SWT EQ 0>
                                <!--- do nothing --->
                            <cfelse>    
                                <cfinvoke method="RenderCPNew" component="session.sire.models.cfc.control-point" returnvariable="RetVarRenderCP">
                                    <cfinvokeargument name="controlPoint" value="#RetVarReadCPDataById.CPOBJ#">
                                    <cfinvokeargument name="inpBatchId" value="#campaignid#">   
                                    <cfinvokeargument name="inpSimpleViewFlag" value="#inpSimpleViewFlag#"> 
                                    <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">                     
                                    <cfinvokeargument name="inpdisplayIntervalType" value="#displayIntervalType#">
                                    <cfinvokeargument name="inpTemplateId" value="#templateId#">                     
                                </cfinvoke> 
                                    <!--- This is the HTML for a CP opject - HTML is maintained in RenderCP function --->   
                                    #RetVarRenderCP.HTML#
                            </cfif>    

                        </cfif>
                    </cfif>
                </cfloop>

        <cfif displayPreview EQ 1>
            <a href="javascript:;" class="btn green-gd campaign-next nextCpeditCheckRule #hiddenClass#">Next <i class="fa fa-caret-down"></i></a>
        <cfelse>        
            <a href="javascript:;" class="btn green-gd campaign-next #hiddenClass#">Next <i class="fa fa-caret-down"></i></a>
        </cfif>
        
    </div>
</div>
<!--- END CP RENDER WRAPPER --->


<!--- BEGIN : SELECT SUBCRIBER LIST --->    
<div class="portlet light bordered cpedit">
    <div class="portlet-body">
        <h4 class="portlet-heading2">Select a Subscriber List</h4>
        <div class="row row-small">
            <div class="col-md-12 col-xs-12">
                <p class="portlet-subheading">
                    A Subscriber List is a list of subscribers that have opted into your campaign by texting your keyword to your short code.<br> 
                    Example: “#shortCode.SHORTCODE#”. Sire automatically captures all of your subscribers phone numbers and saves them in your subscriber list.<br>
                    This is also to help keep non-compliant spammers at bay.
                </p>
            </div>
        </div>
        <div class="row row-small">
            <div class="col-md-6 col-xs-6">
                <div class="form-gd form-lb-large">
                    <div class="form-group">
                        <select class="form-control validate[required] Select2" id="SubscriberList">
                            <cfif arrayLen(subcriberList) GT 0>
                                <cfloop array="#subcriberList#" index="array_index">
                                    <option #campaignData.OPTIN_GROUPID EQ array_index[1] ? "selected" : ""# value="#array_index[1]#">#array_index[2]#</option>
                                </cfloop>
                            </cfif>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-xs-6">
                <a href="##" class="btn grey-mint btn-create-new-list"><i class="fa fa-plus"></i> create new list</a>
            </div>    
        </div>
        <a href="javascript:;" class="btn green-gd campaign-next #hiddenClass#">Next <i class="fa fa-caret-down"></i></a>                                  
    </div>
</div>
<!--- END : SELECT SUBCRIBER LIST --->  


<!--- BEGIN : CHOOSE KEYWORD --->
<div class="portlet light bordered cpedit">
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-8">
                <h4 class="portlet-heading2">Choose a Keyword</h4>
                <p class="portlet-subheading">
                    A Keyword is a text message that your customer sends to a Short Code using their cell phone. <br>
                    <!--- Sire will then respond with a customizable interactive message flow, triggering a new Sire SMS session. --->
                    This Keyword will trigger your SMS campaign and start the communication flow.
                </p>
                <!--- <p class="portlet-subheading">
                    Keywords are one of the fastest ways you can begin to communicate.
                </p> --->

                <div class="row row-small">
                    <div class="col-md-9">
                        <div class="form-gd">
                            <div class="form-group">
                                <input value="#campaignData.Keyword_txt#" class="form-control" id="Keyword" type="text" name="Keyword" maxlength="160" style="width:100%;" />                                        
                                <span class="has-error KeywordStatus" style="display: none;" id="KeywordStatus"></span>                                        
                                <cfif cpNameSectionShow EQ 0><input type="hidden" name="OrganizationName_vch" id="OrganizationName_vch" value="#RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH#"></cfif>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="image2">
                    <img class="img-responsive" src="../assets/layouts/layout4/img/phone-choose-keyword.png" alt="">
                    <h2>
                        <cfif len(shortcode.SHORTCODE) EQ 5>
                            #left(shortcode.SHORTCODE,3)#-#right(shortcode.SHORTCODE,2)#
                        <cfelse>
                            #shortcode.SHORTCODE#
                        </cfif>
                    </h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <a href="javascript:;" class="btn green-gd campaign-next #hiddenClass#">Next <i class="fa fa-caret-down"></i></a>
            </div>    
        </div>                              
    </div>
</div>
<!--- END : CHOOSE KEYWORD --->

<!--- PREVIEW RENDER WRAPPER --->
<cfif displayPreview EQ 1>
    <div class="portlet light bordered cpedit" id="wrap-preview">
        <div class="portlet-body">
            <h4 class="portlet-heading2">Preview Your Campaign: </h4>
            <p>Let’s check your handiwork.  Enter your keyword into the phone’s text box below to preview your survey.  Scroll up to make edits or click “Finished” to start your campaign.</p>
            <input type="hidden" id="inpSMSToAddress" name="inpSMSToAddress" size="20" readonly value="#shortCode.SHORTCODE#">

            <cfinvoke component="session.cfc.administrator.passwordgen" method="GenSecurePassword" returnvariable="getSecurePassword"></cfinvoke>
            <cfset inpContactString = "900000001_Demo_#getSecurePassword#_#LSDateFormat(now(), 'yyyy-mm-dd')##LSTimeFormat(now(), 'HH:mm:ss')#"/>
            <input type="hidden" id="inpContactString" name="inpContactString" placeholder="Enter Contact String Here" size="20" value="#inpContactString#"/>

            <div class="wrap-preview">
                <div class="preview-screen">
                    <div class="image small-hide">
                        <img class="bg-preview-screen" src="../assets/layouts/layout4/img/preview-phone.png" alt="">
                        <h2>
                            <h2>
                                <cfif len(shortcode.SHORTCODE) EQ 5>
                                    #left(shortcode.SHORTCODE,3)#-#right(shortcode.SHORTCODE,2)#
                                <cfelse>
                                    #shortcode.SHORTCODE#
                                </cfif>
                            </h2>
                        </h2>
                    </div>
                    <div class="phone-screen">
                        <div class="inner-phone-screen clearfix" id="SMSHistoryScreenArea"></div>
                            <div class="type-screen">
                                <textarea id="SMSTextInputArea" name="SMSTextInputArea" maxlength="160" placeholder="Text Message"></textarea>
                                <button id="SMSSend" type="button" class="btn-send-area"></button>
                            </div>
                        </div>
                    </div>
            </div>
        </div>

        <button id="SMSPreview" type="button" class="btn newbtn green-gd hidden" style=""> Preview </button>

        <cfif campaignData.TemplateType_ti EQ 1>
            <a href="javascript:;" class="btn newbtn new-blue-gd btn-check-blast">#btnSaveLabel#</a>
        <cfelse>
            <a href="javascript:;" class="btn newbtn new-blue-gd btn-finished">#btnSaveLabel#</a>
        </cfif>
    </div>
</cfif>
<!--- END PREVIEW RENDER WRAPPER --->

</cfoutput>