<cfoutput>  

 <!--- BEGIN : CAMPAIGN NAME --->                                                    
<div class="portlet light bordered">
    <div class="portlet-body-1">
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-gd form-lb-large">
                    <div class="form-group">                        
                        <h4 class="portlet-heading-new">Campaign Name</h4>
                        <h5 id="cp-char-count-1" class="control-point-char text-color-gray">
                        <cfif action eq "CreateNew">
                            <input type="text" style="display: none" class="form-control validate[required,custom[noHTML]]" placeholder="" name="Desc_vch" id="Desc_vch" value="#CDescHTML#"> 
                            <span id="cppNameDisplay" class="portlet-subheading">#CDescHTML#</span>
                            <span class="btn-re-edit edit-name-icon"><img src="/public/sire/images/create-new-pencil-button.png" class="img-responsive"> </i></span>                               

                            <cfif cpNameSectionShow EQ 0><input type="hidden" name="OrganizationName_vch" id="OrganizationName_vch" value="#RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH#"></cfif> 
                        <cfelse>
                            <input type="text" class="form-control validate[required,custom[noHTML]]" placeholder="" name="Desc_vch" id="Desc_vch" value="#CDescHTML#">                                    
                            <span class="help-block hidden">ex: Summer Happy Hour Promo – 20% off</span>  
                            <cfif cpNameSectionShow EQ 0><input type="hidden" name="OrganizationName_vch" id="OrganizationName_vch" value="#RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH#"></cfif>      
                        </cfif>                             
                    </div>
                </div>
            </div>
            <div class="col-md-12 hidden">                                
                <div class="form-group">
                        <a href="javascript:;" class="btn green-gd campaign-next btn-adj-pos #hiddenClass#">Next <i class="fa fa-caret-down"></i></a>&nbsp;&nbsp;
                    <cfif campaignData.BatchId_bi GT 0>
                        <a href="##" onclick="openCampaignSimon()" class="btn green-cancel campaign-simon-cancel pull-right #hiddenClass#">Cancel</a>       
                    </cfif>                   
                </div>
            </div>
        </div>    
    </div>
</div>        
<!--- END : CAMPAIGN NAME ---> 

<!--- BEGIN : CHOOSE KEYWORD --->
<div class="portlet light-new bordered cpedit">
    <div class="portlet-body">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">                
                <h4 class="portlet-heading-new">Choose a Keyword  <a tabindex="0" class="info-popover" data-placement="right" data-trigger="focus" role="button" data-html="true"   title="" data-content="A Keyword is a term or phrase, unique to your business, which is used to trigger a SMS communication between you and your customer.  An example is if you see an ad that says:  Text &##34;Coffee&##34; to 39492.  Coffee is the keyword a customer will use to opt-in or begin a text conversation with you.</br></br><img width='30px' src='../assets/layouts/layout4/img/Icon_Tip.png'/> Make your keyword short and memorable." ><img  src='../assets/layouts/layout4/img/info.png'/></a></h4>                                   
                    
                <div class="form-gd">
                    <div class="form-group">                        
                        <input value="#campaignData.Keyword_txt#" class="form-control validate[required]" id="Keyword" type="text" name="Keyword" maxlength="160" style="width:100%;" />                                                                                                
                    </div>
                    <div class="form-group">                                                
                        <span class="has-error KeywordStatus" style="display: none;" id="KeywordStatus"></span>                                                                                                
                    </div>
                </div>
            </div>
            <div class="col-lg-7 col-md-7 hidden-sm hidden-xs">
                
                <div class="movi-short">                    
                    <div class="movi-heading">
                        <h3 class="movi-heading__number">#session.Shortcode#</h3>
                    </div>
                    <div class="movi-body">
                        <div class="media chat__item chat--sent">
                            <div class="media-body media-bottom">
                                <div class="chat__text">
                                    <span class="span_keyword">Localdinner</span>                                    
                                </div>
                            </div>
                            <div class="media-right media-bottom">
                                <div class="chat__user">
                                    <img src="../assets/layouts/layout4/img/avatar-women.png" />
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div><!--End Mobile View -->
            </div>
        </div>     
        <a href="javascript:;" class="btn green-gd campaign-next check-kw btn-adj-pos #hiddenClass#">Next <i class="fa fa-caret-down"></i></a>                        
    </div>
</div>
<!--- END : CHOOSE KEYWORD --->

<!--- BEGIN CP RENDER WRAPPER --->

<!--- RENDER CP --->
<cfset MaxCPCount = 0 />

<cfif action EQ "Edit">
    <cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPs" returnvariable="RetVarReadCPs">
        <cfinvokeargument name="inpBatchId" value="#campaignid#">
        <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
    </cfinvoke>
<cfelse>
    <cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPsFromTemplateID" returnvariable="RetVarReadCPs">
        <cfinvokeargument name="inpTemplateID" value="#templateid#">            
        <cfinvokeargument name="inpSelectedTemplateType" value="#selectedTemplateType#">
    </cfinvoke>
</cfif>


<cfloop array="#RetVarReadCPs.CPOBJ#" index="CPOBJ">                   
    <cfset MaxCPCount++ />
    <cfif action EQ 'Edit'>
        <cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPDataById" returnvariable="RetVarReadCPDataById">
            <cfinvokeargument name="inpBatchId" value="#campaignid#">
            <cfinvokeargument name="inpQID" value="#CPOBJ.RQ#">
            <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
        </cfinvoke>
    <cfelse>
        <cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPTempalteDataById" returnvariable="RetVarReadCPDataById">
            <cfinvokeargument name="inpTemplateID" value="#templateid#">            
            <cfinvokeargument name="inpSelectedTemplateType" value="#selectedTemplateType#">
            <cfinvokeargument name="inpQID" value="#CPOBJ.RQ#">                         
        </cfinvoke>
    </cfif>
    
    <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'OPTIN'>
        <cfset campaignData.OPTIN_GROUPID = RetVarReadCPDataById.CPOBJ.OIG />
    </cfif>

    <cfif templateId EQ 11>
        <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'ONESELECTION' || RetVarReadCPDataById.CPOBJ.TYPE EQ 'SHORTANSWER' >
            <cfset countCPOneSelection ++ />
        </cfif>    
    <cfelse>
        <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'ONESELECTION'>
            <cfset countCPOneSelection ++ />
        </cfif>    
    </cfif>

    <!--- <cfif RetVarReadCPDataById.CPOBJ.TYPE NEQ 'BRANCH' AND RetVarReadCPDataById.CPOBJ.TYPE NEQ 'OPTIN' > --->
    <cfif RetVarReadCPDataById.CPOBJ.TYPE NEQ 'BRANCH' OR (templateId EQ 7) >
        <cfif RetVarReadCPDataById.CPOBJ.TYPE NEQ 'OPTIN'>

            <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'TRAILER' AND RetVarReadCPDataById.CPOBJ.SWT EQ 0>
                <!--- do nothing --->
            <cfelse>    
                <cfinvoke method="RenderCPNewNew" component="session.sire.models.cfc.control-point" returnvariable="RetVarRenderCP">
                    <cfinvokeargument name="controlPoint" value="#RetVarReadCPDataById.CPOBJ#">
                    <cfinvokeargument name="inpBatchId" value="#campaignid#">   
                    <cfinvokeargument name="inpSimpleViewFlag" value="#inpSimpleViewFlag#"> 
                    <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">                     
                    <cfinvokeargument name="inpdisplayIntervalType" value="#displayIntervalType#">
                    <cfinvokeargument name="inpTemplateId" value="#templateId#">                     
                </cfinvoke> 
               
                <!--- This is the HTML for a CP opject - HTML is maintained in RenderCP function --->   
                <div class="portlet light-new bordered cpedit" >
                <!--- <pre>
                    <cfdump var="#RetVarReadCPDataById.CPOBJ.TYPE#"/>
                </pre> --->
                <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'TRAILER'>
                    <div class="portlet-body-3">
                <cfelse>
                    <div class="portlet-body-2">    
                </cfif>
                        #RetVarRenderCP.HTML#
                    </div>
                </div>
            </cfif>    
        <cfelse>
            <!--- BEGIN : SELECT SUBCRIBER LIST --->    
            <div class="portlet light-new bordered cpedit">
                <div class="portlet-body">
                    <h4 class="portlet-heading-new">Choose a Subscriber List <a tabindex="0" class="info-popover" data-placement="right" data-trigger="focus" role="button"   data-html="true"   title="" data-content="A Subscriber List is a list of people who have opted in to your campaign.  We automatically capture all of your subscribers phone numbers and save them so you can quickly and easily send them messages in the future.</br></br>If this is your first campaign, you can create your first list by clicking &##34;Create New List&##34;.</br></br><a class='tooltip-link' href='https://www.siremobile.com/blog/support/build-your-customer-list-view-report/' target='_blank'>Click Here</a> to learn how to view and manage your lists."><img  src='../assets/layouts/layout4/img/info.png'/></a></h4>
                    
                    <div class="row">
                        <div class="col-md-5 col-xs-12">
                            <div class="form-gd form-lb-large">
                                <div class="form-group">
                                    <select class="form-control validate[required] Select2" id="SubscriberList">
                                        <cfif arrayLen(subcriberList) GT 0>
                                            <cfloop array="#subcriberList#" index="array_index">
                                                <option #campaignData.OPTIN_GROUPID EQ array_index[1] ? "selected" : ""# value="#array_index[1]#">#array_index[2]#</option>
                                            </cfloop>
                                        </cfif>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-xs-12">
                            <a href="##" class="btn grey-mint btn-create-new-list"><i class="fa fa-plus"></i> create new list</a>
                        </div>    
                    </div>
                    <a href="javascript:;" class="btn green-gd campaign-next btn-adj-pos #hiddenClass#">Next <i class="fa fa-caret-down"></i></a>                                  
                </div>
            </div>
            <!--- END : SELECT SUBCRIBER LIST --->  
        </cfif>
    </cfif>
</cfloop>
<!--- END CP RENDER WRAPPER --->
<div class="portlet light bordered cpedit cpg-mlp-spread-the-word ">
    <h4 class="portlet-heading-new">Spread the Word</h4>
    <p class="portlet-subheading text-color-gray set-margin-bt-0">First, let’s test it out. Grab your phone and text your keyword to the phone number 39492. <br>If it looks good, create a Marketing Landing Page (MLP) to let others know about this campaign.<br><br>Click on each section in the MLP to edit the text and logo. <span><a tabindex="0" class="info-popover" data-trigger="focus" role="button" data-placement="right" data-html="true" title="" data-content="<h5 style='color: black; font-size: 16px;' class='desc-hover-ul'>Think of a MLP as a simple web page or digital flyer used to spread the word about your text marketing campaign. Click on each section within the MLP to edit the text and logo.  A link is automatically created so you can share the MLP as a web page. <br><br>To view or edit your MLPs, click on Campaigns > Manage Campaigns in the main menu. When complete you can:</h5><ul><li type='i'>Use the url to share it as a web page.</li> <li type='i'>Download it as a PDF or JPEG file to print and share</li></ul><img width='30px' src='../assets/layouts/layout4/img/Icon_Tip.png'/>Toggling the “Link Access” switch off will deactivate the link." data-original-title=""><img src="../assets/layouts/layout4/img/info.png"></a></span></p>
    <!--- <ul class='campaign-mlp-desc-text'>
        <li type="1">Share this campaign on your social media feeds.</li>
        <li type="1">Create a Marketing Landing Page (MLP) to share as a web page or download for print.</li>
    </ul> --->
    <div class="campaign-mlp-modal-body">
        #RAWCONTENTTOSHOW#
    </div>
    
    <div class="col-lg-12 finished-campaign-new">
        <div class="campaign-mlp-link-access col-lg-12">
            <div class="campaign-mlp-link-access-switch hidden">
                <span class="campaign-mlp-link"><strong>Link Access: </strong></span>
                <label class="switch">
                    <input id="check-campaign-access-link" type="checkbox" checked>
                    <span class="slider round"></span>
                </label>
            </div>

            <div class="">
                <!--- <input type="text" id="cpg-mlp-link-domain" name="cpg-mlp-link-domain" class="form-control cpg-mlp-link" value="https://www.mlp-x.com/lz/" disabled> --->
                <!--- <input type="text" id="cpg-mlp-link" name="cpg-mlp-link" class="form-control cpg-mlp-link" value="<cfoutput>#MLPDOMAINPATH##MLPLINK#</cfoutput>" disabled> --->
                <a href="<cfoutput>#MLPDOMAINPATH##MLPLINK#</cfoutput>" id="cpg-mlp-link" target="blank"><cfoutput>#MLPDOMAINPATH##MLPLINK#</cfoutput></a>
            </div>

        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 finished-campaign-new">
        <div class="col-lg-4 col-md-8 col-sm-6 campaign-mlp-link-social">
            <!--- <div class="container-fluid"> --->
                <div class="form-group social share-url">
                    <div class="campaign-mlp-link-access-ds share-url">
                        <a href title="Facebook" id="campaign-mlp-fb" class="invite-social-ic fb social-auth"><img class="campaign-mlp-link-access-icon" src="/session/sire/images/Share_Facebook.png?v=1.0" alt="Facebook"/><i class="fa fa-facebook-square hidden"></i></a>
                        <div class="campaign-mlp-link-access-text text-center">Facebook</div>
                    </div>

                    <div class="campaign-mlp-link-access-ds ">
                        <a href title="Twitter" id="campaign-mlp-tt" class="invite-social-ic tw social-auth-tw"><img class="campaign-mlp-link-access-icon" src="/session/sire/images/Share_Twitter.png?v=1.0" alt="Twitter"/><i class="fa fa-twitter-square hidden"></i></a>
                        <div class="campaign-mlp-link-access-text text-center">Twitter</div>
                    </div>

                    <!--- <div class="form-group social share-url">
                        <!--- <label class="block">Share</label> --->
                        <a class="invite-social-ic fb social-auth">
                            <i class="fa fa-facebook-square"></i>
                        </a>
                        <a class="invite-social-ic tw social-auth-tw">
                            <i class="fa fa-twitter-square"></i>
                        </a>
                        <a class="invite-social-ic gp social-auth">
                            <i class="fa fa-google-plus-square"></i>
                        </a>
                    </div> --->

                    <div class="campaign-mlp-link-access-ds campaign-mlp-dl">
                        <a target="_blank" title="PDF" id="campaign-mlp-pdf" class="invite-social-ic"><img class="campaign-mlp-link-access-icon" src="/session/sire/images/Share-PDF.png?v=1.0" alt="Youtube"/></a>
                        <div class="campaign-mlp-link-access-text text-center">PDF File</div>
                    </div>

                    <div class="campaign-mlp-link-access-ds campaign-mlp-dl">
                        <a target="_blank" title="JPG" id="campaign-mlp-jpg"  class="invite-social-ic"><img class="campaign-mlp-link-access-icon" src="/session/sire/images/Share-JPG.png?v=1.0" alt="LinkedIn"/></a>
                        <div class="campaign-mlp-link-access-text text-center">JPG File</div>
                    </div>

                </div>
            <!--- </div> --->
        </div>
    </div>

    <div id="previewImage" class="hidden">
    </div>
    <div class="finished-campaign-btn">
        <a href="javascript:;" class="btn newbtn new-blue-gd btn-finished">#btnSaveLabel#</a>
    </div>        
    
</div>




</cfoutput>