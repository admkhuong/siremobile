
<!---.ui-timepicker-wrapper--->
<cfoutput>  
    
        
     <!--- BEGIN : CAMPAIGN NAME --->                                                    
    <div class="portlet light bordered">
        <div class="portlet-body-1">
            
            <div class="row">
                <div class="col-md-6">
                    <div class="form-gd form-lb-large">
                        <div class="form-group">                        
                            <h4 class="portlet-heading-new">Campaign Name</h4>
                            <input type="hidden" id="Keyword"/>
                            <h5 id="cp-char-count-1" class="control-point-char text-color-gray">
                            <cfif action eq "CreateNew">
                                <input type="text" style="display: none" class="form-control validate[required,custom[noHTML]]" placeholder="" name="Desc_vch" id="Desc_vch" value="#CDescHTML#"> 
                                <span id="cppNameDisplay" class="portlet-subheading">#CDescHTML#</span>
                                <span class="btn-re-edit edit-name-icon"><img src="/public/sire/images/create-new-pencil-button.png" class="img-responsive"> </i></span>                               

                                <cfif cpNameSectionShow EQ 0><input type="hidden" name="OrganizationName_vch" id="OrganizationName_vch" value="#RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH#"></cfif> 
                            <cfelse>
                                <input type="text" class="form-control validate[required,custom[noHTML]]" placeholder="" name="Desc_vch" id="Desc_vch" value="#CDescHTML#">                                    
                                <span class="help-block hidden">ex: Summer Happy Hour Promo – 20% off</span>  
                                <cfif cpNameSectionShow EQ 0><input type="hidden" name="OrganizationName_vch" id="OrganizationName_vch" value="#RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH#"></cfif>      
                            </cfif>                             
                        </div>
                    </div>
                </div>
                <div class="col-md-12 hidden">                                
                    <div class="form-group">
                            <a href="javascript:;" class="btn green-gd campaign-next btn-adj-pos #hiddenClass#">Next <i class="fa fa-caret-down"></i></a>&nbsp;&nbsp;
                        <cfif campaignData.BatchId_bi GT 0>
                            <a href="##" onclick="openCampaignSimon()" class="btn green-cancel campaign-simon-cancel pull-right #hiddenClass#">Cancel</a>       
                        </cfif>                   
                    </div>
                </div>
            </div>    
        </div>
    </div>        
    <!--- END : CAMPAIGN NAME ---> 
    
    <!--- RENDER CP --->
    <cfset MaxCPCount = 0 />

    <cfif action EQ "Edit">
        <cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPs" returnvariable="RetVarReadCPs">
            <cfinvokeargument name="inpBatchId" value="#campaignid#">
            <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
        </cfinvoke>
    <cfelse>
        <cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPsFromTemplateID" returnvariable="RetVarReadCPs">
            <cfinvokeargument name="inpTemplateID" value="#templateid#">            
            <cfinvokeargument name="inpSelectedTemplateType" value="#selectedTemplateType#">
        </cfinvoke>
    </cfif>


    <cfloop array="#RetVarReadCPs.CPOBJ#" index="CPOBJ">                   
        <cfset MaxCPCount++ />
        <cfif action EQ 'Edit'>
            <cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPDataById" returnvariable="RetVarReadCPDataById">
                <cfinvokeargument name="inpBatchId" value="#campaignid#">
                <cfinvokeargument name="inpQID" value="#CPOBJ.RQ#">
                <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
            </cfinvoke>
        <cfelse>
            <cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPTempalteDataById" returnvariable="RetVarReadCPDataById">
                <cfinvokeargument name="inpTemplateID" value="#templateid#">            
                <cfinvokeargument name="inpSelectedTemplateType" value="#selectedTemplateType#">
                <cfinvokeargument name="inpQID" value="#CPOBJ.RQ#">                         
            </cfinvoke>
        </cfif>
        
        <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'OPTIN'>
            <cfset campaignData.OPTIN_GROUPID = RetVarReadCPDataById.CPOBJ.OIG />
        </cfif>

        <cfif templateId EQ 11>
            <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'ONESELECTION' || RetVarReadCPDataById.CPOBJ.TYPE EQ 'SHORTANSWER' >
                <cfset countCPOneSelection ++ />
            </cfif>    
        <cfelse>
            <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'ONESELECTION'>
                <cfset countCPOneSelection ++ />
            </cfif>    
        </cfif>

        <!--- <cfif RetVarReadCPDataById.CPOBJ.TYPE NEQ 'BRANCH' AND RetVarReadCPDataById.CPOBJ.TYPE NEQ 'OPTIN' > --->
        <cfif RetVarReadCPDataById.CPOBJ.TYPE NEQ 'BRANCH' OR (templateId EQ 7) >
            <cfif RetVarReadCPDataById.CPOBJ.TYPE NEQ 'OPTIN'>

                <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'TRAILER' AND RetVarReadCPDataById.CPOBJ.SWT EQ 0>
                    <!--- do nothing --->
                <cfelse>    
                    <cfinvoke method="RenderCPNewNew" component="session.sire.models.cfc.control-point" returnvariable="RetVarRenderCP">
                        <cfinvokeargument name="controlPoint" value="#RetVarReadCPDataById.CPOBJ#">
                        <cfinvokeargument name="inpBatchId" value="#campaignid#">   
                        <cfinvokeargument name="inpSimpleViewFlag" value="#inpSimpleViewFlag#"> 
                        <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">                     
                        <cfinvokeargument name="inpdisplayIntervalType" value="#displayIntervalType#">
                        <cfinvokeargument name="inpTemplateId" value="#templateId#">                     
                    </cfinvoke> 
                
                    <!--- This is the HTML for a CP opject - HTML is maintained in RenderCP function --->   
                    <div class="portlet light-new bordered cpedit msg-content" >
                    <!--- <pre>
                        <cfdump var="#RetVarReadCPDataById.CPOBJ.TYPE#"/>
                    </pre> --->
                    <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'TRAILER'>
                        <div class="portlet-body-3">
                    <cfelse>
                        <div class="portlet-body-2">    
                    </cfif>
                            #RetVarRenderCP.HTML#
                        </div>
                    </div>
                </cfif>    
            <cfelse>
                
            </cfif>
        </cfif>
    </cfloop>
    <!--- END CP RENDER WRAPPER --->
    <!--- BEGIN : SELECT SUBCRIBER LIST --->    
    <div class="portlet light-new bordered cpedit">
        <div class="portlet-body-5">
            <h4 class="portlet-heading-new">Who do you want to send this to? <a tabindex="0" class="info-popover" data-placement="right" data-trigger="focus" role="button"   data-html="true"   title="" data-content="A Subscriber List is a list of people who have opted in to your campaign.  We automatically capture all of your subscribers phone numbers and save them so you can quickly and easily send them messages in the future.</br></br>If this is your first campaign, you can create your first list by clicking &##34;Create New List&##34;.</br></br><a class='tooltip-link' href='https://www.siremobile.com/blog/support/build-your-customer-list-view-report/' target='_blank'>Click Here</a> to learn how to view and manage your lists."><img  src='../assets/layouts/layout4/img/info.png'/></a></h4>
            
            <div class="row row-small">
                <div class="col-md-5 col-xs-12">
                    <div class="form-gd form-lb-large">
                        <div class="form-group">
                            <select class="form-control validate[required] Select2" id="SubscriberBlastList" multiple="multiple">
                                <cfif arrayLen(subcriberList) GT 0>
                                    <cfloop array="#subcriberList#" index="array_index">
                                    <!---  <option #campaignData.BLAST_GROUPID EQ array_index[1] ? "selected" : ""# value="#array_index[1]#">#array_index[2]# (#array_index[3]#)</option> --->
                                        <option value="#array_index[1]#">#array_index[2]# (#array_index[3]#)</option>
                                     </cfloop>
                                 </cfif>
                             </select>
                        </div>
                        <div class="help-block pull-right">Total subcribers: <span id="totalSubcriberSelected" class="totalSubcriberSelected">0</span> 
                            <span> 
                                
                                <a tabindex="0" class="info-popover" data-placement="right" data-trigger="focus" role="button"   data-html="true"   title="" data-content="When sending to multiple lists: if a contact appears on more than one list, duplicates will be automatically removed so each contact will only receive 1 message."><img  src='../assets/layouts/layout4/img/info.png'/></a>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-xs-12">
                    
                </div>    
            </div>
            <a href="javascript:;" class="btn green-gd campaign-next adj-margin-bottom #hiddenClass#">Next <i class="fa fa-caret-down"></i></a>                                  
        </div>
    </div>
    <!--- END : SELECT SUBCRIBER LIST --->  

 
    <div class="portlet light bordered cpedit confirm-block">
        <div class="portlet-body-6">
            <h4 class="portlet-heading-new">Confirm & Send</h4>
            <p class="portlet-subheading">
                You are about to send the message
            </p>
            <div class="row">
                <div class="col-lg-6">
                    <!---old version: guess light-blue--->
                    <div class="bubble guess you" id="preview_message">
                        
                    </div>
                </div>
                <div class="col-lg-6">
                <p>                
                    #campaignBlastScheduleInfo.campaignBlastScheduleInfo#
                 </p>               
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p class="portlet-subheading">
                        To <span class="totalSubcriberSelected portlet-heading2">0</span> people 
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <a href="javascript:;" class="btn green-gd btn-blast-now">#btnSendNowText#</a>                
                    <a href="javascript:;" class="btn green-gd campaign-next btn-schedule-later">#btnBlastNowText#</a>
                    <a href="javascript:;" class="btn green-gd btn-save-blast btn-confirm-section">Save & Exit</a>
                </div>  
            </div>  
        </div>
    </div> 

    <!--- BEGIN SCHEDULE --->
    <cfif campaignData.TemplateType_ti EQ 1>

        <div class="portlet light bordered cpedit schedule-section #hiddenClass#">
            <div class="portlet-body-6">
                <h4 class="portlet-heading-new">Schedule Your Blast</h4>  
                <input type="hidden" id="schedule-option" value="#campaignData.ScheduleOption#"/>
                <p class="portlet-subheading">
                    The scheduled time can be changed before the campaign is started.
                </p>              
                <!--- simple schedule --->
                <div id ="simple_schedule">                    
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="row row-small">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input-group datetimepicker date " id="datetimepicker">
                                            <input type='text' class="form-control" value="#scheduleDateTime#" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="row row-small">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Start time</label>                                                                                
                                        <div class="input-group timepicker" id="time-picker-clock-icon">
                                            <input id="schedule-time-start-hour" type='text' class="form-control schedule-time-start-hour" value="#scheduleTime#" placeholder="9:00 AM - 8:00 PM"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>   

                </div>                                               
                <div> 
                    <br>                 
                    <a href="javascript:;" class="btn green-gd btn-save-schedule">Save & Send scheduled blast</a>
                    <a href="javascript:;" class="btn green-gd btn-cancel-schedule">Exit Schedule Options</a>
                </div>
            </div>
        </div> 
    </cfif>
    <div class="portlet light bordered cpedit hidden">
    </div> 
</cfoutput>