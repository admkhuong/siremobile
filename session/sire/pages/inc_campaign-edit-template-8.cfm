<cfoutput>            

 <!--- BEGIN : CAMPAIGN NAME --->                                                    
<div class="portlet light bordered">
    <div class="portlet-body-1">
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-gd form-lb-large">
                    <div class="form-group">                        
                        <h4 class="portlet-heading-new">Campaign Name</h4>
                        <h5 id="cp-char-count-1" class="control-point-char text-color-gray">
                        <cfif action eq "CreateNew">
                            <input type="text" style="display: none" class="form-control validate[required,custom[noHTML]]" placeholder="" name="Desc_vch" id="Desc_vch" value="#CDescHTML#"> 
                            <span id="cppNameDisplay" class="portlet-subheading">#CDescHTML#</span>
                            <span class="btn-re-edit edit-name-icon"><img src="/public/sire/images/create-new-pencil-button.png" class="img-responsive"> </i></span>                               

                            <cfif cpNameSectionShow EQ 0><input type="hidden" name="OrganizationName_vch" id="OrganizationName_vch" value="#RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH#"></cfif> 
                        <cfelse>
                            <input type="text" class="form-control validate[required,custom[noHTML]]" placeholder="" name="Desc_vch" id="Desc_vch" value="#CDescHTML#">                                    
                            <span class="help-block hidden">ex: Summer Happy Hour Promo – 20% off</span>  
                            <cfif cpNameSectionShow EQ 0><input type="hidden" name="OrganizationName_vch" id="OrganizationName_vch" value="#RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH#"></cfif>      
                        </cfif>                             
                    </div>
                </div>
            </div>
            <div class="col-md-12 hidden">                                
                <div class="form-group">
                        <a href="javascript:;" class="btn green-gd campaign-next btn-adj-pos #hiddenClass#">Next <i class="fa fa-caret-down"></i></a>&nbsp;&nbsp;
                    <cfif campaignData.BatchId_bi GT 0>
                        <a href="##" onclick="openCampaignSimon()" class="btn green-cancel campaign-simon-cancel pull-right #hiddenClass#">Cancel</a>       
                    </cfif>                   
                </div>
            </div>
        </div>    
    </div>
</div>        
<!--- END : CAMPAIGN NAME ---> 



<!--- BEGIN CP RENDER WRAPPER --->
<div class="portlet light-new bordered cpedit msg-content first-msg-content">
    <div class="portlet-body">

        <!--- RENDER CP --->
        <cfset MaxCPCount = 0 />        
        <cfset cpDisplayCount = 1 />        

        <cfif action EQ "Edit" or campaignid GT 0>
            <cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPs" returnvariable="RetVarReadCPs">
                <cfinvokeargument name="inpBatchId" value="#campaignid#">
                <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
            </cfinvoke>
        <cfelse>
            <cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPsFromTemplateID" returnvariable="RetVarReadCPs">
                <cfinvokeargument name="inpTemplateID" value="#templateid#">            
                <cfinvokeargument name="inpSelectedTemplateType" value="#selectedTemplateType#">
            </cfinvoke>
        </cfif>
        <cfif action NEQ 'Edit' AND campaignid EQ 0>
            <h4 class="portlet-heading-new msg-number">Drip Message 1</h4>
        </cfif>
        <cfloop array="#RetVarReadCPs.CPOBJ#" index="CPOBJ">                   
            <cfset MaxCPCount++ />

            <cfif action EQ 'Edit'  or campaignid GT 0>
                <cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPDataById" returnvariable="RetVarReadCPDataById">
                    <cfinvokeargument name="inpBatchId" value="#campaignid#">
                    <cfinvokeargument name="inpQID" value="#CPOBJ.RQ#">
                    <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
                </cfinvoke>
            <cfelse>
                <cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPTempalteDataById" returnvariable="RetVarReadCPDataById">
                    <cfinvokeargument name="inpTemplateID" value="#templateid#">            
                    <cfinvokeargument name="inpSelectedTemplateType" value="#selectedTemplateType#">
                    <cfinvokeargument name="inpQID" value="#CPOBJ.RQ#">                         
                </cfinvoke>
            </cfif>

            <!--- <pre>
                <cfdump var="#RetVarReadCPDataById.CPOBJ.TYPE#"/>
            </pre> --->
            
            <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'OPTIN'>
                <cfset campaignData.OPTIN_GROUPID = RetVarReadCPDataById.CPOBJ.OIG />
            </cfif>

            <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'ONESELECTION'>
                <cfset countCPOneSelection ++ />
            </cfif>    
            

            <!--- <cfif RetVarReadCPDataById.CPOBJ.TYPE NEQ 'BRANCH' AND RetVarReadCPDataById.CPOBJ.TYPE NEQ 'OPTIN' > --->
            <cfif RetVarReadCPDataById.CPOBJ.TYPE NEQ 'BRANCH' >
                <cfif RetVarReadCPDataById.CPOBJ.TYPE NEQ 'OPTIN' OR (RetVarReadCPDataById.CPOBJ.TYPE EQ 'OPTIN' AND displayOptin EQ 1)>

                    <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'TRAILER' AND RetVarReadCPDataById.CPOBJ.SWT EQ 0 >
                        <!--- do nothing --->
                    <cfelse>                                    
                        <cfif (RetVarReadCPDataById.CPOBJ.TYPE EQ 'STATEMENT' OR RetVarReadCPDataById.CPOBJ.TYPE EQ 'INTERVAL')>

                            <cfinvoke method="RenderCPNewNew" component="session.sire.models.cfc.control-point" returnvariable="RetVarRenderCP">
                                <cfinvokeargument name="controlPoint" value="#RetVarReadCPDataById.CPOBJ#">
                                <cfinvokeargument name="inpBatchId" value="#campaignid#">   
                                <cfinvokeargument name="inpSimpleViewFlag" value="#inpSimpleViewFlag#"> 
                                <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">                     
                                <cfinvokeargument name="inpdisplayIntervalType" value="#displayIntervalType#">                                    
                                <cfinvokeargument name="inpTemplateId" value="#templateId#">                     
                            </cfinvoke> 
                                <!--- This is the HTML for a CP opject - HTML is maintained in RenderCP function --->   
                            <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'INTERVAL' >
                                <cfif cpDisplayCount GT 1>
                                    <hr class="message-hr"/>    
                                </cfif>
                                <cfif action EQ 'Edit' >
                                    <h4 class="portlet-heading-new msg-number">Drip Message #cpDisplayCount-1#</h4>  
                                <cfelseif campaignid GT 0>
                                    <h4 class="portlet-heading-new msg-number">Drip Message #cpDisplayCount#</h4>                       
                                </cfif>
                                <cfset cpDisplayCount ++>                                
                                <cfset arrayAppend(arrCpInterval,RetVarReadCPDataById.CPOBJ.ID) >
                            <cfelseif RetVarReadCPDataById.CPOBJ.TYPE EQ 'STATEMENT'>
                                <cfset arrayAppend(arrCpStatement,RetVarReadCPDataById.CPOBJ.ID) >    
                            </cfif> 

                                #RetVarRenderCP.HTML#                            
                                
                        </cfif>        
                    </cfif>    
                </cfif>
            </cfif>

            <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'BRANCH'>
                <cfset arrayAppend(arrCpBranch,RetVarReadCPDataById.CPOBJ.ID) >
            </cfif>

            <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'OPTIN'>
                <cfset arrayAppend(arrCpOptIn, RetVarReadCPDataById.CPOBJ.ID) >
                <cfset cpOptIn = RetVarReadCPDataById.CPOBJ.RQ />
            </cfif>

            <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'SHORTANSWER'>
                <cfset arrayAppend(arrCpShortAns, RetVarReadCPDataById.CPOBJ.ID) >
            </cfif>

            <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'TRAILER'>
                <cfset arrayAppend(arrCpTrailer, RetVarReadCPDataById.CPOBJ.ID) >
            </cfif>
            

        </cfloop>        
    </div>
</div>
<!--- END CP RENDER WRAPPER --->


<!--- SELECT TEMPLATE TYPE --->
<div class="portlet light bordered cpedit ">
    <div class="portlet-body blue-green-margin-10">
        <h4 class="portlet-heading-new">
        <span class="tdesc">Who do you want to send this drip to?</span> 
        </h4>  
        <div class="uk-grid uk-grid-medium row-subscriber" uk-grid>
            <div class="uk-width-1-2@s #(campaignData.TemplateType_ti EQ 0 OR campaignData.TemplateType_ti EQ 9) ? "uk-active" : "uk-unactive"#" id="greenboxMain">
                <div class="subscriber-item greenbox campaign-next" data-type='0'<cfif campaignData.TemplateType_ti EQ 9> onclick="UpdateCampaignType('#campaignData.BATCHID_BI#','0');"</cfif>>
                    <div class="uk-grid uk-grid-small uk-flex-middle " >
                        <div class="uk-width-2-5 text-center">                                
                            <img class="img-responsive" src="../assets/layouts/layout4/img/subscriber-keyword.png" alt="" >                                                                
                        </div>
                        <div class="uk-width-3-5">
                            <h4 class="titleh4">Subscribers who send your</h4> 
                            <h4 class="titleh4"> Keyword to your short code </h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-width-1-2@s #(campaignData.TemplateType_ti EQ 1 OR campaignData.TemplateType_ti EQ 9) ? "uk-active" : "uk-unactive"#"  id="blueboxMain">
                <div class="subscriber-item bluebox campaign-next" data-type='1' <cfif campaignData.TemplateType_ti EQ 9 > onclick="UpdateCampaignType('#campaignData.BATCHID_BI#','1');" </cfif>>
                    <div class="uk-grid uk-grid-small uk-flex-middle">
                        <div class="uk-width-2-5 text-center">                                                              
                            <img class="img-responsive " src="../assets/layouts/layout4/img/subscriber-sound.png" alt="">                                
                        </div>
                        <div class="uk-width-3-5">
                            <h4 class="titleh4">Subscribers on an</h4>
                            <h4 class="titleh4"> existing list </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>    

        <div class="mb-30"></div>                    
    </div>
</div>

<!--- END SELECT TEMPLATE TYPE --->

<!--- START green campaign--->
<cfif campaignData.TemplateType_ti EQ 0 or campaignData.TemplateType_ti EQ 9>

    <!--- BEGIN : CHOOSE KEYWORD --->
    <div class="portlet light-new bordered cpedit green-campaign">
        <div class="portlet-body">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">                
                    <h4 class="portlet-heading-new">Choose a Keyword  <a tabindex="0" class="info-popover" data-placement="right" data-trigger="focus" role="button" data-html="true"   title="" data-content="A Keyword is a term or phrase, unique to your business, which is used to trigger a SMS communication between you and your customer.  An example is if you see an ad that says:  Text &##34;Coffee&##34; to 39492.  Coffee is the keyword a customer will use to opt-in or begin a text conversation with you.</br></br><img width='30px' src='../assets/layouts/layout4/img/Icon_Tip.png'/> Make your keyword short and memorable." ><img  src='../assets/layouts/layout4/img/info.png'/></a></h4>                                   
                        
                    <div class="form-gd">
                        <div class="form-group">                        
                            <input value="#campaignData.Keyword_txt#" class="form-control validate[required]" id="Keyword" type="text" name="Keyword" maxlength="160" style="width:100%;" />                                                                                                
                        </div>
                        <div class="form-group">                                                
                            <span class="has-error KeywordStatus" style="display: none;" id="KeywordStatus"></span>                                                                                                
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-md-7 hidden-sm hidden-xs">
                    
                    <div class="movi-short">                    
                        <div class="movi-heading">
                            <h3 class="movi-heading__number">#session.Shortcode#</h3>
                        </div>
                        <div class="movi-body">
                            <div class="media chat__item chat--sent">
                                <div class="media-body media-bottom">
                                    <div class="chat__text">
                                        <span class="span_keyword">Localdinner</span>                                    
                                    </div>
                                </div>
                                <div class="media-right media-bottom">
                                    <div class="chat__user">
                                        <img src="../assets/layouts/layout4/img/avatar-women.png" />
                                    </div>
                                </div>
                            </div>                        
                        </div>
                    </div><!--End Mobile View -->
                </div>
            </div>     
            <a href="javascript:;" class="btn green-gd campaign-next check-kw btn-adj-pos">Next <i class="fa fa-caret-down"></i></a>
        </div>
    </div>

    <!--- END : CHOOSE KEYWORD --->

    <!--- BEGIN : SELECT SUBCRIBER LIST --->    
    <div class="portlet light-new bordered cpedit green-campaign">
        <div class="portlet-body">
            <h4 class="portlet-heading-new">Choose a Subscriber List <a tabindex="0" class="info-popover" data-placement="right" data-trigger="focus" role="button"   data-html="true"   title="" data-content="A Subscriber List is a list of people who have opted in to your campaign.  We automatically capture all of your subscribers phone numbers and save them so you can quickly and easily send them messages in the future.</br></br>If this is your first campaign, you can create your first list by clicking &##34;Create New List&##34;.</br></br><a class='tooltip-link' href='https://www.siremobile.com/blog/support/build-your-customer-list-view-report/' target='_blank'>Click Here</a> to learn how to view and manage your lists."><img  src='../assets/layouts/layout4/img/info.png'/></a></h4>
            
            <div class="row ">
                <div class="col-md-5 col-xs-12">
                    <div class="form-gd form-lb-large">
                        <div class="form-group">
                            <select class="form-control validate[required] Select2" id="SubscriberList">
                                <cfif arrayLen(subcriberList) GT 0>
                                    <cfloop array="#subcriberList#" index="array_index">
                                        <option #campaignData.OPTIN_GROUPID EQ array_index[1] ? "selected" : ""# value="#array_index[1]#">#array_index[2]#</option>
                                    </cfloop>
                                </cfif>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-xs-12">
                    <a href="##" class="btn grey-mint btn-create-new-list"><i class="fa fa-plus"></i> create new list</a>
                </div>    
            </div>
            <a href="javascript:;" class="btn green-gd campaign-next btn-adj-pos #hiddenClass#">Next <i class="fa fa-caret-down"></i></a>                                  
        </div>
    </div>
    <!--- END : SELECT SUBCRIBER LIST ---> 

    <!--- BEGIN CP RENDER WRAPPER --->


    <!--- RENDER CP --->
    <cfif action EQ "Edit">
        <cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPs" returnvariable="RetVarReadCPs">
            <cfinvokeargument name="inpBatchId" value="#campaignid#">
            <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
        </cfinvoke>
    <cfelse>
        <cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPsFromTemplateID" returnvariable="RetVarReadCPs">
            <cfinvokeargument name="inpTemplateID" value="#templateid#">            
            <cfinvokeargument name="inpSelectedTemplateType" value="#selectedTemplateType#">
        </cfinvoke>
    </cfif>
    
    
    <cfloop array="#RetVarReadCPs.CPOBJ#" index="CPOBJ">                   
        
        <cfif action EQ 'Edit'>
            <cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPDataById" returnvariable="RetVarReadCPDataById">
                <cfinvokeargument name="inpBatchId" value="#campaignid#">
                <cfinvokeargument name="inpQID" value="#CPOBJ.RQ#">
                <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
            </cfinvoke>
        <cfelse>
            <cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPTempalteDataById" returnvariable="RetVarReadCPDataById">
                <cfinvokeargument name="inpTemplateID" value="#templateid#">            
                <cfinvokeargument name="inpSelectedTemplateType" value="#selectedTemplateType#">
                <cfinvokeargument name="inpQID" value="#CPOBJ.RQ#">                         
            </cfinvoke>
        </cfif>

        <!--- <cfif RetVarReadCPDataById.CPOBJ.TYPE NEQ 'BRANCH' AND RetVarReadCPDataById.CPOBJ.TYPE NEQ 'OPTIN' > --->
        <cfif RetVarReadCPDataById.CPOBJ.TYPE NEQ 'BRANCH' >
            <!--- <cfif RetVarReadCPDataById.CPOBJ.TYPE NEQ 'OPTIN' OR (RetVarReadCPDataById.CPOBJ.TYPE EQ 'OPTIN' AND displayOptin EQ 1)> --->
            <cfif RetVarReadCPDataById.CPOBJ.TYPE NEQ 'OPTIN' OR (RetVarReadCPDataById.CPOBJ.TYPE EQ 'OPTIN')>
                <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'TRAILER' AND RetVarReadCPDataById.CPOBJ.SWT EQ 0 >
                    <!--- do nothing --->
                <cfelse>                                    
                    <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'SHORTANSWER' OR RetVarReadCPDataById.CPOBJ.TYPE EQ 'TRAILER' OR ( RetVarReadCPDataById.CPOBJ.TYPE EQ 'STATEMENT' AND RetVarReadCPDataById.CPOBJ.RQ EQ 5) >
                        <cfinvoke method="RenderCPNewNew" component="session.sire.models.cfc.control-point" returnvariable="RetVarRenderCP">
                            <cfinvokeargument name="controlPoint" value="#RetVarReadCPDataById.CPOBJ#">
                            <cfinvokeargument name="inpBatchId" value="#campaignid#">   
                            <cfinvokeargument name="inpSimpleViewFlag" value="#inpSimpleViewFlag#"> 
                            <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">                     
                            <cfinvokeargument name="inpdisplayIntervalType" value="#displayIntervalType#">                                    
                            <cfinvokeargument name="inpTemplateId" value="#templateId#">                     
                        </cfinvoke> 
                            <!--- This is the HTML for a CP opject - HTML is maintained in RenderCP function --->   
                            <div class="portlet light-new bordered green-campaign msg-content cpedit " >
                            <!--- <pre>
                                <cfdump var="#RetVarReadCPDataById.CPOBJ.TYPE#"/>
                            </pre> --->
                            <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'TRAILER'>
                                <div class="portlet-body-3">
                            <cfelse>
                                <div class="portlet-body-2">    
                            </cfif>
                                    #RetVarRenderCP.HTML#
                                </div>
                            </div>
                    </cfif>        
                </cfif>    

            </cfif>
        </cfif>
    </cfloop>

           
    <!--- END CP RENDER WRAPPER --->
    <div class="portlet light bordered cpedit cpg-mlp-spread-the-word green-campaign">
        <h4 class="portlet-heading-new">Spread the Word</h4>
        <p class="portlet-subheading text-color-gray set-margin-bt-0">First, let’s test it out. Grab your phone and text your keyword to the phone number 39492. <br>If it looks good, create a Marketing Landing Page (MLP) to let others know about this campaign.<br><br>Click on each section in the MLP to edit the text and logo. <span><a tabindex="0" class="info-popover" data-trigger="focus" role="button" data-placement="right" data-html="true" title="" data-content="<h5 style='color: black; font-size: 16px;' class='desc-hover-ul'>Think of a MLP as a simple web page or digital flyer used to spread the word about your text marketing campaign. Click on each section within the MLP to edit the text and logo.  A link is automatically created so you can share the MLP as a web page. <br><br>To view or edit your MLPs, click on Campaigns > Manage Campaigns in the main menu. When complete you can:</h5><ul><li type='i'>Use the url to share it as a web page.</li> <li type='i'>Download it as a PDF or JPEG file to print and share</li></ul><img width='30px' src='../assets/layouts/layout4/img/Icon_Tip.png'/>Toggling the “Link Access” switch off will deactivate the link." data-original-title=""><img src="../assets/layouts/layout4/img/info.png"></a></span></p>
        <!--- <ul class='campaign-mlp-desc-text'>
            <li type="1">Share this campaign on your social media feeds.</li>
            <li type="1">Create a Marketing Landing Page (MLP) to share as a web page or download for print.</li>
        </ul> --->
        <cfset RAWCONTENTTOSHOW = RAWCONTENTTOSHOW.replace('<h3 style=" margin-top: 20px;">To receive <span class="mlp-off-percent">20% OFF</span></h3>', '')/>
        <cfset RAWCONTENTTOSHOW = RAWCONTENTTOSHOW.replace('<h3>any of our course entrees</h3>', '<h3> To sign up for our text notifications</h3>')/>
        
        <div class="campaign-mlp-modal-body">
            #RAWCONTENTTOSHOW#
        </div>
        
        <div class="col-lg-12 finished-campaign-new">
            <div class="campaign-mlp-link-access col-lg-12">
                <div class="campaign-mlp-link-access-switch hidden">
                    <span class="campaign-mlp-link"><strong>Link Access: </strong></span>
                    <label class="switch">
                        <input id="check-campaign-access-link" type="checkbox" checked>
                        <span class="slider round"></span>
                    </label>
                </div>

                <div class="">
                    <!--- <input type="text" id="cpg-mlp-link-domain" name="cpg-mlp-link-domain" class="form-control cpg-mlp-link" value="https://www.mlp-x.com/lz/" disabled> --->
                    <!--- <input type="text" id="cpg-mlp-link" name="cpg-mlp-link" class="form-control cpg-mlp-link" value="<cfoutput>#MLPDOMAINPATH##MLPLINK#</cfoutput>" disabled> --->
                    <a href="<cfoutput>#MLPDOMAINPATH##MLPLINK#</cfoutput>" id="cpg-mlp-link" target="blank"><cfoutput>#MLPDOMAINPATH##MLPLINK#</cfoutput></a>
                </div>

            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 finished-campaign-new">
            <div class="col-lg-4 col-md-8 col-sm-6 campaign-mlp-link-social">
                <!--- <div class="container-fluid"> --->
                    <div class="form-group social share-url">
                        <div class="campaign-mlp-link-access-ds share-url">
                            <a href title="Facebook" id="campaign-mlp-fb" class="invite-social-ic fb social-auth"><img class="campaign-mlp-link-access-icon" src="/session/sire/images/Share_Facebook.png?v=1.0" alt="Facebook"/><i class="fa fa-facebook-square hidden"></i></a>
                            <div class="campaign-mlp-link-access-text text-center">Facebook</div>
                        </div>

                        <div class="campaign-mlp-link-access-ds ">
                            <a href title="Twitter" id="campaign-mlp-tt" class="invite-social-ic tw social-auth-tw"><img class="campaign-mlp-link-access-icon" src="/session/sire/images/Share_Twitter.png?v=1.0" alt="Twitter"/><i class="fa fa-twitter-square hidden"></i></a>
                            <div class="campaign-mlp-link-access-text text-center">Twitter</div>
                        </div>

                        <!--- <div class="form-group social share-url">
                          <!--- <label class="block">Share</label> --->
                          <a class="invite-social-ic fb social-auth">
                              <i class="fa fa-facebook-square"></i>
                          </a>
                          <a class="invite-social-ic tw social-auth-tw">
                              <i class="fa fa-twitter-square"></i>
                          </a>
                          <a class="invite-social-ic gp social-auth">
                              <i class="fa fa-google-plus-square"></i>
                          </a>
                        </div> --->

                        <div class="campaign-mlp-link-access-ds campaign-mlp-dl">
                            <a target="_blank" title="PDF" id="campaign-mlp-pdf" class="invite-social-ic"><img class="campaign-mlp-link-access-icon" src="/session/sire/images/Share-PDF.png?v=1.0" alt="Youtube"/></a>
                            <div class="campaign-mlp-link-access-text text-center">PDF File</div>
                        </div>

                        <div class="campaign-mlp-link-access-ds campaign-mlp-dl">
                            <a target="_blank" title="JPG" id="campaign-mlp-jpg"  class="invite-social-ic"><img class="campaign-mlp-link-access-icon" src="/session/sire/images/Share-JPG.png?v=1.0" alt="LinkedIn"/></a>
                            <div class="campaign-mlp-link-access-text text-center">JPG File</div>
                        </div>

                    </div>
                <!--- </div> --->
            </div>
        </div>

        <div id="previewImage" class="hidden">
        </div>
        <div class="finished-campaign-btn">
            <a href="javascript:;" class="btn newbtn new-blue-gd btn-finished">#btnSaveLabel#</a>
        </div>        
        
    </div>
</cfif>    
<!--- end green campaign--->

<!--- start blue campaign--->
<!--- BEGIN : SELECT SUBCRIBER LIST --->
<cfif campaignData.TemplateType_ti EQ 1 or campaignData.TemplateType_ti EQ 9>
    <div class="portlet light-new bordered cpedit blue-campaign">
        <div class="portlet-body-5">
            <h4 class="portlet-heading-new">Who do you want to send this to? <a tabindex="0" class="info-popover" data-placement="right" data-trigger="focus" role="button"   data-html="true"   title="" data-content="A Subscriber List is a list of people who have opted in to your campaign.  We automatically capture all of your subscribers phone numbers and save them so you can quickly and easily send them messages in the future.</br></br>If this is your first campaign, you can create your first list by clicking &##34;Create New List&##34;.</br></br><a class='tooltip-link' href='https://www.siremobile.com/blog/support/build-your-customer-list-view-report/' target='_blank'>Click Here</a> to learn how to view and manage your lists."><img  src='../assets/layouts/layout4/img/info.png'/></a></h4>
            
            <div class="row row-small">
                <div class="col-md-5 col-xs-12">
                    <div class="form-gd form-lb-large">
                        <div class="form-group">
                            <select class="form-control validate[required] Select2" id="SubscriberBlastList" multiple="multiple">
                                <cfif arrayLen(subcriberList) GT 0>
                                    <cfloop array="#subcriberList#" index="array_index">
                                    <!---  <option #campaignData.BLAST_GROUPID EQ array_index[1] ? "selected" : ""# value="#array_index[1]#">#array_index[2]# (#array_index[3]#)</option> --->
                                        <option value="#array_index[1]#">#array_index[2]# (#array_index[3]#)</option>
                                     </cfloop>
                                 </cfif>
                             </select>
                        </div>
                        <div class="help-block pull-right">Total subcribers: <span id="totalSubcriberSelected" class="totalSubcriberSelected">0</span> 
                            <span> 
                                
                                <a tabindex="0" class="info-popover" data-placement="right" data-trigger="focus" role="button"   data-html="true"   title="" data-content="When sending to multiple lists: if a contact appears on more than one list, duplicates will be automatically removed so each contact will only receive 1 message."><img  src='../assets/layouts/layout4/img/info.png'/></a>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-xs-12">
                    
                </div>    
            </div>
            <a href="javascript:;" class="btn green-gd campaign-next adj-margin-bottom  #hiddenClass#">Next <i class="fa fa-caret-down"></i></a>                                  
        </div>
    </div>
    <!--- END : SELECT SUBCRIBER LIST --->

    <!--- BEGIN SCHEDULE --->

    <div class="portlet light bordered cpedit blue-campaign confirm-block">
        <div class="portlet-body-6">
            <h4 class="portlet-heading-new">Confirm & Send</h4>
            <p class="portlet-subheading">
                You are about to send the message
            </p>
            <div class="row">
                <div class="col-lg-6">
                    <!---old version: guess light-blue--->
                    <div class="bubble guess you" id="preview_message">
                        
                    </div>
                </div>
                <div class="col-lg-6">
                <p>                
                    #campaignBlastScheduleInfo.campaignBlastScheduleInfo#
                 </p>               
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p class="portlet-subheading">
                        To <span class="totalSubcriberSelected portlet-heading2">0</span> people 
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <a href="javascript:;" class="btn green-gd btn-blast-now">#btnSendNowText#</a>                
                    <a href="javascript:;" class="btn green-gd campaign-next btn-schedule-later">#btnBlastNowText#</a>
                    <a href="javascript:;" class="btn green-gd btn-save-blast btn-confirm-section">Save & Exit</a>
                </div>  
            </div>  
        </div>
    </div> 
    <div class="portlet light bordered cpedit blue-campaign schedule-section #hiddenClass#">
        <div class="portlet-body-6">
            <h4 class="portlet-heading-new">Schedule Your Blast</h4>  
            <input type="hidden" id="schedule-option" value="#campaignData.ScheduleOption#"/>
            <p class="portlet-subheading">
                The scheduled time can be changed before the campaign is started.
            </p>              
            <!--- simple schedule --->
            <div id ="simple_schedule">                    
                <div class="row">
                    <div class="col-lg-6">
                        <div class="row row-small">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group datetimepicker date" id="datetimepicker">
                                        <input type='text' class="form-control" value="#scheduleDateTime#" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-lg-6">
                        <div class="row row-small">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Start time</label>                                                                                
                                    <div class="input-group timepicker" id="time-picker-clock-icon">
                                        <input id="schedule-time-start-hour" type='text' class="form-control schedule-time-start-hour" value="#scheduleTime#" placeholder="9:00 AM - 8:00 PM"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-time"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>   

            </div>                                               
            <div> 
                <br>                 
                <a href="javascript:;" class="btn green-gd btn-save-schedule">Save & Send scheduled blast</a>
                <a href="javascript:;" class="btn green-gd btn-cancel-schedule">Exit Schedule Options</a>
            </div>
        </div>
    </div>     
</cfif>
<!--- END SCHEDULE --->
<!--- end blue campaign--->


</cfoutput>