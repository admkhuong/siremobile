<cfoutput>  

<!--- BEGIN : CAMPAIGN NAME --->    

<div class="portlet light bordered hidden">
    <div class="portlet-body-1">
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-gd form-lb-large">
                    <div class="form-group">                        
                        <h4 class="portlet-heading-new">Campaign Name</h4>
                        <h5 id="cp-char-count-1" class="control-point-char text-color-gray">
                        <input type="text" class="form-control validate[required,custom[noHTML]]" placeholder="" name="Desc_vch" id="Desc_vch" value="#CDescHTML#">                                    
                        <span class="help-block hidden">ex: Summer Happy Hour Promo – 20% off</span>  
                        <cfif cpNameSectionShow EQ 0><input type="hidden" name="OrganizationName_vch" id="OrganizationName_vch" value="#RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH#"></cfif>      
                    </div>
                </div>
            </div>
            <div class="col-md-12 hidden">                                
                <div class="form-group">
                        <a href="javascript:;" class="btn green-gd campaign-next btn-adj-pos #hiddenClass#">Next <i class="fa fa-caret-down"></i></a>&nbsp;&nbsp;
                    <cfif campaignData.BatchId_bi GT 0>
                        <a href="##" onclick="openCampaignSimon()" class="btn green-cancel campaign-simon-cancel pull-right #hiddenClass#">Cancel</a>       
                    </cfif>                   
                </div>
            </div>
        </div>    
    </div>
</div>      
<!--- END : CAMPAIGN NAME ---> 

<!--- BEGIN : CHOOSE KEYWORD --->
<div class="portlet light-new bordered cpedit">
    <div class="portlet-body">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">                
                <h4 class="portlet-heading-new">Choose a Keyword that will start trouble ticket<a tabindex="0" class="info-popover" data-placement="right" data-trigger="focus" role="button" data-html="true"   title="" data-content="A Keyword is a term or phrase, unique to your business, which is used to trigger a SMS communication between you and your customer.  An example is if you see an ad that says:  Text &##34;Coffee&##34; to 39492.  Coffee is the keyword a customer will use to opt-in or begin a text conversation with you.</br></br><img width='30px' src='../assets/layouts/layout4/img/Icon_Tip.png'/> Make your keyword short and memorable." ><img  src='../assets/layouts/layout4/img/info.png'/></a></h4>                                   
                    
                <div class="form-gd">
                    <div class="form-group">                        
                        <input value="#campaignData.Keyword_txt#" class="form-control validate[required]" id="Keyword" type="text" name="Keyword" maxlength="160" style="width:100%;" />                                                                                                
                    </div>
                    <div class="form-group">                                                
                        <span class="has-error KeywordStatus" style="display: none;" id="KeywordStatus"></span>                                                                                                
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 hidden-sm hidden-xs">
                
                <div class="movi-short">                    
                    <div class="movi-heading">
                        <h3 class="movi-heading__number">#session.Shortcode#</h3>
                    </div>
                    <div class="movi-body">
                        <div class="media chat__item chat--sent">
                            <div class="media-body media-bottom">
                                <div class="chat__text">
                                    <span class="span_keyword">Localdinner</span>                                    
                                </div>
                            </div>
                            <div class="media-right media-bottom">
                                <div class="chat__user">
                                    <img src="../assets/layouts/layout4/img/avatar-women.png" />
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div><!--End Mobile View -->
            </div>
        </div>     
<!---         <a href="javascript:;" class="btn green-gd campaign-next check-kw btn-adj-pos #hiddenClass#">Next <i class="fa fa-caret-down"></i></a>                         --->
    </div>
</div>
<!--- END : CHOOSE KEYWORD --->

<!--- BEGIN CP RENDER WRAPPER --->

<!--- RENDER CP --->
<cfset MaxCPCount = 0 />

<cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPs" returnvariable="RetVarReadCPs">
    <cfinvokeargument name="inpBatchId" value="#campaignid#">
    <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
</cfinvoke>

<cfloop array="#RetVarReadCPs.CPOBJ#" index="CPOBJ">                   
    <cfset MaxCPCount++ />
    <cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPDataById" returnvariable="RetVarReadCPDataById">
        <cfinvokeargument name="inpBatchId" value="#campaignid#">
        <cfinvokeargument name="inpQID" value="#CPOBJ.RQ#">
        <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
    </cfinvoke>

    <cfif templateId EQ 11>
        <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'ONESELECTION' || RetVarReadCPDataById.CPOBJ.TYPE EQ 'SHORTANSWER' >
            <cfset countCPOneSelection ++ />
        </cfif>    
    <cfelse>
        <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'ONESELECTION'>
            <cfset countCPOneSelection ++ />
        </cfif>    
    </cfif>
   
    <!--- <cfif RetVarReadCPDataById.CPOBJ.TYPE NEQ 'BRANCH' AND RetVarReadCPDataById.CPOBJ.TYPE NEQ 'OPTIN' > --->
    <cfif RetVarReadCPDataById.CPOBJ.TYPE NEQ 'BRANCH' OR (templateId EQ 7) >
        <cfif RetVarReadCPDataById.CPOBJ.TYPE NEQ 'OPTIN' AND RetVarReadCPDataById.CPOBJ.TYPE NEQ 'API'>

            <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'TRAILER' AND RetVarReadCPDataById.CPOBJ.SWT EQ 0>
                <!--- do nothing --->
            <cfelse>    
                <cfinvoke method="RenderCPNewNew" component="session.sire.models.cfc.control-point" returnvariable="RetVarRenderCP">
                    <cfinvokeargument name="controlPoint" value="#RetVarReadCPDataById.CPOBJ#">
                    <cfinvokeargument name="inpBatchId" value="#campaignid#">   
                    <cfinvokeargument name="inpSimpleViewFlag" value="#inpSimpleViewFlag#"> 
                    <cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">                     
                    <cfinvokeargument name="inpdisplayIntervalType" value="#displayIntervalType#">
                    <cfinvokeargument name="inpTemplateId" value="#templateId#">                     
                </cfinvoke> 
               
                <!--- This is the HTML for a CP opject - HTML is maintained in RenderCP function --->   
                <div class="portlet light-new bordered cpedit" >
                <cfif RetVarReadCPDataById.CPOBJ.TYPE EQ 'TRAILER'>
                    <div class="portlet-body-3">
                <cfelse>
                    <div class="portlet-body-2">    
                </cfif>
                        #RetVarRenderCP.HTML#
                    </div>
                </div>
            </cfif>
        </cfif>
    </cfif>
</cfloop>
<!--- END CP RENDER WRAPPER --->
<div class="portlet light bordered cpedit">
    <div class="finished-campaign-btn">
        <a href="javascript:;" class="btn newbtn new-blue-gd btn-finished-troubleticket">#btnSaveLabel#</a>
    </div>        
</div>

</cfoutput>