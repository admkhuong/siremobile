<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke> 
<script>
    userId= <cfoutput>#Session.UserId#</cfoutput>
    <cfif structKeyExists(url,'firstlogin')>
        firstLogin = <cfoutput>#url.firstlogin#</cfoutput>
        <cfelse>
        firstLogin = 0
    </cfif>    
</script>
<cfif userInfo.USERTYPE EQ 2 >
    <div class="modal be-modal fade md-inte" id="mdIntegrate-0" data-id="0" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content set-width-content1  integrate-modal">                        
                <div class="modal-body">
                    <button type="button" class="btn-close" data-dismiss="modal" aria-hidden="true">x</button>
                    <div class="">                    
                        <div class="row form-group">                        
                            <div class="col-md-7 col-sm-12 col-xs-12">                                       
                                <h4 class="title">
                                    Thanks for joining TheRestaurantExpert.com</br>
                                    text
                                    marketing platform.
                                </h4>
                                <p>
                                    Text marketing is proven to be one of the most 
                                    affordable and effective ways to increase repeat
                                    customers, table turnover rates and revenue.</br>
                                    Let's take a quick tour to get you acclimated.
                                        
                                </p>                          
                            </div>
                            <div class="col-md-1"></div>                        
                            <div class="col-md-4 text-right">
                                <img class="load-back" src='../assets/layouts/layout4/img/dashboard-slide-tre.png'/>
                            </div>
                        </div>
                        <div class="row">                    
                            <div class="col-xs-8">                                                                                           
                                <button type="button" class="btn btn-itegrate btn-style-1" data-id="0"/>NEXT</button>
                                <button type="button" class="btn btn-itegrate btn-style-2" data-dismiss="modal"/>SKIP</button>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="form-check text-right set-pad">
                                    <input type="checkbox" class="md-chk-remember" id="chk1">
                                    <label class="check-label" for="chk1">Do not show again</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>            
            </div>
        </div>
    </div>  
<cfelse>
    <div class="modal be-modal fade md-inte" id="mdIntegrate-0" data-id="0" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content set-width-content1  integrate-modal">                        
                <div class="modal-body">
                    <button type="button" class="btn-close" data-dismiss="modal" aria-hidden="true">x</button>
                    <div class="">                    
                        <div class="row form-group">                        
                            <div class="col-md-7 col-sm-12 col-xs-12">                                       
                                <h4 class="title">
                                    Welcome to Sire!</br>
                                    We're glad to have you</br>
                                    on board.
                                </h4>
                                <p>
                                    Text marketing is proven to be one of the most  
                                    affordable and effective ways to increase repeat 
                                    business, send customer alerts, improve customer  
                                    service and increase revenue. </br> </br>

                                    Let's take a quick tour to get you acclimated.                               
                                    
                                </p>                          
                            </div>
                            <div class="col-md-1"></div>                        
                            <div class="col-md-4 text-right">
                                <img class="load-back" src='../assets/layouts/layout4/img/dashboard-slide-info.png'/>
                            </div>
                        </div>
                        <div class="row">                    
                            <div class="col-xs-8">                                                                                           
                                <button type="button" class="btn btn-itegrate btn-style-1" data-id="0"/>NEXT</button>
                                <button type="button" class="btn btn-itegrate btn-style-2" data-dismiss="modal"/>SKIP</button>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="form-check text-right set-pad">
                                    <input type="checkbox" class="md-chk-remember" id="chk1">
                                    <label class="check-label" for="chk1">Do not show again</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>            
            </div>
        </div>
    </div>  
</cfif>
<div class="modal be-modal fade md-inte" id="mdIntegrate-1" data-id="1" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content set-width-content1  integrate-modal">                        
            <div class="modal-body">
                <button type="button" class="btn-close" data-dismiss="modal" aria-hidden="true">x</button>
                <div class="">                    
                    <div class="row form-group">
                        <div class="col-md-7 col-sm-12 col-xs-12">                                       
                            <h4 class="title">TIP #1</h4>
                            <p>                                
                                Before you build your first campaign, make sure to
                                complete your Account Profile.</br></br>

                                A complete profile will end up being a huge time saver 
                                since this information will be automatically inserted into some campaigns.
                            </p>                          
                        </div>
                        <div class="col-md-1"></div>                        
                        <div class="col-md-4 text-right">
                            <img class="load-back"  src='../assets/layouts/layout4/img/dashboard-slide-1.gif'/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">                                       
                            <button type="button" class="btn btn-itegrate btn-style-1" data-id="1"/>NEXT</button>                            
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="form-check text-right set-pad">
                                <input type="checkbox" class="md-chk-remember" id="chk1">
                                <label class="check-label" for="chk1">Do not show again</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>  
<div class="modal be-modal fade md-inte" id="mdIntegrate-2" data-id="2" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content set-width-content1  integrate-modal">                        
            <div class="modal-body">
                <button type="button" class="btn-close" data-dismiss="modal" aria-hidden="true">x</button>
                <div class="">                    
                    <div class="row form-group">
                        <div class="col-md-7 col-sm-12 col-xs-12">                                       
                            <h4 class="title">TIP #2</h4>
                            <p>
                                Once your profile is complete, let's build your first campaign!
                                Just go back to the dashboard, click on <span class='hightlight'>"Campaigns"</span> in the 
                                side menu and select Create Campaign.  The first campaign
                                you build should always be an <span class='hightlight'>"Opt-in"</span> campaign so you can
                                start building your subscriber list.  The first template is 
                                conveniently labeled <span class='hightlight'>"Build Your First Campaign."</span> 
                                Click on that and simply follow the step-by-step instructions.
                            </p>                          
                        </div>
                        <div class="col-md-1"></div>                        
                        <div class="col-md-4 text-right">
                            <img class="load-back"  src='../assets/layouts/layout4/img/dashboard-slide-2.gif'/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">                                       
                            <button type="button" class="btn btn-itegrate btn-style-1" data-id="2"/>NEXT</button>                            
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="form-check text-right set-pad">
                                <input type="checkbox" class="md-chk-remember" id="chk1">
                                <label class="check-label" for="chk1">Do not show again</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>  
<div class="modal be-modal fade md-inte" id="mdIntegrate-3" data-id="3" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content set-width-content1  integrate-modal">                        
            <div class="modal-body">
                <button type="button" class="btn-close" data-dismiss="modal" aria-hidden="true">x</button>
                <div class="">                    
                    <div class="row form-group">
                        <div class="col-md-7 col-sm-12 col-xs-12">                                       
                            <h4 class="title">TIP #3</h4>
                            <p>
                                You can always view, edit or delete campaigns by clicking on
                                <span class='hightlight'>"Manage Campaigns"</span> in the menu, and follow the simple,
                                step-by-step instructions.
                            </p>                          
                        </div>
                        <div class="col-md-1"></div>                        
                        <div class="col-md-4 text-right">
                            <img class="load-back"  src='../assets/layouts/layout4/img/dashboard-slide-3.gif'/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">                                       
                            <button type="button" class="btn btn-itegrate btn-style-1" data-id="3"/>NEXT</button>                            
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="form-check text-right set-pad">
                                <input type="checkbox" class="md-chk-remember" id="chk1">
                                <label class="check-label" for="chk1">Do not show again</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>  
<div class="modal be-modal fade md-inte" id="mdIntegrate-4" data-id="4" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content set-width-content1  integrate-modal">                        
            <div class="modal-body">
                <button type="button" class="btn-close" data-dismiss="modal" aria-hidden="true">x</button>
                <div class="">                    
                    <div class="row form-group">
                        <div class="col-md-7 col-sm-12 col-xs-12">                                       
                            <h4 class="title">TIP #4</h4>
                            <p>
                                The fun part about Text marketing is watching your subscriber
                                lists grow.  To view, create or delete your lists, click on
                                <span class='hightlight'>"Subscribers"</span> in main Menu.
                            </p>                          
                        </div>
                        <div class="col-md-1"></div>                        
                        <div class="col-md-4 text-right">
                            <img class="load-back"  src='../assets/layouts/layout4/img/dashboard-slide-4.gif'/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">                                       
                            <button type="button" class="btn btn-itegrate btn-style-1" data-id="4"/>NEXT</button>                            
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="form-check text-right set-pad">
                                <input type="checkbox" class="md-chk-remember" id="chk1">
                                <label class="check-label" for="chk1">Do not show again</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>  
<div class="modal be-modal fade md-inte" id="mdIntegrate-5" data-id="5" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content set-width-content1  integrate-modal">                        
            <div class="modal-body">
                <button type="button" class="btn-close" data-dismiss="modal" aria-hidden="true">x</button>
                <div class="">                    
                    <div class="row form-group">
                        <div class="col-md-7 col-sm-12 col-xs-12">                                       
                            <h4 class="title">Don't forget,</br>we're here to help.</h4>
                            <p>                                
                                You can click on the helpful information icons in any template 
                                or click on the blue chat button in the bottom right corner of
                                your screen for help.  If you prefer to speak to someone on the 
                                phone, would like a demo or want tips about how to spread 
                                the word, just email us at <span class='hightlight lower'>hello@siremobile.com</span> and we'll 
                                get that scheduled right away.  We're here to help you 
                                succeed.  We know a new platform can be daunting so don't 
                                be afraid to reach out with questions!  
                            </p>                          
                        </div>
                        <div class="col-md-1"></div>                        
                        <div class="col-md-4 text-right">
                            <img class="load-back"  src='../assets/layouts/layout4/img/dashboard-slide-5.gif'/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">                                       
                            <button type="button" class="btn btn-itegrate btn-style-1" data-id="5"/>GET STARTED</button>                            
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="form-check text-right set-pad">
                                <input type="checkbox" class="md-chk-remember" id="chk1">
                                <label class="check-label" for="chk1">Do not show again</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>  