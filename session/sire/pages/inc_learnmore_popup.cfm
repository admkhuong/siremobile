<div class="modal be-modal fade" id="mdLearmore-1" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content set-width-content center-modal">                        
            <div class="modal-body mdLearmore">
                <button type="button" data-dismiss="modal" aria-label="Close" class="pull-right btn-close-popup-new"><span aria-hidden="true">×</span></button> 
                <div class="mdLearmoreBody">                    
                    <div class="row">
                        <div class="col-md-8 col-sm-12 col-xs-12">                                       
                            <h4 class="learmore_title">Build Your First Campaign:</h4>
                            <ul>                                                                 
                                <li>Create your marketing list</li> 
                                <li>Build customer loyalty</li> 
                                <li>Increase repeat customers</li> 
                                <li>Improve customer satisfaction</li> 
                            </ul>
                            <span class="span-learmore">Learn more  <a href="https://www.siremobile.com/blog/support/build-first-campaign/" target="_blank"><span class="span-here">here</span></a> </span>
                        </div>
                        <div class="col-md-4 hidden-sm hidden-xs">
                            <img class="load-back" data-img='../assets/layouts/layout4/img/keyword-icon.png'/>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>  
<div class="modal be-modal fade" id="mdLearmore-3" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content set-width-content center-modal"> 
                      
            <div class="modal-body mdLearmore">
                <button type="button" data-dismiss="modal" aria-label="Close" class="pull-right btn-close-popup-new"><span aria-hidden="true">×</span></button> 
                <div class="mdLearmoreBody">          
                    <div class="row">
                        <div class="col-md-8 col-sm-12 col-xs-12">   

                            <h4 class="learmore_title">Send a Blast:</h4>
                            <ul>                                                              

                                <li>Promotions & coupons</li>
                                <li>Updates & reminders</li>
                                <li>Flash Sales</li>
                                <li>Emergency alerts</li>

                            </ul>
                            <span class="span-learmore">Learn more  <a href="https://www.siremobile.com/blog/support/how-to-send-a-blast/" target="_blank"><span class="span-here">here</span></a> </span>
                        </div>
                        <div class="col-md-4 hidden-sm hidden-xs">
                            <img  class="load-back" data-img='../assets/layouts/layout4/img/blast-icon.png'/>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>  
<div class="modal be-modal fade" id="mdLearmore-11" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content set-width-content center-modal">                        
            <div class="modal-body mdLearmore">
                <button type="button" data-dismiss="modal" aria-label="Close" class="pull-right btn-close-popup-new"><span aria-hidden="true">×</span></button>
                <div class="mdLearmoreBody">                    
                    <div class="row">
                        <div class="col-md-8 col-sm-12 col-xs-12">   
                            <h4 class="learmore_title">Customer Survey:</h4>
                            <ul>                                
                                <li>Improve quality of products</li>
                                <li>Learn what customers are thinking</li>
                                <li>Enhance customer experience</li>
                                <li>Collect group feedback & consensus</li>                            
                            </ul>
                            <span class="span-learmore">Learn more  <a href="https://www.siremobile.com/blog/support/how-to-create-a-survey/" target="_blank"><span class="span-here">here</span></a> </span>
                        </div>
                        <div class="col-md-4 hidden-sm hidden-xs">
                            <img  class="load-back" data-img='../assets/layouts/layout4/img/survey-icon.png'/>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>  
<div class="modal be-modal fade" id="mdLearmore-9" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content set-width-content center-modal">                        
            <div class="modal-body mdLearmore">
                <button type="button" data-dismiss="modal" aria-label="Close" class="pull-right btn-close-popup-new"><span aria-hidden="true">×</span></button>
                <div class="mdLearmoreBody">                    
                    <div class="row">
                        <div class="col-md-8 col-sm-12 col-xs-12">   
                            <h4 class="learmore_title">Chat with customers:</h4>
                            <ul>                                                            
                                <li>2-way live SMS chat with customers</li>
                                <li>Deter negative Yelp reviews</li>
                                <li>No need to use your personal number</li>
                                <li>Make customers feel like you’re listening</li>
                               
                            </ul>
                            <span class="span-learmore">Learn more  <a href=" https://www.siremobile.com/blog/support/create-2-way-live-chat-customers/" target="_blank"><span class="span-here">here</span></a> </span>
                        </div>
                        <div class="col-md-4 hidden-sm hidden-xs">
                            <img  class="load-back" data-img='../assets/layouts/layout4/img/chat-icon.png'/>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>  
<div class="modal be-modal fade" id="mdLearmore-8" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content set-width-content center-modal">                        
            <div class="modal-body mdLearmore">
                <button type="button" data-dismiss="modal" aria-label="Close" class="pull-right btn-close-popup-new"><span aria-hidden="true">×</span></button>
                <div class="mdLearmoreBody">                    
                    <div class="row">
                        <div class="col-md-8 col-sm-12 col-xs-12">   
                            <h4 class="learmore_title">Time Machine:</h4>
                            <ul>                                
                                <li>Create time-saving automated drips</li>
                                <li>Pre-schedule a series of messages</li>
                                <li>Great for weekly reminders</li>
                                <li>Perfect for lesson plans</li>

                                
                            </ul>
                            <span class="span-learmore">Learn more  <a href="https://www.siremobile.com/blog/support/create-drip-campaign-time-machine-template/" target="_blank"><span class="span-here">here</span></a> </span>
                        </div>
                        <div class="col-md-4 hidden-sm hidden-xs">
                            <img  class="load-back" data-img='../assets/layouts/layout4/img/time-icon.png'/>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>  