<cfset _isNotCustomPlan = ListContains(ValueList(plans.DATA.id), RetUserPlan.PLANID) />

<cfoutput>

<div id="wrap-pricing" class="wrap-pricing">
    
    <div class="uk-grid uk-grid-small uk-grid-match" uk-grid uk-height-match="target: div > div > .head">         
        <cfloop query="#plans.data#">        
        <cfif Listfind("2,3",userInfo.USERTYPE ) GT 0 OR (userInfo.USERTYPE EQ 1 AND  plans.data.Status_int EQ 1)>
            <div class="uk-width-1-4@l uk-width-1-2@s">
                <div class="pricing-item">
                    <div class="head uk-flex uk-flex-middle uk-flex-center">
                        <div class="uk-text-center">
                            <cfif Status_int EQ "4">                                
                                <cfoutput><h4>#name#</h4></cfoutput>
                                <span></span>
                            <cfelseif price eq 0>
                                <cfoutput><h4>#name#</h4></cfoutput>
                                <span>(No Credit Card Required)</span>
                            <cfelse>
                                 <cfoutput><h4>#name#</h4></cfoutput>
                            </cfif>
                        </div>
                            
                    </div>
                    <div class="sub-head uk-flex uk-flex-middle uk-flex-center">
                        
                        <cfif Status_int EQ "4">                                                            
                            <h4 class="uk-text-truncate">Pay As You Go</h4>
                        <cfelse>
                            <cfif RetUserPlan.BILLINGTYPE EQ 1> 
                                <h4 class="uk-text-truncate"><span>$#price#</span> /month</h4>
                            <cfelse>
                                <h4 class="uk-text-truncate"><span>$#yearlyAmount#</span> /month</h4>    
                            </cfif>    
                        </cfif>                        
                        

                    </div>
                    <div class="body uk-flex uk-flex-center">
                        <ul class="uk-list">
                            <li>First #NumberFormat(firstSMSIncluded_int,',')# SMS Included</li>
                            <li>#keywordsLimitNumber_int# User Reserved <a data-toggle="modal" title="What is a Keyword?" href="##" data-target="##InfoModalKeyword">Keywords</a></li>
                            <li>#NumberFormat(PriceMsgAfter_dec,'._')# Cents per Msg after</li>
                            <li>#(MlpsLimitNumber_int EQ 0 ? "Unlimited" : NumberFormat(MlpsLimitNumber_int,','))# <a data-toggle="modal" title="What is a MLP?"  href="##" data-target="##InfoModalMLP"> MLPs</a></li>
                            <li>#(ShortUrlsLimitNumber_int EQ 0 ? "Unlimited" : NumberFormat(ShortUrlsLimitNumber_int,','))# Short URLs</li>
                        </ul>
                    </div>

                    <cfif _isNotCustomPlan GT 0> 

                        <cfif RetUserPlan.PLANID EQ id>
                            <div class="foot uk-flex uk-flex-middle uk-flex-center">
                                <a href="##" class="btn btn-yourplan">Your plan</a>
                            </div>
                        <cfelseif plans.data.Status_int EQ 4>                        
                            <div class="foot uk-flex uk-flex-middle uk-flex-center">
                                <cfif (userInfo.USERTYPE EQ 2 AND userInfo.USERLEVEL EQ 2) >                                
                                    <a href="##" class="btn btn-upgrade" data-plan-id="#id#">Select</a>                                     
                                </cfif>
                            </div>
                        <cfelseif RetUserPlan.PLANORDER LT level>
                            <div class="foot uk-flex uk-flex-middle uk-flex-center">
                                <cfif RetUserPlan.PLANEXPIRED NEQ 1>
                                    <a href="##" class="btn btn-upgrade" data-plan-id="#id#">Upgrade</a> 
                                </cfif>
                            </div>
                        <cfelseif plans.data.price GT RetUserPlan.AMOUNT>
                            <div class="foot uk-flex uk-flex-middle uk-flex-center">
                                <cfif RetUserPlan.PLANEXPIRED NEQ 1>
                                    <a href="##" class="btn btn-upgrade" data-plan-id="#id#">Upgrade</a> 
                                </cfif>
                            </div>
                        <cfelseif (RetUserPlan.PLANID GT id AND id EQ 3) OR (RetUserPlan.PLANID LT id AND RetUserPlan.PLANID LT 3)>
                            <div class="foot uk-flex uk-flex-middle uk-flex-center">
                                <cfif RetUserPlan.PLANEXPIRED NEQ 1>
                                    <a href="##" class="btn btn-upgrade" data-plan-id="#id#">Upgrade</a> 
                                </cfif>
                            </div>
                        <cfelse>
                            <div class="foot uk-flex uk-flex-middle uk-flex-center">
                                                        
                            </div>
                        </cfif>

                    <cfelse>
                        <div class="foot uk-flex uk-flex-middle uk-flex-center">

                        </div>
                    </cfif>
  
                </div>
            </div>
        </cfif>
        </cfloop>
         <a href="##" class="btn btn-downgrade hidden" >Downgrade</a>  

    </div>
</div>
</cfoutput>