<cfparam  name="defaultCouponCode" default="">
<cfif userInfo.APPLUAFFILIATECODE NEQ "">
    <!--- check linkage coupon code with affiliate code--->
    <cfinvoke method="GetCouponFromAffiliateCode" component="public/sire/models/cfc/affiliate" returnvariable="rtGetCouponFromAffiliateCode">
        <cfinvokeargument name="inpAffiliateCode" value="#userInfo.APPLUAFFILIATECODE#"/>
    </cfinvoke>
    <cfif rtGetCouponFromAffiliateCode.RXRESULTCODE EQ 1>                           
        <cfset defaultCouponCode =rtGetCouponFromAffiliateCode.COUPONCODE >
    </cfif>
</cfif>
<cfoutput>

<div id="buyaddon-modal-sections" class="uk-new-modal uk-modal modal-cardholder" uk-modal>
    <div class="uk-modal-dialog addon-modal-dialog">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <form id="addon-form">
            <input type="hidden" name="planStatus" id="planStatus" value="0">
            <input type="hidden" name="upgradePlanAmount" id="upgradePlanAmount" value="0">
            <input type="hidden" name="payment_method_value" id="payment_method_value" value="">
            <input type="hidden" name="listKeyword" id="listKeyword" value="">
            <input type="hidden" name="listMLP" id="listMLP" value="">
            <input type="hidden" name="listShortUrl" id="listShortUrl" value="">
            <input type="hidden" name="currentPlan" id="currentPlan" value="#RetUserPlan.PLANID#">
            <input type="hidden" name="planId" id="planId" value="0">
            <input type="hidden" name="monthYearSwitch" id="monthYearSwitch" value="#RetUserPlan.BILLINGTYPE#">
            <div class="uk-modal-body form-gd">
                <!--- UPGRADE PLAN BOX --->
                <div class="inner-update-cardholder" id="section-2" style="margin-bottom: 50px;">
                    <div class="uk-grid uk-grid-medium" uk-grid>
                        <div class="uk-width-1-1">
                            <div class="uk-grid" uk-grid>
                                <div class="uk-width-1-1@m">
                                    <div class="uk-grid uk-grid-small uk-flex-middle" uk-grid>
                                        <div class="uk-width-1-3@m">
                                            <div class="form-group">
                                                <span class="uk-text-bold">Select Your Plan:</span>
                                            </div>
                                        </div>
                                        <div class="uk-width-1-2@m">
                                            <select name="select-plan-2" id="select-plan-2" class="form-control">
                                                <cfoutput>
                                                    <cfloop query="#plans.DATA#">
                                                        <option id="plan-#id#" value="#id#">#name#</option>
                                                    </cfloop>
                                                </cfoutput>
                                            </select>
                                        </div>
                                    </div>


                                    <!--- <div class="uk-grid uk-grid-small uk-flex-middle">
                                        <section class="monthly-yearly-update">
                                            <div class="text-center month-year-plan-switch">
                                                <div class="inner">
                                                    <span class="month-year-plan-text" id="monthly-plan-text" style="color: rgb(86, 140, 165);">MONTHLY</span>
                                                    <label class="switch">
                                                        <input id="monthYearSwitch" name="monthYearSwitch" type="checkbox" value="#RetUserPlan.BILLINGTYPE#">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    <span class="month-year-plan-text" id="yearly-plan-text" style="color: rgb(146, 146, 146);">YEARLY</span>
                                                    <span class="triangle-isosceles left" style="background: rgb(146, 146, 146);">Save up to 20%</span>
                                                </div>
                                            </div>
                                        </section>
                                    </div> --->

                                    <!--- <cfif RetUserPlan.BILLINGTYPE NEQ 2> --->
                                        <div class="form-group monthly-yearly-update <cfif RetUserPlan.BILLINGTYPE EQ 2>hidden</cfif>">
                                            <label for="">&nbsp</label>
                                            <div class="text-center month-year-plan-switch">
                                                <div class="inner">
                                                    <span class="month-year-plan-text" id="monthly-plan-text">MONTHLY</span>
                                                    <label class="switch">
                                                        <input name="month-Year-Switch" id="month-Year-Switch" type="checkbox" value="#RetUserPlan.BILLINGTYPE#">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    <span class="month-year-plan-text" id="yearly-plan-text">YEARLY</span>
                                                    <span class="triangle-isosceles left">Save up to 20%</span>
                                                </div>
                                            </div>
                                        </div>
                                    <!--- </cfif> --->

                                    <!--- <cfif RetUserPlan.BILLINGTYPE EQ 1> --->
                                        <div class="uk-grid uk-grid-small uk-flex-middle monthly_price <cfif RetUserPlan.BILLINGTYPE EQ 2>hidden</cfif>">
                                            <div class="uk-width-1-3@m">
                                                <div class="form-group">
                                                    <span class="uk-text-bold">Monthly Amount:</span>
                                                </div>
                                            </div>
                                            <div class="uk-width-1-2@m">
                                                <cfoutput>
                                                    <cfloop query="#plans.DATA#">
                                                        <span id="span-plan-price-#id#" class="span-plan-price">$#price#</span>
                                                    </cfloop>
                                                </cfoutput>
                                            </div>
                                        </div>
                                    <!--- <cfelse> --->
                                        <div class="uk-grid uk-grid-small uk-flex-middle yearly_price <cfif RetUserPlan.BILLINGTYPE EQ 1>hidden</cfif>">
                                            <div class="uk-width-1-3@m">
                                                <div class="form-group">
                                                    <span class="uk-text-bold">Yearly Amount:</span>
                                                </div>
                                            </div>
                                            <div class="uk-width-1-2@m">
                                                <cfoutput>
                                                    <cfloop query="#plans.DATA#">
                                                        <span id="span-plan-price-yearly-#id#" class="span-plan-price">$#yearlyAmount*12#</span>
                                                    </cfloop>
                                                </cfoutput>
                                            </div>
                                        </div>    
                                    <!--- </cfif> --->

                                    

                                     <div class="uk-grid uk-grid-small uk-flex-middle coupon-code" uk-grid>
                                        <div class="uk-width-1-3@m">
                                            <div class="form-group">
                                                <span class="uk-text-bold">Coupon Code: </span>
                                            </div>
                                        </div>
                                        <div class="uk-width-1-2@m">
                                            <input type="text" id="inpPromotionCode" name="inpPromotionCode" class="form-control" value="#defaultCouponCode#">
                                            <span class="help-block help-block-coupon-signup-input" style="display: none;font-weight: bold;" id="help-block"></span>
                                        </div>

                                        <div class="uk-width-expand">
                                            <a class="btn green-gd" id="apply-btn">apply</a>
                                            <a class="btn green-cancel" id="remove-btn" style="display: none">remove</a>
                                        </div>

                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--- END UPGRADE PLAN BOX --->
                <div class="form-group hidden">
                    <cfif RetGetPaymentMethodSetting.RESULT EQ 1>
                        <div class="radio-inline" id="rad-Crecard">
                            <label>
                                <input type="radio" name="select_payment_method" class="select_payment_method" id="optionsRadiosMethod1" value="1" checked>
                                Credit/Debit Card
                            </label>
                        </div>
                    <cfelseif RetGetPaymentMethodSetting.RESULT EQ 2>
                        <div class="radio-inline" id="rad-Paycard">
                            <label>
                                <input type="radio" name="select_payment_method" class="select_payment_method" id="optionsRadiosMethod2" value="2" checked>
                                <img src="/public/sire/images/paypal_icon.png" class="paypal_icon_large">
                            </label>
                        </div>
                    <cfelseif RetGetPaymentMethodSetting.RESULT EQ 3>
                        <div class="radio-inline" id="rad-Mojocard">
                            <label>
                                <input type="radio" name="select_payment_method" class="select_payment_method" id="optionsRadiosMethod1" value="3" checked>
                                Credit/Debit Card MoJo
                            </label>
                        </div>
                    </cfif>
                </div>
                <!---
                <div class="form-group hidden">
                    <div class="card-support-type">
                        <img class="img-responsive" src="../assets/layouts/layout4/img/supported-payment-types.png" alt="">
                    </div>
                </div>
                --->
                <div class="inner-update-cardholder">
                    <!--- <h4 class="uk-new-modal-title">Buy Credits - Keywords</h4> --->
                        <cfoutput>
                            <div class="update_cardholder_info">
                                <cfif hidden_card_form NEQ ''>
                                    <ul class="nav list-newinfo">
                                        <li>Card Number: #maskedNumber#</li>
                                        <li>Expiration Date: #expirationDate#</li>
                                        <li>Cardholder Name: #firstName# #lastName#</li>
                                        <li>Address: #line1#</li>
                                        <li>City: #city#</li>
                                        <li>State: #state#</li>
                                        <li>Zip code: #zip#</li>
                                        <li>Country: #country#</li>
                                        <li>Email: #emailAddress#</li>
                                    </ul>

                                    <!--- <div class="form-group">
                                        <button type="button" id="remove-payment-btn" title="Remove Card Infomation" class="btn newbtn green-cancel">Remove</button>
                                    </div> --->

                                    <div class="form-group">
                                        <div class="radio-inline" id="rad-currentcard">
                                            <label>
                                                <input type="radio" name="select_used_card" class="select_used_card" id="optionsRadios1" value="1" checked>
                                                    Current Card
                                            </label>
                                        </div>
                                        <div class="radio-inline" id="rad-newcard" style="padding-left: 22px !important;">
                                            <label>
                                                <input type="radio" name="select_used_card" class="select_used_card" id="optionsRadios2" value="2">
                                                    New Card
                                            </label>
                                        </div>
                                    </div>
                                </cfif>

                                <input type="hidden" id="amount-sms-to-send" name="" value="0">
                                <input type="hidden" id="amount-keyword-to-send" name="" value="0">
                                <input type="hidden" id="amount" name="amount" value="0">
                                <input type="hidden" id="numberSMSToSend" name="numberSMSToSend" value="0">
                                <input type="hidden" id="numberKeywordToSend" name="numberKeywordToSend" value="0">
                                <input type="hidden" id="pricemsgafter" value="#RetUserPlan.PRICEMSGAFTER#">
                                <input type="hidden" id="actionStatus" value="">
                                <input type="hidden" value="#userInfo.FULLEMAIL#" id="h_email" name="h_email">
                                <input type="hidden" name="h_email_save" value="#emailAddress#">

                                <div class="fset_cardholder_info" style="<cfif hidden_card_form NEQ ''>display: none</cfif>">
                                <input type="radio" class="hidden" name="payment_method" class="check_payment_method" id="payment_method_0" value="0" checked/>
                                <div class="form-group">
                                    <div class="card-support-type">
                                        <img class="img-responsive" src="../assets/layouts/layout4/img/supported-payment-types.png" alt="">
                                    </div>
                                </div>

                                <div class="row row-small">
                                    <div class="col-lg-8 col-md-6">
                                        <div class="form-group">
                                            <label>Card Number <span>*</span></label>
                                            <input type="text" class="form-control validate[required,custom[creditCardFunc]]" maxlength="32" id="card_number" name="number" value='' placeholder="---- ---- ---- 1234">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label>Security Code <span>*</span></label>
                                            <input type="text" class="form-control validate[required,custom[onlyNumber]] , minSize[3]" maxlength="4" id="security_code" name="cvv" data-errormessage-custom-error="* Invalid security code" data-errormessage-range-underflow="* Security code should be 3 or 4 characters" >
                                        </div>
                                    </div>
                                </div>

                                <div class="row row-small">
                                    <div class="col-lg-8 col-md-6">
                                        <div class="form-group">
                                            <label>Expiration Date <span>*</span></label>
                                            <input type="hidden" id="expiration_date_upgrade" name="expirationDate" value="">
                                            <select class="form-control validate[required]" id="expiration_date_month_upgrade" name="expiration_date_month_upgrade" class="expiration_date_month">
                                                <option value="" disabled selected>Month</option>
                                                <option value="01">January</option>
                                                <option value="02">February</option>
                                                <option value="03">March</option>
                                                <option value="04">April</option>
                                                <option value="05">May</option>
                                                <option value="06">June</option>
                                                <option value="07">July</option>
                                                <option value="08">August</option>
                                                <option value="09">September</option>
                                                <option value="10">October</option>
                                                <option value="11">November</option>
                                                <option value="12">December</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <select class="form-control validate[required]" id="expiration_date_year_upgrade" name="expiration_date_year_upgrade" class="expiration_date_year">
                                                <option value="" disabled selected>Year</option>
                                                <cfloop from="#year(now())#" to="#year(now())+50#" index="i">
                                                    <option value="#i#">#i#</option>
                                                </cfloop>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row row-small">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>Cardholder Name <span>*</span></label>
                                            <input type="text" class="form-control validate[required,custom[onlyLetterNumberSp]]" maxlength="50" id="first_name" name="firstName" placeholder="First Name" value="">
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <input type="text" class="form-control validate[required,custom[onlyLetterNumberSp]]" maxlength="50" id="last_name" name="lastName" placeholder="Last Name" value="">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Address:<span>*</span></label>
                                    <input type="text" class="form-control validate[required,custom[noHTML], custom[validateAddrAndCity]] cardholder" maxlength="60" id="line1" name="line1" data-value='#line1#'>
                                </div>

                                <div class="form-group">
                                    <label>City:<span>*</span></label>
                                    <input type="text" class="form-control validate[required,custom[noHTML], custom[validateAddrAndCity]] cardholder" maxlength="40" id="city" name="city" data-value='#city#'>
                                </div>


                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>State:<span>*</span></label>
                                            <input type="text" class="form-control validate[required,custom[noHTML], custom[onlyLetterNumberSp]] cardholder" <cfif RetGetPaymentMethodSetting.RESULT EQ 3>maxlength="2"<cfelse>maxlength="40"</cfif> id="state" name="state" data-value="#state#">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Zip Code:<span>*</span></label>
                                            <input type="text" class="form-control validate[required,custom[onlyNumber]] cardholder" maxlength="5" id="zip" name="zip" data-value="#zip#" data-errormessage-custom-error="* Invalid zip code">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Country:<span>*</span></label>
                                    <select class="form-control cardholder validate[required]" id="country" name="country" data-value="#country#" >
                                        <cfinclude template="../views/commons/payment/country.cfm">
                                    </select>
                                </div>

                                <input type="hidden" class="form-control validate[custom[phone]] cardholder" maxlength="30" id="phone" name="phone" data-value="#phone#">

                                <div class="form-group">
                                    <label>Email:</label>
                                    <input type="text" class="form-control validate[custom[email]] cardholder" maxlength="128" id="email" name="email" data-value="#emailAddress#">
                                </div>
                                <div class="form-group hidden">
                                    <label>
                                        <input type="checkbox" id="save_cc_info" name="save_cc_info" value="1" checked> 
                                        Save card information
                                    </label>
                                </div>
                                <hr>
                            </div>
                        </cfoutput>
                    
                    </div>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right buyaddon-footer-modal">
                <!--- <button class="btn newbtn green-cancel uk-modal-close">Cancel</button> --->
               
                <button type="submit" class="btn newbtn green-gd btn-purchase-cc">Purchase</button>
                <button type="button" class="btn newbtn1 green-cancel uk-modal-close btn-cancel-cc">Cancel</button>
            </div>
        </form>
    </div>
</div>

<div id="downgrade-modal-sections" class="uk-new-modal uk-modal modal-cardholder" uk-modal>
    <div class="uk-modal-dialog addon-modal-dialog">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <form id="downgrade-form">
            <input type="hidden" name="downgradePlanId" id="downgradePlanId" value="0">
            <div class="uk-modal-body form-gd">
                <h4 class="uk-new-modal-title">Looking for Downgrade?</h4>
                <p>
                    You have request to downgrade your account from #RetPlan.PLANNAME# to #downgradePlanName#. If this is not your request, or you change your mind, click <a href="javascript:void(0)" title="I Changed My Mind" class="btn-downgrade-cancel">here</a>
                    or button below to cancel your request
                </p>
                <p>If you still want to proceed, downgrading will decrease the number of included keywords, short URLs or MLPs that are included in your account.</p>
            </div>
            <div class="uk-modal-footer uk-text-right buyaddon-footer-modal">
                <button type="button" class="btn newbtn1 green-cancel uk-modal-close">Cancel</button>
                <button type="button" class="btn newbtn1 green-cancel btn-downgrade-cancel">I Change My Mind</button>
                <button type="button" class="btn newbtn green-gd btn-downgrade-next">Next</button>
            </div> 
        </form>
    </div>
</div>      

<div id="downgrade-list-keyword-modal-sections" class="uk-new-modal uk-modal modal-cardholder uk-modal-container" uk-modal>
    <div class="uk-modal-dialog addon-modal-dialog">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <form id="downgrade-form">
            <div class="uk-modal-body form-gd">
                <div class="downgradeNote">
                
                </div>
                <div class="re-table">
                    <div class="table-responsive">
                        <table id="keywordListDetail" class="table table-striped table-bordered table-hover">
                        </table>
                    </div>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right buyaddon-footer-modal">
                <button type="button" class="btn newbtn green-gd btn-downgrade-next-1" >Next</button>
            </div> 
        </form>
    </div>
</div>         

<div id="downgrade-list-confirm-modal-sections" class="uk-new-modal uk-modal modal-cardholder" uk-modal>
    <div class="uk-modal-dialog addon-modal-dialog">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <form id="downgrade-form">
            <div class="uk-modal-body form-gd">
                <p>You have selected the following <span class="item_type"></span/> to be <span class="item_action">removed</span> from your account</p> 
                <div class="list_item">
                </div>    
            </div>
            <div class="uk-modal-footer uk-text-right buyaddon-footer-modal">
                <button type="button" class="btn newbtn1 green-cancel uk-modal-close btn-change-mind-upgrade">I've change my mind</button>
                <button type="button" class="btn newbtn green-gd btn-remove-item-downgrade btn-confirm-mind-upgrade">Confirm</button>
            </div> 
        </form>
    </div>
</div>          

<div id="downgrade-success-modal-sections" class="uk-new-modal uk-modal modal-cardholder" uk-modal>
    <div class="uk-modal-dialog addon-modal-dialog">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <form id="downgrade-form">
            <div class="uk-modal-body form-gd">
                <h4 class="uk-new-modal-title">That was easy!</h4>
                <p> You have downgraded from #RetPlan.PLANNAME# to #downgradePlanName#</p>
                <p>This change will take effect on your next bill date #Dateformat(RetUserPlan.ENDDATE,"yyyy-mm-dd")#. Feel free to reach out to discuss ways for your bussiness to maximize SMS marketing at <a href="mailto:hello@siremobile.com"/> hello@siremobile.com</a></p>
            </div>
            <div class="uk-modal-footer uk-text-right buyaddon-footer-modal">
                <button type="button" class="btn newbtn green-gd uk-modal-close">Close</button>
            </div> 
        </form>
    </div>
</div>
</cfoutput>