<cfoutput>	
	<div class="uk-grid" uk-grid>
        <div class="uk-width-2-3@xl uk-width-2-2@l">
        	<form name="white_glove_form" id="white_glove_form" autocomplete="off">	
	            <div class="form-gd">
	                <div class="clearfix">
	                    <h3 class="form-heading">Save Time. Trust the Experts.</h3>
	                </div>
	                <div class="white-glove-intro">
	                	<p>Sire's "White Glove" service is designed for the user who is looking to save time and prefers to have an expert manage all aspects of the campaign.  This premium feature includes a dedicated representative who will provide all of the following.</p>
	                </div>
	                <div class="white-glove-body">                	
						<h4>Marketing Consultation:</h4>
						<ul class="list-white-glove-square">
							<li>Discuss your marketing objective</li>
							<li>Define your campaign goals</li>
							<li>Outline your blueprint for success</li>						
						</ul>
						<h4>Campaign Management:</h4>	
						<ul class="list-white-glove-square">
							<li>Build up to 3 Campaigns per month</li>
							<li>Create customized templates</li>
							<li>Copy & creative collaboration</li>
							<li>Monthly progress review call</li>
							<li>Monthly analytics reporting</li>
							<li>Priority Customer Support</li>						
						</ul>
						<h4>Monthly Add-on: $49</h4>	
	                </div>
	                <cfif userInfo.WhiteGloveStatusID EQ 1>		                	                	                 
                    <cfelse>   
                        <div class="white-glove-agree">
		                	<input name="agree" class="validate[required]" id="ckagree-white-glove" type="checkbox"> 						
							I’ve read and agree to the <a href="/glove-terms-and-conditions" target="_Blank" title="Terms and Conditions of Use">Terms and Conditions of Use.</a>			
		                </div>
                    </cfif>	                		               
	                <p></p>
	                <div>
	                	<cfif userInfo.WhiteGloveStatusID EQ 1>	                	                  
                        <cfelse>   
                            <button class="btn newbtn green-gd" id="show-white-glove-modal">Sign Me Up</button>
                        </cfif>	                	
	                </div>
	            </div>
        	</form>
        </div>
	</div>						
</cfoutput>