


<cfoutput>
	<cfscript>
	    CreateObject("component","public.sire.models.helpers.layout")    	 
	    .addJs("../assets/pages/scripts/ticket-watcher.js");  	    	    
	</cfscript>
</cfoutput>
 <div class="modal fade" id="list-watcher" tabindex="-1" role="dialog" aria-hidden="true">
	<form name="form-list-watcher" class="col-sm-12" id="form-list-watcher">
	    <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <b><h4 class="modal-title" id="modal-tit">
                        List watchers
                    </h4></b>			                    
                </div>
                <div class="modal-body">	               
	               <div class="modal-body-inner"> 
                        <div class="portlet light bordered">
                            <div class="portlet-body">
                                <div class="new-inner-body-portlet2">
                                    <div class="form-gd">
                                        <div class="row">                                            
                                            <div class="col-xs-8">
                                                <div class="inner-addon left-addon">
                                                    <i class="glyphicon glyphicon-search"></i>
                                                    <input type="search" class="form-control" id="searchbox" placeholder="Search Watcher">
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="pull-right">
                                                    <a href="#" class="btn btn-block green-gd pop-ticket-watcher" data-id="0" data-toggle="modal" data-action="new" data-target="#edit-ticket-watcher">Create New Watcher</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="" style="margin-top: 10px">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="re-table">
                                                        <div class="table-responsive">
                                                            <table id="tblWatcherList" class="table table-striped table-responsive">
                                                
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>			
                                    </div>
                                </div>
                            </div>
                        </div>

                   </div>
                </div>
            </div>
        </div>
    </form>
</div>


<!--- modal--->
 <!--- edit watcher--->
 <div class="modal fade" id="edit-ticket-watcher" tabindex="-1" role="dialog" aria-hidden="true">
	<form name="form-edit-ticket-watcher" class="col-sm-12" id="form-edit-ticket-watcher">
	    <div class="modal-dialog">
	        <div class="modal-content">
	        	<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<b><h4 class="modal-title" id="modal-tit">
						NEW WATCHER
					</h4></b>			
					<input type="hidden" name="watcherId" id="watcherId" value="0" autocomplete="off"/>		
				</div>
	            <div class="modal-body">	               
	               <div class="modal-body-inner">  
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group"> 
                                    <label for="">Type</label>
                                    <select class="form-control cardholder validate[required]" id="watcher-type" name="watcher-type">
                                        <option value="<cfoutput>#_TICKET_WATCHER_TYPE_EMAIL#</cfoutput>">Email</option>
                                        <option value="<cfoutput>#_TICKET_WATCHER_TYPE_PHONE#</cfoutput>">Phone</option>                                         
                                    </select>
                                </div>   
                            </div>
                            <div class="col-sm-9">     
	                            <div class="form-group">           
                                    <label for="">Email/Phone</label>                 
                                    <input type="text" class="form-control validate[required, custom[noHTML], maxSize[250]]" maxlength="250" id="watcher-name" name="watcher-name" placeholder="" data-prompt-position="topLeft:10" autocomplete="off">	                       
                                </div>
                            </div>
	                    </div>	                    
	             	</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn green-cancel" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn green-gd"  id="btnUpdateticket-type">Save</button>
				</div>
			</div>
		</div>
	</form>
 </div>

