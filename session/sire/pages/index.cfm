<cflocation url="/session/sire/pages/campaign-manage" addtoken="false">
<main class="container-fluid page">
	<div class="credits_available">Credits Available: 1,000</div>
	<section class="row bg-white">
		<div class="content-header">
			<div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-6 content-title">
                        	<p class="title">Welcome John!</p>
                        	<p class="sub-title">Your Short Code is <span class="active">55555</span></p>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6 content-menu">
							    
							    <a class="active" href="#"><span class="icon-home"></span><span>Home</span></a>
							    <a href="#"><span class="icon-message"></span><span>Message</span></a>
							    <a href="#"><span class="icon-subscribers"></span><span>Subscribers</span></a>
							    <a href="#"><span class="icon-optin"></span><span>Opt-In</span></a>

                        </div>
            </div>
		</div>
		<hr>
		<div class="content-body">
				<div class="col-lg-8">
					<div class="row heading">
						<div class="col-sm-6 heading-title" >Campaigns</div>
						<div class="col-sm-6 heading-button"><button class="btn btn-larger btn-success-custom">Create campaign</button></div>
					</div>
					<div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Campaign Name</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>11111</td>
                                            <td>BOND Campaign</td>
                                            <td>
                                            	<button class="btn btn-medium btn-success-custom">Manage</button>
                                            	<button class="btn btn-medium btn-back-custom">Delete</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>22222</td>
                                            <td>BOND Campaign</td>
                                            <td>
                                            	<button class="btn btn-medium btn-success-custom">Manage</button>
                                            	<button class="btn btn-medium btn-back-custom">Delete</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>33333</td>
                                            <td>BOND Campaign</td>
                                            <td>
                                            	<button class="btn btn-medium btn-success-custom">Manage</button>
                                            	<button class="btn btn-medium btn-primary-custom">Delete</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
			
				</div>
				<div class="col-md-4">
					<div class="row heading">
					</div>
					<div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Active Feed</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><span class="icon-newsfeed-plus"></span></td>
                                            <td>(111) 000-0001 joined BOND Campaign <p class="time">October 19, 2015 09:15 PM PST</p></td>
                                        </tr>
                                        <tr>
                                            <td><span class="icon-newsfeed-minus"></span></td>
                                            <td>(111) 000-0002 left your Campaign <p class="time">October 18, 2015 09:15 PM PST</p></td>
                                        </tr>
                                        <tr>
                                            <td><span class="icon-newsfeed-message"></td>
                                            <td>Message of BOND campaign sent to  10 subscribers <p class="time"> October 17, 2015 09:15 PM PST</p></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
				</div>
			</div>
	</section>

</main>

<cfinclude template="../views/layouts/main.cfm">