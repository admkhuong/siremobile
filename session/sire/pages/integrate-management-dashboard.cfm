<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission">    
    <cfinvokeargument  name="inpSkipRedirect"  value="1">
</cfinvoke>
<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke> 
<cfset haveRight=0>
<cfif RetVarGetAdminPermission.RXRESULTCODE EQ 1>
    <cfset haveRight=1>
<cfelseif (userInfo.USERTYPE EQ 2 AND userInfo.USERLEVEL LT 3)>
    <cfset haveRight=1>
</cfif>
<cfif haveRight EQ 0 >
    <cflocation url="/session/sire/pages/admin-no-permission" addToken="no" />
</cfif>
<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")  
        .addCss("/session/sire/assets/global/plugins/Chartist/chartist.min.css", true)
        .addCss("../assets/global/plugins/daterangepicker/daterangepicker.css", true)
        .addJs("../assets/global/plugins/highchart/highcharts.js", true)
        .addJs("../assets/global/plugins/highchart/exporting.js", true)
        .addJs("../assets/global/plugins/mobile-detect/mobile-detect.js", true)
        .addJs("../assets/pages/scripts/jquery.textfill.min.js")
        .addJs("/session/sire/assets/global/plugins/Chartist/chartist.min.js", true)
        .addJs("/public/sire/js/js.cookie.js")
        .addJs("/session/sire/assets/global/scripts/filter-bar-generator.js")        
        .addJs("../assets/global/plugins/daterangepicker/moment.min.js", true)
        .addJs("../assets/global/plugins/daterangepicker/daterangepicker.js", true)
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)
        .addJs("/public/sire/js/select2.min.js")       
        .addJs("../assets/pages/scripts/dashboard_management.js");
</cfscript>

<div class="row dashboard">
    <div class="col-sm-3" >
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="admin-dashboard-block-title">Total Users:</div>
                        <div class="" style="padding-left: 0px">
                            <div class="admin-dashboard-value">
                                <div class="" id="total-user-div"><span id="total-user">0</span></div>
                            </div>
                        </div>                        
                    </div>   
                             
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3" >
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">                
                <div class="admin-dashboard-block-title">Select User Level</div>
                </br>
                <div>
                    <select name="select-level" class="form-control select2" id="select-level">
                        <cfif RetVarGetAdminPermission.RXRESULTCODE EQ 1>
                            <option value="0">All Level</option> 
                            <option value="1">Level 1</option> 
                            <option value="2">Level 2</option> 
                            <option value="3">Level 3</option> 
                        <cfelseif userInfo.USERTYPE EQ 2>
                            <cfif userInfo.USERLEVEL EQ 1>
                                <option value="0">All Level</option> 
                                <option value="1">Level 1</option> 
                                <option value="2">Level 2</option> 
                                <option value="3">Level 3</option> 
                            <cfelseif userInfo.USERLEVEL EQ 2>
                                <option value="2">Level 2</option> 
                                <option value="3">Level 3</option> 
                            </cfif>
                        </cfif>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6" >
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">                
                <div class="admin-dashboard-block-title">Selected Date Range</div>
                </br>
                <div class="date-range">
                    <div class="reportrange">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                        <span class="time"></span> <span class="pull-right" style="margin-right: 0"><b class="caret"></b></span>
                        <input type="hidden" name="dateStart" value="">
                        <input type="hidden" name="dateEnd" value="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row dashboard">
    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">
                <div class="admin-dashboard-block-title">Total Message Sent</div>
                <div class="admin-dashboard-value">
                    <div class="crm-message-sent" id="crm-msg-sent-numb"><span  style="font-size: 64px;">0</span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">
                <div class="admin-dashboard-block-title">Total Message Received</div>
                <div class="admin-dashboard-value">
                    <div class="crm-message-sent" id="crm-msg-received-numb"><span  style="font-size: 64px;">0</span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">
                <div class="admin-dashboard-block-title">Total Opt-In</div>
                <div class="admin-dashboard-value">
                    <div class="crm-message-sent" id="crm-opt-in-numb"><span  style="font-size: 64px;">0</span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="portlet light bordered admin-dashboard-mid-card">
            <div class="portlet-body form">
                <div class="admin-dashboard-block-title">Total Opt-Out</div>
                <div class="admin-dashboard-value">
                    <div class="crm-message-sent" id="crm-opt-out-numb"><span  style="font-size: 64px;">0</span></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row dashboard">
    <div class="col-xs-12 col-sm-12 col-lg-12">
        <div class="portlet light bordered">
            <div class="portlet-body form">
                <div class="activity-feed">
                    <h2 class="dashboard-block-title">Billing History</h2>
                    <div class="">
                        <div class="re-table">
                            <div class="table-scrollable">
                                <table id="management-biling" class="table table-striped table-bordered table-responsive">
                                </table>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Modal For Total Sent - Received report-->
<div class="modal fade" id="total-message-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">Total Sent and Received</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="totalMessageFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="total-message-table" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-message-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>

<!-- Modal For Total Sent - Received summary report-->
<div class="modal fade" id="total-message-summary-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">Summary</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="totalMessageSummaryFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="total-message-summary-table" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-message-summary-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>
<!-- Modal For User List-->
<div class="modal fade" id="user-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="user-modal-title">Total Users</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="userFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="userTable" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <!--- <button class="btn-re-dowload user-list"><i class="fa fa-download" aria-hidden="true"></i></button> --->
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <input type="hidden" name="userID" value="">
            </div>
        </div>
    </div>
</div>
<!-- Modal For Total Opt In - Out report-->
<div class="modal fade" id="total-optin-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">Total Opt-in</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="totalOptInFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="total-optin-table" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-optin-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>
<!-- Modal For Total Opt In - Out report-->
<div class="modal fade" id="total-optout-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">Total Opt-out</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="totalOptOutFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="total-optout-table" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-optout-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>

<!-- Modal For Opt In - Out summary report-->
<div class="modal fade" id="total-opt-summary-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">Summary</strong></h4>
            </div>
            <div class="modal-body">
                <div id="credit-update-log">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="totalOptSummaryFilterBox">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="total-opt-summary-table" class="table table-striped table-responsive">
                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <button class="btn-re-dowload total-opt-summary-list"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>


<cfparam name="variables._title" default="Dashboard - Enterprise">
<cfinclude template="../views/layouts/master.cfm">