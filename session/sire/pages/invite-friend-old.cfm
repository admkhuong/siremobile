<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/referral.css">
</cfinvoke>

<cfinclude template="/session/sire/configs/paths.cfm" />

<cfif !structKeyExists(application, 'objMonkehTweet')>
	<cfscript>
		application.objMonkehTweet =
		createObject('component','public.sire.components.monkehTweets.com.coldfumonkeh.monkehTweet').init(
			consumerKey = _TwConsumerKey,
			consumerSecret = _TwConsumerKeySecret
		);
	</cfscript>
</cfif>

<cfinvoke component="session.sire.models.cfc.referral" method="GetUserReferralCode" returnvariable="userRFCode"></cfinvoke>

<cfif userRFCode.RxresultCode NEQ 1>
	<cflocation url="/session/sire/pages"/>
</cfif>

<cfinvoke component="session.sire.models.cfc.referral" method="GetSMSQuota" returnvariable="smsQuota"></cfinvoke>

<cfif smsQuota.RXRESULTCODE NEQ 1>
	<cfset smsQuota.SMSQUOTA = 0>
</cfif>

<input type="hidden" value="<cfoutput>#smsQuota.SMSQUOTA#</cfoutput>" id="sms-quota" />

<cfparam name="variables._title" default="Invite Friend - Sire">

<main class="container-fluid page my-plan-page">
    <cfinclude template="../views/commons/credits_available.cfm">

	<cfinvoke component="session.sire.models.cfc.order_plan" method="GetNextPlanUpgrade" returnvariable="nextPlanUpgrade">
		<cfinvokeargument name="inpPlanOrder" value="#RetUserPlan.PLANORDER#">
	</cfinvoke>

    <section class="row bg-white">
        <div class="content-header">
        	<div class="row">
				<div class="col-sm-4 col-lg-3 col-md-4 col-xs-12 content-title">
                    <cfinclude template="../views/commons/welcome.cfm">
                </div>
                <div class="col-sm-8 col-lg-9 col-md-8 col-xs-12 content-menu" style="float:right">
                    <a href="/session/sire/pages/my-plan" class="unactive"><span class="icon-my-plan-unactive"></span><span>My Plan</span></a>

					<cfif RetUserPlan.PLANEXPIRED EQ 1 >
                    	<a href="javascript:void(0)" class="icon-disable" title="Please extend your plan."><span class="icon icon-upgrade-plan-unactive"></span><span>Upgrade Plan</span></a>	
                    <cfelseif RetUserPlan.PLANEXPIRED GTE 2 && RetUserPlan.KEYWORDBUYAFTEREXPRIED GT 0  >	
                    	<a href="javascript:void(0)" class="icon-disable" title="Please pay for your keywords"><span class="icon icon-upgrade-plan-unactive"></span><span>Upgrade Plan</span></a>	
                    <cfelse>
                		<cfif nextPlanUpgrade.PlanId_int GT 0>
	                    	<cfset next_plan_id = nextPlanUpgrade.PlanId_int>
	                    	<a href="/session/sire/pages/order-plan?plan=<cfoutput>#next_plan_id#</cfoutput>" title="" class=""><span class="icon-upgrade-plan-unactive"></span><span>Upgrade Plan</span></a>	
	                    <cfelse>
	                    	<a href="javascript:void(0)" class="icon-disable" title=""><span class="icon icon-upgrade-plan-unactive"></span><span>Upgrade Plan</span></a>	
	                    </cfif>
                    </cfif>
                    
                    <a href="/session/sire/pages/buy-sms" title="" class=""><span class="icon-buy-credits-unactive"></span><span>Buy Credit</span></a>

                    <cfif RetUserPlan.PLANEXPIRED GTE 2 && RetUserPlan.KEYWORDBUYAFTEREXPRIED GT 0  >
                    	<a href="javascript:void(0)" class="icon-disable" title="Please pay for your keywords"><span class="icon icon-buy-keyword-unactive"></span><span>Buy Keyword</span></a>	
                    <cfelse>	
                    	<a href="/session/sire/pages/buy-keyword" title="" class=""><span class="icon-buy-keyword-unactive"></span><span>Buy Keyword</span></a>
                    </cfif>
                    <a class="active"><span class="icon-invite-friends"></span><span>Invite a friend</span></a>	
	            </div>    
            </div>
        </div>
        <hr class="hrt0">
		<div class="content-body">
			<div class="container-fluid invite-friends-page">
				<p class="sub-title">Invite a Friend</p>
				<div>
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist" id="invite-friends-tabs">
						<li role="presentation" class="active"><a href="#refer_via_email" aria-controls="home" role="tab" data-toggle="tab">Refer via Email</a></li>
						<!--- SMS INVITE FUNCTION --->
						<!--- <li role="presentation"><a href="#refer_via_sms" aria-controls="profile" role="tab" data-toggle="tab">Refer via SMS</a></li> --->
						<li role="presentation"><a href="#refer_via_social" aria-controls="messages" role="tab" data-toggle="tab">Refer via Social Media</a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="refer_via_email">
							<cfinclude template="../views/referral/email.cfm">
						</div>
						<!--- SMS INVITE FUNCTION --->
						<!--- <div role="tabpanel" class="tab-pane" id="refer_via_sms">
							<cfinclude template="../views/referral/sms.cfm">
						</div> --->
						<div role="tabpanel" class="tab-pane" style="padding: 0px;" id="refer_via_social">
							<cfinclude template="../views/referral/social.cfm">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>

<cfoutput>
	<script type="text/javascript">
		var rfLink =   "#userRFCode.RFLink#"+"&type=3";
	 	var shareUrl =  encodeURIComponent("https://"+window.location.hostname+rfLink);
		var hostname = ("https://"+window.location.hostname);
	</script>
</cfoutput>

<!-- Modal -->
<div class="bootbox modal fade" id="ConfirmSendInvitation" tabindex="-1" role="dialog" aria-labelledby="ConfirmSendInvitation" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title title">Confirm send invitation</h4>
			</div>
			<div class="modal-body">
				<p>Do you want to send the invitation?</p>
				<div id="capcha">
					<div class="form-group">
						<script src='https://www.google.com/recaptcha/api.js'></script>
						<cfoutput>
							<div class="g-recaptcha" data-sitekey="#PUBLICKEY#"></div>
						</cfoutput>
					</div>
					<div id="captcha-message" style="color: #dd1037"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success btn-success-custom" id="btn-send-invitation-confirm"> Confirm </button>
				&nbsp; &nbsp; &nbsp;
				<button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/font-awesome.min.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="https://apis.google.com/js/client.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/vendors/list.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/site/invite-friend.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/site/social-friend-invitation.js">
</cfinvoke>


<cfinclude template="../views/layouts/main.cfm">