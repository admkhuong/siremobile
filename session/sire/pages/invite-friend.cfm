<cfset variables.menuToUse = 2 />

<cfset variables.urlpage = 'buysms' />

<!--- RETRIVE CUSTOMER DATA --->
<cfset firstName =''>
<cfset lastName =''>
<cfset emailAddress =''>
<cfset primaryPaymentMethodId = -1>
<cfset expirationDate =''>
<cfset maskedNumber =''>
<cfset line1 =''>
<cfset city =''>
<cfset state =''>
<cfset zip =''>
<cfset country =''> 
<cfset phone =''>
<cfset hidden_card_form =''>
<cfset disabled_field =''>
<cfset cvv =''>
<cfset paymentType = 'card'>
<cfset RetCustomerInfo = ''>

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("../assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css", true)
        .addJs("../assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js", true)
        .addJs("../assets/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js", true)
        // .addJs("../assets/pages/scripts/myplan.js")
        .addJs("https://apis.google.com/js/client.js", true)
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)
        .addJs("/session/sire/js/vendors/list.min.js", true);
</cfscript>

<!--- GET USER BILLING DATA --->
<cfinvoke component="session.sire.models.cfc.billing"  method="GetBalanceCurrentUser"  returnvariable="RetValBillingData"></cfinvoke>

<!--- GET USER PLAN DETAIL --->
<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUserPlan" returnvariable="RetUserPlan"></cfinvoke>

<!--- GET NUMBER OF USED MLPS BY USER --->
<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUsedMLPsByUserId" returnvariable="RetUsedMLP"></cfinvoke>

<!--- GET NUMBER OF USED SHORT URLS BY USER --->
<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUsedShortURLsByUserId" returnvariable="RetUsedShortURL"></cfinvoke>

<!--- GET USER REFERRAL INFO --->
<cfinvoke component="session.sire.models.cfc.referral" method="GetUserReferInfo" returnvariable="RetValUserReferInfo">
    <cfinvokeargument name="inpUserId" value="#SESSION.USERID#">
</cfinvoke>

<!--- GET USER INFO --->
<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke>

<cfinvoke component="session.sire.models.cfc.billing" method="RetrieveCustomer" returnvariable="RetCustomerInfo"></cfinvoke>

<!--- GET NEXT PLAN UPGRADE --->
<cfinvoke component="session.sire.models.cfc.order_plan" method="GetNextPlanUpgrade" returnvariable="nextPlanUpgrade">
    <cfinvokeargument name="inpPlanOrder" value="#RetUserPlan.PLANORDER#">
</cfinvoke>

<cfif RetCustomerInfo.RXRESULTCODE EQ 1>    
    <cfset firstName = RetCustomerInfo.CUSTOMERINFO.firstName>
    <cfset lastName = RetCustomerInfo.CUSTOMERINFO.lastName>
    <cfset emailAddress = RetCustomerInfo.CUSTOMERINFO.emailAddress>
    <cfset primaryPaymentMethodId = 0> <!--- do not print --->
    <cfset expirationDate = RetCustomerInfo.CUSTOMERINFO.expirationDate>
    <cfset maskedNumber = RetCustomerInfo.CUSTOMERINFO.maskedNumber>
    <cfset line1 = RetCustomerInfo.CUSTOMERINFO.line1>
    <cfset city = RetCustomerInfo.CUSTOMERINFO.city>
    <cfset state = RetCustomerInfo.CUSTOMERINFO.state>
    <cfset zip = RetCustomerInfo.CUSTOMERINFO.zip>
    <cfset country = RetCustomerInfo.CUSTOMERINFO.country> 
    <cfset phone = RetCustomerInfo.CUSTOMERINFO.phone>
    <cfset hidden_card_form = 'display:none'>
    <cfset disabled_field ='display: none'>
    <cfset paymentType = RetCustomerInfo.CUSTOMERINFO.paymentType>
    <cfset cvv =RetCustomerInfo.CUSTOMERINFO.cvv>
<cfelseif RetCustomerInfo.RXRESULTCODE EQ -1>
    <main class="container-fluid page my-plan-page">
        <cfinclude template="../views/commons/credits_available.cfm">
            <section class="row bg-white">
                <div class="content-header">
                    <div class="col-md-12">
                        <cfparam name="variables._title" default="My Plan - Sire">
                        <p class="text-danger text-center "><cfoutput>#RetCustomerInfo.MESSAGE#</cfoutput></p>
                    </div>  
                </div>
            </section>  
    </main>
    <cfinclude template="../views/layouts/main.cfm">
    <cfexit>
</cfif>

<cfset userPlanName = 'Free'>
<cfif RetUserPlan.RXRESULTCODE EQ 1>
    <cfset userPlanName =   RetUserPlan.PLANNAME>
</cfif>

<cfif RetUserPlan.PLANEXPIRED GT 0> 
    <cfset monthlyAmount = RetUserPlan.AMOUNT + (RetUserPlan.KEYWORDPURCHASED + RetUserPlan.KEYWORDBUYAFTEREXPRIED)*RetUserPlan.PRICEKEYWORDAFTER >
<cfelse>    
    <cfset monthlyAmount = RetUserPlan.AMOUNT + RetUserPlan.KEYWORDPURCHASED*RetUserPlan.PRICEKEYWORDAFTER >
</cfif>

<!--- GET MAX KEYWORD CAN EXPIRED --->
<cfset UnsubscribeNumberMax = 0>
<cfif RetUserPlan.PLANEXPIRED EQ 1>
    <cfif RetUserPlan.KEYWORDAVAILABLE GT RetUserPlan.KEYWORDBUYAFTEREXPRIED>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDBUYAFTEREXPRIED>
    <cfelse>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDAVAILABLE>
    </cfif>
<cfelseif RetUserPlan.PLANEXPIRED EQ 2> <!--- IF USER PLAN HAS DOWNGRADE IT CAN NOT UNSCRIBE KEYWORD --->
    <!---
    <cfif RetUserPlan.KEYWORDAVAILABLE GT RetUserPlan.KEYWORDBUYAFTEREXPRIED>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDBUYAFTEREXPRIED>
    <cfelse>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDAVAILABLE>
    </cfif>    
    --->
    <cfset UnsubscribeNumberMax = 0>
<cfelse>
    <cfif RetUserPlan.KEYWORDAVAILABLE GT RetUserPlan.KEYWORDPURCHASED>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDPURCHASED>
    <cfelse>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDAVAILABLE>
    </cfif>
</cfif>

<cfset planMonthlyAmount = RetUserPlan.AMOUNT />
<div class="portlet light bordered">
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <!--- <form id="my_plan_form" class="form-gd" action="##" autocomplete="off" role="form"> --->
            <input type="hidden" name="primaryPaymentMethodId" value="<cfoutput>#primaryPaymentMethodId#</cfoutput>">
            <input type="hidden" value="<cfoutput>#userInfo.FULLEMAIL#</cfoutput>" id="h_email">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="clearfix">
                            <h3 class="form-heading">Plan Details</h3>
                        </div>
                        <div class="statistical">
                            <!--- Plan Details included by plan_details.cfm --->
                            <cfinclude template="../views/commons/plan_details.cfm">
                        </div>

                        <div class="tabbable tabbable-tabdrop tab-invite-1">
                            <button class="btn green-gd uk-width-1-1 visible-xs uk-margin-small-bottom" type="button" uk-toggle="target: #toggle-xs; cls: uk-hidden"><i class="fa fa-th" aria-hidden="true"></i> Toggle My Plan Tab</button>
                            <ul id="toggle-xs" class="uk-child-width-1-3 uk-child-width-1-5@s invite-ul uk-tab" uk-margin>
                                <li>
                                    <a href="/session/sire/pages/my-plan">
                                        <span class="tab-img i-1"></span>
                                        <span>My Plan</span>
                                    </a>
                                </li>
                                <li>
                                    <cfif RetUserPlan.PLANEXPIRED EQ 1 > 
                                        <a class="plan-disabled" title="Please extend your plan.">
                                            <span class="tab-img i-2"></span>
                                            <span>Upgrade Plan</span>
                                        </a>   
                                        <cfelseif RetUserPlan.PLANEXPIRED GTE 2 && RetUserPlan.KEYWORDBUYAFTEREXPRIED GT 0  >   
                                            <a class="plan-disabled" title="Please pay for your keywords">
                                                <span class="tab-img i-2"></span>
                                                <span>Upgrade Plan</span>
                                            </a>  
                                            <cfelse>
                                                <cfif nextPlanUpgrade.PlanId_int GT 0>
                                                    <cfset next_plan_id = nextPlanUpgrade.PlanId_int>
                                                    <a href="/session/sire/pages/order-plan?plan=<cfoutput>#next_plan_id#</cfoutput>" title="" class="">
                                                    <span class="tab-img i-2"></span>
                                                    <span>Upgrade Plan</span>
                                                </a>   
                                                <cfelse>
                                                    <a class="plan-disabled" title="">
                                                        <span class="tab-img i-2"></span>
                                                        <span>Upgrade Plan</span>
                                                    </a>   
                                                </cfif>
                                            </cfif> 
                                        </li>
                                        <li>
                                            <!--- /session/sire/pages/buy-sms --->
                                            <a href="/session/sire/pages/my-plan?active=addon" title="" class="link-myplan">
                                                <span class="tab-img i-3"></span>
                                                <span>Buy Credit</span>
                                            </a>
                                        </li>
                                        <li>
                                            <cfif RetUserPlan.PLANEXPIRED GTE 2 && RetUserPlan.KEYWORDBUYAFTEREXPRIED GT 0  >
                                                <a class="plan-disabled" title="Please pay for your keywords">
                                                    <span class="tab-img i-4"></span>
                                                    <span>Buy Keyword</span>
                                                </a> 
                                                <cfelse>  
                                                    <a href="/session/sire/pages/buy-keyword" title="" class="link-myplan">
                                                        <span class="tab-img i-4"></span>
                                                        <span>Buy Keyword</span>
                                                    </a>
                                                </cfif>
                                            </li>
                                            <li class="uk-active">
                                                <a href="/session/sire/pages/invite-friend" class="link-myplan">
                                                    <span class="tab-img i-5"></span>
                                                    <span>Invite a friend</span>
                                                </a>
                                            </li>
                                        </ul>
                            <div class="tab-content invite-content">
                                <div class="tab-pane" id="plantab1">
                                </div>
                                <div class="tab-pane" id="plantab2">
                                    <div class="wrap-upgrade">
                                        <div class="form-group">
                                            <label>Select Your Plan</label>
                                            <select class="bs-select form-control">
                                                <option>Individual</option>
                                                <option>Individual 1</option>
                                                <option>Individual 2</option>
                                                <option>Individual 3</option>
                                            </select>
                                        </div>

                                        <div class="statistical">
                                            <ul class="nav list-plan-statistical">
                                                <li>Monthly Amount: <span>$24</span>
                                                    <em>Your billing date will be the same day every month.</em>
                                                </li>
                                                <li>Credits Available:  <span>1000</span></li>
                                                <li>Keywords Available: <span>5</span></li>
                                                <li>MLPs Available: <span>5</span></li>
                                                <li>Short URLs Available <span>5</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="plantab3">
                                    <div class="wrap-buycredit">
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div class="row">
                                                    <div class="col-xs-7">
                                                        <div class="form-group">
                                                            <label>Number of Credits <span>*</span></label>
                                                            <input disabled="" type="text" class="form-control amount-input" placeholder="">
                                                            <label class="amount">Amount: <span class="text-green">$ 5.04</span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-5">
                                                        <span class="amountx">x 2.7 Cents</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <a href="#" class="btn newbtn green-gd btt-calculate">calculate</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="plantab4">
                                    <div class="wrap-buycredit">
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div class="row">
                                                    <div class="col-xs-7">
                                                        <div class="form-group">
                                                            <label>Number of Keywords <span>*</span></label>
                                                            <input disabled="" type="text" class="form-control amount-input" placeholder="">
                                                            <label class="amount">Amount: <span class="text-green">$ 25</span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-5">
                                                        <span class="amountx">x 5$</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <a href="#" class="btn newbtn green-gd btt-calculate">calculate</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane active" id="plantab5">
                                    <div class="tab-invite tabbable tabbable-tabdrop">
                                        <ul class="nav nav-tabs tab-invite-ul">
                                            <li class="active">
                                                <a href="#refer_via_email" data-toggle="tab">Refer via E-mail</a>
                                            </li>
                                            <li>
                                                <a href="#refer_via_social" data-toggle="tab">Refer via Social Media</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content tab-invite-content">
                                            <div class="tab-pane active" id="refer_via_email">
                                                <div class="form-group">
                                                    <label for="">Email</label>
                                                    <div class="row row-small">
                                                        <div class="col-xs-9 col-lg-10">
                                                            <form id="email_form">
                                                                <input type="text" class="form-control validate[required,custom[email]]" id="emailadd" />
                                                            </form>
                                                        </div>
                                                        <div class="col-xs-3 col-lg-2">
                                                            <a class="btn btn-block green-gd" id="add-email-btn">add</a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="block">Import Contacts</label>
                                                    <a class="btt-invite-email gmail gmail-contacts-btn" id="gmail-contacts-btn">
                                                        <img class="img-responsive" src="../assets/layouts/layout4/img/icon-gmail.png" alt="">
                                                    </a>
                                                    <a class="btt-invite-email outlook outlook-contacts-btn" id="outlook-contacts-btn">
                                                        <img class="img-responsive" src="../assets/layouts/layout4/img/icon-outlook.png" alt="">
                                                    </a>
                                                    <a class="btt-invite-email yahoo yahoo-contacts-btn" id="yahoo-contacts-btn">
                                                        <img class="img-responsive" src="../assets/layouts/layout4/img/icon-yahoo.png" alt="">
                                                    </a>
                                                </div>

                                                <div class="holder-friend friends-list" id="gmail" style="display: none;">
                                                    <div class="row list">
                                                    </div>
                                                </div>
                                                <div class="holder-friend friends-list" id="outlook" style="display: none;">
                                                    <div class="row list">
                                                    </div>
                                                </div>
                                                <div class="holder-friend friends-list" id="yahoo" style="display: none;">
                                                    <div class="row list">
                                                    </div>
                                                </div>
                                                <br/>
                                                <div class="form-group">
                                                    <label class="block">Selected Emails</label>
                                                </div>

                                                <div class="holder-friend selected-email content" id="selected-email">
                                                    
                                                </div>

                                                <div class="dv-send-invite">
                                                    <button class="btn newbtn green-gd btn-send-invite" id="send-email-invitation-btn">Send Invite</button>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="refer_via_social">
                                                <div class="form-group social share-url">
                                                    <label class="block">Share</label>
                                                    <a class="invite-social-ic fb social-auth">
                                                        <i class="fa fa-facebook-square"></i>
                                                    </a>
                                                    <a class="invite-social-ic tw social-auth-tw">
                                                        <i class="fa fa-twitter-square"></i>
                                                    </a>
                                                    <a class="invite-social-ic gp social-auth">
                                                        <i class="fa fa-google-plus-square"></i>
                                                    </a>
                                                </div>

                                                <div class="holder-friend friends-list" id="twitter" style="display: none">
                                                    <div id="twitter-friend-list" class="row list">
                                                        
                                                    </div>
                                                </div>

                                                <div class="selected-friends-section" style="display: none;">
                                                    <div class="form-group">
                                                        <label class="block">Selected Friends</label>
                                                    </div>
                                                    <div class="holder-friend selected-email" id="twitter-friend-list-selected">

                                                    </div>
                                                    <div class="dv-send-invite">
                                                        <button class="btn newbtn green-gd btn-send-invite send-invitation">Send Invite</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 cardholder_details">
                        <!--- Cardholder details included by cardholder_details.cfm --->
                        <cfinclude template="../views/commons/cardholder_details.cfm">
                    </div>
                </div>
            </div>
        <!--- </form> --->
        <!-- END FORM-->
    </div>
</div>

<cfinclude template="../views/commons/sireplandetails.cfm">

<cfparam name="variables._title" default="Invite friends - Sire">

<cfinclude template="../views/layouts/master.cfm">

<cfinclude template="../views/commons/what-mlp-keyword.cfm">

<!--- For invite friend function --->
<cfinclude template="/session/sire/configs/paths.cfm" />

<link rel="stylesheet" type="text/css" href="/session/sire/assets/pages/styles/invite-friend.css">

<cfif !structKeyExists(application, 'objMonkehTweet')>
    <cfscript>
        application.objMonkehTweet =
        createObject('component','public.sire.components.monkehTweets.com.coldfumonkeh.monkehTweet').init(
            consumerKey = _TwConsumerKey,
            consumerSecret = _TwConsumerKeySecret
        );
    </cfscript>
</cfif>

<cfinvoke component="session.sire.models.cfc.referral" method="GetUserReferralCode" returnvariable="userRFCode"></cfinvoke>

<cfoutput>
    <script type="text/javascript">
        var rfLink =   "#userRFCode.RFLink#"+"&type=3";
        var shareUrl =  encodeURIComponent("https://"+window.location.hostname+rfLink);
        var hostname = ("https://"+window.location.hostname);
    </script>
</cfoutput>

<cfinvoke component="session.sire.models.cfc.referral" method="GetSMSQuota" returnvariable="smsQuota"></cfinvoke>

<cfif smsQuota.RXRESULTCODE NEQ 1>
    <cfset smsQuota.SMSQUOTA = 0>
</cfif>

<input type="hidden" value="<cfoutput>#smsQuota.SMSQUOTA#</cfoutput>" id="sms-quota" />

<!-- Modal -->
<div class="bootbox modal fade" id="ConfirmSendInvitation" tabindex="-1" role="dialog" aria-labelledby="ConfirmSendInvitation" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title title">Confirm send invitation</h4>
            </div>
            <div class="modal-body">
                <p>Do you want to send the invitation?</p>
                <div id="capcha">
                    <div class="form-group">
                        <script src='https://www.google.com/recaptcha/api.js'></script>
                        <cfoutput>
                            <div class="g-recaptcha" data-sitekey="#PUBLICKEY#"></div>
                        </cfoutput>
                    </div>
                    <div id="captcha-message" style="color: #dd1037"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-success-custom" id="btn-send-invitation-confirm"> Confirm </button>
                &nbsp; &nbsp; &nbsp;
                <button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script type="text/javascript" src="/session/sire/assets/pages/scripts/invite-friend.js"></script>
<script type="text/javascript" src="/session/sire/assets/pages/scripts/invite-friend-social.js"></script>
<!--- End invite friend function --->