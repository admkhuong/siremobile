<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js">
</cfinvoke>
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/ajaxupload.js">
</cfinvoke>
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/site/common.js">
</cfinvoke>
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/my-account.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/setting.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/font-awesome.min.css">
</cfinvoke>

<cfinclude template="/session/sire/configs/paths.cfm">

<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke>

<cfif userInfo.RESULT NEQ 'SUCCESS'>
	<cflocation url = '/' addtoken="false">
</cfif>

<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="userOrgInfo"></cfinvoke>

<cfset _has_Org = false>	
<cfif userOrgInfo.RXRESULTCODE EQ 1>
	<cfset _has_Org = true>
	<cfset orgInfoData = userOrgInfo.ORGINFO>
</cfif>


<cfset check_user_security_question = false>

<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserListQuestion" returnvariable="userListQuestion">
	<cfinvokeargument name="detail_question" value=0>
</cfinvoke>

<cfif userListQuestion.RESULT EQ 'SUCCESS' >
	<cfset check_user_security_question = true>
</cfif>


<main class="container-fluid page">
	<cfinclude template="../views/commons/credits_available.cfm">
	<section class="row bg-white">
		<div class="content-header">
			<div class="row">
                <div class="col-sm-5 content-title">
                	<cfinclude template="../views/commons/welcome.cfm">
                </div>
                <div class="col-sm-7 content-menu">
                    	<a><span class="icon-my-plan-unactive"></span><span>My Plan</span></a>
                    
                    
                    	<a><span class="icon icon-upgrade-plan-unactive"></span><span>Upgrade Plan</span></a>	
                    	
                    
                    	<a><span class="icon-buy-credits-unactive"></span><span>Buy Credit</span></a>

                    	
                    	<a><span class="icon-buy-keyword-unactive"></span><span>Buy Keyword</span></a>
                    	<a href="##" class="active"><span class="icon-invite-friends"></span><span>Setting</span></a>
	            </div>
            </div>
		</div>
		<hr class="hrt0">
		<div class="content-body">
			<div class="container-fluid invite-friends-page">
				<p class="sub-title">Invite a Friend</p>
				
				<div>

				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs" role="tablist" id="invite-friends-tabs">
				    <li role="presentation" class="active"><a href="#refer_via_email" aria-controls="home" role="tab" data-toggle="tab">Refer via Email</a></li>
				    <li role="presentation"><a href="#refer_via_sms" aria-controls="profile" role="tab" data-toggle="tab">Refer via SMS</a></li>
				    <li role="presentation"><a href="#refer_via_social" aria-controls="messages" role="tab" data-toggle="tab">Refer via Social Media</a></li>
				  </ul>

				  <!-- Tab panes -->
				  <div class="tab-content">
				    <div role="tabpanel" class="tab-pane active" id="refer_via_email">
						<div class="row">
				    		<div class="container-fluid">
								<div class="row">
									<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 invite-friend-input">
										<div class="input-group input-group-custom">
								      		<input type="text" class="form-control input-medium" placeholder="Email Address">
									      	<span class="input-group-btn">
									        	<button class="btn btn-medium btn-success-custom" type="button">Send Invite</button>
								        	</span>
							        	</div>
						        	</div>
						        	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 pull-right invite-friend-input">
										<div class="input-group input-group-custom">
								      		<input type="text" class="form-control input-medium" placeholder="Search">
									      	<span class="input-group-btn">
									        	<button class="btn btn-medium btn-success-custom">Search</button>
								        	</span>
								        </div>
								    </div>
							    </div>
							    <!-- /input-group -->
						    	<div class="mail-pills col-lg-3 col-sm-4 col-xs-12 no-padding">
									<ul>
										<li class="active">
											<a href="#gmail" data-toggle="pill">
												<div class="media">
												  	<div class="media-left">
												      	<img class="media-object" src="../images/icon/icon-gmail.png" alt="...">
											  		</div>
											  		<div class="media-body">
										    			<h4 class="media-heading">Import Gmail Contacts</h4>
												  	</div>
												</div>
											</a>
										</li>
										<li>
											<a href="#outlook" data-toggle="pill">
												<div class="media">
												  <div class="media-left">
												      <img class="media-object" src="../images/icon/icon-outlook.png" alt="...">
												  </div>
												  <div class="media-body">
												    <h4 class="media-heading">Import Outlook Contacts</h4>
												  </div>
												</div>
											</a>
										</li>
										<li>
											<a href="#aol" data-toggle="pill">
												<div class="media">
												  <div class="media-left">
												      <img class="media-object" src="../images/icon/icon-aol.png" alt="...">
												  </div>
												  <div class="media-body">
												    <h4 class="media-heading">Import Aol Contacts</h4>
												  </div>
												</div>
											</a>
										</li>
										<li>
											<a href="#yahoo" data-toggle="pill">
												<div class="media">
												  <div class="media-left">
												      <img class="media-object" src="../images/icon/icon-yahoo.png" alt="...">
												  </div>
												  <div class="media-body">
												    <h4 class="media-heading">Import Yahoo Mail Contacts</h4>
												  </div>
												</div>
											</a>
										</li>
										<li>
											<a href="#contacts" data-toggle="pill">
												<div class="media">
													  <div class="media-left">
													      <img class="media-object" src="../images/icon/icon-contacts.png" alt="...">
													  </div>
													  <div class="media-body">
													    <h4 class="media-heading">Import Other Contacts</h4>
													  </div>
												</div>
											</a>
										</li>
							    	</ul>
						    	</div>
						    	<div class="clearfix visible-xs"></div>
						    	<div class="tab-content col-lg-9 col-sm-8">
									<div class="friends-list tab-pane fade in active" id="gmail">
										<div class="row">
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="signed clearfix">
													<div class="col-xs-1 no-padding checkbox-custom">
														<input type="checkbox" id="user1"/> 
													</div>
													<div class="col-xs-4 no-padding">
														<img src="../images/friend.png" alt="" class="img-responsive"/>
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user1">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Already signed up</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-1 no-padding checkbox-custom">
														<input type="checkbox" id="user2"/> 
													</div>
													<div class="col-xs-5 no-padding">
														<img src="../images/friend.png" alt="" class="img-responsive"/>
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="sent clearfix">
													<div class="col-xs-1 no-padding checkbox-custom">
														<input type="checkbox" id="user3"/> 
													</div>
													<div class="col-xs-5 no-padding">
														<img src="../images/friend.png" alt="" class="img-responsive"/>
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user3">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Invitation sent</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="signed clearfix">
													<div class="col-xs-1 no-padding checkbox-custom">
														<input type="checkbox" id="user1"/> 
													</div>
													<div class="col-xs-4 no-padding">
														<img src="../images/friend.png" alt="" class="img-responsive"/>
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user1">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Already signed up</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-1 no-padding checkbox-custom">
														<input type="checkbox" id="user2"/> 
													</div>
													<div class="col-xs-5 no-padding">
														<img src="../images/friend.png" alt="" class="img-responsive"/>
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="sent clearfix">
													<div class="col-xs-1 no-padding checkbox-custom">
														<input type="checkbox" id="user3"/> 
													</div>
													<div class="col-xs-5 no-padding">
														<img src="../images/friend.png" alt="" class="img-responsive"/>
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user3">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Invitation sent</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="signed clearfix">
													<div class="col-xs-1 no-padding checkbox-custom">
														<input type="checkbox" id="user1"/> 
													</div>
													<div class="col-xs-4 no-padding">
														<img src="../images/friend.png" alt="" class="img-responsive"/>
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user1">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Already signed up</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-1 no-padding checkbox-custom">
														<input type="checkbox" id="user2"/> 
													</div>
													<div class="col-xs-5 no-padding">
														<img src="../images/friend.png" alt="" class="img-responsive"/>
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="sent clearfix">
													<div class="col-xs-1 no-padding checkbox-custom">
														<input type="checkbox" id="user3"/> 
													</div>
													<div class="col-xs-5 no-padding">
														<img src="../images/friend.png" alt="" class="img-responsive"/>
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user3">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Invitation sent</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="signed clearfix">
													<div class="col-xs-1 no-padding checkbox-custom">
														<input type="checkbox" id="user1"/> 
													</div>
													<div class="col-xs-4 no-padding">
														<img src="../images/friend.png" alt="" class="img-responsive"/>
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user1">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Already signed up</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-1 no-padding checkbox-custom">
														<input type="checkbox" id="user2"/> 
													</div>
													<div class="col-xs-5 no-padding">
														<img src="../images/friend.png" alt="" class="img-responsive"/>
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="sent clearfix">
													<div class="col-xs-1 no-padding checkbox-custom">
														<input type="checkbox" id="user3"/> 
													</div>
													<div class="col-xs-5 no-padding">
														<img src="../images/friend.png" alt="" class="img-responsive"/>
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user3">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Invitation sent</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="signed clearfix">
													<div class="col-xs-1 no-padding checkbox-custom">
														<input type="checkbox" id="user1"/> 
													</div>
													<div class="col-xs-4 no-padding">
														<img src="../images/friend.png" alt="" class="img-responsive"/>
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user1">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Already signed up</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-1 no-padding checkbox-custom">
														<input type="checkbox" id="user2"/> 
													</div>
													<div class="col-xs-5 no-padding">
														<img src="../images/friend.png" alt="" class="img-responsive"/>
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="sent clearfix">
													<div class="col-xs-1 no-padding checkbox-custom">
														<input type="checkbox" id="user3"/> 
													</div>
													<div class="col-xs-5 no-padding">
														<img src="../images/friend.png" alt="" class="img-responsive"/>
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user3">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Invitation sent</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="signed clearfix">
													<div class="col-xs-1 no-padding checkbox-custom">
														<input type="checkbox" id="user1"/> 
													</div>
													<div class="col-xs-4 no-padding">
														<img src="../images/friend.png" alt="" class="img-responsive"/>
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user1">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Already signed up</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-1 no-padding checkbox-custom">
														<input type="checkbox" id="user2"/> 
													</div>
													<div class="col-xs-5 no-padding">
														<img src="../images/friend.png" alt="" class="img-responsive"/>
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="sent clearfix">
													<div class="col-xs-1 no-padding checkbox-custom">
														<input type="checkbox" id="user3"/> 
													</div>
													<div class="col-xs-5 no-padding">
														<img src="../images/friend.png" alt="" class="img-responsive"/>
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user3">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Invitation sent</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="signed clearfix">
													<div class="col-xs-1 no-padding checkbox-custom">
														<input type="checkbox" id="user1"/> 
													</div>
													<div class="col-xs-4 no-padding">
														<img src="../images/friend.png" alt="" class="img-responsive"/>
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user1">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Already signed up</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-1 no-padding checkbox-custom">
														<input type="checkbox" id="user2"/> 
													</div>
													<div class="col-xs-5 no-padding">
														<img src="../images/friend.png" alt="" class="img-responsive"/>
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="sent clearfix">
													<div class="col-xs-1 no-padding checkbox-custom">
														<input type="checkbox" id="user3"/> 
													</div>
													<div class="col-xs-5 no-padding">
														<img src="../images/friend.png" alt="" class="img-responsive"/>
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user3">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Invitation sent</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="signed clearfix">
													<div class="col-xs-1 no-padding checkbox-custom">
														<input type="checkbox" id="user1"/> 
													</div>
													<div class="col-xs-4 no-padding">
														<img src="../images/friend.png" alt="" class="img-responsive"/>
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user1">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Already signed up</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-1 no-padding checkbox-custom">
														<input type="checkbox" id="user2"/> 
													</div>
													<div class="col-xs-5 no-padding">
														<img src="../images/friend.png" alt="" class="img-responsive"/>
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="sent clearfix">
													<div class="col-xs-1 no-padding checkbox-custom">
														<input type="checkbox" id="user3"/> 
													</div>
													<div class="col-xs-5 no-padding">
														<img src="../images/friend.png" alt="" class="img-responsive"/>
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user3">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Invitation sent</span>
														</label>
													</div>
												</div>		
											</div>
											
										</div>
									</div>
									<div class="friends-list tab-pane fade" id="outlook">
										<div class="row">
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="signed clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user1"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user1">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Already signed up</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user2"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user2"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="signed clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user1"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user1">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Already signed up</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user2"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="sent clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user3"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user3">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Invitation sent</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user2"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="sent clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user3"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user3">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Invitation sent</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="signed clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user1"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user1">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Already signed up</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user2"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>

											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="signed clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user1"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user1">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Already signed up</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user2"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="sent clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user3"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user3">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Invitation sent</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user2"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="sent clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user3"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user3">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Invitation sent</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="signed clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user1"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user1">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Already signed up</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user2"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="signed clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user1"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user1">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Already signed up</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user2"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="sent clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user3"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user3">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Invitation sent</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user2"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="sent clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user3"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user3">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Invitation sent</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="signed clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user1"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user1">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Already signed up</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user2"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
										</div>
									</div>
									<div class="friends-list tab-pane fade" id="aol">
										<div class="row">
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="sent clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user3"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user3">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Invitation sent</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="signed clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user1"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user1">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Already signed up</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user2"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="sent clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user3"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user3">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Invitation sent</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="signed clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user1"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user1">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Already signed up</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="sent clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user3"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user3">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Invitation sent</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="signed clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user1"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user1">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Already signed up</span>
														</label>
													</div>
												</div>		
											</div>
										</div>
									</div>
									<div class="friends-list tab-pane fade" id="yahoo">
										<div class="row">
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="sent clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user3"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user3">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Invitation sent</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="signed clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user1"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user1">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Already signed up</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user2"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
										</div>
									</div>
									<div class="friends-list tab-pane fade" id="contacts">
										<div class="row">
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="signed clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user1"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user1">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Already signed up</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user2"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="sent clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user3"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user3">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Invitation sent</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="signed clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user1"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user1">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Already signed up</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user2"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="sent clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user3"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user3">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Invitation sent</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user2"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="sent clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user3"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user3">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Invitation sent</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="signed clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user1"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user1">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Already signed up</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user2"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="signed clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user1"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user1">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Already signed up</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user2"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="sent clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user3"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user3">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Invitation sent</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user2"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="sent clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user3"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user3">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Invitation sent</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="signed clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user1"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user1">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<span class="status">Already signed up</span>
														</label>
													</div>
												</div>		
											</div>
											<div class="friend-list-item col-sm-6 col-md-4 col-lg-3">
												<div class="clearfix">
													<div class="col-xs-5 no-padding">
														<input type="checkbox" id="user2"/> 
														<img src="../images/friend.png" alt="" />
													</div>
													<div class="col-xs-7 no-padding">
														<label for="user2">
															<span class="name">Jessie Jones</span>
															<span class="email">email@email.com</span>
															<!--- <span class="status">Already signed up</span> --->
														</label>
													</div>
												</div>		
											</div>
										</div>
									</div>
									<h4><strong>Selected Emails</strong></h4>
									<div class="selected-email clearfix">
										<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 selected-email-item">
											<div class="alert">
											    johndoe@email.com<a href="#" class="close">&times;</a>
											    <div class="text-center type">Gmail</div>
										  	</div>
									  	</div>
									  	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 selected-email-item">
											<div class="alert">
											    johndoe@email.com<a href="#" class="close">&times;</a>
										  	</div>
									  	</div>
									  	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 selected-email-item">
											<div class="alert">
											    johndoe@email.com<a href="#" class="close">&times;</a>
											    <div class="text-center type">Gmail</div>
										  	</div>
									  	</div>
									  	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 selected-email-item">
											<div class="alert">
											    johndoe@email.com<a href="#" class="close">&times;</a>
										  	</div>
									  	</div>
									  	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 selected-email-item">
											<div class="alert">
											    johndoe@email.com<a href="#" class="close">&times;</a>
										  	</div>
									  	</div>
									  	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 selected-email-item">
											<div class="alert">
											    johndoe@email.com<a href="#" class="close">&times;</a>
										  	</div>
									  	</div>
									  	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 selected-email-item">
											<div class="alert">
											    johndoe@email.com<a href="#" class="close">&times;</a>
											    <div class="text-center type">Gmail</div>
										  	</div>
									  	</div>
									  	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 selected-email-item">
											<div class="alert">
											    johndoe@email.com<a href="#" class="close">&times;</a>
											    <div class="text-center type">Gmail</div>
										  	</div>
									  	</div>
									  	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 selected-email-item">
											<div class="alert">
											    johndoe@email.com<a href="#" class="close">&times;</a>
										  	</div>
									  	</div>
									  	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 selected-email-item">
											<div class="alert">
											    johndoe@email.com<a href="#" class="close">&times;</a>
											    <div class="text-center type">Gmail</div>
										  	</div>
									  	</div>
									  	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 selected-email-item">
											<div class="alert">
											    johndoe@email.com<a href="#" class="close">&times;</a>
										  	</div>
									  	</div>
									  	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 selected-email-item">
											<div class="alert">
											    johndoe@email.com<a href="#" class="close">&times;</a>
										  	</div>
									  	</div>
									  	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 selected-email-item">
											<div class="alert">
											    johndoe@email.com<a href="#" class="close">&times;</a>
										  	</div>
									  	</div>
									  	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 selected-email-item">
											<div class="alert">
											    johndoe@email.com<a href="#" class="close">&times;</a>
											    <div class="text-center type">Gmail</div>
										  	</div>
									  	</div>
									</div>
						    	</div>
							</div>
				    	</div>
				    </div>
				    <div role="tabpanel" class="tab-pane" id="refer_via_sms">
				    	<div class="row">
				    		<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 invite-friend-input">
								<div class="input-group input-group-custom">
							      <input type="text" class="form-control input-medium" placeholder="Phone Number">
							      <span class="input-group-btn input-group-btn">
							        <button class="btn btn-medium btn-success-custom" type="button">Add Another</button>
							      </span>
							    </div><!-- /input-group -->
							    <div class="margin-top-10">
					        		<button type="submit" class="btn btn-medium btn-success-custom">Send Invite</button>
					        	</div>
							</div>
							<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 pull-right no-padding invite-friend-input">								
								<div class="input-group input-group-custom">
						      		<input type="text" class="form-control input-medium" placeholder="Search">
							      	<span class="input-group-btn">
							        	<button class="btn btn-medium btn-success-custom">Search</button>
						        	</span>
					        	</div>
						    </div>
				    	</div>
						<div class="row">
							<div class="container-fluid">
								<h4><strong>Selected Phone Number(s)</strong></h4>
								<div class="selected-email clearfix">
									<div class="col-lg-2 col-md-4 col-sm-6 col-xs-6 selected-phone-item">
										<div class="alert">
										    (555) 555 -5555<a href="#" class="close">&times;</a>
									  	</div>
								  	</div>
								  	<div class="col-lg-2 col-md-4 col-sm-6 col-xs-6 selected-phone-item">
										<div class="alert">
										    (555) 555 -5555<a href="#" class="close">&times;</a>
									  	</div>
								  	</div>
								  	<div class="col-lg-2 col-md-4 col-sm-6 col-xs-6 selected-phone-item">
										<div class="alert">
										    (555) 555 -5555<a href="#" class="close">&times;</a>
									  	</div>
								  	</div>
								  	<div class="col-lg-2 col-md-4 col-sm-6 col-xs-6 selected-phone-item">
										<div class="alert">
										    (555) 555 -5555<a href="#" class="close">&times;</a>
									  	</div>
								  	</div>
								  	<div class="col-lg-2 col-md-4 col-sm-6 col-xs-6 selected-phone-item">
										<div class="alert">
										    (555) 555 -5555<a href="#" class="close">&times;</a>
									  	</div>
								  	</div>
								  	<div class="col-lg-2 col-md-4 col-sm-6 col-xs-6 selected-phone-item">
										<div class="alert">
										    (555) 555 -5555<a href="#" class="close">&times;</a>
									  	</div>
								  	</div>
								  	<div class="col-lg-2 col-md-4 col-sm-6 col-xs-6 selected-phone-item">
										<div class="alert">
										    (555) 555 -5555<a href="#" class="close">&times;</a>
									  	</div>
								  	</div>
								  	<div class="col-lg-2 col-md-4 col-sm-6 col-xs-6 selected-phone-item">
										<div class="alert">
										    (555) 555 -5555<a href="#" class="close">&times;</a>
									  	</div>
								  	</div>
								  	<div class="col-lg-2 col-md-4 col-sm-6 col-xs-6 selected-phone-item">
										<div class="alert">
										    (555) 555 -5555<a href="#" class="close">&times;</a>
									  	</div>
								  	</div>
								  	<div class="col-lg-2 col-md-4 col-sm-6 col-xs-6 selected-phone-item">
										<div class="alert">
										    (555) 555 -5555<a href="#" class="close">&times;</a>
									  	</div>
								  	</div>
								  	<div class="col-lg-2 col-md-4 col-sm-6 col-xs-6 selected-phone-item">
										<div class="alert">
										    (555) 555 -5555<a href="#" class="close">&times;</a>
									  	</div>
								  	</div>
								  	<div class="col-lg-2 col-md-4 col-sm-6 col-xs-6 selected-phone-item">
										<div class="alert">
										    (555) 555 -5555<a href="#" class="close">&times;</a>
									  	</div>
								  	</div>
								  	<div class="col-lg-2 col-md-4 col-sm-6 col-xs-6 selected-phone-item">
										<div class="alert">
										    (555) 555 -5555<a href="#" class="close">&times;</a>
									  	</div>
								  	</div>
								  	<div class="col-lg-2 col-md-4 col-sm-6 col-xs-6 selected-phone-item">
										<div class="alert">
										    (555) 555 -5555<a href="#" class="close">&times;</a>
									  	</div>
								  	</div>
								</div>
							</div>
						</div>
				    </div>
				    <div role="tabpanel" class="tab-pane" id="refer_via_social">
				    	<div class="row">
							<div class="col-lg-8 col-md-7 col-sm-6">
								<div class="social">
									<a href="##"><i class="fa fa-facebook-square"></i></a>
									<a href="##"><i class="fa fa-twitter-square"></i></a>
									<a href="##"><i class="fa fa-google-plus-square"></i></a>
									<a href="##"><i class="fa fa-linkedin-square"></i></a>
									<a href="##"><i class="fa fa-pinterest-square"></i></a>
									<a href="##"><i class="fa fa-instagram"></i></a>
								</div>
								
							</div>
							<div class="col-lg-4 col-md-5 col-sm-6">
								<div class="input-group input-group-custom">
							      <input type="text" class="form-control input-medium" placeholder="Search for...">
							      <span class="input-group-btn input-group-btn">
							        <button class="btn btn-medium btn-success-custom" type="button">Search</button>
							      </span>
							    </div><!-- /input-group -->
							</div>
						</div>
						<div class="friends-list">
							<div class="row">
								<div class="col-sm-6 col-md-4 col-lg-3">
									<div class="friend-list-item">
										<input type="checkbox" /> <img src="../images/friend.png" alt="" />
										<label for="">Jessie Jones</label>
									</div>		
								</div>
								<div class="col-sm-6 col-md-4 col-lg-3">
									<div class="friend-list-item">
										<input type="checkbox" /> <img src="../images/friend.png" alt="" />
										<label for="">Jessie Jones</label>
									</div>	
								</div>
								<div class="col-sm-6 col-md-4 col-lg-3">
									<div class="friend-list-item">
										<input type="checkbox" /> <img src="../images/friend.png" alt="" />
										<label for="">Jessie Jones</label>
									</div>	
								</div>
								<div class="col-sm-6 col-md-4 col-lg-3">
									<div class="friend-list-item">
										<input type="checkbox" /> <img src="../images/friend.png" alt="" />
										<label for="">Jessie Jones</label>
									</div>	
								</div>
								<div class="col-sm-6 col-md-4 col-lg-3">
									<div class="friend-list-item">
										<input type="checkbox" /> <img src="../images/friend.png" alt="" />
										<label for="">Jessie Jones</label>
									</div>		
								</div>
								<div class="col-sm-6 col-md-4 col-lg-3">
									<div class="friend-list-item">
										<input type="checkbox" /> <img src="../images/friend.png" alt="" />
										<label for="">Jessie Jones</label>
									</div>		
								</div>
								<div class="col-sm-6 col-md-4 col-lg-3">
									<div class="friend-list-item">
										<input type="checkbox" /> <img src="../images/friend.png" alt="" />
										<label for="">Jessie Jones</label>
									</div>		
								</div>
								<div class="col-sm-6 col-md-4 col-lg-3">
									<div class="friend-list-item">
										<input type="checkbox" /> <img src="../images/friend.png" alt="" />
										<label for="">Jessie Jones</label>
									</div>		
								</div>
							</div>
						</div>
						<div class="text-right margin-top-10">
			        		<button type="submit" class="btn btn-medium btn-success-custom">Send Invite</button>
			        	</div>
				    </div>
				  </div>
				
				</div>
				
			</div>	
		</div>
	</section>
</main>

<cfparam name="variables._title" default="Invite Friends - Sire">
<cfinclude template="../views/layouts/main.cfm">

<!--- Page Tours using bootstrp tour - see http://http://bootstraptour.com/api/ --->
<script type="application/javascript">

	<!--- Set up page tour for new users --->
	$(function() {
		
		
		<!---  Check if campaign list is empty and take them to first tour that points to tours --->
			<cfif  Tour EQ "GettingStarted"> 
        		
		
				// Instance the tour
				var tour = new Tour({				  
				 
				  onEnd: function (tour) {window.location = 'invite-friends' },						  
				  <cfif tourrs EQ "true">storage:false,</cfif>
				  steps: [
				 {
					element: "",
					title: "Guided Tours ",
					content: "To navigate within the Guided Tours use the Tour provided Next and Prev buttons. If you get bored or think you have enough information you can always select the End Tour option. You can restart tours anytime under the Learning and Support top menu option",
					backdrop:true,
					orphan: true,	
					onShown: function (tour) {$("button.disabled[data-role='prev']").hide();  },				
					placement:"top"
				  },
				  {
					element: "#My-Profile-Section",
					title: "Your personal details ",
					content: "Who and Where to contact about your account. Billing, Low Balance Warnings, System Updates, etc...",
					backdrop:true,
					orphan: true,					
					placement:"top"
				  },
				  {
					element: "#Organization-Profile-Section",
					title: "Data about your organization",
					content: "Templates with organizational place holders in them will automatically pre-populate with these values if they are set here.",
					backdrop:true,
					backdropContainer: 'body',
					placement:"top"
				  },
				  {
					element: "#Organization-Short-Name-Section",
					title: "Data about your organization",
					content: "Templates with organizational place holders in them will automatically pre-populate with these values if they are set here.<p><b>Good message compliance ALWAYS identifies who sent the message.</b></p>",
					backdrop:true,
					backdropContainer: 'body',
					placement:"top"
				  },
				  {
					element: "#Help-Section",
					title: "HELP",
					content: "All short codes must respond to HELP requests. The text you set here will be used as the default for your account. If you leave it blank the Sire system default will be used instead. You can optionally set custom HELP messages for each individual campaigns.",
					backdrop:true,
					backdropContainer: 'body',
					placement:"top"
				  },
				  {
					element: "#Stop-Section",
					title: "STOP",
					content: "All short codes must respond to STOP requests. The text you set here will be used as the default for your account. If you leave it blank the Sire system default will be used instead. You can optionally set custom Stop messages for each individual campaigns.",
					backdrop:true,
					backdropContainer: 'body',
					placement:"top"
				  },
				  {
					element: "#createCampaignModal",
					title: "Campaign Management",
					content: "",
					backdrop:true,
					placement:"top",
					path: "/session/sire/pages/campaign-manage?tour=gettingstarted&tourrs=true"
				  }
				  
				]});
				
				// Initialize the tour
				tour.init();
				
				// Start the tour
				<cfif tourrs EQ "true">
					tour.start(true);
					tour.goTo(0);				
				<cfelse>
					tour.start();				
				</cfif>
				
			</cfif>
					
				
		
	});


</script>


