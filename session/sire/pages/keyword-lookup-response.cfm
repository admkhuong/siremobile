<cfparam name="keyword" default=""/>
<cfparam name="ssid" default="0"/>
<cfparam name="showPreview" default="0"/>

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addJs("https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js")
        .addCss("https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css")
        .addJs("https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.3/socket.io.js")
        .addCss("../assets/global/plugins/jquery-mcustomscrollbar/jquery.mCustomScrollbar.css", true)
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/jquery-mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)
        .addCss("../assets/layouts/layout4/css/custom3.css", true)
        .addJs("../assets/pages/scripts/sms_response.js")
        .addJs("../assets/pages/scripts/sms_ws.js");
</cfscript>

<cfinclude template="/public/sire/configs/paths.cfm"/>

<cfset EMSFLAG = ''/>
<cfset TEMPLATEID = '' />

<cfinvoke method="GetKeywordDetails" component="session.sire.models.cfc.smschat" returnvariable="RetVarKeyword">
    <cfinvokeargument name="inpKeyword" value="#keyword#"/>
</cfinvoke>

<cfif RetVarKeyword.RXRESULTCODE LT 1>
    <cflocation url="/session/sire/pages/dashboard" addtoken="false"/>
</cfif>

<cfset EMSFLAG = RetVarKeyword.INFO.EMSFLAG/>
<cfset TEMPLATEID = RetVarKeyword.INFO.TEMPLATEID/>

<cfinvoke component="session.sire.models.cfc.smschat" method="getListSessionForView2" returnvariable="RetVarListSession">
    <cfinvokeargument name="inpBatchId" value="#RetVarKeyword.INFO.BATCHID#"/>
</cfinvoke>

<cfif RetVarListSession.RXRESULTCODE LT 1>
    <cflocation url="/session/sire/pages/dashboard" addtoken="false"/>
</cfif>

<cfinvoke method="GetChatSetting" component="session.sire.models.cfc.smschat" returnvariable="RetVarSetting">
</cfinvoke>

<cfif RetVarSetting.RXRESULTCODE LT 1>
    <cflocation url="/session/sire/pages/dashboard" addtoken="false"/>
</cfif>

<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>

<cfoutput>

<input value="#shortCode.SHORTCODE#" id="ShortCode" type="hidden" name="ShortCode"/>
<input value="#keyword#" id="keyword" type="hidden" name="keyword"/>

<!--- <div class="portlet light bordered">
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <h4 class="portlet-heading2 pull-right"><a href="/session/sire/pages/sms-keyword?keyword=#HTMLEditFormat(RetVarSessionInfo.INFO.KEYWORD)#&NPA=#NPA#&NXX=#NXX#">Back to list</a></h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-lg-6">
            <h4 class="portlet-heading2">#HTMLEditFormat(RetVarSessionInfo.INFO.KEYWORD)#: #formatedNumber#</h4>
            <cfif !disabled>
                <div class="row">
                    <div class="col-lg-10 col-md-10">
                        <form id="text-form">
                            <div class="form-group" style="margin-bottom: 0px">
                                <textarea id="TextToSend" class="form-control validate[required]"></textarea>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-10 col-md-10">
                        <div class="pull-right">
                            <h5 id="cp-char-count" class="text-right text-color-gray">Character Count {0}</h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-10 col-md-10">
                        <div class="pull-right">
                            <button type="button" class="btn btn-success btn-success-custom btn-custom_size" id="btn_send">Send</button>
                        </div>
                    </div>
                </div>
                <br/>
            </cfif>
            <cfif RetVarSessionInfo.INFO.SESSIONSTATE NEQ 1>
                <div class="row">
                    <div class="col-lg-10 col-md-10">
                        <div class="alert alert-info">
                            This conversation has been closed!<br/>
                            <cfif RetVarActiveSession.RXRESULTCODE GT 0>
                                You have another active conversation with this customer, please click <a href="/session/sire/pages/sms-response?sessionid=#RetVarActiveSession.SESSIONID#" class="alert-link">here</a> to enter.
                            <cfelse>
                                Please click <a id="make-new-chat" class="alert-link">here</a> to make a new conversation with this customer.
                            </cfif>
                        </div>
                    </div>
                </div>
            </cfif>
        </div>
        <div class="col-md-6 col-lg-6">
            <div class="portlet light" id="wrap-preview">
                <div class="portlet-body">
                    <div class="wrap-preview">
                        <div class="preview-screen">
                            <img class="bg-preview-screen" src="../assets/layouts/layout4/img/preview-phone.png" alt="">
                            <div class="phone-screen">
                                <div class="inner-phone-screen clearfix" id="SMSHistoryScreenArea">
                                    <cfloop array="#RetVarResponse.RESPONSE#" index="response">
                                        <cfif response.TYPE EQ 2>
                                            <cfset cssClass = "bubble me"/>
                                        <cfelse>
                                            <cfset cssClass = "bubble guess"/>
                                        </cfif>
                                        <div class="#cssClass#">
                                            #HTMLEditFormat(response.MSG)#
                                        </div>
                                        <div style="clear:both"></div>
                                    </cfloop>
                                </div>
                                <div class="type-screen">
                                    <textarea id="SMSTextInputArea" name="SMSTextInputArea" maxlength="160" placeholder="Text Message"></textarea>
                                    <button id="SMSSend" type="button" class="btn-send-area"></button>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> --->

<div class="uk-grid uk-grid-small uk-grid-match" uk-grid>
    <div class="uk-width-1-4@m uk-width-2-5@s" id="list-section">
        <div class="col-match-bg">
            <div class="left-sky">
                <div class="head">
                    <div class="uk-clearfix show-on-phone" style="margin-bottom: 5px">
                        <h4 class="title uk-float-left">
                            <b>
                                <cfif EMSFLAG EQ 20>
                                    <a href="/session/sire/pages/sms-chat">SMS Chat</a> - #keyword#
                                <cfelse>
                                    <a href="/session/sire/pages/campaign-reports?campaignid=#RetVarKeyword.INFO.BATCHID#">Feedback Manager</a> - #keyword#
                                </cfif>
                            </b>
                        </h4>
                    </div>
                    <div class="uk-grid uk-grid-small">
                        <div class="uk-width-4-5">
                            <div class="form-group form-gd form-search">
                                <i class="fa fa-search"></i>
                                <input type="search" id="search" class="form-control" placeholder="Phone" />
                            </div>
                        </div>
                        <div class="uk-width-expand">
                            <div class="type-filter">
                                <a class="val-type" id="filter-parent"><span>All</span> <i class="fa fa-caret-down"></i></a>
                                <div uk-drop="mode: click">
                                    <div class="uk-card uk-card-body uk-card-default">
                                        <ul class="uk-list pick-type">
                                            <li class="filter-child" data-state="0"><a>All</a></li>
                                            <li class="filter-child" data-state="1"><a>Unread</a></li>
                                            <li class="filter-child" data-state="2"><a>Read</a></li>
                                            <li class="filter-child" data-state="101"><a>Open</a></li>
                                            <li class="filter-child" data-state="102"><a>Closed</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="wrap-new-curent" class="body">
                    <div class="first-new" style="display:none">
                        <span class="title">NEW</span>
                        <cfset pos = []/>
                        <ul class="uk-list" id="new-list">
                            <cfloop array="#RetVarListSession.aaData#" index="index">
                                <cfif (index.NewMessage GT 0 AND index.SessionState_int EQ 1) OR index.MessageUnread_int GT 0>
                                    <li class="chat-session" data-id="#index.SessionId_bi#" data-state="#index.SessionState_int#" data-phone="#index.ContactString_vch#"><a><b>#index.ContactString_vch#</b></a> <span>#(index.MessageUnread_int GT 0 ? index.MessageUnread_int : '')#</span></li>
                                    <cfset arrayAppend(pos, index.SessionId_bi)/>
                                </cfif>
                            </cfloop>
                        </ul>
                    </div>
                    <div class="second-current" style="display:none">
                        <span class="title">OPEN</span>
                        
                        <div id="list-number">
                            <ul class="uk-list" id="current-list">
                                <!--- <cfloop array="#RetVarListSession.aaData#" index="index">
                                    <cfif !arrayContains(pos, index.SessionId_bi)>
                                        <li class="chat-session" data-id="#index.SessionId_bi#" data-state="#index.SessionState_int#" data-phone="#index.ContactString_vch#"><a class="#index.SessionState_int NEQ 1 ? 'text-color-gray text-italic' : ''#">#index.ContactString_vch#</a></li>
                                    </cfif>
                                </cfloop> --->
                            </ul>
                        </div>
                    </div>
                    <div class="third-close" style="display:none">
                        <span class="title">CLOSED</span>
                        
                        <div id="list-number">
                            <ul class="uk-list" id="close-list">
                            </ul>
                        </div>
                    </div>
                    <div class="forth-preview" style="display:none">
                        <span class="title">Preview</span>
                        
                        <div id="list-number">
                            <ul class="uk-list" id="preview-list">
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="uk-width-3-4@m uk-width-3-5@s hide-on-phone" id="message-section">
        <div class="col-match-bg">
            <div class="right-sky">
                <div class="head">
                    <div class="uk-clearfix">
                        <h4 class="title uk-float-left hide-on-phone">
                            <cfif EMSFLAG EQ 20>
                                <a href="/session/sire/pages/sms-chat">SMS Chat</a> - #keyword#
                            <cfelse>
                                <a href="/session/sire/pages/campaign-reports?campaignid=#RetVarKeyword.INFO.BATCHID#">Feedback Manager</a> - #keyword#
                            </cfif>
                        </h4>
                        <h4 class="title uk-float-left show-on-phone"><a id="back-to-list"><i class="fa fa-chevron-left"></i> <span id="phone-number"></span></a></h4>
                        <span class="uk-float-right">
                            <a href="javascript:;" class="btn btn-sm new-blue-gd" id="btn-end-chat" style="display:none;margin-right:15px">End Chat</a>
                            <a class="change-screen" id="change-view" title="Change default view"><i class="fa fa-exchange"></i></a>
                        </span>
                    </div>
                </div>
                <cfif RetVarSetting.DefaultView EQ 1>
                    <div class="body">
                        <div class="wrap-preview">
                            <div class="preview-screen">
                                <img class="bg-preview-screen" src="../assets/layouts/layout4/img/preview-phone.png" alt="">
                                <div class="phone-screen">
                                    <div class="inner-phone-screen clearfix" id="SMSHistoryScreenArea">

                                    </div>
                                    <div class="type-screen">
                                        <form id="text-form">
                                            <textarea id="SMSTextInputArea" class="validate[required]" name="SMSTextInputArea" maxlength="160" placeholder="Text Message" disabled></textarea>
                                        </form>
                                        <button id="SMSSend" type="button" class="btn-send-area"></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="part-type-mess">
                            <span class="custom-checkbox">
                                <input type="checkbox" id="enter-to-send" value="1" #RetVarSetting.EnterToSend EQ 1 ? "Checked" : ""#>
                                <label for="enter-to-send" style="margin-right: 25px">
                                    Enter to send
                                </label>
                            </span>
                        </div>
                    </div>
                <cfelse>
                    <div class="body">
                        <div class="screen-non-ip" id="SMSHistoryScreenArea">
                            
                        </div>
                        <div class="part-type-mess">
                            <div class="uk-grid uk-grid-small">
                                <div class="uk-width-expand">
                                    <form id="text-form">
                                        <div class="form-group form-gd">
                                            <!--- <input id="SMSTextInputArea" type="text" class="form-control validate[required]" placeholder="Type your message here"/> --->
                                            <textarea style="height: 34px; resize: vertical;" name="" id="SMSTextInputArea" class="form-control validate[required]" placeholder="Type your message here" disabled></textarea>
                                        </div>
                                    </form>
                                </div>
                                <div class="uk-width-auto">
                                    <button id="SMSSend" type="submit" class="btn green-gd">Send</button>
                                </div>
                            </div>
                            <span class="custom-checkbox">
                                <input type="checkbox" id="enter-to-send" value="1" #RetVarSetting.EnterToSend EQ 1 ? "Checked" : ""#>
                                <label for="enter-to-send" style="margin-right: 25px">
                                    Enter to send
                                </label>
                            </span>
                        </div>
                    </div>
                </cfif>
                <div class="footer view-old" style="display: none">
                    <div class="uk-clearfix">
                        <a id="search-old">View all conversations with this Phone number</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--- User socket channel --->
<cfset us_chn = left(hash("#EnvPrefix#"&"SIRE_CHAT_USER_"&"#session.UserId#"), 6)/>

<script type="text/javascript">
    var keyword = "#keyword#";
    var ss_chn = "";
    var us_chn = "#us_chn#";
    var default_view = #RetVarSetting.DefaultView#;
    var enter_to_send = #RetVarSetting.EnterToSend#;
    var batchid = #RetVarKeyword.INFO.BATCHID#;
    var currState = 0;
    var ssid = "#ssid#";
    var showPreview = #showPreview#;
</script>

<style type="text/css" media="screen">
    .alert-link {
        text-decoration: underline;
    }

    @media screen and (max-width: 450px) {
        .page-header, .page-footer {
            display: none;
        }
        .left-sky .body {
            height: 80vh;
        }
        .right-sky .body .screen-non-ip {
            height: 70vh;
        }
        .notice-title {
            display: none;
        }
        .show-on-phone {
            display: inline;
        }
        .hide-on-phone {
            display: none;
        }
    }

    @media screen and (min-width: 451px) {
        .show-on-phone {
            display: none;
        }
        .hide-on-phone {
            display: inline;
        }
    }
</style>

</cfoutput>

<cfparam name="variables._title" default="SMS Responses - Sire"> 

<cfinclude template="../views/layouts/master.cfm">