<!---
  Created by longtran on 04/07/2017.
--->
<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
    .addJs("../assets/pages/scripts/keyword_lookup.js")
    .addCss("../assets/layouts/layout4/css/custom3.css");
</cfscript>

<cfinclude template="/public/sire/configs/paths.cfm"/>

<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>

<cfoutput>
    <input type="hidden" name="shortcode" id="ShortCode" value="#shortcode.SHORTCODE#"/>
</cfoutput>

<div class="portlet light bordered">
    <div class="row">
        <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
            <h4 class="portlet-heading2">Keyword Lookup</h4>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding-right: 0px">
            <input type="search" class="form-control" id="searchbox" placeholder="Keyword"/>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 box-filter" style="padding-left: 0px" ">
            <button class="btn-apply-filter btn-action" id="btnKeyWordLookUp"><i class="glyphicon glyphicon-search"></i></button>
        </div>
    </div>
    <div class="portlet light hidden" id="refreshAlert" style="padding-bottom: 0px;margin-bottom: 0px">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12">
                <div class="alert alert-info">
                    <span id="alertMessage"></span> Click <a class="alert-link" id="refreshTable">here</a> to update.
                </div>
            </div>
        </div>
    </div>
    <div class="" style="margin-top: 10px">
        <div class="row">
            <div class="col-xs-12">
                <div class="re-table">
                    <div class="table-responsive">
                        <table id="keywordlist" class="table table-striped table-responsive">

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--- MODAL POPUP PREVIEW--->
    <div class="bootbox modal fade" id="previewCampaignModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Preview Campaign</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <!--- <button type="button" class="btn btn-primary btn-success-custom" id="btn-rename-campaign">Save</button> --->
                    <button type="button" class="btn btn-default btn-back-custom" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</div>


<style type="text/css">
    @media (max-width: 767px) {
        .div-keyword {
            margin-bottom: 10px;
        }
    }

    @media (max-width: 768px) {
        .new-keyword {
            margin-top: 10px;
        }
    }
</style>

<!--- User socket channel --->
<cfset us_chn = left(hash("#EnvPrefix#" & "SIRE_CHAT_USER_" & "#session.UserId#"), 6)/>

<cfoutput>
    <script type="text/javascript">
        var us_chn = "#us_chn#";
    </script>
</cfoutput>

<cfparam name="variables._title" default="Keyword Lookup - Sire">
<cfinclude template="../views/layouts/master.cfm">



