<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
    	.addCss("../assets/global/plugins/daterangepicker/daterangepicker.css", true)
        .addCss("/session/sire/assets/global/plugins/Chartist/chartist.min.css", true)
        .addCss("/session/sire/assets/global/plugins/Chartist/plugins/tooltips/chartist-plugin-tooltip.css", true)
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)
    	.addJs("../assets/global/plugins/daterangepicker/moment.min.js", true)
        .addJs("../assets/global/plugins/daterangepicker/daterangepicker.js", true)
        .addJs("/session/sire/assets/global/plugins/Chartist/chartist.min.js", true)
        .addJs("/session/sire/assets/global/plugins/Chartist/plugins/tooltips/chartist-plugin-tooltip.js", true)
        .addJs("/public/sire/js/select2.min.js")
        .addJs("../assets/pages/scripts/keyword-list.js");

</cfscript>


<!--- remove old method: 
<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>								
--->
<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>

<!--- <cfdump var="#keywordList.LISTKEYWORD#" abort="true"> --->
<div class="portlet light bordered keyword">
    <div class="portlet-body form">
        <h2 class="page-title">Keywords</h2>
        <div class="options">
	        <div class="row">
	        	<div class="col-xs-12 col-sm-3">
	        		<div class="select-list">
	        			<p><b>Selected List</b></p>
		                <select class="form-control Select2" id="keyword-list-select">
                            <!--- Insert keyword list through js --->
		                </select>
		            </div>
	        	</div>
	        	<div class="col-xs-12 col-sm-6 col-sm-offset-3">
	        		<div class="date-range">
	        			<p><b>Selected Date Range</b></p>
						<div class="reportrange pull-right">
						    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
						    <span></span> <b class="caret"></b>
                            <input type="hidden" name="dateStart" value="">
                            <input type="hidden" name="dateEnd" value="">
						</div>
	        		</div>
	        	</div>
	        </div>
        </div>
        <!--- options select --->
        <div id="highchart" class="keyword-chart" data-highcharts-chart="0">

        </div>
        <!--- charts --->
        <div class="row">
        	<div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
		        <div class="keyword-list">
        	        <div class="re-table">
                        <div class="table-responsive">
                            <table id="keywordListDetail" class="table table-bordered table-striped">
                                
                            </table>
                        </div>
        	        </div>
		        </div>
        	</div>
        </div>
    </div>
</div>

<cfparam name="variables._title" default="Keyword Report - Sire">

<cfinclude template="../views/layouts/master.cfm">