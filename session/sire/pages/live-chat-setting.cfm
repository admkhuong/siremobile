<cfinclude template="../../sire/configs/paths.cfm">

<!--- Check admin permission --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<!--- <cfdump var="#form#" abort="true"> --->
<!--- <cfdump var="#liveChat#" abort="true"> --->
<cfif structKeyExists(form,"action") AND  form.action eq 'update'>
	<cfinvoke component="session.sire.models.cfc.setting" method="setByName">
		<cfinvokeargument name="inpName" value="liveChatOnOff">
		<cfinvokeargument name="inpValue" value="#form.VariableName#">
	</cfinvoke> 
</cfif>


<cfinvoke component="session.sire.models.cfc.setting" method="getByName" returnvariable="liveChat">
	<cfinvokeargument name="inpName" value="liveChatOnOff">
</cfinvoke> 

<div class="portlet light bordered ">
	<div class="portlet-body">
		<h4 class="portlet-heading">Settings</h4>

			<div class="content-body">
				<div class="container-fluid">
					<form action="" method="POST">
						<input type="hidden" name="action" value="update">
						<div class="row">
							<div class="col-md-2 col-lg-2 col-sm-3 col-xs-5">
								<div class="row">
									<p class="portlet-subheading text-color-gray">
										Live Chat
									</p>
								</div>
							</div>
							<div class="col-md-2 col-lg-2 col-sm-3 col-xs-5">
								<div class="form-group">
									<select name="VariableName" class="form-control">
										<option <cfif liveChat.liveChatOnOff eq 'On'>
											selected
										</cfif> value="On">On</option>
										<option <cfif liveChat.liveChatOnOff eq 'Off'>
											selected
										</cfif>  value="Off">Off</option>	
									</select>
								</div>
							</div>
							<div class="col-md-8 col-lg-8 col-sm-6 col-xs-2">
							</div>
						</div>
						<div class="row">
							<div class="col-md-2 col-lg-2 col-sm-3 col-xs-5">
								<div class="row">
									<button class="btn btn-success-custom" style="submit">Save</button>
								</div>
							</div>
							<div class="col-md-2 col-lg-2 col-sm-3 col-xs-5">
								<div class="form-group"></div>
							<div class="col-md-8 col-lg-8 col-sm-6 col-xs-2">
								
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<cfparam name="variables._title" default="Admin settings - Sire">
<cfinclude template="../views/layouts/master.cfm">
