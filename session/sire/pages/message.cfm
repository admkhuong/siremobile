<div class="portlet light bordered">
	<div class="portlet-body">
		<div class="new-inner-body-portlet">
			<div class="form-gd">
				<div class="row">
					<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
						<h4 class="portlet-heading2">Messages</h4>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div class="inner-addon left-addon">
							<i class="glyphicon glyphicon-search"></i>
							<input type="search" class="form-control" id="searchbox" placeholder="Search Campaign Name">
						</div>
					</div>
				</div>
				<div class="form-body" style="margin-top: 10px">
					<div class="re-table">
						<div class="table-responsive">
							<table id="user-message-list" class="table table-striped table-bordered table-hover dataTable">

							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<cfset variables.menuToUse = 2>

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
    	.addCss("../assets/layouts/layout4/css/custom3.css") 
        .addJs("../assets/pages/scripts/message.js") ;
</cfscript>

<cfparam name="variables._title" default="Messages - Sire">
<cfinclude template="../views/layouts/master.cfm">