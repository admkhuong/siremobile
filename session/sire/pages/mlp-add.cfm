<cfparam name="templateid" default="">

<cfparam name="action" default="">

<cfinvoke component="session.sire.models.cfc.mlp" method="ReadCPPTemplate" returnvariable="RetCPPXData">
	 <cfinvokeargument name="ccpxDataId" value="#templateid#">
</cfinvoke> 

<!---<cfdump var="#RetCPPXData#" >--->
<cfif RetCPPXData.RXRESULTCODE GT 0>

	<cfinvoke component="session.sire.models.cfc.mlp" method="ParseCPPXXmlData" returnvariable="xmlControl">
		<cfinvokeargument name="xmlData" value="#RetCPPXData.DATA#">
	</cfinvoke> 	

	<cfset termOfService = ''>
	<cfset checkMultiSubList = 0>

	<cfloop array="#xmlControl#" index="indexXml">
		<cfif structKeyExists(indexXml, "term-of-service")>
			<cfset termOfService = indexXml['term-of-service']>
		</cfif>
		<cfif structKeyExists(indexXml, "multi-sub-list")>
			<cfset checkMultiSubList = indexXml['multi-sub-list']>
		</cfif>	
	</cfloop>

<cfelse>
	<!--- Redirect to list page --->
	<cflocation url="/session/sire/pages/mlp-list" addtoken="false">		
</cfif>



<cfset objectList = {}>
<cfset objectList['HTML']  = {'LABEL':'Text Object','CONTENT':'<textarea class="normal textHtml cpp-object" name="text-content">%VALUE%</textarea>'}>
<cfset objectList['SMS']   = {'LABEL':'SMS Number Object','CONTENT':'<input type="text" class="form-control sms-number cpp-object" name="sms-number" value="%VALUE%" placeholder="SMS Number" maxlength="255">'}>
<cfset objectList['EMAIL'] = {'LABEL':'Email address Object','CONTENT':'<input type="text" class="form-control email-address cpp-object" name="email-address" value="%VALUE%" placeholder="Email Address" maxlength="255">'}>
<cfset objectList['VOICE'] = {'LABEL':'Voice Number Object','CONTENT':'<input type="text" class="form-control voice-number cpp-object" name="voice-number" value="%VALUE%" placeholder="Voice Number" maxlength="255">'}>
<cfset objectList['SUBCRIBERLIST'] = {'LABEL':'Subscriber List Object','CONTENT':'<div class="cpp-object subcriber-list-group"><div class="row"><div class="col-sm-6"><input type="text" name="subcriber-name" class="form-control subcriber-name" value="%VALUE%" placeholder="Subcriber Name" maxlength="255"></div><div class="col-sm-6"><select class="form-control subcriber-list validate[required]" name="subcriber-list" data-pre_val="%LISTID%"></select></div></div></div>
'}>

<main class="container-fluid page my-plan-page">
	<cfinclude template="../views/commons/credits_available.cfm">
		<section class="row bg-white">
			<div class="content-header">
				<div class="row">
					<div class="col-sm-5 content-title">
                    	<cfinclude template="../views/commons/welcome.cfm">
                	</div>
				</div>	
			</div>
			<!--- END : content-header --->
			<hr class="hrt0">
			<div class="content-body">
			<div class="container-fluid">
                <div class="row heading">
                	<div class="col-sm-12 heading-title">Design a Marketing Landing Portal <hr></div>
                </div>


                <div class="row">
                	<form id="edit_cpp_frm"  autocomplete="off">
                		<cfoutput>
                		<input type="hidden" name="action" value="template" id="action">	
						 <!--- Display term of service --->
						 <div class="objectItem clearfix objectItemTerm">
							<div class="col-md-6 form-horizontal">
								<div class="form-group">
							    <label for="card_number" class="col-sm-4 control-label">Name:<span class="text-danger">*</span></label>
							    <div class="col-sm-8">
							      <input type="text" class="form-control validate[required,ajax[ajaxCPPNameAvailable]]" maxlength="255" id="cppxName" name="cppxName" value='' data-prompt-position="topLeft:100">
							    </div>
						  	</div>

						   	<div class="form-group">
							    <label for="card_number" class="col-sm-4 control-label">Custom URL:</label>
							    <div class="col-sm-8">
							      <input type="text" class="form-control validate[ajax[ajaxCPPUrlAvailable],custom[checkCustomURL]]" maxlength="255" id="cppxURL" name="cppxURL" value='' data-prompt-position="topLeft:100">
							    </div>
						  	</div>

							<div class="checkbox">
							    <label>
							      <input type="checkbox" name='checkTerms' id="checkTerms" value='1' <cfif termOfService NEQ ''> checked</cfif> >Require customers need to accept your terms of service ?
							    </label>
							</div>
							</div>	
							<div class="col-md-6">
			                	<div class="form-group wrapper-terms_of_service" style=<cfif termOfService EQ ''>"display:none"</cfif>>
				                	<textarea name="term-of-service" id="term-of-service" class="normal cpp-object terms_of_service">#termOfService#</textarea>
				                </div>	
							</div>
		                </div>	

		                <ul class="col-lg-10 col-lg-offset-1 sortable" id="listObj">
		                	<!---<cfif action EQ 'Create'>
			                	<li class="groupObjectItem" id="obj_1">
				                	<div class="btn-group pull-right">
					               		<button type="button" class="btn btn-medium btn-success-custom btn-modal-add-obj pull-left">Add New Object</button>	
					               	</div>	
					            </li>
					        </cfif>--->
					        
					        <cfset num = 1>
					        <cfset num_sub_list = 0>       	
				            <cfloop array="#xmlControl#" index="index">
				            	<cfset htmlId = "obj_"&NOW().getTime()&"_"&num>
				            	<cfif structKeyExists(index, "text-content")>
				            		<cfset content = replace( objectList['HTML']['CONTENT'],"%VALUE%", index['text-content'])>
				            		<li class="groupObjectItem" id="#htmlId#">
					               		<!---<div class="btn-group pull-right">
					               			<button type="button" class="btn btn-medium btn-success-custom btn-modal-add-obj pull-left">Add New Object</button>	
					               		</div>--->	
					               		<div class="clearfix"></div>
					               		<div class="objectItem clearfix">
					               			<div class="col-sm-12">
			    								<label class="control-point-label"> #objectList['HTML']['LABEL']#  </label>
			    								<!---<button type="button" class="btn btn-link pull-right cpp-object-btn-delete">
			    									<img src="/session/sire/images/close.png">
												</button>--->
			    							</div>
			    							<div class="col-sm-12 content-area">
			    								#content#
			    							</div>	
					               		</div>
					               	</li>
				            	</cfif>

				            	<cfif structKeyExists(index, "sms-number")>
				            		<cfset content = replace( objectList['SMS']['CONTENT'],"%VALUE%", index['sms-number'])>
				            		<li class="groupObjectItem" id="#htmlId#">
					               		<!---<div class="btn-group pull-right">
					               			<button type="button" class="btn btn-medium btn-success-custom btn-modal-add-obj pull-left">Add New Object</button>	
					               		</div>--->	
					               		<div class="clearfix"></div>
					               		<div class="objectItem clearfix">
					               			<div class="col-sm-12">
			    								<label class="control-point-label"> #objectList['SMS']['LABEL']#  </label>
			    								<!---<button type="button" class="btn btn-link pull-right cpp-object-btn-delete">
			    									<img src="/session/sire/images/close.png">
												</button>--->
			    							</div>
			    							<div class="col-sm-12 content-area">
			    								#content#
			    							</div>	
					               		</div>
					               	</li>
				            	</cfif>

				            	<cfif structKeyExists(index, "email-address")>
				            		<cfset content = replace( objectList['EMAIL']['CONTENT'],"%VALUE%", index['email-address'])>
				            		<li class="groupObjectItem" id="#htmlId#">
					               		<!---<div class="btn-group pull-right">
					               			<button type="button" class="btn btn-medium btn-success-custom btn-modal-add-obj pull-left">Add New Object</button>	
					               		</div>	--->
					               		<div class="clearfix"></div>
					               		<div class="objectItem clearfix">
					               			<div class="col-sm-12">
			    								<label class="control-point-label"> #objectList['EMAIL']['LABEL']#  </label>
			    								<!---<button type="button" class="btn btn-link pull-right cpp-object-btn-delete">
			    									<img src="/session/sire/images/close.png">
												</button>--->
			    							</div>
			    							<div class="col-sm-12 content-area">
			    								#content#
			    							</div>	
					               		</div>
					               	</li>
				            	</cfif>

				            	<cfif structKeyExists(index, "voice-number")>
				            		<cfset content = replace( objectList['VOICE']['CONTENT'],"%VALUE%", index['voice-number'])>
				            		<li class="groupObjectItem" id="#htmlId#">
					               		<!---<div class="btn-group pull-right">
					               			<button type="button" class="btn btn-medium btn-success-custom btn-modal-add-obj pull-left">Add New Object</button>	
					               		</div>	--->
					               		<div class="clearfix"></div>
					               		<div class="objectItem clearfix">
					               			<div class="col-sm-12">
			    								<label class="control-point-label"> #objectList['VOICE']['LABEL']#  </label>
			    								<!---<button type="button" class="btn btn-link pull-right cpp-object-btn-delete">
			    									<img src="/session/sire/images/close.png">
												</button>--->
			    							</div>
			    							<div class="col-sm-12 content-area">
			    								#content#
			    							</div>	
					               		</div>
					               	</li>
				            	</cfif>

				          		<cfif structKeyExists(index, "subcriber-list-group")>
				          			<cfset num_sub_list ++ >
				            		<cfset content = replace( objectList['SUBCRIBERLIST']['CONTENT'],"%VALUE%", index['subcriber-list-group']['NAME'])>
				            		<cfset content  = replace( content,"%LISTID%", index['subcriber-list-group']['LISTID'])>
				            		<li class="groupObjectItem" id="#htmlId#">
					               		<!---<div class="btn-group pull-right">
					               			<button type="button" class="btn btn-medium btn-success-custom btn-modal-add-obj pull-left">Add New Object</button>	
					               		</div>	--->
					               		<div class="clearfix"></div>
					               		<div class="objectItem clearfix">
					               			<div class="col-sm-12">
			    								<label class="control-point-label"> Subscriber List Object  </label>
			    								<!---<button type="button" class="btn btn-link pull-right cpp-object-btn-delete" data-control-point-id="1" data-control-point-type="SHORTANSWER" data-control-point-af="NOFORMAT" data-control-point-oig="0" data-control-point-scdf="" data-control-point-one-selection-no-branch="0">
			    									<img src="/session/sire/images/close.png">
												</button>--->
			    							</div>
			    							<div class="col-sm-12 content-area">
			    								#content#
			    							</div>	
					               		</div>
					               	</li>
				            	</cfif>

				            	<cfset num ++>
				            </cfloop>

				            <!---<cfif action EQ 'Edit'>
			                	<li class="groupObjectItem" id="obj_1">
				                	<div class="btn-group pull-right">
					               		<button type="button" class="btn btn-medium btn-success-custom btn-modal-add-obj pull-left">Add New Object</button>	
					               	</div>
					               	<div class="clearfix"></div>	
					            </li>
					        </cfif>--->
					      
			               	<li class="groupObjectItem checkMultiSubList"  style="<cfif num_sub_list LT 2> display:none </cfif>">
				           		<div class="checkbox">
								    <label>
								      <input type="checkbox" class="cpp-object multi_subcriber_list" name="multi_subcriber_list" id="multi_subcriber_list" <cfif checkMultiSubList EQ '1'> checked</cfif> > Enable multiple subcriberlist selection.
								    </label>
								</div>
					        </li>
		               </ul>
		                <div class="clearfix"></div>

		                <div class="col-md-12">
		                	<hr class="hr10">
		                </div>
		                
		                <div class="col-sm-12 heading-title">Custom CSS<hr></div>
	                 	<div class="col-md-12">
		                		<div class="radio-inline">
								 <label>
								    <input type="radio" name="optionsRadios" class="select_used_css" id="optionsRadios1" value="1" checked>
								    	Upload CSS
								  </label>
								</div>
								<div class="radio-inline">
								  	<label>
								    	<input type="radio" name="optionsRadios" class="select_used_css" id="optionsRadios2" value="2">
								    	CSS Link
								  	</label>
								</div>
								
								<div>
									
									<div id="wrap_css_custome">
										<cfset display_css = 'display:none'>
										<div class="logo-btn-group">	
										<button class="btn btn-success-custom" id="cssUpload"><strong>Upload CSS</strong></button>
										<a href="" id="cssFile" style="<cfoutput>#display_css#</cfoutput>" target="_blank">Css Link</a>
										<button style="<cfoutput>#display_css#</cfoutput>" class="btn btn-link" id="removeLink">Remove Link</button>
										</div>
										<input name="cssCustom" id="cssCustom" type="hidden" value="" />
									</div>
									
									<div id="wrap_css_extenal_link">
										<div class="form-group">
									    <label for="extenal_link" class="col-sm-2 control-label">External Link CSS</label>
									    <div class="col-sm-6">
									    	<input type="text" class="form-control validate[custom[url]]" id="cssExternalLink" name="cssExternalLink" maxlength="255" data-prompt-position="topLeft:100"
									    	value="">
									    </div>
										</div>
									</div>
								</div>
		               </div>
			        	<div class="form-footer text-center pull-right">
			        		<button type="submit" class="btn btn-success-custom btn-save-cpp pull-left">Save</button>
			        		<a href="##" class="btn btn-success-custom btn-preview-add-cpp pull-left" target="_blank">Preview</a>
			        		<button type="button" class="btn btn-success-custom btn-embed-cpp pull-left disabled" data-cpp-url="##" data-cpp-uuid="##">Embed Code</button>
			        		<a href="/session/sire/pages/mlp-list" type="button" class="btn btn-success-custom btn-create-cpp pull-left">Back to list</a>

			        		<!---
			            	<a type="button" class="btn btn-medium btn-back-custom pull-left"  href="javascript:history.go(-1)">Back</a>
			            	--->
			        	</div>
			        </cfoutput>	
	                </form>		
	            </div>    
            </div>
            </div>    
            <!--- END CONTENT BODY --->
		</section>	
</main>


<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/js/tinymce_4.3.8/tinymce.min.js">
</cfinvoke>


<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/js/jquery-ui-1.11.4.custom/jquery-ui.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/public/js/jquery-ui-1.11.4.custom/jquery-ui.theme.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/mlp.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/ajaxupload.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/site/mlp.js">
</cfinvoke>

<cfparam name="variables._title" default="Marketing Landing Portals - Sire">

<script type="text/javascript">
	var objectList = <cfoutput>#serializeJSON(objectList)#</cfoutput>
	var ccpxDataId = 0
</script>

<cfinclude template="../views/layouts/main.cfm">