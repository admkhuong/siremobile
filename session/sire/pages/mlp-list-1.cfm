<cfinclude template="../../sire/configs/paths.cfm">

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/js/site/mlp_list.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/mlp.css">
</cfinvoke>

<cfinvoke component="session.sire.models.cfc.order_plan" method="GetUserPlan" returnvariable="userPlanInfo" />

<style>
	
	.btn-report, .btn-clone 
	{
	    margin-right: 5px;
	}
	
	.btn-actions
	{
		margin-bottom: 1em;
		min-width: 8em;
		
	}
	

	.Frame-Wrapper { width: 100%; height: 100%; padding: 0; overflow: overflow-y:auto; overflow-x:scroll;;-webkit-overflow-scrolling: touch; }
	.scaled-frame { width:100%; height:100%; min-width: 890px; min-height: 600px; border: 0px; overflow-y:auto; overflow-x:scroll; }
	.scaled-frame {
	    zoom: 1;
	    -moz-transform: scale(1);
	    -moz-transform-origin: 0 0;
	    -o-transform: scale(1);
	    -o-transform-origin: 0 0;
	    -webkit-transform: scale(1);
	    -webkit-transform-origin: 0 0;
	}
	
	@media screen and (-webkit-min-device-pixel-ratio:0) {
	 .scaled-frame  { zoom: 1;  }
	}
 	
 	iframe img {
 		width: 100%;
 	}

	
</style>

<main class="container-fluid page">
	<cfinclude template="../views/commons/credits_available.cfm">
	<section class="row bg-white">
		
				
			<div class="content-body "id="MainCampaignConsole" style="margin-top:1em;" >
				<div class="col-md-12" Id="">
					<div class="row heading">
						<div class="col-sm-8">  
							<div class="heading-title">Marketing Landing Pages</div>
							<h4>Full creative freedom, no tech bottlenecks, achieve better marketing results.</h4>
							<div>A Marketing Landing Page (MLP) is a standalone web page dedicated to a single product, service or campaign. Start by using one of our mobile first design templates or build from scratch. Customize your page elements on the fly. Coding not required.</div>					
							<div><a data-toggle="modal"  href="#" data-target="#InfoModalMLP">Why would I need a Marketing Landing Pages?</a></div>
													
						</div>
						<div class="col-sm-4 heading-button" style="padding-top:1em;">
							<div class="btn-actions">
								<p><a style="display: inline;" class="btn btn-success-custom add-mlp-button" href="/session/sire/pages/mlp-edit?action=add">New MLP</a></p>
								<p><a class="btn btn-success-custom add-template-button" href="/session/sire/pages/mlp-template-picker">New MLP From Template</a></p>
								<p><a href="/session/sire/pages/image-manage" class="btn btn-success-custom">Manage Images</a></p>
								<p style="padding: 0 10px;" class="warning-text-mlp hidden"><strong class="text-left" style="color: red;">You have reached max <cfoutput>#userPlanInfo.MLPSLIMITNUMBER#</cfoutput> active MLPs</strong></p>
							</div>
						</div>
					</div>
									
					<div class="table-responsive">
			            <table id="tblListEMS" class="table table-striped table-bordered table-hover" style="table-layout:fixed">
			           
			            </table>
			        </div>
			
				</div>
			</div>
	</section>

</main>


<!-- Modal -->
<div id="InfoModalMLP" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Why would I need a Marketing Landing Pages?</h4>
      </div>
      <div class="modal-body">
      	<p></p>
      	
      	<!--- Now, you can create powerful marketing landing pages that drive more conversions. --->
      	<p>A Marketing Landing Page (MLP) is a standalone web page dedicated to a single product, service or campaign.</p>

	  	<p>While your website or homepage focuses on exploration, your landing pages focus on conversion and compliance.</p>

	  	<p>In other words; landing pages guide visitors toward your conversion goal and provide the ideal foundation for your marketing campaigns.</p>

	  	<p>Sometimes you need to make quick copy and design edits and don't have time to wait for the developers to get back to you.</p>
	  	<p>There are several ways to use Marketing Landing Pages:</p>

	  		<uL>
	  			<li><b>Offer</b> - give the user more detail on your offer. Provide instructions for redemption of offer. Coupons, Sweepstakes, Contests, Rewards, etc.</li>
	  			
	  			<li><b>Click-through</b> - Used to persuade visitors to click to another page</li>

	  			<li><b>Lead generation</b> - Designed to capture user data such as names and emails</li>

	  			<li><b>House-holding Data Capture</b> - Designed to capture user additional data to expand details on your existing subscriber lists</li>
	  			
	  			<li><b>Compliance</b> - A place to list out any terms and conditions associated with your offerings.</li>
	  			
	  		</uL>
	      	      	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!--- 
	A landing page is a standalone web page dedicated to a single product, service or campaign.

While your homepage focuses on exploration, your landing pages focus on conversion.

In other words; landing pages guide visitors toward your conversion goal and provide the ideal foundation for your marketing campaigns.

There are 2 basic types of landing page:

Click-through - Used to persuade visitors to click to another page

Lead generation - Designed to capture user data such as names and emails

 --->

<cfparam name="variables._title" default="Marketing Landing Portals - Sire">
<cfinclude template="../views/layouts/main.cfm">


