<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addJs("../assets/pages/scripts/mlp.js");
</cfscript>


<style>
	
.table-responsive {
    padding: 1em;
}
	
</style>


<cfinvoke component="session.sire.models.cfc.order_plan" method="GetUserPlan" returnvariable="userPlanInfo" />

<div class="portlet light bordered mlp">
    <div class="portlet-body form">
        <h2 class="page-title">Marketing Landing Page</h2>
        <div class="row">
        	<div class="col-xs-12 col-sm-7 col-md-6 col-lg-8 detail-simon">
                <p>A Marketing Landing Page (MLP) is a simple, clean dedicated web page designed to immediately push a visitor to a specific action. It is designed to push your customers to a specific goal such as a promotion or coupon.</p>
                <p>Our MLP designer allows you to create your own page without the expensive cost of designers. You don’t need to know coding. Just upload images and add your own text!</p>
                <p><a data-toggle="modal"  href="#" data-target="#InfoModalMLP" style="text-decoration: underline; cursor: pointer;">Why would I need a Marketing Landing Page?</a></p>
        	</div>
        	<div class="col-xs-12 col-sm-5 col-md-6 col-lg-4">
<!---
        		<div class="mlp-acts">
        			<a href="/session/sire/pages/mlp-edit?action=add" class="btn green-gd btn-re add-mlp-button mlp-action">NEW MLP</a>
        		</div>
--->
<!---
        		<div class="mlp-acts">
        			<a href="/session/sire/pages/mlp-template-picker" class="btn green-gd btn-re add-template-button mlp-action">NEW MLP FROM TEMPLATE</a>
        		</div>
--->

				<div class="mlp-acts">
        			<a href="/session/sire/pages/mlp-template-picker" class="btn green-gd btn-re add-template-button mlp-action">NEW MLP</a>
        		</div>


<!---
        		<div class="mlp-acts">
        			<a href="/session/sire/pages/image-manage" class="btn green-gd btn-re mlp-action">manage images</a>
                <p style="padding: 0 10px;" class="warning-text-mlp hidden"><strong class="text-left" style="color: red;">You have reached max <cfoutput>#userPlanInfo.MLPSLIMITNUMBER#</cfoutput> active MLPs</strong></p>
        		</div>
--->
        	</div>
        </div>
        <!--- <div class="re-table">
        	<div class="table-scrollable">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th colspan="2">MLP1</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                            	<h3>Description:</h3>
                            	<p>New Empty MLP</p>
                            </td>
                            <td>
                                <div class="mlp-action">
	                                <div class="text-right">
	                                	<div class="btn-re-xs">TURN OFF</div>
	                                	<div class="btn-re-xs">EMBED CODE</div>
	                                </div>
                                	<div class="text-right">
                                		<div class="btn-re-dowload"></div>
                                		<div class="btn-re-delete"></div>
                                	</div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            	<h3>Public URL:</h3>
                            	<p>https://mlp-x.com/lz/-ovj8j</p>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                            	<h3>Preview:</h3>
                            	<div class="content-preview">
                            		
                            	</div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!--- table --->
        <div class="dataTables_paginate paging_input text-center">
        	<span class="paginate_button first">
        		<img class="paginate-arrow" src="/session/sire/images/double-left.png">
        	</span>
        	<span class="paginate_button previous" id="tblListShortURL_previous">
        		<img class="paginate-arrow" src="/session/sire/images/left.png">
        	</span>
        	<span class="paginate_page">Page </span>
        	<input type="text" class="paginate_text" style="display: inline;" value="1">
        	<span class="paginate_of"> of 1</span>
        	<span class="paginate_button next" id="tblListShortURL_next">
        		<img class="paginate-arrow" src="/session/sire/images/right.png">
        	</span>
        	<span class="paginate_button last" id="tblListShortURL_last">
        		<img class="paginate-arrow" src="/session/sire/images/double-right.png">
        	</span>
        </div>
        <!--- pagination ---> --->

        <div class="table-responsive">
            <table id="tblListEMS" class="table table-striped table-bordered mlp-list-table" style="table-layout:fixed">
           
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="InfoModalMLP" class="modal fade be-modal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        
      </div>
      <div class="modal-body">
        <h4 class="be-modal-title">Why would I need a Marketing Landing Page?</h4>
        <p></p>
        
        <!--- Now, you can create powerful marketing landing pages that drive more conversions. --->
        <p>A Marketing Landing Page (MLP) is a standalone web page dedicated to a single product, service or campaign.</p>

        <p>While your website or homepage focuses on exploration, your landing pages focus on conversion and compliance.</p>

        <p>In other words; landing pages guide visitors toward your conversion goal and provide the ideal foundation for your marketing campaigns.</p>

        <p>Sometimes you need to make quick copy and design edits and don't have time to wait for the developers to get back to you.</p>
        <p>There are several ways to use Marketing Landing Page:</p>

            <uL>
                <li><b>Offer</b> - give the user more detail on your offer. Provide instructions for redemption of offer. Coupons, Sweepstakes, Contests, Rewards, etc.</li>
                
                <li><b>Click-through</b> - Used to persuade visitors to click to another page</li>

                <li><b>Lead generation</b> - Designed to capture user data such as names and emails</li>

                <li><b>House-holding Data Capture</b> - Designed to capture user additional data to expand details on your existing subscriber lists</li>
                
                <li><b>Compliance</b> - A place to list out any terms and conditions associated with your offerings.</li>
                
            </uL>
                    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<cfparam name="variables._title" default="Marketing Landing Portals - Sire - Sire">
<cfinclude template="../views/layouts/master.cfm">


<script type="text/javascript">
	
	
	$( function() {	
		
		
	
	});
	
	
</script>
	



