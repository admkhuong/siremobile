<div class="portlet light bordered mlp-manage">
    <div class="portlet-body form">
        <h2 class="page-title">Welcome <span>Tuan</span>!</h2>
        <h3 class="mlp-shortcode">Your Short Code is <span>SireAssigned_1</span></h3>
        <div class="upload-image">
	        <div class="row">
	        	<div class="col-xs-12 col-sm-9 col-sm-offset-1">
	        		<div class="btn-upload-image">
		        		<span class="btn green-gd btn-re">
							<i class="glyphicon glyphicon-plus"></i>
							<span>Add image...</span>
						</span>
	        		</div>
	        		<p>Click Add image button or drag and drop image in this page. Accepted format: png  jpg  jpeg  gif <br> Max file size: 10MB.</p>
	        		<p>Filename format: Alphanumeric characters [0-9a-zA-Z] and Special characters -, _, (, )</p>
	        		<div class="mlp-storage-usage">
	        			<div class="row">
	        				<div class="col-xs-12 div col-sm-3">
	        					<div class="mlp-storage-label">Storage usage <br> 0.00MB / 50.00MB</div>
	        				</div>
	        				<div class="col-xs-12 div col-sm-9">
	        					<div class="progress">
	        					  	<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
	        					    	<span class="sr-only">40% Complete (success)</span>
	        					  	</div>
	        					</div>
	        				</div>
	        			</div>
	        		</div>
	        		<div class="mlp-image">
	        			<p class="image-label">Images</p>
	        			<img src="../assets/layouts/layout4/img/bg-profile-step.png" width="1000px">
	        			<p>You haven't upload any image yet</p>
	        		</div>
	        	</div>
	        </div>
        </div>
    </div>
</div>
<cfinclude template="../views/layouts/master.cfm">