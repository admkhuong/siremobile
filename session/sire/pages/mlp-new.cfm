<div class="portlet light bordered mlp-new">
    <div class="portlet-body form">
        <h2 class="page-title">Marketing Landing Portal Editor</h2>
        <div class="row">
        	<div class="col-xs-12 col-sm-8 col-sm-offset-2">
		        <div class="mlp-create-name">
        	        <div class="form-horizontal">
        				<div class="form-group">
        				    <label class="col-sm-4 control-label">Name:<span class="text-danger">*</span></label>
        				    <div class="col-sm-8">
        				      	<input type="text" class="form-control" maxlength="255" name="cppxName" value="New Empty MLP">
        				    </div>
        		  		</div>
        			   	<div class="form-group">
        				    <label class="col-sm-4 control-label"><div>{URL key}: https://MLP-X.com/lz/h-x67y</div></label>
        				    <div class="col-sm-8">
        				      	<input type="text" class="form-control" maxlength="255" name="cppxURL" value="h-x67y">
        				    </div>
        			  	</div>
        			</div>
		        </div>
        	</div>
        </div>
        <!--- mlp-create-name --->
        <div class="row">
        	<div class="col-sm-8 col-sm-offset-2">	
                <div class="mlp-editor"></div>
            </div>
        </div>
        <!--- mlp-editor --->
        <div class="row">	            
            <div class="col-md-8 col-md-offset-2">		
				<div class="mlp-preview">
					content preview
				</div>
        	</div>
		</div>
		<!--- mlp-preview --->
        <div class="row">	            
            <div class="col-md-8 col-md-offset-2">		
				<div class="mlp-style">
					<div class="mlp-style-row">
						<div class="mlp-col-left">
							<div class="col-xs-12">
								Web Safe Font Family
							</div>
						</div>
						<div class="mlp-col-right">
							<select class="form-control">
                                <optgroup label="Serif Fonts">
                                	<option>"Palatino Linotype", "Book Antiqua", Palatino, serif</option>
                                	<option>"Times New Roman", Times, serif</option>
                                </optgroup>
                                <optgroup label="Sans-Serif Fonts">
                                	<option>Arial, Helvetica, sans-serif</option>
                                	<option>"Arial Black", Gadget, sans-serif</option>
                                	<option>"Comic Sans MS", cursive, sans-serif</option>
                                	<option>Impact, Charcoal, sans-serif</option>
                                	<option>"Lucida Sans Unicode", "Lucida Grande", sans-serif</option>
                                	<option>Tahoma, Geneva, sans-serif</option>
                                	<option>"Trebuchet MS", Helvetica, sans-serif</option>
                                	<option>Verdana, Geneva, sans-serif</option>
                                </optgroup>
                                <optgroup>
                                	<option>"Courier New", Courier, monospace</option>
                                	<option>"Lucida Console", Monaco, monospace</option>
                                </optgroup>
                            </select>
						</div>
					</div>
					<!--- 1 --->
					<div class="mlp-style-row">
						<div class="mlp-col-left">
							<div class="col-xs-12">Web Safe Font Family</div>
						</div>
						<div class="mlp-col-right">
							<select class="form-control">
								<option>none</option>
								<option>hidden</option>
								<option>dotted</option>
								<option>dashed</option>
								<option>solid</option>
								<option>double</option>	
							</select>
						</div>
					</div>
					<!--- 2 --->
					<div class="mlp-style-row">
						<div class="mlp-col-left">
							<div class="col-xs-12">Background Color</div>
						</div>
						<div class="mlp-col-right">
							<div class="col-xs-12">value</div>
						</div>
					</div>
					<!--- 3 --->
					<div class="mlp-style-row">
						<div class="mlp-col-left">
							<div class="col-xs-12">Border Color</div>
						</div>
						<div class="mlp-col-right">
							<div class="col-xs-12">value</div>
						</div>
					</div>
					<!--- 4 --->
					<div class="mlp-style-row">
						<div class="mlp-col-left">
							<div class="col-xs-12">Border Width (px)</div>
						</div>
						<div class="mlp-col-right">
							<div class="col-xs-12">value</div>
						</div>
					</div>
					<!--- 5 --->
				</div>
        	</div>
		</div>
		<!--- mlp-style --->
		<div class="row">
			<div class="col-xs-12">
				<div class="footer-botton text-center">
					<button class="btn green-gd btn-re">PRINT PREVIEW</button>
					<button class="btn green-gd btn-re">PREVIEW RAW</button>
					<button class="btn green-gd btn-re">SAVE</button>
				</div>
			</div>
		</div>
    </div>
</div>
<cfinclude template="../views/layouts/master.cfm">