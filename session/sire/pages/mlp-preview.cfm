<cfparam name="inpMLPXURL" default="">


<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("/session/sire/css/mlp.css")
    	.addJs("/session/sire/js/site/mlp-preview.js");
</cfscript>


<cfparam name="variables._title" default="Marketing Landing Portal Preview - Sire">

<div class="portlet light bordered subscriber">
    <div class="portlet-body form">
    	 <h2 class="page-title">Create template from MLP</h2>
        <div class="row">
        	<div class="content-body">
				<div class="col-md-12">
	                <div class="row heading">
	                	<div class="col-sm-12 heading-title">Preview a Marketing Landing Portal <hr></div>
	                </div>
	                <div id="resizable" class="ui-widget-content">
	                	<iframe src="https://<cfoutput>#CGI.server_name#</cfoutput>/mlp-x/lz/<cfoutput>#inpMLPXURL#</cfoutput>?isPreview=1" scrolling="auto" width="100%" height="100%"></iframe>
		            </div>    
	            </div>    
	        </div>    
        </div>
    </div>
</div>



<cfinclude template="../views/layouts/master.cfm">