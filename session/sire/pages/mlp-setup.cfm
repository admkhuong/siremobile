<cfparam name="variables._title" default="Marketing Landing Portals - Sire">
<main class="container-fluid page my-plan-page">
	<cfinclude template="../views/commons/credits_available.cfm">
		<section class="row bg-white">
			<div class="content-header">
				<div class="row">
					<div class="col-sm-5 content-title">
                    	<cfinclude template="../views/commons/welcome.cfm">
                	</div>
				</div>	
			</div>
			<!--- END : content-header --->
			<hr class="hrt0">
			<div class="col-md-12">
                <div class="row heading">
                	<div class="col-sm-12 heading-title">Setting up a Marketing Landing Portal <hr></div>
                </div>
                <div class="row">
                	<form id="create_cpp_frm" autocomplete="off">
		                <div class="col-md-6 form-horizontal">
		                	<div class="form-group">
							    <label for="card_number" class="col-sm-4 control-label">Name:<span class="text-danger">*</span></label>
							    <div class="col-sm-8">
							      <input type="text" class="form-control validate[required,ajax[ajaxCPPNameAvailable]]" maxlength="255" id="cppxName" name="cppxName" value='' data-prompt-position="topLeft:100">
							    </div>
						  	</div>

						   	<div class="form-group">
							    <label for="card_number" class="col-sm-4 control-label">Custom URL:</label>
							    <div class="col-sm-8">
							      <input type="text" class="form-control validate[ajax[ajaxCPPUrlAvailable],custom[checkCustomURL]]" maxlength="32" id="cppxURL" name="cppxURL" value='' data-prompt-position="topLeft:100">
							    </div>
						  	</div>

						  	<div class="checkbox">
							    <label>
							      <input type="checkbox" name='checkTerms' id="checkTerms" value='1'>Require customers need to accept your terms of service ?
							    </label>
							</div>

		                </div>
		                <div class="col-md-6">
		                	<div class="form-group wrapper-terms_of_service" style="display:none">
			                	<textarea name="cppxTermOfService" id="cppxTermOfService" class="normal"></textarea>
			                </div>	
		                </div>

			        	<div class="col-sm-11 col-md-3 col-md-offset-9 col-lg-2 col-lg-offset-10 form-footer text-center form-group">
			        		
			            	<a type="button" class="btn btn-medium btn-back-custom pull-right"  href="javascript:history.go(-1)">Back</a>
			            	<button type="submit" class="btn btn-medium btn-success-custom btn-create-cpp pull-right">Next</button>
			        	</div>
	                </form>		
	            </div>    
            </div>    

		</section>	
</main>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/js/tinymce_4.3.8/tinymce.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/site/mlp.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/mlp.css">
</cfinvoke>

<cfinclude template="../views/layouts/main.cfm">