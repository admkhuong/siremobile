<!---
 This page is meant to be called inside of a bootstrap modal where all of the jquery and bootstrap stuff is already loaded. Do not load twice!
--->

<!--- MLP TEMPLATE PICKER --->
<cfinvoke component="session.sire.models.cfc.mlp" method="getCPPTemplateList" returnvariable="listTemplate"></cfinvoke>

<!--- GET USER SHORTCODE --->

<!--- remove old method: 
<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>							
--->
<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>

<cfif findNoCase('iPhone', cgi.http_user_agent,1) OR findNoCase('iPad', cgi.http_user_agent,1) OR findNoCase('iPod', cgi.http_user_agent,1)>
    <cfoutput>
        <style type="text/css">
            @media only screen and (max-width: 480px), only screen and (max-device-width: 480px) {
                iframe {
                    width: 1px;
                    min-width: 100%;
                    /**width: 100%;*/
                    /*height: 1px;*/
                    min-height: 100%;
                    /**height: 100%;*/
                    overflow-y: scroll;
                }
                .iframe-wrapper {
                    overflow-y: scroll;
                    -webkit-overflow-scrolling: touch;
                }
            }

            @media only screen and (max-width: 1024px), only screen and (max-device-width: 1024px) {
                iframe {
                    width: 1px;
                    min-width: 100%;
                    /**width: 100%;*/
                    /*height: 1px;*/
                    min-height: 100%;
                    /**height: 100%;*/
                    overflow-y: scroll;
                }
                .iframe-wrapper {
                    overflow-y: scroll;
                    -webkit-overflow-scrolling: touch;
                }
            }
        </style>
    </cfoutput>
    
<cfelse>
    
    <style>

	.scaled-frame-mlp {		
			zoom: 0.50;		
			-moz-transform: scale(0.50);
			-moz-transform-origin: 0 0;
			-o-transform: scale(0.50);
			-o-transform-origin: 0 0;
			-webkit-transform: scale(0.50);
			-webkit-transform-origin: 0 0;
			
			width: 200%;
			height: 200%;
		}    
    
    </style>
    
</cfif>



<style>


	.btn-select-template
	{
		display: none;
		margin: 0 auto;
	}

	.mlp-template-item:hover
	{
		-webkit-box-shadow: -1px 2px 5px 0px rgba(50, 50, 50, 0.4);
		-moz-box-shadow:    -1px 2px 5px 0px rgba(50, 50, 50, 0.4);
		box-shadow:         -1px 2px 5px 0px rgba(50, 50, 50, 0.4);
	}

	.mlp-template-item:hover .btn-select-template
	{
		display: inline;
		color: fff;
	}
	
	.mlp-select-template:hover
	{
		color: fff;
		text-decoration: none;		
	}

</style>


<div class="portlet light bordered mlp-new-template">
    <div class="portlet-body form">
     
        <div id="createCampaignModal" class="mlp-template-list">
            <cfif listTemplate.RXRESULTCODE GT 0>  
                <cfset listData = listTemplate.DATA>
                <form action="##" name="form-create-campaign" id="form-create-campaign" autocomplete="off">
              
                    <!--- <div class="container-fluid TemplatePickerContainer"> --->
                        <div class="row">
	                        
	                        <!--- Add Parent if TOC --->
	                        
	                        
                            <cfloop array="#listData#" index="item">
                                <cfif Len(item.NAME) GT 150>
                                    <cfset displayName = Left(item.NAME, 150) & ' ...'>
                                <cfelse>
                                    <cfset displayName = item.NAME>                              
                                </cfif>
                             
                                <cfoutput>
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                        <div class="mlp-template-item" style="height: 20vh; position: relative; margin-bottom: 1em;">
                                                
                                            <div id="templateid-#item.ID#" href='/session/sire/pages/mlpx-edit?action=add&ccpxDataIdSource=#item.ID#' class="content mlp-tmp-content"></div>                                                   
                                            <iframe src="/mlp-x/lz/index.cfm?inpURL=<cfoutput>#item.ID#</cfoutput>&isPreview=1&action=template" scrolling="auto" frameborder="0" width="100%" height="100%" class="scaled-frame-mlp"></iframe>
                                                                                            
                                            <div class="btn-select-template" style="position: absolute; bottom: 1em; width: 100%; text-align: center;">
                                                <a type="button" class="mlp-btn mlp-select-template" data-attr-template-id="#item.ID#">Select</a>  <!--- href="/session/sire/pages/mlpx-edit?action=add&ccpxDataIdSource=#item.ID#" --->    
                                            </div>
                                                    
                                        </div>
                                        
                                        <div class="mlp-title">
                                            <a href="##"><p class="template-name"><strong> <cfoutput>#item.NAME#</cfoutput> </strong></p></a>
                                        </div> 
		                                            
                                    </div>
                                </cfoutput>
                            </cfloop>
                        </div>
                    <!--- </div> --->
                </form>
            <cfelse>  
                <form action="##" name="form-create-campaign" id="form-create-campaign" autocomplete="off">
                    <p class="title">No template!</p>
                </form>  
            </cfif>
        </div>

    </div>
</div>

<cfparam name="variables._title" default="MLP Template Picker">
