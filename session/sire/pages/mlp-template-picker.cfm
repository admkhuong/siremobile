<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("../assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css", true)
        .addJs("../assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js", true)
        .addJs("../assets/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js", true)
        .addJs("../assets/pages/scripts/mlppicker.js");
</cfscript>

<!--- MLP TEMPLATE PICKER --->
<cfinvoke component="session.sire.models.cfc.mlp" method="getCPPTemplateList" returnvariable="listTemplate"></cfinvoke>

<!--- GET USER SHORTCODE --->

<!--- remove old method: 
<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>									
--->
<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>

<cfif findNoCase('iPhone', cgi.http_user_agent,1) OR findNoCase('iPad', cgi.http_user_agent,1) OR findNoCase('iPod', cgi.http_user_agent,1)>
    <cfoutput>
        <style type="text/css">
            @media only screen and (max-width: 480px), only screen and (max-device-width: 480px) {
                iframe {
                    /*width: 1px;*/
                    /*min-width: 100%;*/
                    /**width: 100%;*/
                    /*height: 1px;*/
                    /*min-height: 100%;*/
                    /**height: 100%;*/
                    overflow-y: scroll;
                }
                .iframe-wrapper {
                    overflow-y: scroll;
                    -webkit-overflow-scrolling: touch;
                }
            }

            @media only screen and (max-width: 1024px), only screen and (max-device-width: 1024px) {
                iframe {
                    /*width: 1px;*/
                    /*min-width: 100%;
                    /**width: 100%;*/
                    /*height: 1px;*/
                    /*min-height: 100%;*/
                    /**height: 100%;*/
                    overflow-y: scroll;
                }
                .iframe-wrapper {
                    overflow-y: scroll;
                    -webkit-overflow-scrolling: touch;
                }
            }
        </style>
    </cfoutput>
    
<cfelse>
    
    <style>

	.scaled-frame-mlp {		
			zoom: 0.50;		
			-moz-transform: scale(0.50);
			-moz-transform-origin: 0 0;
			-o-transform: scale(0.50);
			-o-transform-origin: 0 0;
			-webkit-transform: scale(0.50);
			-webkit-transform-origin: 0 0;
			
			width: 200%;
			height: 200%;
		}    
    
    </style>
    
</cfif>

<style>




<!---


.scaled-frame-mlp {		
		zoom: 0.50;		
		-moz-transform: scale(0.50);
		-moz-transform-origin: 0 0;
		-o-transform: scale(0.50);
		-o-transform-origin: 0 0;
		-webkit-transform: scale(0.50);
		-webkit-transform-origin: 0 0;
	}

	
	@media screen and (-webkit-min-device-pixel-ratio:0) {
		.scaled-frame-mlp  { 
			
			zoom: 1;
			
			-moz-transform: scale(1.0);
			-moz-transform-origin: 0 0;
			-o-transform: scale(1.0);
			-o-transform-origin: 0 0;
			-webkit-transform: scale(1.0);
			-webkit-transform-origin: 0 0;
		
		  }
	}
--->
	
</style>


<div class="portlet light bordered mlp-new-template">
    <div class="portlet-body form">
        <h2 class="page-title">Select a MLP Template to get started!</h2>
        <!--- <cfif shortCode.SHORTCODE NEQ ''>
            <h3 class="mlp-shortcode">Your Short Code is <span><cfoutput>#shortCode.SHORTCODE#</cfoutput></span></h3>
        <cfelse>
            <h3 class="mlp-shortcode"><span>Set Keywords</span></h3>
        </cfif> --->

        <div id="createCampaignModal" class="mlp-template-list">
            <cfif listTemplate.RXRESULTCODE GT 0>  
                <cfset listData = listTemplate.DATA>
                <form action="##" name="form-create-campaign" id="form-create-campaign" autocomplete="off">
              
                    <!--- <div class="container-fluid TemplatePickerContainer"> --->
                        <div class="row">
                            <cfloop array="#listData#" index="item">
                                <cfif Len(item.NAME) GT 150>
                                    <cfset displayName = Left(item.NAME, 150) & ' ...'>
                                <cfelse>
                                    <cfset displayName = item.NAME>                              
                                </cfif>
                             
                                <cfoutput>
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                        <div class="mlp-template-item">
                                            <div class="mlp-thumb">
                                                <div class="mlp-thumb-content item-grid iframe-wrapper">
                                                    <div id="templateid-#item.ID#" href='/session/sire/pages/mlpx-edit?action=add&ccpxDataIdSource=#item.ID#' class="content mlp-tmp-content"></div>                                                   
                                                    <iframe src="/mlp-x/lz/index.cfm?inpURL=<cfoutput>#item.ID#</cfoutput>&isPreview=1&action=template" scrolling="auto" frameborder="0" width="100%" height="100%" class="scaled-frame-mlp"></iframe>
                                                </div>
                                                <div class="btn-select-template">
                                                    <a type="button" class="btn green-gd btn-re btn-select-cpp" href="/session/sire/pages/mlpx-edit?action=add&ccpxDataIdSource=#item.ID#">Select</a>    
                                                </div>
                                            </div>
                                            <div class="mlp-title">
                                                <a href="##"><p class="template-name"><strong> <cfoutput>#item.NAME#</cfoutput> </strong></p></a>
                                            </div>         
                                        </div>
                                    </div>
                                </cfoutput>
                            </cfloop>
                        </div>
                    <!--- </div> --->
                </form>
            <cfelse>  
                <form action="##" name="form-create-campaign" id="form-create-campaign" autocomplete="off">
                    <p class="title">No template!</p>
                </form>  
            </cfif>
        </div>


        <!--- <div class="mlp-template-list">
        	<div class="row">
        		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
        			<div class="mlp-template-item">
                        <div class="mlp-thumb">
                            <div class="mlp-thumb-content">
                                
                            </div>
                            <div class="btn-select-template">
                                <button type="button" class="btn green-gd btn-re">Select</button>    
                            </div>
                        </div>
                        <div class="mlp-title">
                            <a href="">lorem</a>
                        </div>         
                    </div>
        		</div>
        		<!---  --->
        		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                    <div class="mlp-template-item">
                        <div class="mlp-thumb">
                            <div class="mlp-thumb-content">
                                
                            </div>
                            <div class="btn-select-template">
                                <button type="button" class="btn green-gd btn-re">Select</button>    
                            </div>
                        </div>
                        <div class="mlp-title">
                            <a href="">lorem</a>
                        </div>         
                    </div>
                </div>
        		<!---  --->
        		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                    <div class="mlp-template-item">
                        <div class="mlp-thumb">
                            <div class="mlp-thumb-content">
                                
                            </div>
                            <div class="btn-select-template">
                                <button type="button" class="btn green-gd btn-re">Select</button>    
                            </div>
                        </div>
                        <div class="mlp-title">
                            <a href="">lorem</a>
                        </div>         
                    </div>
                </div>
        		<!---  --->
        		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                    <div class="mlp-template-item">
                        <div class="mlp-thumb">
                            <div class="mlp-thumb-content">
                                
                            </div>
                            <div class="btn-select-template">
                                <button type="button" class="btn green-gd btn-re">Select</button>    
                            </div>
                        </div>
                        <div class="mlp-title">
                            <a href="">lorem</a>
                        </div>         
                    </div>
                </div>
        	</div>
        </div> --->
    </div>
</div>

<cfparam name="variables._title" default="MLP Manage - Sire">
<cfinclude template="../views/layouts/master.cfm">