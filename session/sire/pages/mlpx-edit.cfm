<cfparam name="inpCPPID" default="">
<cfparam name="ccpxDataId" default="0">
<cfparam name="ccpxDataIdSource" default="0">
<cfparam name="templateid" default="0">
<cfparam name="tocid" default="-1">

<!--- 1= show, it 0 = ignore it Save certain scripts and code from only loading until on actual MLP - certain js libraries can alter the DOM in ways that get saved into MLP storage and can hose final display - Make available to javascript --->
<cfparam name="MLPDelayLoad" default="0">

<cfparam name="action" default="">

<cfparam name="variables._title" default="Marketing Landing Portal Editor - Sire">
<title><cfoutput>#variables._title#</cfoutput></title>


<!---

	Notes and things to try

	try siremobile/dir/code to load mlp from main pages from DB - better on SEO

	ajax api call to load MLP remotely? still maintain in MLP editor but allow easier site integration for devs?

	Generic Form Post - send email? Rest option?


	See here - what is actually editable
	https://github.com/GetmeUK/ContentTools/issues/231
	Basically - editable text is only ,p> and <h1>, <h2>, ... tags
	You can make a <div tag text editable by adding <div data-ce-tag="p"> to it so text is treated like a paragraph


	http://www.quackit.com/html/codes/contenteditable.cfm
	ContentEdiable
	document.queryCommandValue("ForeColor");
	document.execCommand('styleWithCSS', false, true);
	document.execCommand('ForeColor', false, color.toRgbString());

--->

<cfinclude template="/session/sire/configs/paths.cfm">

<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="/session/sire/js/vendors/darktooltip/darktooltip.css">
<link rel="stylesheet" href="/public/sire/css/bootstrap.css" media="screen">
<link rel="stylesheet" href="/public/sire/assets/plugins/uikit/uikit3.css" media="screen">
<!--- https://select2.github.io/  --->
<link rel="stylesheet" href="/public/sire/css/select2.min.css">
<link rel="stylesheet" href="/public/sire/css/select2-bootstrap.min.css">

<!--- https://bgrins.github.io/spectrum/ --->
<link rel="stylesheet" type="text/css" href="/session/sire/js/vendors/spectrum/spectrum.css">

<!--- https://github.com/kenshin54/popline --->
<link rel="stylesheet" type="text/css" href="/session/sire/js/vendors/popline/themes/popclip.css" />

<!--- http://www.dropzonejs.com/    	https://github.com/enyo/dropzone --->
<link rel="stylesheet" type="text/css" href="/session/sire/js/vendors/dropzone/dropzone.css" />

<!--- https://github.com/abpetkov/switchery --->
<link rel="stylesheet" type="text/css" href="/session/sire/js/vendors/switchery/switchery.min.css" />

<!--- validation engine --->
<link href="/public/sire/css/validationEngine.jquery.css" rel="stylesheet" media="screen">

 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!--- <script type="text/javascript" src="/public/sire/js/jquery-2.1.4.js"></script> --->
<!--- <script type="text/javascript" src="/session/sire/js/jquery-3.2.1.min.js"></script> --->
<script type="text/javascript" src="/public/sire/js/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<!--- http://touchpunch.furf.com/ - Make jquery ui work on touch devices - hack for now but works until better option comes along --->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
<!--- <script type="text/javascript" src="/public/sire/js/bootstrap.js"></script> --->
<script type="text/javascript" src="/public/sire/assets/plugins/uikit/uikit.min.js"></script>
<script type="text/javascript" src="/session/sire/js/vendors/darktooltip/jquery.darktooltip.js"></script>
<script type="text/javascript" src="/public/sire/js/bootbox.min.js"></script>
<!--- https://select2.github.io/  --->
<script type="text/javascript" src="/public/sire/js/select2.min.js"></script>

<!--- https://bgrins.github.io/spectrum/ --->
<script type="text/javascript" src="/session/sire/js/vendors/spectrum/spectrum.js"></script>

<!--- https://github.com/kenshin54/popline --->
<script type="text/javascript" src="/session/sire/js/vendors/popline/scripts/jquery.popline.js"></script>
<script type="text/javascript" src="/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.link.js"></script>
<!--- <script type="text/javascript" src="/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.blockquote.js"></script> --->
<script type="text/javascript" src="/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.decoration.js"></script>
<script type="text/javascript" src="/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.list.js"></script>
<script type="text/javascript" src="/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.justify.js"></script>
<script type="text/javascript" src="/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.blockformat.js"></script>
<script type="text/javascript" src="/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.social.js"></script>
<!--- <script type="text/javascript" src="/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.backcolor.js"></script> --->
<script type="text/javascript" src="/session/sire/js/vendors/popline/scripts/plugins/jquery.popline.textcolor-spectrum.js"></script>

<!--- http://www.dropzonejs.com/    	https://github.com/enyo/dropzone --->
<script type="text/javascript" src="/session/sire/js/vendors/dropzone/min/dropzone.min.js"></script>

<!-- BEGIN DATATABLE -->
<script type="text/javascript" src="/public/js/datatables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/session/sire/js/dataTables.inputPagination.js"></script>
<!-- END DATATABLE -->

<!--- http://lovasoa.github.io/tidy-html5/ --->
<script type="text/javascript" src="/session/sire/js/vendors/tidy/tidy.js"></script>

<!--- https://github.com/abpetkov/switchery --->
<script type="text/javascript" src="/session/sire/js/vendors/switchery/switchery.min.js"></script>

<!--- https://github.com/swisnl/jQuery-contextMenu --->
<link rel="stylesheet" type="text/css" href="/session/sire/js/vendors/contextmenu/jquery.contextMenu.min.css" />
<script type="text/javascript" src="/session/sire/js/vendors/contextmenu/jquery.contextMenu.min.js"></script>


<!---
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
--->



<!--- Guides and additional info on the tools used on this page

	Notes:

		Dont use jquery.validationEngine.en.js - write your own methods and calls that need to be validated. Some stuff makes sense local validation, while other stuff makes more sense on server side validation. the Ajax methods in the previously used library are jacked up and do not work well - async vs sync. multiple callbacks, etc.

		Include google fonts in the the "Custom Global Header" which takes the place of the old style editor

		All new MLP docs will have a master <div id="MLP-Master-Section"

		Major modifications were done to mlp-x/index.cfm  -- there was a lot of legacy stuff and missing newer stuff
			All padding and margins are now controlled in MLP doc itself

		Save changes are published automatically by default, but there is a new option to save changes while editing and then only publish when ready.

		Make sure all DB changes are carried into your dev/QA environments see https://docs.google.com/document/d/1EVb67-fIqsyj782Vi2wa9usnlh2b0vF-u1e0KTqSIsc/edit
			Many changes over the last few weeks in here

		Seems dir creation is by tomcat8…. Need to reset this too for testing uploads
		sudo chmod -R 755 /var/www/dev.siremobile.com/webroot/public/sire/upload
		sudo chown -R tomcat8:tomcat8 /var/www/dev.siremobile.com/webroot/public/sire/upload



	Flex layout to start
	http://codepen.io/torbencolding/pen/WboQPr

	https://css-tricks.com/snippets/css/a-guide-to-flexbox/
	http://getcontenttools.com/getting-started
	http://stackoverflow.com/questions/37934496/contenttools-appending-data-via-jquery

	https://css-tricks.com/viewport-sized-typography/

	http://html5doctor.com/the-contenteditable-attribute/
	https://css-tricks.com/show-and-edit-style-element/

	Title with line knocked out - http://jsfiddle.net/attenzione/MeDDM/

	$('#container *').attr('contenteditable','true');

	Confirm Delete
	Tool Tip Confirm - https://github.com/rubentd/darktooltip

	Redo
	http://stackoverflow.com/questions/2301431/how-to-restore-an-element-removed-with-jquery

	*Task: Hook up image uploading to S3 and user id
	http://getcontenttools.com/tutorials/handling-image-uploads

	Image upload tools
	http://www.jqueryrain.com/demo/jquery-crop-image-plugin/


	http://www.dropzonejs.com/
	https://github.com/enyo/dropzone

	inplace edit for description at top
	http://jsfiddle.net/egstudio/aFMWg/1/

	https://css-tricks.com/stripes-css/

	Options to clean up code for coder view - http://lovasoa.github.io/tidy-html5/
	http://tidy.sourceforge.net/docs/quickref.html

	https://collaboration133.com/how-to-scale-iframe-content-in-ie-chrome-firefox-and-safari/2717/

	https://css-tricks.com/snippets/css/momentum-scrolling-on-ios-overflow-elements/

	http://www.web2feel.com/creating-a-fullscreen-hero-div-for-your-site-header/

	https://css-tricks.com/annoying-mobile-double-tap-link-issue/
	https://joshtronic.com/2015/04/19/handling-click-and-touch-events-on-the-same-element/

	https://v4-alpha.getbootstrap.com/components/popovers/

	https://www.smashingmagazine.com/2015/01/designing-for-print-with-css/


	Image sorce for icons with size and color selector
	https://iconmonstr.com/rocket-19/?png
	Cool off color - #5F6B67

	Bug Fix - modified jquery.popline.blockformat.js to keep contenteditable="true" where it might be turned off at parent

	https://stackoverflow.com/questions/3115150/how-to-escape-regular-expression-special-characters-using-javascript


	Fancy image data - IOS 10 SMS link preview enhancements
	https://stackoverflow.com/questions/11616697/how-to-use-og-meta-tag-for-facebook-share
	https://developers.facebook.com/tools/debug/sharing/?q=https%3A%2F%2Fwww.mlp-x.com%2Flz%2FEOBQ

	https://moz.com/blog/meta-data-templates-123



	Template Ideas and Free Img Sources:

	https://libreshot.com/tag/sea/
	https://www.pexels.com/photo/beach-sand-girl-wind-6706/


	https://github.com/swisnl/jQuery-contextMenu

	Use CDN for custom templates
	https://cdnjs.com/libraries



 --->


<!--- 1= show, it 0 = ignore it Save certain scripts and code from only loading until on actual MLP - certain js libraries can alter the DOM in ways that get saved into MLP storage and can hose final display - Make available to javascript --->
<script type="text/javascript">
	var MLPDelayLoad = "<cfoutput>#MLPDelayLoad#</cfoutput>";
</script>


<!--- Possible Todo: Minfy js output on load - so data send to browser is smaller/lighter/faster - Test: Will server side delays offset client side delays of extra data--->
<cfinclude template="../assets/pages/styles/mlpx-editor-css.cfm" />

<!--- Styles that need to also be on main MLP-X page --->
<!--- I may want to move these into templates directly and assume nothing in basic pages --->
<style>

	<!--- https://www.w3schools.com/cssref/tryit.asp?filename=trycss_float_clear_overflow --->
	.clearfix::after {
	    content: "";
	    clear: both;
	    display: table;
	}

	.img-responsive {
	    display: block;
	    height: auto;
	    width: 100%;
	    padding: 0;
	}

	<!--- https://zellwk.com/blog/responsive-typography/ --->
	<!--- https://css-tricks.com/viewport-sized-typography/ --->
	#MLPMain h1, #main-stage-content h1 {
		font-size: 5.7vw;
	}

	#MLPMain h2, #main-stage-content h2 {
		font-size: 4.0vw;
	}

	#MLPMain h3, #main-stage-content h3 {
		font-size: 2.8vw;
	}

	#MLPMain h4, #main-stage-content h4 {
		font-size: 2.0vw;
	}

	#MLPMain h5, #main-stage-content h5 {
		font-size: 1.6vw;
	}

	#MLPMain p, #main-stage-content p {
		font-size: 2.5vw;
	}

	@media all and (min-width: 800px) {
	  #MLPMain h1, #main-stage-content h1 {
			font-size: 5.7vw;
		}

		#MLPMain h2, #main-stage-content h2 {
			font-size: 4.0vw;
		}

		#MLPMain h3, #main-stage-content h3 {
			font-size: 2.8vw;
		}

		#MLPMain h4, #main-stage-content h4 {
			font-size: 2.0vw;
		}

		#MLPMain h5, #main-stage-content h5 {
			font-size: 1.6vw;
		}

		#MLPMain p, #main-stage-content p {
			font-size: 2.5vw;
		}
	}

	@media all and (min-width: 1200px) {
	  #MLPMain h1, #main-stage-content h1 {
			font-size: 2.65vw;
		}

		#MLPMain h2, #main-stage-content h2 {
			font-size: 2.0vw;
		}

		#MLPMain h3, #main-stage-content h3 {
			font-size: 1.4vw;
		}

		#MLPMain h4, #main-stage-content h4 {
			font-size: 1.0vw;
		}

		#MLPMain h5, #main-stage-content h5 {
			font-size: 0.8vw;
		}

		#MLPMain p, #main-stage-content p {
			font-size: 1.2vw;
		}
	}


	.section-arrow-top:before {
	    left: 50%;
	    -ms-transform: translateX(-50%);
	    transform: translateX(-50%);
	    border: solid transparent;
	    border-color: transparent;
	    content: "";
	    height: 0;
	    pointer-events: none;
	    position: absolute;
	    width: 0;
	    z-index: 1;
	    border-bottom-color: inherit !important;
	    border-width: 0 30px 20px !important;
	    bottom: 100%;
	}

	.section-arrow-bottom:before {
	    left: 50%;
	    -ms-transform: translateX(-50%);
	    transform: translateX(-50%);
	    border: solid transparent;
	    border-color: transparent;
	    content: "";
	    height: 0;
	    pointer-events: none;
	    position: absolute;
	    width: 0;
	    z-index: 1;
	    border-top-color: inherit !important;
	    border-width: 20px 30px 0 !important;
	    top: 100%;
	}

	.MLPEditable {
	    position: relative;
	    padding: 2em;
	}


 	<cfif MLPDelayLoad EQ 0>
		.MLPXDesignHidden
		{
			display:none;
		}

	</cfif>

</style>







<cfif action EQ "template">

	<cfif ccpxDataId EQ 0>
		<!--- Create new MLP Template and refresh page with new Template Id--->

		<!--- Check if this new MLP Template from existing MLP Source --->
		<cfif ccpxDataIdSource GT 0 >

			<cfinvoke component="session.sire.models.cfc.mlp" method="ReadCPP" returnvariable="RetCPPXData">
				<cfinvokeargument name="ccpxDataId" value="#ccpxDataIdSource#">
			</cfinvoke>

			<cfinvoke component="session.sire.models.cfc.mlp" method="AddMLPTemplate" returnvariable="RetAddTemplate">
				<cfinvokeargument name="inpRawContent" value="#RetCPPXData.RAWCONTENT#">
				<cfinvokeargument name="inpCustomCSS" value="#RetCPPXData.CUSTOMCSS#">
				<cfinvokeargument name="inpDesc" value="New Template based on #RetCPPXData.CPPNAME#">
			</cfinvoke>

		<cfelse>

			<!--- Create blank template --->
			<cfinvoke component="session.sire.models.cfc.mlp" method="AddMLPTemplate" returnvariable="RetAddTemplate">
				 <cfinvokeargument name="ccpxDataId" value="#ccpxDataId#">
			</cfinvoke>

		</cfif>

		<!--- Reload page as an editor now --->
		<cfif RetAddTemplate.RXRESULTCODE EQ 1>
			<cflocation url="mlpx-edit?action=template&ccpxDataId=#RetAddTemplate.CPPID#" addToken="no" />
		</cfif>

	</cfif>

	<!--- Read exisitng template data --->
	<cfinvoke component="session.sire.models.cfc.mlp" method="ReadMLPTemplate" returnvariable="RetCPPXData">
		 <cfinvokeargument name="ccpxDataId" value="#ccpxDataId#">
	</cfinvoke>


<cfelseif action EQ "add">
	<!--- Check if this new MLP Template from existing MLP Source --->
	<cfif ccpxDataIdSource GT 0 >

		<!--- Read exisitng template data --->
		<cfinvoke component="session.sire.models.cfc.mlp" method="ReadMLPTemplate" returnvariable="RetCPPXData">
			 <cfinvokeargument name="ccpxDataId" value="#ccpxDataIdSource#">
		</cfinvoke>

		<!--- Read Profile data and put into form --->
        <cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="RetVarGetUserOrganization"></cfinvoke>

        <!---Create a data set to run against dynamic data --->
        <cfset inpFormData = StructNew() />

        <cfset inpFormData.inpBrand = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONBUSINESSNAME_VCH />
        <cfset inpFormData.inpBrandFull = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH />
        <cfset inpFormData.inpBrandWebURL = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITE_VCH />
        <cfset inpFormData.inpBrandWebTinyURL = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITETINY_VCH/>
        <cfset inpFormData.inpBrandPhone = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONPHONE_VCH />
        <cfset inpFormData.inpBrandeMail = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONEMAIL_VCH />

        <cfif inpFormData.inpBrand EQ ""><cfset inpFormData.inpBrand = "inpbrand"></cfif>
        <cfif inpFormData.inpBrandFull EQ ""><cfset inpFormData.inpBrandFull = "inpBrandFull"></cfif>
        <cfif inpFormData.inpBrandWebURL EQ ""><cfset inpFormData.inpBrandWebURL = "inpBrandWebURL"></cfif>
        <cfif inpFormData.inpBrandWebTinyURL EQ ""><cfset inpFormData.inpBrandWebTinyURL = "inpBrandWebURLShort"></cfif>
        <cfif inpFormData.inpBrandPhone EQ ""><cfset inpFormData.inpBrandPhone = "inpBrandPhone"></cfif>
        <cfif inpFormData.inpBrandeMail EQ ""><cfset inpFormData.inpBrandeMail = "inpBrandeMail"></cfif>

        <cfset inpFormData.CompanyNameShort = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONBUSINESSNAME_VCH />
        <cfset inpFormData.CompanyName = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH />
        <cfset inpFormData.CompanyWebSite = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITE_VCH />
        <cfset inpFormData.CompanyWebSiteShort = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITETINY_VCH/>
        <cfset inpFormData.CompanyPhoneNumber = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONPHONE_VCH />
        <cfset inpFormData.CompanyEMailAddress = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONEMAIL_VCH />


		<!--- Do the MLP transforms here --->
        <cfinvoke component="session.cfc.csc.csc" method="DoDynamicTransformations" returnvariable="RetVarDoDynamicTransformations">
            <cfinvokeargument name="inpResponse_vch" value="#RetCPPXData.RAWCONTENT#">
            <cfinvokeargument name="inpContactString" value="">
            <cfinvokeargument name="inpRequesterId_int" value="">
            <cfinvokeargument name="inpFormData" value="#inpFormData#">
            <cfinvokeargument name="inpMasterRXCallDetailId" value="0">
            <cfinvokeargument name="inpShortCode" value="">
            <cfinvokeargument name="inpBatchId" value="">
            <cfinvokeargument name="inpBrandingOnly" value="1">
        </cfinvoke>

        <!--- Update the current template data --->
		<cfset RetCPPXData.RAWCONTENT = RetVarDoDynamicTransformations.RESPONSE />

		<cfinvoke component="session.sire.models.cfc.mlp" method="AddMLP" returnvariable="RetAddMLP">
			<cfinvokeargument name="inpRawContent" value="#RetCPPXData.RAWCONTENT#">
			<cfinvokeargument name="inpDesc" value="New MLP based on #RetCPPXData.CPPNAME#">
			<cfinvokeargument name="inpCustomCSS" value="#RetCPPXData.CUSTOMCSS#">
		</cfinvoke>

	<cfelse>

		<cfinvoke component="session.sire.models.cfc.mlp" method="AddMLP" returnvariable="RetAddMLP">
			<cfinvokeargument name="inpRawContent" value="">
			<cfinvokeargument name="inpDesc" value="New Empty MLP">
		</cfinvoke>

	</cfif>

	<!--- Reload page as an editor now --->
	<cfif RetAddMLP.RXRESULTCODE EQ 1>
		<cflocation url="mlpx-edit?ccpxDataId=#RetAddMLP.CPPID#" addToken="no" />
	<cfelse>
		<cflocation url="mlp-list" addToken="no" />
	</cfif>

<cfelse>

	<!--- Read exisitng MLP data --->
	<cfinvoke component="session.sire.models.cfc.mlp" method="ReadCPP" returnvariable="RetCPPXData">
		 <cfinvokeargument name="ccpxDataId" value="#ccpxDataId#">
	</cfinvoke>

</cfif>

<cfoutput>#RetCPPXData.CUSTOMCSS#</cfoutput>

<!--- Custom dropzone.js previewTemplate - specify in options on binding the obj --->
<cfsavecontent variable="MLPPreviewTemplate">
	<cfoutput>

		<div class="dz-preview dz-file-preview" style="line-height: 1em;">
		  <div class="dz-details">
		    <div class="dz-filename"><span data-dz-name style="margin-right: 2em;"></span> <span data-dz-size></span></div>
	    		<img data-dz-thumbnail style="margin-top: 2em;" />
			</div>

			<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
			<div class="dz-error-message"><span data-dz-errormessage></span></div>
		</div>

	</cfoutput>
</cfsavecontent>

<cfsavecontent variable="MLPSplitTemplate">
	<cfoutput>

		<div class="MLPMasterSectionContainer row sortable sortable-item connectedSortable" style="margin: 1em;">

			<div class="MLPMasterSection MLPMasterRowEmpty MLPEditable droppable sortable-item col-xs-6" data-auto-margin="0 auto" contenteditable="false" style="text-align: center; min-height: 4em;" data-helper-text="Box Container">

				&nbsp;

				<div class="MLPEditMenu">

					<span style="min-width:120px; font-size: 1em;" contenteditable="false">
						<i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>
						<i class="fa fa-arrows mlp-icon mlp-icon-drag"></i>
						<i class="fa fa-remove mlp-icon-alert" data-tooltip="Are you sure?" style=""></i>
						<i class="fa fa-columns mlp-icon mlp-icon-split-section"></i>
					</span>

				</div>

			</div>

			<div class="MLPMasterSection MLPMasterRowEmpty MLPEditable droppable sortable-item col-xs-6" data-auto-margin="0 auto" contenteditable="false" style="text-align: center; min-height: 4em;" data-helper-text="Box Container">

				&nbsp;

				<div class="MLPEditMenu">

					<span style="min-width:120px; font-size: 1em;" contenteditable="false">
						<i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>
						<i class="fa fa-arrows mlp-icon mlp-icon-drag"></i>
						<i class="fa fa-remove mlp-icon-alert" data-tooltip="Are you sure?" style=""></i>
						<i class="fa fa-columns mlp-icon mlp-icon-split-section"></i>
					</span>

				</div>

			</div>

		</div>

	</cfoutput>
</cfsavecontent>


<cfinclude template="mlpx-widgets/mlp-headline.cfm" />
<cfinclude template="mlpx-widgets/mlp-line.cfm" />
<cfinclude template="mlpx-widgets/mlp-text.cfm" />
<cfinclude template="mlpx-widgets/mlp-hero.cfm" />
<cfinclude template="mlpx-widgets/mlp-img.cfm" />
<cfinclude template="mlpx-widgets/mlp-section.cfm" />
<cfinclude template="mlpx-widgets/mlp-row.cfm" />
<cfinclude template="mlpx-widgets/mlp-article-one.cfm" />
<cfinclude template="mlpx-widgets/mlp-section-three.cfm" />
<cfinclude template="mlpx-widgets/mlp-owl.cfm" />


<!-- Define content -->
<div class="wrapper" id="mlp-builder">

	<!--- Top Menu --->
	<header class="header">

		<div style="display: table; width: 100%; ">
			<!-- BEGIN LOGO -->
            <div style="display: table-cell; width: 1px; white-space: nowrap">
                <a href="/session/sire/pages/dashboard">
					<img src="/session/sire/assets/layouts/layout4/img/logo-be.png" alt="logo" class="logo-default" />
                </a>
            </div>
            <!-- END LOGO -->

			<div style="display: table-cell; padding-left: 3em; text-align: right; padding-right:2em;">
				<cfoutput>
            		<input type="hidden" id="ccpxDataId" name="ccpxDataId" value="#RetCPPXData['CPPID']#">
            		<input type="hidden" id="action" name="action" value="#action#">
					<input type="hidden" class="form-control  validate[required,custom[noHTML],ajax[ajaxCPPNameAvailable]]" maxlength="255" id="cppxName" name="cppxName" value="#RetCPPXData['CPPNAME']#" data-prompt-position="topLeft:100" >

				    <span id="cppxNameDisplay" style="display: inline; width: 100%; box-sizing: border-box;">#RetCPPXData['CPPNAME']#</span>

        		</cfoutput>
			</div>

			<div style="display: table-cell; width: 1px; white-space: nowrap; padding-left: 3em; vertical-align: middle;" >
				<button type="button" id="Undo" class="mlp-btn" style="margin-right: 1em;">Undo</button>
			</div>

			<div style="display: table-cell; width: 1px; white-space: nowrap; padding-left: 1em; vertical-align: middle;" >
				<button type="button" id="Redo" class="mlp-btn" style="margin-right: 1em;">Redo</button>
			</div>

			<div style="display: table-cell; width: 1px; white-space: nowrap; padding-left: 1em; vertical-align: middle;" >
				<button type="button" id="SaveContent" class="mlp-btn" style="margin-right: 1em;">Save</button>
			</div>

		</div>

	</header>

	<section class="content">
    	<div class="columns">

			<div class="sidebar-first">

			  <!--- Side Menu - widgets, layout, style, tracking options  --->
				<ul class="side-nav">

					<li class="menu-item widgets panel-active">
						<i class="fa fa-cogs mlp-icon"></i>
						<p>Widgets</p>
					</li>

					<li class="menu-item styles">
						<i class="fa fa-code mlp-icon"></i>
						<p>Coder</p>
					</li>

					<cfif action NEQ "template" AND  action NEQ "add">
						<li class="menu-item mlp-history">
							<i class="fa fa-history mlp-icon"></i>
							<p>History</p>
						</li>
					</cfif>

					<li class="menu-item publish-mlp">
						<i class="fa fa-share-square mlp-icon"></i>
						<p>Publish</p>
					</li>

					<cfif action NEQ "template" AND  action NEQ "add" AND tocid NEQ -1>
						<li id="mlp-toc" class="menu-item mlp-toc">
							<i class="fa fa-th-list mlp-icon"></i>
							<p>T O C</p>
						</li>
					</cfif>


				</ul>

			</div>

	      	<!--- Menu panels --->
		  	<div class="sidebar-second">

			    <i class="fa fa-angle-double-left panel-close-button"></i>

			  	<div class="tools-panel widgets-panel">

		<!--- 			<h4>Widgets go here...</h4> --->

					<div class="tile-item-container">
						<div class="tile-item draggable mlp-master-section"> <i class="fa fa-columns tile-item-icon"></i> <span class="tile-item-label">Section</span> </div>
					</div>

					<div class="tile-item-container">
						<div class="tile-item draggable mlp-headline"> <i class="fa fa-heading tile-item-icon"></i> <span class="tile-item-label">Headline</span> </div>
					</div>

					<div class="tile-item-container">
						<div class="tile-item draggable mlp-text"> <i class="tile-item-icon">T</i> <span class="tile-item-label">Text</span> </div>
					</div>

					<div class="tile-item-container">
						<div class="tile-item draggable mlp-line"> <i class="tile-item-icon">---</i> <span class="tile-item-label">Line</span> </div>
					</div>

					<div class="tile-item-container">
						<div class="tile-item draggable mlp-img"> <i class="fa fa-image tile-item-icon"></i> <span class="tile-item-label">Image</span> </div>
					</div>

					<div class="tile-item-container">
						<div class="tile-item draggable mlp-hero"> <i class="fa fa-image tile-item-icon"></i> <span class="tile-item-label">Hero Section</span> </div>
					</div>

<!---
					<div class="tile-item-container">
						<div class="tile-item draggable mlp-article-one"> <i class="fa fa-gitlab tile-item-icon"></i> <span class="tile-item-label">Article 1</span> </div>
					</div>

					<div class="tile-item-container">
						<div class="tile-item draggable mlp-section-three"> <i class="fa fa-gitlab tile-item-icon"></i> <span class="tile-item-label">3 Col Section</span> </div>
					</div>

					<div class="tile-item-container">
						<div class="tile-item draggable mlp-owl"> <i class="fa fa-play tile-item-icon"></i> <span class="tile-item-label">Carousel</span> </div>
					</div>
--->
<!---

					<div class="tile-item-container">
						<div class="tile-item draggable mlp-section-template"> <i class="fa fa-columns tile-item-icon"></i> <span class="tile-item-label">Section Template</span> </div>
					</div>
--->

<!---
					<div class="tile-item-container">
						<div class="tile-item draggable mlp-row-3-1"> <i class="fa fa-columns tile-item-icon"></i> <span class="tile-item-label">3-1 Column Row</span> </div>
					</div>

					<div class="tile-item-container">
						<div class="tile-item draggable mlp-row-2-1"> <i class="fa fa-columns tile-item-icon"></i> <span class="tile-item-label">2-1 Column Row</span> </div>
					</div>
--->

					<!---
								<div class="tile-item-container">
									<div xdraggable="true" class="tile-item draggable MLPButton"> <i class="fa fa-hand-o-up tile-item-icon"></i> <span class="tile-item-label">Button</span> </div>
								</div>
					--->

					<!--- Buttons
						Popup - modal
						or
						submit form
					--->

				</div>


				<div id="MLPCoder" class="tools-panel coder-panel mlp-section-settings">

					<h5>Custom Global Header</h5>

					<div>
						<button type="button" id="RawCSSEdit" class="mlp-btn" style="margin-right: 1em; display: block;">Edit</button>
						<textarea id="inpRawCSS" readonly style="width: 100%; min-height:20em; max-width: 100%; overflow: auto; margin-top: .5em; border-radius: 4px;"><cfoutput>#RetCPPXData.CUSTOMCSS#</cfoutput></textarea>
					</div>

					<hr>

					<h5>Raw HTML</h5>

					<div>
						<button type="button" id="mlp-coder" class="mlp-btn" style="margin-right: 1em; display: block;">Edit</button>
						<textarea id="inpRawHTML" readonly style="width: 100%; min-width:100%; min-height:20em; max-width: 100%; overflow: auto; margin-top: .5em; border-radius: 4px; white-space: pre;"><cfoutput>#RetCPPXData.RAWCONTENT#</cfoutput></textarea>
					</div>

					<hr/>

					<div style="padding-top: .5em;">
						<span class="section-label">Social</span><button type="button" id="Meta-Tag-Editor" class="mlp-btn Meta-Tag-Editor-Modal" style="margin-right: 1em;" data-toggle="modal" href="#" data-target="#Meta-Tag-Editor-Modal">Social</button>

						<h6 id="mlp-background-image-url" style="word-break: break-all;"></h6>
					</div>

				</div>


				<cfif tocid NEQ -1>



					<div id="mlp-toc-panel" class="tools-panel mlp-section-settings">


						<!--- Table of Contents is top level object  --->

						<!--- Topics - parent = TOC  - Sub topic parent is TOC OR subtopic - allows nesting

							1=TOC
							2=Topic



						--->

						<cfinvoke component="session.sire.models.cfc.mlp-toc" method="ReadMLPTOCDescription" returnvariable="RetValReadMLPTOCDescription">
							 <cfinvokeargument name="inpTOCId" value="#tocid#">
						</cfinvoke>

						<h5>Table of Contents</h5>
						<h6><cfoutput>#RetValReadMLPTOCDescription.DESC#</cfoutput></h6>

							<cfif tocid EQ 0>

								<div style="padding-top: .5em;">

									<select id="TOCList" class="Select2" style="width:100%;">
										<option value="0" selected>-- Loading Data ... --</option>
									</select>
								</div>

								<div style="padding-top: .5em;">
									<button type="button" id="trigger-MLP-TOC-Add-Modal" class="mlp-btn" style="margin-right: 1em;" data-toggle="modal" href="#" data-target="#MLP-TOC-Add-Modal">Add TOC</button>

									<h6 id="mlp-background-image-url" style="word-break: break-all;"></h6>
								</div>

							</cfif>

						<hr/>

						<div class="TopLevelParent">

							<div class="MLP-Top-Topic">

								<div class="MLPSubTopicsSection sortable-topic-container top-sortable-topic-container">

								</div>

							</div>


						</div>

					</div>

				</cfif>


				<div class="tools-panel publish-mlp-panel">

			  		<cfif action NEQ "template" AND  action NEQ "add">

						<cfoutput>

							<label id="MLPURLLable" for="cppxURL" class=""><div>Custom {URL key}:</div></label>
							<input type="text" class="form-control" maxlength="255" id="cppxURL" name="cppxURL" value="#RetCPPXData['CPPURL']#" data-prompt-position="topLeft:100" data-container="body" data-toggle="popover-manual" data-placement="top" data-content="">
							<h5>https://www.mlp-x.com/<span class="mlp-url-key">#RetCPPXData['CPPURL']#</span></h5>
						   	<div id="MLPURLStatus" style="font-size: .8em; color: red; min-height: 3em;">&nbsp;</div>

							<!--- Allow MLP to expire - if served in javascript iframe loader, then expired will not show by default --->
							<h5>Expire MLP Date</h5>
							<div><input type="text" id="Expire_dt" value="#RetCPPXData.EXPIRE_DT#" style="color:##555; padding: .2em;"></div>

							<hr/>

							<!--- This script embed method will allow us to look for expiration and any setup variables we want to control --->
							<label id="MLPURLLable" for="cppxURL" class=""><div>Embed Code {script}:</div></label>
							<h6 style="background-color: ##777; padding: .5em; border-radius: .5em;">&lt;script src="https://mlp-x.com/lz/js/loadmlp.min.js" data-mlp-id="<span class="mlp-url-key">#RetCPPXData['CPPURL']#</span>" data-mlp-h="200px" data-mlp-w="100%" &gt;&lt;/script&gt;</h6>

							<hr>

							<label id="MLPURLLable" for="cppxURL" class=""><div>Embed Code {iframe}:</div></label>
							<h6 style="background-color: ##777; padding: .5em; border-radius: .5em;">&lt;iframe src="https://mlp-x.com/<span class="mlp-url-key">#RetCPPXData['CPPURL']#</span>" scrolling="auto" frameborder="0" id="CPPReviewFrame" width="100%" height="100%"&gt;&lt;/iframe&gt;</h6>

						</cfoutput>

					<cfelse>
						<!--- Leave an empty hidden so dateformat stuff work s on save for templates --->
						<input type="hidden" id="Expire_dt" value="">
					</cfif>

						<hr/>

						<h5>Social</h5>

						<div style="padding-top: .5em;">
							<button type="button" id="Meta-Tag-Editor" class="mlp-btn Meta-Tag-Editor-Modal" style="margin-right: 1em;" data-toggle="modal" href="#" data-target="#Meta-Tag-Editor-Modal">Social Tags</button>

							<h6 id="mlp-background-image-url" style="word-break: break-all;"></h6>
						</div>

					<cfif action NEQ "template" AND  action NEQ "add">

						<cfoutput>
							<hr>

						  	<h5>Preview</h5>


						  	<div style="display: table; width: 100%;">

							  	<div id="preview-icon-desktop" class="preview-icon" style="display: table-cell; width: 33%; text-align: center;" data-height=720 data-width=1080 data-hdt=0>
								  	<i class="fa fa-desktop" style="font-size: 2em;"></i>
							  		<div class="tile-item-label">Screen</div>
							  	</div>

							  	<div id="preview-icon-tablet" class="preview-icon" style="display: table-cell; width: 33%; text-align: center;" data-height=1024 data-width=703 data-hdt=0>
								  	<i class="fa fa-tablet" style="font-size: 2em;"></i>
								  	<div class="tile-item-label">Tablet</div>
							  	</div>

							  	<div id="preview-icon-mobile" class="preview-icon" style="display: table-cell; width: 33%; text-align: center;" data-height=667 data-width=375 data-hdt=0>
								  	<i class="fa fa-mobile" style="font-size: 2em;"></i>
								  	<div class="tile-item-label">Phone</div>
							  	</div>

						  	</div>

						</cfoutput>

					   <hr/>

					   <div>
						    <h5>Publish Latest Changes</h5>
					   		<button type="button" id="PublishMLP" class="mlp-btn" style="margin-right: 1em; display: block;">Publish</button>

					   		<h6>Count saves since last published<br/><i id="CountSavedSinceLastPublished">loading...</i></h6>
					   		<h6>Last Published Date<br/><i id="LastPublishedDate">loading...</i></h6>
					   		<h6>Last Saved Date<br/><i id="LastSavedDate">loading...</i></h6>

					   		<cfif action NEQ "template" AND  action NEQ "add">
							   <div>
								   <span class="tile-item-label" style="margin-right: 1em;">Auto Publish Saved Changes</span>
								   <input id="AutoPublish" type="checkbox" class="js-switch" <cfif RetCPPXData.AUTOPUBLISH GT 0>checked</cfif> />
							   </div>

							<cfelse>
								<input type="hidden" id="AutoPublish" value="1" />
							</cfif>

					   </div>

					</cfif>

				</div>

				<div id="mlp-history-panel" class="tools-panel mlp-history-panel">

			  		<cfif action NEQ "template" AND  action NEQ "add">

					   <h5>Revision History</h5>


					    <div class="table-responsive">
				            <table id="tblListHistory" class="table table-striped table-bordered xmlp-list-table" style="table-layout:fixed">

				            </table>
				        </div>

					</cfif>

				</div>

				<div id="mlp-section-settings" class="tools-panel mlp-section-settings">

					<h4>Section Settings</h4>

					<div style="overflow-y: auto; overflow-x:hidden; position: relative; height: calc(100vh - 72px); width: 100%;">

					  	<div id="section-delete-container" style="text-align: right;"><i id="section-delete" class="fa fa-trash"></i></div>

				  		<div>
					  		<hr/>

					  		<span class="section-label">Name</span><input type="text" id="section-id" value="Generic Section" class="form-control" style="display: inline; width: 80%;" />

				  		</div>

				  		<hr/>

						<div>

							<h5>Background Color</h5>

							<div>
								<span class="section-label">Color</span><input type="text" id="background-color" value="#FFFFFF" />
							</div>


							<h5 id="background-image-toggle">Background Image <i class="far fa-caret-square-down mlp-icon"></i></h5>

							<div id="background-image-section">

								<div style="padding-top: .5em;">
									<span class="section-label">Image</span><button type="button" id="ImagePicker" class="mlp-btn ImageChooserModal" style="margin-right: 1em;" data-toggle="modal" href="#" data-target="#ImageChooserModal" data-image-target="background-image">Choose Image</button>

									<h6 id="mlp-background-image-url" style="word-break: break-all;"></h6>
								</div>

								<div style="margin-top:1em;">

									<span class="section-label">Repeat Background Image</span>
									<select name="background-repeat" id="background-repeat" class="Select2">
										<option value="repeat">repeat</option>
										<option value="repeat-x">repeat-x</option>
										<option value="repeat-y">repeat-y</option>
										<option value="no-repeat">no-repeat</option>
										<option value="initial">initial</option>
										<option value="inherit">inherit</option>
									</select>

								</div>

								<div style="margin-top:1em;">

									<span class="section-label">Position Background Image</span>
									<select name="background-position" id="background-position" class="Select2">
										<option value="left top">left top</option>
										<option value="left center">left center</option>
										<option value="left bottom">left bottom</option>
										<option value="right top">right top</option>
										<option value="right center">right center</option>
										<option value="right bottom">right bottom</option>
										<option value="center top">center top</option>
										<option value="center center">center center</option>
										<option value="center bottom">center bottom</option>
										<option value="0% 0%">0% 0%</option>
										<option value="initial">initial</option>
										<option value="inherit">inherit</option>
									</select>

								</div>

								<div style="margin-top:1em;">

									<span class="section-label">Size Background Image</span>
									<select name="background-size" id="background-size" class="Select2">
										<option value="auto">auto</option>
										<option value="cover">cover</option>
										<option value="contain">contain</option>
										<option value="25%">25%</option>
										<option value="50%">50%</option>
										<option value="75%">75%</option>
										<option value="100%">100%</option>
										<option value="initial">initial</option>
										<option value="inherit">inherit</option>
									</select>

								</div>

								<div style="margin-top:1em;">

									<span class="section-label">Attachment Background Image</span>
									<select name="background-attachment" id="background-attachment" class="Select2">
										<option value="auto">scroll</option>
										<option value="cover">fixed</option>
										<option value="contain">local</option>
										<option value="initial" selected>initial</option>
										<option value="inherit">inherit</option>
									</select>

								</div>

							</div>

						</div>

				  		<hr/>

				  		<div>

					  		<h5>Alignment</h5>

					  		<div id="alignment" style="text-align: left;">
						  		<i class="fa fa-align-left" style="font-size: 2em;"></i>
						  		<i class="fa fa-align-center" style="font-size: 2em; margin-left: 1em;"></i>
						  		<i class="fa fa-align-right" style="font-size: 2em; margin-left: 1em;"></i>
						  		<i class="far fa-circle" style="font-size: 2em; margin-left: 1em;"></i>
					  		</div>

				  		</div>

				 		<hr/>

				 		<div>

					  		<h5>Font Options</h5>

					  		<div>
								<span class="section-label">Color</span><input type="text" id="color" value="#FFFFFF" />
							</div>

							<div>
								<span class="section-label">Style</span>
								<span id="bold" class="font-style">Bold</span>
								<span id="italic" class="font-style" style="display: inline; margin-left: 1em;">Italic</span>
							</div>

				  		</div>


				  		<hr/>

				  		<div>

							<h5>Padding</h5>

							<div class="row">

								<div class="col-xs-6">
									<div class="row">

										<div class="col-xs-6" style="text-align: right; text-transform: uppercase; font-size: .8em; padding: 0 3px; height: 2em; line-height: 2em;">top</div>
										<div class="col-xs-6" style="text-align: left; padding: 0 3px;">
											<input type="text" id="padding-top" value="1" style="width: 3em; color: #000000; text-align: center; border-radius: 1em; border: none;" />
										</div>

										<div class="col-xs-10 col-md-offset-1" style="margin-top:.5em;">
											<input type="range" min="0" max="150" value="3" id="padding-top-fader">
										</div>
									</div>

									<br/>

									<div class="row">

										<div class="col-xs-6" style="text-align: right; text-transform: uppercase; font-size: .8em; padding: 0 3px; height: 2em; line-height: 2em;">bottom</div>
										<div class="col-xs-6" style="text-align: left; padding: 0 3px;">
											<input type="text" id="padding-bottom" value="1" style="width: 3em; color: #000000; text-align: center; border-radius: 1em; border: none;" />
										</div>

										<div class="col-xs-10 col-md-offset-1" style="margin-top:.5em;">
											<input type="range" min="0" max="150" value="3" id="padding-bottom-fader">
										</div>
									</div>
								</div>

								<div class="col-xs-6">
									<div class="row">

										<div class="col-xs-6" style="text-align: right; text-transform: uppercase; font-size: .8em; padding: 0 3px; height: 2em; line-height: 2em;">left</div>
										<div class="col-xs-6" style="text-align: left; padding: 0 3px;">
											<input type="text" id="padding-left" value="1" style="width: 3em; color: #000000; text-align: center; border-radius: 1em; border: none;" />
										</div>

										<div class="col-xs-10 col-md-offset-1" style="margin-top:.5em;">
											<input type="range" min="0" max="150" value="3" id="padding-left-fader">
										</div>
									</div>

									<br/>

									<div class="row">
										<div class="col-xs-6" style="text-align: right; text-transform: uppercase; font-size: .8em; padding: 0 3px; height: 2em; line-height: 2em;">right</div>
										<div class="col-xs-6" style="text-align: left; padding: 0 3px;">
											<input type="text" id="padding-right" value="1" style="width: 3em; color: #000000; text-align: center; border-radius: 1em; border: none;" />
										</div>

										<div class="col-xs-10 col-md-offset-1" style="margin-top:.5em;">
											<input type="range" min="0" max="150" value="3" id="padding-right-fader">
										</div>
									</div>
								</div>

							</div>

							<hr style="border-top: dotted 1px;" />

							<h5>Margin <span style="text-align: right; float: right;">Auto <input type="checkbox" id="auto-margin" value="0"></span></h5>

							<div class="row">

								<div class="col-xs-6">
									<div class="row">

										<div class="col-xs-6" style="text-align: right; text-transform: uppercase; font-size: .8em; padding: 0 3px; height: 2em; line-height: 2em;">top</div>
										<div class="col-xs-6" style="text-align: left; padding: 0 3px;">
											<input type="text" id="margin-top" value="1" style="width: 3em; color: #000000; text-align: center; border-radius: 1em; border: none;" />
										</div>

										<div class="col-xs-10 col-md-offset-1" style="margin-top:.5em;">
											<input type="range" min="0" max="150" value="3" id="margin-top-fader">
										</div>
									</div>

									<br/>

									<div class="row">

										<div class="col-xs-6" style="text-align: right; text-transform: uppercase; font-size: .8em; padding: 0 3px; height: 2em; line-height: 2em;">bottom</div>
										<div class="col-xs-6" style="text-align: left; padding: 0 3px;">
											<input type="text" id="margin-bottom" value="1" style="width: 3em; color: #000000; text-align: center; border-radius: 1em; border: none;" />
										</div>

										<div class="col-xs-10 col-md-offset-1" style="margin-top:.5em;">
											<input type="range" min="0" max="150" value="3" id="margin-bottom-fader">
										</div>
									</div>
								</div>

								<div class="col-xs-6">
									<div class="row">

										<div class="col-xs-6" style="text-align: right; text-transform: uppercase; font-size: .8em; padding: 0 3px; height: 2em; line-height: 2em;">left</div>
										<div class="col-xs-6" style="text-align: left; padding: 0 3px;">
											<input type="text" id="margin-left" value="1" style="width: 3em; color: #000000; text-align: center; border-radius: 1em; border: none;" />
										</div>

										<div class="col-xs-10 col-md-offset-1" style="margin-top:.5em;">
											<input type="range" min="0" max="150" value="3" id="margin-left-fader">
										</div>
									</div>

									<br/>

									<div class="row">
										<div class="col-xs-6" style="text-align: right; text-transform: uppercase; font-size: .8em; padding: 0 3px; height: 2em; line-height: 2em;">right</div>
										<div class="col-xs-6" style="text-align: left; padding: 0 3px;">
											<input type="text" id="margin-right" value="1" style="width: 3em; color: #000000; text-align: center; border-radius: 1em; border: none;" />
										</div>

										<div class="col-xs-10 col-md-offset-1" style="margin-top:.5em;">
											<input type="range" min="0" max="150" value="3" id="margin-right-fader">
										</div>
									</div>
								</div>

							</div>

							<hr/>

						</div>

						<div>

							<h5>Border</h5>

							<div>
								<span class="section-label">Color</span><input type="text" id="border-color" value="#FFFFFF" />
							</div>

							<div style="margin-top:1em;">

								<span class="section-label">Border Style</span>
								<select name="border-style" id="border-style" class="Select2">
									<option value="none" selected="">none</option>
									<option value="hidden">hidden</option>
									<option value="dotted">dotted</option>
									<option value="dashed">dashed</option>
									<option value="solid">solid</option>
									<option value="double">double</option>
									<option value="groove">groove</option>
									<option value="ridge">ridge</option>
									<option value="inset">inset</option>
									<option value="outset">v</option>
								</select>

							</div>

							<div style="margin-top:1em;">
								<span class="section-label">Border Width</span><input type="text" id="border-width" value="1" style="width: 3em; color: #000000; text-align: center; border-radius: 1em; border: none;" />
							</div>

							<div style="margin-top:.5em;">
								<input type="range" min="0" max="150" value="3" id="border-width-fader">
							</div>

							<div style="margin-top:1em;">
								<span class="section-label">Border Radius</span><input type="text" id="border-radius" value="1" style="width: 3em; color: #000000; text-align: center; border-radius: 1em; border: none;" />
							</div>

							<div style="margin-top:.5em;">
								<input type="range" min="0" max="150" value="3" id="border-radius-fader">
							</div>


							<div style="margin-top:1em;">
								<span class="section-label">Arrow Border</span>

								<div id="arrow-border" style="text-align: left;">
							  		<i class="fa fa-caret-up" style="font-size: 2em;"></i>
							  		<i class="fa fa-caret-down" style="font-size: 2em; margin-left: 1em;"></i>
							  		<i class="far fa-circle" style="font-size: 2em; margin-left: 1em;"></i>
						  		</div>
							</div>

							<hr/>

						</div>

				  		<div>

					  		<h5 id="width-height-toggle">Width and Height <i class="far fa-caret-square-down mlp-icon"></i></h5>

							<div id="width-height-section">
								<h5>Width</h5>

								<div style="margin-top:1em;">

									<span class="section-label">Width Units</span>
									<select name="width-units" id="width-units" class="Select2">
										<option value="px" selected>px</option>
										<option value="%">%</option>
										<option value="em">em</option>
										<option value="rem">rem</option>
										<option value="vw">vw</option>
										<option value="vh">vh</option>
									</select>

								</div>

								<div style="margin-top:1em;">
									<span class="section-label">Width</span><input type="text" id="width" value="1" style="width: 3em; color: #000000; text-align: center; border-radius: 1em; border: none;" />
								</div>

								<div style="margin-top:.5em;">
									<input type="range" min="0" max="2500" value="0" id="width-fader">
								</div>

								<h5>Minimum Width</h5>

								<div style="margin-top:1em;">

									<span class="section-label">Minimum Width Units</span>
									<select name="min-width-units" id="min-width-units" class="Select2">
										<option value="px" selected>px</option>
										<option value="%">%</option>
										<option value="em">em</option>
										<option value="rem">rem</option>
										<option value="vw">vw</option>
										<option value="vh">vh</option>
									</select>

								</div>

								<div style="margin-top:1em;">
									<span class="section-label">Minimum Width</span><input type="text" id="min-width" value="1" style="width: 3em; color: #000000; text-align: center; border-radius: 1em; border: none;" />
								</div>

								<div style="margin-top:.5em;">
									<input type="range" min="0" max="2500" value="0" id="min-width-fader">
								</div>

								<h5>Maximum Width</h5>

								<div style="margin-top:1em;">

									<span class="section-label">Maximum Width Units</span>
									<select name="max-width-units" id="max-width-units" class="Select2">
										<option value="px" selected>px</option>
										<option value="%">%</option>
										<option value="em">em</option>
										<option value="rem">rem</option>
										<option value="vw">vw</option>
										<option value="vh">vh</option>
									</select>

								</div>

								<div style="margin-top:1em;">
									<span class="section-label">Maximum Width</span><input type="text" id="max-width" value="1" style="width: 3em; color: #000000; text-align: center; border-radius: 1em; border: none;" />
								</div>

								<div style="margin-top:.5em;">
									<input type="range" min="0" max="2500" value="0" id="max-width-fader">
								</div>

								<h5>Height</h5>

								<div style="margin-top:1em;">

									<span class="section-label">Height Units</span>
									<select name="height-units" id="height-units" class="Select2">
										<option value="px" selected>px</option>
										<option value="%">%</option>
										<option value="em">em</option>
										<option value="rem">rem</option>
										<option value="vw">vw</option>
										<option value="vh">vh</option>
									</select>

								</div>

								<div style="margin-top:1em;">
									<span class="section-label">Height</span><input type="text" id="height" value="1" style="width: 3em; color: #000000; text-align: center; border-radius: 1em; border: none;" />
								</div>

								<div style="margin-top:.5em;">
									<input type="range" min="0" max="2500" value="0" id="height-fader">
								</div>

								<h5>Minimum Height</h5>

								<div style="margin-top:1em;">

									<span class="section-label">Minimum Height Units</span>
									<select name="min-height-units" id="min-height-units" class="Select2">
										<option value="px" selected>px</option>
										<option value="%">%</option>
										<option value="em">em</option>
										<option value="rem">rem</option>
										<option value="vw">vw</option>
										<option value="vh">vh</option>
									</select>

								</div>

								<div style="margin-top:1em;">
									<span class="section-label">Minimum Height</span><input type="text" id="min-height" value="1" style="width: 3em; color: #000000; text-align: center; border-radius: 1em; border: none;" />
								</div>

								<div style="margin-top:.5em;">
									<input type="range" min="0" max="2500" value="0" id="min-height-fader">
								</div>

								<h5>Maximum Height</h5>

								<div style="margin-top:1em;">

									<span class="section-label">Maximum Height Units</span>
									<select name="max-height-units" id="max-height-units" class="Select2">
										<option value="px" selected>px</option>
										<option value="%">%</option>
										<option value="em">em</option>
										<option value="rem">rem</option>
										<option value="vw">vw</option>
										<option value="vh">vh</option>
									</select>

								</div>

								<div style="margin-top:1em;">
									<span class="section-label">Maximum Height</span><input type="text" id="max-height" value="1" style="width: 3em; color: #000000; text-align: center; border-radius: 1em; border: none;" />
								</div>

								<div style="margin-top:.5em;">
									<input type="range" min="0" max="2500" value="0" id="max-height-fader">
								</div>

				  			</div>

						</div>

							<hr/>

					  		<h5>Advanced</h5>

					  		<div style="margin-top:1em;">
						  		<span class="section-label">Position</span>
								<select name="position" id="position" class="Select2">
									<option value="static" selected>static</option>
									<option value="absolute">absolute</option>
									<option value="fixed">fixed</option>
									<option value="relative">relative</option>
									<option value="initial">initial</option>
									<option value="inherit">inherit</option>
								</select>
					  		</div>

					  		<h5>Padding</h5>

							<div class="row">

								<div class="col-xs-6">
									<div class="row">

										<div class="col-xs-6" style="text-align: right; text-transform: uppercase; font-size: .8em; padding: 0 3px; height: 2em; line-height: 2em;">top</div>
										<div class="col-xs-6" style="text-align: left; padding: 0 3px;">
											<input type="text" id="top" value="1" style="width: 3em; color: #000000; text-align: center; border-radius: 1em; border: none;" />
										</div>

										<div class="col-xs-10 col-md-offset-1" style="margin-top:.5em;">
											<input type="range" min="0" max="150" value="3" id="top-fader">
										</div>
									</div>

									<br/>

									<div class="row">

										<div class="col-xs-6" style="text-align: right; text-transform: uppercase; font-size: .8em; padding: 0 3px; height: 2em; line-height: 2em;">bottom</div>
										<div class="col-xs-6" style="text-align: left; padding: 0 3px;">
											<input type="text" id="bottom" value="1" style="width: 3em; color: #000000; text-align: center; border-radius: 1em; border: none;" />
										</div>

										<div class="col-xs-10 col-md-offset-1" style="margin-top:.5em;">
											<input type="range" min="0" max="150" value="3" id="bottom-fader">
										</div>
									</div>
								</div>

								<div class="col-xs-6">
									<div class="row">

										<div class="col-xs-6" style="text-align: right; text-transform: uppercase; font-size: .8em; padding: 0 3px; height: 2em; line-height: 2em;">left</div>
										<div class="col-xs-6" style="text-align: left; padding: 0 3px;">
											<input type="text" id="left" value="1" style="width: 3em; color: #000000; text-align: center; border-radius: 1em; border: none;" />
										</div>

										<div class="col-xs-10 col-md-offset-1" style="margin-top:.5em;">
											<input type="range" min="0" max="150" value="3" id="left-fader">
										</div>
									</div>

									<br/>

									<div class="row">
										<div class="col-xs-6" style="text-align: right; text-transform: uppercase; font-size: .8em; padding: 0 3px; height: 2em; line-height: 2em;">right</div>
										<div class="col-xs-6" style="text-align: left; padding: 0 3px;">
											<input type="text" id="right" value="1" style="width: 3em; color: #000000; text-align: center; border-radius: 1em; border: none;" />
										</div>

										<div class="col-xs-10 col-md-offset-1" style="margin-top:.5em;">
											<input type="range" min="0" max="150" value="3" id="right-fader">
										</div>
									</div>
								</div>

							</div>

					  		<div style="margin-top:1em;">
						  		<span class="section-label">Float</span>
								<select name="float" id="float" class="Select2">
									<option value="none" selected>none</option>
									<option value="left">left</option>
									<option value="right">right</option>
									<option value="initial">initial</option>
									<option value="inherit">inherit</option>
								</select>
					  		</div>

					  		<div style="margin-top:1em;">
							   <span class="tile-item-label" style="margin-right: 1em;">Clear Fix (for floats)</span>
							   <input id="ClearFix" type="checkbox" class="js-switch" />
						   	</div>

					</div>  <!--- mlp-section-setting inner overflow-y: auto --->

				</div>	<!--- mlp-section-settings --->

			</div> <!--- sidebar-second --->

			<main class="main">

				<div id="main-stage">

					<div id="main-stage-content" class="" style="padding-top: 0;" contenteditable="true">

						<!--- Make sure there is a master section --->
						<cfif find('MLP-Master-Section', RetCPPXData.RAWCONTENT) GT 0 >

							<cfoutput>#RetCPPXData.RAWCONTENT#</cfoutput>

						<cfelse>

							<!--- Mark stuff you dont want inplace editable to be deleted by accident on backspace or delete key - this is usually the outer container - will need ot add a gloabal content editable fix on the main mlp-x page to remove these options in final contenteditable="true" published page --->
							<div id="MLP-Master-Section" class="MLPMasterSection MLPEditable droppable sortable connectedSortable" data-auto-margin="0 0" contenteditable="false" style="text-align: center; margin: 0 auto;" data-helper-text="Main Container"  width-units="%" min-width-units="vw" max-width-units="px" height-units="%" min-height-units="vh" max-height-units="px" mlp-min-height="100" mlp-min-width="initial" mlp-width="100" mlp-height="100">

								<cfoutput>#RetCPPXData.RAWCONTENT#</cfoutput>

								<div class="MLPEditMenu">

									<span style="min-width:120px; font-size: 1em;" contenteditable="false">
										<i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>
									</span>

								</div>

							</div>


						</cfif>
					</div>

				</div>

			</main>

    	</div>

	</section>

</div>


<!--- Image Chooser --->
<div class="bootbox modal fade" id="ImageChooserModal" tabindex="-1" role="dialog" aria-labelledby="ImageChooserModal" aria-hidden="true" style="">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Choose Image</h4>
            </div>
            <div class="modal-body">

				<div class="tabbable">
				    <ul class="nav nav-tabs" id="myTabs">
				        <li class="active"><a href="#MLPUploadImage"  class="active" data-toggle="tab">Upload New Image</a></li>
				        <li><a href="/session/sire/models/cfm/imagebrowser-mlp.cfm" data-toggle="MLPImagePicker" data-target="#MLPImagePicker">Your Image Library</a></li>
				        <li><a href="#MLPImageLink"  class="active" data-toggle="tab3">External Link</a></li>
				    </ul>

				    <div class="tab-content">

				        <div class="tab-pane active" id="MLPUploadImage" style="min-height: 10em;">

					        <div class="row>">
								<div class="MLPDropZone" style="xmin-height:2em; text-align:center; line-height:6em; border:dashed 1px #ccc; padding-bottom: 3em;" contenteditable="false">Drop files here or click to upload.



								</div>
					        </div>

							<cfinvoke method="GetUserStorageInfo" component="session.sire.models.cfc.image" returnvariable="retValUserUsage">
							</cfinvoke>

							<cfif retValUserUsage.RXRESULTCODE GT 0>

								<cfset usagePercent = DecimalFormat((retValUserUsage.USERUSAGE/retValUserUsage.USERSTORAGEPLAN)*100) />

								<cfset UsageMBSize = retValUserUsage.USERUSAGE / 1048576>
								<cfset StoragePlanMBSize = retValUserUsage.USERSTORAGEPLAN / 1048576>

								<cfif UsageMBSize GT 1024>
									<cfset UsageMBSize = DecimalFormat(UsageMBSize / 1024)>
									<cfset UsageMBSize = UsageMBSize&"GB">
								<cfelse>
									<cfset UsageMBSize = DecimalFormat(UsageMBSize)&"MB">
								</cfif>

								<cfif StoragePlanMBSize GT 1024>
									<cfset StoragePlanMBSize = DecimalFormat(StoragePlanMBSize / 1024)>
									<cfset StoragePlanMBSize = StoragePlanMBSize&"GB">
								<cfelse>
									<cfset StoragePlanMBSize = DecimalFormat(StoragePlanMBSize)&"MB">
								</cfif>

								<cfoutput>

									<input type="hidden" id="inpUserUsage" value="#retValUserUsage.USERUSAGE#">
									<input type="hidden" id="inpUserStoragePlan" value="#retValUserUsage.USERSTORAGEPLAN#">

									<div class="row" style="padding-top:20px">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="">
												Account Storage Usage
											</div>
											<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
												<b><span id="userUsageSpan">#UsageMBSize#</span> / <span id="userStoragePlanSpan">#StoragePlanMBSize#</span></b>
											</div>
											<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
												<div class="progress">
													<div class="progress-bar progress-bar-striped progress-bar-success" role="progressbar" id="usageProgress"
														aria-valuemin="0" aria-valuemax="100" style="width:#usagePercent#%">
													</div>
												</div>
											</div>
										</div>
									</div>

								</cfoutput>
							</cfif>

						</div>

				        <div class="tab-pane" id="MLPImagePicker"></div>

				        <div class="tab-pane" id="MLPImageLink"  style="min-height: 10em;">

					        <div class="MLPDropZone" style="min-height:2em; text-align:left; line-height:1em;" contenteditable="false">Image Link (URL)</div>
					        <input type="text" id="MLPImageURL" value="" style="width: 100%; height: 1.5em;" />

				        </div>

				    </div>
				</div>

            </div>

            <div class="modal-footer">
                <button type="button" class="mlp-btn" id="btn-upload-image">Use This Image</button>
                <button type="button" class="mlp-btn" id="btn-use-image-link">Use Specified Link</button>
                &nbsp; &nbsp; &nbsp;
                <button type="button" class="mlp-btn" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>

</div>

<cfif action NEQ "template" AND  action NEQ "add">
	<!--- Preview --->
	<div class="bootbox modal fade" id="PreviewModal" tabindex="-1" role="dialog" aria-labelledby="PreviewModal" aria-hidden="true" style="">

	    <div class="modal-dialog" role="document" style="">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title" id="exampleModalLabel">Preview MLP</h4>
	            </div>
	            <div class="modal-body">

					<iframe src="/mlp-x/lz/<cfoutput>#RetCPPXData['CPPURL']#</cfoutput>" scrolling="auto" frameborder="0" id="CPPReviewFrame" width="768px" height="1004px" class="scaled-frame"></iframe>

	            </div>

	            <div class="modal-footer">

		            <input type="hidden" id="inpHDT" value="0"/>
	                <button type="button" id="RevertHistorical" class="mlp-btn" data-dismiss="modal">Revert</button>
	                <button type="button" class="mlp-btn" data-dismiss="modal">Cancel</button>
	            </div>
	        </div>
	    </div>

	</div>
</cfif>

<!--- Coder --->
<div class="bootbox modal fade" id="CoderModal" tabindex="-1" role="dialog" aria-labelledby="CoderModal" aria-hidden="true" style="">

    <div class="modal-dialog" role="document" style="">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Coder - Raw HTML</h4>
            </div>
            <div class="modal-body" style="overflow: auto;">

        		<h5>Edit HTML here.</h5>
        		<h6>***Note: For Coders and Developers Only! You are on your own to experiment with this feature. Your page will potentially not work as expected if you do anything unexpected here. Be sure to test your changes.</h6>
				<textarea id="RawCode" style="min-width: 100%; min-height: 60%; overflow: auto; -webkit-overflow-scrolling: touch; <!--- min-width: 1920px; min-height: 60%;  --->"></textarea>

            </div>

            <div class="modal-footer">
                <button type="button" id="btn-save-raw" class="mlp-btn" data-dismiss="modal">Save</button>
                <button type="button" class="mlp-btn" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>

</div>

<!--- Advanced CSS --->
<div class="bootbox modal fade" id="CSSModal" tabindex="-1" role="dialog" aria-labelledby="CSSModal" aria-hidden="true" style="">

    <div class="modal-dialog" role="document" style="">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Global CSS - Raw</h4>
            </div>
            <div class="modal-body" style="">

        		<h5>Edit CSS here.</h5>
        		<h6>***Note: For Coders and Developers Only! You are on your own to experiment with this feature. Your page will potentially not work as expected if you do anything unexpected here. Be sure to test your changes.</h6>
				<textarea id="RawCSS" style="min-width: 100%; min-height: 60%; overflow-y: auto;"></textarea>

            </div>

            <div class="modal-footer">
                <button type="button" id="btn-save-raw-css" class="mlp-btn" data-dismiss="modal">Save</button>
                <button type="button" class="mlp-btn" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>

</div>


<!--- Section Template Picker CSS --->
<div class="bootbox modal fade" id="Section-Template-Picker" tabindex="-1" role="dialog" aria-labelledby="Section-Template-Picker" aria-hidden="true" style="">

    <div class="modal-dialog" role="document" style="">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Section-Template-Picker</h4>
            </div>

            <div class="modal-body" style="">

       		   <!--- List possible templates here --->
       		   <a href="#" id="MLP-Template-Simple-Section">Simple Section</a>

            </div>

            <div class="modal-footer">
                <button type="button" class="mlp-btn" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>

</div>


<!--- MEta Tag Editor CSS --->
<div class="bootbox modal fade" id="Meta-Tag-Editor-Modal" tabindex="-1" role="dialog" aria-labelledby="Meta-Tag-Editor-Modal" aria-hidden="true" style="">

    <div class="modal-dialog" role="document" style="">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Social Tags</h4>
            </div>

            <div class="modal-body" style="">

        		<!--- https://moz.com/blog/meta-data-templates-123 --->

        		<div>
					<h3>Google</h3>
					<p>
					    Add some more of that SEO goodness to your MLP.
					</p>

					<div class="social-tags-section">

						<div>
							<p class="social-label">Page Title - This will be displayed in tabs, as well as some Social sharing.</p>
							<input class="social-tags" type="text" id="google-title" name="google-description" placeholder="Page Title">
						</div>

						<div>
							<p class="social-label">It's important to give your site a good description. This will be displayed in search engine results, as well as some Social sharing.</p>
							<input class="social-tags" type="text" id="google-description" name="google-description" placeholder="A brief description of the content.">
						</div>

						<div>
							<p class="social-label">The URL for your Google+ personal profile page.</p>
						    <input class="social-tags" type="text" id="google-author" name="google-author" placeholder="The URL for your Google+ personal profile page.">
						</div>

						<div>
							<p class="social-label">(Optional) This should be the URL of your Google+ Business page. Not a personal profile page as above.</p>
						    <input class="social-tags" type="text" id="google-publisher" name="google-publisher" placeholder="This should be the URL of your Google+ Business page.">
						</div>

						<div>
							<p class="social-label">(Optional) You may add this copyright tag to all of your web-pages,</p>
						    <input class="social-tags" type="text" id="google-copyright" name="google-copyright" placeholder="Copyright notice - who owns this content.">
						</div>

						<div>
							<p class="social-label">Keywords list - Pretty much ignored these days. Keywords are now pulled from actual text found on the page.</p>
						</div>

					</div>

				</div>

       		  	<div>
					<h3>Facebook</h3>
					<p>
					    A lot of content is shared via Facebook, so filling out these fields will ensure your page looks great when people share a link to your home page on Facebook.
					    <a href="https://developers.facebook.com/docs/sharing/best-practices" target="_blank">Read more about Facebook's best sharing practices here.</a>
					</p>

					<div class="social-tags-section">
						<div>
							<p class="social-label">The title of your article, excluding any branding. Don't use your website name. Think of a catchy headline.</p>
							<input class="social-tags" type="text" id="og-title" name="og-title" placeholder="The title of your article, excluding any branding.">
						</div>

						<div>
							<p class="social-label">A brief description of the content, usually between 2 and 4 sentences. This will displayed below the title of the post on Facebook.</p>
							<input class="social-tags" type="text" id="og-description" name="og-description" placeholder="A brief description of the content.">
						</div>

						<div>
							<p class="social-label">The name of your website. Not the URL, but the name. (i.e. "Google" not "google.com".)</p>
						    <input class="social-tags" type="text" id="og-name" name="og-name" placeholder="The name of your website, not the URL.">
						</div>

						<div>
							<p class="social-label">This is an image associated with your media. We suggest that you use an image of at least 1200x630 pixels.</p>
						    <input class="social-tags" type="text" id="og-image" name="og-image" placeholder="A link to an image you want displayed when your page is shared.">
						</div>

						<div>
							<p class="social-label">(Optional)The type of media of your content. This tag impacts how your content shows up in News Feed.  If you don't specify a type, the default is <code>website</code>. Each URL should be a single object, so multiple <code>og:type</code> values are not possible. Find the full list of object types in our <a href="/docs/reference/opengraph#object-type">Object Types Reference</a></p>
						    <input class="social-tags" type="text" id="og-type" name="og-type" placeholder="The type of media of your content. Default is WebSite">
						</div>

						<div>
							<p class="social-label">(Optional)The locale of the resource. The default is en_US. Format is ll_cc where "ll" is language and "cc" is Country.</p>
						    <input class="social-tags" type="text" id="og-locale" name="og-locale" placeholder="Your locale. Use en_US if unsure.">
						</div>

						<div>
							<p class="social-label">(Optional) The unique ID that lets Facebook know the identity of your site. This is crucial for Facebook Insights to work properly. See <a href="https://developers.facebook.com/docs/insights" target="_blank">Facebooks Insights Documentation</a> for more details.</p>
						    <input class="social-tags" type="text" id="og-id" name="og-id" placeholder="">
						</div>

					</div>

				</div>

				<div>
					<h3>twitter</h3>
					<p>
					    Twitter offers 'Summary cards' to enable better sharing of content. Please fill out the tags if you want your link to look great in Twitter shares. When live on your MLP, you can validate at <a href="https://cards-dev.twitter.com/validator" target="_blank">Validate twitter card</a>
					</p>

					<div class="social-tags-section">

						<div>
							<p class="social-label">twitter card  - default type is summary.  For more info see: <a href="https://dev.twitter.com/cards/types" target="_blank">the twitter card types</a></p>
						    <input class="social-tags" type="text" id="twitter-card" name="twitter-card" placeholder="The twitter card type" value="summary">
						</div>

						<div>
							<p class="social-label">The title of your article, excluding any branding. Don't use your website name. Think of a catchy headline.</p>
							<input class="social-tags" type="text" id="twitter-title" name="twitter-title" placeholder="The title of your article, excluding any branding.">
						</div>

						<div>
							<p class="social-label">A brief description of the content, usually between 2 and 4 sentences. A description that concisely summarizes the content as appropriate for presentation within a Tweet. You should not re-use the title as the description or use this field to describe the general services provided by the website.</p>
							<input class="social-tags" type="text" id="twitter-description" name="twitter-description" placeholder="A brief description of the content.">
						</div>

						<div>
							<p class="social-label">The Twitter @username the card should be attributed to. <a href="https://dev.twitter.com/cards/overview" target="_blank">For more info see the twitter card overview</a></p>
						    <input class="social-tags" type="text" id="twitter-name" name="twitter-name" placeholder="Your Twitter username (include the @)">
						</div>

						<div>
							<p class="social-label">The URL for your MLP page.</p>
						    <input class="social-tags" type="text" id="twitter-url" name="twitter-url" placeholder="The URL for your MLP page.">
						</div>

						<div>
							<p class="social-label">The image must be a minimum size of 120px by 120px and must be less than 1MB in file size. The image will be cropped to a square on all platforms.</p>
						    <input class="social-tags" type="text" id="twitter-image" name="twitter-image" placeholder="A link to an image you want displayed when your link is shared.">
						</div>

						<div>
							<p class="social-label">A text description of the image conveying the essential nature of an image to users who are visually impaired.</p>
						    <input class="social-tags" type="text" id="twitter-image" name="twitter-image-alt" placeholder="A text description of the image">
						</div>

					</div>
				</div>

            </div>

            <div class="modal-footer">
                <button type="button" id="btn-save-meta-tags" class="mlp-btn" data-dismiss="modal">Save</button>
                <button type="button" class="mlp-btn" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>

</div>


<!--- Add new TOC --->
<div class="bootbox modal fade" id="MLP-TOC-Add-Modal" tabindex="-1" role="dialog" aria-labelledby="MLP-TOC-Add-Modal" aria-hidden="true" style="">

    <div class="modal-dialog" role="document" style="">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Add New TOC</h4>
            </div>

            <div class="modal-body" style="">

    			<label for="New-TOC-Name">
    				New TOC Name:*
    			</label>

				<div>
					<input id="New-TOC-Name" class="" maxlength="255" style="width:100%;" >
				</div>

            </div>

             <div class="modal-footer">
                <button type="button" id="btn-save-mlp-toc" class="mlp-btn" data-dismiss="modal">Save</button>
                <button type="button" class="mlp-btn" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>

</div>

<!--- Add new Topic --->
<div class="bootbox modal fade" id="MLP-Add-Topic-Modal" tabindex="-1" role="dialog" aria-labelledby="MLP-Add-Topic-Modal" aria-hidden="true" style="">

    <div class="modal-dialog" role="document" style="">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Add New Topic</h4>
            </div>

            <div class="modal-body" style="">

    			<label for="New-Topic-Name">
    				New Topic Name:*
    			</label>

				<div>
					<input id="New-Topic-Name" class="" maxlength="255" style="width:100%;" >
				</div>

            </div>

             <div class="modal-footer">
                <button type="button" id="btn-save-mlp-topic" class="mlp-btn" data-dismiss="modal">Save</button>
                <button type="button" class="mlp-btn" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>

</div>

<!--- Add new Topic --->
<div class="bootbox modal fade" id="MLP-Topic-Template-Picker-Modal" tabindex="-1" role="dialog" aria-labelledby="MLP-Topic-Template-Picker-Modal" aria-hidden="true" style="">

    <div class="modal-dialog" role="document" style="">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Select an MLP Template</h4>
            </div>

            <div id="MLP-Topic-Template-Picker-Modal-Modal-Body" class="modal-body" style="overflow: auto;">

				<cfinclude template="mlp-template-picker-inc.cfm" />

            </div>

             <div class="modal-footer">
                <button type="button" id="btn-new-mlp-topic-link" class="mlp-btn" data-dismiss="modal">Save</button>
                <button type="button" class="mlp-btn" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>

</div>


<!--- Possible Todo: Minfy js output on load - so data send to browser is smaller/lighter/faster - Test: Will server side delays offset client side delays of extra data--->
<cfinclude template="../assets/pages/scripts/mlpx-js.cfm" />
