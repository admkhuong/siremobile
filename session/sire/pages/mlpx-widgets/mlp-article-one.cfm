<cfsavecontent variable="MLPArticleOneTemplate">
	<cfoutput>

		<!--- Mark stuff you dont want inplace editable to be deleted by accident on backspace or delete key - this is usually the outer container - will need ot add a gloabal content editable fix on the main mlp-x page to remove these options in final contenteditable="true" published page --->
		 <div class="MLPEditable draggable sortable-item" data-helper-text="Article Style One Container">


			<div id="what-we-do" class="" contenteditable="true">

				<div class="what-we-do-features margin-100-bottom">
	                 <h3><span><i class="fa fa-diamond" aria-hidden="true"></i></span>Content Development</h3>

	                 <p>Lorem Ipsum is simulated dummy text of the printing and typesetting Donec pulvinar mi sit amet viverra justo hendrerit.</p>
	            </div>

			</div>

			<div class="MLPEditMenu">

				<span style="min-width:120px; font-size: 1em;" contenteditable="false">
					<i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>
					<i class="fa fa-arrows-alt mlp-icon mlp-icon-drag"></i>
					<i class="fa fa-trash-alt mlp-icon-alert" data-tooltip="Are you sure?" style=""></i>
				</span>

			</div>
		</div>

	</cfoutput>
</cfsavecontent>
