<cfsavecontent variable="MLPHeadlineTemplate">
	<cfoutput>

		<!--- Mark stuff you dont want inplace editable to be deleted by accident on backspace or delete key - this is usually the outer container - will need ot add a gloabal content editable fix on the main mlp-x page to remove these options in final contenteditable="true" published page --->
		<div class="MLPEditable draggable sortable-item" contenteditable="false" data-helper-text="Headline Container">
			<h1 contenteditable="true">New Headline</h1>

			<div class="MLPEditMenu" contenteditable="false">

				<span style="min-width:120px; font-size: 1em;" contenteditable="false">
					<i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>
					<i class="fa fa-arrows-alt mlp-icon mlp-icon-drag"></i>
					<i class="fa fa-trash-alt mlp-icon-alert" data-tooltip="Are you sure?" style=""></i>
				</span>

			</div>
		</div>

	</cfoutput>
</cfsavecontent>
