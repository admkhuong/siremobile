<cfsavecontent variable="MLPHeroTemplate">
	<cfoutput>

		<!--- Mark stuff you dont want inplace editable to be deleted by accident on backspace or delete key - this is usually the outer container - will need ot add a gloabal content editable fix on the main mlp-x page to remove these options in final contenteditable="true" published page --->

		 	<!--- wallpaper-2846361.jpg --->
		 	<!--- dreamstime_xl_24928711.jpg --->

		 	<!--- Add options for background-attachment: fixed; background-repeat: none;  --->

		 	<div id="mlp-hero" data-helper-text="Hero Container" class="MLPMasterSection draggable droppable MLPEditable sortable-item" style="background-image:url(https://www.siremobile.com/public/sire/images/mlp-x/wallpaper-2846361.jpg); background-size:cover; position:relative; height:100vh; background-attachment: fixed; background-repeat: no-repeat; ">

			    <div class="MLPEditMenu">
					<span style="min-width:120px;" contenteditable="false"> <i class="fa fa-cogs mlp-icon mlp-icon-settings"></i> <i class="fa fa-arrows-alt mlp-icon mlp-icon-drag"></i> <i class="fa fa-trash-alt mlp-icon-alert" data-tooltip="Are you sure?"></i>	    </span>
				</div>

			    <div class="mlp-header-hero MLPMasterSection droppable MLPEditable" style="position: absolute; top: 50%; text-align: center; width: 100%; transform: translate(0px, -50%); color:##fff; margin: 5px;">

				    <h1 contenteditable="true">Fullscreen Hero image</h1>
			        			          
					<div class="MLPEditMenu">
						<span style="min-width:120px;" contenteditable="false"> <i class="fa fa-cogs mlp-icon mlp-icon-settings"></i> <i class="fa fa-arrows-alt mlp-icon mlp-icon-drag"></i> <i class="fa fa-trash-alt mlp-icon-alert" data-tooltip="Are you sure?"></i>	    </span>
					</div>

			    </div>

			</div>

	</cfoutput>
</cfsavecontent>
