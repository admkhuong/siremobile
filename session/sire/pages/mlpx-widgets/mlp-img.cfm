<cfsavecontent variable="MLPImgContainer">
	<cfoutput>

		<div class="MLPEditable MLPImageContainer draggable sortable-item" contenteditable="false" id="New Image Section" data-helper-text="Image Container" style="">

			<img src="/session/sire/images/imgplaceholder.png" style="max-width: 100%;" class=""></img>

			<div class="MLPEditMenu">

				<span style="min-width:120px;" contenteditable="false">
					<i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>
					<i class="fa fa-trash-alt mlp-icon-alert" data-tooltip="Are you sure?"></i>
					<i class="fa fa-arrows-alt mlp-icon mlp-icon-drag"></i> 
					<i class="fa fa-image mlp-icon ImageChooserModal" data-toggle="modal" href="##" data-target="##ImageChooserModal" data-image-target="img"></i>
					<i class="fa fa-cogs mlp-icon mlp-icon-img-settings"></i>
				</span>

			</div>

		</div>

	</cfoutput>
</cfsavecontent>
