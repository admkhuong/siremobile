<cfsavecontent variable="MLPMainTemplate">
	<cfoutput>

		<!--- Mark stuff you dont want inplace editable to be deleted by accident on backspace or delete key - this is usually the outer container - will need ot add a gloabal content editable fix on the main mlp-x page to remove these options in final contenteditable="true" published page --->
		<div id="MLP-Master-Section" class="MLPMasterSection MLPEditable droppable" data-auto-margin="0 0" contenteditable="false" style="text-align: center; margin: 0 auto; height: 100%; width: 100%;" min-height-units="em" mlp-min-height="6" data-helper-text="Box Container">

			<div class="MLPEditMenu">

				<span style="min-width:120px; font-size: 1em;" contenteditable="false">
					<i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>
				</span>

			</div>
										
		</div>

	</cfoutput>
</cfsavecontent>
