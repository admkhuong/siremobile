<cfsavecontent variable="MLP_3_1_RowTemplate">
	<cfoutput>

		<!--- Mark stuff you dont want inplace editable to be deleted by accident on backspace or delete key - this is usually the outer container - will need ot add a gloabal content editable fix on the main mlp-x page to remove these options in final contenteditable="true" published page --->
		<div class="MLPMasterSectionContainer droppable draggable" contenteditable="false" data-helper-text="Row Container">

			<div class="row">

				<div class="MLPMasterSection MLPMasterRowEmpty MLPEditable droppable col-xs-12 col-sm-4 col-md-4" data-auto-margin="1em auto" contenteditable="true" style="text-align: center; min-height: 4em;" data-helper-text="Box Container">

					Container 1

					<div class="MLPEditMenu">

						<span style="min-width:120px; font-size: 1em;" contenteditable="false">
							<i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>
							<i class="fa fa-trash-alt mlp-icon-alert" data-tooltip="Are you sure?" style=""></i>
						</span>

					</div>

				</div>

				<div class="MLPMasterSection MLPMasterRowEmpty MLPEditable droppable col-xs-12 col-sm-4 col-md-4" data-auto-margin="1em auto" contenteditable="true" style="text-align: center; min-height: 4em;" data-helper-text="Box Container">

					Container 2

					<div class="MLPEditMenu">

						<span style="min-width:120px; font-size: 1em;" contenteditable="false">
							<i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>
							<i class="fa fa-trash-alt mlp-icon-alert" data-tooltip="Are you sure?" style=""></i>
						</span>

					</div>

				</div>

				<div class="MLPMasterSection MLPMasterRowEmpty MLPEditable droppable col-xs-12 col-sm-4 col-md-4" data-auto-margin="1em auto" contenteditable="true" style="text-align: center; min-height: 4em;" data-helper-text="Box Container">

					Container 3

					<div class="MLPEditMenu">

						<span style="min-width:120px; font-size: 1em;" contenteditable="false">
							<i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>
							<i class="fa fa-trash-alt mlp-icon-alert" data-tooltip="Are you sure?" style=""></i>
						</span>

					</div>

				</div>

			</div>

			<span class="MLPMasterSectionEdit" contenteditable="false"> <i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>  <i class="fa fa-arrows-alt mlp-icon mlp-icon-drag"></i> </span> 
		</div>

	</cfoutput>
</cfsavecontent>

<cfsavecontent variable="MLP_2_1_RowTemplate">
	<cfoutput>

		<!--- Mark stuff you dont want inplace editable to be deleted by accident on backspace or delete key - this is usually the outer container - will need ot add a gloabal content editable fix on the main mlp-x page to remove these options in final contenteditable="true" published page --->
		<div class="MLPMasterSectionContainer droppable draggable" contenteditable="false" data-helper-text="Row Container">

			<div class="row">

				<div class="MLPMasterSection MLPMasterRowEmpty MLPEditable droppable col-xs-12 col-sm-6 col-md-6" data-auto-margin="1em auto" contenteditable="true" style="text-align: center; min-height: 6em;" data-helper-text="Box Container">

					Container 1

					<div class="MLPEditMenu">

						<span style="min-width:120px; font-size: 1em;" contenteditable="false">
							<i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>
							<i class="fa fa-trash-alt mlp-icon-alert" data-tooltip="Are you sure?" style=""></i>
						</span>

					</div>

				</div>

				<div class="MLPMasterSection MLPMasterRowEmpty MLPEditable droppable col-xs-12 col-sm-6 col-md-6" data-auto-margin="1em auto" contenteditable="true" style="text-align: center; min-height: 6em;" data-helper-text="Box Container">

					Container 2

					<div class="MLPEditMenu">

						<span style="min-width:120px; font-size: 1em;" contenteditable="false">
							<i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>
							<i class="fa fa-trash-alt mlp-icon-alert" data-tooltip="Are you sure?" style=""></i>
						</span>

					</div>

				</div>

			</div>

			<span class="MLPMasterSectionEdit" contenteditable="false"> <i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>  <i class="fa fa-arrows-alt mlp-icon mlp-icon-drag"></i> </span>
		</div>

	</cfoutput>
</cfsavecontent>
