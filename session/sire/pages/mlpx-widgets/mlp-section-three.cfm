<cfsavecontent variable="MLPSectionThreeTemplate">
	<cfoutput>

		<!--- Mark stuff you dont want inplace editable to be deleted by accident on backspace or delete key - this is usually the outer container - will need ot add a gloabal content editable fix on the main mlp-x page to remove these options in final contenteditable="true" published page --->
		<div id="New Section" class="MLPMasterSection MLPMasterSectionEmpty MLPEditable droppable draggable sortable sortable-item connectedSortable container" data-auto-margin="1em auto" contenteditable="false" style="text-align: center; margin: 0 auto; min-height: 6em; padding: 2em 0;" min-height-units="em" mlp-min-height="6" data-helper-text="Box Container">




				<div class="row">

					<div class="MLPMasterSection MLPMasterRowEmpty MLPEditable droppable col-xs-12 col-sm-4 col-md-4 sortable-item wow fadeInLeft animated" data-auto-margin="1em auto" contenteditable="true" style="text-align: center; min-height: 4em; padding: 2em 0;" data-helper-text="Box Container">

						Container 1

						<div class="MLPEditMenu">

							<span style="min-width:120px; font-size: 1em;" contenteditable="false">
								<i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>
								<i class="fa fa-trash-alt mlp-icon-alert" data-tooltip="Are you sure?" style=""></i>
							</span>

						</div>

					</div>

					<div class="MLPMasterSection MLPMasterRowEmpty MLPEditable droppable col-xs-12 col-sm-4 col-md-4 sortable-item wow fadeIn animated" data-auto-margin="1em auto" contenteditable="true" style="text-align: center; min-height: 4em; padding: 2em 0;" data-helper-text="Box Container">

						Container 2

						<div class="MLPEditMenu">

							<span style="min-width:120px; font-size: 1em;" contenteditable="false">
								<i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>
								<i class="fa fa-trash-alt mlp-icon-alert" data-tooltip="Are you sure?" style=""></i>
							</span>

						</div>

					</div>

					<div class="MLPMasterSection MLPMasterRowEmpty MLPEditable droppable col-xs-12 col-sm-4 col-md-4 sortable-item wow fadeInRight animated" data-auto-margin="1em auto" contenteditable="true" style="text-align: center; min-height: 4em; padding: 2em 0;" data-helper-text="Box Container">

						Container 3

						<div class="MLPEditMenu">

							<span style="min-width:120px; font-size: 1em;" contenteditable="false">
								<i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>
								<i class="fa fa-trash-alt mlp-icon-alert" data-tooltip="Are you sure?" style=""></i>
							</span>

						</div>

					</div>

				</div>





			<div class="MLPEditMenu">

				<span style="min-width:120px; font-size: 1em;" contenteditable="false">
					<i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>
					<i class="fa fa-arrows-alt mlp-icon mlp-icon-drag"></i>
					<i class="fa fa-trash-alt mlp-icon-alert" data-tooltip="Are you sure?" style=""></i>
					<i class="fa fa-columns mlp-icon mlp-icon-split-section"></i>
				</span>

			</div>

		</div>

<!---
		<!--- Mark stuff you dont want inplace editable to be deleted by accident on backspace or delete key - this is usually the outer container - will need ot add a gloabal content editable fix on the main mlp-x page to remove these options in final contenteditable="true" published page --->
		 <div class="MLPEditable draggable sortable-item" data-helper-text="Article Style One Container">


			<div id="what-we-do" class="padding-one" contenteditable="true">
            	<div class="container">


			        <!--- Mark stuff you dont want inplace editable to be deleted by accident on backspace or delete key - this is usually the outer container - will need ot add a gloabal content editable fix on the main mlp-x page to remove these options in final contenteditable="true" published page --->
					<div id="New Section" class="MLPMasterSection MLPMasterSectionEmpty MLPEditable droppable draggable sortable sortable-item connectedSortable" data-auto-margin="1em auto" contenteditable="false" style="" min-height-units="em" mlp-min-height="6" data-helper-text="Box Container">

						<div class="MLPEditMenu">

							<span style="min-width:120px; font-size: 1em;" contenteditable="false">
								<i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>
								<i class="fa fa-arrows-alt mlp-icon mlp-icon-drag"></i>
								<i class="fa fa-trash-alt mlp-icon-alert" data-tooltip="Are you sure?" style=""></i>
								<i class="fa fa-columns mlp-icon mlp-icon-split-section"></i>
							</span>

						</div>


	                    <div class="row">
	                         <div class="col-sm-12 wow fadeInRight animated" style="visibility: visible;">
	                              <h2>What We Do</h2>
	                              <hr>

	                              <p class="text-center p-margin">Lorem Ipsum is dummy text of the printing and typesetting Quisque enim dolor, mauris ac sacvbxcvbxcvgittis erat pulvinar leo, sed ultrices ipsum elit sed leo dolor in justo pretium et elit.</p>
	                         </div>
	                    </div>

					</div>



                    <div class="row margin-100-top" id="none">
                         <div class="col-sm-4 wow fadeInLeft animated" style="visibility: visible;">
                              <div class="row">
                                   <div class="col-xs-12">

	                                   <!--- Mark stuff you dont want inplace editable to be deleted by accident on backspace or delete key - this is usually the outer container - will need ot add a gloabal content editable fix on the main mlp-x page to remove these options in final contenteditable="true" published page --->
										<div class="MLPEditable draggable sortable-item" data-helper-text="Article Style One Container" style="padding: 0;">


											<div id="what-we-do" class="" contenteditable="true">

												<div class="what-we-do-features margin-100-bottom">
									                 <h3><span><i class="fa fa-gitlab" aria-hidden="true"></i></span>Template Development</h3>

									                 <p>Lorem Ipsum is simulated dummy text of the printing and typesetting Donec pulvinar mi sit amet viverra justo hendrerit.</p>
									            </div>

											</div>

											<div class="MLPEditMenu">

												<span style="min-width:120px; font-size: 1em;" contenteditable="false">
													<i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>
													<i class="fa fa-arrows-alt mlp-icon mlp-icon-drag"></i>
													<i class="fa fa-trash-alt mlp-icon-alert" data-tooltip="Are you sure?" style=""></i>
												</span>

											</div>
										</div>

										<!--- Mark stuff you dont want inplace editable to be deleted by accident on backspace or delete key - this is usually the outer container - will need ot add a gloabal content editable fix on the main mlp-x page to remove these options in final contenteditable="true" published page --->
										<div class="MLPEditable draggable sortable-item" data-helper-text="Article Style One Container" style="padding: 0;">


											<div id="what-we-do" class="" contenteditable="true">

												<div class="what-we-do-features margin-100-bottom">
									                  <h3><span><i class="fa fa-paper-plane" aria-hidden="true"></i></span>Web Designing</h3>

													  <p>Lorem Ipsum is simcxvbxcvbxcvbply dummy text of the printing and typesetting Donec pulvinar mi sit amet viverra justo hendrerit.</p>
									            </div>

											</div>

											<div class="MLPEditMenu">

												<span style="min-width:120px; font-size: 1em;" contenteditable="false">
													<i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>
													<i class="fa fa-arrows-alt mlp-icon mlp-icon-drag"></i>
													<i class="fa fa-trash-alt mlp-icon-alert" data-tooltip="Are you sure?" style=""></i>
												</span>

											</div>
										</div>

										<!--- Mark stuff you dont want inplace editable to be deleted by accident on backspace or delete key - this is usually the outer container - will need ot add a gloabal content editable fix on the main mlp-x page to remove these options in final contenteditable="true" published page --->
										<div class="MLPEditable draggable sortable-item" data-helper-text="Article Style One Container" style="padding: 0;">


											<div id="what-we-do" class="" contenteditable="true">

												<div class="what-we-do-features margin-100-bottom">
									                <h3><span><i class="fa fa-paper-plane" aria-hidden="true"></i></span>Our Misson</h3>

													<p>Lorem Ipsum is simply dummy text of the printing and txcvbxcvbxcvypesetting Donec pulvinar mi sit amet viverra justo hendrerit.</p>
									            </div>

											</div>

											<div class="MLPEditMenu">

												<span style="min-width:120px; font-size: 1em;" contenteditable="false">
													<i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>
													<i class="fa fa-arrows-alt mlp-icon mlp-icon-drag"></i>
													<i class="fa fa-trash-alt mlp-icon-alert" data-tooltip="Are you sure?" style=""></i>
												</span>

											</div>
										</div>

                                    </div>
                              </div>
                         </div>

                         <div class="col-sm-4 wow fadeIn animated" id="device" style="visibility: visible;">
                              <div>
                                   <div>
                                        <div id="mobile-img">
                                             <div class="MLPEditable MLPImageContainer draggable sortable-item" contenteditable="false" id="New Image Section" data-helper-text="Image Container" style="padding: 0;">
                                                  <img src="https://s3-us-west-1.amazonaws.com/siremobile/mlp_image/7043353CB907E2384C30D41102D46A7D/device.jpg" style="max-width: 100%;" class="">

                                                  <div class="MLPEditMenu" style="display: none;">
                                                       <span style="min-width:120px;" contenteditable="false"><i class="fa fa-cogs mlp-icon mlp-icon-settings"></i> <i class="fa fa-trash-alt mlp-icon-alert" data-tooltip="Are you sure?" style="cursor: pointer;"></i> <i class="fa fa-arrows-alt mlp-icon mlp-icon-drag ui-sortable-handle"></i> <i class="fa fa-image mlp-icon ImageChooserModal" data-toggle="modal" href="#" data-target="#ImageChooserModal" data-image-target="img"></i> <i class=
                                                       "fa fa-cogss mlp-icon mlp-icon-img-settings"></i></span>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>

                         <div class="col-sm-4 wow fadeInRight animated" style="visibility: visible;">
                              <div class="col-xs-12">
                                   <div class="row">
                                        <!--- Mark stuff you dont want inplace editable to be deleted by accident on backspace or delete key - this is usually the outer container - will need ot add a gloabal content editable fix on the main mlp-x page to remove these options in final contenteditable="true" published page --->
										<div class="MLPEditable draggable sortable-item" data-helper-text="Article Style One Container" style="padding: 0;">


											<div id="what-we-do" class="" contenteditable="true">

												<div class="what-we-do-features margin-100-bottom">
									                 <h3 class="text-right"><span class="pull-right"><i class="fa fa-gitlab" aria-hidden="true"></i></span>Template Development</h3>

									                 <p class="text-right">Lorem Ipsum is simulated dummy text of the printing and typesetting Donec pulvinar mi sit amet viverra justo hendrerit.</p>
									            </div>

											</div>

											<div class="MLPEditMenu">

												<span style="min-width:120px; font-size: 1em;" contenteditable="false">
													<i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>
													<i class="fa fa-arrows-alt mlp-icon mlp-icon-drag"></i>
													<i class="fa fa-trash-alt mlp-icon-alert" data-tooltip="Are you sure?" style=""></i>
												</span>

											</div>
										</div>

										<!--- Mark stuff you dont want inplace editable to be deleted by accident on backspace or delete key - this is usually the outer container - will need ot add a gloabal content editable fix on the main mlp-x page to remove these options in final contenteditable="true" published page --->
										<div class="MLPEditable draggable sortable-item" data-helper-text="Article Style One Container" style="padding: 0;">


											<div id="what-we-do" class="" contenteditable="true">

												<div class="what-we-do-features margin-100-bottom">
									                 <h3 class="text-right"><span class="pull-right"><i class="fa fa-archive" aria-hidden="true"></i></span>Our Locations</h3>

													 <p class="text-right">Lorem Ipsum is simply dummy text of the printing and typesetting Donec pulvinar mi sit amet viverra justo hendrerit.</p>
									            </div>

											</div>

											<div class="MLPEditMenu">

												<span style="min-width:120px; font-size: 1em;" contenteditable="false">
													<i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>
													<i class="fa fa-arrows-alt mlp-icon mlp-icon-drag"></i>
													<i class="fa fa-trash-alt mlp-icon-alert" data-tooltip="Are you sure?" style=""></i>
												</span>

											</div>
										</div>

										<!--- Mark stuff you dont want inplace editable to be deleted by accident on backspace or delete key - this is usually the outer container - will need ot add a gloabal content editable fix on the main mlp-x page to remove these options in final contenteditable="true" published page --->
										<div class="MLPEditable draggable sortable-item" data-helper-text="Article Style One Container" style="padding: 0;">


											<div id="what-we-do" class="" contenteditable="true">

												<div class="what-we-do-features margin-100-bottom">
													<h3 class="text-right"><span class="pull-right"><i class="fa fa-futbol-o" aria-hidden="true"></i></span>24/7 Live Support</h3>

													<p class="text-right">Lorem Ipsum is simply dummy text of the printing axcvbcvbnd typesetting Donec pulvinar mi sit amet viverra justo hendrerit.</p>

									            </div>

											</div>

											<div class="MLPEditMenu">

												<span style="min-width:120px; font-size: 1em;" contenteditable="false">
													<i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>
													<i class="fa fa-arrows-alt mlp-icon mlp-icon-drag"></i>
													<i class="fa fa-trash-alt mlp-icon-alert" data-tooltip="Are you sure?" style=""></i>
												</span>

											</div>
										</div>


                                   </div>
                              </div>
                         </div>
                    </div>

               </div>
          </div>



			<div class="MLPEditMenu">

				<span style="min-width:120px; font-size: 1em;" contenteditable="false">
					<i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>
					<i class="fa fa-arrows-alt mlp-icon mlp-icon-drag"></i>
					<i class="fa fa-trash-alt mlp-icon-alert" data-tooltip="Are you sure?" style=""></i>
				</span>

			</div>
		</div>
--->

	</cfoutput>
</cfsavecontent>
