<cfsavecontent variable="MLPSectionTemplate">
	<cfoutput>

		<!--- Mark stuff you dont want inplace editable to be deleted by accident on backspace or delete key - this is usually the outer container - will need ot add a gloabal content editable fix on the main mlp-x page to remove these options in final contenteditable="true" published page --->
		<div id="New Section" class="MLPMasterSection MLPMasterSectionEmpty MLPMasterRowEmpty MLPEditable droppable draggable sortable sortable-item connectedSortable" data-auto-margin="1em auto" contenteditable="false" style="text-align: center; margin: 0 auto; min-height: 6em; max-width: 960px;" min-height-units="em" mlp-min-height="6" data-helper-text="Box Container">

			<div class="MLPEditMenu">

				<span style="min-width:120px; font-size: 1em;" contenteditable="false">
					<i class="fa fa-cogs mlp-icon mlp-icon-settings"></i>
					<i class="fa fa-arrows-alt mlp-icon mlp-icon-drag"></i>
					<i class="fa fa-trash-alt mlp-icon-alert" data-tooltip="Are you sure?" style=""></i>
					<i class="fa fa-columns mlp-icon mlp-icon-split-section"></i>
				</span>

			</div>

		</div>

	</cfoutput>
</cfsavecontent>
