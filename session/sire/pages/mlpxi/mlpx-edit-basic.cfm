<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>MLP XI</title>

    <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/grapesjs/0.14.33/css/grapes.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/grapesjs/0.14.33/grapes.min.js"></script>

<style>
     /* Let's highlight canvas boundaries */
     #gjs {
       border: 3px solid #444;
     }

     /* Reset some default styling */
     .gjs-cv-canvas {
       top: 0;
       width: 100%;
       height: 100%;
     }
</style>


</head>
<body>

     <div id="gjs">
          <h1>Hello World Component!</h1>
    </div>


  </div>


  <script type="text/javascript">

  const editor = grapesjs.init({
    // Indicate where to init the editor. You can also pass an HTMLElement
    container: '#gjs',
    // Get the content for the canvas directly from the element
    // As an alternative we could use: `components: '<h1>Hello World Component!</h1>'`,
    fromElement: true,
    // Size of the editor
    height: '300px',
    width: 'auto',
    // Disable the storage manager for the moment
    storageManager: { type: null },
    // Avoid any default panel
    panels: { defaults: [] },
  });


  </script>
</body>
</html>
