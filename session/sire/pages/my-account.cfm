<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
  <cfinvokeargument name="path" value="/public/sire/js/jquery-ui-1.12.1.custom/jquery-ui.min.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
  <cfinvokeargument name="path" value="/public/sire/js/jquery-ui-1.12.1.custom/jquery-ui.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/ajaxupload.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/site/common.js">
</cfinvoke>
<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/dropzone.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/image-manage.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/vendors/dropzone.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/my-account.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/setting.css">
</cfinvoke>

<cfinclude template="/session/sire/configs/paths.cfm">

<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke>

<cfif userInfo.RESULT NEQ 'SUCCESS'>
	<cflocation url = '/' addtoken="false">
</cfif>

<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="userOrgInfo"></cfinvoke>

<cfset _has_Org = false>	
<cfif userOrgInfo.RXRESULTCODE EQ 1>
	<cfset _has_Org = true>
	<cfset orgInfoData = userOrgInfo.ORGINFO>
</cfif>


<cfset check_user_security_question = false>

<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserListQuestion" returnvariable="userListQuestion">
	<cfinvokeargument name="detail_question" value=0>
</cfinvoke>

<cfif userListQuestion.RESULT EQ 'SUCCESS' >
	<cfset check_user_security_question = true>
</cfif>


<main class="container-fluid page">
	<cfinclude template="../views/commons/credits_available.cfm">
	<section class="row bg-white">
		<div class="content-header">
			<div class="row">
                <div class="col-sm-6 col-md-6 col-lg-6 content-title">
                	<cfinclude template="../views/commons/welcome.cfm">
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6 content-menu">
				    <a class="active"><span class="icon-setting"></span><span>Setting</span></a>
                </div>
            </div>
		</div>
		<hr class="hrt0">
		<div class="content-body">
			<div class="container-fluid">
				<form autocomplete="off" name="my_account_form" id="my_account_form" action"##" enctype='multipart/form-data'>
					<div class="col-sm-10 col-md-6 padding20" id="My-Profile-Section">
						
						<div class="row">
							<div class="content col-sm-12 col-lg-9">
				                <div class="round-corner">                                       
					                <div id="OrgContainer" style="padding-bottom: 40px; padding-top: 12px">
					                  
										<div class="orginization-progress-box">            

											<!--- Calculate percentage complete --->
											<cfset TotalOrgSteps = 12 />

											<!--- Assume user name and password are already set from sign up --->
											<cfset TotalStepsComplete = 2 />

											<!--- Check for first name --->
											<cfif LEN(TRIM(userInfo.USERNAME)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>

											<!--- Check for last name --->
											<cfif LEN(TRIM(userInfo.LASTNAME)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>

											<!--- Check for SMS number --->
											<cfif LEN(TRIM(userInfo.FULLMFAPHONE)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>

											<!--- Check for ADDRESSe --->
											<cfif LEN(TRIM(userInfo.ADDRESS)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>

											<!--- Check for POSTALCODE --->
											<cfif LEN(TRIM(userInfo.POSTALCODE)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>

											<!--- Check for ORGANIZATIONBUSINESSNAME_VCH --->
											<cfif LEN(TRIM(userOrgInfo.ORGINFO.ORGANIZATIONBUSINESSNAME_VCH)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>

											<!--- Check for ORGANIZATIONNAME_VCH --->
											<cfif LEN(TRIM(userOrgInfo.ORGINFO.ORGANIZATIONNAME_VCH)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>

											<!--- Check for ORGANIZATIONWEBSITE_VCH --->
											<cfif LEN(TRIM(userOrgInfo.ORGINFO.ORGANIZATIONWEBSITE_VCH)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>

											<!--- Check for ORGANIZATIONWEBSITETINY_VCH --->
											<cfif LEN(TRIM(userOrgInfo.ORGINFO.ORGANIZATIONWEBSITETINY_VCH)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>

											<!--- Check for ORGANIZATIONPHONE_VCH --->
											<cfif LEN(TRIM(userOrgInfo.ORGINFO.ORGANIZATIONPHONE_VCH)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>

											<!--- Check for ORGANIZATIONEMAIL_VCH --->
											<cfif LEN(TRIM(userOrgInfo.ORGINFO.ORGANIZATIONEMAIL_VCH)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>
											<div class="row" style="display: flex;">
												<div id="console-ic-profile-div" style="width: 12%; margin: auto">
													<div style="text-align: center;"><img src="/public/sire/images/ic-profile-1.png" id="console-ic-profile" class="" style="width: 100%;"></div>
												</div>

												<div id="progress-bar-div" style="width: 85%;">
													<div style="font-weight: bold;"><cfoutput>#Round((TotalStepsComplete/TotalOrgSteps)*100)#</cfoutput> % Complete</div>
													<input type="hidden" name="progress-bar-percent" value="<cfoutput>#Round((TotalStepsComplete/TotalOrgSteps)*100)#</cfoutput>">
													<div style="margin-top: .5em;" id="ProfileProgressBar"></div>
												</div>
												
											</div>
											  

										</div>                    
					                                                
					                </div>   
				              	</div>
				            </div>
						</div>

						<p class="sub-title">My Profile</p>
						
						<div class="profile-wrapper form-horizontal">
							<div class="form-group">
							    <label for="fname" class="col-sm-3 col-lg-3 control-label">First Name <span class="text-danger">*</span></label>
							    <div class="col-sm-9 col-lg-6">
							    	<input type="text" class="form-control validate[required,custom[noHTML]]" id="fname" name="fname" maxlength="50" value="<cfoutput>#userInfo.USERNAME#</cfoutput>" data-prompt-position="topLeft:100">
							    </div>
							</div>
							<div class="form-group">
							    <label for="lname" class="col-sm-3 col-lg-3 control-label">Last Name <span class="text-danger">*</span></label>
							    <div class="col-sm-9 col-lg-6">
							    	<input type="text" class="form-control validate[required,custom[noHTML]]" id="lname" name="lname" maxlength="50" value="<cfoutput>#userInfo.LASTNAME#</cfoutput>" data-prompt-position="topLeft:100">
							    </div>
							</div>
							<div class="form-group">
							    <label for="sms_number" class="col-sm-3 col-lg-3 control-label">SMS Number <span class="text-danger">*</span></label>
							    <div class="col-sm-9 col-lg-6">
							    	<input type="text" class="form-control validate[required,custom[usPhoneNumber]]" id="sms_number" name="sms_number" maxlength="15" value="<cfoutput>#userInfo.FULLMFAPHONE#</cfoutput>" data-prompt-position="topLeft:100"> 
							    </div>
							</div>
							<div class="form-group">
							    <label for="emailadd" class="col-sm-3 col-lg-3 control-label">E-mail Address <span class="text-danger">*</span></label>
							    <div class="col-sm-9 col-lg-6">
							    	<input type="email" class="form-control validate[required,custom[email]]" id="emailadd" name="emailadd" value="<cfoutput>#userInfo.FULLEMAIL#</cfoutput>" maxlength="250" data-prompt-position="topLeft:100" disabled>
							    </div>
							</div>

							<div class="form-group">
							    <label for="address" class="col-sm-3 col-lg-3 control-label">Address</label>
							    <div class="col-sm-9 col-lg-6">
							    	<input type="text" class="form-control" id="address" name="address" maxlength="250" value="<cfoutput>#userInfo.ADDRESS#</cfoutput>" data-prompt-position="topLeft:100">
							    </div>
							</div>

							<div class="form-group">
							    <label for="zip_code" class="col-sm-3 col-lg-3 control-label">Zip Code</label>
							    <div class="col-sm-9 col-lg-6">
							    	<input type="text" class="form-control" id="zip_code" name="zip_code" maxlength="10" value="<cfoutput>#userInfo.POSTALCODE#</cfoutput>" data-prompt-position="topLeft:100">
							    </div>
							</div>

							
							<div class="form-group">	
								<div class="checkbox col-sm-7 col-md-7 col-lg-6">
								  	<label>
								    	<input type="checkbox" class="checkbox_device_reg" name="makeDefault" value="1" 
								    	<cfif userInfo.MFACONTACTTYPE EQ 3 OR  userInfo.MFACONTACTTYPE EQ 2> <cfoutput> checked </cfoutput> </cfif> >
								    	Make this my default authentication method
								  	</label>
								</div>
								<div class="selexbox col-sm-5 col-md-5 col-lg-3">
									<select class="form-control" name="sendType" id="sendType">
									  <option value="3" <cfif userInfo.MFACONTACTTYPE EQ 3> <cfoutput>selected</cfoutput> </cfif> > Mobile </option>
									  <option value="2" <cfif userInfo.MFACONTACTTYPE EQ 2> <cfoutput>selected</cfoutput> </cfif> > Email </option>
									</select>
								</div>
							</div>
							

							<div class="form-group">
								<div class="checkbox col-sm-6 col-md-7 col-lg-5">
								<cfif check_user_security_question>
									    <label>
									     	<input type="checkbox" name="SecurityQuestionEnabled" id="SecurityQuestionEnabled" <cfif userInfo.SQUESTIONENABLE EQ 1><cfoutput>checked</cfoutput></cfif> > 
									     	Enable the security question
									    </label>
								</cfif>	
								</div> 
								 <div class="col-sm-6 col-md-5 col-lg-4">
								  	<a href="/session/sire/pages/security-question-edit" class="btn btn-success btn-success-custom link-to-edit security-question-edit pull-right">
								  		<cfif check_user_security_question> Edit Security Questions <cfelse> Create Security Questions </cfif>
								  	</a>	
								  </div>
							</div>  
						</div>	
					</div>

					<div class="col-sm-10 col-md-6 organization-profile padding20" id="Organization-Profile-Section">
						<p class="sub-title">Organization Profile</p>
						
						<div class="row">
							<div class="form-group col-sm-12 col-lg-5">
								<input type="hidden" name="OrganizationLogo_vch" id="OrganizationLogo_vch" value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationLogo_vch#</cfoutput></cfif>">
								<input type="hidden" name="Old_OrganizationLogo_vch" id="OrganizationLogo_vch" value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationLogo_vch#</cfoutput></cfif>">
								<input type="hidden" name="UpdateOrganizationLogo" id="UpdateOrganizationLogo" value="0">
								<cfif _has_Org AND orgInfoData.OrganizationLogo_vch NEQ ''>
									<cfset display_logo = ''>
									<div class="OrgLogo-wrapper col-sm-6 col-lg-3" style="<cfoutput>#display_logo#</cfoutput>">
										<img id="OrgLogo" src="<cfoutput>#_S3IMAGEURL##orgInfoData.OrganizationLogo_vch#</cfoutput>"  />
										<!--- <div style="position:absolute"> <span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span></div> --->
                                	</div>
                                	
								<cfelse>
									<cfset display_logo = 'display:none'>
									<div class="OrgLogo-wrapper col-sm-6 col-lg-3" style="<cfoutput>#display_logo#</cfoutput>">
										<img id="OrgLogo" src=""/>
                                	</div>
								</cfif>	
								<div class="logo-btn-group col-sm-3 col-lg-2">
									<div id="uploadArea"  style="display: none">
										<div class="table table-striped" class="files" id="previews">
											<div id="template" class="file-row">
												<!-- This is used as the file preview template -->
												<div>
													<span class="preview"><img data-dz-thumbnail /></span>
												</div>
												<div>
													<p class="name" data-dz-name></p>
													<strong class="error text-danger" data-dz-errormessage></strong>
												</div>
												<div>
													<p class="size" data-dz-size></p>
													<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
														<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
													</div>
												</div>
												<div>
													<button class="btn btn-primary start">
														<i class="glyphicon glyphicon-upload"></i>
														<span>Start</span>
													</button>
													<button data-dz-remove class="btn btn-warning cancel">
														<i class="glyphicon glyphicon-ban-circle"></i>
														<span>Cancel</span>
													</button>
													<button data-dz-remove class="btn btn-danger delete">
														<i class="glyphicon glyphicon-trash"></i>
														<span>Delete</span>
													</button>
												</div>
											</div>
										</div>
									</div>
									<!--- <button class="btn btn-info upload-avatar" id="imageUpload"><b>Upload Logo</b></button> --->
										<span style="margin-top: 10px" class="btn btn-info upload-avatar fileinput-button"><b>Upload Logo</b></span>
										<button style="<cfoutput>#display_logo#</cfoutput>; margin-top: 10px" class="btn btn-link" id="removeLogo"><strong>Remove Logo</strong></button>
									
								</div>	
							</div>	
						</div>
					
						<div class="profile-wrapper form-horizontal">
							<input type="hidden" name="OrganizationId_int" id="OrganizationId_int" value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationId_int#</cfoutput></cfif>">

							<div class="form-group">
							    <label for="OrganizationName_vch" class="col-sm-3 col-lg-4 control-label">Company Name</label>
							    <div class="col-sm-9 col-lg-6">
							    	<input type="text" class="form-control" name="OrganizationName_vch" id="OrganizationName_vch" maxlength="250" data-prompt-position="topLeft:100" 
							    	value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationName_vch#</cfoutput></cfif>">
							    </div>
							</div>

<!---
							<div class="form-group" id="Organization-Short-Name-Section">
							    <label for="OrganizationBusinessName_vch" class="col-sm-3 col-lg-4 control-label">Co. Name Abbr.</label>
							    <div class="col-sm-9 col-lg-6">
							    	<input type="text" class="form-control" name="OrganizationBusinessName_vch" id="OrganizationBusinessName_vch" maxlength="250" data-prompt-position="topLeft:100"
									value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationBusinessName_vch#</cfoutput></cfif>">
							    </div>	
							</div>
--->
							
							<div class="form-group">
							    <label for="OrganizationPhone_vch" class="col-sm-3 col-lg-4 control-label">Phone Number</label>
							    <div class="col-sm-9 col-lg-6">
							    	<input type="text" class="form-control validate[custom[usPhoneNumber]]" id="OrganizationPhone_vch" name="OrganizationPhone_vch" maxlength="15" data-prompt-position="topLeft:100"
							    	value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationPhone_vch#</cfoutput></cfif>">
							    </div>
							</div>
							<div class="form-group">
							    <label for="OrganizationEmail_vch" class="col-sm-3 col-lg-4 control-label">Email Address</label>
							    <div class="col-sm-9 col-lg-6">
							    	<input type="text" class="form-control validate[custom[email]]" id="OrganizationEmail_vch" name="OrganizationEmail_vch" maxlength="250" data-prompt-position="topLeft:100" 
							    	value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationEmail_vch#</cfoutput></cfif>">

							    </div>
							</div>
							<div class="form-group">
							    <label for="OrganizationTagline_vch" class="col-sm-3 col-lg-4 control-label">Tagline</label>
							    <div class="col-sm-9 col-lg-6">
							    	<input type="text" class="form-control" id="OrganizationTagline_vch" name="OrganizationTagline_vch" maxlength="250" data-prompt-position="topLeft:100"
							    	value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationTagline_vch#</cfoutput></cfif>">
							    </div>
							</div>
							<div class="form-group">
							    <label for="OrganizationHOO_vch" class="col-sm-3 col-lg-4 control-label">Hours of Operation</label>
							    <div class="col-sm-9 col-lg-6">
							    	<input type="text" class="form-control" id="OrganizationHOO_vch" name="OrganizationHOO_vch" maxlength="250" data-prompt-position="topLeft:100"
							    	value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationHOO_vch#</cfoutput></cfif>">
							    </div>
							</div>
							<div class="form-group">
							    <label for="OrganizationAdd_vch" class="col-sm-3 col-lg-4 control-label">Address</label>
							    <div class="col-sm-9 col-lg-6">
							    	<input type="text" class="form-control" id="OrganizationAdd_vch" name="OrganizationAdd_vch" maxlength="250" data-prompt-position="topLeft:100"
							    	value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationAdd_vch#</cfoutput></cfif>">
							    </div>
							</div>
							<div class="form-group">
							    <label for="OrganizationZipcode_vch" class="col-sm-3 col-lg-4 control-label">Zip Code</label>
							    <div class="col-sm-9 col-lg-6">
							    	<input type="text" class="form-control" id="OrganizationZipcode_vch" name="OrganizationZipcode_vch" maxlength="10" data-prompt-position="topLeft:100"
							    	value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationZipcode_vch#</cfoutput></cfif>">
							    </div>
							</div>
							<div class="form-group">
							    <label for="OrganizationWebsite_vch" class="col-sm-3 col-lg-4 control-label">Website</label>
							    <div class="col-sm-9 col-lg-6">
							    	<input type="text" class="form-control validate[funcCall[isValidUrl]]" id="OrganizationWebsite_vch" name="OrganizationWebsite_vch" maxlength="250" data-prompt-position="topLeft:100"
							    	value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationWebsite_vch#</cfoutput></cfif>">
							    </div>
							</div>
                            
                            <div class="form-group">
							    <label for="OrganizationWebsite_vch" class="col-sm-3 col-lg-4 control-label">Website Tiny URL Version</label>
							    <div class="col-sm-9 col-lg-6">
							    	<input type="text" class="form-control validate[funcCall[isValidUrl]]" id="OrganizationWebsiteTiny_vch" name="OrganizationWebsiteTiny_vch" maxlength="250" data-prompt-position="topLeft:100"
							    	value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationWebsiteTiny_vch#</cfoutput></cfif>">
							    </div>
							</div>
							<p class="sub-title" id="help-stop-my-account"> Account Help and Stop message defaults </p>
								<div class="form-group" id="Help-Section">
									<label for="CustomHelpMessage_vch" class="col-sm-3 col-lg-4 control-label">Help Message</label>
									<div class="col-sm-9 col-lg-6">
										<textarea class="form-control custom-message" id="custom-help-message" name="CustomHelpMessage_vch" maxlength="160" data-prompt-position="topLeft:150" rows="3"><cfif _has_Org><cfoutput>#orgInfoData.CustomHelpMessage_vch#</cfoutput></cfif></textarea>
										 <p class="help-block pull-right" id="help-block"> <span id="custom_help_available1">0</span>/160 characters available</p>
									</div>	
								</div>

								<div class="form-group" id="Stop-Section">
									<label for="CustomStopMessage_vch" class="col-sm-3 col-lg-4 control-label">Stop Message</label>
									<div class="col-sm-9 col-lg-6">
										<textarea class="form-control custom-message" id="custom-stop-message" name="CustomStopMessage_vch" maxlength="160" data-prompt-position="topLeft:150" rows="3"><cfif _has_Org><cfoutput>#orgInfoData.CustomStopMessage_vch#</cfoutput></cfif></textarea>
										 <p class="help-block pull-right" id="stop-block"> <span id="custom_stop_available1">0</span>/160 characters available</p>
									</div>  
								</div>
								<div class="col-xs-12 col-sm-12 cbic" id="horizontal-help-stop-message">
			    					<div class="row">
									 	&nbsp;
							 		</div> 
									<hr class="hrt0 hrb10">
								</div>
								
			    				<div class="col-sm-12" id="help-stop-my-account-desc">
			    					<p>All campaigns must respond with messages for users who send <b>HELP</b> and <b>STOP</b> keywords.</p><p>By default we will use the messages that you setup on this page.</p><p>You may also specify custom <b>HELP</b> and <b>STOP</b> response messages for each campaign.</p>
			    				</div>

							<div class="form-group">
								<div class="btn-group-setting">
									<button type="button" class="btn btn-success btn-success-custom btn-custom_size" id="btn_save_my_account">Save</button>
									<a href="/session/sire/pages/campaign-manage" type="button" class="btn btn-primary btn-back-custom btn-custom_size" id="btn_cancel">Cancel</a>
								</div>	
							</div>
						</div>	
					</div>
				</form>
			</div>	
		</div>
	</section>
	<style type="text/css">
	div.padding20 {
		padding: 20px 15px!important;
	}

	#upload_avarta {
		min-height: 40px!important;
	}
	</style>
</main>

<cfparam name="variables._title" default="My Account - Sire">
<cfinclude template="../views/layouts/main.cfm">

<!--- Page Tours using bootstrp tour - see http://http://bootstraptour.com/api/ --->
<script type="application/javascript">

	<!--- Set up page tour for new users --->
	$(function() {
		
		
		<!---  Check if campaign list is empty and take them to first tour that points to tours --->
			<cfif  Tour EQ "GettingStarted"> 
        		
		
				// Instance the tour
				var tour = new Tour({				  
				 
				  onEnd: function (tour) {window.location = 'my-account' },						  
				  <cfif tourrs EQ "true">storage:false,</cfif>
				  steps: [
				 {
					element: "",
					title: "Guided Tours ",
					content: "To navigate within the Guided Tours use the Tour provided Next and Prev buttons. If you get bored or think you have enough information you can always select the End Tour option. You can restart tours anytime under the Learning and Support top menu option",
					backdrop:true,
					orphan: true,	
					onShown: function (tour) {$("button.disabled[data-role='prev']").hide();  },				
					placement:"top"
				  },
				  {
					element: "#My-Profile-Section",
					title: "Your personal details ",
					content: "Who and Where to contact about your account. Billing, Low Balance Warnings, System Updates, etc...",
					backdrop:true,
					orphan: true,					
					placement:"top"
				  },
				  {
					element: "#Organization-Profile-Section",
					title: "Data about your organization",
					content: "Templates with organizational place holders in them will automatically pre-populate with these values if they are set here.",
					backdrop:true,
					backdropContainer: 'body',
					placement:"top"
				  },
				  {
					element: "#Organization-Short-Name-Section",
					title: "Data about your organization",
					content: "Templates with organizational place holders in them will automatically pre-populate with these values if they are set here.<p><b>Good message compliance ALWAYS identifies who sent the message.</b></p>",
					backdrop:true,
					backdropContainer: 'body',
					placement:"top"
				  },
				  {
					element: "#Help-Section",
					title: "HELP",
					content: "All short codes must respond to HELP requests. The text you set here will be used as the default for your account. If you leave it blank the Sire system default will be used instead. You can optionally set custom HELP messages for each individual campaigns.",
					backdrop:true,
					backdropContainer: 'body',
					placement:"top"
				  },
				  {
					element: "#Stop-Section",
					title: "STOP",
					content: "All short codes must respond to STOP requests. The text you set here will be used as the default for your account. If you leave it blank the Sire system default will be used instead. You can optionally set custom Stop messages for each individual campaigns.",
					backdrop:true,
					backdropContainer: 'body',
					placement:"top"
				  },
				  {
					element: "#createCampaignModal",
					title: "Campaign Management",
					content: "",
					backdrop:true,
					placement:"top",
					path: "/session/sire/pages/campaign-manage?tour=gettingstarted&tourrs=true"
				  }
				  
				]});
				
				// Initialize the tour
				tour.init();
				
				// Start the tour
				<cfif tourrs EQ "true">
					tour.start(true);
					tour.goTo(0);				
				<cfelse>
					tour.start();				
				</cfif>
				
			</cfif>
					
				
		
	});


</script>
