<cfinclude template="/session/sire/configs/credits.cfm">

<cfset variables.menuToUse = 2 />
<cfset variables.urlpage = 'myplan' />
<cfparam name="activeTab" default=""/>
<cfparam name="action" default=""/>
<cfparam name="downgradePlanName" default=""/>
<cfparam name="downgradePlanId" default="0"/>
<cfparam name="displayDowngrade" default="0"/>
<cfparam name="listKeyword" default="#arrayNew(1)#"/>
<cfparam name="listKeywordName" default="#arrayNew(1)#"/>
<cfparam name="listMLP" default="#arrayNew(1)#"/>
<cfparam name="listMLPName" default="#arrayNew(1)#"/>
<cfparam name="listShortUrl" default="#arrayNew(1)#"/>
<cfparam name="listShortUrlName" default="#arrayNew(1)#"/>

<cfparam name="displayBuyKeyword" default="1"/>

<cfif structKeyExists(url, 'active')>
    <cfset activeTab = url.active >
</cfif>

<cfif structKeyExists(url, 'action')>
    <cfset action = url.action >
</cfif>

<!--- RETRIVE CUSTOMER DATA --->
<cfset firstName =''>
<cfset lastName =''>
<cfset emailAddress =''>
<cfset primaryPaymentMethodId = -1>
<cfset expirationDate =''>
<cfset maskedNumber =''>
<cfset line1 =''>
<cfset city =''>
<cfset state =''>
<cfset zip =''>
<cfset country =''> 
<cfset phone =''>
<cfset hidden_card_form =''>
<cfset disabled_field =''>
<cfset cvv =''>
<cfset paymentType = 'card'>
<cfset RetCustomerInfo = ''>
<cfset discount_plan_price_percent = 0/>
<cfset discount_plan_price_flat_rate = 0/>

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
    .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
    .addJs("../assets/global/plugins/uikit/js/uikit.min.js", false)
    .addJs("../assets/global/plugins/crypto-js/crypto-js.js", true)
    .addJs("/session/sire/js/vendors/list.min.js", true)
    .addJs("https://apis.google.com/js/client.js", true)
    .addJs("/session/sire/assets/pages/scripts/invite-friend.js", false)
    .addJs("/session/sire/assets/pages/scripts/invite-friend-social.js", false)
    .addJs("/session/sire/assets/pages/scripts/my-plan.js", false)
    .addJs("../assets/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js", true);
    // .addJs("/session/sire/assets/global/plugins/uniform/jquery.uniform.min.js",true)
    // .addCss("/session/sire/assets/global/plugins/uniform/css/uniform.default.css", true) ;
</cfscript>

<!--- GET USER BILLING DATA --->
<cfinvoke component="session.sire.models.cfc.billing"  method="GetBalanceCurrentUser"  returnvariable="RetValBillingData"></cfinvoke>

<!--- GET USER PLAN DETAIL --->
<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUserPlan" returnvariable="RetUserPlan"></cfinvoke>

<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetOrderPlan"  returnvariable="RetPlan">
    <cfinvokeargument name="plan" value="#RetUserPlan.PLANID#">
</cfinvoke>

<!--- GET NUMBER OF USED MLPS BY USER --->
<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUsedMLPsByUserId" returnvariable="RetUsedMLP"></cfinvoke>

<!--- GET NUMBER OF USED SHORT URLS BY USER --->
<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUsedShortURLsByUserId" returnvariable="RetUsedShortURL"></cfinvoke>

<!--- GET USER REFERRAL INFO --->
<cfinvoke component="session.sire.models.cfc.referral" method="GetUserReferInfo" returnvariable="RetValUserReferInfo">
    <cfinvokeargument name="inpUserId" value="#SESSION.USERID#">
</cfinvoke>

<!--- GET USER INFO --->
<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke>
<cfif userInfo.USERLEVEL EQ 3 AND userInfo.USERTYPE EQ 2>
    <!--- <cfabort> --->
</cfif>

<cfinvoke component="public.sire.models.cfc.plan" method="GetActivedPlans" returnvariable="plans"></cfinvoke>

<!--- Get Payment method setting --->
<cfinvoke component="session.sire.models.cfc.billing" method="GetPaymentMethodSettingByUserId" returnvariable="RetGetPaymentMethodSetting"></cfinvoke>
<!--- 
<cfif Session.loggedIn EQ 1>
    <cfinvoke method="GetUserPlan" component="session/sire/models/cfc/order_plan" returnvariable="getUserPlan">
    </cfinvoke>
</cfif>  
--->   

<cfif RetGetPaymentMethodSetting.RESULT EQ 1 OR RetGetPaymentMethodSetting.RESULT EQ 0 >
    <cfinvoke component="session.sire.models.cfc.billing" method="RetrieveCustomer" returnvariable="RetCustomerInfo"></cfinvoke>   
<cfelseif  RetGetPaymentMethodSetting.RESULT EQ 2>
    <!--- paypal --->    
    <cfinvoke component="session.sire.models.cfc.vault-paypal" method="getDetailCreditCardInVault" returnvariable="RetCustomerInfo">        
    </cfinvoke>
    
<cfelseif RetGetPaymentMethodSetting.RESULT EQ 3>
    <cfinvoke component="session.sire.models.cfc.vault-mojo" method="getDetailCreditCardInVault" returnvariable="RetCustomerInfo">
    </cfinvoke>
<cfelseif RetGetPaymentMethodSetting.RESULT EQ 4><!--- Total Apps --->
    <cfinvoke component="session.sire.models.cfc.vault-totalapps" method="getDetailCreditCardInVault" returnvariable="RetCustomerInfo"></cfinvoke>           
<cfelse>
    <cfset RetCustomerInfo= {}/>
    <cfset RetCustomerInfo.RXRESULTCODE= 0/>
</cfif>

<cfif RetCustomerInfo.RXRESULTCODE EQ 1>    
    <cfset firstName = RetCustomerInfo.CUSTOMERINFO.firstName>
    <cfset lastName = RetCustomerInfo.CUSTOMERINFO.lastName>
    <cfset emailAddress = RetCustomerInfo.CUSTOMERINFO.emailAddress>
    <cfset primaryPaymentMethodId = 0> <!--- do not print --->
    <cfset expirationDate = RetCustomerInfo.CUSTOMERINFO.expirationDate>
    <cfset maskedNumber = RetCustomerInfo.CUSTOMERINFO.maskedNumber>
    <cfset line1 = RetCustomerInfo.CUSTOMERINFO.line1>
    <cfset city = RetCustomerInfo.CUSTOMERINFO.city>
    <cfset state = RetCustomerInfo.CUSTOMERINFO.state>
    <cfset zip = RetCustomerInfo.CUSTOMERINFO.zip>
    <cfset country = RetCustomerInfo.CUSTOMERINFO.country> 
    <cfset phone = RetCustomerInfo.CUSTOMERINFO.phone>
    <cfset hidden_card_form = 'display:none'>
    <cfset disabled_field ='display: none'>
    <cfset paymentType = RetCustomerInfo.CUSTOMERINFO.paymentType>
    <cfset cvv =RetCustomerInfo.CUSTOMERINFO.cvv>

<cfelseif RetCustomerInfo.RXRESULTCODE EQ -1>
    <main class="container-fluid page my-plan-page">
        <cfinclude template="../views/commons/credits_available.cfm">
        <section class="row bg-white">
            <div class="content-header">
                <div class="col-md-12">
                    <cfparam name="variables._title" default="My Plan - Sire">
                    <p class="text-danger text-center "><cfoutput>#RetCustomerInfo.MESSAGE#</cfoutput></p>
                </div>  
            </div>
        </section>  
    </main>
    <cfinclude template="../views/layouts/master.cfm">
    <cfexit>
    </cfif>

    <cfset userPlanName = 'FREE'>
    <cfif RetUserPlan.RXRESULTCODE EQ 1>
        <cfset userPlanName = UCase(RetUserPlan.PLANNAME)>
    </cfif>

    <cfif RetUserPlan.TOTALKEYWORDHAVETOPAID GT 0>
        <cfset displayBuyKeyword = 0 />
    </cfif>

    <cfif RetUserPlan.PLANEXPIRED GT 0> 
        <cfset monthlyAmount = RetUserPlan.AMOUNT + (RetUserPlan.KEYWORDPURCHASED + RetUserPlan.KEYWORDBUYAFTEREXPRIED)*RetUserPlan.PRICEKEYWORDAFTER >
    <cfelse>    
        <cfset monthlyAmount = RetUserPlan.AMOUNT + RetUserPlan.KEYWORDPURCHASED*RetUserPlan.PRICEKEYWORDAFTER >
    </cfif>

    <cfset planMonthlyAmount = RetUserPlan.AMOUNT />

    <!--- GET MAX KEYWORD CAN EXPIRED --->
    <cfset UnsubscribeNumberMax = 0>
    <cfif RetUserPlan.PLANEXPIRED EQ 1>
        <cfif RetUserPlan.KEYWORDAVAILABLE GT RetUserPlan.KEYWORDBUYAFTEREXPRIED>
            <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDBUYAFTEREXPRIED>
        <cfelse>
            <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDAVAILABLE>
        </cfif>
    <cfelseif RetUserPlan.PLANEXPIRED EQ 2> <!--- IF USER PLAN HAS DOWNGRADE IT CAN NOT UNSCRIBE KEYWORD --->
    <!---
    <cfif RetUserPlan.KEYWORDAVAILABLE GT RetUserPlan.KEYWORDBUYAFTEREXPRIED>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDBUYAFTEREXPRIED>
    <cfelse>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDAVAILABLE>
    </cfif>    
--->
<cfset UnsubscribeNumberMax = 0>
<cfelse>
    <cfif RetUserPlan.KEYWORDAVAILABLE GT RetUserPlan.KEYWORDPURCHASED>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDPURCHASED>
    <cfelse>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDAVAILABLE>
    </cfif>
</cfif>


<!--- GET INFOR DISPLAY DOWNGRADE --->
<cfif RetUserPlan.USERDOWNGRADEPLAN GT 0>
    <cfset listKeyword = deserializeJSON(RetUserPlan.USERDOWNGRADEKEYWORD)/>

    <cfset listMLP = deserializeJSON(RetUserPlan.USERDOWNGRADEMLP)/>
    <cfset listShortUrl = deserializeJSON(RetUserPlan.USERDOWNGRADESHORTURL)/>

    <cfset downgradePlanId = RetUserPlan.USERDOWNGRADEPLAN/>
    <cfloop query="#plans.DATA#">
        <cfif id EQ RetUserPlan.USERDOWNGRADEPLAN>
            <cfset downgradePlanName = name/>    
        </cfif>
    </cfloop>

    <!--- GET LIST KEYWORD NAME --->
    <cfinvoke component="session.sire.models.cfc.keywords" method="GetKeywordByListId" returnvariable="RetKeywordsInfo">
        <cfinvokeargument name="inpListKeywordId" value="#listKeyword#"/>
    </cfinvoke>
  
    <cfif RetKeywordsInfo.RXRESULTCODE GT 0>
        <cfif arrayLen(RetKeywordsInfo.NAMELIST) GT 0>
            <cfset listKeywordName = RetKeywordsInfo.NAMELIST/>  
        </cfif>
    </cfif>

    <!--- GET LIST MLP NAME --->
    <cfinvoke component="session.sire.models.cfc.mlp" method="GetMLPByListId" returnvariable="RetMLPInfo">
        <cfinvokeargument name="inpListMLPId" value="#listMLP#"/>
    </cfinvoke>
  
    <cfif RetMLPInfo.RXRESULTCODE GT 0>
        <cfif arrayLen(RetMLPInfo.NAMELIST) GT 0>
            <cfset listMLPName = RetMLPInfo.NAMELIST/>  
        </cfif>
    </cfif>

    <!--- GET LIST SHORT URL NAME --->
    <cfinvoke component="session.sire.models.cfc.urltools" method="GetShortUrlByListId" returnvariable="RetShortUrlInfo">
        <cfinvokeargument name="inpListShortUrlId" value="#listShortUrl#"/>
    </cfinvoke>

    <cfif RetShortUrlInfo.RXRESULTCODE GT 0>
        <cfif arrayLen(RetShortUrlInfo.NAMELIST) GT 0>
            <cfset listShortUrlName = RetShortUrlInfo.NAMELIST/>  
        </cfif>
    </cfif>



    <cfif action EQ 'downgradeDetail'>
        <cfset displayDowngrade = 1/>
    </cfif>

</cfif>

<cfoutput>    
    
    <input type="hidden" name="" id="to-renew-plan" value="#userInfo.YEARLYRENEW#">
    <div class="portlet light bordered">
        <div class="portlet-body">
            <div class="new-inner-body-portlet2">
                <ul class="uk-tab session-tab" uk-tab>
                    <li><a href="##">Plan Features</a></li>
                    <cfif userInfo.USERTYPE EQ 1 OR (userInfo.USERTYPE EQ 2 AND userInfo.USERLEVEL EQ 1)>
                    <li><a href="##" id="addon_link">Add On's</a></li>
                    <li><a href="##" id="upgradeplan_tab">Upgrade Plan</a></li>
                    <li><a href="##" id="whiteglove_tab">White Glove</a></li>
                    <li><a href="##">Refer a Friend</a></li>
                    </cfif>
                </ul>

                <ul class="uk-switcher uk-margin content-tab-pricing">
                    <li>
                        <div class="uk-grid" uk-grid>
                            <div class="uk-width-1-4@xl uk-width-1-3@l uk-width-1-2@m">
                                <div class="statistical">
                                    <ul class="nav list-plan-statistical">
                                        <li>Plan Type: <b>#userPlanName#</b></li>
                                        
                                        <cfif RetUserPlan.PLANSTATUS EQ 4>
                                            <cfif userInfo.USERLEVEL EQ 3>
                                                <li>Limit Credit:  <b>#NumberFormat(RetUserPlan.USERPLANFIRSTSMSINCLUDED,',')#</b></li>
                                            </cfif>
                                        <cfelse>
                                            <li>Monthly Amount:  <b>$#NumberFormat(monthlyAmount,',')#</b></li>
                                        </cfif>
                                        
                                        <li>Credits Available: 
                                            <b>
                                                <cfif RetValBillingData.Balance GT 0>
                                                    #NumberFormat(RetValBillingData.Balance,',')#
                                                <cfelse>
                                                    0
                                                </cfif>
                                            </b>
                                        </li>
                                        <li>Keywords Available: 
                                            <b>
                                                <cfif RetUserPlan.KEYWORDAVAILABLE GT 0>
                                                    #RetUserPlan.KEYWORDAVAILABLE#
                                                <cfelse>
                                                    0
                                                </cfif>
                                            </b>
                                        </li>
                                        <li>Keywords Purchased: 
                                            <b>
                                                <cfset keywordsPurchased = RetUserPlan.KEYWORDPURCHASED + RetUserPlan.KEYWORDBUYAFTEREXPRIED>
                                                <cfif keywordsPurchased GT 0>
                                                    #keywordsPurchased#
                                                <cfelse>
                                                    0
                                                </cfif>
                                            </b>
                                        </li>

                                        <li>Keywords un-paid: 
                                            <b>
                                                <cfif RetUserPlan.TOTALKEYWORDHAVETOPAID GT 0>
                                                    #RetUserPlan.TOTALKEYWORDHAVETOPAID#
                                                <cfelse>
                                                    0
                                                </cfif>
                                            </b>
                                        </li>

                                        <li>
                                            
                                            MLPs Available: 
                                            <b>                                                                                                
                                                #((IsNumeric(RetUserPlan.MLPSLIMITNUMBER) AND RetUserPlan.MLPSLIMITNUMBER GT 0) ? (NumberFormat(RetUserPlan.MLPSLIMITNUMBER-RetUsedMLP.usedMLP,',') GT 0 ? (NumberFormat(RetUserPlan.MLPSLIMITNUMBER-RetUsedMLP.usedMLP,',')): 0 ) : "Unlimited")#                                                                                               
                                            </b>
                                        </li>
                                        <li>                                            
                                            Short URLs Available: 
                                            <b>                                                
                                               #((IsNumeric(RetUserPlan.SHORTURLSLIMITNUMBER) AND RetUserPlan.SHORTURLSLIMITNUMBER GT 0) ? ( NumberFormat(RetUserPlan.SHORTURLSLIMITNUMBER-RetUsedShortURL.usedURL,',') GT 0 ? ( NumberFormat(RetUserPlan.SHORTURLSLIMITNUMBER-RetUsedShortURL.usedURL,',')) : 0 ): "Unlimited")#                                                
                                            </b>
                                        </li>
                                        <li>Friends Referred: 
                                            <b>
                                                <cfif RetValUserReferInfo.FriendsReferred GT 0>
                                                    #RetValUserReferInfo.FriendsReferred#
                                                <cfelse>
                                                    0
                                                </cfif>
                                            </b>
                                        </li>
                                        <li>Referral Credits Received: 
                                            <b>
                                                <cfif RetValUserReferInfo.PromoCreditsReceived GT 0>
                                                    #RetValUserReferInfo.PromoCreditsReceived#
                                                <cfelse>
                                                    0
                                                </cfif>
                                            </b>
                                        </li>
                                        <cfif RetUserPlan.BILLINGTYPE EQ 2>
                                            <li>
                                                Annually Renew:
                                                <div class="to-renew-plan-choice" style="display: inline-block;">
                                                    <label class="checkbox-inline">
                                                        Yes
                                                        <input type="radio" id="agree-yearly-renew" class="radio-sendtype" value="1" name="yearly-renew" <cfif userInfo.YEARLYRENEW EQ 1> <cfoutput> checked </cfoutput> </cfif> >
                                                    </label>
                                                    <label class="checkbox-inline">
                                                        No
                                                        <input type="radio" id="disagree-yearly-renew" class="radio-sendtype" value="0" name="yearly-renew" <cfif userInfo.YEARLYRENEW EQ 0> <cfoutput> checked </cfoutput> </cfif>>
                                                    </label>
                                                </div>
                                                
                                                
                                            </li>
                                        </cfif>
                                        <li>
                                            WG Premium:                                            
                                            <cfif userInfo.WhiteGloveStatusID EQ 1>
                                                <b class="whiteglove_statusText">Active</b>
                                            <cfelse>
                                                <b class="whiteglove_statusText">Inactive</b>
                                            </cfif>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <cfif userInfo.USERTYPE EQ 1 OR (userInfo.USERTYPE EQ 2 AND userInfo.USERLEVEL EQ 1)>
                    <li>
                        <form id="buy-credit-keyword">
                            <div class="uk-grid" uk-grid>
                                <div class="uk-width-1-3@xl uk-width-1-2@l">
                                    <div class="wrap-buycredit form-gd uk-margin-large-bottom #RetUserPlan.PLANSTATUS EQ '4' ? 'hidden' : ''#">
                                        <div class="clearfix">
                                            <h3 class="form-heading">Buy Credits</h3>
                                        </div>
                                        
                                        <div class="row row-small">
                                            <div class="col-lg-8">
                                                <div class="row row-small">
                                                    <div class="col-xs-7">
                                                        <div class="form-group">
                                                            <label>Number of Credits <span>*</span></label>
                                                            <input type="text" class="form-control amount-input validate[custom[onlyNumberNotZero]]" placeholder="" id="numberSMS" name="numberSMS" style="text-align:right">
                                                            <label class="amount">Amount: <span class="text-green" id="amount-sms">$0</span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-5">
                                                        <span class="amountx">x <cfoutput>#NumberFormat(RetUserPlan.PRICEMSGAFTER,'._')#</cfoutput> Cents</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <a class="btn newbtn green-gd btt-calculate" id="sms_calculate">calculate</a>
                                            </div>
                                        </div>
                                    </div>    

                                    <div class="wrap-buycredit form-gd uk-margin-large-bottom">
                                        <div class="clearfix">
                                            <h3 class="form-heading">Buy Keywords</h3>
                                        </div>

                                        <cfif displayBuyKeyword EQ 1>
                                            <cfoutput>
                                                <input type="hidden" id="amount-keyword-to-send" name="amount" value="0">
                                                <input type="hidden" id="pricekeywordafter" value="#RetUserPlan.PRICEKEYWORDAFTER#">
                                            </cfoutput>

                                            <div class="row row-small">
                                                <div class="col-lg-8">
                                                    <div class="row row-small">
                                                        <div class="col-xs-7">
                                                            <div class="form-group">
                                                                <label>Number of Keywords <span>*</span></label>
                                                                <input type="text" class="form-control amount-input validate[custom[onlyNumberNotZero]]" placeholder="" id="numberKeyword" name="numberKeyword" style="text-align:right">
                                                                <label class="amount">Amount: <span class="text-green" id="amount-keyword">$0</span></label>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-5">
                                                            <span class="amountx">x <cfoutput>$#NumberFormat(RetUserPlan.PRICEKEYWORDAFTER,'._')#</cfoutput></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <a class="btn newbtn green-gd btt-calculate" id="keyword_calculate">calculate</a>
                                                </div>
                                            </div>
                                            <!---
                                            <div class="form-group">
                                                <h3 class="form-heading">Payment Method</h3>
                                                <select class="form-control validate[required]" id="payment_method" name ="payment_method">
                                                    <cfif RetGetPaymentMethodSetting.RESULT EQ 1>
                                                        <option value="1" selected="selected">WorldPay</option>
                                                    <cfelseif RetGetPaymentMethodSetting.RESULT EQ 2>
                                                        <option value="0" selected="selected">Paypal</option>
                                                    <cfelse>
                                                        <option value="1" selected="selected">WorldPay</option>
                                                        <option value="0">Paypal</option>
                                                    </cfif>
                                                </select>
                                            </div>
                                            --->
                                        <cfelse>
                                            <div class="row row-small">
                                                <div class="col-xs-12">
                                                    You need to upgrade your plan before buy keywords.
                                                </div>
                                            </div>    
                                        </cfif>
                                       
                                    </div>  
                                    
                                    <!--- IF USER DO NOT PAY FOR PLAN OR MONTHLY KEYWORD -> THEY CAN NOT BUY MORE KEYWORD/ CREDITS --->
                                    <cfif RetUserPlan.BILLINGTYPE EQ 1>
                                        <cfif RetUserPlan.PLANEXPIRED NEQ 1>
                                            <a href="##" class="btn newbtn green-gd" id="show-buyaddon-modal">Purchase</a>
                                            <a href="##" class="btn newbtn green-cancel" id="clear-calc-field">Cancel</a>
                                        </cfif>
                                    <cfelse>
                                        <cfif isDate(RetUserPlan.MONTHLYADDBENEFITDATE)>
                                            <cfif dateCompare(RetUserPlan.MONTHLYADDBENEFITDATE, NOW() ) GTE 0 >
                                                <a href="##" class="btn newbtn green-gd" id="show-buyaddon-modal">Purchase</a>
                                                <a href="##" class="btn newbtn green-cancel" id="clear-calc-field">Cancel</a>
                                            </cfif>    
                                       <!---  <cfelse>
                                            <a href="##" class="btn newbtn green-gd" id="show-buyaddon-modal">Purchase</a>
                                            <a href="##" class="btn newbtn green-cancel" id="clear-calc-field">Cancel</a> --->
                                        </cfif>   
                                    </cfif>

                                </div>
                            </div>
                        </form>
                    </li>
                    <li>                        
                        <cfinclude template="inc_upgrade_plan.cfm">
                    </li>
                    <li>                                                
                        <cfinclude template="inc_white_glove.cfm">                                                    
                    </li>
                    <li>
                        <div class="uk-grid" uk-grid>
                            <div class="uk-width-1-3@xl uk-width-1-2@l">
                                <div class="form-gd">
                                    <div class="clearfix">
                                        <h3 class="form-heading">Invite Friends</h3>
                                    </div>
                                </div>

                            <!--- <div class="tab-invite tabbable tabbable-tabdrop">
                                <ul class="nav nav-tabs tab-invite-ul">
                                    <li class="active">
                                        <a href="##invitetab1" data-toggle="tab">Refer via E-mail</a>
                                    </li>
                                    <li>
                                        <a href="##invitetab2" data-toggle="tab">Refer via Social Media</a>
                                    </li>
                                </ul>
                                <div class="tab-content tab-invite-content">
                                    <div class="tab-pane active" id="invitetab1">
                                        <div class="form-group">
                                            <label for="">Email</label>
                                            <div class="row row-small">
                                                <div class="col-xs-9 col-lg-10">
                                                    <input type="text" class="form-control" />
                                                </div>
                                                <div class="col-xs-3 col-lg-2">
                                                    <a href="##" class="btn btn-block green-gd">add</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="block" style="font-size: 1.8rem;">Import Contacts</label>
                                            <a href="##" class="btt-invite-email active">
                                                <img class="img-responsive" src="\session\sire\assets/layouts/layout4/img/icon-gmail.png" alt="">
                                            </a>
                                            <a href="##" class="btt-invite-email">
                                                <img class="img-responsive" src="\session\sire\assets/layouts/layout4/img/icon-outlook.png" alt="">
                                            </a>
                                            <a href="##" class="btt-invite-email">
                                                <img class="img-responsive" src="\session\sire\assets/layouts/layout4/img/icon-yahoo.png" alt="">
                                            </a>
                                        </div>

                                        <div class="form-group">
                                            <label class="block" style="font-size: 1.8rem;">Selected Emails</label>
                                        </div>

                                        <div class="holder-friend"></div>

                                        <div class="dv-send-invite">
                                            <button type="submit" class="btn newbtn green-gd">Send Invite</button>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="invitetab2">
                                        <div class="form-group">
                                            <label class="block">Share</label>
                                            <a href="##" class="invite-social-ic fb">
                                                <i class="fa fa-facebook-square"></i>
                                            </a>
                                            <a href="##" class="invite-social-ic tw">
                                                <i class="fa fa-facebook-square"></i>
                                            </a>
                                            <a href="##" class="invite-social-ic gp">
                                                <i class="fa fa-google-plus-square"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div> --->   
                            <div class="tab-invite tabbable tabbable-tabdrop">
                                <ul class="nav nav-tabs tab-invite-ul">
                                    <li class="active">
                                        <a href="##refer_via_email" data-toggle="tab">Refer via E-mail</a>
                                    </li>
                                    <li>
                                        <a href="##refer_via_social" data-toggle="tab">Refer via Social Media</a>
                                    </li>
                                </ul>
                                <div class="tab-content tab-invite-content">
                                    <div class="tab-pane active" id="refer_via_email">
                                        <div class="form-group">
                                            <label for="">Email</label>
                                            <div class="row row-small">
                                                <div class="col-xs-9 col-lg-10">
                                                    <form id="email_form">
                                                        <input type="text" class="form-control validate[required,custom[email]]" id="emailadd" />
                                                    </form>
                                                </div>
                                                <div class="col-xs-3 col-lg-2">
                                                    <a class="btn btn-block green-gd" id="add-email-btn">add</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="block">Import Contacts</label>
                                            <a class="btt-invite-email gmail gmail-contacts-btn" id="gmail-contacts-btn">
                                                <img class="img-responsive" src="../assets/layouts/layout4/img/icon-gmail.png" alt="">
                                            </a>
                                            <a class="btt-invite-email outlook outlook-contacts-btn" id="outlook-contacts-btn">
                                                <img class="img-responsive" src="../assets/layouts/layout4/img/icon-outlook.png" alt="">
                                            </a>
                                            <a class="btt-invite-email yahoo yahoo-contacts-btn" id="yahoo-contacts-btn">
                                                <img class="img-responsive" src="../assets/layouts/layout4/img/icon-yahoo.png" alt="">
                                            </a>
                                        </div>

                                        <div class="holder-friend friends-list" id="gmail" style="display: none;">
                                            <div class="row list">
                                            </div>
                                        </div>
                                        <div class="holder-friend friends-list" id="outlook" style="display: none;">
                                            <div class="row list">
                                            </div>
                                        </div>
                                        <div class="holder-friend friends-list" id="yahoo" style="display: none;">
                                            <div class="row list">
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="form-group">
                                            <label class="block">Selected Emails</label>
                                        </div>

                                        <div class="holder-friend selected-email content" id="selected-email">
                                            
                                        </div>

                                        <div class="dv-send-invite">
                                            <button class="btn newbtn green-gd btn-send-invite" id="send-email-invitation-btn">Send Invite</button>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="refer_via_social">
                                        <div class="form-group social share-url">
                                            <label class="block">Share</label>
                                            <a class="invite-social-ic fb social-auth">
                                                <i class="fa fa-facebook-square"></i>
                                            </a>
                                            <a class="invite-social-ic tw social-auth-tw">
                                                <i class="fa fa-twitter-square"></i>
                                            </a>
                                            <a class="invite-social-ic gp social-auth">
                                                <i class="fa fa-google-plus-square"></i>
                                            </a>
                                        </div>

                                        <div class="holder-friend friends-list" id="twitter" style="display: none">
                                            <div id="twitter-friend-list" class="row list">
                                                
                                            </div>
                                        </div>

                                        <div class="selected-friends-section" style="display: none;">
                                            <div class="form-group">
                                                <label class="block">Selected Friends</label>
                                            </div>
                                            <div class="holder-friend selected-email" id="twitter-friend-list-selected">

                                            </div>
                                            <div class="dv-send-invite">
                                                <button class="btn newbtn green-gd btn-send-invite send-invitation">Send Invite</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>                                                                                    
                        </div>
                    </li>
                    </cfif>
                </ul>
        </div>
    </div>
</div>
</cfoutput>

<cfinclude template="inc_upgrade_plan_popup.cfm"/>


<!--- For invite friend function --->
<cfinclude template="/session/sire/configs/paths.cfm" />

<link rel="stylesheet" type="text/css" href="/session/sire/assets/pages/styles/invite-friend.css">

<cfif !structKeyExists(application, 'objMonkehTweet')>
    <cfscript>
        application.objMonkehTweet =
        createObject('component','public.sire.components.monkehTweets.com.coldfumonkeh.monkehTweet').init(
            consumerKey = _TwConsumerKey,
            consumerSecret = _TwConsumerKeySecret
            );
        </cfscript>
    </cfif>

    <cfinvoke component="session.sire.models.cfc.referral" method="GetUserReferralCode" returnvariable="userRFCode"></cfinvoke>

    <cfoutput>
        <script type="text/javascript">
            var rfLink =   "#userRFCode.RFLink#"+"&type=3";
            var shareUrl =  encodeURIComponent("https://"+window.location.hostname+rfLink);
            var hostname = ("https://"+window.location.hostname);
        </script>
    </cfoutput>

    <cfinvoke component="session.sire.models.cfc.referral" method="GetSMSQuota" returnvariable="smsQuota"></cfinvoke>

    <cfif smsQuota.RXRESULTCODE NEQ 1>
        <cfset smsQuota.SMSQUOTA = 0>
    </cfif>

    <input type="hidden" value="<cfoutput>#smsQuota.SMSQUOTA#</cfoutput>" id="sms-quota" />

    <!-- Modal -->
    <div class="bootbox modal fade" id="ConfirmSendInvitation" tabindex="-1" role="dialog" aria-labelledby="ConfirmSendInvitation" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title title">Confirm send invitation</h4>
                </div>
                <div class="modal-body">
                    <p>Do you want to send the invitation?</p>
                    <div id="capcha">
                        <div class="form-group">
                            <script src='https://www.google.com/recaptcha/api.js'></script>
                            <cfoutput>
                                <div class="g-recaptcha" data-sitekey="#PUBLICKEY#"></div>
                            </cfoutput>
                        </div>
                        <div id="captcha-message" style="color: #dd1037"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success btn-success-custom" id="btn-send-invitation-confirm"> Confirm </button>
                    &nbsp; &nbsp; &nbsp;
                    <button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal">Cancel</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


<div id="my-id" class="uk-modal">  
    <div class="uk-modal-dialog">  
        <a class="uk-modal-close uk-close"></a>  
        <!--- <img src="images/myimage.jpg" alt="" width="" height="" />   --->
    </div>  
</div>  

    <!--- End invite friend function --->

    <script type="text/javascript">
        var activeTab = <cfoutput>"#activeTab#"</cfoutput>;
        var showPopupDowngrade = <cfoutput>"#displayDowngrade#"</cfoutput>;
        var downgradePlanId = <cfoutput>#downgradePlanId#</cfoutput>;
        var downgradePlanName = "<cfoutput>#downgradePlanName#</cfoutput>";

        var listKeyword = <cfoutput>#SerializeJSON(listKeyword)#</cfoutput>;
        var listKeywordName = <cfoutput>#SerializeJSON(listKeywordName)#</cfoutput>;
        var listMLP = <cfoutput>#SerializeJSON(listMLP)#</cfoutput>;
        var listMLPName= <cfoutput>#SerializeJSON(listMLPName)#</cfoutput>;
        var listShortUrl = <cfoutput>#SerializeJSON(listShortUrl)#</cfoutput>;
        var listShortUrlName = <cfoutput>#SerializeJSON(listShortUrlName)#</cfoutput>;
        var paypalCheckoutUrl = <cfoutput>'#_PaypalCheckOutUrl#'</cfoutput>
    </script>

    <cfinclude template="../views/commons/sireplandetails.cfm">

    <cfparam name="variables._title" default="My Plan - Sire">

    <cfinclude template="../views/commons/what-mlp-keyword.cfm">
    <cfinclude template="../views/layouts/master.cfm">