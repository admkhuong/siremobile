<cfscript>
    createObject("component", "public.sire.models.helpers.layout")
        .addJs("/public/js/jquery.tmpl.min.js", true)
        .addJs("/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js")
        .addCss("/public/js/jquery-ui-1.11.4.custom/jquery-ui.css", true)
        
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", false)

        .addCss("/session/sire/assets/global/plugins/uniform/css/uniform.default.css", true)
        .addJs('/session/sire/assets/global/scripts/filter-bar-generator.js', true)
        .addJs('/session/sire/assets/global/plugins/uniform/jquery.uniform.min.js', true)
        .addJs("/session/sire/assets/pages/scripts/calendar_accepted_event.js");        
</cfscript>


<!--- <cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" /> --->

<cfparam name="isDialog" default="0">
<cfparam name="isUpdateSuccess" default="">


<div class="portlet light bordered">
    <!--- Filter bar --->
    <div class="row">
        <div class="col-sm-3 col-md-4">
            <h2 class="page-title">Accepted Events List</h2>
        </div>
        <div class="col-sm-9 col-md-8">
            <div id="box-filter" class="form-gd">
            </div>
        </div>
    </div>
    <!--- Listing --->
    <div class="row">
        <div class="col-md-12">
            <div class="re-table">
                <div class="table-responsive">
                    <table id="tblListEMS" class="table-striped dataTables_wrapper">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<cfset RetCustomerInfo = ''>
<cfparam name="maskedNumber" default="">
<cfparam name="UserID" default="">
<cfparam name="expirationDate" default="">
<cfparam name="firstName" default="">
<cfparam name="lastName" default="">
<cfparam name="line1" default="">
<cfparam name="city" default="">
<cfparam name="state" default="">
<cfparam name="zip" default="">
<cfparam name="country" default="">
<cfparam name="phone" default="">
<cfparam name="emailAddress" default="">
<cfparam name="hidden_card_form" default="">

<!--- Change CC info before charge --->

<!--- End change CC info before charge --->
<cfparam name="variables._title" default="Accepted Events List">
<cfinclude template="../views/layouts/master.cfm">

