<cfparam name="cid" default="1" />

<cfparam name="inpcontactstring" default="888"/>
<cfparam name="inpbatchId" default="1"/>
<cfparam name="emai" default=""/>
<cfparam name="phone" default=""/>
<!--- auto create a camapign for user first uses --->
<cfinvoke component="session.sire.models.cfc.calendar" method="SetCalendarConfigurationDefault" returnvariable="RetValSetCalendarConfigurationDefault"></cfinvoke> 
<cfif RetValSetCalendarConfigurationDefault.RXRESULTCODE GT 0>
	<cfset cid = RetValSetCalendarConfigurationDefault.CALENDARID>
	<cfset inpbatchId = RetValSetCalendarConfigurationDefault.BATCHID>
</cfif>

<cfinvoke component="session.sire.models.cfc.calendar" method="GetCalendarConfiguration" returnvariable="RetValGetCalendarConfiguration">
	<cfinvokeargument name="inpCalendarId" value="#cid#">
</cfinvoke> 

<cfinvoke component="session.sire.models.cfc.calendar" method="GetUserInfo" returnvariable="RetValGetUserInfo">
	<cfinvokeargument name="inpUserId" value="#session.UserId#">
</cfinvoke> 

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addJs("/public/sire/js/moment.min.js", true)
        .addCss("/session/sire/css/calendar_config.css")
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)
        .addJs("/public/sire/js/select2.min.js", true)
        .addJs("/session/sire/assets/global/plugins/bootstrap-datetimepicker/js/jquery.timepicker.min.js ", true)
        .addCss("/public/sire/css/jquery.timepicker.min.css", true)
        .addJs("/session/sire/assets/pages/scripts/calendar-config.js")
        .addCss("/public/sire/css/jquery.timepicker.min.css", true)
		.addCss("../assets/layouts/layout4/css/custom.css", true);
</cfscript>

<!--- 
	Notes:
	Tasks:	
--->
 

<cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPDataById" returnvariable="rxCP">
	<cfinvokeargument name="inpBatchId" value="#RetValGetCalendarConfiguration.BATCHID#"/>
	<cfinvokeargument name="inpQID" value="1"/>
</cfinvoke>

<!--- <cfdump var="#rxCP.CPSOOBj.StartHour_ti#" abort="true"/> --->


<div class="portlet light bordered">
	
	<div class="row">
       <div class="col-xs-12 col-md-12">
	   <h2 style="float: right; margin: 8px;">
			<i class="fa fa-cogs" aria-hidden="true"></i>
	   </h2>
	       <h2>Calendar Configuration</h2>
       </div>
   </div>
</div>
	
<div class="portlet">
	<div class="row">
       <div class="col-xs-12 col-md-6">
			<div style="display: table; padding: 0 .5em; margin: 0 auto; height: 34px; background-color: #fff; width: 100%; border-radius: 3px;">
			    <div class="col-xs-6 config-title">
			        Auto Schedule Reminder Message 
			    </div>
			    <div class="col-xs-6" style="text-align: right; display: table-cell; vertical-align: middle;">
			       <label class="toggle-switch">
					  <input id="ASR" type="checkbox" <cfif RetValGetCalendarConfiguration.ASR GT 0>checked</cfif>>
					  <span class="toggle-slider round"></span> 
					</label>
			    </div>
			</div>
       </div>
	   <div class="col-xs-12 col-md-6">
			<div style="display: table; padding: 0 .5em; margin: 0 auto; height: 34px; background-color: #fff; width: 100%; border-radius: 3px;">
			    <div class="col-xs-7 config-title">
			        Calendar Campaign (<cfoutput>#RetValGetCalendarConfiguration.BATCHID#</cfoutput>)
			    </div>
			    <div class="col-xs-5" style="text-align: right; display: table-cell; vertical-align: middle;">
			    	<h4><a target="_blank" href="/session/sire/pages/campaign-calendar-edit?campaignid=<cfoutput>#RetValGetCalendarConfiguration.BATCHID#</cfoutput>" class="alert-link">
			       			<!--- <cfoutput>#RetValGetCalendarConfiguration.BATCHID#</cfoutput> --->
			       			Update message
			       		</a>
			   		</h4>
			    </div>
			</div>	
       </div>
	</div>

	<div class="row" style="margin-top: 1em;">
		<div class="col-xs-12 col-md-6">
			<div style="display: table; padding: .3em .5em 0 .5em; margin: 0 auto; height: 34px; background-color: #fff; width: 100%; border-radius: 3px;">
				<div class="col-xs-6 config-title">
					Default Time Display 
				</div>
				<div class="col-xs-6" style="text-align: right; display: table-cell; vertical-align: middle; text-align: right;">
					<label class="webfont_scrolltime">
						<input type="text" id="TimeStepScroll" class="time ui-timepicker-input TimeStepScroll form-control" autocomplete="off" style=" width: 103px; display: inline; border: 1px solid #e5e5e5; text-align: right;">
					</label>
				</div>
			</div>	
				
		</div>		
		<div class="col-xs-12 col-md-6">
			<div style="display: table; padding: .3em .5em 0 .5em; margin: 0 auto; height: 34px; background-color: #fff; width: 100%; border-radius: 3px;">
				<div class="col-xs-6 config-title">
					Batch Schedule Setting
				</div>
				<div class="col-xs-6" style="text-align: right; display: table-cell; vertical-align: middle; text-align: right;">
					Start Time
					<label class="webfont_scrolltime">						
						<input type="text" id="StartTimeStepScroll" class="time ui-timepicker-input StartTimeStepScroll form-control" autocomplete="off" style=" width: 103px; display: inline; border: 1px solid #e5e5e5; text-align: right;">
					</label>
					End Time
					<label class="webfont_scrolltime">						
						<input type="text" id="EndTimeStepScroll" class="time ui-timepicker-input EndTimeStepScroll form-control" autocomplete="off" style=" width: 103px; display: inline; border: 1px solid #e5e5e5; text-align: right;">
					</label>
				</div>
			</div>
		</div>
             
	</div>
	<div class="row" style="margin-top: 1em;">
		<div class="col-xs-12 col-md-6">
	   		<div style="display: table; padding: .3em .5em 0 .5em; margin: 0 auto; height: 45px; background-color: #fff; width: 100%; border-radius: 3px;">
			    <div class="col-xs-6 config-title">
			        Notification Method
			    </div>
			    <div class="col-xs-6 notifi-method"> <!---  style="text-align: left; display: table-cell; vertical-align: middle;" --->
			    	<span class="sms-method">
						<input type="radio" name="noti_method" value="sms" id="sms" <cfif RetValGetCalendarConfiguration.NOTIFIMETHOD EQ 1>checked</cfif>>
						<label for="sms">SMS Number</label>
					</span>
					<span class="email-method">
						<input type="radio" name="noti_method" value="email" id="email" <cfif RetValGetCalendarConfiguration.NOTIFIMETHOD EQ 0>checked</cfif>>
						<label for="email">Email Address</label>
					</span>
			    </div>
			</div>
			<div class="uk-width-1-2@l">
				<form name="frm-notifi-method" id="frm-notifi-method">
				<div class="uk-grid uk-grid-small sms-number-wrap noMinus" <cfif RetValGetCalendarConfiguration.NOTIFIMETHOD EQ 0>style="display:none;"</cfif> uk-grid>
					<div class="uk-width-expand@s hold-more-content col-xs-12" style="background-color: #fff;">
						<!---
						<div class="uk-grid uk-grid-small uk-flex-middle sms-number-item">
							<div class="uk-width-expand col-xs-10">
								<input type="text" class="form-control phone-style validate[custom[usPhoneNumber]] sms-number" disabled value="<cfoutput>#RetValGetUserInfo.CONTACTSTRING#</cfoutput>"/>
							</div>
							<div class="uk-width-auto col-for-more col-xs-2">
								<a href="#" class="trigger-plus">
									<i class="fa fa-plus-circle"></i>
								</a>
							</div>
						</div>
						--->
						<!--- Loop show list phone --->
						<cfif RetValGetCalendarConfiguration.NOTIFIMETHOD EQ 1>
							<cfif RetValGetCalendarConfiguration.NOTIFIITEM NEQ ''>
								<cfloop list="#RetValGetCalendarConfiguration.NOTIFIITEM#" index="phone">
									<!---<cfif phone NEQ RetValGetUserInfo.CONTACTSTRING>
									</cfif>--->
									<div class="uk-grid uk-grid-small uk-flex-middle sms-number-item">
										<div class="uk-width-expand col-xs-10">
											<input type="text" class="form-control validate[custom[usPhoneNumber]] sms-number" value="<cfoutput>#phone#</cfoutput>"/>
										</div>
										<div class="uk-width-auto col-for-more col-xs-1">
											<a href="##" class="trigger-minus"><i class="fa fa-minus-circle"></i></a>
										</div>
										<div class="uk-width-auto col-for-more col-xs-1">
											<a href="##" class="trigger-plus"><i class="fa fa-plus-circle"></i></a>
										</div>
									</div>
								</cfloop>
							<cfelse>
								<div class="uk-grid uk-grid-small uk-flex-middle sms-number-item">
									<div class="uk-width-expand col-xs-10">
										<input type="text" class="form-control phone-style validate[custom[usPhoneNumber]] sms-number" value=""/>
									</div>
									<div class="uk-width-auto col-for-more col-xs-2">
										<a href="#" class="trigger-plus">
											<i class="fa fa-plus-circle"></i>
										</a>
									</div>
								</div>
							</cfif>
						<cfelse>
							<div class="uk-grid uk-grid-small uk-flex-middle sms-number-item">
								<div class="uk-width-expand col-xs-10">
									<input type="text" class="form-control phone-style validate[custom[usPhoneNumber]] sms-number" value=""/>
								</div>
								<div class="uk-width-auto col-for-more col-xs-2">
									<a href="#" class="trigger-plus">
										<i class="fa fa-plus-circle"></i>
									</a>
								</div>
							</div>
						</cfif>
					</div>
				</div> 
				<div class="uk-grid uk-grid-small email-add-wrap noMinus" <cfif RetValGetCalendarConfiguration.NOTIFIMETHOD EQ 1>style="display:none;"</cfif> uk-grid>
					<div class="uk-width-expand@s hold-more-content col-xs-12" style="background-color: #fff;">
						<!---
						<div class="uk-grid uk-grid-small uk-flex-middle email-add-item">
							<div class="uk-width-expand col-xs-10">
								<input type="text" class="form-control" disabled value="<cfoutput>#RetValGetUserInfo.EMAIL#</cfoutput>"/>
							</div>
							<div class="uk-width-auto col-for-more col-xs-2">
								<a href="#" class="trigger-plus">
									<i class="fa fa-plus-circle"></i>
								</a>
							</div>
						</div>
						--->
						<!--- Loop show list email --->
						<cfif RetValGetCalendarConfiguration.NOTIFIMETHOD EQ 0>
							<cfif RetValGetCalendarConfiguration.NOTIFIITEM NEQ '' >
								<cfloop list="#RetValGetCalendarConfiguration.NOTIFIITEM#" index="email">
									<!---<cfif email NEQ RetValGetUserInfo.EMAIL></cfif>--->
									<div class="uk-grid uk-grid-small uk-flex-middle email-add-item">
										<div class="uk-width-expand col-xs-10">
												<input type="text" class="form-control validate[custom[email]]" value="<cfoutput>#email#</cfoutput>"/>
										</div>
										<div class="uk-width-auto col-for-more col-xs-1">
												<a href="##" class="trigger-minus"><i class="fa fa-minus-circle"></i></a>
										</div>
										<div class="uk-width-auto col-for-more col-xs-1">
											<a href="##" class="trigger-plus"><i class="fa fa-plus-circle"></i></a>
										</div>
									</div>
									
								</cfloop>
							<cfelse>
								<div class="uk-grid uk-grid-small uk-flex-middle email-add-item">
									<div class="uk-width-expand col-xs-10">
										<input type="text" class="form-control" value=""/>
									</div>
									<div class="uk-width-auto col-for-more col-xs-2">
										<a href="#" class="trigger-plus">
											<i class="fa fa-plus-circle"></i>
										</a>
									</div>
								</div>
							</cfif>
						<cfelse>
							<div class="uk-grid uk-grid-small uk-flex-middle email-add-item">
								<div class="uk-width-expand col-xs-10">
									<input type="text" class="form-control" value=""/>
								</div>
								<div class="uk-width-auto col-for-more col-xs-2">
									<a href="#" class="trigger-plus">
										<i class="fa fa-plus-circle"></i>
									</a>
								</div>
							</div>
						</cfif>
					</div>
				</div>
				<div class="btn-save-notification col-xs-12" style="padding: .3em 1.6em 0 .5em; margin: 0 auto; height: 45px; background-color: #fff; width: 100%; border-radius: 3px;">
                	<a id="savenotifisetting" class="btn green-gd btn-re pull-right">Save</a>
                </div>
                </form>	
			</div>
		</div>		
		<div class="col-xs-12 col-md-6">
			<div style="display: table; padding: .3em .5em 0 .5em; margin: 0 auto; height: 43px; background-color: #fff; width: 100%; border-radius: 3px;">
					<div class="col-xs-6 config-title">
						Notification
					</div>
					<div id="IntervalValue" style="text-align: right; display: table-cell; vertical-align: middle; color: #000; font-weight: 500; padding-right: 1em; width: 100px;">
						<select name="VALUE" class ="form-control Select2">
							<option value="1" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '1'>selected</cfif>>1</option>
							<option value="2" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '2'>selected</cfif>>2</option>
							<option value="3" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '3'>selected</cfif>>3</option>
							<option value="4" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '4'>selected</cfif>>4</option>
							<option value="5" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '5'>selected</cfif>>5</option>
							<option value="6" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '6'>selected</cfif>>6</option>
							<option value="7" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '7'>selected</cfif>>7</option>
							<option value="8" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '8'>selected</cfif>>8</option>
							<option value="9" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '9'>selected</cfif>>9</option>
							<option value="10" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '10'>selected</cfif>>10</option>
							<option value="11" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '11'>selected</cfif>>11</option>
							<option value="12" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '12'>selected</cfif>>12</option>
							<option value="13" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '13'>selected</cfif>>13</option>
							<option value="14" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '14'>selected</cfif>>14</option>
							<option value="15" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '15'>selected</cfif>>15</option>
							<option value="16" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '16'>selected</cfif>>16</option>
							<option value="17" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '17'>selected</cfif>>17</option>
							<option value="18" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '18'>selected</cfif>>18</option>
							<option value="19" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '19'>selected</cfif>>19</option>
							<option value="20" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '20'>selected</cfif>>20</option>
							<option value="21" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '21'>selected</cfif>>21</option>
							<option value="22" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '22'>selected</cfif>>22</option>
							<option value="23" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '23'>selected</cfif>>23</option>
							<option value="24" <cfif RetValGetCalendarConfiguration.INTERVALVALUE EQ '24'>selected</cfif>>24</option>
						</select>
					</div>
					<div id="IntervalType" style="text-align: right; display: table-cell; vertical-align: middle; color: #000; font-weight: 500; width: 100px;">
						<select name="ITYPE" class ="form-control Select2">
							<option value="HOUR" <cfif RetValGetCalendarConfiguration.INTERVALTYPE EQ 'HOUR'>selected</cfif>>Hour(s)</option>
							<option value="DAY" <cfif RetValGetCalendarConfiguration.INTERVALTYPE EQ 'DAY'>selected</cfif>>Day(s)</option>
						</select>
					</div>

				</div>	
				<br>
				<div style="display: table; padding: .3em .5em 0 .5em; margin: 0 auto; height: 45px; background-color: #fff; width: 100%; border-radius: 3px;">
			    <div class="col-xs-7 config-title">
			        Sync Google Calendar 
			    </div>
			    <div class="col-xs-5" style="text-align: right; display: table-cell; vertical-align: middle;">
			    	 <a id="signin-button" class="btn green-gd btn-re">Sync</a>
			    </div>
			</div>						
       </div>
	</div>
	<div class="row" style="margin-top: 1em;">
		<div class="col-xs-12 col-md-6">	   		
		</div>		
		<div class="col-xs-12 col-md-6">			
			<!--- <div style="display: table; padding: .3em .5em 0 .5em; margin: 0 auto; height: 45px; background-color: #fff; width: 100%; border-radius: 3px;">
			    <div class="col-xs-7 config-title">
			        Sync Google Calendar 
			    </div>
			    <div class="col-xs-5" style="text-align: right; display: table-cell; vertical-align: middle;">
			    	 <a id="signin-button" class="btn green-gd btn-re">Sync</a>
			    </div>
			</div>	 --->
       </div>
	</div>
	<!--- Back to Sire Calendar --->
	<div class="row" style="margin-top: 1em;">
       <div class="col-xs-12 col-md-6">
			<a href="/session/sire/pages/calendar" class="btn green-gd btn-re">Back to Sire Calendar</a>
       </div>
	</div>
</div>

<input type="text" id="nextPageToken" style ="display:none;">

<cfparam name="variables._title" default="Sire Calendar Configuration">
<cfinclude template="../views/layouts/master.cfm">


<!--- http://jonthornton.github.io/jquery-timepicker/ --->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script>

<script src="https://apis.google.com/js/api.js"></script>



<!--- Include javascript here for better cf documentation --->
<script type="application/javascript">

	<!--- init Interval --->
	var cpData = '<cfoutput>#SerializeJSON(rxCP)#</cfoutput>'
	var d = JSON.parse(cpData)
	var ControlPointData = {
	'AF' : d.CPOBJ.AF,
	'API_ACT' : d.CPOBJ.API_ACT,
	'API_DATA' : d.CPOBJ.API_DATA,
	'API_DIR' : d.CPOBJ.API_DIR,
	'API_DOM' : d.CPOBJ.API_DOM,
	'API_PORT' : d.CPOBJ.API_PORT,
	'API_RA' : d.CPOBJ.API_RA,
	'API_TYPE' : d.CPOBJ.API_TYPE,
	'BOFNQ' : d.CPOBJ.BOFNQ,
	'CONDITIONS' : d.CPOBJ.CONDITIONS,
	'ERRMSGTXT' : "",
	'GID' : d.CPOBJ.GID,
	'ID' : d.CPOBJ.ID,
	'IENQID' : d.CPOBJ.IENQID,
	'IHOUR' : d.CPOBJ.IHOUR,
	'IMIN' : d.CPOBJ.IMIN,
	'IMRNR' : d.CPOBJ.IMRNR,
	'INOON' : d.CPOBJ.INOON,
	'INRMO' : d.CPOBJ.INRMO,
	'ITYPE' : d.CPOBJ.ITYPE,
	'IVALUE' : d.CPOBJ.IVALUE,
	'LMAX' : d.CPOBJ.LMAX,
	'LDIR' : d.CPOBJ.LDIR,
	'LMSG' : d.CPOBJ.LMSG,
	'OIG' : d.CPOBJ.OIG,
	'OPTIONS' : d.CPOBJ.OPTIONS,
	'ESOPTIONS' : d.CPOBJ.ESOPTIONS,
	'REQUIREDANS' : "undefined",
	'RQ' : d.CPOBJ.RQ,
	'SCDF' : d.CPOBJ.SCDF,
	'TDESC' : d.CPOBJ.TDESC,
	'TEXT' : d.CPOBJ.TEXT,
	'TYPE' : d.CPOBJ.TYPE,
	'SWT' : d.CPOBJ.SWT,
	'ANSTEMP' : d.CPOBJ.ANSTEMP,
	'T64' : d.CPOBJ.T64
	};
	var appBatchId = <cfoutput>#RetValGetCalendarConfiguration.BATCHID#</cfoutput>

	var inpCPE =  $(".setInterval"); 
	inpCPE.find('#ITYPE').val(ControlPointData.ITYPE);
	inpCPE.find('#IVALUE').val(ControlPointData.IVALUE);
	inpCPE.find('#IMRNR').val(ControlPointData.IMRNR);

	<!--- END: init Interval --->

	var clientId = '20348249418-cpq9052uguml07j3evagt0f2mv7fejb6.apps.googleusercontent.com';
	var apiKey = 'AIzaSyC4Hel1V9G57UDYm7068M2xlRkcPwlPb4c';
	var eventObj =  { 'eventName':"", 'startDate':"", 'endDate':"", 'status':""}
	var listEvent = new Array();
	var nextPageToken = '';
	var listGoogleId = [];
	var CalendarConfigurationInterval = 'INTERVAL 1 DAY';
	var CalendarConfigurationIntervalResend = '';

	var today = new Date();
	var nextday = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;
	var mmnext = mm + 1;
	var yyyy = today.getFullYear();
	var yyyynext = today.getFullYear();

	if(dd<10) {
		dd = '0'+dd;
	} 
	if(mm<10) {
		mm = '0'+mm;
	} 

	if(mm==13){
		mm = '01';
		yyyy = yyyy + 1;
	}

	if(mmnext<10) {
		mmnext = '0'+mmnext;
	} 

	if(mmnext==13){
		mmnext = '01';
		yyyynext = yyyynext + 1;
	}

	if(mmnext==14){
		mmnext = '02';
		yyyynext = yyyynext + 1;
	}
	today = yyyy+'-'+mm + '-' + dd + 'T00:00:00Z';
	nextday = yyyynext+'-'+mmnext + '-' + dd + 'T00:00:00Z';
	var options = {
	'calendarId': 'primary',
	'singleEvents': true,
	'maxResults':100,
	//'pageToken': 'CigKGjMzNzFyZGwwNGEyNGt1cDY4bG9jdjgybnN0GAEggICAit_724QWGg0IABIAGMDZw_3cqdgC',
	'timeMin': today,
	'timeMax': nextday,
	}
	var noti_method = <cfoutput>#RetValGetCalendarConfiguration.NOTIFIMETHOD#</cfoutput>;
	<!--- On page loaded --->
	$(function() {

		
		$("#frm-notifi-method").validationEngine({promptPosition : "topLeft", scroll: true,focusFirstField : false,});

		$('input[type=radio][name=noti_method]').change(function() {

			if ($('#frm-notifi-method').validationEngine('validate')) {

				$('.btn-save-notification').show();
				// noti_method = this.value;
	            if (this.value == 'sms') {
					noti_method = 1;
					// 1: sms
					$('.sms-number-wrap').show();
					$('.email-add-wrap').hide();

					// user need to click save 
					return false;
					<!--- Save notification method --->				
					$.ajax({
						type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
						url: '/session/sire/models/cfc/calendar.cfc?method=SaveNotificationMethodCalendar&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
						dataType: 'json',
						async: true,
						data:  
						{ 
							inpCalendarId : '<cfoutput>#cid#</cfoutput>',
							inpNoticeMethod : 1
										
						},					  
						error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
						success:		
						<!--- Default return function for call back --->
						function(d) 
						{																																		
							<!--- RXRESULTCODE is 1 if everything is OK --->
							if (d.RXRESULTCODE == 1) {
								//console.log("Sms notification updated!");
							}			
						} 	
					});
	            }
	            if (this.value == 'email') {
					// 0: email
					noti_method = 0;
					$('.email-add-wrap').show();
					$('.sms-number-wrap').hide();

					// user need to click save 
					return false;
					<!--- Save notification method --->				
					$.ajax({
						type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
						url: '/session/sire/models/cfc/calendar.cfc?method=SaveNotificationMethodCalendar&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
						dataType: 'json',
						async: true,
						data:  
						{ 
							inpCalendarId : '<cfoutput>#cid#</cfoutput>',
							inpNoticeMethod : 0
										
						},					  
						error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
						success:		
						<!--- Default return function for call back --->
						function(d) 
						{																																		
							<!--- RXRESULTCODE is 1 if everything is OK --->
							if (d.RXRESULTCODE == 1) {
								//console.log("Email notification updated!");
							}										
						} 		
							
					});
            	}
            }else{
				if (this.value == 'email'){
					var radiobtn = document.getElementById("sms");
					radiobtn.checked = true;
				}else{
					var radiobtn = document.getElementById("email");
					radiobtn.checked = true;
				}
			}
        });
		// Notification by SMS
		var fieldStr = '';

		fieldStr += '<div class="uk-grid uk-grid-small uk-flex-middle sms-number-item">';
			fieldStr += '<div class="uk-width-expand col-xs-10">';
				fieldStr += '<input type="text" class="form-control phone-style validate[custom[usPhoneNumber]] sms-number" />';
			fieldStr += '</div>';
			fieldStr += '<div class="uk-width-auto col-for-more col-xs-1">';
				fieldStr += '<a href="##" class="trigger-minus"><i class="fa fa-minus-circle"></i></a>';
			fieldStr += '</div>';
			fieldStr += '<div class="uk-width-auto col-for-more col-xs-1">';
				fieldStr += '<a href="##" class="trigger-plus"><i class="fa fa-plus-circle"></i></a>';
			fieldStr += '</div>';
		fieldStr += '</div>';

		$('.sms-number-wrap').on('click', '.trigger-plus', function(event) {
			event.preventDefault();

			if ($(".sms-number-item").length >= 10) {
				return false;
			}

			$(this).parents('.hold-more-content').append(fieldStr);

			var $item = $(".sms-number-wrap .hold-more-content").children('.sms-number-item');

			if ($item.length > 1) {
				$('.sms-number-wrap').addClass('hasMinus');
			}
		});

		$('.sms-number-wrap').on('click', '.trigger-minus', function(event) {
			event.preventDefault();

			$(this).parents('.sms-number-item').remove();

			var $item = $(".sms-number-wrap .hold-more-content").children('.sms-number-item');

			if ($item.length === 1) {                    
				$('.sms-number-wrap').removeClass('hasMinus');
			}
		});
		// Notification by Email
		// var $rendered = new EJS({url:'assets/pages/scripts/sms-number-template.ejs'}).render();
            var fieldsStr = '';

            fieldsStr += '<div class="uk-grid uk-grid-small uk-flex-middle email-add-item">';
                fieldsStr += '<div class="uk-width-expand col-xs-10">';
                    fieldsStr += '<input type="text" class="form-control validate[custom[email]]" />';
                fieldsStr += '</div>';
                fieldsStr += '<div class="uk-width-auto col-for-more col-xs-1">';
                    fieldsStr += '<a href="##" class="trigger-minus"><i class="fa fa-minus-circle"></i></a>';
				fieldsStr += '</div>';
				fieldsStr += '<div class="uk-width-auto col-for-more col-xs-1">';
                    fieldsStr += '<a href="##" class="trigger-plus"><i class="fa fa-plus-circle"></i></a>';
                fieldsStr += '</div>';
            fieldsStr += '</div>';

            $('.email-add-wrap').on('click', '.trigger-plus', function(event) {
                event.preventDefault();

                if ($(".email-add-item").length >= 10) {
                    return false;
                }

                $(this).parents('.hold-more-content').append(fieldsStr);

                var $item = $(".email-add-wrap .hold-more-content").children('.email-add-item');

                if ($item.length > 1) {
                    $('.email-add-wrap').addClass('hasMinus');
                }
            });

            $('.email-add-wrap').on('click', '.trigger-minus', function(event) {
                event.preventDefault();

                $(this).parents('.email-add-item').remove();

                var $item = $(".email-add-wrap .hold-more-content").children('.email-add-item');

                if ($item.length === 1) {                    
                    $('.email-add-wrap').removeClass('hasMinus');
                }
            });

			 $('#savenotifisetting').on('click', function(event) {

			 	if ($('#frm-notifi-method').validationEngine('validate')) {

					var inpListOfEmails = [];
					var inpListOfPhones = [];
	                event.preventDefault();
					if(noti_method == 1){
						$(".sms-number-item").each(function(index, el) {
							var phone = $(el).find("input").val();
							if (phone !== "") {
								inpListOfPhones.push(phone);
							}
						});
						
					}else{
						$(".email-add-item").each(function(index, el) {
							var email = $(el).find("input").val();
							if (email !== "") {
								inpListOfEmails.push(email);
							}
						});
						
					}

					$.ajax({
						type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
						url: '/session/sire/models/cfc/calendar.cfc?method=SaveNotificationItemCalendar&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
						dataType: 'json',
						async: true,
						data:  
						{ 
							inpCalendarId : '<cfoutput>#cid#</cfoutput>',
							inpNoticeMethod : noti_method,
							inpNoticeSMS: inpListOfPhones.join(","),
							inpNoticeEMAIL: inpListOfEmails.join(",")	
						},					  
						error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
						success:		
						<!--- Default return function for call back --->
						function(d) 
						{																																		
							<!--- RXRESULTCODE is 1 if everything is OK --->
							if (d.RXRESULTCODE == 1) {
								bootbox.dialog({
								    message: "Update notification method success.",
								    title: "SIRE",
								    buttons: {
								        success: {
								            label: "Ok",
								            className: "btn btn-medium btn-success-custom",
								            callback: function() {
								            }
								        }
								    }
								});	
								//console.log("Notification item updated!");
							}										
						} 		
							
					});
					// alert(inpListOfPhones.join(","));
					// alert(inpListOfEmails.join(","));
				}
            });

		// INTERVAL RESEND
		$("#saveInterval").on('click',function(){
			saveInterval(1);
		})

		function saveInterval(auto){
			if( parseInt(appBatchId) > 0)
			{
				<!--- update interval only --->
				if(auto === 1){
					ControlPointData.IMRNR = (inpCPE.find('#IMRNR').val() == null) ? "0" : inpCPE.find('#IMRNR').val();
					ControlPointData.ITYPE = (inpCPE.find('#ITYPE').val() == null) ? "0" : inpCPE.find('#ITYPE').val();
					ControlPointData.IVALUE = (inpCPE.find('#IVALUE').val() == null) ? "0" : inpCPE.find('#IVALUE').val();	

					CalendarConfigurationIntervalResend = "INTERVAL " + ControlPointData.IVALUE + " " + ControlPointData.ITYPE + "; " + ControlPointData.IMRNR;	
					SaveIntervalResend();
				}
				else{
					ControlPointData.IMRNR = 0;
					ControlPointData.ITYPE = 0
					ControlPointData.IVALUE = 0;

					CalendarConfigurationIntervalResend = "";	
					SaveIntervalResend();
				}
				
				<!--- Save to XMLControlString --->				
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url: '/session/sire/models/cfc/control-point.cfc?method=CP_Save&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					async: false,
					data:  
					{ 
						INPBATCHID : appBatchId, 
						controlPoint : JSON.stringify(ControlPointData),
						inpSimpleViewFlag : 1,
						inpTemplateFlag : 0	 
					},					  
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						<!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},					  
					success:		
						<!--- Default return function for Async call back --->
					function(d) 
					{																						
						<!--- RXRESULTCODE is 1 if everything is OK --->
						if (d.RXRESULTCODE == 1) 
						{	
							bootbox.dialog({
							    message: "Update auto schedule success.",
							    title: "SIRE",
							    buttons: {
							        success: {
							            label: "Ok",
							            className: "btn btn-medium btn-success-custom",
							            callback: function() {
							            }
							        }
							    }
							});				
						}
						else
						{
							<!--- No result returned --->		
							bootbox.alert("General Error processing your request.");
						}			
										
					} 		
						
				});
			}
		}

		$(".Select2").select2( { theme: "bootstrap"} );

		function handleClientLoad() {
			// Loads the client library and the auth2 library together for efficiency.
			// Loading the auth2 library is optional here since `gapi.client.init` function will load
			// it if not already loaded. Loading it upfront can save one network request.
			gapi.load('client:auth2', initClient);
		}

		function initClient() {
			// Initialize the client with API key and People API, and initialize OAuth with an
			// OAuth 2.0 client ID and scopes (space delimited string) to request access.
			gapi.client.init({
				apiKey: apiKey,
				discoveryDocs: ["https://people.googleapis.com/$discovery/rest?version=v1"],
				clientId: clientId,
				scope: 'https://www.googleapis.com/auth/calendar'
			}).then(function () {
			// Listen for sign-in state changes.
			gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

			// Handle the initial sign-in state.
			updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
			});
		}

		function updateSigninStatus(isSignedIn) {
			// When signin status changes, this function is called.
			// If the signin status is changed to signedIn, we make an API call.
			if (isSignedIn) {
			gapi.client.load('calendar', 'v3',makeApiCall);
			// gapi.client.load('calendar', 'v3').then(function () {

			//   //displayInbox();
			//   makeApiCall();
			// });

			}
			else{
			handleSignInClick();
			}
		}

		function handleSignInClick(event) {
			// Ideally the button should only show up after gapi.client.init finishes, so that this
			// handler won't be called before OAuth is initialized.
			gapi.auth2.getAuthInstance().signIn();
		}

		function handleSignOutClick(event) {
			gapi.auth2.getAuthInstance().signOut();
		}

		function makeApiCall(nextPageToken) {
			var listEvent = [];
			//return new Promise(function(re){
				if (nextPageToken!='') {
				options.pageToken = nextPageToken;
				}

				//console.log(1);

				var request = gapi.client.calendar.events.list(options);
				request.then(function(response){
					var resp = response.result;
					nextPageToken = resp.nextPageToken;
					if(resp.items.length > 0) 
					{
					//console.log(resp.items);	
					for (var i = 0; i < resp.items.length; i++) 
						{
							//console.log(listGoogleId);
							eventObj = {};
							if(listGoogleId.length > 0){
								var checkexist = listGoogleId.replace(resp.items[i].id, "@@@@@@");
								var findGoogleId = checkexist.split("@@@@@@");
							}
							else{
								var checkexist = '';
								var findGoogleId = [];
							}
							
							eventObj['id'] = resp.items[i].id;
							eventObj['eventName'] = resp.items[i].summary;
							eventObj['AllDayFlag'] = 0;
							if(resp.items[i].start.dateTime){
								var startdate = resp.items[i].start.dateTime;
								startdate = startdate.substr(0,19);
								eventObj['startDate'] = startdate;
							}
							else if(resp.items[i].start.date){
								var startdate = resp.items[i].start.date;
								eventObj['startDate'] = startdate;	
							}

							if(resp.items[i].end.dateTime){
								var enddate = resp.items[i].end.dateTime;
								enddate = enddate.substr(0,19);
								eventObj['endDate'] =  enddate;
							}
							else{
								eventObj['endDate'] =  eventObj['startDate']+'T23:59:59';
							}

							eventObj['ConfirmationFlag'] = 0;
							eventObj['status'] = resp.items[i].status;
							//if(resp.items[i].start.dateTime){
							//console.log(resp.items[i].id +" Check"+ findGoogleId.length);
							if(findGoogleId.length < 2){
								listEvent.push(eventObj);
							}
							//}
						}
					}
					
					$("#nextPageToken").val(nextPageToken);

					if($("#nextPageToken").val()!=''){
						makeApiCall($("#nextPageToken").val());
					}
					//console.log('call API to save data'+JSON.stringify(listEvent));

					if(listEvent.length > 0){
						$.ajax({
							type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
							url: '/session/sire/models/cfc/calendar.cfc?method=AddEventSync&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true' ,   
							dataType: 'json',
							async: true,
							data:  
							{ 			
							inpEvent : JSON.stringify(listEvent)			
							},					  
							error: function(XMLHttpRequest, textStatus, errorThrown) { 
								$("#processingPayment").hide();
								<!--- No result returned ---> alertBox("Add Event Failed.","ERROR"); },					  
							success:		
							<!--- Default return function for call back --->
							function(d) 
							{		
								$("#processingPayment").hide();																																						
							<!--- RXRESULTCODE is 1 if everything is OK --->
							if (parseInt(d.RXRESULTCODE) == 1) 
							{			
								if(listEvent.length == 1){
									alertBox("Synchronized "+listEvent.length+" event successfully","MESSAGE");
								}else{
									alertBox("Synchronized "+listEvent.length+" events successfully","MESSAGE");
								}				
							}										
							} 		
							
						});

					}else{
						$("#processingPayment").hide();
						 alertBox("The data synchronized already.","MESSAGE");
					}
				});
			// console.log(nextPageToken);  
			//$("#data").text(JSON.stringify(listEvent));
			
		}

		$("#signin-button").on('click', function(){

			bootbox.dialog({
	         	message: "Are you sure you want to sync?",
	         	title: "Sync with google calendar",
	         	buttons: {
	             	success: {
	                 	label: "Agree",
	                 	className: "btn green-gd btn-re",
	                 	callback: function() {
	                 		$("#processingPayment").show();
							<!--- Get list Google calendar Id --->
							$.ajax({
								type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
								url: '/session/sire/models/cfc/calendar.cfc?method=GetCalendarEventInfoSync&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true' ,   
								dataType: 'json',
								async: true,
								data: {},					  
								error: function(XMLHttpRequest, textStatus, errorThrown) { 
									$("#processingPayment").hide();
									<!--- No result returned ---> 
									alertBox("No Response from the remote server. Check your connection and try again.","ERROR"); 
								},					  
								success:		
								<!--- Default return function for call back --->
								function(d) 
								{																																								
									<!--- RXRESULTCODE is 1 if everything is OK --->
									if (parseInt(d.RXRESULTCODE) == 1) 
									{								
										listGoogleId = d.GGIDLIST;
									}										
								} 		
								
							})
							.done(function(data) {
								handleClientLoad();
							});
	                 	}
	             	},
	             	cancel: {
		                label: "Cancel",
		                className: "green-cancel",
		                callback: function () {
		                	
		                }
			        }
	         	}
	     	});

			
		});
		
		<!--- inplace editing of description  see http://jsfiddle.net/egstudio/aFMWg/1/ --->
		var replaceWith = $('<input name="temp" type="text" style="color: #000; display: inline; width:100%; box-sizing: border-box; text-align:left; padding-left:2em;" />'),
		connectWith = $('input[name="DefaultEventTitle"]');
		
		$('#DefaultEventTitleDisplay').inlineEdit(replaceWith, connectWith);
		
		
		<!--- Bind checkbox for ASR - auto save changes--->
		$('#ASR').change(function(){		
			// if($("#ASR").is(':checked')){
			// 	$(".setInterval").removeClass('hidden');
			// }
			// else{
			// 	$(".setInterval").addClass('hidden');
			// 	saveInterval(0);
			// }

			SaveASR();
		});

		$('#IntervalResend').change(function(){		
			if($("#IntervalResend").is(':checked')){
				$(".setInterval").removeClass('hidden');
			}
			else{
				$(".setInterval").addClass('hidden');
				saveInterval(0);
			}

			//SaveASR();
		});

			
		if('<cfoutput>#TRIM(RetValGetCalendarConfiguration.SCROLLTIME)#</cfoutput>' != '')
		{
			var inpScrollMoment = moment('<cfoutput>#RetValGetCalendarConfiguration.SCROLLTIME#</cfoutput>', 'HH:mm:ss');
			$('#TimeStepScroll').val(inpScrollMoment.format("HH:mm A"));
		
		}
				
		$('#TimeStepScroll').timepicker({ 	
			'step': 15, 
			'scrollDefault': '9:00 AM',
			'timeFormat': 'h:i A'			   							
		});
					
		$('#TimeStepScroll').change(function(){	
			
			
			if($('#TimeStepScroll').val() != "")
			{	
				
				//console.log($('#TimeStepScroll').val());
				<!--- Convert to Moments for formating option--->	
				var s = moment($('#TimeStepScroll').val(), 'HH:mm A');    
			  	
			  	SaveScrollTime(s.format("HH:mm:ss"));
			}
					 
		});

		// Setting for start/end time		
		$('#StartTimeStepScroll').timepicker({ 	
			'step': 60, 
			'scrollDefault': '9:00 AM',
			'timeFormat': 'h:i A',
			'minTime': '9:00 AM',
			'maxTime': '9:00 PM'			   							
		});
		
		$('#EndTimeStepScroll').timepicker({ 	
			'step': 60, 
			'scrollDefault': '9:00 PM',
			'timeFormat': 'h:i A',
			'minTime': '9:00 AM',
			'maxTime': '9:00 PM'			   							
		});

		//#rxCP.CPSOOBj.StartHour_ti#
		if('<cfoutput>#TRIM(rxCP.CPSOOBj.StartHour_ti)#</cfoutput>' != '')
		{									
			var showTime = parseInt('<cfoutput>#rxCP.CPSOOBj.StartHour_ti#</cfoutput>');
			var showTimeType = "AM";
			if(parseInt('<cfoutput>#rxCP.CPSOOBj.StartHour_ti#</cfoutput>') > 12){
				showTime = parseInt('<cfoutput>#rxCP.CPSOOBj.StartHour_ti#</cfoutput>') - 12;
				showTimeType = "PM";
			}
			else if (parseInt('<cfoutput>#rxCP.CPSOOBj.StartHour_ti#</cfoutput>') == 0){
				showTime = 12;
			}
			else if (parseInt('<cfoutput>#rxCP.CPSOOBj.StartHour_ti#</cfoutput>') == 12){
				showTimeType = "PM";
				showTime = 12;
			}
			var inpScrollMoment = moment(showTime, 'HH:mm:ss');
			$('#StartTimeStepScroll').val(inpScrollMoment.format("HH:mm ") + showTimeType);		
		}
		else{
			$('#StartTimeStepScroll').val("09:00 AM");
		}
		//#rxCP.CPSOOBj.EndHour_ti#
		if('<cfoutput>#TRIM(rxCP.CPSOOBj.EndHour_ti)#</cfoutput>' != '')
		{			
			var showTime = parseInt('<cfoutput>#rxCP.CPSOOBj.EndHour_ti#</cfoutput>');
			var showTimeType = "AM";
			if(parseInt('<cfoutput>#rxCP.CPSOOBj.EndHour_ti#</cfoutput>') > 12){
				showTime = parseInt('<cfoutput>#rxCP.CPSOOBj.EndHour_ti#</cfoutput>') - 12;
				showTimeType = "PM";
			}
			else if (parseInt('<cfoutput>#rxCP.CPSOOBj.EndHour_ti#</cfoutput>') == 0){
				showTime = 12;				
			}
			else if (parseInt('<cfoutput>#rxCP.CPSOOBj.EndHour_ti#</cfoutput>') == 12){
				showTimeType = "PM";
				showTime = 12;
			}
			var inpScrollMoment = moment(showTime, 'HH:mm:ss');			
			$('#EndTimeStepScroll').val(inpScrollMoment.format("HH:mm ") + showTimeType);
		
		}
		else{
				
			$('#EndTimeStepScroll').val("09:00 PM");
		}

		$('#StartTimeStepScroll').change(function(){				

			if($('#StartTimeStepScroll').val() != "")
			{			
				var s = moment($('#StartTimeStepScroll').val(), 'HH:mm A');    
			  	var res = s.format("HH:mm:ss").split(":");
				var hourInput = parseInt(res[0]);

				// Check with start time
				var sEnd = moment($('#EndTimeStepScroll').val(), 'HH:mm A');    
			  	var resEnd = sEnd.format("HH:mm:ss").split(":");
				var endInput = parseInt(resEnd[0]);	
				
				// if(hourInput == 0)									{
				// 	hourInput =  12;
				// }
				// else if(hourInput == 12)									{
				// 	hourInput =  24;
				// }										
				if(hourInput < 9 || hourInput > 21){					
					alertBox('Start time must be between 09:00 AM and 09:00 PM. End Time will be reset by the Start time!','Invalid!');	
					reSetStartTime();	
					rSaveBatchScheduleTime(sEnd.format("HH:mm:ss"), 0);
					return;							
				}				
				if(parseInt(endInput) < hourInput){					
					alertBox('Start time must be less than End time. Start Time will be reset by the End time!','Invalid!'); 
					
					reSetStartTime();
					
					SaveBatchScheduleTime(sEnd.format("HH:mm:ss"), 0);
					return;
				}

			  	SaveBatchScheduleTime(s.format("HH:mm:ss"), 0);
			}
					 
		});
		$('#EndTimeStepScroll').change(function(e){				
			if($('#EndTimeStepScroll').val() != "")
			{												
				//validate for EndTime				
				var s = moment($('#EndTimeStepScroll').val(), 'HH:mm A');    
			  	var res = s.format("HH:mm:ss").split(":");
				var hourInput = parseInt(res[0]);	
				// Check with start time
				var sStart = moment($('#StartTimeStepScroll').val(), 'HH:mm A');    
			  	var resStart = sStart.format("HH:mm:ss").split(":");
				var startInput = parseInt(resStart[0]);	

				// if(hourInput == 0)									{
				// 	hourInput =  12;
				// }
				// else if(hourInput == 12)									{
				// 	hourInput =  24;
				// }

				if(hourInput < 9 || hourInput > 21){									
					alertBox('End time must be between 09:00 AM and 09:00 PM. End Time will be reset by the Start time!','Invalid!');			
					reSetEndTime();	
					SaveBatchScheduleTime(sStart.format("HH:mm:ss"), 1);	
					return;									
				}			
				if(parseInt(startInput) > hourInput){

					alertBox('End time must be greater than Start time. End Time will be reset by the Start time!','Invalid!'); 				
					reSetEndTime();									
					SaveBatchScheduleTime(sStart.format("HH:mm:ss"), 1);	
					return;				
				}
																				
			  	SaveBatchScheduleTime(s.format("HH:mm:ss"), 1);
			}
					 
		});
		function reSetEndTime(){				
			var startTime = moment($('#StartTimeStepScroll').val(), 'HH:mm A');   
			var res = startTime.format("HH:mm:ss").split(":");
			var showTime = parseInt(res[0]);	
						
			var showTimeType = "AM";
			if(parseInt(showTime) > 12){
				showTime = parseInt(showTime) - 12;
				showTimeType = "PM";
			}
			else if (parseInt(showTime) == 0){
				showTime = 12;
			}
			var inpScrollMoment = moment(showTime, 'HH:mm:ss');		
			$('#EndTimeStepScroll').val(inpScrollMoment.format('HH:mm ') + showTimeType);
		}
		function reSetStartTime(){				
			var endTime = moment($('#EndTimeStepScroll').val(), 'HH:mm A');   
			var res = endTime.format("HH:mm:ss").split(":");
			var showTime = parseInt(res[0]);

			var showTimeType = "AM";
			if(parseInt(showTime) > 12){
				showTime = parseInt(showTime) - 12;
				showTimeType = "PM";
			}
			else if (parseInt(showTime) == 0){
				showTime = 12;
			}	
			var inpScrollMoment = moment(showTime, 'HH:mm:ss');		
			$('#StartTimeStepScroll').val(inpScrollMoment.format('HH:mm ') + showTimeType);
		}


		// End setting for start/end time
		
	});	
	
	<!--- http://jsfiddle.net/egstudio/aFMWg/1/ --->	
	$.fn.inlineEdit = function(replaceWith, connectWith) {

	    $(this).hover(function() {
	        $(this).addClass('hover');
	    }, function() {
	        $(this).removeClass('hover');
	    });
	
	    $('#DefaultEventTitleDisplay, #edit-default-title').on("click touchstart", function(e){
	
	        var elem = $('#DefaultEventTitleDisplay');
	
	        elem.hide();
	        $('#edit-default-title').hide();
	        elem.after(replaceWith);
	        
	        if(elem.text() != 'blank')
	        	replaceWith.val(elem.text());
	        else
	        	replaceWith.val('');	
	        
	        replaceWith.focus();
	
	        replaceWith.blur(function() {
	
	            if ($(this).val() != "") 
	            {
	                connectWith.val($(this).val()).change();
	                elem.text($(this).val());
	            }
	            else
	            {
		            if($(this).val() == 'blank')
		            	$(this).val('');
		            	
		            connectWith.val($(this).val()).change();
	                elem.text('blank');		            
	            }
	
	            $(this).remove();
	            elem.show();
	            $('#edit-default-title').show();
	        });
	        
	        <!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();
			
	    });
	};	
	<!--- CalendarConfig --->

	$(document).on("change", "#IntervalType, #IntervalValue" , function(){
		CalendarConfigurationInterval = "INTERVAL " + $('select[name=VALUE]').val() + " " + $('select[name=ITYPE]').val();
		SaveInterval();
	});

	function SaveInterval()	
	{											
		<!--- Save interval time to send reminder --->				
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/calendar.cfc?method=SaveConfigInterval&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: true,
			data:  
			{ 
				inpCalendarId : '<cfoutput>#cid#</cfoutput>',
				inpInterval : CalendarConfigurationInterval
							
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
			success:		
			<!--- Default return function for call back --->
			function(d) 
			{																																		
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1) 
				{														
				}
				else
				{
					<!--- No result returned --->	
					if(d.ERRMESSAGE != "")	
						alertBox("Error while saving changes to Notification Reminder setting","ERROR");
				}												
			} 		
				
		});		
		
	}

	$(document).on("change", "#ResendIntervalType, #ResendIntervalValue" , function(){
		CalendarConfigurationIntervalResend = "INTERVAL " + $('select[name=RESENDVALUE]').val() + " " + $('select[name=RESENDTYPE]').val();
		SaveIntervalResend();
	});

	function SaveIntervalResend()	
	{											
		<!--- Save interval time to send reminder --->				
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/calendar.cfc?method=SaveConfigIntervalResend&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: true,
			data:  
			{ 
				inpCalendarId : '<cfoutput>#cid#</cfoutput>',
				inpInterval : CalendarConfigurationIntervalResend
							
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
			success:		
			<!--- Default return function for call back --->
			function(d) 
			{																																		
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1) 
				{														
				}
				else
				{
					<!--- No result returned --->	
					if(d.ERRMESSAGE != "")	
						alertBox("Error while saving changes to Interval Resend Reminder setting","ERROR");
				}												
			} 		
				
		});		
		
	}
	
	function SaveCalendarConfig()
	{				
		
		<!--- Read current form for current confgi settings --->
		var inpConfig = new Object();
				
		inpConfig.PhoneNumberFormat = RetValGetCalendarConfiguration.PHONEFORMAT;
		inpConfig.Confirmation_BatchId = RetValGetCalendarConfiguration.BATCHID;
		inpConfig.ScrollTime = RetValGetCalendarConfiguration.SCROLLTIME;
		inpConfig.AutoSendReminder = RetValGetCalendarConfiguration.ASR;
		inpConfig.ReminderIntervalOne = CalendarConfiguration.INTERVALONE;
		 													
		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->				
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/calendar.cfc?method=SaveConfig&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true' ,   
			dataType: 'json',
			async: true,
			data:  
			{ 			
				inpCalendarId : '<cfoutput>#cid#</cfoutput>',
				inpConfig : JSON.stringify(inpConfig)			
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> alertBox("No Response from the remote server. Check your connection and try again.","ERROR"); },					  
			success:		
			<!--- Default return function for call back --->
			function(d) 
			{																																								
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (parseInt(d.RXRESULTCODE) == 1) 
				{			
												
				}
				else
				{
					<!--- No result returned --->	
					if(d.ERRMESSAGE != "")	
						alertBox(d.ERRMESSAGE,"MESSAGE");
				}												
			} 		
				
		});				
													
	}	
	
	
	function SaveASR()	
	{											
		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->				
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/calendar.cfc?method=SaveConfigAutoRemind&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: true,
			data:  
			{ 
				inpCalendarId : '<cfoutput>#cid#</cfoutput>',
				inpAutoRemind : $('#ASR').is(':checked')
							
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
			success:		
			<!--- Default return function for call back --->
			function(d) 
			{																																		
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1) 
				{	
					
																		
				}
				else
				{
					<!--- No result returned --->	
					if(d.ERRMESSAGE != "")	
						alertBox("Error while saving changes to Auto Schedule Reminder setting","ERROR");
				}												
			} 		
				
		});		
		
	}
	
	function SaveScrollTime(inpScrollTime)	
	{											
		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->				
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/calendar.cfc?method=SaveConfigScrollTime&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: true,
			data:  
			{ 
				inpCalendarId : '<cfoutput>#cid#</cfoutput>',
				inpScrollTime : inpScrollTime
							
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> alertBox("No Response from the remote server. Check your connection and try again.","ERROR!"); },					  
			success:		
			<!--- Default return function for call back --->
			function(d) 
			{																																		
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1) 
				{	
					
																		
				}
				else
				{
					<!--- No result returned --->	
					if(d.ERRMESSAGE != "")	
						bootbox.alert("Error while saving changes to defualt scroll time setting");
				}												
			} 		
				
		});		
	}
	function SaveBatchScheduleTime(inpInputTime, type)	
	{													
		if(inpInputTime != ""){			
			var res = inpInputTime.split(":");
			var hourInput = parseInt(res[0]);		
			<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->				
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/calendar.cfc?method=SaveConfigBatchScheduleTime&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				async: true,
				data:  
				{ 
					inpCalendarId : '<cfoutput>#RetValGetCalendarConfiguration.BATCHID#</cfoutput>',
					inpBatchScheduleTime : hourInput,
					inpType: type
								
				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> alertBox("No Response from the remote server. Check your connection and try again.","ERROR!"); },					  
				success:		
				<!--- Default return function for call back --->
				function(d) 
				{																																	
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1) 
					{																								
					}
					else
					{
						<!--- No result returned --->	
						if(d.ERRMESSAGE != "")	
							bootbox.alert("Error while saving changes to defualt scroll time setting");
					}												
				} 						
			});		
		}	
	}
</script>	