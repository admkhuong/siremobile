<cfscript>
    createObject("component", "public.sire.models.helpers.layout")
        .addJs("/public/js/jquery.tmpl.min.js", true)
        .addJs("/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js")
        .addCss("/public/js/jquery-ui-1.11.4.custom/jquery-ui.css", true)
        
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", false)

        .addCss("/session/sire/assets/global/plugins/uniform/css/uniform.default.css", true)
        .addJs('/session/sire/assets/global/scripts/filter-bar-generator.js', true)
        .addJs('/session/sire/assets/global/plugins/uniform/jquery.uniform.min.js', true)
        .addJs("/session/sire/assets/pages/scripts/calendar_event_report.js");        
</cfscript>

<cfparam name="isDialog" default="0">
<cfparam name="isUpdateSuccess" default="">
<style>
    .event-report{
        margin-top: 10px;
        font-weight: 900;
    }
</style>

<div class="portlet light bordered">
    <!--- Filter bar --->
    <div class="row">
        <div class="col-sm-3 col-md-4 event-report">
            <select id="select-event-report" class="form-control">
				<option value="EventsUpcomingList">Upcoming event List</option>
				<option value="EventsReminderList">Future reminder List</option>
				<option value="EventsAcceptedList">Accepted event List</option>
                <option value="EventsDeclinedList">Declined event List</option>
                <option value="EventsOpenChatList">Open Chat event List</option>
<!---                 <option value="EventsNoResponeList">No Respone Events List</option> --->
			</select>
        </div>
        <div class="col-sm-9 col-md-8">
            <div id="box-filter" class="form-gd">
            </div>
        </div>
    </div>
    <!--- Listing --->
    <div class="row">
        <div class="col-md-12">
            <div class="re-table">
                <div class="table-responsive">
                    <table id="tblListEMS" class="table-striped dataTables_wrapper">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<cfset RetCustomerInfo = ''>
<cfparam name="maskedNumber" default="">
<cfparam name="UserID" default="">
<cfparam name="expirationDate" default="">
<cfparam name="firstName" default="">
<cfparam name="lastName" default="">
<cfparam name="line1" default="">
<cfparam name="city" default="">
<cfparam name="state" default="">
<cfparam name="zip" default="">
<cfparam name="country" default="">
<cfparam name="phone" default="">
<cfparam name="emailAddress" default="">
<cfparam name="hidden_card_form" default="">

<!--- Change CC info before charge --->

<!--- End change CC info before charge --->
<cfparam name="variables._title" default="Calendar Event Report">
<cfinclude template="../views/layouts/master.cfm">

