<cfparam name="cid" default="1" />
<cfparam name="inpcontactstring" default="888"/>
<cfparam name="inpbatchId" default="1"/>
<cfparam name="inpEnterToSend" default="0"/>
<cfparam name="inpShortCode" default=""/>

<cfinclude template="/session/cfc/csc/constants.cfm">

<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>
<!---  SHORTCODEID, SHORTCODE--->
<cfset inpShortCode = shortCode.SHORTCODE>
<!---
<cfinvoke component="session.sire.models.cfc.optin-out" method="checkContactStringOptout" returnvariable="RetValCheckContactStringOptout">
	<cfinvokeargument name="inpContactString" value="8888888800">
	<cfinvokeargument name="inpShortCode" value="QA2Nowhere">
</cfinvoke> 
<cfdump var = "#RetValCheckContactStringOptout#"/>
--->
<!--- auto create a camapign for user first uses --->
<cfinvoke component="session.sire.models.cfc.calendar" method="SetCalendarConfigurationDefault" returnvariable="RetValSetCalendarConfigurationDefault"></cfinvoke> 
<cfif RetValSetCalendarConfigurationDefault.RXRESULTCODE GT 0>
	<cfset cid = RetValSetCalendarConfigurationDefault.CALENDARID>
	<cfset inpbatchId = RetValSetCalendarConfigurationDefault.BATCHID>
</cfif>

<cfinvoke component="session.sire.models.cfc.calendar" method="GetCalendarConfiguration" returnvariable="RetValGetCalendarConfiguration">
	<cfinvokeargument name="inpCalendarId" value="#cid#">
</cfinvoke> 

<cfinvoke method="GetChatSetting" component="session.sire.models.cfc.smschat" returnvariable="RetVarSetting">
</cfinvoke>

<cfif RetVarSetting.RXRESULTCODE GT 0>
	<cfif RetVarSetting.EnterToSend EQ 1>
		<cfset inpEnterToSend = 1>
	</cfif>
</cfif>

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("/session/sire/css/calendar.css")
</cfscript>

			
<!--- 
	Notes:
		Start with only one calendar per account - makes it simple
	
	CREATE SCHEMA `calendar` DEFAULT CHARACTER SET latin1 ;
	
	
	
	https://fullcalendar.io/docs/event_data/events_json_feed/
	
	Requires user to be logged in to Sire edit - secured by Session.UserId
	
	

	
	ConfirmationFlag States
	
	0 - No reminder sent (Hide)
	1 - Scheduled
	2 - Accepted
	3 - Declined
	4 - Change Request
	5 - Reminder Error - Bad SMS, eMail or other error (Hide)
	6 - Off
	7 - Sent
	8 - Chat Active
	9 - Opt Out
	10 - Chat Close

			
	
	Tasks:
	
	Look at Apple calendar app on iphone to see how they do UI small
	
	
	More Bulletproof - verify no schedule already set in case event "loses" (ajax timeout / error /etc) the events DTSId
	
	Need to work on verify MAX sessions - see logic where IRESESSIONSTATE_TERMINATED_TOO_MANY_SESSIONS_IN_PROGRESS in inc_smsdelivery
	
	Update status when/if reminder sent - 
	Update UI result
	
	Need to be sure to set a default schedule for reminders - when batch is picked / defined
	Make sure end date is next century
	
	Look for Distirbution Id in results to see if campaing was scheduled OK
	Also look for DNC or other errors
	
	If first message confirms - adjust next messge - Delete? BR just say time?
	
		
	UI FEEDBACK WHILE LOADING APPT AND SETTING UP REMINDER
	
	
	Atuo switch to popup on small screen vs popover
	
	Setup default reminder campaign
	Log queued id in event - use this to update or change dates
	Setup time to remind - before event
	Option to remind if event is scheduled past reminder window
	New duplicate logic for reminders - no more than one per day? what about multiple pre-scheduled appt reminders? Both greater than and less than a certain limit? Pre-check before attempt on queue? Better to check first?
	
	
	Mulitple Remiders Option - use advanced flows if needed
	Flow Option	
	BR - Check if confirmed or denied yet - if not confirm again later?
	
	
	Verify batch owner on scheduling of reminder - make sure no hacks
	
		
	Option to auto remind
	Option to confirm each reminder
	
	New green fro reminders that cant go out
	
	NOC monitor for automated processes
	look for abuse
	look for not running
	look for too slow
	look for counts in use
	
	Multithreaded cron for processing quickly - every hour 6AM PST til 11PM PST - covers Hawaii?
	Reminder Schedule is 9-6 local - not directly user adjustable?
	
	
		Setting panel -
		Choose time ranges and increments
		Choose colors?
		Choose Campaing to verify
		OR
		Choose Campaign to Notify
		OR
		Just choose reminder campaign	
		Option to create remider campaign from hidden template
		
		New CDF - Event Reminder ID - Store in JSON data and use in API call to update calendar event
		
		
		More detailed event editor
		Notes
		Name
		Use data from event in CDF JSON to message
		
		
		
	
		Asyncornous load?
		Only load current page?
	
		Sync to external calander option
		cfc
		   Add Event
		   Read Event
		   Update Event
		   Reminder Campaing - coming, reschedule, yes / no NLP - pop-response in calendar automatically
		   Expose as Sire secure API
		   Expose to public?
		
		Check warn DNC on add appointment
			Tool for user to opt back in once they opt out
		   	
		Name
		Number
		First 
		Last
				
		
		*When an event is deleted - any scheduled reminders in queue need to be removed
		
		Complicance on appotinment reminders   
	
	
		On add appointment - check rules
		
		
		email through a service 
		opt out rules etc
		
		
		
		Allow chat with appointment session
			Cant make it? When is a good time?
		
	
		Reporting and Stats
		Canceled Counts?
		Changed Counts?
		Daily overview?
		
	
		Cron:
		
		Search the events DB by date range and schedule that days calander items - starts at midnight
		
		
	
	
 --->
 


<div class="portlet light bordered">
	<div class="row">
		<div class ="reminder-calendar-title col-xs-12">
			<a class="title_calendar" title="Refresh Calendar">SIRE CALENDAR <a/><a title="Hide/show filter" data-toggle="toggle" data-target="#Calendar_collapse"><i class="fa fa-bars icon_calendar" aria-hidden="true"></i></a>
		</div>
	</div>
	<div class="row">
		<div id='Calendar_collapse' class="col-xs-2"> <!---collapse in--->
			<!--- Calendar left --->
			<div id='calendar_left'>
				<label class="container1"><p class="status-name"> Scheduled </p> <!--- 1 --->
					<input type="checkbox" value=<cfoutput>#APPT_REMINDER_QUEUED#</cfoutput> <cfif listFind(RetValGetCalendarConfiguration.FILTERCALENDAR, '#APPT_REMINDER_QUEUED#') GT 0> checked="checked"<cfelse></cfif>>
					<span class="checkmark remind-inqueue"></span>
				</label>
				<label class="container1"><p class="status-name"> Off </p> <!--- 6 --->
					<input type="checkbox" value=<cfoutput>#APPT_REMINDER_DO_NOT_SEND#</cfoutput> <cfif listFind(RetValGetCalendarConfiguration.FILTERCALENDAR, '#APPT_REMINDER_DO_NOT_SEND#') GT 0> checked="checked"<cfelse></cfif>>
					<span class="checkmark remind-dont-send"></span>
				</label>
				<label class="container1"><p class="status-name"> Sent </p> <!--- 7 --->
					<input type="checkbox" value=<cfoutput>#APPT_REMINDER_SENT#</cfoutput> <cfif listFind(RetValGetCalendarConfiguration.FILTERCALENDAR, '#APPT_REMINDER_SENT#') GT 0> checked="checked"<cfelse></cfif>>
					<span class="checkmark remind-sent"></span>
				</label>
				<label class="container1"><p class="status-name"> Chat Active</p> <!--- 8 --->
					<input type="checkbox" value=<cfoutput>#APPT_REMINDER_CHAT_ACTIVE#</cfoutput> <cfif listFind(RetValGetCalendarConfiguration.FILTERCALENDAR, '#APPT_REMINDER_CHAT_ACTIVE#') GT 0> checked="checked"<cfelse></cfif>>
					<span class="checkmark remind-chat-active"></span>
				</label>
				<label class="container1"><p class="status-name"> Opt Out </p> <!--- 9 --->
					<input type="checkbox" value=<cfoutput>#APPT_REMINDER_OPT_OUT#</cfoutput> <cfif listFind(RetValGetCalendarConfiguration.FILTERCALENDAR, '#APPT_REMINDER_OPT_OUT#') GT 0> checked="checked"<cfelse></cfif>>
					<span class="checkmark remind-opt-out"></span>
				</label>
				<label class="container1"><p class="status-name"> Accepted </p> <!--- 2 --->
					<input type="checkbox" value=<cfoutput>#APPT_REMINDER_ACCEPTED#</cfoutput> <cfif listFind(RetValGetCalendarConfiguration.FILTERCALENDAR, '#APPT_REMINDER_ACCEPTED#') GT 0> checked="checked"<cfelse></cfif>>
					<span class="checkmark remind-accepted"></span>
				</label>
				<label class="container1"><p class="status-name"> Change Request </p> <!--- 4 --->
					<input type="checkbox" value=<cfoutput>#APPT_REMINDER_CHANGE_REQUEST#</cfoutput> <cfif listFind(RetValGetCalendarConfiguration.FILTERCALENDAR, '#APPT_REMINDER_CHANGE_REQUEST#') GT 0> checked="checked"<cfelse></cfif>>
					<span class="checkmark remind-chang-request"></span>
				</label>
				<label class="container1"><p class="status-name"> Declined</p> <!--- 3 --->
					<input type="checkbox" value=<cfoutput>#APPT_REMINDER_DECLINED#</cfoutput> <cfif listFind(RetValGetCalendarConfiguration.FILTERCALENDAR, '#APPT_REMINDER_DECLINED#') GT 0> checked="checked"<cfelse></cfif>>
					<span class="checkmark remind-decline"></span>
				</label>
				<label class="container1"><p class="status-name"> Chat Closed</p> <!--- 10 --->
					<input type="checkbox" value=<cfoutput>#APPT_REMINDER_CHAT_CLOSE#</cfoutput> <cfif listFind(RetValGetCalendarConfiguration.FILTERCALENDAR, '#APPT_REMINDER_CHAT_CLOSE#') GT 0> checked="checked"<cfelse></cfif>>
					<span class="checkmark remind-chat-close"></span>
				</label>
			</div>
			
		</div>
		
       <div id ="box_calendar" class="col-xs-10">
	   		<div id='calendar'></div>
       </div>
   </div>
</div>
<!--- Use the content froma popup in the popovers - default to popover on smaller screens --->
<!--- Set a js variable to track screen size on load and redraw --->

<div class="bootbox modal fade" id="Calendar-Event-Modal" tabindex="-1" role="dialog" aria-labelledby="Calendar-Event-Modal" aria-hidden="true" style="">
    
    <div class="modal-dialog" role="document" style="">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Calendar Event</h4>
            </div>
<!---
<div id="popover-markup" class="popover-markup"> 
    <div class="head hide">Calendar Event</div>
    <div class="content hide">

	<div class="event-bubble-close close" tabindex="0" style="z-index: 1000;" role="button" aria-label="close">X</div>

--->
	    	<!--- Popover section without header --->
		    <div class="popover-section modal-body popover-content">	
			        
			    <div class="event-bubble">
			    
				    <h5><span class="event-link event-link-active"><!---Event</span> | <span class="reminder-link">Reminder</span> | <span class="chat-link">Chat</span> | <span class="respone"><a>Respone History</a></span>---></h5>
				    <div class="event-group">
				        <div class="form-group">
							<label>Event Name</label>	        
				            <input 
				                type="text" 
				                class="form-control inpTitle" 
				                placeholder="Notes…">
				        </div>
				        <div class="form-group">
					        <label><i class="glyphicon glyphicon-phone"></i> Mobile Phone Number (SMS Capable)</label>        
				            <input 
				                type="text" 
				                class="form-control inpSMSNumber" 
				                placeholder="Mobile Phone Number">
				        </div> 
						 <div class="form-group">
						 	<label><span class="glyphicon glyphicon-calendar"></span> Event Date</label>
							<div class="input-group datetimepicker date set-z-index" id="datetimestart">
								<input type='text' class="form-control datetimestart"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>
						 <div class="form-group hide">
						 	<label><span class="glyphicon glyphicon-calendar"></span> End Date</label>
							<div class="input-group datetimepicker date set-z-index" id="datetimeend">
								<input type='text' class="form-control datetimeend"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>
							
				        <div class="form-group">
					        <table cellpadding="5" cellspacing="5" border="0" style="">
					        	<tr>
						        	<td><label><span class="glyphicon glyphicon-time"></span> Start Time</label></td>
						        	<td><label><span class="glyphicon glyphicon-time"></span> End Time</label></td>
					        	</tr>	
								<tr>
							        <td><input type="text" class="time ui-timepicker-input TimeStepStart form-control" autocomplete="off" style="width: 80%; display: inline;"></td>					
									<td><input type="text" class="time ui-timepicker-input TimeStepEnd form-control" autocomplete="off" style="width: 80%; display: inline;"></td>
								</tr>				
					        </table>
				        </div>
						<div class="form-group event-status">
					        
					       <label>Status:</label>
				            <select id="inpConfirmationFlag" class="Select2">	
								<option value=<cfoutput>#APPT_REMINDER_QUEUED#</cfoutput> >Scheduled</option> 
								<option value=<cfoutput>#APPT_REMINDER_ACCEPTED#</cfoutput> >Accepted</option>
								<option value=<cfoutput>#APPT_REMINDER_DECLINED#</cfoutput> >Declined</option>
								<option value=<cfoutput>#APPT_REMINDER_CHANGE_REQUEST#</cfoutput> >Change Request</option>
 								<option value=<cfoutput>#APPT_REMINDER_DO_NOT_SEND#</cfoutput> >Off</option>
 								<option value=<cfoutput>#APPT_REMINDER_SENT#</cfoutput> >Sent</option>  
								<option value=<cfoutput>#APPT_REMINDER_CHAT_ACTIVE#</cfoutput> >Chat Active</option>
								<option value=<cfoutput>#APPT_REMINDER_OPT_OUT#</cfoutput> >Opt Out</option>
								<option value=<cfoutput>#APPT_REMINDER_CHAT_CLOSE#</cfoutput> >Chat Close</option>                      
							</select>	   
						</div>
						<div class="form-group tab-chat-calendar">
							 <table style="margin-top: 10px; margin-bottom: 0px;">
					        	<tr>
						        	<td><button type="button" class="button-chat-calendar"><i class="fa fa-comment-o" style ="color:white;" aria-hidden="true"></i> <b>Chat</b></button></td>
					        	</tr>				
					        </table>
				        </div>
				    </div>      
			    </div>
			    
			    <div class="modal-footer">
			        
			        <button type="button" class="fc-button fc-state-default fc-sire-event-button xmlp-btn btn-delete-event" data-dismiss="modal" style="float: left;">Delete</button>
					<button type="button" class="fc-button fc-state-default fc-sire-event-button xmlp-btn btn-resend-event" data-dismiss="modal" style="float: left; margin-left: 8px; display:none;">Resend</button>
			        <button type="button" class="fc-button fc-state-default fc-sire-event-button xmlp-btn btn-save-event" data-dismiss="modal">Save</button>
			        <button type="button" class="fc-button fc-state-default fc-sire-event-button xmlp-btn btn-cancel-event" data-dismiss="modal">Cancel</button>
			        
			    </div>
	        
		    </div>
        </div>
    </div>
   
</div>

<!--- Modal chat --->
<div class="modal fade" id="chat-modal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
      	<button type="button" class="closeChat" data-dismiss="modal">&times;</button>
        <div class="modal-header">                	        	 		
			<div class="col-md-12">				
				<div class="row" id="event_phonenumber">
					<div class="col-md-3">
						<label class="control-label" id ="event_phonenumber_title"><b>Contact:</b></label>
					</div>
					<div class="col-md-9">
						<label class="control-label" id ="event_phonenumber_value"></label>
					</div>
				</div>
				<div class="row" id="event_first_name" style="display:none">
					<div class="col-md-3">
						<label class="control-label" id="event_first_name_title"><b>First name:</b></label>
					</div>
					<div class="col-md-9">
						<label class="control-label" id="event_first_name_value"></label>
					</div>
				</div>
				<div class="row" id="event_last_name" style="display:none">
					<div class="col-md-3">
						<label class="control-label" id="event_last_name_title"><b>Last name:</b></label>
					</div>
					<div class="col-md-9">
						<label class="control-label" id="event_last_name_value"></label>
					</div>
				</div>
				<div class="row" id="event_name" style="display:none">
					<div class="col-md-3">
						<label class="control-label" id="event_name_title"><b>Event name:</b></label>
					</div>
					<div class="col-md-9">
						<label class="control-label" id="event_name_value"></label>
					</div>
				</div>
				<div class="row" id="event_start" style="display:none">
					<div class="col-md-3">
						<label class="control-label" id="event_start_title"><b>Start date:</b></label>
					</div>
					<div class="col-md-9">
						<label class="control-label" id="event_start_value"></label>
					</div>
				</div>
				<div class="row" id="event_end" style="display:none">
					<div class="col-md-3">
						<label class="control-label" id="event_end_title"><b>End date:</b></label>
					</div>
					<div class="col-md-9">
						<label class="control-label" id="event_end_value"></label>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label><b>Notes</b></label>
						<textarea type="text" class="form-control event_chat_notes" onmouseout ="UpdateNoteschat()"  placeholder="Notes…"></textarea>
					</div>
				</div>
				<div class="row form-group"> 
					<div class="col-md-12">
						<label>Status:</label>
						<select id="inpConfirmationFlag" class="Select2 form-control">	
							<option value=<cfoutput>#APPT_REMINDER_QUEUED#</cfoutput> >Scheduled</option> 
							<option value=<cfoutput>#APPT_REMINDER_ACCEPTED#</cfoutput> >Accepted</option>
							<option value=<cfoutput>#APPT_REMINDER_DECLINED#</cfoutput> >Declined</option>
							<option value=<cfoutput>#APPT_REMINDER_CHANGE_REQUEST#</cfoutput> >Change Request</option>
							<option value=<cfoutput>#APPT_REMINDER_DO_NOT_SEND#</cfoutput> >Off</option>
							<option value=<cfoutput>#APPT_REMINDER_SENT#</cfoutput> >Sent</option>  
							<option value=<cfoutput>#APPT_REMINDER_CHAT_ACTIVE#</cfoutput> >Chat Active</option>
							<option value=<cfoutput>#APPT_REMINDER_OPT_OUT#</cfoutput> >Opt Out</option>
							<option value=<cfoutput>#APPT_REMINDER_CHAT_CLOSE#</cfoutput> >Chat Close</option>                 
						</select>
					</div>	   
				</div>
			</div>
        </div>
        <div class="modal-body">
          	<div class="inner-phone-screen clearfix mCustomScrollbar _mCS_1 mCS_no_scrollbar" style ="overflow-y:auto;height:400px;" id="SMSHistoryScreenArea">

			</div>
			<div class="part-type-mess">
				<div class="uk-grid uk-grid-small">
					<div class="uk-width-expand">
						<form id="text-form">
							<div class="form-group form-gd">
								<!--- <input id="SMSTextInputArea" type="text" class="form-control validate[required]" placeholder="Type your message here"/> --->
								<textarea style="height: 34px; resize: none; width:99%" name="" id="SMSTextInputArea" class="form-control validate[required]" placeholder="Type your message here" ></textarea><!---disabled--->
							</div>
						</form>
					</div>
					<!---
					<div class="uk-width-auto">
						<div style="text-align: left; padding: 0px; margin: 0px; width: 50%; valign: left;"><button type="button" class="btn green-gd btn-endchat">End chat</button></div>
						<div style="text-align: right; padding: 0px; margin: 0px;width: 50%; valign: right;"><button id="SMSSend" type="submit" class="btn green-gd">Send</button></div>
					</div>
					--->
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6" style="text-align: left;">
					<button type="button" class="btn new-blue-gd btn-endchat">End chat</button>
				</div>
				<div class="col-xs-6" style="text-align: right; padding-right:25px;">
					<button id="SMSSend" type="submit" class="btn green-gd">Send</button>
				</div>
			</div>
        </div>
        <div class="modal-footer">
			<div class="col-xs-12">
				<span class="custom-checkbox">
					<input type="checkbox" id="enter-to-send" value="1" <cfif inpEnterToSend EQ 1>Checked</cfif>>
					<label for="enter-to-send" style="margin-right: 25px">
						Enter to send
					</label>
				</span>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
        </div>
      </div>
      
    </div>
</div>
<!--- Modal Customer respone --->
<!--- MODAL POPUP PREVIEW--->
    <div class="bootbox modal fade" id="previewCampaignModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Respone History</h4>
                </div>
                <div class="modal-body">
					
                </div>
                <div class="modal-footer">
                    <!--- <button type="button" class="btn btn-primary btn-success-custom" id="btn-rename-campaign">Save</button> --->
                    <button type="button" class="btn btn-default btn-back-custom" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


<cfparam name="variables._title" default="Sire Calendar">
<cfinclude template="../views/layouts/master.cfm">


<!--- https://fullcalendar.io --->
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js'></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.css"/> 
<!--- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.print.css"/>  --->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script> 


<!--- http://jonthornton.github.io/jquery-timepicker/ --->
<!--- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.css" />	
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script> --->
<link rel="stylesheet" href="../assets/global/css/timepicker/jquery.timepicker.css" />	
<script src="../assets/global/plugins/timepicker/jquery.timepicker.js"></script>

<!--- http://jonthornton.github.io/jquery-datepicker/ --->
<!--- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script> --->
<script src="../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min-4.7.14.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" />
<link rel="stylesheet" href="/public/sire/css/select2-bootstrap.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.3/socket.io.js"></script>

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addJs("https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js")
        .addCss("https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css")
        .addCss("../assets/global/plugins/jquery-mcustomscrollbar/jquery.mCustomScrollbar.css", true)
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/jquery-mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)
        .addCss("../assets/layouts/layout4/css/custom3.css", true)
		.addCss("../assets/layouts/layout4/css/custom.css", true)
		.addJs("../assets/pages/scripts/sms_ws.js");

		// .addJs("../assets/global/css/timepicker/jquery.timepicker.css", true)
		// .addJs("../assets/global/plugins/timepicker/jquery.timepicker.js", true);
</cfscript>

<cfinclude template="/public/sire/configs/paths.cfm"/>
<!--- 
	
	
	$('#yourCalendar').fullCalendar('unselect');  
	
	unselect: function(){
    $('#submitButton').unbind();
},


	--->



<!--- Include javascript here for better cf documentation --->
<cfset us_chn = left(hash("#EnvPrefix#"&"SIRE_CHAT_USER_"&"#session.UserId#"), 6)/>

<script type="application/javascript">

	var isMobile = {
	    Android: function() {
	        return navigator.userAgent.match(/Android/i);
	    },
	    BlackBerry: function() {
	        return navigator.userAgent.match(/BlackBerry/i);
	    },
	    iOS: function() {
	        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	    },
	    Opera: function() {
	        return navigator.userAgent.match(/Opera Mini/i);
	    },
	    Windows: function() {
	        return navigator.userAgent.match(/IEMobile/i);
	    },
	    any: function() {
	        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
	    }
	};

	var selectAllDate;
	var selectHour;
	var data_event_obj = '';
	var data_date = '';
	var data_event_action = '';
	var data_title = '';
	var data_chatsessionid = '';
	var UserShortCodeId = '';
	var us_chn = "#us_chn#";
	var currChannel = '';
	var enter_to_send = <cfoutput>#RetVarSetting.EnterToSend#</cfoutput>;
	var checkPhonenumber ='';	
	var SkipCheckOptOut = 0;
	var SkipCheckSchedule = 0;
	// var isChooseAgendaDay = false;
	<!--- On page loaded --->
	
	$(function() {

		$( document ).ready(function() {
		    if(<cfoutput>#RetVarSetting.EnterToSend#</cfoutput> != 1){
				$("#enter-to-send").prop("checked",false);	
			}
			else{
				$("#enter-to-send").prop("checked",true);
			}

			// Manually set global variables when choose agenda day/week/month
			// $(".fc-agendaDay-button").click(function() {				
			//   	isChooseAgendaDay = true;
			// });		
			// $(".fc-agendaWeek-button").click(function() {				
			//   	isChooseAgendaDay = false;
			// });		
			// $(".fc-agendaWeek-button").click(function() {				
			//   	isChooseAgendaDay = false;
			// });		
		});
		// Event when choose agenda day button
		

		var reloadEvent = setInterval(function(){
		 	$('#calendar').fullCalendar('refetchEvents');	
		}, 15000);


		function getCookie(cname) {
			var name = cname + "=";
			var decodedCookie = decodeURIComponent(document.cookie);
			var ca = decodedCookie.split(';');
			for(var i = 0; i <ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0) == ' ') {
					c = c.substring(1);
				}
				if (c.indexOf(name) == 0) {
					return c.substring(name.length, c.length);
				}
			}
			return "";
		}

		// set cookie filter calendar
		var val_filter_calendar = getCookie('FL_CALENDAR');
		if(val_filter_calendar == ''){
			$('input:checkbox').removeAttr('checked');
			$(':checkbox').each(function() {
				this.checked = true;                        
			});
		}

		$(document).on("click", ".reminder-calendar-title" , function(){
			$('#calendar').fullCalendar('refetchEvents');
		});


		$("[data-toggle='toggle']").click(function() {
			var selector = $(this).data("target");
			$(selector).toggleClass('in');
			
		});

		$(document).on("click", ".icon_calendar" , function(){
				
			var collapse_in = $('#Calendar_collapse').attr("class");
			//console.log(collapse_in);
			if(collapse_in == "col-xs-2 in"){
				$('#box_calendar').removeClass("col-xs-10");
				$('#box_calendar').addClass("col-xs-12");
				$('#Calendar_collapse').hide();
				
			}else{
				$('#box_calendar').removeClass("col-xs-12");
				$('#box_calendar').addClass("col-xs-10");
				$('#Calendar_collapse').show();
			}
		});

		 $('#calendar').fullCalendar({

			views: {
		        basic: {
		            // options apply to basicWeek and basicDay views
		        },
		        agenda: {
		            // options apply to agendaWeek and agendaDay views
		            
		            slotDuration: '00:15:00'
		        },
		        week: {			        
			        // options apply to basicWeek and agendaWeek views
		            slotDuration: '00:15:00'
		        },
		        day: {
		            // options apply to basicDay and agendaDay views
		            slotDuration: '00:15:00'
		        } 
			},
			allDaySlot: false,
			// Click to show calendar config
			customButtons: {
		        myCustomButton: {
		            text: '',
		            click: function() {
		                var url = "calendar-config?cid=<cfoutput>#cid#</cfoutput>";
						window.location = url;
		            }
		        },
				myReportButton: {
					text: '',
					click: function(){
						var url = "calendar-event-report";
						window.open(url,'_blank');
					},
					icon: ''
				}

		    },
			defaultView: 'agendaWeek',
			defaultTimedEventDuration: '00:30:00',
			<!--- The name of the parmeters you wish to pass to your event source function --->
			startParam : 'inpStart',
			endParam : 'inpEnd',
			eventSources: [
				
		        // your event source
		        {
		            url: '/session/sire/models/cfc/calendar.cfc?method=ReadEvents&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		            type: 'POST',
		            data: {
		                	inpCalendarId : '<cfoutput>#cid#</cfoutput>'
		                  },
		            error: function() {
						//clearInterval(reloadEvent);
		                //alertBox('There was an error while fetching events!','ERROR');
						
		            }
		         //   ,
		         //   color: '#83dd8f',   
		         //   borderColor: '#00aa00', 
		         //   textColor: '#fff' 
		        }
		
		        // any other sources...
		
		    ],    
			select: function(start, end, jsEvent){
	          console.log('select');											        
	        }, 			 
			<!--- Click on a day to add an event to that day ---> 
			dayClick: function(date, jsEvent, view) {
				console.log('dayClick');
				
				$('.popover').popover('hide');
										
				$('#calendar').fullCalendar('select', date);

				$('.datetimepicker').datetimepicker({
					format: 'YYYY-MM-DD'
				});

				data_date = $(this).attr('data-date');
			    data_event_action = 'add';
			    data_event_obj = null;
			    data_title = '';

				if(isMobile.any())
				{
					selectAllDate = date;
					$('#Calendar-Event-Modal').modal('show');
					$('#Calendar-Event-Modal').find('.button-chat-calendar').hide();
					$('#Calendar-Event-Modal').find('.tab-chat-calendar').hide();
					$('#Calendar-Event-Modal').find('.event-status').hide();

					$('#Calendar-Event-Modal').find('.inpSMSNumber').removeAttr("disabled");
					$('#Calendar-Event-Modal').find('.inpTitle').removeAttr("disabled");
					$('#Calendar-Event-Modal').find('.datetimestart').removeAttr("disabled");
					$('#Calendar-Event-Modal').find('.TimeStepEnd').removeAttr("disabled");
					$('#Calendar-Event-Modal').find('.TimeStepStart').removeAttr("disabled");
					$('#Calendar-Event-Modal').find('.btn-save-event').removeAttr("disabled");

				}
				else{
					<!--- Hide any open popovers --->
					$(".fc-time-grid, .fc-time-grid-container").css('overflow-y','auto');

					$('[data-toggle="popover"],[data-original-title]').each(function () {
			        		                  
			            (($(this).popover('hide').data('bs.popover')||{}).inState||{}).click = false  // fix for BS 3.3.6
			       	});
			        		
					(($('.fc-day, .fc-event').popover('hide').data('bs.popover')||{}).inState||{}).click = false  // fix for BS 3.3.6									
												 									
					$(this).popover({
						
						top: jsEvent.pageY - 16,
					    html: true,
					    container: 'body',
					    title: function () {
					        return  '' <!--- $('#Calendar-Event-Modal').find('.modal-header').html(); --->
					    },
					    content: function () {
						   						    
						    <!--- Set some attributes to be passed through as part of new content - this gets the date associated with the day that is clicked and stores it as an attribute of the popup content --->
						    <!--- Add a close button to top of pop-over --->
						    var NewObj = $('<div class="event-bubble-close close" tabindex="0" style="z-index: 1000;" role="button" aria-label="close">X</div>' + $('#Calendar-Event-Modal').find('.popover-section').html());			    
						  

						    NewObj.attr('data-date', $(this).attr('data-date'));
						    NewObj.data('data-event-obj', null);	
						    NewObj.attr('data-event-action', 'add');					    
						    NewObj.find('.inpTitle').val('');		
						   					   					
							var s = null;
							var e = null;
							var ds = null;
							var de = null;
							
							
							// if(event.start != undefined){
							// 	s = event.start.format('HH:mm A');
							// 	ds = event.start.format('YYYY-MM-DD');
							// }   
								
							// if(event.end != undefined){
							// 	e = event.end.format('HH:mm A');
							// 	de = event.end.format('YYYY-MM-DD');
							// } 
												
							
						    NewObj.find('.TimeStepStart').val(s);
						    NewObj.find('.TimeStepEnd').val(e);	
							NewObj.find('.datetimestart').val(ds);	
							NewObj.find('.datetimeend').val(de);	
						    
						    <!--- Set up all the #main-stage-content select2 boxes --->
							
							NewObj.find('.Select2').select2( { theme: "bootstrap"} );
						    
						    <!--- Bind options to change groups --->
							BindReminderLink(NewObj.find('.reminder-link'));
							BindEventLink(NewObj.find('.event-link'));
							BindChatLink(NewObj.find('.chat-link'));
							
							<!--- If US phone numbers put on some helpful formatting --->
							<cfif RetValGetCalendarConfiguration.PHONEFORMAT EQ 1> 
								NewObj.find('.inpSMSNumber').mask("(000) 000-0000");
							</cfif>

							//NewObj.find('.button-chat-calendar').prop('disabled', true);
							NewObj.find('.inpSMSNumber').removeAttr("disabled");
							NewObj.find('.inpTitle').removeAttr("disabled");
							NewObj.find('.datetimestart').removeAttr("disabled");
							NewObj.find('.TimeStepEnd').removeAttr("disabled");
							NewObj.find('.TimeStepStart').removeAttr("disabled");
							NewObj.find('.btn-save-event').removeAttr("disabled");

							NewObj.find('.button-chat-calendar').hide();
							NewObj.find('.tab-chat-calendar').hide();
							NewObj.find('.event-status').hide();
							
							    
					        return NewObj;
					    },
					    animation:false, <!--- If animation is true, the hide commands are no longer synchronous -  --->
			            trigger: 'manual',
			            placement: function (context, source) {
					        var position = $(source).offset(); <!--- Use offset because some events on day list are 0% position --->
					
					        var height = $(window).height();
					 		var width = $(window).width();

					 		var widthHalf = parseInt(width/2);

					 		var rightPos = position.left + $(source).width();
					 		
					 		var popoverWidth = 500;
					 		
					        if (position.left > widthHalf) {
					            return "left";
					        }
					
					        if (position.left <= widthHalf) {

					        	if( parseInt(rightPos + popoverWidth) > width){
					 				return "top";
					 			}
					 			else
					 				return "right";
					        }
					
					        return "top";
					    }
					}).on('shown.bs.popover', function () {	      
						//$("body").css('overflow-y','hidden');
		      	      	<!--- Add new Create new --->
		      	      	<!--- Make the UI flow better - preset the times to when the user clicks --->
		      	      	<!--- Use lowercase hh here for user experience - not really storing it yet so ok to not be 24 hour clock --->
		      	      	SkipCheckOptOut = 0;
						SkipCheckSchedule = 0;
		      	      	var ClickTimeStart = date.format('hh:mm A');
		      	      	var ClickDateStart = date.format('YYYY-MM-DD');
		      	      	var returned_endate = date.add(30, 'minutes');
						//var returned_endate = date.add(30, 'minutes');
		      	      	var ClickTimeEnd = returned_endate.format('hh:mm A');

						// var ClickDateStart = date.format('YYYY-MM-DD');
		      	      	var ClickDateEnd = date.format('YYYY-MM-DD');

		      	      	var footerOffset = $(".page-footer").offset();
		      	      	var popoverTop = parseInt(jsEvent.pageY) - parseInt($('.popover').height()/2);
		      	      	if( (popoverTop + 400) > footerOffset.top ){
		      	      		popoverTop = jsEvent.pageY-300;
		      	      	}

		      	      	$('.popover').css('top',popoverTop + 'px');
		      	      	
		      	      	<!--- Look at page size and center verticle and align top --->
		      	      	
						$('.datetimepicker').datetimepicker({
							format: 'YYYY-MM-DD'
						});

						$('.TimeStepStart').timepicker({ 	
							'step': 15, 
							'scrollDefault': '9:00 AM',
							'timeFormat': 'h:i A'			   							
						});						
						
						$('.datetimestart').val(ClickDateStart);
						$('.datetimeend').val(ClickDateEnd);

						<!--- Dont set time in month view if no time is found --->
						if(ClickTimeStart != '00:00 AM')
							$('.TimeStepStart').val(ClickTimeStart);
						   						
						$('.TimeStepEnd').timepicker({ 	
							'step': 15, 
							'scrollDefault': '9:00 AM',
							'timeFormat': 'h:i A'			   							
						});   					
						
						<!--- Dont set time in month view if no time is found --->
						if(ClickTimeEnd != '01:00 AM' && view.name != 'month'){
							if(ClickTimeEnd.substring(6) != ClickTimeStart.substring(6) && ClickTimeEnd.substring(6) =='AM'){
								$('.TimeStepEnd').val('11:59 PM');
							}else{
								$('.TimeStepEnd').val(ClickTimeEnd);
							}
						}else{
							$('.TimeStepEnd').val(ClickTimeEnd);
						}	
					}).on('hide.bs.popover', function () {
						$(".fc-time-grid, .fc-time-grid-container").css('overflow-y','auto');
						//$("body").css('overflow-y','auto');
					});
	        
					<!--- Manually trigger the popover --->
					$(this).popover('show');

					// Manually show/hide arrow
					$(".popover .arrow").hide();
					// if(isChooseAgendaDay){
					// 	$(".popover .arrow").hide();
					// }
				}
    		},
     			 
	        header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay, myCustomButton, myReportButton'   <!---agendaDay--->
			},
			defaultDate: '<cfoutput>#LSDateFormat(NOW(), "yyyy-mm-dd")#</cfoutput>',
			navLinks: true, // can click day/week names to navigate views
			editable: false, // Disables eventDrag and eventResize 
			eventLimit: true, // allow "more" link when too many events
			timeFormat : 'hh:mm a',		
			<!--- Default time at top of agend views --->
			scrollTime: '<cfoutput>#RetValGetCalendarConfiguration.SCROLLTIME#</cfoutput>',
			<!--- Keep user info scrolled into view https://stackoverflow.com/questions/26458108/fullcalendar-v2-how-to-maintain-the-same-scroll-time-when-navigating-weeks --->
			viewRender: function(view, element){
				console.log('viewRender');

				<!--- Hide any open popovers --->
				$('.popover').remove();
				
	        	if(scroll > -1 && (view.name=='agendaDay' || view.name=='agendaWeek')){
	                    setTimeout(function(){
	                    document.querySelector('.fc-scroller').scrollTop = scroll;
	                },0);
	
	            }
	        },
	        viewDestroy: function(view) {
				console.log('viewDestroy');
				if(view.name=='agendaDay' || view.name=='agendaWeek')
					scroll = document.querySelector('.fc-scroller').scrollTop;				
			},
			// selectable: true,
			selectHelper: true,	
			unselectAuto: false, <!--- Setting this to false lets the popover control when to remove the helper for new events - otherwise event disappears as soon as popover form is clicked on to edit - be sure to manually remove event on cancel or close --->
			eventResize: function(inpEvent, delta, revertFunc) {
				console.log('eventResize');
				//eventResize
					// if(inpEvent != 'undefined' && inpEvent != null)
					// {
					// 	<!--- Note: inpEvent.start is required in all events--->
					// 	inpEvent.inpStart = $.fullCalendar.formatDate(inpEvent.start, 'YYYY-MM-DD HH:mm:ss');
																									
					// 	if(inpEvent.end != null)
					// 		inpEvent.inpEnd = $.fullCalendar.formatDate(inpEvent.end, 'YYYY-MM-DD HH:mm:ss');
					// 	else
					// 		inpEvent.inpEnd = '';
						
					// 	var s = null;
					// 	var e = null; 

					// 	var ds = null;
					// 	var de = null; 

					// 	s = inpEvent.start.format('HH:mm A');
					// 	ds = inpEvent.start.format('MM/DD/YYYY');
							
					// 	// if(event.end != undefined)   
					// 	e = inpEvent.end.format('HH:mm A');	
					// 	de =  inpEvent.end.format('MM/DD/YYYY');
						
					// 	$('.popover-content').find('.TimeStepStart').val(s);
					// 	$('.popover-content').find('.TimeStepEnd').val(e);	

					// 	$('.popover-content').find('.datetimestart').val(ds);
					// 	$('.popover-content').find('.datetimeend').val(de);

					// 	inpEvent.inpTitle = inpEvent.title;

					// 	inpEvent.inpAllDayFlag = 0;				
					// 	inpEvent.inpURL = ''; 
					// 	inpEvent.inpClassName = ''; 
					// 	inpEvent.inpEditableFlag = 1; 
					// 	inpEvent.inpStartEditableFlag = 1; 
					// 	inpEvent.inpDurationEditableFlag = 1; 
					// 	inpEvent.inpResourceEditableFlag = 1; 
					// 	inpEvent.inpRendering = ''; 
					// 	inpEvent.inpOverlapFlag = true; 
					// 	inpEvent.inpConstraint = ''; 
					// 	inpEvent.inpColor = ''; 
					// 	inpEvent.inpBackgroundColor = ''; 
					// 	inpEvent.inpBorderColor = ''; 
					// 	inpEvent.inpTextColor = ''; 
					// 	// inpEvent.inpSMSNumber = ''; 
					// 	inpEvent.inpeMail = ''; 
					// 	inpEvent.inpNotes = ''; 
					// 	// inpEvent.inpConfirmationFlag;
					
					// 	<!--- Save changes to the dropped event - this is not called for external objects--->
					// 	var inpDoASR = parseInt('<cfoutput>#RetValGetCalendarConfiguration.ASR#</cfoutput>');
					
					// 	if(inpDoASR == 0 && inpEvent.inpSMSNumber.length > 9)
					// 	{
					// 		bootbox.dialog({
					// 			message: '<h4 class="be-modal-title">WARNING REMINDER</h4><p>Do you want to schedule a reminder?</p>',
					// 			title: '&nbsp;',
					// 			className: "be-modal",
					// 			buttons: {
					// 				success: {
					// 					label: "Yes",
					// 					className: "btn btn-medium green-gd",
					// 					callback: function(result) {
					// 						SaveCalendarEvent(inpEvent, 1);
					// 					}
					// 				},
					// 				cancel: {
					// 					label: "NO",
					// 					className: "green-cancel",
					// 					callback: function(result) {
					// 						SaveCalendarEvent(inpEvent, 0);
					// 					}
					// 				},
					// 			}
					// 		});

					// 		// bootbox.confirm("Do you want to schedule a reminder?", function(result) {
					// 		//   	if(result)
					// 		//    		inpDoASR = 1;
							
					// 		//    	SaveCalendarEvent(inpEvent, inpDoASR);
					// 		// });
					// 	}
					// 	else
					// 		SaveCalendarEvent(inpEvent, inpDoASR);
									
					// } 
				// eventResize  
		
		    },
			eventDragStart: function( event, jsEvent, ui, view ) {
				console.log('eventDragStart');
			},
			eventDrop: function(inpEvent, delta, revertFunc) {
				console.log('eventDrop');
				//eventDrop
					// if(inpEvent != 'undefined' && inpEvent != null)
					// {
					// 	<!--- Note: inpEvent.start is required in all events--->
					// 	inpEvent.inpStart = $.fullCalendar.formatDate(inpEvent.start, 'YYYY-MM-DD HH:mm:ss');
																									
					// 	if(inpEvent.end != null)
					// 		inpEvent.inpEnd = $.fullCalendar.formatDate(inpEvent.end, 'YYYY-MM-DD HH:mm:ss');
					// 	else
					// 		inpEvent.inpEnd = '';
													
					// 	inpEvent.inpTitle = inpEvent.title;

					// 	inpEvent.inpAllDayFlag = 0;				
					// 	inpEvent.inpURL = ''; 
					// 	inpEvent.inpClassName = ''; 
					// 	inpEvent.inpEditableFlag = 1; 
					// 	inpEvent.inpStartEditableFlag = 1; 
					// 	inpEvent.inpDurationEditableFlag = 1; 
					// 	inpEvent.inpResourceEditableFlag = 1; 
					// 	inpEvent.inpRendering = ''; 
					// 	inpEvent.inpOverlapFlag = true; 
					// 	inpEvent.inpConstraint = ''; 
					// 	inpEvent.inpColor = ''; 
					// 	inpEvent.inpBackgroundColor = ''; 
					// 	inpEvent.inpBorderColor = ''; 
					// 	inpEvent.inpTextColor = ''; 
					// 	// inpEvent.inpSMSNumber = ''; 
					// 	inpEvent.inpeMail = ''; 
					// 	inpEvent.inpNotes = ''; 
					// 	// inpEvent.inpConfirmationFlag;
				
					// 	<!--- Save changes to the dropped event - this is not called for external objects--->
					// 	var inpDoASR = parseInt('<cfoutput>#RetValGetCalendarConfiguration.ASR#</cfoutput>');
					
					// 	if(inpDoASR == 0 && inpEvent.inpSMSNumber.length > 9)
					// 	{
					// 		bootbox.dialog({
					// 			message: '<h4 class="be-modal-title">WARNING REMINDER</h4><p>Do you want to schedule a reminder?</p>',
					// 			title: '&nbsp;',
					// 			className: "be-modal",
					// 			buttons: {
					// 				success: {
					// 					label: "Yes",
					// 					className: "btn btn-medium green-gd",
					// 					callback: function(result) {
					// 						SaveCalendarEvent(inpEvent, 1);
					// 					}
					// 				},
					// 				cancel: {
					// 					label: "NO",
					// 					className: "green-cancel",
					// 					callback: function(result) {
					// 						SaveCalendarEvent(inpEvent, 0);
					// 					}
					// 				},
					// 			}
					// 		});
					// 		// bootbox.confirm("Do you want to schedule a reminder?", function(result) {
					// 		//   	if(result)
					// 		//    		inpDoASR = 1;
							
					// 		//    	SaveCalendarEvent(inpEvent, inpDoASR);
					// 		// });
					// }
					// else{
					// 	SaveCalendarEvent(inpEvent, inpDoASR);		    	
					// }   
	    	    //eventDrop
	        },
			eventClick: function(event, jsEvent, view) {

				console.log('eventClick');

				$('.popover').popover('hide');
				//    event.title = "CLICKED!";		
				//   $('#calendar').fullCalendar('updateEvent', event);
												
					<!--- Hide any open popovers --->
				$('[data-toggle="popover"],[data-original-title]').each(function () {
										
					(($(this).popover('hide').data('bs.popover')||{}).inState||{}).click = false  // fix for BS 3.3.6
				});
				
				(($('.fc-day, .fc-event').popover('hide').data('bs.popover')||{}).inState||{}).click = false  // fix for BS 3.3.6
				if(event.id){
					if( isMobile.any()){
						data_date = $(this).attr('data-date');
						data_event_action = 'update';
						data_event_obj = event;
						data_title = event.title;
						$('#Calendar-Event-Modal').find('.button-chat-calendar').show();
						$('#Calendar-Event-Modal').find('.tab-chat-calendar').show();
						$('#Calendar-Event-Modal').find('.event-status').show();
						$('#Calendar-Event-Modal').modal('show');
					}else{
						$(this).popover('show');  
						data_event_obj = event;	
						$('.button-chat-calendar').show();
						$('.tab-chat-calendar').show();
						$('.event-status').show();
						$('.datetimepicker').datetimepicker({
							format: 'YYYY-MM-DD'
						});					
					}	
				}else{
					$('#calendar').fullCalendar('unselect');
				}
		    },
	        eventRender: function(event, element){
	         	console.log('eventRender');
	        	//console.log(event);
	         	// data_date = $(this).attr('data-date');
	         	// data_event_action = 'update';
				// data_event_obj = event;
				// data_title = event.title;

	          	if( isMobile.any()){
	          		//render data
	          		selectHour =  event;
	          	}
	          	else{
	          		$(element).popover({
				    html: true,
				    container: 'body',
				    title: function () {
				        return  '' <!--- $('#Calendar-Event-Modal').find('.modal-header').html(); --->
				    },
				    content: function () {
						console.log("Get content - eventRender");						
					    <!--- Warning - called twice -  https://github.com/twbs/bootstrap/issues/12563 &&  https://stackoverflow.com/questions/42203109/content-function-called-twice-in-bootstrap-popover --->
					    
					    <!--- Set some attributes to be passed through as part of new content - this gets the date associated with the day that is clicked and stores it as an attribute of the popup content --->
					    <!--- Add a close button to top of pop-over --->
					    var NewObj = $('<div class="event-bubble-close close" tabindex="0" style="z-index: 1000;" role="button" aria-label="close">X</div>' + $('#Calendar-Event-Modal').find('.popover-section').html());			    

					    NewObj.attr('data-date', $(this).attr('data-date'));
					    NewObj.data('data-event-obj', event);	
					    NewObj.attr('data-event-action', 'update');					    
					    NewObj.find('.inpTitle').val(event.title);
					    				   					
						var s = null;
						var e = null;
						var ds = null;
						var de = null;
						
						if(event.start != undefined){
							s = event.start.format('HH:mm A');
							ds = event.start.format('YYYY-MM-DD');
						}   

						if(event.end != undefined){
							e = event.end.format('HH:mm A');
							de = event.end.format('YYYY-MM-DD');
						}else{
							event.end = event.start;
						}						
						
					    NewObj.find('.TimeStepStart').val(s);
					    NewObj.find('.TimeStepEnd').val(e);	
					
						NewObj.find('.datetimestart').val(ds);
					    NewObj.find('.datetimeend').val(de);	
					    
					    <!--- Set up all the #main-stage-content select2 boxes --->
						NewObj.find('.Select2').select2( { theme: "bootstrap"} );

						NewObj.find('#inpConfirmationFlag').val(event.inpConfirmationFlag).trigger("change");
						<!--- Bind options to change groups --->
						BindReminderLink(NewObj.find('.reminder-link'));
						BindEventLink(NewObj.find('.event-link'));
						BindChatLink(NewObj.find('.chat-link'));

						if (event.inpConfirmationFlag != <cfoutput>#APPT_REMINDER_NOT_SENT#</cfoutput> && event.inpConfirmationFlag != <cfoutput>#APPT_REMINDER_DO_NOT_SEND#</cfoutput> && event.inpConfirmationFlag != <cfoutput>#APPT_REMINDER_QUEUED#</cfoutput> && event.inpConfirmationFlag != <cfoutput>#APPT_REMINDER_OPT_OUT#</cfoutput>){
							NewObj.find('.inpSMSNumber').attr("disabled","disabled");
							NewObj.find('.inpTitle').attr("disabled","disabled");
							NewObj.find('.datetimestart').attr("disabled","disabled");
							NewObj.find('.TimeStepEnd').attr("disabled","disabled");
							NewObj.find('.TimeStepStart').attr("disabled","disabled");
							NewObj.find('.btn-save-event').attr("disabled","disabled");
						}
						
						NewObj.find('.inpSMSNumber').val(event.inpSMSNumber);
						 
						<!--- If US phone numbers put on some helpful formatting --->
						<cfif RetValGetCalendarConfiguration.PHONEFORMAT EQ 1> 
							NewObj.find('.inpSMSNumber').mask("(000) 000-0000");
						</cfif>
					        
				        return NewObj;
				    },
				    animation:false, <!--- If animation is true, the hide commands are no longer synchronous -  --->
		            trigger: 'manual',
		            placement: function (context, source) {

						console.log("Get placement - eventRender");
			       		var position = $(source).offset(); <!--- Use offset because some events on day list are 0% position --->
				        var height = $(window).height();
				 		var width = $(window).width();
				 		var widthHalf = parseInt(width/2);
				 		var rightPos = position.left + $(source).width();
				 		var popoverWidth = 500;
				 		
				        if (position.left > widthHalf) {
				            return "left";
				        }
				
				        if (position.left <= widthHalf) {
				        	if( parseInt(rightPos + popoverWidth) > width){
				 				return "top";
				 			}
				 			else
				 				return "right";
				        }
				
				        return "top";
				    }
					}).on('shown.bs.popover', function () {	 
						SkipCheckOptOut = 0;
						SkipCheckSchedule = 0;

		      	      	var footerOffset = $(".page-footer").offset();
		      	      	var popoverTop = element.offset().top;

		      	      	if( (popoverTop + 400) > footerOffset.top ){
		      	      		popoverTop = footerOffset-300;
		      	      	}

		      	      	$('.popover').css('top',popoverTop + 'px');

						$('.datetimepicker').datetimepicker({
							format: 'YYYY-MM-DD'
						});

						$('.TimeStepStart').timepicker({ 	
					       							'step': 15, 
						   							'scrollDefault': '9:00 AM',
						   							'timeFormat': 'h:i A'			   							
						   						});
						   						
						$('.TimeStepEnd').timepicker({ 	
					       							'step': 15, 
						   							'scrollDefault': '9:00 AM',
						   							'timeFormat': 'h:i A'			   							
						   						});   
						// Manually show/hide arrow
						$(".popover .arrow").hide();
						// if(isChooseAgendaDay){
						// 	$(".popover .arrow").hide();
						// }     	      	

						
		      	      	$(".fc-time-grid, .fc-time-grid-container").css('overflow-y','hidden');
						//$("body").css('overflow-y','hidden');

				    }).on('hide.bs.popover', function () {
				    	$(".fc-time-grid, .fc-time-grid-container").css('overflow-y','auto');
						//$("body").css('overflow-y','auto');
				    });	  		
				                       			
	          	}
	        },
	        eventAfterAllRender: function (view) {
	        	if($('.popover').length > 0){
	        		$(".fc-time-grid, .fc-time-grid-container").css('overflow-y','hidden');	
	        	}
                
            },
			events: [

			]
	    })
	
		$("#calendar").dblclick(function() {
			event.preventDefault();
			//do something here
		});
	
		<!--- Allow Agency Ad to expire - if served in javascript iframe loader, then expired will not show by default --->
        var InitExpire = $( "#Start_dt" ).val();
		$( "#Start_dt" ).datepicker({
		
			onSelect: function(dateText, inst) { 
		      var dateAsString = dateText; //the first parameter of this function
		      var dateAsObject = $(this).datepicker( 'getDate' ); //the getDate method
		   }
			
		});	
		
		$('.datetimepicker').datetimepicker({
			format: 'YYYY-MM-DD'
		});

		$(document).on("click", ".popover .btn-delete-event, #Calendar-Event-Modal .btn-delete-event" , function(){
			
			var inpEvent = $(this).parents(".popover-content").find('.event-bubble').data('data-event-obj');
			var eventID= 0;
			if(inpEvent != null && typeof inpEvent === "object"){
				eventID= inpEvent.id;
			}
			else
			{
				(($(this).parents(".popover").popover('hide').data('bs.popover')||{}).inState||{}).click = false  // fix for BS 3.3.6	        
	        	$('#calendar').fullCalendar('unselect'); 
				return ;
			}

			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/calendar.cfc?method=GetCalendarEventInfo&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				async: true,
				data:  
				{ 
					inpEventId : 	eventID
				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
				success:		
				<!--- Default return function for call back --->
				function(d) {																																		
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1){
						bootbox.dialog({
							message: '<h4 class="be-modal-title">DELETE EVENT</h4><p>Are you sure you want to delete this appointment?</p>',
							title: '&nbsp;',
							className: "be-modal",
							buttons: {
								success: {
									label: "Yes",
									className: "btn btn-medium green-gd",
									callback: function(result) {
										DeleteCalendarEvent(inpEvent);
									}
								},
								cancel: {
									label: "NO",
									className: "green-cancel",
									callback: function(result) {
									}
								},
							}
						});
					}else{
							if(d.ERRMESSAGE != "")	
							alertBox(d.ERRMESSAGE,"DELETE FAIL");										
					}												
				} 	
			});	
			
			(($(this).parents(".popover").popover('hide').data('bs.popover')||{}).inState||{}).click = false  // fix for BS 3.3.6
			
		});

		// Chat with customer
		var Createnewchatsession = function(){
			event.preventDefault();
			$.ajax({
				url: '/session/sire/models/cfc/calendar.cfc?method=MakeNewChatSessionForContactStringCalendar&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				data: {
					inpContactString: data_event_obj.inpSMSNumber,
					inpSkipCheckOPT: 1
				},
				beforeSend: function () {
					$("#processingPayment").show();
				}
			})
			.done(function(data) {
				$("#processingPayment").hide();
				data = JSON.parse(data);
				//console.log(data);
				if (parseInt(data.RXRESULTCODE) > 0) {
					data_chatsessionid = data.NEWSESSIONID;
					//console.log('CHATSESSION' + data.NEWSESSIONID);
					// Update Event with chatsessionid
					$.ajax({
						url: '/session/sire/models/cfc/calendar.cfc?method=UpdateEventWithChatSessionID&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
						type: 'POST',
						data: {
							inpEventId: data_event_obj.id,
							inpChatSessionId: data_chatsessionid
						},
						beforeSend: function () {
							$("#processingPayment").show();
						}
					})
					.done(function(result) {
						if (parseInt(result.RXRESULTCODE) > 0) {
							//console.log('Update Event with chatsessionid');
							$("#processingPayment").hide();
							GetResponseList(data.NEWSESSIONID);
							$('#chat-modal').modal('show');
							// $('#chat-modal').find('#titlemodalchat').text("Chat with Customer Create new "+ checkPhonenumber);
						} else {
							alertBox(result.MESSAGE, 'Oops!');
						}
					})
					.fail(function(e, msg) {
						//console.log(msg);
					})
				} else {
					alertBox(data.MESSAGE, 'Oops!');
				}
			})
			.fail(function(e, msg) {
				//console.log(msg);
				$("#processingPayment").hide();
			})
			.always(function() {
				$("#processingPayment").hide();
			});
		}

		$(document).on("click", ".popover .button-chat-calendar, .event-bubble .button-chat-calendar" , function(){
			//Check exist chat session data_event_obj.id
			event.preventDefault();
			GetResponseList(0);
			var checkPhonenumber = $(this).parents(".popover-content").find('.inpSMSNumber').val();
			if(checkPhonenumber == ''){
				alertBox("Mobile Phone Number invalid. Please update Mobile Phone Number!.", "ERROR");
				return;
			}
			// Check phone number with many chat sessiong with the same user
			data_event_obj.inpConfirmationFlag = <cfoutput>#APPT_REMINDER_CHAT_ACTIVE#</cfoutput>;
			$.ajax({
				url: '/session/sire/models/cfc/calendar.cfc?method=CheckSessionChatCalendar&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				data: {
					inpEventId: data_event_obj.id,
					inpContactString: data_event_obj.inpSMSNumber
				},
				beforeSend: function () {
					$("#processingPayment").show();
				}
			})
			.done(function(data) {
				if (parseInt(data.RXRESULTCODE) > 0) {
					$('#chat-modal').find('#inpConfirmationFlag').val(data_event_obj.inpConfirmationFlag).trigger("change");
					// set Event status to Chat Active
					UserShortCodeId = data.SHORTCODEID;
					if(data.CHATSESSIONID > 0){
						// get chatsession id
						data_chatsessionid = data.CHATSESSIONID;
						//GetResponseList(data_chatsessionid);
						fnReactiveChatSession(data_chatsessionid);
						setTimeout(function() {
							$("#processingPayment").hide();
							$('#chat-modal').modal('show');
						}, 1000);
					} else {
					// Start New Chat debug
						// $.ajax({
						// 	url: '/session/sire/models/cfc/calendar.cfc?method=MakeNewChatSessionForContactStringCalendar&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
						// 	type: 'POST',
						// 	data: {
						// 		inpContactString: data_event_obj.inpSMSNumber,
						// 		inpSkipCheckOPT: 1
						// 	},
						// 	beforeSend: function () {
						// 		$("#processingPayment").show();
						// 	}
						// })
						// .done(function(data) {
						// 	$("#processingPayment").hide();
						// 	data = JSON.parse(data);
						// 	//console.log(data);
						// 	if (parseInt(data.RXRESULTCODE) > 0) {
						// 		data_chatsessionid = data.NEWSESSIONID;
						// 		//console.log('CHATSESSION' + data.NEWSESSIONID);
						// 		// Update Event with chatsessionid
						// 		$.ajax({
						// 			url: '/session/sire/models/cfc/calendar.cfc?method=UpdateEventWithChatSessionID&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
						// 			type: 'POST',
						// 			data: {
						// 				inpEventId: data_event_obj.id,
						// 				inpChatSessionId: data_chatsessionid
						// 			},
						// 			beforeSend: function () {
						// 				$("#processingPayment").show();
						// 			}
						// 		})
						// 		.done(function(result) {
						// 			if (parseInt(result.RXRESULTCODE) > 0) {
						// 				$("#processingPayment").hide();
						// 				$('#chat-modal').modal('show');
						// 			} else {
						// 				alertBox(result.MESSAGE, 'Oops!');
						// 			}
						// 		})
						// 		.fail(function(e, msg) {
						// 			//console.log(msg);
						// 		})
						// 	} else {
						// 		alertBox(data.MESSAGE, 'Oops!');
						// 	}
						// })
						// .fail(function(e, msg) {
						// 	//console.log(msg);
						// 	$("#processingPayment").hide();
						// })
						// .always(function() {
						// 	$("#processingPayment").hide();
						// });
					// start old
					Createnewchatsession();
					}
				}else{ // validate chat session
					if(parseInt(data.RXRESULTCODE) == -2){
						$("#processingPayment").hide();
						//This contact string is available on another chat session
						alertBox(data.MESSAGE, 'Oops!');
					}else if(parseInt(data.RXRESULTCODE) == -3){
						$("#processingPayment").hide();
						//There is an active chat session with this subscriber
						bootbox.dialog({
							message: '<h4 class="be-modal-title">CHAT SESSION</h4><p>'+data.MESSAGE+'</p>',
							title: '&nbsp;',
							className: "be-modal",
							buttons: {
								success: {
									label: "Yes",
									className: "btn btn-medium green-gd",
									callback: function(result) {
										data_chatsessionid = data.CHATSESSIONID;
										fnEndChatOtherSession(data_event_obj.inpSMSNumber,data.CHATSESSIONID);
									}
								},
								cancel: {
									label: "NO",
									className: "green-cancel",
									callback: function(result) {
									}
								},
							}
						});

					}
				}
			})
			.fail(function(e, msg) {
				//console.log(msg);
			})
			.always(function() {
				//$("#processingPayment").hide();
			});
		});

		$(document).on("click", "#make-new-chat" , function(){
			// $("#processingPayment").show();
			$.ajax({
					url: '/session/sire/models/cfc/calendar.cfc?method=GetCalendarEventInfo&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					type: 'POST',
					data: {
						inpEventId: data_event_obj.id
					},
					beforeSend: function () {
					}
				})
				.done(function(data) {
					if (parseInt(data.RXRESULTCODE) > 0) {
						UserShortCodeId = data.SHORTCODEID;
						fnReactiveChatSession(data.CHATSESSIONID);
						$(".btn-endchat").prop('disabled', false);
						//Re Active Chat Session
					}
				})
				.fail(function(e, msg) {
					//console.log(msg);
				})
				.always(function() {
					$("#processingPayment").hide();
				});
				
		});

		//$(document).on("click", "#end-chat" , function(){
			// //console.log(data_event_obj);
			// $.ajax({
			// 	url: '/session/sire/models/cfc/calendar.cfc?method=completeSessionSameContact&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			// 	type: 'POST',
			// 	data: {
			// 		inpContactString: data_event_obj.inpSMSNumber
			// 	},
			// 	beforeSend: function () {
			// 	},				  
			// 	error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
			// 	success:		
			// 	<!--- Default return function for call back --->
			// 	function(d) {																																		
			// 		<!--- RXRESULTCODE is 1 if everything is OK --->
			// 		d = JSON.parse(d);
			// 		if (d.RXRESULTCODE == 1){
			// 			Createnewchatsession();
			// 		}else{
			// 			if(d.ERRMESSAGE != "")	
			// 			alertBox(d.ERRMESSAGE,"END CHAT");										
			// 		}												
			// 	} 
			// })
			// .fail(function(e, msg) {
			// 	//console.log(msg);
			// })
			// .always(function() {
			// 	$("#processingPayment").hide();
			// });
				
		//});

		var fnEndChatOtherSession = function(contactstring, chatsessioncurr) {
			$.ajax({
				url: '/session/sire/models/cfc/calendar.cfc?method=completeSessionSameContact&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				data: {
					inpContactString: contactstring
				},
				beforeSend: function () {
				},				  
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
				success:		
				<!--- Default return function for call back --->
				function(d) {																																		
					<!--- RXRESULTCODE is 1 if everything is OK --->
					d = JSON.parse(d);
					if (d.RXRESULTCODE == 1){
						if(chatsessioncurr == 0){// chat session exist
							Createnewchatsession();
						}else{// chat session exist
							fnReactiveChatSession(chatsessioncurr);
							//GetResponseList(chatsessioncurr);
							setTimeout(function() {
								$("#processingPayment").hide();
								$('#chat-modal').modal('show');
							}, 1000);
						}
						
					}else{
						if(d.ERRMESSAGE != "")	
						alertBox(d.ERRMESSAGE,"CLOSE OTHER CHAT");										
					}												
				} 
			})
			.fail(function(e, msg) {
				//console.log(msg);
			})
			.always(function() {
				$("#processingPayment").hide();
			});
		};

		$('body').on('click', '#SMSSend', function(event) {
			event.preventDefault();
			SendSMS();
		});

		<!--- Enter to Send--->
		$("#SMSTextInputArea").keypress(function(e) {
			if (enter_to_send && e.keyCode == 13) {
				e.preventDefault();
				SendSMS();
			}
		});

		$("body").on('change', '#enter-to-send', function(event) {			
			event.preventDefault();
			var value = this.checked ? 1 : 0;
			var type = 2;
			$.ajax({
				url: '/session/sire/models/cfc/smschat.cfc?method=UpdateUserSetting&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				data: {
					inpSettingType: 2,
					inpValue: value
				},
			});
			enter_to_send = value;
		});
		<!--- Enter to Send--->

		<!--- End chat --->
		var fnMarkSessionComplete = function(sessionid) {
			$.ajax({
				url: '/session/sire/models/cfc/smschat.cfc?method=completeSession&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				data: {inpSessionId: sessionid},
				beforeSend: function() {
					$("#processingPayment").show();
				}
			})
			.done(function(data) {
				data = JSON.parse(data);
				console.log(data_chatsessionid);
				console.log(sessionid);
				if (parseInt(data.RXRESULTCODE) > 0) {
					GetResponseList(sessionid);
					fnUpdateChatCloseStatus(sessionid);
					$(".btn-endchat").prop('disabled', true);
				} else {
					alertBox(data.MESSAGE);
				}
			})
			.fail(function(e,msg) {
				//console.log(msg);
			})
			.always(function() {
				$("#processingPayment").hide();
			});
		};

		var fnUpdateChatCloseStatus = function(sessionid) {
			$.ajax({
				url: '/session/sire/models/cfc/calendar.cfc?method=UpdateChatCloseStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				data: {inpSessionId: sessionid},
				beforeSend: function() {
				}
			})
			.done(function(data) {
				if(parseInt(data.RXRESULTCODE) < 0) {
					//alertBox(data.MESSAGE);
				}
			})
			.fail(function(e,msg) {
			})
			.always(function() {
			});
		};

		var fnReactiveChatSession = function(sessionid) {
			$.ajax({
				url: '/session/sire/models/cfc/calendar.cfc?method=ReactiveChatSession&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				data: {inpSessionId: sessionid},
				beforeSend: function() {
				}
			})
			.done(function(data) {
				if(parseInt(data.RXRESULTCODE) == 1) {
					GetResponseList(sessionid);
				}else{
					alertBox(data.MESSAGE);
				}
			})
			.fail(function(e,msg) {
			})
			.always(function() {
			});
		};

		$("body").on('click', '.btn-endchat', function(event) {
			event.preventDefault();
			if(data_chatsessionid > 0){
				bootbox.dialog({
					message: '<h4 class="be-modal-title">End chat</h4><p>Are you sure you want to close this chat?</p>',
					title: '&nbsp;',
					className: "be-modal",
					buttons: {
						success: {
							label: "Yes",
							className: "btn btn-medium green-gd",
							callback: function(result) {
							// Change status to Chat Close
							fnMarkSessionComplete(data_chatsessionid);
							}
						},
						cancel: {
							label: "NO",
							className: "green-cancel",
							callback: function () {
							}
						},
					}
				});
			}else{
				alertBox("The Session Id not exist.","End Chat");
			}
			
		});
		
		<!--- End chat --->

		var GetResponseList = function (sessionid) {
			
			$.ajax({
				url: '/session/sire/models/cfc/smschat.cfc?method=GetResponseForIRESessionCalendar&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				data: {inpSessionId: sessionid}
			})
			.done(function(data) {
				$('#mCSB_1_container').html('');
				if (parseInt(data.RXRESULTCODE) > 0) {
					if (data.PREVIEW) {
						$("#SMSTextInputArea").val('');
						$("#SMSTextInputArea").prop('disabled', true);
						$("#SMSSend").prop('disabled', true);
						DisplayMessage('newbubble info', "This session is for preview function. You can't send message on this.");
						$(".newbubble.info").css('margin-bottom', '20px');
						SocketListen(data.SSCHN);
					}
					for (var i = 0; i < data.RESPONSE.length; i++) {
						var m = data.RESPONSE[i];
						var cssClass = (m.TYPE == 1) ? "newbubble me" : "newbubble guess";
						DisplayMessage(cssClass,escapeHtml(m.MSG), m.TIME);
					}
					if (!data.PREVIEW) {
						if (parseInt(data.SESSIONSTATE) != 1) {
							if (parseInt(data.EMSFLAG) == 20) {
								CheckActiveSession();
							} else {
								var msg = 'This session is closed.';
								DisplayMessage('newbubble info', msg);
							}
							$("#SMSTextInputArea").val('');
							$("#SMSTextInputArea").prop('disabled', true);
							$("#SMSSend").prop('disabled', true);
							RemoveListener(currChannel);
						} else {
							$("#SMSTextInputArea").val('');
							$("#SMSTextInputArea").prop('disabled', false);
							$("#SMSSend").prop('disabled', false);
							SocketListen(data.SSCHN);
						}
					}
				}
				UpdateScrollBar();
				//$('#chat-modal').modal('show');
			})
			.fail(function(e,msg) {
				//console.log(msg);
			});
		}

		var CheckActiveSession = function () {
			if (data_event_obj.inpSMSNumber == '') {
				return false;
			}
			$.ajax({
				url: '/session/sire/models/cfc/smschat.cfc?method=GetActiveSessionForContactString&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				data: {inpContactString: data_event_obj.inpSMSNumber},
			})
			.done(function(data) {
				//if (parseInt(data.RXRESULTCODE) == 1) {
				//	var msg = 'Session Closed. You have another active conversation with this customer, please click <a href="/session/sire/pages/sms-response?ssid='+data.SESSIONID+'" class="alert-link">here</a> to enter.';
				//} else {
					var msg = 'Session Closed. Please <a id="make-new-chat" class="alert-link text-color-red" <!---data-dismiss="modal"--->>click here</a> to make a new conversation with this customer.';
				//}
				DisplayMessage('newbubble info', msg);
			})
			.fail(function(e,msg) {
				//console.log(msg);
			});
		}

		var UpdateScrollBar = function () {
			if ($("#mCSB_1_container").children('div').length > 0) {
				$("#SMSHistoryScreenArea").mCustomScrollbar("update");
				$("#SMSHistoryScreenArea").mCustomScrollbar("scrollTo", "last");
			}
		}

		var DisplayMessage = function(css, msg, time) {
			$('#mCSB_1_container').append('<div style="clear:both"></div><div class="'+css+'">' + (typeof msg != "undefined" ? msg.replace(/\r?\n/g, '<br />') : "") + '</div><span>'+ (typeof time != "undefined" ? time : "") +'</span>');
		}

		 /* Socket io */
		var socket = io('https://ws.siremobile.com:8443/');

		var RemoveListener = function(channel){
			socket.removeListener(channel);
		}

		var SocketListen = function(channel) {
			// Remove current channel being listening
			RemoveListener(currChannel);
			console.log("Socket run");

			socket.on(channel, function(data){
				//console.log('receive'+ Date.now()+" "+channel);
				data = jQuery.parseJSON(data);
				if (data.type == 2) {
					var cssClass = 'newbubble guess';
				} else {
					var cssClass = 'newbubble me';
				}

				DisplayMessage(cssClass, escapeHtml(data.msg), data.time);

				UpdateScrollBar();

				<!--- if (data.type == 2 && data.msg.toLowerCase() == "exit") {
					if (data.sessionid == currSessionId) {
						location.href = "/session/sire/pages/sms-response?ssid="+currSessionId;
					} else {
						setTimeout(function () {
							GetSessionList(1);
						}, 2000);
					}
				} --->

				<!--- if (data.sessionid == currSessionId) {
					setTimeout(function () {
						SetSessionRead(currSessionId);
						$(".chat-session[data-id='"+currSessionId+"']").find('span').text('');
					}, 2000);
				} --->
			});
			currChannel = channel;
		}

		socket.on(us_chn, function(data){
			data = jQuery.parseJSON(data);
			if (data.msg.toLowerCase() == "exit") {
				<!--- setTimeout(function () {
					GetSessionList(1);
				}, 2000); --->
			} else {
				<!--- GetSessionList(1); --->
			}
		});
		$("#text-form").validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : true});
		var SendSMS = function () {
			if ($("#text-form").validationEngine('validate')) {
				if (data_chatsessionid <= 0) {
					$("#SMSTextInputArea").val('');
					return false;
				}
				var inpTextToSend = $("#SMSTextInputArea").val();
				$.ajax({
					url: '/session/sire/models/cfc/calendar.cfc?method=SendSMS&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					type: 'POST',
					data: {
						inpSessionId: data_chatsessionid,
						inpTextToSend: inpTextToSend,
						inpShortCodeId: UserShortCodeId
					},
					beforeSend: function () {
						$("#processingPayment").show();
					}
				})
				.done(function(data) {
					data = JSON.parse(data);
					if (parseInt(data.RXRESULTCODE) > 0) {
						$("#SMSTextInputArea").val('');
					} else {
						bootbox.dialog({
							message: '<h4 class="be-modal-title">Oops!</h4><p>'+ data.MESSAGE +'</p>',
							title: '&nbsp;',
							className: "be-modal",
							buttons: {
								success: {
									label: "OK",
									className: "green-gd",
									callback: function(){
										//$(".chat-session[data-id='"+currSessionId+"']").trigger('click');
										$("#SMSTextInputArea").val();
									}
								},
							},
							onEscape: function () {
								//$(".chat-session[data-id='"+currSessionId+"']").trigger('click');
								$("#SMSTextInputArea").val();
							}
						});
					}
				})
				.fail(function(e, msg) {
					//console.log(msg);
				})
				.always(function() {
					$("#processingPayment").hide();
					fnUpdateUserCurrentBalance();
					GetResponseList(data_chatsessionid);
				});
			}
		}

		// Save Event
		$(document).on("click", ".popover-content .btn-save-event, #Calendar-Event-Modal .btn-save-event" , function(){
	    		    	
	    	 // var inpEvent = new Object();
	      	<!--- Save the event to the Calendar --->
	        var Today = $(this).parents(".popover-content").find('.event-bubble').attr('data-date');
	    	var Endday = $(this).parents(".popover-content").find('.event-bubble').attr('data-date');
	    	var inpEvent = $(this).parents(".popover-content").find('.event-bubble').data('data-event-obj');
			var SaveOrUpdate = 0; 
			var queueStatus = 0;
			var optout = 0;			
			//alert(inpEvent);
			if(inpEvent != 'undefined' && inpEvent != null)
			{
				<!--- Note: inpEvent.start is required in all events--->				
				Today = $.fullCalendar.formatDate(inpEvent.start, 'YYYY-MM-DD');
				Endday = $.fullCalendar.formatDate(inpEvent.end, 'YYYY-MM-DD');		

				if($(this).parents(".popover-content").find('.datetimestart').val().length == 0){
					alertBox("The Event Date is required.","WARRING");
					return false;
				}

				if($(this).parents(".popover-content").find('.TimeStepStart').val().length == 0){
					alertBox("Start time is required.", "TIME INVALID");
					return false;
				}else{
					inpEvent.inpStart = $(this).parents(".popover-content").find('.datetimestart').val() + ' ' + $(this).parents(".popover-content").find('.TimeStepStart').val();
					var s = moment(inpEvent.inpStart, 'YYYY-MM-DD HH:mm A');  
				}

				if($(this).parents(".popover-content").find('.TimeStepEnd').val().length == 0){
					alertBox("End time is required.", "TIME INVALID");
					return false;
				}else{
					inpEvent.inpEnd = $(this).parents(".popover-content").find('.datetimestart').val() + ' ' + $(this).parents(".popover-content").find('.TimeStepEnd').val();	
					var e = moment(inpEvent.inpEnd, 'YYYY-MM-DD HH:mm A'); 
				}

				if(s != 'undefined' && s != null && e != 'undefined' && e != null)
				{
					if (s >= e){
						alertBox("Start Time must less than End Time!.","WARRING");
						return false;
					}
				}


				if($(this).parents(".popover-content").find('.TimeStepStart').val().length > 0)
				{		
					inpEvent.inpStart = $(this).parents(".popover-content").find('.datetimestart').val() + ' ' + $(this).parents(".popover-content").find('.TimeStepStart').val();
					<!--- Convert to Moments --->						
					var s = moment(inpEvent.inpStart, 'YYYY-MM-DD HH:mm A');    
					inpEvent.start = s; // s.format("YYYY-MM-DDTHH:mm:ss");
				}
				else
				{
					inpEvent.start = null;
					inpEvent.inpStart = '';
				}
				
				if($(this).parents(".popover-content").find('.TimeStepEnd').val().length > 0)
				{
					inpEvent.inpEnd = $(this).parents(".popover-content").find('.datetimestart').val() + ' ' + $(this).parents(".popover-content").find('.TimeStepEnd').val();	
					<!--- Convert to Moments --->
					var e = moment(inpEvent.inpEnd, 'YYYY-MM-DD HH:mm A'); 
					inpEvent.end = e; // e.format("YYYY-MM-DDTHH:mm:ss");
				}
				else
				{
					inpEvent.end = null;
					inpEvent.inpEnd = '';
				}
										
				inpEvent.inpTitle = $(this).parents(".popover-content").find('.inpTitle').val();
				inpEvent.title = $(this).parents(".popover-content").find('.inpTitle').val();
				
				inpEvent.inpAllDayFlag = 0;				
				inpEvent.inpURL = ''; 
				inpEvent.inpClassName = ''; 
				inpEvent.inpEditableFlag = 1; 
				inpEvent.inpStartEditableFlag = 1; 
				inpEvent.inpDurationEditableFlag = 1; 
				inpEvent.inpResourceEditableFlag = 1; 
				inpEvent.inpRendering = ''; 
				inpEvent.inpOverlapFlag = true; 
				inpEvent.inpConstraint = ''; 
				inpEvent.inpColor = ''; 
				inpEvent.inpBackgroundColor = ''; 
				inpEvent.inpBorderColor = ''; 
				inpEvent.inpTextColor = ''; 
				inpEvent.inpSMSNumber = $(this).parents(".popover-content").find('.inpSMSNumber').val();
				inpEvent.inpeMail = ''; 
				inpEvent.inpNotes = ''; 
				inpEvent.inpConfirmationFlag = $(this).parents(".popover-content").find('#inpConfirmationFlag').val();
							
			}   
			else
			{		    	
				inpEvent = new Object();
				
				inpEvent.id = '';

				if($(this).parents(".popover-content").find('.datetimestart').val().length == 0){
					alertBox("The Event Date is required.","WARRING");
					return false;
				}

				if($(this).parents(".popover-content").find('.TimeStepStart').val().length == 0){
					alertBox("Start time is required.","WARRING");
					return false;
				}

				if($(this).parents(".popover-content").find('.TimeStepEnd').val().length == 0){
					alertBox("End time is required.","WARRING");
					return false;
				}

				inpEvent.inpStart = $(this).parents(".popover-content").find('.datetimestart').val() + ' ' + $(this).parents(".popover-content").find('.TimeStepStart').val();
				inpEvent.inpEnd = $(this).parents(".popover-content").find('.datetimestart').val() + ' ' + $(this).parents(".popover-content").find('.TimeStepEnd').val();	
				<!--- Convert to Moments --->	

				var s = moment(inpEvent.inpStart, 'YYYY-MM-DD HH:mm A');    
				inpEvent.start = s.format("YYYY-MM-DDTHH:mm:ss");
				var e = moment(inpEvent.inpEnd, 'YYYY-MM-DD HH:mm A'); 
				inpEvent.end = e.format("YYYY-MM-DDTHH:mm:ss");

				if (s >= e){
					alertBox("Start Time must less than End Time!.","WARRING");
					return false;
				}
					
				inpEvent.inpTitle = $(this).parents(".popover-content").find('.inpTitle').val();
				inpEvent.title = $(this).parents(".popover-content").find('.inpTitle').val();
				
				inpEvent.inpAllDayFlag = 0;				
				inpEvent.inpURL = ''; 
				inpEvent.inpClassName = ''; 
				inpEvent.inpEditableFlag = 1; 
				inpEvent.inpStartEditableFlag = true; 
				inpEvent.inpDurationEditableFlag = true; 
				inpEvent.inpResourceEditableFlag = true; 
				inpEvent.inpRendering = ''; 
				inpEvent.inpOverlapFlag = true; 
				inpEvent.inpConstraint = ''; 
				inpEvent.inpColor = ''; 
				inpEvent.inpBackgroundColor = ''; 
				inpEvent.inpBorderColor = ''; 
				inpEvent.inpTextColor = ''; 
				inpEvent.inpSMSNumber = $(this).parents(".popover-content").find('.inpSMSNumber').val();
				inpEvent.inpeMail = ''; 
				inpEvent.inpNotes = ''; 
				inpEvent.inpConfirmationFlag = $(this).parents(".popover-content").find('#inpConfirmationFlag').val();	
				inpEvent.inpConfirmationOneDTSId = '0';
			}	   	

			<!--- 1 contactstring just get limit 3 reminders on 1 day--->
			//console.log(inpEvent);
			
			if(inpEvent.inpTitle == ''){
				alertBox("The Event name is required.","WARRING");
				return false;
			}
			if(inpEvent.inpSMSNumber == ''){
				alertBox("The Phone number is required.","WARRING");
				return false;
			}
			
			if($(this).parents(".popover-content").find('.event-bubble').attr('data-event-action') == 'add' ){
				SaveOrUpdate = 0;
			}else{
				SaveOrUpdate = 1;
			}

			try{
			// old code
				// $.ajax({
				// 	type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				// 	url: '/session/sire/models/cfc/calendar.cfc?method=ContactstringLimited&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				// 	dataType: 'json',
				// 	async: true,
				// 	data:  
				// 	{ 	
				// 		inpEventId : inpEvent.id,
				// 		inpEventStart : inpEvent.inpStart,
				// 		inpSMSNumber : inpEvent.inpSMSNumber							
				// 	},	
				// 	beforeSend: function(){
				// 		$('#processingPayment').show();
				// 	},				  
				// 	error: function(XMLHttpRequest, textStatus, errorThrown) {
				// 		$('#processingPayment').hide(); 
				// 		alertBox("No Response from the remote server. Check your connection and try again.","ERROR1"); 
				// 	},					  
				// 	success:function(d) 
				// 	{	
				// 		$('#processingPayment').hide();
				// 		<!--- RXRESULTCODE is 1 if everything is OK --->
				// 		if (parseInt(d.RXRESULTCODE) == 2)  //&& SaveOrUpdate == 0 ???
				// 		{		
				// 			alertBox("This phone number have exceeded reminder limit.", "WARNING!");	
				// 			return false;		
				// 		}else{	
				// 				var startdate = new Date(inpEvent.start);	
				// 				var currentdate = new Date();																										
				// 				<!--- RXRESULTCODE is 1 if everything is OK --->
				// 					if(SaveOrUpdate == 0)	// Add new	
				// 					{
				// 						<!--- Save changes event - this is not called for external objects--->
				// 						var inpDoASR = parseInt('<cfoutput>#RetValGetCalendarConfiguration.ASR#</cfoutput>');
				// 						<!--- Check Phone number opt-out --->
				// 						$.ajax({
				// 							type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				// 							url: '/session/sire/models/cfc/optin-out.cfc?method=checkContactStringOptout&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				// 							dataType: 'json',
				// 							async: true,
				// 							data:  
				// 							{ 
				// 								inpContactString : inpEvent.inpSMSNumber,
				// 								inpShortCode: '<cfoutput>#inpShortCode#</cfoutput>'
				// 							},					  
				// 							error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
				// 							success:		
				// 							<!--- Default return function for call back --->
				// 							function(d) {																																		
				// 								<!--- RXRESULTCODE is 1 if everything is OK --->
				// 								if (d.RXRESULTCODE == 1){
				// 								// Phone number opt out
				// 									bootbox.dialog({
				// 										message: '<h4 class="be-modal-title">PHONE NUMBER OPT-OUT</h4><p>Are you sure you want to create this appointment?</p>',
				// 										title: '&nbsp;',
				// 										className: "be-modal",
				// 										buttons: {
				// 											success: {
				// 												label: "Yes",
				// 												className: "btn btn-medium green-gd",
				// 												callback: function(result) {
				// 													queueStatus = 0; // Create Event with phone number opt out.
				// 													optout = 1;
				// 													AddCalendarEvent(inpEvent, queueStatus,optout);
				// 												}
				// 											},
				// 											cancel: {
				// 												label: "NO",
				// 												className: "green-cancel",
				// 												callback: function(result) {
				// 													$('.popover').popover('hide');
				// 													$('#calendar').fullCalendar('unselect');

				// 												}
				// 											},
				// 										}
				// 									});
				// 								}else{
				// 								// Phone number not opt out
				// 									if(inpDoASR == 0 && inpEvent.inpSMSNumber.length > 9 && startdate >= currentdate)
				// 									{
				// 										bootbox.dialog({
				// 											message: '<h4 class="be-modal-title">WARNING REMINDER</h4><p>Do you want to schedule a reminder?</p>',
				// 											title: '&nbsp;',
				// 											className: "be-modal",
				// 											buttons: {
				// 												success: {
				// 													label: "Yes",
				// 													className: "btn btn-medium green-gd",
				// 													callback: function(result) {
				// 														//AddCalendarEvent(inpEvent, 1);
				// 														queueStatus = 1;
				// 														AddCalendarEvent(inpEvent, queueStatus);
				// 													}
				// 												},
				// 												cancel: {
				// 													label: "NO",
				// 													className: "green-cancel",
				// 													callback: function(result) {
				// 														//AddCalendarEvent(inpEvent, 0);
				// 														//location.reload();
				// 														queueStatus = 0;
				// 														AddCalendarEvent(inpEvent, queueStatus);
				// 													}
				// 												},
				// 											}
				// 										});
				// 									}
				// 									else{
				// 										//AddCalendarEvent(inpEvent, inpDoASR);
				// 										//location.reload();
				// 										queueStatus = inpDoASR;
				// 										AddCalendarEvent(inpEvent, queueStatus);
				// 									}	
				// 								}												
				// 							} 	
				// 						});
				// 					}
				// 					else
				// 					{
				// 					// Edit Event
				// 						<!--- Check Phone number opt-out --->
				// 						$.ajax({
				// 							type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				// 							url: '/session/sire/models/cfc/optin-out.cfc?method=checkContactStringOptout&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				// 							dataType: 'json',
				// 							async: true,
				// 							data:  
				// 							{ 
				// 								inpContactString : inpEvent.inpSMSNumber,
				// 								inpShortCode: '<cfoutput>#inpShortCode#</cfoutput>'
				// 							},					  
				// 							error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
				// 							success:		
				// 							<!--- Default return function for call back --->
				// 							function(data) {																																		
				// 								<!--- RXRESULTCODE is 1 if everything is OK --->
				// 								// if (d.RXRESULTCODE == 1){
				// 								// // Phone number opt out
				// 								// 	alertBox("The event can not save with phone number opt out!","WARRING");
				// 								// 	//bootbox.confirm("The event can not save with phone number opt out!", function(result){ 
				// 								// 	//});
				// 								// }else{
				// 								// Phone number not opt out
				// 									$.ajax({
				// 										type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				// 										url: '/session/sire/models/cfc/calendar.cfc?method=GetCalendarEventInfo&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				// 										dataType: 'json',
				// 										async: true,
				// 										data:  
				// 										{ 
				// 											inpEventId : inpEvent.id	
				// 										},					  
				// 										error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
				// 										success:		
				// 										<!--- Default return function for call back --->
				// 										function(d) {																																		
				// 											<!--- RXRESULTCODE is 1 if everything is OK --->
				// 											if (d.RXRESULTCODE == 1){
				// 												var phonenumberstr = inpEvent.inpSMSNumber;
				// 												if(data.RXRESULTCODE == 1 && d.CONTACTSTRING != phonenumberstr.replace(/[^0-9]/g,"")){ // Just allow edit without phone number opt-out
				// 													alertBox("The event can not save with phone number opt out!","WARRING");
				// 												}else if(d.DTSID == 0 || d.CONTACTQUEUESTATUS == 9){
				// 													//aaaaaaa
				// 													var inpDoASR = parseInt('<cfoutput>#RetValGetCalendarConfiguration.ASR#</cfoutput>');
				// 													if(inpDoASR == 0 && inpEvent.inpSMSNumber.length > 9 && startdate >= currentdate)
				// 													{
				// 														bootbox.dialog({
				// 															message: '<h4 class="be-modal-title">WARNING REMINDER</h4><p>Do you want to schedule a reminder?</p>',
				// 															title: '&nbsp;',
				// 															className: "be-modal",
				// 															buttons: {
				// 																success: {
				// 																	label: "Yes",
				// 																	className: "btn btn-medium green-gd",
				// 																	callback: function(result) {
				// 																		//SaveCalendarEvent(inpEvent, 1);
				// 																		queueStatus = 1;
				// 																		SaveCalendarEvent(inpEvent, queueStatus);
				// 																	}
				// 																},
				// 																cancel: {
				// 																	label: "NO",
				// 																	className: "green-cancel",
				// 																	callback: function(result) {
				// 																		//SaveCalendarEvent(inpEvent, 0);
				// 																		queueStatus = 0;
				// 																		SaveCalendarEvent(inpEvent, queueStatus);
				// 																	}
				// 																},
				// 															}
				// 														});
				// 													}
				// 													else{
				// 														queueStatus = inpDoASR;
				// 														SaveCalendarEvent(inpEvent, queueStatus);
				// 													}	
				// 												}else{
				// 													queueStatus = 1;
				// 													SaveCalendarEvent(inpEvent, queueStatus);
				// 												}
				// 												// // check valid input
				// 												// var checkValid = checkValidInput(d);
				// 												// if(checkValid === 1)
				// 												// SaveCalendarEvent(inpEvent, queueStatus);
				// 											}else{
				// 												if(d.ERRMESSAGE != "")	
				// 													alertBox("Get event info fail.","ERROR");
				// 											}												
				// 										} 	
				// 									});	
				// 								// }												
				// 							} 	
				// 						});	
				// 					}

				// 			(($(this).parents(".popover").popover('hide').data('bs.popover')||{}).inState||{}).click = false  // fix for BS 3.3.6
				// 			//$('.popover').remove();
						
				// 		}												
				// 	} 	
				// });
			//
				SaveCalendarEventNew(inpEvent)
			}catch(ex){
				alertBox(ex);
			}

	    });
				
		$(document).on("click", ".popover .close, .popover .btn-cancel-event" , function(){
	        
	        (($(this).parents(".popover").popover('hide').data('bs.popover')||{}).inState||{}).click = false  // fix for BS 3.3.6
	        
	        $('#calendar').fullCalendar('unselect'); 
	        
	    });
		
		$(document).on('click', ".btn-resend-event", function(){
					<!--- Save the event to the Calendar --->
					var Today = $(this).parents(".popover-content").find('.event-bubble').attr('data-date');
					var Endday = $(this).parents(".popover-content").find('.event-bubble').attr('data-date');
					var inpEvent = $(this).parents(".popover-content").find('.event-bubble').data('data-event-obj');
					var SaveOrUpdate = 0; // 0 - Add new; 1 - Update
					// Don't edit 2: - Sent ; 3: - Accepted; 4:- Declined; 5:- Change Request

					if(inpEvent != 'undefined' && inpEvent != null)
					{
						<!--- Note: inpEvent.start is required in all events--->
						Today = $.fullCalendar.formatDate(inpEvent.start, 'YYYY-MM-DD');
						Endday = $.fullCalendar.formatDate(inpEvent.end, 'YYYY-MM-DD');		

						if($(this).parents(".popover-content").find('.TimeStepStart').val().length > 0)
						{					
							// inpEvent.inpStart = Today + ' ' + $(this).parents(".popover-content").find('.TimeStepStart').val();

							inpEvent.inpStart = $(this).parents(".popover-content").find('.datetimestart').val() + ' ' + $(this).parents(".popover-content").find('.TimeStepStart').val();
							
							<!--- Convert to Moments --->						
							var s = moment(inpEvent.inpStart, 'YYYY-MM-DD HH:mm A');    
							inpEvent.start = s; // s.format("YYYY-MM-DDTHH:mm:ss");
						}
						else
						{
							inpEvent.start = null;
							inpEvent.inpStart = '';
						}
						
						if($(this).parents(".popover-content").find('.TimeStepEnd').val().length > 0)
						{
							// inpEvent.inpEnd = Endday + ' ' + $(this).parents(".popover-content").find('.TimeStepEnd').val();	
							//inpEvent.inpEnd = $(this).parents(".popover-content").find('.datetimeend').val() + ' ' + $(this).parents(".popover-content").find('.TimeStepEnd').val();
							inpEvent.inpEnd = $(this).parents(".popover-content").find('.datetimestart').val() + ' ' + $(this).parents(".popover-content").find('.TimeStepEnd').val();	
						
							<!--- Convert to Moments --->
							var e = moment(inpEvent.inpEnd, 'YYYY-MM-DD HH:mm A'); 
							inpEvent.end = e; // e.format("YYYY-MM-DDTHH:mm:ss");
						}
						else
						{
							inpEvent.end = null;
							inpEvent.inpEnd = '';
						}
												
						inpEvent.inpTitle = $(this).parents(".popover-content").find('.inpTitle').val();
						inpEvent.title = $(this).parents(".popover-content").find('.inpTitle').val();
						
						inpEvent.inpAllDayFlag = 0;				
						inpEvent.inpURL = ''; 
						inpEvent.inpClassName = ''; 
						inpEvent.inpEditableFlag = 1; 
						inpEvent.inpStartEditableFlag = 1; 
						inpEvent.inpDurationEditableFlag = 1; 
						inpEvent.inpResourceEditableFlag = 1; 
						inpEvent.inpRendering = ''; 
						inpEvent.inpOverlapFlag = true; 
						inpEvent.inpConstraint = ''; 
						inpEvent.inpColor = ''; 
						inpEvent.inpBackgroundColor = ''; 
						inpEvent.inpBorderColor = ''; 
						inpEvent.inpTextColor = ''; 
						inpEvent.inpSMSNumber = $(this).parents(".popover-content").find('.inpSMSNumber').val();
						inpEvent.inpeMail = ''; 
						inpEvent.inpNotes = ''; 
						inpEvent.inpConfirmationFlag = $(this).parents(".popover-content").find('#inpConfirmationFlag').val();
									
					}   
					else
					{		    	
						inpEvent = new Object();
						
						inpEvent.id = '';
						inpEvent.inpStart = $(this).parents(".popover-content").find('.datetimestart').val() + ' ' + $(this).parents(".popover-content").find('.TimeStepStart').val();
						inpEvent.inpEnd = $(this).parents(".popover-content").find('.datetimestart').val() + ' ' + $(this).parents(".popover-content").find('.TimeStepEnd').val();	
						<!--- Convert to Moments --->	

						var s = moment(inpEvent.inpStart, 'YYYY-MM-DD HH:mm A');    
						inpEvent.start = s.format("YYYY-MM-DDTHH:mm:ss");
						
						var e = moment(inpEvent.inpEnd, 'YYYY-MM-DD HH:mm A'); 
						
						inpEvent.end = e.format("YYYY-MM-DDTHH:mm:ss");
							
						inpEvent.inpTitle = $(this).parents(".popover-content").find('.inpTitle').val();
						inpEvent.title = $(this).parents(".popover-content").find('.inpTitle').val();
						
						inpEvent.inpAllDayFlag = 0;				
						inpEvent.inpURL = ''; 
						inpEvent.inpClassName = ''; 
						inpEvent.inpEditableFlag = 1; 
						inpEvent.inpStartEditableFlag = true; 
						inpEvent.inpDurationEditableFlag = true; 
						inpEvent.inpResourceEditableFlag = true; 
						inpEvent.inpRendering = ''; 
						inpEvent.inpOverlapFlag = true; 
						inpEvent.inpConstraint = ''; 
						inpEvent.inpColor = ''; 
						inpEvent.inpBackgroundColor = ''; 
						inpEvent.inpBorderColor = ''; 
						inpEvent.inpTextColor = ''; 
						inpEvent.inpSMSNumber = $(this).parents(".popover-content").find('.inpSMSNumber').val();
						inpEvent.inpeMail = ''; 
						inpEvent.inpNotes = ''; 
						inpEvent.inpConfirmationFlag = $(this).parents(".popover-content").find('#inpConfirmationFlag').val();	
						inpEvent.inpConfirmationOneDTSId = '0';
					}
			// try{
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url: '/session/sire/models/cfc/calendar.cfc?method=ResendReminderByEventId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true' ,   
					dataType: 'json',
					async: true,
					data:  
					{ 			
						inpCalendarId : '<cfoutput>#cid#</cfoutput>',
						inpEvent : JSON.stringify(inpEvent),
						inpASR: 1
						// inpEvent : inpEvent								
					},	
					beforeSend: function(){
						$('#processingPayment').show();
					},				  
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						$('#processingPayment').hide();
						<!--- No result returned ---> alertBox("No Response from the remote server. Check your connection and try again.","ERROR"); 
					},					  
					success:		
					<!--- Default return function for call back --->
					function(d) 
					{	
						$('#processingPayment').hide();																																							
						<!--- RXRESULTCODE is 1 if everything is OK --->
						if (parseInt(d.RXRESULTCODE) == 1) 
						{			
							<!---
								ConfirmationFlag States
								0 - No reminder sent (Hide)
								1 - Scheduled
								2 - Accepted
								3 - Declined
								4 - Change Request
								5 - Reminder Error - Bad SMS, eMail or other error (Hide)
								6 - Off
								7 - Sent
								8 - Chat Active
								9 - Opt Out	
								10 - Chat Close	          	
							--->
							
							switch(parseInt(inpEvent.inpConfirmationFlag)) 
							{
							case <cfoutput>#APPT_REMINDER_NOT_SENT#</cfoutput>:
								inpEvent.backgroundColor = "#3a87ad";
								inpEvent.borderColor = "#0000aa";
								break;
							
							case <cfoutput>#APPT_REMINDER_QUEUED#</cfoutput>:
								inpEvent.backgroundColor = "#C8BFE7";
								inpEvent.borderColor = "#0000aa";
								break;
							
							case <cfoutput>#APPT_REMINDER_ACCEPTED#</cfoutput>:
								inpEvent.backgroundColor = "#83dd8f";
								inpEvent.borderColor = "#00aa00";
								inpEvent.textColor = "#fff";
								break;
							
							case <cfoutput>#APPT_REMINDER_DECLINED#</cfoutput>:
								inpEvent.backgroundColor = "#ad433a";
								inpEvent.borderColor = "#aa0000";
								break;
							
							case <cfoutput>#APPT_REMINDER_CHANGE_REQUEST#</cfoutput>:
								inpEvent.backgroundColor = "#000000";
								inpEvent.borderColor = "#0000aa";
								break;
		
							case <cfoutput>#APPT_REMINDER_ERROR#</cfoutput>:
								inpEvent.backgroundColor = "#dd8683";
								inpEvent.borderColor = "#0000aa";
								break;
		
							case <cfoutput>#APPT_REMINDER_DO_NOT_SEND#</cfoutput>:
								inpEvent.backgroundColor = "#3a87ad";
								inpEvent.borderColor = "#0000aa";
								break;
							
							case <cfoutput>#APPT_REMINDER_SENT#</cfoutput>:
								inpEvent.backgroundColor = "#008B00";
								inpEvent.borderColor = "#0000aa";
								break;
							case <cfoutput>#APPT_REMINDER_CHAT_ACTIVE#</cfoutput>:
								inpEvent.backgroundColor = "#0000ff";
								inpEvent.borderColor = "#0000aa";
								break;
							case <cfoutput>#APPT_REMINDER_OPT_OUT#</cfoutput>:
								inpEvent.backgroundColor = "#dd8683";
								inpEvent.borderColor = "#0000aa";
								break;
							case <cfoutput>#APPT_REMINDER_CHAT_CLOSE#</cfoutput>:
								inpEvent.backgroundColor = "#999999";
								inpEvent.borderColor = "#0000aa";
								break;
			
							default:
								inpEvent.backgroundColor = "#3a87ad";
								inpEvent.borderColor = "#0000aa";
							}
							$('#calendar').fullCalendar('updateEvent', inpEvent);

							alertBox("Resend reminder successfully.","Resend Reminder");		
						}
						else
						{
							<!--- No result returned --->	
							if(d.ERRMESSAGE != "")	
								alertBox(d.ERRMESSAGE,"Resend Reminder");
						}												
					} 		
						
				});
		});

		$(document).on('click', ".respone", function(){
			try{
				$.ajax({
					type: "GET",
					url: '/session/sire/models/cfm/calendar_respone_preview.cfm?inpcontactstring='+data_event_obj.inpSMSNumber+'&inpcontactqueueId='+ data_event_obj.inpConfirmationOneDTSId +'&inpbatchId=<cfoutput>#inpbatchId#</cfoutput>',
					beforeSend: function( xhr ) {
						$('#processingPayment').show();
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						$('#processingPayment').hide();
						bootbox.dialog({
							message: "Get respone Fail",
							title: "Calendar Respone",
							buttons: {
								success: {
									label: "Ok",
									className: "btn btn-medium btn-success-custom",
									callback: function() {}
								}
							}
						});
					},
					success:function(d){
						$('#processingPayment').hide();
						$('#previewCampaignModal .modal-body').html(d);
						$('#previewCampaignModal').modal('show');
					}
				});
			}catch(ex){
				$('#processingPayment').hide();
				bootbox.dialog({
					message: "Get respone Fail",
					title: "Calendar Respone",
					buttons: {
						success: {
							label: "Ok",
							className: "btn btn-medium btn-success-custom",
							callback: function() {}
						}
					}
				});
			}

		});
		// FILTER CALENDAR CHECKBOX
		$(document).on('click', "#calendar_left input[type=checkbox]", function(){
			// Get list status filter
			
			var checkedBoxList = $( "#calendar_left input:checked" );
			var checkBoxSelectedItems = new Array();
			for (var i = 0; i < checkedBoxList.length; i++) {                                                          
					if (checkedBoxList[i].checked) {                                                                       
						checkBoxSelectedItems.push(checkedBoxList[i].value);                               
					}                                                                                                     
				} 
			var FilterCalendarList = checkBoxSelectedItems.toString();
			
			// Save list status filter
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/calendar.cfc?method=SaveConfigFilterCalendar&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				async: true,
				data:  
				{ 
					inpCalendarId : '<cfoutput>#cid#</cfoutput>',
					inpFilterCalendar : FilterCalendarList
								
				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
				success:		
				<!--- Default return function for call back --->
				function(d) {																																		
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1){
							//console.log(d);
							// reload data calendar follow filter
							$('#calendar').fullCalendar('refetchEvents');
					}else{
							if(d.ERRMESSAGE != "")	
								alertBox("Error while saving changes to Auto Schedule Reminder setting","ERROR");
					}												
				} 	
			});	

		});

		$('#Calendar-Event-Modal').on('show.bs.modal', function (e) {
		   // do something...
		   	SkipCheckOptOut = 0;
			SkipCheckSchedule = 0;
		    var s = null;
			var e = null;
			var ds = null;
		   	var NewObj = $(this);	
		   	NewObj.find('.Select2').select2( { theme: "bootstrap"} );

			NewObj.find('.event-bubble').attr('data-date', data_date);
		    NewObj.find('.event-bubble').attr('data-event-action', data_event_action);

		    if(data_event_obj){ 

				NewObj.find('#inpConfirmationFlag').val(data_event_obj.inpConfirmationFlag).trigger("change");	
				NewObj.find('.inpSMSNumber').val(data_event_obj.inpSMSNumber);
				if(data_event_obj.start != undefined){
					s = data_event_obj.start.format('HH:mm A');
					ds = data_event_obj.start.format('YYYY-MM-DD');
				}
				if(data_event_obj.end != undefined){   
					e = data_event_obj.end.format('HH:mm A');	  
		   		 }
				//show time
				NewObj.find('.TimeStepStart').val(s);
				NewObj.find('.TimeStepEnd').val(e);	
				//show date
				NewObj.find('.datetimestart').val(ds);
		   	}else{

				var ClickTimeStart = selectAllDate.format('hh:mm A');
				var returned_endate = selectAllDate.add(30, 'minutes');
				var ClickTimeEnd = returned_endate.format('hh:mm A');
				var ClickDateStart = selectAllDate.format('YYYY-MM-DD');

		    	NewObj.find('.inpSMSNumber').val('');
				//show time
				NewObj.find('.TimeStepStart').val(ClickTimeStart);
				NewObj.find('.TimeStepEnd').val(ClickTimeEnd);	
				//show date
				NewObj.find('.datetimestart').val(ClickDateStart);
		    }

		    NewObj.find('.event-bubble').data('data-event-obj', data_event_obj);	
		    NewObj.find('.inpTitle').val(data_title);
		
			<!--- If US phone numbers put on some helpful formatting --->
			// <cfif RetValGetCalendarConfiguration.PHONEFORMAT EQ 1> 
			NewObj.find('.inpSMSNumber').mask("(000) 000-0000");
			// </cfif>
		    NewObj.find('.TimeStepStart').timepicker({ 	
				'step': 15, 
				'scrollDefault': '9:00 AM',
				'timeFormat': 'h:i A'			   							
			});   						
			NewObj.find('.TimeStepEnd').timepicker({ 	
				'step': 15, 
				'scrollDefault': '9:00 AM',
				'timeFormat': 'h:i A'			   							
			}); 				
		});

	    $('#Calendar-Event-Modal').on('hidden.bs.modal', function (e) {
		  // do something...
		   $('#calendar').fullCalendar('unselect'); 
		});
	    
	    <!--- Details... close any popovers on window resize - so they dont move about relative to their parent --->
	    $(window).resize(function () {
		    
		    <!--- Hide any open popovers --->
			$('[data-toggle="popover"],[data-original-title]').each(function () {		        		                  
	            (($(this).popover('hide').data('bs.popover')||{}).inState||{}).click = false  // fix for BS 3.3.6
	       	});
		});
			
	    $( "#Start_dt" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
		$( "#Start_dt" ).datepicker('setDate', InitExpire);
	     
	});	
	
	
	function BindReminderLink(inpObj)
	{
		<!--- $('.reminder-link') --->
		inpObj.on("click touchstart", function(e){
		   	
		    <!--- If not already active --->
		    if(!$(this).closest('.event-bubble').find('.reminder-link').hasClass("event-link-active"))
		    {			
				$(this).closest('.event-bubble').find('.reminder-group').show();
				$(this).closest('.event-bubble').find('.event-group').hide();
				$(this).closest('.event-bubble').find('.chat-group').hide();
		        
			    $(this).closest('.event-bubble').find('.reminder-link').addClass("event-link-active");
			    $(this).closest('.event-bubble').find('.event-link').removeClass("event-link-active");
				$(this).closest('.event-bubble').find('.chat-link').removeClass("event-link-active");
		    }
		    
		    <!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();
			
		});		
	}
	
	function BindEventLink(inpObj)
	{
		<!--- $('.event-link') --->
		inpObj.on("click touchstart", function(e){
		    
		    <!--- If not already active --->
		    if(!$(this).closest('.event-bubble').find('.event-link').hasClass("event-link-active"))
		    {
		   		$(this).closest('.event-bubble').find('.reminder-group').hide();
				$(this).closest('.event-bubble').find('.event-group').show();
				$(this).closest('.event-bubble').find('.chat-group').hide();
			    $(this).closest('.event-bubble').find('.reminder-link').removeClass("event-link-active");
			    $(this).closest('.event-bubble').find('.event-link').addClass("event-link-active");
				$(this).closest('.event-bubble').find('.chat-link').removeClass("event-link-active");
		     
		    }  
		    <!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();
			
		});
	}

	function BindChatLink(inpObj)
	{
		<!--- $('.event-link') --->
		inpObj.on("click touchstart", function(e){
		    
		    <!--- If not already active --->
		    if(!$(this).closest('.event-bubble').find('.chat-link').hasClass("event-link-active"))
		    {
				$(this).closest('.event-bubble').find('.chat-group').show();
		   		$(this).closest('.event-bubble').find('.reminder-group').hide();
				$(this).closest('.event-bubble').find('.event-group').hide();
				$(this).closest('.event-bubble').find('.chat-link').addClass("event-link-active");
			    $(this).closest('.event-bubble').find('.reminder-link').removeClass("event-link-active");
			    $(this).closest('.event-bubble').find('.event-link').removeClass("event-link-active");
		     
		    }
		     	    
		    <!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();
			
		});
	}

	function AddCalendarEvent(inpEvent, inpDoASR, inpoptout)
	{							
		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->		
		inpEvent.source = null;	
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/calendar.cfc?method=AddEvent&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true' ,   
			dataType: 'json',
			async: true,
			data:  
			{ 			
				inpCalendarId : '<cfoutput>#cid#</cfoutput>',
				inpEvent : JSON.stringify(inpEvent),
				inpASR: inpDoASR							
			},
			beforeSend: function () {
                $("#processingPayment").show();
            },				  
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				$("#processingPayment").hide();
				<!--- No result returned ---> alertBox("No Response from the remote server. Check your connection and try again.","ERROR"); 
			},					  
			success:		
			<!--- Default return function for call back --->
			function(d) 
			{	
				$("#processingPayment").hide();																																							
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (parseInt(d.RXRESULTCODE) == 1) 
				{								
					if(parseInt(d.EVENTID) > 0)
					{	
						//Update status for Event with Phone number optout 
						if(inpoptout == 1){
							$.ajax({
								type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
								url: '/session/sire/models/cfc/calendar.cfc?method=UpdateEventStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
								dataType: 'json',
								async: true,
								data:  
								{ 
									inpEventId : d.EVENTID,
									inpEventStatus: <cfoutput>#APPT_REMINDER_OPT_OUT#</cfoutput>
								},beforeSend: function( xhr ) {
									$('#processingPayment').show();
								},
								error: function(XMLHttpRequest, textStatus, errorThrown) {
									$('#processingPayment').hide();
									alertBox("No Response from the remote server. Check your connection and try again.","ERROR"); 
								},
								success:function(d){
									$('#processingPayment').hide();
									if(d.RXRESULTCODE !=1){	
									}
								}
							});
						}
					}

					location.reload();
		
				}
				else
				{
					<!--- No result returned --->	
					if(d.ERRMESSAGE != "")	
						bootbox.dialog({
							message: d.ERRMESSAGE,
							title: "Create Event",
							buttons: {
								success: {
									label: "Ok",
									className: "btn btn-medium btn-success-custom",
									callback: function() {
										location.reload();
									}
								}
							}
						});
				}												
			}
		});											
	}	
	
	function DeleteCalendarEvent(inpEvent)
	{				
		<!--- Watch out for circular reference - remove the reference to itself! Dont need it to write to DB --->				
		inpEvent.source = null;
					
		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->				
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/calendar.cfc?method=DeleteEvent&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true' ,   
			dataType: 'json',
			async: true,
			data:  
			{ 			
				inpCalendarId : '<cfoutput>#cid#</cfoutput>',
				inpEvent : JSON.stringify(inpEvent)							
			},	
			beforeSend: function(){
				$('#processingPayment').show();
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
				$('#processingPayment').hide();
				<!--- No result returned ---> alertBox("No Response from the remote server. Check your connection and try again.","ERROR");
			},					  
			success:		
			<!--- Default return function for call back --->
			function(d) 
			{		
				$('#processingPayment').hide();																																						
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (parseInt(d.RXRESULTCODE) == 1) 
				{								
					$('#calendar').fullCalendar('removeEvents',inpEvent.id);
				}
				else
				{
					<!--- No result returned --->	
					if(d.ERRMESSAGE != "")	
						bootbox.alert(d.ERRMESSAGE);
				}												
			} 		
				
		});				
													
	}	
	
	function SaveCalendarEvent(inpEvent, inpDoASR)
	{				
		<!--- Watch out for circular reference - remove the reference to itself! Dont need it to write to DB --->				
		inpEvent.source = null;
												
		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->				
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/calendar.cfc?method=SaveEvent&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true' ,   
			dataType: 'json',
			async: true,
			data:  
			{ 			
				inpCalendarId : '<cfoutput>#cid#</cfoutput>',
				inpEvent : JSON.stringify(inpEvent),
				inpASR: inpDoASR
				// inpEvent : inpEvent								
			},	
			beforeSend: function(){
				$('#processingPayment').show();
			},				  
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
				$('#processingPayment').hide();
				<!--- No result returned ---> alertBox("No Response from the remote server. Check your connection and try again.","ERROR"); 
			},					  
			success:		
			<!--- Default return function for call back --->
			function(d) 
			{	
				$('#processingPayment').hide();																																							
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (parseInt(d.RXRESULTCODE) == 1) 
				{			
					<!---
						ConfirmationFlag States
						0 - No reminder sent (Hide)
						1 - Scheduled
						2 - Accepted
						3 - Declined
						4 - Change Request
						5 - Reminder Error - Bad SMS, eMail or other error (Hide)
						6 - Off
						7 - Sent
						8 - Chat Active
						9 - Opt Out
						10 - Chat Close	          	
					--->
		          	switch(parseInt(inpEvent.inpConfirmationFlag)) 
		          	{
						case <cfoutput>#APPT_REMINDER_NOT_SENT#</cfoutput>:
							inpEvent.backgroundColor = "#3a87ad";
							inpEvent.borderColor = "#0000aa";
							break;
						
						case <cfoutput>#APPT_REMINDER_QUEUED#</cfoutput>:
							inpEvent.backgroundColor = "#C8BFE7";
							inpEvent.borderColor = "#0000aa";
							break;
						
						case <cfoutput>#APPT_REMINDER_ACCEPTED#</cfoutput>:
							inpEvent.backgroundColor = "#83dd8f";
							inpEvent.borderColor = "#00aa00";
							inpEvent.textColor = "#fff";
							break;
						
						case <cfoutput>#APPT_REMINDER_DECLINED#</cfoutput>:
							inpEvent.backgroundColor = "#ad433a";
							inpEvent.borderColor = "#aa0000";
							break;
						
						case <cfoutput>#APPT_REMINDER_CHANGE_REQUEST#</cfoutput>:
							inpEvent.backgroundColor = "#000000";
							inpEvent.borderColor = "#0000aa";
							break;
	
						case <cfoutput>#APPT_REMINDER_ERROR#</cfoutput>:
							inpEvent.backgroundColor = "#dd8683";
							inpEvent.borderColor = "#0000aa";
							break;
	
						case <cfoutput>#APPT_REMINDER_DO_NOT_SEND#</cfoutput>:
							inpEvent.backgroundColor = "#3a87ad";
							inpEvent.borderColor = "#0000aa";
							break;
						
						case <cfoutput>#APPT_REMINDER_SENT#</cfoutput>:
							inpEvent.backgroundColor = "#008B00";
							inpEvent.borderColor = "#0000aa";
							break;
						case <cfoutput>#APPT_REMINDER_CHAT_ACTIVE#</cfoutput>:
							inpEvent.backgroundColor = "#0000ff";
							inpEvent.borderColor = "#0000aa";
							break;
						case <cfoutput>#APPT_REMINDER_OPT_OUT#</cfoutput>:
							inpEvent.backgroundColor = "#dd8683";
							inpEvent.borderColor = "#0000aa";
							break;
						case <cfoutput>#APPT_REMINDER_CHAT_CLOSE#</cfoutput>:
							inpEvent.backgroundColor = "#999999";
							inpEvent.borderColor = "#0000aa";
							break;
		
						default:
							inpEvent.backgroundColor = "#3a87ad";
							inpEvent.borderColor = "#0000aa";
					}
					$('#calendar').fullCalendar('updateEvent', inpEvent);
					// Hiden Popup when update successfullys
					// Reload after Edit Event
					$('.popover').popover('hide');
					$('#calendar').fullCalendar('refetchEvents');
				}
				else
				{
					<!--- No result returned --->	
					if(d.ERRMESSAGE != "")	
						alertBox(d.ERRMESSAGE,"Edit Event");
					$('.popover').popover('hide');
					$('#calendar').fullCalendar('refetchEvents');
				}												
			} 		
				
		});												
	}	

	$(document).on("click", ".input-group-addon" , function(){ 
		$('.ui-timepicker-wrapper').hide();
	});	

	$('#chat-modal').on('hidden.bs.modal', function (e) {
		// do something...
		//console.log(data_event_obj);
		$('#Calendar-Event-Modal').modal('hide');
		$('.popover').popover('hide');
		$('#calendar').fullCalendar('refetchEvents');		
	});

	$('#chat-modal').on('show.bs.modal', function (e) {
		// do something...
		// Change Event statu to Chat Active 
		$('#processingPayment').show();
		console.log(data_event_obj);
		$('#chat-modal').find("#event_phonenumber_value").text(data_event_obj.inpSMSNumber);
		$('#chat-modal').find("#event_phonenumber_value").mask("(000) 000-0000");
		//$('#chat-modal').find("#inpConfirmationFlag").val(Event_chatstatus).trigger("change");
		// Get info chat header
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/calendar.cfc?method=GetInfoChatActiveCalendar&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: true,
			data:  
			{ 
				inpEventId : data_event_obj.id,
				inpPhonenumber: data_event_obj.inpSMSNumber

			},beforeSend: function( xhr ) {
				$('#processingPayment').show();
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				$('#processingPayment').hide();
				alertBox("No Response from the remote server. Check your connection and try again.","ERROR"); 
			},
			success:function(d){
				//$('#processingPayment').hide();
				$('#chat-modal').find("#event_first_name").hide();
				$('#chat-modal').find("#event_last_name").hide();
				$('#chat-modal').find("#event_name").hide();
				$('#chat-modal').find("#event_start").hide();
				$('#chat-modal').find("#event_end").hide();
				if(d.RXRESULTCODE ==1){
					if(d.FIRSTNAME != ''){
						$('#chat-modal').find("#event_first_name").show();
						$('#chat-modal').find("#event_first_name_value").text(d.FIRSTNAME);
						// display firt name
					}
					if(d.LASTNAME != ''){
						$('#chat-modal').find("#event_last_name").show();
						$('#chat-modal').find("#event_last_name_value").text(d.LASTNAME);
						// display last name
					}
					if(d.EVENT != ''){
						$('#chat-modal').find("#event_name").show();
						$('#chat-modal').find("#event_name_value").text(d.EVENT);
						// display event name
					}
					if(d.EVENTSTART != ''){
						$('#chat-modal').find("#event_start").show();
						$('#chat-modal').find("#event_start_value").text(d.EVENTSTART);
						// display event start
					}
					if(d.EVENTEND != ''){
						$('#chat-modal').find("#event_end").show();
						$('#chat-modal').find("#event_end_value").text(d.EVENTEND);
						// display event end
					}
					$('#chat-modal').find(".event_chat_notes").val(d.CHATNOTES);
					
					if(d.CHATSTATUS == 4){
						$('#chat-modal').find(".btn-endchat").prop('disabled', true);
					}else{
						$('#chat-modal').find(".btn-endchat").prop('disabled', false);
					}
				}
				
			}
		});

		setTimeout(function () {
			$('#processingPayment').hide();
		}, 4000);

	});

	function checkValidStatus(oldevalue,newvalue){
		var result = 1;
		if(oldevalue == 1 && newvalue != 1)
		{
			alertBox("This reminder is SCHEDULED. You can not change status","WARNING");
			result = 0;
		}
		else if(oldevalue == 2 && newvalue != 2)
		{
			if(newvalue == 1 || newvalue == 8 || newvalue == 9 || newvalue == 6){
				alertBox("This reminder is ACCEPTED. You can not change to this status.","WARNING");
				result = 0;
			}
			
		}
		else if(oldevalue == 3 && newvalue != 3)
		{
			if(newvalue == 1 || newvalue == 8 || newvalue == 9 || newvalue == 6){
				alertBox("This reminder is DECLINED. You can not change to this status.","WARNING");
				result = 0;
			}
		}
		else if(oldevalue == 4 && newvalue != 4)
		{
			if(newvalue == 1 || newvalue == 8 || newvalue == 9 || newvalue == 6){
				alertBox("This reminder is CHANGE REQUEST. You can not change to this status.","WARNING");
				result = 0;
			}
		}
		else if(oldevalue == 6 && newvalue != 6) 
		{
			if(newvalue == 1 || newvalue == 8 || newvalue == 9){
				alertBox("This reminder is OFF. You can not change to this status.","WARNING");
				result = 0;
			}
		}
		else if(oldevalue == 7 && newvalue != 7) 
		{
			if(newvalue == 1 || newvalue == 8 || newvalue == 9 || newvalue == 6){
				alertBox("This reminder is SENT. You can not change to this status.","WARNING");
				result = 0;
			}
		}
		else if(oldevalue == 8 && newvalue != 8)
		{
			if(newvalue == 1 || newvalue == 9 || newvalue == 6){
				alertBox("This reminder is CHAT ACTIVE. You can not change to this status.","WARNING");
				result = 0;
			}
		}
		else if(oldevalue == 9 && newvalue != 9)
		{
			if(newvalue == 1 || newvalue == 8 || newvalue == 6){
				alertBox("This reminder is OPT OUT. You can not change to this status.","WARNING");
				result = 0;
			}
		}
		else if(oldevalue == 10 && newvalue != 10)
		{
			if(newvalue == 1 || newvalue == 9 || newvalue == 6){
				alertBox("This reminder is CHAT CLOSE. You can not change to this status.","WARNING");
				result = 0;
			}
		}
		return result;
	}

	$(document).on("change", ".popover-content #inpConfirmationFlag" , function(){ 
		try{
			if(data_event_obj){
				if(checkValidStatus(data_event_obj.inpConfirmationFlag,this.value) == 1){
					$.ajax({
						type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
						url: '/session/sire/models/cfc/calendar.cfc?method=UpdateEventStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
						dataType: 'json',
						async: true,
						data:  
						{ 
							inpEventId : data_event_obj.id,
							inpEventStatus: this.value
						},beforeSend: function( xhr ) {
							$('#processingPayment').show();
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							$('#processingPayment').hide();
							alertBox("No Response from the remote server. Check your connection and try again.","ERROR"); 
						},
						success:function(d){
							$('#processingPayment').hide();
							if(d.RXRESULTCODE ==1){
								data_event_obj.inpConfirmationFlag = d.RXRESULTSTATUS;
								if(d.RXRESULTSTATUS == <cfoutput>#APPT_REMINDER_ACCEPTED#</cfoutput> || d.RXRESULTSTATUS == <cfoutput>#APPT_REMINDER_DECLINED#</cfoutput> || d.RXRESULTSTATUS == <cfoutput>#APPT_REMINDER_CHANGE_REQUEST#</cfoutput> || d.RXRESULTSTATUS == <cfoutput>#APPT_REMINDER_SENT#</cfoutput> || d.RXRESULTSTATUS == <cfoutput>#APPT_REMINDER_CHAT_ACTIVE#</cfoutput>){
									$('.popover-content').find('.inpSMSNumber').attr("disabled","disabled");
									$('.popover-content').find('.inpTitle').attr("disabled","disabled");
									$('.popover-content').find('.datetimestart').attr("disabled","disabled");
									$('.popover-content').find('.TimeStepEnd').attr("disabled","disabled");
									$('.popover-content').find('.TimeStepStart').attr("disabled","disabled");
									$('.popover-content').find('.btn-save-event').attr("disabled","disabled");

								}else{
									$('.popover-content').find('.inpSMSNumber').removeAttr("disabled");
									$('.popover-content').find('.inpTitle').removeAttr("disabled");
									$('.popover-content').find('.datetimestart').removeAttr("disabled");
									$('.popover-content').find('.TimeStepEnd').removeAttr("disabled");
									$('.popover-content').find('.TimeStepStart').removeAttr("disabled");
									$('.popover-content').find('.btn-save-event').removeAttr("disabled");
								}
								//alertBox(d.MESSAGE,"WARRING!");
								$('#calendar').fullCalendar('refetchEvents');	
							}else{
								alertBox(d.MESSAGE,"WARRING!");
							}
						}
					});
				}else{
					$('.popover-content').find('#inpConfirmationFlag').val(data_event_obj.inpConfirmationFlag).trigger("change");
				}
			}
		}catch(ex){
			//console.log(ex);			
		}
	});

	$(document).on("change", "#Calendar-Event-Modal #inpConfirmationFlag" , function(){ 
		//alert($('#Calendar-Event-Modal').find("#inpConfirmationFlag").val());
		try{
			if(data_event_obj){
				if(checkValidStatus(data_event_obj.inpConfirmationFlag,this.value) == 1){
					$.ajax({
						type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
						url: '/session/sire/models/cfc/calendar.cfc?method=UpdateEventStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
						dataType: 'json',
						async: true,
						data:  
						{ 
							inpEventId : data_event_obj.id,
							inpEventStatus: this.value
						},beforeSend: function( xhr ) {
							$('#processingPayment').show();
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							$('#processingPayment').hide();
							alertBox("No Response from the remote server. Check your connection and try again.","ERROR"); 
						},
						success:function(d){
							$('#processingPayment').hide();
							if(d.RXRESULTCODE ==1){
								data_event_obj.inpConfirmationFlag = d.RXRESULTSTATUS;
								if(d.RXRESULTSTATUS == <cfoutput>#APPT_REMINDER_ACCEPTED#</cfoutput> || d.RXRESULTSTATUS == <cfoutput>#APPT_REMINDER_DECLINED#</cfoutput> || d.RXRESULTSTATUS == <cfoutput>#APPT_REMINDER_CHANGE_REQUEST#</cfoutput> || d.RXRESULTSTATUS == <cfoutput>#APPT_REMINDER_SENT#</cfoutput> || d.RXRESULTSTATUS == <cfoutput>#APPT_REMINDER_CHAT_ACTIVE#</cfoutput>){
									$('#Calendar-Event-Modal').find('.inpSMSNumber').attr("disabled","disabled");
									$('#Calendar-Event-Modal').find('.inpTitle').attr("disabled","disabled");
									$('#Calendar-Event-Modal').find('.datetimestart').attr("disabled","disabled");
									$('#Calendar-Event-Modal').find('.TimeStepEnd').attr("disabled","disabled");
									$('#Calendar-Event-Modal').find('.TimeStepStart').attr("disabled","disabled");
									$('#Calendar-Event-Modal').find('.btn-save-event').attr("disabled","disabled");
								}else{
									$('#Calendar-Event-Modal').find('.inpSMSNumber').removeAttr("disabled");
									$('#Calendar-Event-Modal').find('.inpTitle').removeAttr("disabled");
									$('#Calendar-Event-Modal').find('.datetimestart').removeAttr("disabled");
									$('#Calendar-Event-Modal').find('.TimeStepEnd').removeAttr("disabled");
									$('#Calendar-Event-Modal').find('.TimeStepStart').removeAttr("disabled");
									$('#Calendar-Event-Modal').find('.btn-save-event').removeAttr("disabled");
								}	
							}else{
								alertBox(d.MESSAGE,"WARRING!");
							}
							$('#calendar').fullCalendar('refetchEvents');
						}
					});
				}else{
					$('.popover-content').find('#inpConfirmationFlag').val(data_event_obj.inpConfirmationFlag).trigger("change");
				}
			}
		}catch(ex){
			//console.log(ex);
		}
	});

	$(document).on("change", "#chat-modal #inpConfirmationFlag" , function(){ 
		if(checkValidStatus(data_event_obj.inpConfirmationFlag,this.value) == 1){
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/calendar.cfc?method=UpdateEventStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				async: true,
				data:  
				{ 
					inpEventId : data_event_obj.id,
					inpEventStatus: this.value
				},beforeSend: function( xhr ) {
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alertBox("No Response from the remote server. Check your connection and try again.","ERROR"); 
				},
				success:function(d){
					if(d.RXRESULTCODE ==1){
						data_event_obj.inpConfirmationFlag = d.RXRESULTSTATUS;
						//alertBox(d.MESSAGE,"WARRING!");	
					}
				}
			});
		}else{
			$('#chat-modal').find('#inpConfirmationFlag').val(data_event_obj.inpConfirmationFlag).trigger("change");
		}
	});

	$(document).on("change", ".TimeStepStart" , function(){ 
		//var parsetimeStart = '2018-01-01 ' + $(this).parents(".popover-content").find('.TimeStepStart').val();		
		var parsetimeStart = "January 01, 2018 " + $(this).parents(".popover-content").find('.TimeStepStart').val();
		var d = new Date(parsetimeStart);		
		var startHour = d.getHours();
		var startMin = d.getMinutes();
		var isPM = parsetimeStart.toUpperCase().includes("PM");		
		var dd = d.setMinutes(d.getMinutes()+ 30);
		var ddd = new Date(dd); 

		if(ddd.getHours() > 12){
			var hoursEnd = ddd.getHours() - 12;
			var timenoon = 'PM';
		}else{
			var hoursEnd = ddd.getHours();
			var timenoon = 'AM';
		}

		if(ddd.getMinutes() == 0){
			var minutesEnd = '00';
		}else{
			var minutesEnd = ddd.getMinutes();
		}
		// case with StartTime between 11:30am to 11:45am
		if(startHour == 11 && (startMin >= 30 && startMin <= 59)){
			var timenoon = 'PM';
		}
		// case with StartTime between 12:00pm to 12:30pm
		if(startHour == 12 && (startMin >= 0 && startMin <= 29) && isPM == true){
			var timenoon = 'PM';
		}

		var parsetimeEnd = hoursEnd + ':' + minutesEnd + ' ' + timenoon;
		if($(this).parents(".popover-content").find('.TimeStepStart').val() == '11:30 PM' || $(this).parents(".popover-content").find('.TimeStepStart').val() == '11:45 PM'){
			$(this).parents(".popover-content").find('.TimeStepEnd').val('11:59 PM');
		}else{
			$(this).parents(".popover-content").find('.TimeStepEnd').val(parsetimeEnd);
		}
			
	});

	function SaveCalendarEventNew(Event)
	{    
	<!--- Watch out for circular reference - remove the reference to itself! Dont need it to write to DB --->    
	//inpEvent.source = null;   
	<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->    
		$.ajax({
		type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
		url: '/session/sire/models/cfc/calendar.cfc?method=validateEvent&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true' ,   
		dataType: 'json',
		async: false,
		cache:false,
		data:  
		{   
			inpEvent: JSON.stringify(Event),
			inpType: '0',
			inpSkipCheckOptOut: SkipCheckOptOut,
			inpSkipCheckSchedule: SkipCheckSchedule 
		}, 
		beforeSend: function(){
			$('#processingPayment').show();
		},      
		error: function(XMLHttpRequest, textStatus, errorThrown) { 
			$('#processingPayment').hide();
			<!--- No result returned ---> alertBox("No Response from the remote server. Check your connection and try again.","ERROR"); 
		},       
		success:  
		<!--- Default return function for call back --->
			function(d) 
			{ 
				$('#processingPayment').hide();                                       
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (parseInt(d.RXRESULTCODE) == 1) 
					{   
						bootbox.dialog({
							message: d.MESSAGE,
							title: "SAVE EVENT",
							buttons: {
								success: {
									label: "Ok",
									className: "btn btn-medium btn-success-custom",
									callback: function() {
										if(d.RELOAD == 1){
											location.reload();
										}
										else{
											$('#calendar').fullCalendar('refetchEvents');
										}
									}
								}
							}
						});
						$('.popover').popover('hide');
					}
				else if (parseInt(d.RXRESULTCODE) == 0) 
					{
						if(parseInt(d.TYPE) == 3 )
						{
							labelYes = 'Create event with schedule';
							labelNo = 'Create event without schedule';
						}
						else{
							labelYes = 'Yes';
							labelNo = 'No';
						}

						bootbox.dialog({
							message: d.MESSAGE,
							title: '&nbsp;',
							className: "be-modal",
							buttons: {
								success: {
									label: labelYes,
									className: "btn btn-medium green-gd",
									callback: function(result) {
										if(parseInt(d.TYPE) == 1){//check opt-out
											SkipCheckOptOut = 1;
										}else if(parseInt(d.TYPE) == 2){//schedule ON 
											SkipCheckSchedule = 1;
										}else if(parseInt(d.TYPE) == 3){//schedule OFF
											SkipCheckSchedule = 1;
										}
										SaveCalendarEventNew(Event);
									}
								},
								cancel: {
									label: labelNo,
									className: "green-cancel",
									callback: function(result) {
										if(parseInt(d.TYPE) == 3){//schedule OFF
											SkipCheckSchedule = 3;
											SaveCalendarEventNew(Event);
										}
									}
								},
							}
						});
					}
				else
				{
					<!--- No result returned ---> 
					if(d.ERRMESSAGE != "") 
					alertBox(d.ERRMESSAGE,"SAVE EVENT");
					$('.popover').popover('hide');
					$('#calendar').fullCalendar('refetchEvents');
				}            
			}	   
			
		});            
	}	

	function UpdateNoteschat(){
		$.ajax({
		type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
		url: '/session/sire/models/cfc/calendar.cfc?method=UpdatEventChatNotes&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true' ,   
		dataType: 'json',
		async: false,
		cache:false,
		data:  
		{   
			inpEventId: data_event_obj.id,
			inpNotes: $("#chat-modal").find(".event_chat_notes").val()
		}, 
		beforeSend: function(){
		},      
		error: function(XMLHttpRequest, textStatus, errorThrown) { 
			alertBox("No Response from the remote server. Check your connection and try again.","ERROR"); 
		},       
		success:  
		<!--- Default return function for call back --->
			function(d) 
			{ 
				$('#processingPayment').hide();                                       
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (parseInt(d.RXRESULTCODE) != 1) 
					{   
						alertBox(d.MESSAGE,'Update Notes');
					}        
			}	   
			
		});
	}

</script>	