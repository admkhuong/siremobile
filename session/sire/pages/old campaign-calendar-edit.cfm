<cfparam name="templateid" default="">
<cfparam name="campaignid" default="">
<cfparam name="adv" default="1">
<cfparam name="Tour" default="">
<cfparam name="inpTemplateFlag" default="0">
<cfparam name="swt" default="0"> <!--- Simplified Walk Through (swt) style - only ask one question at a time. Rely on template descriptions to guide the user through --->
<cfparam name="swt_desc" default="1">
<cfparam name="swt_keyword" default="1">
<cfparam name="swt_qa" default="1">
<cfparam name="blast" default="0"> 
<cfparam name="swt_aal" default="0"> 
<cfparam name="XMLCSEditable" default="false">
<cfparam name="customCampaign" default="0">
<cfparam name="calendar" default ="1"> <!--- 0: default campaign; 1: calendar campaign (Turn campaign into a template and let user only edit are questions and responds, all others are way to confusing, need to hide it. Also, remove the keyword.) --->


<!---
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" /> 
<link rel="stylesheet" href="/public/sire/assets/emoji-picker/lib/css/emoji.css" />
--->


<!--- tt is for Template Type 0 is either 1=keyword 2=blast --->
<cfparam name="tt" default="0"> 


<!--- Advanced overrides any Simplified Walk Through requests --->
<cfif adv EQ 1>
	<cfset swt = 0 />
</cfif>	 

<!--- <cfif swt EQ 1 ><cfset inpSimpleViewFlag = 0 /></cfif> --->

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/vendors/chartjs/Chart.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/filter_table.css">
</cfinvoke>

<cfinvoke method="GetUserInfor" component="public.sire.models.users" returnvariable="RevalGetUserInfor">
	 <cfinvokeargument name="inpiduser" value="#Session.USERID#">
</cfinvoke>

<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="GetAdminPermission">
	 <cfinvokeargument name="inpSkipRedirect" value="1">
</cfinvoke>

<cfif GetAdminPermission.ISADMINOK EQ "1">
	<cfset XMLCSEditable = true />
<cfelse>
	<cfif RevalGetUserInfor.ALLEDITXMLCS EQ "0">
		<cfset XMLCSEditable = false />
	<cfelse>
		<cfset XMLCSEditable = true />
	</cfif>
</cfif>

<!--- Verify System Admin only can do this for now - may add ability for user to edit their own custom templates later--->	
<cfif inpTemplateFlag NEQ 0>

	<!--- This will redirect to /session/sire/pages/admin-no-permission if currently logged in Sire user does not have permission to access Admin funtions otherwise processing will continue --->
	<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />
</cfif>
<cfinvoke component="session.cfc.emergencymessages" method="GetCustomDataFields" returnvariable="retValCustomDataField" />
<!--- Use this to pass in control flag to CP display options --->
<cfset inpSimpleViewFlag = "#adv#" />

<!--- Style fixes for new CP editor options --->
<style>
	
	.BuilderOption
	{
		<cfif adv EQ 0>
	    	display: none;	
	    </cfif>		
	}
	
	.builder-btn 
	{
    	padding: 0;
		margin: 3px -10px 0 .8em;
		/* display: none; */
		
	}	
	
	.control-point-body:hover
	{
		cursor: pointer;		
		
	}

	.select2-container--open
	{
		z-index: 10010;
		
	}
	.ajax-loader-inline {
	   
	}

	#FlowBuilder
	{		
		min-width:6em;		
	}
	
	.icon-builder 
	{
	  display:block;
	  background: url("../images/icon/flow_chart-32.png") no-repeat scroll center center transparent;
	  height:36px;
	}

	.active .icon-builder 
	{
		background: url("../images/icon/flow_chart-32_active.png") no-repeat scroll center center transparent;
	}

	.control-point-btn-add
	{   display: none;
    	margin-bottom: .8em;		
	}	

    .control-point-btn-delete{
        display: none;
    }
    
	.control-point-btn-add
	{
    	margin-bottom: .8em;	
    	background-image: url('/session/sire/images/icon/add-create-plus-icon-23.png');
    	background-repeat: no-repeat; 
    	line-height: 24px;
    	min-height: 24px;
    	min-width: 24px;
    	height: 24px;
    	min-height: 24px;
    	text-decoration: none;
	}	
	
	.control-point-btn-add:hover
	{
		
		background-image: url('/session/sire/images/icon/add-create-plus-icon-23-off.png');
		text-decoration: none;
	}
	
	.cp-edit-icon
	{
		
		color: #fafafa; 
		font-size: 1.4em; 
		margin-top: 0;
		line-height: 32px;
		height: 32px;
	}		
	
	.cp-edit-icon:hover
	{
		cursor: pointer;
		color: #5c5c5c;
	}

	.cp-obj-header
	{
		 background-color: #578ca5;
		 color: #fafafa; 
		 border-radius: 6px 6px 0 0; 
	}
	
	.AnswerItem input, .AnswerItem input.form-control {
    	width: calc(100% - 10px);
	}	
	
	.Select2
	{
		width: 100% !important;
	}
	
	.col-right-align
	{
		text-align: right;		
	}
	
	.col-left-align
	{
		text-align: left;		
	}
	
	.mbTight
	{
		margin-bottom: 0;		
	}
	
	.bc-title {
	    font-size: .8em;
	    font-weight: bold;
	}
	
	.ConditionItem
	{
		border: dashed #cec9c9 1px;
		padding: 1em;		
		
	}

	.bigdrop .select2-results {max-height: 300px;}
	
	.icon-med
	{
		font-size: 26px !important;
		height: 31px !important;
		width: 100% !important;
		background-size: contain !important;
		color: #5c5c5c;
		text-decoration: none;
		
	}
	
	.icon-med:hover
	{
		cursor:pointer;
	}

	.icon-med-text
	{
		font-size: .9 em;				
	}	
		
	.icon-cm {
	    display: block;
	    background: url("../images/icon/help-stop.png") no-repeat scroll center center transparent;
	    background-size: contain !important;
	    margin-top: -10px;
	}

	.content-menu a, .content-title a {
	    display: inline-block;
	    margin-left: auto;
	    color: #5c5c5c;
	    text-decoration: none;
	}
	
	#template-description-countdown, #template-name-countdown {
		text-decoration: none;
    	color: #71c37b;
	}

	#template-description, #template-xml {
		resize: none;
	}
<!--- 	.CPEContent{border: none !important; box-shadow: none: !important;} --->
	
	
	<!--- http://stackoverflow.com/questions/26620110/css-media-queries-many-vs-few --->
	<!--- Text align blast stats based on big(right) and small(left) --->	
	@media(max-width:767px){ #BlastStats {text-align: left;} }
	@media(min-width:768px){ #BlastStats {text-align: left;}}
	@media(min-width:992px){ #BlastStats {text-align: right;}}
	@media(min-width:1200px){ #BlastStats {text-align: right;}}
				
	<!--- Possible fix for overbound parent on Firefox. --->
	.select2-container--bootstrap, .select2-container--default {
	display: table!important;
	table-layout: fixed!important;
	}

	.center-vertical{
		position: absolute;
		top: 50%;
		left: 50%;
		/* bring your own prefixes */
		transform: translate(-50%, -50%);	     
	}
	
	.SlimTop
	{
		margin-top: 0;
	}
	
	.console-link-box
	{
		position: relative;
		min-height: 4em;
		padding: 1em;
		border: rgba(0, 0, 0, 0.1) solid 1px;
		border-radius: 1em;
	
		margin-bottom: 2em;
		-webkit-box-shadow: 6px 6px 5px 0px rgba(0,0,0,0.55);
		-moz-box-shadow: 6px 6px 5px 0px rgba(0,0,0,0.55);
		box-shadow: 6px 6px 5px 0px rgba(0,0,0,0.55);
	}
	
	.console-link-box:hover
	{
		cursor: pointer;

		-webkit-box-shadow: 3px 3px 2px 0px rgba(0,0,0,0.55);
		-moz-box-shadow: 3px 3px 2px 0px rgba(0,0,0,0.55);
		box-shadow: 3px 3px 2px 0px rgba(0,0,0,0.55);
		
	}	
	
	pre
	{
		background-color:inherit !important;
		border: none !important;
		font-size: inherit !important;
		padding: 0;
		margin: 0;
		color: inherit !important;
		line-height: inherit !important;
		width: 80%;
		
	}
	
	.bubble
	{
		word-break: break-word;
	}

	.save-new-template-btn-group{
		float:right;
	}

	.dd-option-text{
		line-height: 71px !important;
	}
	
	
	.scaled-frame
	{
		width: 100%;
		height: 400px;
	}

	@media only screen and (max-width: 480px){
		.dd-selected-image{
			width: 32px;
		}

		.dd-option-image{
			width: 32px;
		}

		.dd-option-text{
			line-height: 30px !important;
		}

		.dd-selected-text{
			line-height: 30px !important;
		}

		label.dd-selected-text {
			text-overflow: ellipsis;
			max-width: 80% !important;
		}

		label.dd-option-text {
			text-overflow: ellipsis;
			max-width: 80% !important;
		}
	}
	
	@media only screen and (min-width: 481px) {
		.dd-option-text{
			line-height: 58px !important;
		}

		.dd-selected-text{
			line-height: 58px !important;
		}
	}

	#SWTChooseBlast {
		margin-bottom: 15px; 
		background-color: #578CA5 !important; 
		border-color: #416d82 !important; 
		box-shadow: 0.418px 2.971px 0px 0px rgb( 59, 97, 116 ) !important;
	}
	
	@media only screen and (max-width: 480px) {
		#add-sub-list-modal-content {
			width: 90% !important;
			margin-left: 5% !important;

		}
	}
		
	/*@media only screen and (max-width: 990px) {
		#select-campagin-method {
			margin-left: 42% !important;
		}
	}*/
	#filter_rows{
		padding-left: 15px;
	}
	
	.Preview-Me .emoji-picker-icon {
	   
	    right: 25px !important;
	    bottom: 15px !important;
	    top: auto !important;
	    
	}
	
	.Preview-Me .emoji-wysiwyg-editor
	{
		text-shadow: none !important;
	}

	<!--- What is session/sire/assets/global/plugins/simple-line-icons/simple-line-icons.min.css used for? Why is it here? Need to overide or picker does not look right --->
	.Preview-Me .icon-bell:before {
	    content: '';
	}

	.Preview-Me .icon-grid:before {
	    content: '';
	}
	
	<!--- 
			https://coderwall.com/p/ruv9hq/display-emoji-glyphs-intermingled-with-arbitrary-text
			https://gist.github.com/mfornos/9991865 
	--->
	@font-face {
	  font-family: emoji;
	  
	  /* Fonts for text outside emoji blocks */
	  src: local('Droid Sans Mono'),
	       local('Lucida Console'),
	       local('Arial Monospaced'),
	       local(Arial);
	}
	
	@font-face {
	  font-family: emoji;
	
	  src: local('Apple Color Emoji'),
	       local('Android Emoji'),
	       local('Segoe UI'),
	       local(EmojiSymbols),
	       local(Symbola),
	       url('font/Symbola-Emoji.eot?#iefix') format('embedded-opentype'),
	       url('font/Symbola-Emoji.woff') format('woff'),
	       url('font/Symbola-Emoji.ttf') format('truetype');
	
	  /* Emoji unicode blocks */
	  unicode-range: U+1F300-1F5FF, U+1F600-1F64F, U+1F680-1F6FF, U+2600-26FF;
	}
	
	.Preview-Me
	{
		font-family: emoji;
		letter-spacing: 2px;		
	}
	
	.CPNumberOnPage
	{
		display:none;
	}
</style>

<!--- remove old method: 
<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>			
--->
<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode">	
	<cfinvokeargument name="inpBatchId" value="#campaignid#">
</cfinvoke>

<!--- Partial multi-short code support - Enterprise accounts - 
	  Look if Batch is being used by alternate short code - look in keyword - if it is use this short code 
--->

<!--- Only check for real batches being edited - not templates and not new request --->
<cfif isNumeric(campaignid) AND campaignid GT 0 AND inpTemplateFlag EQ 0>
		
	<cfinvoke component="session.sire.models.cfc.control-point" method="GetAssignedShortCode" returnvariable="RetVarGetAssignedShortCode">
		<cfinvokeargument name="inpBatchId" value="#campaignid#">
	</cfinvoke>

	<cfif RetVarGetAssignedShortCode.RXRESULTCODE EQ 1>	
		<cfset shortCode.SHORTCODE = RetVarGetAssignedShortCode.SHORTCODE />	
	</cfif>
	
</cfif>

<cfset campaignData = {
	BatchId_bi = '',
	KeywordId_int = '',
	Keyword_vch = '',
	Keyword_txt = '',
	Desc_vch = '',
	EMS_Flag_int = 0,
	GroupId_int = 0,
	Schedule_arr = [],
	ControlPoint_arr = [],
	CustomHelpMessage_vch = '',
	CustomStopMessage_vch = '',
	MLPID = 0
}>

<cfif isNumeric(campaignid) AND campaignid GT 0 AND inpTemplateFlag EQ 0>
		
	<cfinvoke method="GetBatchDetails" component="session.sire.models.cfc.control-point" returnvariable="RetVarGetBatchDetails">
		<cfinvokeargument name="inpBatchID" value="#campaignid#">
	</cfinvoke>
		    
    <cfif RetVarGetBatchDetails.RECORDCOUNT LE 0>
    	<cflocation url="/session/sire/pages/campaign-manage" addtoken="false">
    </cfif>
    
<!--- I need to be able to edit advaced options like schedule and stuff even for TEMPLATE created campaigns
	<cfif RetVarGetBatchDetails.TEMPLATEID GT 0>
		<cflocation url="/session/sire/pages/campaign-template?campaignId=#campaignid#" addtoken="false">
	</cfif>
--->

	<cfif RetVarGetBatchDetails.RXRESULTCODE GT 0>
		<cfif RetVarGetBatchDetails.SHORTCODEID NEQ shortCode.SHORTCODEID>
			<cflocation url="/session/sire/pages/campaign-manage" addtoken="false"/>
		</cfif>
	</cfif>

    <cfset campaignData.BatchId_bi = RetVarGetBatchDetails.BATCHID>
    <cfset campaignData.Desc_vch = RetVarGetBatchDetails.DESC>
    <cfset campaignData.EMS_Flag_int = RetVarGetBatchDetails.EMSFLAG>
    <cfset campaignData.GroupId_int = RetVarGetBatchDetails.GROUPID>
    <cfset campaignData.MLPID = RetVarGetBatchDetails.MLPID>
    
    <!--- Dont display MLP if it is no longer available or online --->    
    <cfif campaignData.MLPID GT 0> 
    	<cfinvoke component="session.sire.models.cfc.mlp" method="ReadCPP" returnvariable="RetCPPXData">
			 <cfinvokeargument name="ccpxDataId" value="#campaignData.MLPID#">
		</cfinvoke> 
		
		<cfif RetCPPXData.STATUS EQ 0>
			<cfset campaignData.MLPID = 0 />	
		</cfif>		
		
	</cfif>								
  
	<cfinclude template="/session/cfc/csc/constants.cfm">
	<cfquery name="keywordQuery" datasource="#session.DBSourceREAD#">
		SELECT KeywordId_int, Keyword_vch, Created_dt, Active_int, EMSFlag_int
		FROM sms.keyword WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#RetVarGetBatchDetails.BATCHID#">
			AND IsDefault_bit = 0
		ORDER BY Created_dt DESC, KeywordId_int DESC
	</cfquery>
	
	<cfif keywordQuery.RecordCount GT 0 && keywordQuery.Active_int[1] EQ 1>
		<cfset campaignData.KeywordId_int = keywordQuery.KeywordId_int[1]>
		<cfset campaignData.Keyword_vch = keywordQuery.Keyword_vch[1]>
		<cfif keywordQuery.EMSFlag_int[1] EQ 0>
			<cfset campaignData.Keyword_txt = campaignData.Keyword_vch>
		</cfif>
	</cfif>
	
	<!--- --->
	<cfinvoke method="GetSchedule" component="session.cfc.schedule" returnvariable="ScheduleByBatchId">
		<cfinvokeargument name="INPBATCHID" value="#campaignid#">
	</cfinvoke>
	<cfif ScheduleByBatchId.RXRESULTCODE GT 0>
		<cfloop array="#ScheduleByBatchId.ROWS#" index="ROW">						
			<cfset Arrayappend(campaignData.Schedule_arr, ROW)>
		</cfloop>
	</cfif>
	
	<cfparam name="SCHEDULE" default="#campaignData.Schedule_arr#" >
	<!--- --->
	
	<cfquery name="customMessageQuery" datasource="#session.DBSourceREAD#">
		SELECT * FROM sms.smsshare WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#campaignid#">
	</cfquery>

	<cfloop query="#customMessageQuery#">
		<cfswitch expression="#Keyword_vch#" >
		<cfcase value="HELP">
			<cfset campaignData.CustomHelpMessage_vch = Content_vch>
		</cfcase>
		<cfcase value="STOP">
			<cfset campaignData.CustomStopMessage_vch = Content_vch>
		</cfcase>
		</cfswitch>
	</cfloop>

<!--- If campaignid is specified and template edit mode is enbabled by the inpTemplateFlag=1, then assume campaignid is the templateId--->
<cfelseif isNumeric(campaignid) AND campaignid GT 0 AND inpTemplateFlag EQ 1>

	<cfset campaignData = {
		BatchId_bi = '',
		KeywordId_int = '',
		Keyword_vch = '',
		Keyword_txt = '',
		Desc_vch = '',
		EMS_Flag_int = 0,
		GroupId_int = 0,
		Schedule_arr = [],
		ControlPoint_arr = [],
		CustomHelpMessage_vch = '',
		CustomStopMessage_vch = '',
		MLPID = 0
	}>
	
	<cfinvoke method="GetBatchDetails" component="session.sire.models.cfc.control-point" returnvariable="RetVarGetBatchDetails">
		<cfinvokeargument name="inpBatchID" value="#campaignid#">
		<cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
	</cfinvoke>
		    
    <cfif RetVarGetBatchDetails.RECORDCOUNT LE 0>
    	<cflocation url="/session/sire/pages/campaign-manage" addtoken="false">
    </cfif>
    
    <cfset campaignData.BatchId_bi = RetVarGetBatchDetails.BATCHID>
    <cfset campaignData.Desc_vch = RetVarGetBatchDetails.DESC>
    <cfset campaignData.EMS_Flag_int = RetVarGetBatchDetails.EMSFLAG>
    <cfset campaignData.GroupId_int = RetVarGetBatchDetails.GROUPID>
    <cfset campaignData.MLPID = RetVarGetBatchDetails.MLPID>

<!--- Template was selected and template edit mode is enbabled by the inpTemplateFlag=1 --->	
<cfelseif isNumeric(templateid) AND inpTemplateFlag EQ 1>
    <cfset campaignData = {
		BatchId_bi = '',
		KeywordId_int = '',
		Keyword_vch = '',
		Keyword_txt = '',
		Desc_vch = '',
		EMS_Flag_int = 0,
		GroupId_int = 0,
		Schedule_arr = [],
		ControlPoint_arr = [],
		CustomHelpMessage_vch = '',
		CustomStopMessage_vch = '',
		MLPID = 0
	}>

	<cfset campaignData.BatchId_bi = templateid>
	<cfset campaignid = templateid>
	
	<cfinvoke method="GetBatchDetails" component="session.sire.models.cfc.control-point" returnvariable="RetVarGetBatchDetails">
		<cfinvokeargument name="inpBatchID" value="#campaignid#">
		<cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
	</cfinvoke>
		    
    <cfif RetVarGetBatchDetails.RECORDCOUNT LE 0>
    	<cflocation url="/session/sire/pages/campaign-manage" addtoken="false">
    </cfif>
    
    <cfset campaignData.BatchId_bi = RetVarGetBatchDetails.BATCHID>
    <cfset campaignData.Desc_vch = RetVarGetBatchDetails.DESC>
    <cfset campaignData.EMS_Flag_int = RetVarGetBatchDetails.EMSFLAG>
    <cfset campaignData.GroupId_int = RetVarGetBatchDetails.GROUPID>
    <cfset campaignData.MLPID = RetVarGetBatchDetails.MLPID>
    
<cfelseif isNumeric(templateid) AND inpTemplateFlag EQ 0>
	
	<!--- By default - create a batch to store - 
		
		I dont like preview option forceing save with full validation
				
		when each CP is saved update the real Batch 
		Save with default keyword	Sire userid batchid
				
		Force final checks on final save - dont validate on early save
		
		Add option to cancel when still in intial creation
				
	--->
			
	<!--- Set default keyword so preview works too --->	
	
	<!--- Create new Batch --->
	<cfinvoke method="AddNewBatchFromTemplate" component="session.sire.models.cfc.control-point" returnvariable="RetVarAddNewBatchFromTemplate">
		<cfinvokeargument name="inpTemplateId" value="#templateid#">
		<cfinvokeargument name="inpDesc" value="">
		<cfinvokeargument name="inpKeyword" value="">
		<cfinvokeargument name="inpShortCode" value="#shortCode.SHORTCODE#">
		<cfinvokeargument name="inpAddList" value="#swt_aal#">
	</cfinvoke>
	
	<!--- 	<cfdump var="#RetVarAddNewBatchFromTemplate#">	 <cfabort/> --->
	
   <!--- Skip the middle man and just load page after template is copied --->       
   
    <cfif ADV EQ 1>
	    <cfset ADVBuff = "&ADV=1" />
	<cfelse>
		<cfset ADVBuff = "" />
	</cfif>
   
    <cfif RetVarAddNewBatchFromTemplate.BATCHID GT 0>
	    <cfif Tour neq "" >
	    	<cflocation url="campaign-edit?campaignid=#RetVarAddNewBatchFromTemplate.BATCHID#&Tour=#Tour##ADVBuff#&swt=1&tt=#RetVarAddNewBatchFromTemplate.TEMPLATETYPE#&customCampaign=#customCampaign#" addToken="false" />
	    <cfelse>
		    <cflocation url="campaign-edit?campaignid=#RetVarAddNewBatchFromTemplate.BATCHID##ADVBuff#&swt=1&tt=#RetVarAddNewBatchFromTemplate.TEMPLATETYPE#&customCampaign=#customCampaign#" addToken="false" />
	    </cfif>	
    </cfif>
<cfelse>
	<cflocation url="/session/sire/pages/campaign-manage" addtoken="false">
</cfif>

<cfset cpOneSelectionNoBranchs = []>
<cfset cpOneSelectionInBranchs = []>


<!--- Lookup whether this BtchId has any blasts associated with it. Modify display options accordiningly --->
<cfinvoke component="session.sire.models.cfc.control-point" method="GetBatchBlasts" returnvariable="RetVarGetBatchBlasts">
	<cfinvokeargument name="inpBatchId" value="#campaignid#">
</cfinvoke>
	
     	


<cfset subcriberArray = []>

<cfinvoke component="session.sire.models.cfc.subscribers" method="GetSubcriberList" returnvariable="groupList">
	<cfinvokeargument name="inpShortCode" value="#shortCode.SHORTCODE#">	
	<cfinvokeargument name="inpShortCodeId" value="#shortCode.SHORTCODEID#">		
</cfinvoke>
<cfif groupList.RXRESULTCODE EQ 1 AND groupList.iTotalRecords GT 0>
	<cfset subcriberList = groupList.listData>
	<cfloop array ="#subcriberList#" index="item">
	    <cfset subcriberArray[item[1]] = item[2]>
	</cfloop>
<cfelse>
	<cfset subcriberList = []>
</cfif>


<!--- MINHHTN HOT FIX --->
<cfparam name="Session.AdditionalDNC" default="">

<!---<cfinclude template="/public/sire/configs/paths.cfm">

<cfinclude template="/public/sire/configs/userConstants.cfm">--->

<cfset totalSubcriber = 0>

<cfparam name="batchid" default="#campaignid#">
<cfparam name="group_id" default="#campaignData.GroupId_int#">
<cfset disableClass="">


<cfinvoke component="session.sire.models.cfc.order_plan" method="getUserPlan" returnvariable="userPlanQuery"></cfinvoke >
<cfif userPlanQuery.RXRESULTCODE EQ 1 AND userPlanQuery.AMOUNT GT 0>
	<cfset keywordMinLength = 5>
<cfelse>
	<cfset keywordMinLength = 8>
</cfif>


<cfoutput>
<div class="portlet light bordered subscriber">
<!--- <main class="container-fluid page"> Remove for NBE integrate--->
	
	<!--- <cfif swt EQ 0>
	<cfinclude template="../views/commons/credits_available.cfm">
	</cfif> --->
	
	<!--- <section class="row bg-white"> Remove for NBE integrate--->
		
		
<!--- 		<button id="btn-test-auto" type="button" class="btn btn-success btn-success-custom" onclick="TestAutoAdd();"> Test </button> --->
		
		<!--- Hide these option on walkthrough ---> 
    	<cfif swt EQ 0>
			<div class="content-header">
				
				<cfif inpTemplateFlag NEQ 0>
					<!--- Verify System Admin only can do this for now - may add ability for user to edit their own custom templates later--->	
					<div class="row">
		                <div class="col-sm-12 col-md-12 col-lg-12 content-title">
		            		<h1>Template Edit Mode</h1>
		            	</div>
					</div>
				</cfif>
				
				
<!---
				 <div class="container">
      <div class="row justify-content-center">
        <div class="col-10">
          <div class="text-left">
            <p class="lead emoji-picker-container">
              <input type="email" class="form-control" placeholder="Input field" data-emojiable="true">
            </p>
            <p class="lead emoji-picker-container">
              <input type="email" class="form-control" placeholder="Input with max length of 10" data-emojiable="true" maxlength="10">
            </p>
            <p class="lead emoji-picker-container">
              <textarea class="form-control textarea-control" rows="3" placeholder="Textarea with emoji image input" data-emojiable="true"></textarea>
            </p>
            <p class="lead emoji-picker-container">
              <textarea class="form-control textarea-control" rows="3" placeholder="Textarea with emoji Unicode input" data-emojiable="true" data-emoji-input="unicode"></textarea>
            </p>
          </div>
        </div>
      </div>
    </div>
--->
				<!--- Only show menu if not in template edit mode --->
				<cfif inpTemplateFlag EQ 0>			
			    	<div class="row">
		                <div class="col-xs-12 col-sm-6 col-md-8 col-lg-8 content-title" id="campaign-detail-div">
		            		
		            		<p class="<!--- title Remove for NBE integrate--->page-title">Manage Campaign <cfoutput>Id ###batchid#</cfoutput></p>

		            	</div>
		            	 				            
		            	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 content-menu" id="campaign-option-div">
				        
				         <div class="dropdown" style="position: absolute; right: 1em; top:0em;">
						
<!---
							  <a href="##" data-toggle="dropdown" class="dropdown-toggle">Campaign Edit Options
							  <span class="caret"></span></a>
--->
							  
							 <!--- 
							  <a class="active dropdown-toggle" href="##" data-toggle="dropdown" style="position: relative; text-align: right;">
<!--- 				            	<span class="glyphicon icon-edit" style="xposition:absolute; right: 0; top: 0; min-width: 32px;"></span> --->
								<div class="glyphicon-class"><img src="../images/icon/icon_edit_active.png" /></div>
				            	<div class="glyphicon-class">Campaign Options<span class="caret"></span></div>
				            	
				              </a>
							  --->
							  <ul class="dropdown-menu" style="left: -8em;">
							    <li>
							    	<a href="/session/sire/pages/campaign-edit?campaignid=#campaignid#&swt=1&tt=#tt#">Template Walk-Through</a>
								</li>
							    <li>
							    	<a href="##" class="SMSBlastOption" id="SMSBlastSelect">Blast</a>
					            </li>
							  
							    <li>
							    	<a data-toggle="modal" href="##" data-target="##cloneCampaignModal">Clone</a>
								</li>
								<!--- <li>
							    	<a href="/session/sire/pages/campaign-report?campaignid=#campaignid#&af_batch_id=#campaignId#">Report</a>
								</li> --->
								
								<li class="dropdown-header">Advanced Features</li>
								
								<cfif findnocase('adv=1', cgi.query_string ) GT 0>
									<li><a href="<cfoutput>#cgi.script_name#?campaignid=#campaignid#&adv=0</cfoutput>" style="padding-left:2em;">(X) Cancel Edit Campaign Flow</a></li>	
								<cfelse>
									<li><a href="<cfoutput>#cgi.script_name#?campaignid=#campaignid#&adv=1</cfoutput>" style="padding-left:2em;">Edit Campaign Flow</a></li>
								</cfif>
								<li>
							    	<a href="##" data-toggle="modal" data-target="##CustomMessageModal" style="padding-left:2em;">Custom Help / Stop</a>
						        </li>
						        <cfif GetAdminPermission.ISADMINOK EQ  1>
							        <cfif XMLCSEditable EQ true>
								        <li>
											<a href="##" data-toggle="modal" data-target="##EditControlString" style="padding-left:2em;">Edit XMLControlString</a>
										</li>
							        </cfif>
								<cfelse>
									<cfif XMLCSEditable EQ true>
								        <li>
											<a href="##" data-toggle="modal" data-target="##EditControlString" style="padding-left:2em;">Edit XMLControlString</a>
										</li>
							        </cfif>
							    </cfif>

							   </ul>
							</div>

		            	</div>		            				
		            	
			    	</div>	    	
		    	</cfif>
		    	
			</div>
				
			<hr class="hrt0">
		
		</cfif>
		
		<form id="campaign_edit" class="clearfix" autocomplete="off">
			<input value="#campaignid#" id="campaign_id" type="hidden" name="BatchId_bi">
			<input value="#templateid#" id="template_id" type="hidden" name="TID_int">
			<input value="#shortCode.SHORTCODE#" id="ShortCode" type="hidden" name="ShortCode">
			<input value="#campaignData.KeywordId_int#" id="keywordId" type="hidden" name="keywordId">
<!--- 			<input value="#campaignData.Keyword_vch#" id="KeywordVch" type="hidden" name="Keyword_vch"> --->
		    
			<div class="content-body">
								    			
				    			
    			<!--- Hide these option on walkthrough ---> 
    			<cfif swt EQ 0>
	    			    		
		    		<!--- This section is for specifying "Blast" values --->
					<div id="BlastContainer" class="row mb15" style="display:none;">	    			
		    			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-6">
		    			
		    				<button type="button" class="close btn-cancel-blast-data SMSBlastOption" aria-label="Close" style="z-index: 6;"><span aria-hidden="true">&times;</span></button>
		    			
		    				<div class="col-sm-12" style="" id="PickAList">
								<label for="disabledSelect" class="">Select the Subscriber List <span>* <i>required</i></span></label>
								<div class="warning-block">						        
				                    								
									<select id="SubscriberList" data-type="old-blast" class="Select2" style="width:100%;">	
										<option value="0" selected >Select Subscriber List</option>
										<cfloop array ="#subcriberList#" index="item">
											<option value="#item[1]#" data-total-subcribers="#item[3]#">#item[2]# (#item[3]#)</option>
						            	</cfloop>                                  
									</select>
								
							    </div>
						    <br>
						    <div class="box-subcriber-filter hidden" class="warning-info">
							    <label id="lbl-show-filter-bar" for="subcriber-filter-bar" class="btn btn-success-custom showed">Show filter</label>
							
							    <div id="subcriber-filter-bar" class="hidden">
							    	<cfoutput>
							            <!---set up column --->
							            <cfset datatable_ColumnModel = arrayNew(1)>

							            <cfloop array="#retValCustomDataField.CDFARRAY#" index="item">
								            <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = item.VARIABLENAME, CF_SQL_TYPE = 'CF_SQL_VARCHAR', TYPE='TEXT', SQL_FIELD_NAME = item.CDFID})>
							            </cfloop>

							            <!---we must define javascript function name to be called from filter later--->
							            <cfset datatable_jsCallback = "InitControl">
							            <br>
							            <input class="form-control" placeholder="Contact String" name="inpContactStringFilter" id="inpContactStringFilter">
							            <cfinclude template="/session/sire/pages/campaign-edit-blast-filter.cfm" >
							            <div class="table-responsive">
								            <table id="tblListEMS" class="table table-striped table-bordered table-hover dataTable">
								            	<thead>
								            		<tr>
								            			<th>Contact ID</th><th>Contact</th>
								            		</tr>
								            	</thead>	
								            </table>
							            </div>
						            </cfoutput>
							    </div>
						    </div>
							</div>
						
							<div class="col-sm-12">
								<hr class="hrb10">
							    <label for="opt-out-txt">Schedule</label>
							    <div class="warning-block">
							        <div id="warning-content"><span class="warning-text">Note:</span> Messages are by default scheduled between 9am - 7pm (PST).</div>
							        <a data-toggle="modal" data-target="##ScheduleOptionsModal" href="##">
							            Click here to enable alternate delivery times.
							        </a>
							    </div>
							    
							</div>
		    			
			    			<div class="col-sm-12">
				    			<div class="modal-footer">
									<button id="btn-save-send" type="button" class="btn btn-success btn-success-custom btn-save-send" data-dismiss="modal"> Send </button>			
									<button type="button" class="btn btn-primary btn-back-custom btn-prev-swt btn-move-swt">Cancel</button>
								</div>	
			    			</div>
		    				    			
		    			</div>
		    			
						<div class="col-xs-12 col-sm-12">
							<hr class="hrb10">
						</div>
						
	    			</div>

	    		
	    			<div id="SWTDescSection" class="form-group clearfix">
											
						<div class="col-xs-12 col-sm-12 hidden-md hidden-lg ">
							<div class="row">
							 	&nbsp;
					 		</div> 
						</div>
									
						<!--- This section is for dispalying "Blast" values --->
						<!--- Do this here to avoid FOUC flicker --->
						<cfif RetVarGetBatchBlasts.BLASTS.RECORDCOUNT GT 0> 
							<!--- Only show if there has been a blast --->
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="BlastStats">
						<cfelse>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="BlastStats" style="display:none;">
						</cfif>
						    				
		    				<div id="ProcessingBlastStats" style="min-height: 60px; float: left;">
								<img src="/public/sire/images/loading.gif" class="ajax-loader">
							</div>							
													
							<div class="col-xs-12 col-sm-12">
								<label id="BlastQueue" style="display: block;">Total Messages Still In Blast Queue: &nbsp;</label>
								<label id="BlastComplete" style="display: block;">Total Blast Messages Completed: &nbsp;</label>
								<h4 id="btn-reload-blast-stats"><a href="##" style="color:##568ca5;">Refresh Stats</a></h4>
							</div>	
							
		    			</div>
													
					</div>
					    		
	    		</cfif>
	    			
    			
    			<!--- Optional walk through on initial creation - do not use Simplistic Walk Through for template editing. --->
    			<cfif swt EQ 1 AND inpTemplateFlag EQ 0> 
	    			
	    			<div class="row mb15">
		    			
		    			<div class="col-lg-12">
			    			
			    			<!--- Show here on small screens - show lower on larger --->
			    			<div class="hidden-xs xhidden-sm" style="float:left; padding: 15px;">
								<h3 style="margin-bottom: 0em; margin-top: 0;">Template Walk-Through</h3>
							</div>
							
			    			<!--- Show the navigation buttons at top and bottom of display so user is not scrolling back and forth --->
			    			<div class="modal-footer SWTNavStuff SWTTopNavButtons" style="border: none;">
								<button id="btn-prev-swt" type="button" class="btn btn-success btn-success-custom btn-prev-swt btn-move-swt" data-dismiss="modal" dir="0" style="" disabled> PREVIOUS </button>
								<button id="btn-next-swt" type="button" class="btn btn-success btn-success-custom btn-next-swt btn-move-swt" data-dismiss="modal" dir="1" style="" disabled> NEXT </button>						
								<button type="button" class="btn btn-primary btn-back-custom btn-complete-swt">Finished</button>
							</div>
						
							<div class="modal-footer" style="text-align: left; padding-bottom: 0; padding-top: 0;">
							
															
							</div>
																					
		    			</div>
		    			
	    			</div>	
	    			
	    			<div class="row mb15">
		    					    					    			
		    			
		    			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 SWTNavStuff" id="SWTLocation" style="text-align: left; position: relative;">
			    			
			    			<!--- Show here on bigger screens - show higher on larger --->		    						    			
						    <div class="col-xs-12 hidden-sm hidden-md hidden-lg" style="">
								<h3 style="margin-bottom: 1em; margin-top: 0;">Template Walk Through</h3>
							</div>
				    		<div class="col-xs-12" style="height: 150px; width: 100%; margin-bottom: 1em;">
			    				<!--- Chart to show completion of the the walk through so far --->
				    			<canvas id="myChartSWTCompletion" width="150" height="150" style="" class="center-vertical"></canvas>
				    			
			    				<div id="Chart-Legend-Completion" class="chart-legend center-vertical" style="display: none;"></div>			    			
			    			
		    				</div>	
		    			</div>	
	    			
		    			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" id="SWTContainer" style="min-height: 400px;">
			    			
			    			<div id="SWTChooser" class="col-xs-12 col-sm-12 form-group clearfix " style="display: none; margin-top: 0;">   
				    			
				    			<h4>How do you want to use this campaign with your subscribers?</h4>
				    			
				    			<hr>
				    			<div class="row">
				    				<div class="col-xs-12 col-sm-12 col-lg-5 col-md-5" style="text-align: center">
				    					<p>
						    				<h4><b>A:</b> Your subscriber sends a Keyword to your short code</h4>
						    				<img src="/session/sire/images/keyword-walk-through.png" style="margin-bottom: 20px; margin-top: 20px">
						    						
						    			</p>
						    			<button id="SWTChooseKeyword" type="button" class="btn btn-success btn-success-custom" data-dismiss="modal" style="margin-bottom: 20px"> Set a Keyword </button>
						    			<p style="">
						    				<a data-toggle="modal" href="##" data-target="##InfoModalKeyword" class="ConsoleLinks" style="">What is a Keyword?</a>
						    			</p>
				    				</div>
				    				<div class="col-xs-12 col-sm-12 col-lg-2 col-md-2 text-center">
  										<h4 id="select-campagin-method" class="text-center"><strong>OR</strong></h4>
  										<div class="hidden-xs hidden-sm" style="width: 1px; margin: 0 auto; border-color: ##eee; height: 300px; border-right-style: solid; border-width: 1px;">
  										</div>
									</div>
				    				<div class="col-xs-12 col-sm-12 col-lg-5 col-md-5 pull-right" style="text-align: center">
				    					<p>
						    				<h4><b>B:</b> You send a Blast to a list of subscribers        </h4>
						    				<img src="/session/sire/images/blast-walk-through.png" style="margin-bottom: 20px; margin-top: 20px">
						    				
						    			</p>
						    			<button id="SWTChooseBlast" type="button" class="btn btn-success btn-success-custom" data-dismiss="modal" style="margin-bottom: 20px"> Setup a Blast </button>	
						    			
						    			<p>
						    				<a data-toggle="modal" href="##" data-target="##InfoModalBlast" class="ConsoleLinks" style="">What is a Blast?</a>
						    			</p>
				    				</div>
				    				<!--- <p>OR</p> --->
				    								    			
				    			</div>
				    			
<!---
				    			<div class="col-xs-12 col-sm-12 col-lg-5 col-md-5 LearnMore" style="text-align: left">
					    			<a href="##" class="ConsoleLinks">Learn More</a>
					    			<div>Additional Content</div>
					    		</div>
					    		
					    		<div class="col-xs-12 col-sm-12 col-lg-5 col-md-5 LearnMore" style="text-align: left">
					    			<a href="##" class="ConsoleLinks">Learn More</a>
					    			<div>Additional Content</div>
					    		</div>
--->
					    			
			    			</div>
			    							    			
			    			<!--- This section is for specifying "Blast" values --->
							<div id="SWTBlastContainer" class="form-group clearfix" style="display: none; margin-top: 0;">   			
				    			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				    							    								    			
				    				<div class="col-sm-12" style="" id="PickAList">
										<label for="disabledSelect" class="">Select the Subscriber List <span>* <i>required</i></span></label>
										<div class="warning-block">						        
						                    								
											<select id="SubscriberList" class="Select2" style="width:100%;">	
												<option value="0" selected >Select Subscriber List</option>
												<cfloop array ="#subcriberList#" index="item">
													<option value="#item[1]#" data-total-subcribers="#item[3]#">#item[2]# (#item[3]#)</option>
								            	</cfloop>                                  
											</select>
										
									    </div>
									</div>

				    				<div class="col-sm-12">					    			
						    			<p style="margin-top: 1em;">
						    				<a data-toggle="modal" href="##" data-target="##InfoModalBlast" class="ConsoleLinks" style="">What is a Blast? Why do I need a subscriber list?</a>
						    			</p>
				    				</div>
				    				    			
				    			</div>
				    			
								
								
			    			</div>
			    	
							<div id="SWTDescSection" class="form-group clearfix" style="display: none; margin-top: 0;">
				    			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					    				    							
									<!--- Batch Desc section with editor option --->
									<div class="form-group clearfix">
														        			
					        			<div class="col-sm-12 col-md-12 col-lg-12">
						        			
						        			<!--- Preserve possible newlines in display but format all other tags --->
											<!--- Expected HTML output is different for straight div vs text area - Preview, Edit, SWT etc --->                    
						                    <!--- Allow newline in text desc messages - reformat for display --->
											<!--- Regular HTML --->
											<cfset CDescHTML = ReplaceNoCase(campaignData.Desc_vch, "newlinex", "<br>", "ALL")>
											<cfset CDescHTML = Replace(CDescHTML, "\n", "<br>", "ALL")>
											<cfset CDescHTML = Replace(CDescHTML, "#chr(10)#", "<br>", "ALL")>
											<cfset CDescHTML = Replace(CDescHTML, "{defaultShortcode}", "#shortcode.SHORTCODE#", "ALL")>
											<!--- Text Area version --->				
											<cfset CDescTA = Replace(campaignData.Desc_vch, "\n", "#chr(10)#", "ALL")>
											<cfset CDescTA = Replace(CDescTA, "<br>", "#chr(10)#", "ALL")>
											<cfset CDescTA = Replace(CDescTA, "{defaultShortcode}", "#shortcode.SHORTCODE#", "ALL")>
												        			
					        				<div id="BatchDescEditor" style="">   
						        				<label for="campaign_name">Give Your Campaign a Name:</label>     	
						        				<textarea class="form-control" id="BatchDesc" maxlength="1000" rows="3">#CDescTA#</textarea>	
						        			</div>
					        				
					        				<!--- Spacer to MAtch Keyword --->
					        				<div id="" style="font-size: .8em; color: red; min-height: 3em;">&nbsp;</div>	
					        				
					    				</div>
					    									    								    				
						    			<p>
					    					<a data-toggle="modal" href="##" data-target="##InfoModalCampaignName" class="ConsoleLinks" style="padding-left:1em;">What makes a good Campaign Name?</a>
						    			</p>	
					    						    						    				
					    			</div>	      			 	  								
			
														    				
								</div>
							</div>
							
							<div id="SWTKeywordSection" class="form-group clearfix" style="display: none; margin-top: 0;">
				    			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				   			 		
				   			 		<!--- Only show keyword option if this is not in template edit mode --->
				   			 		<cfif inpTemplateFlag EQ 0>							
										
										<div class="form-group clearfix" id="KeywordDefineSection">
											<div class="col-sm-9 col-md-8 col-lg-8" style="">    
												<label for="campaign_name">Keyword:</label>       				 	
												<!--- <cfif campaignData.Keyword_txt EQ ''> validate[required,custom[onlyLetterNumberSp],minSize[#keywordMinLength#],ajax[ajaxKeywordAvailable]]</cfif>  data-errormessage-range-underflow="* Not enough characters (upgrade to higher paid account)"--->
						    					<input value="#campaignData.Keyword_txt#" class="form-control" id="Keyword" type="text" name="Keyword" maxlength="160" style="width:100%;" />  
						    					<div id="KeywordStatus" style="font-size: .8em; color: red; min-height: 3em;">&nbsp;</div>						
						        			</div>
										</div>
										
				        			<cfelse>
					        			<input value="#campaignData.Keyword_txt#" class="form-control" id="Keyword" type="hidden" name="Keyword" maxlength="160"> 	        				
				   			 		</cfif>
			    	
				   			 		<h4>Set a <b>Keyword</b> that your subscriber sends to your <b>Short Code</b></h4>
					   			 	<p>
				    					<a data-toggle="modal" href="##" data-target="##InfoModalKeyword" class="ConsoleLinks" style="padding-left:1em;">What is a Keyword?</a>
				    				</p>
			    				
								</div>										
							  
							</div>
													
		    			
							
							<!--- Do this here to get simple counts of RQs and which ones have WT flag set - also used save on round trips to remote server for where we are at --->
							<cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPs" returnvariable="RetVarReadCPs">
								<cfinvokeargument name="inpBatchId" value="#campaignid#">
								<cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
							</cfinvoke>
							
							<script>								
							
								<!--- Global var for tracking where we are in the walk through --->
								var CurrentSWTRQ = 0;
								var CurrentSWTStep = 0;
								<!--- This outputs a previously defined Coldfusion variable as a javascript variable ---> 
								<cfoutput> 
									var #toScript(RetVarReadCPs, "jsVarRetVarReadCPs")#; 
								</cfoutput> 
							</script>										
							
							<div id="WalkThroughStepEditor" data-control-point-rq="0" style="margin-top: 0em;">
								
								<div id="ProcessingOptCounts" style="min-height: 400px; width: 100%; float: left;" class="console-box">
									<img src="/public/sire/images/loading.gif" class="ajax-loader">									
								</div>
				
							</div>	
										    			
		    			</div>
	    			
	    			</div>
	    			
	    			<div class="row mb15">
		    			
		    			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			    			
			    			<!--- Show the navigation buttons at top and bottom of display so user is not scrolling back and forth --->
			    			<div class="modal-footer SWTNavStuff">
								<button id="btn-prev-swt" type="button" class="btn btn-success btn-success-custom btn-prev-swt btn-move-swt" data-dismiss="modal" dir="0" style="" disabled> PREVIOUS </button>
								<button id="btn-next-swt" type="button" class="btn btn-success btn-success-custom btn-next-swt btn-move-swt" data-dismiss="modal" dir="1" style="" disabled> NEXT </button>				
								<button type="button" class="btn btn-primary btn-back-custom btn-complete-swt">Finished</button>
							</div>
						
		    			</div>
		    			
	    			</div>	
	    				
    			<cfelse>
    				   			
	    			<div class="row mb15">
		    				    			    				    			
		    			<!-- <div class="col-md-6" id="CPContainer"> Remove for NBE integrate-->
			    			<div class="col-md-12 col-lg-12" id="CPContainer">
			    			
			    			<!--- Hide these option on walkthrough ---> 
			    			<cfif swt EQ 0>
				    			
				    			<!--- Preserve possible newlines in display but format all other tags --->
								<!--- Expected HTML output is different for straight div vs text area - Preview, Edit, SWT etc --->                    
			                    <!--- Allow newline in text desc messages - reformat for display --->
								<!--- Regular HTML --->
								<cfset CDescHTML = ReplaceNoCase(campaignData.Desc_vch, "newlinex", "<br>", "ALL")>
								<cfset CDescHTML = Replace(CDescHTML, "\n", "<br>", "ALL")>
								<cfset CDescHTML = Replace(CDescHTML, "#chr(10)#", "<br>", "ALL")>
								<cfset CDescHTML = Replace(CDescHTML, "{defaultShortcode}", "#shortcode.SHORTCODE#", "ALL")>
								<!--- Text Area version --->				
								<cfset CDescTA = Replace(campaignData.Desc_vch, "\n", "#chr(10)#", "ALL")>
								<cfset CDescTA = Replace(CDescTA, "<br>", "#chr(10)#", "ALL")>
								<cfset CDescTA = Replace(CDescTA, "{defaultShortcode}", "#shortcode.SHORTCODE#", "ALL")>
					
								    		
				    		</cfif>	    		
		    		
							<cfset MaxCPCount = 0 />
			    			<cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPs" returnvariable="RetVarReadCPs">
								<cfinvokeargument name="inpBatchId" value="#campaignid#">
								<cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
							</cfinvoke>
		
			    			<cfloop array="#RetVarReadCPs.CPOBJ#" index="CPOBJ">	    			
				    		    
				    		    <cfset MaxCPCount = MaxCPCount + 1 />
				    		        
				    			<cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPDataById" returnvariable="RetVarReadCPDataById">
									<cfinvokeargument name="inpBatchId" value="#campaignid#">
									<cfinvokeargument name="inpQID" value="#CPOBJ.RQ#">
									<cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">
								</cfinvoke>
																	
				    			<cfinvoke method="RenderCP" component="session.sire.models.cfc.control-point" returnvariable="RetVarRenderCP">
									<cfinvokeargument name="controlPoint" value="#RetVarReadCPDataById.CPOBJ#">
									<cfinvokeargument name="inpBatchId" value="#campaignid#">	
									<cfinvokeargument name="inpSimpleViewFlag" value="#inpSimpleViewFlag#">	
									<cfinvokeargument name="inpTemplateFlag" value="#inpTemplateFlag#">						
								</cfinvoke>
							
								<cfif RetVarReadCPDataById.CPOBJ.TYPE EQ "TRAILER" OR RetVarReadCPDataById.CPOBJ.TYPE EQ "SHORTANSWER">
									#Replace(Replace(RetVarRenderCP.HTML,'#chr(35)# <span class="CPNumberOnPage">1</span>. <span class="">Question & Wait for Answer</span>',"<span>What do you want your reminder message to say?.</span>", "ALL"),'#chr(35)# <span class="CPNumberOnPage">4</span>. <span class="">Final Message - This will end the conversation</span>',"<span>What would you like to say to customers to conlude the conversation?.</span>","ALL")# 
								<cfelse>
									<div style="display:none">
										#RetVarRenderCP.HTML#	
									</div>
								</cfif>
							
								<!--- This is the HTML for a CP opject - HTML is maintained in RenderCP function --->	

			    			</cfloop>
			    			
			    			<!--- Give user a chance to add a CP at the bottom --->    			
			    			<!--- MaxCPCount = Master variable of CP postiions as list of items is read - regardless of CP type - each campaignData.ControlPoint_arr refers to this as ORDER --->
			    			<!--- Assign MaxCPCount + 1 to data-control-point-id  --->
			    			<!--- Special case button needs the extra div (id="cp#MaxCPCount#") so jquery insertBefore based on cp .attr('id') will work  --->
                           
                            <!---
                                <cfset MaxCPCount = MaxCPCount + 1 />
                                <div class="form-group clearfix control-point" id="cp#MaxCPCount#" data-control-point-physical-id="0" data-control-rq-id="MaxCPCount" data-control-point-type="NONE" >
                                    <div class="col-sm-12" style="text-align: center;">
                                        <button type="button" class="BuilderOption builder-btn btn btn-link control-point-btn-add" style="margin-bottom: .8em; opacity: 0.6; filter: alpha(opacity=60);"
                                            data-control-point-id="#MaxCPCount#" 
                                            data-control-point-type=""     									
                                        >&nbsp;</button>
                                    </div>
                                </div>
                            --->			
	   			 			
		    			</div>
		    			
		    					
		
	    			</div>
   			  	</cfif>  								
	    	</div>
		</form>
<!--- 	</section>
</main> Remove for NBE integrate--->
</div>

<!--- Set some globally avaialble javascript vars based on current data --->
<script TYPE="text/javascript">
	
	var INPBATCHID = '#campaignid#';
	var schedule_data = "";
	var SHORTCODE = "#shortCode.SHORTCODE#";
	
	var date_txt_1 = '#LSDateFormat(DateAdd("d", 30, Now()), "m-d-yyyy")#';
	var date_txt_2 = '#LSDateFormat(NOW(), "m-d-yyyy")#';

	var ScheduleTimeData;
	
	<!--- Set Global var for inpBatchId for Preview .js --->
	var inpBatchId = '<cfoutput>#campaignData.BatchId_bi#</cfoutput>'

</script>
</cfoutput>


	<!--- Store the structure of a Branch Condition Item here and use ToScript to output it in later javascript function --->
	<!--- Cleaner and easier to maintain as straight HTML with syntax highlighting intact--->
	<!--- Use jQuery to initialize any values --->
	<cfsavecontent variable="BranchConditionItemTemplate">
		<cfoutput>

			<div class="col-xs-12 mb5 ConditionItem" style="position:relative;">
			
				<input type="hidden" id="CID" value="" />
			
				<!--- Because the delete button is absolutely positions - make sure other objects dont block it via z-index --->
				<div style="position:absolute; top:0; right:0px; margin: 0 .2em 0 0; z-index: 6;">
					<h3 id="CIDLabel" class="" style="margin: 0 .2em 0 0; display: inline;" >## N</h3>
									
					<button type="button" class="btn btn-default RemoveCondition" style="padding: 3px 6px;">
						<span class="glyphicon glyphicon-minus" aria-hidden="true" ></span>
					</button>
					
				</div>
				
				<!--- Because the delete button is absolutely positions - make sure other objects dont block it via z-index --->		
				<div class="col-xs-12 col-sm-12 col-left-align" style="z-index: 5;">
				 	<label class="bc-title mbTight" >Rule Type</label>
													
					<select id="CTYPE" class="Select2" data-width="100%">
                        <option value="RESPONSE" selected>Single Selection Compare</option>
                        <option value="RESPONSE2">Response Compare</option>
                        <option value="CDF">Data Compare</option>		                            
                    </select>
				</div>						
		 		
				<div class="col-xs-12 col-sm-12">								
					<label class="bc-title mbTight">Question Answer to Compare</label>
					<select id="BOQ" class="Select2" data-width="100%">
                            <option value="0" selected>-- Loading Data ... --</option>
                    </select>
				</div>
				
				<div class="col-xs-12 col-sm-12">								    
                    
                    <label class="bc-title mbTight">Custom Data Field to Compare</label>
					<input id="BOCDV" value="" class="form-control" maxlength="1000" style="width:100%;">
												
			 	</div>
			 	
				<div class="col-xs-12 col-sm-12">
					
					<label class="bc-title mbTight">Comparison Type:</label>
													
					<select id="BOC" class="Select2" data-width="100%">	
						<option value="=" selected >Equal<!---- Use Answer Choice, Freeform CSV, or Freeform Regular expression. Note: NOT CASE sensitive---></option>
						<option value="LIKE" >Similar<!--- - Specify Static value, CSV, or Regular expression---></option>
						<option value="<" >Less than</option>
						<option value=">" >More than</option>
						<option value="<=" ><=</option>
						<option value=">=">>=</option>                                    
					</select>
									
				</div>
				
				<div class="col-xs-12 col-sm-12">								
					<label class="bc-title mbTight">Answer List</label>
					<select id="BOV" class="Select2" data-width="100%">
                            <option value="0" selected>-- Loading Data ... --</option>
                    </select>
				</div>
				
			 	<div class="col-xs-12 col-sm-12">								    
                    
                    <label class="bc-title mbTight">Answer Value(s) to Compare to</label>
					<textarea id="BOAV" value="" class="form-control" maxlength="1000" style="width:100%;" rows="3"></textarea>
												
			 	</div>
		 	
			 	<div class="col-xs-12 col-sm-12">								
					<label class="bc-title mbTight">Rule Matched Next Action</label>
					<select id="BOTNQ" class="Select2" data-width="100%">
                            <option value="0" selected>-- Loading Data ... --</option>
                    </select>
				</div>
				
			</div>
		
		</cfoutput>				
	</cfsavecontent>	
	
	<!--- <span class="AnswerItem-list-style">Answer List Style</span>    '+ getOptionListStyle(format_answer, answers_list.find('.AnswerItem').length) + ' --->
	<!--- Store the structure of a Answer Items here and use ToScript to output it in later javascript function --->
	<!--- Cleaner and easier to maintain as straight HTML with syntax highlighting intact--->
	<!--- Use jQuery to initialize any values --->
	<cfsavecontent variable="AnswerListItemTemplate">
		<cfoutput>
											
			<div class="AnswerItem" style="position:relative;"> 
				<div class="col-xs-4 col-sm-4 mb15">					
					<input id="OPTION" value="" class="form-control" maxlength="1000" style="width:100%;">
			 	</div>
			 	
			 	<div class="col-xs-3 col-sm-3 mb15">					
					<input id="AVAL" value="" class="form-control" maxlength="1000" style="width:100%;">
			 	</div>
			 	
			 	<div class="col-xs-3 col-sm-3 mb15">					
					<input type="hidden" id="AVALREG" value="" class="form-control" maxlength="1000" style="width:100%;">
										
<!--- 				<a id="AVALREGLINK" data-toggle="modal" href="##" data-target="##AVALREGEditModal">RegEx</a> --->
					<a id="AVALREGLINK" href="##a">Reg Exp Editor</a>
										
			 	</div>
			 	
				<div class="col-xs-2 col-sm-2 mb15">
			 		<button type="button" class="btn btn-default answer_remove">
						<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
					</button>
			 	</div> 
			</div>
		
		</cfoutput>				
	</cfsavecontent>
	
	<cfsavecontent variable="ExpertSystemListItemTemplate">
		<cfoutput>
							
							
			<!--- https://github.com/VerbalExpressions/JSVerbalExpressions --->				
			<!--- http://buildregex.com/  --->
			<div class="ESItem" style="position:relative;"> 
							 				 				 	
			 	<div class="col-sm-12 mb15">
															
					<input type="hidden" id="ESREG" value="" class="form-control" maxlength="1000" style="width:100%;">
										
<!--- 				<a iwd="AVALREGLINK" data-toggle="modal" href="##" data-target="##AVALREGEditModal">RegEx</a> --->
					<a id="ESREGLINK" href="##a">Reg Exp Editor</a>
										
			 	</div>

					<div id="ESTextSection" class="row">
								 																		
					<div class="col-xs-12"><lable for="ESTEXT" class="bc-title mbTight">Text to send:</lable></div>
					<div class="col-xs-12 mb15"><div class="Preview-Me Preview-Bubble" style="width: 90%; height: auto;"><textarea class="form-control" id="ESTEXT" maxlength="1000" rows="6" style="border-radius: 5px;"></textarea></div></div>
											
					<div class="col-xs-12 mb15" id="TDESCPLAIN"></div> 
					
				</div>	
									
			 	
				<div class="col-xs-2 col-sm-2 mb15">
			 		<button type="button" class="btn btn-default es_remove">
						<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
					</button>
			 	</div> 
			</div>
		
		</cfoutput>				
	</cfsavecontent>
		
	<!--- Store the structure of an Action Editor here and use ToScript to output it in later javascript function --->
	<!--- Cleaner and easier to maintain as straight HTML with syntax highlighting intact--->
	<!--- Use jQuery to initialize any values --->
	<cfsavecontent variable="CPEditorTemplate">
		<cfoutput>
			
			<div class="CPEContent <cfif swt EQ 0>modal-content</cfif>">
											
				<div id="CPSelector" class="modal-body">
					
					<button type="button" class="close btn-cancel-cpe" data-dismiss="modal" aria-label="Close" style="z-index: 6;"><span aria-hidden="true">&times;</span></button>
					
					<div class="modal-body-inner row" style="z-index: 5;">
				    	<h3 class="col-sm-12">What action do you want to add at this step?</h3>
				    	<p class="col-sm-12 alert-message">Please select one and then hit the CONTINUE button.</p>
				    	
				    	<div class="col-sm-12">        	
				        	<select id="AddNewCPType" name="AddNewCPType" size="9" style="width:100%;">		        	
					        	<option value="STATEMENT" selected>Simple Message</option>
					        	<option value="ONESELECTION">Question & Wait for Answer</option>
					        	<option value="TRAILER">Final Message - This will end the conversation</option>
					        	<option value="BRANCH">Rules Engine</option>
					        	<option value="INTERVAL">Action Interval</option>
					        	<option value="API">API Call</option>
								<option value="OPTIN">Capture Opt In</option> 
								<option value="CDF">Capture Custom Data Field</option>  
								<option value="ES">Expert System ("Chat Bot")</option>   
								<option value="RESET">Drip Marketing Reset</option>
				        	</select>	
				    	</div>
										
						<div class="col-sm-12" style="margin-top: 1em;"> 
					        <p class="col-sm-12 alert-message"><a data-toggle="modal" href="##" data-target="##InfoModalCP">What is an Action?</a></p>
					        <p class="col-sm-12 alert-message"><a data-toggle="modal" href="##" data-target="##InfoModalCPTypes">What are the different types of Actions?</a></p>
					    </div>
				            	
					</div>							   
								      
					<div class="modal-footer">
						<button type="button" class="btn btn-success btn-success-custom btn-add-cp" data-dismiss="modal"> CONTINUE </button>			
						<button type="button" class="btn btn-primary btn-back-custom btn-cancel-cpe">Cancel</button>
					</div>
				
				</div>

				<div id="CPEBody" class="<cfif swt EQ 0>modal-body</cfif> CPEBody" >
					
					<cfif swt EQ 0>
					<button type="button" class="close btn-cancel-cpe" data-dismiss="modal" aria-label="Close" style="z-index: 6;"><span aria-hidden="true">&times;</span></button>
					</cfif>
					<input type="hidden" id="ACTION" value="EDIT" />
		            <input type="hidden" id="RQ" value="" />
		            <input type="hidden" id="ID" value="" />
		            <input type="hidden" id="TYPE" value="" />
		            
		            	            		            
		            <div class="row BuilderOption">					
						<div class="col-xs-12 col-sm-12 mb15" style="z-index: 5;">
							<h4 class="modal-title title" style="display: inline;"></h4> <lable for="ITYPE" class="bc-title mbTight">Action: <a href="##x" id="ChangeCPType"><span id="CPTYPE"></span></a></lable>	
						</div>	
					</div>
		    								
					<div id="ActionTextSection" class="row">
													 																		
						<div class="col-xs-12"><lable for="CPText" class="bc-title mbTight">Text to send:</lable></div>
														            
						<div class="col-xs-12 mb15"><div class="Preview-Me Preview-Bubble" style="width: 90%; height: auto;"><textarea class="form-control" id="CPText" maxlength="1000" rows="6" style="border-radius: 5px;" data-emojiable="true"></textarea></div></div>
												
						<div class="col-xs-12 mb15" id="TDESCPLAIN"></div> 
						
					</div>	
					
					<!--- Outer option so BuilderOption can hide/show whole TDESC section --->
					<div class="BuilderOption" id="DescSectionOuter">
						<div class="row" id="DescLink">
													
							<div class="col-xs-12 mb15"><lable for="TDESC" class="bc-title mbTight"><a href="##x">Click here to add a description for this Action:</a></lable></div>
							
						</div>	
						
						<div class="row" id="DescSection">
													
							<div class="col-xs-4"><lable for="TDESC" class="bc-title mbTight">Description:</lable></div>
							<div class="col-xs-8"><h5 class="text-right control-point-char" style="margin-left:1em;"></h5></div>
							 
							<div class="col-xs-12 mb15"><textarea class="form-control" id="TDESC" maxlength="1500" rows="3"></textarea></div>
							
						</div>
												
					</div>		
					
					<div id="AnswerOptionsContainer" class="row">
						<div class="col-sm-12 mb15">
							
							
							<fieldset>
								<legend><a id="UserSelectableAnswerListSwitch" href="##x">Answer Choices List</a> <span style="font-size: .8em;"><i>(Optional)</i></span></legend>
								
								<div id="UserSelectableAnswerList">
									 
									<div class="col-sm-12" style="text-align: center;">
						    			<a id="SelectAnswerSet" href="##x">Insert Answer Template</a>					    								    			
					    			</div>
									
									<div id="AnswerTemplateSelector" class="modal-body">
										<button type="button" class="close btn-cancel-answer-list-template" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<div class="modal-body-inner row">
									    	<h3 class="col-sm-12">Select from a set of pre-formatted question answers</h3>
									    	<p class="col-sm-12 alert-message">Please select one and then hit the ADD button.</p>
									    	
									    	<div class="col-sm-12">        	
									        	<select id="ANSLISTTEMPLATE" class="select2" size="9" data-width="100%">	                                    
												    <option value="1" selected>Yes or No?</option>
												    <option value="2">True or False?</option>
												    <option value="3">Agree or Disagree?</option>
												    <option value="4">Useful or Not?</option>
												    <option value="5">Easy or Not?</option>
												    <option value="6">Likely or Not?</option>
												    <option value="7">Important or Not?</option>	
												    <option value="8">Satisfied or Not?</option>
												    <option value="9">Net Promoter Score</option>			                                                                                                        
												</select>	
									    	</div>
										        	
										</div>
																	   												      
										<div class="modal-footer">
											<button type="button" class="btn btn-success btn-success-custom btn-add-answer-list-template" data-dismiss="modal"> ADD </button>			
											<button type="button" class="btn btn-primary btn-back-custom btn-cancel-answer-list-template">Cancel</button>
										</div>
										
									</div>
			    			
									<div class="col-sm-12 mb15">
		
										<div><lable for="AF" class="bc-title mbTight">Format answers:</lable></div>
										<select id="AF" class="Select2" style="width: 12em;">	
											<option value="NOFORMAT" selected="selected">No Formatting</option>
											<option value="NUMERIC">1 for ..., 2 for ..., 3 for... </option>
											<option value="NUMERICPAR">1) ..., 2) ..., 3) ... </option>
											<option value="ALPHA">A for ..., B for ..., C for ...</option>
											<option value="ALPHAPAR">A) ..., B) ..., C) ...</option>
											<option value="HIDDEN">Hidden</option>
										</select>	
							
									</div>
							
									<div class="col-xs-4 col-sm-4">					
										<h5>Answer Text</h5>
								 	</div>
								 	
								 	<div class="col-xs-3 col-sm-3">					
										<h5 style="white-space: nowrap; overflow: hidden;"><a data-toggle="modal" href="##" data-target="##InfoModalAVal">S-Val</a></h5>
								 	</div>
								 	
								 	<div class="col-xs-3 col-sm-3">					
										<h5 style="white-space: nowrap; overflow: hidden;"><a data-toggle="modal" href="##" data-target="##InfoModalRegExp">RegExp</a></h5>
								 	</div>
								 	
									<div class="col-xs-2 col-sm-2">
										
									</div>
						
									<div id="answers_list" class="mb15"></div>
									<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1">
										<button type="button" id="answer_add" class="col-xs-11 col-sm-11 mb5 btn btn-default"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
									</div>							
								
								</div>
								
							</fieldset>
													 
						</div>
						
					</div>
					
					<div class="">
					
						<div id="BranchOptionsContainer" class="row BuilderOption">
							<div class="col-sm-12 mb15">
														
								<fieldset>
									<h4>Business Logic Conditions</h4>  
									
									<p class="col-sm-12 alert-message"><a data-toggle="modal" href="##" data-target="##InfoModalBusinessRules">What are Business Logic Conditions?</a></p>
														
									<div id="ConditionsList" class="col-xs-12 col-sm-12 mb15"></div>
									<div class="col-xs-12 col-sm-12">
										<label class="bc-title mbTight">Add Another Condition</label>
										<button type="button" id="AddCondition" class="col-xs-12 col-sm-12 mb5 btn btn-default"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
									</div>	
																		
									<div class="col-xs-12 col-sm-12">								
										<label class="bc-title mbTight">No Rule(s) Matched Next Action</label>
										<select id="BOFNQ" class="Select2" data-width="100%">
					                            <option value="0" selected>-- Loading Data ... --</option>
					                    </select>
									</div>
									
								</fieldset>
														 
							</div>
							
						</div>					
										
						<div id="IntervalOptions" class="row"> 					
																		    			
							<div class="col-sm-12 mb15 <cfif swt EQ 0>BuilderOption</cfif>" id="IntervalOptionsSubSection">
												    			
								<fieldset>
									<legend class="mb5">
										<span class="ResponseInterval"><a id="IntervalSettingSectionSwitch" href="##x">Response Interval Settings</a> <span style="font-size: .8em; color:##333;"><i>(Optional)</i></span></span> 
										<span class="ControlPointInterval">Action Interval Settings</span> 
									</legend>
									
									<div id="IntervalSettingSection" class="col-sm-12 mb15" <cfif swt EQ 1>style="display:none;"</cfif> >
									
										<div class="mb15 ResponseInterval" style="width:100%;">
											<a data-toggle="modal" href="##" data-target="##InfoModalQuestionInterval">What is this?</a></span>
										</div>	
										
										<div class="mb15 ControlPointInterval" style="width:100%;">
											<a data-toggle="modal" href="##" data-target="##InfoModalCPInterval">What is this?</a>
										</div>	
																							
										<label for="ITYPE" class="bc-title mbTight">Select Interval Type:</label>						
										<select id="ITYPE" class="Select2" data-width="100%">	
											<option value="SECONDS">Seconds(s)</option>
											<option value="MINUTES">Minute(s)</option>
											<option value="HOURS">Hour(s)</option>
											<option value="DAYS">Day(s)</option>
											<option value="WEEKS">Week(s)</option>
											<option value="MONTHS">Month(s)</option> 
											<option value="TODAY">Today at xx:xx</option> 
											<option value="WEEKDAY">Weekday(s) M-F</option> 
											<option value="DATE">Specific Date</option> 
											<option value="SUN">Sunday</option> 
											<option value="MON">Monday</option> 
											<option value="TUE">Tuesday</option> 
											<option value="WED">Wednesday</option> 
											<option value="THU">Thursday</option> 
											<option value="FRI">Friday</option> 
											<option value="SAT">Saturday</option> 
										</select>		
										
										<div id="IVALUE_Container">
											<label for="IVALUE" class="bc-title mbTight" id="IVALUE_Label">How many:</label>
				                            <select id="IVALUE" class="Select2" data-width="100%">	                                    
				                                <option value="0">0</option>
				                                <option value="1">1</option>
				                                <option value="2">2</option>
				                                <option value="3">3</option>
				                                <option value="4">4</option>
				                                <option value="5">5</option>
				                                <option value="6">6</option>
				                                <option value="7">7</option>
				                                <option value="8">8</option>
				                                <option value="9">9</option>
				                                <option value="10">10</option>
				                                <option value="11">11</option>
				                                <option value="12">12</option>
				                                <option value="13">13</option>
				                                <option value="14">14</option>
				                                <option value="15">15</option>
				                                <option value="16">16</option>
				                                <option value="17">17</option>
				                                <option value="18">18</option>
				                                <option value="19">19</option>
				                                <option value="20">20</option>
				                                <option value="21">21</option>
				                                <option value="22">22</option>
				                                <option value="23">23</option>
				                                <option value="24">24</option>
				                                <option value="25">25</option>
				                                <option value="26">26</option>
				                                <option value="27">27</option>
				                                <option value="28">28</option>
				                                <option value="29">29</option>
				                                <option value="30">30</option>
				                                <option value="31">31</option>
				                                <option value="32">32</option>
				                                <option value="33">33</option>
				                                <option value="34">34</option>
				                                <option value="35">35</option>
				                                <option value="36">36</option>
				                                <option value="37">37</option>
				                                <option value="38">38</option>
				                                <option value="39">39</option>
				                                <option value="40">40</option>
				                                <option value="41">41</option>
				                                <option value="42">42</option>
				                                <option value="43">43</option>
				                                <option value="44">44</option>
				                                <option value="45">45</option>
				                                <option value="46">46</option>
				                                <option value="47">47</option>
				                                <option value="48">48</option>
				                                <option value="49">49</option>
				                                <option value="50">50</option>
				                                <option value="51">51</option>
				                                <option value="52">52</option>
				                                <option value="53">53</option>
				                                <option value="54">54</option>
				                                <option value="55">55</option>
				                                <option value="56">56</option>
				                                <option value="57">57</option>
				                                <option value="58">58</option>
				                                <option value="59">59</option>
				                                <option value="60">60</option>                                                                                                       
				                            </select>
			                        	</div>
			                        				
			                        	
			                        	<div id="IntervalTimeContainer">
					                        					                           
					                    	<label class="bc-title mbTight">Interval Timeout Time: Time zone Specific to Phone Number</label>
					                        
					                        <div style="width: 100%;"> 
						                        <div style="display: inline-block; clear: both;">       
					                                <select id="IHOUR" class="Select2" style="width:5em;" data-width="100%">
					                                    <option value="0" SELECTED="SELECTED">Hour</option>
					                                    <option value="00">12</option>
					                                    <option value="01">01</option>
					                                    <option value="02">02</option>
					                                    <option value="03">03</option>
					                                    <option value="04">04</option>
					                                    <option value="05">05</option>
					                                    <option value="06">06</option>
					                                    <option value="07">07</option>
					                                    <option value="08">08</option>
					                                    <option value="09">09</option>
					                                    <option value="10">10</option>
					                                    <option value="11">11</option>
					                                </select>
						                        </div>
				                        
					                            <div style="display: inline-block; clear: both;"> 
						                                <select id="IMIN" class="Select2" style="width:5em;" data-width="100%">
					                                    <option value="0" SELECTED="SELECTED">Min</option>
					                                    <option value="00">00</option>
					                                    <option value="01">01</option>
					                                    <option value="02">02</option>
					                                    <option value="03">03</option>
					                                    <option value="04">04</option>
					                                    <option value="05">05</option>
					                                    <option value="06">06</option>
					                                    <option value="07">07</option>
					                                    <option value="08">08</option>
					                                    <option value="09">09</option>
					                                    <option value="10">10</option>
					                                    <option value="11">11</option>
					                                    <option value="12">12</option>
					                                    <option value="13">13</option>
					                                    <option value="14">14</option>
					                                    <option value="15">15</option>
					                                    <option value="16">16</option>
					                                    <option value="17">17</option>
					                                    <option value="18">18</option>
					                                    <option value="19">19</option>
					                                    <option value="20">20</option>
					                                    <option value="21">21</option>
					                                    <option value="22">22</option>
					                                    <option value="23">23</option>
					                                    <option value="24">24</option>
					                                    <option value="25">25</option>
					                                    <option value="26">26</option>
					                                    <option value="27">27</option>
					                                    <option value="28">28</option>
					                                    <option value="29">29</option>
					                                    <option value="30">30</option>
					                                    <option value="31">31</option>
					                                    <option value="32">32</option>
					                                    <option value="33">33</option>
					                                    <option value="34">34</option>
					                                    <option value="35">35</option>
					                                    <option value="36">36</option>
					                                    <option value="37">37</option>
					                                    <option value="38">38</option>
					                                    <option value="39">39</option>
					                                    <option value="40">40</option>
					                                    <option value="41">41</option>
					                                    <option value="42">42</option>
					                                    <option value="43">43</option>
					                                    <option value="44">44</option>
					                                    <option value="45">45</option>
					                                    <option value="46">46</option>
					                                    <option value="47">47</option>
					                                    <option value="48">48</option>
					                                    <option value="49">49</option>
					                                    <option value="50">50</option>
					                                    <option value="51">51</option>
					                                    <option value="52">52</option>
					                                    <option value="53">53</option>
					                                    <option value="54">54</option>
					                                    <option value="55">55</option>
					                                    <option value="56">56</option>
					                                    <option value="57">57</option>
					                                    <option value="58">58</option>
					                                    <option value="59">59</option>
					                                </select>
					                            </div>
					                            
					                            <div style="display: inline-block; clear: both;"> 
					                                <select id="INOON" class="Select2" style="width:5em;" data-width="100%"> 
					                                    <option value="01" selected>PM</option>
					                                    <option value="00">AM</option>				                                    
					                                </select>
					                            </div>
					                                
					                        </div>    
					                            
					                    </div>		
			
										<label class="bc-title mbTight">Interval Expired: Next Action</label>
										<select id="IENQID" class="Select2" data-width="100%">
			                                    <option value="0" selected>-- Loading Data ... --</option>
			                            </select>
			                        	
			                        	<label class="bc-title mbTight">Retry on No Response Option</label>
										<select id="IMRNR" class="Select2" data-width="100%">
			                                    <option value="0" selected>No Retry</option>
			                                    <option value="1">Retry 1 time</option>
			                                    <option value="2">Retry 2 times</option>
			                                    <option value="3">Retry 3 times</option>
			                            </select>
			                            
			                            <label class="bc-title mbTight">Still No Response After Maximum Retries Option</label>
										<select id="INRMO" class="Select2" data-width="100%">
			                                <option value="END">End Conversation</option>
			                                <option value="NEXT" selected>Proceed to Next Action</option>
			                            </select>
			                            
									</div>	   			
								</fieldset>
							
							</div>					
						</div>
						
						<div id="APIOptions" class="row"> 					
							
							<div class="col-sm-12 mb15">
								
								<fieldset>
									<legend>
										<span id="API">API <!--- <a data-toggle="modal" href="##" data-target="##InfoModalAPI">What is this?</a> ---></span>
									</legend>
																		
									<!--- 
										
										<Q AF='NOFORMAT' API_ACT='JSON' API_DATA='sd;lafsaldjasjkd' API_DIR='someplace/something' API_DOM='somwhere.com' API_PORT='80' API_RA='STORE' API_TYPE='GET' GID='1' ID='6' IENQID='0' IHOUR='0' IMIN='0' IMRNR='2' INOON='0' INRMO='END' ITYPE='MINUTES' IVALUE='0' MDF='0' OIG='0' REQANS='undefined' RQ='1' TDESC='' TEXT='Reset' TYPE='API' errMsgTxt='undefined'></Q>
										
									--->
																						
									<label for="API_RA" class="bc-title mbTight">Action with API Results</label>										
									<select id="API_RA" class="Select2" data-width="100%"> 	
			                            <option value="SEND" selected="selected">Send result data to user</option>
			                            <option value="STORE">Just Store Results</option>                                 
			                        </select>
                        			                        			
                        			<label for="API_TYPE" class="bc-title mbTight">REST API Verb</label>										
									<select id="API_TYPE" class="Select2" data-width="100%">	
			                            <option value="GET" selected="selected">GET</option>
										<option value="POST">POST</option>                              
			                        </select>
                        			
                        			<div class="">	
					                    <label class="bc-title mbTight">Domain</label>
										<input id="API_DOM" value="" class="form-control" maxlength="1000" style="width:100%;">																	
								 	</div>
								 	
								 	<div class="">	
					                    <label class="bc-title mbTight">Directory/File</label>
										<input id="API_DIR" value="" class="form-control" maxlength="1000" style="width:100%;">																	
								 	</div>
								 	
								 	<div class="">	
					                    <label class="bc-title mbTight">PORT</label>
										<input id="API_PORT" value="" class="form-control" maxlength="1000" style="width:100%;">																	
								 	</div>
			 				 	
                        			<label for="API_ACT" class="bc-title mbTight">Additional Content to Send Type</label>										
									<select id="API_ACT" class="Select2">	
			                            <option value="TEXT" selected="selected">Plain Text</option>
			                            <option value="JSON">JSON</option>  
			                            <option value="XML">XML</option>                             
			                        </select>
                        			                        												                        				
		                        	<div class="">		
					                    <label class="bc-title mbTight">Additional Content To Send</label>
										<textarea id="API_DATA" value="" class="form-control" maxlength="1000" style="width:100%;" rows="3"></textarea>												
								 	</div>	
		                            			
								</fieldset>
							
							</div>					
						</div>
						
					</div>						
						
					<div id="ESOptions" class="row"> 					
							
							<div class="col-sm-12 mb15">
								
								<fieldset>
									<legend>
										<span id="ES">Expert System ("Chat Bot") <!--- <a data-toggle="modal" href="##" data-target="##InfoModalExpertSystem">What is this?</a> ---></span>
									</legend>
									
									
									
									
									<!--- 
										
										<Q AF='NOFORMAT' API_ACT='JSON' API_DATA='sd;lafsaldjasjkd' API_DIR='someplace/something' API_DOM='somwhere.com' API_PORT='80' API_RA='STORE' API_TYPE='GET' GID='1' ID='6' IENQID='0' IHOUR='0' IMIN='0' IMRNR='2' INOON='0' INRMO='END' ITYPE='MINUTES' IVALUE='0' MDF='0' OIG='0' REQANS='undefined' RQ='1' TDESC='' TEXT='Reset' TYPE='API' errMsgTxt='undefined'></Q>
										
									--->
									
									<div id="es_list" class="mb15"></div>
									<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1">
										<button type="button" id="es_add" class="col-xs-11 col-sm-11 mb5 btn btn-default"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
									</div>
									
											                            			
								</fieldset>
							
							</div>					
						</div>
						
					</div>						
								
					<div id="CDFContainer" >						
						<div class="col-xs-12 col-sm-12">							
							<label class="bc-title mbTight">Select Custom Field to Store Last Response</label>
							<select id="SCDF" class="Select2" data-width="100%">
								
							</select>
						</div>
						
						<div class="col-xs-12 col-sm-12">
							<label class="bc-title mbTight">Don't see a CDF you want? <br class="visible-xs"/>Try Adding a New one.</label>
							<button type="button" id="btn-add-cdf" class="btn btn-success btn-success-custom" style="margin-bottom: 1em; margin-top: 1em"> Add </button>
						</div>	
						
						<div class="col-sm-12" style="margin-top: 1em;"> 
					        <p class="col-sm-12 alert-message"><a data-toggle="modal" href="##" data-target="##InfoModalCDF">What is a CDF?</a></p>					        
					    </div>
					    
					</div>
					
					<div id="SubscriberListContainer">							
						<div class="col-xs-12 col-sm-12">							
							<label class="bc-title mbTight">Select Subscriber List To Opt In To</label>
							<select id="OIG" class="Select2" data-width="100%">
								
							</select>						
						</div>
						
						<div class="col-xs-12 col-sm-12">
							<label class="bc-title mbTight">Don't see a Subscriber List you want? <br class="visible-xs"/>Try Adding a New one.</label>
							<button type="button" id="btn-add-group" class="btn btn-success btn-success-custom" style="margin-bottom: 1em; margin-top: 1em"> Add </button>
						</div>	
						
						<div class="col-xs-12 col-sm-12">
				    		<a data-toggle="modal" href="##" data-target="##InfoModalSubscriber" class="ConsoleLinks" style="padding-left:0em;">What is a Subscriber List?</a>
						</div>				    									
					</div>
					
					<cfif inpTemplateFlag NEQ 0>
						
						<div id="SWTSection" class="row" style="margin-bottom: 1em;">
								
							<div class="col-sm-12">        	
						        <label for="SWT" class="bc-title mbTight" id="SWT_Label">Show this section on Simplified Walk Through:</label>
	
					        	<select id="SWT" class="Select2" size="1" data-width="100%">	                                    
								    <option value="0">No</option>
								    <option value="1" selected>Yes</option>							    
								</select>	
					    	</div>
				    	
						</div>				    	
			    	<cfelse>
			    		<input type="hidden" id="SWT" value="1" />
					</cfif>
					    
					<!--- Hide these option on walkthrough --->  
	                <div class="CPEFooter modal-footer" <cfif swt EQ 1> style="visibility: hidden; position: absolute"</cfif>>
		                <button type="button" class="btn btn-success btn-success-custom btn-save-cp"> Save </button>
		                &nbsp; &nbsp; &nbsp; 
		                <button type="button" class="btn btn-primary btn-back-custom btn-cancel-cpe">Cancel</button>
		            </div>
		            
		            
		        
		        </div>		            
			</div>
		</cfoutput>				
	</cfsavecontent>	
			

<!--- Answer Match RegEx Editor --->
<div class="bootbox modal fade" id="AVALREGEditModal" tabindex="-1" role="dialog" aria-labelledby="AVALREGEditModal" aria-hidden="true">
    <form action="##" name="form-aval-regedit-campaign" id="form-aval-regedit-campaign" autocomplete="off">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Answer Match Editor</h4>
                </div>
                <div class="modal-body">
                    
                	<div class="col-xs-12 mb15">
						<div class="col-xs-12"><lable class="bc-title mbTight">Answer Option:</lable></div>
						<div class="col-xs-12 mb15" id="AVALREGEditAnswerText"></div>
					</div>	
					
					
					<div class="col-xs-12">	
						<!--- List common answers and counts --->
						<!--- List NO MATCH answeres and counts --->
					</div>	
						
					
					<div class="col-xs-12">						 																		
						<div class="col-xs-12"><lable class="bc-title mbTight">Regular Expression:</lable></div>
						<div class="col-xs-12 mb15"><div class="" style="height: auto;"><textarea class="form-control" id="AVALREGEdit" maxlength="1000" rows="6" style="border-radius: 5px;"></textarea></div></div>
					</div>
					    
                </div>
                <div class="modal-footer">
                     <button type="button" id="btn-save-aval-regex" class="btn btn-success btn-success-custom btn-save-aval-regex"> Save </button>
	                &nbsp; &nbsp; &nbsp; 
	                <button type="button" class="btn btn-primary btn-back-custom btn-cancel-aval-regex" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </form>
</div>



<!--- Expert System Match RegEx Editor --->
<div class="bootbox modal fade" id="ESREGEditModal" tabindex="-1" role="dialog" aria-labelledby="ESREGEditModal" aria-hidden="true">
    <form action="##" name="form-aval-regedit-campaign" id="form-aval-regedit-campaign" autocomplete="off">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Expert System (REGEX) Editor</h4>
                </div>
                <div class="modal-body">
                    
                    
              				
					
					<div class="col-xs-12">						 																		
						<div class="col-xs-12"><lable class="bc-title mbTight">Regular Expression:</lable></div>
						<div class="col-xs-12 mb15"><div class="" style="height: auto;"><textarea class="form-control" id="ESREGEdit" maxlength="1000" rows="6" style="border-radius: 5px;"></textarea></div></div>
					</div>
					    
                </div>
                <div class="modal-footer">
                     <button type="button" id="btn-save-es-regex" class="btn btn-success btn-success-custom btn-save-es-regex"> Save </button>
	                &nbsp; &nbsp; &nbsp; 
	                <button type="button" class="btn btn-primary btn-back-custom btn-cancel-es-regex" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </form>
</div>


<!--- CLONE CAMPAIGN --->
<div class="bootbox modal fade" id="cloneCampaignModal" tabindex="-1" role="dialog" aria-labelledby="cloneCampaignModal" aria-hidden="true">
    <form action="##" name="form-clone-campaign" id="form-clone-campaign" autocomplete="off">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Clone Campaign</h4>
                </div>
                <div class="modal-body">
                    
                    <div class="col-sm-12" style="margin-top: 1em;"> 
					    <p class="col-sm-12 alert-message">Are you sure?</p>					        
					</div>
					    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success btn-success-custom" id="btn-clone-campaign">Clone</button>
                    &nbsp; &nbsp; &nbsp;
                    <button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </form>
</div>

<!-- Modal -->
<div class="bootbox modal fade" id="AddNewSubscriberList" tabindex="-1" role="dialog" aria-labelledby="AddNewSubscriberList" aria-hidden="true">
    
    <div class="modal-dialog">
    	<form id="subscriber_list_form_add" class="row">
	        <div class="modal-content" id="add-sub-list-modal-content">
	        	<div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
	                <h4 class="modal-title title">Add New Subscriber List</h4>            
	            </div>
	        	<div class="modal-body">
	        		<div class="row">
		        		<div class="col-sm-4">
		        			<label for="subscriber_list_name">
		        				Subscriber List Name:*
		        			</label>
		        		</div>
	        			<div class="col-sm-8">
	        				<input id="subscriber_list_name" class="form-control validate[required, custom[onlyLetterNumberSp]]" maxlength="255">
	        			</div>
        			</div>
	           	</div>
	            <div class="modal-footer">
	                <button type="submit" id="btn-save-group" class="btn btn-success btn-success-custom"> Save </button>
	                &nbsp; &nbsp; &nbsp; 
	                <button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal">Cancel</button>
	            </div>
	        </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="bootbox modal fade" id="AddNewCDFModal" tabindex="-1" role="dialog" aria-labelledby="AddNewCDFModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        	<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title title">Add New Custom Data Field (CDF) to List</h4>            
            </div>
        	<div class="modal-body">
        		<form id="subscriber_list_form" class="row">
        			<label for="subscriber_list_name" class="col-sm-4">
        				CDF Name:*
        			</label>
        			<div class="col-sm-8">
        				<input id="inpCDFName" class="form-control" maxlength="255">
        			</div>
        		</form>
           	</div>
            <div class="modal-footer">
                <button type="button" id="btn-save-cdf" class="btn btn-success btn-success-custom"> Save </button>
                &nbsp; &nbsp; &nbsp; 
                <button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="ScheduleOptionsModal" tabindex="-1" role="dialog" aria-labelledby="ScheduleOptionsModal" aria-hidden="true">
    <div class="modal-dialog" style="width:auto; max-width:900px;">
        <div class="modal-content">
        	<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>            
				<h4 class="modal-title title">Schedule Options</h4>
            </div>
            <cfinclude template="dsp_advance_schedule.cfm">
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="ConfirmLaunchModal" tabindex="-1" role="dialog" aria-labelledby="ConfirmLaunchModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        	<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Confirm Launch Campaign</h4>            
            </div>
			<div class="modal-body clearfix">
				
				<div id="ProcessingBlast" style="display: none; min-height: 150px;">
					<h4>Your blast is loading into queue now:</h4>
					<img src="/public/sire/images/loading.gif" class="ajax-loader">
				</div>

				<!--- Confirmation Display --->
				<div class="ConfirmationSummary">
					<div class="form-group clearfix">
						<div class="col-xs-9 col-sm-6">Number of Subscribers:</div>
						<div class="col-xs-3 col-sm-6" id="confirm-number-subscribers">0</div>
					</div>
					
					<div class="form-group clearfix">
						<div class="col-xs-9 col-sm-6">Already Blasted this Campaign:</div>
						<div class="col-xs-3 col-sm-6" id="confirm-list-duplicates">0</div>
					</div>
					
					<div class="form-group clearfix">
						<div class="col-xs-9 col-sm-6">Total Eligible:</div>
						<div class="col-xs-3 col-sm-6" id="confirm-total-elligable">0</div>
					</div>
					
					<div class="form-group clearfix">
						<div class="col-xs-9 col-sm-6">Credits Needed:</div>
						<div class="col-xs-3 col-sm-6" id="confirm-credits-needed">0</div>
					</div>
					
					<div class="form-group clearfix">
						<div class="col-xs-9 col-sm-6">Credits Available:</div>
						<div class="col-xs-3 col-sm-6" id="confirm-credits-available">0</div>
					</div>
								
					<div id="confirm-launch-alert-text" class="clearfix" style="display: none;">
						<div class="col-xs-12 col-sm-12">
							<!--- /session/sire/pages/buy-sms --->
							<div class="alert alert-info" role="alert">
								You do not have enough credits to launch this campaign.<br>
								Please <a id="confirm-launch-buy-credits" href="/session/sire/pages/my-plan?active=addon" target="_blank">click here</a> to buy more credits.
							</div>
						</div>
					</div>
					
				</div>
				
			</div>
			<div class="modal-footer ConfirmationSummary">
				<button type="button" class="btn btn-success btn-success-custom" id="btn-confirm-save-and-send">Confirm Blast & Send Now!</button>
                &nbsp;
                <button type="button" class="btn btn-primary btn-back-custom" id="btn-cancel-send" data-dismiss="modal">Cancel</button>
			</div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!--- Save campaign befor preview --->
<div class="modal fade" id="ConfirmSaveModal" tabindex="-1" role="dialog" aria-labelledby="ConfirmSaveModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-body-inner row">
        	<h4 class="col-sm-10 modal-title">Preview</h4>
        	<p class="col-sm-12 alert-message">Please save the campaign for preview.</p>
        </div>
      </div>
		<div class="modal-footer">
			<button type="button" class="btn btn-success btn-success-custom" id="btn-save-for-preview">Save & Preview</button>
	        &nbsp; &nbsp; &nbsp; 
	        <button type="button" class="btn btn-primary btn-back-custom" id="btn-cancel-for-preview" data-dismiss="modal">Cancel</button>
		</div>
    </div>
  </div>
</div>


<!-- Modal -->
<div id="InfoModalRegExp" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;"> 
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What is a Regular Expression?</h4>
      </div>
      <div class="modal-body">
	      
	      <p class="MedFont"><b>Regular Expression</b> (Optional) If the user's answer matches the supplied regular expression, the answer value will be stored along with the actual answer. The order of comparison is done with the first possible answer and stops when a match is found.</p>
				
		  <p>A regular expression, often called a pattern, is an expression used to specify a set of strings required for a particular purpose.</p>
		  
		  <p>You are not required to use or know regular expression, but if you want to use use your own you will need to be familiar with the concepts. Google is your friend.</p>
		  
		  <p>You should always test your regular expression to make sure they behave as you expect.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="InfoModalAVal" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;"> 
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What is a Stored Value (S-Val)?</h4>
      </div>
      <div class="modal-body">
	      
	      <p class="MedFont"><b>Stored Values</b> Additional values associated with possible user inputs can be stored in the results. You can assign numerical values for later use in analytical processing, or you can report on best match based on a range of possible user inputs.</p>
		 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- Modal -->
<div id="InfoModalKeyword" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;"> 
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What is a Keyword?</h4>
      </div>
      <div class="modal-body">
	      
	      <p class="MedFont"><b>Keywords</b> are one of the fastest ways you can begin to communicate with your customers. <b>Keywords</b> are special messages sent to a Short Code. <b>Keywords</b> are used to trigger new Sire SMS sessions. Sessions can include simple response messages, list building via opt in confirmation, surveys, meet-ups, drip marketing, and additional two-way data exchange and capture.</p>
					
		  <img src="/public/sire/images/learning/keyword-special-simple.png" style="width: 100%; max-width:533px; height: auto;" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="InfoModalAPI" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;"> 
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What is an API Trigger?</h4>
      </div>
      <div class="modal-body">
	   
	   		<h4>Advanced users and/or Developers can trigger SMS message flows with dynamic variables via API calls. The Sire engine is then used to deliver personalized interactive content, while the Sire Campaigns can be used as an SMS Content Management System.</h4>
		    <p>To trigger a Transactional SMS, you need to send information via an HTTPS request to the URL address <b>https://api.siremobile.com/ire/secure/triggerSMS</b>.  See the Transactional API tutorial under Learning and Support for more information.</p>
                          
	   
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="InfoModalExpertSystem" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;"> 
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What is an Expert System ("Chat Bot") Trigger?</h4>
      </div>
      <div class="modal-body">
	   
	   		<h4>The Expert System ("Chat Bot") is supposed to exchange data between the the Sire server and the Expert System control points, resulting in responses based on a set of possible questions, or statements. </h4>
		    <p>In artificial intelligence, an expert system is a computer system that emulates the decision-making ability of a human expert. Expert systems are designed to solve complex problems by reasoning about knowledge, represented mainly as if–then rules rather than through conventional procedural code.

</p>
                          
	   
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="InfoModalCampaignName" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;"> 
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What makes a good Campaign Name?</h4>
      </div>
      <div class="modal-body">
	      
	     
			Coming up with a name for your campaign isn’t that difficult, but you can future-proof and organize your campaigns by utilizing clear naming conventions… just like a pro.	
			
			<h3>Benefits of Using a Naming Convention</h3>
			<p>Being disciplined about how you name your campaigns has several notable benefits:</p>
			
			<ul>
				<li><h4>Future-proof Your Account</h4>Even if your account grows to include hundreds of campaigns, a consistent naming convention ensures the it remains easy to manage. You’ll be able to find existing campaigns easily and identify opportunities for new campaigns quickly.</li> 
				
				<li><h4>Shareable</h4>Using a clear naming structure also makes your account structure easily understandable by others that may work on your account in the future. Collaboration becomes easy because colleagues will know exactly what a campaign is meant to do.</li>
				
				<li><h4>Filtering</h4>You can also search for, or filter by, parts of the campaign name (e.g., You can filter by campaigns that target the US by search for campaign names that contain “CA -“). </li>
				
				<li><h4>Group Similar Campaigns</h4>Once your naming convention is in place, you can group similar campaigns together quickly and easily. All it takes is a quick click on the campaign name header to sort the names alphabetically.												
				<br><i>Pro tip: Sorting similar campaigns works best if you start your naming structure with the most common settings first and then move on to the more unique settings (e.g., start with “target audience type” and end with the “main keyword”).</i></li>
				
				<li><h4>High-level Reporting</h4>When similar campaigns are easily sorted and filtered by name, spotting trends becomes easier</li>
			</ul>
		
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="InfoModalSubscriber" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;"> 
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What is a Subscriber List?</h4>
      </div>
      <div class="modal-body">
	      
	      <p class="MedFont">A <b>Subscriber List</b> is a list of subscribers that have opted into your campaign by texting your keyword to your short code. Example:“39492”. Sire automatically captures all of your subscribers phone numbers and saves them in your subscriber list. This is also to help keep non-compliant spammers at bay.</p>
					
		  <img src="/public/sire/images/learning/opt-in.jpg" style="width: 100%; max-width:600px; height: auto;" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="InfoModalBlast" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;"> 
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What  is a blast?</h4>
      </div>
      <div class="modal-body">	      
	      
		<h4>With SMS blast, you can send a message to an entire subscriber list without using a template or keyword.	</h4>	
			
		<p>Messages can be static or the start of a survey or other interactive programs.</p>	
		<p>Subscriber list is required:</p>
		<p class="MedFont">A <b>Subscriber List</b> is a list of subscribers that have opted into your campaign by texting your keyword to your short code. Example:“39492”. Sire automatically captures all of your subscribers phone numbers and saves them in your subscriber list. This is also to help keep non-compliant spammers at bay.</p>
		
		
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!--- Custom Message --->
<!-- Modal -->
<div class="bootbox modal fade" id="CustomMessageModal" tabindex="-1" role="dialog" aria-labelledby="CustomMessageModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        	<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title title">Help/Stop Message</h4>
<!---                 <h5 class="">Tools for Compliance</h5>  --->           
            </div>
        	<div class="modal-body">
    			<div class="row">
    				<cfoutput>
    				<div class="col-sm-12">
    					<label for="CustomHelpMessage_vch" class="col-sm-4 control-label">Help Message</label>
    					<div class="col-sm-8">
							<textarea class="form-control" id="CustomHelpMessage_vch" name="CustomHelpMessage_vch" maxlength="160" rows="3">#campaignData.CustomHelpMessage_vch#</textarea>
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-12 cbic">
    					<div class="row">
						 	&nbsp;
				 		</div> 
					</div>
					
    				<div class="col-sm-12">
    					<label for="CustomStopMessage_vch" class="col-sm-4 control-label">Stop Message</label>
    					<div class="col-sm-8">
							<textarea class="form-control" id="CustomStopMessage_vch" name="CustomStopMessage_vch" maxlength="160" rows="3">#campaignData.CustomStopMessage_vch#</textarea>
						</div>
    				</div>
    				</cfoutput>
    				
    				<div class="col-xs-12 col-sm-12 cbic">
    					<div class="row">
						 	&nbsp;
				 		</div> 
						<hr class="hrt0 hrb10">
					</div>
					
    				<div class="col-sm-12">
					<!---<p>All campaigns must respond with messages for users who send <b>HELP</b> and <b>STOP</b> keywords.</p><p>By default we will use the messages that you  setup in your organizational profile on your <a href="my-account">MY ACCOUNT->SETTINGS</a> page.</p><p>You may also specify custom <b>HELP</b> and <b>STOP</b> response messages for each campaign.</p> --->
    					<p>All campaigns must respond with messages for users who send <b>HELP</b> and <b>STOP</b> keywords.</p><p>By default we will use the messages that you  setup in your organizational profile on your <a href="profile">MY ACCOUNT->SETTINGS</a> page.</p><p>You may also specify custom <b>HELP</b> and <b>STOP</b> response messages for each campaign.</p>
    				</div>
    				
    			</div>
           	</div>
            <div class="modal-footer">
	            
	            <button type="button" class="btn btn-success btn-success-custom" id="btn-save-custom-responses">Save</button>
	        &nbsp; &nbsp; &nbsp; 
	        	<button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal">Cancel</button>
	        
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!--- Alert popup --->
<div class="modal fade" id="bootstrapAlert" tabindex="-1" role="dialog" aria-labelledby="alertLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-body-inner row">
        	<h4 class="col-sm-10 modal-title"></h4>
        	<p class="col-sm-12 alert-message">       		
        	</p>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div id="InfoModalQuestionInterval" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;"> 
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What is a Response Interval?</h4>
      </div>
      <div class="modal-body">
      	<p><b>Response Intervals</b> are how long you wish to wait for a message recipient to reply. By default (0) the wait time is infinite. <b>Response Intervals</b> give you dynamic control on how long you wish to wait before moving on to the next specified Action if no response is received. </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="InfoModalCPInterval" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;"> 
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What is an Action Interval?</h4>
      </div>
      <div class="modal-body">
      	<p><b>Action Intervals</b> are how long you wish to wait  until moving on to the next Action. By default (0) the wait time is infinite. <b>Action Intervals</b> give you dynamic control on how long you wish to wait before moving on to the next specified Action. Responses are ignored on this session until the <b>Action Intervals</b> expires. </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="InfoModalCP" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;"> 
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What is an Action?</h4>
      </div>
      <div class="modal-body">
      	<p>When building or modifying a Campaign flow, each point in the conversation is called an <b>Action</b>. An <b>Action</b> is the place in the message flow you are at. All message flows begin at  <b>Action</b> 1 and move on from there. Generally a conversation will flow linearly. With Business Rules you can move around to different <b>Actions</b> and change a conversation based on end user responses or information you already know about the end user.</p>
     
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="InfoModalCDF" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;"> 
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What is a Custom Data Field (CDF)?</h4>
      </div>
      <div class="modal-body">
      	<p>Tools to manage your custom data. Your account. Your data. No Limits! </p>
      	<p>Used within an Action as part of the flow of a conversation, you can store a users response to the previous question and link it to the current phone number.</p>
      	<p>Your company's unique strengths are its competitive advantage. Why should you compromise with vanilla application deployments or other vendor limits? With Sire's Custom Data Fields, you don't have too.</p>
      	<p>Any CDFs in the message flow will be replaced with the data you send as part of the triggerSMS API call. Send these additional CDF values as part of the URL query string in GET and POST requests or as part of the JSON data in the POST requests. </p>
      
	  	<ul>
		  	<li><b>Personalization:</b> Personalize different parts of each message for each individual recipient.</li>
		  	<li><b>Customization:</b> Customize parts of the message to relate to each individual recipient's transaction history. The knowledge that a company has about a customer needs to be put into practice and the information held has to be taken into account in order to be able to give the client exactly what he wants</li>
		  	<li><b>Rules Engine:</b> You can change the flow of a conversation based on previously captured input.</li>
	  	</ul>  	
		  	
		  	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="InfoModalCPTypes" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;"> 
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">What are my different Actions used for?</h4>
      </div>
      <div class="modal-body">
      	<p><b>Simple Message</b>: Specify the message you want to send when you reach this Action. Will then move on to the next Action unless there are no more Actions.</p>
      	<p><b>Question & Wait for Answer</b>: Send a question and then waits for a response. Response is open ended and can be anything or response can be Compared against a pre-selected list of possible responses..</p>
      	<p><b>Final Message - This will end the conversation</b>: Specify the message you want to send when you reach this Action. Conversation flow is halted at the specific action, and no more message will be sent, regardless if there a more actions defined after this one. Any action that does not have a next control point will also halt the conversation when the action is completed.</p>
      	<p><b>Rules Engine</b>: Lots of powerful control here. Allows you to move around to different Actions based on: 
	      	<ul>
		      	<li>Previous Responses to Questions. These can be compared to specific answers or compared to a comma separated list of values or Regular Expression.</li>
		      	<li>Data (CDFs) known about the particular subscriber. Can be data previously captured and stored as a CDF.</li>
		      	<li>Data passed as query string inputs as part of the Sire API call that can initiate a conversation</li>
		      	<li>Can be used without any conditions specified - will just navigate to default <b>No Rule(s) Matched Action </b> </li>			  	
	      	</ul>
	      	 either data (CDFs) passed in as part of the API the initiates the conversation, OR </p>
      	<p><b>Action Intervals</b>: are how long you wish to wait  until moving on to the next Action. By default (0) the wait time is infinite. <b>Action Intervals</b> give you dynamic control on how long you wish to wait before moving on to the next specified Action. Responses are ignored on this session until the <b>Action Intervals</b> expires. </p>
      	<p><b>API Call</b>: you can call out to an external REST API. Data can be read back in conversation or stored for later usage in further Rules Engine.</p>
      	<p><b>Expert System ("Chat Bot")</b>: AI responses to customer input.</p>
      	<p><b>Capture Opt In</b>: Will store the phone number to a subscriber list of your choosing.</p>
      	<p><b>Capture Custom Data Field</b>: Stores the last response to a the previous action as a CDF associated with the phone number of the respondent that is already on the list. If a message is defined , will read it out. If not message is defined, conversational flow will just move on to next defined action.</p>
      	<p><b>Drip Marketing Reset</b>: Erases security limits on looping. This allows campaign that are designed to run and repeat many times up to indefinitely.</p>      						
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- Modal -->
<div id="InfoModalBusinessRules" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;"> 
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">What are Business Logic Conditions?</h4>
		</div>
      	
      	<div class="modal-body">
      
			<p><b>Business Logic Conditions</b> let you define a set of rules as to which action to move to if a particular answer value is detected.</p>
			
			<p>Use these rules to estimate, measure, or note the similarity or dissimilarity between answers provided and what you are looking for.</p>
			
			<p><i>Many templates are provided with already defined rules for those who do not want to write their own. For the more adventurous read on to learn how to modify these rules or make your own!</i></p>
			
			<p>Advanced users can define as many rules as they like. Rules are tested in the order they are defined. If a rule is met, than the action that rule specifies is then the next action processed in the conversational flow, and all further rules are bypassed.</p>
			
			<p>If none of the rules are met, you can specify a default action to take.</p>
			
			<p>
				You can use the plus button to add more rules to the end of the list. Rules are tested in the order they are defined. <label class="bc-title mbTight">#1,#2,#3 ...<label>
				<div class="col-xs-12 col-sm-12">
					<label class="bc-title mbTight">Add Another Condition</label>
					<button type="button" id="AddCondition" class="col-xs-12 col-sm-12 mb5 btn btn-default"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
				</div>
				
			</p>	  	
			<p>	
				Use the delete button to remove a rule.
				<button type="button" class="btn btn-default RemoveCondition" style="padding: 3px 6px;">
					<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
				</button>
			</p>
			
			<div class="col-xs-12 col-sm-12 cbic">
				<div class="row">
					&nbsp;
		 		</div> 
				<hr class="hrt0 hrb10">
			</div>
			
			<p class="clearfix">
				<label class="bc-title mbTight" style="display: block;">Rule Type</label>
				You can pick from three possible rule types:	
			</p>
			<p class="clearfix">
				<div class="col-xs-12 col-sm-12 col-left-align" style="z-index: 5;">
				 	<label class="bc-title mbTight">Rule Type</label>
													
					<select id="CTYPE" class="Select2" data-width="100%" tabindex="-1" aria-hidden="true">
						 <option value="RESPONSE" selected="">Single Selection Compare</option>
						 <option value="RESPONSE2">Response Compare</option>
						 <option value="CDF">Data Compare</option>		 
					</select>
				</div>						  	
			</p>			
			<p class="clearfix">   	
				<ul>
					<li><b>Single Selection Compare:</b> This rules lets you pick a question that you have previously defined with a pre-set list of possible answers.</li>
					<li><b>Response Compare:</b> This rule type rules lets you pick a question that you have previously defined with no pre-set answers and then lets you test for various possible free-form answers. </li>
					<li><b>Data Compare:</b> This rule type lets test against known or collected data via Custom Data Fields.</li>
				</ul>  	
			</p>
			
			<div class="col-xs-12 col-sm-12 cbic">
				<div class="row">
					&nbsp;
		 		</div> 
				<hr class="hrt0 hrb10">
			</div>
					
			<p class="clearfix">
				<label class="bc-title mbTight" style="display: block;">Question Answer to Compare</label>
				Choose from a list of previously defined actions. This option is only available for the first two rule types <b>Single Selection Compare</b> and <b>Response Compare</b>
			</p>			
			<p class="clearfix">
				
				<div class="col-xs-12 col-sm-12" style="display: block;">								
					<label class="bc-title mbTight">Question Answer to Compare</label>
					<select id="BOQ" class="Select2" data-width="100%" inpval="2" tabindex="-1" aria-hidden="true"><option value="0">Please Select an Existing Action</option><option value="2">Sire: Signup Verification for SMART Project Promotional Alert msgs. Reply YES to confirm, HELP for help or STOP to cancel. Up to 5 msg/mo. Msg&amp;Data rates may apply</option><option value="4">Branch</option><option value="7">Sire: You have sent an invalid response. Please call us at 999-999-9999 for assistance. </option><option value="9">Branch</option><option value="11">Sire: Registration for SMART Project Promotional Alerts is complete. Reply HELP for help or STOP to cancel.</option></select>
				</div>
				
			</p>				  		
		  	
		  	<div class="col-xs-12 col-sm-12 cbic">
				<div class="row">
					&nbsp;
		 		</div> 
				<hr class="hrt0 hrb10">
			</div>
		  	
		  	
		  	<p class="clearfix">
				<label class="bc-title mbTight" style="display: block;">Custom Data Value to Compare</label>
				For rule type <b>Data Compare</b> Specify a CDF you want to compare to. Use the CDF format {%Name-Of-CDF-Here%}
			</p>	
					
			<p class="clearfix">
				<div class="col-xs-12 col-sm-12" style="display: block;">								
					<div class="col-xs-12 col-sm-12">								 
	 					<label class="bc-title mbTight">Custom Data Value to Compare</label>
						<input id="BOCDV" value="{%Name-Of-CDF-Here%}" class="form-control" maxlength="1000" style="width:100%;">
					</div>
				</div>
			</p>				  		
		  	
		  	<div class="col-xs-12 col-sm-12 cbic">
				<div class="row">
					&nbsp;
		 		</div> 
				<hr class="hrt0 hrb10">
			</div>
				  	
		  	
		  	<p class="clearfix">
				<label class="bc-title mbTight" style="display: block;">Comparison Type</label>
				Look at answers in different ways, based on the comparison type you chose. You can look for exact Compare or look for answers that are close.
				
				<ul>
					<li>Equal</li>
					<li>Similar</li>
					<li>Less than</li>
					<li>More than</li>
					<li>Less than or Equal to</li>
					<li>More than or Equal to</li>						
				</ul>				
			</p>	
					
			<p class="clearfix">				
				<div class="col-xs-12 col-sm-12" style="display: block;">								
					<label class="bc-title mbTight">Comparison Type:</label>														
					<select id="BOC" class="Select2" data-width="100%" tabindex="-1" aria-hidden="true">	
						<option value="=" selected="">Equal</option>
						<option value="LIKE">Similar</option>
						<option value="<">Less than</option>
						<option value=">">More than</option>
						<option value="<=">&lt;=</option>
						<option value=">=">&gt;=</option> 
					</select>
				</div>				
			</p>	
			
		  	<div class="col-xs-12 col-sm-12 cbic">
				<div class="row">
					&nbsp;
		 		</div> 
				<hr class="hrt0 hrb10">
			</div>
			
			<p class="clearfix">
				<label class="bc-title mbTight" style="display: block;">Answer Value(s) to Compare to</label>
				
				<p>
					For rule type  <b>Single Selection Compare</b> this is the list of possible answers you defined as part of the original question.
				</p>	
				
				<p>
					<div class="col-xs-12 col-sm-12" style="display: block; margin-bottom: 1em;">								
						<label class="bc-title mbTight">Answer List:</label>														
						<select id="BOV" class="Select2" data-width="100%" tabindex="-1" aria-hidden="true"><option value="1" selected>Extremely satisfied</option><option value="2">Very satisfied</option><option value="3">Moderately satisfied</option><option value="4">Slightly satisfied</option><option value="5">Not at all satisfied</option></select>
					</div>	
							 			
				</p>
								
				<p>For rule types  <b>Response Compare</b> and <b>Data Compare</b>, you can define one or more possible answers to compare to.</p>
				
				<h4>Examples</h4>
				<ul>
					<li>A single answer
						<p><label class="bc-title mbTight" style="display: block;">Answer Value(s) to Compare to</label><textarea id="BOAV" value="" class="form-control" maxlength="1000" style="width: 100%; margin-top: 0px; margin-bottom: 0px; height: 88px;" rows="3">yes</textarea></p>
					</li>
					<li>
						A comma separated list of answers
						<p><label class="bc-title mbTight" style="display: block;">Answer Value(s) to Compare to</label><textarea id="BOAV" value="" class="form-control" maxlength="1000" style="width: 100%; margin-top: 0px; margin-bottom: 0px; height: 88px;" rows="3">yes,si,yup,ok</textarea></p>
					</li>
					
					<li>A regular expression search for Natural Human Language: 					
						<p><label class="bc-title mbTight" style="display: block;">Answer Value(s) to Compare to</label><textarea id="BOAV" value="" class="form-control" maxlength="1000" style="width: 100%; margin-top: 0px; margin-bottom: 0px; height: 88px;" rows="3">REGEXP(?i)^y$|yes|si|ya|in|yup|yea|yeah|sure|ok|okay|all right|very well|of course|by all means|sure|certainly|absolutely|indeed|right|affirmative|agreed|roger|aye|uh-huh|okey|aye|positive|^1$|confirm|yass|yas</textarea></p>					
					</li>										
				</ul>				
			</p>	

			<div class="col-xs-12 col-sm-12 cbic">
				<div class="row">
					&nbsp;
		 		</div> 
				<hr class="hrt0 hrb10">
			</div>
			
			<p class="clearfix">
				<label class="bc-title mbTight" style="display: block;">Rule Matched Next Action</label>
				If the rule as you defined it, matches, conversational flow will move on to the next action you specify here. Advanced users can define as many rules as they like. Rules are tested in the order they are defined. If a rule is met, than the action that rule specifies is then the next action processed in the conversational flow, and all further rules are bypassed.
			</p>	
			
			<p class="clearfix">				
				<div class="col-xs-12 col-sm-12" style="display: block;">								
					<label class="bc-title mbTight">Rule Matched Next Action:</label>														
					<select id="BOTNQ" class="Select2" data-width="100%" inpval="11" tabindex="-1" aria-hidden="true"><option value="0">Default to next Control Point</option><option value="2">Sire: Signup Verification for SMART Project Promotional Alert msgs. Reply YES to confirm, HELP for help or STOP to cancel. Up to 5 msg/mo. Msg&amp;Data rates may apply</option><option value="4">Branch</option><option value="7">Sire: You have sent an invalid response. Please call us at 999-999-9999 for assistance. </option><option value="9">Branch</option><option value="11">Sire: Registration for SMART Project Promotional Alerts is complete. Reply HELP for help or STOP to cancel.</option></select>
				</div>				
			</p>	
			
			<div class="col-xs-12 col-sm-12 cbic">
				<div class="row">
					&nbsp;
		 		</div> 
				<hr class="hrt0 hrb10">
			</div>
			
			<p class="clearfix">
				<label class="bc-title mbTight" style="display: block;">No Rule(s) Matched Next Action </label>
					If none of the rules are met, you can specify a default action to take. You can use this option without any rules defined to "Branch" to whatever action you want. By default is this is not specified, flow will just move on to the next action in sequence.
			</p>	
			
			<p class="clearfix">				
				<div class="col-xs-12 col-sm-12" style="display: block;">								
					<label class="bc-title mbTight">No Rule(s) Matched Next Action:</label>														
					<select id="BOTNQ" class="Select2" data-width="100%" inpval="11" tabindex="-1" aria-hidden="true"><option value="0">Default to next Control Point</option><option value="2">Sire: Signup Verification for SMART Project Promotional Alert msgs. Reply YES to confirm, HELP for help or STOP to cancel. Up to 5 msg/mo. Msg&amp;Data rates may apply</option><option value="4">Branch</option><option value="7">Sire: You have sent an invalid response. Please call us at 999-999-9999 for assistance. </option><option value="9">Branch</option><option value="11">Sire: Registration for SMART Project Promotional Alerts is complete. Reply HELP for help or STOP to cancel.</option></select>
				</div>				
			</p>	
	
		  	<p class="clearfix">   	
				
			</p>
		  	
      </div>
      <div class="modal-footer clearfix">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal rename template -->
<div class="modal be-modal fade" id="edit-cp-text" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="edit-cp-text-form" name="edit-cp-text-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><strong>EDIT MESSAGE</strong></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Message:</label>
                        <textarea class="form-control no-resize validate[required,maxSize[1000]]" id="message-content" name="message-content" rows="10" data-prompt-position="topLeft:100"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn-save btn btn-success-custom" >Save</button>
                    <button type="button" class="btn btn-back-custom" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>


<cfif XMLCSEditable EQ true >
	<!-- Modal Edit XMLControlSTring-->
	<div id="EditControlString"  tabindex="-1" class="modal fade" role="dialog"> 
	  <div class="modal-dialog">
	    <!-- Modal content-->
	    <form id="frmEditXMLCString">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Edit XMLControlString</h4>
		      </div>
		      <div class="modal-body">
			     
			      	<div class="form-group">
			      		<textarea rows="6" class="form-control validate[required]" form="frmEditXMLCString" name="inpRawXMLControlString" id="inpRawXMLControlString">
			      		</textarea>
			      		<input type="hidden" name="campaignid" value="<cfoutput>#campaignid#</cfoutput>">
			      	</div>
			      
		      </div>
		      <div class="modal-footer">
		      	<button type="submit" id="btnSaveXMLCS" class="btn btn-success-custom" >Save</button>
		        <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
		      </div>
		    </div>
	    </form>
	  </div>
	</div>
</cfif>

<!--- Save as new template modal --->
<div class="modal fade" id="AdminSaveNewTemplateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
            	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><strong>SAVE AS NEW TEMPLATE</strong></h4>
	            <div class="modal-body-inner row">
		            <form name="save-as-new-template" class="col-sm-12" autocomplete="off" id="save-as-new-template-form">

            			<div class="form-group">
						  	<label for="templatename" id="template-name-label">Template name</label>
						  	<label class="pull-right"><a id="template-name-countdown">255</a> left</label>
						  	<input type="text" class="form-control validate[required, custom[noHTML], maxSize[255]]" id="template-name" name="template-name">
						</div>

						<div class="form-group">
							<label for="templatecategory" id="template-category-label">Template category</label>
							<select class="form-control" id="select-template-category">
								<!---  --->
							</select>
						</div>

						<div class="form-group">
							<label for="templatedescription" id="template-description-label">Template Description</label>
							<label class="pull-right"><a id="template-description-countdown">1000</a> left</label>
							<textarea class="form-control validate[maxSize[1000], required]" id="template-description" name="template-description" placeholder="" rows="6"></textarea>
						</div>

						<div class="form-group">
							<label for="templateimage" id="template-image-label">Template Image</label>
							<select class="form-control" id="select-template-image">
								<!---  --->
							</select>
						</div>
						
						<div class="form-group">
							<label for="inpTemplateType" class="bc-title mbTight">Template Type</label>										
							<select id="inpTemplateType" class="Select2" data-width="100%">	
	                            <option value="0" selected="selected">Default Keyword OR Blast</option>
								<option value="1">Keyword Only</option> 
								<option value="2">Blast Only</option>                              
	                        </select>			            
						</div>
						            
						<div class="row hidden">
							<div class="form-group col-sm-3 col-xs-6 col-md-4 col-lg-4">
								<label for="templateorder" id="template-order-label">Template order</label>
								<select class="form-control" id="select-template-order">
									<!---  --->
								</select>
							</div>
						</div>
						

						<div class="form-group">
							<label for="templatexml" id="template-xml-label">Template XML String</label>
							<textarea class="form-control validate[required]" id="template-xml" name="template-xml" placeholder="" rows="6" data-prompt-position="topLeft:100"></textarea>
						</div>

		            </form>
		            <div class="save-new-template-btn-group">
					  	<button class="btn btn-back-custom" id="close-save-new-template-btn" data-dismiss="modal">Cancel</button>
					  	<button class="btn btn-success-custom" id="confirm-save-new-template-btn" type="submit">Save</button>
					</div>
				</div>
        	</div>
        </div>
    </div>
</div>


<!--- Only load this if there is a linked MLP - load after jquery has been loaded --->
<cfif campaignData.MLPID GT 0>
	<!-- Modal SWTMLPSection-->
	<div id="InfoModalEditMLP" data-backdrop-limit="1" tabindex="-1" class="modal fade modalBigTop" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;"> 
	  <div class="modal-dialog">
	
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Marketing Landing Portal Editor</h4>
	      </div>
	      <div class="modal-body">
		      
		      	<cfset inpCPPID= "">
		      	<cfset ccpxDataId= campaignData.MLPID>
		      	<cfset ccpxDataIdSource= "0">
		      	<cfset templateid= "0">
		      	<cfset action= "edit">
		      	<cfset ModalMode = 1 />
				
				<cfinclude template="mlp-edit.cfm" /> 
		    
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	
	  </div>
	</div>
</cfif>


<!--- Read Current Profile data and put into form --->
<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="RetVarGetUserOrganization"></cfinvoke>

<!--- Save as new template modal --->
<div class="modal fade" id="UpdateCompanyNameModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
            	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><strong>Set Your Company Name</strong></h4>
	            <div class="modal-body-inner row">
		            
		            	<div style="text-align: center; background-color: #578ca5; width: 70px; height: 70px; float: left; margin-right: 1em; margin-bottom: 2em; border-radius: 5px;"><img src="/public/sire/images/ic_profile.png" id="console-ic-profile" class="" style="width: 64px; height: 64px; margin: 3px 3px;"></div>
		            
				                                   
						<input type="hidden" name="OrganizationId_int" id="OrganizationId_int" value="<cfoutput>#RetVarGetUserOrganization.ORGINFO.OrganizationId_int#</cfoutput>">
						            									
						<p>This template requires a Company Name. You should provide the name of your company now.</p>
						
<!--- 						<p><div style="text-align: center; margin-top: 1em;"><a href="/session/sire/pages/my-account" style="display: inline;" class="ConsoleLinks">Manage My Account Info</a></div></p> --->
					    
					   
					    
					    <div class="col-sm-12 col-lg-10">
						    <label for="OrganizationName_vch" class="control-label">Company Name</label>
					    	<input type="text" class="form-control" name="OrganizationName_vch" id="OrganizationName_vch" maxlength="250" data-prompt-position="topLeft:100" 
					    	value="<cfoutput>#RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH #</cfoutput>">
					    </div>
					
						<div class="col-sm-12 col-lg-10">
							<p>You can always change your default settings later at <span style="white-space: nowrap">MY ACCOUNT<b style="font-size: .8em;" class="glyphicon glyphicon-arrow-right"></b>MY PROFILE</span></p>
					 	</div>
					 
				</div>
				
				<div class="modal-footer">
				  	<button class="btn btn-back-custom" id="close-save-company-info-btn" data-dismiss="modal">Cancel</button>
				  	<button type="button" class="btn btn-success btn-success-custom" id="confirm-save-company-info-btn">Save</button>
				</div>
        	</div>
        </div>
    </div>
</div>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js">
</cfinvoke>

<!--- Used in Schedule for now.... --->
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/js/jquery.tmpl.min.js">
</cfinvoke>

<!---
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/js/vendors/tinymce/tinymce.min.js">
</cfinvoke>
--->

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/js/schedule.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/js/campaign_edit.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/js/common.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/js/preview.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/css/campaign/mycampaign/schedule.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/js/jquery-ui-1.11.4.custom/jquery-ui.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/message.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/preview.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/schedule.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/sire/css/select2.min.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/sire/css/select2-bootstrap.min.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/style.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/sire/js/select2.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/js/jquery.ddslick.js">
</cfinvoke>

  
<cfparam name="variables._title" default="Campaign Edit - Sire">

<cfinclude template="../views/layouts/master.cfm">



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/emoji-picker/1.1.5/css/emoji.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/emoji-picker/1.1.5/js/config.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/emoji-picker/1.1.5/js/util.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/emoji-picker/1.1.5/js/jquery.emojiarea.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/emoji-picker/1.1.5/js/emoji-picker.min.js"></script>



<!---
<script src="/public/sire/assets/emoji-picker/lib/js/config.js"></script>
<script src="/public/sire/assets/emoji-picker/lib/js/util.js"></script>
<script src="/public/sire/assets/emoji-picker/lib/js/jquery.emojiarea.js"></script>
<script src="/public/sire/assets/emoji-picker/lib/js/emoji-picker.js"></script>
  
--->

<script type="application/javascript">

	<!--- Page Global to store looked up options for SELECT boxes --- null will force reload on next read --->
	var QuestionListBox = null;	
	var QuestionsOnlyListBox = null;
	var GroupsListBox = null;
	var CDFListBox = null;
	var ChooserActive = false;

	function isAsciiOnly(str) {
	    for (var i = 0; i < str.length; i++)
	        if (str.charCodeAt(i) > 127)
	            return false;
	    return true;
	}

	function hasUnicodeChar(str) {
		var UnicodeRegex = new RegExp('[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]');
		if(UnicodeRegex.test(str)){
			return true;
		}
		return false;
	}

	//UTF8 decode for messages that have Unicode characters
	function utf8_decode (strData) {

		var tmpArr = []
		var i = 0
		var c1 = 0
		var seqlen = 0

		strData += ''

		while (i < strData.length) {
			c1 = strData.charCodeAt(i) & 0xFF
			seqlen = 0

			// http://en.wikipedia.org/wiki/UTF-8#Codepage_layout
			if (c1 <= 0xBF) {
				c1 = (c1 & 0x7F)
				seqlen = 1
			} else if (c1 <= 0xDF) {
				c1 = (c1 & 0x1F)
				seqlen = 2
			} else if (c1 <= 0xEF) {
				c1 = (c1 & 0x0F)
				seqlen = 3
			} else {
				c1 = (c1 & 0x07)
				seqlen = 4
			}

		for (var ai = 1; ai < seqlen; ++ai) {
			c1 = ((c1 << 0x06) | (strData.charCodeAt(ai + i) & 0x3F))
		}

		if (seqlen === 4) {
			c1 -= 0x10000
			tmpArr.push(String.fromCharCode(0xD800 | ((c1 >> 10) & 0x3FF)))
			tmpArr.push(String.fromCharCode(0xDC00 | (c1 & 0x3FF)))
		} else {
			tmpArr.push(String.fromCharCode(c1))
			}

			i += seqlen
		}

		return tmpArr.join('')
	}

	
	//Open Editor for Message that has Unicode characters		
	$('body').on('click', '.see-unicode-mess', function(event) {
		event.preventDefault();
		$('#edit-cp-text').modal('show');
		var cpBtnEdit = $(this).parents('.control-point').find('.control-point-btn-edit');
		var newCPObject;
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/control-point.cfc?method=ReadCPDataById&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: false,
			data:  
			{ 
				INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>", 
				inpQID : $(this).parents('.control-point').find('.control-point-btn-edit').attr('data-control-point-id'),
				inpTemplateFlag : "<cfoutput>#inpTemplateFlag#</cfoutput>",
				
				<cfif adv EQ 1>
					RenderHTML : 0
				<cfelse>
					RenderHTML : 1
				</cfif>					
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("*Error. No Response from the remote server. Check your connection and try again.");},					  
			success:		
			<!--- Default return function for Async call back --->
			function(d) 
			{																						
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1) 
				{					
					// RenderCPEditor(d.CPOBJ, "EDIT", inpObj.parents('.control-point'));					
					$('#message-content').val(unescape(encodeURIComponent(d.CPOBJ.TEXT)));					
					// $('#message-content').val(decodeURIComponent(escape(unescape(encodeURIComponent(d.CPOBJ.TEXT)))));

					newCPObject = d.CPOBJ;
					$('#edit-cp-text-form').on('submit', function(event){
						event.preventDefault();
						if($('#edit-cp-text-form').validationEngine('validate', {promptPosition : "topLeft", scroll: false,focusFirstField : true})){
							// var newMessage;
							// if(!isAsciiOnly($('#message-content').val())){
								// newMessage = decodeURIComponent(escape($('#message-content').val()));
								// newMessage = $('#message-content').val();
							// }
							// else{
								// newMessage = $('#message-content').val();
							// }

							var newMessage  = utf8_decode($('#message-content').val());
							
							var ControlPointData = {
				
								'AF' : d.CPOBJ.AF,
								'API_ACT' : d.CPOBJ.API_ACT,
								'API_DATA' : d.CPOBJ.API_DATA,
								'API_DIR' : d.CPOBJ.API_DIR,
								'API_DOM' : d.CPOBJ.API_DOM,
								'API_PORT' : d.CPOBJ.API_PORT,
								'API_RA' : d.CPOBJ.API_RA,
								'API_TYPE' : d.CPOBJ.API_TYPE,
								'BOFNQ' : d.CPOBJ.BOFNQ,
								'CONDITIONS' : d.CPOBJ.CONDITIONS,
								'ERRMSGTXT' : "",
								'GID' : "1",
								'ID' : d.CPOBJ.ID,
								'IENQID' :  d.CPOBJ.IENQID,
								'IHOUR' :  d.CPOBJ.IHOUR,
								'IMIN' :  d.CPOBJ.IMIN,
								'IMRNR' :  d.CPOBJ.IMRNR,
								'INOON' :  d.CPOBJ.INOON,
								'INRMO' :  d.CPOBJ.INRMO,
								'ITYPE' :  d.CPOBJ.ITYPE,
								'IVALUE' :  d.CPOBJ.IVALUE,
								'LMAX' :  d.CPOBJ.LMAX,
								'LDIR' :  d.CPOBJ.LDIR,
								'LMSG' :  d.CPOBJ.LMSG,
								'OIG' : d.CPOBJ.OIG,
								'OPTIONS' : d.CPOBJ.OPTIONS,
								'ESOPTIONS' : d.CPOBJ.ESOPTIONS,
								'REQUIREDANS' : "undefined",
								'RQ' : d.CPOBJ.RQ,
								'SCDF' : d.CPOBJ.SCDF,
								'TDESC' : d.CPOBJ.TDESC,
								'TEXT' : newMessage,
								'TYPE' : d.CPOBJ.TYPE,
								'SWT' : d.CPOBJ.SWT,
								'ANSTEMP' : d.CPOBJ.ANSTEMP,
								'T64' : "0"
							};

							$.ajax({
								type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
								url: '/session/sire/models/cfc/control-point.cfc?method=CP_Save&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
								dataType: 'json',
								async: false,
								data:  
								{ 
									INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>", 
									controlPoint : JSON.stringify(ControlPointData),
									inpSimpleViewFlag : "<cfoutput>#inpSimpleViewFlag#</cfoutput>",
									inpTemplateFlag : "<cfoutput>#inpTemplateFlag#</cfoutput>"	 
								},					  
								error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},					  
								success:		
								<!--- Default return function for Async call back --->
								function(d) 
								{																						
									$('#edit-cp-text').modal('hide');
									<!--- RXRESULTCODE is 1 if everything is OK --->
									if (d.RXRESULTCODE == 1) 
									{	
										
										<!--- Create a jquery reference to the returned HTML and store it in 'el' --->
										var el = $(d.HTML);																				
										
										<!--- When adding new HTML we must add bind events to any buttons or clickable objects --->
										
										<!--- Replace existing content with new render --->
										$('#cp' + $(el).attr('data-control-rq-id')).html($(el).html());	
												
										<!--- Hide these option on walkthrough ---> 
										<cfif swt EQ 0>															
											<!--- Scroll to this objects position - dont make the user search for it. Account for menu hight above it and then a little extra (- 100) --->
											$("body").scrollTop($('#cp' + $(el).attr('data-control-rq-id')).offset().top - 100);
										</cfif>
										
										<!--- Bind delete button action to newly created object --->
										BindDeleteCPInline($('#cp' + $(el).attr('data-control-rq-id')).find('.control-point-btn-delete'));							
										
										<!--- Bind edit button action to newly created object --->	
										BindEditCPInline($('#cp' + $(el).attr('data-control-rq-id')).find('.control-point-btn-edit'));
										BindAltEditCPInline($('#cp' + $(el).attr('data-control-rq-id')).find('.control-point-body'));							
																	
										<!--- Bind add button action to newly created object --->
										BindAddCPInline($('#cp' + $(el).attr('data-control-rq-id')).find('.control-point-btn-add'));
																
										<!--- Re-Index CPs on HTML page - Physical Ids remain the same. --->	
										reIndexQuestions();
										
										<!--- Reset Question SELECTs to re lookup next time they are needed --->
										QuestionListBox = null;
										QuestionsOnlyListBox = null;
																	
									}
									else
									{
										<!--- No result returned --->		
										bootbox.alert("General Error processing your request.");
									}
									// BindEditCPInline(cpBtnEdit);			
																	
								}

									
							});
						}
			    	});

				}
				else
				{
					<!--- No result returned --->		
					bootbox.alert("General Error processing your request. ");
				}			
								
			} 		
				
		});
	});

	
	$(function() {
		
		<cfif campaignData.MLPID GT 0>
			InitMLPEditor();
		</cfif>
					
		<!--- Set up all the select2 boxes --->
		$(".Select2").select2( { theme: "bootstrap"} );
					
		<!--- Becuase these buttons can be dynamically added - set click method here for each obj--->	
		$.each($('.control-point-btn-edit'), function (index, item) {
		   	BindEditCPInline($(this));
		});
		
		<!--- Becuase these buttons can be dynamically added - set click method here for each obj--->	
		$.each($('.control-point-btn-delete'), function (index, item) {
		   	BindDeleteCPInline($(this));
		});
		
		<!--- Becuase these buttons can be dynamically added - set click method here for each obj--->	
		$.each($('.control-point-btn-add'), function (index, item) {
		   BindAddCPInline($(this));
		});			
				
		<!--- Extra way to get to editor - just click on text body for users who cant see the edit icon... --->
		$(".control-point-body").click(function()
		{						
			$(this).parents('.control-point').find('.control-point-btn-edit').click();
		});		
		
		$('#InfoModalEditMLP').on('hidden.bs.modal', function () {
			
			<!--- Refresh iFram on modal edit close --->
			var _theframe = document.getElementById("MLPIFrame");
			_theframe.contentWindow.location.href = _theframe.src;

		})			
						
		<!--- Display keyword editor --->
		$(".console-link-box").click(function()
		{								
			location.href = $(this).attr('rel1');
		});		
		
		<!--- Display keyword editor --->
		$(".short-code-btn-edit").click(function()
		{												
			$('#ShortCodeDisplaySection').hide();
			$('#ShortCodeDefineSection').show();			
								
		});
				
		<!--- Revert display and revert to current keyword --->
		$(".btn-cancel-short-code-data").click(function()
		{	
			<!--- revert to current display value  --->
			$('#ShortCodeSelect').val($("#ShortCodeDisplayValue").html());
			$('#select2-ShortCodeSelect-container').html($("#ShortCodeDisplayValue").html());
			$('#select2-ShortCodeSelect-container').attr('title', $("#ShortCodeDisplayValue").html());
								
			$('#ShortCodeDisplaySection').show();		
			$('#ShortCodeDefineSection').hide();
		});
				
		<!--- Display keyword editor --->
		$(".keyword-btn-edit").click(function()
		{						
			$('#KeywordDisplaySection').hide();
			$('#KeywordDefineSection').show();			
		
			<!--- Nicely set focus to end of existing keyword --->	
			$("#Keyword").focus();
			var tmpStr = $("#Keyword").val();
			$("#Keyword").val('');
			$("#Keyword").val(tmpStr);
			
		});
		
		<!--- Save AVAL REGEXE changes --->
		$("#btn-save-aval-regex").click(function()
		{						
			<!--- Save changes to RegEx to current answer item --->
			$('#AVALREGEditModal').data('inpAnswerItem').find('#AVALREG').val($('#AVALREGEditModal').find('#AVALREGEdit').val());
			
			<!--- Remove current answer item reference - safe so that future modals dont change wrong data --->
			$('#AVALREGEditModal').data('inpAnswerItem', null)
								
			$('#AVALREGEditModal').modal('hide');		
		});

		<!--- Save Expert System REGEX changes --->
		$("#btn-save-es-regex").click(function()
		{						
			<!--- Save changes to RegEx to current answer item --->
			$('#ESREGEditModal').data('inpESItem').find('#ESREG').val($('#ESREGEditModal').find('#ESREGEdit').val());
			
			<!--- Remove current answer item reference - safe so that future modals dont change wrong data --->
			$('#ESREGEditModal').data('inpESItem', null)
								
			$('#ESREGEditModal').modal('hide');		
		});
						
		<!--- Save keyword changes --->
		$("#btn-save-keyword-data").click(function()
		{						
			SaveKeyword(true);			
		});
		
		<!--- Revert display and revert to current keyword --->
		$(".btn-cancel-keyword-data").click(function()
		{	
			<!--- revert to current display value  --->
			$('#Keyword').val($("#KeywordDisplayValue").html());
			
			if($("#KeywordDisplayValue").html().length > 0)
				$('#StartKeywordStatus').hide();
			else
				$('#StartKeywordStatus').show();
								
			$('#KeywordDisplaySection').show();		
			$('#KeywordStatus').html('&nbsp;');
			$('#KeywordDefineSection').hide();
			
		});
		
		
		<!--- Display batch desc editor --->
		$(".batch-desc-btn-edit").click(function()
		{	
<!---
		//	$('#BatchDesc').html($("#BatchDescDisplayValue").html());
		//	$('#BatchDesc').val($("#BatchDescDisplayValue").html());
--->
								
			$('#BatchDescDisplaySection').hide();
			$('#BatchDescDefineSection').show();
			
			<!--- Nicely set focus to end of existing keyword --->	
			$("#BatchDesc").focus();
			var tmpStr = $("#BatchDesc").val();
			$("#BatchDesc").val('');
			$("#BatchDesc").val(tmpStr);
			
		});
				
		<!--- Save batch desc changes --->
		$("#btn-save-batch-desc-data").click(function()
		{						
			SaveCampaignName(true);			
		});
		
		<!--- Revert display and revert to current batch desc --->
		$(".btn-cancel-batch-desc-data").click(function()
		{									
			$('#BatchDescDisplaySection').show();
			$('#BatchDescDefineSection').hide();
		});
					
		<!--- Allow user to add a group (Subscriber List) --->
		$('#subscriber_list_form_add').validationEngine('attach', {
			promptPosition : "topLeft", 
			autoPositionUpdate: true, 
			showArrow: false, 
			scroll: false,
			focusFirstField : false
		});

		$('#subscriber_list_form_add').on('submit', function(event)
		{	
			event.preventDefault();

			var inpCPE = $('#btn-save-group').data('inpCPE');
			if($(this).validationEngine('validate')){
				$.ajax({
					url: '/session/sire/models/cfc/control-point.cfc?method=AddGroup&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					type: 'post',
					dataType: 'json',
					data: {INPGROUPDESC: $('#subscriber_list_name').val()},
					beforeSend: function()
					{
						$("#btn-save-group").prop("disabled",true);
						
					},
					complete: function()
					{
						$("#btn-save-group").prop("disabled",false);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); $("#btn-save-group").prop("disabled",false);	},
					success: function(d) {
											
						if (d.RXRESULTCODE == 1) 
						{	
							<!--- Make sure to add and select the new Subcriber list in the SELECT box --->
							GroupsListBox = null;
							PopulateSubscriberListSelectBox(inpCPE.find('#OIG'), d.INPGROUPID)
							$('#AddNewSubscriberList').modal('hide');
						} 
						else 
						{
							bootbox.alert('Error: Add New Subscriber List: <br/>' + d.MESSAGE);
						}					
						
						$("#btn-save-group").prop("disabled",false);
					}
				});
			}			
			
		});	
		
		
		<!--- Clone Campaign --->
		$("#confirm-save-company-info-btn").click(function()
		{			
			<!--- Allow blank to erase responses --->
			$("#confirm-save-company-info-btn").prop("disabled",true);		
		
			$.ajax({
			type: "POST",
			url: '/session/sire/models/cfc/control-point.cfc?method=UpdateCompanyName&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: true,
			data: {
					OrganizationId_int : $("#OrganizationId_int").val(), 
					OrganizationName_vch : $("#OrganizationName_vch").val()				
				},
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); $("#confirm-save-company-info-btn").prop("disabled",false);	},					  
			success:		
				function(d) {
					$('#processingPayment').hide();
					
					if(d.RXRESULTCODE == 1)
					{
						$('#confirm-save-company-info-btn').prop('disabled',false);
						$('#UpdateCompanyNameModal').modal('hide');
												
						if($("#OrganizationName_vch").val().length > 0)	
						{				
							var str = $('#CPText').val();
							var res = str.replace('{%CompanyName%}', $("#OrganizationName_vch").val());
							
							$('#CPText').val(res);
						}
						
						$('html,body').animate({scrollTop:0},0);
						
					
						return;
					}
					else
					{
						//bootbox.alert(d.MESSAGE, function() {});
						bootbox.dialog({
					    message: " " + d.MESSAGE,
					    title: "Save Company Name Failed",
						    buttons: {
						        success: {
						            label: "Ok",
						            className: "btn btn-medium btn-success-custom",
						            callback: function() {}
						        }
						    }
						});
						
						$("#confirm-save-company-info-btn").prop("disabled",false);	
					}
				} 
			});

															
		});		
				
		<!--- Validate keyword in real time - ajax with warnings  --->
		$("#Keyword").on("input", function() {
				    
		    if(this.value.length == 0)
		    {
			    <!--- Give blank to erase warning if one was previously set --->
			    if( $('#KeywordDisplayValue').html.length > 0)
			    {
				    $('#KeywordStatus').css('color', 'red');
			    	$('#KeywordStatus').html("Save blank to erase existing Keyword if any.");
			    }
			    else
			    {
			    	$('#KeywordStatus').css('color', 'red');
			    	$('#KeywordStatus').html('');			    
			    }
		    }		    
		    else
		    {
			    $('#KeywordStatus').html('<img src="/public/sire/images/loading.gif" class="ajax-loader-inline" style="width:2em; height:2em;"> ');
			    
			    <!--- Save Custom Responses - validation on server side - will return error message if not valid --->				
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url: '/session/sire/models/cfc/control-point.cfc?method=ValidateKeyword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					async: true,
					data:  
					{ 
						inpBatchId : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>", 
						inpKeyword : this.value,
						inpShortCode : $("#ShortCode").val()					
						
					},					  
					error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
					success:		
					<!--- Default return function for Async call back --->
					function(d) 
					{																																		
						<!--- RXRESULTCODE is 1 if everything is OK --->
						if (d.RXRESULTCODE == 1) 
						{	
							
							$('#KeywordStatus').css('color', 'green');	
							$('#KeywordStatus').html(d.MESSAGE);							
																										
						}
						else
						{
							<!--- No result returned --->	
							if(d.ERRMESSAGE != "")
							{	
								$('#KeywordStatus').css('color', 'red');
								$('#KeywordStatus').html(d.ERRMESSAGE);
							}
							
								
						}												
					} 		
						
				});
			}
		});

		
		<!--- Allow user to add a CDF (Custom Data Field) --->
		$('#AddNewCDFModal').find("#btn-save-cdf").click(function()
		{	
			$("#btn-save-cdf").prop("disabled",true);
			
			var inpCPE = $('#btn-save-cdf').data('inpCPE');
						
			$.ajax({
				url: '/session/sire/models/cfc/control-point.cfc?method=AddCDF&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'post',
				dataType: 'json',
				data: {
						inpCDFName: $('#inpCDFName').val(),
						inpOptions : {},
						inpCheckOption : 0,
						inpDefaultValue : ''
						
				},
				beforeSend: function()
				{
					$("#btn-save-cdf").prop("disabled",true);
					
				},
				complete: function()
				{
					$("#btn-save-cdf").prop("disabled",false);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); $("#btn-save-cdf").prop("disabled",false);	},
				success: function(d) {
										
					if (d.RXRESULTCODE == 1) 
					{												
						CDFListBox = null;
						PopulateCDFSelectBox(inpCPE.find('#SCDF'), d.CDFNAME)
						$('#AddNewCDFModal').modal('hide');						
					} 
					else 
					{
						bootbox.alert('Error: Add New CDF: <br/>' + d.MESSAGE);
					}					
					
					$("#btn-save-cd").prop("disabled",false);
				}
			});
		});	
		
		<!--- Clone Campaign --->
		$("#btn-clone-campaign").click(function()
		{			
			<!--- Allow blank to erase responses --->
			$("#btn-clone-campaign").prop("disabled",true);		
		
			$.ajax({
			type: "POST",
			url: '/session/sire/models/cfc/control-point.cfc?method=CloneCampaign&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: true,
			data: {
				inpBatchId : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>", 
				inpShortCode : $("#ShortCode").val()				
				},
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); $("#btn-save-custom-responses").prop("disabled",false);	},					  
			success:		
				function(d) {
					$('#processingPayment').hide();
					$('#cloneCampaignModal').modal('hide');
					if(d.RXRESULTCODE == 1){
						$('#btn-clone-campaign').prop('disabled',false);
												
						bootbox.dialog({
						    message: " " + d.MESSAGE,
						    title: "Campaign Cloned Success",
						    closeButton: false,
						    buttons: {
						        success: {
						            label: "Ok",
						            className: "btn btn-medium btn-success-custom",
						            callback: function() {
						            	location.href = '/session/sire/pages/campaign-edit?campaignid=' + d.NEXTCAMPAINID + '<cfif ADV EQ 1>&ADV=1</cfif>';
						            }
						        }
						    }
						});
						return;
					}
					else
					{
						//bootbox.alert(d.MESSAGE, function() {});
						bootbox.dialog({
					    message: " " + d.MESSAGE,
					    title: "Clone Campaigns",
						    buttons: {
						        success: {
						            label: "Ok",
						            className: "btn btn-medium btn-success-custom",
						            callback: function() {}
						        }
						    }
						});
						
						$("#btn-clone-campaign").prop("disabled",false);	
					}
				} 
			});

															
		});		
		
		
		
		<!--- If page is refrshed with a selected list already set - go ahead and enable button. --->		
		if($('#SubscriberList').val() > 0 )
			$(".btn-save-send").prop("disabled",false);	
						
		<!--- Prepare to send blast - give user chance to confirm --->
		$(".btn-save-send").click(function()
		{	
			BlastNow();												
		});	
				
		<!--- Confirm to send blast - Blast will now be sent --->
		$("#btn-confirm-save-and-send").click(function()
		{						
			$("#btn-confirm-save-and-send").prop("disabled",true);	
			
			<!--- Change to loading icon --->
			$('#ProcessingBlast').show();
			$('.ConfirmationSummary').hide();
			
			<!--- Save Custom Responses - validation on server side - will return error message if not valid --->				
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/control-point.cfc?method=BlastToList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				async: true,
				data:  
				{ 
					inpBatchId : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>", 
					inpGroupId : $('#SubscriberList').val(),
					inpShortCode : $("#ShortCode").val(),
					inpScheduledTimeData : JSON.stringify( GetScheduleTimeList() ),
					inpCdffilter: $('input.data-cdf-filter').data('contactStringCdfFilter'),
					inpContactFilter: $('#inpContactStringFilter').val()					
				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); $("#btn-confirm-save-and-send").prop("disabled",false);	},					  
				success:		
				<!--- Default return function for Async call back --->
				function(d) 
				{																																		
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1) 
					{							
						<!--- Kill loading icon --->						
						
						$('#ConfirmLaunchModal').modal('hide');
						
						<!--- Change to loading icon --->
						$('#ProcessingBlast').hide();
						$('.ConfirmationSummary').show();
												
						<!--- Re-enable button to send for next time --->
						$("#btn-confirm-save-and-send").prop("disabled",false);	

						if (d.RETVALADDFILTERSTOQUEUE != "") {
							<!--- If this is an active SWT about to be finished move on if success --->
							<cfif swt EQ 1 >						
								bootbox.alert('<h3>Your blast has been loaded and will start going out shortly subject to your scheduled times.</h3 <h4>Count loaded into send queue = (' + d.RETVALADDFILTERSTOQUEUE.COUNTQUEUEDUPSMS + ')</h4>', function(){ FinalFinishSWT(); }); 						
							<cfelse>
								bootbox.alert('<h3>Your blast has been loaded and will start going out shortly subject to your scheduled times.</h3 <h4>Count loaded into send queue = (' + d.RETVALADDFILTERSTOQUEUE.COUNTQUEUEDUPSMS + ')</h4>', function(){  }); 	
							</cfif>
						} else {
							/* This is for queued blast when contact string number is over 100 */
							<cfif swt EQ 1 >						
								bootbox.alert(d.MESSAGE, function () {
									FinalFinishSWT();
								});					
							<cfelse>
								bootbox.alert(d.MESSAGE); 	
							</cfif>
						}
						
					}
					else
					{
						<!--- No result returned --->	
						if(d.ERRMESSAGE != "")
						{	
							bootbox.alert(d.ERRMESSAGE);
							
							$('#ConfirmLaunchModal').modal('hide');
						
							<!--- Change to loading icon --->
							$('#ProcessingBlast').hide();
							$('.ConfirmationSummary').show();
													
							<!--- Re-enable button to send for next time --->
							$("#btn-confirm-save-and-send").prop("disabled",false);
						}	
					}												
				} 		
					
			});
												
		});	
		
		
		<!--- Write Batch Desc to DB --->
		$("#btn-save-batch-data").click(function()
		{	
			<!--- Similar but different style editor for keyword --->
			<cfif swt EQ 1 >
				var ExistingKeyword =  $('#Keyword').val();  			
			<cfelse>
				var ExistingKeyword = $('#KeywordDisplayValue').html();						
			</cfif>
			
			if($("#Keyword").val().length == 0 && ExistingKeyword.length > 0)
			{
					
				bootbox.confirm('Are you sure you want to remove the reserved keyword?', function(r) 
				{
					<!--- Only complete if user comfirms it --->
					if (r == true) 
					{
						<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->						
						$.ajax({
							type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
							url: '/session/sire/models/cfc/control-point.cfc?method=UpdateBatchData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
							dataType: 'json',
							async: true,
							data:  
							{ 
								INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>", 
								inpDesc : $('#BatchDesc').val(),
								inpKeyword : $("#Keyword").val(),
								inpShortCode : $("#ShortCode").val(),
								inpTemplateFlag : "<cfoutput>#inpTemplateFlag#</cfoutput>"	
								
							},					  
							error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
							success:		
							<!--- Default return function for Async call back --->
							function(d) 
							{																																		
								<!--- RXRESULTCODE is 1 if everything is OK --->
								if (d.RXRESULTCODE == 1) 
								{										
									$('#KeywordStatus').html('');
								}
								else
								{
									<!--- No result returned --->	
									if(d.ERRMESSAGE != "")	
										bootbox.alert(d.ERRMESSAGE);
								}												
							} 		
								
						});
						
					}	
					
					<!--- Return true to close bootbox when complete --->
					return true;
				});
						
			}
			else
			{
				<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->				
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url: '/session/sire/models/cfc/control-point.cfc?method=UpdateBatchData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					async: true,
					data:  
					{ 
						INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>", 
						inpDesc : $('#BatchDesc').val(),
						inpKeyword : $("#Keyword").val(),
						inpShortCode : $("#ShortCode").val(),
						inpTemplateFlag : "<cfoutput>#inpTemplateFlag#</cfoutput>"	
						
					},					  
					error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
					success:		
					<!--- Default return function for Async call back --->
					function(d) 
					{																																		
						<!--- RXRESULTCODE is 1 if everything is OK --->
						if (d.RXRESULTCODE == 1) 
						{								
							$('#KeywordStatus').html('');
						}
						else
						{
							<!--- No result returned --->	
							if(d.ERRMESSAGE != "")	
								bootbox.alert(d.ERRMESSAGE);
						}												
					} 		
						
				});				
				
			}			
															
		});	
		
		
		
		
		<!--- Show Blast by default --->
		if(parseInt(<cfoutput>#blast#</cfoutput>) == 1 && parseInt(<cfoutput>#swt#</cfoutput>) == 0)
		{
			$("#BlastContainer").toggle();				
		}	
			
		<!--- Toggle the advanced editor Button function --->
		$(".SMSBlastOption").click(function()
		{
			$("#BlastContainer").toggle();																	
		});
				
		
		$('#SWTChooseKeyword').click(function()
		{
			$('#SWTChooser').hide();	
			$('#SWTKeywordSection').show();	
			$('#SWTPreviewSection').hide();						
			$('#SWTBlastContainer').hide();	
			$('#SWTMLPSection').hide();	
			
			<!--- Nicely set focus to end of existing keyword --->	
			$("#Keyword").focus();
			var tmpStr = $("#Keyword").val();
			$("#Keyword").val('');
			$("#Keyword").val(tmpStr);
			
			ChooserActive = true;																	
		});


		$('#SWTChooseBlast').click(function()
		{
			$('#SWTChooser').hide();
			$('#SWTKeywordSection').hide();	
			$('#SWTPreviewSection').hide();						
			$('#SWTBlastContainer').show();	
			$('#SWTMLPSection').hide();	
			
			ChooserActive = true;																
		});
		
						
		<!--- Update/Set Custom Response --->
		$("#btn-save-custom-responses").click(function()
		{			
			<!--- Allow blank to erase responses --->
			$("#btn-save-custom-responses").prop("disabled",true);		
			
			<!--- Save Custom Responses - validation on server side - will return error message if not valid --->				
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/control-point.cfc?method=SetCustomStandardReplyHELPSTOP&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				async: true,
				data:  
				{ 
					INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>", 
					inpKeyword : "HELP",
					inpShortCode : $("#ShortCode").val(),
					inpHELPResponse : $("#CustomHelpMessage_vch").val(),
					inpSTOPResponse : $("#CustomStopMessage_vch").val()
				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); $("#btn-save-custom-responses").prop("disabled",false);	},					  
				success:		
				<!--- Default return function for Async call back --->
				function(d) 
				{																																		
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1) 
					{		
						$('#CustomMessageModal').modal('hide');
						$("#btn-save-custom-responses").prop("disabled",false);								
					}
					else
					{
						<!--- No result returned --->	
						if(d.ERRMESSAGE != "")	
							bootbox.alert(d.ERRMESSAGE);
						
						$("#btn-save-custom-responses").prop("disabled",false);	
					}												
				} 		
					
			});
												
		});		
		
		<!--- Careful: Coldfusion toScript outputs all array data as lowercase but .CFC will return upper case so be careful which type of object you are working with  --->
		<!--- Walk Through Control --->
		<cfif swt EQ 1> 			
										
			<!--- Walkthrough Move Button --->
			$(".btn-move-swt").click(function()
			{	
				
				
					if(ChooserActive)
					{							
						$('#SWTKeywordSection').hide();						
						$('#SWTBlastContainer').hide();	
						$('#SWTPreviewSection').hide();	
						$('#SWTMLPSection').hide();	
						
						ChooserActive = false;
						
						
						<cfif tt eq 0>				
							
							$('#SWTChooser').show();
						
							if(parseInt($(this).attr("dir")) != 1)
								return;

						<cfelse>
							$('#SWTChooser').hide();						
						</cfif>	
					}				
				
				
				<!--- dont let user too click happy --->
				$('.btn-prev-swt').prop("disabled",true);
				$('.btn-next-swt').prop("disabled",true);
												
												
				<!--- Get current RQ number --->
				var NextRQ = parseInt($('#WalkThroughStepEditor').attr("data-control-point-rq"));	
				var PreviousRQ = parseInt($('#WalkThroughStepEditor').attr("data-control-point-rq"));	
				var PreviousSWTStep = CurrentSWTStep;
				var PreviousSWTLocationInfo = $('#SWTLocationInfo').html();
				<!--- Dont keep having to calculate this - just store in local var for reuse --->
				var CurrentCPObjLength = jsVarRetVarReadCPs.cpobj.length;	
				var ExtraSteps = 0;
				var ExtraOptionalSteps = 0;
				
				<!--- Set max steps - default to number of SWT ediable CPs --->	
				var TotalSteps = parseInt(jsVarRetVarReadCPs.swtcount);
			
				<!--- Add a step for extra stuff like keyword and campaign description --->	
				if(parseInt('<cfoutput>#swt_desc#</cfoutput>') > 0)
					ExtraSteps = ExtraSteps + 1;
				
				<!--- Add a step for extra stuff like keyword and campaign description --->	
				if(parseInt('<cfoutput>#swt_keyword#</cfoutput>') > 0)
					ExtraSteps = ExtraSteps + 1;
					
				<!--- Add a step for extra stuff like keyword and campaign description --->	
				if(parseInt('<cfoutput>#swt_qa#</cfoutput>') > 0)
					ExtraSteps = ExtraSteps + 1;
					
				<!--- Add a step for extra stuff like keyword and campaign description --->	
				if(parseInt('<cfoutput>#campaignData.MLPID#</cfoutput>') > 0)
				{
					ExtraSteps = ExtraSteps + 1;
					ExtraOptionalSteps = ExtraOptionalSteps + 1;				
				}
				
				<!--- Add extra steps to go beyond CP editing - Desc, Keyword, Blasts etc --->
				TotalSteps = TotalSteps + ExtraSteps;
				CurrentCPObjLength = CurrentCPObjLength + ExtraSteps;
											
				<!--- Save Current CP data --->			
				if(NextRQ > 0)
				{					
					<!--- Run any validations first Subscriber List?  --->	
					if($('#WalkThroughStepEditor').find('#CPEBody').find('#TYPE').val() == 'OPTIN')
					{
						<!--- Check if current step is a list and make sure one is select - else auto create one for them --->
						if(parseInt($('#WalkThroughStepEditor').find('#CPEBody').find('#OIG').val()) == 0)
						{							
							AutoCreateSubscriberList();
						}
					}
									
					$('.btn-save-cp').click();
				}			
						
				<!--- Save Desc if displayed --->	
				if(CurrentSWTStep == (parseInt(jsVarRetVarReadCPs.swtcount) + 1))
				{
					SaveCampaignName(false);					
					
				}
						
				<!--- Save Keyword if displayed --->
				if(CurrentSWTStep == (parseInt(jsVarRetVarReadCPs.swtcount) + 2))
				{
					SaveKeyword(false);				
					
				}	
										
				<!--- Boundary checking - display is 1 based but java arrays are 0 based - be careful! --->
				//if(NextRQ <= CurrentCPObjLength)
				if(NextRQ <= CurrentCPObjLength)
				{				
					
					if(parseInt($(this).attr("dir")) == 1)
					{
						
						<!--- Move forward until next SWT=1 or END --->
						while (1 == 1) 
						{						
							   
							NextRQ = NextRQ + 1;
							CurrentSWTRQ = CurrentSWTRQ + 1;						
																				
							<!--- Sanity Check - Dont loop past length of array of all RQs --->
							if(CurrentSWTRQ > (CurrentCPObjLength - 1) || CurrentSWTRQ < 0)
							{
								CurrentSWTStep = CurrentSWTStep + 1;
								break;
							}													
							
							<!--- Make sure we are still in real CPs and not the extra stuff - desc, keyword, blasts, etc --->
							if(jsVarRetVarReadCPs.cpobj[CurrentSWTRQ-1])	
							{											
								if(parseInt(jsVarRetVarReadCPs.cpobj[CurrentSWTRQ-1].swt) == 1)
								{
									<!--- Move to next SWT step --->	
									CurrentSWTStep = CurrentSWTStep + 1;
									break;
								}	
							}
							else
							{
								<!--- Move to next SWT step --->	
								CurrentSWTStep = CurrentSWTStep + 1;
								break;
								
							}
						
						}
						
					}
					else
					{	
						<!--- Move backward until next SWT=1 or BEGINNING --->
						
						while (1 == 1) 
						{						
							   
							NextRQ = NextRQ - 1;
							CurrentSWTRQ = CurrentSWTRQ - 1;						
																					
							<!--- Sanity Check - Dont loop past length of array of all RQs --->
							if(CurrentSWTRQ > (CurrentCPObjLength - 1) || CurrentSWTRQ < 0)
							{
								
								
								CurrentSWTStep = CurrentSWTStep - 1;
								
								if(CurrentSWTRQ < 0)
								{
									CurrentSWTRQ = 1;									
								}
								
								break;
							}													
							
							<!--- Make sure we are still in real CPs and not the extra stuff - desc, keyword, blasts, etc --->
							if(jsVarRetVarReadCPs.cpobj[CurrentSWTRQ-1])	
							{													
								if(parseInt(jsVarRetVarReadCPs.cpobj[CurrentSWTRQ-1].swt) == 1)
								{
									<!--- Move to next SWT step --->	
									CurrentSWTStep = CurrentSWTStep - 1;
									break;
								}	
							}
							else
							{
								<!--- Move to next SWT step --->	
								CurrentSWTStep = CurrentSWTStep - 1;
								break;
								
							}
						
						}
					}					
																				
					
				}
				else
				{
					<!--- Provide out of range error handling --->	
					
				}
				
			
				<!--- Beginning step and there is more --->
				if(CurrentSWTStep == 1 && CurrentSWTStep < TotalSteps)
				{
					$('.btn-prev-swt').prop("disabled",true);
					$('.btn-next-swt').prop("disabled",false);
				}				
							
				<!--- Beginning step and there is NOT more --->
				if(CurrentSWTStep == 1 && CurrentSWTStep == TotalSteps)
				{						
					$('.btn-prev-swt').prop("disabled",true);
					$('.btn-next-swt').prop("disabled",true);							
				}	
							
				<!--- Past Beginning step and there is NOT more --->
				if(CurrentSWTStep > 1 && CurrentSWTStep == TotalSteps)
				{
					$('.btn-prev-swt').prop("disabled",false);
					$('.btn-next-swt').prop("disabled",true);
				}			
				
				<!--- Past Beginning step and there is more --->
				if(CurrentSWTStep > 1 && CurrentSWTStep < TotalSteps)
				{					
					$('.btn-prev-swt').prop("disabled",false);
					$('.btn-next-swt').prop("disabled",false);
								
				}	
				
				<!--- Update positional display and chart --->	

				<!--- Update global tracking var --->
				
							
				<!--- Check if this is 1st RQ - hide previous button --->
				<!--- Check if this is Last RQ - hide next button --->	
				
				<!--- Description Section --->
				if(CurrentSWTStep == (parseInt(jsVarRetVarReadCPs.swtcount) + 2))
				{
					
					$('#SWTChooser').hide();
					$('#SWTKeywordSection').hide();						
					$('#SWTBlastContainer').hide();	
					$('#SWTPreviewSection').hide();	
					$('#SWTMLPSection').hide();	
					
					ChooserActive = false;
					
					<!--- Remove any existing content --->
					$('#WalkThroughStepEditor').empty();
					
					
									
					<!--- Set the current RQ --->
					$('#WalkThroughStepEditor').attr("data-control-point-rq", NextRQ)	

					<!--- display desc area --->
					$('#SWTDescSection').show(100, function(){
						UpdateChartSWTCompletion(CurrentSWTStep, TotalSteps);
					});		
					
					<!--- Return with out trying to load CP; --->
					return;
							
				}
				else
				{					
					$('#SWTDescSection').hide();	
					$('#SWTPreviewSection').hide();		
					$('#SWTMLPSection').hide();				
				}
				
				<!--- Chooser Section --->
				if(CurrentSWTStep == (parseInt(jsVarRetVarReadCPs.swtcount) + 1))
				{
					
					$('#SWTDescSection').hide();
											
					<!--- Remove any existing content --->
					$('#WalkThroughStepEditor').empty();
															

					<!--- Set the current RQ --->
					$('#WalkThroughStepEditor').attr("data-control-point-rq", NextRQ)	

											
					<!--- tt is for Template Type 0 is either 1=keyword 2=blast --->
					<cfif tt EQ 1>
					
						$('#SWTChooser').hide();	
						$('#SWTKeywordSection').show();	
						$('#SWTPreviewSection').hide();						
						$('#SWTBlastContainer').hide();	
						$('#SWTMLPSection').hide();	
						
						ChooserActive = true;	
						
						<!--- Nicely set focus to end of existing keyword --->	
						$("#Keyword").focus();
						var tmpStr = $("#Keyword").val();
						$("#Keyword").val('');
						$("#Keyword").val(tmpStr);	
						
						UpdateChartSWTCompletion(CurrentSWTStep, TotalSteps);
					
					<cfelseif tt EQ 2>
					
						$('#SWTChooser').hide();
						$('#SWTKeywordSection').hide();	
						$('#SWTPreviewSection').hide();	
						$('#SWTMLPSection').hide();						
						$('#SWTBlastContainer').show();	
						
						ChooserActive = true;		
						
						UpdateChartSWTCompletion(CurrentSWTStep, TotalSteps);	
					
					<cfelse>
							
						ChooserActive = false;
						
						<!--- display chooser area --->
						$('#SWTChooser').show(100, function(){
							UpdateChartSWTCompletion(CurrentSWTStep, TotalSteps);
						});	
						
					</cfif>
									
					
					<!--- Return with out trying to load CP; --->
					return;
				
					
							
				}
				else
				{					
					$('#SWTChooser').hide();
					$('#SWTKeywordSection').hide();						
					$('#SWTBlastContainer').hide();	
					$('#SWTPreviewSection').hide();	
					$('#SWTMLPSection').hide();	
					ChooserActive = false;
				}
				
				<!--- Qa Preview  Section --->
				if(CurrentSWTStep == (parseInt(jsVarRetVarReadCPs.swtcount) + 3))
				{
					$('#SWTDescSection').hide();
										
					<!--- Remove any existing content --->
					$('#WalkThroughStepEditor').empty();
										
					
					<!--- Set the current RQ --->
					$('#WalkThroughStepEditor').attr("data-control-point-rq", NextRQ)	

					<!--- display desc area --->
					$('#SWTPreviewSection').show(100, function(){
						UpdateChartSWTCompletion(CurrentSWTStep, TotalSteps);
					});	
					
					ChooserActive = false;
					
					<!--- Return with out trying to load CP; --->
					return;
							
				}
				else
				{					
					$('#SWTChooser').hide();
					$('#SWTKeywordSection').hide();						
					$('#SWTBlastContainer').hide();	
					$('#SWTPreviewSection').hide();	
					$('#SWTMLPSection').hide();	
					ChooserActive = false;
				}

				if(parseInt('<cfoutput>#campaignData.MLPID#</cfoutput>') > 0)
				{
					<!--- MLP Preview Section - Optional so watch counts on where we are at --->
					if(CurrentSWTStep == (parseInt(jsVarRetVarReadCPs.swtcount) + 4))
					{
						$('#SWTDescSection').hide();
											
						<!--- Remove any existing content --->
						$('#WalkThroughStepEditor').empty();
											
						
						<!--- Set the current RQ --->
						$('#WalkThroughStepEditor').attr("data-control-point-rq", NextRQ)	
	
	
						UpdateMLPWithKeywordAndShortCode();
	
						<!--- display desc area --->
						$('#SWTMLPSection').show(100, function(){
							UpdateChartSWTCompletion(CurrentSWTStep, TotalSteps);
						});	
						
						ChooserActive = false;
						
						<!--- Return with out trying to load CP; --->
						return;
								
					}
					else
					{					
						$('#SWTChooser').hide();
						$('#SWTKeywordSection').hide();						
						$('#SWTBlastContainer').hide();	
						$('#SWTPreviewSection').hide();	
						$('#SWTMLPSection').hide();	
						ChooserActive = false;
					}
	
				}	

				<!--- Read Current Data From XMLControlString - run async=false so happy fingered users cant get too far out of sync--->				
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url: '/session/sire/models/cfc/control-point.cfc?method=ReadCPDataById&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					async: false, 
					data:  
					{ 
						INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>", 
						inpQID : NextRQ,
						inpTemplateFlag : "<cfoutput>#inpTemplateFlag#</cfoutput>"					
					},					  
					error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("*Error. No Response from the remote server. Check your connection and try again.");},					  
					success:		
					<!--- Default return function for Async call back --->
					function(d) 
					{																						
						<!--- RXRESULTCODE is 1 if everything is OK --->
						if (d.RXRESULTCODE == 1) 
						{	
							   
							if(parseInt(d.STRUCTCOUNT) > 0 )
							{
								UpdateChartSWTCompletion(CurrentSWTStep, TotalSteps);
								
								<!--- Remove any existing content --->
								$('#WalkThroughStepEditor').empty();
								
								<!--- Set the current RQ --->
								$('#WalkThroughStepEditor').attr("data-control-point-rq", NextRQ)	
																								
								<!--- Render an editor for the current Step --->					
								RenderCPEditor(d.CPOBJ, "EDIT", $('#WalkThroughStepEditor'));
								
								
								<!--- Check if current text still contains {%CompanyName%} --->
																
								if($('#CPText').val().indexOf('{%CompanyName%}') !== -1)
								{
									<!--- $('.btn-save-cp').click()  --->
									$('#UpdateCompanyNameModal').modal('show');
									
								}
							}
							else
							{
								<!--- If error reading then revert to previous settings --->
								CurrentSWTRQ = PreviousRQ;
								CurrentSWTStep = PreviousSWTStep;
								$('#SWTLocationInfo').html(PreviousSWTLocationInfo);
								
								bootbox.alert("Error while communicating with server please try again ");
														
								
							}
						}
						else
						{
							<!--- If error reading then revert to previous settings --->
							CurrentSWTRQ = PreviousRQ;
							CurrentSWTStep = PreviousSWTStep;
							$('#SWTLocationInfo').html(PreviousSWTLocationInfo);
							
							bootbox.alert("Error while communicating with server please try again ");
						}			
										
					} 		
						
				});
																				
			});
			
			<!--- Walkthrough Cancel Button --->
			$(".btn-complete-swt").click(function()
			{				
				<!--- Save Current CP data --->			
				if(parseInt($('#WalkThroughStepEditor').attr("data-control-point-rq"))	 > 0)
				{					
					<!--- Run any validations first Subscriber List?  --->					
					$('.btn-save-cp').click();
				}			
						
				<!--- Save Desc if displayed --->	
				if(CurrentSWTStep == (parseInt(jsVarRetVarReadCPs.swtcount) + 3))
				{
					SaveCampaignName(false);
				}
						
				<!--- Save Keyword if displayed --->
				if(CurrentSWTStep == (parseInt(jsVarRetVarReadCPs.swtcount) + 2))
				{
					SaveKeyword(false);
				}	
				
				<!--- Subscriber list selected prior to finish - give chance to preview and or finhs sending with confirmation --->
				<!--- If a blast subsubscriber list has been selected --->
				if($('#SubscriberList').val() > 0)
				{	
					<!--- Setup blast and give user one final confirmation --->									
					BlastNow();
					
					<!--- Skip final finish - blast will call FinalFinishSWT if successful --->
					return;
				}
				
				<!--- Call final finish - will redirect when done --->
				FinalFinishSWT();
															
			});
		
		
			<!--- Start at step 1 - Step 0 is rendered blank/loading by default until this loads async --->
			$("#btn-next-swt").click();
			
		</cfif>
			
		$('#processingPayment').hide(); 
						
		<!--- User Refreshable Stats --->
		$("#btn-reload-blast-stats").click(function()
		{
			<!--- Async load data on previous blasts - helps UI load faster for user --->
			CheckForPreviousBlasts();										
		});
		
		<cfif RetVarGetBatchBlasts.BLASTS.RECORDCOUNT GT 0> 
			<!--- Only show if there has been a blast --->
			<!--- Async load data on previous blasts - helps UI load faster for user --->
			CheckForPreviousBlasts();	
		</cfif>
		
		
<!---
		// Initializes and creates emoji set from sprite sheet
        window.emojiPicker = new EmojiPicker({
          emojiable_selector: '[data-emojiable=true]',
          assetsPath: '/public/sire/assets/emoji-picker/lib/img/',
          popupButtonClasses: 'fa fa-smile-o'
        });
        // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
        // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
        // It can be called as many times as necessary; previously converted input fields will not be converted again
        window.emojiPicker.discover();


		// new EmojiPicker().discover();
--->
			
	});
	
	<!--- Becuase these buttons can be dynamically added - set click method here each time they are added --->
	function BindEditCPInline(inpObj)
	{		
		<!--- Unbind any events already bound to this object - re-render will need to rebind objects --->
		inpObj.off();
		inpObj.unbind();
				
		<!--- Read data from current obj to pre-populate modal window with current data OR read from DB? --->	
		
		inpObj.click(function(){			
			
			<!--- Read Current Data From XMLControlString --->				
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/control-point.cfc?method=ReadCPDataById&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				async: false,
				data:  
				{ 
					INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>", 
					inpQID : $(this).attr('data-control-point-id'),
					inpTemplateFlag : "<cfoutput>#inpTemplateFlag#</cfoutput>",
					
					<cfif adv EQ 1>
						RenderHTML : 0
					<cfelse>
						RenderHTML : 1
					</cfif>					
				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("*Error. No Response from the remote server. Check your connection and try again.");},					  
				success:		
				<!--- Default return function for Async call back --->
				function(d) 
				{																						
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1) 
					{								
						RenderCPEditor(d.CPOBJ, "EDIT", inpObj.parents('.control-point'));
					}
					else
					{
						<!--- No result returned --->		
						bootbox.alert("General Error processing your request. ");
					}			
									
				} 		
					
			});
				
		});		
	}
	
	
	<!--- Becuase these buttons can be dynamically added - set click method here each time they are added --->
	function BindLearnMoreInline(inpObj)
	{		
		<!--- Unbind any events already bound to this object - re-render will need to rebind objects --->
		inpObj.off();
		inpObj.unbind();
		
		inpObj.click(function(){			
			
			<!--- Extra way to get to more information - just click on text body or Learn More to toggle --->
			$(this).find('div').toggle();
				
		});		
	}


	<!--- Becuase these objects can be dynamically added - set click method here each time they are added --->
	function BindAltEditCPInline(inpObj)
	{					
		<!--- Unbind any events alreay bound to this object - re-render will need to rebind objects --->
		inpObj.off();
		inpObj.unbind();
				
		<!--- Read data from current obj to pre-populate modal window with current data OR read from DB? --->	
		
		inpObj.click(function(){	
			$(this).parents('.control-point').find('.control-point-btn-edit').click();
											
		});		
	}

	
	<!--- Becuase these buttons can be dynamically added - set click method here each time they are added --->
	function BindDeleteCPInline(inpObj)
	{	
		<!--- Unbind any events already bound to this object - re-render will need to rebing objects --->
		inpObj.off();
				
		inpObj.click(function(){			
			inpCPObj = $('#cp' + inpObj.attr('data-control-point-id'))	;			
			
			bootbox.confirm('Are you sure you want to delete this Action?', function(r) 
			{
				<!--- Only complete if user comfirms it --->
				if (r == true) 
				{
					$.ajax({
						type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
						url: '/session/sire/models/cfc/control-point.cfc?method=DeleteControlPoint&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
						dataType: 'json',
						async: true,
						data:  
						{ 
							INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>", 
							INPPID : inpObj.attr("data-control-point-physical-id"),
							inpTemplateFlag : "<cfoutput>#inpTemplateFlag#</cfoutput>"
						},					  
						error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},					  
						success:		
						<!--- Default return function for Async call back --->
						function(d) 
						{																						
							<!--- RXRESULTCODE is 1 if everything is OK --->
							if (d.RXRESULTCODE == 1) 
							{																					
								<!--- Remove current Action from page --->															
								inpCPObj.remove();
							
								<!--- Re-Index CPs on HTML page - Physical Ids remain the same. --->	
								reIndexQuestions();
								
								<!--- Reset Question SELECTs to re lookup next time they are needed --->
								QuestionListBox = null;
								QuestionsOnlyListBox= null;
							
							}
							else
							{
								<!--- No result returned --->		
								bootbox.alert("General Error processing your request. ");
							}			
											
						} 		
							
					});
			
				}	
				
				<!--- Return true to close bootbox when complete --->
				return true;
			});
				
		});
	}
	
	<!--- Becuase these buttons can be dynamically added - set click method here each time they are added --->
	function BindAddCPInline(inpObj)
	{	
		<!--- Unbind any events alreay bound to this object - re-render will need to rebing objects --->
		inpObj.off();
				
		inpObj.click(function(){			
			inpCPObj = $('#cp' + inpObj.attr('data-control-point-id'))	;			
			
			<!--- Set this value as the new CPs RQ POSITION --->
			$("#ChooseCPToAdd .btn-add-cp").attr('data-control-point-id', $(this).attr('data-control-point-id'));
			
			<!--- Store the CP this was click on - used for Render Editor --->
			$("#ChooseCPToAdd .btn-add-cp").data('inpCP', $(this).parents('.control-point'));	
						
			<!--- Build an empty Action DataObj --->
			var Options = [];
			var ESOptions = [];
			var Conditions = [];
			
			var ControlPointData = {
					
					'AF' : "NOFORMAT",
					'API_ACT' : "",
					'API_DATA' : "",
					'API_DIR' : "someplace/something",
					'API_DOM' : "somwhere.com",
					'API_PORT' : "80",
					'API_RA' : "",
					'API_TYPE' : "GET",
					'BOFNQ' : "0",
					'CONDITIONS' : Conditions,
					'ERRMSGTXT' : "",
					'GID' : "1",
					'ID' : "0",
					'IENQID' : "0",
					'IHOUR' : "0",
					'IMIN' : "0",
					'IMRNR' : "2",
					'INOON' : "01",
					'INRMO' : "NEXT",
					'ITYPE' : "",
					'IVALUE' : "0",
					'LMAX' : "0",
					'LDIR' : "-1",
					'LMSG' : "",
					'OIG' : "0",
					'OPTIONS' : Options,
					'ESOPTIONS' : ESOptions,
					'REQUIREDANS' : "undefined",
					'RQ' : inpObj.attr('data-control-point-id'),
					'SCDF' : "0",
					'TDESC' : "",
					'TEXT' : "",
					'TYPE' : inpCPObj.find('#AddNewCPType').val(),
					'SWT' : "1",
					'ANSTEMP' : "0",
					'T64' : "0"
											
				};			
															
			RenderCPEditor(ControlPointData, "ADD", inpCPObj );					
				
		});
	}		
		
	<!--- If an item is added to the page and or removed - re-order the CPs to Compare display order --->
	function reIndexQuestions(){
			
		var position = 1;
		
		<!--- The RQ is used to identify the top level CP object in the HTML - Delete uses PID -BUT- HTML is removed by RQ  --->
		position = 1;			
		$.each($('.control-point'), function(index, value) {
		    $(this).attr('id', 'cp' + position);
		    $(this).attr('data-control-rq-id', position);
		    position = position + 1;							
		});
		
		<!--- Reset all the CP identifiers in order shown --->
		position = 1;
		$.each($('.CPNumberOnPage:visible'), function(index, value) {
		   			   												
			$(this).html("" + position);
			position = position + 1;
			
		});	
		
		<!--- Reset all the delete buttons RQs in order shown --->
		position = 1;
		$.each($('.control-point-btn-delete'), function(index, value) {
		   $(this).attr('data-control-point-id', position);
		   position = position + 1;
							
		});
				
		<!--- Reset all the add buttons RQs in order shown --->
		position = 1;
		$.each($('.control-point-btn-add'), function(index, value) {
		   $(this).attr('data-control-point-id', position);
		   position = position + 1;
							
		});
		
		<!--- Reset all the edit buttons RQs in order shown --->
		position = 1;
		$.each($('.control-point-btn-edit'), function(index, value) {
		   $(this).attr('data-control-point-id', position);
		   position = position + 1;
							
		});
								
	}
	
	<!--- If an item is added to the page and or removed - re-order the CPs to Compare display order --->
	function reIndexConditions(inpCPE){
			
		var position = 1;
		<!--- Reset all the CID identifiers in order shown --->
		position = 1;
		$.each(inpCPE.find('.ConditionItem'), function(index, value) {
		   				
			$(this).find('#CID').val(position);
			$(this).find('#CIDLabel').html("#" + position);
			
			position = position + 1;
			
		});	
								
	}
	
	<!--- Logic here modifies the display to only show the options available for the selected CP Type  --->
	function RenderCPEditor(inpCPObj, inpAction, inpCP)
	{			
		<!--- 		
			inpCPObj: is a structure of values for the data for the Action
			inpAction: ADD new or save EDIT
			inpCP: jQuery Obj for the HTML of the Action
		 --->				
	
		<!--- The HTML for Answer and CPEditor are stored in coldfusion variables  AnswerListItemTemplate, CPEditorTemplate ---> 
		<!--- This outputs a previously defined Coldfusion variable as a javascript variable ---> 		
		<cfoutput> 
			var #toScript(AnswerListItemTemplate, "AnswerItemjsVar")#; 
		</cfoutput> 		
		
		<!--- Create a jquery reference to the returned HTML and store it in 'inpAnswerItem' --->
		var inpAnswerItem = $(AnswerItemjsVar);
				
		<!--- The HTML for Answer and CPEditor are stored in coldfusion variables  AnswerListItemTemplate, CPEditorTemplate ---> 
		<!--- This outputs a previously defined Coldfusion variable as a javascript variable ---> 		
		<cfoutput> 
			var #toScript(ExpertSystemListItemTemplate, "ESItemjsVar")#; 
		</cfoutput> 		
		
		<!--- Create a jquery reference to the returned HTML and store it in 'inpESItem' --->
		var inpESItem = $(ESItemjsVar);
		
		<!--- This outputs a previously defined Coldfusion variable as a javascript variable ---> 
		<cfoutput> 
			var #toScript(CPEditorTemplate, "jsVar")#; 
		</cfoutput> 	
		
		<!--- Create a jquery reference to the returned HTML and store it in 'inpCPE' --->
		var inpCPE = $(jsVar);

		inpCPE.find('#AF option').each(function(){

			$(this).removeAttr('selected');
			
			if($(this).attr('value') == inpCPObj.AF){
				$(this).attr('selected', 'selected');
			}
		});

		<!--- Store the CP this was click on - used for later Render Editor hide/show of parent CP --->
		inpCPE.data('inpCP', inpCP);
								
		<!--- Write each value to the current editor --->
		inpCPE.find('#ACTION').val(inpAction);
		
		<!--- Hide the Answer selector by default --->
		inpCPE.find('#AnswerTemplateSelector').hide();
					
		if(inpCPE.find('#ACTION').val() == "ADD")
		{	
			inpCPE.find('#CPSelector').show();
			inpCPE.find('#CPEBody').hide();		
		}
		else
		{
			inpCPE.find('#CPSelector').hide();
			inpCPE.find('#CPEBody').show();			
		}	
		
		<!--- Store the real type in a hidden field --->
		inpCPE.find('#TYPE').val(inpCPObj.TYPE)
		
		<!--- Smaller interface if this is not used --->
		if(inpCPObj.TDESC)
		{			
			inpCPE.find('#TDESC').val(inpCPObj.TDESC);	
			inpCPE.find('#TDESCPLAIN').html(inpCPObj.TDESC);			
			inpCPE.find('#DescLink').hide();
			inpCPE.find('#DescSection').show();

			<!--- Hide this in advanced mode so only textarea is shown --->	
			<cfif adv EQ 1>
	 			inpCPE.find('#TDESCPLAIN').hide(); 
	 		</cfif>	

		}
		else
		{
			inpCPE.find('#TDESC').val('');
			inpCPE.find('#TDESCPLAIN').html('');
			inpCPE.find('#DescLink').show();
			inpCPE.find('#DescSection').hide();	
			inpCPE.find('#TDESCPLAIN').show();				
		}
		
		<!--- Advanced Template feature for hide/show CP on simplified walk through --->
		inpCPE.find('#SWT').val(inpCPObj.SWT);
		
		<!--- Turn on editor to change description - less UI/UX space when not used --->
		inpCPE.find('#DescLink').click(function()
		{						
			inpCPE.find('#DescLink').hide();
			inpCPE.find('#TDESCPLAIN').hide();
			inpCPE.find('#DescSection').show();			
		});
				
		inpCPE.find('#CDFContainer').hide();
		inpCPE.find('#SubscriberListContainer').hide();
				
		<!--- Clear any previous OPTIONS - re add them if inpCPObj.TYPE supports them  --->
		inpCPE.find('.AnswerItem').each(function(id, el){ 	$(el).remove() });
			
		<!--- Default hide the Interval Help Type --->							
		inpCPE.find('.ResponseInterval').hide();
		inpCPE.find('.ControlPointInterval').hide();		
		
		
		<!--- Do this here so TYPE can be changed --->
		SetupEditorDisplayBasedOnType(inpCPE, inpCPObj);				
		
		<!--- Bind click behaviors here to this inpCPE - this allows javascript Closure reference to  the outer inpCPE --->
		inpCPE.find('#ITYPE').change(function(){		
			onChangeIntervalType(inpCPE);						
		});
		
		inpCPE.find('#answers_list').on('click', '.answer_remove', function()
		{
			$(this).parents('.AnswerItem').remove();
			$('#format_answer').change();			
		});
		
		inpCPE.find('#es_list').on('click', '.es_remove', function()
		{
			$(this).parents('.ESItem').remove();						
		});
	
		<!--- Add an empty item to the list - clone inpAnswerItem instead of using up the javascript var - can be reused multiple times this way - simple append will move the entire object and then it cant be reused --->
		inpCPE.find('#answer_add').click(function()
		{	
			inpCPE.find('#answers_list').append(inpAnswerItem.clone());
			inpCPE.find('#answers_list .answer_remove').show();
			
			<!--- Minor detail but remove focus from add button since we dont reload the page --->
			$(this).blur();
		});		
		
		<!--- Add an empty item to the list - clone inpESItem instead of using up the javascript var - can be reused multiple times this way - simple append will move the entire object and then it cant be reused --->
		inpCPE.find('#es_add').click(function()
		{	
			inpCPE.find('#es_list').append(inpESItem.clone());
			inpCPE.find('#es_list .es_remove').show();
			
			<!--- Minor detail but remove focus from add button since we dont reload the page --->
			$(this).blur();
		});	
		
		inpCPE.find('#AddCondition').click(function()
		{	
			<!--- SAMPLE XML we are trying to populate 		
				<COND BOAV='' BOC='=' BOCDV='undefined' BOQ='2' BOTNQ='0' BOV='1' CID='1' TYPE='RESPONSE'></COND>	
				<COND BOAV='Test' BOC='=' BOCDV='{%%INPPA2}' BOQ='undefined' BOTNQ='4' BOV='' CID='2' TYPE='CDF'></COND>	
			 ---> 
			
			<!--- Default blank item --->
			var inpConditionObj = { 
					
				'CID':  inpCPE.find('.ConditionItem').length + 1,
				'TYPE' : 'RESPONSE',
				'BOQ' : '0',
				'BOC' : '=',
				'BOV' : '',
				'BOAV' : '',
				'BOCDV' : '',
				'BOTNQ' : '0',
				'DESC' : 'DESC'					
			}

			<!--- AddContitionToList gets called for loading editor from XML or on adding ne Condition --->
			<!--- Keep the code for this maintained in this function - all in one place - easier to maintain --->
			AddContitionToList(inpConditionObj, inpCPE);
			
			<!--- Minor detail but remove focus from add button since we dont reload the page --->
			$(this).blur();;
			
		});
		
		<!--- Allow user to remove conditions --->
		inpCPE.find('#ConditionsList').on('click', '.RemoveCondition', function()
		{
			<!--- Remove the XML - next save will not include this --->	
			$(this).parents('.ConditionItem').remove();		
			
			<!--- Conditions are applied in order shown - first Compare wins --->
			reIndexConditions(inpCPE);	
		});
		
		
		<!--- Show CPEBody when clicked --->
		inpCPE.find('.btn-add-cp').click(function(){
					alert("KAKAKA");
			<!--- Make sure a valid value is selected --->			
			if(inpCPE.find('#AddNewCPType').val())
			{				
				<!--- Store the real type in a hidden field --->
				inpCPE.find('#TYPE').val(inpCPE.find('#AddNewCPType').val())
				inpCPObj.TYPE = inpCPE.find('#AddNewCPType').val();
			
				<!--- Re-Render the types to turn on and off --->			
				SetupEditorDisplayBasedOnType(inpCPE, inpCPObj);
							
				inpCPE.find('#CPSelector').hide();
				inpCPE.find('#CPEBody').show();	
								
				<!--- Scroll to this objects position - dont make the user search for it. Account for menu hight above it and then a little extra (- 100) --->
				$("body").scrollTop(inpCP.offset().top - 100);
							
			}									
			else
			{
				<!--- Alert to select type --->
				var bootstrapAlert = $('#bootstrapAlert');
				bootstrapAlert.find('.modal-title').text('Warning');
				bootstrapAlert.find('.alert-message').text('Please select an Action Type before moving on.');
				bootstrapAlert.modal('show');				
			}
			
		});
				
		<!--- Show Interval Settings when clicked --->
		inpCPE.find('#IntervalSettingSectionSwitch').click(function(){															
			inpCPE.find('#IntervalSettingSection').toggle();			
		});
		
		<!--- Show Answer Options when clicked --->
		inpCPE.find('#UserSelectableAnswerListSwitch').click(function(){															
			inpCPE.find('#UserSelectableAnswerList').toggle();			
		});		
		
		<!--- Show CPSelector when clicked --->
		inpCPE.find('#SelectAnswerSet').click(function(){	
											
			inpCPE.find('#AnswerTemplateSelector').show();
			inpCPE.find('#SelectAnswerSet').hide();	
		});
		
		<!--- Cancel will NOT save changes --->		
		inpCPE.find('.btn-cancel-answer-list-template').click(function(){
			
			inpCPE.find('#AnswerTemplateSelector').hide();
			inpCPE.find('#SelectAnswerSet').show();
			
		});

		inpCPE.find('.btn-add-answer-list-template').click(function(){
			
			switch($('#ANSLISTTEMPLATE').val())
			{				
				<!--- Yes or No --->
				case '1':									
					AddOptionToList('Yes', 'Yes', '(?i)^y$|yes|si|ya|yup|yea|yeah|sure|ok|okay|all right|very well|of course|by all means|sure|certainly|absolutely|indeed|right|affirmative|agreed|roger|aye|uh-huh|okey|aye|positive|^1$|confirm|in|yass|yas', inpCPE);
					AddOptionToList('No', 'No', '(?i)^n$|no|non|ne|nyet|no way|negative|^0$|out', inpCPE);
				break;
				
				<!--- True or False --->
				case '2':									
					AddOptionToList('True', '1', '', inpCPE);
					AddOptionToList('False', '0', '', inpCPE);
				break;
				
				<!--- Agree or Disagree --->
				case '3':									
					AddOptionToList('Strong Disagree', '1', '', inpCPE);
					AddOptionToList('Disagree', '2', '', inpCPE);
					AddOptionToList('Neither Disagree or Agree', '3', '', inpCPE);
					AddOptionToList('Agree', '4', '', inpCPE);
					AddOptionToList('Strong Agree', '5', '', inpCPE);				
				break;
								
				<!--- Useful or Not --->
				case '4':									
					AddOptionToList('Not useful', '1', '', inpCPE);
					AddOptionToList('Somewhat useful', '2', '', inpCPE);
					AddOptionToList('Useful', '3', '', inpCPE);
					AddOptionToList('Mostly useful', '4', '', inpCPE);
					AddOptionToList('Very useful', '5', '', inpCPE);				
				break;
				
				<!--- Easy or Not --->
				case '5':									
					AddOptionToList('Not easy', '1', '', inpCPE);
					AddOptionToList('Somewhat easy', '2', '', inpCPE);
					AddOptionToList('Easy', '3', '', inpCPE);
					AddOptionToList('Mostly easy', '4', '', inpCPE);
					AddOptionToList('Extremely easy', '5', '', inpCPE);				
				break;
				
				<!--- Likely or Not --->
				case '6':									
					AddOptionToList('Extremely likely', '1', '', inpCPE);
					AddOptionToList('Very likely', '2', '', inpCPE);
					AddOptionToList('Moderately likely', '3', '', inpCPE);
					AddOptionToList('Slightly likely', '4', '', inpCPE);
					AddOptionToList('Not at all likely', '5', '', inpCPE);				
				break;

				<!--- Important or Not --->
				case '7':									
					AddOptionToList('Extremely important', '1', '', inpCPE);
					AddOptionToList('Very important', '2', '', inpCPE);
					AddOptionToList('Moderately important', '3', '', inpCPE);
					AddOptionToList('Slightly important', '4', '', inpCPE);
					AddOptionToList('Not at all important', '5', '', inpCPE);					
				break;

				<!--- Satisfied or Not --->
				case '8':									
					AddOptionToList('Extremely satisfied', '1', '', inpCPE);
					AddOptionToList('Very satisfied', '2', '', inpCPE);
					AddOptionToList('Moderately satisfied', '3', '', inpCPE);
					AddOptionToList('Slightly satisfied', '4', '', inpCPE);
					AddOptionToList('Not at all satisfied', '5', '', inpCPE);					
				break;
								
				<!--- Net Promoter Score --->
				case '9':									
					AddOptionToList('0 Not at all satisfied', '0', '', inpCPE);
					AddOptionToList('1', '1', '', inpCPE);
					AddOptionToList('2', '2', '', inpCPE);
					AddOptionToList('3', '3', '', inpCPE);
					AddOptionToList('4', '4', '', inpCPE);
					AddOptionToList('5', '5', '', inpCPE);
					AddOptionToList('6', '6', '', inpCPE);
					AddOptionToList('7', '7', '', inpCPE);
					AddOptionToList('8', '8', '', inpCPE);
					AddOptionToList('9', '9', '', inpCPE);
					AddOptionToList('10 Extremely satisfied', '10', '', inpCPE);
									
				break;
				
				default:
				
				
				break;				
				
			}
			
			inpCPE.find('#AnswerTemplateSelector').hide();
			inpCPE.find('#SelectAnswerSet').show();			
			
		});
		
		<!--- Show CPSelector when clicked --->
		inpCPE.find('#ChangeCPType').click(function(){	
			
			<!--- Set the value here in case this was entered in EDIT mode where it is not already set --->
			inpCPE.find('#AddNewCPType').val(inpCPE.find('#TYPE').val());
					
			inpCPE.find('#CPSelector').show();
			inpCPE.find('#CPEBody').hide();	
		});
				
		<!--- Allow user to add a group (Subscriber List) --->
		inpCPE.find("#btn-add-group").click(function()
		{	
			<!--- Store the referecne to the current CP editor here in modal so new OPTION can be added --->
			$('#btn-save-group').data('inpCPE', inpCPE);
			
			$('#subscriber_list_name').val('');	
			$('#AddNewSubscriberList').modal('show');
		});	
		
		<!--- Allow user to add a CDF (Custom Data Field) --->
		inpCPE.find("#btn-add-cdf").click(function()
		{	
			<!--- Store the referecne to the current CP editor here in modal so new OPTION can be added --->
			$('#btn-save-cdf').data('inpCPE', inpCPE);
			
			$('#inpCDFName').val('');	
			$('#AddNewCDFModal').modal('show');
		});				
		
		<!--- Cancel will NOT save changes --->		
		inpCPE.find('.btn-cancel-cpe').click(function(){
			
			inpCPE.data('inpCP').find('.CPDisplaySection').show();
				
			inpCPE.parent().remove();
		});
						
		<!--- This will save changes to XMLControlString --->	
		inpCPE.find('.btn-save-cp').click(function(){		
			<!--- Keeping it simple for now - Read every property of the current CP HTML - no ugly code that is difficult to maintain for all the checking what changed (less chance of bugs / easier to understand flow for updates) --->
					
			<!--- Prevent slow connections from letting user click save button multiple times --->
			inpCPE.find('.btn-save-cp').prop("disabled",true);		
							
			var Options = [];
			var ESOptions = [];
			var Conditions = [];			
			
			<!--- Loop over specifed OPTIONS --->
			var OptionCounter = 1;
			inpCPE.find('.AnswerItem').each(function(id, el){
				
				var OptionItem = { 
					
					'AVAL': $(el).find("#AVAL").val(),
					'AVALREG': $(el).find("#AVALREG").val(),
					'ID' : OptionCounter,
					'TEXT' : $(el).find("#OPTION").val()					
				}
				
				Options.push(OptionItem);
				
				OptionCounter++;
						
			});
			
			<!--- Loop over specifed Expert System OPTIONS --->
			var OptionCounter = 1;
			inpCPE.find('.ESItem').each(function(id, el){
				
				var ESOptionItem = { 
					
					'ESREG': ($(el).find('#ESREG').val() == null) ? "" : $(el).find('#ESREG').val(),
					'ESTEXT': ($(el).find('#ESTEXT').val() == null) ? "" : $(el).find('#ESTEXT').val(),
					'ESID' : OptionCounter				
				}
				
				ESOptions.push(ESOptionItem);
				
				OptionCounter++;
						
			});
			
			<!--- Hide the complexity of differing question types from the end user - in UI all are ONESELECTION but do not require answer list --->
			if(Options.length > 0 && inpCPE.find('#TYPE').val() == "SHORTANSWER")
			{				
				inpCPE.find('#TYPE').val("ONESELECTION");				
			}
			
			<!--- Hide the complexity of differing question types from the end user - in UI all are ONESELECTION but do not require answer list --->
			if(Options.length == 0 && inpCPE.find('#TYPE').val() == "ONESELECTION")
			{				
				inpCPE.find('#TYPE').val("SHORTANSWER");				
			}
					
			<!--- Loop over specifed CONDITIONS --->
			var ConditionsCounter = 1;
			inpCPE.find('.ConditionItem').each(function(id, el){
								
				<!--- Overwite RESPONSE2 with Valid RESPONSE --->
				var CurrType = ($(el).find('#CTYPE').val() == null) ? "RESPONSE" : $(el).find('#CTYPE').val();				
				CurrType = (CurrType == 'RESPONSE2') ? "RESPONSE" : CurrType;
				
				<!--- Also show default BOVA text - this text is automatically searched for on single seltions as well as any answer format options --->				
				if(($(el).find('#BOAV').val() == null || $(el).find('#BOAV').val() == '') && ($(el).find('#BOV').val() != null && $(el).find('#BOV').val() != ''))
				{	
					$(el).find('#BOAV').val($(el).find('#BOV :selected').text());								
				}
								
				var ConditionItem = { 					
					'CID':  ConditionsCounter,
					'TYPE' : CurrType,
					'BOQ' : ($(el).find('#BOQ').val() == null) ? "0" : $(el).find('#BOQ').val(),
					'BOC' : ($(el).find('#BOC').val() == null) ? "=" : $(el).find('#BOC').val(),
					'BOV' : ($(el).find('#BOV').val() == null) ? "" : $(el).find('#BOV').val(),
					'BOAV' : ($(el).find('#BOAV').val() == null) ? "" : $(el).find('#BOAV').val(),
					'BOCDV' : ($(el).find('#BOCDV').val() == null) ? "" : $(el).find('#BOCDV').val(),
					'BOTNQ' : ($(el).find('#BOTNQ').val() == null) ? "0" : $(el).find('#BOTNQ').val(),
					'DESC' : 'DESC'					
				}
				
				Conditions.push(ConditionItem);
				
				ConditionsCounter++;
						
			});
									
			<!--- If this is not API type - delete default API values  - makes string smaller in DB--->
			if(inpCPE.find('#TYPE').val() != "API")
			{				
				inpCPE.find('#API_ACT').val("");
				inpCPE.find('#API_DATA').val("");
				inpCPE.find('#API_DIR').val("");
				inpCPE.find('#API_DOM').val("");
				inpCPE.find('#API_PORT').val("");
				inpCPE.find('#API_RA').val("");
				inpCPE.find('#API_TYPE').val("");				
			}
					 			
			<!--- Cleanup Nulls in this structure - the value from key [XXX] is NULL, which is the same as not existing in CFML --->
			var ControlPointData = {
					
				'AF' : inpCPE.find('#AF').val(),
				'API_ACT' : (inpCPE.find('#API_ACT').val() == null) ? "" : inpCPE.find('#API_ACT').val(),
				'API_DATA' : (inpCPE.find('#API_DATA').val() == null) ? "" : inpCPE.find('#API_DATA').val(),
				'API_DIR' : (inpCPE.find('#API_DIR').val() == null) ? "" : inpCPE.find('#API_DIR').val(),
				'API_DOM' : (inpCPE.find('#API_DOM').val() == null) ? "" : inpCPE.find('#API_DOM').val(),
				'API_PORT' : (inpCPE.find('#API_PORT').val() == null) ? "" : inpCPE.find('#API_PORT').val(),
				'API_RA' : (inpCPE.find('#API_RA').val() == null) ? "" : inpCPE.find('#API_RA').val(),
				'API_TYPE' : (inpCPE.find('#API_TYPE').val() == null) ? "" : inpCPE.find('#API_TYPE').val(),
				'BOFNQ' : (inpCPE.find('#BOFNQ').val() == null) ? "0" : inpCPE.find('#BOFNQ').val(),
				'CONDITIONS' : Conditions,
				'ERRMSGTXT' : "",
				'GID' : "1",
				'ID' : inpCPE.find('#ID').val(),
				'IENQID' :  (inpCPE.find('#IENQID').val() == null) ? "0" : inpCPE.find('#IENQID').val(),
				'IHOUR' :  (inpCPE.find('#IHOUR').val() == null) ? "10" : inpCPE.find('#IHOUR').val(),
				'IMIN' :  (inpCPE.find('#IMIN').val() == null) ? "00" : inpCPE.find('#IMIN').val(),
				'IMRNR' :  (inpCPE.find('#IMRNR').val() == null) ? "0" : inpCPE.find('#IMRNR').val(),
				'INOON' :  (inpCPE.find('#INOON').val() == null) ? "01" : inpCPE.find('#INOON').val(),
				'INRMO' :  (inpCPE.find('#INRMO').val() == null) ? "NEXT" : inpCPE.find('#INRMO').val(),
				'ITYPE' :  (inpCPE.find('#ITYPE').val() == null) ? "SECONDS" : inpCPE.find('#ITYPE').val(),
				'IVALUE' :  (inpCPE.find('#IVALUE').val() == null) ? "0" : inpCPE.find('#IVALUE').val(),				
				'LMAX' :  (inpCPE.find('#LMAX').val() == null) ? "0" : inpCPE.find('#LMAX').val(),
				'LDIR' :  (inpCPE.find('#LDIR').val() == null) ? "-1" : inpCPE.find('#LDIR').val(),
				'LMSG' :  (inpCPE.find('#LMSG').val() == null) ? "" : inpCPE.find('#LMSG').val(),
				'OIG' : (inpCPE.find('#OIG').val() == null) ? "0" : inpCPE.find('#OIG').val(),
				'OPTIONS' : Options,
				'ESOPTIONS' : ESOptions,
				'REQUIREDANS' : "undefined",
				'RQ' : inpCPE.find('#RQ').val(),
				'SCDF' : (inpCPE.find('#SCDF').val() == null) ? "0" : inpCPE.find('#SCDF').val(),
				'TDESC' : inpCPE.find('#TDESC').val(),
				'TEXT' : inpCPE.find('#CPText').val(),
				'TYPE' : inpCPE.find('#TYPE').val(),
				'SWT' : inpCPE.find('#SWT').val(),
				'ANSTEMP' : (inpCPE.find('#ANSTEMP').val() == null) ? "" : inpCPE.find('#ANSTEMP').val(),
				'T64' : "0"
			};
					 			
			if(inpCPE.find('#ACTION').val() == "ADD")
			{																								
				<!--- Add to XMLControlString --->				
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url: '/session/sire/models/cfc/control-point.cfc?method=Add_ControlPoint&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					async: false,
					data:  
					{ 
						INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>", 
						controlPoint : JSON.stringify(ControlPointData),
						inpSimpleViewFlag : "<cfoutput>#inpSimpleViewFlag#</cfoutput>",
						inpTemplateFlag : "<cfoutput>#inpTemplateFlag#</cfoutput>"						
					},					  
					error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},					  
					success:		
					<!--- Default return function for Async call back --->
					function(d) 
					{																						
						<!--- RXRESULTCODE is 1 if everything is OK --->
						if (d.RXRESULTCODE == 1) 
						{	
							<!--- Create a jquery reference to the returned HTML and store it in 'el' --->
							var el = $(d.HTML);																				
														
							<!--- When adding new HTML we must add bind events to any buttons or clickable objects --->
							
							<!--- Bind delete button action to newly created object --->
							BindDeleteCPInline(el.find('.control-point-btn-delete'));							
							
							<!--- Bind edit button action to newly created object --->	
							BindEditCPInline(el.find('.control-point-btn-edit'));
							BindAltEditCPInline(el.find('.control-point-body'));
														
							<!--- Bind add button action to newly created object --->
							BindAddCPInline(el.find('.control-point-btn-add'));
							
							<!--- Add to HTML --->
							<!--- insertBefore top CP parent of the add button --->		
							$(el).insertBefore($( "#" + inpCP.attr('id')));
							
							inpCPE.parent().remove();
													
							<!--- Re-Index CPs on HTML page - Physical Ids remain the same. --->	
							reIndexQuestions();	
							
							<!--- Reset Question SELECTs to re lookup next time they are needed --->
							QuestionListBox = null;		
							QuestionsOnlyListBox = null;	
																					
							<!--- Scroll to this objects position - dont make the user search for it. Account for menu hight above it and then a little extra (- 100) --->
							$("body").scrollTop($(el).offset().top - 100);
										
						}
						else
						{
							<!--- No result returned --->		
							bootbox.alert("General Error processing your request.");
						}			
										
					} 		
						
				});
				
			}
			else if(inpCPE.find('#ACTION').val() == "EDIT")
			{							
				<!--- Save to XMLControlString --->				
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url: '/session/sire/models/cfc/control-point.cfc?method=CP_Save&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					async: false,
					data:  
					{ 
						INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>", 
						controlPoint : JSON.stringify(ControlPointData),
						inpSimpleViewFlag : "<cfoutput>#inpSimpleViewFlag#</cfoutput>",
						inpTemplateFlag : "<cfoutput>#inpTemplateFlag#</cfoutput>"	 
					},					  
					error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},					  
					success:		
					<!--- Default return function for Async call back --->
					function(d) 
					{																						
						<!--- RXRESULTCODE is 1 if everything is OK --->
						if (d.RXRESULTCODE == 1) 
						{	
							
							<!--- Create a jquery reference to the returned HTML and store it in 'el' --->
							var el = $(d.HTML);																				
							
							<!--- When adding new HTML we must add bind events to any buttons or clickable objects --->
							
							<!--- Replace existing content with new render --->
							$('#cp' + $(el).attr('data-control-rq-id')).html($(el).html());	
									
							<!--- Hide these option on walkthrough ---> 
							<cfif swt EQ 0>															
								<!--- Scroll to this objects position - dont make the user search for it. Account for menu hight above it and then a little extra (- 100) --->
								$("body").scrollTop($('#cp' + $(el).attr('data-control-rq-id')).offset().top - 100);
							</cfif>
							
							<!--- Bind delete button action to newly created object --->
							BindDeleteCPInline($('#cp' + $(el).attr('data-control-rq-id')).find('.control-point-btn-delete'));							
							
							<!--- Bind edit button action to newly created object --->	
							BindEditCPInline($('#cp' + $(el).attr('data-control-rq-id')).find('.control-point-btn-edit'));
							BindAltEditCPInline($('#cp' + $(el).attr('data-control-rq-id')).find('.control-point-body'));							
														
							<!--- Bind add button action to newly created object --->
							BindAddCPInline($('#cp' + $(el).attr('data-control-rq-id')).find('.control-point-btn-add'));
													
							<!--- Re-Index CPs on HTML page - Physical Ids remain the same. --->	
							reIndexQuestions();
							
							<!--- Reset Question SELECTs to re lookup next time they are needed --->
							QuestionListBox = null;
							QuestionsOnlyListBox = null;
														
						}
						else
						{
							<!--- No result returned --->		
							bootbox.alert("General Error processing your request.");
						}			
										
					} 		
						
				});
				
			}		
					
		});		
								
		<!--- Initialize any interval options --->		
		onChangeIntervalType(inpCPE);
				
		if(inpCPE.find('#ACTION').val() == "ADD")
		{			
			inpCPE.find('.modal-title').html( 'New Action:<br/> ');
		}
		else
		{
			<!--- Show numer on page - not actual RQ in case advanced stuff is hidden --->			
			inpCPE.find('.modal-title').html( '#' + inpCPE.data('inpCP').find('.CPNumberOnPage').html() + ':');
		}
		
		inpCPE.find('#RQ').val(inpCPObj.RQ);
		inpCPE.find('#ID').val(inpCPObj.ID);	
		
		inpCPE.find('.btn-save-cp').prop("disabled",false);
			
		var el = $('<div class="col-sm-12 mb15"></div>');
		el.append(inpCPE);
				
		var $ref = null;
		
		<!--- Change location of editor --->	
		if(inpCPE.find('#ACTION').val() == "ADD")
		{														
			$ref = inpCP.prepend(el);
<!--- 			// inpCPE.data('inpCP').find('.CPDisplaySection').hide(); --->
		}
		else
		{
			$ref = inpCP.append(el);
			inpCPE.data('inpCP').find('.CPDisplaySection').hide();
		}
				
		<!---  RE-intitialize any newly added SELECT2 boxes --->
		$ref.find(".Select2").select2( { theme: "bootstrap"} ); 		
		
		<!--- Hide these option on walkthrough ---> 
    	<cfif swt EQ 0>
			<!--- Scroll to this objects position - dont make the user search for it. Account for menu hight above it and then a little extra (- 100) --->
			$("body").scrollTop(inpCPE.offset().top - 100);
		</cfif>		
		
		<!--- Nicely set focus to end of existing CP Text - Do this here only after section is .show()n  - also only do this inside click event with asysnc=false jquery calls or IOS will ignore this --->	
		$ref.find('#CPText').focus();
		var tmpStr2 = $ref.find('#CPText').val();
		$ref.find('#CPText').val('');
		$ref.find('#CPText').val(tmpStr2);
		
		
		// Initializes and creates emoji set from sprite sheet
        window.emojiPicker = new EmojiPicker({
          emojiable_selector: '[data-emojiable=true]',
          assetsPath: '/public/sire/assets/emoji-picker/lib/img/',
          popupButtonClasses: 'fa fa-smile-o'
        });
        // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
        // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
        // It can be called as many times as necessary; previously converted input fields will not be converted again
        window.emojiPicker.discover();
        
		// new EmojiPicker().discover();
			
	}		
	
	function SetupEditorDisplayBasedOnType(inpCPE, inpCPObj)
	{	
			
		<!--- Select box does not line up if default value not real --->
		if(inpCPObj.INOON == "" || inpCPObj.INOON == "0")
		{
			<!--- PM by default! --->
			inpCPObj.INOON = "01";
		}
							
		var QuestionSelectObjs = [];
		var tmpStr = '';
													
		switch(inpCPObj.TYPE) 
		{
			
			case 'STATEMENT':
									
				<!--- Set friendly action name --->					
				inpCPE.find('#CPTYPE').html('Simple Message');
			
				inpCPE.find('#CPText').val(inpCPObj.TEXT);
																
				<!--- Show Action Text --->
				inpCPE.find('#ActionTextSection').show();				
				
				<!--- Nicely set focus to end of existing CP Text --->	
				inpCPE.find('#CPText').focus();
				tmpStr = inpCPE.find('#CPText').val();
				inpCPE.find('#CPText').val('');
				inpCPE.find('#CPText').val(tmpStr);
				
				// tinymce.get('CPText').setContent(inpCPObj.TEXT);
								
				<!--- Hide Interval Options container - not used for this CP Type --->
				inpCPE.find('#IntervalOptions').hide();
				
				<!--- Hide Answer Options container - not used for this CP Type --->
				inpCPE.find('#AnswerOptionsContainer').hide();
				
				<!--- Hide Branch Conditions container - not used for this CP Type --->
				inpCPE.find('#BranchOptionsContainer').hide();	
				
				<!--- Hide API Options container - not used for this CP Type --->
				inpCPE.find('#APIOptions').hide();
				
				<!--- Hide Expert System Options container - not used for this CP Type --->
				inpCPE.find('#ESOptions').hide();
				
				<!--- Hide CDF Container --->
				inpCPE.find('#CDFContainer').hide();
				
				<!--- Hise Subscriber List Container --->
				inpCPE.find('#SubscriberListContainer').hide();
				
			break;
	
			case 'INTERVAL':
				
				<!--- Set friendly action name --->
				inpCPE.find('#CPTYPE').html('Action Interval');
			
				<!--- Hide Action Text --->
				inpCPE.find('#ActionTextSection').hide();	
				
				<!--- Set interval values --->
				inpCPE.find('#ITYPE').val(inpCPObj.ITYPE);
				inpCPE.find('#IVALUE').val(inpCPObj.IVALUE);
				inpCPE.find('#IMIN').val(inpCPObj.IMIN);
				inpCPE.find('#IHOUR').val(inpCPObj.IHOUR);
				inpCPE.find('#INOON').val(inpCPObj.INOON);
				inpCPE.find('#IMRNR').val(inpCPObj.IMRNR);
				inpCPE.find('#INRMO').val(inpCPObj.INRMO);
				
				<!--- Less trips to server and DB - send array of select boxes to populate --->
				QuestionSelectObjs = [];
				QuestionSelectObjs.push(inpCPE.find('#IENQID'));
				inpCPE.find('#IENQID').attr("inpVal", inpCPObj.IENQID);
					
				<!--- Load list of possible CPs to select from --->
				PopulateQuestionSelectBox(QuestionSelectObjs);
				
				<!--- Default hide the Response Interval Help Type --->							
				inpCPE.find('.ResponseInterval').hide();
				<!--- Default show the Contorl Flow Interval Help Type --->
				inpCPE.find('.ControlPointInterval').show();	
				
				<!--- Show the Interval Options for this CP Type --->				
				inpCPE.find('#IntervalOptions').show();
				
				<!--- Hide Text container - not used for this CP Type --->		
				inpCPE.find('#cp_textarea').hide();
				
				inpCPE.find('#CPText').val(inpCPObj.TEXT);
															
				<!--- Hide Answer Options container - not used for this CP Type --->
				inpCPE.find('#AnswerOptionsContainer').hide();
				
				<!--- Hide Branch Conditions container - not used for this CP Type --->
				inpCPE.find('#BranchOptionsContainer').hide();	
				
				<!--- Hide API Options container - not used for this CP Type --->
				inpCPE.find('#APIOptions').hide();
				
				<!--- Hide Expert System Options container - not used for this CP Type --->
				inpCPE.find('#ESOptions').hide();
				
				<!--- Hide CDF Container --->
				inpCPE.find('#CDFContainer').hide();
				
				<!--- Hide Subscriber List Container --->
				inpCPE.find('#SubscriberListContainer').hide();
				
				<!--- Show Intervals Container --->
				inpCPE.find('#IntervalOptionsSubSection').show();
						
			break;
			
			<!--- Samne UI for both types  --->
			case 'SHORTANSWER':
			<!--- Set friendly action name --->
				inpCPE.find('#CPTYPE').html('Question & Wait for Answer');
			
				<!--- Set interval values --->
				inpCPE.find('#ITYPE').val(inpCPObj.ITYPE);
				inpCPE.find('#IVALUE').val(inpCPObj.IVALUE);
				inpCPE.find('#IMIN').val(inpCPObj.IMIN);
				inpCPE.find('#IHOUR').val(inpCPObj.IHOUR);
				inpCPE.find('#INOON').val(inpCPObj.INOON);
				inpCPE.find('#IMRNR').val(inpCPObj.IMRNR);
				inpCPE.find('#INRMO').val(inpCPObj.INRMO);
				
				<!--- Less trips to server and DB - send array of select boxes to populate --->
				QuestionSelectObjs = [];
				QuestionSelectObjs.push(inpCPE.find('#IENQID'));
				inpCPE.find('#IENQID').attr("inpVal", inpCPObj.IENQID);
					
				<!--- Load list of possible CPs to select from --->
				PopulateQuestionSelectBox(QuestionSelectObjs);
								
				<!--- Default show the Response Interval Help Type --->							
				inpCPE.find('.ResponseInterval').show();
				<!--- Default hide the Contorl Flow Interval Help Type --->
				inpCPE.find('.ControlPointInterval').hide();			
	
				<!--- Show the Interval Options for this CP Type --->
				<cfif swt EQ 1>
					inpCPE.find('#IntervalOptions').hide();				
				<cfelse>
					inpCPE.find('#IntervalOptions').show();				
				</cfif>
				
				inpCPE.find('#CPText').val(inpCPObj.TEXT);
				
				<!--- Show Action Text --->
				inpCPE.find('#ActionTextSection').show();	
				
				<!--- Nicely set focus to end of existing CP Text --->	
				inpCPE.find('#CPText').focus();
				tmpStr = inpCPE.find('#CPText').val();
				inpCPE.find('#CPText').val('');
				inpCPE.find('#CPText').val(tmpStr);
								
				<!--- If only default interval settings are set - dont show by default --->
				if((inpCPObj.ITYPE == "" || inpCPObj.ITYPE == "SECONDS") && inpCPObj.IVALUE == "0" && inpCPObj.IENQID == "0" && inpCPObj.IMRNR == "2" && inpCPObj.INRMO == "END")
				{
					
					
					inpCPE.find('#IntervalSettingSection').hide();	
				}
				
				
				<!--- Read options from inpCPObj --->
				var arrayLength = inpCPObj.OPTIONS.length;
				for (var i = 0; i < arrayLength; i++) {
				    AddOptionToList(inpCPObj.OPTIONS[i].TEXT, inpCPObj.OPTIONS[i].AVAL, inpCPObj.OPTIONS[i].AVALREG, inpCPE);				    
				}
				
				<!--- Hide option by default if there are none specified --->
				if(arrayLength == 0)
				{
					inpCPE.find('#UserSelectableAnswerList').hide();					
				}

				inpCPE.find('#answers_list .answer_remove').show();
				
				<!--- Show the Answer Options container --->
				inpCPE.find('#AnswerOptionsContainer').hide();
				
				<!--- Hide Branch Conditions container - not used for this CP Type --->
				inpCPE.find('#BranchOptionsContainer').hide();	
				
				<!--- Hide API Options container - not used for this CP Type --->
				inpCPE.find('#APIOptions').hide();
				
				<!--- Hide Expert System Options container - not used for this CP Type --->
				inpCPE.find('#ESOptions').hide();
				
				<!--- Hide CDF Container --->
				inpCPE.find('#CDFContainer').hide();
				
				<!--- Hise Subscriber List Container --->
				inpCPE.find('#SubscriberListContainer').hide();
			
			break;


			case 'ONESELECTION':
			
				<!--- Set friendly action name --->
				inpCPE.find('#CPTYPE').html('Question & Wait for Answer');
			
				<!--- Set interval values --->
				inpCPE.find('#ITYPE').val(inpCPObj.ITYPE);
				inpCPE.find('#IVALUE').val(inpCPObj.IVALUE);
				inpCPE.find('#IMIN').val(inpCPObj.IMIN);
				inpCPE.find('#IHOUR').val(inpCPObj.IHOUR);
				inpCPE.find('#INOON').val(inpCPObj.INOON);
				inpCPE.find('#IMRNR').val(inpCPObj.IMRNR);
				inpCPE.find('#INRMO').val(inpCPObj.INRMO);
				
				<!--- Less trips to server and DB - send array of select boxes to populate --->
				QuestionSelectObjs = [];
				QuestionSelectObjs.push(inpCPE.find('#IENQID'));
				inpCPE.find('#IENQID').attr("inpVal", inpCPObj.IENQID);
					
				<!--- Load list of possible CPs to select from --->
				PopulateQuestionSelectBox(QuestionSelectObjs);
								
				<!--- Default show the Response Interval Help Type --->							
				inpCPE.find('.ResponseInterval').show();
				<!--- Default hide the Contorl Flow Interval Help Type --->
				inpCPE.find('.ControlPointInterval').hide();			
	
				<!--- Show the Interval Options for this CP Type --->
				inpCPE.find('#IntervalOptions').show();	
				
				inpCPE.find('#CPText').val(inpCPObj.TEXT);
				
				<!--- Show Action Text --->
				inpCPE.find('#ActionTextSection').show();	
				
				<!--- Nicely set focus to end of existing CP Text --->	
				inpCPE.find('#CPText').focus();
				tmpStr = inpCPE.find('#CPText').val();
				inpCPE.find('#CPText').val('');
				inpCPE.find('#CPText').val(tmpStr);
								
				<!--- If only default interval settings are set - dont show by default --->
				if((inpCPObj.ITYPE == "" || inpCPObj.ITYPE == "SECONDS") && inpCPObj.IVALUE == "0" && inpCPObj.IENQID == "0" && inpCPObj.IMRNR == "2" && inpCPObj.INRMO == "END")
				{	
					inpCPE.find('#IntervalSettingSection').hide();	
				}
								
				<!--- Read options from inpCPObj --->
				var arrayLength = inpCPObj.OPTIONS.length;
				for (var i = 0; i < arrayLength; i++) {
				    AddOptionToList(inpCPObj.OPTIONS[i].TEXT, inpCPObj.OPTIONS[i].AVAL, inpCPObj.OPTIONS[i].AVALREG, inpCPE);				    
				}
				
				<!--- Hide option by default if there are none specified --->
				if(arrayLength == 0)
				{
					inpCPE.find('#UserSelectableAnswerList').hide();					
				}

				inpCPE.find('#answers_list .answer_remove').show();
				
				<!--- Show the Answer Options container --->
				inpCPE.find('#AnswerOptionsContainer').show();
				
				<!--- Hide Branch Conditions container - not used for this CP Type --->
				inpCPE.find('#BranchOptionsContainer').hide();	
				
				<!--- Hide API Options container - not used for this CP Type --->
				inpCPE.find('#APIOptions').hide();
				
				<!--- Hide Expert System Options container - not used for this CP Type --->
				inpCPE.find('#ESOptions').hide();
				
				<!--- Hide CDF Container --->
				inpCPE.find('#CDFContainer').hide();
				
				<!--- Hise Subscriber List Container --->
				inpCPE.find('#SubscriberListContainer').hide();
			
			break;
			
			
			case 'OPTIN':
				
				<!--- Set friendly action name --->
				inpCPE.find('#CPTYPE').html('Capture Opt In');
			
				inpCPE.find('#OIG').val( inpCPObj.OIG );
				
				inpCPE.find('#CPText').val(inpCPObj.TEXT);
				
				<!--- Show Action Text --->
				inpCPE.find('#ActionTextSection').hide();	
				
				<!--- Show the OPT IN Container --->
				inpCPE.find('#SubscriberListContainer').show();
				
				<!--- Hide Answer Options container - not used for this CP Type --->
				inpCPE.find('#AnswerOptionsContainer').hide();
				
				<!--- Hide Interval Options container - not used for this CP Type --->
				inpCPE.find('#IntervalOptions').hide();
				
				<!--- Hide Branch Conditions container - not used for this CP Type --->
				inpCPE.find('#BranchOptionsContainer').hide();	
				
				<!--- Hide API Options container - not used for this CP Type --->
				inpCPE.find('#APIOptions').hide();
				
				<!--- Hide Expert System Options container - not used for this CP Type --->
				inpCPE.find('#ESOptions').hide();
				
				<!--- Hide CDF Container --->
				inpCPE.find('#CDFContainer').hide();
				
				PopulateSubscriberListSelectBox(inpCPE.find('#OIG'), inpCPObj.OIG)
				
				
			break;
			
										
			case 'CDF':
			
				<!--- Set friendly action name --->
				inpCPE.find('#CPTYPE').html('Capture Custom Data Field');
			
				<!--- Set the curretn value for SCDF --->			
				inpCPE.find('#SCDF').val(inpCPObj.SCDF);
			
				<!--- Show the CDF Container --->
				inpCPE.find('#CDFContainer').show();
				
				<!--- Hide Action Text --->
				inpCPE.find('#ActionTextSection').hide();	
				
				<!--- Hide Answer Options container - not used for this CP Type --->
				inpCPE.find('#AnswerOptionsContainer').hide();
				
				<!--- Hide Interval Options container - not used for this CP Type --->
				inpCPE.find('#IntervalOptions').hide();
				
				<!--- Hide Branch Conditions container - not used for this CP Type --->
				inpCPE.find('#BranchOptionsContainer').hide();	
				
				<!--- Hide API Options container - not used for this CP Type --->
				inpCPE.find('#APIOptions').hide();
				
				<!--- Hide Expert System Options container - not used for this CP Type --->
				inpCPE.find('#ESOptions').hide();
				
				<!--- Hise Subscriber List Container --->
				inpCPE.find('#SubscriberListContainer').hide();
								
				PopulateCDFSelectBox(inpCPE.find('#SCDF'), inpCPObj.SCDF)
				
			break;
			
			case 'TRAILER':
			
				<!--- Set friendly action name --->
				inpCPE.find('#CPTYPE').html('Final Message - This will end the conversation');
			
				inpCPE.find('#CPText').val(inpCPObj.TEXT);
				
				<!--- Show Action Text --->
				inpCPE.find('#ActionTextSection').show();
				
				<!--- Nicely set focus to end of existing CP Text --->	
				inpCPE.find('#CPText').focus();
				tmpStr = inpCPE.find('#CPText').val();
				inpCPE.find('#CPText').val('');
				inpCPE.find('#CPText').val(tmpStr);
											
				<!--- Hide Interval Options container - not used for this CP Type --->
				inpCPE.find('#IntervalOptions').hide();
								
				<!--- Hide Answer Options container - not used for this CP Type --->
				inpCPE.find('#AnswerOptionsContainer').hide();
				
				<!--- Hide Branch Conditions container - not used for this CP Type --->
				inpCPE.find('#BranchOptionsContainer').hide();	
				
				<!--- Hide API Options container - not used for this CP Type --->
				inpCPE.find('#APIOptions').hide();
				
				<!--- Hide Expert System Options container - not used for this CP Type --->
				inpCPE.find('#ESOptions').hide();
				
				<!--- Hide CDF Container --->
				inpCPE.find('#CDFContainer').hide();
				
				<!--- Hise Subscriber List Container --->
				inpCPE.find('#SubscriberListContainer').hide();

			
			break;		
			
			case 'BRANCH':
			
				<!--- Set friendly action name --->
				inpCPE.find('#CPTYPE').html('Rules Engine');
			
				inpCPE.find('#CPText').val(inpCPObj.TEXT);
				
				<!--- Hide Action Text --->
				inpCPE.find('#ActionTextSection').hide();	
				
				<!--- Less trips to server and DB - send array of select boxes to populate --->
				QuestionSelectObjs = [];
							
				QuestionSelectObjs.push(inpCPE.find('#BOFNQ'));				
				inpCPE.find('#BOFNQ').attr("inpVal", inpCPObj.BOFNQ);
										
				<!--- Load list of possible CPs to move to --->
				PopulateQuestionSelectBox(QuestionSelectObjs);									
			
				<!--- Hide Interval Options container - not used for this CP Type --->
				inpCPE.find('#IntervalOptions').hide();
								
				<!--- Hide Answer Options container - not used for this CP Type --->
				inpCPE.find('#AnswerOptionsContainer').hide();
				
				<!--- Hide API Options container - not used for this CP Type --->
				inpCPE.find('#APIOptions').hide();
				
				<!--- Hide Expert System Options container - not used for this CP Type --->
				inpCPE.find('#ESOptions').hide();
				
				<!--- Hide CDF Container --->
				inpCPE.find('#CDFContainer').hide();
				
				<!--- Hise Subscriber List Container --->
				inpCPE.find('#SubscriberListContainer').hide();
				
				<!--- Show Branch Conditions container --->
				inpCPE.find('#BranchOptionsContainer').show();	
							
				
				<!--- Remove all old conditions --->
			    inpCPE.find('#ConditionsList').empty();          
                    
				<!--- Read conditions from inpCPObj --->																
				<!--- No condition is required by default - empty structure is OK--->
								
				var arrayLength = inpCPObj.CONDITIONS.length;
				for (var ic = 0; ic < arrayLength; ic++) {
				    AddContitionToList(inpCPObj.CONDITIONS[ic], inpCPE);				    
				}
				
				<!--- Conditions are applied in order shown - first Compare wins --->
				reIndexConditions(inpCPE);
			
			break;
			
			case 'API':
				
				<!--- Set friendly action name --->
				inpCPE.find('#CPTYPE').html('API Call');
												
				inpCPE.find('#CPText').val(inpCPObj.TEXT);
				
				inpCPE.find('#API_ACT').val(inpCPObj.API_ACT);
				inpCPE.find('#API_DATA').val(inpCPObj.API_DATA);
				inpCPE.find('#API_DIR').val(inpCPObj.API_DIR);
				inpCPE.find('#API_DOM').val(inpCPObj.API_DOM);
				inpCPE.find('#API_PORT').val(inpCPObj.API_PORT);
				inpCPE.find('#API_RA').val(inpCPObj.API_RA);
				inpCPE.find('#API_TYPE').val(inpCPObj.API_TYPE);
				
				<!--- Show API Options container  --->
				inpCPE.find('#APIOptions').show();
				
				<!--- Hide Action Text --->
				inpCPE.find('#ActionTextSection').hide();	
								
				<!--- Hide Interval Options container - not used for this CP Type --->
				inpCPE.find('#IntervalOptions').hide();
				
				<!--- Hide Answer Options container - not used for this CP Type --->
				inpCPE.find('#AnswerOptionsContainer').hide();
				
				<!--- Hide Branch Conditions container - not used for this CP Type --->
				inpCPE.find('#BranchOptionsContainer').hide();	
				
				<!--- Hide CDF Container --->
				inpCPE.find('#CDFContainer').hide();
				
				<!--- Hise Subscriber List Container --->
				inpCPE.find('#SubscriberListContainer').hide();
				
				<!--- Hide Expert System Options container - not used for this CP Type --->
				inpCPE.find('#ESOptions').hide();
				
			break;

			case 'ES':
				
				<!--- Set friendly action name --->
				inpCPE.find('#CPTYPE').html('Expert System ("Chat Bot")');
												
				inpCPE.find('#CPText').val(inpCPObj.TEXT);
				
				inpCPE.find('#API_ACT').val(inpCPObj.API_ACT);
				inpCPE.find('#API_DATA').val(inpCPObj.API_DATA);
				inpCPE.find('#API_DIR').val(inpCPObj.API_DIR);
				inpCPE.find('#API_DOM').val(inpCPObj.API_DOM);
				inpCPE.find('#API_PORT').val(inpCPObj.API_PORT);
				inpCPE.find('#API_RA').val(inpCPObj.API_RA);
				inpCPE.find('#API_TYPE').val(inpCPObj.API_TYPE);
				
				<!--- Show ES Options container  --->
				inpCPE.find('#ESOptions').show();
												
				<!--- Read options from inpCPObj --->
				var arrayLength = inpCPObj.ESOPTIONS.length;
				for (var i = 0; i < arrayLength; i++) {
				    
				    AddESOptionToList(inpCPObj.ESOPTIONS[i].ESID, inpCPObj.ESOPTIONS[i].ESTEXT, inpCPObj.ESOPTIONS[i].ESREG, inpCPE)
				}
				
				<!--- Hide option by default if there are none specified --->
				if(arrayLength == 0)
				{
										
				}
								
				inpCPE.find('#es_list .es_remove').show();
				
				<!--- Show API Options container  --->
				inpCPE.find('#APIOptions').hide();
				
				<!--- Hide Action Text --->
				inpCPE.find('#ActionTextSection').hide();	
								
				<!--- Hide Interval Options container - not used for this CP Type --->
				inpCPE.find('#IntervalOptions').hide();
				
				<!--- Hide Answer Options container - not used for this CP Type --->
				inpCPE.find('#AnswerOptionsContainer').hide();
				
				<!--- Hide Branch Conditions container - not used for this CP Type --->
				inpCPE.find('#BranchOptionsContainer').hide();	
				
				<!--- Hide CDF Container --->
				inpCPE.find('#CDFContainer').hide();
				
				<!--- Hise Subscriber List Container --->
				inpCPE.find('#SubscriberListContainer').hide();
				
			break;
			
			case 'RESET':
			
				<!--- Set friendly action name --->
				inpCPE.find('#CPTYPE').html('Drip Marketing Reset');
																
				inpCPE.find('#CPText').val(inpCPObj.TEXT);
				
				<!--- Hide Action Text --->
				inpCPE.find('#ActionTextSection').hide();					
								
				<!--- Hide Interval Options container - not used for this CP Type --->
				inpCPE.find('#IntervalOptions').hide();
				
				<!--- Hide Answer Options container - not used for this CP Type --->
				inpCPE.find('#AnswerOptionsContainer').hide();
				
				<!--- Hide Branch Conditions container - not used for this CP Type --->
				inpCPE.find('#BranchOptionsContainer').hide();	
				
				<!--- Hide API Options container - not used for this CP Type --->
				inpCPE.find('#APIOptions').hide();
				
				<!--- Hide Expert System Options container - not used for this CP Type --->
				inpCPE.find('#ESOptions').hide();
				
				<!--- Hide CDF Container --->
				inpCPE.find('#CDFContainer').hide();
				
				<!--- Hise Subscriber List Container --->
				inpCPE.find('#SubscriberListContainer').hide();

				
			break;
					
			default:
										
			break;
		}

		
	}
	
	<!--- Render / Add each item to the list from Template --->
	function AddOptionToList(inpOptionText, inpOptionAval, inpOptionAvalReg, inpCPE)
	{			
		<!--- The HTML for Answer and CPEditor are stored in coldfusion variables  AnswerListItemTemplate, CPEditorTemplate ---> 
		<!--- This outputs a previously defined Coldfusion variable as a javascript variable ---> 		
		<cfoutput> 
			var #toScript(AnswerListItemTemplate, "AnswerItemjsVar")#; 
		</cfoutput> 		
		
		<!--- Create a jquery reference to the returned HTML and store it in 'inpAnswerItem' --->
		var inpAnswerItem = $(AnswerItemjsVar);
		
		var max = 0;
		inpCPE.find('#answers_list').find('.AnswerItem').each(function() {
		    max = Math.max(parseInt($(this).attr('option-id')), max);
		});
		max++;

		<!--- Set a unnique Id for each item --->		
		inpAnswerItem.attr('option-id', max )

		<!--- Set OPTION text --->			
		inpAnswerItem.find("#OPTION").val(inpOptionText);
		inpAnswerItem.find("#AVAL").val(inpOptionAval);
		inpAnswerItem.find("#AVALREG").val(inpOptionAvalReg);
				
		inpAnswerItem.find("#AVALREGLINK").click(function()
		{							
			<!--- Store a refence to this AVAL item so it can be updated by modal --->
			$('#AVALREGEditModal').data('inpAnswerItem', inpAnswerItem);
								
			$('#AVALREGEditModal #AVALREGEditAnswerText').html(inpAnswerItem.find("#OPTION").val());
			$('#AVALREGEditModal #AVALREGEdit').val(inpAnswerItem.find("#AVALREG").val());
										
			$('#AVALREGEditModal').modal('show');
			
		});	
		
		
		<!--- Apend to list --->		
		inpCPE.find('#answers_list').append(inpAnswerItem);						
	}
	
	<!--- Render / Add each item to the list from Template --->
	function AddESOptionToList(inpOptionId, inpOptionText, inpOptionReg, inpCPE)
	{			
		<!--- The HTML for Answer and CPEditor are stored in coldfusion variables  AnswerListItemTemplate, CPEditorTemplate ---> 
		<!--- This outputs a previously defined Coldfusion variable as a javascript variable ---> 		
		<cfoutput> 
			var #toScript(ExpertSystemListItemTemplate, "ESItemjsVar")#; 
		</cfoutput> 		
		
		<!--- Create a jquery reference to the returned HTML and store it in 'inpESItem' --->
		var inpESItem = $(ESItemjsVar);
		
		var max = 0;
		inpCPE.find('#es_list').find('.ESItem').each(function() {
		    max = Math.max(parseInt($(this).attr('option-id')), max);
		});
		max++;
		
		<!--- Set a unnique Id for each item --->		
		inpESItem.attr('option-id', max )
		
		<!--- Set OPTION text --->			
		inpESItem.find("#ESID").val(inpOptionId);
		inpESItem.find("#ESTEXT").val(inpOptionText);
		inpESItem.find("#ESREG").val(inpOptionReg);
		
		
		inpESItem.find("#ESREGLINK").click(function()
		{				
			<!--- Store a refence to this AVAL item so it can be updated by modal --->
			$('#ESREGEditModal').data('inpESItem', inpESItem);
					
			$('#ESREGEditModal #ESREGEdit').val(inpESItem.find("#ESREG").val());
										
			$('#ESREGEditModal').modal('show');
			
		});	
		
		
		<!--- Apend to list --->		
		inpCPE.find('#es_list').append(inpESItem);						
	}
		
	<!--- Render Branch Condition --->
	function AddContitionToList(inpConditionObj, inpCPE)
	{			
		<!--- SAMPLE XML we are trying to populate 		
			<COND BOAV='' BOC='=' BOCDV='undefined' BOQ='2' BOTNQ='0' BOV='1' CID='1' TYPE='RESPONSE'></COND>	
			<COND BOAV='Test' BOC='=' BOCDV='{%%INPPA2}' BOQ='undefined' BOTNQ='4' BOV='' CID='2' TYPE='CDF'></COND>	
		 ---> 
		 
		<!--- The HTML for Branch Condition is stored in a coldfusion variable BranchConditionItemTemplate ---> 
		<!--- This outputs a previously defined Coldfusion variable as a javascript variable ---> 
		<cfoutput> 
			var #toScript(BranchConditionItemTemplate, "jsVar")#; 
		</cfoutput> 	
		
		
		<!--- Cleanup defaults --->	
		
		
		<!--- Default Comparison is "=" --->
		if(!inpConditionObj.BOC)
		{			
			inpConditionObj.BOC = '=';
		}
		
		<!--- Default Rule Compare to next Action --->
		if(!inpConditionObj.BOTNQ)
		{			
			inpConditionObj.BOTNQ = 0;
		}
		
		<!--- Create a jquery reference to the returned HTML and store it in 'el' --->
		var el = $(jsVar);
		el.find('#CTYPE').val(inpConditionObj.TYPE);
		el.find('#BOAV').val(inpConditionObj.BOAV);
		el.find('#BOC').val(inpConditionObj.BOC);
		el.find('#BOCDV').val(inpConditionObj.BOCDV);
		el.find('#BOTNQ').val(inpConditionObj.BOTNQ);
		el.find('#BOV').val(inpConditionObj.BOV);
		el.find('#CID').val(inpConditionObj.CID);
		el.find('#CIDLabel').html("#" + inpConditionObj.CID);
						
		<!--- .val() wont work yet... Select the default selection from the attr - if any - since Questions have not been added to DOM yet, jQuery cant read SELECTION when it has not been set yet--->
		el.find('#BOQ').attr('inpVal', inpConditionObj.BOQ);
		
		<!--- .val() wont work yet... Select the default selection from the attr - if any - since Questions have not been added to DOM yet, jQuery cant read SELECTION when it has not been set yet--->
		el.find('#BOTNQ').attr('inpVal', inpConditionObj.BOTNQ);
				
		<!--- Less trips to server and DB - send array of select boxes to populate --->
		var QuestionSelectObjs = [];
					
		QuestionSelectObjs.push(el.find('#BOQ'));
		QuestionSelectObjs.push(el.find('#BOTNQ'));
				
		<!--- Load list of possible CPs to select from --->
		PopulateQuestionSelectBox(QuestionSelectObjs);	
		
		<!--- Make sure to reload answers if question is changed - if there are no answers this will be an empty select box --->
		el.find('#BOQ').change(function(){	
			onChangeQuestionSelection($(this), 0);		
		});				
		
		<!--- Load list of possible Answers to select from - BOV is set in this function after options have been loaded --->			
		PopulateQuestionAnswersSelectBox(el.find('#BOV'), inpConditionObj.BOQ, inpConditionObj.BOV );
			
		<!--- Manually Re-Initialize the answers from previously loaded question on change is not fired yet due to not added to DOM until the end of setup --->
		<!--- Also loads list of possible Answers to select from - BOV is set in this function after options have been loaded --->
		onChangeQuestionSelection(el.find('#BOQ'), inpConditionObj.BOV);	
				
		<!---  RE-intitialize any newly added SELECT2 boxes --->
		el.find(".Select2").select2( { theme: "bootstrap"} ); 
				
		
		<!--- Re setup displayed options based on selected type --->
		el.find('#CTYPE').change(function(){	
			RenderCondition($(this));
		});
										
		<!--- Have to be carefule here - there are multiple "TYPE"s in the XML - one is for Question (Q) TYPE and the other is for Branch (COND) TYPE - use CTYPE in HTML to differentiate--->
		if(inpConditionObj.TYPE == "RESPONSE" && inpConditionObj.BOAV && inpConditionObj.BOV =="")
		{			
			el.find('#CTYPE').val("RESPONSE2");
		}	
		
		<!--- Render condition based on input --->
		RenderCondition(el.find('#CTYPE'));
																
		<!--- Add this condition to the editors list of Branch Conditions --->		
		inpCPE.find('#ConditionsList').append(el);		
		
	}
	
	<!--- There is really only two types  RESPONSE and CDF, but since they can be used in multiple ways we will reconfig display based on "text" of the CTYPE --->
	function RenderCondition(inpObj)
	{						
		switch( inpObj.val())
		{			
			case 'RESPONSE' :
				<!--- Hide ignored values - note besure to hide the entire option container--->
				inpObj.parents('.ConditionItem').find('#BOAV').parent().hide();
				inpObj.parents('.ConditionItem').find('#BOCDV').parent().hide();
				
				<!--- its one of the list - not all of them - empty non-used values --->
				inpObj.parents('.ConditionItem').find('#BOCDV').val('');
				inpObj.parents('.ConditionItem').find('#BOAV').val('');
				
				<!--- Show require values --->
				inpObj.parents('.ConditionItem').find('#BOQ').parent().show();
				inpObj.parents('.ConditionItem').find('#BOC').parent().show();
				inpObj.parents('.ConditionItem').find('#BOV').parent().show();
				
				break;
				
			case 'RESPONSE2' :	
			
				<!--- Hide ignored values - note besure to hide the entire option container--->
				inpObj.parents('.ConditionItem').find('#BOCDV').parent().hide();
				inpObj.parents('.ConditionItem').find('#BOV').parent().hide();
				
				<!--- its one of the list - not all of them - empty non-used values --->
				inpObj.parents('.ConditionItem').find('#BOCDV').val('');
				inpObj.parents('.ConditionItem').find('#BOV').val('');
				
				<!--- Show require values --->
				inpObj.parents('.ConditionItem').find('#BOQ').parent().show();
				inpObj.parents('.ConditionItem').find('#BOC').parent().show();
				inpObj.parents('.ConditionItem').find('#BOAV').parent().show();					
			
				break;
				
			case 'CDF' :
								
				<!--- Hide ignored values - note besure to hide the entire option container--->
				inpObj.parents('.ConditionItem').find('#BOQ').parent().hide();
				inpObj.parents('.ConditionItem').find('#BOV').parent().hide();
				
				<!--- its one of the list - not all of them - empty non-used values --->
				inpObj.parents('.ConditionItem').find('#BOQ').val('');
				inpObj.parents('.ConditionItem').find('#BOV').val('');			
				
				<!--- Show require values --->
				inpObj.parents('.ConditionItem').find('#BOCDV').parent().show();
				inpObj.parents('.ConditionItem').find('#BOC').parent().show();
				inpObj.parents('.ConditionItem').find('#BOAV').parent().show();
																	
			break;			
			
			default:
			
				inpObj.val('RESPONSE2');
			
				<!--- Hide ignored values - note besure to hide the entire option container--->
				inpObj.parents('.ConditionItem').find('#BOCDV').parent().hide();
				inpObj.parents('.ConditionItem').find('#BOV').parent().hide();
				
				<!--- its one of the list - not all of them - empty non-used values --->
				inpObj.parents('.ConditionItem').find('#BOCDV').val('');
				inpObj.parents('.ConditionItem').find('#BOV').val('');
				
				<!--- Show require values --->
				inpObj.parents('.ConditionItem').find('#BOQ').parent().show();
				inpObj.parents('.ConditionItem').find('#BOC').parent().show();
				inpObj.parents('.ConditionItem').find('#BOAV').parent().show();		
			
			break;
		}
		
	}
	
	function getOptionListStyle(AF, id)
	{
		switch(AF) {
			case 'NUMERICPAR':
				return id + 1 + ')';
				break;
			case 'NUMERIC':
				return id + 1 + ' for';
				break;
			case 'ALPHAPAR':
				return String.fromCharCode(65+id) + ')';
				break;
			case 'ALPHA':
				return String.fromCharCode(65+id) + ' for';
				break;				
			case 'HIDDEN':
				return '';
				break;	
			default:
				return '';
		}
		
	}

	<!--- Only display Interval Options that make sense - hide the ones not used --->
	function onChangeIntervalType(inpCPE)
	{		
				
		switch(inpCPE.find('#ITYPE').val())
		{
			
			case "SECONDS":								
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Seconds(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Hide the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').hide();
				
			break;
			
			case "MINUTES":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Minute(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Hide the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').hide();
			
			break;
			
			case "HOURS":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Hour(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Hide the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').hide();			
			
			break;
			
			case "DAYS":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Day(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Show the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').show();			
			
			break;
			
			case "WEEKS":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Week(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Show the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').show();			
			
			break;
			
			case "MONTHS":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Month(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Show the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').show();			
			
			break;
			
			case "TODAY":
				<!--- Hide the IVALUE Container	--->
				inpCPE.find('#IVALUE_Container').hide();
				<!--- Show the Interval Time Container	--->	
				inpCPE.find('#IntervalTimeContainer').show();		
			
			break;
			
			case "WEEKDAY":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Weekdays to Skip? ');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Show the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').show();			
			
			break;
			
			case "DATE":
				<!--- Hide the IVALUE Container	--->
				inpCPE.find('#IVALUE_Container').hide();
				<!--- Show the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').show();		
			
			break;
			
			case "SUN":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Sunday(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Show the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').show();			
			
			break;
			
			case "MON":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Monday(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Show the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').show();			
			
			break;
			
			case "TUE":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Tuesday(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Show the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').show();			
			
			break;
			
			case "WED":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Wednesday(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Show the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').show();			
			
			break;
			
			case "THU":			
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Thursday(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Show the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').show();			
			
			break;
			
			case "FRI":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Friday(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Show the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').show();
						
			break;
			
			case "SAT":
				<!--- Tie the Intervalue Label to the selected specific Interval Type --->
				inpCPE.find('#IVALUE_Label').html('How Many Saturday(s)?');
				<!--- Show the Interval Value Container	--->
				inpCPE.find('#IVALUE_Container').show();
				<!--- Show the Interval Time Container	--->
				inpCPE.find('#IntervalTimeContainer').show();
				
			break;					
			
		}
	}	
	
	<!--- Becuase these buttons can be dynamically added - set click method here each time they are added --->
	<!--- BOQ should only load real questions with answers - everthing else should load all actions/CPs --->
	function PopulateQuestionSelectBox(inpObjs)
	{			
		if(QuestionListBox != null)
		{
			$.each(inpObjs , function(i, val) 	
			{ 													
				<!--- Remove current Questions from Box --->
				inpObjs[i].empty();	
				
				
				<!--- Set default message for Question selecter vs default Action director --->
				if(inpObjs[i].attr('id') == "BOQ")
				{
					<!--- Only hit DOM once to add all of the options --->	
					inpObjs[i].html(QuestionsOnlyListBox.join(''));	
					
					<!--- Now set the selection here - Use .attr("inpVal") instead of .val(). It wont work previously... Select the default selection from the attr - if any - since Questions have not been added to DOM yet, jQuery cant read SELECTION when it has not been set yet--->
					inpObjs[i].val(inpObjs[i].attr("inpVal"));		
				
					inpObjs[i].find('option[value="0"]').text("Please Select an Existing Action")
				}
				else
				{
					<!--- Only hit DOM once to add all of the options --->	
					inpObjs[i].html(QuestionListBox.join(''));	
					
					<!--- Now set the selection here - Use .attr("inpVal") instead of .val(). It wont work previously... Select the default selection from the attr - if any - since Questions have not been added to DOM yet, jQuery cant read SELECTION when it has not been set yet--->
					inpObjs[i].val(inpObjs[i].attr("inpVal"));	
				}
			});	
		}
		else
		{					
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/control-point.cfc?method=ReadXMLQuestions&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				async: false,
				data:  
				{ 
					INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>",
					inpTemplateFlag : "<cfoutput>#inpTemplateFlag#</cfoutput>"
				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},					  
				success:		
				<!--- Default return function for Async call back --->
				function(d) 
				{																						
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1) 
					{											
						<!--- Store Questions for Select box here --->
						var output = [];
						var outputQuestionsOnly = [];
							   
						$.each(d.QUESTIONS , function(i, val) 
						{ 														
						  	<!--- HTML Encode Text for Display - needs to be 'quote safe' --->
						  	<!--- Use the Physical - non-changing ID for each reference --->
						  	
						  	<!--- Only show questions with answers non-questions --->
						  	if(d.QUESTIONS[i].TYPE == "ONESELECTION" || d.QUESTIONS[i].TYPE == "SHORTANSWER" || d.QUESTIONS[i].TYPE == "API") 
						  		outputQuestionsOnly.push('<option value="'+ d.QUESTIONS[i].ID +'">'+ d.QUESTIONS[i].TEXT +'</option>');	  
						});
						
						
						$.each(d.QUESTIONS , function(i, val) 
						{ 														
						  	<!--- HTML Encode Text for Display - needs to be 'quote safe' --->
						  	<!--- Use the Physical - non-changing ID for each reference --->
						  	
						  	if(d.QUESTIONS[i].TEXT == '')
								output.push('<option value="'+ d.QUESTIONS[i].ID +'">'+ d.QUESTIONS[i].TDESC +'</option>');	  	
						  	else
							  	output.push('<option value="'+ d.QUESTIONS[i].ID +'">'+ d.QUESTIONS[i].TEXT +'</option>');	  
						});
						
						<!--- Store here to minimize trips to DB  --->
						QuestionListBox = output;
						QuestionsOnlyListBox = outputQuestionsOnly;
												
						$.each(inpObjs , function(i, val) 	
						{ 													
							<!--- Remove current Questions from Box --->
							inpObjs[i].empty();	
							
							
							<!--- Set default message for Question selecter vs default Action director --->
							if(inpObjs[i].attr('id') == "BOQ")
							{
								<!--- Only hit DOM once to add all of the options --->	
								inpObjs[i].html(outputQuestionsOnly.join(''));	
								
								<!--- Now set the selection here - Use .attr("inpVal") instead of .val(). It wont work previously... Select the default selection from the attr - if any - since Questions have not been added to DOM yet, jQuery cant read SELECTION when it has not been set yet--->
								inpObjs[i].val(inpObjs[i].attr("inpVal"));						
							
								inpObjs[i].find('option[value="0"]').text("Please Select an Existing Action")
							}
							else
							{							
								<!--- Only hit DOM once to add all of the options --->	
								inpObjs[i].html(output.join(''));	
								
								<!--- Now set the selection here - Use .attr("inpVal") instead of .val(). It wont work previously... Select the default selection from the attr - if any - since Questions have not been added to DOM yet, jQuery cant read SELECTION when it has not been set yet--->
								inpObjs[i].val(inpObjs[i].attr("inpVal"));									
							}																	
						});					
					
					}
					else
					{
						<!--- No result returned --->		
						bootbox.alert("General Error processing your request.");
					}			
									
				} 		
					
			});
		}	
	}

	<!--- Only display answers Compared to single selection question --->
	function onChangeQuestionSelection(inpObj, inpBOV)
	{
		<!--- Read parant ConditionItem Obj for BOQ and BOV  needed--->
		if(inpObj.parents('.ConditionItem').find('#BOQ').val() > 0)
		{
			PopulateQuestionAnswersSelectBox(inpObj.parents('.ConditionItem').find('#BOV'), inpObj.parents('.ConditionItem').find('#BOQ').val(), inpBOV);
		}
	}

	<!--- Becuase these items can be dynamically added - set click method here each time they are added --->
	function PopulateQuestionAnswersSelectBox(inpObj, inpPID, inpBOV)
	{	
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/control-point.cfc?method=ReadXMLQuestionAnswers&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: false,
			data:  
			{ 
				INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>",
				inpQID : inpPID,
				inpIDKey : "ID",
				inpTemplateFlag : "<cfoutput>#inpTemplateFlag#</cfoutput>"
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},					  
			success:		
			<!--- Default return function for Async call back --->
			function(d) 
			{																						
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1) 
				{																										
					<!--- Remove current Questions from Box --->															
					inpObj.empty();
					
					<!--- Store Questions for Select box here --->
					var output = [];
   
					$.each(d.CPOBJ.OPTIONS , function(i, val) 
					{ 
					  <!--- HTML Encode Text for Display - needs to be 'quote safe' --->
					  <!--- Use the Physical - non-changing ID for each reference --->
					  output.push('<option value="'+ d.CPOBJ.OPTIONS[i].ID +'">'+ d.CPOBJ.OPTIONS[i].TEXT +'</option>');
					
					});

					<!--- Only hit DOM once to add all of the options --->
					inpObj.html(output.join(''));
					
					<!--- Be sure to select store value if there is one --->
					inpObj.val(inpBOV);
				
				}
				else
				{
					<!--- No result returned --->		
					bootbox.alert("General Error processing your request.");
				}			
								
			} 		
				
		});

	}
	
	<!--- Becuase these items can be dynamically added - set here each time they are added inpObj here is the SELECT box --->
	function PopulateSubscriberListSelectBox(inpObj, inpOIG)
	{					
		if(GroupsListBox != null)
		{																
			<!--- Remove current Questions from Box --->
			inpObj.empty();	
			
			<!--- Only hit DOM once to add all of the options --->	
			inpObj.html(GroupsListBox.join(''));	
			
			<!--- Now set the selection here --->
			inpObj.val(inpOIG);		
		}
		else
		{		
			
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/control-point.cfc?method=GetGroups&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				async: true,
				data:  
				{ 
					
				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},					  
				success:		
				<!--- Default return function for Async call back --->
				function(d) 
				{																											
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1) 
					{																										
						<!--- Remove current Questions from Box --->															
						inpObj.empty();
						
						<!--- Store Questions for Select box here --->
						var output = [];
											
						<!--- Default 0 --->
						output.push('<option value="0">Select Subscriber List</option>');
											
						$.each($(d.GROUPS.DATA.GroupId_bi), function(i, val) 
						{ 
						  <!--- HTML Encode Text for Display - needs to be 'quote safe' --->
						  <!--- Use the Physical - non-changing ID for each reference --->						  
						  if(parseInt(inpOIG) == parseInt(d.GROUPS.DATA.GroupId_bi[i]))
						  	output.push('<option value="'+ d.GROUPS.DATA.GroupId_bi[i] +'" selected>'+ d.GROUPS.DATA.GroupName_vch[i] +'</option>');
						  else
						  	output.push('<option value="'+ d.GROUPS.DATA.GroupId_bi[i] +'">'+ d.GROUPS.DATA.GroupName_vch[i] +'</option>');	
						
						});
						
						<!--- Store here to minimize trips to DB  --->
						GroupsListBox = output;
	
						<!--- Only hit DOM once to add all of the options --->
						inpObj.html(output.join(''));
						
						<!--- Be sure to select stored value if there is one --->
						inpObj.val(parseInt(inpOIG));
					
					}
					else
					{
						<!--- No result returned --->		
						bootbox.alert("General Error processing your request.");
					}			
									
				} 		
					
			});
			
		}

	}



	<!--- Becuase these items can be dynamically added - set here each time they are added inpObj here is the SELECT box --->
	function PopulateCDFSelectBox(inpObj, inpCDF)
	{					
		if(CDFListBox != null)
		{																
			<!--- Remove current Questions from Box --->
			inpObj.empty();	
			
			<!--- Only hit DOM once to add all of the options --->	
			inpObj.html(CDFListBox.join(''));	
			
			<!--- Now set the selection here --->
			inpObj.val(inpCDF);		
		}
		else
		{		
			
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/control-point.cfc?method=GetCDFs&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				async: false,
				data:  
				{ 
					
				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");},					  
				success:		
				<!--- Default return function for Async call back --->
				function(d) 
				{																											
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1) 
					{																										
						<!--- Remove current Questions from Box --->															
						inpObj.empty();
						
						<!--- Store Questions for Select box here --->
						var output = [];
											
						<!--- Default 0 --->
						output.push('<option value="0">Select CDF To Use</option>');
											
						$.each($(d.CDFS.DATA.CDFId_int), function(i, val) 
						{ 
						  <!--- HTML Encode Text for Display - needs to be 'quote safe' --->
						  <!--- Use the Physical - non-changing ID for each reference --->
						  output.push('<option value="'+ d.CDFS.DATA.CDFName_vch[i] +'">'+ d.CDFS.DATA.CDFName_vch[i] +'</option>');
						
						});
						
						<!--- Store here to minimize trips to DB  --->
						CDFListBox = output;
	
						<!--- Only hit DOM once to add all of the options --->
						inpObj.html(output.join(''));
						
						<!--- Be sure to select stored value if there is one --->
						inpObj.val(inpCDF);
					
					}
					else
					{
						<!--- No result returned --->		
						bootbox.alert("General Error processing your request.");
					}			
									
				} 		
					
			});
			
		}

	}

	<!--- After each character is typed run keyword validations --->
	function ValidateKeyword(inpKeyword)
	{
		<!--- Valid char --->
		
		<!--- Length --->
		
		<!--- Validate user does not have too many keywords --->
		
		<!--- Available --->
						
	}
	
	<!--- Validate Contorl Point Actions - make sure they are minimally completed and saved --->
	function ValidateCPActionsCompleted()
	{		
		var LocalReturnValue = true;
		
		<!--- Loop over each CP --->
		$.each($('.control-point'), function(index, value) {
		   	
			
			<!--- Check to see if any editors are open - let the user finsih saving chages first --->
			<!--- This looks to see if the current control point has a CP type changer option - closed editors wont have this --->
			if( $(this).find('#ChangeCPType').length )
			{
				
				<!--- Scroll to this position --->
				$("body").scrollTop($(this).offset().top - 100);
				
				bootbox.alert('You still have an editor open: <b>' + $(this).find('.control-point-label').html() + '</b>. Be sure to save your changes before continuing.');
											
				<!--- Set the return value for the main validation function --->
				LocalReturnValue = false;
				
				<!--- Returning false here just breaks out of the jQuery each loop --->
				return false;				
			}
			
			<!--- Check if this is a OPTIN type --->
			if($(this).attr('data-control-point-type') == 'OPTIN')
			{
				<!--- Check if Subscriber List has been set --->
				if(parseInt( $(this).find('.control-point-optin').attr('data-control-point-OIG')) == 0)
				{
					
					<!--- Scroll to this position --->
					$("body").scrollTop($(this).offset().top - 100);
					
					bootbox.alert('<b>Subscriber List</b> has not yet been set on action item <b>' + $(this).find('.control-point-label').html() + '</b>. You will need a <b>Subscriber List</b> to be able captured and store for future access your Opt In requests.');
				
					<!--- Open this editor --->						
					$(this).find('.control-point-btn-edit').click();
					
					<!--- Set the return value for the main validation function --->
					LocalReturnValue = false;
					
					<!--- Returning false here just breaks out of the jQuery each loop --->
					return false;
				}
				
			}			
			
		});	
		
		return LocalReturnValue;		
	}
	
	
	<!--- Check for blast stats asyncronously --->
	function CheckForPreviousBlasts()
	{				
		$('#ProcessingBlastStats').show();
				
		$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/control-point.cfc?method=GetPreviousBlasts&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				async: true,
				data:  
				{ 
					inpBatchId : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>"
				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {<!--- SQUASH ERRORS No result returned  bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); ---> },					  
				success:		
				<!--- Default return function for Async call back --->
				function(d) 
				{			
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1) 
					{																										
						<!--- Remove current Questions from Box --->															
						
						
						$('#BlastQueue').html('Total Messages Still In Blast Queue: ' + parseInt(d.QUEUED));
						$('#BlastComplete').html('Total Blast Messages Completed: ' + parseInt(d.COMPLETED));
						
						
						$('#ProcessingBlastStats').hide();
										
					}
					else
					{
						<!--- No result returned - SQUASH ERRORS--->		
						<!--- bootbox.alert("General Error processing your request."); --->
					}			
									
				} 		
					
			});		
	}
	
	
	
	function UpdateChartSWTCompletion(inpStep, inpTotalSteps)
	{
		
		// myChartSWTCompletion
		var ctx = $("#myChartSWTCompletion");
	
		var myChartKeywords = new Chart(ctx, {
		    type: 'doughnut',
		    animation:{
				animateRotate:true
    		},
    		data: {
			   
		        labels: ["Step","Total"],						        
		        datasets: [{
		            data: [inpStep, (inpTotalSteps - inpStep)],  
		            backgroundColor: ['#D46A6A','#ccdddd' ], 
		            borderColor: [
		                'rgba(255,255,255,1)',
		                'rgba(255,255,255,1)',
		                'rgba(255,255,255,1)',
		                'rgba(255,255,255,1)',
		                'rgba(255,255,255,1)',
		                'rgba(255,255,255,1)'						              
		            ],
		            borderWidth: 3
		        }]
		    },
		    options: {
			   		responsive: true,
			   		maintainAspectRatio: false,
			   		cutoutPercentage: 80,
			   		legend: {
						display: false,
					},
					tooltips: {
						enabled: false,
					},
					<!--- Legend callback string --->
					legendCallback: function(chart) {

						var text = [];
		                text.push('<ul>');
		                for (var i=0; i<chart.data.datasets[0].data.length; i++) {
		                    text.push('<li>');
		                    text.push('<span style="background-color:' + chart.data.datasets[0].backgroundColor[i] + '">' + chart.data.datasets[0].data[i] + '</span>');
		                    if (chart.data.labels[i]) {
		                        text.push(chart.data.labels[i]);
		                    }
		                    text.push('</li>');
		                }
		                text.push('</ul>');
		                return text.join("");
					   
					}	

				}
		        
		        
			});			
		
		<!--- Special chart.js method to generate legend where you want it. Style is with your own CSS to ctrol flow. --->
<!--- 		$('#Chart-Legend-Completion').html("<div style='text-align: center;'><h3 class='SlimTop'>" + Math.round((inpStep/inpTotalSteps)*100 ) + '%' + "</h3><h5 class='SlimTop'>Complete</h5></div>"  ); --->
		$('#Chart-Legend-Completion').html("<div style='text-align: center;'><h5 class='SlimTop'>Step</h5><h3 class='SlimTop'>" + inpStep + ' of ' + inpTotalSteps + "</h3></div>"  );
		$('#Chart-Legend-Completion').fadeIn(400);
						
		
	}
	
	
	<!--- Write Batch Desc to DB - write syncronously to keep steps in order --->
	function SaveCampaignName(inpEditorFlag)
	{			
		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->				
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/control-point.cfc?method=UpdateBatchDesc&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: false,
			data:  
			{ 
				INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>", 
				inpDesc : $('#BatchDesc').val(),
				inpTemplateFlag : "<cfoutput>#inpTemplateFlag#</cfoutput>"	
				
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
			success:		
			<!--- Default return function for call back --->
			function(d) 
			{																																		
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1) 
				{								
					$('#KeywordStatus').html('');
					
					<!--- If advance flow editor - hide keyword editor when complete --->
					if(inpEditorFlag)
					{	
						$('#BatchDescDisplayValue').html($("#BatchDesc").val());									
						$('#BatchDescDisplaySection').show();
						$('#BatchDescDefineSection').hide();
					}
								
				}
				else
				{
					<!--- No result returned --->	
					if(d.ERRMESSAGE != "")	
						bootbox.alert(d.ERRMESSAGE);
				}												
			} 		
				
		});				
													
	}
	
<!---
	<!--- Write Batch Desc to DB - write syncronously to keep steps in order --->
	function TestAutoAdd()
	{			
		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->				
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/control-point.cfc?method=CreateAndLinkSubscriberList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: false,
			data:  
			{ 
				INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>"				
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
			success:		
			<!--- Default return function for call back --->
			function(d) 
			{																																		
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1) 
				{								
					
				}
				else
				{
					<!--- No result returned --->	
					if(d.ERRMESSAGE != "")	
						bootbox.alert(d.ERRMESSAGE);
				}												
			} 		
				
		});				
													
	}
	
--->
	<!--- Write keyword to DB --->
	function SaveKeyword(inpEditorFlag)
	{	
		<!--- Similar but different style editor for keyword for SWT --->
		<cfif swt EQ 1 >
			var ExistingKeyword =  $('#Keyword').val();  			
		<cfelse>
			var ExistingKeyword = $('#KeywordDisplayValue').html();						
		</cfif>							
		
		if($("#Keyword").val().length == 0 && ExistingKeyword.length > 0)
		{				
			bootbox.confirm('Are you sure you want to remove the reserved keyword?', function(r) 
			{
				<!--- Only complete if user comfirms it --->
				if (r == true) 
				{
					<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->						
					$.ajax({
						type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
						url: '/session/sire/models/cfc/control-point.cfc?method=ClearKeyword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
						dataType: 'json',
						async: false,
						data:  
						{ 
							INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>", 
							inpDesc : $('#BatchDesc').val(),
							inpKeyword : $("#Keyword").val(),
							inpShortCode : $("#ShortCode").val(),
							inpTemplateFlag : "<cfoutput>#inpTemplateFlag#</cfoutput>"	
							
						},					  
						error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
						success:		
						<!--- Default return function for call back --->
						function(d) 
						{																																		
							<!--- RXRESULTCODE is 1 if everything is OK --->
							if (d.RXRESULTCODE == 1) 
							{										
								$('#KeywordStatus').html('');
								
								<!--- If advance flow editor - hide keyword editor when complete --->
								if(inpEditorFlag)
								{																			
									$('#KeywordDisplayValue').html($("#Keyword").val());		
									
									if($("#KeywordDisplayValue").html().length > 0)
										$('#StartKeywordStatus').hide();
									else
										$('#StartKeywordStatus').show();	
											
									$('#KeywordDisplaySection').show();
									$('#KeywordDefineSection').hide();
								}
							}
							else
							{
								<!--- No result returned --->	
								if(d.ERRMESSAGE != "")	
									bootbox.alert(d.ERRMESSAGE);
							}												
						} 		
							
					});
					
				}	
				
				<!--- Return true to close bootbox when complete --->
				return true;
			});					
		}
		else
		{			
			if($("#Keyword").val().length > 0 )
			{			
				<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->				
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url: '/session/sire/models/cfc/control-point.cfc?method=SaveKeyword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					async: false,
					data:  
					{ 
						INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>", 
						inpDesc : $('#BatchDesc').val(),
						inpKeyword : $("#Keyword").val(),
						inpShortCode : $("#ShortCode").val(),
						inpTemplateFlag : "<cfoutput>#inpTemplateFlag#</cfoutput>"	
						
					},					  
					error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
					success:		
					<!--- Default return function for call back --->
					function(d) 
					{																																		
						<!--- RXRESULTCODE is 1 if everything is OK --->
						if (d.RXRESULTCODE == 1) 
						{								
							$('#KeywordStatus').html('');
							
							<!--- If advance flow editor - hide keyword editor when complete --->
							if(inpEditorFlag)
							{	
								$('#KeywordDisplayValue').html($("#Keyword").val());
								
								if($("#KeywordDisplayValue").html().length > 0)
									$('#StartKeywordStatus').hide();
								else
									$('#StartKeywordStatus').show();
												
								$('#KeywordDisplaySection').show();
								$('#KeywordDefineSection').hide();
							}
						}
						else
						{
							<!--- No result returned --->	
							if(d.ERRMESSAGE != "")	
								bootbox.alert(d.ERRMESSAGE);
						}												
					} 		
						
				});				
			}
		}																	
	}
	
	<!--- Auto create subscriber list if none is selected --->			
	function AutoCreateSubscriberList()
	{
		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->				
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/control-point.cfc?method=CreateAndLinkSubscriberList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: false,
			data:  
			{ 
				INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>"				
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
			success:		
			<!--- Default return function for call back --->
			function(d) 
			{																																		
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1) 
				{	
					<!--- !important - Force reload of list box on next display call --->
					GroupsListBox = null;
					
					<!--- Append new list to existing OIG - reload causes it to not be visible to CP_Save mehtod --->
					$('#WalkThroughStepEditor').find('#CPEBody').find('#OIG').append($('<option/>', { 
				        value: d.NEWLISTID,
				        text : d.NEWName 
				    }));
				    
					<!--- Add the native list item --->
				    $('#WalkThroughStepEditor').find('#CPEBody').find('#OIG').append($('<option>', {value: d.NEWLISTID, text: d.NEWNAME}));
				    			
				    <!--- Add the data to the select2 object --->				    
				    $('#WalkThroughStepEditor').find('#CPEBody').find('#OIG').select2('data', {id: d.NEWLISTID, text: d.NEWNAME});   
				    
				    <!--- Select the new list by default --->				    				    										
					$('#WalkThroughStepEditor').find('#CPEBody').find('#OIG').val(parseInt(d.NEWLISTID));
					    
				}
				else
				{
					<!--- No result returned --->	
					if(d.ERRMESSAGE != "")	
						bootbox.alert(d.ERRMESSAGE);
				}												
			} 		
				
		});			
		
	}

	$('#lbl-show-filter-bar').click(function(e){
		$('#subcriber-filter-bar').toggleClass('hidden');
		if($(this).hasClass('showed')){
			$(this).text('hide filter');
			$(this).removeClass('showed');
		} else {
			$(this).addClass('showed');
			$(this).text('show filter');
		}
	});


	$('a.apply-filter-btn').click(function(e){
		var contactString  = $('#inpContactStringFilter').val(),
		    subCrbl = $('#SubscriberList[data-type="old-blast"]').val(),
		    CFDInclude = true;
		var filterRows = $('#filter_rows > div.filter_row');
		    if(validateFilter(filterRows)){

		    	InitControl(subCrbl, contactString, filterRows);
		    }
	});


	$('body').on('change', '#inpContactStringFilter', function(e){
		
		var contactString  = $(this).val(),
		    subCrbl = $('#SubscriberList[data-type="old-blast"]').val();
		    ClearFilter_();
		    InitControl(subCrbl, contactString);

	});

	$('a.clear-filter-btn').click(function(event){
		var subCrbl = $('#SubscriberList[data-type="old-blast"]').val();
	  	$('#inpContactStringFilter').val("");
	    ClearFilter_();
		InitControl(subCrbl);

	});


	$('#SubscriberList[data-type="old-blast"]').change(function(e){
		
		subCrbl = $(this).val();
		InitControl(subCrbl);
		$('#inpContactStringFilter').val("");
		
		if ($(this).val() > 0) 
		{
			$('#btn-save-send').prop('disabled', false);
			$('div.box-subcriber-filter').removeClass('hidden');
		}
		else
		{
			$('#btn-save-send').prop('disabled', true);
			$('div.box-subcriber-filter').addClass('hidden');
		}
	});


	function BlastNow()
	{
		
		$(".btn-save-send").prop("disabled",true);	
			
			<!--- Check for basic validation of Control Point data before continuing - give user warning and allow them to fix --->
			if(!ValidateCPActionsCompleted())
			{
				$(".btn-save-send").prop("disabled",false);
				return;
				
			}
				
			
			<!--- Save Custom Responses - validation on server side - will return error message if not valid --->				
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '/session/sire/models/cfc/control-point.cfc?method=GetBlastToListStats&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				async: true,
				data:  
				{ 
					inpBatchId : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>", 
					inpGroupId : $('#SubscriberList').val(),
					inpShortCode : $("#ShortCode").val(),
					inpContactFilter: $("#inpContactStringFilter").val(),
					inpCdffilter: $('input.data-cdf-filter').data('contactStringCdfFilter')					
					
				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); $(".btn-save-send").prop("disabled",false);	},					  
				success:		
				<!--- Default return function for Async call back --->
				function(d) 
				{																																		
					<!--- RXRESULTCODE is 1 if everything is OK --->
					if (d.RXRESULTCODE == 1) 
					{	
						$('#confirm-number-subscribers').text(d.LISTCOUNT);
						$('#confirm-credits-in-queue').text(d.QUEUECOUNT);
						$('#confirm-credits-available').text(d.BALANCE);
						
						$('#confirm-list-duplicates').text(d.LISTCOUNTDUPLICATE);
						
						var TotalElligable = parseInt(d.LISTCOUNT) - parseInt(d.LISTCOUNTDUPLICATE);
							TotalElligable = TotalElligable > 0 ? TotalElligable : 0;
						$('#confirm-total-elligable').text(TotalElligable);
						$('#confirm-credits-needed').text(TotalElligable);
						
						/* Check if this batch and subscriber list already in queue together */

						if (parseInt(d.HASACTIVEQUEUE) > 0) {
							bootbox.alert('This campaign already has processed a blast with this subscribers list.<br>Please choose another list.');
						} else {
							<!--- Disable send button and warn if credits not enough --->
							if (parseInt(d.BALANCENUMBER) >= parseInt(d.LISTCOUNT)) 
							{
								$('#btn-confirm-save-and-send').prop('disabled', false);
								$('#confirm-launch-alert-text').hide();
							}
							else
							{
								$('#btn-confirm-save-and-send').prop('disabled', true);
								$('#confirm-launch-alert-text').show();
							}
								
							
							$('#ConfirmLaunchModal').modal('show');
						}
						
						<!--- Re-enable button to send for next time --->
						$(".btn-save-send").prop("disabled",false);	
																									
					}
					else
					{
						<!--- No result returned --->	
						if(d.ERRMESSAGE != "")
						{	
							bootbox.alert(d.ERRMESSAGE);
							$(".btn-save-send").prop("disabled",false);		
						}
						
							
					}												
				} 		
					
			});
		
		
		
	}	
	
	function FinalFinishSWT()
	{
		<!--- Remove any existing content --->
		$('#WalkThroughStepEditor').empty();
		
		<!--- Set the current RQ --->
		$('#WalkThroughStepEditor').attr("data-control-point-rq", 0)	
						
		$('#SWTDescSection').hide();
		$('#SWTKeywordSection').hide();
		$('#SWTPreviewSection').hide();	
		$('#SWTMLPSection').hide();	
		$('#SWTLocation').hide();
		$('.SWTNavStuff').hide();				
		
		<!--- location.href = '/session/sire/pages/campaign-edit?campaignid=<cfoutput>#campaignData.BatchId_bi#</cfoutput>'; --->
		location.href = '/session/sire/pages/campaign-manage' 
				
	}

	<!--- Auto create subscriber list if none is selected --->			
	function UpdateMLPWithKeywordAndShortCode()
	{
		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->				
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/models/cfc/control-point.cfc?method=UpdateMLPWithKeywordAndShortCode&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: false,
			data:  
			{ 
				INPBATCHID : "<cfoutput>#campaignData.BatchId_bi#</cfoutput>", 
				inpShortCode : $("#ShortCode").val(),
				inpKeyword : $("#Keyword").val()
								
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
			success:		
			<!--- Default return function for call back --->
			function(d) 
			{	
				<!--- Refresh iFram on content changed --->	
				var _theframe = document.getElementById("MLPIFrame");
				_theframe.contentWindow.location.href = _theframe.src;															
			} 		
				
		});			
		
	}

		
</script>


