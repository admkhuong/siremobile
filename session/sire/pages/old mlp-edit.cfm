<cfparam name="inpCPPID" default="">
<cfparam name="ccpxDataId" default="0">
<cfparam name="ccpxDataIdSource" default="0">
<cfparam name="templateid" default="0">

<cfparam name="action" default="">
<cfparam name="ModalMode" default="0">


<!--- Change how this page displays based on stand alone or as Modal --->
<cfif ModalMode EQ 0>		
	<cfset MLPMainSection = "col-md-8 col-md-offset-2" />
<cfelse>
	<cfset MLPMainSection = "col-md-12" />
</cfif>	
<cfif action EQ "template">
	
	<cfif ccpxDataId EQ 0>
		<!--- Create new MLP Template and refresh page with new Template Id--->
				
		<!--- Check if this new MLP Template from existing MLP Source --->
		<cfif ccpxDataIdSource GT 0 >		
		
			<cfinvoke component="session.sire.models.cfc.mlp" method="ReadCPP" returnvariable="RetCPPXData">
				<cfinvokeargument name="ccpxDataId" value="#ccpxDataIdSource#">
			</cfinvoke> 
									
			<cfinvoke component="session.sire.models.cfc.mlp" method="AddMLPTemplate" returnvariable="RetAddTemplate">
				<cfinvokeargument name="ccpxDataId" value="#ccpxDataId#">
				<cfinvokeargument name="inpRawContent" value="#RetCPPXData.RAWCONTENT#">
				<cfinvokeargument name="inpDesc" value="New Template based on #RetCPPXData.CPPNAME#">
			</cfinvoke> 		
		
		<cfelse>
		
			<!--- Create blank template --->
			<cfinvoke component="session.sire.models.cfc.mlp" method="AddMLPTemplate" returnvariable="RetAddTemplate">
				 <cfinvokeargument name="ccpxDataId" value="#ccpxDataId#">
			</cfinvoke> 
		
		</cfif>
		
		<!--- Reload page as an editor now --->				
		<cfif RetAddTemplate.RXRESULTCODE EQ 1>
			<cflocation url="mlp-edit?action=template&ccpxDataId=#RetAddTemplate.CPPID#" addToken="no" />
		</cfif>
		
	</cfif>	
	
	<!--- Read exisitng template data --->
	<cfinvoke component="session.sire.models.cfc.mlp" method="ReadMLPTemplate" returnvariable="RetCPPXData">
		 <cfinvokeargument name="ccpxDataId" value="#ccpxDataId#">
	</cfinvoke> 


<cfelseif action EQ "add">
	<!--- Check if this new MLP Template from existing MLP Source --->
	<cfif ccpxDataIdSource GT 0 >		
	
		<!--- Read exisitng template data --->
		<cfinvoke component="session.sire.models.cfc.mlp" method="ReadMLPTemplate" returnvariable="RetCPPXData">
			 <cfinvokeargument name="ccpxDataId" value="#ccpxDataIdSource#">
		</cfinvoke> 
		
		<!--- Read Profile data and put into form --->
        <cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="RetVarGetUserOrganization"></cfinvoke>
      
        <!---Create a data set to run against dynamic data --->
        <cfset inpFormData = StructNew() />
        
        <cfset inpFormData.inpBrand = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONBUSINESSNAME_VCH />
        <cfset inpFormData.inpBrandFull = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH />
        <cfset inpFormData.inpBrandWebURL = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITE_VCH />
        <cfset inpFormData.inpBrandWebTinyURL = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITETINY_VCH/>
        <cfset inpFormData.inpBrandPhone = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONPHONE_VCH />
        <cfset inpFormData.inpBrandeMail = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONEMAIL_VCH />
        
        <cfif inpFormData.inpBrand EQ ""><cfset inpFormData.inpBrand = "inpbrand"></cfif>
        <cfif inpFormData.inpBrandFull EQ ""><cfset inpFormData.inpBrandFull = "inpBrandFull"></cfif>
        <cfif inpFormData.inpBrandWebURL EQ ""><cfset inpFormData.inpBrandWebURL = "inpBrandWebURL"></cfif>
        <cfif inpFormData.inpBrandWebTinyURL EQ ""><cfset inpFormData.inpBrandWebTinyURL = "inpBrandWebURLShort"></cfif>
        <cfif inpFormData.inpBrandPhone EQ ""><cfset inpFormData.inpBrandPhone = "inpBrandPhone"></cfif>
        <cfif inpFormData.inpBrandeMail EQ ""><cfset inpFormData.inpBrandeMail = "inpBrandeMail"></cfif>
                    
        <cfset inpFormData.CompanyNameShort = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONBUSINESSNAME_VCH />
        <cfset inpFormData.CompanyName = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONNAME_VCH />
        <cfset inpFormData.CompanyWebSite = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITE_VCH />
        <cfset inpFormData.CompanyWebSiteShort = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONWEBSITETINY_VCH/>
        <cfset inpFormData.CompanyPhoneNumber = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONPHONE_VCH />
        <cfset inpFormData.CompanyEMailAddress = RetVarGetUserOrganization.ORGINFO.ORGANIZATIONEMAIL_VCH />

		
		<!--- Do the MLP transforms here --->  
        <cfinvoke component="session.cfc.csc.csc" method="DoDynamicTransformations" returnvariable="RetVarDoDynamicTransformations">
            <cfinvokeargument name="inpResponse_vch" value="#RetCPPXData.RAWCONTENT#"> 
            <cfinvokeargument name="inpContactString" value="">
            <cfinvokeargument name="inpRequesterId_int" value=""> 
            <cfinvokeargument name="inpFormData" value="#inpFormData#"> 
            <cfinvokeargument name="inpMasterRXCallDetailId" value="0">
            <cfinvokeargument name="inpShortCode" value="">
            <cfinvokeargument name="inpBatchId" value="">
            <cfinvokeargument name="inpBrandingOnly" value="1">
        </cfinvoke>   
        
        <!--- Update the current template data --->           
		<cfset RetCPPXData.RAWCONTENT = RetVarDoDynamicTransformations.RESPONSE />  
								
		<cfinvoke component="session.sire.models.cfc.mlp" method="AddMLP" returnvariable="RetAddMLP">
			<cfinvokeargument name="inpRawContent" value="#RetCPPXData.RAWCONTENT#">
			<cfinvokeargument name="inpDesc" value="New MLP based on #RetCPPXData.CPPNAME#">
			<cfinvokeargument name="inpCustomCSS" value="#RetCPPXData.CUSTOMCSS#">
		</cfinvoke> 		
			
	<cfelse>
	
		<cfinvoke component="session.sire.models.cfc.mlp" method="AddMLP" returnvariable="RetAddMLP">
			<cfinvokeargument name="inpRawContent" value="">
			<cfinvokeargument name="inpDesc" value="New Empty MLP">
		</cfinvoke> 		
	
	</cfif>
			
	<!--- Reload page as an editor now --->				
	<cfif RetAddMLP.RXRESULTCODE EQ 1>
		<cflocation url="mlp-edit?ccpxDataId=#RetAddMLP.CPPID#" addToken="no" />
	<cfelse>
		<cflocation url="mlp-list" addToken="no" />
	</cfif>

<cfelse>

	<!--- Read exisitng MLP data --->
	<cfinvoke component="session.sire.models.cfc.mlp" method="ReadCPP" returnvariable="RetCPPXData">
		 <cfinvokeargument name="ccpxDataId" value="#ccpxDataId#">
	</cfinvoke> 

</cfif>

<cfparam name="variables._title" default="Marketing Landing Portal Editor - Sire">

<cfif RetCPPXData.CUSTOMCSS NEQ "">

		<cfset CUSTOMCSS = deSerializeJSON(RetCPPXData.CUSTOMCSS) />
		
		
		<cfif !StructKeyExists(CUSTOMCSS, "backgroundcolor") >
			<cfset CUSTOMCSS.backgroundcolor = "##FFFFFF"/>
		</cfif>	
	
		<cfif !StructKeyExists(CUSTOMCSS, "fontfamily") >
			<cfset CUSTOMCSS.fontfamily = "inherit" />
		</cfif>	
		
		<cfif !StructKeyExists(CUSTOMCSS, "borderwidth") >
			<cfset CUSTOMCSS.borderwidth = "0px"/>
		</cfif>	
		
		<cfif !StructKeyExists(CUSTOMCSS, "bordercolor") >
			<cfset CUSTOMCSS.bordercolor = "##FF0000"/>
		</cfif>	
		
		<cfif !StructKeyExists(CUSTOMCSS, "borderstyle") >
			<cfset CUSTOMCSS.borderstyle = "none"/>
		</cfif>	
				
<cfelse>
	
	<cfset CUSTOMCSS = structNew() />
	
	<cfset CUSTOMCSS.backgroundcolor = "##FFFFFF"/>
	<cfset CUSTOMCSS.fontfamily = "inherit" />	
	<cfset CUSTOMCSS.borderwidth = "0"/>
	<cfset CUSTOMCSS.bordercolor = "##FFFFFF"/>
	<cfset CUSTOMCSS.borderstyle = "none"/>				    
	
</cfif>

<style>

	.color_input, .cssinput {
	    border: 0px !important;
	    padding: 0px !important;
	    margin-bottom: 0px !important;
	    width: 108px	!important;
	    padding-left: 0; !important;
	    color: #333333 !important;
	    min-height: 34px;
		line-height: 34px;
	  }

	span.colorChooser {
	   
	    margin-left: .1em;
	    position: relative;
	    display: inline-block;
	    overflow: visible;
	    vertical-align: middle;
	    z-index: 1;
	    background-image: none !important;
	    width: 64px !important;
	    margin-bottom: .1em !important;
	    cursor: pointer !important;
	    border: 1px solid #CCCCCC;
	    height: 1em;
	    
	}
		
		.cssboxRight input
		{
			width: 100%;
			height:1em;
			border: none;
			padding-top: 0;
			padding-bottom: 0;
		}

		
		.cssRow
		{
			margin-bottom: 1em;
			
		}	
				
		.cssboxLeft
		{	
			border: 1px solid #5c5c5c;
			border-right: none;
			border-radius: 4px 0px 0px 4px;		
			height: 36px;
			line-height: 34px;
		}
		
		.cssboxRight
		{	
	 		border: 1px solid #5c5c5c;	
			border-radius: 0px 4px 4px 0px;
			height: 36px;
			line-height: 34px;
		}

		.DropWidth
		{
			width:100%;
			
		}	
			
		.DropBox
		{
			padding-left: 0;
			padding-right: 0;
		}	
		
		.select2-container--bootstrap .select2-selection 
		{
	    	border: none !important;        	
		}
		
		
		#PreviewMLP 
		{ 
			outline: 2px dashed red; 
			padding: 2em; 
			margin: 2em 0; 
			border-style: dashed
			
		}
		
		<!--- Remove CKEditor styles on editable elements so preview look natural  --->
		#PreviewMLP img.cke_hidden
		{
			background-image:none !important;
			border: none !important;
			
		}	
		
		#PreviewMLP .cke_editable form {
	    	border: none !important;
	    	padding: 0;
		}

				
		.bootstrapeditor > .row > .col-xs-1, .bootstrapeditor > .row > .col-xs-2, .bootstrapeditor > .row > .col-xs-3, .bootstrapeditor > .row > .col-xs-4, .bootstrapeditor > .row > .col-xs-5, .bootstrapeditor > .row > .col-xs-6, .bootstrapeditor > .row > .col-xs-7, .bootstrapeditor > .row > .col-xs-8, .bootstrapeditor > .row > .col-xs-9, .bootstrapeditor > .row > .col-xs-10, .bootstrapeditor > .row > .col-xs-11, .bootstrapeditor > .row > .col-xs-12, .bootstrapeditor > .row > .columns {
	    background-color: #FFFFFF;
		}

	<!--- 	/* Editor is a container - need to override bootstrap so it looks good on this page */ --->
		.container {
	    	width: 100% !important;
		}

		@media only screen and (max-width: 440px){
			.page-header.navbar .top-menu .navbar-nav{
				width: 100% !important;
			}
		}

</style>

<cfif ModalMode EQ 0>	
	<main class="container-fluid page my-plan-page portlet light bordered">

		<section class="bg-white">
</cfif>	
		
			
			<cfif ModalMode EQ 0>
			
				<div class="content-header">
					<div class="row">
						<div class="col-sm-12 page-title" style="margin-bottom: 1em; margin-left: 20px">Marketing Landing Portal <cfif action EQ "template"><span style="color:maroon">TEMPLATE</span></cfif> Editor</div>
					</div>
										
				</div>
			
			</cfif>
			
			<!--- END : content-header --->
			<div class="content-body">
			<form id="mlp-sim-form"> 

				<div class="row no-print" style="">    
					<div class="<cfoutput>#MLPMainSection#</cfoutput>" style="padding:2em;">			            	
	            	
	            		<cfoutput>
		            		<input type="hidden" id="ccpxDataId" name="ccpxDataId" value="#RetCPPXData['CPPID']#">
		            		<input type="hidden" id="action" name="action" value="#action#">
							 <!--- Display term of service --->
							 <div class="clearfix">
							 	<div class="col-md-12 form-horizontal">
									<div class="form-group">
									    <label for="card_number" class="col-sm-4 control-label">Name:<span class="text-danger">*</span></label>
									    <div class="col-sm-8">
									      <input type="text" class="form-control  validate[required,custom[noHTML],ajax[ajaxCPPNameAvailable]]" maxlength="255" id="cppxName" name="cppxName" value="#RetCPPXData['CPPNAME']#" data-prompt-position="topLeft:100" >
									    </div>
							  		</div>
	
							  		<cfif action NEQ "template" AND  action NEQ "add">	
									   	<div class="form-group">
										    <label id="MLPURLLable" for="cppxURL" class="col-sm-4 control-label"><div>{URL key}: https://MLP-X.com/lz/#RetCPPXData['CPPURL']#</div></label>
										    <div class="col-sm-8">
										      <input type="text" class="form-control validate[ajax[ajaxCPPUrlAvailable],custom[checkCustomURL]]" maxlength="255" id="cppxURL" name="cppxURL" value="#RetCPPXData['CPPURL']#" data-prompt-position="topLeft:100">
										    </div>
									  	</div>							  	
									</cfif>
									
								</div>								
			                </div>	
	            		</cfoutput>
					</div>
					
	            </div>
			
				<div class="row no-print">	            
		            <div class="col-md-12 wrap-mlp-simon">
		            	<div class="mlp-simon">
			                <div id="BSEditor" class=""><cfoutput>#RetCPPXData.RAWCONTENT#</cfoutput></div>    
		            	</div>
		            </div>
				</div>    		        
		        
		        <div class="row">	            
		        	<cfif RetCPPXData.CUSTOMCSS NEQ "">
		        		<style type="text/css">
		        			@media print{

		        				body
								{
			        				<cfoutput>
			        				<cfif RetCPPXData.CUSTOMCSS NEQ "">
		
										<cfset CUSTOMCSS = deSerializeJSON(RetCPPXData.CUSTOMCSS) />
										
										
										<cfif !StructKeyExists(CUSTOMCSS, "backgroundcolor") >
											<cfset CUSTOMCSS.backgroundcolor = "##FFFFFF"/>
										</cfif>	
									
										<cfif !StructKeyExists(CUSTOMCSS, "fontfamily") >
											<cfset CUSTOMCSS.fontfamily = "inherit" />
										</cfif>	
													
										<cfif !StructKeyExists(CUSTOMCSS, "borderwidth") >
											<cfset CUSTOMCSS.borderwidth = "0px"/>
										</cfif>	
										
										<cfif !StructKeyExists(CUSTOMCSS, "bordercolor") >
											<cfset CUSTOMCSS.bordercolor = "##FF0000"/>
										</cfif>	
										
										<cfif !StructKeyExists(CUSTOMCSS, "borderstyle") >
											<cfset CUSTOMCSS.borderstyle = "none"/>
										</cfif>	
														
										background-color: <cfoutput>#CUSTOMCSS.backgroundcolor#</cfoutput>;
										font-family: <cfoutput>#CUSTOMCSS.fontfamily#</cfoutput>;
										border-width: <cfoutput>#CUSTOMCSS.borderwidth#</cfoutput>;
										border-color: <cfoutput>#CUSTOMCSS.bordercolor#</cfoutput>;
										border-style: <cfoutput>#CUSTOMCSS.borderstyle#</cfoutput>;
									</cfif>
									</cfoutput>
								}
		        			}
		        		</style>
		        	</cfif>	
		            <div class="<cfoutput>#MLPMainSection#</cfoutput>" style="padding:2em;">		

						<div id="PreviewMLP" class="PreviewCSS"></div>
		        
		        	</div>
				</div> 
				
		        <div class="no-print">
			            
					<div class="<cfoutput>#MLPMainSection#</cfoutput>" style="padding:2em;">		
																	
						<div class="row cssRow">
							<div class="col-sm-6 cssboxLeft">Web Safe Font Family
							</div>
							<div class="col-sm-6 cssboxRight DropBox">
								<select name="font-family" id="font-family" onchange="ApplyCssToPreviewBox();" class="DropWidth">
									<option value="None">None</option>
																		
									<optgroup label="Serif Fonts">
										<option value="Georgia, serif"></option>
										<option value='"Palatino Linotype", "Book Antiqua", Palatino, serif'>"Palatino Linotype", "Book Antiqua", Palatino, serif</option>
										<option value='"Times New Roman", Times, serif'>"Times New Roman", Times, serif</option>
									</optgroup>
									
									<optgroup label="Sans-Serif Fonts">
										<option value='Arial, Helvetica, sans-serif'>Arial, Helvetica, sans-serif</option>
										<option value='"Arial Black", Gadget, sans-serif'>"Arial Black", Gadget, sans-serif</option>
										<option value='"Comic Sans MS", cursive, sans-serif'>"Comic Sans MS", cursive, sans-serif</option>
										<option value='Impact, Charcoal, sans-serif'>Impact, Charcoal, sans-serif</option>
										<option value='"Lucida Sans Unicode", "Lucida Grande", sans-serif'>"Lucida Sans Unicode", "Lucida Grande", sans-serif</option>
										<option value='Tahoma, Geneva, sans-serif'>Tahoma, Geneva, sans-serif</option>
										<option value='"Trebuchet MS", Helvetica, sans-serif'>"Trebuchet MS", Helvetica, sans-serif</option>
										<option value='Verdana, Geneva, sans-serif'>Verdana, Geneva, sans-serif</option>
									</optgroup>
									
									<optgroup label="Monospace Fonts">
										<option value='"Courier New", Courier, monospace'>"Courier New", Courier, monospace</option>
										<option value='"Lucida Console", Monaco, monospace'>"Lucida Console", Monaco, monospace</option>
									</optgroup>									
																		
								</select>
							</div>
						</div>
						
						<div class="row cssRow">
							<div class="col-sm-6 cssboxLeft">Border Style</div>
							<div class="col-sm-6 cssboxRight DropBox">
								<select name="border-style" id="border-style" onchange="ApplyCssToPreviewBox();" class="DropWidth">
									<option value="none">none</option>
									<option value="hidden">hidden</option>
									<option value="dotted">dotted</option>
									<option value="dashed">dashed</option>
									<option value="solid" selected>solid</option>
									<option value="double">double</option>
									<option value="groove">groove</option>
									<option value="ridge">ridge</option>
									<option value="inset">inset</option>
									<option value="outset">v</option>
									
								</select>
							</div>
						</div>
						
						<div class="row cssRow">
							<div class="col-sm-6 cssboxLeft">Background Color</div>
							<div class="col-sm-6 cssboxRight">
								<input type="text" class="color_input color colorpickinput" id="background-color" value="#FFFFFF" />
							</div>
						</div>

						<div class="row cssRow">
							<div class="col-sm-6 cssboxLeft">Border Color</div>
							<div class="col-sm-6 cssboxRight">
								<input type="text" class="color_input color colorpickinput" id="border-color" value="#FF0000" />
							</div>
						</div>
						
						<div class="row cssRow">
							<div class="col-sm-6 cssboxLeft" id="BorderWidthLabel">Border Width (px)</div>
							<div class="col-sm-6 cssboxRight">
								<input type="text" onchange="ApplyCssToPreviewBox();" class="input_box cssinput" name="border-width" id="border-width" value="0">
							</div>
						</div>
																					
					</div>
		                   
				</div>
	            
	            <div class="col-md-12 no-print">
		            <div class="form-footer text-center" style="margin-bottom: 3em;">

		        		<a href="javascript:void(0)" target="_blank" class="btn btn-success-custom btn-print-preview mlp-action">Print Preview</a>

		        		<cfif ModalMode EQ 0>	
			        		
			        		<a data-toggle="modal" title="Preview Raw" class="btn btn-success-custom btn-preview-cpp mlp-action" href="#previewObjectRaw" id="PreviewButtonRaw">Preview Raw</a>
			
							<button type="button" class="btn btn-success-custom btn-save-cpp mlp-action" id="SaveMLP">Save</button>
							
						<cfelse>
	
							<button type="button" class="btn btn-success-custom btn-save-cpp mlp-action" id="SaveMLP">Save Changes</button>
						
						</cfif>	
		        	</div>
	            </div>
			</form>   	 
         </div>
	


<cfif ModalMode EQ 0>			
		</section>	

	</main>
</cfif>




<!--- MODAL POPUP --->
<div class="bootbox modal fade no-print" id="previewObject" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel">Preview</h4>
      </div>
      <div class="modal-body PreviewCSS">
        
      </div>
      <div class="modal-footer">
        <!--- <button type="button" class="btn btn-primary btn-success-custom" id="btn-rename-campaign">Save</button> --->
        <button type="button" class="btn btn-default btn-back-custom" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="bootbox modal fade no-print" id="previewObjectRaw" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel">Preview</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <!--- <button type="button" class="btn btn-primary btn-success-custom" id="btn-rename-campaign">Save</button> --->
        <button type="button" class="btn btn-default btn-back-custom" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>





<div class="bootbox modal fade no-print" id="print-preview" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Print Preview</h4>
      </div>
      <div class="modal-body">
        <iframe id="print-preview-page" src="" style="top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;"></iframe>
      </div>
      <div class="modal-footer">
        <!--- <button type="button" class="btn btn-primary btn-success-custom" id="btn-rename-campaign">Save</button> --->
        <button type="button" class="btn btn-default btn-back-custom" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>




<!--- 
	
	
<!--- SMS Input Type - Leave in &nbsp; in empty objects or CKEditor will strip them - even with ACF off --->


<form id="CustomerPreferenceForm" name="CustomerPreferenceForm">

<input name="SubscriberListId" type="hidden" value="132" />

<div class="form-group">
    <div class="input-group">
	    <input type="input" class="USMobileNumber form-control validate[required,custom[usPhoneNumber]] sms-number cpp-object" name="sms-number" placeholder="SMS Number">
	    <span class="input-group-addon remove-input-object" style="display:none"><i class="fa fa-minus">&nbsp;</i></span>
	    <span class="input-group-addon add-input-object"><i class="fa fa-plus-circle fa-lg">&nbsp;</i></span>
	</div>    
</div>

<p><input name="SubmitCPP" type="submit" value="Submit" /></p>

</form>

				   			
Require a hidden Subcriber List Id?
	
	
	
<form id="cppForm" name="cppForm">
	<input class="subcriber-list" name="SubscriberListId" type="hidden" value="132"  />
	<div class="form-group">
		<div class="input-group">
			<input class="sms-number form-control validate[required,custom[usPhoneNumber]] sms-number cpp-object" name="sms-number" placeholder="SMS Number" type="input" /> 
			<span class="input-group-addon remove-input-object" style="display:none"><i class="fa fa-minus">&nbsp;</i></span> 
			<span class="input-group-addon add-input-object"><i class="fa fa-plus-circle fa-lg">&nbsp;</i></span>
		</div>
	</div>

	<p><input id="btn-submit-cpp" name="btn-submit-cpp" type="button" value="Submit" /></p>
</form>

<div class="g-recaptcha" data-sitekey="6LdGsCQTAAAAADVkuNsuTKPlqKqB4dceqTML3ydc"></div>

--->

<!--- 
	
	<div class="row"><div class="col-xs-12" aria-describedby="cke_76" style="position: relative;"><p><input name="GoNow" data-cke-saved-name="GoNow" value="GoNow" type="button" data-cke-editable="1" contenteditable="false">​​​​​​​<br></p></div></div><div class="row bootstrapeditor-row-content"><div class="col-xs-4 bootstrapeditor-col-plugin-editor cke_editable cke_editable_inline cke_contents_ltr cke_show_borders" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, editor2" title="Rich Text Editor, editor2" aria-describedby="cke_179" style="position: relative;"><p>Column 4/12</p></div><div class="col-xs-8" aria-describedby="cke_250" style="position: relative;"><p>Column 8/12</p></div></div>
	
	
--->
	<!--- https://select2.github.io/  --->

	<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
		<cfinvokeargument name="path" value="/session/sire/css/mlp.css">
	</cfinvoke>
	
	<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
		<cfinvokeargument name="path" value="/session/sire/js/vendors/bootstrapeditor/bootstrapeditor.css">
	</cfinvoke>
	
	<!--- http://www.menucool.com/color-picker --->
	<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
		<cfinvokeargument name="path" value="/session/sire/css/mccolorpicker.css">
	</cfinvoke>

	<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
		<cfinvokeargument name="path" value="/session/sire/css/print-preview.css">
	</cfinvoke>
	
	<!--- http://www.menucool.com/color-picker --->
	<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
		<cfinvokeargument name="path" value="/session/sire/js/mccolorpicker.js">
	</cfinvoke>
	
	
	<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
		<cfinvokeargument name="path" value="/session/sire/js/site/mlpv3.js">
	</cfinvoke>

	

	<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
		<cfinvokeargument name="path" value="/public/sire/js/jquery.browser.js">
	</cfinvoke>

	<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
		<cfinvokeargument name="path" value="/session/sire/js/vendors/printpreview/jquery.tools.min.js">
	</cfinvoke>

	<!---
	<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
		<cfinvokeargument name="path" value="/session/sire/js/vendors/printpreview/jquery.tools.min.js">
	</cfinvoke>
	--->

	<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
		<cfinvokeargument name="path" value="/session/sire/js/vendors/printpreview/jquery.print-preview.js">
	</cfinvoke>

	<!--- Dependencies --->
	<!--- http://ckeditor.com/download --->
	<!--- WARNING: There are JLP Mods* made to this - so dont just update with newer version--->
	<!--- WARNING: There are also JLP Mods* made to ckeditor.js which is called from in here - so dont just update with newer version--->
	<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
		<cfinvokeargument name="path" value="/session/sire/js/vendors/bootstrapeditor/bootstrapeditor.js">
	</cfinvoke>
	
<!--- For modal widows that have already loaded these dont load these twice --->
<cfif ModalMode EQ 0>

	<!--- https://select2.github.io/  --->
	<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	    <cfinvokeargument name="path" value="/public/sire/css/select2.min.css">
	</cfinvoke>
	
	<!--- https://select2.github.io/  --->
	<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	    <cfinvokeargument name="path" value="/public/sire/css/select2-bootstrap.min.css">
	</cfinvoke>
	
	<!--- https://select2.github.io/  --->
	<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	    <cfinvokeargument name="path" value="/public/sire/js/select2.min.js">
	</cfinvoke>

	
	<cfinclude template="../views/layouts/master.cfm">

</cfif>



<!--- <script src='https://www.google.com/recaptcha/api.js'></script> --->
<!--- CKEditor Custom build -  http://ckeditor.com/builder   --->
<!--- Allow CKEditor to save generic HTML content without rules to scrub it - standard ACF will remove too much important stuff - CKEditorConfig: {allowedContent:true} 
	
	Note:
	
	CKEditorConfig: {allowedContent:true} is being ignored on load
	You need to adding CKEDITOR.config.allowedContent = true; to your ckeditor/config.js file to allow divs and other special content,
	see /webroot/session/sire/js/vendors/bootstrapeditor/plugins/editor/ckeditor/config.js
	
	
		editor = editor.replace( 'editor', {
		    on: {
		        // Synchronize the preview on user action that changes the content.
		        change: syncPreview,
		        
		        // Synchronize the preview when the new data is set.
		        contentDom: syncPreview
		    }
		} );
		
		
		CKEDITOR.on('instanceCreated', function(e) {
		    e.editor.on('change', function (event) {
		        // change event in CKEditor 4.x
		        $('#PreviewMLP').html( e.getData() );

		    });
		}); 


--->

<script type="text/javascript">
	
	var editor;
	var preview;
	
	
	<cfif ModalMode EQ 0>
	
		$(function() 
		{
			InitMLPEditor();						
			    					
		});
	
	</cfif>
	
	
	function InitMLPEditor()
	{
		editor = BootstrapEditor.init('BSEditor', {createDefaultRow:true, CKEditorConfig: {allowedContent:true} });
			
			initCustomCSS();
			
			<!--- Loading this twice will cause issues so only load if stand alone mode --->
			<cfif ModalMode EQ 0>				
				// Make all select boxes select2 style
				$("SELECT").select2( { theme: "bootstrap"} );
			</cfif>
			
			ApplyCssToPreviewBox();
			
			<!--- Stupid editor is adding <br> to empty elements but it will mess tons of things up - so remove with the .split('><br></').join('></') --->
			$('#PreviewMLP').html( editor.getContent().split('><br></').join('></') );
			
			
			$('#PreviewButton').on('click', function(e){
			
				<!--- Stupid editor is adding <br> to empty elements but it will mess tons of things up - so remove with the .split('><br></').join('></') --->
				var contents = editor.getContent().split('><br></').join('></');	
						
				$("#previewObject .modal-body").html( contents );			 
			
			});			
			
			$('#PreviewButtonRaw').on('click', function(e){
			
				<!--- Stupid editor is adding <br> to empty elements but it will mess tons of things up - so remove with the .split('><br></').join('></') --->
				var contents = editor.getContent().split('><br></').join('></');		
						
				 $("#previewObjectRaw .modal-body").html('<textarea cols="50" rows="7" style="font-family:monospace; max-width: 100%;">' + contents + '</textarea>' );			 
			
			});			
			
			$('#SaveMLP').on('click', function(e){
											
				var inpCustomCSS = JSON.stringify(GetCSSCustomObj(true));				
				SaveMLP(inpCustomCSS);		 		 
			
			});	
			
			
		
		
	}
	
	// Get save values and update display
	function initCustomCSS()
	{	
		$('#background-color').val('<cfoutput>#CUSTOMCSS.backgroundcolor#</cfoutput>');
		$('#font-family').val('<cfoutput>#CUSTOMCSS.fontfamily#</cfoutput>');
		$('#border-width').val('<cfoutput>#CUSTOMCSS.borderwidth#</cfoutput>');
		$('#border-color').val('<cfoutput>#CUSTOMCSS.bordercolor#</cfoutput>');
		$('#border-style').val('<cfoutput>#CUSTOMCSS.borderstyle#</cfoutput>');
	}
	
	function syncPreview() {
    	$('#PreviewMLP').html( editor.getContent().split('><br></').join('></') );
	}

	function OnColorChanged(selectedColor, colorPickerIndex) 
	{
		ApplyCssToPreviewBox();	 
	}
			
	function GetCSSCustomObj(inpIsStorage)
	{
		
		if(inpIsStorage)
		{
			// JAva wont let you store variables with dashes in them! So only store without dashes in DB
			var obj = 
			{
				'backgroundcolor': $('#background-color').val(),
			    'fontfamily': $('#font-family').val(),			    
			    'borderwidth': $('#border-width').val(),
			    'bordercolor': $('#border-color').val(),
			    'borderstyle': $('#border-style').val(),
			    'overflow': 'hidden'   
			};
			
		}
		else
		{
			
			var obj = 
			{
				'background-color': $('#background-color').val(),
			    'font-family': $('#font-family').val(),
			    'border-width': $('#border-width').val(),
			    'border-color': $('#border-color').val(),
			    'border-style': $('#border-style').val(),
			    'overflow': 'hidden'   
			};	
		}
		
		

		return obj;
		
	}
	
	function ApplyCssToPreviewBox()
	{
		var inpCustomCSS = GetCSSCustomObj(false);
		$('.PreviewCSS').css(inpCustomCSS);
	}
	
</script>

<script type="text/javascript">


	$( document ).ready(function() {
		$('a.btn-print-preview').printPreview();
		$("#mlp-sim-form").validationEngine();
	});


	$('#cppxName, #cppxURL').on('keypress', function (event) {
	    var regex = new RegExp("^[<>]+$");
	    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	    if (regex.test(key)) {
	       event.preventDefault();
	       return false;
	    }
	});

	$('#cppxName, #cppxURL').bind("cut copy paste",function(e) {
		e.preventDefault();
	});

	$(document).bind('DOMSubtreeModified', function () {
		if($('#print-modal').length){
			// location.href = "#print-modal";
			 $('html, body').animate({
		        scrollTop: $("#print-modal").offset().top
		    });
		}
	});

</script>

	