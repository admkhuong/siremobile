<cfparam name="url.plan" default="0">
<cfset variables.urlpage = 'upgradeplan' />
<cfset discount_plan_price_percent = 0/>
<cfset discount_plan_price_flat_rate = 0/>

<cfquery name="checkExistPlan" datasource="#Session.DBSourceREAD#">
    SELECT PlanId_int,Order_int FROM simplebilling.plans WHERE Status_int = 1 AND PlanId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.plan#"> 
</cfquery>
<cfif checkExistPlan.RecordCount EQ 0>
    <cflocation url="/plans-pricing" addtoken="false">
</cfif>

<cfinvoke method="GetUserPlan" component="session/sire/models/cfc/order_plan" returnvariable="getUserPlan">
</cfinvoke>

<cfif getUserPlan.PLANORDER GTE checkExistPlan.Order_int >
    <cflocation url="/plans-pricing" addtoken="false">
</cfif>



<cfquery name="getPlans" datasource="#Session.DBSourceREAD#">
    SELECT * FROM simplebilling.plans 
    WHERE 
        Status_int = 1 AND  Order_int > <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getUserPlan.PLANORDER#"> 
    ORDER BY Order_int  
</cfquery>


<!---<cfinvoke component="public.sire.models.cfc.order_plan" method="GetOrderPlan" returnvariable="price2">
    <cfinvokeargument name="plan" value="2">
</cfinvoke>
<cfinvoke component="public.sire.models.cfc.order_plan" method="GetOrderPlan" returnvariable="price3">
    <cfinvokeargument name="plan" value="3">
</cfinvoke>
--->

<cfinvoke component="public.sire.models.cfc.order_plan" method="GetOrderPlan" returnvariable="price">
    <cfinvokeargument name="plan" value="#url.plan#">
</cfinvoke>


<cfset BuyKeywordNumber = 0>
<!--- When upgrate plan get plan keyword limit number https://setaintl2008.atlassian.net/browse/SIRE-965 --->

<cfset TotalKeyword = getUserPlan.PLANKEYWORDSLIMITNUMBER + getUserPlan.KEYWORDPURCHASED > 

<cfif TotalKeyword GT price.KEYWORDSLIMITNUMBER>
  <cfset BuyKeywordNumber = (TotalKeyword - price.KEYWORDSLIMITNUMBER)>
</cfif>

<!--- CHECK USER PROMOTION --->
<cfquery name="getUserPromotion" datasource="#Session.DBSourceREAD#">
  SELECT 
    up.PromotionId_int,
    up.PromotionLastVersionId_int,
    up.UsedDate_dt,
    up.RecurringTime_int
  FROM simplebilling.userpromotions up 
  WHERE
      up.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.USERID#">
      AND up.UsedTime_int < up.RecurringTime_int
      AND up.PromotionStatus_int = 1
  LIMIT 1
</cfquery>


<cfset priceBuyKeywordNumber = (BuyKeywordNumber * getUserPlan.PRICEKEYWORDAFTER)>
<cfset totalAMOUNT = (price.AMOUNT + BuyKeywordNumber * getUserPlan.PRICEKEYWORDAFTER)>
<cfset totalAMOUNTPlan = totalAMOUNT>

<!--- Check User Have Promotion --->
<cfif getUserPromotion.RecordCount GT 0>

    <cfquery name="getPromotion" datasource="#Session.DBSourceREAD#">
        SELECT
          pc.promotion_id,
          pc.origin_id,
          pc.discount_type_group,
          pc.discount_plan_price_percent,
          pc.discount_plan_price_flat_rate,
          pc.promotion_keyword,
          pc.promotion_MLP,
          pc.promotion_short_URL,
          pc.promotion_credit
        FROM 
            simplebilling.promotion_codes pc 
        WHERE
            pc.promotion_id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getUserPromotion.PromotionId_int#">
            AND pc.promotion_status  = 1
        LIMIT 1
    </cfquery>

    <!--- Have a Promotion --->
    <cfif getUserPromotion.RecordCount GT 0>
        <cfif getPromotion.discount_type_group EQ 1>
            <cfset totalAMOUNT = (price.AMOUNT - (price.AMOUNT * getPromotion.discount_plan_price_percent / 100) - getPromotion.discount_plan_price_flat_rate + BuyKeywordNumber * getUserPlan.PRICEKEYWORDAFTER)>
            <cfset discount_plan_price_percent = getPromotion.discount_plan_price_percent/>
            <cfset discount_plan_price_flat_rate = getPromotion.discount_plan_price_flat_rate/>
        </cfif>

    </cfif>
</cfif>

<cfif totalAMOUNT < 0 >
    <cfset totalAMOUNT = 0>
</cfif>



<cfset variables.menuToUse = 2 />

<!--- RETRIVE CUSTOMER DATA --->
<cfset firstName =''>
<cfset lastName =''>
<cfset emailAddress =''>
<cfset primaryPaymentMethodId = -1>
<cfset expirationDate =''>
<cfset maskedNumber =''>
<cfset line1 =''>
<cfset city =''>
<cfset state =''>
<cfset zip =''>
<cfset country =''> 
<cfset phone =''>
<cfset hidden_card_form =''>
<cfset disabled_field =''>
<cfset cvv =''>
<cfset paymentType = 'card'>
<cfset RetCustomerInfo = ''>

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("../assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css", true)
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js", true)
        .addJs("../assets/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js", true)
        .addJs("../assets/pages/scripts/upgradeplan.js")
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true);
</cfscript>

<!--- GET USER BILLING DATA --->
<cfinvoke component="session.sire.models.cfc.billing"  method="GetBalanceCurrentUser"  returnvariable="RetValBillingData"></cfinvoke>

<!--- GET USER PLAN DETAIL --->
<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUserPlan" returnvariable="RetUserPlan"></cfinvoke>

<!--- GET NUMBER OF USED MLPS BY USER --->
<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUsedMLPsByUserId" returnvariable="RetUsedMLP"></cfinvoke>

<!--- GET NUMBER OF USED SHORT URLS BY USER --->
<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUsedShortURLsByUserId" returnvariable="RetUsedShortURL"></cfinvoke>

<!--- GET USER REFERRAL INFO --->
<cfinvoke component="session.sire.models.cfc.referral" method="GetUserReferInfo" returnvariable="RetValUserReferInfo">
    <cfinvokeargument name="inpUserId" value="#SESSION.USERID#">
</cfinvoke>

<!--- GET USER INFO --->
<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke>

<cfinvoke component="session.sire.models.cfc.billing" method="RetrieveCustomer" returnvariable="RetCustomerInfo"></cfinvoke>

<cfinclude template="../configs/credits.cfm">

<cfinvoke component="session.sire.models.cfc.order_plan" method="checkUserPlanExpried" returnvariable="checkUserPlan">
    <cfinvokeargument name="userId" value="#SESSION.USERID#">
</cfinvoke>

<cfif checkUserPlan.RXRESULTCODE GT 0> <!--- IF USER PLAN EXPIRED REDIRECT USER TO MY PLAN PAGE --->
    <cflocation url="/session/sire/pages/my-plan" addtoken="false">
</cfif>    

<cfif RetCustomerInfo.RXRESULTCODE EQ 1>    
    <cfset firstName = RetCustomerInfo.CUSTOMERINFO.firstName>
    <cfset lastName = RetCustomerInfo.CUSTOMERINFO.lastName>
    <cfset emailAddress = RetCustomerInfo.CUSTOMERINFO.emailAddress>
    <cfset primaryPaymentMethodId = 0> <!--- do not print --->
    <cfset expirationDate = RetCustomerInfo.CUSTOMERINFO.expirationDate>
    <cfset maskedNumber = RetCustomerInfo.CUSTOMERINFO.maskedNumber>
    <cfset line1 = RetCustomerInfo.CUSTOMERINFO.line1>
    <cfset city = RetCustomerInfo.CUSTOMERINFO.city>
    <cfset state = RetCustomerInfo.CUSTOMERINFO.state>
    <cfset zip = RetCustomerInfo.CUSTOMERINFO.zip>
    <cfset country = RetCustomerInfo.CUSTOMERINFO.country> 
    <cfset phone = RetCustomerInfo.CUSTOMERINFO.phone>
    <cfset hidden_card_form = 'display:none'>
    <cfset disabled_field ='display: none'>
    <cfset paymentType = RetCustomerInfo.CUSTOMERINFO.paymentType>
    <cfset cvv =RetCustomerInfo.CUSTOMERINFO.cvv>
<cfelseif RetCustomerInfo.RXRESULTCODE EQ -1>
    <main class="container-fluid page my-plan-page">
        <cfinclude template="../views/commons/credits_available.cfm">
            <section class="row bg-white">
                <div class="content-header">
                    <div class="col-md-12">
                        <cfparam name="variables._title" default="My Plan - Sire">
                        <p class="text-danger text-center "><cfoutput>#RetCustomerInfo.MESSAGE#</cfoutput></p>
                    </div>  
                </div>
            </section>  
    </main>
    <cfinclude template="../views/layouts/master.cfm">
    <cfexit>
</cfif>

<cfset userPlanName = 'Free'>
<cfif RetUserPlan.RXRESULTCODE EQ 1>
    <cfset userPlanName =   RetUserPlan.PLANNAME>
</cfif>

<cfif RetUserPlan.PLANEXPIRED GT 0> 
    <cfset monthlyAmount = RetUserPlan.AMOUNT + (RetUserPlan.KEYWORDPURCHASED + RetUserPlan.KEYWORDBUYAFTEREXPRIED)*RetUserPlan.PRICEKEYWORDAFTER >
<cfelse>    
    <cfset monthlyAmount = RetUserPlan.AMOUNT + RetUserPlan.KEYWORDPURCHASED*RetUserPlan.PRICEKEYWORDAFTER >
</cfif>
<cfset planMonthlyAmount = RetUserPlan.AMOUNT />

<!--- GET MAX KEYWORD CAN EXPIRED --->
<cfset UnsubscribeNumberMax = 0>
<cfif RetUserPlan.PLANEXPIRED EQ 1>
    <cfif RetUserPlan.KEYWORDAVAILABLE GT RetUserPlan.KEYWORDBUYAFTEREXPRIED>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDBUYAFTEREXPRIED>
    <cfelse>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDAVAILABLE>
    </cfif>
<cfelseif RetUserPlan.PLANEXPIRED EQ 2> <!--- IF USER PLAN HAS DOWNGRADE IT CAN NOT UNSCRIBE KEYWORD --->
    <!---
    <cfif RetUserPlan.KEYWORDAVAILABLE GT RetUserPlan.KEYWORDBUYAFTEREXPRIED>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDBUYAFTEREXPRIED>
    <cfelse>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDAVAILABLE>
    </cfif>    
    --->
    <cfset UnsubscribeNumberMax = 0>
<cfelse>
    <cfif RetUserPlan.KEYWORDAVAILABLE GT RetUserPlan.KEYWORDPURCHASED>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDPURCHASED>
    <cfelse>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDAVAILABLE>
    </cfif>
</cfif>

<div class="portlet light bordered">
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form id="buy_credits" class="form-gd" action="##" autocomplete="off" role="form">

            <cfoutput>
            <input type="hidden" name="primaryPaymentMethodId" value="#primaryPaymentMethodId#">
            <input type="hidden" value="#userInfo.FULLEMAIL#" id="h_email">
            <input type="hidden" name="h_email_save" value="#userInfo.FULLEMAIL#">
            <!--- <input type="hidden" name="expirationDate" value="#expirationDate#"> --->
            <input type="hidden" id="plan" name="plan" value="#url.plan#">
            <input type="hidden" id="actionStatus" value="">
            <input type="hidden" id="amount" name="amount" value="#totalAMOUNT#">
            <input type="hidden" id="discount_plan_price_percent" name="discount_plan_price_percent" value="#discount_plan_price_percent#">
            <input type="hidden" id="discount_plan_price_flat_rate" name="discount_plan_price_flat_rate" value="#discount_plan_price_flat_rate#">
            <input type="hidden" value="#userInfo.FULLEMAIL#" id="h_email" name="h_email">
            </cfoutput>
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="clearfix">
                            <h3 class="form-heading">Plan Details</h3>
                        </div>
                        <div class="statistical">
                            <!--- Plan Details included by plan_details.cfm --->
                            <cfinclude template="../views/commons/plan_details.cfm">
                        </div>

                        <div class="tabbable tabbable-tabdrop tab-invite-1">
                            <button class="btn green-gd uk-width-1-1 visible-xs uk-margin-small-bottom" type="button" uk-toggle="target: #toggle-xs; cls: uk-hidden"><i class="fa fa-th" aria-hidden="true"></i> Toggle My Plan Tab</button>
                            <ul id="toggle-xs" class="uk-child-width-1-3 uk-child-width-1-5@s invite-ul uk-tab" uk-margin>
                                <li>
                                    <a href="/session/sire/pages/my-plan">
                                        <span class="tab-img i-1"></span>
                                        <span>My Plan</span>
                                    </a>
                                </li>
                                <li class="uk-active">
                                    <a>
                                        <span class="tab-img i-2"></span>
                                        <span>Upgrade Plan</span>
                                    </a>
                                </li>
                                <li>
                                    <!--- /session/sire/pages/buy-sms --->
                                    <a href="/session/sire/pages/my-plan?active=addon">
                                        <span class="tab-img i-3"></span>
                                        <span>Buy Credit</span>
                                    </a>
                                </li>
                                <li>
                                    <cfif RetUserPlan.PLANEXPIRED GTE 2 && RetUserPlan.KEYWORDBUYAFTEREXPRIED GT 0  >
                                        <a href="javascript:void(0)" class="icon-disable" title="Please pay for your keywords"><span class="tab-img i-4"></span><span>Buy Keyword</span></a> 
                                    <cfelse>  
                                        <a href="/session/sire/pages/buy-keyword" title="" class=""><span class="tab-img i-4"></span><span>Buy Keyword</span></a>
                                    </cfif>
                                </li>
                                <li>
                                    <a href="/session/sire/pages/invite-friend"><span class="tab-img i-5"></span><span>Invite a friend</span></a>
                                </li>
                            </ul>
                            <div class="wrap-upgrade">
                                <div class="form-group">
                                    <label>Select Your Plan</label>
                                    <cfoutput>
                                        <select class="bs-select form-control" id="order-plan">
                                            <cfloop query="getPlans">
                                                <option value="#getPlans.PlanId_int#" <cfif url.plan EQUAL getPlans.PlanId_int>selected</cfif>>#getPlans.PlanName_vch#</option>
                                            </cfloop>
                                        </select>
                                    </cfoutput>
                                </div>

                                <div class="statistical">
                                    <ul class="nav list-plan-statistical">
                                        <li>Monthly Amount: <span id="MonthlyAmountTotal">$<cfoutput>#totalAMOUNTPlan#</cfoutput></span>
                                            <em>Your billing date will be the same day every month.</em>
                                        </li>
                                        <li>Credits Available:  <span id="FirstSMSInclude"><cfoutput>#NumberFormat(price.FIRSTSMSINCLUDED,',')#</cfoutput></span></li>
                                        <li>Keywords Available: <span id="NumberKeyword"><cfoutput>#price.KEYWORDSLIMITNUMBER#</cfoutput></span></li>
                                        <li>MLPs Available: <span id="MLPAvailable"><cfoutput>#price.MLPSLIMITNUMBER#</cfoutput></span></li>
                                        <li>Short URLs Available: <span id="ShortURLAvailable"><cfoutput>#price.SHORTURLSLIMITNUMBER#</cfoutput></span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <!--- Cardholder details included by cardholder_details.cfm --->
                        <cfinclude template="../views/commons/cardholder_details.cfm">
                        
                        <button type="submit" class="btn newbtn green-gd btn-payment-credits">Upgrade Now</button>
                        <a type="button" class="btn newbtn green-cancel"  href="javascript:history.go(-1)">Cancel</a>

                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>

<cfinclude template="../views/commons/sireplandetails.cfm">

<cfparam name="variables._title" default="Upgrade Plan - Sire">

<cfinclude template="../views/layouts/master.cfm">

<cfinclude template="../views/commons/what-mlp-keyword.cfm">
