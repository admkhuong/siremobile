<cfscript>
    createObject("component", "public.sire.models.helpers.layout")
        .addJs("/public/js/jquery.tmpl.min.js", true)
        .addJs("/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js")
        .addCss("/public/js/jquery-ui-1.11.4.custom/jquery-ui.css", true)
        
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", false)

        .addCss("/session/sire/assets/global/plugins/uniform/css/uniform.default.css", true)
        .addJs('/session/sire/assets/global/scripts/filter-bar-generator.js', true)
        .addJs('/session/sire/assets/global/plugins/uniform/jquery.uniform.min.js', true)
        .addJs("/session/sire/assets/pages/scripts/phone_number_lookup.js");        
</cfscript>


<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission">
    <cfinvokeargument name="inpSkipRedirect" value="1">
</cfinvoke>

<cfparam name="isDialog" default="0">
<cfparam name="isUpdateSuccess" default="">


<div class="portlet light bordered">
    <!--- Filter bar --->
    <div class="row">
        <div class="col-sm-3 col-md-4">
            <h2 class="page-title">Contact String History Report</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3 col-md-4">
            <h4 style="font-weight: bold;">Phone Number:</h4>
        </div>
        <div class="col-sm-3 col-md-4">
            <h4 style="font-weight: bold;">Select Report Type:</h4>
        </div>
        <div class="col-sm-3 col-md-4">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3 col-md-4">
            <form id="phonenumber-option">
                <input type="text" class ="form-control validate[required,custom[usPhoneNumber]]" id="phone-number" value="">
            </form>
        </div>
        <div id="select-report" class="col-sm-3 col-md-4">
            <select id="report-select" class="form-control" >
                <option value="1">The Campaign List</option>
                <option value="2">Opt In and Opt Out List</option>
                <option value="3">Message Received from Device (MO) List</option>
            </select>
        </div>
        <div class="col-sm-3 col-md-4">
            <button type="button" class="btn btn-success btn-success-custom btn-custom_size" id="btn_Lookup">Look up now</button>
        </div>
    </div>
    <div class="row">
        <!--- row blank --->
        <div class="col-sm-3 col-md-4"> <h2 class="page-title"></h2></div>
        <div class="col-sm-9 col-md-8"></div>
    </div>
    <div class="row">
        <div class="col-sm-3 col-md-4 shorturl">
            <b><p id ="report-detail">Report Detail:</p></b>
        </div>
        <div class="col-sm-9 col-md-8">
            <div id="box-filter" class="form-gd">
            </div>
        </div>
    </div>
    <!--- Listing --->
    <div class="row">
        <div class="col-md-12">
            <div id="campaign-list">
                <div class="re-table">
                    <div class="table-responsive">
                        <table id="tblCampaignList" class="table table-responsive table-striped">
                            <!--- report data --->
                        </table>
                    </div>
                </div>
            </div>

            <div style="display:none;" id="optin-optout-list">
                <div class="re-table">
                    <div class="table-responsive">
                        <table id="tblOptInOptOutList" class="table table-bordered table-striped">
                            <!--- report data --->
                        </table>
                    </div>
                </div>
            </div>

            <div style="display:none;" id="MO-list">
                <div class="re-table">
                    <div class="table-responsive">
                        <table id="tblMOList" class="table table-bordered table-striped">
                             <!--- report data --->
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--- Modal For Revenue report --->
<div class="modal fade" id="phone-conversation-view" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content" style ="width:1200px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title gray-text"><strong id="keyword-support-modal-title">Phone Conversation Detail</strong></h4>
            </div>
            <div class="modal-body">
                <div id="phone-conversation">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="tblPhoneConversationDetail" class="table table-responsive table-striped">
                                        <!--- report data --->
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Close</button>
                <!--- <input type="hidden" name="userID" value=""> --->
            </div>
        </div>
    </div>
</div>

<cfparam name="variables._title" default="Phone Number Lookup Report">
<cfinclude template="../views/layouts/master.cfm">

