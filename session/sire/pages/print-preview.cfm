
<cfparam name="ccpid" default="0">
<cfinvoke component="session.sire.models.cfc.mlp" method="ReadCPP" returnvariable="RetCPPXData">
	<cfinvokeargument name="ccpxDataId" value="#ccpid#">
</cfinvoke> 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
	    <title>Print Preview Example Page</title>
	    <!---
	    <link rel="stylesheet" href="/session/sire/css/print.css" type="text/css" media="print" />
	    
	    <link rel="stylesheet" href="/session/sire/css/print-preview.css" type="text/css" media="screen">
	    <link href="/public/sire/css/bootstrap.css" rel="stylesheet" media="screen">
	    --->
	</head>
	<body>   
	<cfif StructKeyExists(Session, "USERID") AND RetCPPXData.USERID EQ Session.USERID>             
		<div class="row">
			<div class="col-xs-12" aria-describedby="cke_78" style="position: relative;">
				<cfoutput>#RetCPPXData.RAWCONTENT#</cfoutput>
			</div>
		</div>
		<div class="text-center" id="aside"></div>
	</cfif>
	<!---
	<script src="/public/sire/js/jquery-2.1.4.js"></script>
	<script src="/public/sire/js/jquery.browser.js"></script>
	<script src="/session/sire/js/vendors/printpreview/jquery.tools.min.js"></script>
	<script src="/session/sire/js/vendors/printpreview/jquery.print-preview.js" type="text/javascript" charset="utf-8"></script>

	<script type="text/javascript">
	    $(function() {
	        /*
	         * Initialise print preview plugin
	         */
	        // Add link for print preview and intialise
	        $('#aside').prepend('<a class="print-preview btn btn-success">Print Preview</a>');
	        $('a.print-preview').printPreview();
	        
	        // Add keybinding (not recommended for production use)
	        $(document).bind('keydown', function(e) {
	            var code = (e.keyCode ? e.keyCode : e.which);
	            if (code == 80 && !$('#print-modal').length) {
	                $.printPreview.loadPrintPreview();
	                return false;
	            }            
	        });
	    });
	</script>
	--->
	</body>
</html>