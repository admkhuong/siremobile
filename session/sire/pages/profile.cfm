<cfset variables.menuToUse = 2 />
<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("/public/sire/css/validationEngine.jquery.css", true)
        .addCss("/public/sire/js/jquery-ui-1.12.1.custom/jquery-ui.min.css", true)
        .addCss("/session/sire/css/dropzone.css", true)        
        .addJs("/public/sire/js/jquery-ui-1.12.1.custom/jquery-ui.min.js")
        .addJs("/public/sire/js/jquery.mask.min.js")
        .addJs("/session/sire/js/vendors/dropzone.js", true)
        .addJs("../assets/global/plugins/jquery-knob/js/jquery.knob.js", true)
        .addJs("../assets/pages/scripts/profile.js")
        .addCss("/session/sire/assets/global/plugins/uniform/css/uniform.default.css", true)        
        .addJs("/session/sire/assets/global/plugins/uniform/jquery.uniform.min.js",true)
        .addJs("../assets/global/plugins/chartjs/Chart.bundle.js", true);

</cfscript>

<cfinclude template="/session/sire/configs/paths.cfm">

<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke>

<cfif userInfo.RESULT NEQ 'SUCCESS'>
    <cflocation url = '/' addtoken="false">
</cfif>

<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="userOrgInfo"></cfinvoke>

<cfset _has_Org = false>    
<cfif userOrgInfo.RXRESULTCODE EQ 1>
    <cfset _has_Org = true>
    <cfset orgInfoData = userOrgInfo.ORGINFO>
</cfif>


<cfset check_user_security_question = false>

<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserListQuestion" returnvariable="userListQuestion">
    <cfinvokeargument name="detail_question" value=0>
</cfinvoke>

<cfif userListQuestion.RESULT EQ 'SUCCESS' >
    <cfset check_user_security_question = true>
</cfif>

 <div class="portlet light bordered">
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form role="form" class="form-gd" autocomplete="off" name="my_account_form" id="my_account_form">
            <div class="form-body">
                <div class="row">                 
                    <div class="col-md-10">
                        <div class="col-wrap-profile">
                            <div class="clearfix handle-float-heading">
                                <h3 class="form-heading" >Profile</h3>
                                <cfif _has_Org AND orgInfoData.OrganizationLogo_vch NEQ ''>
                                    <cfset display_logo = ''>
                                <cfelse>
                                    <cfset display_logo = 'display:none'>
                                </cfif> 
                                <a class="anchor-upload upload-avatar fileinput-button">Upload Logo</a><br><br>
                                <!--- <a style="<cfoutput>#display_logo#</cfoutput>; margin-top: 10px" class="anchor-upload" id="removeLogo">Remove Logo</a> --->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                    <div class="col-md-4">                        
                        <div class="col-wrap-profile">
                            <input type="hidden" name="OrganizationLogo_vch" id="OrganizationLogo_vch" value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationLogo_vch#</cfoutput></cfif>">
                            <input type="hidden" name="Old_OrganizationLogo_vch" id="OrganizationLogo_vch" value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationLogo_vch#</cfoutput></cfif>">
                            <input type="hidden" name="UpdateOrganizationLogo" id="UpdateOrganizationLogo" value="0">
                            <cfif display_logo EQ ''>
                                <div class="clearfix wrap-show-img">
                                    <div class="OrgLogo-wrapper" style="<cfoutput>#display_logo#</cfoutput>">
                                        <img id="OrgLogo" src="<cfoutput>#_S3IMAGEURL##orgInfoData.OrganizationLogo_vch#</cfoutput>"/>
                                    </div>
                                    <a style="<cfoutput>#display_logo#</cfoutput>" class="anchor-upload" id="removeLogo">
                                        <i class="fa fa-times"></i>
                                    </a>                                                                    
                                </div>
                            <cfelse>
                                <div class="clearfix wrap-show-img">
                                    <div class="OrgLogo-wrapper" style="<cfoutput>#display_logo#</cfoutput>">
                                        <img id="OrgLogo" src=""/>
                                        <a class="anchor-upload" id="removeLogoSimon" onclick="hiddenImage()">
                                            <i class="fa fa-times"></i>
                                        </a>                                                                            
                                    </div>
                                </div>

                            </cfif>                            
                            <div class="logo-btn-group col-sm-3 col-lg-2">
                                <div id="uploadArea"  style="display: none">
                                    <div class="table table-striped" class="files" id="previews">
                                        <div id="template" class="file-row">
                                            <!-- This is used as the file preview template -->
                                            <div>
                                                <span class="preview"><img data-dz-thumbnail /></span>
                                            </div>
                                            <div>
                                                <p class="name" data-dz-name></p>
                                                <strong class="error text-danger" data-dz-errormessage></strong>
                                            </div>
                                            <div>
                                                <p class="size" data-dz-size></p>
                                                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                                    <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                                                </div>
                                            </div>
                                            <div>
                                                <button class="btn btn-primary start">
                                                    <i class="glyphicon glyphicon-upload"></i>
                                                    <span>Start</span>
                                                </button>
                                                <button data-dz-remove class="btn btn-warning cancel">
                                                    <i class="glyphicon glyphicon-ban-circle"></i>
                                                    <span>Cancel</span>
                                                </button>
                                                <button data-dz-remove class="btn btn-danger delete">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                    <span>Delete</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--- <button class="btn btn-info upload-avatar" id="imageUpload"><b>Upload Logo</b></button> --->
                                    <!--- <span style="margin-top: 10px" class="btn btn-info upload-avatar fileinput-button"><b>Upload Logo</b></span>
                                    <button style="<cfoutput>#display_logo#</cfoutput>; margin-top: 10px" class="btn btn-link" id="removeLogo"><strong>Remove Logo</strong></button> --->
                                
                            </div>  
                        </div>  
                    </div>        
                    <div class="col-md-4"></div>        
                    <div class="col-md-5">
                        <div class="col-wrap-profile">                           
                            <div class="form-group">
                                <input type="hidden" name="OrganizationId_int" id="OrganizationId_int" value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationId_int#</cfoutput></cfif>">
                                <label>Company Name</label>
                                <input type="text" class="form-control" name="OrganizationName_vch" id="OrganizationName_vch" maxlength="250" data-prompt-position="topLeft:100" 
                                        value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationName_vch#</cfoutput></cfif>">
                            </div>
                            <div class="form-group">
                                <label>Phone Number</label>
                                <input type="text" class="form-control validate[custom[usPhoneNumberII]]" id="OrganizationPhone_vch" name="OrganizationPhone_vch" maxlength="15" data-prompt-position="topLeft:100"
                                        value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationPhone_vch#</cfoutput></cfif>">
                            </div>
                            <div class="form-group">
                                <label>E-mail</label>
                                <input type="text" class="form-control validate[custom[email]]" id="OrganizationEmail_vch" name="OrganizationEmail_vch" maxlength="250" data-prompt-position="topLeft:100" 
                                        value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationEmail_vch#</cfoutput></cfif>">
                            </div>
                            <div class="form-group">
                                <label>Website</label>
                                <input type="text" class="form-control validate[funcCall[isValidUrl]]" id="OrganizationWebsite_vch" name="OrganizationWebsite_vch" maxlength="250" data-prompt-position="topLeft:100"
                                        value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationWebsite_vch#</cfoutput></cfif>">
                            </div>
                            <div class="form-group">
                                <label>Website Tiny URL Version</label>
                                <input type="text" class="form-control validate[funcCall[isValidUrl]]" id="OrganizationWebsiteTiny_vch" name="OrganizationWebsiteTiny_vch" maxlength="250" data-prompt-position="topLeft:100"
                                        value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationWebsiteTiny_vch#</cfoutput></cfif>">
                            </div>
                           
                            <div class="form-group">
                                <label>Tagline</label>
                                <input type="text" class="form-control" id="OrganizationTagline_vch" name="OrganizationTagline_vch" maxlength="250" data-prompt-position="topLeft:100"
                                        value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationTagline_vch#</cfoutput></cfif>">
                            </div>
                            
                        </div>                            
                    </div>

                    <div class="col-md-5">
                        <div class="col-wrap-profile">
                            <div class="row">
                                <div class="col-sm-12 col-lg-12">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Hours of Operation</label>
                                <input type="text" class="form-control" id="OrganizationHOO_vch" name="OrganizationHOO_vch" maxlength="250" data-prompt-position="topLeft:100"
                                        value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationHOO_vch#</cfoutput></cfif>">
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" class="form-control" id="OrganizationAdd_vch" name="OrganizationAdd_vch" maxlength="250" data-prompt-position="topLeft:100"
                                        value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationAdd_vch#</cfoutput></cfif>">
                            </div>
                             <div class="form-group">
                                <label>City</label>
                                <input type="text" class="form-control" id="OrganizationCity_vch" name="OrganizationCity_vch" maxlength="250" data-prompt-position="topLeft:100" 
                                        value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationCity_vch#</cfoutput></cfif>">
                            </div>
                            <div class="form-group">
                                <label>State</label>
                                <input type="text" class="form-control" id="OrganizationState_vch" name="OrganizationState_vch" maxlength="250" data-prompt-position="topLeft:100" 
                                        value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationState_vch#</cfoutput></cfif>">
                            </div>
                            <div class="form-group">
                                <label>Zip Code</label>
                                <input type="text" class="form-control" id="OrganizationZipcode_vch" name="OrganizationZipcode_vch" maxlength="10" data-prompt-position="topLeft:100"
                                        value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationZipcode_vch#</cfoutput></cfif>">
                            </div>                           
                            
                            
                            <div class="form-group">
                                <label>Timezone</label>
                                <!--- <input type="text" class="form-control validate[funcCall[isValidUrl]]" id="OrganizationTimeZone_vch" name="OrganizationWebsiteTiny_vch" maxlength="250" data-prompt-position="topLeft:100"
                                        value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationWebsiteTiny_vch#</cfoutput></cfif>"> --->
                                <select class="form-control" id="time-zone">
                                    <option timeZoneId="0" gmtAdjustment="GMT-12:00" useDaylightTime="0" value="-13">None</option>
                                    <option timeZoneId="1" gmtAdjustment="GMT-12:00" useDaylightTime="0" value="-12">(GMT-12:00) International Date Line West</option>
                                    <option timeZoneId="2" gmtAdjustment="GMT-11:00" useDaylightTime="0" value="-11">(GMT-11:00) Midway Island, Samoa</option>
                                    <option timeZoneId="3" gmtAdjustment="GMT-10:00" useDaylightTime="0" value="-10">(GMT-10:00) Hawaii</option>
                                    <option timeZoneId="4" gmtAdjustment="GMT-09:00" useDaylightTime="1" value="-9">(GMT-09:00) Alaska</option>
                                    <option timeZoneId="5" gmtAdjustment="GMT-08:00" useDaylightTime="1" value="-8">(GMT-08:00) Pacific Time (US & Canada)</option>
                                    <option timeZoneId="6" gmtAdjustment="GMT-08:00" useDaylightTime="1" value="-8">(GMT-08:00) Tijuana, Baja California</option>
                                    <option timeZoneId="7" gmtAdjustment="GMT-07:00" useDaylightTime="0" value="-7">(GMT-07:00) Arizona</option>
                                    <option timeZoneId="8" gmtAdjustment="GMT-07:00" useDaylightTime="1" value="-7">(GMT-07:00) Chihuahua, La Paz, Mazatlan</option>
                                    <option timeZoneId="9" gmtAdjustment="GMT-07:00" useDaylightTime="1" value="-7">(GMT-07:00) Mountain Time (US & Canada)</option>
                                    <option timeZoneId="10" gmtAdjustment="GMT-06:00" useDaylightTime="0" value="-6">(GMT-06:00) Central America</option>
                                    <option timeZoneId="11" gmtAdjustment="GMT-06:00" useDaylightTime="1" value="-6">(GMT-06:00) Central Time (US & Canada)</option>
                                    <option timeZoneId="12" gmtAdjustment="GMT-06:00" useDaylightTime="1" value="-6">(GMT-06:00) Guadalajara, Mexico City, Monterrey</option>
                                    <option timeZoneId="13" gmtAdjustment="GMT-06:00" useDaylightTime="0" value="-6">(GMT-06:00) Saskatchewan</option>
                                    <option timeZoneId="14" gmtAdjustment="GMT-05:00" useDaylightTime="0" value="-5">(GMT-05:00) Bogota, Lima, Quito, Rio Branco</option>
                                    <option timeZoneId="15" gmtAdjustment="GMT-05:00" useDaylightTime="1" value="-5">(GMT-05:00) Eastern Time (US & Canada)</option>
                                    <option timeZoneId="16" gmtAdjustment="GMT-05:00" useDaylightTime="1" value="-5">(GMT-05:00) Indiana (East)</option>
                                    <option timeZoneId="17" gmtAdjustment="GMT-04:00" useDaylightTime="1" value="-4">(GMT-04:00) Atlantic Time (Canada)</option>
                                    <option timeZoneId="18" gmtAdjustment="GMT-04:00" useDaylightTime="0" value="-4">(GMT-04:00) Caracas, La Paz</option>
                                    <option timeZoneId="19" gmtAdjustment="GMT-04:00" useDaylightTime="0" value="-4">(GMT-04:00) Manaus</option>
                                    <option timeZoneId="20" gmtAdjustment="GMT-04:00" useDaylightTime="1" value="-4">(GMT-04:00) Santiago</option>
                                    <option timeZoneId="21" gmtAdjustment="GMT-03:30" useDaylightTime="1" value="-3.5">(GMT-03:30) Newfoundland</option>
                                    <option timeZoneId="22" gmtAdjustment="GMT-03:00" useDaylightTime="1" value="-3">(GMT-03:00) Brasilia</option>
                                    <option timeZoneId="23" gmtAdjustment="GMT-03:00" useDaylightTime="0" value="-3">(GMT-03:00) Buenos Aires, Georgetown</option>
                                    <option timeZoneId="24" gmtAdjustment="GMT-03:00" useDaylightTime="1" value="-3">(GMT-03:00) Greenland</option>
                                    <option timeZoneId="25" gmtAdjustment="GMT-03:00" useDaylightTime="1" value="-3">(GMT-03:00) Montevideo</option>
                                    <option timeZoneId="26" gmtAdjustment="GMT-02:00" useDaylightTime="1" value="-2">(GMT-02:00) Mid-Atlantic</option>
                                    <option timeZoneId="27" gmtAdjustment="GMT-01:00" useDaylightTime="0" value="-1">(GMT-01:00) Cape Verde Is.</option>
                                    <option timeZoneId="28" gmtAdjustment="GMT-01:00" useDaylightTime="1" value="-1">(GMT-01:00) Azores</option>
                                    <option timeZoneId="29" gmtAdjustment="GMT+00:00" useDaylightTime="0" value="0">(GMT+00:00) Casablanca, Monrovia, Reykjavik</option>
                                    <option timeZoneId="30" gmtAdjustment="GMT+00:00" useDaylightTime="1" value="0">(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London</option>
                                    <option timeZoneId="31" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="1">(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</option>
                                    <option timeZoneId="32" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="1">(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague</option>
                                    <option timeZoneId="33" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="1">(GMT+01:00) Brussels, Copenhagen, Madrid, Paris</option>
                                    <option timeZoneId="34" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="1">(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb</option>
                                    <option timeZoneId="35" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="1">(GMT+01:00) West Central Africa</option>
                                    <option timeZoneId="36" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Amman</option>
                                    <option timeZoneId="37" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Athens, Bucharest, Istanbul</option>
                                    <option timeZoneId="38" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Beirut</option>
                                    <option timeZoneId="39" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Cairo</option>
                                    <option timeZoneId="40" gmtAdjustment="GMT+02:00" useDaylightTime="0" value="2">(GMT+02:00) Harare, Pretoria</option>
                                    <option timeZoneId="41" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius</option>
                                    <option timeZoneId="42" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Jerusalem</option>
                                    <option timeZoneId="43" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Minsk</option>
                                    <option timeZoneId="44" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Windhoek</option>
                                    <option timeZoneId="45" gmtAdjustment="GMT+03:00" useDaylightTime="0" value="3">(GMT+03:00) Kuwait, Riyadh, Baghdad</option>
                                    <option timeZoneId="46" gmtAdjustment="GMT+03:00" useDaylightTime="1" value="3">(GMT+03:00) Moscow, St. Petersburg, Volgograd</option>
                                    <option timeZoneId="47" gmtAdjustment="GMT+03:00" useDaylightTime="0" value="3">(GMT+03:00) Nairobi</option>
                                    <option timeZoneId="48" gmtAdjustment="GMT+03:00" useDaylightTime="0" value="3">(GMT+03:00) Tbilisi</option>
                                    <option timeZoneId="49" gmtAdjustment="GMT+03:30" useDaylightTime="1" value="3.5">(GMT+03:30) Tehran</option>
                                    <option timeZoneId="50" gmtAdjustment="GMT+04:00" useDaylightTime="0" value="4">(GMT+04:00) Abu Dhabi, Muscat</option>
                                    <option timeZoneId="51" gmtAdjustment="GMT+04:00" useDaylightTime="1" value="4">(GMT+04:00) Baku</option>
                                    <option timeZoneId="52" gmtAdjustment="GMT+04:00" useDaylightTime="1" value="4">(GMT+04:00) Yerevan</option>
                                    <option timeZoneId="53" gmtAdjustment="GMT+04:30" useDaylightTime="0" value="4.5">(GMT+04:30) Kabul</option>
                                    <option timeZoneId="54" gmtAdjustment="GMT+05:00" useDaylightTime="1" value="5">(GMT+05:00) Yekaterinburg</option>
                                    <option timeZoneId="55" gmtAdjustment="GMT+05:00" useDaylightTime="0" value="5">(GMT+05:00) Islamabad, Karachi, Tashkent</option>
                                    <option timeZoneId="56" gmtAdjustment="GMT+05:30" useDaylightTime="0" value="5.5">(GMT+05:30) Sri Jayawardenapura</option>
                                    <option timeZoneId="57" gmtAdjustment="GMT+05:30" useDaylightTime="0" value="5.5">(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi</option>
                                    <option timeZoneId="58" gmtAdjustment="GMT+05:45" useDaylightTime="0" value="5.75">(GMT+05:45) Kathmandu</option>
                                    <option timeZoneId="59" gmtAdjustment="GMT+06:00" useDaylightTime="1" value="6">(GMT+06:00) Almaty, Novosibirsk</option>
                                    <option timeZoneId="60" gmtAdjustment="GMT+06:00" useDaylightTime="0" value="6">(GMT+06:00) Astana, Dhaka</option>
                                    <option timeZoneId="61" gmtAdjustment="GMT+06:30" useDaylightTime="0" value="6.5">(GMT+06:30) Yangon (Rangoon)</option>
                                    <option timeZoneId="62" gmtAdjustment="GMT+07:00" useDaylightTime="0" value="7">(GMT+07:00) Bangkok, Hanoi, Jakarta</option>
                                    <option timeZoneId="63" gmtAdjustment="GMT+07:00" useDaylightTime="1" value="7">(GMT+07:00) Krasnoyarsk</option>
                                    <option timeZoneId="64" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="8">(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi</option>
                                    <option timeZoneId="65" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="8">(GMT+08:00) Kuala Lumpur, Singapore</option>
                                    <option timeZoneId="66" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="8">(GMT+08:00) Irkutsk, Ulaan Bataar</option>
                                    <option timeZoneId="67" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="8">(GMT+08:00) Perth</option>
                                    <option timeZoneId="68" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="8">(GMT+08:00) Taipei</option>
                                    <option timeZoneId="69" gmtAdjustment="GMT+09:00" useDaylightTime="0" value="9">(GMT+09:00) Osaka, Sapporo, Tokyo</option>
                                    <option timeZoneId="70" gmtAdjustment="GMT+09:00" useDaylightTime="0" value="9">(GMT+09:00) Seoul</option>
                                    <option timeZoneId="71" gmtAdjustment="GMT+09:00" useDaylightTime="1" value="9">(GMT+09:00) Yakutsk</option>
                                    <option timeZoneId="72" gmtAdjustment="GMT+09:30" useDaylightTime="0" value="9.5">(GMT+09:30) Adelaide</option>
                                    <option timeZoneId="73" gmtAdjustment="GMT+09:30" useDaylightTime="0" value="9.5">(GMT+09:30) Darwin</option>
                                    <option timeZoneId="74" gmtAdjustment="GMT+10:00" useDaylightTime="0" value="10">(GMT+10:00) Brisbane</option>
                                    <option timeZoneId="75" gmtAdjustment="GMT+10:00" useDaylightTime="1" value="10">(GMT+10:00) Canberra, Melbourne, Sydney</option>
                                    <option timeZoneId="76" gmtAdjustment="GMT+10:00" useDaylightTime="1" value="10">(GMT+10:00) Hobart</option>
                                    <option timeZoneId="77" gmtAdjustment="GMT+10:00" useDaylightTime="0" value="10">(GMT+10:00) Guam, Port Moresby</option>
                                    <option timeZoneId="78" gmtAdjustment="GMT+10:00" useDaylightTime="1" value="10">(GMT+10:00) Vladivostok</option>
                                    <option timeZoneId="79" gmtAdjustment="GMT+11:00" useDaylightTime="1" value="11">(GMT+11:00) Magadan, Solomon Is., New Caledonia</option>
                                    <option timeZoneId="80" gmtAdjustment="GMT+12:00" useDaylightTime="1" value="12">(GMT+12:00) Auckland, Wellington</option>
                                    <option timeZoneId="81" gmtAdjustment="GMT+12:00" useDaylightTime="0" value="12">(GMT+12:00) Fiji, Kamchatka, Marshall Is.</option>
                                    <option timeZoneId="82" gmtAdjustment="GMT+13:00" useDaylightTime="0" value="13">(GMT+13:00) Nuku'alofa</option>
                                </select>
                                <input class="form-group" type="hidden" name="TimezoneId_int" id="TimezoneId_int">
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="col-md-12"></div>
                    <div class="col-md-5">
                        <div class="col-wrap-profile">
                            <h3 class="form-section"  style="color: rgb(92,92,92); font-family:'Source Sans Pro', sans-serif; margin-left: 0px;">Account Help and Stop Message Defaults</h3>
                            <p>                                
                                The Help and Stop message must be of minimum length 10 characters. If you leave it blank, default messages will be used.
                            </p>
                            <div class="form-group"> 
                                <label>Help Message</label> 
                                <textarea class="form-control custom-message" id="custom-help-message" name="CustomHelpMessage_vch" maxlength="160" data-prompt-position="topLeft:150" rows="3"><cfif _has_Org><cfoutput>#orgInfoData.CustomHelpMessage_vch#</cfoutput></cfif></textarea>

                                <div class="row">
                                    <div class="col-md-8 col-lg-7 col-sm-7 col-xs-7">
                                        <span class="help-block tdesc simon-italic">ex. For more help, visit www.yoursite.com or call 123-456-7890</span>
                                    </div>
                                    <div class="col-md-4 col-lg-5 col-sm-5 col-xs-5">
                                        <!--- <p class="help-block pull-right" id="help-block" style="margin-top: 5px;"> <span id="custom_help_available1">0</span>/160 characters available</p> --->
                                        <p class="help-block pull-right" id="help-block" style="margin-top: 5px;"> <span id="custom_help_available1"></span></p>
                                    </div>
                                </div>

                            </div>


                            <div class="form-group">
                                <label>Stop Message</label>
                                <textarea class="form-control custom-message" id="custom-stop-message" name="CustomStopMessage_vch" maxlength="160" data-prompt-position="topLeft:150" rows="3" placeholder=""><cfif _has_Org><cfoutput>#orgInfoData.CustomStopMessage_vch#</cfoutput></cfif></textarea>

                                 <div class="row">
                                    <div class="col-md-8 col-lg-7 col-sm-7 col-xs-7">
                                        <span class="help-block simon-italic">ex. You are opted out and will receive no more msgs. Need more Help? 888-747-4411</span>
                                    </div>
                                    <div class="col-md-4 col-lg-5 col-sm-5 col-xs-5">
                                        <!--- <p class="help-block pull-right" id="stop-block" style="margin-top: 5px;"> <span id="custom_stop_available1">0</span>/160 characters available</p> --->
                                        <p class="help-block pull-right" id="stop-block" style="margin-top: 5px;"> <span id="custom_stop_available1"></span></p>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 col-lg-12">
                                    <p class="help-block detail-simon-helper">
                                        <i>
                                            All campaigns must respond with messages for users who send HELP and STOP keywords. By default we will use the messages that you setup on this page. You may also specify custom HELP and STOP response messages for each campaign.
                                        </i>
                                    </p>
                                </div>
                            </div>    
                            <div class="form-actions">
                                <button type="submit" class="btn newbtn green-gd" id="btn_save_my_account">Save</button>
                                <a href="/session/sire/pages/profile" type="button" class="btn newbtn green-cancel" id="btn_cancel">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>

<style type="text/css">
    .blue-gd {
        background: #578ca5;
        color: #fff;
    }
    #modal-signout {
        padding-right: 0;
    }
    #modal-signout.fade {
        background: rgba(0, 0, 0, 0.59);
    }
    #modal-signout .modal-content {
        border-radius: 4px;
        box-shadow: 0 14px 59px #19171a;
    }
    #modal-signout .modal-header {
        border-bottom: none;
    }
    #modal-signout .modal-header .close {
        margin-top: 0 !important;
    }
    #modal-signout .modal-body{
        padding: 15px 15px 45px 15px;
    }
    #modal-signout .message {
        margin-top: 0;
        margin-bottom: 25px;
        text-align: center;
        font-size: 22px;
        font-weight: bold;
        color: #5c5c5c;
    }
    #modal-signout .button-confirm-signout button {
        font-size: 14px;
        margin: 0px 5px 5px 5px;
    }
    #modal-signout .button-confirm-signout button:focus,
    #modal-signout .button-confirm-signout button:hover {
        color: #fff;
    }
</style>

<cfsavecontent variable="variables.portleft">
<div class="portlet light bordered box-widget-sidebar hidden-sm hidden-xs">
    <cfset TotalOrgSteps = 13 />

    <!--- Assume user name and password are already set from sign up --->
    <cfset TotalStepsComplete = 0 />

    <!--- Check for first name --->
    <!--- <cfif LEN(TRIM(userInfo.USERNAME)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif> --->

    <!--- Check for last name --->
    <!--- <cfif LEN(TRIM(userInfo.LASTNAME)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif> --->

    <!--- Check for SMS number --->
    <!--- <cfif LEN(TRIM(userInfo.FULLMFAPHONE)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif> --->

    <!--- Check for ADDRESSe --->
   <!---  <cfif LEN(TRIM(userInfo.ADDRESS)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif> --->

    <!--- Check for POSTALCODE --->
    <!--- <cfif LEN(TRIM(userInfo.POSTALCODE)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif> --->

    <!--- Check for ORGANIZATIONBUSINESSNAME_VCH --->
    <!--- <cfif LEN(TRIM(userOrgInfo.ORGINFO.ORGANIZATIONBUSINESSNAME_VCH)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif> --->

    <!--- Check for ORGANIZATIONNAME_VCH --->
    <cfif LEN(TRIM(userOrgInfo.ORGINFO.ORGANIZATIONNAME_VCH)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>

    <!--- Check for ORGANIZATIONWEBSITE_VCH --->
    <cfif LEN(TRIM(userOrgInfo.ORGINFO.ORGANIZATIONWEBSITE_VCH)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>


    <cfif LEN(TRIM(userOrgInfo.ORGINFO.OrganizationHOO_vch)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>

    <!--- Check for ORGANIZATIONWEBSITETINY_VCH --->
    <cfif LEN(TRIM(userOrgInfo.ORGINFO.ORGANIZATIONWEBSITETINY_VCH)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>
    
    <!--- Check for ORGANIZATIONZIPCODE --->
    <cfif LEN(TRIM(userOrgInfo.ORGINFO.OrganizationZipcode_vch)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>

    <!--- Check for ORGANIZATIONPHONE_VCH --->
    <cfif LEN(TRIM(userOrgInfo.ORGINFO.ORGANIZATIONPHONE_VCH)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>

    <cfif LEN(TRIM(userOrgInfo.ORGINFO.ORGANIZATIONADD_VCH)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>

    <!--- Check for ORGANIZATIONEMAIL_VCH --->
    <cfif LEN(TRIM(userOrgInfo.ORGINFO.ORGANIZATIONEMAIL_VCH)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>
    
    <!--- Check for ORGANIZATIONCUSTOMHELPMESSAGE --->
    <cfif LEN(TRIM(userOrgInfo.ORGINFO.CUSTOMHELPMESSAGE_VCH)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>

    <!--- Check for ORGANIZATIONCUSTOMSTOPMESSAGE --->
    <cfif LEN(TRIM(userOrgInfo.ORGINFO.CUSTOMSTOPMESSAGE_VCH)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>

    <!--- Check for ORGANIZATIONTAGLINE --->
    <cfif LEN(TRIM(userOrgInfo.ORGINFO.ORGANIZATIONTAGLINE_VCH)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>

    <!--- Check for ORGANIZATIONCUSTOMSTOPMESSAGE --->
    <cfif userOrgInfo.ORGINFO.TIMEZONEID_INT GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>

    <!--- Check for ORGANIZATIONLOGO --->
    <cfif LEN(TRIM(userOrgInfo.ORGINFO.ORGANIZATIONLOGO_VCH)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>

    <!--- Check for ORGNAIZATIONCITY --->
     <cfif LEN(TRIM(userOrgInfo.ORGINFO.ORGANIZATIONCITY_VCH)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>
    <!--- Check for ORGNAIZATIONSTATE --->
     <cfif LEN(TRIM(userOrgInfo.ORGINFO.OrganizationState_vch)) GT 0> <cfset TotalStepsComplete = TotalStepsComplete + 1 /></cfif>
    
    <div class="portlet-body">
        <div class="wrap-dial-step">
            <input type="hidden" name="" id="percent-complete" value="<cfoutput>#(TotalStepsComplete/TotalOrgSteps)*100#</cfoutput>">
            <input type="hidden" name="" id="company-name" value="<cfoutput>#TRIM(userOrgInfo.ORGINFO.ORGANIZATIONNAME_VCH)#</cfoutput>">
            <h4 class="stt-step"><span class="pie_value"></span>% Complete</h4>
            <h4 class="sub-stt-step hide">You're Ready to Create a Campaign</h4>
            <!--- <div class="">
                <input type="text" id="dialStep" class="dial">
            </div> --->

            <div class="bg-step">
                <canvas id="dialStep"></canvas>
            </div>

            <!--- <a href="##" class="click-test">Test</a> --->
        </div>
    </div>
</div>
</cfsavecontent>


<cfparam name="variables._title" default="Profile - Sire">

<cfinclude template="../views/layouts/master.cfm">