<cfheader name="expires" value="#now()#">
<cfheader name="pragma" value="no-cache">
<cfheader name="cache-control" value="no-cache, no-store, must-revalidate">


<!--- Dont allow any exports that take more than a hour to run.... --->
<cfsetting requestTimeout="3600" />
<cfparam name="prefixFileName" default="DispositionExport">

<cfparam name="inprel1" default="">
<cfparam name="inprel2" default="CSV">
<cfparam name="inpBatchIdList" default="0000">
<cfparam name="inpStart" default="#dateformat(DateAdd('d', -1, NOW()),'yyyy-mm-dd')#">
<cfparam name="inpEnd" default="#dateformat(NOW(),'yyyy-mm-dd')#">
<cfparam name="inpcustomdata1" default="">
<cfparam name="inpcustomdata2" default="">
<cfparam name="inpcustomdata3" default="">
<cfparam name="inpcustomdata4" default="">
<cfparam name="inpcustomdata5" default="">
<cfparam name="iDisplayLength" default="5000">
<cfparam name="iDisplayStart"  default="0">


<!---  See sample query logic at bottom of the page --->    
<!--- New reporting requirement - keep all reports in a cfc and method in the session.sire.models.cfc.reports dir --->
<!--- component="session.sire.models.cfc.reports.#TargetCFC#" method="#TargetReportName#" --->
<!--- Queries used here need to be able to return limited data - same paging mechanism used in DataTables - LIMIT (START) (END) --->
<cfset TargetCFC = "" />
<cfset TargetReportName = "" />   
       
<cfif ListLen(inprel1, '.') EQ 2>
	
	<cfset TargetCFC = ListGetAt(inprel1, 1, '.') />
	<cfset TargetReportName = ListGetAt(inprel1, 2, '.') />
	
</cfif>	
   
<cfset RetVarQueryToCSV = "">
    	      
    <cfif LEN(TRIM(inpBatchIdList)) EQ 0>
    	<cfset inpBatchIdList = "EXPORTALL" />
    </cfif>
    
    <cfswitch expression="#inprel2#">
    	
        <cfcase value="CSV">
            <cfcontent type="application/msexcel">
            <cfheader 
                name="Content-Disposition" 
                value="filename=#prefixFileName#_#ListGetAt(inpBatchIdList, 1)#_At_#DateFormat(Now(), 'yyyy_mm_dd')#_#LSTimeFormat(Now(),'hh_mm_ss')#.csv" 
                charset="utf-8">  
                                   
        </cfcase>
        
        <cfcase value="WORD">
            <cfcontent type="application/msword">
            <cfheader 
                name="Content-Disposition" 
                value="filename=#prefixFileName#_#ListGetAt(inpBatchIdList, 1)#_At_#DateFormat(Now(), 'yyyy_mm_dd')#_#LSTimeFormat(Now(),'hh_mm_ss')#.doc" 
                charset="utf-8">   
                                  
        </cfcase>
                   
        <cfdefaultcase>
            <cfcontent type="application/msexcel">
            <cfheader 
                name="Content-Disposition" 
                value="filename=#prefixFileName#_#ListGetAt(inpBatchIdList, 1)#_At_#DateFormat(Now(), 'yyyy_mm_dd')#_#LSTimeFormat(Now(),'hh_mm_ss')#.csv" 
                charset="utf-8"> 
                              
        </cfdefaultcase>
        
    </cfswitch>    
    
    <!--- Common data processing --->
    <!--- New logic so memory is not used up - avoid huge amounts of data are not loaded up into web server memeory --->
	<cftry>

		<!--- Add log - Compliance tracking--->
	    <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
	      	<cfinvokeargument name="moduleName" value="Report Survey">
	        <cfinvokeargument name="operator" value="Export report #inpBatchIdList# for #inprel1#">
		</cfinvoke>	


		<!--- Robustness check - dont keep looping over same data --->
        <cfif iDisplayLength GT 10000>
            <cfoutput>Export Error - Please don't kill the Web Server. You are trying to grab too much data at once. You requested iDisplayLength=#iDisplayLength# where the Max system iDisplayLength is 10,000</cfoutput>
        	<cfabort/>
        </cfif>
            
		<!--- Make sure we dont end up in infinite loops here - will crash web server if we do --->
		<cfset SecurityCheck_iDisplayStart = iDisplayStart>

		<!--- Get query specified --->
	    <cfinvoke component="session.sire.models.cfc.reports.#TargetCFC#" method="#TargetReportName#" returnvariable="RetVarinprel1">
	        <cfinvokeargument name="inpBatchIdList" value="#inpBatchIdList#">
	        <cfinvokeargument name="inpStart" value="#inpStart#">
	        <cfinvokeargument name="inpEnd" value="#inpEnd#">
	        <cfinvokeargument name="OutQueryResults" value="1">
	        <cfinvokeargument name="inpcustomdata1" value="#inpcustomdata1#"/>
	        <cfinvokeargument name="inpcustomdata2" value="#inpcustomdata2#"/>
	        <cfinvokeargument name="inpcustomdata3" value="#inpcustomdata3#"/>
	        <cfinvokeargument name="inpcustomdata4" value="#inpcustomdata4#"/>
	        <cfinvokeargument name="inpcustomdata5" value="#inpcustomdata5#"/>
	        <cfinvokeargument name="iDisplayLength" value="#iDisplayLength#"/>
	        <cfinvokeargument name="iDisplayStart" value="#iDisplayStart#"/>
	    </cfinvoke>
		
		<cfinvoke method="QueryToCSV" component="session.sire.models.cfc.reports" returnvariable="RetVarQueryToCSV">
	        <cfinvokeargument name="Query" value="#RetVarinprel1.QUERYRES#">
	        <!--- Only show header data on first loop --->
	       	<cfinvokeargument name="CreateHeaderRow" value="true">
	        <cfinvokeargument name="Delimiter" value=",">
	    </cfinvoke>

		<cfoutput>#RetVarQueryToCSV#</cfoutput> 
		<cfflush/> 
		
		<!--- Make sure next loop starts at new position --->
		<cfset iDisplayStart = iDisplayStart + iDisplayLength />  
						
		<cfset SecurityCheck = 0>  
		<cfset MaxLoops = 100000>
				    			  			    
        <cfloop condition = "iDisplayStart LESS THAN RetVarinprel1.iTotalDisplayRecords" >
            
            <!--- Robustness check - dont keep looping over same data --->
            <cfif SecurityCheck_iDisplayStart EQ iDisplayStart>
	            <cfoutput>Export Error - Same Data found in multiple loops - SecurityCheck_iDisplayStart = #SecurityCheck_iDisplayStart# iDisplayStart = #iDisplayStart# iDisplayLength = #iDisplayLength#</cfoutput>
	        	<cfbreak/>
            </cfif>
            
            <!--- Get query specified --->
		    <cfinvoke component="session.sire.models.cfc.reports.#TargetCFC#" method="#TargetReportName#" returnvariable="RetVarinprel1">
		        <cfinvokeargument name="inpBatchIdList" value="#inpBatchIdList#">
		        <cfinvokeargument name="inpStart" value="#inpStart#">
		        <cfinvokeargument name="inpEnd" value="#inpEnd#">
		        <cfinvokeargument name="OutQueryResults" value="1">
		        <cfinvokeargument name="inpcustomdata1" value="#inpcustomdata1#"/>
		        <cfinvokeargument name="inpcustomdata2" value="#inpcustomdata2#"/>
		        <cfinvokeargument name="inpcustomdata3" value="#inpcustomdata3#"/>
		        <cfinvokeargument name="inpcustomdata4" value="#inpcustomdata4#"/>
		        <cfinvokeargument name="inpcustomdata5" value="#inpcustomdata5#"/>
		        <cfinvokeargument name="iDisplayLength" value="#iDisplayLength#"/>
		        <cfinvokeargument name="iDisplayStart" value="#iDisplayStart#"/>
		    </cfinvoke>

            <cfinvoke method="QueryToCSV" component="session.sire.models.cfc.reports" returnvariable="RetVarQueryToCSV">
		        <cfinvokeargument name="Query" value="#RetVarinprel1.QUERYRES#">
		        <!--- Only show header data on first loop --->
		       	<cfinvokeargument name="CreateHeaderRow" value="false">
		        <cfinvokeargument name="Delimiter" value=",">
			</cfinvoke>

			<cfoutput>#RetVarQueryToCSV#</cfoutput>
			<cfflush/>
			
			<!--- Make sure next loop starts at new position --->
			<cfset SecurityCheck_iDisplayStart = iDisplayStart>
			<cfset iDisplayStart = iDisplayStart + iDisplayLength />
			
			<cfset SecurityCheck = SecurityCheck + 1>
			
			<!--- Sanity check to make sure we never end up in endless loop by accident - either need bigger loops or dont use this export tool --->
			<cfif SecurityCheck GT MaxLoops>  
				<cfoutput>Export Error - You have exceeded a reasonable number of loops - try increasing your iDisplayLength size</cfoutput>	
				<cfbreak/>					
			</cfif>				
				                
        </cfloop>                                    
                               
    <cfcatch type="any">
				  
	    <!---<cfdump var="#cfcatch#">
	    <cfabort>
	    --->
	</cfcatch>
	
	</cftry>        
    
    <!--- Sample query logic and expected return values can be seen here....  Can be used for datatables and write to file
	    
	    
	<cffunction name="cshistorytable" access="remote" output="false" returntype="any" hint="list all responses to a given control point for a given short code">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">
    <cfargument name="inpcustomdata1" TYPE="any" required="no" default="1" hint="The Control Point" />
    <cfargument name="inpcustomdata2" TYPE="any" required="no" default="10" hint="The Top (Limit) responses in descending order " />
	<cfargument name="inpcustomdata3" TYPE="any" required="yes" default="0" hint="The Batch Id" />
    <cfargument name="inpcustomdata4" TYPE="string" required="no" default="0" hint="The Short Code" />
    <cfargument name="OutQueryResults" required="no" type="string" default="0" hint="This allows query results to be output for QueryToCSV method.">
    
    <!--- Paging --->
    <!--- handle Paging, Filtering and Ordering a bit different than some of the other server side versions --->
    <cfargument name="iDisplayStart" required="no"  default="0">
    <cfargument name="iDisplayLength" required="no"  default="5">
    
	<!--- Filtering NOTE: this does not match the built-in DataTables filtering which does it
		  word by word on any field. It's possible to do here, but concerned about efficiency
		  on very large tables, and MySQL's regex functionality is very limited
    --->
    <!--- ColdFusion Specific Note: I am handling this in the actual query call, because i want the statement parameterized to avoid possible sql injection ---> 
    <cfargument name="sSearch" required="no"  default="">
    <cfargument name="iSortingCols" required="no"  default="0">
  
        <cfset var GetNumbersCount = '' />
        <cfset var qFiltered = '' />
        
        <cfset var DataOut = {}>
		<cfset DataOut.DATA = ArrayNew(1)>
        
        <!---ListEMSData is key of DataTable control--->
		<cfset DataOut["aaData"] = ArrayNew(1)>
        
        <cfset DataOut.RXRESULTCODE = 1 />
		<cfset DataOut.TYPE = "" />
        <cfset DataOut.MESSAGE = "" />
        <cfset DataOut.ERRMESSAGE = "" />
        
        <cfset DataOut["iTotalRecords"] = 0>
        <cfset DataOut["iTotalDisplayRecords"] = 0>
                 
     	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
	    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">
        
        
        <cftry>
             
            <!--- list of database columns which should be read and sent back to DataTables -- used to help enforce valid filter/ordering --->
            <cfset listColumns = "BatchId_bi,ContactString_vch,Response_vch,Created_dt" />
                  
                  
            <!---Get total  EMS for paginate --->
            <cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
                SELECT 
					 COUNT(*) AS TOTALCOUNT
				FROM 
		            simplexresults.ireresults
		        WHERE
		        	 userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.userid#">
		        AND
		        	shortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcustomdata4#">
		        AND
		        	Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
		        AND
		            Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
		        AND
		       		IREType_int= 2   
		        <cfif len(trim(sSearch))>    
		    		AND
						ContactString_vch like <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#sSearch#%">
	     		</cfif>
	        </cfquery>
            
            <cfif GetNumbersCount.TOTALCOUNT GT 0>
                <cfset DataOut["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
                <cfset DataOut["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>
            </cfif>
                                    
            <!--- Get data to display --->         
            <!--- Data set after filtering --->
            <cfquery name="qFiltered" datasource="#Session.DBSourceREAD#"> 	            
	            
	            SELECT 
					BatchId_bi,
					ContactString_vch,
					Response_vch,
					Created_dt 
		        FROM 
		            simplexresults.ireresults
		        WHERE
		        	 userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.userid#">
		        AND
		        	shortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpcustomdata4#">
		        AND
		        	Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
		        AND
		            Created_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
		        AND
		       		IREType_int= 2   
		        <cfif len(trim(sSearch))>    
		    		AND
						ContactString_vch like <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#sSearch#%">
	     		</cfif>
				<cfif iSortingCols gt 0>
	                ORDER BY <cfloop from="0" to="#iSortingCols-1#" index="thisS"><cfif thisS is not 0>, </cfif>#listGetAt(listColumns,(url["iSortCol_"&thisS]+1))# <cfif listFindNoCase("asc,desc",url["sSortDir_"&thisS]) gt 0>#url["sSortDir_"&thisS]#</cfif> </cfloop>
	            </cfif>
	            
	            <!--- 
		            By limiting results to paging here instead of once query returns, we can:
		            limit how much data is transfered from DB
		            the amount of memory this query results takes up
		        	Greatly speed up processing     
		        --->
		     		<cfif GetNumbersCount.TOTALCOUNT GT iDisplayLength>	
                        LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
                    <cfelse>
                        LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#"> 
                    </cfif>
                                   
            </cfquery>
           
            <cfif OutQueryResults EQ "1">
	            
	            <cfset DataOut.RXRESULTCODE = 1 />
				<cfset DataOut.QUERYRES = qFiltered />
				<cfset DataOut.TYPE = "" />
		        <cfset DataOut.MESSAGE = "" />
		        <cfset DataOut.ERRMESSAGE = "" />
	            
                <cfreturn DataOut />  
            </cfif>
                   
            <!---
                Output
             --->
             
            <cfloop query="qFiltered">	
                                    
                <cfset tempItem = [				
                    '#qFiltered.BatchId_bi#',
                    '#qFiltered.ContactString_vch#',
                    '#qFiltered.Response_vch#',
                    '#qFiltered.Created_dt#'
                ]>		
                <cfset ArrayAppend(DataOut["aaData"],tempItem)>
            </cfloop>
            
             <!--- Append min to 5 - add blank rows --->
            <cfloop from="#qFiltered.RecordCount#" to="4" step="1" index="LoopIndex">	
                                
                <cfset tempItem = [			
                    ' ',
                    ' ',
                    ' ',
                    ' '		
                ]>		
                <cfset ArrayAppend(DataOut["aaData"],tempItem)>
            </cfloop>
                    
  		<cfcatch type="Any" >
			<cfset DataOut.RXRESULTCODE = -1 />
		    <cfset DataOut.TYPE = "#cfcatch.TYPE#" />
		    <cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset DataOut.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset DataOut["iTotalRecords"] = 0>
			<cfset DataOut["iTotalDisplayRecords"] = 0>
			<cfset DataOut["aaData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn serializeJSON(DataOut)>
        
		</cffunction>	    
	    
    --->
    
    
    
    
    