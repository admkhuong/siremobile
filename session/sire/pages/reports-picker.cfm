<cfset rootUrl = ""/>
<cfset SessionPath = "session"/>
<cfset PublicPath = "public"/>

<!---check permission--->
<cfinclude template="/#sessionPath#/administration/constants/userConstants.cfm">

<!--- Only show certain Reports to Admins --->
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission"><cfinvokeargument name="inpSkipRedirect" value="1"></cfinvoke>



<!--- Used to specify default Dashboard Object Sizes --->
<cfset CounterTinyClassString = "DashObj DashLeft DashBorder DashSize_Height_Tiny DashSize_Width_2 col-xs-12 col-sm-12 col-md-4" />
<cfset CounterSmallClassString = "DashObj DashLeft DashBorder DashSize_Height_Small DashSize_Width_2 col-xs-12 col-sm-12 col-md-4" />
<cfset ListMediumClassString = "DashObj DashLeft DashBorder DashSize_Height_Small DashSize_Width_3 col-xs-12 col-sm-12 col-md-6" />
<cfset ChartMediumClassString = "DashObj DashLeft DashBorder DashSize_Height_Small DashSize_Width_3 col-xs-12 col-sm-12 col-md-6" />




<script type="text/javascript">
	
	$(document).ready(function()
	{
		LoadDashboardTemplateList();
    					
	});
	
	function LoadDashboardTemplateList()
	{			
	
		$('#DashboardTemplates').empty();
		
		$('#DashboardTemplates').html('<li style="list-style-type: none; margin-bottom:1em;">&nbsp;<span id="TemplateTrash" style="float:right; margin: 2px 17px 2px 2px;" title="Drag User Defined Templates Here to Remove Them."><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/icons/trash-icon_24x24.png" alt="Drag User Defined Templates Here to Remove Them." /></span></li>');
	
		<!--- Update DB first --->	
		$.ajax({
			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=ReadTemplateList&returnformat=json&queryformat=column',   
			dataType: 'json',
			async:true,
			data:  { 
				
			},					  
			success:function(data)
			{				
				<!--- Update display--->
				<!--- Update display--->
				if(parseInt(data.RXRESULTCODE) == 1)
				{																				
					for(i=0; i< data.TEMPLATELIST.length; i++)
					{											
						var $NewTemplate;
					
						if(parseInt(data.TEMPLATELIST[i][2]) > 0 )	
						{
							$NewTemplate =  $('<li><a href="##" class="menuItem" data-dismiss="modal" rel1="TEMPLATE" rel2="TEMPLATE" rel4="' + data.TEMPLATELIST[i][0] + '" rel5="' + data.TEMPLATELIST[i][2] + '"  >' +   data.TEMPLATELIST[i][1] + '</a></li>').appendTo("#DashboardTemplates");
						}
						else
						{
							$NewTemplate =  $('<li><a href="##" class="menuItem" data-dismiss="modal" rel1="TEMPLATE" rel2="TEMPLATE" rel4="' + data.TEMPLATELIST[i][0] + '" rel5="' + data.TEMPLATELIST[i][2] + '"  >' +  '(sys)&nbsp;&nbsp;' +  data.TEMPLATELIST[i][1] + '</a></li>').appendTo("#DashboardTemplates");
						}
							
						$NewTemplate.children().draggable({
								revert: "invalid",
								containment: "",
								opacity: 0.7,
								helper: "clone",
								cursor: "move",
								start: function(event, ui){
									$('.droppable').css({
										'border': 'solid 2px red'
									})
									
								},
								stop: function(event, ui){
									$('.droppable').css({
										'border': 'solid 1px #999999'
									})
								}
							});
							
						
					}
					
				}
				
				
				
				$("#TemplateTrash").droppable(
				{<!--- Droppable config --->
				
					<!--- Visual feedback cues --->	
					over: function(event, ui)
					{
						$(this).css({
							'border': 'solid 2px #15436C',
							'box-shadow': '0 0px 10px #777',
							'-webkit-box-shadow': '0 0px 10px #777',
							'-moz-box-shadow': '0 0px 10px #777',
							'margin': '0px 15px 0px 0px'
						});
					},
					<!--- Visual feedback cues --->
					out: function(event, ui)
					{
						$(this).css({
							'border': 'none',
							'box-shadow': 'none',
							'-webkit-box-shadow': 'none',
							'-moz-box-shadow': 'none',
							'margin': '2px 17px 2px 2px'
						});
					},
					<!--- Process when somthing is dropped here --->
					drop: function(event, ui)
					{<!--- drop event --->
						
						$(this).css({
							'border': 'none',
							'box-shadow': 'none',
							'-webkit-box-shadow': 'none',
							'-moz-box-shadow': 'none',
							'margin': '2px 17px 2px 2px'
						});
						
						if(ui.draggable.attr('rel2') == "TEMPLATE")
						{	
							
							if(ui.draggable.attr('rel5') == 0)
							{								
								bootbox.alert("You can remove user defined templates by dragging them to the trash can." + "\n" + "System Templates are not removeable!", function(result) { } );	
								return false;
							}
											
							RemoveUserTemplate(ui.draggable.attr('rel4'), ui.draggable);
														
							return;	
						}
					}
										
				});
				
			
				<!--- DO this after all templates are loaded --->
				$(".menuItem" ).click(function() {
	 
					<!---console.log($(this).attr("rel1"));--->
					AddChartItem($(this));
					
					<!---$("div.droppableContentDashboard").trigger("dropthis", [{},{draggable : $(this)}]);--->
					
					
				});	
				
										
			}, 		
			error:function(jqXHR, textStatus)
			{				
				<!---
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				bootbox.alert("Connection Failure!" + textStatus + "\n" + "Read Dashboard Request failed!", function(result) { } );	
				--->
			} 		
		
		});			
	}
	
	
	function RemoveUserTemplate(inpId, inpObj)
	{		
		<!--- Update DB first --->	
		$.ajax({
			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=RemoveTemplate&returnformat=json&queryformat=column',   
			dataType: 'json',
			async:false,
			data:  
			{ 
				inpId: parseInt(inpId)
			},					  
			success:function(data)
			{				
				<!--- Update display--->
				if(parseInt(data.RXRESULTCODE) == 1)
				{
					inpObj.remove();
				}
										
			}, 		
			error:function(jqXHR, textStatus)
			{	
				bootbox.alert("Connection Failure!" + textStatus + "\n" + "Template Request failed!", function(result) { } );	
			} 		
		
		});			
	}
	
	
</script>
    

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Select the report you wish to add to the dashboard</h4>     
</div>


<div class="no-print">
       
        <cfoutput>    
           	          
                <ul id="ReportMenu">
                
               
                                                
                                           
                     
                   
                        <h3><a href="##">Counters</a></h3>
                    
                        <ul>			
                        
                        	<li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="sms.Display_c_smsmt" rel2="FORM" rel3="#CounterTinyClassString#">
                                    SMS MT Counter
                                </a>
                            </li>
                                
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="sms.Display_c_smsmo" rel2="FORM" rel3="#CounterTinyClassString#" title="MO's that are bound for this users campaigns.">
                                    SMS MO Counter
                                </a>
                            </li>
                            
                        </ul>                        
                        
                        <h3><a href="##">Location Graphs</a></h3>
                        
                    	<ul>
	                    		     
                             <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="sms.MTCountByState" rel2="CHART" title="SMS MT Count by State">
                                    SMS MT Counts By State
                                </a>
                            </li>
                           
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="sms.MTCountByZip" rel2="CHART" title="SMS MT Count by Top 125 Zip codes">
                                    SMS MT Counts By Zip Code
                                </a>
                            </li>
                           
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="sms.MTCountByMSA" rel2="CHART" title="SMS MT Count by The Metropolitan Statistical Area (MSA) code assigned by the Office of Management and Budget. Use this code as an index key in the MSA file.">
                                    SMS MT Counts By MSA
                                </a>
                            </li>
                       
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="sms.MTCountByPMSA" rel2="CHART" title="SMS MT Count by The Primary Metropolitan Statistical Area (PMSA) code assigned by the Office of Management and Budget. Use this code as an index key in the MSA file.">
                                    SMS MT Counts By PMSA
                                </a>
                            </li>
                           
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="sms.MTCountByFIPS" rel2="CHART" title="SMS MT Count by The FIPS (Federal Information Processing Standard) number assigned to each county in the U.S. by the Census Bureau. The first two digits are the state number, and the last three digits are the county number.">
                                    SMS MT Counts By FIPS
                                </a>
                            </li>


                        </ul>                        
                        
						<h3><a href="##">Surveys</a></h3>
                                            
                        <ul>
	                        
	                        <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="cpresponsesregex.Form_cpresponses" rel2="FORM" title="MO IC RegEx Mathed Response Counts by Control Point" inpcustomdata1="0">
                                    Chart Reg Exp Matched Responses for Selected CP
                                </a>
                            </li>
                            
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="cpresponses.Form_cpresponses" rel2="FORM" title="MO IC Response Counts by Control Point" inpcustomdata1="0">
                                    Chart Raw Responses for Selected CP
                                </a>
                            </li>
                            
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1= "contactstringdetails.Form_CSdetails" rel2="FORM" >
                                  Contact String Responses
                                </a>
                            </li>
                            
                        </ul>
                    
						<h3><a href="##">Contacts and Subscribers</a></h3>
                                            
                        <ul>
                        	 <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="cdrs.Form_cdrs" rel2="FORM" title="Activity on Short Code for this User Account and Contact String">
                                    Contact Record Details (CDRs)
                                </a>
                            </li>

							<li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="stop.Form_stop" rel2="FORM" title="Opt Out Activity on Short Code for this User Account and Contact String">
                                    Opt Out
                                </a>
                            </li>
                            
                        </ul>
                                       
						<cfif RetVarGetAdminPermission.ISADMINOK EQ  1>
							<h3><a href="##">Billing Details</a></h3>
                                                
	                        <ul>
	                        	 <li>
                        	        <a href="##" class="menuItem" data-dismiss="modal" rel1="billing-reports-3.Form_BillingInvoice3" rel2="FORM" title="Generate an Invoice for the specified User Id and Date Range">
	                                    Billing Invoice
	                                </a>
	                            </li>
	                            
	                            <li>
	                                <a href="##" class="menuItem" data-dismiss="modal" rel1="billablecdrs.Form_cdrs" rel2="FORM" title="Activity on Short Code for this User Account and Contact String">
	                                    Billable - Contact Record Details (CDRs)
	                                </a>
	                            </li>
								
<!---
								<br/>.
								<br/>.
								<br/>.
                            
								<li>
                        	        <a href="##" class="menuItem" data-dismiss="modal" rel1="billing-reports.Form_BillingInvoice" rel2="FORM" title="Generate an Invoice for the specified User Id and Date Range">
	                                    Billing Invoice Old
	                                </a>
	                            </li>
	                            
	                            <br/>.
								<br/>.
								<br/>.
                            
								<li>
                        	        <a href="##" class="menuItem" data-dismiss="modal" rel1="billing-reports-2.Form_BillingInvoice2" rel2="FORM" title="Generate an Invoice for the specified User Id and Date Range">
	                                    Billing Invoice Old
	                                </a>
	                            </li>
--->


	                        </ul>
                        </cfif>   
                                                     
<!---
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="SMSSurveyComboResults" rel2="CHART" rel3="#ChartMediumClassString#" title="Status of Surveys Started in Date Range Specified">
                                    SMS Survey Status
                                </a>
                            </li>
                            
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_c_smsSurvey" rel2="FORM" rel3="#CounterTinyClassString#" title="MO's that are bound for this users campaigns.">
                                    SMS Surveys Requested
                                </a>
                            </li>
                            
                           
	                            <li>
	                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_optin" rel2="FORM" title="number of users optin campaigns.">
	                                    SMS Optin
	                                </a>
	                            </li>
                           
                            
                           	                            <li>
	                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_optout" rel2="FORM" title="number of users optout campaigns.">
	                                    SMS Optout
	                                </a>
	                            </li>
                                                      
                         	                            <li>
	                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_activeusers" rel2="FORM" title="number of active users.">
	                                    Active Users
	                                </a>
	                            </li>
                           
	                            <li>
	                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_dropout" rel2="FORM" title="number of drop out users.">
	                                    Campaign Dropouts 
	                                </a>
	                            </li>
                                                       
                            	                            <li>
	                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_CSinformation" rel2="FORM" title="Contact strong response">
	                                    User Information 
	                                </a>
	                            </li>
	                            
	                            <li>
	                                <a href="##" class="menuItem" data-dismiss="modal" rel1="legacy_Form_CSinformation" rel2="FORM" title="Contact strong response">
	                                    User Information (Legacy)
	                                </a>
	                            </li>
                           
                            
                           
	                            <li>
	                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_cshistory" rel2="FORM" title="History of Contact String">
	                                    User History
	                                </a>
	                            </li>
                           
                            
                          
	                            <li>
	                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_cpcompare" rel2="FORM" title="Compare control points in keyword">
	                                    Compare question responses 
	                                </a>
	                            </li>
                                                        
                        	<li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_chartMO" rel2="FORM" title="Time chart MO">
                                    MO Charts 
                                </a>
                            </li>
                            
                           
                                <li>
                                    <a href="##" class="menuItem" data-dismiss="modal" rel1="SMSSessionStates" rel2="CHART">
                                        SMS Session States
                                    </a>
                                </li>
--->
                                                  <!---    <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel2="CHART">
                                    SMS Delivered
                                </a>
                            </li>
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel2="CHART">
                                    SMS Undelivered
                                </a>
                            </li>--->
                            
                         <!---   <cfif permissionObject.havePermission(Reporting_ActiveICBy_Keyword_Title).havePermission>
                             <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="ICByKeyword" rel2="CHART" title="Active Interactive Campaign (IC) by Keyword">
                                    Active IC By Keyword
                                </a>
                            </li>
                            </cfif>
						
                    
                             <cfif permissionObject.havePermission(Reporting_ActiveIC_Details_Title).havePermission>
                             <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Display_ICByKeywordDetails" rel2="TABLE" title="Active Interactive Campaign (IC) Details">
                                    Active IC Details
                                </a>
                            </li>
                            </cfif>
                    
                            <cfif permissionObject.havePermission(Reporting_ActiveIC_Counts_Title).havePermission>
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="ICActivated" rel2="CHART" title="Active Interactive Campaign (IC) for Date Range">
                                    Actived IC Counts
                                </a>
                            </li>
                            </cfif>
                            
							--->
                            
<!---
                            
                            
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_ContactStringMTStatusUpdates" rel2="FORM" title="MT Delivery Results for Contact String for Date Range">
                                    MT Delivery Results
                                </a>
                            </li>
                                               		
                    		<li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1= "Form_campaigndetails" rel2="FORM" >
                                  Keyword Response Details
                                </a>
                            </li>
                            
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1= "Form_CSdetails" rel2="FORM" >
                                  Contact String Details
                                </a>
                            </li>
                            
                          <!---  <cfif permissionObject.havePermission(Reporting_MO_Responses_Title).havePermission>
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_moresponses" rel2="FORM" title="MO Keyword Response Counts by Short Code">
                                    MO Responses
                                </a>
                            </li>
                            </cfif>
							--->
                            
                                                       <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_cpresponses" rel2="FORM" title="MO IC Response Counts by Control Point">
                                    CP Responses
                                </a>
                            </li>
                                                     
                            
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="display_smsfinaldisposition" rel2="TABLE" title="All user history for a batch">
                                    SMS Final Disposition
                                </a>
                            </li>
                            
                            
--->
                        <!---  These are commented out until they are working within format and indexed properly   
                            <cfif permissionObject.havePermission(Reporting_MO_Responses_Title).havePermission>
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="CompareKeywords" rel2="FORM" title="User's Response">
                                    Compare Keywords
                                </a>
                            </li>
                            </cfif>
                            <cfif permissionObject.havePermission(Reporting_MO_Responses_Title).havePermission>
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_getRegUsers" rel2="FORM" title="User's Response">
                                    Registered Users
                                </a>
                            </li>
                            </cfif>
                            
                            <cfif permissionObject.havePermission(Reporting_MO_Responses_Title).havePermission>
                            <li>
                                <a href="##" class="menuItem" data-dismiss="modal" rel1="Form_filteredresponses" rel2="FORM" title="Survey Results Filtered">
                                    Survey Status on Campaign Filter
                                </a>
                            </li>
                            </cfif>
							
						--->
                                                    
                                      
                                    
                    <!--- Templates --->
                    
                        <h3><a href="##">Dashboard Templates</a></h3>
                   		
                        
                        <ul id="DashboardTemplates">
                       
                       		<li><span id="TemplateTrash" style="float:right; margin: 2px 17px 2px 2px;"><img src="#rootUrl#/#publicPath#/images/icons/trash-icon_24x24.png" alt="Drag User Defined Templates Here to Remove Them." /></a></li>
                                                                                    
                        </ul>
                    
                    
                                       
                    
                   <!--- <cfif permissionObject.havePermission(Reporting_Test_Title).havePermission>                     
                         <li>
                            <h3><a href="##">Special</a></h3>
                        
                            <ul>
                            
                         
                                <li>
                                    <a href="##" class="menuItem" data-dismiss="modal" rel1="SMSKeywordSummary" rel2="CHART" >
                                        TestNew
                                    </a>
                                </li>
                            </ul>
                            
                        </li>        
                    </cfif>--->
                    
                </ul>
           
        </cfoutput>
         
             
        </div>     
        
        
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>           
        </div>
                
                