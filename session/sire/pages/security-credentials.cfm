<cfset variables.menuToUse = 2>
<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addJs("/public/js/datatables/js/jquery.dataTables.min.js")
        .addJs("/public/sire/js/jquery.validationEngine.Functions.js")
        .addJs("/session/sire/assets/pages/scripts/security_credential.js");

</cfscript>

<style type="text/css">
    ul.secure-password-requirement {
        padding-left: 18px;
    }
    ul.secure-password-requirement li{
        color: gray;
    }
    table p {
        margin: 5px 0;
    }

    table a.btn-action {
        background-color: #5c5c5c;
        color: #FFFFFF;
        padding: 0px 5px ;
        text-transform: uppercase;
    }
</style>

<!--- get list question --->
<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="getSecurityQuestionList" returnvariable="questionList"></cfinvoke>

<!--- get user info --->
<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke>
<cfoutput>
    <script type="text/javascript">
        var UserIDLogin = "#userInfo.USERID#";
    </script>
</cfoutput>

<cfset check_user_security_question = false>
<cfset hidden_char = "">

<!--- get List Question of User --->
<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserListQuestion" returnvariable="userListQuestion">
    <cfinvokeargument name="detail_question" value=0>
</cfinvoke>


<cfif userListQuestion.RESULT EQ 'SUCCESS' >
    <cfset check_user_security_question = true>
    <cfset hidden_char = "******">
</cfif>
<cfset arrUserQuestion = userListQuestion.LISTQUESTION>
<cfif !isArray(arrUserQuestion)>
    <cfset arrUserQuestion =array()>
    <cfset arrUserQuestion[1] ={QUESTIONID:"",ANSWER:"",ANSWERID:""}>
    <cfset arrUserQuestion[2] ={QUESTIONID:"",ANSWER:"",ANSWERID:""}>
    <cfset arrUserQuestion[3] ={QUESTIONID:"",ANSWER:"",ANSWERID:""}>
</cfif>

<!--- <cfdump var="#arrUserQuestion#" abort="true"> --->

<cfinvoke component="session.cfc.administrator.credential" method="countCredentials" returnvariable="countCredentials">

<cfset collectionListQuestions = createObject("component", "session.sire.models.cfc.myaccountinfo").getSecurityQuestionList().DATA />

<cfset userInfo = createObject("component", "session.sire.models.cfc.myaccountinfo").GetUserListQuestion() />
<cfset listUserAQ = userInfo.LISTQUESTION />

<!--- <cfdump var="#userInfo#" abort="true"> --->

<div class="row">
    <div class="col-md-6 col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-body form">
                <form role="form" name="change-password" class="form-gd">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="clearfix">
                                    <h3 class="form-heading">Change password</h3>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label>Email </label>
                                    <input type="text" name="email" value="<cfoutput>#SESSION.EmailAddress#</cfoutput>" readonly="readonly" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label>Old Password <span>*</span></label>
                                    <input type="password" name="old-password" class="form-control validate[required,funcCall[isValidPassword]]" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label>New Password <span>*</span></label>
                                    <input type="password" name="new-password" id="new-password" class="form-control validate[required,funcCall[isValidPassword]]" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label>Confirm New Password <span>*</span></label>
                                    <input type="password" name="renew-password" class="form-control validate[required,equals[new-password],funcCall[isValidPassword]]" placeholder="">
                                </div>
                                <br>
                                <div class="form-actions">
                                    <button type="submit" class="btn green-gd">Save</button>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div>
                                    <p><strong>Secure Password Requirements</strong></p>
                                </div>
                                <ul class="secure-password-requirement">
                                    <li>
                                        <p><em>must be at least 8 character</em></p>
                                    </li>
                                    <li>
                                        <p><em>must have at least 1 number</em></p>
                                    </li>
                                    <li>
                                        <p><em>must have at least 1 uppercase letter</em></p>
                                    </li>
                                    <li>
                                        <p><em>must have at least 1 lowercase letter</em></p>
                                    </li>
                                    <li>
                                        <p><em>must have at least 1 special character</em></p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="portlet light bordered clearfix">
           <div class="portlet-body form">
                    <div class="form-body">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="clearfix">
                                    <h3 class="form-heading"><cfif userInfo.RESULT == "SUCCESS"> Update <cfelse> Create </cfif> Security Questions</h3>
                                </div>
                            </div>
                        </div>
                        <cfif check_user_security_question>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="clearfix">
                                        <p><strong  style="color: rgb(92,92,92); font-family:'Source Sans Pro', sans-serif;">
                                            Select three security questions below. These questions will help us verify your identity should you forget your password.
                                        </strong></p>
                                    </div>
                                    <cfoutput>
                                    <form name="frm-security-questions" class="form-gd">
                                        <cfset qstNumber = 0 />
                                        <cfloop from="1" to="#collectionListQuestions.len()#" index="key">
                                            <cfset qstNumber++  />
                                            <div class="form-group">
                                                <label>Question #key# <span>*</span></label>
                                                <select data-number="qst-#qstNumber#" name="question_id[]" class="form-control">
                                                    <cfloop array="#collectionListQuestions["list_"&key]#" index="qstItem">
                                                        <option <cfif qstItem.ID EQ listUserAQ[qstNumber]["QUESTIONID"]>selected</cfif> value="#qstItem.ID#">#qstItem.QUESTION#</option>
                                                    </cfloop>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Your Answer <span>*</span></label>
                                                <input type="text" class="form-control validate[required]" data-number="anw-#qstNumber#" id="inp_simon_#qstNumber#" name="answer_txt[]" placeholder="" value="#hidden_char#">
                                                <input type="hidden" class="form-control" name="hidden_answer_id[]" value="#listUserAQ[qstNumber]['ANSWERID']#">
                                            </div>
                                        </cfloop>
                                        <br>
                                        <div class="form-actions">
                                            <button type="submit" class="btn green-gd">Save</button>
                                        </div>
                                    </form>
                                    </cfoutput>
                                </div>
                            </div>
                        <cfelse>
                            <!--- ADD Security FORM --->
                            <form class="form-horizontal form-gd" method="post" action="##" name="add_security_question_form" id="add_security_question_form" autocomplete="off">
                                <div class="clearfix">
                                    <p><strong  style="color: rgb(92,92,92); font-family:'Source Sans Pro', sans-serif;">Please define 3 security questions and answers, in the event you lost your device or password.</strong></p>
                                </div>
                                <cfset num =1>
                                <cfloop index="userQuestion" array="#arrUserQuestion#">
                                <cfset selected = ''>
                                <cfset questionListIndex = 'list_'&num>
                                    <div class="form-group col-lg-10">
                                        <label>Question <cfoutput>#num#</cfoutput></label>
                                        <div class="">
                                            <select class="form-control" name="question_id[]" id="select_question_<cfoutput>#num#</cfoutput>">
                                                <cfloop array="#questionList.DATA[questionListIndex]#" index="question_item">
                                                    <cfif userQuestion['QUESTIONID'] EQ question_item.ID >
                                                        <cfset selected = 'selected'>           
                                                    <cfelse>    
                                                        <cfset selected = ''>       
                                                    </cfif>
                                                    <option value="<cfoutput>#question_item.ID#</cfoutput>" <cfoutput>#selected#</cfoutput> >
                                                        <cfoutput>#question_item.QUESTION#</cfoutput>
                                                    </option>
                                                </cfloop>
                                            </select> 
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-10">
                                        <label>Your Answer <span class="text-danger">*</span></label>
                                        <div class="">
                                            <input type="text" class="form-control validate[required] input_answer" id="input_answer_<cfoutput>#num#</cfoutput>" maxlength="255" name="answer_txt[]" value="<cfoutput>#hidden_char#</cfoutput>" data-prompt-position="topLeft:100">
                                        </div>
                                    </div>
                                    <cfset num++>
                                </cfloop>
                                <div class="form-group btn-group-answer-security-question">
                                    <div class="col-lg-10">
                                        <button type="submit" class="btn green-gd" id="btn_add_security_question text-uppercase">Create</button>
                                    </div>
                                </div>  
                            </form>
                        </cfif>


                   </div>
            </div>

        </div>
    </div>
 </div>

 <div class="row">
    <div class="col-xs-12">
        <div class="portlet light bordered">
            <div class="portlet-body">
                <div class="form-gd">
                    <div class="clearfix">
                        <h3 class="form-heading">Security Credentials</h3>
                    </div>
                    <div class="table-responsive re-table">          
                        <table id="tblListCredentials" class="table-responsive table-striped dataTables_wrapper dataTable">
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <button class="btn green-gd pull-right btn-new-access-key mt-20">Create a new access key</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="md-contact-support" tabindex="-1" role="dialog" aria-hidden="true">	
    <div class="modal-dialog set-width-350">
        <div class="modal-content">                    
            <div class="modal-body"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <div class="form-group  ">
                    <center>
                        <h4 class="portlet-heading2">Customer Support</h4>                                                
                        <div class="md-import-row-span"><span class="span-title">EmailL:</span> <a href="mailto:hello@siremobile.com?Subject=SupportImportTools" target="_top">hello@siremobile.com</a></div>
                        <div class="md-import-row-span"><span class="span-title">Phone:</span> (888) 747-4411</div>
                        <div class="md-import-row-span"><span class="span-title">Chat with us: </span> 9AM - 6PM Pacific </div>                         
                    </center>
                </div>
            </div>        
        </div>
    </div>
</div>  

<cfparam name="variables._title" default="Security Credentials - Sire">
<cfinclude template="../views/layouts/master.cfm">

