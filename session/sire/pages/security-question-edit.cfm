<cfinclude template="/session/sire/configs/paths.cfm">

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js">
</cfinvoke>
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/ajaxupload.js">
</cfinvoke>
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/my-account.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/setting.css">
</cfinvoke>

<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="getSecurityQuestionList" returnvariable="questionList"></cfinvoke>

<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke>

<cfset check_user_security_question = false>
<cfset hidden_char = "">

<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserListQuestion" returnvariable="userListQuestion">
	<cfinvokeargument name="detail_question" value=0>
</cfinvoke>

<cfif userListQuestion.RESULT EQ 'SUCCESS' >
	<cfset check_user_security_question = true>
	<cfset hidden_char = "******">
</cfif>
<cfset arrUserQuestion = userListQuestion.LISTQUESTION>
<cfif !isArray(arrUserQuestion)>
	<cfset arrUserQuestion =array()>
	<cfset arrUserQuestion[1] ={QUESTIONID:"",ANSWER:"",ANSWERID:""}>
	<cfset arrUserQuestion[2] ={QUESTIONID:"",ANSWER:"",ANSWERID:""}>
	<cfset arrUserQuestion[3] ={QUESTIONID:"",ANSWER:"",ANSWERID:""}>
</cfif>
	
<main class="container-fluid page">
	<section class="row bg-white">
		<div class="content-header">
			<div class="row">
                <div class="col-sm-6 col-md-6 col-lg-6 content-title">
                	<p class="title">Welcome <cfoutput>#Session.FULLNAME#</cfoutput></p>
                </div>
            </div>
		</div>
		<hr class="hrt0">
		<div class="content-body">
			<div class="container-fluid">
				
				<div class="row">
					<div class="col-xs-7 col-sm-6 col-lg-4">
						<cfif check_user_security_question>
							<h2><strong>Edit Security Questions</strong></h2>
						<cfelse>		
							<h2><strong>Create Security Questions</strong></h2>
						</cfif>
						
					</div>
					
					<form action="##" method="post" id="enable_security_question_form" autocomplete="off"> 
					<!---	
					<div class="col-xs-5 col-sm-4 col-lg-2 pull-right checkbox enable-security-checkbox">
					    <label>
					     	<input type="checkbox" name="SecurityQuestionEnabled"  id="SecurityQuestionEnabled" <cfif userInfo.SQUESTIONENABLE EQ 1><cfoutput>checked</cfoutput></cfif> ><Strong> Enable the security question </strong>
						</label>
					</div>
					--->
					</form>
				</div>	
				<p> We value your security. Please define 3 security questions and your answers in the event you lose your device or password </p>


				<cfif questionList.RESULT EQ 'SUCCESS'>
					<cfif check_user_security_question>
						<!-- EDIT FORM --->
						<form class="form-horizontal" method="post" action="##" name="edit_security_question_form" id="edit_security_question_form" autocomplete="off">
							<cfset num =1>
							<cfloop index="userQuestion" array="#arrUserQuestion#">
								<cfset selected = ''>
								<cfset questionListIndex = 'list_'&num>
								<!--- QUESTION 1 --->
								<div class="form-group">
								    <label class="col-xs-4 col-sm-3 col-lg-1 control-label">Question <cfoutput>#num#</cfoutput></label>
								    <div class="col-xs-8 col-sm-9 col-lg-5">
								    	<select class="form-control select_question select" id="select_question_<cfoutput>#num#</cfoutput>" name="question_id[]" data-selected="<cfoutput>#userQuestion['QUESTIONID']#</cfoutput>" 
								    	data-input-answer="input_answer_<cfoutput>#num#</cfoutput>">
								    		<cfloop array="#questionList.DATA[questionListIndex]#" index="question_item">
								    			<cfif userQuestion['QUESTIONID'] EQ question_item.ID >
								    				<cfset selected = 'selected'>			
								    			<cfelse>	
								    				<cfset selected = ''>		
								    			</cfif>
									    		<option value="<cfoutput>#question_item.ID#</cfoutput>" <cfoutput>#selected#</cfoutput> >
									    			<cfoutput>#question_item.QUESTION#</cfoutput>
									    		</option>
									    	</cfloop>
								     	</select> 
								    </div>
								</div>
								<div class="form-group">
								    <label for="input" class="col-xs-4 col-sm-3 col-lg-1 control-label">Your Answer</label>
								    <div class="col-xs-8 col-sm-9 col-lg-5">
								     	<input type="text" class="form-control input_answer" id="input_answer_<cfoutput>#num#</cfoutput>" maxlength="255" name="answer_txt[]" value="" data-prompt-position="topLeft:100">
								     	<input type="hidden" class="form-control" name="hidden_answer_id[]" value="<cfoutput>#userQuestion['ANSWERID']#</cfoutput>">
								    </div>
								</div>
								<cfset num++>
							</cfloop>
							<div class="form-group btn-group-answer-security-question">
			                	<div class="col-xs-offset-4 col-sm-offset-3 col-lg-offset-1 col-xs-8 col-sm-8 col-lg-7">
			                        <button class="btn btn-success btn-success-custom btn-custom_size" id="btn_add_security_question text-uppercase">Save Changes</button>
			                        <a href="/session/sire/pages/my-account" class="btn btn-primary btn-back-custom btn-custom_size" >Cancel</a>
			                    </div>
			                </div>  
						</form>
					<cfelse>
						<!-- ADD FORM --->
						<form class="form-horizontal" method="post" action="##" name="add_security_question_form" id="add_security_question_form" autocomplete="off">
							<cfset num =1>
							<cfloop index="userQuestion" array="#arrUserQuestion#">
							<cfset selected = ''>
							<cfset questionListIndex = 'list_'&num>
							<!--- QUESTION 1 --->
								<div class="form-group">
								    <label class="col-xs-4 col-sm-3 col-lg-1 control-label">Question <cfoutput>#num#</cfoutput></label>
								    <div class="col-xs-8 col-sm-9 col-lg-5">
								    	<select class="form-control" name="question_id[]" id="select_question_<cfoutput>#num#</cfoutput>">
								    		<cfloop array="#questionList.DATA[questionListIndex]#" index="question_item">
								    			<cfif userQuestion['QUESTIONID'] EQ question_item.ID >
								    				<cfset selected = 'selected'>			
								    			<cfelse>	
								    				<cfset selected = ''>		
								    			</cfif>
									    		<option value="<cfoutput>#question_item.ID#</cfoutput>" <cfoutput>#selected#</cfoutput> >
									    			<cfoutput>#question_item.QUESTION#</cfoutput>
									    		</option>
									    	</cfloop>
								     	</select> 
								    </div>
								</div>
								<div class="form-group">
								    <label for="input" class="col-xs-4 col-sm-3 col-lg-1 control-label">Your Answer <span class="text-danger">*</span></label>
								    <div class="col-xs-8 col-sm-9 col-lg-5">
								     	<input type="text" class="form-control validate[required] input_answer" id="input_answer_<cfoutput>#num#</cfoutput>" maxlength="255" name="answer_txt[]" value="<cfoutput>#hidden_char#</cfoutput>" data-prompt-position="topLeft:100">
								    </div>
								</div>
								<cfset num++>
							</cfloop>
							<div class="form-group btn-group-answer-security-question">
			                	<div class="col-xs-offset-4 col-sm-offset-3 col-lg-offset-1 col-xs-8 col-sm-8 col-lg-8">
			                        <button class="btn btn-success btn-success-custom btn-custom_size" id="btn_add_security_question text-uppercase">Create</button>
			                        <a href="/session/sire/pages/my-account" class="btn btn-primary btn-back-custom btn-custom_size" >Cancel</a>
			                    </div>
			                </div>  
						</form>
					</cfif>

				<cfelse>
					<p> No security question in system.</p>		
				</cfif>

			</div>	
		</div>
	</section>
</main>

<cfparam name="variables._title" default="Security Questions Manage - Sire">

<cfinclude template="../views/layouts/main.cfm">