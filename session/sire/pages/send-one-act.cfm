<!--- 
	Standalone send message bassed on current account credetials 
	
	

Return result message and original paramters back to send one page for resend 	
	
--->

<cfinclude template="/public/paths.cfm" >
<cfinclude template="/session/sire/configs/paths.cfm">
	

<cfparam name="inpContactString" default="" />
<cfparam name="inpBatchId" default="" />




<cfset Session.SendOneData = StructNew() />
<cfset Session.SendOneData.inpContactString = inpContactString />
<cfset Session.SendOneData.inpBatchId = inpBatchId />
<cfset Session.SendOneData.ResultMessage = "Unable to send single message at this time. Please try again later." />



<cftry>        
        
	<cfif Session.USERID EQ "" OR Session.USERID EQ "0" OR  Session.USERID LT 0>
		 <cfset Session.SendOneData.ResultMessage = "No currently logged in user data found. Failure to send message. You must be logged in to use this feature." />
    	<cfthrow MESSAGE="User Id is invalid." TYPE="Any" detail="" errorcode="-2">
    </cfif>
            
    
    <!--- Things that can go wrong  --->
    
    
    <!-- Check that Batch Id user wants to trigger is valid and owned by current user --->
    
    <!--- Get API credentials for current user --->
    <cfquery name="GetCredentials" datasource="#Session.DBSourceEBM#">
        SELECT 
   	    		AccessKey_vch,
				SecretKey_vch,
				Active_ti,
                SecretName_vch
		    FROM 
		    	simpleobjects.securitycredentials
			WHERE
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_integer" VALUE="#session.userId#">
			AND	
				Active_ti = 1
			LIMIT 1
	</cfquery>
	
	<cfif GetCredentials.RecordCount eq 0 >
		<cfset Session.SendOneData.ResultMessage = "No current security credentials found for currently logged in user. Failure to send message. You must set/activate your accounts security credentials first.<br/>Click here to set: <a href='security-credentials'>security-credentials</a>" />
    	<cfthrow MESSAGE="No Active Security Credentials Found!" TYPE="Any" detail="" errorcode="-2">
    </cfif>
    
    		
        
    <!--- Check if user has opted in already  --->        
    
    <!--- Check if user has enough credits --->        
 
	
	<!--- Log send single requests?  --->       
 
 
	 <cfhttp url="https://api.siremobile.com/ire/secure/triggerSMS" method="POST" result="returnStruct" >
		
		    <!--- By default EBM API will return json or XML --->
		    <cfhttpparam name="Accept" type="header" value="application/json" />    	
		    
	        <!--- If DebugAPI is greater than 0 and is specidfied then use Dev DB for testing--->
	        <!--- If in debug mode make sure you are using the correct credentials--->
	        <cfhttpparam type="formfield" name="DebugAPI" value="0" />
	       
	        <cfhttpparam type="formfield" name="inpBatchId" value="#inpBatchId#" />
	        <cfhttpparam type="formfield" name="inpContactString" value="#inpContactString#" />
	        <cfhttpparam type="formfield" name="inpUserName" value="#GetCredentials.AccessKey_vch#" />
	        <cfhttpparam type="formfield" name="inpPassword" value="#GetCredentials.SecretKey_vch#" />
	          
	</cfhttp>

<!---
	<cfdump var="#returnStruct#">  
	<cfabort>
--->
	
	<cfif returnStruct.status_code NEQ 200>
		<cfset Session.SendOneData.ResultMessage = "Unable to send single message at this time. Code: #returnStruct.status_code#" />
    	<cfthrow MESSAGE="Failed API Call: Status Code is not 200 Success!" TYPE="Any" detail="" errorcode="-2">
	</cfif>


	<cfif Len(TRIM(returnStruct.Filecontent)) EQ 0>		
		<cfset Session.SendOneData.ResultMessage = "Unable to send single message at this time. Code: -105" />
    	<cfthrow MESSAGE="Failed API Call: No JSON result" TYPE="Any" detail="" errorcode="-2">
	</cfif>	
		
	<cfset ResultsVar = deserializeJSON(returnStruct.Filecontent) />
	
	<cfif ResultsVar.SMSINIT EQ 0>
	    
	    <cfif ResultsVar.STATUSCODE EQ -5>
	    
		    <cfset Session.SendOneData.ResultMessage = "Unable to access Campaign Id #inpBatchId# from this account" />
			<cfthrow MESSAGE="Failed API Call: Unable to access Campaign Id #inpBatchId#!" TYPE="Any" detail="" errorcode="-2">

		<cfelseif ResultsVar.STATUSCODE EQ 1>
		
			<cfif ResultsVar.BLOCKEDBYDNC EQ 1>
				
				<cfset Session.SendOneData.ResultMessage = "Blocked by DNC request! Unable to send message to #inpContactString# at this time. This number has requested STOP to not receive any more text messages. If they want to get more messages from you then they need to double opt in from their mobile device to a campaign that you create for this purpose on your account." />
				<cfthrow MESSAGE="Failed API Call: Blocked by DNC #inpContactString#!" TYPE="Any" detail="" errorcode="-2">
		
			<cfelse>
		
				<cfset Session.SendOneData.ResultMessage = "Unable to send message to #inpContactString# at this time. This may be an invalid number or is unable to recieve text messages at this time." />
				<cfthrow MESSAGE="Failed API Call: Unable to send to this number #inpContactString#!" TYPE="Any" detail="" errorcode="-2">
			
			</cfif>
			
	    </cfif>
	
	<cfelse>
	
		<cfset Session.SendOneData.ResultMessage = "Message Sent OK" />	  
	    	  
	</cfif>	  
	
	
<cfcatch>
<!---
    <cfdump var="#cfcatch#">  
	<cfabort>
--->
         
</cfcatch>

</cftry>            
            
<!---        <cfdump var="#Session.SendOneData#" />    --->
<cflocation url="send-one" addToken="no" /> 



           
            
            
            












