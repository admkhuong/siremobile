<cfinclude template="/public/paths.cfm" >
<cfinclude template="/session/sire/configs/paths.cfm">

<cfparam name="inpContactString" default="9494000553" />
<cfparam name="inpTextToSend" default="Test message..." />
<cfparam name="inpShortCode" default="39492" />


<cfinclude template="../../sire/configs/paths.cfm">

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("/public/sire/css/select2.min.css")
        .addCss("/public/sire/css/select2-bootstrap.min.css")
        .addJs("/public/sire/js/jquery.mask.min.js")
        .addJs("/public/sire/js/select2.min.js")
        .addJs("/public/js/common.js");
</cfscript>


<style type="text/css">
	#send_one_form .select2-container{
		z-index: 960;
	}
	
	.Preview-Me {
	    margin: 5px 45px 5px 20px;
	    background-color: #66ccff;
	    color: #ffffff;
	    text-shadow: 1px 1px #AAAAAA;
	}
	
	.Preview-Bubble {
	    background-color: #28CBEF;
	    border-radius: 5px;
	    box-shadow: 0 0 6px #B2B2B2;
	    display: inline-block;
	    padding: 10px 18px;
	    position: relative;
	    vertical-align: top;
	    font-family: Helvetica, sans-serif;
	    font-size: 15px;
	    text-align: left;
	}
	
	.Preview-Me::before {
	    box-shadow: -2px 2px 2px 0 rgba( 178, 178, 178, .4 );
	    left: -7px;
	    background-color: #66ccff;
	}
	
	.Preview-Bubble::before {
	    background-color: #28CBEF;
	    content: "\00a0";
	    display: block;
	    height: 12px;
	    position: absolute;
	    top: 10px;
	    transform: rotate( 29deg ) skew( -35deg );
	    -moz-transform: rotate( 29deg ) skew( -35deg );
	    -ms-transform: rotate( 29deg ) skew( -35deg );
	    -o-transform: rotate( 29deg ) skew( -35deg );
	    -webkit-transform: rotate( 29deg ) skew( -35deg );
	    width: 16px;
	}
	
	#MessageSendResult
	{
		display: none;
	}

</style>
<div class="portlet light bordered">
    <div class="row">
        <div class="col-xs-12">
        	<h2 class="page-title">Send a Single MT SMS Message</h2>
        </div>  
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-8">
        	<div class="clearfix">
				<div class="profile-wrapper form-horizontal">
					
					
					<!--- Only display list of short codes assigned to current user--->
	                <cfinvoke component="session.cfc.csc.csc" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
	                <cfset SCMBbyUser = getShortCodeRequestByUser.getSCMB />

					
					<div class="form-group">
					    <label for="inpListShortCode" class="col-sm-3 col-lg-3 control-label text-left">Short Code<span class="text-danger">*</span></label>
					    <div class="col-sm-9 col-lg-6"> 
	                       <select id="inpListShortCode" class="select2" style="width: 100%;">
	                       
	                       		<cfoutput>
	                            	<option value="0">Select a Short Code</option>
		                            <cfloop query="SCMBbyUser">
		                                                                      
		                               <cfif ShortCode_vch EQ inpShortCode >
		                                   <option value="#ShortCode_vch#" selected="selected">#SCMBbyUser.ShortCode_vch#</option>
		                               <cfelse>
		                                   <option value="#ShortCode_vch#">#SCMBbyUser.ShortCode_vch#</option>
		                               </cfif>
		                               
		                               
		                           </cfloop>
							   </cfoutput>
	                        </select>
	                    
	                    </div>
	                    
	                </div>   
	                    
					<div class="form-group">
					    <label for="inpTextToSend" class="col-sm-3 col-lg-3 control-label text-left">Text to send<span class="text-danger">*</span></label>
					    <div class="col-sm-9 col-lg-6">					    											 																		
							
							<div class="Preview-Me Preview-Bubble" style="width: 100%; height: auto;"><textarea class="form-control" id="inpTextToSend" maxlength="1000" rows="6" style="border-radius: 5px;"><cfoutput>#inpTextToSend#</cfoutput></textarea></div>
										
					    </div>
					</div>
					
					<div class="form-group">
					    <label for="inpContactString" class="col-sm-3 col-lg-3 control-label text-left">SMS Number <span class="text-danger">*</span></label>
					    <div class="col-sm-9 col-lg-6">
					    	<input type="text" class="form-control validate[required,custom[usPhoneNumber]]" id="inpContactString" name="inpContactString" maxlength="15" value="<cfoutput>#inpContactString#</cfoutput>" data-prompt-position="topLeft:100"> 
					    </div>
					</div>
					

					<div class="form-group">
						<div class="btn-group-setting">									
							<div class="col-sm-9 col-lg-6">
								<button type="button" class="btn btn-success btn-success-custom btn-custom_size" id="btn_SendNow">Send Now</button>
								<a href="##" class="btn btn-primary btn-back-custom btn-custom_size" onclick="ketina()">Reset</a>
							</div>
						</div>	
					</div>
					
						
					
					<div id="MessageSendResult" style="border: solid 3px rgba(0, 0, 0, 0.5); padding: 1em; border-radius: 2em;">
						<h2 style="background-color: #274e60; color: white; padding: .5em; border-radius: .5em;">Message Send Result</h2>
						<h3 id="MessageSendResultText" style="padding: .5em;">Message Sent OK</h3>
					</div>												
					
					
				</div>
        		
        	</div>
        </div>  
	</div>
</div>

<cfparam name="variables._title" default="Send Message MT Single Send">
<cfinclude template="../views/layouts/master.cfm">



<script type="application/javascript">

	function ketina(){
	    var inpBatchId=document.forms["send_one_form"]["inpBatchId"].value;
	    var inpContactString=document.forms["send_one_form"]["inpContactString"].value;
	    if (inpBatchId==null && inpContactString==null, inpBatchId=="" && inpContactString=="")
		{
			console.log('Null');
			return false;
		}else{
			location.href='/session/sire/pages/send-one-single-mt';
		}
	}

	
	$("#send_one_form").validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : false});

	<!--- Set up page tour for new users --->
	$(function() {
		
		
		<!--- Only mask for US if international numbers are not allowed --->
		<cfif !structKeyExists(Session,"AllowInternational") >
	
			$("#inpContactString").mask("(000)000-0000");
			
		<cfelse>
		
			<cfif Session.AllowInternational EQ 0 >
		
				$("#inpContactString").mask("(000)000-0000");
		
			</cfif>		

		</cfif>


		$("#inpBatchId").select2( { theme: "bootstrap"} );
		$("#inpListShortCode").select2( { theme: "bootstrap"} );
				
		<!--- Give user chance to cancel request --->
		$('#btn_SendNow').on('click', function(e){
			
			
			if ($('#send_one_form').validationEngine('validate'))
			{
				<!--- help prevent user from clicking button twice (or more) per message sent --->
				$('#btn_SendNow').attr("disabled", true);
				
				e.preventDefault();
				bootbox.dialog({
				    message:'Are you sure you want to send this message to <b>'+ $("#inpContactString").val() + '</b> now?',
				    title: "Send Message",
				    buttons: {
				        success: {
				            label: "Confirm",
				            className: "btn btn-medium btn-success-custom",
				            callback: function() {
				            				            	
				            	<!--- User re-verified OK to send so submit form now --->
				            	SendOneMessage();
				            	
				            }
				        },
				         danger: {
					      label: "Reset",
					      className: "btn btn-primary btn-back-custom",
					      callback: function() {
					      
					      	$('#btn_SendNow').attr("disabled", false);
					      
					      }
					    },
				    }
				});	
			}		
		})


			
	});

	
	function SendOneMessage(){
	
		<!--- Disable button after send so no duplicates --->
					
		 $.ajax({
        url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=SendSingleMT&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
        type: 'POST',
        dataType: 'json',
        data:   {                     
                    inpShortCode : $('#inpListShortCode').val(),
                    inpContactString : $('#inpContactString').val(),
                    inpTextToSend : $('#inpTextToSend').val()
                },                    
        error: function(XMLHttpRequest, textStatus, errorThrown) { $('#MessageSendResultText').html("General Error - " + textStatus);  $('#MessageSendResult').show(); <!---console.log(textStatus, errorThrown);--->},                    
        success:        
            <!--- Default return function for Do CFTE Demo - Async call back --->
            function(d) 
            {
                <!--- Alert if failure --->
                                                                                            
                <!--- Get row 1 of results if exisits--->
                                                    
                    <!--- Check if variable is part of JSON result string --->                              
                    if(typeof(d.RXRESULTCODE) != "undefined")
                    {                           
                        CurrRXResultCode = d.RXRESULTCODE;  
                        
                        if(CurrRXResultCode > 0)
                        {                              
                           	$('#btn_SendNow').attr("disabled", false);
                        }
                        else
                        {
                            
                        }
                        
                        var MessageSendResultText = "";
                        
                        if(parseInt(d.PROKF) == 1 && parseInt(d.BLOCKEDBYDNC) == 0)
                        {
	                        MessageSendResultText += " Message Sent OK";	                        
                        }
                        
                        if(parseInt(d.BLOCKEDBYDNC) != 0)
                        {
	                        MessageSendResultText += " Message Blocked by Previous DNC request. Unitl they opt back in you can not message them.";	                        
                        }
                        
                        MessageSendResultText += d.PRM + " ";
                        MessageSendResultText += d.PROKF + " ";
                        MessageSendResultText += d.ERRMESSAGE + " ";
                        MessageSendResultText += d.MESSAGE + " ";
                        
                        $('#MessageSendResultText').html(MessageSendResultText);
                    }
                    else
                    {<!--- Invalid structure returned --->  
                        
                    }
                    
                    
                    $('#MessageSendResult').show(); 
                                           
            }       
            
        });
        return false;




		
		
	}



	


</script>

