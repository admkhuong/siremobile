<cfinclude template="/public/paths.cfm" >
<cfinclude template="/session/sire/configs/paths.cfm">

<cfparam name="inpContactString" default="" />
<cfparam name="inpBatchId" default="" />

<!--- In case user has not been here set default values for passback structure from actual send request --->
<cfif !structKeyExists(Session,"SendOneData")>
	
	<cfset Session.SendOneData = StructNew() />
	
	<cfset Session.SendOneData.inpContactString = inpContactString />
	<cfset Session.SendOneData.inpBatchId = inpBatchId />
	<cfset Session.SendOneData.ResultMessage = "" />
	
</cfif>

<!--- <cfdump var="#Session.SendOneData#" /> --->


<cfinclude template="../../sire/configs/paths.cfm">

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("/public/sire/css/select2.min.css")
        .addCss("/public/sire/css/select2-bootstrap.min.css")
        .addJs("/public/sire/js/jquery.mask.min.js")
        .addJs("/public/sire/js/select2.min.js")
        .addJs("/public/js/common.js");
</cfscript>

<!---<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/campaign.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/sire/css/select2.min.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/sire/css/select2-bootstrap.min.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/sire/js/select2.min.js">
</cfinvoke>--->


<!--- remove old method: 
<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortcode"></cfinvoke>								
--->
<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortcode">
	<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
</cfinvoke>

<style type="text/css">
	#send_one_form .select2-container{
		z-index: 960;
	}
</style>
<div class="portlet light bordered">
    <div class="row">
        <div class="col-xs-12">
        	<h2 class="page-title">Send a Message</h2>
        </div>  
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-8">
        	<div class="clearfix">
        		<form autocomplete="off" name="send_one_form" id="send_one_form" action="send-one-act.cfm" method="post">
						<div class="profile-wrapper form-horizontal">
							
							<div class="form-group">
							    <label for="inpBatchId" class="col-sm-3 col-lg-3 control-label text-left">Campaign Id <span class="text-danger">*</span></label>
							    <div class="col-sm-9 col-lg-6">
							    
							    
							    	<!--- Get Select Box of Active Batches --->
									<cftry>      
		           	                
		            
						              	<cfquery name="GetBatches" datasource="#Session.DBSourceEBM#">
						                    SELECT        
						                    	BatchId_bi,	                   
						                        Desc_vch 
						                    FROM 
						                        simpleobjects.batch 
						                    WHERE 
						                        UserId_int = #Session.USERID#
						                    AND
						                    	Active_int = 1
						                    AND
						                    	EMS_Flag_int IN (0,1,2)
						                    AND
						                    	ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#shortcode.SHORTCODEID#"/>
										</cfquery>
										
										<select name="inpBatchId" id="inpBatchId" size="5" style="width: 100%;" class="form-control validate[required,custom[noHTML]]">
									
									
											<cfif GetBatches.RecordCount GT 0>
										    
										    	<cfloop query="GetBatches">
										         	<cfoutput><option value="#GetBatches.BatchId_bi#" style="" <cfif GetBatches.BatchId_bi EQ Session.SendOneData.inpBatchId>selected</cfif>   >#GetBatches.BatchId_bi# - #GetBatches.Desc_vch#</option></cfoutput>
												</cfloop>
											
											<cfelse>
											
												<option value="0" style="">0 - No Campaigns Found!</option>
											
											</cfif>	 
									    
									    
									    </select>
		     
		     
									<cfcatch TYPE="any">
										
						                
						            </cfcatch>
						            </cftry>     

							    </div>
							</div>
							
							<div class="form-group">
							    <label for="inpContactString" class="col-sm-3 col-lg-3 control-label text-left">SMS Number <span class="text-danger">*</span></label>
							    <div class="col-sm-9 col-lg-6">
							    	<input type="text" class="form-control validate[required,custom[usPhoneNumber]]" id="inpContactString" name="inpContactString" maxlength="15" value="<cfoutput>#Session.SendOneData.inpContactString#</cfoutput>" data-prompt-position="topLeft:100"> 
							    </div>
							</div>
							

							<div class="form-group">
								<div class="btn-group-setting">									
									<div class="col-sm-9 col-lg-6">
										<button type="button" class="btn btn-success btn-success-custom btn-custom_size" id="btn_SendNow">Send Now</button>
										<a href="##" class="btn btn-primary btn-back-custom btn-custom_size" onclick="ketina()">Reset</a>
									</div>
								</div>	
							</div>
							
							
							<!--- Look for any transaction alert messages from previous send  --->
							
							<cfif Len(Session.SendOneData.ResultMessage) GT 0>
								<div style="border: solid 3px rgba(0, 0, 0, 0.5); padding: 1em; border-radius: 2em;">
									<h2 style="background-color: #274e60; color: white; padding: .5em; border-radius: .5em;">Message Send Result</h2>
									<h3 style="padding: .5em;"><cfoutput>#Session.SendOneData.ResultMessage#</cfoutput></h3>
								</div>
							</cfif>
							
							
							<!--- Look for any recent MO's from given phone number - maybe they are asking random questions? --->
							
							<!--- Advanced user feature - allow custom message definition - Dedicated short code only users --->
							
							
							<!--- Batch Id lookup tool - What message is about to be sent OR only come in from Campaign Manage? ---> 
							
						</div>
        		</form>
        	</div>
        </div>  
	</div>
</div>

<cfparam name="variables._title" default="Send Message">
<cfinclude template="../views/layouts/master.cfm">


<!--- Page Tours using bootstrp tour - see http://http://bootstraptour.com/api/ --->
<script type="application/javascript">

	function ketina(){
	    var inpBatchId=document.forms["send_one_form"]["inpBatchId"].value;
	    var inpContactString=document.forms["send_one_form"]["inpContactString"].value;
	    if (inpBatchId==null && inpContactString==null, inpBatchId=="" && inpContactString=="")
		{
			console.log('Null');
			return false;
		}else{
			location.href='/session/sire/pages/send-one';
		}
	}

	
	$("#send_one_form").validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : false});

	<!--- Set up page tour for new users --->
	$(function() {
		
		
		<!--- Only mask for US if international numbers are not allowed --->
		<cfif !structKeyExists(Session,"AllowInternational") >
	
			$("#inpContactString").mask("(000)000-0000");
			
		<cfelse>
		
			<cfif Session.AllowInternational EQ 0 >
		
				$("#inpContactString").mask("(000)000-0000");
		
			</cfif>		

		</cfif>


		$("#inpBatchId").select2( { theme: "bootstrap"} );
				
		<!--- Give user chance to cancel request --->
		$('#btn_SendNow').on('click', function(e){
			
			
			if ($('#send_one_form').validationEngine('validate'))
			{
			
				$('#btn_SendNow').attr("disabled", true);
				
				e.preventDefault();
				bootbox.dialog({
				    message:'Are you sure you want to send this message to <b>'+ $("#inpContactString").val() + '</b> now?',
				    title: "Send Message",
				    buttons: {
				        success: {
				            label: "Confirm",
				            className: "btn btn-medium btn-success-custom",
				            callback: function() {
				            				            	
				            	<!--- User re-verified OK to send so submit form now --->
				            	$('#send_one_form').submit();
				            	
				            }
				        },
				         danger: {
					      label: "Reset",
					      className: "btn btn-primary btn-back-custom",
					      callback: function() {
					      
					      	$('#btn_SendNow').attr("disabled", false);
					      
					      }
					    },
				    }
				});	
			}		
		})


			
	});

	
	function SendOneMessage(){
	
	
		<!--- Disable button after send so no duplicates --->
		
		
	}



<!--- Reset session pass through data to blank - only used to reload data on this page after send --->
<cfset Session.SendOneData = StructNew() />
<cfset Session.SendOneData.inpContactString = "" />
<cfset Session.SendOneData.inpBatchId = "" />
<cfset Session.SendOneData.ResultMessage = "" />
	
	$('.short-code-list-select').on('change', function () {
        location.reload();
    });

</script>

