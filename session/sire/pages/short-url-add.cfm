<!--- Simple modal to allow adding new shortened URLs  --->
<cfparam name="inpPK" default="0"> 

<cfset USERID = "" />
<cfset UNIQUETARGETID = "" />
<cfset REDIRECTCODE = "" />
<cfset CREATED = "" />
<cfset LASTCLICKED = "" />
<cfset DESC = "" />
<cfset SHORTURL = "" />
<cfset TARGETURL = "" />
<cfset DYNAMICDATA = "" />
<cfset CLICKCOUNT = "" />

<cfif inpPK GT 0>

	<cfinvoke method="GetShortURLData" component="session.sire.models.cfc.urltools" returnvariable="RetVarGetShortURLData">
		<cfinvokeargument name="inpPK" value="#inpPK#">  
			<cfinvokeargument name="REQSESSION" value="1">
			</cfinvoke>  

			<cfset USERID = RetVarGetShortURLData.USERID />
			<cfset UNIQUETARGETID = RetVarGetShortURLData.UNIQUETARGETID />
			<cfset REDIRECTCODE = RetVarGetShortURLData.REDIRECTCODE />
			<cfset CREATED = RetVarGetShortURLData.CREATED />
			<cfset LASTCLICKED = RetVarGetShortURLData.LASTCLICKED />
			<cfset DESC = RetVarGetShortURLData.DESC />
			<cfset SHORTURL = RetVarGetShortURLData.SHORTURL />
			<cfset TARGETURL = RetVarGetShortURLData.TARGETURL />
			<cfset DYNAMICDATA = RetVarGetShortURLData.DYNAMICDATA />
			<cfset CLICKCOUNT = RetVarGetShortURLData.CLICKCOUNT />

		</cfif>


		<style>


		</style>

		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

			<cfif inpPK GT 0>
				<h4 class="modal-title">Update Shortened URL</h4>
			<cfelse>    
				<h4 class="modal-title">Add Shortened URL</h4>
			</cfif>  
		</div>

		<cfoutput>
			<div class="modal-body">        
				<form action="" method="POST" id="cid_form">

					<input type="hidden" name="" id="inpPK" value="#inpPK#" />
					<input type="hidden" name="inpLength" id="inpLength" value="6" />
					<input type="hidden" name="inpUniqueTarget" id="inpUniqueTarget" value="0" />
					<input type="hidden" name="inpRedirectCode" id="inpRedirectCode" value="301" />


					<div class="">
						<b>Description</b> - Something to help you remember what this link is for.
					</div>

					<div class="">
						<textarea type="text" class="" id="inpDesc" row="5" maxlength='612' style="min-height:60px; width:100%; padding:1em;">#DESC#</textarea>
					</div>

					<div class="">
						<b>Target URL</b> - The URL you would type in your browser to get to the final destination. Example https://google.com/maps
					</div>

					<div class="">
						<textarea type="text" class="" id="inpTargetURL" row="5" maxlength='612' style="min-height:60px; width:100%; padding:1em;">#TARGETURL#</textarea>
						<span class="has-error KeywordStatus" style="display: none;" id="targetUrlStatus"></span>
					</div>	            

					<div class="">
						<b>Dynamic Data</b> - This can be used by advanced users to append additional data to the end of a url (inpData=something&inpData2=something Else)<br\>It is OK to leave this blank if you are not using it.
					</div>

					<div class="">
						<textarea type="text" class="" id="inpDynamicData" row="5" maxlength='612' style="min-height:60px; width:100%; padding:1em;">#DYNAMICDATA#</textarea>
					</div>	 

					<cfif inpPK EQ 0>
						<div class="">
							<b>Custom Short URL Key</b> - Optionally Specify your own key at the end of the standard short URL http://mlp-x.com. This key will be used to redirect from the short URL to your Target URL you specify. Example: the key <i>fashion</i> - this would then be used on the end as http://mlp-x.com/fashion
						</div>

						<div class="">
							<textarea type="text" class="" id="inpShortURLKey" row="5" maxlength='612' style="min-height:60px; width:100%; padding:1em;">#SHORTURL#</textarea>
							<span class="has-error KeywordStatus" style="display: none;" id="ShortURLStatus"></span>
						</div>
					</cfif>

				</form>
			</div>        
		</cfoutput>

		<div class="modal-footer">

			<button type="button" class="btn green-cancel" data-dismiss="modal">Cancel</button>
			<button type="button" class="btn green-gd" onclick ="Save();return false" id="btnSaveShortURL">Save</button>

		</div>	

		<script type="text/javascript">	

			var shortURLValid = 0;
			var targetURLValid = 0;

			function Save() {				
				var targetUrl = encodeURI($('#inpTargetURL').val());
				<cfif inpPK GT 0>
					var method =  "UpdateShortURLData" ;
					var inpPK = "<cfoutput>#inpPK#</cfoutput>";
					var dataPost = {
						inpPK : inpPK,
						inpTargetURL : targetUrl,
						inpDesc : $('#inpDesc').val(),
						inpTargetURL : $('#inpTargetURL').val(),
						inpDynamicData : $('#inpDynamicData').val()		
					};
				<cfelse>
					var method =  "AddShortURL" ;

					var dataPost = { 				 
						inpDesc : $('#inpDesc').val(),
						inpTargetURL : targetUrl,
						inpDynamicData : $('#inpDynamicData').val(),
						inpLength : $('#inpLength').val(),
						inpUniqueTarget : $('#inpUniqueTarget').val(),
						inpRedirectCode : $('#inpRedirectCode').val(),
						inpShortURLKey : $('#inpShortURLKey').val()					
					};		
				</cfif>

				var url = "/session/sire/models/cfc/urltools.cfc?method="+method+"&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true"; 

				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url: url,   
					dataType: 'json',
					data:  dataPost,					  
					error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
					success:function(d)
					{
						if(d.RXRESULTCODE > 0){	
							var currentPage = $('input.paginate_text').val();
							var oTable = $('#tblListShortURL').dataTable();
					//oTable.fnDraw();
					oTable.fnPageChange(parseInt(currentPage-1));

					$('#AddShortURLModal').modal('hide').data( 'bs.modal', null );
				}else{
					$('#AddShortURLModal').modal('hide').data( 'bs.modal', null );}
				}});
			}

			$("#inpShortURLKey").delayOn("input", 800, function(element, event) {

				$('#ShortURLStatus').removeClass('has-error');       
				$('#ShortURLStatus').removeClass('not-has-error');

				if(element.value.length != 0){
					$('#ShortURLStatus').html('<img src="/public/sire/images/loading.gif" class="ajax-loader-inline" style="width:2em; height:2em;"> ');

					$.ajax({
						type: "POST",
	 			url: '/session/sire/models/cfc/mlp.cfc?method=checkMLPUrlAvailability&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
						dataType: 'json',
						async: false,
						data:  
						{
	 							inpURL : element.value,
						},                    
						error: function(XMLHttpRequest, textStatus, errorThrown) { /*<!--- No result returned --->*/
							alertBox("Error. No Response from the remote server. Check your connection and try again.");
						},                    
						success:        
						/*<!--- Default return function for Async call back --->*/
						function(d) 
						{
							/*<!--- RXRESULTCODE is 1 if everything is OK --->*/
							if (parseInt(d.RXRESULTCODE) == 1) 
							{   
								$('#ShortURLStatus').addClass('not-has-error');
								$('#ShortURLStatus').html(d.MESSAGE);
									shortURLValid = 1;
									if (targetURLValid == 1) {
										$('#btnSaveShortURL').prop('disabled', false);
									}
								}
								else{
									$('#ShortURLStatus').addClass('has-error');
									$('#ShortURLStatus').html(d.MESSAGE);
									$('#btnSaveShortURL').prop('disabled', true);
									shortURLValid = 0;
								}
								$('#ShortURLStatus').show();                                     
							}
						});
				}else{
					shortURLValid = 0;
					$('#ShortURLStatus').hide();
					$('#ShortURLStatus').removeClass('has-error');       
					$('#ShortURLStatus').removeClass('not-has-error');
				}
			});


			$("#inpTargetURL").delayOn("input", 800, function(element, event) {
		
					$('#targetUrlStatus').removeClass('has-error');       
					$('#targetUrlStatus').removeClass('not-has-error');

					if(element.value.length != 0){
						$('#targetUrlStatus').html('<img src="/public/sire/images/loading.gif" class="ajax-loader-inline" style="width:2em; height:2em;"> ');

						$.ajax({
							type: "POST",
							url: '/session/sire/models/cfc/urltools.cfc?method=CheckTargetURL&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
							dataType: 'json',
							async: false,
							data:  
							{
								inpTargetURLValue : element.value,
							},                    
							error: function(XMLHttpRequest, textStatus, errorThrown) { /*<!--- No result returned --->*/
								alertBox("Error. No Response from the remote server. Check your connection and try again.");
							},                    
							success:        
							/*<!--- Default return function for Async call back --->*/
							function(d) 
							{
								/*<!--- RXRESULTCODE is 1 if everything is OK --->*/
								if (parseInt(d.RXRESULTCODE) == 1) 
								{   
									$('#targetUrlStatus').addClass('not-has-error');
									$('#targetUrlStatus').html(d.MESSAGE);
									targetURLValid = 1;
									if (shortURLValid == 1) {
										$('#btnSaveShortURL').prop('disabled', false);
									}
								}
								else
								{
									$('#targetUrlStatus').addClass('has-error');
									$('#targetUrlStatus').html(d.MESSAGE);
									$('#btnSaveShortURL').prop('disabled', true);
									targetURLValid = 0;
								}
								$('#targetUrlStatus').show();                                  
							}
						});
					}else{
						targetURLValid = 0;
						$('#targetUrlStatus').hide();
						$('#targetUrlStatus').removeClass('has-error');       
						$('#targetUrlStatus').removeClass('not-has-error');
					}
				});
			</script>
