<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/assets/pages/scripts/short-url.js">
</cfinvoke>

<cfinvoke component="session.sire.models.cfc.urltools" method="AllowAddUrl" returnvariable="checkAllowAddUrl"> 
</cfinvoke>

<cfinvoke component="session.sire.models.cfc.order_plan" method="GetUserPlan" returnvariable="userPlanInfo" />


<!--- <cfdump var="#checkAllowAddUrl#" abort="true"> --->

 <div class="portlet light bordered">
   <div class="row">
       <div class="col-xs-12">
            <div class="clearfix">
                <h4 class="form-heading"><strong  style="color: rgb(92,92,92); font-family:'Source Sans Pro', sans-serif;">Short URL</strong></h4>
            </div>
<!---             <p class="detail-simon">Easy to type, shortened web links (<a data-toggle="modal" href="#" data-target="#InfoModalURL">URLs</a>) that can be useful in many places for Marketing and CRM messaging. The shortened <a data-toggle="modal" href="#" data-target="#InfoModalURL">URLs</a> will redirect to a longer <a data-toggle="modal" href="#" data-target="#InfoModalURL">URLs</a> that you specify. In <a data-toggle="modal" href="#" data-target="#InfoModalSMS">SMS</a> this will help you save characters in your In <a data-toggle="modal" href="#" data-target="#InfoModalSMS">SMS</a> messages. You can use the default random codes for the short <a data-toggle="modal" href="#" data-target="#InfoModalURL">URLs</a> key or reserve your own custom named keys if it is not used by someone else already. You also get a quick view of how many people clicked (Hits) on your links. <a data-toggle="modal" href="#" data-target="#InfoModalSample">Click here to see an Example</a></p> --->
            <p class="detail-simon">In case you didn’t know, the length of one text message (credit) is 160 characters. While that may be enough for a standard message, a lengthy <a data-toggle="modal" href="#" data-target="#InfoModalURL">URL</a> can really eat up a lot of that character space. Short <a data-toggle="modal" href="#" data-target="#InfoModalURL">URLs</a> are here to save the day! This handy tool will allow you to create a short <a data-toggle="modal" href="#" data-target="#InfoModalURL">URL</a> to use in your messages. Don’t worry, it’ll redirect to the original (longer) URL automatically. <a data-toggle="modal" href="#" data-target="#InfoModalSample">Click here to see an Example</a></p>
       </div>  
   </div>
   <div class="row">
       <div class="col-xs-12">
           <a class="btn green-gd pull-right add-shorturl-simon-button" data-toggle="modal" href="short-url-add" data-target="#AddShortURLModal">Add Short Url</a>
       </div>
       <!--- <cfif !checkAllowAddUrl> --->
           <div class="mlp-acts">
               <p class="warning-text-simon"><strong class="text-right">You have reached max <cfoutput>#userPlanInfo.SHORTURLSLIMITNUMBER#</cfoutput> Short URLs</strong></p>
           </div>
       <!--- </cfif> --->
   </div>
   <br>
   <div class="row">
       <div class="col-xs-12">
            <div class="table-responsive re-table simon-table">
                    <table id="tblListShortURL" class="table table-striped table-bordered table-responsive">
                    </table>
                </div>
            </div>
       </div>  
   </div>
</div>

<!-- Modal -->
<div class="modal fade no-print" id="AddShortURLModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Add Short URL</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn green-cancel" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn green-gd">Save</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- Modal -->
<div id="InfoModalSMS" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">What is a Text Message?</h4>
            </div>
            <div class="modal-body">
                <p><b>SMS</b> stands for <b>Short Message Service</b> and is also commonly referred to as a <b>"text message"</b>. With <b>SMS</b>, you can send a message of up to 160 characters to another device. Longer messages will automatically be split up into several parts. Each additional increment is charged at 1 credit.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green-cancel" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div id="InfoModalURL" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">What is a URL?</h4>
            </div>
            <div class="modal-body">
                <p><b>URL</b> stands for Uniform Resource Locator, and is used to specify addresses on the World Wide Web. A URL is the fundamental network identification for any resource connected to the web (e.g., hypertext pages, images, and sound files). URLs have the following format: protocol://hostname/other_information.</p>
                <p>Sire is located at https://www.siremobile.com</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green-cancel" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="InfoModalSample" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">What is a Short URL used for?</h4>
            </div>
            <div class="modal-body">
                <p>Let's say you need to publish directions to a new store location as part of an SMS message. Instead of using up tons of characters with a huge URL to a map location, you can instead just send your subscribers a shorter link that will forward to the much longer destination.</p>
                <p>
                    <div><b>Location of South Coast Plaza</b></div>
                    <div class="dont-break-out"><a style="word-wrap: break-word;" href="https://www.google.com/maps/place/South+Coast+Plaza/@33.6911652,-117.8911167,17z/data=!3m1!4b1!4m5!3m4!1s0x80dcdf2525c7200b:0xd1d9f268d34c30d8!8m2!3d33.6911652!4d-117.8889227">https://www.google.com/maps/place/South+Coast+Plaza/@33.6911652,-117.8911167,17z/data=!3m1!4b1!4m5!3m4!1s0x80dcdf2525c7200b:0xd1d9f268d34c30d8!8m2!3d33.6911652!4d-117.8889227</a></div>
                </p>
                <p>You can instead use our URL shortening service.</p>
                <p><a href="http://mlp-x.com/fashion" target="_blank">http://mlp-x.com/fashion</a> will take you to the longer location you specify. Click the link to try it!</p>
                <p style="margin-top:1em;"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3319.687108289821!2d-117.89111138479446!3d33.691165180705696!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcdf2525c7200b%3A0xd1d9f268d34c30d8!2sSouth+Coast+Plaza!5e0!3m2!1sen!2sus!4v1468987417844" scrolling="auto" frameborder="0" id="maplocation" width="100%" height="100%"></iframe></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success-custom" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<cfparam name="variables._title" default="Short URL Management">
<cfinclude template="../views/layouts/master.cfm">






        
    



