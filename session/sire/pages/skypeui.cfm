<cfset variables.menuToUse = 1 />
<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("../assets/global/plugins/jquery-mcustomscrollbar/jquery.mCustomScrollbar.css", true)
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/jquery-mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)
        .addJs("../assets/pages/scripts/skypeui.js");
</cfscript> 

<div class="uk-grid uk-grid-small uk-grid-match" uk-grid>
    <div class="uk-width-1-4@m uk-width-2-5@s">
        <div class="col-match-bg">
            <div class="left-sky">
                <div class="head">
                    <div class="uk-grid uk-grid-small">
                        <div class="uk-width-4-5">
                            <div class="form-group form-gd form-search">
                                <i class="fa fa-search"></i>
                                <input type="text" class="form-control" placeholder="Search" />
                            </div>
                        </div>
                        <div class="uk-width-expand">
                            <div class="type-filter">
                                <a class="val-type"><span>All</span> <i class="fa fa-caret-down"></i></a>
                                <div uk-drop="mode: click">
                                    <div class="uk-card uk-card-body uk-card-default">
                                        <ul class="uk-list pick-type">
                                            <li><a href="##">All</a></li>
                                            <li><a href="##">Unread</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="wrap-new-curent" class="body">
                    <div class="first-new">
                        <span class="title">NEW</span>

                        <ul class="uk-list">
                            <li><a href="##"><b>(763) 444-4058</b></a> <span>1</span></li>
                            <li><a href="##"><b>(763) 444-5555</b></a> <span>3</span></li>
                            <li><a href="##"><b>(763) 444-5555</b></a> <span>3</span></li>
                        </ul>
                    </div>
                    <div class="second-current">
                        <span class="title">CURRENT</span>
                        
                        <div id="list-number">
                            <ul class="uk-list">
                                <li><a href="##">(763) 444-4058</a></li>
                                <li><a href="##">(763) 444-555</a></li>
                                <li><a href="##">(763) 444-4058</a></li>
                                <li><a href="##">(763) 444-555</a></li>
                                <li><a href="##">(763) 444-4058</a></li>
                                <li><a href="##">(763) 444-555</a></li>
                                <li><a href="##">(763) 444-4058</a></li>
                                <li><a href="##">(763) 444-555</a></li>
                                <li><a href="##">(763) 444-4058</a></li>
                                <li><a href="##">(763) 444-555</a></li>
                                <li><a href="##">(763) 444-4058</a></li>
                                <li><a href="##">(763) 444-555</a></li>
                                <li><a href="##">(763) 444-4058</a></li>
                                <li><a href="##">(763) 444-555</a></li>
                                <li><a href="##">(763) 444-4058</a></li>
                                <li><a href="##">(763) 444-555</a></li>
                                <li><a href="##">(763) 444-4058</a></li> 
                                <li><a href="##">(763) 444-555</a></li>
                                <li><a href="##">(763) 444-4058</a></li>
                                <li><a href="##">(763) 444-555</a></li>
                                <li><a href="##">(763) 444-4058</a></li>
                                <li><a href="##">(763) 444-555</a></li>
                                <li><a href="##">(763) 444-4058</a></li>  
                                <li><a href="##">(763) 444-4058</a></li>
                                <li><a href="##">(763) 444-555</a></li>
                                <li><a href="##">(763) 444-4058</a></li>
                                <li><a href="##">(763) 444-555</a></li>
                                <li><a href="##">(763) 444-4058</a></li>   
                                <li><a href="##">(763) 444-4058</a></li>
                                <li><a href="##">(763) 444-555</a></li>
                                <li><a href="##">(763) 444-4058</a></li>
                                <li><a href="##">(763) 444-555</a></li>
                                <li><a href="##">(763) 444-4058</a></li>                    
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="uk-width-3-4@m uk-width-3-5@s">
        <div class="col-match-bg">
            <div class="right-sky">
                <div class="head">
                    <div class="uk-clearfix">
                        <h4 class="title uk-float-left">ServiceNeeded</h4>
                        <a href="##" class="change-screen uk-float-right"><i class="fa fa-exchange"></i></a>
                    </div>
                </div>

                <div class="body">
                    <div class="wrap-preview">
                        <div class="preview-screen">
                            <img class="bg-preview-screen" src="../assets/layouts/layout4/img/preview-phone.png" alt="">
                            <div class="phone-screen">
                                <div class="inner-phone-screen clearfix" id="SMSHistoryScreenArea">
                                    <!--- <cfloop array="#RetVarResponse.RESPONSE#" index="response">
                                        <cfif response.TYPE EQ 2>
                                            <cfset cssClass = "bubble me"/>
                                        <cfelse>
                                            <cfset cssClass = "bubble guess"/>
                                        </cfif>
                                        <div class="#cssClass#">
                                            #HTMLEditFormat(response.MSG)#
                                        </div>
                                        <div style="clear:both"></div>
                                    </cfloop> --->
                                </div>
                                <div class="type-screen">
                                    <textarea id="SMSTextInputArea" name="SMSTextInputArea" maxlength="160" placeholder="Text Message"></textarea>
                                    <button id="SMSSend" type="button" class="btn-send-area"></button>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<cfparam name="variables._title" default="Skype UI - Sire">

<cfinclude template="../views/layouts/master.cfm">