<cfparam name="keyword" default="">

<cfset variables.menuToUse = 1 />
<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addJs("../assets/global/plugins/jquery-knob/js/jquery.knob.js", true)
        .addJs("../assets/global/plugins/chartjs/Chart.bundle.js", true)
        .addJs("/public/sire/js/select2.min.js")
        .addJs("../assets/pages/scripts/ejs_production.js", true)
        .addJs("../assets/pages/scripts/campaign-chat.js")
        .addJs("../assets/pages/scripts/preview.js")
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)
        .addJs("/public/js/jquery.ddslick.js", true);
        
</cfscript>

<style type="text/css">
.help-block {
    font-style: italic;
    font-size: 1.5rem;
    /*color: #8f8f8f;*/
    color: rgb(143,143,143);
}
.row-subscriber .uk-active .subscriber-item.greenbox:hover {
    background-color: #74c37f !important;
}
.row-subscriber .uk-active .subscriber-item.bluebox:hover {
    background-color: #578ca5 !important;
}

.campaign-simon-cancel{
    margin-top: 1rem;
    margin-bottom: 3rem;
    padding-left: 2rem;
    padding-right: 2rem;
}

</style>

<!--- Read Current Profile data and put into form --->
<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="RetVarGetUserOrganization"></cfinvoke>

<cfset campaignData = {
    BatchId_bi = 0,
    KeywordId_int = '',
    Keyword_vch = '',
    Keyword_txt = '',
    Desc_vch = '',
    EMS_Flag_int = 0,
    GroupId_int = 0,
    Schedule_arr = [],
    ControlPoint_arr = [],
    CustomHelpMessage_vch = '',
    CustomStopMessage_vch = '',
    TEMPLATEID = 0,
    OPTIN_GROUPID = 0,
    BLAST_GROUPID = 0,
    TemplateType_ti = 0
}>


<!--- remove old method: 
<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>								
--->
<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>

<cfif keyword NEQ "">
    <cfinvoke method="GetKeywordDetails" component="session.sire.models.cfc.smschat" returnvariable="RetVarKeyword">
        <cfinvokeargument name="inpKeyword" value="#keyword#"/>
    </cfinvoke>

    <cfif RetVarKeyword.RXRESULTCODE LT 1 OR RetVarKeyword.INFO.EMSFLAG NEQ 20>
        <cflocation url="/session/sire/pages/dashboard" addtoken="false"/>
    </cfif>

    <cfif RetVarKeyword.RXRESULTCODE GT 0>
        <cfif RetVarKeyword.INFO.SHORTCODEID NEQ shortCode.SHORTCODEID>
            <cflocation url="/session/sire/pages/sms-chat" addtoken="false"/>
        </cfif>
    </cfif>

    <cfinvoke method="getAPISettingsByBatchId" component="session.sire.models.cfc.smschat" returnvariable="RetVarSetting">
        <cfinvokeargument name="inpBatchId" value="#RetVarKeyword.INFO.BATCHID#"/>
    </cfinvoke>

    <cfif RetVarSetting.RXRESULTCODE GTE 1>
        <cfset welcomeMsg = RetVarSetting.WELCOMEMSG/>
        <cfset endMsg = RetVarSetting.ENDMSG/>
        <cfset listOfPhones = listToArray(RetVarSetting.SETTINGS.inpSMSList)/>
        <cfset listOfEmails = listToArray(RetVarSetting.SETTINGS.inpEmailList)/>
        <cfset sendEmailStart = RetVarSetting.SETTINGS.inpSendEmailStart/>
        <cfset sendEmailReceive = RetVarSetting.SETTINGS.inpSendEmailReceived/>
        <cfset sendSMSStart = RetVarSetting.SETTINGS.inpSendSMSStart/>
        <cfset sendSMSReceive = RetVarSetting.SETTINGS.inpSendSMSReceived/>
    <cfelse>
        <cfset welcomeMsg = ""/>
        <cfset endMsg = ""/>
        <cfset listOfPhones = []/>
        <cfset listOfEmails = []/>
        <cfset sendEmailStart = 0/>
        <cfset sendEmailReceive = 0/>
        <cfset sendSMSStart = 0/>
        <cfset sendSMSReceive = 0/>
    </cfif>

    <cfoutput>
        <input type="hidden" name="Batchid" id="Batchid" value="#RetVarKeyword.INFO.BATCHID#">
        <input type="hidden" name="Keyword" id="Keyword" value="#keyword#">
    </cfoutput>

    <cfset action="Edit"/>
<cfelse>
    <cfset welcomeMsg = ""/>
    <cfset endMsg = ""/>
    <cfset listOfPhones = []/>
    <cfset listOfEmails = []/>
    <cfset sendEmailStart = 0/>
    <cfset sendEmailReceive = 0/>
    <cfset sendSMSStart = 0/>
    <cfset sendSMSReceive = 0/>
    <cfset action = "CreateNew"/>

    <cfoutput>
        <input type="hidden" name="Batchid" id="Batchid" value="0">
    </cfoutput>
</cfif>

<cfoutput>

<input type="hidden" name="action" id="action" value="#action#">

<input value="#shortCode.SHORTCODE#" id="ShortCode" type="hidden" name="ShortCode">

<div>
    <form name="frm-campaign" id="frm-campaign">
        <div class="content-for-first-campaign">
            <div class="portlet light bordered cpedit">
                <div class="portlet-body">
                    <div class="row">
                        <cfif keyword EQ "">
                            <div class="col-md-8">
                                <h4 class="portlet-heading2">Choose a Keyword</h4>
                                <p class="portlet-subheading">
                                    A Keyword is a text message that your customer sends to a Short Code using their cell phone. <br>
                                    This Keyword will trigger your SMS campaign and start the communication flow.
                                </p>
                                <div class="row row-small">
                                    <div class="col-md-9">
                                        <div class="form-gd">
                                            <div class="form-group">
                                                <input value="#campaignData.Keyword_txt#" autocomplete="false" class="form-control validate[required]" id="Keyword" type="text" name="Keyword" maxlength="160" style="width:100%;" />
                                                <span class="has-error KeywordStatus" style="display: none;" id="KeywordStatus"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <a href="javascript:;" class="btn green-gd campaign-next keyword-next" style="display: none">Next <i class="fa fa-caret-down"></i></a>
                            </div>
                            <div class="col-md-4">
                                <div class="image2">
                                    <img class="img-responsive" src="../assets/layouts/layout4/img/phone-choose-keyword.png" alt="">
                                <h2>
                                    <cfif len(shortcode.SHORTCODE) EQ 5>
                                        #left(shortcode.SHORTCODE,3)#-#right(shortcode.SHORTCODE,2)#
                                    <cfelse>
                                        #shortcode.SHORTCODE#
                                    </cfif>
                                </h2>
                            </div>
                            </div>
                        <cfelse>
                            <div class="col-md-8">
                                <h4 class="portlet-heading2">Keyword: #keyword#</h4>
                            </div>
                        </cfif>
                    </div>                              
                </div>
            </div>
            

           <div class="portlet light bordered cpedit">
                <div class="portlet-body">
                    <h4 class="portlet-heading2">Write Your Message</h4>
                    <div class="form-group clearfix control-point " id="cp1" data-control-point-physical-id="1" data-control-rq-id="1" data-control-point-type="SHORTANSWER" data-add-new="Edit">
                        <div class="control-point-body">
                            <p class="portlet-subheading tguide">Enter the message your customers will receive when they text your Keyword to your Short Code.</p>
                            <div class="row">
                                <div class="col-md-11">
                                    <div class="form-gd">
                                        <div class="form-group">
                                            <textarea maxlength="160" name="" data-control="text-message" cols="30" rows="5" class="form-control text validate[required]" id="welcomeMsg">#welcomeMsg#</textarea>
                                            <div class="row">
                                                <div class="col-md-9 col-lg-9 col-sm-9 col-xs-9">
                                                    <span class="help-block tdesc">ex: Welcome to Chat support - you are now in session. What can I help you with?</span>
                                                </div>
                                                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                                                    <h5 id="cp-char-count-1" class="text-right control-point-char text-color-gray"></h5>
                                                    <a class="see-unicode-mess pull-right hidden">You have Unicode characters in your message</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div>  
                    </div>

                <div class="form-group clearfix control-point " id="cp2" data-control-point-physical-id="2" data-control-rq-id="2" data-control-point-type="API" data-add-new="Edit">
                    <div class="control-point-body">
                        <!--- New Update Html, Tell The Manager Template --->
                        <p class="portlet-subheading tguide">Please check the contact method you prefer to be contacted for notification.</p>
                        <div class="uk-grid uk-grid-small block-contact-method">
                            <div class="uk-width-1-2@l">
                                <div class="uk-grid uk-grid-small sms-number-wrap #(len(listOfPhones) GT 0 ? "hasMinus" : "noMinus")#" uk-grid>
                                    <div class="uk-width-auto@s col-for-label">
                                        <label class="check-method"><input id="tellmgr-sms-checkbox" class="uk-checkbox" type="checkbox" #(len(listOfPhones) GT 0 ? "checked" : "")#> SMS Number</label>
                                    </div>
                                    <div class="uk-width-expand@s hold-more-content">
                                        <cfif len(listOfPhones) GT 0>
                                            <cfloop array="#listOfPhones#" index="phone">
                                                <div class="uk-grid uk-grid-small uk-flex-middle sms-number-item">
                                                    <div class="uk-width-expand">
                                                        <input type="text" class="form-control phone-style" value="#phone#"/>
                                                    </div>
                                                    <div class="uk-width-auto col-for-more">
                                                        <cfif len(listOfPhones) GT 1>
                                                            <a href="##" class="trigger-minus"><i class="fa fa-minus-circle"></i></a>
                                                        </cfif>
                                                        <a href="##" class="trigger-plus"><i class="fa fa-plus-circle"></i></a>
                                                    </div>
                                                </div>
                                            </cfloop>
                                        <cfelse>
                                            <div class="uk-grid uk-grid-small uk-flex-middle sms-number-item">
                                                <div class="uk-width-expand">
                                                    <input type="text" class="form-control phone-style" value="" disabled/>
                                                </div>
                                                <div class="uk-width-auto col-for-more">
                                                    <a href="##" class="trigger-minus"><i class="fa fa-minus-circle"></i></a>
                                                    <a href="##" class="trigger-plus"><i class="fa fa-plus-circle"></i></a>
                                                </div>
                                            </div>
                                        </cfif>
                                    </div>
                                </div>

                                <div class="uk-grid uk-grid-small email-add-wrap #(len(listOfEmails) GT 0 ? "hasMinus" : "noMinus")#" uk-grid>
                                    <div class="uk-width-auto@s col-for-label">
                                        <label class="check-method"><input id="tellmgr-email-checkbox" class="uk-checkbox" type="checkbox" #(len(listOfEmails) GT 0 ? "checked" : "")#> Email Address</label>
                                    </div>
                                    <div class="uk-width-expand@s hold-more-content">
                                        <cfif len(listOfEmails) GT 0>
                                            <cfloop array="#listOfEmails#" index="email">
                                                <div class="uk-grid uk-grid-small uk-flex-middle email-add-item">
                                                    <div class="uk-width-expand">
                                                        <input type="text" class="form-control" value="#email#"/>
                                                    </div>
                                                    <div class="uk-width-auto col-for-more">
                                                        <cfif len(listOfEmails) GT 1>
                                                            <a href="##" class="trigger-minus"><i class="fa fa-minus-circle"></i></a>
                                                        </cfif>
                                                        <a href="##" class="trigger-plus"><i class="fa fa-plus-circle"></i></a>
                                                    </div>
                                                </div>
                                            </cfloop>
                                        <cfelse>
                                            <div class="uk-grid uk-grid-small uk-flex-middle email-add-item">
                                                <div class="uk-width-expand">
                                                    <input type="text" class="form-control" value="" disabled/>
                                                </div>
                                                <div class="uk-width-auto col-for-more">
                                                    <a href="##" class="trigger-minus"><i class="fa fa-minus-circle"></i></a>
                                                    <a href="##" class="trigger-plus"><i class="fa fa-plus-circle"></i></a>
                                                </div>
                                            </div>
                                        </cfif>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-grid uk-grid-small">
                            <div class="uk-width-1-1">
                                <span class="des-for-heading uk-dislay-block"> Customers feedback will be forwarded to this email address in real time</span>
                            </div>
                        </div>
                        <!--- End New Update Html --->
                    </div>  
                </div>

                <div class="form-group clearfix control-point " id="cp3" data-control-point-physical-id="3" data-control-rq-id="3" data-control-point-type="TRAILER" data-add-new="Edit">
                    <div class="control-point-body">
                        <p class="portlet-subheading tguide">Enter the message your customers will receive when they want to end the chat by text "exit" to your Short code.</p>
                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <textarea maxlength="160" name="" id="endMsg" data-control="text-message" cols="30" rows="5" class="text form-control validate[required]">#endMsg#</textarea>
                                        <div class="row">
                                            <div class="col-md-9 col-lg-9 col-sm-9 col-xs-9">
                                                <span class="help-block tdesc">ex: {%Company%} Goodbye!</span>
                                            </div>
                                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                                            <h5 id="cp-char-count-2" class="text-right control-point-char text-color-gray">Character Count {111}</h5>
                                            <a class="see-unicode-mess pull-right hidden">You have Unicode characters in your message</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>  
                </div>
                <a href="javascript:;" class="btn green-gd campaign-next finished">Preview <i class="fa fa-caret-down"></i></a></div>
            </div>

            <div class="portlet light bordered cpedit" id="wrap-preview">
                <div class="portlet-body">
                    <h4 class="portlet-heading2">Preview of First Message</h4>
                    <input type="hidden" id="inpSMSToAddress" name="inpSMSToAddress" size="20" readonly value="#shortCode.SHORTCODE#">

                    <cfinvoke component="session.cfc.administrator.passwordgen" method="GenSecurePassword" returnvariable="getSecurePassword"></cfinvoke>
                    <cfset inpContactString = "900000001_Demo_#getSecurePassword#_#LSDateFormat(now(), 'yyyy-mm-dd')##LSTimeFormat(now(), 'HH:mm:ss')#"/>
                    <input type="hidden" id="inpContactString" name="inpContactString" placeholder="Enter Contact String Here" size="20" value="#inpContactString#"/>

                    <div class="wrap-preview">
                        <div class="preview-screen">
                            <div class="image">
                                <img class="bg-preview-screen" src="../assets/layouts/layout4/img/preview-phone.png" alt="">
                                <h2>
                                    <cfif len(shortcode.SHORTCODE) EQ 5>
                                        #left(shortcode.SHORTCODE,3)#-#right(shortcode.SHORTCODE,2)#
                                    <cfelse>
                                        #shortcode.SHORTCODE#
                                    </cfif>
                                </h2>
                            </div>
                            <div class="phone-screen">
                                <div class="inner-phone-screen clearfix" id="SMSHistoryScreenArea">
                                </div>
                                <div class="type-screen">
                                    <textarea id="SMSTextInputArea" name="SMSTextInputArea" maxlength="160" placeholder="Text Message"></textarea>
                                    <button id="SMSSend" type="button" class="btn-send-area"></button>
                                    
                                </div>
                            </div>
                        </div>
                        <!--- <br/> --->
                    </div>
                </div>
              
                <a href="javascript:;" class="btn newbtn new-blue-gd btn-finished">Finish</a>
                <button id="SMSPreview" type="button" class="btn newbtn green-gd hidden" style=""> Preview </button>
            </div>
        </div>
    </form>
</div>

</cfoutput>


<!-- Modal -->
<div class="modal be-modal fade sr-modal" id="CompleteModal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" data-dismiss="modal">
            <!--- <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div> --->
            <div class="modal-body" data-dismiss="modal">
                <div class="portlet light box-widget-sidebar">
                    <div class="portlet-body">
                        <div class="wrap-dial-step text-center">
                            <h4 class="stt-step">Voila, You're Done!</h4>
                            <div class="step-holder">
                                <canvas id="completeStep" class="mauto" width="200" height="200"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn-save btn btn-success-custom">Exit</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<cfif action EQ 'Edit'>
        <cfparam name="variables._title" default="Edit Chat Keyword - Sire">
    <cfelse>
        <cfparam name="variables._title" default="Create New Chat Keyword - Sire">        
</cfif>

<cfinclude template="../views/layouts/master.cfm">

<style type="text/css">
    .image { 
        position: relative;
    }

    .image > h2 { 
        position: absolute; 
        top: 121px; 
        left: 0px;
        right: 0px;
        margin-left: auto;
        margin-right: auto;
        font-size: 12px;
        color: black;
        font-weight: bold;
    }

    .image2 { 
        position: relative;
    }

    .image2 > h2 { 
        position: absolute; 
        top: 25%; 
        left: 52%;
        right: 0px;
        margin-left: auto;
        margin-right: auto;
        font-size: 10px;
        color: black;
        font-weight: bold;
    }

    @media screen and (min-width: 1921px) {
        .image2 > h2 {
            top: 99px; 
            left: 196px;
        }
    }

    @media screen and (min-width: 1700px) and (max-width: 1921px) {
        .image2 > h2 {
            top: 99px;
            left: 196px;
        }
    }

    @media screen and (min-width: 1600px) and (max-width: 1700px) {
        .image2 > h2 {
            top: 99px;
            left: 12vw;
        }
    }

    @media screen and (min-width: 1456px) and (max-width: 1599px) {
        .image2 > h2 {
            top: 6.3vw;
            left: 12.4vw;
        }
    }

    @media screen and (min-width: 1400px) and (max-width: 1455px) {
        .image2 > h2 {
            top: 6vw;
            left: 12.4vw;
        }
    }

    @media screen and (min-width: 1360px) and (max-width: 1399px) {
        .image2 > h2 {
            top: 5.7vw;
            left: 12vw;
        }
    }

    @media screen and (min-width: 1280px) and (max-width: 1359px) {
        .image2 > h2 {
            top: 5.7vw;
            left: 11.8vw;
        }
    }

    @media screen and (min-width: 1248px) and (max-width: 1280px) {
        .image2 > h2 {
            top: 67px;; 
            left: 11.5vw;
        }
    }

    @media screen and (max-width: 1240px) {
        .image2 > h2 {
        top: 25%; 
        left: 52%;
        font-size: 0.7vw;
        }
    }

    @media screen and (max-width: 1140px) {
        .image2 > h2 {
        top: 24%; 
        left: 52%;
        font-size: 0.7vw;
        }
    }

    @media screen and (max-width: 1070px) {
        .image2 > h2 {
        top: 22%; 
        left: 52%;
        font-size: 7px;
        font-weight: bold;
        }
    }

    @media screen and (max-width: 991px) {
        .image2 > h2 {
            top: 98px; 
            left: 194px;
            font-size: 10px;
        }
    }

    @media screen and (max-width: 484px) {
        .image2 > h2 {
            top: 27%; 
            left: 194px;
        }
    }

    @media screen and (max-width: 452px) {
        .image2 > h2 {
            top: 27%; 
            left: 52%;
        }
    }

    @media screen and (max-width: 380px) {
        .image2 > h2 {
            top: 25.5%; 
            left: 52%;
        }
    }
</style>