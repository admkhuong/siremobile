<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addJs("https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js")
        .addCss("https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css")
        .addJs("https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.3/socket.io.js")
        .addJs("../assets/pages/scripts/sms_chat.js")
        .addCss("../assets/layouts/layout4/css/custom3.css")
        .addJs("../assets/pages/scripts/sms_ws.js");
</cfscript>

<cfinclude template="/public/sire/configs/paths.cfm"/>


<!--- remove old method: 
<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="defaultShortCode"/>									
--->
<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="defaultShortCode"></cfinvoke>
<cfoutput>
    <input type="hidden" name="shortcode" id="ShortCode" value="#defaultShortCode.SHORTCODE#"/>
</cfoutput>

<div class="portlet light bordered">
    <div class="row">
        <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
            <h4 class="portlet-heading2">SMS Chat</h4>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="inner-addon left-addon">
                <i class="glyphicon glyphicon-search"></i>
                <input type="search" class="form-control" id="searchbox" placeholder="Keyword" />
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 new-keyword">
            <div class="pull-right div-keyword">
                <a href="/session/sire/pages/sms-chat-new" class="btn btn-block green-gd">Add New Keyword</a>
            </div>
        </div>
    </div>
    <div class="portlet light hidden" id="refreshAlert" style="padding-bottom: 0px;margin-bottom: 0px">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12">
                <div class="alert alert-info">
                    <span id="alertMessage"></span> Click <a class="alert-link" id="refreshTable">here</a> to update.
                </div>
            </div>
        </div>
    </div>
    <div class="" style="margin-top: 10px">
        <div class="row">
            <div class="col-xs-12">
                <div class="re-table">
                    <div class="table-responsive">
                        <table id="keywordlist" class="table table-striped table-responsive">
            
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="bootbox modal fade" id="AddNewKeyword" tabindex="-1" role="dialog" aria-labelledby="AddNewKeyword" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title title">Add New Keyword</h4>            
            </div>
            <div class="modal-body">
                <form name="keyword-form" id="keyword-form">
                    <div class="form-group">
                        <h4>Keyword:</h4>
                        <input type="text" class="form-control validate[required, custom[onlyLetterNumber]]" id="Keyword" maxlength="160" autocomplete="off">
                        <span class="has-error KeywordStatus" style="display: none;" id="KeywordStatus"></span>
                    </div>
                    <div class="form-group">
                        <h4>Notify options:</h4>
                        <div class="checkbox checkleft">
                            <label>
                                <input type="checkbox" id="SendEmailStart"> Send email when start a conversation
                            </label>
                        </div>  
                        <div class="checkbox checkleft">
                            <label>
                                <input type="checkbox" id="SendEmailReceived"> Send email when received a new message
                            </label>
                        </div>
                        <label>Email address list (separate each email address with a comma):</label>
                        <textarea class="form-control" rows="2" id="EmailList"></textarea>
                        <div class="checkbox checkleft">
                            <label>
                                <input type="checkbox" id="SendSMSStart"> Send SMS when start conversation
                            </label>
                        </div>  
                        <div class="checkbox checkleft">
                            <label>
                                <input type="checkbox" id="SendSMSReceived"> Send SMS when received a new message
                            </label>
                        </div>
                        <label>Phone number list (separate each phone number with a comma):</label>
                        <textarea class="form-control" rows="2" id="SMSList"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success-custom btn-save-group" id="save-keyword"> Save </button>
                &nbsp; &nbsp; &nbsp; 
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<div class="bootbox modal fade" id="KeywordSettings" tabindex="-1" role="dialog" aria-labelledby="KeywordSettings" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title title">Keyword Settings</h4>            
            </div>
            <div class="modal-body">
                <form name="keyword-form" id="keyword-settings-form">
                    <div class="form-group">
                        <h4>Keyword: <b id="KeywordSettingsKeywordText"></b></h4>
                        <input type="hidden" id="SettingsBatchId" />
                    </div>
                    <div class="form-group">
                        <h4>Notify options:</h4>
                        <div class="checkbox checkleft">
                            <label>
                                <input type="checkbox" id="SettingsSendEmailStart"> Send email when start a conversation
                            </label>
                        </div>  
                        <div class="checkbox checkleft">
                            <label>
                                <input type="checkbox" id="SettingsSendEmailReceived"> Send email when received a new message
                            </label>
                        </div>
                        <label>Email address list (separate each email address with a comma):</label>
                        <textarea class="form-control" rows="2" id="SettingsEmailList"></textarea>
                        <div class="checkbox checkleft">
                            <label>
                                <input type="checkbox" id="SettingsSendSMSStart"> Send SMS when start conversation
                            </label>
                        </div>  
                        <div class="checkbox checkleft">
                            <label>
                                <input type="checkbox" id="SettingsSendSMSReceived"> Send SMS when received a new message
                            </label>
                        </div>
                        <label>Phone number list (separate each phone number with a comma):</label>
                        <textarea class="form-control" rows="2" id="SettingsSMSList"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success-custom btn-save-group" id="save-settings"> Save </button>
                &nbsp; &nbsp; &nbsp; 
                <button type="button" class="btn btn-back-custom" data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<style type="text/css">
    @media (max-width: 767px) {
        .div-keyword {
            margin-bottom: 10px;
        }
    }
    @media (max-width: 768px) {
        .new-keyword {
            margin-top: 10px;
        }
    }
</style>

<!--- User socket channel --->
<cfset us_chn = left(hash("#EnvPrefix#"&"SIRE_CHAT_USER_"&"#session.UserId#"), 6)/>

<cfoutput>
<script type="text/javascript">
    var us_chn = "#us_chn#";
</script>
</cfoutput>

<cfparam name="variables._title" default="SMS Chat - Sire">
<cfinclude template="../views/layouts/master.cfm">
