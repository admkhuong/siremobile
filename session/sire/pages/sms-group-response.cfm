<cfparam name="inpSessionIds" default="0"/>
<cfparam name="KEYWORD" default=""/>
<cfparam name="NPA" default=""/>
<cfparam name="NXX" default=""/>
<cfparam name="RetVarGroupResponse" default="{}"/>
<cfparam name="contactResponse" default="{}"/>
<cfparam name="formatedNumber" default=""/>
<cfparam name="disabled" default=""/>


<cfinvoke component="session.sire.models.cfc.smschat" method="getGroupResponse" returnvariable="RetVarGroupResponse">
	<cfinvokeargument name="inpSessionIds" value="#inpSessionIds#"/>
</cfinvoke>	

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
    	.addCss("../assets/layouts/layout4/css/custom3.css")
        .addJs("../assets/pages/scripts/sms_group_response.js");
</cfscript>

<cfoutput>
<div class="portlet light bordered">
    <div class="row">
        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
            <h4 class="portlet-heading2">Group Response</h4>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
            <h4 class="portlet-heading2 pull-right">
                <a href="/session/sire/pages/sms-keyword?keyword=#KEYWORD#&NPA=#NPA#&NXX=#NXX#">Back to list</a>
            </h4>
        </div>
    </div>
    <cfif len(RetVarGroupResponse.PHONEERROR) NEQ 0>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12">
                <div class="alert alert-info">
                    Following phone numbers will not be able to receive message. They may have already end the conversation.
                    <cfloop array="#RetVarGroupResponse.PHONEERROR#" index="phone">
                        <cfinvoke component="session.sire.models.cfc.smschat" method="formatContactString" returnvariable="formatedNumber">
                            <cfinvokeargument name="inpContactString" value="#phone#"/>
                        </cfinvoke>
                        <strong>
                            <br/>#formatedNumber#
                        </strong>
                    </cfloop>
                </div>
            </div>
        </div>
    </cfif>
    <cfif len(RetVarGroupResponse["aaData"]) GT 0>
    	<div class="row">
            <div class="col-md-5 col-lg-4">
                <div class="row">
                    <div class="col-lg-10 col-md-10">
    		        	<div class="form-group">
    		        		<div id="GroupToSend" class="form-control">
    	        				<cfloop array="#RetVarGroupResponse["aaData"]#" index="contactResponse">
    								<cfinvoke component="session.sire.models.cfc.smschat" method="formatContactString" returnvariable="formatedNumber">
    								    <cfinvokeargument name="inpContactString" value="#contactResponse.ContactString_vch#"/>
    								</cfinvoke> 
    								#formatedNumber#<br>
    	        				</cfloop>	
    		        		</div>	
    		        	</div>	
    	        	</div>	
            	</div>	
            </div>
            <div class="col-md-7 col-lg-6">
                <div class="row">
                    <div class="col-lg-10 col-md-10">
                        <form id="text-form">
                            <div class="form-group">
                            	<input type="hidden" id="SessionIds" name="inpSessionIds" value="#inpSessionIds#">
                                <textarea id="TextToSend" name="inpTextToSend" class="form-control validate[required]" #disabled#></textarea>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-10 col-md-10">
                        <div class="pull-right">
                            <button type="button" class="btn btn-success btn-success-custom btn-custom_size" id="btn_send" #disabled#>Send</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </cfif>
</div>
</cfoutput>

<cfparam name="variables._title" default="Admin SMS Group Reponses - Sire"> 

<cfinclude template="../views/layouts/master.cfm">