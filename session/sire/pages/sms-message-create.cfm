<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/js/jquery.tmpl.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/js/site/common.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/js/schedule.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/js/message.js">
</cfinvoke>



<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/css/campaign/mycampaign/schedule.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/js/jquery-ui-1.11.4.custom/jquery-ui.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/message.css">
</cfinvoke>


<!--- MINHHTN HOT FIX --->
<cfparam name="Session.AdditionalDNC" default="">

<cfinclude template="/public/sire/configs/paths.cfm">

<cfinclude template="/public/sire/configs/userConstants.cfm">


<!--- remove old method: 
<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>									
--->
<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>

<cfinvoke component="session.sire.models.cfc.subscribers" method="GetSubcriberList" returnvariable="groupList"></cfinvoke>

<cfif groupList.RXRESULTCODE EQ 1 AND groupList.iTotalRecords GT 0>
	<cfset subcriberList = groupList.listData>
	<cfset totalSubcriber = subcriberList[1][3]>
	<cfset group_id = groupList.listData[1][1]>
<cfelse>
	<cfset subcriberList = []>
	<cfset totalSubcriber = 0>
	<cfset group_id = 0>
</cfif>

<cfparam name="batchid" default="">
<cfset disableClass="">

<cfif batchid GT 0>
	<cfinvoke component="session.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
		<cfinvokeargument name="operator" value="#EMS_Edit_Title#">
	</cfinvoke>

	<!--- Check permission against acutal logged in user not "Shared" user--->
	<cfif Session.CompanyUserId GT 0>
	    <cfinvoke component="session.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
	        <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
	    </cfinvoke>
	<cfelse>
	    <cfinvoke component="session.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
	        <cfinvokeargument name="userId" value="#session.userId#">
	    </cfinvoke>
	</cfif>


	<cfinvoke component="session.cfc.administrator.permission" method="havePermission" returnvariable="SMSPermissionStr">
		<cfinvokeargument name="operator" value="#EMS_SMS_Title#">
	</cfinvoke>

	<cfif NOT permissionStr.havePermission 	OR (session.userRole EQ 'CompanyAdmin' AND getUserByUserId.CompanyAccountId NEQ session.companyId)	
		OR (session.userRole EQ 'User' AND getUserByUserId.userId NEQ session.userId) >
		<div style="color:red;">
			You do not have permission to edit new EMS!
		</div>
		<cfexit>
	</cfif>

	<cfinvoke 
		component="session.cfc.emergencymessages" 
		method="GetEmergencyById" 
		returnvariable="EmergencyById">
		<cfinvokeargument name="batchid" value="#batchid#" >
	</cfinvoke>
	<cfset mode = "edit">
	
	<cfif EmergencyById.RXRESULTCODE LT 0>
		<cflocation url="/session/sire/pages/campaign-manage" addtoken="false">
	</cfif>

	<cfset campaign_name = EmergencyById.CAMPAIGNNAME>
	<cfset sms_txt = EmergencyById.SMSDATA.MAINMESSAGE>
	<cfset group_id =  EmergencyById.GROUPID>

	<cfset SCHEDULE = Arraynew(1)>
	<cfif Arraylen(EmergencyById.SCHEDULEBYBATCHID) GT 0>
		<cfset SCHEDULE = EmergencyById.SCHEDULEBYBATCHID>	
	</cfif>

	<cfinvoke component="session.sire.models.cfc.campaign" method="checkCampaignStatus" returnvariable="dataCampaignStatus">
    	<cfinvokeargument name="emsId" value="#batchid#">
	</cfinvoke>

	<cfset campaignStatus = 'InReview'>
	<cfif dataCampaignStatus.RXRESULTCODE EQ 1 AND arrayLen(dataCampaignStatus.ListEMSData) GT 0 >
		<cfset campaignStatus = dataCampaignStatus.ListEMSData[1][3]>	
	</cfif>

	<cfif campaignStatus EQ 'Scheduled'>
		<cfset disableClass="disabled">
	</cfif>

<cfelse>
	<cfset mode = "create">
	<cfset campaign_name = "Campaign "&DateTimeFormat(Now(),'mmm-dd-yyyy HH:nn:ss')>
	<cfset sms_txt = ""> 
</cfif>



<main class="container-fluid page">
	<cfinclude template="../views/commons/credits_available.cfm">
	<section class="row bg-white">
	    <div class="content-header">
	    	<div class="row">
                <div class="col-xs-12 col-sm-5 content-title">
		            <p class="title"> <cfoutput >Your Shortcode : #shortCode.SHORTCODE#</cfoutput></p>
	            </div>
	            <div class="col-xs-12 col-sm-7 content-menu">
	                <cfif mode EQ "create">
	                		<a class="active"><span class="icon-edit"></span><span>Create SMS</span></a>
	                	<cfelse>
	                		<a ><span class="icon-report"></span><span>Report SMS</span></a>
	                		<a class="active"><span class="icon-edit"></span><span>Edit SMS</span></a>
	                		<a ><span class="icon-play"></span><span>Run SMS</span></a>
	                		<a ><span class="icon-stop"></span><span>Stop SMS</span></a>
	                </cfif>
                </div>
	        </div>
	    </div>
	    <hr class="hrt0">
	    <div class="content-body">
	    <form action="##" id="form-sms">
	    	<div class="col-md-6 col-sm-12">
	    		<div class="form-group">
	    			<label for="campaign-name-txt">Campaign Name</label>
	    			<input type="text" name="campaign-name-txt" id="campaign-name-txt" class="validate[required] form-control campaign-name" 
	    			value="<cfoutput>#campaign_name#</cfoutput>" maxlength="255"></input> 
	    			<input type="hidden" id="IVRBatchId" value="<cfoutput>#batchid#</cfoutput>">
			    </div>

	    		<div class="form-group">
                	<label for="exampleInputEmail1">Enter your text messsage below</label>
                	<span class="pull-right total_num_subcribers">
                		<cfoutput>#totalSubcriber#</cfoutput> subcriber<cfif totalSubcriber GT 1>s</cfif>
                	</span>
                	<textarea class="form-control validate[required] custom-message" id="txtSMS" name="txtSMS" data-prompt-position="topLeft:150" rows="5" <cfoutput>#disableClass#</cfoutput>> <cfoutput>#sms_txt#</cfoutput></textarea>
                	<span class="help-block pull-right" id="help-msg-block">&nbsp</span>
              	</div>
              	<div class="clearfix"></div>
              	<div class="form-group">
              		<button type="button" class="btn btn-medium btn-success-custom" data-toggle="modal" data-target="#AddLinkModal"  <cfoutput>#disableClass#</cfoutput>>Add Link</button>
              		<button type="button" class="btn btn-medium btn-success-custom" data-toggle="modal" data-target="#AddFieldModal"  <cfoutput>#disableClass#</cfoutput>>Add Fields</button>
              	</div>

              	<div class="form-inline">
	              	<div class="form-group">
					    <input type="checkbox" name="opt-out-checkbox" id="opt-out-checkbox" <cfoutput>#disableClass#</cfoutput>> 
					    <label for="opt-out-checkbox">Opt-out</label>
				  	</div>
				  	<div class="form-group">
			            <input type="text" name="opt-out-txt" id="opt-out-txt" class="form-control opt-out-txt" value="To opt-out reply STOP" disabled></input> 
			        </div>		
				</div>
				<hr/>
				<div class="form-group">
					<div class="info-block">
						<label for="disabledSelect">Select the Subscriber List</label>
							<cfif arrayLen(subcriberList) GT 0>
								<cfloop array ="#subcriberList#" index="item">
									<cfset checked_group=''>
									<cfif group_id EQ item[1]>
										<cfset checked_group='checked'>
									</cfif>
									<div class="radio">
										<label>
									    <input type="radio" class="validate[required] <cfoutput>#disableClass#</cfoutput>" name="optionsSubscriberList" value="<cfoutput>#item[1]#</cfoutput>" data-total-subcribers="<cfoutput>#item[3]#</cfoutput>" <cfoutput>#disableClass#</cfoutput> <cfoutput>#checked_group#</cfoutput>
									    >
									   	<cfoutput>#item[2]#</cfoutput> (<cfoutput>#item[3]#</cfoutput>)
										</label>
									</div>
								</cfloop>
							<cfelse>
							<p class="help-block"> You don't have any Subscriber List.</p>	
							</cfif>	
					</div>	
				</div>	
				<hr/>
				<div class="form-group">
					<label for="opt-out-txt">Scheduled</label>
					<div class="warning-block">
						<div id="warning-content"><span class="warning-text">Note:</span> Messages are scheduled between 9am - 7pm.</div>
						<a data-toggle="modal" data-target="#ScheduleOptionsModal" href="#">
						Click here to enable alternate delivery times.
						</a>
						<br>
						<div class="clear"></div>
					</div>
				</div>

				<div class="form-group">
					<cfif mode EQ 'edit'>
						<button type="button" class="btn btn-medium btn-success-custom btn-update-for-review" id="btn-update-for-review">Update</button>
	              		<button type="button" class="btn btn-medium btn-success-custom btn-update-lunch" >Update & Launch</button>
					<cfelse>
						<button type="button" class="btn btn-medium btn-success-custom btn-save-for-review" id="btn-save-for-review">Save for Review</button>
	              		<button type="button" class="btn btn-medium btn-success-custom btn-lunch" >Launch Now</button>
					</cfif>
              			<a href="/session/sire/pages" class="btn btn-medium btn-back-custom pull-right" id="btn-cancel">Cancel</a>		
              	</div>

	    	</div>
	    	<div class="col-md-6 col-sm-12">
	            <div class="optin-demo">
	                <div id="QAScreen">
		              	<img class="ScreenBackground" src="/session/sire/images/phonedemo.png">
		                <div id="PhoneScreenArea">
		                  <div id="SMSToAddress"><cfoutput>#shortCode.SHORTCODE#</cfoutput></div>
		                  <div id="SMSHistoryScreenArea">
		                    <div class="bubble you" rel="274"></div>
		              	  </div>
		                </div>
		                <!--<img class="ScreenBackgroundGlare" src="/public/sire/images/iphonebaseglareonly.png">-->
	          		</div>
	            </div>
        	</div>
        </form>		
	    </div>

	</section>

</main>

<!-- Modal -->
<div class="bootbox modal fade" id="ScheduleOptionsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:auto; max-width:900px;">
        <div class="modal-content">
        	<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>            
                 
            </div>
        	<div class="modal-body">
        		<div class="modal-body-inner row">    
		            <div id="AdvanceScheduleContent">
		                <cfinclude template="dsp_advance_schedule.cfm">
		            </div>
		        </div>    
           	</div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<div class="bootbox modal fade" id="ConfirmLaunchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<div class="bootbox modal fade" id="AddLinkModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Add Link</h4>
            </div>
            <div class="modal-body">
            	<div class="row"> 
            		<div class="col-md-12">
	            		<form class="form-horizontal" id="form-add-link"> 
	            			<div class="form-group"> 
	            				<div class="col-md-12">
	            					<input placeholder="http://" class="form-control validate[required,custom[url]]" id="tblLink" >
	            				</div> 
	            			</div> 
	            		</form> 
            		</div>  
            	</div>
            </div>
            <div class="modal-footer">
            	<button type="button" class="btn btn btn-medium btn-success-custom" id="btn-add-link">Add</button>
                <button type="button" class="btn btn btn-medium btn-back-custom" data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserOrganization" returnvariable="userOrgInfo"></cfinvoke>
<cfset _has_Org = false>	
<cfif userOrgInfo.RXRESULTCODE EQ 1>
	<cfset _has_Org = true>
	<cfset orgInfoData = userOrgInfo.ORGINFO>
</cfif>

<div class="bootbox modal fade" id="AddFieldModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Add Field</h4>
      </div>
      <form action="##" name="form-add-field" id="form-add-field" autocomplete="off">
      <div class="modal-body">
        
		<div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Field</th>
                        <th>Value</th>
                        <th>Select</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Organization Name</td>
                        <td><cfif _has_Org><cfoutput>#orgInfoData.OrganizationName_vch#</cfoutput></cfif></td>
                        <td style="text-align:center"><input class="optradio" type="radio" name="optradio" value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationName_vch#</cfoutput></cfif>" checked></td>
                    </tr>
                    <tr>
                        <td>Organization Short Name</td>
                        <td><cfif _has_Org><cfoutput>#orgInfoData.OrganizationBusinessName_vch#</cfoutput></cfif></td>
                        <td style="text-align:center"><input class="optradio" type="radio" name="optradio" value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationBusinessName_vch#</cfoutput></cfif>"></td>
                    </tr>
                    <tr>
                        <td>Phone Number</td>
                        <td><cfif _has_Org><cfoutput>#orgInfoData.OrganizationPhone_vch#</cfoutput></cfif></td>
                        <td style="text-align:center"><input class="optradio" type="radio" name="optradio" value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationPhone_vch#</cfoutput></cfif>"></td>
                    </tr>
                    <tr>
                        <td>Email Address</td>
                        <td><cfif _has_Org><cfoutput>#orgInfoData.OrganizationEmail_vch#</cfoutput></cfif></td>
                        <td style="text-align:center"><input class="optradio" type="radio" name="optradio" value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationEmail_vch#</cfoutput></cfif>"></td>
                    </tr>
                    <tr>
                        <td>Tagline</td>
                        <td><cfif _has_Org><cfoutput>#orgInfoData.OrganizationTagline_vch#</cfoutput></cfif></td>
                        <td style="text-align:center"><input class="optradio" type="radio" name="optradio" value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationTagline_vch#</cfoutput></cfif>"></td>
                    </tr>
                    <tr>
                        <td>Hours of Operation</td>
                        <td><cfif _has_Org><cfoutput>#orgInfoData.OrganizationHOO_vch#</cfoutput></cfif></td>
                        <td style="text-align:center"><input class="optradio" type="radio" name="optradio" value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationHOO_vch#</cfoutput></cfif>"></td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td><cfif _has_Org><cfoutput>#orgInfoData.OrganizationAdd_vch#</cfoutput></cfif></td>
                        <td style="text-align:center"><input class="optradio" type="radio" name="optradio" value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationAdd_vch#</cfoutput></cfif>"></td>
                    </tr>
                    <tr>
                        <td>Zip Code</td>
                        <td><cfif _has_Org><cfoutput>#orgInfoData.OrganizationZipcode_vch#</cfoutput></cfif></td>
                        <td style="text-align:center"><input class="optradio" type="radio" name="optradio" value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationZipcode_vch#</cfoutput></cfif>"></td>
                    </tr>
                    <tr>
                        <td>Website</td>
                        <td><cfif _has_Org><cfoutput>#orgInfoData.OrganizationWebsite_vch#</cfoutput></cfif></td>
                        <td style="text-align:center"><input class="optradio" type="radio" name="optradio" value="<cfif _has_Org><cfoutput>#orgInfoData.OrganizationWebsite_vch#</cfoutput></cfif>"></td>
                    </tr>
                    <cfloop array="#_UserCustomFields#" index="index">
	                    <tr>
	                        <td><cfoutput>#index#</cfoutput></td>
	                        <td><cfoutput>{%#index#%}</cfoutput></td>
	                        <td style="text-align:center"><input class="optradio" type="radio" name="optradio" value="<cfoutput>{%#index#%}</cfoutput>"></td>
	                    </tr>
                    </cfloop>
                </tbody>
            </table>
        </div>

        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-medium btn-success-custom" id="btn-add-field">Add</button>
        <button type="button" class="btn btn-medium btn-back-custom" data-dismiss="modal">Cancel</button>
      </div>
      </form>
    </div>
  </div>
</div>



<script TYPE="text/javascript">
	var mode = '<cfoutput>#mode#</cfoutput>';
	var INPBATCHID = '<cfoutput>#batchid#</cfoutput>';
	var schedule_data = "";
	var SHORTCODE = "<cfoutput>#shortCode.SHORTCODE#</cfoutput>";
	
	var date_txt_1 = '<cfoutput>#LSDateFormat(DateAdd('d', 30, Now()), 'm-d-yyyy')#</cfoutput>';
	var date_txt_2 = '<cfoutput>#LSDateFormat(NOW(), 'm-d-yyyy')#</cfoutput>';

	var ScheduleTimeData;
</script>

<cfinclude template="../views/layouts/main.cfm">