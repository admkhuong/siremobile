
<!--- GET USER INFO --->
<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke>
<cfif userInfo.USERTYPE NEQ 2>
    <cfabort>
</cfif>

<cfset variables.menuToUse = 2 />
<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("../assets/global/plugins/daterangepicker/daterangepicker.css", true)        
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)
        .addJs("../assets/global/plugins/daterangepicker/moment.min.js", true)
        .addJs("../assets/global/plugins/daterangepicker/daterangepicker.js", true)
        .addJs('/session/sire/assets/global/scripts/filter-bar-generator.js', true)  
        .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)      
        
        .addJs("/public/sire/js/select2.min.js")
        
        .addJs("../assets/pages/scripts/sub-account.js");
</cfscript>


<cfoutput>
<div class="portlet light bordered">
    <div class="portlet light bordered">
        <div class="portlet-body form">  
            
            <div class="row">
                <div class="col-sm-3 col-md-4">
                    <h2 class="page-title">Sub account List</h2>
                </div>
                <div class="col-sm-9 col-md-8">
                    <div id="box-filter" class="form-gd">

                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12">
                    <div class="re-table table-data">
                        <div class="table-responsive">
                            <form name="frSubAcc" id="frSubAcc">
                            <table id="sub-account-list" class="table table-striped table-responsive">
                
                            </table>
                            </form>
                        </div>
                    </div>                
                </div>
            </div>                   
            
        </div>
    </div>
</div>
<div class="modal fade" id="mdUpdateSubAccBilling" role="dialog">
    <form name="frUpdateSubAccBilling" class="col-sm-12" id="frUpdateSubAccBilling">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <input type="hidden" id="subAccId" value="0"/>
                <b><h4 class="modal-title" id="modal-tit">
                UPDATE SUB ACCOUNT BILLING
                </h4></b>	
            </div>
            <div class="modal-body">	
                <div class="modal-body-inner"> 
                    <div class="form-group">
                        <label>Monthly Credit Limit</label>
                        <input type="text" class="form-control validate[required]" id="creditVal" value="">                        
                    </div>
                    <div class="form-group">
                        <label>Threshold Alert</label>
                        <input type="text" class="form-control validate[required]" id="thresHoldVal" value="">                        
                    </div>                      
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green-cancel" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn green-gd" id="btnUpdateMessage">Save</button>
            </div>
        </div>
    </div>
    </form>
</div>

</cfoutput>

<cfparam name="variables._title" default="Sub account management">
<cfinclude template="../views/layouts/master.cfm">
