<cfparam name="url.contactId" default="0">
<cfif !isNumeric(url.contactId) OR url.contactId LT 1>
	<cflocation url="/session/sire/pages/subscriber" addtoken="false">
</cfif>

<cfinvoke component="session.sire.models.cfc.subscribers" method="getSubscriber4Edit" returnvariable="contactData">
	<cfinvokeargument name="inpContactId" value="#url.contactId#">
</cfinvoke>

<cfif contactData.GetContactStringData.ContactType_int != 4>
	<cfset phone_number = '(' & Left(contactData.GetContactStringData.ContactString_vch,3)& ') '& MID(contactData.GetContactStringData.ContactString_vch,4,3)& '-' &RIGHT(contactData.GetContactStringData.ContactString_vch,4)/>
<cfelse>
	<cfset phone_number = contactData.GetContactStringData.ContactString_vch/>
</cfif>

<cfoutput>
	<div class="portlet light bordered subscriber">
		<div class="portlet-body form">
			<h2 class="page-title">Subscriber Edit </h2>
			<h3 class="">
				Contact: #phone_number#
				<a href="javascript:history.go(-1)" class="pull-right">Back</a>
			</h3><br/>
			<div class="row">
				<div class="col-md-6">
					<div class="row form-group">
						<label class="col-md-6 control-label">Mobile Phone Carrier:</label>
						<label class="col-md-6 control-label">#contactData.carrierQuery.Operator_vch#&nbsp;</label>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row form-group">
						<label class="col-md-6 control-label">Last Subscription Date:</label>
						<label class="col-md-6 control-label">
							<cfif contactData.GetContactStringData.OptIn_dt GT ''>
								#dateFormat(contactData.GetContactStringData.OptIn_dt, "mm/dd/yyyy")#
							<cfelse>
								#dateFormat(contactData.GetContactStringData.LastUpdated_dt, "mm/dd/yyyy")#
							</cfif>
						</label>
					</div>
				</div>
			</div>
			<!--- <div class="row">
				<div class="col-md-6">
					<div class="row form-group">
						<label class="col-md-6 control-label">Subscriber State:</label>
						<label class="col-md-6 control-label" id="SubscriberState">
							<cfif contactData.optoutLastest.RecordCount GT 0>
								Unsubscribed
							<cfelse>
								Subscribed
							</cfif>
						</label>
					</div>	
				</div>
				<div class="col-md-6">
					<div class="row form-group">
						<label class="col-md-6 control-label">Subscription Type:</label>
						<label class="col-md-6 control-label">
							<cfif contactData.Keyword GT ''>
								#contactData.Keyword#
							<cfelse>
								CPP
							</cfif>
						</label>
					</div>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-md-6">
					<div class="row form-group">
						<label class="col-md-6 control-label">Unsubscribe Date:</label>
						<label class="col-md-6 control-label" id="unsubscribeView">
							<cfif contactData.optoutLastest.RecordCount GT 0>
								#dateFormat(contactData.GetContactStringData.OptOut_dt,  "mm/dd/yyyy")#
							<cfelse>
								<a href="##" id="unsubscribe" data-contact-string="#contactData.GetContactStringData.ContactString_vch#">Unsubscribe</a>
							</cfif>
						</label>
					</div>
				</div>
				<!--- <div class="col-md-6">
					<div class="row form-group">
						<label class="col-md-6 control-label">First Name:</label>
						<label class="col-md-6 control-label mb5" id="firstNameView">
							<span class="control-view"><!--- #htmlEditFormat(firstName.value)# ---></span>
							<a href="##" class="btn-cf-edit">Edit</a>
						</label>
					</div>
				</div> --->
			</div> --->

			<cfset GetContactStringData = contactData.GetContactStringData />
			<cfset ListContactStrings = contactData.ListContactStrings />
			<cfset GetCustomFieldsData = contactData.GetCustomFieldsData />
			<cfset GetCustomFields = contactData.GetCustomFields />
			<cfset GetListDefaultValuesCDFOfUser = contactData.GetListDefaultValuesCDFOfUser />
			
			
			<cfset _i = 0>
			<cfloop query="GetCustomFields">
				<cfset _i = (_i + 1) % 2 />
				<cfif _i EQ 1>
					<div class="row">
				</cfif>
						<div class="col-md-6">
							<div class="row form-group">
								<div class="col-md-6">
									<label class="control-label">#GetCustomFields.VariableName_vch#:</label>
								</div>
								<div class="col-md-6 control-cf-view">
									<span class="control-view">
									<cfif (GetCustomFieldsData[ "CdfId_int" ].IndexOf( JavaCast( "int", "#GetCustomFields.CdfId_int#" ) ) GTE 0)>
										<cfif GetCustomFields.type_field EQ 1>
											<cfset cdfValue = 'N/A' />
											<cfloop query="GetListDefaultValuesCDFOfUser">
												<cfif GetListDefaultValuesCDFOfUser.CdfId_int EQ GetCustomFields.CdfId_int>
													<cfif GetCustomFieldsData.VariableValue_vch[GetCustomFieldsData[ "CdfId_int" ].IndexOf( JavaCast( "int", "#GetCustomFields.CdfId_int#" ) ) + 1] EQ GetListDefaultValuesCDFOfUser.id>
														#htmlEditFormat(GetListDefaultValuesCDFOfUser.default_value)#
														<cfset cdfValue = '' />
														<cfbreak/>
													</cfif>
												</cfif>
											</cfloop>
											#cdfValue#
										<cfelse>
											<cfset cdfValue = GetCustomFieldsData.VariableValue_vch[GetCustomFieldsData[ "CdfId_int" ].IndexOf( JavaCast( "int", "#GetCustomFields.CdfId_int#" ) ) + 1]/>
											#cdfValue NEQ '' ? htmlEditFormat(cdfValue) : 'N/A'# 
										</cfif>
									<cfelse>
									    <cfif GetCustomFields.type_field GT 0>
											<cfset cdfValue = 'N/A' />											
											<cfloop query="GetCustomFieldsData">												
												<cfif GetCustomFieldsData.CdfId_int EQ GetCustomFields.CdfId_int>
													<cfset cdfValue=GetCustomFieldsData.VariableValue_vch>
												</cfif>
											</cfloop>
											#cdfValue#
										<cfelse>
											N/A
										</cfif>
									</cfif>
									</span>
									<a href="##" class="btn-cf-edit">Edit</a>
								</div>
								<div class="col-md-6 control-cf-edit">
									<cfif (GetCustomFieldsData[ "CdfId_int" ].IndexOf( JavaCast( "int", "#GetCustomFields.CdfId_int#" ) ) GTE 0)>
										<cfif GetCustomFields.type_field EQ 1>
											<select data-cfv-id="#GetCustomFields.CdfId_int#" name="#GetCustomFields.CdfId_int#" class="form-control default-value-cdf" autocomplete="off">
												<cfloop query="GetListDefaultValuesCDFOfUser">
													<cfif GetListDefaultValuesCDFOfUser.CdfId_int EQ GetCustomFields.CdfId_int>
														<cfif GetCustomFieldsData.VariableValue_vch[GetCustomFieldsData[ "CdfId_int" ].IndexOf( JavaCast( "int", "#GetCustomFields.CdfId_int#" ) ) + 1] EQ GetListDefaultValuesCDFOfUser.id>
															<option value="#GetListDefaultValuesCDFOfUser.id#" selected="selected">#GetListDefaultValuesCDFOfUser.default_value#</option>
														<cfelse>
															<option value="#GetListDefaultValuesCDFOfUser.id#">#GetListDefaultValuesCDFOfUser.default_value#</option>
														</cfif>
													</cfif>
												</cfloop>
											</select>
										<cfelse>
											<input type="text" data-cfv-id="#GetCustomFields.CdfId_int#" name="#GetCustomFields.CdfId_int#" class="form-control validate[maxSize[459]]" maxlength="459" value="#GetCustomFieldsData.VariableValue_vch[GetCustomFieldsData[ "CdfId_int" ].IndexOf( JavaCast( "int", "#GetCustomFields.CdfId_int#" ) ) + 1]#"/>
										</cfif>
									<cfelse>
										<cfif GetCustomFields.type_field EQ 1>
											<select data-cfv-id="#GetCustomFields.CdfId_int#" name="#GetCustomFields.CdfId_int#" class="form-control default-value-cdf" autocomplete="off">
												<cfloop query="GetListDefaultValuesCDFOfUser">
													<cfif GetListDefaultValuesCDFOfUser.CdfId_int EQ GetCustomFields.CdfId_int>
														<cfif GetCustomFieldsData.VariableValue_vch[GetCustomFieldsData[ "CdfId_int" ].IndexOf( JavaCast( "int", "#GetCustomFields.CdfId_int#" ) ) + 1] EQ GetListDefaultValuesCDFOfUser.id >
															<option value="#GetListDefaultValuesCDFOfUser.id#" selected="selected">#GetListDefaultValuesCDFOfUser.default_value#</option>
														<cfelse>
															<option value="#GetListDefaultValuesCDFOfUser.id#">#GetListDefaultValuesCDFOfUser.default_value#</option>
														</cfif>
													</cfif>
												</cfloop>
											</select>
										<cfelse>
											<input type="text" data-cfv-id="#GetCustomFields.CdfId_int#" name="#GetCustomFields.CdfId_int#" class="form-control validate[maxSize[459]]" maxlength="459" value=""/>
										</cfif>
									</cfif>
									<div class="control-cf-btns">
										<button type="button" class="btn btn-default btn-xs btn-save" disabled="disabled">
											<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
										</button>
										<button type="button" class="btn btn-default btn-xs btn-cancel">
											<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
										</button>
									</div>
								</div>
							</div>
						</div>
				<cfif _i EQ 0>
					</div>
				</cfif>
			</cfloop>
				<cfif _i EQ 1>
					</div>
				</cfif>

		</div>
	</div>
	
	<input type="hidden" id="contactid" value="#GetContactStringData.ContactId_bi#" />
</cfoutput>




<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
    	.addJs("../assets/pages/scripts/subscriber_edit.js")
    	//.addJs("/session/sire/js/subscriber_edit.js")
    	;
</cfscript>

<cfparam name="variables._title" default="Subcriber list - Sire">
<cfinclude template="../views/layouts/master.cfm">
