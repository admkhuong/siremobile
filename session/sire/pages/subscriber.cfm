
<cfinvoke method="GetTotalOptOutByUserId" component="session.sire.models.cfc.subscribers" returnvariable="rtGetTotalOptOutByUserId">

</cfinvoke>

<div class="portlet light bordered subscriber">
    <div class="portlet-body form">
        <h2 class="page-title">Subscribers</h2>
        <div class="options">
	        <div class="row">
	        	<div class="col-xs-12 col-sm-3">
	        		<div class="select-list">
	        			<p><b>Selected List</b></p>
		                <select name="subcriber-list" class="form-control select2">
                            <option value="0">All</option>
		                </select>
		            </div>
	        	</div>
	        	<div class="col-xs-12 col-sm-6 col-sm-offset-3">
	        		<div class="date-range">

	        			<p><b>Selected Date Range</b></p>
						<div class="reportrange pull-right">
						    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
						    <span></span> <b class="caret"></b>
						</div>
                        <input type="hidden" name="dateStart" value="">
                        <input type="hidden" name="dateEnd" value="">
	        		</div>
	        	</div>
	        </div>
        </div>
        <!--- options select --->

        <div id="highchart" class="subscriber-highchart-details">

        </div>


        <!--- charts --->
        <div class="row">
    	<div class="col-lg-8 col-md-12 left-group-contact">
                <div class="row">
                    <div class="col-md-6 col-xs-6 col-lg-6 col-sm-6" id="add-newsldiv">
                        <button type="button" id="btn-add-new" class="btn green-gd btn-re">ADD NEW LIST</button>
                    </div>
                    <div class="col-md-6 col-xs-6 col-lg-6 col-sm-6" id="subscribered-countdiv">
                        <label class="label-search-subscriber" id="subscribered-count">
                            <!--- <strong>Subscribed: <span id="total-subcriber"></span></strong>
                            - ---><strong>Total Opted-out: <span id="totalOptout"><cfoutput>#rtGetTotalOptOutByUserId.TOTALOPTOUT#</cfoutput></span></strong>
                        </label>

                    </div>

                </div>

		        <div class="subscriber-list">
        	        <div class="re-table">
        	        	<div class="table-responsive">
        	                <table id="tblSubcriberList" class="table table-bordered table-striped">

        	                </table>
        	            </div>
        	        </div>
		        </div>
        	</div>
        	<div class="col-lg-4 col-md-12">
    			<div class="search-contact">
	    			<div class="form-group">
	        			<div class="row">
	                        <div class="col-md-12">
	                            <div class="search-subscriber">
		                            <div class="form-group">
			                            <div class="input-group">
				                            <input type="text" name="contact-keyword" class="form-control" placeholder="Search Contacts">
                                            <input type="hidden" name="sub-id" value="">
				                            <span class="input-group-addon btn-search-contact">
				                                <i class="fa fa-search" aria-hidden="true"></i>
				                            </span>
				                        </div>
		                            </div>
	                            </div>
	                        </div>
	                    </div>
	        		</div>
    			</div>
		        <div class="subscriber-phone-list">
        	        <div class="re-table">
        	        	<div class="table-responsive">
        	                <table id="subcriberListDetail" class="table table-bordered table-hover">

        	                </table>
        	            </div>
        	        </div>
		        </div>
        	</div>
        </div>
    </div>
</div>
<!-- Modal rename template -->
<div class="modal fade" id="rename-subscriber-list" role="dialog">
    <div class="modal-dialog">
        <form id="rename-subscriber-list-form" name="rename-subscriber-list-form">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><strong>RENAME SUBSCRIBER LIST</strong></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>New subscriber list name:</label>
                        <input type="hidden" id="contact-group-id">
                        <input type="text" name="new-subscriber-list-name" class="form-control validate[required]" id="new-subscriber-list-name" maxlength="255">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-back-custom" data-dismiss="modal">Cancel</button>
                    &nbsp; &nbsp; &nbsp;
                    <button type="submit" class="btn-save btn btn-success-custom" >Save</button>
                </div>
            </div>
        </form>
    </div>
</div>


<!-- Modal -->
<div class="bootbox modal fade" id="AddNewSubscriberList" tabindex="-1" role="dialog" aria-labelledby="AddNewSubscriberList" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form name="frm-add-new">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title title">Add New Subscriber List</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="subscriber_list_name">Subscriber List Name:<span class="text-danger"> *</span></label>
                        <input type="text" class="form-control validate[required, custom[onlyLetterNumberSp]]" id="subscriber_list_name" maxlength="255">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-back-custom" data-dismiss="modal">Cancel</button>
                    &nbsp; &nbsp; &nbsp;
                    <button type="submit" class="btn btn-success-custom btn-save-group"> Save </button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<div class="bootbox modal fade bs-example-modal-lg" id="ContactFormEdit" tabindex="-1" role="dialog" aria-labelledby="ContactFormEdit" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="subscribed_contact_form" name="subscribed_contact_form" method="post" action="/session/sire/models/cfc/subscribers.cfc?method=setContactStringData">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title title">Edit contact</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-back-custom" data-dismiss="modal">Cancel</button>
                    &nbsp; &nbsp; &nbsp;
                    <button type="submit" class="btn btn-success-custom btn-save-contact"> Save </button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("../assets/global/plugins/daterangepicker/daterangepicker.css", true)
        .addCss("/session/sire/assets/global/plugins/Chartist/chartist.min.css", true)
        .addCss("/session/sire/assets/global/plugins/Chartist/plugins/tooltips/chartist-plugin-tooltip.css", true)
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)
        .addJs("../assets/global/plugins/daterangepicker/moment.min.js", true)
        .addJs("../assets/global/plugins/daterangepicker/daterangepicker.js", true)
        .addJs("../assets/pages/scripts/script-04.js")
        .addJs("/public/sire/js/select2.min.js")
        .addJs("/session/sire/assets/global/plugins/Chartist/chartist.min.js", true)
        .addJs("/session/sire/assets/global/plugins/Chartist/plugins/tooltips/chartist-plugin-tooltip.js", true)
        .addJs("../assets/pages/scripts/subscriber-list.js");
</cfscript>

<cfparam name="variables._title" default="Subscriber List - Sire">
<cfinclude template="../views/layouts/master.cfm">
