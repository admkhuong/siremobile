
<!--- remove old method: 
<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>									
--->
<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>
<cfoutput>
<main class="container-fluid page">
    <cfinclude template="../views/commons/credits_available.cfm">
    <section class="row bg-white">
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-6 content-title">
                	<a class="active"><span class="icon-subscribers"></span><span>Subscribers</span></a>
                    <!---<p class="title">#campaignDetail.BatchObject.Desc_vch# Campaign (#shortCode.SHORTCODE#)</p>
                    <cfif keyword.RXRESULTCODE EQ 1>
                    	<p class="sub-title">Broadcast: <span class="active">Text #keyword.KEYWORD# to #shortCode.SHORTCODE# to subscribe</span></p>
                    <cfelse>
                    	<p class="sub-title"><a class="active" href="##">Set Keywords</a></p>
                    </cfif>--->
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6 content-menu">
				    <!---<a href="/session/sire/pages/campaign-home?id=#url.id#"><span class="icon-home"></span><span>Home</span></a>
				    <a href="##"><span class="icon-message"></span><span>Message</span></a>
				    <a class="active" href="##"><span class="icon-subscribers"></span><span>Subscribers</span></a>
				    <a href="##"><span class="icon-optin"></span><span>Opt-In</span></a>--->
                </div>
            </div>
        </div>
        <hr class="hrt0">
        <div class="container-fluid content-body">
        	<div class="row">
	        	<div class="col-lg-5">
					<div class="heading-title">
						Subscribers List
						<button type="button" class="btn btn-success-custom pull-right" id="btn-add-new"> Add New </button>
					</div>
					<div class="table-responsive">
						<table id="tblListGroupContact" class="table table-striped table-bordered table-hover">
						</table>
					</div>
					<br>
	        	</div>
	        	<div class="col-lg-7">
					<div class="form-inline heading-title heading-tools">
						<select id="searchOptInOut" class="form-control input-sm hidden">
							<!--- <option value="-1">All</option> --->
							<option value="1">Subscribed</option>
							<!--- <option value="0">Unsubscribed</option> --->
						</select>
                        <span>Subscribed</span>
						<span id="subscriberTotal"></span>
						<input id="searchContact" class="form-control input-sm" placeholder="Search Contacts">
						
						
						<a class="download-subscribers" href="##">Download Subscribers</a>
					</div>
					<div class="table-responsive">
						<table id="tblListContact" class="table table-striped table-bordered table-hover">
						</table>
					</div>
	        	</div>
        	</div>
    	</div>
    </section>
</main>
</cfoutput>

<!-- Modal -->
<div class="bootbox modal fade" id="AddNewSubscriberList" tabindex="-1" role="dialog" aria-labelledby="AddNewSubscriberList" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        	<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title title">Add New Subscriber List</h4>            
            </div>
        	<div class="modal-body">
        		<form id="subscriber_list_form" class="row">
        			<label for="subscriber_list_name" class="col-sm-4">
        				Subscriber List Name:<span class="text-danger">*</span>
        			</label>
        			<div class="col-sm-8">
        				<input id="subscriber_list_name" class="form-control validate[required, custom[onlyLetterNumberSp]]" maxlength="255">
        			</div>
        		</form>
           	</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-success-custom btn-save-group"> Save </button>
                &nbsp; &nbsp; &nbsp; 
                <button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!--- Alert popup --->
<div class="modal fade" id="bootstrapAlert" tabindex="-1" role="dialog" aria-labelledby="alertLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-body-inner row">
        	<h4 class="col-sm-10 modal-title"></h4>
        	<p class="col-sm-12 alert-message">       		
        	</p>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Modal rename template -->
<div class="modal fade" id="rename-subscriber-list" role="dialog">
    <div class="modal-dialog">
        <form id="rename-subscriber-list-form" name="rename-subscriber-list-form">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><strong>RENAME SUBSCRIBER LIST</strong></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>New subscriber list name:</label>
                        <input type="hidden" id="contact-group-id">
                        <input type="text" name="new-subscriber-list-name" class="form-control validate[required]" id="new-subscriber-list-name">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn-save btn btn-success-custom" >Save</button>
                    <button type="button" class="btn btn-back-custom" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>
    </div>
</div>


<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/subscribers.js">
</cfinvoke>

<script type="text/javascript">
    var shortCode = "<cfoutput>#shortCode.SHORTCODE#</cfoutput>";
</script>

<cfparam name="variables._title" default="Subscribers - Sire">

<cfinclude template="../views/layouts/main.cfm">