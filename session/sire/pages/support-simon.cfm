<cfset variables.menuToUse = 2 />

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/contact_us.js">
</cfinvoke>


<div class="portlet light bordered">
	<div class="portlet-body">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h4 class="portlet-heading">Support</h4>
					<form name="contact-us" class="row contact-form" id="contact_form">
						<div class="form-group col-sm-4">
							<!--<input type="text" class="form-control" name="infoName" maxlength="120" placeholder="Name" required>-->
							<input type="text" class="form-control validate[required,custom[noHTML]]" id="infoName" name="infoName" maxlength="120" placeholder="Name" data-prompt-position="topLeft:100"/>
						</div>
						<div class="form-group col-sm-8">
							<!--<input type="text" class="form-control" name="infoContact" maxlength="250" placeholder="Contact Address (Email or Phone)" >-->
							<input type="text" class="form-control validate[required,custom[email_or_phone]]" id="infoContact" name="infoContact" maxlength="250" placeholder="Contact Address (Email or Phone)" data-prompt-position="topLeft:100"/>
						</div>
						<div class="form-group col-sm-12">
							<!--<input type="text" class="form-control" name="infoSubject" maxlength="250" placeholder="Subject" >-->
							<input type="text" class="form-control validate[required,custom[noHTML]]" id="infoSubject" name="infoSubject" maxlength="250" placeholder="Subject" data-prompt-position="topLeft:100"/>
						</div>
						<div class="form-group col-sm-12">
							<!--<textarea class="form-control" name="infoMsg" placeholder="Message" rows="6" maxlength="3000" ></textarea>-->
							<textarea class="form-control validate[required,custom[noHTML]]" id="infoMsg" name="infoMsg" placeholder="Message" rows="6" maxlength="3000" data-prompt-position="topLeft:100"></textarea>
						</div>
						<div class="col-sm-12 contact-us-message"></div>
						<div class="form-group col-sm-12 submit-contact">
							<button type="submit" class="btn btn-success btn-success-custom btn-send-message pull-right">Send Message</button>
						</div>
					</form>
				</div>
			</div>
	</div>
</div>




<cfparam name="variables._title" default="Support - Sire">

<cfinclude template="../views/layouts/master.cfm">

<!--- Include javascript here for better cf documentation --->
