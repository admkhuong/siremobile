<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js">
</cfinvoke>


<style>

	body
	{
		background-color:#FFFFFF;		
	}
		

</style>

<main class="container-fluid page">
  <cfinclude template="../views/commons/credits_available.cfm">
  <section class="row">
      
    <div class="content-body">
        <div class="col-md-12">
        
        
			<cfinclude template="../../../public/sire/pages/cs-transactional-api-inc.cfm">

  		</div>
      </div>
  </section>

</main>


<cfinclude template="../views/layouts/main.cfm">


<script type="application/javascript">

(function($){
	$("#http-api-form").validationEngine({promptPosition : "topRight", scroll: false});

	$("#btn_build_URL").click(function() {
		
		var HTTPSAPIURL = 'https://api.siremobile.com/ire/secure/triggerSMS?inpContactString=' + encodeURIComponent($("#inpContactString").val()) + '&inpBatchId=' + encodeURIComponent($("#inpBatchId").val()) + '&inpUserName=' + encodeURIComponent($("#inpUserName").val()) + '&inpPassword=' + encodeURIComponent($("#inpPassword").val()) ;
		
		$("#HTTPSAPIURL").html(HTTPSAPIURL);
		
		
	var HTTPSAPIJSON = '{\n'
       +'"inpContactString" : "'+ encodeURIComponent($("#inpContactString").val()) + '",\n'
       +'"inpBatchId"  : "' + encodeURIComponent($("#inpBatchId").val()) + '",\n'
       +'"inpUserName" : "' + encodeURIComponent($("#inpUserName").val()) + '",\n'
	   +'"inpPassword" : "' + encodeURIComponent($("#inpPassword").val())  + '"\n'
       +'}';
		
		$("#HTTPSAPIJSON").html(HTTPSAPIJSON);
			
	});	
	
	$("#btn_build_JSON").click(function() {		
		
		var HTTPSAPIJSON = '{\n'
       +'"inpContactString" : "'+ encodeURIComponent($("#inpContactString").val()) + '",\n'
       +'"inpBatchId"  : "' + encodeURIComponent($("#inpBatchId").val()) + '",\n'
       +'"inpUserName" : "' + encodeURIComponent($("#inpUserName").val()) + '",\n'
	   +'"inpPassword" : "' + encodeURIComponent($("#inpPassword").val())  + '"\n'
       +'}';
		
		$("#HTTPSAPIJSON").html(HTTPSAPIJSON);
			
	});	

	$("#inpContactString").mask("(000) 000-0000");
    
	var HTTPSAPIURL = 'https://api.siremobile.com/ire/secure/triggerSMS?inpContactString=' + encodeURIComponent($("#inpContactString").val()) + '&inpBatchId=' + encodeURIComponent($("#inpBatchId").val()) + '&inpUserName=' + encodeURIComponent($("#inpUserName").val()) + '&inpPassword=' + encodeURIComponent($("#inpPassword").val()) ;
	$("#HTTPSAPIURL").html(HTTPSAPIURL);
	
	var HTTPSAPIJSON = '{\n'
       +'"inpContactString" : "'+ encodeURIComponent($("#inpContactString").val()) + '",\n'
       +'"inpBatchId"  : "' + encodeURIComponent($("#inpBatchId").val()) + '",\n'
       +'"inpUserName" : "' + encodeURIComponent($("#inpUserName").val()) + '",\n'
	   +'"inpPassword" : "' + encodeURIComponent($("#inpPassword").val())  + '"\n'
       +'}';
		
	$("#HTTPSAPIJSON").html(HTTPSAPIJSON);
		
		
})(jQuery);


</script>
