



<style>

	body
	{
		background-color:#FFFFFF;		
	}
		

</style>

<main class="container-fluid page">
  <cfinclude template="../views/commons/credits_available.cfm">
  <section class="row">
      
    <div class="content-body">
        <div class="col-md-12">
        
        
			
			
			

		<section class="main-wrapper feature-main" id="feature-main">
		
		
			<div class="feature-white no-padding-bottom">
		        <div class="container">
		            <div class="row">
						
						
		                
						<div class="col-sm-12 col-lg-8 col-lg-offset-2">
							<div class="feature-title" id="feature1">What can I do?</div>
		    					 
<!--- 		                    <img class="feature-img-no-bs" alt="" src="../../../public/sire/images/learning/https-api.png"> --->

								<ul>
									<li>Respond to SMS Keyword triggers with customized Messaging Campaigns</li>
									<li>Build Lists for Marketing</li>
									<li>SMS Blasts to your Lists</li>
									<li>Trigger transactional SMS to your customers</li>
									<li>Coming Soon: Marketing Landing Pages</li>	
									<li>Coming Soon: Customer Preference Portals</li>									
								</ul>
							
		                      
		                </div>
		         	
		            </div>
		        </div>
		    </div>
		
		
		
			<div class="feature-white no-padding-bottom">
		        <div class="container">
		            <div class="row">
						
						
		                
						<div class="col-sm-12 col-lg-8 col-lg-offset-2">
							<div class="feature-title" id="feature1">Respond to SMS Keyword triggers with customized Messaging Campaigns</div>
		    					 
								<p>A keyword is the word or phrase that you or your customer needs to send as an SMS message to the short code address to trigger a campaign response.</p><p><ul><li>Keywords must be unique.</li><li>Avoid special characters</li></ul></p>
			                 
		                </div>
		     		
		            </div>
		        </div>
		    </div>
		    
		    
		</section>
		
  		</div>
      </div>
  </section>

</main>






<cfinclude template="../views/layouts/main.cfm">


