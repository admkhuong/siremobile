<cfset variables.menuToUse = 2 />

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/js/site/support_user.js">
</cfinvoke>

<div class="portlet light bordered">
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <h4 class="portlet-heading">Support</h4>
                <p><strong class="title-from-support">
                How can we help you?
                </strong></p>                
                <p><strong class="title-from-support">
                We're here for you every step of the way. Send us your question or a detailed description of the issue you are experiencing and a support specialist will reply to you within 1 business day.
                </strong></p>
                
                <div class="row">
                <form name="support-form" class="support-form" id="support_form">
                    <div class="form-group col-sm-4">
                        <!--<input type="text" class="form-control" name="infoName" maxlength="120" placeholder="Name" required>-->
                        <input type="text" class="form-control validate[required,custom[noHTML]]" id="infoName" name="infoName" maxlength="120" placeholder="Name" data-prompt-position="topLeft:100"/>
                    </div>
                    <div class="form-group col-sm-8">
                        <!--<input type="text" class="form-control" name="infoContact" maxlength="250" placeholder="Contact Address (Email or Phone)" >-->
                        <input type="text" class="form-control validate[required,custom[email_or_phone]]" id="infoContact" name="infoContact" maxlength="250" placeholder="Email" data-prompt-position="topLeft:100"/>
                    </div>
                    <div class="form-group col-sm-12">
                        <!--<input type="text" class="form-control" name="infoSubject" maxlength="250" placeholder="Subject" >-->
                        <input type="text" class="form-control validate[required,custom[noHTML]]" id="infoSubject" name="infoSubject" maxlength="250" placeholder="Subject" data-prompt-position="topLeft:100"/>
                    </div>
                    <div class="form-group col-sm-12">
                        <!--<textarea class="form-control" name="infoMsg" placeholder="Message" rows="6" maxlength="3000" ></textarea>-->
                        <textarea class="form-control validate[required,custom[noHTML]]" id="infoMsg" name="infoMsg" placeholder="Message" rows="6" maxlength="3000" data-prompt-position="topLeft:100"></textarea>
                    </div>
                    <div class="col-sm-11 contact-us-message"></div>
                    <div class="form-group col-sm-12 ">
                        <button type="submit" class="btn btn-success btn-success-custom btn-send-message pull-right" id="send-support">Send Message</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js">
</cfinvoke>

<cfparam name="variables._title" default="Support - Sire">

<cfinclude template="../views/layouts/master.cfm">
<!--- 
<script type="text/javascript">
    $('.support').addClass('active');
</script> --->