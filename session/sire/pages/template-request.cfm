<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/template-request.css">
</cfinvoke>

<section class="main-wrapper" id="feature-main">


		<div class="container text-center">
               
                   <h2 class="heading-home">We will give you the template that you want to use for your campaign.</h2>
        
        </div>
        
        
<div class="container">
	<div class="row contact-func">
		<h3>Please describe specific template you want:</h3>	
		<div class="col-sm-8 template-request-content">

			<form name="template-detail" class="row template-request-form" id="template_form">
				<div class="form-group">
					<input type="hidden" id="user-id" name="user-id" value="<cfoutput>#Session.USERID#</cfoutput>">
				</div>

				<div class="form-group">
					<input type="hidden" id="user-fullname" name="user-fullname" value="<cfoutput>#Session.FULLNAME#</cfoutput>">
				</div>

				<div class="form-group">
					<input type="hidden" id="user-email" name="user-email" value="<cfoutput>#Session.USERNAME#</cfoutput>">
				</div>

				<div class="form-group">
					<input type="hidden" id="user-firstname" name="user-firstname" value="<cfoutput>#Session.FIRSTNAME#</cfoutput>">
				</div>

				<div class="form-group col-sm-11">
				    <label>Template Name <span class="text-danger">*</span></label>
					<input type="text" id="template-request-name" class="form-control validate[required,custom[noHTML]]" id="number-of-question" name="number-of-question" maxlength="255" data-prompt-position="topLeft:100" />
				</div>

				<div class="form-group">
					<label class="col-sm-11">How many messages or questions do you want to send to your subscribers? (Limit to 15 messages) <span class="text-danger">*</span></label>
					<div class="col-sm-4 col-xs-5 col-lg-2 col-md-2">
						<select class="form-control validate[required]" id="number-of-question">
							<cfloop from="1" to="15" index="i">
								<cfoutput>
									<option value="#i#">#i#</option>
								</cfoutput>
							</cfloop>
						</select>
						
					</div>
				</div>
				
				<div class="form-group template-describe hidden">
					<label class="col-sm-11" style="margin-top: 15px">Type your messages here (Limit to 160 characters per message) <span class="text-danger">*</span></label>
					<div class="message-detail">
						<!--- <div class="form-group col-sm-11">
							<textarea class="form-control validate[required,custom[noHTML]]" id="infoMsg" name="infoMsg" rows="6" placeholder="Type your messages here" maxlength="1000" data-prompt-position="topLeft:100">
							</textarea>
						</div> --->
					</div>
				</div>

				<div class="form-group col-sm-11 template-describe hidden">
					<label>Describe your business logics (if any)</label>
					<textarea class="form-control validate[maxSize[2000],custom[noHTML]]"  id="template-branch" rows="6" placeholder="Type your template branch logic here" data-prompt-position="topLeft:100"></textarea>
				</div>

			</form>

				<button type="submit" class="btn btn-success btn-success-custom btn-send-message template-describe hidden" id="btn-send-message">Send Message</button>
			</div>

		<div class="content-to-send hidden">
			<ol>
			</ol>
		</div>
		<div class="col-sm-4">
			<p>
				4533 MacArthur Blvd, Suite 1090<br>
				Newport Beach, CA 92660<br>
				<br>
				For billing or technical questions, Text "<strong>support</strong>" to 39492<br>
				<br>
				<!---Mon - Fri 9AM - 6PM Pacific--->
				For sales and pricing questions, please call (888) 747-4411 between 9am and 5pm PST.
			</p>
		</div>
	</div>
    
    
</div>

</section>
	
 <cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js">
</cfinvoke>	
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/template_request.js">
</cfinvoke>
	
<cfparam name="variables._title" default="Template Request - Sire">
<cfparam name="variables.body_class" default="">
<!--- <cfinclude template="../views/layouts/template_request.cfm"> --->
<cfinclude template="../views/layouts/main.cfm">
<script type="text/javascript">
    $('.li-contact').addClass('active');
</script>
