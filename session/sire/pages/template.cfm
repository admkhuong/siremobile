<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/template.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/waitlist-html.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/waitlist-html.js">
</cfinvoke>


<cfoutput>
<main class="container-fluid page si-template">
	<cfinclude template="../views/commons/credits_available.cfm">
	<section class="bg-white si-content">
		<h3>Toogle button</h3>
		<div class="row">
			<div class="switch">
				<input id="cmn-toggle-1" class="cmn-toggle cmn-toggle-round" type="checkbox">
				<label for="cmn-toggle-1"></label>
			</div>
		</div>
		<h3>Dropdow menu boostrap</h3>
		<div class="row">
			<div class="si-dropdow dropdown">
				<div href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><p>Dropdown</p> <span class="glyphicon glyphicon-triangle-bottom"></span></div>
				<ul class="dropdown-menu">
					<li><a href="">Action</a></li>
					<li><a href="">Another</a></li>
					<li><a href="">Something</a></li>
					<li><a href="">Separated</a></li>
					<li><a href="">One more</a></li>
				</ul>
			</div>
		</div>
		<h3>Tags</h3>
		<div class="row">
			<a href="" class="si-tag">[wistlist]</a>
			<a href="" class="si-tag">[wistlist]</a>
			<a href="" class="si-tag">[wistlist]</a>
			<a href="" class="si-tag">[wistlist]</a>
			<a href="" class="si-tag">[wistlist]</a>
		</div>
	</section>
</main>
</cfoutput>

<cfparam name="variables._title" default="Subscribers - Sire">

<cfinclude template="../views/layouts/main.cfm">

