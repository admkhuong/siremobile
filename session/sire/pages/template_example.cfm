<cfoutput>
    <!-- Start For Single Mesage -->
    <div class="content-for-single-message">
        <div class="portlet light bordered">
            <div class="portlet-body">
                <h4 class="portlet-heading">Single Message</h4>
                <h4 class="portlet-heading2 mb-20">Who do you want to send this message to?</h4>      

                <div class="row com-flex com-flex-wrap row-subscriber" data-uk-switcher="{connect:'##message-switcher'}">
                    <div class="col-lg-6">
                        <div class="subscriber-item greenbox">
                            <div class="row com-flex com-flex-middle">
                                <div class="col-xs-4 text-center">
                                    <img class="img-responsive" src="../assets/layouts/layout4/img/subscriber-keyword.png" alt="">
                                </div>
                                <div class="col-xs-8">
                                    <h4 class="titleh4">Subscribers who send your Keyword to your short code</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="subscriber-item bluebox">
                            <div class="row com-flex com-flex-middle">
                                <div class="col-xs-4 text-center">
                                    <img class="img-responsive" src="../assets/layouts/layout4/img/subscriber-sound.png" alt="">
                                </div>
                                <div class="col-xs-8">
                                    <h4 class="titleh4">Subscribers on an existing list</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    

                <div class="mb-20"></div>                    
            </div>
        </div>

        <div id="message-switcher" class="uk-switcher">
            <div class="for-green-mess">
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <label>Give Your Campaign a Name</label>
                                        <input type="text" class="form-control" placeholder="">
                                        <span class="help-block">ex: Simple Subscribers (Opt In) 2017-01-23 18:09:13</span>
                                    </div>

                                    <div class="form-group">
                                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>                                    
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Company Name</h4>
                        <p class="portlet-subheading">
                            Phone carriers require all business messages to be easily identifiable to the company who sends them. Your company name will appear before your message.
                        </p>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>    

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h4 class="portlet-heading2">Choose a Keyword</h4>
                                <p class="portlet-subheading">
                                    A Keywords is a text message that your customer sends to a Short Code using their cell phone. <br>
                                    Sire will then respond with a customizable interactive Message flow, triggering a new Sire SMS session. 
                                </p>
                                <p class="portlet-subheading">
                                    Keywords are one of the fastest ways you can begin to communicate.
                                </p>

                                <div class="row row-small">
                                    <div class="col-md-9">
                                        <div class="form-gd">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="">
                                                <span class="help-block has-error">This Keyword is not long enough. Your current account requires 8 number of characters (not including spaces)</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                            </div>
                            <div class="col-md-4">
                                <img class="img-responsive" src="../assets/layouts/layout4/img/phone-choose-keyword.png" alt="">
                            </div>
                        </div>                              
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Write Your Message</h4>
                        <p class="portlet-subheading">
                            Enter the message you would like to send your customer list.
                        </p>

                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                        <span class="help-block"><i>ex: Don’t forget, our new hours are 9-5 on weekdays and 10-3 on Weekends! Reply HELP for help or STOP to cancel.</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Select a Subscriber List</h4>
                        <p class="portlet-subheading">
                            A Subscriber List is a list of subscribers that have opted into your campaign by texting your keyword to your short code. <br>
                            Example: “39492”. Sire automatically captures all of your subscribers phone numbers and saves them in your subscriber list. <br>
                            This is also to help keep non-compliant spammers at bay.
                        </p>

                        <div class="row row-small">
                            <div class="col-md-6 col-xs-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option></option>
                                            <option>Individual 1</option>
                                            <option>Individual 2</option>
                                            <option>Individual 3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-6">
                                <a data-toggle="modal" href="##basic2" class="btn grey-mint btn-create-new-list"><i class="fa fa-plus"></i> create new list</a>
                            </div>
                        </div>

                        <!-- START MODAL -->
                        <div class="modal be-modal fade" id="basic2" tabindex="-1" role="basic" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">                                            
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    </div>
                                    <div class="modal-body">
                                        <h4 class="be-modal-title">Add new subcriber list</h4>
                                        <div class="form-gd">
                                            <div class="form-group">
                                                <label for="">Subscriber List Name: </label>
                                                <input type="text" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <button type="button" class="btn green-gd">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- END MODAL -->

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                  
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Preview</h4>

                        <div class="wrap-preview">
                            <div class="preview-screen">
                                <img class="bg-preview-screen" src="../assets/layouts/layout4/img/preview-phone.png" alt="">
                                <div class="phone-screen">
                                    <div class="inner-phone-screen scroller">
                                        <div class="bubble me">
                                            FREELUNCH
                                        </div>
                                        <div class="bubble guess">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas culpa, molestias, numquam vitae vel assumenda.
                                        </div>
                                    </div>
                                    <div class="type-screen">
                                        <textarea id="SMSTextInputArea" name="SMSTextInputArea" maxlength="160"></textarea>
                                        <button id="SMSSend" type="button" class="btn-send-area"><i class="fa fa-arrow-up"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="for-blue-mess">
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <label>Give Your Campaign a Name</label>
                                        <input type="text" class="form-control" placeholder="">
                                        <span class="help-block">ex: Simple Subscribers (Opt In) 2017-01-23 18:09:13</span>
                                    </div>

                                    <div class="form-group">
                                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>                                    
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Company Name</h4>
                        <p class="portlet-subheading">
                            Phone carriers require all business messages to be easily identifiable to the company who sends them. Your company name will appear before your message.
                        </p>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>    

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Select a Subscriber List</h4>
                        <p class="portlet-subheading">
                            A Subscriber List is a list of subscribers that have opted into your campaign by texting your keyword to your short code. <br>
                            Example: “39492”. Sire automatically captures all of your subscribers phone numbers and saves them in your subscriber list. <br>
                            This is also to help keep non-compliant spammers at bay.
                        </p>

                        <div class="row row-small">
                            <div class="col-md-6 col-xs-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <select class="bs-select form-control">
                                            <option></option>
                                            <option>Individual 1</option>
                                            <option>Individual 2</option>
                                            <option>Individual 3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-6">
                                <a data-toggle="modal" href="##basic2" class="btn grey-mint btn-create-new-list"><i class="fa fa-plus"></i> create new list</a>
                            </div>
                        </div>

                        <!-- START MODAL -->
                        <div class="modal be-modal fade" id="basic2" tabindex="-1" role="basic" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">                                            
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    </div>
                                    <div class="modal-body">
                                        <h4 class="be-modal-title">Add new subcriber list</h4>
                                        <div class="form-gd">
                                            <div class="form-group">
                                                <label for="">Subscriber List Name: </label>
                                                <input type="text" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <button type="button" class="btn green-gd">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- END MODAL -->

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                  
                    </div>
                </div>        
                
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Schedule Your Blast</h4>
                        <p class="portlet-subheading">
                            Schedule a start time for this blast. If left blank, the blast will trigger immediately.
                        </p>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-gd">
                                    <div class="row row-small">
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">Month</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">Day</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                        </div>                                                            
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">Year</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                        </div>                                                            
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">00:00 am</option>
                                                    <option value="">00:01 am</option>
                                                    <option value="">00:02 am</option>
                                                </select>
                                            </div>
                                        </div> 
                                    </div>
                                       
                                </div>
                            </div>
                        </div>    

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                
                    </div>
                </div>                                                                

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Write Your Message</h4>
                        <p class="portlet-subheading">
                            Enter the message you would like to send your customer list.
                        </p>

                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                        <span class="help-block"><i>ex: Don’t forget, our new hours are 9-5 on weekdays and 10-3 on Weekends! Reply HELP for help or STOP to cancel.</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Preview</h4>

                        <div class="wrap-preview">
                            <div class="preview-screen">
                                <img class="bg-preview-screen" src="../assets/layouts/layout4/img/preview-phone.png" alt="">
                                <div class="phone-screen">
                                    <div class="inner-phone-screen scroller">
                                        <div class="bubble me">
                                            FREELUNCH
                                        </div>
                                        <div class="bubble guess">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas culpa, molestias, numquam vitae vel assumenda.
                                        </div>
                                    </div>
                                    <div class="type-screen">
                                        <textarea id="SMSTextInputArea" name="SMSTextInputArea" maxlength="160"></textarea>
                                        <button id="SMSSend" type="button" class="btn-send-area"><i class="fa fa-arrow-up"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                                    
            </div>
        </div>
    </div>
    <!-- End For Single Mesage -->

    <!-- Start For Send Coupon -->
    <div class="content-for-send-coupon">
        <div class="portlet light bordered">
            <div class="portlet-body">
                <h4 class="portlet-heading">Send a Coupon or Promo</h4>
                <h4 class="portlet-heading2 mb-20">Who do you want to send this message to?</h4>      

                <div class="row com-flex com-flex-wrap row-subscriber" data-uk-switcher="{connect:'##message-switcher2'}">
                    <div class="col-lg-6">
                        <div class="subscriber-item greenbox">
                            <div class="row com-flex com-flex-middle">
                                <div class="col-xs-4 text-center">
                                    <img class="img-responsive" src="../assets/layouts/layout4/img/subscriber-keyword.png" alt="">
                                </div>
                                <div class="col-xs-8">
                                    <h4 class="titleh4">Subscribers who send your Keyword to your short code</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="subscriber-item bluebox">
                            <div class="row com-flex com-flex-middle">
                                <div class="col-xs-4 text-center">
                                    <img class="img-responsive" src="../assets/layouts/layout4/img/subscriber-sound.png" alt="">
                                </div>
                                <div class="col-xs-8">
                                    <h4 class="titleh4">Subscribers on an existing list</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    

                <div class="mb-20"></div>                    
            </div>
        </div>

        <div id="message-switcher2" class="uk-switcher">
            <div class="for-green-mess">
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <label>Give Your Campaign a Name</label>
                                        <input type="text" class="form-control" placeholder="">
                                        <span class="help-block">ex: Simple Subscribers (Opt In) 2017-01-23 18:09:13</span>
                                    </div>

                                    <div class="form-group">
                                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>                                    
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Company Name</h4>
                        <p class="portlet-subheading">
                            Phone carriers require all business messages to be easily identifiable to the company who sends them. Your company name will appear before your message.
                        </p>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>    

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h4 class="portlet-heading2">Choose a Keyword</h4>
                                <p class="portlet-subheading">
                                    A Keywords is a text message that your customer sends to a Short Code using their cell phone. <br>
                                    Sire will then respond with a customizable interactive Message flow, triggering a new Sire SMS session. 
                                </p>
                                <p class="portlet-subheading">
                                    Keywords are one of the fastest ways you can begin to communicate.
                                </p>

                                <div class="row row-small">
                                    <div class="col-md-9">
                                        <div class="form-gd">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="">
                                                <span class="help-block has-error">This Keyword is not long enough. Your current account requires 8 number of characters (not including spaces)</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                            </div>
                            <div class="col-md-4">
                                <img class="img-responsive" src="../assets/layouts/layout4/img/phone-choose-keyword.png" alt="">
                            </div>
                        </div>                              
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Write Your Message</h4>
                        <p class="portlet-subheading">
                            Once your customer texts your Keyword to your Short Code, they will need to “opt-in” by responding to this automated response. <br>
                            Type your automated “opt-in” request message.
                        </p>

                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                        <span class="help-block">ex: You've chosen to receive msgs from our company. Reply YES to confirm, HELP for help or STOP to cancel. Up to 5 msg/mo. Msg&Data rates may apply</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <p class="portlet-subheading">Enter the coupon or promotion details you want to send your customer after they’ve opted in.</p>

                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                        <span class="help-block">ex: Show this text to the cashier for $10 off your next oil change. Valid through Sunday!</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <p class="portlet-subheading mb-0">Enter the message a customer will receive if they enter an invalid response.</p>
                        <i class="sub-tit">Tip: ask them to reconsider and retry.</i>

                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                        <span class="help-block">ex: oops! Looks like you entered an invalid response. Please try again.</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Select a Subscriber List</h4>
                        <p class="portlet-subheading">
                            A Subscriber List is a list of subscribers that have opted into your campaign by texting your keyword to your short code. <br>
                            Example: “39492”. Sire automatically captures all of your subscribers phone numbers and saves them in your subscriber list. <br>
                            This is also to help keep non-compliant spammers at bay.
                        </p>

                        <div class="row row-small">
                            <div class="col-md-6 col-xs-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <select class="bs-select form-control">
                                            <option></option>
                                            <option>Individual 1</option>
                                            <option>Individual 2</option>
                                            <option>Individual 3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-6">
                                <a data-toggle="modal" href="##basic2" class="btn grey-mint btn-create-new-list"><i class="fa fa-plus"></i> create new list</a>
                            </div>
                        </div>

                        <!-- START MODAL -->
                        <div class="modal be-modal fade" id="basic2" tabindex="-1" role="basic" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">                                            
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    </div>
                                    <div class="modal-body">
                                        <h4 class="be-modal-title">Add new subcriber list</h4>
                                        <div class="form-gd">
                                            <div class="form-group">
                                                <label for="">Subscriber List Name: </label>
                                                <input type="text" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <button type="button" class="btn green-gd">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- END MODAL -->

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                  
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Preview</h4>

                        <div class="wrap-preview">
                            <div class="preview-screen">
                                <img class="bg-preview-screen" src="../assets/layouts/layout4/img/preview-phone.png" alt="">
                                <div class="phone-screen">
                                    <div class="inner-phone-screen scroller">
                                        <div class="bubble me">
                                            FREELUNCH
                                        </div>
                                        <div class="bubble guess">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas culpa, molestias, numquam vitae vel assumenda.
                                        </div>
                                    </div>
                                    <div class="type-screen">
                                        <textarea id="SMSTextInputArea" name="SMSTextInputArea" maxlength="160"></textarea>
                                        <button id="SMSSend" type="button" class="btn-send-area"><i class="fa fa-arrow-up"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="for-blue-mess">
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <label>Give Your Campaign a Name</label>
                                        <input type="text" class="form-control" placeholder="">
                                        <span class="help-block">ex: Simple Subscribers (Opt In) 2017-01-23 18:09:13</span>
                                    </div>

                                    <div class="form-group">
                                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>                                    
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Company Name</h4>
                        <p class="portlet-subheading">
                            Phone carriers require all business messages to be easily identifiable to the company who sends them. Your company name will appear before your message.
                        </p>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>    

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Select a Subscriber List</h4>
                        <p class="portlet-subheading">
                            A Subscriber List is a list of subscribers that have opted into your campaign by texting your keyword to your short code. <br>
                            Example: “39492”. Sire automatically captures all of your subscribers phone numbers and saves them in your subscriber list. <br>
                            This is also to help keep non-compliant spammers at bay.
                        </p>

                        <div class="row row-small">
                            <div class="col-md-6 col-xs-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <select class="bs-select form-control">
                                            <option></option>
                                            <option>Individual 1</option>
                                            <option>Individual 2</option>
                                            <option>Individual 3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-6">
                                <a data-toggle="modal" href="##basic2" class="btn grey-mint btn-create-new-list"><i class="fa fa-plus"></i> create new list</a>
                            </div>
                        </div>

                        <!-- START MODAL -->
                        <div class="modal be-modal fade" id="basic2" tabindex="-1" role="basic" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">                                            
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    </div>
                                    <div class="modal-body">
                                        <h4 class="be-modal-title">Add new subcriber list</h4>
                                        <div class="form-gd">
                                            <div class="form-group">
                                                <label for="">Subscriber List Name: </label>
                                                <input type="text" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <button type="button" class="btn green-gd">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- END MODAL -->

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                  
                    </div>
                </div>        
                
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Schedule Your Blast</h4>
                        <p class="portlet-subheading">
                            Schedule a start time for this blast. If left blank, the blast will trigger immediately.
                        </p>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-gd">
                                    <div class="row row-small">
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">Month</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">Day</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                        </div>                                                            
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">Year</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                        </div>                                                            
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">00:00 am</option>
                                                    <option value="">00:01 am</option>
                                                    <option value="">00:02 am</option>
                                                </select>
                                            </div>
                                        </div> 
                                    </div>
                                       
                                </div>
                            </div>
                        </div>    

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                
                    </div>
                </div>                                                                

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Write Your Message</h4>
                        <p class="portlet-subheading">
                            Enter the message containing your awesome coupon or promotion.
                        </p>

                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                        <span class="help-block"><i>ex: Show this text to the cashier for $10 off your next oil change. Valid through Sunday!</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Preview</h4>

                        <div class="wrap-preview">
                            <div class="preview-screen">
                                <img class="bg-preview-screen" src="../assets/layouts/layout4/img/preview-phone.png" alt="">
                                <div class="phone-screen">
                                    <div class="inner-phone-screen scroller">
                                        <div class="bubble me">
                                            FREELUNCH
                                        </div>
                                        <div class="bubble guess">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas culpa, molestias, numquam vitae vel assumenda.
                                        </div>
                                    </div>
                                    <div class="type-screen">
                                        <textarea id="SMSTextInputArea" name="SMSTextInputArea" maxlength="160"></textarea>
                                        <button id="SMSSend" type="button" class="btn-send-area"><i class="fa fa-arrow-up"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                                    
            </div>
        </div>                            
    </div>
    <!-- End For Send Coupon -->

    <!-- Start For Multiple Choice -->
    <div class="content-for-multichoice-question">
        <div class="portlet light bordered">
            <div class="portlet-body">
                <h4 class="portlet-heading">Single Multiple Choice Question</h4>
                <h4 class="portlet-heading2 mb-20">Who do you want to send this message to?</h4>      

                <div class="row com-flex com-flex-wrap row-subscriber" data-uk-switcher="{connect:'##message-switcher3'}">
                    <div class="col-lg-6">
                        <div class="subscriber-item greenbox">
                            <div class="row com-flex com-flex-middle">
                                <div class="col-xs-4 text-center">
                                    <img class="img-responsive" src="../assets/layouts/layout4/img/subscriber-keyword.png" alt="">
                                </div>
                                <div class="col-xs-8">
                                    <h4 class="titleh4">Subscribers who send your Keyword to your short code</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="subscriber-item bluebox">
                            <div class="row com-flex com-flex-middle">
                                <div class="col-xs-4 text-center">
                                    <img class="img-responsive" src="../assets/layouts/layout4/img/subscriber-sound.png" alt="">
                                </div>
                                <div class="col-xs-8">
                                    <h4 class="titleh4">Subscribers on an existing list</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    

                <div class="mb-20"></div>                    
            </div>
        </div>

        <div id="message-switcher3" class="uk-switcher">
            <div class="for-green-mess">
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <label>Give Your Campaign a Name</label>
                                        <input type="text" class="form-control" placeholder="">
                                        <span class="help-block">ex: Simple Subscribers (Opt In) 2017-01-23 18:09:13</span>
                                    </div>

                                    <div class="form-group">
                                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>                                    
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Company Name</h4>
                        <p class="portlet-subheading">
                            Phone carriers require all business messages to be easily identifiable to the company who sends them. Your company name will appear before your message.
                        </p>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>    

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h4 class="portlet-heading2">Choose a Keyword</h4>
                                <p class="portlet-subheading">
                                    A Keywords is a text message that your customer sends to a Short Code using their cell phone. <br>
                                    Sire will then respond with a customizable interactive Message flow, triggering a new Sire SMS session. 
                                </p>
                                <p class="portlet-subheading">
                                    Keywords are one of the fastest ways you can begin to communicate.
                                </p>

                                <div class="row row-small">
                                    <div class="col-md-9">
                                        <div class="form-gd">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="">
                                                <span class="help-block has-error">This Keyword is not long enough. Your current account requires 8 number of characters (not including spaces)</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                            </div>
                            <div class="col-md-4">
                                <img class="img-responsive" src="../assets/layouts/layout4/img/phone-choose-keyword.png" alt="">
                            </div>
                        </div>                              
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2 mb-30">Write Your Message</h4>

                        <div class="write-mess-body">
                            <h4 class="portlet-heading2">Questions</h4>
                            <p class="portlet-subheading">
                                Enter the multiple choice question you would like to ask. 
                            </p>

                            <div class="row">
                                <div class="col-md-11">
                                    <div class="form-gd">
                                        <div class="form-group">
                                            <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                            <span class="help-block">ex: Thank you for participating in the {%CompanyName%} Market Test. How useful is the product to your group/business?</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-11">
                                    <div class="row">
                                        <div class="col-lg-9">
                                            <h4 class="portlet-heading2">Answer Choices List</h4>

                                            <div class="answer-wrap clearfix">
                                                <label for="">Format Answers:</label>
                                                <div class="answer-wrap-select">
                                                    <select name="" id="" class="bs-select form-control">
                                                        <option value=""></option>
                                                        <option value="">asfas</option>
                                                        <option value="">sdfsdf</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-11">
                                                    <div class="wrapper-answer-text">
                                                        <div class="form-gd field-anser-holder">
                                                            <div class="row row-small">
                                                                <div class="col-xs-8 col-sm-10">
                                                                    <div class="form-group mb-0">
                                                                        <label>Answer Text</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group text-center mb-0">
                                                                        <label>S-Val</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group text-center mb-0">
                                                                        <label>Delete</label>
                                                                    </div>
                                                                </div>
                                                            </div>                                                                            
                                                            <div class="row row-small">
                                                                <div class="col-xs-8 col-sm-10">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control readonly-val text-center" value="1" readonly />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <button class="form-control delete-answer">
                                                                            <i class="fa fa-minus"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row row-small">
                                                                <div class="col-xs-8 col-sm-10">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control readonly-val text-center" value="1" readonly />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <button class="form-control delete-answer">
                                                                            <i class="fa fa-minus"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row row-small">
                                                                <div class="col-xs-8 col-sm-10">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control readonly-val text-center" value="1" readonly />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <button class="form-control delete-answer">
                                                                            <i class="fa fa-minus"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row row-small">
                                                                <div class="col-xs-8 col-sm-10">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control readonly-val text-center" value="1" readonly />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <button class="form-control delete-answer">
                                                                            <i class="fa fa-minus"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row row-small">
                                                                <div class="col-xs-8 col-sm-10">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control readonly-val text-center" value="1" readonly />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <button class="form-control delete-answer">
                                                                            <i class="fa fa-minus"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row row-small">
                                                            <div class="col-xs-4 col-xs-offset-8 col-sm-2 col-sm-offset-10">
                                                                <a href="##" id="add-more-answer" class="btn btn-block grey-mint btn-create-new-list add-more-answer">add more</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <h4 class="portlet-heading2">Answer Template (Quick Add)</h4>
                                            <h4 class="portlet-heading3 mb-30">If you would like, select one and then hit the APPLY button.</h4>
                                            
                                            <div class="row">
                                                <div class="col-lg-10">
                                                    <div class="list-answer-template">
                                                        <ul class="nav">
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-1">
                                                                <label for="ans-temp-1">Yes or No?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-2">
                                                                <label for="ans-temp-2">True or False?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-3">
                                                                <label for="ans-temp-3">Agree or Disagree?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-4">
                                                                <label for="ans-temp-4">Useful or Not?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-5">
                                                                <label for="ans-temp-5">Easy or Not?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-6">
                                                                <label for="ans-temp-6">Likely or Not?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-7">
                                                                <label for="ans-temp-7">Important or Not?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-8">
                                                                <label for="ans-temp-8">Satisfied or Not?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-9">
                                                                <label for="ans-temp-9">Net Promoter Score</label>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <a href="##" class="btn green-gd">apply</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h4 class="portlet-heading2">Response Interval Settings (Optional)</h4>
                                            <p class="des-for-heading">
                                                Response Intervals are how long you wish to wait for a message recipient to reply. By default (0) the wait time is infinite. Response Intervals give you dynamic control on 
                                                how long you wish to wait before moving on to the next specified Action if no response is received.
                                            </p>

                                            <div class="form-gd">
                                                <div class="row row-small">
                                                    <div class="col-lg-2 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="">Interval Time</label>
                                                            <select class="bs-select form-control">
                                                                <option value=""></option>
                                                                <option value="">tyru</option>
                                                                <option value="">tyi</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="">&nbsp;</label>
                                                            <select class="bs-select form-control">
                                                                <option value=""></option>
                                                                <option value="">fgjhfg</option>
                                                                <option value="">fghfg</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="">Retry on No Response?</label>
                                                            <select class="bs-select form-control">
                                                                <option value=""></option>
                                                                <option value="">etert</option>
                                                                <option value="">vbhnfg</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="">Still No Response?</label>
                                                            <select class="bs-select form-control">
                                                                <option value=""></option>
                                                                <option value="">fghfgjh</option>
                                                                <option value="">dfhgdfg</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                                                
                        </div>

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Select a Subscriber List</h4>
                        <p class="portlet-subheading">
                            A Subscriber List is a list of subscribers that have opted into your campaign by texting your keyword to your short code. <br>
                            Example: “39492”. Sire automatically captures all of your subscribers phone numbers and saves them in your subscriber list. <br>
                            This is also to help keep non-compliant spammers at bay.
                        </p>

                        <div class="row row-small">
                            <div class="col-md-6 col-xs-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <select class="bs-select form-control">
                                            <option></option>
                                            <option>Individual 1</option>
                                            <option>Individual 2</option>
                                            <option>Individual 3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-6">
                                <a data-toggle="modal" href="##basic2" class="btn grey-mint btn-create-new-list"><i class="fa fa-plus"></i> create new list</a>
                            </div>
                        </div>

                        <!-- START MODAL -->
                        <div class="modal be-modal fade" id="basic2" tabindex="-1" role="basic" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">                                            
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    </div>
                                    <div class="modal-body">
                                        <h4 class="be-modal-title">Add new subcriber list</h4>
                                        <div class="form-gd">
                                            <div class="form-group">
                                                <label for="">Subscriber List Name: </label>
                                                <input type="text" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <button type="button" class="btn green-gd">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- END MODAL -->

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                  
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Preview</h4>

                        <div class="wrap-preview">
                            <div class="preview-screen">
                                <img class="bg-preview-screen" src="../assets/layouts/layout4/img/preview-phone.png" alt="">
                                <div class="phone-screen">
                                    <div class="inner-phone-screen scroller">
                                        <div class="bubble me">
                                            FREELUNCH
                                        </div>
                                        <div class="bubble guess">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas culpa, molestias, numquam vitae vel assumenda.
                                        </div>
                                    </div>
                                    <div class="type-screen">
                                        <textarea id="SMSTextInputArea" name="SMSTextInputArea" maxlength="160"></textarea>
                                        <button id="SMSSend" type="button" class="btn-send-area"><i class="fa fa-arrow-up"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="for-blue-mess">
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <label>Give Your Campaign a Name</label>
                                        <input type="text" class="form-control" placeholder="">
                                        <span class="help-block">ex: Simple Subscribers (Opt In) 2017-01-23 18:09:13</span>
                                    </div>

                                    <div class="form-group">
                                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>                                    
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Company Name</h4>
                        <p class="portlet-subheading">
                            Phone carriers require all business messages to be easily identifiable to the company who sends them. Your company name will appear before your message.
                        </p>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>    

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Select a Subscriber List</h4>
                        <p class="portlet-subheading">
                            A Subscriber List is a list of subscribers that have opted into your campaign by texting your keyword to your short code. <br>
                            Example: “39492”. Sire automatically captures all of your subscribers phone numbers and saves them in your subscriber list. <br>
                            This is also to help keep non-compliant spammers at bay.
                        </p>

                        <div class="row row-small">
                            <div class="col-md-6 col-xs-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <select class="bs-select form-control">
                                            <option></option>
                                            <option>Individual 1</option>
                                            <option>Individual 2</option>
                                            <option>Individual 3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-6">
                                <a data-toggle="modal" href="##basic2" class="btn grey-mint btn-create-new-list"><i class="fa fa-plus"></i> create new list</a>
                            </div>
                        </div>

                        <!-- START MODAL -->
                        <div class="modal be-modal fade" id="basic2" tabindex="-1" role="basic" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">                                            
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    </div>
                                    <div class="modal-body">
                                        <h4 class="be-modal-title">Add new subcriber list</h4>
                                        <div class="form-gd">
                                            <div class="form-group">
                                                <label for="">Subscriber List Name: </label>
                                                <input type="text" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <button type="button" class="btn green-gd">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- END MODAL -->

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                  
                    </div>
                </div>        
                
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Schedule Your Blast</h4>
                        <p class="portlet-subheading">
                            Schedule a start time for this blast. If left blank, the blast will trigger immediately.
                        </p>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-gd">
                                    <div class="row row-small">
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">Month</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">Day</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                        </div>                                                            
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">Year</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                        </div>                                                            
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">00:00 am</option>
                                                    <option value="">00:01 am</option>
                                                    <option value="">00:02 am</option>
                                                </select>
                                            </div>
                                        </div> 
                                    </div>
                                       
                                </div>
                            </div>
                        </div>    

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                
                    </div>
                </div>                                                                

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2 mb-30">Write Your Message</h4>

                        <div class="write-mess-body">
                            <h4 class="portlet-heading2">Questions</h4>
                            <p class="portlet-subheading">
                                Enter the multiple choice question you would like to ask. 
                            </p>

                            <div class="row">
                                <div class="col-md-11">
                                    <div class="form-gd">
                                        <div class="form-group">
                                            <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                            <span class="help-block">ex: Thank you for participating in the {%CompanyName%} Market Test. How useful is the product to your group/business?</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-11">
                                    <div class="row">
                                        <div class="col-lg-9">
                                            <h4 class="portlet-heading2">Answer Choices List</h4>

                                            <div class="answer-wrap clearfix">
                                                <label for="">Format Answers:</label>
                                                <div class="answer-wrap-select">
                                                    <select name="" id="" class="bs-select form-control">
                                                        <option value=""></option>
                                                        <option value="">asfas</option>
                                                        <option value="">sdfsdf</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-11">
                                                    <div class="wrapper-answer-text">
                                                        <div class="form-gd field-anser-holder">
                                                            <div class="row row-small">
                                                                <div class="col-xs-8 col-sm-10">
                                                                    <div class="form-group mb-0">
                                                                        <label>Answer Text</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group text-center mb-0">
                                                                        <label>S-Val</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group text-center mb-0">
                                                                        <label>Delete</label>
                                                                    </div>
                                                                </div>
                                                            </div>                                                                            
                                                            <div class="row row-small">
                                                                <div class="col-xs-8 col-sm-10">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control readonly-val text-center" value="1" readonly />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <button class="form-control delete-answer">
                                                                            <i class="fa fa-minus"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row row-small">
                                                                <div class="col-xs-8 col-sm-10">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control readonly-val text-center" value="1" readonly />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <button class="form-control delete-answer">
                                                                            <i class="fa fa-minus"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row row-small">
                                                                <div class="col-xs-8 col-sm-10">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control readonly-val text-center" value="1" readonly />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <button class="form-control delete-answer">
                                                                            <i class="fa fa-minus"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row row-small">
                                                                <div class="col-xs-8 col-sm-10">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control readonly-val text-center" value="1" readonly />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <button class="form-control delete-answer">
                                                                            <i class="fa fa-minus"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row row-small">
                                                                <div class="col-xs-8 col-sm-10">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control readonly-val text-center" value="1" readonly />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <button class="form-control delete-answer">
                                                                            <i class="fa fa-minus"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row row-small">
                                                            <div class="col-xs-4 col-xs-offset-8 col-sm-2 col-sm-offset-10">
                                                                <a href="##" id="add-more-answer" class="btn btn-block grey-mint btn-create-new-list add-more-answer">add more</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <h4 class="portlet-heading2">Answer Template (Quick Add)</h4>
                                            <h4 class="portlet-heading3 mb-30">If you would like, select one and then hit the APPLY button.</h4>
                                            
                                            <div class="row">
                                                <div class="col-lg-10">
                                                    <div class="list-answer-template">
                                                        <ul class="nav">
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-1">
                                                                <label for="ans-temp-1">Yes or No?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-2">
                                                                <label for="ans-temp-2">True or False?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-3">
                                                                <label for="ans-temp-3">Agree or Disagree?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-4">
                                                                <label for="ans-temp-4">Useful or Not?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-5">
                                                                <label for="ans-temp-5">Easy or Not?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-6">
                                                                <label for="ans-temp-6">Likely or Not?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-7">
                                                                <label for="ans-temp-7">Important or Not?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-8">
                                                                <label for="ans-temp-8">Satisfied or Not?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-9">
                                                                <label for="ans-temp-9">Net Promoter Score</label>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <a href="##" class="btn green-gd">apply</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h4 class="portlet-heading2">Response Interval Settings (Optional)</h4>
                                            <p class="des-for-heading">
                                                Response Intervals are how long you wish to wait for a message recipient to reply. By default (0) the wait time is infinite. Response Intervals give you dynamic control on 
                                                how long you wish to wait before moving on to the next specified Action if no response is received.
                                            </p>

                                            <div class="form-gd">
                                                <div class="row row-small">
                                                    <div class="col-lg-2 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="">Interval Time</label>
                                                            <select class="bs-select form-control">
                                                                <option value=""></option>
                                                                <option value="">tyru</option>
                                                                <option value="">tyi</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="">&nbsp;</label>
                                                            <select class="bs-select form-control">
                                                                <option value=""></option>
                                                                <option value="">fgjhfg</option>
                                                                <option value="">fghfg</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="">Retry on No Response?</label>
                                                            <select class="bs-select form-control">
                                                                <option value=""></option>
                                                                <option value="">etert</option>
                                                                <option value="">vbhnfg</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="">Still No Response?</label>
                                                            <select class="bs-select form-control">
                                                                <option value=""></option>
                                                                <option value="">fghfgjh</option>
                                                                <option value="">dfhgdfg</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                                                
                        </div>

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Preview</h4>

                        <div class="wrap-preview">
                            <div class="preview-screen">
                                <img class="bg-preview-screen" src="../assets/layouts/layout4/img/preview-phone.png" alt="">
                                <div class="phone-screen">
                                    <div class="inner-phone-screen scroller">
                                        <div class="bubble me">
                                            FREELUNCH
                                        </div>
                                        <div class="bubble guess">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas culpa, molestias, numquam vitae vel assumenda.
                                        </div>
                                    </div>
                                    <div class="type-screen">
                                        <textarea id="SMSTextInputArea" name="SMSTextInputArea" maxlength="160"></textarea>
                                        <button id="SMSSend" type="button" class="btn-send-area"><i class="fa fa-arrow-up"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                                    
            </div>
        </div>
    </div>
    <!-- End For Multiple Choice -->

    <!-- Start For Yes No Question -->
    <div class="content-for-yesno-question">
        <div class="portlet light bordered">
            <div class="portlet-body">
                <h4 class="portlet-heading">Single Yes/No Question</h4>
                <h4 class="portlet-heading2 mb-20">Who do you want to send this message to?</h4>      

                <div class="row com-flex com-flex-wrap row-subscriber" data-uk-switcher="{connect:'##message-switcher4'}">
                    <div class="col-lg-6">
                        <div class="subscriber-item greenbox">
                            <div class="row com-flex com-flex-middle">
                                <div class="col-xs-4 text-center">
                                    <img class="img-responsive" src="../assets/layouts/layout4/img/subscriber-keyword.png" alt="">
                                </div>
                                <div class="col-xs-8">
                                    <h4 class="titleh4">Subscribers who send your Keyword to your short code</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="subscriber-item bluebox">
                            <div class="row com-flex com-flex-middle">
                                <div class="col-xs-4 text-center">
                                    <img class="img-responsive" src="../assets/layouts/layout4/img/subscriber-sound.png" alt="">
                                </div>
                                <div class="col-xs-8">
                                    <h4 class="titleh4">Subscribers on an existing list</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    

                <div class="mb-20"></div>                    
            </div>
        </div>

        <div id="message-switcher4" class="uk-switcher">
            <div class="for-green-mess">
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <label>Give Your Campaign a Name</label>
                                        <input type="text" class="form-control" placeholder="">
                                        <span class="help-block">ex: Simple Subscribers (Opt In) 2017-01-23 18:09:13</span>
                                    </div>

                                    <div class="form-group">
                                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>                                    
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Company Name</h4>
                        <p class="portlet-subheading">
                            Phone carriers require all business messages to be easily identifiable to the company who sends them. Your company name will appear before your message.
                        </p>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>    

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h4 class="portlet-heading2">Choose a Keyword</h4>
                                <p class="portlet-subheading">
                                    A Keywords is a text message that your customer sends to a Short Code using their cell phone. <br>
                                    Sire will then respond with a customizable interactive Message flow, triggering a new Sire SMS session. 
                                </p>
                                <p class="portlet-subheading">
                                    Keywords are one of the fastest ways you can begin to communicate.
                                </p>

                                <div class="row row-small">
                                    <div class="col-md-9">
                                        <div class="form-gd">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="">
                                                <span class="help-block has-error">This Keyword is not long enough. Your current account requires 8 number of characters (not including spaces)</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                            </div>
                            <div class="col-md-4">
                                <img class="img-responsive" src="../assets/layouts/layout4/img/phone-choose-keyword.png" alt="">
                            </div>
                        </div>                              
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Write Your Message</h4>
                        <p class="portlet-subheading">
                            Once your customer texts your Keyword to your Short Code, they will need to “opt-in” by responding to this automated response. <br>
                            Type your automated “opt-in” request message.
                        </p>

                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                        <span class="help-block"><i>ex: You've chosen to receive msgs from our company. Reply YES to confirm, HELP for help or STOP to cancel. Up to 5 msg/mo. Msg&Data rates may apply</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <p class="portlet-subheading">Enter your message if the response is “Yes”</p>

                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                        <span class="help-block"><i>ex: Great! See you there.</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <p class="portlet-subheading">Enter your message if the response is “No”</p>

                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                        <span class="help-block"><i>ex: Bummer.  We’ll miss your input. Please be there next time.</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Select a Subscriber List</h4>
                        <p class="portlet-subheading">
                            A Subscriber List is a list of subscribers that have opted into your campaign by texting your keyword to your short code. <br>
                            Example: “39492”. Sire automatically captures all of your subscribers phone numbers and saves them in your subscriber list. <br>
                            This is also to help keep non-compliant spammers at bay.
                        </p>

                        <div class="row row-small">
                            <div class="col-md-6 col-xs-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <select class="bs-select form-control">
                                            <option></option>
                                            <option>Individual 1</option>
                                            <option>Individual 2</option>
                                            <option>Individual 3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-6">
                                <a data-toggle="modal" href="##basic2" class="btn grey-mint btn-create-new-list"><i class="fa fa-plus"></i> create new list</a>
                            </div>
                        </div>

                        <!-- START MODAL -->
                        <div class="modal be-modal fade" id="basic2" tabindex="-1" role="basic" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">                                            
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    </div>
                                    <div class="modal-body">
                                        <h4 class="be-modal-title">Add new subcriber list</h4>
                                        <div class="form-gd">
                                            <div class="form-group">
                                                <label for="">Subscriber List Name: </label>
                                                <input type="text" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <button type="button" class="btn green-gd">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- END MODAL -->

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                  
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Preview</h4>

                        <div class="wrap-preview">
                            <div class="preview-screen">
                                <img class="bg-preview-screen" src="../assets/layouts/layout4/img/preview-phone.png" alt="">
                                <div class="phone-screen">
                                    <div class="inner-phone-screen scroller">
                                        <div class="bubble me">
                                            FREELUNCH
                                        </div>
                                        <div class="bubble guess">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas culpa, molestias, numquam vitae vel assumenda.
                                        </div>
                                    </div>
                                    <div class="type-screen">
                                        <textarea id="SMSTextInputArea" name="SMSTextInputArea" maxlength="160"></textarea>
                                        <button id="SMSSend" type="button" class="btn-send-area"><i class="fa fa-arrow-up"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="for-blue-mess">
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <label>Give Your Campaign a Name</label>
                                        <input type="text" class="form-control" placeholder="">
                                        <span class="help-block">ex: Simple Subscribers (Opt In) 2017-01-23 18:09:13</span>
                                    </div>

                                    <div class="form-group">
                                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>                                    
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Company Name</h4>
                        <p class="portlet-subheading">
                            Phone carriers require all business messages to be easily identifiable to the company who sends them. Your company name will appear before your message.
                        </p>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>    

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Select a Subscriber List</h4>
                        <p class="portlet-subheading">
                            A Subscriber List is a list of subscribers that have opted into your campaign by texting your keyword to your short code. <br>
                            Example: “39492”. Sire automatically captures all of your subscribers phone numbers and saves them in your subscriber list. <br>
                            This is also to help keep non-compliant spammers at bay.
                        </p>

                        <div class="row row-small">
                            <div class="col-md-6 col-xs-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <select class="bs-select form-control">
                                            <option></option>
                                            <option>Individual 1</option>
                                            <option>Individual 2</option>
                                            <option>Individual 3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-6">
                                <a data-toggle="modal" href="##basic2" class="btn grey-mint btn-create-new-list"><i class="fa fa-plus"></i> create new list</a>
                            </div>
                        </div>

                        <!-- START MODAL -->
                        <div class="modal be-modal fade" id="basic2" tabindex="-1" role="basic" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">                                            
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    </div>
                                    <div class="modal-body">
                                        <h4 class="be-modal-title">Add new subcriber list</h4>
                                        <div class="form-gd">
                                            <div class="form-group">
                                                <label for="">Subscriber List Name: </label>
                                                <input type="text" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <button type="button" class="btn green-gd">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- END MODAL -->

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                  
                    </div>
                </div>        
                
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Schedule Your Blast</h4>
                        <p class="portlet-subheading">
                            Schedule a start time for this blast. If left blank, the blast will trigger immediately.
                        </p>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-gd">
                                    <div class="row row-small">
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">Month</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">Day</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                        </div>                                                            
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">Year</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                        </div>                                                            
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">00:00 am</option>
                                                    <option value="">00:01 am</option>
                                                    <option value="">00:02 am</option>
                                                </select>
                                            </div>
                                        </div> 
                                    </div>
                                       
                                </div>
                            </div>
                        </div>    

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                
                    </div>
                </div>                                                                

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Write Your Message</h4>
                        <p class="portlet-subheading">
                            Enter the Yes/No question you would like to ask.
                        </p>

                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                        <span class="help-block"><i>ex: Our planning committee meeting is on Wed at 6:00 PM. Can you make it?</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <p class="portlet-subheading">Enter your message if the response is “Yes”</p>

                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <input type="text" class="form-control" />
                                        <span class="help-block"><i>ex: Great! See you there.</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <p class="portlet-subheading">Enter your message if the response is No</p>

                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <input type="text" class="form-control" />
                                        <span class="help-block"><i>ex: Bummer. We’ll miss your input. Please be there next time!</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Preview</h4>

                        <div class="wrap-preview">
                            <div class="preview-screen">
                                <img class="bg-preview-screen" src="../assets/layouts/layout4/img/preview-phone.png" alt="">
                                <div class="phone-screen">
                                    <div class="inner-phone-screen scroller">
                                        <div class="bubble me">
                                            FREELUNCH
                                        </div>
                                        <div class="bubble guess">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas culpa, molestias, numquam vitae vel assumenda.
                                        </div>
                                    </div>
                                    <div class="type-screen">
                                        <textarea id="SMSTextInputArea" name="SMSTextInputArea" maxlength="160"></textarea>
                                        <button id="SMSSend" type="button" class="btn-send-area"><i class="fa fa-arrow-up"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                                    
            </div>
        </div>                            
    </div>
    <!-- End For Yes No Question -->

    <!-- Start For Open Question -->
    <div class="content-for-open-question">
        <div class="portlet light bordered">
            <div class="portlet-body">
                <h4 class="portlet-heading">Open-Ended Question</h4>
                <h4 class="portlet-heading2 mb-20">Who do you want to send this message to?</h4>      

                <div class="row com-flex com-flex-wrap row-subscriber" data-uk-switcher="{connect:'##message-switcher5'}">
                    <div class="col-lg-6">
                        <div class="subscriber-item greenbox">
                            <div class="row com-flex com-flex-middle">
                                <div class="col-xs-4 text-center">
                                    <img class="img-responsive" src="../assets/layouts/layout4/img/subscriber-keyword.png" alt="">
                                </div>
                                <div class="col-xs-8">
                                    <h4 class="titleh4">Subscribers who send your Keyword to your short code</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="subscriber-item bluebox">
                            <div class="row com-flex com-flex-middle">
                                <div class="col-xs-4 text-center">
                                    <img class="img-responsive" src="../assets/layouts/layout4/img/subscriber-sound.png" alt="">
                                </div>
                                <div class="col-xs-8">
                                    <h4 class="titleh4">Subscribers on an existing list</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    

                <div class="mb-20"></div>                    
            </div>
        </div>

        <div id="message-switcher5" class="uk-switcher">
            <div class="for-green-mess">
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <label>Give Your Campaign a Name</label>
                                        <input type="text" class="form-control" placeholder="">
                                        <span class="help-block">ex: Simple Subscribers (Opt In) 2017-01-23 18:09:13</span>
                                    </div>

                                    <div class="form-group">
                                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>                                    
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Company Name</h4>
                        <p class="portlet-subheading">
                            Phone carriers require all business messages to be easily identifiable to the company who sends them. Your company name will appear before your message.
                        </p>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>    

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h4 class="portlet-heading2">Choose a Keyword</h4>
                                <p class="portlet-subheading">
                                    A Keywords is a text message that your customer sends to a Short Code using their cell phone. <br>
                                    Sire will then respond with a customizable interactive Message flow, triggering a new Sire SMS session. 
                                </p>
                                <p class="portlet-subheading">
                                    Keywords are one of the fastest ways you can begin to communicate.
                                </p>

                                <div class="row row-small">
                                    <div class="col-md-9">
                                        <div class="form-gd">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="">
                                                <span class="help-block has-error">This Keyword is not long enough. Your current account requires 8 number of characters (not including spaces)</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                            </div>
                            <div class="col-md-4">
                                <img class="img-responsive" src="../assets/layouts/layout4/img/phone-choose-keyword.png" alt="">
                            </div>
                        </div>                              
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Write Your Message</h4>
                        <p class="portlet-subheading">
                            Once your customer texts your Keyword to your Short Code, they will need to “opt-in” by responding to this automated response. <br>
                            Type your automated “opt-in” request message.
                        </p>

                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                        <span class="help-block"><i>ex: You've chosen to receive msgs from our company. Reply YES to confirm, HELP for help or STOP to cancel. Up to 5 msg/mo. Msg&Data rates may apply</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <p class="portlet-subheading">Enter the open-ended question you want to send your customer after they’ve opted in.</p>

                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                        <span class="help-block"><i>ex: What changes would you like to see in our product?</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <p class="portlet-subheading">Let your subscribers know that you have received their response.</p>

                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                        <span class="help-block"><i>ex: Thank you for your response! You rock!</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Select a Subscriber List</h4>
                        <p class="portlet-subheading">
                            A Subscriber List is a list of subscribers that have opted into your campaign by texting your keyword to your short code. <br>
                            Example: “39492”. Sire automatically captures all of your subscribers phone numbers and saves them in your subscriber list. <br>
                            This is also to help keep non-compliant spammers at bay.
                        </p>

                        <div class="row row-small">
                            <div class="col-md-6 col-xs-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <select class="bs-select form-control">
                                            <option></option>
                                            <option>Individual 1</option>
                                            <option>Individual 2</option>
                                            <option>Individual 3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-6">
                                <a data-toggle="modal" href="##basic2" class="btn grey-mint btn-create-new-list"><i class="fa fa-plus"></i> create new list</a>
                            </div>
                        </div>

                        <!-- START MODAL -->
                        <div class="modal be-modal fade" id="basic2" tabindex="-1" role="basic" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">                                            
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    </div>
                                    <div class="modal-body">
                                        <h4 class="be-modal-title">Add new subcriber list</h4>
                                        <div class="form-gd">
                                            <div class="form-group">
                                                <label for="">Subscriber List Name: </label>
                                                <input type="text" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <button type="button" class="btn green-gd">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- END MODAL -->

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                  
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Preview</h4>

                        <div class="wrap-preview">
                            <div class="preview-screen">
                                <img class="bg-preview-screen" src="../assets/layouts/layout4/img/preview-phone.png" alt="">
                                <div class="phone-screen">
                                    <div class="inner-phone-screen scroller">
                                        <div class="bubble me">
                                            FREELUNCH
                                        </div>
                                        <div class="bubble guess">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas culpa, molestias, numquam vitae vel assumenda.
                                        </div>
                                    </div>
                                    <div class="type-screen">
                                        <textarea id="SMSTextInputArea" name="SMSTextInputArea" maxlength="160"></textarea>
                                        <button id="SMSSend" type="button" class="btn-send-area"><i class="fa fa-arrow-up"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="for-blue-mess">
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <label>Give Your Campaign a Name</label>
                                        <input type="text" class="form-control" placeholder="">
                                        <span class="help-block">ex: Simple Subscribers (Opt In) 2017-01-23 18:09:13</span>
                                    </div>

                                    <div class="form-group">
                                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>                                    
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Company Name</h4>
                        <p class="portlet-subheading">
                            Phone carriers require all business messages to be easily identifiable to the company who sends them. Your company name will appear before your message.
                        </p>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>    

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Select a Subscriber List</h4>
                        <p class="portlet-subheading">
                            A Subscriber List is a list of subscribers that have opted into your campaign by texting your keyword to your short code. <br>
                            Example: “39492”. Sire automatically captures all of your subscribers phone numbers and saves them in your subscriber list. <br>
                            This is also to help keep non-compliant spammers at bay.
                        </p>

                        <div class="row row-small">
                            <div class="col-md-6 col-xs-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <select class="bs-select form-control">
                                            <option></option>
                                            <option>Individual 1</option>
                                            <option>Individual 2</option>
                                            <option>Individual 3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-6">
                                <a data-toggle="modal" href="##basic2" class="btn grey-mint btn-create-new-list"><i class="fa fa-plus"></i> create new list</a>
                            </div>
                        </div>

                        <!-- START MODAL -->
                        <div class="modal be-modal fade" id="basic2" tabindex="-1" role="basic" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">                                            
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    </div>
                                    <div class="modal-body">
                                        <h4 class="be-modal-title">Add new subcriber list</h4>
                                        <div class="form-gd">
                                            <div class="form-group">
                                                <label for="">Subscriber List Name: </label>
                                                <input type="text" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <button type="button" class="btn green-gd">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- END MODAL -->

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                  
                    </div>
                </div>        
                
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Schedule Your Blast</h4>
                        <p class="portlet-subheading">
                            Schedule a start time for this blast. If left blank, the blast will trigger immediately.
                        </p>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-gd">
                                    <div class="row row-small">
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">Month</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">Day</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                        </div>                                                            
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">Year</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                        </div>                                                            
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">00:00 am</option>
                                                    <option value="">00:01 am</option>
                                                    <option value="">00:02 am</option>
                                                </select>
                                            </div>
                                        </div> 
                                    </div>
                                       
                                </div>
                            </div>
                        </div>    

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                
                    </div>
                </div>                                                                

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Write Your Message</h4>
                        <p class="portlet-subheading">
                            Enter the open-ended question you would like to ask your subscribers.
                        </p>

                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                        <span class="help-block"><i>ex: What changes would you like to see in our product?</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <p class="portlet-subheading">Let your subscribers know that you have received their response.</p>

                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <input type="text" class="form-control" />
                                        <span class="help-block"><i>ex: Thank you for your response!  You rock!</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Preview</h4>

                        <div class="wrap-preview">
                            <div class="preview-screen">
                                <img class="bg-preview-screen" src="../assets/layouts/layout4/img/preview-phone.png" alt="">
                                <div class="phone-screen">
                                    <div class="inner-phone-screen scroller">
                                        <div class="bubble me">
                                            FREELUNCH
                                        </div>
                                        <div class="bubble guess">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas culpa, molestias, numquam vitae vel assumenda.
                                        </div>
                                    </div>
                                    <div class="type-screen">
                                        <textarea id="SMSTextInputArea" name="SMSTextInputArea" maxlength="160"></textarea>
                                        <button id="SMSSend" type="button" class="btn-send-area"><i class="fa fa-arrow-up"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                                    
            </div>
        </div>                            
    </div>
    <!-- End For Open Question -->

    <!-- Start For Multi Choice Survey -->
    <div class="content-for-multichoice-survey">
        <div class="portlet light bordered">
            <div class="portlet-body">
                <h4 class="portlet-heading">Multiple Choice Survey</h4>
                <h4 class="portlet-heading2 mb-20">Who do you want to send this message to?</h4>      

                <div class="row com-flex com-flex-wrap row-subscriber" data-uk-switcher="{connect:'##message-switcher6'}">
                    <div class="col-lg-6">
                        <div class="subscriber-item greenbox">
                            <div class="row com-flex com-flex-middle">
                                <div class="col-xs-4 text-center">
                                    <img class="img-responsive" src="../assets/layouts/layout4/img/subscriber-keyword.png" alt="">
                                </div>
                                <div class="col-xs-8">
                                    <h4 class="titleh4">Subscribers who send your Keyword to your short code</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="subscriber-item bluebox">
                            <div class="row com-flex com-flex-middle">
                                <div class="col-xs-4 text-center">
                                    <img class="img-responsive" src="../assets/layouts/layout4/img/subscriber-sound.png" alt="">
                                </div>
                                <div class="col-xs-8">
                                    <h4 class="titleh4">Subscribers on an existing list</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    

                <div class="mb-20"></div>                    
            </div>
        </div>

        <div id="message-switcher6" class="uk-switcher">
            <div class="for-green-mess">
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <label>Give Your Campaign a Name</label>
                                        <input type="text" class="form-control" placeholder="">
                                        <span class="help-block">ex: Simple Subscribers (Opt In) 2017-01-23 18:09:13</span>
                                    </div>

                                    <div class="form-group">
                                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>                                    
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Company Name</h4>
                        <p class="portlet-subheading">
                            Phone carriers require all business messages to be easily identifiable to the company who sends them. Your company name will appear before your message.
                        </p>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>    

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h4 class="portlet-heading2">Choose a Keyword</h4>
                                <p class="portlet-subheading">
                                    A Keywords is a text message that your customer sends to a Short Code using their cell phone. <br>
                                    Sire will then respond with a customizable interactive Message flow, triggering a new Sire SMS session. 
                                </p>
                                <p class="portlet-subheading">
                                    Keywords are one of the fastest ways you can begin to communicate.
                                </p>

                                <div class="row row-small">
                                    <div class="col-md-9">
                                        <div class="form-gd">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="">
                                                <span class="help-block has-error">This Keyword is not long enough. Your current account requires 8 number of characters (not including spaces)</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                            </div>
                            <div class="col-md-4">
                                <img class="img-responsive" src="../assets/layouts/layout4/img/phone-choose-keyword.png" alt="">
                            </div>
                        </div>                              
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Write Your Message</h4>
                        <p class="portlet-subheading">
                           Once your customer texts your Keyword to your Short Code, they will need to “opt-in” by responding to this automated response. Type your automated “opt-in” request message. 
                        </p>

                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                        <span class="help-block">ex: You’ve chosen to receive msgs from our company. Reply YES to confirm, HELP for help or STOP to ccancel. Up to 5 msg/mo. Msg&data rates may apply.</span>
                                    </div>
                                </div>
                            </div>
                        </div>    

                        <div class="write-mess-body">
                            <h4 class="portlet-heading2">Question 1</h4>
                            <p class="portlet-subheading">
                                Enter the multiple choice question you would like to ask. 
                            </p>

                            <div class="row">
                                <div class="col-md-11">
                                    <div class="form-gd">
                                        <div class="form-group">
                                            <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                            <span class="help-block">ex: Thank you for participating in the {%CompanyName%} Market Test. How useful is the product to your group/business?</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-11">
                                    <div class="row">
                                        <div class="col-lg-9">
                                            <h4 class="portlet-heading2">Answer Choices List</h4>

                                            <div class="answer-wrap clearfix">
                                                <label for="">Format Answers:</label>
                                                <div class="answer-wrap-select">
                                                    <select name="" id="" class="bs-select form-control">
                                                        <option value=""></option>
                                                        <option value="">asfas</option>
                                                        <option value="">sdfsdf</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-11">
                                                    <div class="wrapper-answer-text">
                                                        <div class="form-gd field-anser-holder">
                                                            <div class="row row-small">
                                                                <div class="col-xs-8 col-sm-10">
                                                                    <div class="form-group mb-0">
                                                                        <label>Answer Text</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group text-center mb-0">
                                                                        <label>S-Val</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group text-center mb-0">
                                                                        <label>Delete</label>
                                                                    </div>
                                                                </div>
                                                            </div>                                                                            
                                                            <div class="row row-small">
                                                                <div class="col-xs-8 col-sm-10">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control readonly-val text-center" value="1" readonly />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <button class="form-control delete-answer">
                                                                            <i class="fa fa-minus"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row row-small">
                                                                <div class="col-xs-8 col-sm-10">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control readonly-val text-center" value="1" readonly />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <button class="form-control delete-answer">
                                                                            <i class="fa fa-minus"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row row-small">
                                                                <div class="col-xs-8 col-sm-10">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control readonly-val text-center" value="1" readonly />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <button class="form-control delete-answer">
                                                                            <i class="fa fa-minus"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row row-small">
                                                                <div class="col-xs-8 col-sm-10">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control readonly-val text-center" value="1" readonly />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <button class="form-control delete-answer">
                                                                            <i class="fa fa-minus"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row row-small">
                                                                <div class="col-xs-8 col-sm-10">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control readonly-val text-center" value="1" readonly />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <button class="form-control delete-answer">
                                                                            <i class="fa fa-minus"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row row-small">
                                                            <div class="col-xs-4 col-xs-offset-8 col-sm-2 col-sm-offset-10">
                                                                <a href="##" id="add-more-answer" class="btn btn-block grey-mint btn-create-new-list add-more-answer">add more</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <h4 class="portlet-heading2">Answer Template (Quick Add)</h4>
                                            <h4 class="portlet-heading3 mb-30">If you would like, select one and then hit the APPLY button.</h4>
                                            
                                            <div class="row">
                                                <div class="col-lg-10">
                                                    <div class="list-answer-template">
                                                        <ul class="nav">
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-1">
                                                                <label for="ans-temp-1">Yes or No?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-2">
                                                                <label for="ans-temp-2">True or False?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-3">
                                                                <label for="ans-temp-3">Agree or Disagree?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-4">
                                                                <label for="ans-temp-4">Useful or Not?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-5">
                                                                <label for="ans-temp-5">Easy or Not?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-6">
                                                                <label for="ans-temp-6">Likely or Not?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-7">
                                                                <label for="ans-temp-7">Important or Not?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-8">
                                                                <label for="ans-temp-8">Satisfied or Not?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-9">
                                                                <label for="ans-temp-9">Net Promoter Score</label>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <a href="##" class="btn green-gd">apply</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h4 class="portlet-heading2">Response Interval Settings (Optional)</h4>
                                            <p class="des-for-heading">
                                                Response Intervals are how long you wish to wait for a message recipient to reply. By default (0) the wait time is infinite. Response Intervals give you dynamic control on 
                                                how long you wish to wait before moving on to the next specified Action if no response is received.
                                            </p>

                                            <div class="form-gd">
                                                <div class="row row-small">
                                                    <div class="col-lg-2 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="">Interval Time</label>
                                                            <select class="bs-select form-control">
                                                                <option value=""></option>
                                                                <option value="">tyru</option>
                                                                <option value="">tyi</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="">&nbsp;</label>
                                                            <select class="bs-select form-control">
                                                                <option value=""></option>
                                                                <option value="">fgjhfg</option>
                                                                <option value="">fghfg</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="">Retry on No Response?</label>
                                                            <select class="bs-select form-control">
                                                                <option value=""></option>
                                                                <option value="">etert</option>
                                                                <option value="">vbhnfg</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="">Still No Response?</label>
                                                            <select class="bs-select form-control">
                                                                <option value=""></option>
                                                                <option value="">fghfgjh</option>
                                                                <option value="">dfhgdfg</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row row-small mt-30 mb-30">
                                        <div class="col-lg-8">
                                            <div class="add-another-question">
                                                <a href="##" class="btn green-gd btn-add-another">add another question</a>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="hide">pagination</div>
                                        </div>
                                    </div>

                                    <div class="row box-multi-choice">
                                        <div class="col-lg-12">
                                            <p class="portlet-subheading mb-0">Enter the message a customer will receive if they enter an invalid response.</p>
                                            <i class="sub-tit">Tip: ask them to reconsider and retry.</i>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-gd">
                                                        <div class="form-group">
                                                            <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                                            <span class="help-block"><i>ex: oops! Looks like you entered an invalid response. Please try again.</i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                                                
                        </div>

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Select a Subscriber List</h4>
                        <p class="portlet-subheading">
                            A Subscriber List is a list of subscribers that have opted into your campaign by texting your keyword to your short code. <br>
                            Example: “39492”. Sire automatically captures all of your subscribers phone numbers and saves them in your subscriber list. <br>
                            This is also to help keep non-compliant spammers at bay.
                        </p>

                        <div class="row row-small">
                            <div class="col-md-6 col-xs-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <select class="bs-select form-control">
                                            <option></option>
                                            <option>Individual 1</option>
                                            <option>Individual 2</option>
                                            <option>Individual 3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-6">
                                <a data-toggle="modal" href="##basic2" class="btn grey-mint btn-create-new-list"><i class="fa fa-plus"></i> create new list</a>
                            </div>
                        </div>

                        <!-- START MODAL -->
                        <div class="modal be-modal fade" id="basic2" tabindex="-1" role="basic" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">                                            
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    </div>
                                    <div class="modal-body">
                                        <h4 class="be-modal-title">Add new subcriber list</h4>
                                        <div class="form-gd">
                                            <div class="form-group">
                                                <label for="">Subscriber List Name: </label>
                                                <input type="text" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <button type="button" class="btn green-gd">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- END MODAL -->

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                  
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Preview</h4>

                        <div class="wrap-preview">
                            <div class="preview-screen">
                                <img class="bg-preview-screen" src="../assets/layouts/layout4/img/preview-phone.png" alt="">
                                <div class="phone-screen">
                                    <div class="inner-phone-screen scroller">
                                        <div class="bubble me">
                                            FREELUNCH
                                        </div>
                                        <div class="bubble guess">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas culpa, molestias, numquam vitae vel assumenda.
                                        </div>
                                    </div>
                                    <div class="type-screen">
                                        <textarea id="SMSTextInputArea" name="SMSTextInputArea" maxlength="160"></textarea>
                                        <button id="SMSSend" type="button" class="btn-send-area"><i class="fa fa-arrow-up"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="for-blue-mess">
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <label>Give Your Campaign a Name</label>
                                        <input type="text" class="form-control" placeholder="">
                                        <span class="help-block">ex: Simple Subscribers (Opt In) 2017-01-23 18:09:13</span>
                                    </div>

                                    <div class="form-group">
                                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>                                    
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Company Name</h4>
                        <p class="portlet-subheading">
                            Phone carriers require all business messages to be easily identifiable to the company who sends them. Your company name will appear before your message.
                        </p>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>    

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Select a Subscriber List</h4>
                        <p class="portlet-subheading">
                            A Subscriber List is a list of subscribers that have opted into your campaign by texting your keyword to your short code. <br>
                            Example: “39492”. Sire automatically captures all of your subscribers phone numbers and saves them in your subscriber list. <br>
                            This is also to help keep non-compliant spammers at bay.
                        </p>

                        <div class="row row-small">
                            <div class="col-md-6 col-xs-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <select class="bs-select form-control">
                                            <option></option>
                                            <option>Individual 1</option>
                                            <option>Individual 2</option>
                                            <option>Individual 3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-6">
                                <a data-toggle="modal" href="##basic2" class="btn grey-mint btn-create-new-list"><i class="fa fa-plus"></i> create new list</a>
                            </div>
                        </div>

                        <!-- START MODAL -->
                        <div class="modal be-modal fade" id="basic2" tabindex="-1" role="basic" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">                                            
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    </div>
                                    <div class="modal-body">
                                        <h4 class="be-modal-title">Add new subcriber list</h4>
                                        <div class="form-gd">
                                            <div class="form-group">
                                                <label for="">Subscriber List Name: </label>
                                                <input type="text" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <button type="button" class="btn green-gd">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- END MODAL -->

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                  
                    </div>
                </div>        
                
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Schedule Your Blast</h4>
                        <p class="portlet-subheading">
                            Schedule a start time for this blast. If left blank, the blast will trigger immediately.
                        </p>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-gd">
                                    <div class="row row-small">
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">Month</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">Day</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                        </div>                                                            
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">Year</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                        </div>                                                            
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">00:00 am</option>
                                                    <option value="">00:01 am</option>
                                                    <option value="">00:02 am</option>
                                                </select>
                                            </div>
                                        </div> 
                                    </div>
                                       
                                </div>
                            </div>
                        </div>    

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                
                    </div>
                </div>                                                                

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2 mb-30">Write Your Message</h4>   

                        <div class="write-mess-body">
                            <h4 class="portlet-heading2">Question 1</h4>
                            <p class="portlet-subheading">
                                Enter the multiple choice question you would like to ask. 
                            </p>

                            <div class="row">
                                <div class="col-md-11">
                                    <div class="form-gd">
                                        <div class="form-group">
                                            <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                            <span class="help-block">ex: Thank you for participating in the {%CompanyName%} Market Test. How useful is the product to your group/business?</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-11">
                                    <div class="row">
                                        <div class="col-lg-9">
                                            <h4 class="portlet-heading2">Answer Choices List</h4>

                                            <div class="answer-wrap clearfix">
                                                <label for="">Format Answers:</label>
                                                <div class="answer-wrap-select">
                                                    <select name="" id="" class="bs-select form-control">
                                                        <option value=""></option>
                                                        <option value="">asfas</option>
                                                        <option value="">sdfsdf</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-11">
                                                    <div class="wrapper-answer-text">
                                                        <div class="form-gd field-anser-holder">
                                                            <div class="row row-small">
                                                                <div class="col-xs-8 col-sm-10">
                                                                    <div class="form-group mb-0">
                                                                        <label>Answer Text</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group text-center mb-0">
                                                                        <label>S-Val</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group text-center mb-0">
                                                                        <label>Delete</label>
                                                                    </div>
                                                                </div>
                                                            </div>                                                                            
                                                            <div class="row row-small">
                                                                <div class="col-xs-8 col-sm-10">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control readonly-val text-center" value="1" readonly />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <button class="form-control delete-answer">
                                                                            <i class="fa fa-minus"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row row-small">
                                                                <div class="col-xs-8 col-sm-10">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control readonly-val text-center" value="1" readonly />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <button class="form-control delete-answer">
                                                                            <i class="fa fa-minus"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row row-small">
                                                                <div class="col-xs-8 col-sm-10">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control readonly-val text-center" value="1" readonly />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <button class="form-control delete-answer">
                                                                            <i class="fa fa-minus"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row row-small">
                                                                <div class="col-xs-8 col-sm-10">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control readonly-val text-center" value="1" readonly />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <button class="form-control delete-answer">
                                                                            <i class="fa fa-minus"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row row-small">
                                                                <div class="col-xs-8 col-sm-10">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control readonly-val text-center" value="1" readonly />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-2 col-sm-1">
                                                                    <div class="form-group">
                                                                        <button class="form-control delete-answer">
                                                                            <i class="fa fa-minus"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row row-small">
                                                            <div class="col-xs-4 col-xs-offset-8 col-sm-2 col-sm-offset-10">
                                                                <a href="##" id="add-more-answer" class="btn btn-block grey-mint btn-create-new-list add-more-answer">add more</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <h4 class="portlet-heading2">Answer Template (Quick Add)</h4>
                                            <h4 class="portlet-heading3 mb-30">If you would like, select one and then hit the APPLY button.</h4>
                                            
                                            <div class="row">
                                                <div class="col-lg-10">
                                                    <div class="list-answer-template">
                                                        <ul class="nav">
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-1">
                                                                <label for="ans-temp-1">Yes or No?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-2">
                                                                <label for="ans-temp-2">True or False?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-3">
                                                                <label for="ans-temp-3">Agree or Disagree?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-4">
                                                                <label for="ans-temp-4">Useful or Not?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-5">
                                                                <label for="ans-temp-5">Easy or Not?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-6">
                                                                <label for="ans-temp-6">Likely or Not?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-7">
                                                                <label for="ans-temp-7">Important or Not?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-8">
                                                                <label for="ans-temp-8">Satisfied or Not?</label>
                                                            </li>
                                                            <li>
                                                                <input type="radio" name="ans-group" id="ans-temp-9">
                                                                <label for="ans-temp-9">Net Promoter Score</label>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <a href="##" class="btn green-gd">apply</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h4 class="portlet-heading2">Response Interval Settings (Optional)</h4>
                                            <p class="des-for-heading">
                                                Response Intervals are how long you wish to wait for a message recipient to reply. By default (0) the wait time is infinite. Response Intervals give you dynamic control on 
                                                how long you wish to wait before moving on to the next specified Action if no response is received.
                                            </p>

                                            <div class="form-gd">
                                                <div class="row row-small">
                                                    <div class="col-lg-2 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="">Interval Time</label>
                                                            <select class="bs-select form-control">
                                                                <option value=""></option>
                                                                <option value="">tyru</option>
                                                                <option value="">tyi</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="">&nbsp;</label>
                                                            <select class="bs-select form-control">
                                                                <option value=""></option>
                                                                <option value="">fgjhfg</option>
                                                                <option value="">fghfg</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="">Retry on No Response?</label>
                                                            <select class="bs-select form-control">
                                                                <option value=""></option>
                                                                <option value="">etert</option>
                                                                <option value="">vbhnfg</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="">Still No Response?</label>
                                                            <select class="bs-select form-control">
                                                                <option value=""></option>
                                                                <option value="">fghfgjh</option>
                                                                <option value="">dfhgdfg</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row row-small mt-30 mb-30">
                                        <div class="col-lg-8">
                                            <div class="add-another-question">
                                                <a href="##" class="btn green-gd btn-add-another">add another question</a>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="hide">pagination</div>
                                        </div>
                                    </div>
                                </div>
                            </div>                                                
                        </div>

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Preview</h4>

                        <div class="wrap-preview">
                            <div class="preview-screen">
                                <img class="bg-preview-screen" src="../assets/layouts/layout4/img/preview-phone.png" alt="">
                                <div class="phone-screen">
                                    <div class="inner-phone-screen scroller">
                                        <div class="bubble me">
                                            FREELUNCH
                                        </div>
                                        <div class="bubble guess">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas culpa, molestias, numquam vitae vel assumenda.
                                        </div>
                                    </div>
                                    <div class="type-screen">
                                        <textarea id="SMSTextInputArea" name="SMSTextInputArea" maxlength="160"></textarea>
                                        <button id="SMSSend" type="button" class="btn-send-area"><i class="fa fa-arrow-up"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                                    
            </div>
        </div>                            
    </div>
    <!-- End For Multi Choice Survey -->

    <!-- Start For Timed Message -->
    <div class="content-for-timed-message">
        <div class="portlet light bordered">
            <div class="portlet-body">
                <h4 class="portlet-heading">Timed Series of Messages</h4>
                <h4 class="portlet-heading2 mb-20">Who do you want to send this message to?</h4>      

                <div class="row com-flex com-flex-wrap row-subscriber" data-uk-switcher="{connect:'##message-switcher7'}">
                    <div class="col-lg-6">
                        <div class="subscriber-item greenbox">
                            <div class="row com-flex com-flex-middle">
                                <div class="col-xs-4 text-center">
                                    <img class="img-responsive" src="../assets/layouts/layout4/img/subscriber-keyword.png" alt="">
                                </div>
                                <div class="col-xs-8">
                                    <h4 class="titleh4">Subscribers who send your Keyword to your short code</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="subscriber-item bluebox">
                            <div class="row com-flex com-flex-middle">
                                <div class="col-xs-4 text-center">
                                    <img class="img-responsive" src="../assets/layouts/layout4/img/subscriber-sound.png" alt="">
                                </div>
                                <div class="col-xs-8">
                                    <h4 class="titleh4">Subscribers on an existing list</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    

                <div class="mb-20"></div>                    
            </div>
        </div>

        <div id="message-switcher7" class="uk-switcher">
            <div class="for-green-mess">
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <label>Give Your Campaign a Name</label>
                                        <input type="text" class="form-control" placeholder="">
                                        <span class="help-block">ex: Simple Subscribers (Opt In) 2017-01-23 18:09:13</span>
                                    </div>

                                    <div class="form-group">
                                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>                                    
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Company Name</h4>
                        <p class="portlet-subheading">
                            Phone carriers require all business messages to be easily identifiable to the company who sends them. Your company name will appear before your message.
                        </p>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>    

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h4 class="portlet-heading2">Choose a Keyword</h4>
                                <p class="portlet-subheading">
                                    A Keywords is a text message that your customer sends to a Short Code using their cell phone. <br>
                                    Sire will then respond with a customizable interactive Message flow, triggering a new Sire SMS session. 
                                </p>
                                <p class="portlet-subheading">
                                    Keywords are one of the fastest ways you can begin to communicate.
                                </p>

                                <div class="row row-small">
                                    <div class="col-md-9">
                                        <div class="form-gd">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="">
                                                <span class="help-block has-error">This Keyword is not long enough. Your current account requires 8 number of characters (not including spaces)</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                            </div>
                            <div class="col-md-4">
                                <img class="img-responsive" src="../assets/layouts/layout4/img/phone-choose-keyword.png" alt="">
                            </div>
                        </div>                              
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Write Your Message</h4>
                        <p class="portlet-subheading">
                            Once your customer texts your Keyword to your Short Code, they will need to “opt-in” by responding to this automated response. <br>
                            Type your automated “opt-in” request message.
                        </p>

                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                        <span class="help-block"><i>ex: You've chosen to receive msgs from our company. Reply YES to confirm, HELP for help or STOP to cancel. Up to 5 msg/mo. Msg&Data rates may apply</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <p class="portlet-subheading mb-0">Enter the message a customer will receive if they enter an invalid response.</p>
                        <i class="sub-tit">Tip: ask them to reconsider and retry.</i>

                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                        <span class="help-block"><i>ex: You have chosen not to join this program. Join later by re-sending Keyword "{%KEYWORD%}" to Short Code "{%SHORTCODE%}"</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <p class="portlet-subheading">Enter the first message you would like to send to your subscribers</p>

                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="wrap-box-another-mesage">
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="##" class="btn green-gd add-plus">add another mesage +</a>
                                </div>
                            </div>

                            <div class="wrap-item-message">
                                <div class="item-another-mesage">
                                    <div class="row">
                                        <div class="col-md-11">
                                            <div class="form-gd">
                                                <div class="form-group">
                                                    <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                            

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Select a Subscriber List</h4>
                        <p class="portlet-subheading">
                            A Subscriber List is a list of subscribers that have opted into your campaign by texting your keyword to your short code. <br>
                            Example: “39492”. Sire automatically captures all of your subscribers phone numbers and saves them in your subscriber list. <br>
                            This is also to help keep non-compliant spammers at bay.
                        </p>

                        <div class="row row-small">
                            <div class="col-md-6 col-xs-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <select class="bs-select form-control">
                                            <option></option>
                                            <option>Individual 1</option>
                                            <option>Individual 2</option>
                                            <option>Individual 3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-6">
                                <a data-toggle="modal" href="##basic2" class="btn grey-mint btn-create-new-list"><i class="fa fa-plus"></i> create new list</a>
                            </div>
                        </div>

                        <!-- START MODAL -->
                        <div class="modal be-modal fade" id="basic2" tabindex="-1" role="basic" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">                                            
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    </div>
                                    <div class="modal-body">
                                        <h4 class="be-modal-title">Add new subcriber list</h4>
                                        <div class="form-gd">
                                            <div class="form-group">
                                                <label for="">Subscriber List Name: </label>
                                                <input type="text" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <button type="button" class="btn green-gd">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- END MODAL -->

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                  
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Preview</h4>

                        <div class="wrap-preview">
                            <div class="preview-screen">
                                <img class="bg-preview-screen" src="../assets/layouts/layout4/img/preview-phone.png" alt="">
                                <div class="phone-screen">
                                    <div class="inner-phone-screen scroller">
                                        <div class="bubble me">
                                            FREELUNCH
                                        </div>
                                        <div class="bubble guess">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas culpa, molestias, numquam vitae vel assumenda.
                                        </div>
                                    </div>
                                    <div class="type-screen">
                                        <textarea id="SMSTextInputArea" name="SMSTextInputArea" maxlength="160"></textarea>
                                        <button id="SMSSend" type="button" class="btn-send-area"><i class="fa fa-arrow-up"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="for-blue-mess">
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <label>Give Your Campaign a Name</label>
                                        <input type="text" class="form-control" placeholder="">
                                        <span class="help-block">ex: Simple Subscribers (Opt In) 2017-01-23 18:09:13</span>
                                    </div>

                                    <div class="form-group">
                                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>                                    
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Company Name</h4>
                        <p class="portlet-subheading">
                            Phone carriers require all business messages to be easily identifiable to the company who sends them. Your company name will appear before your message.
                        </p>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>    

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Select a Subscriber List</h4>
                        <p class="portlet-subheading">
                            A Subscriber List is a list of subscribers that have opted into your campaign by texting your keyword to your short code. <br>
                            Example: “39492”. Sire automatically captures all of your subscribers phone numbers and saves them in your subscriber list. <br>
                            This is also to help keep non-compliant spammers at bay.
                        </p>

                        <div class="row row-small">
                            <div class="col-md-6 col-xs-6">
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <select class="bs-select form-control">
                                            <option></option>
                                            <option>Individual 1</option>
                                            <option>Individual 2</option>
                                            <option>Individual 3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-6">
                                <a data-toggle="modal" href="##basic2" class="btn grey-mint btn-create-new-list"><i class="fa fa-plus"></i> create new list</a>
                            </div>
                        </div>

                        <!-- START MODAL -->
                        <div class="modal be-modal fade" id="basic2" tabindex="-1" role="basic" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">                                            
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    </div>
                                    <div class="modal-body">
                                        <h4 class="be-modal-title">Add new subcriber list</h4>
                                        <div class="form-gd">
                                            <div class="form-group">
                                                <label for="">Subscriber List Name: </label>
                                                <input type="text" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <button type="button" class="btn green-gd">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- END MODAL -->

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                  
                    </div>
                </div>        
                
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Schedule Your Blast</h4>
                        <p class="portlet-subheading">
                            Schedule a start time for this blast. If left blank, the blast will trigger immediately.
                        </p>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-gd">
                                    <div class="row row-small">
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">Month</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">Day</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                        </div>                                                            
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">Year</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                        </div>                                                            
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <select class="bs-select form-control">
                                                    <option value="">00:00 am</option>
                                                    <option value="">00:01 am</option>
                                                    <option value="">00:02 am</option>
                                                </select>
                                            </div>
                                        </div> 
                                    </div>
                                       
                                </div>
                            </div>
                        </div>    

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                
                    </div>
                </div>                                                                

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Write Your Message</h4>
                        <p class="portlet-subheading">
                            Enter the Yes/No question you would like to ask.
                        </p>

                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                        <span class="help-block"><i>ex: Our planning committee meeting is on Wed at 6:00 PM. Can you make it?</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <p class="portlet-subheading">Enter your message if the response is “Yes”</p>

                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <input type="text" class="form-control" />
                                        <span class="help-block"><i>ex: Great! See you there.</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <p class="portlet-subheading">Enter your message if the response is No</p>

                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <input type="text" class="form-control" />
                                        <span class="help-block"><i>ex: Bummer. We’ll miss your input. Please be there next time!</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                    </div>
                </div>

                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <h4 class="portlet-heading2">Preview</h4>

                        <div class="wrap-preview">
                            <div class="preview-screen">
                                <img class="bg-preview-screen" src="../assets/layouts/layout4/img/preview-phone.png" alt="">
                                <div class="phone-screen">
                                    <div class="inner-phone-screen scroller">
                                        <div class="bubble me">
                                            FREELUNCH
                                        </div>
                                        <div class="bubble guess">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas culpa, molestias, numquam vitae vel assumenda.
                                        </div>
                                    </div>
                                    <div class="type-screen">
                                        <textarea id="SMSTextInputArea" name="SMSTextInputArea" maxlength="160"></textarea>
                                        <button id="SMSSend" type="button" class="btn-send-area"><i class="fa fa-arrow-up"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                                    
            </div>
        </div>                            
    </div>
    <!-- End For Timed Message -->

    <!-- Start For Collect Feedback -->
    <div class="content-for-collect-feedback">
        <div class="portlet light bordered">
            <div class="portlet-body">
                <h4 class="portlet-heading">Collect Feedback</h4>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-gd form-lb-large">
                            <div class="form-group">
                                <label>Give Your Campaign a Name</label>
                                <input type="text" class="form-control" placeholder="">
                                <span class="help-block">ex: Simple Subscribers (Opt In) 2017-01-23 18:09:13</span>
                            </div>

                            <div class="form-group">
                                <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                            </div>
                        </div>
                    </div>
                </div>                                    
            </div>
        </div>

        <div class="portlet light bordered">
            <div class="portlet-body">
                <h4 class="portlet-heading2">Company Name</h4>
                <p class="portlet-subheading">
                    Phone carriers require all business messages to be easily identifiable to the company who sends them. Your company name will appear before your message.
                </p>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-gd form-lb-large">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>    

                <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                
            </div>
        </div>

        <div class="portlet light bordered">
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-8">
                        <h4 class="portlet-heading2">Choose a Keyword</h4>
                        <p class="portlet-subheading">
                            A Keywords is a text message that your customer sends to a Short Code using their cell phone. <br>
                            Sire will then respond with a customizable interactive Message flow, triggering a new Sire SMS session. 
                        </p>
                        <p class="portlet-subheading">
                            Keywords are one of the fastest ways you can begin to communicate.
                        </p>

                        <div class="row row-small">
                            <div class="col-md-9">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="">
                                        <span class="help-block has-error">This Keyword is not long enough. Your current account requires 8 number of characters (not including spaces)</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                    </div>
                    <div class="col-md-4">
                        <img class="img-responsive" src="../assets/layouts/layout4/img/phone-choose-keyword.png" alt="">
                    </div>
                </div>                              
            </div>
        </div>

        <div class="portlet light bordered">
            <div class="portlet-body">
                <h4 class="portlet-heading2">Write Your Message</h4>
                <p class="portlet-subheading">
                    Enter the message your customers will receive when they text your Keyword to your Short Code.
                </p>

                <div class="row">
                    <div class="col-md-11">
                        <div class="form-gd">
                            <div class="form-group">
                                <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                <span class="help-block"><i>ex: Your comments are important to us. Please tell us more about your visit. Your response will be forwarded right away to the manager for immediate review.</i></span>
                            </div>
                        </div>
                    </div>
                </div>

                <p class="portlet-subheading">Let your subscribers know that you have received their response.</p>

                <div class="row">
                    <div class="col-md-11">
                        <div class="form-gd">
                            <div class="form-group">
                                <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                <span class="help-block">ex: {%Company%} is committed to excellence. With your feedback, we will be able to respond with expanded and improved services.</span>
                            </div>
                        </div>
                    </div>
                </div>

                <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
            </div>
        </div>

        <div class="portlet light bordered">
            <div class="portlet-body">
                <h4 class="portlet-heading2">Preview of First Message</h4>

                <div class="wrap-preview">
                    <div class="preview-screen">
                        <img class="bg-preview-screen" src="../assets/layouts/layout4/img/preview-phone.png" alt="">
                        <div class="phone-screen">
                            <div class="inner-phone-screen scroller">
                                <div class="bubble me">
                                    FREELUNCH
                                </div>
                                <div class="bubble guess">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas culpa, molestias, numquam vitae vel assumenda.
                                </div>
                            </div>
                            <div class="type-screen">
                                <textarea id="SMSTextInputArea" name="SMSTextInputArea" maxlength="160"></textarea>
                                <button id="SMSSend" type="button" class="btn-send-area"><i class="fa fa-arrow-up"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>                            
    </div>
    <!-- End For Collect Feedback -->

    <!-- Start For Single OnTime -->
    <div class="content-for-single-ontime">
        <div class="portlet light bordered">
            <div class="portlet-body">
                <h4 class="portlet-heading">Single One-Time Message</h4>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-gd form-lb-large">
                            <div class="form-group">
                                <label>Give Your Campaign a Name</label>
                                <input type="text" class="form-control" placeholder="">
                                <span class="help-block">ex: Simple Subscribers (Opt In) 2017-01-23 18:09:13</span>
                            </div>

                            <div class="form-group">
                                <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                            </div>
                        </div>
                    </div>
                </div>                                    
            </div>
        </div>

        <div class="portlet light bordered">
            <div class="portlet-body">
                <h4 class="portlet-heading2">Company Name</h4>
                <p class="portlet-subheading">
                    Phone carriers require all business messages to be easily identifiable to the company who sends them. Your company name will appear before your message.
                </p>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-gd form-lb-large">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>    

                <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>                                
            </div>
        </div>

        <div class="portlet light bordered">
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-8">
                        <h4 class="portlet-heading2">Choose a Keyword</h4>
                        <p class="portlet-subheading">
                            A Keywords is a text message that your customer sends to a Short Code using their cell phone. <br>
                            Sire will then respond with a customizable interactive Message flow, triggering a new Sire SMS session. 
                        </p>
                        <p class="portlet-subheading">
                            Keywords are one of the fastest ways you can begin to communicate.
                        </p>

                        <div class="row row-small">
                            <div class="col-md-9">
                                <div class="form-gd">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="">
                                        <span class="help-block has-error">This Keyword is not long enough. Your current account requires 8 number of characters (not including spaces)</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
                    </div>
                    <div class="col-md-4">
                        <img class="img-responsive" src="../assets/layouts/layout4/img/phone-choose-keyword.png" alt="">
                    </div>
                </div>                              
            </div>
        </div>

        <div class="portlet light bordered">
            <div class="portlet-body">
                <h4 class="portlet-heading2">Write Your Message</h4>
                <p class="portlet-subheading">
                    Enter the message your customers will receive when they text your Keyword to your Short Code.
                </p>

                <div class="row">
                    <div class="col-md-11">
                        <div class="form-gd">
                            <div class="form-group">
                                <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
                                <span class="help-block"><i>ex: Thanks for attending our seminar. Here's the coupon code we promised: "SEMINAR1"</i></span>
                            </div>
                        </div>
                    </div>
                </div>

                <a href="javascript:;" class="btn green-gd campaign-next">Next <i class="fa fa-caret-down"></i></a>
            </div>
        </div>

        <div class="portlet light bordered">
            <div class="portlet-body">
                <h4 class="portlet-heading2">Preview of First Message</h4>

                <div class="wrap-preview">
                    <div class="preview-screen">
                        <img class="bg-preview-screen" src="../assets/layouts/layout4/img/preview-phone.png" alt="">
                        <div class="phone-screen">
                            <div class="inner-phone-screen scroller">
                                <div class="bubble me">
                                    FREELUNCH
                                </div>
                                <div class="bubble guess">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas culpa, molestias, numquam vitae vel assumenda.
                                </div>
                            </div>
                            <div class="type-screen">
                                <textarea id="SMSTextInputArea" name="SMSTextInputArea" maxlength="160"></textarea>
                                <button id="SMSSend" type="button" class="btn-send-area"><i class="fa fa-arrow-up"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>                            
    </div>
    <!-- Start For Single OnTime -->
</cfoutput>