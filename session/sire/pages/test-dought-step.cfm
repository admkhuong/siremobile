<cfset variables.menuToUse = 1 />
<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addJs("../assets/global/plugins/chartjs/Chart.bundle.js", true)
        .addJs("../assets/pages/scripts/doughnut.js");
</cfscript>

<div class="portlet light bordered">
    <div class="portlet-body">
        <h4>Test Doughnut Step</h4>
        <!--- <div class="step-holder" >
            <canvas id="myStep" width="200" height="200"></canvas>
        </div> --->

        <a href="##" id="nextStep" class="btn green-gd mt-20">Next <i class="fa fa-caret-down"></i></a>
    </div>
</div>


<cfsavecontent variable="variables.portleft">
<div class="portlet light bordered box-widget-sidebar">
    <div class="portlet-body">
        <div class="wrap-dial-step">
            <h4 class="stt-step">Progress</h4>
            <div class="step-holder">
                <canvas id="myStep"></canvas>
            </div>
        </div>
    </div>
</div>
</cfsavecontent>

<cfparam name="variables._title" default="Doughnut - Sire">

<cfinclude template="../views/layouts/master.cfm">