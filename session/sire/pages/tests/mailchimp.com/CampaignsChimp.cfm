<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfsetting requesttimeout="300">

<cfparam name="url.api_key" default="999166486eb24581bed46922e3458e2c-us15">

<cfparam name="result" default="">
<cfparam name="campaignInfo" default={}>
<cfparam name="campaignsInfo" default=[]>
<cfparam name="apiroot" default="https://us15.api.mailchimp.com/3.0/">

<cfhttp url="#apiroot#campaigns" result="result" method="GET" charset="utf-8">
	<cfhttpparam type="header" name="content-type" value="application/json">
	<cfhttpparam type="header" name="Authorization" value="#'Basic ' & ToBase64('anystring:' & url.api_key)#">
</cfhttp>
<cfset campaignsInfo = DeserializeJSON(result.filecontent)>

<cfoutput>
<div class="row dashboard">
	<div class="col-xs-12">
		<div class="portlet light bordered">
			<h4 class="portlet-heading">Test Trigger to MailChimp</h4>
			<cfif StructKeyExists(campaignsInfo,'total_items') AND campaignsInfo.total_items GT 0>
				<table class="table table-striped table-bordered table-responsive">
					<tr>
						<th>id</th>
						<th>title</th>
						<th>status</th>
					</tr>
					<cfloop array="#campaignsInfo.campaigns#" index="campaignInfo">
					<tr>
						<td>#campaignInfo.id#</td>
						<td>#campaignInfo.settings.title#</td>
						<td>#campaignInfo.status#</td>
					</tr>
					</cfloop>
				</table>
			<cfelse>
				Total Number of Campaign: 0
			</cfif>
		</div>
	</div>
</div>
</cfoutput>
<cfparam name="variables._title" default="Test Trigger to MailChimp">
<cfinclude template="../../../views/layouts/master.cfm">