<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission" />

<cfsetting requesttimeout="300">

<cfparam name="form.send" default="false">
<cfparam name="form.emails" default="">
<cfparam name="form.mail_chimp_campaign_id" default="">
<cfparam name="form.api_key" default="6e1d6bf0d9573de68ffdac28545bb6e8-us15">
<!--- 999166486eb24581bed46922e3458e2c-us15 --->

<cfparam name="msg" default="">
<cfparam name="body" default={}>
<cfparam name="result" default="">
<cfparam name="newMember" default={}>
<cfparam name="newMembers" default=[]>
<cfparam name="campaignInfo" default={}>
<cfparam name="apiroot" default="https://us15.api.mailchimp.com/3.0/">
<!--- "mwon@siremobile.com" --->
<cfparam name="fromEmail" default="trungnv6371@seta-international.vn">

<cfif form.send>

	<cfset _Authorization = 'Basic ' & ToBase64('anystring:' & form.api_key)> 

	<cfhttp url="#apiroot#campaigns/#form.mail_chimp_campaign_id#" result="result" method="GET" charset="utf-8">
		<cfhttpparam type="header" name="content-type" value="application/json">
		<cfhttpparam type="header" name="Authorization" value="#_Authorization#">
	</cfhttp>

	<cfset campaignInfo = DeserializeJSON(result.filecontent)>
	
	<cfif result.status_code EQ 200 AND StructKeyExists(campaignInfo, 'id')>
		<cfif IsStruct(campaignInfo.recipients) AND StructKeyExists(campaignInfo.recipients, 'list_id')>
			
			<cfset campaignList = campaignInfo.recipients>
	
			<cfset newMembers = []>
			<cfset emailMembers = ListToArray(form.emails, ',')>
			
			<cfloop array="#emailMembers#" index="emailMemberElement" >
			
				<cfset newMemberBody = {
					"email_address": emailMemberElement, 
					"status": "subscribed"
				}>
				<cfhttp url="#apiroot#lists/#campaignList.list_id#/members" result="result" method="POST" charset="utf-8">
					<cfhttpparam type="header" name="content-type" value="application/json">
					<cfhttpparam type="header" name="Authorization" value="#_Authorization#">
					<cfhttpparam type="body" value="#SerializeJSON(newMemberBody)#">
				</cfhttp>
			
				<cfset newMember = DeserializeJSON(result.filecontent)>
				<cfset ArrayAppend(newMembers, newMember)>
				
				<cfif !StructKeyExists(newMember, 'id')>
					<cfset msg &= '- Add "' & emailMemberElement & '" to this list: ' & newMember.title & '<br>' & newMember.detail & '<br>'>
				</cfif>
	
			</cfloop>
	
			<cfhttp url="#apiroot#/campaigns/#form.mail_chimp_campaign_id#/actions/send" result="result" method="POST" charset="utf-8">
				<cfhttpparam type="header" name="content-type" value="application/json">
				<cfhttpparam type="header" name="Authorization" value="#_Authorization#">
			</cfhttp>
	
			<!--- zzz --->
			<cfif IsJSON(result.filecontent)>
				<cfset campaignInfo = DeserializeJSON(result.filecontent)>
				<cfif StructKeyExists(campaignInfo, 'title')>
					<cfset msg &= '- Send the campaign: ' & campaignInfo.title & '<br>' & campaignInfo.detail & '<br>'>
				</cfif>
			</cfif>
			<!---<cfdump var="#result#"><cfabort>--->
		
		<cfelse>
			<cfset msg = "- Get campaign info: The campaign don't have a list">
		</cfif>
	<cfelse>
		<cfset msg = '- Get campaign info: ' & campaignInfo.title & '<br>' & campaignInfo.detail>
	</cfif>
</cfif>

<!--- ===== --->
<!--- https://sire.lc/session/sire/pages/tests/mailchimp.com/TestChimp.cfm --->
<!--- ===== --->

<cfoutput>

<div class="row dashboard">
	<div class="col-xs-12">
		<div class="portlet light bordered">
			
			
			
			<h4 class="portlet-heading">Test Trigger to MailChimp</h4>
			<h5>#msg#</h5>
			<form method="post">
				<label for="emails">
					Emails (comma separated list of emails):<br>
					<textarea id="emails" name="emails" cols="80" rows="8">#form.emails#</textarea>
				</label>
				<br>
				<label for="mail_chimp_campaign_id">
					MailChimp Campaign Id:<br>
					<input type="text" id="mail_chimp_campaign_id" name="mail_chimp_campaign_id" value="#form.mail_chimp_campaign_id#" size="50">
				</label>
				<br>
				<label for="api_key">
					API Key:<br>
					<input type="text" id="api_key" name="api_key" value="#form.api_key#" size="50">
					<input type="button" value="show list campaign" onclick="showListCampaign()">
				</label>
				<br>
				<input type="hidden" name="send" value="1">
				<button type="submit">
					Send
				</button>
			</form>



		</div>
	</div>
</div>
</cfoutput>
<script type="text/javascript">
	var showListCampaign = function() {
		window.open( 'CampaignsChimp.cfm?api_key=' + $('#api_key').val(), 'MailChimpCampaignList' );
	}
</script>
<cfparam name="variables._title" default="Test Trigger to MailChimp">
<cfinclude template="../../../views/layouts/master.cfm">
