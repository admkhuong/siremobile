


<cfoutput>
	<cfscript>
	    CreateObject("component","public.sire.models.helpers.layout")
	    .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
	    .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)	 
	    .addCss("../assets/layouts/layout4/css/custom3.css") 	     	 
	    .addJs("../assets/pages/scripts/ticket-type-management.js");  	    	    
	</cfscript>
</cfoutput>
<div class="portlet light bordered">
	<div class="portlet-body">
		<div class="new-inner-body-portlet2">
			<div class="form-gd">
				<div class="row">
					<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
						<h4 class="portlet-heading">Ticket Type List</h4>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div class="inner-addon left-addon">
							<i class="glyphicon glyphicon-search"></i>
							<input type="search" class="form-control" id="searchbox" placeholder="Search Ticket Type">
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div class="pull-right">
							<a href="#" class="btn btn-block green-gd pop-ticket-type" data-id="0" data-toggle="modal" data-action="new" data-target="#edit-ticket-type">Create New Type</a>
						</div>
					</div>
				</div>
				<div class="" style="margin-top: 10px">
			        <div class="row">
			            <div class="col-xs-12">
			                <div class="re-table">
			                    <div class="table-responsive">
			                        <table id="ticket-type-list" class="table table-striped table-responsive">
			            
			                        </table>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>			
			</div>
	    </div>
	</div>
</div>
<!--- modal--->
 <!--- edit ticket-type--->
 <div class="modal fade" id="edit-ticket-type" tabindex="-1" role="dialog" aria-hidden="true">
	<form name="form-edit-ticket-type" class="col-sm-12" id="form-edit-ticket-type">
	    <div class="modal-dialog">
	        <div class="modal-content">
	        	<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<b><h4 class="modal-title" id="modal-tit">
						NEW TYPE
					</h4></b>			
					<input type="hidden" name="ticketTypeId" id="ticketTypeId" value="0" autocomplete="off"/>		
				</div>
	            <div class="modal-body">	               
	               <div class="modal-body-inner">                  
	                    <div class="form-group">
	                       <input type="text" class="form-control validate[required, custom[noHTML], maxSize[255]]" maxlength="50" id="ticket-type-name" name="ticket-type-name" placeholder="Type" data-prompt-position="topLeft:10" autocomplete="off">	                       
	                    </div>	                    
	             	</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn green-cancel" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn green-gd"  id="btnUpdateticket-type">Save</button>
				</div>
			</div>
		</div>
	</form>
 </div>
<cfparam name="variables._title" default="Ticket Type Management">
<cfinclude template="../views/layouts/master.cfm">
