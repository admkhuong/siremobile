<cfset variables.menuToUse = 1>
<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")

        .addCss("../assets/layouts/layout4/css/custom3.css")        
        .addCss("/public/sire/css/jquery.timepicker.min.css", true)
        .addCss("/session/sire/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css", true)      
        .addJs("/session/sire/assets/global/plugins/bootstrap-datetimepicker/js/moment-with-locales.js", true)
        .addJs("/session/sire/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js", true)
        .addJs("/session/sire/assets/global/plugins/bootstrap-datetimepicker/js/jquery.timepicker.min.js", true)

        .addJs("/session/sire/assets/pages/scripts/trouble-ticket.js")
        .addJs('/session/sire/assets/global/scripts/filter-bar-generator.js', true)
        .addJs("../assets/global/plugins/daterangepicker/moment.min.js", true)
        .addCss("../assets/global/plugins/daterangepicker/daterangepicker.css", true)
        .addCss("/session/sire/assets/global/plugins/Chartist/chartist.min.css", true)
        .addCss("/session/sire/css/filter_table.css", true)        
        .addJs("/public/sire/js/select2.min.js", true)
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)
        .addJs("../assets/global/plugins/daterangepicker/daterangepicker.js", true)
        .addJs("/session/sire/css/trouble-ticket.css")   
     
</cfscript>
<cfinclude template="/session/sire/configs/paths.cfm">
<cfinclude template="/session/cfc/csc/constants.cfm">
<!--- Check trouble ticket batch valid --->
<cfinvoke method="CreateDefaultBatch" component="session.sire.models.cfc.troubleTicket.trouble-ticket" returnvariable="RetVarGetBatchID"></cfinvoke>

<div class="portlet light bordered">
    <!--- if trouble ticket batch is invalid then show notice to custome update --->
    <cfif RetVarGetBatchID.KEYWORDVALID NEQ 1>
        <div class="row">  
            <div class="col-sm-12">     
                <h4 class="text-danger">Please click <a href="/session/sire/pages/campaign-template-troubleticket" class="notice-title text-primary">here</a> to create KEYWORD.</h4>
            </div>
        </div>
    </cfif>
    <!--- Filter bar --->
    <div class="row">
        <div class="col-sm-3 col-md-2">
            <h2 class="page-title">Trouble Ticket App</h2>
        </div>
        <div class="col-sm-3 col-md-3">
            <div class="date-range trouble-ticket-daterang-style">
                <div class="reportrange">
                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                    <span class="time"></span> <span class="pull-right" style="margin-right: 0"><b class="caret"></b></span>
                    <input type="hidden" name="dateStart" value="">
                    <input type="hidden" name="dateEnd" value="">
                </div>
            </div>
        </div> 
        <div class="col-sm-6 col-md-7">           
            <div id="box-filter" class="form-gd">
            </div>
        </div> 
            
    </div>      
    <br/>   
    <div class="row">     
        <div class="col-md-9">
            <p>
            <div class="trouble-ticket-summary-margin-left-none">
                <strong>Total: <label id="txtTotal"></label></strong>                
            </div>        
            <div class="trouble-ticket-summary-margin-left-100">            
                <strong >Open: <label id="txtOpen"></label></strong>
            </div>        
            <div class="trouble-ticket-summary-margin-left-100">
                <strong>Closed: <label id="txtClosed"></label></strong>                            
            </div>    
             <div class="trouble-ticket-summary-margin-left-100">                
                <strong >On Hold: <label id="txtOnHold"></label></strong>
            </div>    
            <div class="trouble-ticket-summary-margin-left-100">
                <strong>In Progress: <label id="txtInProgress"></label></strong>                
            </div>   
            <div class="trouble-ticket-summary-margin-left-100">                
                <strong >Over Due: <label id="txtOverDue"></label></strong>
            </div>   
            </p>   
        </div>      
        <div class="col-md-3 text-right">
            <a href="#" class="btn green-gd mr-15 btn-add-new-ticket" data-toggle="modal" data-target="#modal-sections">Create New Ticket</a>
        </div>
    </div>
    <br/> 
    <!--- Listing --->
    <div class="row">
        <div class="col-md-12">
            <div class="re-table">
                <div class="table-responsive">
                    <table id="tblListTroubleTickets" class="table-striped dataTables_wrapper">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!--- Modal create new trouble ticket --->
<div id="modal-sections" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Create New Ticket</h4>
            </div>            
            <form id="create_ticket_form">            
            <div class="inner-update-cardholder">                
                    <cfoutput>
                        <div class="update_cardholder_info">                        
                            <div id="fset_cardholder_info" >                 
                            <div class="form-group">
                                <div class="row row-small">                                                        
                                    <label>Name:<span>*</span></label>
                                        <input type="text" class="form-control validate[required] maxSize[50] custom[noHTML]" id="TicketName" name="TicketName" placeholder="Ticket name">
                                </div>                                   
                            </div>
                            <div class="form-group">
                                <div class="row row-small">
                                    <div class="col-lg-6 col-md-4">
                                        <label>Phone number:<span>*</span></label>
                                        <input type="text" class="form-control validate[required,custom[usPhoneNumberII]]" id="PhoneNumber" placeholder="( ) - ">                                     
                                    </div>                                    
                                </div>                                    
                            </div>      
                            <div class="form-group">
                                <div class="row row-small">
                                    <!--- <div class="col-lg-3 col-md-1"> --->
                                    <div class="col-lg-6 col-md-4">
                                        <label>Status:<span>*</span></label>
                                        <select class="form-control cardholder validate[required]" id="TicketStatus" name="TicketStatus">
                                            <option value="#_TICKET_OPEN#">#_TICKET_OPEN_TEXT#</option>
                                            <option value="#_TICKET_INPROGRESS#">#_TICKET_INPROGRESS_TEXT#</option>                                   
                                            <option value="#_TICKET_ONHOLD#">#_TICKET_ONHOLD_TEXT#</option>                                   
                                            <option value="#_TICKET_CLOSED#">#_TICKET_CLOSED_TEXT#</option>                                  
                                        </select>
                                    </div>                                    
                                </div>                                     
                            </div>          
                            <div class="form-group">
                                <div class="row row-small">
                                    <div class="col-lg-6 col-md-3">
                                        <label>Type:</label>
                                        <select name="ticket-typeId" id="ticket-typeId" class="form-control"></select>           <input type="hidden" name="selected-ticket-typeId" id="selected-ticket-typeId" value=""/>   
                                        <!--- <label>Type:</label>
                                        <div class="ticket-type-zone"></div>     --->                                  
                                    </div>   
                                    <div class="col-lg-3 col-md-1">
                                        <label>&nbsp;</label>
                                        <div>
                                            <button type="button" class="btn btn-block green-gd pop-add-ticket-type">Add New Type</button>
                                        </div>                                        
                                    </div>   
                                                                
                                </div>                                        
                            </div>   
                            <div class="form-group">
                                <div class="row row-small">
                                    <div class="col-lg-3 col-md-1">
                                        <label>Priority:<span>*</span></label>
                                        <select class="form-control cardholder validate[required]" id="TicketPriority" name="TicketPriority">
                                            <option value="#_TICKET_MAJOR#">#_TICKET_MAJOR_TEXT#</option>
                                            <option value="#_TICKET_MINOR#">#_TICKET_MINOR_TEXT#</option>                                   
                                        </select>
                                    </div>                                    
                                </div>                                       
                            </div> 
                            <div class="form-group">
                                <div class="row row-small">
                                    <div class="col-lg-3 col-md-1">
                                        <label>ETA:<span>*</span></label>
                                        <input type="number" class="form-control validate[required]" id="TicketETA" name="TicketETA" placeholder="ETA" min="0.25" step="0.25">
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <div class="create-ticket-line-height-label">Hours</div>
                                        </div>
                                    </div>
                                </div>                                
                            </div>  
                            <div class="form-group">
                                <label>Description:</label>
                                <textarea class="form-control" id="TicketDescription" name="TicketDescription" data-prompt-position="topLeft:150" rows="4" placeholder="Description"></textarea>                                
                            </div>                           
                        </div>
                    </cfoutput>                
                </div>
            </div>            
            <div class="modal-footer uk-text-right buyaddon-footer-modal">                                
                <button type="button" class="btn btnCancel green-cancel" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btnCreateNewTicket green-gd">Save</button>
            </div>
            </form>
            
        </div>  
    </div>  
</div>

<!--- modal view add new type --->
<div class="modal fade" id="new-ticket-type" tabindex="-1" role="dialog" aria-hidden="true">
    <form name="form-edit-ticket-type" class="col-sm-12" id="form-edit-ticket-type">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <b><h4 class="modal-title" id="modal-tit">
                        Add New Type
                    </h4></b>                               
                </div>
                <div class="modal-body">                   
                   <div class="modal-body-inner">                  
                        <div class="form-group">
                           <input type="text" class="form-control validate[required, custom[noHTML], maxSize[50]]" id="ticket-type-name" name="ticket-type-name" placeholder="Type" data-prompt-position="topLeft:10" autocomplete="off">
                        </div>                      
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn green-cancel" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn green-gd"  id="btnUpdateticket-type">Save</button>
                </div>
            </div>
        </div>
    </form>
 </div>

<!-- Modal view ticket details-->
<div class="modal fade" id="ViewTroubleTicketModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:auto; max-width:1000px;">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><strong>TROUBLE TICKET DETAILS</strong></h4>
                <br/>
                
                    <div class="modal-body-inner row">
                        <form name="edit-trouble-ticket" class="col-sm-12" autocomplete="off" id="edit-trouble-ticket-form">

                            <cfoutput>
                                <div class="row">
                                    <div class="col-lg-5 col-sm-5 col-md-4 col-xs-5">
                                        <div class="form-group">
                                            <label for="EditTicketNo" id="edit-ticket-no-label">Ticket No.</label>
                                            <div>
                                                <label id="EditTicketNo" name="EditTicketNo"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">
                                        <div class="form-group">
                                            <label for="EditTicketCreatedDate" id="edit-ticket-created-date-label">Created Date</label>
                                            <div>
                                                <label id="EditTicketCreatedDate" class="create-ticket-line-height-label" name="EditTicketCreatedDate"></label>
                                            </div>
                                        </div>
                                        <!--- <div class="form-group">                                        
                                            <label for="status">Status<span>*</span></label>
                                            <select class="form-control cardholder validate[required]" id="EditTicketStatus" name="EditTicketStatus">
                                                <option value="#_TICKET_OPEN#">#_TICKET_OPEN_TEXT#</option>
                                                <option value="#_TICKET_INPROGRESS#">#_TICKET_INPROGRESS_TEXT#</option>                                   
                                                <option value="#_TICKET_ONHOLD#">#_TICKET_ONHOLD_TEXT#</option>                                   
                                                <option value="#_TICKET_CLOSED#">#_TICKET_CLOSED_TEXT#</option>                                  
                                            </select>
                                        </div> --->
                                    </div>
                                    <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3">
                                        <div class="form-group">
                                            <label for="EditTicketUpdateDate" id="edit-ticket-updated-date-label">Updated Date</label>
                                            <div>
                                                <label id="EditTicketUpdateDate" class="create-ticket-line-height-label" name="EditTicketUpdateDate"></label>
                                            </div>
                                        </div>
                                       <!---  <div class="form-group">
                                            <label for="EditTicketCreatedDate" id="edit-ticket-created-date-label">Created Date</label>
                                            <div>
                                                <label id="EditTicketCreatedDate" class="create-ticket-line-height-label" name="EditTicketCreatedDate"></label>
                                            </div>
                                        </div> --->
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="col-lg-5 col-sm-5 col-md-5 col-xs-5">
                                        <div class="form-group">
                                            <label for="EditTicketName" id="edit-ticket-name-label">Name<span>*</span></label>
                                            <div>
                                                <input type="text" class="form-control validate[required] maxSize[50] custom[noHTML]" id="EditTicketName" name="EditTicketName" placeholder="Ticket name">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">
                                        <div class="form-group">                                        
                                            <label for="status">Status<span>*</span></label>
                                            <select class="form-control cardholder validate[required]" id="EditTicketStatus" name="EditTicketStatus">
                                                <option value="#_TICKET_OPEN#">#_TICKET_OPEN_TEXT#</option>
                                                <option value="#_TICKET_INPROGRESS#">#_TICKET_INPROGRESS_TEXT#</option>                                   
                                                <option value="#_TICKET_ONHOLD#">#_TICKET_ONHOLD_TEXT#</option>                                   
                                                <option value="#_TICKET_CLOSED#">#_TICKET_CLOSED_TEXT#</option>                                  
                                            </select>
                                        </div>
                                       <!---  <div class="form-group">                                        
                                            <label for="EditPhoneNumber">Phone number<span>*</span></label>
                                            <input type="text" class="form-control validate[required,custom[usPhoneNumberII]]" id="EditPhoneNumber" placeholder="( ) - ">  
                                        </div> --->
                                    </div>
                                    <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3">
                                        <div class="form-group">
                                            <label for="ShowListWatchers">&nbsp;</label>
                                            <div>
                                                <span class="create-ticket-line-height-label show-list-watchers" id="ShowListWatchers">List watchers</span>                                                
                                            </div>                                        
                                        </div>
                                        <!--- <div class="form-group">
                                            <label for="EditTicketUpdateDate" id="edit-ticket-updated-date-label">Updated Date</label>
                                            <div>
                                                <label id="EditTicketUpdateDate" class="create-ticket-line-height-label" name="EditTicketUpdateDate"></label>
                                            </div>
                                        </div> --->
                                    </div>
                                </div>   
                                <div class="row">
                                    <div class="col-lg-5 col-sm-5 col-md-5 col-xs-5">
                                         <div class="form-group">                                        
                                            <label for="EditPhoneNumber">Phone number<span>*</span></label>
                                            <input type="text" class="form-control validate[required,custom[usPhoneNumberII]]" id="EditPhoneNumber" placeholder="( ) - ">  
                                        </div>
                                        <!--- <div class="form-group">
                                            <label for="EditTicketETA" id="edit-ticket-eta-label">Type</label>
                                            <div class="edit-ticket-type-zone"></div>                                                                                       
                                        </div> --->
                                    </div>
                                    <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">
                                        <div class="form-group">
                                              <label>Type:</label>
                                                <select name="edit-ticket-typeId" id="edit-ticket-typeId" class="form-control"></select>           
                                                <input type="hidden" name="selected-edit-ticket-typeId" id="selected-edit-ticket-typeId" value=""/>
                                            <!--- <label for="EditTicketETA" id="edit-ticket-eta-label">Type</label>
                                            <div class="edit-ticket-type-zone"></div>   --->                                                                                     
                                        </div>
                                        <!--- <div class="form-group">  
                                            <label for="EditTicketPriority">Priority<span>*</span></label>
                                            <select class="form-control cardholder validate[required]" id="EditTicketPriority" name="EditTicketPriority">
                                                <option value="#_TICKET_MAJOR#">#_TICKET_MAJOR_TEXT#</option>
                                                <option value="#_TICKET_MINOR#">#_TICKET_MINOR_TEXT#</option>                                   
                                            </select>
                                        </div> --->
                                    </div>
                                    <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3">
                                        <!--- <div class="form-group">
                                            <label for="ShowListWatchers">&nbsp;</label>
                                            <div>
                                                <span class="create-ticket-line-height-label show-list-watchers" id="ShowListWatchers">List watchers</span>                                                
                                            </div>                                        
                                        </div> --->
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="col-lg-5 col-sm-5 col-md-5 col-xs-5">
                                        <div class="form-group">  
                                            <label for="EditTicketPriority">Priority<span>*</span></label>
                                            <select class="form-control cardholder validate[required]" id="EditTicketPriority" name="EditTicketPriority">
                                                <option value="#_TICKET_MAJOR#">#_TICKET_MAJOR_TEXT#</option>
                                                <option value="#_TICKET_MINOR#">#_TICKET_MINOR_TEXT#</option>                                   
                                            </select>
                                        </div>
                                        <!--- <div class="form-group">                                            
                                            <div class="row row-small">
                                                <div class="col-lg-5 col-md-3">
                                                    <label for="EditTicketETA" id="edit-ticket-eta-label">ETA<span>*</span></label>
                                                    <div>
                                                        <input type="number" class="form-control validate[required]" id="EditTicketETA" name="EditTicketETA" placeholder="ETA" min="0.25" step="0.25">
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-6">
                                                    <div class="form-group">
                                                        <label>&nbsp;</label>
                                                        <div class="create-ticket-line-height-label">Hours</div>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div> --->
                                    </div>
                                    <div class="col-lg-7 col-sm-7 col-md-7 col-xs-7">
                                        <div class="form-group">                                            
                                            <div class="row row-small">
                                                <div class="col-lg-3 col-md-1">
                                                    <label for="EditTicketETA" id="edit-ticket-eta-label">ETA<span>*</span></label>
                                                    <div>
                                                        <input type="number" class="form-control validate[required]" id="EditTicketETA" name="EditTicketETA" placeholder="ETA" min="0.25" step="0.25">
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-6">
                                                    <div class="form-group">
                                                        <label>&nbsp;</label>
                                                        <div class="create-ticket-line-height-label">Hours</div>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                        <!--- <div class="form-group">                                        
                                            <label for="EditTicketDescription">Description</label>
                                             <textarea class="form-control" id="EditTicketDescription" name="EditTicketDescription" data-prompt-position="topLeft:150" rows="4" placeholder="Description"></textarea>   
                                        </div> --->
                                    </div>                                
                                </div> 
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="form-group">                                        
                                            <label for="EditTicketDescription">Description</label>
                                             <textarea class="form-control" id="EditTicketDescription" name="EditTicketDescription" data-prompt-position="topLeft:150" rows="4" placeholder="Description"></textarea>   
                                        </div>                                            
                                    </div>
                                </div>
                            </cfoutput>  
                            <br>
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tabcomment" id="tabComment">Comment</a></li>
                                <li><a data-toggle="tab" href="#tabworklog" id="tabWorkLog">Work log</a></li>
                                <li><a data-toggle="tab" href="#tabhistory" id="tabHistory">History</a></li>                                
                              </ul>

                              <div class="tab-content">
                                <div id="tabcomment" class="tab-pane fade in active">
                                    <cfinclude  template="include/inc_ticket_detail_tab_comments.cfm">                                
                                </div>
                                <div id="tabworklog" class="tab-pane fade">
                                    <cfinclude  template="include/inc_ticket_detail_tab_worklogs.cfm">                              
                                </div>
                                <div id="tabhistory" class="tab-pane fade">
                                  <cfinclude  template="include/inc_ticket_detail_tab_history.cfm">                              
                                </div>                                
                              </div>

                            <div class="modal-footer uk-text-right buyaddon-footer-modal">                                
                                <button type="button" class="btn btnCancel green-cancel" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btnEditTroubleTicket green-gd">Save</button>
                            </div>                            
                        </form>
                    </div>                
            </div>
        </div>
    </div>
</div>
<!--- edit work log--->
 <div class="modal fade" id="edit-work-log" tabindex="-1" role="dialog" aria-hidden="true">
	<form name="form-edit-work-log" class="col-sm-12" id="form-edit-work-log">
	    <div class="modal-dialog">
	        <div class="modal-content">
	        	<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<b><h4 class="modal-title" id="modal-tit">
						ADD WORK LOG
					</h4></b>			
					<input type="hidden" name="workLogId" id="workLogId" value="0" autocomplete="off"/>		
				</div>
	            <div class="modal-body">	               
	               <div class="modal-body-inner">                                                             
                        <div class="form-group">
                            <label>Start date time</label>     
                            <div class="input-group datetimepicker date " id="datetimepicker">
                                <input type='text' id="date-start" class="form-control validate[required]" value="" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                                                
                         
                        <div class="form-group">
                            <label>Time spend (hours)</label>     
                            <input class="form-control validate[required]" type="number" name="totalTime" id="totalTime" value="" min="0.25" step="0.25" >
                        </div>
	             	</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn green-cancel" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn green-gd"  id="btnUpdateWorkLog">Save</button>
				</div>
			</div>
		</div>
	</form>
 </div>
<!-- Modal Trouble ticket notify -->
<div class="modal fade" id="TroubleTicketNotifyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:auto; max-width:900px;">
        <div class="modal-content">
            <div class="modal-body">
            	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title"><strong>NOTIFY TROUBLE TICKET</strong></h4>
	            <div class="modal-body-inner row">
		            <form name="notify-trouble-ticket" class="col-sm-12" autocomplete="off" id="notify-trouble-ticket-form">
		            	<input type="hidden" name="" id="notify-ticket-id" value="">
		            	<div class="row">

		            		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
				            	<div class="form-group">
									<label for="notify-method">How do you want to send notify message?</label>
									<select  id="notify-method" class="form-control">
										<option value="0">SMS</option>
										<option value="1">Email</option>
									</select>
								</div>
							</div>

		            		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
		            			<div class="form-group">
								  	<label for="notify-phone" id="notify-phone-label">Phone</label>
								  	<input type="text" class="form-control validate[required,custom[usPhoneNumberII]]" id="notify-phone" name="notify-phone">
								</div>
		            		</div>

		            		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-6" style="display: none;">
		            			<div class="form-group">
									<label for="notify-email-address">Email</label>
									<input type="text" class="form-control validate[custom[email]]" id="notify-email-address" name="notify-email-address">
								</div>
		            		</div>
		            	</div>

		            	<div class="form-group">
							<label for="notifysms">Message</label>
							<label class="pull-right" id="notify-sms-mess-countdown-label"><a id="notify-sms-mess-countdown">160</a> left</label>
							<textarea class="form-control no-resize validate[maxSize[1000]]" id="notify-sms-message" name="notify-sms-message" placeholder="" rows="6" data-prompt-position="topLeft:100"></textarea>
						</div>
						<div class="form-group" style="display: none;">
							<label for="notifyemailsubject">Subject</label>
							<label class="pull-right" id="notify-email-sub-countdown-label"><a id="notify-email-sub-countdown">255</a> left</label>
							<input type="text" class="form-control validate[maxSize[255]]" id="notify-email-subject" name="notify-email-subject" placeholder="" data-prompt-position="topLeft:100"/>
						</div>
						<div class="form-group" style="display: none;">
							<label for="notifyemail">Message</label>
							<label class="pull-right" id="notify-email-mess-countdown-label"><a id="notify-email-mess-countdown">1000</a> left</label>
							<textarea class="form-control no-resize validate[maxSize[1000]]" id="notify-email-message" name="notify-email-message" placeholder="" rows="6" data-prompt-position="topLeft:100"></textarea>
						</div>
						<div class="">
							<div class="modal-footer">
							  	<button class="btn btn-back-custom" id="close-nofity-ticket-btn" data-dismiss="modal">Cancel</button>
							  	<button class="btn btn-success-custom" id="confirm-notify-ticket-btn" type="submit">Send</button>
							</div>
						</div>
		            </form>
		           
				</div>
        	</div>
        </div>
    </div>
</div>
<!--- edit comment--->
 <div class="modal fade" id="edit-comment" tabindex="-1" role="dialog" aria-hidden="true">
    <form name="form-edit-comment" class="col-sm-12" id="form-edit-comment">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <b><h4 class="modal-title" id="modal-tit">
                        EDIT COMMENT
                    </h4></b>           
                    <input type="hidden" name="commentId" id="commentId" value="0" autocomplete="off"/>     
                </div>
                <div class="modal-body">                   
                   <div class="modal-body-inner">                                                                                                             
                        <div class="form-group">
                            <label>Comment</label>     
                            <textarea class="form-control validate[required]" id="commentText" name="commentText" rows="6" placeholder="Comment"></textarea>                          
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn green-cancel" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn green-gd"  id="btnUpdateComment">Save</button>
                </div>
            </div>
        </div>
    </form>
 </div>
<cfinclude  template="include/inc_ticket_modal_watcher.cfm">                              

<cfparam name="variables._title" default="Trouble ticket - Sire">
<cfinclude template="../views/layouts/master.cfm">