<cfscript>
    CreateObject("component","public.sire.models.helpers.layout")
        .addCss("../assets/global/plugins/daterangepicker/daterangepicker.css", true)        
        .addCss("/public/sire/css/select2.min.css", true)
        .addCss("/public/sire/css/select2-bootstrap.min.css", true)
        .addJs("../assets/global/plugins/daterangepicker/moment.min.js", true)
        .addJs("../assets/global/plugins/daterangepicker/daterangepicker.js", true)
        .addJs('/session/sire/assets/global/scripts/filter-bar-generator.js', true)        
        
        .addJs("/public/sire/js/select2.min.js")
        
        .addJs("../assets/pages/scripts/user-chat-tools.js");
</cfscript>


<cfoutput>


<div class="portlet light bordered">
    <div class="portlet-body form">
        <div class="row">
            <div class="col-sm-3 col-md-4">
                <h2 class="page-title">Chat Campaigns</h2>
            </div>
            <div class="col-sm-9 col-md-8">
                <div id="box-filter" class="form-gd">

                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-xs-12">
                <div class="re-table table-data hidden">
                    <div class="table-responsive">
                        <table id="chat-campaign" class="table table-striped table-responsive">
            
                        </table>
                    </div>
                </div>
                <div class="nodata-div hidden">
                    <p class="portlet-subheading">
                        There is no chat with customer campaign created, please create at least one.
                        You have not setup your chat campaign, to start chatting with your customers, please
                        &nbsp;
                        <a href="/session/sire/pages/campaign-template?templateId=9&selectedTemplateType=0" target="_blank">click here</a>
                        &nbsp;
                        to setup
                        
                    </p>
                </div>
            </div>
        </div>
            
    </div>
</div>       


</cfoutput>



<cfparam name="variables._title" default="let's Chat">
<cfinclude template="../views/layouts/master.cfm">

