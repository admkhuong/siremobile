<cfinvoke component="session.sire.models.cfc.user-import-contact" method="GetUserMaxImportNumber" returnvariable="ReturnMaxImport"></cfinvoke>
<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="CheckHelpStopSetup" returnvariable="CheckUserSetupHelpStop"></cfinvoke>
<cfif ReturnMaxImport.UPLOADPERMISSION EQ 1>
    <div class="portlet light bordered">
        <div class="portlet-body">
            <p class="portlet-heading2 alert alert-warning">
                 <span class="glyphicon glyphicon-warning-sign"></span>
                &nbsp;
                The use of this feature requires additional permissions. In order to be granted authorization, please contact <a data-toggle="modal" data-target="#md-contact-support">customer support</a> and request access to this feature.
            </p>
        </div>
    </div>
<cfelseif (CheckUserSetupHelpStop.RXRESULTCODE EQ 1 AND CheckUserSetupHelpStop.NOTSETUP EQ 1)>
    <div class="portlet light bordered">
        <div class="portlet-body">
            <p class="portlet-heading2 alert alert-warning">
                 <span class="glyphicon glyphicon-warning-sign"></span>
                &nbsp;
                A completed 'My Account' <a href="/session/sire/pages/profile" target="_blank">profile</a> is a prerequisite to use the Import tool. Please fill out your <a href="/session/sire/pages/profile" target="_blank">profile</a>, including your Company Name, Help and Stop message in order to continue.
            </p>
        </div>
    </div>

<cfelse>
    <cfscript>
        CreateObject("component","public.sire.models.helpers.layout")
            .addCss("../assets/global/plugins/daterangepicker/daterangepicker.css", true)
            .addCss("/public/sire/css/select2.min.css", true)
            .addCss("/public/sire/css/select2-bootstrap.min.css", true)
            .addJs("../assets/global/plugins/daterangepicker/moment.min.js", true)
            .addJs("../assets/global/plugins/daterangepicker/daterangepicker.js", true)
            .addJs("../assets/global/plugins/drag-upload/dropzone.js", true)
            .addCss("../assets/global/plugins/uikit/css/uikit3.css", true)
        .addJs("../assets/global/plugins/uikit/js/uikit.min.js", true)

            .addJs("/public/sire/js/select2.min.js")
            .addJs("../assets/pages/scripts/user-import-contact-map.js")
            .addJs("../assets/pages/scripts/user-import-contact.js");
    </cfscript>
    <cfinclude template="/public/sire/configs/bulk_insert_constants.cfm"/>
    <script type="text/javascript">
        var uploadStatus = [];
        <cfoutput>
            <cfloop collection="#UPLOADS_STATUS_LANG#" item="key">
                uploadStatus[#key#] = "#UPLOADS_STATUS_LANG[key]#"
            </cfloop>
        </cfoutput>
    </script>

    <cfoutput>
    <cfparam name="ActionStep" default="upload">
    <cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>
    <cfinvoke component="session.sire.models.cfc.import-contact" method="getTZList" returnvariable="tzList"></cfinvoke>


    <!--- GET SUBCRIBER LIST --->
    <cfinvoke component="session.sire.models.cfc.subscribers" method="GetSubcriberList" returnvariable="groupList">
        <cfinvokeargument name="inpShortCodeId" value="#shortCode.SHORTCODEID#">
    </cfinvoke>
    <cfif groupList.RXRESULTCODE EQ 1 AND groupList.iTotalRecords GT 0>
        <cfset subcriberList = groupList.listData>
    <cfelse>
        <cfset subcriberList = []/>
    </cfif>
    <cfinvoke component="session.sire.models.cfc.user-import-contact" method="GetUserCDF" returnvariable="ListCDF"></cfinvoke>
    <cfif ListCDF.RXRESULTCODE EQ 1 >
        <cfset CDFList = ListCDF.listData>
    <cfelse>
        <cfset CDFList = []/>
    </cfif>
    <cfset CDFType= ArrayNew(1)>

    <cfset arrayappend(CDFType,['0','Text Field'])>
    <cfset arrayappend(CDFType,['10','Date Field'])>
    <cfset arrayappend(CDFType,['20','Number Field'])>
    <!--- hidden filed section--->
    <input type="hidden" name="filenameupload_org" id="file-name-upload-org"/>
    <input type="hidden" name="filenameupload" id="file-name-upload"/>


    <div class="portlet light bordered">
        <div class="portlet-body form">

            <ul class="uk-tab session-tab" uk-tab>
                <li><a href="##">Import Contacts</a></li>
                <li><a href="##">Individual Import</a></li>
            </ul>

            <ul class="uk-switcher uk-margin content-tab-pricing">
                <li>
                    <!--- upload step--->
                    <cfif ActionStep is "upload">
                        <div class="row">
                            <div class="form-group clearfix">
                                <div class="col-lg-12 col-xs-12">
                                    <button class="btn green-cancel btn-advance" id="btn-advance">Advanced Settings</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div id="section-advance" class="col-md-3" style="display: block">
                                <!---
                                <div class="form-group hidden">
                                    <label>Total number of columns used in your file</label>
                                    <input class="form-control" type="text" name="inpColumnNumber" id="inpColumnNumber" value="1">
                                </div>
                                <div class="form-group hidden">
                                    <label>Column number for phone on your list</label>
                                    <select class="form-control" name="inpContactStringCol" id="inpContactStringCol">
                                        <option>1</option>
                                    </select>
                                </div>
                                <div class="form-group hidden">
                                    <label>Column Names (separate by enter)</label>
                                    <textarea style="resize: vertical;" class="form-control" name="inpColumnNames" id="inpColumnNames">ContactString_vch</textarea>
                                </div>
                                <div class="form-group hidden">
                                    <label>Field delimiters</label>
                                    <input class="form-control" type="text" name="inpSeparator" id="inpSeparator" value="," maxlength="1" size="1">
                                </div>
                                --->
                                <div class="form-group">
                                    <label>Total lines to ignore when importing <br> (e.g. headers)</label>
                                    <input class="form-control" type="number" name="inpSkipLine" id="inpSkipLine" value="1">
                                </div>
                                <div class="form-group">
                                    <label>Number of errors allowed</label>
                                    <input class="form-control" type="number" name="inpErrorsIgnore" id="inpErrorsIgnore" value="0">
                                </div>
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>

                        <div class="row ">
                            <div class="form-group">
                                <div class="col-md-6 col-xs-12">
                                    <h4 class="portlet-heading2">Uploading Instructions</h4>
                                    <p class="portlet-subheading">
                                        You can Import up to #ReturnMaxImport.MAXLIMIT# contacts using a .csv file.
                                    </p>
                                    <!---
                                    <form action="/session/sire/models/cfc/user-import-contact.cfc?method=UploadCSVContact" method="post" enctype="multipart/form-data" class="dropzone" id="UploadFile">
                                        <div class="fallback">
                                            <input name="file" type="file" id="fileImport"/>
                                        </div>
                                    </form>
                                    --->
                                    <div id="dropzone2" class="dz-default">
                                        <i class="fa fa-cloud-upload  fa-2x" aria-hidden="true"></i><br/>
                                        Drag and drop a CSV file here to upload<br/>
                                        or<br/>
                                        <a href="javascript:;" class="open-upload">Select a file</a>
                                    </div>
                                </div>

                                <div class="col-md-6 col-xs-12">

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 col-xs-12">

                                <h4 class="portlet-heading2">Add to Existing list</h4>
                                <div class="form-gd form-lb-large">
                                    <div class="form-group">
                                        <select class="form-control validate[required] Select2 SubscriberList" id="SubscriberList">
                                            <cfif arrayLen(subcriberList) GT 0>
                                                <cfloop array="#subcriberList#" index="array_index">
                                                    <option value="#array_index[1]#">#array_index[2]#</option>
                                                </cfloop>
                                            </cfif>
                                        </select>
                                        <a href="##" class="" data-toggle="modal" data-target="##AddNewSubscriberList"> Create new subscriber list</a>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-2 col-xs-12">

                            </div>
                        </div>


                        <div class="row">
                            <div class="form-group clearfix">
                                <div class="col-lg-12 col-xs-12">
                                    <a href="javascript:;" class="btn green-gd mr-15 file-import" id="file-import">Import</a>
                                </div>
                            </div>
                        </div>
                    </cfif>
                    <!--- map step--->
                    <div id="mapFieldContent">
                    </div>

                    <!--- import summary--->
                    <div class="portlet light bordered">
                        <div class="portlet-body form">
                            <h4 class="portlet-heading2">Import Summary</h4>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="re-table">
                                        <div class="table-responsive">
                                            <table id="import-summary" class="table table-striped table-responsive">

                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="row ">
                        <div class="form-group">
                            <div class="col-md-6 col-xs-12">
                                <h4 class="portlet-heading2">Contacts</h4>
                                <p class="portlet-subheading">
                                    Manually add phone numbers to your subscriber lists by entering one phone number per line.
                                </p>
                                <div class="form-group">
                                    <textarea maxlength="1500" name="ListTextImport" id="ListTextImport" cols="10" rows="10" class="text form-control validate[required]"></textarea>
                                </div>
                            </div>

                            <div class="col-md-6 col-xs-12">

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 col-xs-12">

                            <h4 class="portlet-heading2">Add to Existing list</h4>
                            <div class="form-gd form-lb-large">
                                <div class="form-group">
                                    <select class="form-control validate[required] Select2 SubscriberList" id="SubscriberList">
                                        <cfif arrayLen(subcriberList) GT 0>
                                            <cfloop array="#subcriberList#" index="array_index">
                                                <option value="#array_index[1]#">#array_index[2]#</option>
                                            </cfloop>
                                        </cfif>
                                    </select>

                                </div>
                                <div class="form-group">
                                    <a href="##" class="" data-toggle="modal" data-target="##AddNewSubscriberList">Create new subscriber list</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-xs-12">

                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group clearfix">
                            <div class="col-lg-12 col-xs-12">
                                <a href="javascript:;" class="btn green-gd individual-import" id="individual-import">Import</a>
                            </div>
                        </div>
                    </div>
                    <!--- import summary--->
                    <div class="portlet light bordered">
                        <div class="portlet-body form">
                            <h4 class="portlet-heading2">Import Summary</h4>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="re-table">
                                        <div class="table-responsive">
                                            <table id="individual-summary" class="table table-striped table-responsive">

                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

            </ul>

            <!--- <h2 class="page-title">Import Contacts</h2>     --->
            <!---  <ul class="admin-tool-head nav">
                <li class="active"><a href="/session/sire/pages/user-import-contact-file">File Import</a></li>
                <li><a href="/session/sire/pages/user-import-contact-file">Individual Import</a></li>
            </ul> --->

        </div>
    </div>

    <!--- start modal--->
    <div class="modal fade" id="confirm-import" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <div class="modal-body-inner">

                        <p class="">
                            <b class="portlet-subheading">By clicking ‘Upload’, I am confirming the following:</b>
                            <div class="padding-terms-set portlet-subheading">
                                <span class="">
                                    a.	These contacts have opted-in to this subscriber list.<br/>
                                </span>
                                <span class="">
                                    b.	These contacts are expecting to receive messages from me.<br/>
                                </span>
                                <span class="">
                                    c.	This list was not purchased, rented, appended or provided by a third party.<br/>
                                </span>
                            </div>
                            <div class="portlet-subheading">
                                Sending unsolicited marketing text messages is a violation of Sire policy and Federal law. You will be held solely responsible for any violation as outlined in our <a href="/term-of-use" target="_blank"> Terms of Service.</a><br/>
                            </div>

                        </p>
                        <p class="text-right">
                            <a href="javascript:;" id="chkAgreeTerms" class="btn green-gd">Upload</a>&nbsp;&nbsp;
                            <a href="javascript:;" data-dismiss="modal" class="btn green-cancel">Cancel</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="edit-cdf" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
        <form name="fr-edit-cdf" id="fr-edit-cdf">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <b><h4 class="modal-title" id="modal-tit">
                    Create a new Custom Field
                    </h4></b>
                </div>
                <div class="modal-body">

                    <div class="modal-body-inner">
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label for="">Field Type:</label>
                                    <select class="form-control validate[required] Select2" id="cdf-field-type">
                                        <cfif arrayLen(CDFType) GT 0>
                                            <cfloop array="#CDFType#" index="array_index">
                                                <option value="#array_index[1]#">#array_index[2]#</option>
                                            </cfloop>
                                        </cfif>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label for="">Field Name:</label>
                                    <input type="text" class="form-control validate[required,custom[onlyLetterSp]]" id="cdf-field-name" max-leng="255">
                                    <input type="hidden" id="cdf-field-source"/>
                                </div>
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn green-gd">Create</button>
                            <button type="button" class="btn green-cancel" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        </div>
    </div>
    <div class="modal fade" id="AddNewSubscriberList" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="frm_subscriber_list">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <b><h4 >Add new subcriber list</h4></b>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Subscriber List Name: </label>
                            <input id="subscriber_list_name" class="form-control validate[required, custom[onlyLetterNumberSp]]" maxlength="255">
                            <span class="help-block">ex: Happy Hour Promo List</span>
                        </div>

                        <div class="text-right">
                            <button type="button" id="btn-save-group-sub" class="btn green-gd">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="bootbox modal fade" id="show-contact" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog set-width-350">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title CapitalFirstChar">Contacts</h4>
                </div>
                <div class="modal-body">
                    <div class="re-table">
                        <table id="show-contact-ds" class="table table-responsive table-bordered table-striped">

                        </table>
                    </div>
                </div>
                <div class="modal-footer hidden">
                    <input type="hidden" value="" name="id">
                    <input type="hidden" value="" name="type">
                    <input type="hidden" value='<cfoutput>#SerializeJSON(tzList)#</cfoutput>' name="tzlist">
                    <button type="button" class="btn btn-primary btn-success-custom" name="deleteSelectedItem" data-delete-all="1">Delete all</button>
                    <button type="button" class="btn btn-primary btn-success-custom" name="deleteSelectedItem" data-delete-all="0">Delete selected</button>
                    <button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn-success-custom" data-dismiss="modal">Reload</button>
                    <button type="button" class="btn btn-primary btn-success-custom visible btn-print">Print</button>
                </div>
            </div>
        <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="bootbox modal fade" id="update-contact" style="z-index: 10000" tabindex="-2" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Update contact</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger hidden"></div>
                    <div class="form-group">
                        <label>Contact</label>
                        <input type="text" class="form-control" name="contact">
                    </div>
                    <div class="form-group">
                        <label>Timezone</label>
                        <select class="form-control" name="contact-tz">
                            <cfloop array="#tzList#" index="item">
                                <option value="<cfoutput>#item.VALUE#</cfoutput>"><cfoutput>#item.NAME#</cfoutput></option>
                            </cfloop>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" value="" name="id">
                    <input type="hidden" value="" name="fileID">
                    <input type="hidden" value="" name="type">
                    <button type="button" name="save" class="btn btn-primary btn-success-custom">Save</button>
                    <button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal">Close</button>
                </div>
            </div>
        <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="bootbox modal fade" id="show-file-history" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">File Update history</h4>
                </div>
                <div class="modal-body">
                    <form name="file-update-history">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <label>File Date</label>
                                    <input type="text" value="" data-type="date-single" data-control="start-date" name="file-date" class="form-control">
                                    <cfoutput>
                                        <input type="hidden" name="file-date-start" value="#DateFormat(NOW().add("d", -30), "yyyy-mm-dd")#" class="form-control" data-value="#DateFormat(NOW().add("d", -30), "mm/dd/yyyy")#">
                                        <input type="hidden" name="file-date-end" value="#DateFormat(NOW(), "yyyy-mm-dd")#" class="form-control" data-value="#DateFormat(NOW(), "mm/dd/yyyy")#">
                                    </cfoutput>
                                </div>
                            </div>
                            <!--- <div class="col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <label>File Name</label>
                                    <select name="file-name" class="form-control">
                                    </select>
                                </div>
                            </div> --->
                            <div class="col-sm-2 col-xs-12">
                                <div class="form-group">
                                    <label class="hidden-xs">&nbsp;</label>
                                    <button type="button" name="view" class="btn btn-success form-control">View</button>
                                </div>
                            </div>
                            <div class="col-sm-2 col-xs-12">
                                <div class="form-group">
                                    <label class="hidden-xs">&nbsp;</label>
                                    <button type="button" name="clear" class="btn btn-primary form-control">Clear</button>
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-12"></div>
                        </div>
                    </form>
                    <div class="re-table">
                        <table id="show-file-history-ds" class="table table-responsive table-bordered table-striped">

                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" value="" name="id">
                    <button type="button" class="btn btn-primary btn-back-custom" data-dismiss="modal">Close</button>
                </div>
            </div>
        <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="ModalImportSucess" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <b><h4 >Import Result</h4></b>
                    </div>
                    <div class="modal-body">
                        <div class="form-group clearfix">
                            <div class="col-xs-9 col-sm-6">Number of Contact Import success:</div>
                            <div class="col-xs-3 col-sm-6" id="ModalImportSucess_Success"></div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="col-xs-9 col-sm-6">Number of Contact Import Fail:</div>
                            <div class="col-xs-3 col-sm-6" id="ModalImportSucess_Fail"></div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="col-xs-12 col-sm-12">
                            <div class="re-table">
                                <div class="table-responsive">
                                    <table id="import-indi-result">

                                    </table>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>
    <div class="modal fade" id="md-indi-view-data" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog set-width-350">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <b><h4>Data Preview</h4></b>
                </div>
                <div class="modal-body">
                    <div class="row ">
                        <div class="col-xs-12">
                            <div class="re-table table-data">
                                <div class="table-responsive">
                                    <table id="tbl-indi-view-data" class="table table-striped table-responsive">

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </cfoutput>
</cfif>
<div class="modal fade" id="md-contact-support" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog set-width-350">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <div class="form-group  ">
                    <center>
                        <h4 class="portlet-heading2">Customer Support</h4>
                        <div class="md-import-row-span"><span class="span-title">EmailL:</span> <a href="mailto:hello@siremobile.com?Subject=SupportImportTools" target="_top">hello@siremobile.com</a></div>
                        <div class="md-import-row-span"><span class="span-title">Phone:</span> (888) 747-4411</div>
                        <div class="md-import-row-span"><span class="span-title">Chat with us: </span> 9AM - 6PM Pacific </div>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>



<cfparam name="variables._title" default="Subscriber List - Import Contacts">
<cfinclude template="../views/layouts/master.cfm">
