<cfinclude template="/session/sire/configs/paths.cfm">
<cfinvoke component="session.sire.models.cfc.user-import-contact" method="GetUserCDF" returnvariable="ListCDF">    
</cfinvoke>
<cfif ListCDF.RXRESULTCODE EQ 1 >
    <cfset CDFList = ListCDF.listData>
<cfelse>
    <cfset CDFList = []/>    
</cfif>    
<cfset CDFType= ArrayNew(1)>

<cfset arrData= ArrayNew(1)>
<cfset arrCheck= ArrayNew(1)>

<cfset arrayappend(CDFType,['0','Text Field'])>
<cfset arrayappend(CDFType,['10','Date Field'])>
<cfset arrayappend(CDFType,['20','Number Field'])>

<cfoutput>
<cfparam name="filenameupload" default="">
<cfparam name="imp_skipline" default="0">
<cfparam name="imp_ErrorsIgnore" default="0">

<!--- <cfhttp name="qs_check_item" method="get" url="http://sire.lc/public/sire/upload/contactstring_csv/contact-string-476-Sep-06-2017-20-15-57.csv"/>
    <cfdump var="#qs_check_item#" abort="true"/> --->

<cfset uploadPath = ExpandPath(UPLOAD_CSV_PATH) />
<cfset filePath = expandPath(UPLOAD_CSV_PATH&'/'&'#filenameupload#') />

<!--- check if file is valid --->
<cfset dataFile = fileOpen( filePath, "read" ) />
<cfset numLine = 0/>
<cfset numskipline = 0/>
 <cfloop condition="!fileIsEOF( dataFile )">
    <cfset line = fileReadLine( dataFile ) />
    <!--- <cfset line= REReplace(line,'("[^",]+),([^"]+")'," ","ALL")> --->
    <!--- <cfdump var="#line#"/> --->
    <cfset numskipline ++/>
    <cfif numskipline GT imp_skipline>
    <!---
        
        <cfif arrayLen(arrCheck) EQ 1>
            <!--- <cfset line = "#arrCheck[1]#" & "#replace(arrCheck[2],",","","ALL")#" & "#arrCheck[3]#"> --->
            <cfset arrayAppend(arrData, listToArray(line,",",true,true))/>
           
        </cfif>
    --->
        <cfset arrCheck = listToArray(line,'"',true,true)>
        <cfif arrayLen(arrCheck) EQ 1>
            <cfset arrayAppend(arrData, listToArray(line,",",true,true))/>
        <cfelse>
            <cfset line = line & ','>
            <cfset reg = '".+?"|[^"]+?(?=,)|(\?<=,)[^"]+'/>
            <cfset arrCheck = REMatchNoCase(reg, line)>
            <cfset arrayAppend(arrData, arrCheck)/>
        </cfif>
        <cfset numLine ++/>
    </cfif>
    <Cfif numLine EQ 6>
        <cfbreak/>
    </Cfif>
</cfloop>
<cfset fileClose( dataFile ) />


<!--- Close the file stream to prevent locking. --->

<div class="portlet light bordered">
    <div class="portlet-body form">
        
        <div class="row" style="margin-top: 10px">
            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                <h4 class="portlet-heading2">Map Field Name</h4>
                <p class="help-block">Preview information</p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <!---<div class="pull-right">
                <a href="##" class="btn btn-block green-gd pop-cdf" data-id="0" data-toggle="modal" data-cdf-action="new" data-target="##edit-cdf">Add New CDF</a>
                </div>--->
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="re-table" style="margin-top: 10px">
                    <div class="table-responsive">
                        <table id="import-map-field" class="table table-striped table-responsive">
                            <thead>
                                <tr>    
                                    <cfset rID=0>    
                                    <cfloop index = "LoopCount" from = "1" to = "#arrayLen(arrData[1])#"> 
<!---                                     <cfloop list="#ArrayToList(arrData[1])#" index="col" > --->
                                    <cfset rID++>
                                        <th>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                <select class="form-control validate[required] Select2 cdf-data" id="cdf-data-#rID#">
                                                    <option value="ContactString_vch">PhoneNumber</option>
                                                    <cfif arrayLen(CDFList) GT 0>                                                                                    
                                                        <cfloop array="#CDFList#" index="array_index">
                                                            <option value="#array_index[1]#">#array_index[2]#<span class="sl2-icon-del"></option>
                                                        </cfloop>
                                                    </cfif>
                                                </select>  
                                                </div >
                                                <div class="col-xs-12 text-left">
                                                    <a class="remove-column" href="javascript:;">Remove</a>
                                                </div>                               
                                            </div>    
                                        </th>
                                    </cfloop>
                                </tr>
                            </thead>                            
                           <!--- <cfset arrayDeleteAt(arrData, 1)/> --->                           
                            <cfloop array="#arrData#" index="i">
                                <tr>
                                    <cfloop array="#i#" index="col" >
                                         <td><xmp><cfif left(col,1) EQ ",">#replace(col,",","","one")#<cfelseif right(col,1) EQ ",">#left(col,len(col)-1)#<cfelse>#replace(col,'"',"","ALL")#</cfif></xmp></td>
                                    </cfloop> 
                                </tr>
                            </cfloop>
                        </table>                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group clearfix">  
                <div class="col-lg-12 col-xs-12">                       
                    <a href="javascript:;" class="btn green-gd btn-save" id="btn-save">Save</a> 
                    <a href="javascript:;" class="btn green-gd btn-cancel" id="btn-cancel">Cancel</a> 
                </div>
            </div>
        </div>
    </div>    
</div>
</cfoutput>

