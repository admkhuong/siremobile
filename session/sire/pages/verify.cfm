<cfinclude template="/session/sire/configs/paths.cfm">

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/assets/global/scripts/filter-bar-generator.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/js/site/admin_support_verify.js">
</cfinvoke>



<div class="portlet light bordered ">
	<div class="portlet-body row">
		<div class="col-sm-12 col-md-4">
			<h2 class="page-title">Admin - Verify</h2>
		</div>
		<div class="form-group col-sm-12 col-md-4 pull-right">

			<!--- <div id="box-filter" class="box-filter">
				
			</div> --->
			
			<label><b>Campaign Id:</b></label>
			<select id="select-verify-batch" class="form-control">
				<option value="166875">166875</option>
				<option value="160745">160745</option>
				<option value="160745MFA">160745 - MFA</option>
			</select>
			
		</div>
		<div class="content-body">
			<div class="container-fluid">
				<div class="re-table">
					<div class="table-responsive">
						<table id="tblList" class="table table-striped table-bordered table-hover">
							
						</table>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>

<cfparam name="variables._title" default="Admin - Verify">
<cfinclude template="../views/layouts/master.cfm">

