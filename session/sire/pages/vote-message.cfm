<cfparam name="templateid" default="">
<cfparam name="campaignid" default="">

<cfset campaignData = {
	BatchId_bi = '',
	KeywordId_int = '',
	Keyword_vch = '',
	Desc_vch = '',
	GroupId_int = 0,
	Schedule_arr = []
}>

<cfif isNumeric(campaignid)>
	<cfquery name="getCampaignDetail" datasource="#session.DBSourceREAD#">
    	SELECT BatchId_bi, UserId_int, Desc_vch, GroupId_int, XMLControlString_vch
        FROM simpleobjects.batch
        WHERE batchid_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#campaignid#">
        	AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
        	AND Active_int > 0
        	AND EMS_Flag_int = 2
    </cfquery>
    <cfif getCampaignDetail.RecordCount LE 0>
    	<cflocation url="/session/sire/pages/campaign-manage" addtoken="false">
    </cfif>
    <cfset xmlControl = XmlParse("<XMLControlStringDoc>" & #getCampaignDetail.XMLControlString_vch# & "</XMLControlStringDoc>") />

	<!--- --->
	<cfinvoke method="GetSchedule" component="schedule" returnvariable="ScheduleByBatchId">
		<cfinvokeargument name="INPBATCHID" value="#campaignid#">
	</cfinvoke>
	<cfif ScheduleByBatchId.RXRESULTCODE GT 0>
		<cfloop array="#ScheduleByBatchId.ROWS#" index="ROW">						
			<cfset Arrayappend(campaignData.Schedule_arr, ROW)>
		</cfloop>
	</cfif>
	<!--- --->
<cfelseif isNumeric(templateid)>
	<cfquery name="getTemplateXMLDetail" datasource="#session.DBSourceREAD#">
    	SELECT XMLControlString_vch
        FROM simpleobjects.templatesxml
        WHERE TID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#templateid#">
    </cfquery>
    <cfif getTemplateXMLDetail.RecordCount LE 0>
    	<cflocation url="/session/sire/pages/campaign-manage" addtoken="false">
    </cfif>
    <cfset xmlControl = XmlParse("<XMLControlStringDoc>" & #getTemplateXMLDetail.XMLControlString_vch# & "</XMLControlStringDoc>") />
<cfelse>
	<cflocation url="/session/sire/pages/campaign-manage" addtoken="false">
</cfif>

<cfinvoke component="session.sire.models.cfc.campaign" method="getCampaignAndTypeByXML" returnvariable="campaignControl">
	<cfinvokeargument name="xmlControl" value="#xmlControl#">
</cfinvoke>
<cfif campaignControl.type NEQ 'VOTE'>
	<cflocation url="/session/sire/pages/campaign-manage" addtoken="false">
</cfif>


<cfparam name="mode" default="create">

<cfparam name="Session.AdditionalDNC" default="">

<cfinclude template="/public/sire/configs/paths.cfm">

<cfinclude template="/public/sire/configs/userConstants.cfm">


<!--- remove old method: 
<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>
--->
<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode">
	<cfinvokeargument name="inpBatchId" value="#campaignid#">
</cfinvoke>

<cfinvoke component="session.sire.models.cfc.subscribers" method="GetSubcriberList" returnvariable="groupList"></cfinvoke>

<cfif groupList.RXRESULTCODE EQ 1 AND groupList.iTotalRecords GT 0>
	<cfset subcriberList = groupList.listData>
	<cfset totalSubcriber = subcriberList[1][3]>
	<cfset group_id = groupList.listData[1][1]>
<cfelse>
	<cfset subcriberList = []>
	<cfset totalSubcriber = 0>
	<cfset group_id = 0>
</cfif>
<cfif campaignData.GroupId_int GT 0>
	<cfset group_id = campaignData.GroupId_int>
</cfif>

<cfset disableClass="">

<cfset _numAnswer = arrayLen(campaignControl.answers)>
<cfset _maxNumAnswer = max(9, _numAnswer)>

<cfset _answerFormatList = {
	NUMERICPAR = '1) ..., 2) ..., 3) ...',
	NUMERIC = '1 for , 2 for , 3 for',
	ALPHAPAR = 'A) ..., B) ..., C) ...',
	ALPHA = 'A for , B for , C for'
}>

<cfoutput>
<main class="container-fluid page">
	<cfinclude template="../views/commons/credits_available.cfm">
	<cfdump var="#campaignid#">
	<section class="row bg-white">
		<form id="vote_message" class="">
		    <div class="content-header">
		    	<div class="row">
	                <div class="col-sm-8 content-title">
			            <p class="title">Welcome <cfoutput>#Session.FIRSTNAME#</cfoutput>!</p>
			            <p class="sub-title">Your Shortcode : #shortCode.SHORTCODE#</p>
		            </div>
		            <div class="col-sm-4 content-menu">
		            	<a class="active"><span class="icon-edit"></span><span>Create SMS</span></a>
		            	<!---<a class="active"><span class="icon-message"></span><span>Vote Message</span></a>--->
	            	</div>
		    	</div>
			</div>
	    	<hr class="hrt0">
			<div class="content-body">
				<div class="clearfix">
					<div class="col-md-7">
						<div class="row form-group">
							<div class="col-sm-4">
		            			<label for="campaign_name">Campaign Name:*</label>
	            			</div>
	            			<div class="col-sm-8">
	            				<input value="" class="form-control validate[required]" id="campaign_name" size="35" maxlength="255">
            				</div>
            			</div>
						<div class="row">
							<div class="col-md-4"><label for="txtKeyword">Keyword:</span></div>
							<div class="col-md-8">
								<div class="keyword-group">
						 		<input type="hidden" id="keywordId" name="keywordId" value="#campaignData.KeywordId_int#">
						 		<input type="text" class="form-control validate[required]" id="txtKeyword" value="#campaignData.Keyword_vch#" maxlength="160"> 
						 		<span id='KeywordAvailabilityMessage'><span id='KeywordAvailabilityText'></span></span>
						 		</div>
						 	</div>
						</div>
					 	<p class="opt-subtitle">Keyword must not contain spaces or any other special characters</p>
						<hr>
			    		<div class="row">
			    			<div class="col-sm-7 content-title">
			    				<span id="num_subscriber" class="sub-title text-green">
			    					Send to 10 subscribers
			    				</span>
			    			</div>
			    			<div id="num_char_available" class="col-sm-5 text-right">
			    				29 characters available
			    			</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-sm-12 mb15 form-inline sub-title">
								<lable for="">Question Text:*</lable>
								&nbsp; &nbsp; &nbsp; 
								<input value="#campaignControl.question#" class="form-control validate[required]" id="question_text" maxlength="1000">
							</div>
						</div>
						<div class="row form-inline">
							<div class="col-sm-6 mb15 sub-title">
								<lable for="">Answers to be selectd:</lable>
								&nbsp; &nbsp; &nbsp;
								<select class="form-control" id="num_answer">
									<cfloop from="2" to="#_maxNumAnswer#" index="i">
										<option value="#i#"<cfif i EQ _numAnswer> selected="selected"</cfif>>#i#</option>
									</cfloop>
								</select> 
							</div>
							<div class="col-sm-6 mb15 sub-title">
								<lable for="">Format answers:</lable>
								&nbsp; &nbsp; &nbsp;
								<select class="form-control" id="format_answer">
									<cfloop collection="#_answerFormatList#" index="i">
										<option value="#i#"<cfif i EQ campaignControl.answersFormat> selected="selected"</cfif>>#_answerFormatList[i]#</option>
									</cfloop>
								</select> 
							</div>
							<div id="answersList" class="col-sm-10 col-sm-offset-1 mb15">
								<cfloop from="1" to="#_maxNumAnswer#" index="i">
									<div class="col-sm-12 mb5 answerItem" style="display:none">
										<span class="answerItemShortChar">#i#.</span> &nbsp; 
										<input value="#arrayIsDefined(campaignControl.answers,i) ? campaignControl.answers[i] : ''#" class="form-control validate[required] answerItemText" maxlength="1000">
										<button type="button" class="btn btn-link answerItemDelete">
											<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
										</button>
									</div>
								</cfloop>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 form-group">
								<hr>
							    <div class="info-block">
							        <label for="disabledSelect" class="sub-title">Select the Subscriber List</label>
							        <cfif arrayLen(subcriberList) GT 0>
							            <cfloop array ="#subcriberList#" index="item">
							                <cfset checked_group=''>
							                    <cfif group_id EQ item[1]>
							                        <cfset checked_group='checked'>
							                    </cfif>
							                    <div class="radio">
							                        <label>
							                            <input type="radio" class="validate[required] <cfoutput>#disableClass#</cfoutput>" name="optionsSubscriberList" value="<cfoutput>#item[1]#</cfoutput>" data-total-subcribers="<cfoutput>#item[3]#</cfoutput>" #disableClass# #checked_group#>
							                            <cfoutput>#item[2]#</cfoutput> (<cfoutput>#item[3]#</cfoutput>)
							                        </label>
							                    </div>
							            </cfloop>
						            <cfelse>
						                <p class="help-block">You don't have any Subscriber List.</p>
							        </cfif>
							    </div>
							</div>
							<div class="col-sm-12 form-group">
								<hr class="hr10">
							    <label for="opt-out-txt">Scheduled</label>
							    <div class="warning-block">
							        <div id="warning-content"><span class="warning-text">Note:</span> Messages are scheduled between 9am - 7pm.</div>
							        <a data-toggle="modal" data-target="##ScheduleOptionsModal" href="##">
							            Click here to enable alternate delivery times.
							        </a>
							    </div>
							</div>
							<div class="col-sm-12 form-group">
								<cfif mode EQ 'edit'>
									<button type="button" class="btn btn-medium btn-success-custom btn-update-for-review" id="btn-update-for-review">Update</button>
				              		<button type="button" class="btn btn-medium btn-success-custom btn-update-lunch" >Update & Launch</button>
								<cfelse>
									<button type="button" class="btn btn-medium btn-success-custom btn-save-for-review" id="btn-save-for-review">Save & Send</button>
				              		<button type="button" class="btn btn-medium btn-success-custom btn-lunch" > Save </button>
								</cfif>
			              			<a href="/session/sire/pages" class="btn btn-medium btn-back-custom pull-right" id="btn-cancel">Cancel</a>		
			              	</div>
						</div>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-4">
						<div class="optin-demo">
						    <div id="QAScreen">
						        <img class="ScreenBackground" src="/session/sire/images/phonedemo.png">
						        <div id="PhoneScreenArea">
						            <div id="SMSToAddress"><cfoutput>#shortCode.SHORTCODE#</cfoutput></div>
						            <div id="SMSHistoryScreenArea">
						                <div class="bubble you" rel="274"></div>
						            </div>
						        </div>
						        <!--<img class="ScreenBackgroundGlare" src="/public/sire/images/iphonebaseglareonly.png">-->
						    </div>
						</div>
					</div>
				</div>
	    	</div>
		</form>
	</section>
</main>
<script TYPE="text/javascript">
	var mode = '#mode#';
	var INPBATCHID = '#campaignid#';
	var schedule_data = "";
	var SHORTCODE = "#shortCode.SHORTCODE#";
	
	var date_txt_1 = '#LSDateFormat(DateAdd("d", 30, Now()), "m-d-yyyy")#';
	var date_txt_2 = '#LSDateFormat(NOW(), "m-d-yyyy")#';

	var ScheduleTimeData;
</script>
</cfoutput>

<!-- Modal -->
<div class="bootbox modal fade" id="ScheduleOptionsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:auto; max-width:900px;">
        <div class="modal-content">
        	<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>            
                 
            </div>
        	<div class="modal-body">
        		<div class="modal-body-inner row">    
		            <div id="AdvanceScheduleContent">
		                <cfinclude template="dsp_advance_schedule.cfm">
		            </div>
		        </div>    
           	</div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<div class="bootbox modal fade" id="ConfirmLaunchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/js/jquery.tmpl.min.js">
</cfinvoke>

<!---<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/js/site/common.js">
</cfinvoke>--->

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/js/schedule.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/js/vote_message.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/css/campaign/mycampaign/schedule.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/js/jquery-ui-1.11.4.custom/jquery-ui.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/message.css">
</cfinvoke>

<cfinclude template="../views/layouts/main.cfm">