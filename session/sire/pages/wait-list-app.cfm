<cfparam name="URL.com" default="app"/>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/js/jquery.tmpl.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/js/jquery-ui-1.11.4.custom/jquery-ui.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/filter_table.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/template.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/waitlist-html.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/js/waitlist-html.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/waitlistapp.css">
</cfinvoke>

<cfinclude template="/session/sire/configs/paths.cfm" />

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.1.2/css/rowReorder.dataTables.min.css">
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="https://cdn.datatables.net/rowreorder/1.1.2/js/dataTables.rowReorder.min.js">
</cfinvoke>

<cfparam name="variables._title" default="Waitlist app - Sire">

<main class="container-fluid page my-plan-page">
    <cfinclude template="../views/commons/credits_available.cfm">

	<cfinvoke component="session.sire.models.cfc.order_plan" method="GetNextPlanUpgrade" returnvariable="nextPlanUpgrade">
		<cfinvokeargument name="inpPlanOrder" value="#RetUserPlan.PLANORDER#">
	</cfinvoke>

    <section class="row bg-white">
        <div class="content-header">
        	<div class="row">
				<div class="col-sm-4 col-lg-3 col-md-4 col-xs-12 content-title">
                    <cfinclude template="../views/commons/welcome.cfm">
                </div> 
            </div>
        </div>
        <hr class="hrt0">
		<div class="content-body">
			<div class="container-fluid">
			<!--- Start waitlist content --->
				<cfinvoke method="GetWaitLists" component="session.sire.models.cfc.waitlistmanage" returnvariable="WaitLists"></cfinvoke>

				<cfif arrayLen(WaitLists.RESULT) LT 1>
					<!--- User does not have any waitlist --->
					You don't have any waitlist. Please go to <a href="/session/sire/pages/wait-list-manage">Waitlist Manage Page</a> and create your first waitlist
				<cfelse>
					<cfif structKeyExists(url, 'listid')>
						<cfinvoke method="GetWaitListById" component="session.sire.models.cfc.waitlistmanage" returnvariable="Waitlist">
							<cfinvokeargument name="inpListId" value="#url.listid#"/>
						</cfinvoke>
						<cfset Waitlist = deserializeJSON(Waitlist)/>

						<cfif Waitlist.RXRESULTCODE GT 0>
							<cfset currentWaitListId = Waitlist.RESULT.ListId_bi />
						<cfelse>
							<cflocation url="/session/sire/pages/wait-list-manage" addtoken="false"/>
						</cfif>
					<cfelse>
						<cfinvoke method="GetDefaultList" component="session.sire.models.cfc.waitlistmanage" returnvariable="DefautlList"></cfinvoke>
						<cfset currentWaitListId = DefautlList.LISTID />
					</cfif>

					<cfoutput>
						<input type="hidden" id="current-list-id" value="#currentWaitListId#">
					</cfoutput>

					<cfinvoke component="session.sire.models.cfc.waitlistapp" method="CreateDefaultWaitlistAppSetting" returnvariable="CreateDefaultWaitlistAppSettingResult">
						<cfinvokeargument name="inpUserId" value="#Session.USERID#">
						<cfinvokeargument name="inpListId" value="#currentWaitListId#">
					</cfinvoke>

					<cfinvoke component="session.sire.models.cfc.waitlistapp" method="GetUserWaitlistAppSetting" returnvariable="userWaitlistAppSetting">
						<cfinvokeargument name="inpUserId" value="#Session.USERID#">
						<cfinvokeargument name="inpListId" value="#currentWaitListId#">
					</cfinvoke>

					<cfinvoke component="session.sire.models.cfc.waitlistapp" method="GetUserWaitlistServices" returnvariable="userWaitlistAppService">
						<cfinvokeargument name="inpUserId" value="#Session.USERID#">
						<cfinvokeargument name="inpListId" value="#currentWaitListId#">
					</cfinvoke>

					<cfinvoke component="session.sire.models.cfc.waitlistapp" method="GetTodayStatistic" returnvariable="todayStatistic">
						<cfinvokeargument name="inpListId" value="#currentWaitListId#">
					</cfinvoke>



					<cfset todayStatistic = deserializeJSON(todayStatistic) />

					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pull-right">
							<a href="/session/sire/pages/wait-list-manage" class="btn btn-success-custom pull-right">Waitlist Manage</a>
						</div>
						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 pull-left">
							<cfif URL.com NEQ 'analytics' AND  URL.com NEQ 'customers' AND URL.com NEQ 'setting' >
								<form class="form-inline">
									<div class="form-group">
										<label class="control-label" for="waitlist-select">Waitlist:</label>
										<select class="form-control" id="waitlist-select" style="width: 250px">
											<cfoutput>
												<cfloop array="#WaitLists.RESULT#" index="list">
													<option data-listid="#list.ID#" #iIf(list.ID EQ currentWaitListId, DE("selected"), DE(""))#>#list.NAME#</option>
												</cfloop>
											</cfoutput>
										</select>
									</div>
								</form>
							</cfif>
						</div>
					</div>
					<section class="row si-content si-config">
						<div class="tab-title">
							<!-- Nav tabs -->
							<ul>
								<cfif URL.com EQ 'app'>
									<li class="active" id="waiting-tab">Waiting</li>
									<li id="history-tab">History</li>
								<cfelseif URL.com EQ 'analytics'>	
									<!--- <li id="analytics-tab" class="active">Analytics</li> --->
								<cfelseif URL.com EQ 'customers'>	
									<!--- <li id="customers-tab" class="active">Customers</li> --->
								<cfelseif URL.com EQ 'setting'>
									<!--- <li id="setting-tab" class="active">Setting</li> --->
								</cfif>
							</ul>
						</div>

						<!-- Tab panes -->
						<div class="tab-content">
							<ul>
								<cfif URL.com EQ 'app'>
									<li class="active">
										<div class="row">
											<cfinclude template="../views/waitlistapp/waiting.cfm" />
										</div>
									</li>
									<li>
										<div class="row">
											<cfinclude template="../views/waitlistapp/history.cfm" />
										</div>
									</li>
								<cfelseif URL.com EQ 'analytics'>
									<li class="active">
										<cfinclude template="../views/waitlistapp/analytics.cfm" />
									</li>
								<cfelseif URL.com EQ 'customers'>
									<li class="active">
										<cfinclude template="../views/waitlistapp/customers.cfm" />
									</li>
								<cfelseif URL.com EQ 'setting'>
									<li class="active">
										<cfinclude template="../views/waitlistapp/setting.cfm" />
									</li>
								</cfif>	
							</ul>
						</div>
					</section>
				</cfif>
			<!--- End waistlist content --->
			</div>
		</div>
	</section>
</main>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/font-awesome.min.css"/>
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js"/>
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/vendors/countdown.min.js"/>
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/vendors/date.js"/>
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/site/waitlistapp/wait-list-app.js"/>
</cfinvoke>

<cfinclude template="../views/layouts/main.cfm" />

