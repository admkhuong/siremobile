<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/js/jquery.tmpl.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/js/jquery-ui-1.11.4.custom/jquery-ui.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/filter_table.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/waitlistapp.css">
</cfinvoke> 

<cfinclude template="/session/sire/configs/paths.cfm" />

<cfparam name="variables._title" default="Waitlist manage - Sire">

<main class="container-fluid page my-plan-page">
    <cfinclude template="../views/commons/credits_available.cfm">

	<cfinvoke component="session.sire.models.cfc.order_plan" method="GetNextPlanUpgrade" returnvariable="nextPlanUpgrade">
		<cfinvokeargument name="inpPlanOrder" value="#RetUserPlan.PLANORDER#">
	</cfinvoke>

    <section class="row bg-white">
        <div class="content-header">
        	<div class="row">
				<div class="col-sm-4 col-lg-3 col-md-4 col-xs-6 content-title">
                    <cfinclude template="../views/commons/welcome.cfm">
                </div> 

                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 content-menu pull-right">
		         	<div class="dropdown" style="position: absolute; right: 1em; top:0em;">
					  
					  <a class="active dropdown-toggle" href="##" data-toggle="dropdown" style="position: relative; text-align: right;">
						<div class="glyphicon-class"><img src="../images/icon/icon_edit_active.png" /></div>
		            	<div class="glyphicon-class">Waitlist Options<span class="caret"></span></div>
		            	
		              </a>
					  
					  <ul class="dropdown-menu" style="left: -1em;">
					    <li>
					    	<a href="/session/sire/pages/wait-list-app?com=analytics">Analytics</a>
						</li>
						<li>
					    	<a href="/session/sire/pages/wait-list-app?com=customers">Customers</a>
						</li>
					  </ul>

					</div>
		        </div>	

            </div>
        </div>
        <hr class="hrt0">
		<div class="content-body">
			<div class="container-fluid">
				<p class="sub-title">Waitlist manage</p>

				<div class="col-lg-12 col-sm-12 col-md-12 no-padding-right">
					<div class="row">
						<div class="col-md-3 col-sm-3 col-xs-12">
							<div class="form-group">
								<input type="text" placeholder="Search Waitlist name" class="form-control filter-control" data-operator="LIKE" id="txtKeyword" name="">
							</div>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<div class="form-group">
								<button class="btn btn-success-custom" style="line-height: 1.25;" id="search">Search</button>
							</div>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<div class="form-group">
								<input type="text" placeholder="New waitlist name" class="form-control filter-control validate[required, custom[noHTML], maxSize[50]]" data-operator="LIKE" id="new-waitlist-name" data-prompt-position="topLeft" name="waitlist-name">
							</div>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<button type="button" class="btn btn-success-custom" id="add-waitlist">Add New Waitlist</button>
						</div>
					</div>
					<div class="row list">
						<div class="col-md-12 col-sm-12 col-lg-12">
							<div class="table-responsive">
								<table id="waitlist" class="table table-striped table-bordered table-hover table-custom">
									<thead>
										<tr class="center">
											<th class="waitlistnumth">Number</th><th class="waitlistnameth">Waitlist Name</th><th class="waitlistcreatedth">Created</th><th class="waitlistactionth">Action</th><th class="waitlistinfoth">Info</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/font-awesome.min.css"/>
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js"/>
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/site/waitlistapp/wait-list-manage.js"/>
</cfinvoke>

<cfinclude template="../views/layouts/main.cfm" />

<!--- Edit waitlist --->
<div class="modal fade" id="edit-waitlist-modal" tabindex="-1" role="dialog" aria-labelledby="editWaitlistLabel">
    <div class="modal-dialog modal-md" role="document">
	    <div class="modal-content">
	      	<div class="modal-body">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <div class="modal-body-inner row">
		        	<h4 class="col-sm-8 modal-title">Edit Waitlist</h4>
					<div class="form-group">
						<input type="text" class="form-control filter-control validate[required, custom[onlyLetterNumberSp, maxSize[50]]]" data-operator="LIKE" id="edit-waitlist-input" data-prompt-position="topLeft" name="waitlist-name">
					</div><br/>
					<div class="confirm-edit-waitlist-btn-group">
					  	<button class="btn btn-back-custom" data-dismiss="modal">Close</button>
					  	<button class="btn btn-success-custom" id="btn-edit-confirm" type="submit">Save</button>
					</div>
		        </div>
	        </div>
	    </div>
    </div>
</div>
<!--- Modal end --->

<style type="text/css">
	tr td:nth-child(4n+4),
	tr td:nth-child(5n+5) {
	    text-align: center;
	}
	tr.center th {
		text-align: center;
	}
	@media only screen and (max-width: 870px) {
		.dataTables_paginate {
			width: 312px;
		}
		.table-custom {
			width: 820px;
		}
	}
</style>