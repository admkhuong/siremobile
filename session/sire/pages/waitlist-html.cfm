<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/template.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/waitlist-html.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/session/sire/js/waitlist-html.js">
</cfinvoke>


<cfoutput>
<main class="container-fluid page">
    <cfinclude template="../views/commons/credits_available.cfm">
    <section class="row bg-white si-content si-config">
        <div class="tab-title">
        	<ul>
        		<li class="active">Waiting(<span>3</span>)</li>
        		<li>History(<span>3</span>)</li>
        		<li>Analytics</li>
        		<li>Customers</li>
        		<li>Settings</li>
        	</ul>
        </div>
        <h2 class="heading">Waitlist</h2>
        <div class="tab-content">
        	<ul>
        		<li class="active">
	        		<div class="row">
	        			<div class="col-md-4 pr10">
	        				<div class="col-left">
	        					<div class="item">
	        						<p class="name">Served Today</p>
	        						<p class="number">0</p>
	        					</div>
	        					<div class="item">
	        						<p class="name">No-shows Today</p>
	        						<p class="number">0</p>
	        					</div>
	        					<div class="item">
	        						<p class="name">Texted Today</p>
	        						<p class="number">0</p>
	        					</div>
	        				</div>
	        			</div>
	        			<div class="col-md-8">
	        				<div class="col-right">
	        					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	        					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	        					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	        					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
	        					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
	        					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	        				</div>
	        			</div>
	        		</div>
        		</li>
        		<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
        		<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
        		<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
        		<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
        	</ul>
        </div>
    </section>
</main>
</cfoutput>

<cfparam name="variables._title" default="Subscribers - Sire">

<cfinclude template="../views/layouts/main.cfm">