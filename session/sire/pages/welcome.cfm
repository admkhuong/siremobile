<cfinclude template="../../sire/configs/paths.cfm">

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/campaign.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/session/sire/css/learning.css">
</cfinvoke>

<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetWelcomePreference" returnvariable="RetVarGetWelcomePreference"></cfinvoke>


<style>
	
	.learning
	{
        background-color: #ffffff;
        padding: 2em;
        border-radius: 8px;
        overflow-wrap: break-word;
		word-wrap: break-word;

    }

	.carousel-inner
	{
        min-height: 640px;
    }
	
	
	.content-body, .content-header 
	{
    	background-color: #648ba4;
	}
	
	
	.carousel-control.left, .carousel-control.right 
	{
    	background-image: none !important;
    
	}
	
	.carousel-indicators 
	{
    	bottom: 1em !important; 
	}

	.content-header {
    	min-height: 20px;
		padding: 5px;
	}
	
	.glyphicon-chevron-right, .glyphicon-chevron-left 
	{
    
		top: 1em !important;
	}

	.carousel .item
	{
		
		padding-top: 2em;
		
	}
	.inptExpert_label{
		color: #fff;
		vertical-align:middle;
    	
	}

	#inpExpert{
	  margin-top:-2px;
	  margin-left:5px;
	  vertical-align:middle;
	}
	.mycheckboxdiv{
	    text-align:right;
	    margin:5px;
	}
						
</style>


	
<!--- 
<li>Beginner’s Guide to SMS</li>
<li>Creating the Perfect SMS Message</li>
<li>How to Select SMS Keywords</li>
<li>TCPA Compliance</li>
<li>CTIA Compliance</li>
--->

<main class="container-fluid page">
	<cfinclude template="../views/commons/credits_available.cfm">
	<section class="row bg-white">
		<div class="content-header">
			<div class="row">
		        <div class="col-sm-4 col-xs-7 pull-left" style="margin-top:5px;">
		    		<span style="color:#ddd; float: left; color: #fff; font-size: 17px; margin-right: 1em; margin-left: 1em;">Welcome Overview</span>
		    	</div>
		    	<div class="col-sm-offset-5 col-sm-3  pull-right">
		    		<!---
					<p style="margin: 0em 1em 0em 0; padding: .5em; border-radius: 8px; min-height: 4em; text-align: right; font-size: .7em; color:#ddd;">Default<input type="checkbox" name="inpExpert" id="inpExpert" style="margin-left: .5em;" <cfif RetVarGetWelcomePreference.PREF EQ 0>CHECKED="CHECKED"</cfif> > </p>
					--->
				
					<div class="form-group mycheckboxdiv">
		            <label class="inptExpert_label">
		            Skip Tutorial?
		            <input type="checkbox" name="inpExpert" id="inpExpert" class="mycheckbox" <cfif RetVarGetWelcomePreference.PREF EQ 0>CHECKED="CHECKED"</cfif> >
		            </label>
		              
		          </div>

				</div>
			</div>	
		</div>
		

		<hr class="hrt0" style="margin-bottom: 0;">
		
		<div class="content-body "id="WelcomePageTutorial" >
			
			
			<div id="myCarousel" class="carousel slide" data-ride="carousel" style="padding-top: 0em;">		
			
				<ol class="carousel-indicators">
				  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				  <li data-target="#myCarousel" data-slide-to="1"></li>
				  <li data-target="#myCarousel" data-slide-to="2"></li>
				  <li data-target="#myCarousel" data-slide-to="3"></li>
				  <li data-target="#myCarousel" data-slide-to="4"></li>
				  <li data-target="#myCarousel" data-slide-to="5"></li>				  
				</ol>
			
				<div class="carousel-inner" style="padding-bottom: 4em; ">
					<div class="item active"> 
					    <div class="container">				      
					      
						    <div style="text-align: center; color: #fff;">				
								<h3>Keyword Campaigns</h3>	
							</div>

					     	<div class="learning">
							
								<div class="row">
								
									<div class="col-sm-8 col-lg-8 col-md-8">
										
										
									    <p class="welcome-headline"><a href="/session/sire/pages/campaign-manage?tour=keyword"><img class="" style="display: inline;" src="../../../public/sire/images/learning/icon-guided-tour.png"><br class="visible-xs" /><br class="visible-xs" />My First Keyword</a></p>
									    								
										<p>Respond to SMS Keyword triggers with customized Messaging Campaigns</p>
										<p>A keyword is the word or phrase that you or your customer needs to send as an SMS message to your short code address to trigger a campaign response.</p>
										<p>Your Keywords can quickly:</p>
										<p>
											<ul>
												<li>Provide Simple Response</li>
												<li>Ingest Form Information</li>
												<li>Take Surveys</li>	
												<li>Capture Custom Data Fields (CDF)</li>
												<li>Start Drip Marketing Campaigns</li>	
												<li>Start complex interactions across multiple systems</li>								
											</ul>
										</p>
										<p>Sire assists you with easy to customize design templates that create the campaigns that will delight your customers. Sire is your <b>S</b>marter <b>I</b>nteractive <b>R</b>esponse <b>E</b>ngine.</p>
										
									</div>
									
									<div class="col-sm-4 col-lg-4 col-md-4">							
										<img class="ImgFill" style="" src="../../../public/sire/images/learning/keyword-research.jpg">
									</div>
									
								</div>
							</div>	
					    </div>
					</div>
					<div class="item"> 
					    <div class="container">
					      
					      	<div style="text-align: center; color: #fff;">				
								<h3>Build Lists for SMS Marketing</h3>	
							</div>
							
					      	<div class="learning">
								<div class="row">
																		
									<div class="col-sm-8 col-lg-8 col-md-8">
										
										<p class="welcome-headline"><a href="/session/sire/pages/campaign-manage?tour=list"><img class="" style="display: inline;" src="../../../public/sire/images/learning/icon-guided-tour.png"><br class="visible-xs"><br class="visible-xs" />My First List</a></p>
									    									    
										<p>Sire will help you to capture, store and manage SMS subscribers. Your users can either send a keyword to join your list(s) or they can sign up via a form and that will trigger a confirmation via an API call.</p>
										<p>
											<ul>
												<li>Common Short Code accounts are limited to sending triggered SMS to only those phone numbers already on their double opt in lists.</li>
												<li>Enterprise clients may upload their existing lists for use on their own dedicated short codes for pre-approved campaigns.</li>
											</ul>	
										</p>
										
										<p> <a href="/cs-list-building-tips">31 Tips and Tricks for Building Your Own SMS Marketing List</a></p>
									</div>
									
									<div class="col-sm-4 col-lg-4 col-md-4">							
										<img class="ImgFill" style="" src="../../../public/sire/images/feature-demo3.png">								
									</div>
																											
								</div>							
							</div>	
					    </div>
					</div>
					  
					<div class="item"> 
					  	<div class="container">
					      
					      	<div style="text-align: center; color: #fff;">				
								<h3>SMS Marketing Blasts to your List(s)</h3>	
							</div>
							
					      	<div class="learning">
							
								<div class="row">
								
									<div class="col-sm-8 col-lg-8 col-md-8">
										
										<p class="welcome-headline"><a href="/session/sire/pages/campaign-manage?tour=blast"><img class="" style="display: inline;" src="../../../public/sire/images/learning/icon-guided-tour.png"><br class="visible-xs"><br class="visible-xs" />My First Blast</a></p>
									  
										<p>SMS marketing is messaging customers who you've subscribed to various lists. They give you permission to send them messages, and as long as you follow all guidelines and best practices, there should never be negative feedback on your brand. Targeting is also key, which is why you should use the profile data collection options when you subscribe your customers to better target your messages in the most relevant way possible.</p>
										
									</div>
									
									<div class="col-sm-4 col-lg-4 col-md-4">							
										<img class="ImgFill" style="" src="../../../public/sire/images/learning/marketing-strategy-full.png">
									</div>
								</div>								
							</div>
					    </div>
					</div>	
					
					<div class="item"> 
					  	<div class="container">
					      
					      	<div style="text-align: center; color: #fff;">				
								<h3>Trigger a Transactional SMS</h3>	
							</div>
							
					      	<div class="learning">
							
								<div class="row">
																		
									<div class="col-sm-8 col-lg-8 col-md-8">
										
										<p style="padding: 0 0 1em 3em;"><a href="/session/sire/pages/support-transactional-api.cfm">How To Use: The Transactional API</a></p>
									  
										<p>To trigger a Transactional SMS, you need to send information via an HTTPS request to the URL address https://api.siremobile.com/ire/secure/triggerSMS</p>
																		
									</div>
									
									<div class="col-sm-4 col-lg-4 col-md-4">							
										<img class="ImgFill" style="" src="../../../public/sire/images/learning/https-api.png">	
									</div>

								</div>
								
							</div>
					    </div>
					</div>		
					
					<div class="item"> 
					  	<div class="container">		
						  	
						  	<div style="text-align: center; color: #fff;">				
								<h3>Tools</h3>	
							</div>
										      
					      	<div class="learning">
						      	
						      	<p class="welcome-headline"><a href="/session/sire/pages/campaign-manage?tour=gettingstarted&tourrs=true"><img class="" style="display: inline;" src="../../../public/sire/images/learning/icon-guided-tour.png"><br class="visible-xs"><br class="visible-xs" />Campaign Management Console</a></p>
									  
								<h4>Campaign Management</h4>
								<p>Design and launch powerful mobile campaigns with our intuitive Campaign Management Console.</p>
								
							</div>
							
							<div class="learning" style="margin-top: 2em;">
								
								<p class="welcome-headline"><a href="/session/sire/pages/my-account?tour=gettingstarted&tourrs=true"><img class="" style="display: inline;" src="../../../public/sire/images/learning/icon-guided-tour.png"><br class="visible-xs">Your Business Profile</a></p>
									  
								<h4>Manage Your Business Profile</h4>
								<p>Templates with organizational place holders in them will automatically pre-populate with your own business' profile values</p>
								
							</div>

					    </div>
					</div>		

					<div class="item"> 
					  	<div class="container">		
						  	
						  	<div style="text-align: center; color: #fff;">				
								<h3>New Version - Now in Beta</h3>	
							</div>
										      
					      	<div class="learning">
								<h4>Marketing Landing Pages</h4>
								<p>Design and launch landing pages for your marketing efforts.</p>	
								<p>
									<ul>
										<li>Short/Tiny URLs</li>
										<li>Mobile Optimized</li>
										<li>Marketing Landing Pages</li>
										<li>Sign Up Forms</li>
										<li>Form and Data Capture</li>
										<li>Customer Preference Portals</li>
									</ul>	
								</p>	
								
								<p style="padding: 0 0 1em 3em;"><a href="/session/sire/pages/mlp-list">Marketing Landing Pages</a></p>

								<p style="padding: 0 0 1em 3em;"><a href="/session/sire/pages/short-url-manage">Short/Tiny Urls</a></p>
				
							</div>
							
					    </div>
					</div>		
					

<!---


					<div class="item"> 
					  	<div class="container">	
						  	
						  					      
					      	<div class="learning">
								<h4>Frequently Asked Questions</h4>
					          					            
								<cfinclude template="../../../public/sire/pages/cs-faqs-inc.cfm" />
								
								<div style="margin: 2em 0 2em 0;">

		            	<div class="col-md-10 col-md-offset-2">  
									<div class="feature-content" style="text-align: center;">
				                    	<a id="ShowAll" href="javascript:void(0)">Show All</a> <a id="HideAll" href="javascript:void(0)">Hide All</a>
				                    </div>
					                

								</div>
					  		</div>		
					    </div>
--->
										
<!---

					<div class="item"> 
					  	<div class="container">					      
					      	<div class="learning">
								<img class="feature-img-no-bs" alt="" src="../../../public/sire/images/learning/platform.png" style="margin-bottom: 3em;">													
							</div>	
					    </div>
					</div>	

--->

						
					<a class="left carousel-control" href="#myCarousel" data-slide="prev">
					  <span class="glyphicon glyphicon-chevron-left"></span>
					</a>
					
					<a class="right carousel-control" href="#myCarousel" data-slide="next">
					  <span class="glyphicon glyphicon-chevron-right"></span>
					</a>
					
				</div>
				
			</div>			
				
		</div>
	
	</section>
	
</main>

<!-- Modal -->
<div id="WOInfoModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Welcome Overview Default Page</h4>
      </div>
      <div class="modal-body">
	     
	  	<img class="" style="float: right; margin-left: 1em;" height="55px" width="55px" src="../../../public/sire/images/lionink.png">    
	  	<!--- https://www.vectorstock.com/royalty-free-vector/lion-head-on-a-white-background-tattoo-vector-2047730  --->
 	    <p>Feeling Confident?</p>
        <p>When this box is checked, advanced users can default to the Campaign Management Console when they log in. The default landing page when you click on the top left Sire logo will also go to the Campaign Management Console</p>

		<p>Unchecking this box will return the default behavior to Welcome Overview page.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<cfparam name="variables._title" default="Welcome to Sire">
<cfinclude template="../views/layouts/main.cfm">

<!--- https://github.com/mattbryson/TouchSwipe-Jquery-Plugin --->
<script src="/public/sire/js/jquery.touchSwipe.min.js"></script>

<!---
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/js/jquery.touchSwipe.min.js">
</cfinvoke>
--->


<!--- Page Tours using bootstrp tour - see http://http://bootstraptour.com/api/ --->
<script type="application/javascript">

	<!--- Set up page tour for new users --->
	$(function() {
		
		$('.carousel').each(function(){
	        $(this).carousel({
	            interval: false
	        });
	    });
	    
		$(".expand-content-link").click(function() {
			$(this).next(".hidden-content").toggle();
			return false;   
		});
		
		
		
		$("#ShowAll").click(function() {
			$(".hidden-content").show();
			return false;   
		});
		
		$("#HideAll").click(function() {
			$(".hidden-content").hide();
			return false;   
		});

		
		$('#inpExpert').change(function() {
	       
	        if($(this).is(":checked")) 
	        {
	            //var returnVal = confirm("Are you sure?");
	            // $(this).attr("checked", returnVal);
	            
	            UpdateWelcomePref(0);
	            $('#WOInfoModal').modal('show');
	        }
	        else
	        {
		    	 UpdateWelcomePref(1);   
		    	 $('#WOInfoModal').modal('show');
		        
	        }
              
   		});
    

		//Enable swiping...
		$(".carousel-inner").swipe( { allowPageScroll:"auto",
				//Generic swipe handler for all directions
				swipeLeft:function(event, direction, distance, duration, fingerCount) {
					$(this).parent().carousel('next'); 
				},
				swipeRight: function() {
					$(this).parent().carousel('prev'); 
				},
				//Default is 75px, set to 0 for demo so any distance triggers swipe
				threshold:0
			});		
	
	});

	
	function UpdateWelcomePref(inpPref){
		
		if ($('#my_account_form').validationEngine('validate')) {
			try
			{
				$.ajax({
					type: "POST",
					url: '/session/sire/models/cfc/myaccountinfo.cfc?method=SetWelcomePreference&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					data: {inpPref: inpPref},
					error: function(XMLHttpRequest, textStatus, errorThrown) {	},					  
					success:		
						function(d) {	} 		
				});
			}
			catch(ex){		}
		}
	}



</script>

