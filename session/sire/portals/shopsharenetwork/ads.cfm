
<!--- Filter by Category / Brand --->
<cfparam name="category" default="">

<!--- Filter by contains text - Wildcard by default --->
<cfparam name="filter" default="">

<!--- Filter by active - Wildcard by default --->
<cfparam name="active" default="1">



<link rel="stylesheet" href="/session/sire/css/font-awesome.min.css">

<!--- 
	
	Steps to add Campaign
	
	
		Loop over each Agent
	
	
	
	Steps to add agent
	
		Loop over each agency ad
	



	Reserve keyowrds? What happens if keyword is unavailable? Add a character?

	
	
	
	
	Tool to check for orphan ads
	Tool to repair orphaned ads
	
	
	
	Need new CDF - has already signed up for ad
	
	
	Use new CDF for expiration date? - Naw just use hard code date... 
	
	
	
--->





<style>
	#ImageChooserModal .modal-body img {
	    vertical-align: middle;
	    max-width: 100%;
	    max-height: 100%;
	}
	
	.tab-content {
	    border-left: 1px solid #ddd;
	    border-right: 1px solid #ddd;
	    border-bottom: 1px solid #ddd;
	    padding: 10px;
	}
	
	.nav-tabs {
	    margin-bottom: 0;
	}
	
	
	.agency-ad
	{
		position: relative;
		width: 170px;
		height: 250px;
		overflow: hidden;
		margin: .5em 0 0 .5em;		
		background-color: #fff;
<!--- 		border: solid 1px black; --->
		display: inline-block;
		padding: .5em;
<!--- 		box-shadow: 0 2px 1px 0 rgba(0,0,0,0.01); --->
		box-shadow: -1px 2px 5px 0px rgba(50, 50, 50, 0.4);
		border-radius: 3px;
	}
	
	.agency-ad:hover
	{
		box-shadow: 0 0 3pt 2pt rgba(96, 109, 188, 0.68);
		
	}
	
	.ad-image
	{
		max-width: 130px;
		max-height: 130px;		
		display: block;
		margin: 0 auto;	
	}
	
	.ad-slide
	{
		text-align: center;
	}
	
	.ad-slide p
	{
		
		width: 60%;
		margin-left: 20% !important;
		margin-right: 20% !important;
		margin-top: 2em !important;
	}

	.ad-expires
	{
		position: absolute;
		bottom: 1em;
		right: 1em;
		text-align: right;
		color: efefef;
		font-size: .7em;		
	}

	.item_action .template-desc {
	    text-align: left;
	    font-size: .8em;
	    margin-left: .8em;
	}
	
	.item_action {
	    background-color: rgba(39,78,96,0.9);
	    bottom: 0;
	    color: #fff;
	    font-size: 1em;
	    line-height: 20px;
	    padding: 6px 6px 0;
	    position: absolute;
	    left: 0;
	    max-width: 100%;
	    overflow: hidden;
	    text-overflow: ellipsis;
	    /*white-space: nowrap;*/
	    height: 100%;
	    width: 100%;
		text-align:center;
		display: none; 
		cursor: pointer;
	}
	
	.item_action:hover
	{
		
		display: block;
	}
	
	.btn
	{
		color: #FFF;
	    background-color: #74c37f;
	    border-color: #74c37f;
	    text-transform: uppercase;
	    font-weight: 700;
	    text-align: center;
	    border-radius: 1em;
	}
	
	
	.btn-edit-ad, .btn-change-img
	{	
		color: #FFF;	
	    width: 60%;	
	    position: absolute;
	    left: 20%;
	    bottom: 2em;
	}
	
	.mlp-btn {
	    display: inline-block;
	    vertical-align: middle;
	    padding: 10px 24px;
	    border-radius: 15em;
	    border: 1px solid;
	    margin: 2px;
	    min-width: 60px;
	    font-size: .875rem;
	    text-align: center;
	    outline: 0;
	    line-height: 1;
	    cursor: pointer;
	    color: #FFF;
	    background-color: #74c37f;
	    border-color: #74c37f;
	    text-transform: uppercase;
	    font-weight: 700;
	}
	
	.mlp-icon-alert {
	    /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#ff3019+0,cf0404+100;Red+3D */
		background: #ff3019; /* Old browsers */
		background: -moz-linear-gradient(top, #ff3019 0%, #cf0404 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top, #ff3019 0%,#cf0404 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom, #ff3019 0%,#cf0404 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff3019', endColorstr='#cf0404',GradientType=0 ); /* IE6-9 */
	    border-radius: 50%;
	    color: #fff;
	    font-size: 1.125rem;
	    height: 27px;
	    margin: 3px 3px;
	    padding: 3px 3px;
	    transition: background-color .2s ease,border .2s ease;
	    width: 27px;
        line-height: 18px;
        position: absolute;
        right: 1em;
        top: 1em;
	}

	.mlp-icon-alert:hover {
	   /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#ff1a00+0,ff1a00+100;Red+Flat+%231 */
		background: #ff1a00; /* Old browsers */
		background: -moz-linear-gradient(top, #ff1a00 0%, #ff1a00 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top, #ff1a00 0%,#ff1a00 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom, #ff1a00 0%,#ff1a00 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff1a00', endColorstr='#ff1a00',GradientType=0 ); /* IE6-9 */
	    -webkit-box-shadow: -1px 2px 5px 0px rgba(50, 50, 50, 0.4);
		-moz-box-shadow:    -1px 2px 5px 0px rgba(50, 50, 50, 0.4);
		box-shadow:         -1px 2px 5px 0px rgba(50, 50, 50, 0.4);
	    
	}

	.dark-tooltip.dark ul.confirm li.darktooltip-yes
	{
		background-color: #930000;		
	}
	
	.dark-tooltip.dark ul.confirm li.darktooltip-yes:hover
	{
		background-color: #bb0000;		
	}
	
</style>	



<!--- Custom dropzone.js previewTemplate - specify in options on binding the obj --->
<cfsavecontent variable="MLPPreviewTemplate">
	<cfoutput>

		<div class="dz-preview dz-file-preview" style="line-height: 1em;">
		  <div class="dz-details">
		    <div class="dz-filename"><span data-dz-name style="margin-right: 2em;"></span> <span data-dz-size></span></div>
	    		<img data-dz-thumbnail style="margin-top: 2em;" />
			</div>

			<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
			<div class="dz-error-message"><span data-dz-errormessage></span></div>
		</div>
						
	</cfoutput>				
</cfsavecontent>


<!--- Use a javascript template so it can be included in MLPs --->
<script type="text/template" data-template="AgencyAdTemplate">
    <div class="agency-ad item-grid" data-attr-agency-ad-id="0">
					
		<input type="hidden" id="Description" value="" />
		<input type="hidden" id="Expires" value="" />
		
	    <div class="ad-slide">				
			<p class="ContentOne"></p>
			<img src="" class="ad-image" />
	    	<p class="ContentTwo"></p>
	    	
			<div class="ad-expires">Expires: N/A</div>	
										    	
		</div>					
		
		<div class="item_action">
			
			<i class="fa fa-remove mlp-icon-alert" data-tooltip="Are you sure?" style=""></i>                        
            <button type="button" class="btn btn-edit-ad" >Edit</button>

		</div>
							
    </div> 
</script>

<!---

<cfsavecontent variable="AgencyAdTemplate">
	<cfoutput>

		<div class="agency-ad item-grid" data-attr-agency-ad-id="0">
					
			<input type="hidden" id="Description" value="" />
			<input type="hidden" id="Expires" value="" />
			
		    <div class="ad-slide">				
				<p class="ContentOne"></p>
				<img src="" class="ad-image" />
		    	<p class="ContentTwo"></p>
		    	
				<div class="ad-expires">Expires: N/A</div>	
											    	
			</div>					
			
			<div class="item_action">
				
				<i class="fa fa-remove mlp-icon-alert" data-tooltip="Are you sure?" style=""></i>                        
                <button type="button" class="btn btn-edit-ad" >Edit</button>

			</div>
								
	    </div> 
	</cfoutput>				
</cfsavecontent>
--->


<!--- style="background: #F6F6F6;" --->


<div class="portlet">
   <div class="row">
       <div class="col-xs-12">
            <div class="clearfix">
                <h4 class="form-heading"><strong  style="color: rgb(92,92,92); font-family:'Source Sans Pro', sans-serif;">Ads</strong></h4>
            </div>
				
			<div class="col-xs-2">	
				<button id="btn-add-ad" type="button" class="btn btn-add-ad" > <i class="fa fa-plus" aria-hidden="true"></i> Add</button>	
			</div>
			
			<div class="col-xs-6" style="float: right; text-align: right; padding-right: 1em;">	
			   <span class="tile-item-label" style="margin-right: 1em;">Show Expired Ads</span>				   
			   <input id="ShowExpired" type="checkbox" class="" <cfif active EQ 0>checked</cfif> />
		   </div>  
	
       </div>  
   </div>
   
    <div class="row" >
	    	    
	    <div id="AdContainer" class="col-xs-12">
<!---
		  	<cfset AdEditor = 1 />
		    <cfinclude template="/session/sire/portals/shopsharenetwork/inc_ads.cfm" />
--->
		</div>

    </div>    
	    
 </div>
<!---

<cfdump var="#RetVarReadAgencyAds#" />
--->

<!--- Add new Topic --->
<div class="bootbox modal fade" id="Add-Ad-Modal" tabindex="-1" role="dialog" aria-labelledby="Add-Ad-Modal" aria-hidden="true" style="">
    
    <div class="modal-dialog" role="document" style="">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="AdModalLabel">Add New Ad</h4>
            </div>

            <div class="modal-body" style="">
        			        		
				<div>
					<label for="New-Topic-Name">
	    				Ad Description:*
	    			</label>
    			
					<input id="inpDesc" class="" maxlength="255" style="width:100%; padding: .3em;" value="New Agency Ad">
				</div>	 	 				
				
				
				<div style="margin-top: 2em;">
					<label for="inpContentOne">
	    				Content One:*
	    			</label>
    			
					<input id="inpContentOne" class="" maxlength="1024" style="width:100%; padding: .3em;" value="Text <b>BOGO770</b> to <b>39492</b>">
				</div>
    			
				<div id="inpImageLinkOne" class="ImageChooserModal" style="margin-top: 2em;">
					<label for="inpImageLinkOne">
	    				Image Source One:*
	    			</label>
    			
					<div class="item-grid ad-image" style="position: relative;">
    			
						<img src="https://s3-us-west-1.amazonaws.com/siremobile/mlp_image/7043353CB907E2384C30D41102D46A7D/iconmonstr-rocket-19-240.png" class="ad-image" />
						
						
						<div class="item_action" style="">
							                        
	                        <button type="button" class="btn btn-change-img" >Change</button>
	                          
						</div>
					
					</div>
					
				</div>		
				
				<h5>Expire MLP Date { format: yyyy-mm-dd }</h5>	
				<div><input type="text" id="Expire_dt" value="" style="color:##555; padding: .2em;"></div>		 
            
            </div>
            
             <div class="modal-footer">
                <button type="button" id="btn-save-agency-ad" class="mlp-btn" data-dismiss="modal">Save</button>
                <button type="button" class="mlp-btn" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
   
</div>

<!--- Image Chooser --->
<div class="bootbox modal fade" id="ImageChooserModal" tabindex="-1" role="dialog" aria-labelledby="ImageChooserModal" aria-hidden="true" style="">
    
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Choose Image</h4>
            </div>
            <div class="modal-body">
        			
				<div class="tabbable">
				    <ul class="nav nav-tabs" id="myTabs">    
				        <li class="active"><a href="#MLPUploadImage"  class="active" data-toggle="tab">Upload New Image</a></li>
				        <li><a href="/session/sire/models/cfm/imagebrowser-mlp.cfm" data-toggle="MLPImagePicker" data-target="#MLPImagePicker">Your Image Library</a></li>	
				        <li><a href="#MLPImageLink"  class="active" data-toggle="tab3">External Link</a></li>			       
				    </ul>
				 
				    <div class="tab-content">
				        
				        <div class="tab-pane active" id="MLPUploadImage" style="min-height: 10em;">		
					        
					        <div class="row>">			         
								<div class="MLPDropZone" style="xmin-height:2em; text-align:center; line-height:6em; border:dashed 1px #ccc; padding-bottom: 3em;" contenteditable="false">Drop files here or click to upload.
									
									
									
								</div>		
					        </div>
							
							<cfinvoke method="GetUserStorageInfo" component="session.sire.models.cfc.image" returnvariable="retValUserUsage">
							</cfinvoke>
	
							<cfif retValUserUsage.RXRESULTCODE GT 0>
				
								<cfset usagePercent = DecimalFormat((retValUserUsage.USERUSAGE/retValUserUsage.USERSTORAGEPLAN)*100) />
				
								<cfset UsageMBSize = retValUserUsage.USERUSAGE / 1048576>
								<cfset StoragePlanMBSize = retValUserUsage.USERSTORAGEPLAN / 1048576>
				
								<cfif UsageMBSize GT 1024>
									<cfset UsageMBSize = DecimalFormat(UsageMBSize / 1024)>
									<cfset UsageMBSize = UsageMBSize&"GB">
								<cfelse>
									<cfset UsageMBSize = DecimalFormat(UsageMBSize)&"MB">
								</cfif>
				
								<cfif StoragePlanMBSize GT 1024>
									<cfset StoragePlanMBSize = DecimalFormat(StoragePlanMBSize / 1024)>
									<cfset StoragePlanMBSize = StoragePlanMBSize&"GB">
								<cfelse>
									<cfset StoragePlanMBSize = DecimalFormat(StoragePlanMBSize)&"MB">
								</cfif>
				
								<cfoutput>
				
									<input type="hidden" id="inpUserUsage" value="#retValUserUsage.USERUSAGE#">
									<input type="hidden" id="inpUserStoragePlan" value="#retValUserUsage.USERSTORAGEPLAN#">
				
									<div class="row" style="padding-top:20px">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="">
												Account Storage Usage
											</div>	
											<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
												<b><span id="userUsageSpan">#UsageMBSize#</span> / <span id="userStoragePlanSpan">#StoragePlanMBSize#</span></b>
											</div>
											<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
												<div class="progress">
													<div class="progress-bar progress-bar-striped progress-bar-success" role="progressbar" id="usageProgress" 
														aria-valuemin="0" aria-valuemax="100" style="width:#usagePercent#%">
													</div>
												</div>
											</div>
										</div>
									</div>
				
								</cfoutput>
							</cfif>
			        
						</div>
				        
				        <div class="tab-pane" id="MLPImagePicker"></div>
				        
				        <div class="tab-pane" id="MLPImageLink"  style="min-height: 10em;">
					        
					        <div class="MLPDropZone" style="min-height:2em; text-align:left; line-height:1em;" contenteditable="false">Image Link (URL)</div>
					        <input type="text" id="MLPImageURL" value="" style="width: 100%; height: 1.5em;" />					        
					        
				        </div>
				       
				    </div>
				</div>
								    
            </div>
            
            <div class="modal-footer">
                <button type="button" class="mlp-btn" id="btn-upload-image">Use This Image</button>
                <button type="button" class="mlp-btn" id="btn-use-image-link">Use Specified Link</button>
                &nbsp; &nbsp; &nbsp;
                <button type="button" class="mlp-btn" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
   
</div>



<cfparam name="variables._title" default="Sire Portals">
<cfinclude template="../../views/layouts/master.cfm">

<!--- http://www.dropzonejs.com/    	https://github.com/enyo/dropzone --->
<link rel="stylesheet" type="text/css" href="/session/sire/js/vendors/dropzone/dropzone.css" />
<!--- http://www.dropzonejs.com/    	https://github.com/enyo/dropzone --->
<script type="text/javascript" src="/session/sire/js/vendors/dropzone/min/dropzone.min.js"></script> 

<!--- http://rubentd.com/darktooltip/ --->
<link rel="stylesheet" type="text/css" href="/session/sire/js/vendors/darktooltip/darktooltip.css">
<script type="text/javascript" src="/session/sire/js/vendors/darktooltip/jquery.darktooltip.js"></script>


<!--- Include javascript here for better cf documentation --->
<script type="application/javascript">
	
		

	<!--- On page loaded --->
	$(function() {


		<cfoutput> 
			var #toScript(MLPPreviewTemplate, "MLPPreviewTemplate")#; 
		</cfoutput> 	
	
		$('#ShowExpired').change(function() {  	
	  		
	  		console.log('#ShowExpired');
	  			  		  			
  			if($(this).is(":checked")) 
  			{	            
	           	var url = "ads?active=0";
				window.location = url;	            
	        }
	        else
	        {
		       	var url = "ads?active=1";
				window.location = url;
	        }	        
	          			  			
  		});   		
	
		<!--- Allow Agency Ad to expire - if served in javascript iframe loader, then expired will not show by default --->
        var InitExpire = $( "#Expire_dt" ).val();
		$( "#Expire_dt" ).datepicker({
		
			onSelect: function(dateText, inst) { 
		      var dateAsString = dateText; //the first parameter of this function
		      var dateAsObject = $(this).datepicker( 'getDate' ); //the getDate method
		   }
			
		});		
		$( "#Expire_dt" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
		$( "#Expire_dt" ).datepicker('setDate', InitExpire);

		<!--- When user mouses over somthing that is no longer selected hide it --->
		$( document ).on( "mouseleave", ".item-grid", function(event) {
	
			// Keep the selection sticky if the preview option is still active		
			if(  !($("#previewCampaignModal").data('bs.modal') || {isShown: false}).isShown)
			{
				event.preventDefault();
				$('.item_action').hide();
				$(".item-grid").removeClass('selected');
			}
		});

		// TEMPLATE PICKER
		<!--- For mobile device handle onclick as well as mouse over --->
		$('.item-grid').click(function(){
		
			// Keep the selection sticky if the preview option is still active		
			if(  !($("#previewCampaignModal").data('bs.modal') || {isShown: false}).isShown)
			{
				event.preventDefault();
				$('.item_action').hide();
				$(".item-grid").removeClass('selected');
			}
			
			event.preventDefault();
			$('.item_action').hide();
			$(".item-grid").removeClass('selected');
			$(this).addClass('selected');
			campaign_url = $(this).attr("href");
			$('#btn-select-campaign').prop('disabled',false);
	
			$(this).find('.item_action').show();
			
		});	
			
		$( document ).on( "mouseenter", ".item-grid", function(event) {
			event.preventDefault();
			$('.item_action').hide();
			$(".item-grid").removeClass('selected');
			$(this).addClass('selected');
			campaign_url = $(this).attr("href");
			$('#btn-select-campaign').prop('disabled',false);
	
			$(this).find('.item_action').show();
	
		});
		
		$( document ).on( "mouseleave", ".item-grid", function(event) {
		
			// Keep the selection sticky if the preview option is still active		
			if(  !($("#previewCampaignModal").data('bs.modal') || {isShown: false}).isShown)
			{
				event.preventDefault();
				$('.item_action').hide();
				$(".item-grid").removeClass('selected');
			}
		});
		
		
		
		<!--- Add an Ad Modal --->
		$('#btn-add-ad').on("click touchstart", function(e){
			
			$("#Add-Ad-Modal").attr('data-attr-agency-ad-id', 0);
			$("#Add-Ad-Modal").find('#AdModalLabel').html('Add New Ad');
				
			$('#inpDesc').val('New Agency Ad');
					
			$('#inpImageLinkOne').find('img').attr('src', "https://s3-us-west-1.amazonaws.com/siremobile/mlp_image/7043353CB907E2384C30D41102D46A7D/iconmonstr-rocket-19-240.png")
			
			$('#inpContentOne').val("Text <b>BOGOXXX</b> to <b>39492</b>");
			
			$( "#Expire_dt" ).datepicker('setDate', "");
											
			$('#Add-Ad-Modal').modal('show');
		
		});
		
		$('#btn-save-agency-ad').on("click touchstart", function(e){
			AddAgencyAd();	
		});
				
		<!--- Edit existing agency ad --->
		$('.btn-edit-ad').on("click touchstart", function(e){
			BindAdEdit($(this));	
		});
		
		$('.mlp-icon-alert').each(function( index ) {
			BindAdRemove($(this));	
		});		
		
		$('.ImageChooserModal').on("click touchstart", function(e){
			
			<!--- Bind target to this modal --->			
			$('#ImageChooserModal').data('data-target', $(this));
			$('#ImageChooserModal').modal('show');
						
			$('#ImageChooserModal').attr('data-image-target', "img" );
									
			<!--- stop event propogation to prevent hide of menu on click --->
			<!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();
							  	
		});
		
		<!--- Only load the image browser when it is clicked --->
		$('a[data-toggle="MLPImagePicker"]').on("click touchstart", function(e){
		    e.preventDefault()
		    var loadurl = $(this).attr('href')
		    var targ = $(this).attr('data-target')
		    
		    
		    $.get(loadurl, function(data) {
		        $(targ).html(data)
		
		    });
		    
		    $('#btn-upload-image').hide();
		    $('#btn-use-image-link').hide();
		    
		    $(this).tab('show')
		    
		    <!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();
			
		});
		
		$('a[data-toggle="tab"]').on("click touchstart", function(e){
		   		    
		    $('#btn-upload-image').show();
		    $('#btn-use-image-link').hide();
		    
		    $(this).tab('show')
		    
		    <!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();
			
		});

		$('a[data-toggle="tab3"]').on("click touchstart", function(e){
		   		    
		    $('#btn-upload-image').hide();
		    $('#btn-use-image-link').show();
		    
		    var $TargetObj = $('#ImageChooserModal').data('data-target');
							
			<!--- make sure this obj exists --->		
			if($('#ImageChooserModal').attr('data-image-target') == "img" && $TargetObj.find('img').length > 0)
			{				
				$('#MLPImageURL').val($TargetObj.find('img').attr('src'));
			}
			else
			{								
				var bg_url = $TargetObj.css('background-image');
			    // ^ Either "none" or url("...urlhere..")
			    bg_url = /^url\((['"]?)(.*)\1\)$/.exec(bg_url);
			    bg_url = bg_url ? bg_url[2] : ""; // If matched, retrieve url, otherwise ""			    
				
				$('#MLPImageURL').val(bg_url);					
			}
				    
		    $(this).tab('show')
		    
		    <!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();
			
		});
		
		$('#btn-use-image-link').on("click touchstart", function(e){
		  						
		  	<!--- presumes this is getting called in a bootsrap modal #ImageChooserModal and that it is bind(ed) to a valid img editor section --->
			var $TargetObj = $('#ImageChooserModal').data('data-target');
			
			<!--- make sure this obj exists --->		
			if($('#ImageChooserModal').attr('data-image-target') == "img" && $TargetObj.find('img').length > 0)
			{
				$TargetObj.find('img').attr('src', $('#MLPImageURL').val())				
			}
			else
			{
				$TargetObj.css('background-image', 'url(' + $('#MLPImageURL').val() + ')');			
				$('#mlp-section-settings #mlp-background-image-url').html($('#MLPImageURL').val());	
			}
				
			$('#ImageChooserModal').modal('hide');
		
		  	<!--- hack for touch devices not to fire click/touch events twice --->
			e.stopPropagation();
			
		});
		
		
		
		$(".MLPDropZone").dropzone( {
		// var myDropzone = new Dropzone( '.MLPDropZone', { 
			url: "/session/sire/models/cfc/image.cfc?method=SaveImage&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
			thumbnailWidth: 125,
			thumbnailHeight: 125,
			uploadMultiple: false,
			autoQueue: false,
			maxFiles: 1,
			parallelUploads: 1,
			paramName: "upload",
			acceptedFiles: "image/png,image/jpg,image/gif,image/jpeg,image/svg+xml,.svg",
			resize: function(file) {
	        	var resizeInfo = {
		            srcX: 0,
		            srcY: 0,
		            trgX: 0,
		            trgY: 0,
		            srcWidth: file.width,
		            srcHeight: file.height,
		            trgWidth: this.options.thumbnailWidth,
		            trgHeight: this.options.thumbnailHeight
		        };
	
				return resizeInfo;
		    },
		    previewTemplate : MLPPreviewTemplate,
		    init: function() {
			    										    									    
			    this.on("maxfilesexceeded", function(file){
			       // this.removeFile(file);
			    });
			    
			    this.on("addedfile", function() {
			      if (this.files[1]!=null){
			        this.removeFile(this.files[0]);
			      }
			    });
			    
			    // Execute when file uploads are complete
			    this.on("complete", function() {
				   												    												    
				  this.removeAllFiles(true);
				  												    
			      // If all files have been uploaded
			      if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
			        var _this = this;
			        // Remove all files
			        _this.removeAllFiles(true);
			        
			        if(this.files.length != 0){
			            for(i=0; i<this.files.length; i++){
			                this.files[i].previewElement.remove();
			                this.files[i].accepted = false;
			            }
			            this.files.length = 0;
			        }											         
			       
			      }
			    });
			    
			    this.on("addedfile", function(file) {
		
					var _this = this;
					
					<!--- Bad bug - this click event would not work a second time because it was retaining an old reference to the "file" but not the most recent file ''doh --->
					$("#btn-upload-image").off('click');
					
					// Hookup the start button
					$("#btn-upload-image").on("click touchstart", function(e){ 
																										    	
						_this.enqueueFile(file);    
						
						<!--- hack for touch devices not to fire click/touch events twice --->
						e.stopPropagation();
						
						
					 });
				
					if (file.size > maxFilesize) {
						_this.removeFile(file);
						bootbox.alert('Warning!! ' + "This file is too large! Max file size is 10MB");
					}				
				
					var allFilesAdded = _this.getFilesWithStatus(Dropzone.ADDED);
					var userUsage = $("#inpUserUsage").val();
					var userStoragePlan = $("#inpUserStoragePlan").val();
				
					var totalFileSize = 0;
					$.each(allFilesAdded, function(index, val) {
						 totalFileSize += val.size;
					});
				
					if (parseInt(parseInt(userUsage) + parseInt(totalFileSize)) > parseInt(userStoragePlan)) {
						bootbox.alert('Storage limit exceeded! Cannot upload this file');
						$("#btn-upload-image").on("click touchstart", function(e){  <!--- hack for touch devices not to fire click/touch events twice --->
							e.stopPropagation();
							
						});
					}
				
					var plainFileName = file.name.substr(0, file.name.lastIndexOf('.'));
					var fileType = file.name.substr(file.name.lastIndexOf('.')+1, file.name.length);
					if (RegName.test(plainFileName) && plainFileName.length <= 255) {
						try {
							$.ajax({
								url: '/session/sire/models/cfc/image.cfc?method=CheckImageName&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
								type: 'POST',
								dataType: 'json',
								data: {inpPlainFileName: plainFileName, inpFileType: fileType.toLowerCase()}
							})
							.done(function(data) {
								if (parseInt(data.RXRESULTCODE) == 1) {
									if (parseInt(data.CHECKRESULT) == 1) {
										bootbox.dialog({
											message: "This file name ("+plainFileName+"."+fileType+") is already exist! Do you want to replace existing file?",
											title: 'Warning!!',
											buttons: {
												yes: {
													label: "Yes",
													className: "btn btn-medium btn-success-custom",
													callback: function() {
																																				
													}
												},
												no: {
													label: "No",
													className: "btn btn-medium btn-primary btn-back-custom",
													callback: function() {
														_this.removeFile(file);
													}
												},
											}
										});
									} else if (parseInt(data.CHECKRESULT) == 2) {
										bootbox.alert('Filename error ' + data.MESSAGE);
									}
									else
									{
										<!--- file added ok - nothing to do --->
										
									}
								} else {
									bootbox.alert('Warning!! ' + (data.ERRMESSAGE == '' ? 'Error when checking file! Please try again later!' : data.ERRMESSAGE));
								}
								
								$('#processingPayment').hide();
							})
<!---
							.complete(function() {
								$('#processingPayment').hide();
							});
--->
							
							$("#btn-upload-image").prop("disabled", false);
							
							
						} catch (e) {
														
							bootbox.alert('Warning!! ' + "Can't connect to server! Please refresh and try again later!");
						}
					} else {
						_this.removeFile(file);
						if (!RegName.test(plainFileName)) {
							bootbox.alert('Warning!! ' + 'Filename syntax error!<br>Alphanumeric characters [0-9a-zA-Z] and Special characters -, _, (, ) accepted only');
						} else {
							bootbox.alert('Warning!! ' + 'Filename is too long. Please use filename less than 256 characters');
						}
					}
				});
				
				// Update the total progress bar
				this.on("totaluploadprogress", function(progress) {
		<!--- 			document.querySelector("#total-progress .progress-bar").style.width = progress + "%"; --->
				});
				
				this.on("sending", function(file) {
					// Show the total progress bar when upload starts
		<!--- 			document.querySelector("#total-progress").style.opacity = "1"; --->
					// And disable the start button
					$("#btn-upload-image").prop("disabled", true);
				});
				
				// Hide the total progress bar when nothing's uploading anymore
				this.on("queuecomplete", function(progress) {
		<!--- 			document.querySelector("#total-progress").style.opacity = "0"; --->
		
				});
				
				// Hide file when upload done
				this.on("success", function (file, result) {
															
					this.removeFile(file);
					this.removeAllFiles(true);
					
					if (parseInt(result.RXRESULTCODE) == 1) {
										
						<!--- presumes this is getting called in a bootsrap modal #ImageChooserModal and that it is bind(ed) to a valid img editor section --->
						var $TargetObj = $('#ImageChooserModal').data('data-target');
											
						<!--- make sure this obj exists --->		
						<!--- update img obj or background depeneding on where this is opened from  --->
						if($('#ImageChooserModal').attr('data-image-target') == "img" && $TargetObj.find('img').length > 0)
						{
							$TargetObj.find('img').attr('src', result.IMAGEURL)				
						}
						else
						{
							$TargetObj.css('background-image', 'url(' + result.IMAGEURL + ')');		
							$('#mlp-section-settings #mlp-background-image-url').html(result.IMAGEURL);
						}
					
						var oldImg = $('img[src$="'+result.IMAGETHUMBURL+'"]');
				
						if (oldImg.length > 0) {
							var d = new Date();
							oldImg.attr('src', oldImg.attr('src')+'?'+d.getTime());
						} else {
							var html = '<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 image-list-item">'+
											'<img class="image" src="'+result.IMAGETHUMBURL+'" alt="'+result.IMAGENAME+'">'+
											'<button class="btn btn-small btn-danger image-delete" data-imageid="'+result.IMAGEID+'">Delete</button>'+
										'</div>';
							var imageList = $('#image-list');
							if (imageList.children().length == 0) {
								imageList.text('');
							}
							imageList.prepend(html);
						}
				
						updateUserUsageInfo();
													
						<!--- close the image chooser modal --->							
						$('#ImageChooserModal').modal('hide');
						
						$("#btn-upload-image").prop("disabled", true);
				
					} else {
						bootbox.alert('Warning!! ' + (result.ERRMESSAGE == '' ? 'Error when checking file! Please try again later!' : result.ERRMESSAGE));
					}
				});
			}
		});
	
	
		LoadAgencyAds();
		

	});	

	function BindAdEdit(inpObj)
	{						
		$("#Add-Ad-Modal").find('#AdModalLabel').html('Edit Existing Ad');		
		
		$("#Add-Ad-Modal").attr('data-attr-agency-ad-id', inpObj.closest('.item-grid').attr('data-attr-agency-ad-id') );
				
		$('#inpDesc').val(inpObj.closest('.item-grid').find('#Description').val());
				
		$('#inpImageLinkOne').find('img').attr('src', inpObj.closest('.item-grid').find('img').attr('src'))
		
		$('#inpContentOne').val(inpObj.closest('.item-grid').find('.ContentOne').html());
		
		$( "#Expire_dt" ).datepicker('setDate', inpObj.closest('.item-grid').find('#Expires').val());
						
		$('#Add-Ad-Modal').modal('show');
	}
	
	function BindAdRemove(inpObj)
	{		
		inpObj.darkTooltip({ 
			content: 'Are you sure?',
			trigger: 'click',
			modal: false,
			animation:'flipIn',			
			confirm:true,
			yes:'Delete',
			no:'Cancel',			
			finalMessage: 'deleted!',
			onYes: function(){			
				
				var TargetObj = inpObj.closest('.item-grid');
				
				<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->				
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url: '/session/sire/portals/shopsharenetwork/agency.cfc?method=RemoveAgencyAdd&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					async: true,
					data:  
					{ 
						inpPKId : TargetObj.attr('data-attr-agency-ad-id')										
					},					  
					error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
					success:		
					<!--- Default return function for call back --->
					function(d) 
					{																																		
						<!--- RXRESULTCODE is 1 if everything is OK --->
						if (d.RXRESULTCODE == 1) 
						{								
							TargetObj.remove();						
							
							<!--- Since I am removing the object anyway - kill the tool tip --->
							$(".dark-tooltip:visible").remove();			
						}
						else
						{
							<!--- No result returned --->	
							if(d.ERRMESSAGE != "")	
								bootbox.alert(d.ERRMESSAGE);
						}												
					} 		
						
				});				
												
			},
			onClose: function(){			
									 				
				
			}
		});
			
	}	
	
	
	function AddAgencyAd()
	{	
		var target = null;
		
		if(parseInt( $("#Add-Ad-Modal").attr('data-attr-agency-ad-id') ) > 0 )
			target = '/session/sire/portals/shopsharenetwork/agency.cfc?method=UpdateAgencyAd&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
		else
			target = '/session/sire/portals/shopsharenetwork/agency.cfc?method=AddAgencyAd&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true'	
		
		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->				
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: target,   
			dataType: 'json',
			async: true,
			data:  
			{ 
				inpPKId : $("#Add-Ad-Modal").attr('data-attr-agency-ad-id'),
				inpDesc : $('#inpDesc').val(),
				inpImageLinkOne : $('#inpImageLinkOne').find('img').attr('src'),
				inpContentOne : $('#inpContentOne').val(),
				inpExpire:  $.datepicker.formatDate("yy-mm-dd", $('#Expire_dt').datepicker('getDate')),
				inpFilterActive: $('#AutoPublish').is(':checked')
								
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
			success:		
			<!--- Default return function for call back --->
			function(d) 
			{																																		
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1) 
				{								
					if(parseInt(d.NEWADID) > 0)	
					{	
						var AgencyAdTemplate = $('script[data-template="AgencyAdTemplate"]').text();			
		
						var NewObj = $(AgencyAdTemplate);
						
						NewObj.attr('data-attr-agency-ad-id', parseInt(d.NEWADID));
						
						NewObj.find("img.ad-image").attr('src', $('#inpImageLinkOne').find('img').attr('src'));

						if($.datepicker.formatDate("yy-mm-dd", $('#Expire_dt').datepicker('getDate')).trim().length > 0)
							NewObj.find("div.ad-expires").html('Expires: ' + $.datepicker.formatDate("yy-mm-dd", $('#Expire_dt').datepicker('getDate')) );
						else
							NewObj.find("div.ad-expires").html('Expires: N/A');
							
						NewObj.find("p.ContentOne").html($('#inpContentOne').val());
						
						NewObj.find("#Description").val($('#inpDesc').val());	
						
						NewObj.find("#Expires").val($.datepicker.formatDate("yy-mm-dd", $('#Expire_dt').datepicker('getDate')));	
																								
						BindAdRemove(NewObj.find(".mlp-icon-alert"));												
												
						<!--- Edit existing agency ad --->
						NewObj.find('.btn-edit-ad').on("click touchstart", function(e){
							BindAdEdit($(this));	
						});						
												
						$('#AdContainer').append(NewObj);
		
					}
					else
					{
						var CurrAd = $("div.agency-ad[data-attr-agency-ad-id='" + $("#Add-Ad-Modal").attr('data-attr-agency-ad-id') + "']");
						
						CurrAd.find("img.ad-image").attr('src', $('#inpImageLinkOne').find('img').attr('src'));

						if($.datepicker.formatDate("yy-mm-dd", $('#Expire_dt').datepicker('getDate')).trim().length > 0)
							CurrAd.find("div.ad-expires").html('Expires: ' + $.datepicker.formatDate("yy-mm-dd", $('#Expire_dt').datepicker('getDate')) );
						else
							CurrAd.find("div.ad-expires").html('Expires: N/A');
							
						CurrAd.find("p.ContentOne").html($('#inpContentOne').val());
						
						CurrAd.find("#Description").html($('#inpDesc').val());	
						
						CurrAd.find("#Expires").val($.datepicker.formatDate("yy-mm-dd", $('#Expire_dt').datepicker('getDate')));					
					}
								
				}
				else
				{
					<!--- No result returned --->	
					if(d.ERRMESSAGE != "")	
						bootbox.alert(d.ERRMESSAGE);
				}												
			} 		
				
		});				
													
	}	
	
	
	function LoadAgencyAds()
	{						
		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->				
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/portals/shopsharenetwork/agency.cfc?method=ServeAgencyAds&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async: true,
			data:  
			{ 
				inpPKId : 0,
				c: '1',
				u: '522',
				inpFilterActive: $('#AutoPublish').is(':checked')
								
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
			success:		
			<!--- Default return function for call back --->
			function(d) 
			{																																						
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1) 
				{						
					var AgencyAdTemplate = $('script[data-template="AgencyAdTemplate"]').text();
					
					$.each(d.QUERY.DATA.PKId_bi , function(i, val) 
					{ 					  		
						var NewObj = $(AgencyAdTemplate);
						
						NewObj.attr('data-attr-agency-ad-id', parseInt(d.QUERY.DATA.PKId_bi[i]));
						
						NewObj.find("img.ad-image").attr('src', d.QUERY.DATA.ImageLinkOne_vch[i]);

						if(d.QUERY.DATA.FORMATEDEXPIRE_DT[i].trim().length > 0)
							NewObj.find("div.ad-expires").html('Expires: ' + d.QUERY.DATA.FORMATEDEXPIRE_DT[i] );
						else
							NewObj.find("div.ad-expires").html('Expires: N/A');
							
						NewObj.find("p.ContentOne").html(d.QUERY.DATA.ContentOne_vch[i]);
						
						NewObj.find("#Description").val(d.QUERY.DATA.Desc_vch[i]);	
						
						NewObj.find("#Expires").val(d.QUERY.DATA.FORMATEDEXPIRE_DT[i]);	
																								
						BindAdRemove(NewObj.find(".mlp-icon-alert"));												
												
						<!--- Edit existing agency ad --->
						NewObj.find('.btn-edit-ad').on("click touchstart", function(e){
							BindAdEdit($(this));	
						});						
												
						$('#AdContainer').append(NewObj);
	
					  		
					  		 
					});		
					
					
					<!--- 
						$('.bxslider').bxSlider({
							  video: true,
							  useCSS: false,
							  pager: false
							});
							
					--->					
								
				}
				else
				{
					<!--- No result returned - inform user --->	
					
				}												
			} 		
				
		});				
													
	}	
	
</script>	
