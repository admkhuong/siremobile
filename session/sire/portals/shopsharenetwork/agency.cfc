<cfcomponent>
	<cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.loggedIn" default="0"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
        	
	<cfset LOCAL_LOCALE = "English (US)">
			
	<cffunction name="ReadAgencyAds" access="public" output="false" hint="Read All agency ads - optionally filter by text and/or category">
        <cfargument name="inpFilterText" TYPE="string" required="no" default="" hint="Allow to filter on content text"/>
        <cfargument name="inpFilterActive" TYPE="string" required="no" default="1" hint="Allow filtering for non-expired Ads only"/>
                               
        <cfset var dataout = {} />    
		<cfset var ReadAds = '' />
             
        <cftry>      
        
        	<!--- Set default return values --->       
            <cfset dataout.RXRESULTCODE = "-1" />
            <cfset dataout.QUERY = {} />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
                                                       
            <!--- always get latest entry for operator Id regardless of whose list it is on --->
            <cfquery name="ReadAds" datasource="#Session.DBSourceEBM#">
               SELECT 
               		PKId_bi,
				    Status_int,
				    UserId_int,
				    Created_dt,
				    Expire_dt,
				    CategoryId_int,
				    Desc_vch,
				    BaseKeyword_vch,
				    ImageLinkOne_vch,
				    ImageLinkTwo_vch,
				    ImageLinkThree_vch,
				    ContentOne_vch,
				    ContentTwo_vch,
				    ContentThree_vch,
				    ShortURLLink_vch
				FROM 
					simplexprogramdata.agency_ads
				WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">     
                <cfif inpFilterActive EQ "1">
	                AND (Expire_dt > NOW() OR Expire_dt IS NULL)
                </cfif>   
                
                 <cfif LEN(TRIM(inpFilterText)) GT 0>
	                AND 
	                	(
		                	ContentOne_vch LIKE <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#inpFilterText#%"/>
		                	OR
		                	ContentTwo_vch LIKE <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#inpFilterText#%"/>
		                	OR
		                	ContentThree_vch LIKE <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#inpFilterText#%"/>
	                	)
	                	
                </cfif>   
                AND
                	Status_int > 0
                ORDER BY 
                		Expire_dt, PKId_bi
                                                           
            </cfquery>         
                                     
            <cfset dataout.RXRESULTCODE = "1" />
            <cfset dataout.QUERY = ReadAds />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
                     
        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />           
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
        </cfcatch>
        
        </cftry> 

        <cfreturn dataout />
    </cffunction>

	<cffunction name="AddAgencyAd" access="remote" output="false" hint="Add a new Agency ad">
        <cfargument name="inpCategory" TYPE="string" required="no" default="1" hint="The category for this Agency ad"/>
        <cfargument name="inpExpire" TYPE="string" required="no" default="" hint="Format yyyy-mm-dd Set an expiration date for an Agency Ad - Blank will set erase any expiration date"/>
        <cfargument name="inpStatus" TYPE="string" required="no" default="1" hint="Status of the new Agency Ad"/>
        <cfargument name="inpDesc" TYPE="string" required="no" default="New Agency Ad" hint="Description of the new Agency Ad"/>
        <cfargument name="inpImageLinkOne" TYPE="string" required="no" default="" hint="Image Link for use in Agency Ad"/>
        <cfargument name="inpImageLinkTwo" TYPE="string" required="no" default="" hint="Image Link for use in Agency Ad"/>
        <cfargument name="inpImageLinkThree" TYPE="string" required="no" default="" hint="Image Link for use in Agency Ad"/>
		<cfargument name="inpContentOne" TYPE="string" required="no" default="" hint="Content for use in Agency Ad"/>
        <cfargument name="inpContentTwo" TYPE="string" required="no" default="" hint="Content for use in Agency Ad"/>
        <cfargument name="inpContentThree" TYPE="string" required="no" default="" hint="Content for use in Agency Ad"/>
        <cfargument name="inpShortURL" TYPE="string" required="no" default="" hint="Short URL for use with links send in response to Agency Ad"/>
		                               
		                               
        <cfset var dataout = {} />    
		<cfset var AddAgencyAdQuery = '' />
		<cfset var insertResult	= '' />
             
        <cftry>      
        
        	<!--- Set default return values --->       
            <cfset dataout.RXRESULTCODE = "-1" />           
            <cfset dataout.TYPE = "" />
            <cfset dataout.NEWADID = "0" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            
                     
            <!--- Validation MLP Id --->
            <cfif !isNumeric(inpStatus) >
	            <cfset dataout.RXRESULTCODE = "-7" />           
	            <cfset dataout.TYPE = "" />
	            <cfset dataout.NEWADID = "0" />
	            <cfset dataout.MESSAGE = "Invalid Status Specified" />
	            <cfset dataout.ERRMESSAGE = "" />
	            
	            <cfreturn dataout />
            </cfif> 
                                                        
            <!--- always get latest entry for operator Id regardless of whose list it is on --->
            <cfquery name="AddAgencyAdQuery" datasource="#Session.DBSourceEBM#" result="insertResult">
               INSERT INTO 
               		simplexprogramdata.agency_ads
                    (
                    	PKId_bi,
					    Status_int,
					    UserId_int,
					    Created_dt,
					    Expire_dt,
					    CategoryId_int,
					    Desc_vch,
					    BaseKeyword_vch,
					    ImageLinkOne_vch,
					    ImageLinkTwo_vch,
					    ImageLinkThree_vch,
					    ContentOne_vch,
					    ContentTwo_vch,
					    ContentThree_vch,
					    ShortURLLink_vch
                    )
               VALUES
                    (	
                    	NULL,
                    	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpStatus#">,
                    	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">,
	                    NOW(), 
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpExpire)#" null="#NOT(LEN(TRIM(inpExpire)))#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCategory#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDesc#">,
	                    NULL,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpImageLinkOne#" null="#NOT(LEN(TRIM(inpImageLinkOne)))#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpImageLinkTwo#" null="#NOT(LEN(TRIM(inpImageLinkTwo)))#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpImageLinkThree#" null="#NOT(LEN(TRIM(inpImageLinkThree)))#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContentOne#" null="#NOT(LEN(TRIM(inpContentOne)))#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContentTwo#" null="#NOT(LEN(TRIM(inpContentTwo)))#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContentThree#" null="#NOT(LEN(TRIM(inpContentThree)))#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortURL#" null="#NOT(LEN(TRIM(inpShortURL)))#">	
	                )                                        
            </cfquery>         
            
            <cfif insertResult.GENERATED_KEY GT 0>
	        	<cfset dataout.RXRESULTCODE = 1 />
	        	<cfset dataout.MESSAGE = 'Created Agency Ad OK.'>
	        	<cfset dataout.NEWADID = insertResult.GENERATED_KEY />
	        	
	        	<cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
	                <cfinvokeargument name="moduleName" value="Agency Ad">
	                <cfinvokeargument name="operator" value="Agency Ad Added - #dataout.NEWADID#">
	            </cfinvoke>
	            
	        <cfelse>
	        	<cfset dataout.MESSAGE = 'Create Agency Ad failed. Please try again later'>	
	        </cfif>
	                 
        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />            
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
        </cfcatch>
        
        </cftry> 

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="UpdateAgencyAd" access="remote" output="false" hint="Add a new Agency ad">
        <cfargument name="inpPKId" TYPE="string" required="no" default="0" hint="The Unique Agency ad Id - only can update own users data"/>
        <cfargument name="inpCategory" TYPE="string" required="no" default="1" hint="The category for this Agency ad"/>
        <cfargument name="inpExpire" TYPE="string" required="no" default="" hint="Format yyyy-mm-dd Set an expiration date for an Agency Ad - Blank will set erase any expiration date"/>
        <cfargument name="inpStatus" TYPE="string" required="no" default="1" hint="Status of the new Agency Ad"/>
        <cfargument name="inpDesc" TYPE="string" required="no" default="New Agency Ad" hint="Description of the new Agency Ad"/>
        <cfargument name="inpImageLinkOne" TYPE="string" required="no" default="" hint="Image Link for use in Agency Ad"/>
        <cfargument name="inpImageLinkTwo" TYPE="string" required="no" default="" hint="Image Link for use in Agency Ad"/>
        <cfargument name="inpImageLinkThree" TYPE="string" required="no" default="" hint="Image Link for use in Agency Ad"/>
		<cfargument name="inpContentOne" TYPE="string" required="no" default="" hint="Content for use in Agency Ad"/>
        <cfargument name="inpContentTwo" TYPE="string" required="no" default="" hint="Content for use in Agency Ad"/>
        <cfargument name="inpContentThree" TYPE="string" required="no" default="" hint="Content for use in Agency Ad"/>
        <cfargument name="inpShortURL" TYPE="string" required="no" default="" hint="Short URL for use with links send in response to Agency Ad"/>
		                               
        <cfset var dataout = {} />    
		<cfset var UpdateAgencyAdQuery = '' />
		<cfset var insertResult	= '' />
             
        <cftry>      
        
        	<!--- Set default return values --->       
            <cfset dataout.RXRESULTCODE = "-1" />           
            <cfset dataout.TYPE = "" />
            <cfset dataout.NEWADID = "0" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
                    
            <!--- Validation inpPKId --->
            <cfif !isNumeric(inpPKId) >
	            <cfset dataout.RXRESULTCODE = "-6" />           
	            <cfset dataout.TYPE = "" />
	            <cfset dataout.NEWADID = "0" />
	            <cfset dataout.MESSAGE = "Invalid Agency Ad Id Specified" />
	            <cfset dataout.ERRMESSAGE = "" />
	            
	            <cfreturn dataout />
            </cfif> 
            
            <!--- Validation MLP Id --->
            <cfif !isNumeric(inpStatus) >
	            <cfset dataout.RXRESULTCODE = "-7" />           
	            <cfset dataout.TYPE = "" />
	            <cfset dataout.NEWADID = "0" />
	            <cfset dataout.MESSAGE = "Invalid Status Specified" />
	            <cfset dataout.ERRMESSAGE = "" />
	            
	            <cfreturn dataout />
            </cfif> 
                                                        
            <!--- always get latest entry for operator Id regardless of whose list it is on --->
            <cfquery name="UpdateAgencyAdQuery" datasource="#Session.DBSourceEBM#" result="insertResult">
               	UPDATE 
               		simplexprogramdata.agency_ads
               	SET
               		Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpStatus#">,
					Expire_dt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpExpire)#" null="#NOT(LEN(TRIM(inpExpire)))#">,
					CategoryId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCategory#">,
					Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDesc#">,
					BaseKeyword_vch = NULL,	
					ImageLinkOne_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpImageLinkOne#" null="#NOT(LEN(TRIM(inpImageLinkOne)))#">,
					ImageLinkTwo_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpImageLinkTwo#" null="#NOT(LEN(TRIM(inpImageLinkTwo)))#">,
					ImageLinkThree_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpImageLinkThree#" null="#NOT(LEN(TRIM(inpImageLinkThree)))#">,
					ContentOne_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContentOne#" null="#NOT(LEN(TRIM(inpContentOne)))#">,
					ContentTwo_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContentTwo#" null="#NOT(LEN(TRIM(inpContentTwo)))#">,
					ContentThree_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContentThree#" null="#NOT(LEN(TRIM(inpContentThree)))#">,
					ShortURLLink_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortURL#" null="#NOT(LEN(TRIM(inpShortURL)))#">
                WHERE
	                PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpPKId#">
	            AND
	           		UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                                
            </cfquery>         
            
        	<cfset dataout.RXRESULTCODE = 1 />
        	<cfset dataout.MESSAGE = 'Agency Ad Updated OK.'>
        	<cfset dataout.NEWADID = 0 />
        	
        	<cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
	                <cfinvokeargument name="moduleName" value="Agency Ad">
	                <cfinvokeargument name="operator" value="Agency Ad Updated - #arguments.inpPKId#">
	        </cfinvoke>
	                 
        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />            
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
        </cfcatch>
        
        </cftry> 

        <cfreturn dataout />
    </cffunction>
    
    <cffunction name="RemoveAgencyAdd" access="remote" output="false" hint="update MLP status">
		<cfargument name="inpPKId" TYPE="string" required="no" default="0" hint="The Unique Agency ad Id - only can update own users data"/>
        
        <cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = "" />
	   	<cfset dataout.ERRMESSAGE = "" />
	   	<cfset var UpdateAgencyAdQuery = '' />
	   	<cfset var UpdateResult = '' />
	   
        <cftry>
	    	
	    	<!--- always get latest entry for operator Id regardless of whose list it is on --->
            <cfquery name="UpdateAgencyAdQuery" datasource="#Session.DBSourceEBM#" result="UpdateResult">
               	UPDATE 
               		simplexprogramdata.agency_ads
               	SET
               		Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">
			    WHERE
	                PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpPKId#">
	            AND
	           		UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                                
            </cfquery>         

	        <cfif UpdateResult.RecordCount GT 0>
	        	<cfset dataout.MESSAGE = "" />
	        	<cfset dataout.RXRESULTCODE = 1 />

	            <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
	                <cfinvokeargument name="moduleName" value="Agency Ad">
	                <cfinvokeargument name="operator" value="Agency Ad Deleted - #arguments.inpPKId#">
	            </cfinvoke>
	            
	        <cfelse>
	        	<cfset dataout.MESSAGE = "">
	        </cfif>
       			
		<cfcatch>	
		   	<cfset dataout.RXRESULTCODE = "-2" />            
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />  
		</cfcatch>    	
	    </cftry>         
	    <cfreturn dataout>
	</cffunction>

	<cffunction name="ServeAgencyAds" access="remote" output="false" hint="Read All agency ads - optionally filter by text and/or category">
		<cfargument name="c" TYPE="string" required="no" default="1" hint="List of categorty Ids to serve"/>
		<cfargument name="u" TYPE="string" required="no" default="0" hint="User / Owner of the ads"/>
        <cfargument name="inpFilterText" TYPE="string" required="no" default="" hint="Allow to filter on content text"/>
        <cfargument name="inpFilterActive" TYPE="string" required="no" default="1" hint="Allow filtering for non-expired Ads only"/>
                               
        <cfset var dataout = {} />    
		<cfset var ReadAds = '' />
             
        <cftry>      
        
        	<!--- Set default return values --->       
            <cfset dataout.RXRESULTCODE = "-1" />
            <cfset dataout.QUERY = {} />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
                                                       
            <!--- always get latest entry for operator Id regardless of whose list it is on --->
            <cfquery name="ReadAds" datasource="#Session.DBSourceEBM#">
               SELECT 
               		PKId_bi,
				    Status_int,
				    UserId_int,
				    Created_dt,
				    Expire_dt,
				    DATE_FORMAT(Expire_dt,'%Y-%m-%d') FORMATEDEXPIRE_DT,
				    CategoryId_int,
				    Desc_vch,
				    BaseKeyword_vch,
				    ImageLinkOne_vch,
				    ImageLinkTwo_vch,
				    ImageLinkThree_vch,
				    ContentOne_vch,
				    ContentTwo_vch,
				    ContentThree_vch,
				    ShortURLLink_vch
				FROM 
					simplexprogramdata.agency_ads
				WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#U#">     
                <cfif inpFilterActive EQ "1">
	                AND (Expire_dt > NOW() OR Expire_dt IS NULL)
                </cfif>   
                
                 <cfif LEN(TRIM(inpFilterText)) GT 0>
	                AND 
	                	(
		                	ContentOne_vch LIKE <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#inpFilterText#%"/>
		                	OR
		                	ContentTwo_vch LIKE <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#inpFilterText#%"/>
		                	OR
		                	ContentThree_vch LIKE <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#inpFilterText#%"/>
	                	)
	                	
                </cfif>  
                
<!---
                AND
                	CategoryId_int IN (<cfqueryparam value="#c#" cfsqltype="CF_SQL_INTEGER" list="yes" /> )
--->
                AND
                	Status_int > 0
                                                           
            </cfquery>         
                                     
            <cfset dataout.RXRESULTCODE = "1" />
            <cfset dataout.QUERY = ReadAds />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
                     
        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = "-2" />           
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />                
        </cfcatch>
        
        </cftry> 

        <cfreturn dataout />
    </cffunction>

	
</cfcomponent>	