					<cfparam name="AdEditor" default="0"/>
					<cfparam name="active" default="1"/>
										
					
					<cfinvoke method="ServeAgencyAds" component="session.sire.portals.shopsharenetwork.agency" returnvariable="RetVarReadAgencyAds">
						<cfinvokeargument name="c" value="c">
						<cfinvokeargument name="u" value="522">
						<cfinvokeargument name="inpFilterActive" value="#active#">
						
					</cfinvoke> 
			
					
					<cfloop query="RetVarReadAgencyAds.QUERY">
						
						<cfif AdEditor NEQ 1> 
							<li class="MLPXDesignHidden">
						</cfif>
							
								<div class="agency-ad item-grid" data-attr-agency-ad-id="<cfoutput>#RetVarReadAgencyAds.QUERY.PKId_bi#</cfoutput>">
									
									<input type="hidden" id="Description" value="<cfoutput>#RetVarReadAgencyAds.QUERY.Desc_vch#</cfoutput>" />					
									<input type="hidden" id="Expires" value="<cfoutput>#LSDateFormat(RetVarReadAgencyAds.QUERY.Expire_dt, 'yyyy-mm-dd')#</cfoutput>" />
									
								    <div class="ad-slide">				
										<p class="ContentOne"><cfoutput>#RetVarReadAgencyAds.QUERY.ContentOne_vch#</cfoutput></p>
	
										<img src="<cfoutput>#RetVarReadAgencyAds.QUERY.ImageLinkOne_vch#</cfoutput>" class="ad-image" />
	
										<p class="ContentTwo"><cfoutput>#RetVarReadAgencyAds.QUERY.ContentTwo_vch#</cfoutput></p>
								    								    	
										<div class="ad-expires">Expires: <cfif LEN(TRIM(RetVarReadAgencyAds.QUERY.Expire_dt)) GT 0> <cfoutput>#LSDateFormat(RetVarReadAgencyAds.QUERY.Expire_dt, 'yyyy-mm-dd')# </cfoutput> <cfelse> N/A </cfif></div>				    	
									</div>					
														
							    </div> 
							    
						<cfif AdEditor NEQ 1> 		
							</li>
						</cfif>
							
					</cfloop>