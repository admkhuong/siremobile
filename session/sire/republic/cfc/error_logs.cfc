<cfcomponent>
	<cfsetting showdebugoutput="false">

    <!--- <cfinclude template="../../../paths.cfm"> --->
	<cfinclude template="/session/sire/republic/configs/paths.cfm">
    <cfinclude template="/session/sire/republic/configs/env_paths.cfm">

	<!--- Used IF locking down by session - User has to be logged in --->

	<cfparam name="Session.DBSourceEBM" default="Bishop"/>
    <cfparam name="Session.DBSourceREAD" default="bishop_read">

	<cfset LOCAL_LOCALE = "English (US)">

	<cffunction name="LogErrorAction" access="remote" output="true">
		<cfargument name="INPERRORMESSAGE" required="yes" default="" type="string">
		<cfargument name="INPREQUESTURL" required="yes" default="" type="string">
		<cfargument name="INPERRORDETAIL" required="yes" default="" type="string">
		<cfargument name="INPHTTPUSERAGENT" required="yes" default="" type="string">
		<cfargument name="INPHTTPREFERER" required="yes" default="" type="string">
		<cfset var addLog =''>
		<cfset var userId =''>

		<cfif structKeyExists(SESSION, "USERID")>
			<cfset userId = SESSION.USERID>
		</cfif>

		<cftry>
			<cfquery name="addLog" datasource="#Session.DBSourceEBM#">
            INSERT INTO simpleobjects.sire_error_logs
                (
                    ErrorMessage_txt,
                    Created_dt,
                    UserIp_vch,
                    RequestUrl_txt,
					UserId_int,
					ErrorDetail_txt,
					HttpUserAgent_txt,
					HttpReferer_txt
                )
            VALUES
            	(
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.INPERRORMESSAGE#">,
					NOW(),
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CGI.REMOTE_ADDR#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.INPREQUESTURL#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.INPERRORDETAIL#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.INPHTTPUSERAGENT#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ARGUMENTS.INPHTTPREFERER#">
				)
	        </cfquery>
		<cfcatch>
		</cfcatch>

		</cftry>

		<cfreturn false>

	</cffunction>

	<cffunction name="GetErrorList" access="remote" output="true"><!--- returnformat="JSON"--->
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="customFilter" default=""><!---this is the custom filter data --->


		<cfset var dataOut = {}>
		<cfset dataOut.DATA = ArrayNew(1)>

		<cfset var total_pages = 1>
		<cfset var order 	= "">
		<cfset var filterItem = '' />
		<cfset var permissionStr = '' />
		<cfset var getUserByUserId = '' />

		<!--- CHECK PERMISSION
		<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
			<cfinvokeargument name="operator" value="#EMS_List_Title#">
		</cfinvoke>
		<!--- Check permission against acutal logged in user not "Shared" user--->
		<cfif Session.CompanyUserId GT 0>
		    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
		        <cfinvokeargument name="userId" value="#Session.CompanyUserId#">
		    </cfinvoke>
		<cfelse>
		    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getUserByUserId">
		        <cfinvokeargument name="userId" value="#session.userId#">
		    </cfinvoke>
		</cfif>


		<cfif NOT permissionStr.havePermission 	OR (session.userRole EQ 'CompanyAdmin' AND getUserByUserId.CompanyAccountId NEQ session.companyId)
			OR (session.userRole EQ 'User' AND getUserByUserId.userId NEQ session.userId) >
			<cfset dataout.RXRESULTCODE = -2 />
            <cfset dataout.MESSAGE = "#permissionStr.message#" />
			<cfset dataout.TYPE = "" />
		    <cfset dataout.ERRMESSAGE = "" />
			<cfreturn dataout>
		</cfif>
		--->

		<!---end check permission --->


		<cfset order = #BuildSortingParamsForDatatable(iSortCol_0,iSortCol_1,sSortDir_0,sSortDir_1)#>

		<cftry>

			<cfset var GetNumbersCount = ''>
			<cfset var GetERORRS = ''>
			<cfset var status = "">
			<cfset var actionHtml = "">

		 	<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />

			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>

			<!---ListEMSData is key of DataTable control--->
			<cfset dataout["ListEMSData"] = ArrayNew(1)>

			<!---Get total EMS for paginate --->
			<cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
				SELECT
					COUNT(*) AS TOTALCOUNT
				FROM
					simpleobjects.sire_error_logs se

			    <cfif customFilter NEQ "">
			   		WHERE 1 = 1
					<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
						<cfif filterItem.NAME EQ ' se.Status_int ' AND  filterItem.VALUE LT 0>
						<!--- DO nothing --->
						<cfelseif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
							AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
						<cfelse>
							AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
						</cfif>
					</cfloop>
				</cfif>
			</cfquery>
			<cfif GetNumbersCount.TOTALCOUNT GT 0>
				<cfset dataout["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
				<cfset dataout["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>
	        </cfif>

	        <!--- Get ems data --->
			<cfquery name="GetERORRS" datasource="#Session.DBSourceREAD#">
				SELECT
					se.SireErrorLogsId_int,
					se.ErrorMessage_txt,
					se.RequestUrl_txt,
					se.UserId_int,
					se.UserIp_vch,
					se.Created_dt,
					se.Status_int
				FROM
					simpleobjects.sire_error_logs se
			   <cfif customFilter NEQ "">
			   	WHERE 1 = 1
					<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
						<cfif filterItem.NAME EQ ' se.Status_int ' AND  filterItem.VALUE LT 0>
						<!--- DO nothing --->
						<cfelseif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
							AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
						<cfelse>
							AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
						</cfif>
					</cfloop>
				</cfif>
				#order#
				LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
	        </cfquery>

		    <cfloop query="GetERORRS">
                <cfset actionHtml = ''>
                <cfset status = ''>

                <cfswitch expression = #GetERORRS.Status_int#>
                	<cfcase value="0"> <cfset status = 'Open'> </cfcase>
                	<!--- <cfcase value="1"> <cfset status = 'In Progess'> </cfcase>
                	<cfcase value="2"> <cfset status = 'Tech Review'> </cfcase>
                	<cfcase value="3"> <cfset status = 'QA Review'> </cfcase>  --->
                	<cfcase value="4"> <cfset status = 'Closed'> </cfcase>
                </cfswitch>


				<cfset var tempItem = {
					ID = '#GetERORRS.SireErrorLogsId_int#',
					ERROR_MSG = '#GetERORRS.ErrorMessage_txt#',
					REQUEST_URL = '#GetERORRS.RequestUrl_txt#',
					USER_ID = '#GetERORRS.UserId_int#',
					USER_IP = '#GetERORRS.UserIp_vch#',
					CREATED_DT = '#GetERORRS.Created_dt#',
					STATUS = '#status#'
				}>
				<cfset dataout["ListEMSData"].append(tempItem)>
		    </cfloop>


        	<cfcatch type="Any" >
				<cfset dataout.RXRESULTCODE = -1 />
			    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
			    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
			    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
				<cfset dataout["iTotalRecords"] = 0>
				<cfset dataout["iTotalDisplayRecords"] = 0>
				<cfset dataout["ListEMSData"] = ArrayNew(1)>
       		</cfcatch>

        </cftry>


        <cfreturn dataOut>
	</cffunction>

	<cffunction name="BuildSortingParamsForDatatable" access="public">
		<cfargument name="iSortCol_0" default="-1">
		<cfargument name="iSortCol_1" default="-1">
		<!---these arethe index of colums that need to be sorted, their value could be
			1 is EMS Name,
			2 is Created,
			3 is Delivered,
			4 is Count Delivered,
			5 is Recipient--->
		<cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
			<cfset var dataout= "">
			<cftry>
            	<!---set order param for query--->
				<cfif iSortCol_0 EQ 0>
					<cfset order_0 = "desc">
				<cfelse>
					<cfif sSortDir_0 EQ "asc" OR sSortDir_0 EQ "desc">
						<cfset var order_0 = sSortDir_0>
					<cfelse>
						<cfset order_0 = "">
					</cfif>
				</cfif>
				<cfif sSortDir_1 EQ "asc" OR sSortDir_1 EQ "desc">
					<cfset var order_1 = sSortDir_1>
				<cfelse>
					<cfset order_1 = "">
				</cfif>
				<cfif iSortCol_0 neq -1 and order_0 NEQ "">
					<cfset dataout = dataout &" order by ">
					<cfif iSortCol_0 EQ 1>
						<cfset dataout = dataout &" se.SireErrorLogsId_int ">
					<cfelseif iSortCol_0 EQ 2>
							<cfset dataout = dataout &" se.Created_dt ">
					<cfelse>
							<cfset dataout = dataout &" se.SireErrorLogsId_int ">
					</cfif>
					<cfset dataout = dataout &"#order_0#">
				</cfif>
				<cfif iSortCol_1 neq -1 and order_1 NEQ "">
					<cfset dataout = dataout &" , ">
					<cfif iSortCol_1 EQ 1>
						<cfset dataout = dataout &" se.SireErrorLogsId_int ">
						<cfset dataout = dataout &"#order_0#">
					<cfelseif iSortCol_1 EQ 2>
						<cfset dataout = dataout &" se.Created_dt ">
						<cfset dataout = dataout &"#order_0#">
					<cfelse>
						<cfset dataout = dataout &" se.SireErrorLogsId_int ">
						<cfset dataout = dataout &"desc">
					</cfif>
				</cfif>
            <cfcatch type="Any" >
				<cfset dataout = "">
            </cfcatch>
            </cftry>
			<cfreturn dataout>
	</cffunction>

    <cffunction name="getErrorLogDetailById" access="public" output="true">
        <cfargument name="errid" required="yes" default="" type="string">

        <cfset var dataout = {} />
        <cfset var logData = '' />
        <cftry>
            <cfquery datasource="#Session.DBSourceREAD#" name="logData">
                SELECT
                    se.SireErrorLogsId_int,
                    se.ErrorMessage_txt,
                    se.RequestUrl_txt,
                    se.UserId_int,
                    se.UserIp_vch,
                    se.ErrorDetail_txt,
                    se.HttpUserAgent_txt,
                    se.HttpReferer_txt,
                    se.Status_int,
                    se.Created_dt
                FROM
                    simpleobjects.sire_error_logs se
                WHERE
                    SireErrorLogsId_int = <CFQUERYPARAM CFSQLTYPE='CF_SQL_Integer' VALUE='#errid#'>
            </cfquery>

            <cfif logData.RecordCount GT 0>
                <cfset dataout.RXRESULTCODE = 1 />
                <cfset dataout.DATA = logData>
            <cfelse>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "No error found." />
                <cfset dataout.ERRMESSAGE = "" />
            </cfif>

            <cfcatch type="any">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout>
    </cffunction>

    <cffunction name="updateErrorStatus" access="remote" output="true">
        <cfargument name="errid" required="yes" default="" type="string">
        <cfargument name="status" required="yes" default="" type="string">

        <cfset var dataout = {} />
        <cfset var logData = '' />

        <cftry>
            <cfquery datasource="#Session.DBSourceEBM#" name="logData">
                UPDATE
                    simpleobjects.sire_error_logs
                SET
                    Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_Integer" VALUE="#status#">,
                    LastAdminId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_Integer" VALUE="#session.USERID#">,
                    LastUpdated_dt = NOW()
                WHERE
                    SireErrorLogsId_int = <CFQUERYPARAM CFSQLTYPE='CF_SQL_Integer' VALUE='#errid#'>
            </cfquery>

            <cfset dataout.SUCCESS = true />
            <cfset dataout.MESSAGE = "Update successfully." />
            <cfset dataout.ERRMESSAGE = "" />

            <cfcatch type="any">
                <cfset dataout.SUCCESS = false />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout>
    </cffunction>
</cfcomponent>
