<cfcomponent>
    <cffunction name="QueryToStruct" access="public" returntype="any" output="false"
        hint="Converts an entire query or the given record to a struct. This might return a structure (single record) or an array of structures.">
        <cfargument name="Query" type="query" required="true" />
        <cfargument name="Row" type="numeric" required="false" default="0" />

        <cfscript>
            var LOCAL = StructNew();
            if (ARGUMENTS.Row){
                LOCAL.FromIndex = ARGUMENTS.Row;
                LOCAL.ToIndex = ARGUMENTS.Row;
            } else {
                LOCAL.FromIndex = 1;
                LOCAL.ToIndex = ARGUMENTS.Query.RecordCount;
            }
            LOCAL.Columns = ListToArray( ARGUMENTS.Query.ColumnList );
            LOCAL.ColumnCount = ArrayLen( LOCAL.Columns );
            LOCAL.DataArray = ArrayNew( 1 );
            for (LOCAL.RowIndex = LOCAL.FromIndex ; LOCAL.RowIndex LTE LOCAL.ToIndex ; LOCAL.RowIndex = (LOCAL.RowIndex + 1)){
                ArrayAppend( LOCAL.DataArray, StructNew() );
                LOCAL.DataArrayIndex = ArrayLen( LOCAL.DataArray );
                for (LOCAL.ColumnIndex = 1 ; LOCAL.ColumnIndex LTE LOCAL.ColumnCount ; LOCAL.ColumnIndex = (LOCAL.ColumnIndex + 1)){
                    LOCAL.ColumnName = LOCAL.Columns[ LOCAL.ColumnIndex ];
                    LOCAL.DataArray[ LOCAL.DataArrayIndex ][ LOCAL.ColumnName ] = ARGUMENTS.Query[ LOCAL.ColumnName ][ LOCAL.RowIndex ];
                }
            }
            if (ARGUMENTS.Row){
                return( LOCAL.DataArray[ 1 ] );
            } else {
                return( LOCAL.DataArray );
            }
        </cfscript>
    </cffunction>
</cfcomponent>    