<cfcomponent>
	<cfinclude template="/public/sire/configs/paths.cfm">

	<cffunction name='getActivePromoCode' access="remote" output="true" hint="get active promotion code via input code">
		<cfargument name="inpCode" type="string" required="yes">
		<cfargument name="inpIsYearlyPlan" type="string" required="no" default="0">
		<cfargument name="inpEmail" type="string" required="no">
		
		<cfset var getActivePromoCode = {} />
		<cfset var dataout	= {} />
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        
        <cfset dataout.promotion_id = 0 />
        <cfset dataout.recurring_time = 0 />
        <cfset dataout.promotion_discount = '' />
		
        <cfset var discountValue = '' />
        <cfset var percentageValue = '' />
        <cfset var flatrateValue = '' />
        <cfset var creditValue = '' />
        <cfset var keywordValue = '' />
        <cfset var mlpValue = '' />
        <cfset var shortUrlValue = '' />
        <cfset var findEmailInInvitationList = '' />

		<cftry>
			<cfset var now = Now()>

			<!--- <cfif arguments.inpIsYearlyPlan EQ 1>
				<cfset dataout.RXRESULTCODE = -1 />
        		<cfset dataout.MESSAGE = "This coupon is not available for this plan. We are sorry for this inconvenience." />
				<cfreturn dataout />
			</cfif> --->
			
			<cfquery name="getActivePromoCode" datasource="#Session.DBSourceREAD#">
				SELECT
					promotion_id,
					promotion_desc,
					origin_id,
					recurring_time,
					expiration_date,
					DATEDIFF(expiration_date,CURDATE()) AS DaysUntilExpires,
					discount_plan_price_percent,
					discount_plan_price_flat_rate,
					promotion_credit,
					promotion_keyword,
					promotion_MLP,
					promotion_short_URL,
					discount_type_group,
					invitation_only_ti,
					invitation_email_list
				FROM
					`simplebilling`.`promotion_codes`
				WHERE
					latest_version = 1 
					AND promotion_code LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCode#"> 
					AND promotion_status = 1
					AND date(start_date) <=  <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#now#">
					AND IFNULL(expiration_date, date(start_date) <=  <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#now#">)
					AND (( redemption_count_int > 0 AND max_redemption_int > 0) OR max_redemption_int = 0)
					AND promotion_id > 0
					AND origin_id > 0
				ORDER BY
					promotion_id DESC
				LIMIT 1
			</cfquery>
				 
			<cfif getActivePromoCode.RecordCount GT 0>
				<cfloop query="getActivePromoCode">

					<cfif getActivePromoCode.invitation_only_ti EQ 1>
						<cfset findEmailInInvitationList = Find('#arguments.inpEmail#',getActivePromoCode.invitation_email_list)/>
						<cfif findEmailInInvitationList LTE 0>
							<cfset dataout.MESSAGE = "Promo Code: #arguments.inpCode#. We're sorry, the promotional code you entered is expired or inactive, please try again." />
							<cfset dataout.RXRESULTCODE = -2 />
							<cfreturn dataout />
						</cfif>
					</cfif>
					<cfif getActivePromoCode.expiration_date EQ '' OR DaysUntilExpires GTE 0>
				        <cfset dataout.promotion_id = getActivePromoCode.promotion_id />
				        <cfset dataout.recurring_time = getActivePromoCode.recurring_time />
				        <cfset dataout.origin_id = getActivePromoCode.origin_id />

						<cfif getActivePromoCode.discount_plan_price_percent GT 0>
							<cfset percentageValue = '#getActivePromoCode.discount_plan_price_percent#' & '% off plan discount' & '<br/>'/>
						</cfif>
						<cfif getActivePromoCode.discount_plan_price_flat_rate GT 0>
							<cfset flatrateValue = '$'&'#NumberFormat(getActivePromoCode.discount_plan_price_flat_rate, '9.99')#' & ' plan price discount' & '<br/>' />
						</cfif>
						<cfif getActivePromoCode.promotion_credit GT 0>
							<cfset creditValue = '#getActivePromoCode.promotion_credit#' & ' addtional credits' & '<br/>' />
						</cfif>
						<cfif getActivePromoCode.promotion_keyword GT 0>
							<cfset keywordValue = '#getActivePromoCode.promotion_keyword#' & ' addtional keywords' & '<br/>' />
						</cfif>
						<cfif getActivePromoCode.promotion_MLP GT 0>
							<cfset mlpValue = '#getActivePromoCode.promotion_MLP#' & ' addtional MLPs' & '<br/>' />
						</cfif>
						<cfif getActivePromoCode.promotion_short_URL GT 0>
							<cfset shortUrlValue = '#getActivePromoCode.promotion_short_URL#' & ' addtional Short URLs' & '<br/>'/>
						</cfif>

						<cfif getActivePromoCode.discount_type_group EQ 0 >
							<!--- <cfset dataout.promotion_discount = 'No Discount' & '<br/>' & '#getActivePromoCode.promotion_desc#'/> --->
							<cfset dataout.promotion_discount = reReplace(getActivePromoCode.promotion_desc, '\n', '<br />', 'ALL')/>
							<cfset dataout.promotion_discount = '#dataout.promotion_discount#'/>
						<cfelseif getActivePromoCode.discount_type_group EQ 1 >
							<!--- <cfset dataout.promotion_discount = '#percentageValue#' & '#flatrateValue#' & '#getActivePromoCode.promotion_desc#'/> --->
							<cfset dataout.promotion_discount = reReplace(getActivePromoCode.promotion_desc, '\n', '<br />', 'ALL')/>
							<cfset dataout.promotion_discount = '#dataout.promotion_discount#'/>
						<cfelse>
							<!--- <cfset dataout.promotion_discount = '#creditValue#' & '#keywordValue#' & '#mlpValue#' & '#shortUrlValue#' & '#getActivePromoCode.promotion_desc#'/> --->
							<cfset dataout.promotion_discount = reReplace(getActivePromoCode.promotion_desc, '\n', '<br />', 'ALL')/>
							<cfset dataout.promotion_discount = '#dataout.promotion_discount#'/>
						</cfif>

						<cfset dataout.RXRESULTCODE = 1/>
				   	<cfelse>
				    	<cfset dataout.RXRESULTCODE = -3 />
				    	<cfset dataout.MESSAGE = "Promo Code: #arguments.inpCode#. We're sorry, the promotional code you entered is expired or inactive, please try again." />
				    </cfif>
				</cfloop>
			<cfelse>
				<cfset dataout.MESSAGE = "Promo Code: #arguments.inpCode#. We're sorry, the promotional code you entered is expired or inactive, please try again."/>
			</cfif>
			
            <cfcatch>
                <cfset dataout.type = "#cfcatch.Type#" />
                <cfset dataout.message = "#cfcatch.Message#" />
                <cfset dataout.errMessage = "#cfcatch.detail#" />
            </cfcatch>
		</cftry>
		
		<cfreturn dataout /> 
	</cffunction>

	<cffunction name='UpdateUserPromoCodeUsedTime' access="remote" output="false" hint="Update used time">
		<cfargument name="inpUserId" type="numeric" required="yes">
		
		<cfset var updateUserPromoCode = {} />
		<cfset var updateUserPromoCodeResult = {} />
		<cfset var dataout = {}>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />

        <cfif inpUserId GT 0>
        	<cftry>

				<cfquery name="updateUserPromoCode" datasource="#Session.DBSourceEBM#" result="updateUserPromoCodeResult">
					UPDATE simplebilling.userpromotions
					SET 
						UsedTime_int = UsedTime_int + 1
					WHERE
						UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
					AND
						PromotionStatus_int = 1
				</cfquery>

				<cfif updateUserPromoCodeResult.RecordCount GT 0>
					<cfset dataout.RXRESULTCODE = 1 />	
				</cfif>
			<cfcatch>
                <cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.MESSAGE = "#cfcatch.Message#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>	
			</cftry>
		</cfif>
		
		<cfreturn dataout /> 
		
	</cffunction>

	<cffunction name="GetCouponDetails" access="remote" output="true" hint="Get coupon detail by promotion id">
		<cfargument name="inpCouponId" required="true" type="numeric">
		<cfargument name="isIndepentOnVersion" required="no" default="0">

		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.RXRESULTCODE = -1 />

		<cfset var getCouponDetails = '' />

		<cftry>
			<cfquery name="getCouponDetails" datasource="#Session.DBSourceREAD#">
				SELECT
					origin_id,
					promotion_id,
    				promotion_code,
    				promotion_name,
    				promotion_desc,
    				promotion_status,
    				discount_type_group,
    				discount_plan_price_percent,
    				discount_plan_price_flat_rate,
    				promotion_credit,
    				promotion_keyword,
    				promotion_MLP,
    				promotion_short_URL,
    				start_date,
    				expiration_date,
    				recurring_time,
    				limit_cb1_ti,
    				limit_cb2_ti,
    				invitation_only_ti,
    				invitation_email_list,
    				max_redemption_int,
    				used_time_by_recurring_int,
    				used_time_by_upgrade_int
    			FROM
    				simplebilling.promotion_codes
    			WHERE
    				promotion_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpCouponId#">
    			<cfif arguments.isIndepentOnVersion EQ 0>
	    			AND 
	    				latest_version = 1
	    		</cfif>
			</cfquery>

			<cfif getCouponDetails.RECORDCOUNT GT 0>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.MESSAGE = "Get coupon detail successfully!"/>
				<cfset dataout.ORIGINID = getCouponDetails.origin_id/>
				<cfset dataout.PROMOTIONID = getCouponDetails.promotion_id/>
				<cfset dataout.COUPONCODE = getCouponDetails.promotion_code/>
				<cfset dataout.COUPONNAME = getCouponDetails.promotion_name />
				<cfset dataout.COUPONDESC = getCouponDetails.promotion_desc />
				<cfset dataout.COUPONSTATUS = getCouponDetails.promotion_status />
				<cfset dataout.COUPONDISCOUNTTYPE = getCouponDetails.discount_type_group />
				<cfset dataout.COUPONDISCOUNTPRICEPERCENT = getCouponDetails.discount_plan_price_percent />
				<cfset dataout.COUPONDISCOUNTPRICEFLATRATE = getCouponDetails.discount_plan_price_flat_rate />
				<cfset dataout.COUPONCREDIT = getCouponDetails.promotion_credit />
				<cfset dataout.COUPONKEYWORD = getCouponDetails.promotion_keyword />
				<cfset dataout.COUPONMLP = getCouponDetails.promotion_MLP />
				<cfset dataout.COUPONSHORTURL = getCouponDetails.promotion_short_URL />
				<cfset dataout.COUPONSTARTDATE = DateFormat(getCouponDetails.start_date, 'yyyy-mm-dd') />
				<cfset dataout.COUPONENDDATE = DateFormat(getCouponDetails.expiration_date, 'yyyy-mm-dd') />
				<cfset dataout.COUPONRECURRING = getCouponDetails.recurring_time />
				<cfset dataout.LIMITCB1 = getCouponDetails.limit_cb1_ti />
				<cfset dataout.LIMITCB2 = getCouponDetails.limit_cb2_ti />
				<cfset dataout.INVITATIONONLY = getCouponDetails.invitation_only_ti />
				<cfset dataout.INVITATIONLIST = getCouponDetails.invitation_email_list />
				<cfset dataout.COUPONREDEMPTION = getCouponDetails.max_redemption_int />
				<cfset dataout.USEDTIME = getCouponDetails.used_time_by_recurring_int + getCouponDetails.used_time_by_upgrade_int />
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = 'Invalid coupon!'/>
			</cfif>

			<cfcatch>
                <cfset dataout.TYPE = "#cfcatch.Type#" />
                <cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.MESSAGE = "#cfcatch.Message#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<!--- CheckAvailableCouponForUser	 --->
	<cffunction name="CheckAvailableCouponForUser" access="remote" hint="Check if coupon is available for user">
		<cfargument name="inpUserId" required="true">

		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout['DATALIST'] = [] />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.RXRESULTCODE = 0 />

		<cfset var checkAvailableCoupon = '' />
		<cfset var getUserCoupon = '' />
		<cfset var getCouponResult = '' />
		<cfset var disableCoupon = '' />
		<cfset var temp = {} />
		<cfset temp.userPromotionId = 0/>

		<cftry>
			<cfquery name="getUserCoupon" datasource="#Session.DBSourceEBM#">
				SELECT
					UserPromotionId_int,
					PromotionLastVersionId_int,
					UsedTimeByRecurring_int,
					RecurringTime_int
				FROM
					simplebilling.userpromotions
				WHERE
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
				AND
					PromotionStatus_int = 1
			</cfquery>

			<cfif getUserCoupon.RECORDCOUNT GT 0>
				<cfloop query="getUserCoupon">
					<cfif getUserCoupon.RecurringTime_int EQ -1>
						<cfinvoke method="GetCouponDetails" returnvariable="getCouponResult">
							<cfinvokeargument name="inpCouponId" value="#getUserCoupon.PromotionLastVersionId_int#"/>
							<cfinvokeargument name="isIndepentOnVersion" value="1"/>
						</cfinvoke>

						<cfif getCouponResult.RXRESULTCODE GT 0>
							<cfset temp = getCouponResult />
							<cfset temp.userPromotionId = getUserCoupon.UserPromotionId_int/>

							<cfset arrayAppend(dataout['DATALIST'], temp)/>
						</cfif>
					<cfelseif (getUserCoupon.UsedTimeByRecurring_int GT getUserCoupon.RecurringTime_int AND getUserCoupon.RecurringTime_int NEQ -1) OR (getUserCoupon.RecurringTime_int EQ 0)>
						<cfquery name="disableCoupon" datasource="#Session.DBSourceEBM#">
							UPDATE
								simplebilling.userpromotions
							SET
								PromotionStatus_int = 0
							WHERE
								UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
							AND
								PromotionLastVersionId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#getUserCoupon.PromotionLastVersionId_int#">
						</cfquery>
						<cfset dataout.RXRESULTCODE = -1/>
						<cfset dataout.MESSAGE = 'Coupon not available' />
					<cfelse>
						<cfinvoke method="GetCouponDetails" returnvariable="getCouponResult">
							<cfinvokeargument name="inpCouponId" value="#getUserCoupon.PromotionLastVersionId_int#"/>
							<cfinvokeargument name="isIndepentOnVersion" value="1"/>
						</cfinvoke>
						<cfif getCouponResult.RXRESULTCODE GT 0>
							<cfset temp = getCouponResult />
							<cfset temp.userPromotionId = getUserCoupon.UserPromotionId_int/>
							<cfset arrayAppend(dataout['DATALIST'], temp)/>
						</cfif>
					</cfif>
				</cfloop>

				<cfif arrayLen(dataout['DATALIST']) GT 0>
					<cfset dataout.MESSAGE = 'Get available coupon list successfully' />
					<cfset dataout.RXRESULTCODE = 1/>
				<cfelse>
					<cfset dataout.MESSAGE = 'Empty list' />
					<cfset dataout.RXRESULTCODE = 0/>
				</cfif>
				
			<cfelse>
				<cfset dataout.RXRESULTCODE = -3/>
				<cfset dataout.MESSAGE = 'No coupon found' />
				<cfreturn dataout />
			</cfif>

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -2 />
				<cfset dataout.MESSAGE = "#cfcatch.Message#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		
		<cfreturn dataout />
	</cffunction>

	<cffunction name="IncreaseCouponUsedTime" access="remote" hint="Increase Coupon Used Time">
		<cfargument name="inpCouponId" required="true"/>
		<cfargument name="inpUsedFor" required="true" hint="1 for recurring, 2 for upgrade plan"/>

		<cfset var dataout = {} />
		<cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset var updateCouponUsedTime = "" />

        <cftry>
        	<cfquery result="updateCouponUsedTime" datasource="#Session.DBSourceEBM#">
        		UPDATE
        			simplebilling.promotion_codes
        		<cfif arguments.inpUsedFor EQ 1>
	        		SET
	        			used_time_by_recurring_int = used_time_by_recurring_int + 1
	        	<cfelseif arguments.inpUsedFor EQ 2>
	        		SET
	        			used_time_by_upgrade_int = used_time_by_upgrade_int + 1
	        	</cfif>
        		WHERE
        			origin_id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCouponId#">
        		AND
        			promotion_id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCouponId#">
        	</cfquery>

        	<cfif updateCouponUsedTime.RECORDCOUNT GT 0>
        		<cfset dataout.RXRESULTCODE = 1 />
        		<cfset dataout.MESSAGE = 'Increase coupon used time successfully!'/>
        	<cfelse>
        		<cfset dataout.RXRESULTCODE = 0 />
        		<cfset dataout.MESSAGE = 'Increase coupon used time failed!'/>
        	</cfif>
        	<cfcatch>
                <cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.MESSAGE = "#cfcatch.Message#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
        </cftry>

        <cfreturn dataout />
	</cffunction>

	<!--- Insert Promotion CodeUsed Log --->
	<cffunction name="InsertPromotionCodeUsedLog" access="remote" output="false" hint="Insert Promotion Code Used Log">
		
		<cfargument name="inpCouponId" type="numeric" required="yes">
		<cfargument name="inpOriginCouponId" type="numeric" required="yes">
		<cfargument name="inpUserId" type="numeric" required="yes">
		<cfargument name="inpUsedFor" type="string" required="yes">

		<cfset var insertUsedLogPromoCode = {} />
		<cfset var insertUsedLogPromoCodeResult = {} />
		<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cftry>

			<cfquery name="insertUsedLogPromoCode" datasource="#Session.DBSourceEBM#" result="insertUsedLogPromoCodeResult">
		        INSERT INTO
		            simplebilling.log_used_promotion_codes
		            (
		            promotion_id_int,
		            promotion_origin_id_int,
		            UserId_int,
		            UsedFor_vch
		            )
		        VALUES 
		            (
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCouponId#">,
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpOriginCouponId#">,
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">,
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpUsedFor#">
		            )                                        
		    </cfquery> 

			<cfif insertUsedLogPromoCodeResult.RecordCount GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = "Insert promotion code used log successfully" />
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = "Insert promotion code used log failed" />
			</cfif>
			<cfcatch>
			      <cfset dataout.RXRESULTCODE = -2 />
			      <cfset dataout.MESSAGE = "#cfcatch.Message#" />
			      <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch> 
		</cftry>
	  
	  <cfreturn dataout />
	</cffunction>

	<cffunction name="DeactiveUserCoupon" access="remote" hint="Deactive user's coupon code">
		<cfargument name="inpUserId" required="true"/>

		<cfset var dataout = {} />
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var deactiveUserCoupon = "" />
		<cfset var updateResult = "" />

		<cftry>
			<cfquery name="deactiveUserCoupon" datasource="#Session.DBSourceEBM#" result="updateResult">
				UPDATE
					simplebilling.userpromotions
				SET
					PromotionStatus_int = 0
				WHERE
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
			</cfquery>

			<cfif updateResult.RECORDCOUNT GT 0>
				<cfset dataout.RXRESULTCODE = 1/>
				<cfset dataout.MESSAGE = "Deactive coupon code successfully"/>
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0/>
				<cfset dataout.MESSAGE = "No coupon found or deactive failed"/>
			</cfif>
			<cfcatch>
			      <cfset dataout.RXRESULTCODE = -2 />
			      <cfset dataout.MESSAGE = "#cfcatch.Message#" />
			      <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

</cfcomponent>