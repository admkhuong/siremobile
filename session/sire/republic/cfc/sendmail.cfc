<cfcomponent>
	<cfparam name="Session.DBSourceEBM" default="Bishop"/>

	<!--- <cfinclude template="../../../paths.cfm"> --->
	<cfinclude template="/public/sire/configs/paths.cfm">
	<cffunction name="sendmailtemplate" output="true">
		<cfargument name="to" type="string" required="true">
		<!---<cfargument name="from" required="true" default="">--->
		<cfargument name="type" required="true" default="html">
		<cfargument name="subject" type="string" required="true" default="No subject">
		<cfargument name="template" type="string" required="true">
		<cfargument name="data" type="string" default="">

		<cfset var catchContent = '' />

		<cftry>
			<cfmail to="#arguments.to#" subject="#arguments.subject#" type="#arguments.type#" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#"  port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
				<cfinclude template="#arguments.template#" >
			</cfmail>
		<cfcatch type="any">
			<cftry>
		    	<!--- Log bug to file /session/sire/logs/bugs/payment  --->
				<cfif !isDefined('variables._Response')>
					<cfset variables._PageContext = GetPageContext()>
					<!--- Coldfusion --->
					<!---<cfset variables._Response = variables._PageContext.getCFOutput()>--->
					<cfset variables._Response = variables._PageContext.getOut()>
				</cfif>
				<cfset variables._Response.clear()>

				<cfset catchContent = variables._Response.getString()>
				<cfset variables._Response.clear()>

		    	<cffile action="write" output="#catchContent#"
		    	file="#ExpandPath('/')#session/sire/logs/bugs/payment/securenet_email_error_#REReplace('#now()#', '\W', '_', 'all')#.txt">
		         <cfcatch type="any">
		        </cfcatch>
    		</cftry>
		</cfcatch>
		</cftry>

	</cffunction>

	<cffunction access="public" name="SendMailByStringContent" output="false" hint="Send email with formatted content">
		<cfargument name="inpTo" type="string" required="true">
		<cfargument name="inpSubject" type="string" required="true" default="No subject">
		<cfargument name="inpContent" type="string" default="">

		<cfset var catchContent = '' />

		<cftry>
			<cfmail to="#arguments.inpTo#" subject="#arguments.inpSubject#" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#"  port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
				#arguments.inpContent#
			</cfmail>
		<cfcatch type="any">
			<cftry>
		    	<!--- Log bug to file /session/sire/logs/bugs/payment  --->
				<cfif !isDefined('variables._Response')>
					<cfset variables._PageContext = GetPageContext()>
					<!--- Coldfusion --->
					<!---<cfset variables._Response = variables._PageContext.getCFOutput()>--->
					<cfset variables._Response = variables._PageContext.getOut()>
				</cfif>
				<cfset variables._Response.clear()>

		    	<cfdump var="#cfcatch#">
				<cfset catchContent = variables._Response.getString()>
				<cfset variables._Response.clear()>

		    	<cffile action="write" output="#catchContent#"
		    	file="#ExpandPath('/')#session/sire/logs/bugs/payment/securenet_email_error_#REReplace('#now()#', '\W', '_', 'all')#.txt">
		         <cfcatch type="any">
		        </cfcatch>
    		</cftry>
		</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>
