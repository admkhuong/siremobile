<cfset PublicPath = "/public/sire/pages" />
<cfset BrandShort = "SIRE" />
<cfset _UserCustomFields = ['First Name', 'Email Address', 'Date of Birth', 'Zip code']>
<cfset _FreePlanCredits = 25>
<cfset _SIREPhoneNumber = '(888) 747-4411'>

<cfparam name="Session.DBSourceEBM" default="bishop">
<cfparam name="Session.DBSourceREAD" default="bishop_read">

<cfif FINDNOCASE(CGI.SERVER_NAME,'siremobile.com') GT 0 OR FINDNOCASE(CGI.SERVER_NAME,'www.siremobile.com') GT 0 OR FINDNOCASE(CGI.SERVER_NAME,'cron.siremobile.com') GT 0 OR LCase(CGI.SERVER_NAME) EQ 'dev.siremobile.com' OR FINDNOCASE(CGI.SERVER_NAME,'api.siremobile.com') GT 0>

  	<cfset SupportEMailFromNoReply = "noreply@esiremobile.com" />
    <cfset SupportEMailFrom = "support@siremobile.com" />
    <cfset SupportEMailServer = "smtp.gmail.com" />
    <cfset SupportEMailUserName = "support@siremobile.com" />
    <cfset SupportEMailPassword = "SF927$tyir3" />
    <cfset SupportEMailPort = "465" />
    <cfset SupportEMailUseSSL = "true" />
    <cfset application.Env = true />
    <!--- API key short url google service - Production env --->
	<cfset ServerAPIKeyShortUrl = "AIzaSyB0c-sx7hvlu8j2UdScdK2vVIj4qQD8-Yg" />
	<cfset _MailChimpDefaultListId = 'd68bdee54f' />
	<cfset _MailChimpNewList = '' />
	<cfset EnvPrefix = 'PRO_'/>
	<cfset PaypalUserId = ""/>
	<cfset PaypalPassword = ""/>
	<cfset PaypalSignature = ""/>
<cfelse>
	<cfset SupportEMailFromNoReply = "noreply@siremobile.com" />
	<cfset SupportEMailFrom = "sirevn1@gmail.com" />
	<cfset SupportEMailServer = "smtp.gmail.com" />
	<cfset SupportEMailUserName = "sirevn1@gmail.com" />
	<cfset SupportEMailPassword = "set@2016" />
	<cfset SupportEMailPort = "465" />
	<cfset SupportEMailUseSSL = "true" />
	<cfset application.Env = false />
	<!--- API key short url google service - Dev env --->
	<cfset ServerAPIKeyShortUrl = "AIzaSyB0c-sx7hvlu8j2UdScdK2vVIj4qQD8-Yg" />
	<cfset _MailChimpDefaultListId = '785f35b9df' />
	<cfset _MailChimpNewList = '1fe9d279e8' />
	<cfset EnvPrefix = 'DEV_'/>
	<cfset PaypalUserId = "seta.sire-facilitator_api1.gmail.com"/>
	<cfset PaypalPassword = "M7BPY33WZXEBW2V9"/>
	<cfset PaypalSignature = "Av.9i2azchfYhjyEYnXe202q94pfA16phgE47mRygBB5IXn3YzHJ9lmV"/>
</cfif>

<cfset _LimitRecurringProcessNumber = 1>

<!---
	_SiteMaintenance = 0: Live
	_SiteMaintenance = 1: Comming Soon
	_SiteMaintenance = 2: Scheduled maintenance
--->

<cfset _APNS_SANDBOX = 0/>

<cfif _APNS_SANDBOX EQ 1>
	<cfset _CertPath = ExpandPath('/') & '/apns/' & 'SIRE_Development_Push_Certificates.p12'>
	<cfset _CertPass = 'sire@2016'>
<cfelse>
	<cfset _CertPath = ExpandPath('/') & '/apns/' & 'SIRE_Production_Push_Certificate.p12'>
	<cfset _CertPass = 'Sire@2016'>
</cfif>

<cfset _SiteMaintenance = 0/>
<cfset _ReferralSignUpLink = '/signup?rf='/>
<cfset _ReferralPlanPricingPath = '/plans-pricing?rf='/>

<!--- Setup max sms invite user can sent --->
<cfset _MaxSMSInvite = 5 />
<!--- Setup single phone number invite duration --->
<cfset _SameNumberInviteDuration = 30 />

<cfset _InvitationSubject = 'SIRE' />
<cfset _ToInviterEmailSubject = 'Congratulations from Sire!' />

<!---
<!--- QA SITE --->
<cfset _SMSInviteBatchId = 161321 />
--->

<!--- STAGING SITE --->
<cfset _SMSInviteBatchId = 161466 />

<cfset _ReferralLimit = 5 />

<!--- Credits to add for Inviter via Referral  --->
<cfset _PromotionCredits = 1000>

<cfset _DeactiveWarningBatch = 162303 />
<cfset _TroubleTicketBatch = 162463 />


<!--- ADMIN UPLOAD TOOLS --->
<cfset _UploadCompleteEmailSubject = '[Sire Mobile] Your upload is complete.' />

<!--- MAILCHIMP --->
<cfset _MailChimpApiKey = '6e1d6bf0d9573de68ffdac28545bb6e8-us15' />
<cfset _MailChimpApiUrl = 'https://us15.api.mailchimp.com/3.0/' />

<cfset _DEFAULT_SHORTCODE = 50/>
