<tfoot>
	<tr style="text-align: center" bgcolor="#274e60">
		<td style="font-family: Arial; font-size: 12px; padding-left: 15px; color: white" colspan="2">
		<p>For support requests, please call or email us at:<br>
		(888) 747-4411 | <a href="mailto:support@siremobile.com" style="text-decoration: none; color: white">support@siremobile.com</a> | Text "<a style="color: white; text-decoration: none;"><strong>support</strong></a>" to <a style="color: white; text-decoration: none;"><strong>39492</strong></a></p>
		</td>
	</tr>

	<tr style="text-align: center" bgcolor="#274e60">
		<td width="100%" style="padding-bottom: 15px" colspan="2">
	    	<a style="padding-right: 7px" href="https://www.facebook.com/siremobile/"><img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/facebook.png?v=1.0" alt="fb"/></a>
            <a style="padding-right: 7px" href="https://twitter.com/sire_mobile"><img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/twitter.png?v=1.0" alt="twt"></a>
            <a style="padding-right: 7px" href="https://www.youtube.com/channel/UCYqFX_Uy0MIY5i7gjgr_LXg" target="_blank" title="Youtube"><img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/youtube.png?v=1.0" alt="Youtube"/></a>
			<a style="padding-right: 7px" href="https://www.linkedin.com/company/12952559" target="_blank" title="Linkedin"><img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/linkedin.png?v=1.0" alt="LinkedIn"/></a>
            <a href="https://plus.google.com/116619152084831668774"><img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/g+.png?v=1.0" alt="g+"></a>
	    </td>
	</tr>
</tfoot>