<cfset var content	= {} />
<cfset content = deserializeJSON(arguments.data) />

<!DOCTYPE html>
<html>
<head>
	<style>
		table {
			font-family: arial, sans-serif;
			border-collapse: collapse;
			background-color: #f1f1c1;
			width: 100%;
		}

		td, th {
			border: 1px solid #dddddd;
			text-align: left;
			padding: 8px;
		}

		tr:nth-child(even) {
			background-color: #dddddd;
		}

		strong.red {
			color: red;
		}
	</style>
</head>
<cfoutput>
<body>
	<table>
		<thead>
			<tr>
				<th><h3 style="text-align: center;">#content.NOCName#</h3></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>#content.Details#</td>
			</tr>
		</tbody>
	</table>
</body>
</html>
</cfoutput>