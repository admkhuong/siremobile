<cfset var content	= {} />
<cfset content = deserializeJSON(arguments.data) />

<!DOCTYPE html>
<html>
<head>
	<style>
		table {
			font-family: arial, sans-serif;
			border-collapse: collapse;
			background-color: #f1f1c1;
			width: 100%;
		}

		td, th {
			border: 1px solid #dddddd;
			text-align: left;
			padding: 8px;
		}

		tr:nth-child(even) {
			background-color: #dddddd;
		}

		strong.red {
			color: red;
		}
	</style>
</head>
<cfoutput>
<body>
	<table>
		<thead>
			<tr>
				<th><h3 style="text-align: left;">Account ID</h3></th>
				<th><h3 style="text-align: left;">Email</h3></th>
				<th><h3 style="text-align: left;">Plan</h3></th>
				<th><h3 style="text-align: left;">Card Number</h3></th>
				<th><h3 style="text-align: left;">Payment Date</h3></th>
				<th><h3 style="text-align: left;">Amount($)</h3></th>
				<th><h3 style="text-align: left;">Payment Status</h3></th>
				<th><h3 style="text-align: left;">Reason</h3></th>
			</tr>
		</thead>
		<tbody>
			
				<cfloop array="#content['DATALIST']#" index="index">
					<tr>
						<td>#index.ACCOUNT_ID#</td>
						<td>#index.EMAIL#</td>
						<td>#index.PLAN#</td>
						<td>#index.CREDIT_CARD_NUMBER#</td>
						<td>#index.PAYMENT_DATE#</td>
						<td>#index.AMOUNT#</td>
						<td>#index.PAYMENTSTATUS#</td>
						<td>#index.REASON#</td>
					</tr>
				</cfloop>
		</tbody>
	</table>
</body>
</html>
</cfoutput>