<cfset var content	= {} />
<cfset content = deserializeJSON(arguments.data)>
<cfset content.ROOTURL_HTTP = "https://#CGI.SERVER_NAME#">

<cfif findNoCase('cron.siremobile.com', CGI.SERVER_NAME)>
	<cfset content.ROOTURL_HTTP = "https://www.siremobile.com"/>
</cfif>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Sire - your credits are running low</title>
</head>
<body style="font-family: Arial; font-size: 12px;">
	<table style="width: 600px; margin: 0 auto;" cellpadding="0" cellspacing="0">
		<cfinclude template="header.cfm">
		<tbody style="margin: 0; padding: 0;">
			<tr>
				<td colspan="2" style="margin: 0; padding: 0;">
					<div style="text-align: center;">
						<img src="http://siremobile.com/public/sire/images/emailtemplate/monthly-billing-email.jpg" />
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding: 0 30px;">
					<p style="font-size: 16px;">Hi <cfoutput>#content.USERNAME#</cfoutput>,</p>
					<p style="font-size: 16px;">
						It looks like you've been using Sire quite a bit lately. Great job! I'm always here for you if you have questions on how to make the most of our platform or if you're just confused about anything you've used.
					</p>
					<p style="font-size: 16px;">
						More importantly, I wanted to send you a friendly reminder that your free credits are running low.  We have upgrade options available for you to connect with your audience.  <a href="https://www.siremobile.com/plans-pricing">https://www.siremobile.com/plans-pricing</a>
					</p>
					<p style="font-size: 16px;">
						Don't forget, we are currently giving an extra 1,000 credits if you upgrade for Individual, Pro, or SMB plans.
					</p>
					<p style="font-size: 16px;">
						Shoot me an email at <a href="mailto:hello@siremobile.com">hello@siremobile.com</a> if you have any questions or concerns. I'll be more than happy to help you make the most of your SMS marketing campaigns.  I'm here to help!
					</p>
					<p style="font-size: 16px;">
						Thanks!
					</p>
					<p style="font-size: 16px;">
						Mindy<br/>
						Client Experience Guru
					</p>
				</td>
			</tr>
		</tbody>
		<cfinclude template="footer.cfm">
	</table>
</body>
</html>