<cfset var content	= {} />
<cfset content = deserializeJSON(arguments.data)>
<cfset content.ROOTURL_HTTP = "https://#CGI.SERVER_NAME#">

<!--- SET DOMAIN FOR CRON --->
<cfif LCase(CGI.SERVER_NAME) EQ 'cron.siremobile.com'>
	<cfset content.ROOTURL_HTTP = 'https://siremobile.com'>	
</cfif>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Recurring fail</title>
	</head>

	<body style="font-family: Arial; font-size: 12px;">
		<table style="width: 600px; margin: 0 auto;" cellpadding="0" cellspacing="0">
			<cfinclude template="header.cfm">
		  	<tbody style="margin: 0; padding: 0;">
		  		<tr>
		  			<td colspan="2" style="margin: 0; padding: 0;">
		  				<!---
		  				<div style="background: url('<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/authen-img.jpg'); height: 318px;"></div>
		  				--->
		  				<div style="text-align: center;">
		  					<img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/monthly-billing-email.jpg" />
		  				</div>
		  			</td>
		  		</tr>
		  		<tr>
		  			<td colspan="2" style="padding: 0 30px;">

						<p style="font-size: 16px;color: #5c5c5c;">Hello <cfoutput>#content.FirstName_vch#</cfoutput>!</p>

						<p style="font-size: 16px;color: #5c5c5c;"><cfoutput>We have encounted an error processing your latest monthly payment fee for #DateFormat(content.StartDate_dt, 'mm/dd/yyyy')# to #DateFormat(content.EndDate_dt, 'mm/dd/yyyy')#. Here is the account information we have on file:</cfoutput></p>


						<table style="width:100%">
							<tr>
								<td style="width:10%"></td>
								<td style="width:40%">
									<p style="font-size: 16px;color: #5c5c5c;">Plan:</p> 
									<p style="font-size: 16px;color: #5c5c5c;">Plan ID: </p>
									<p style="font-size: 16px;color: #5c5c5c;">Purchased Keyword:</p>
									<!---<p style="font-size: 16px;color: #5c5c5c;">Credit Card Number:</p>--->
									<p style="font-size: 16px;color: #5c5c5c;"><b>Due Date:</b></p>
									<p style="font-size: 16px;color: #5c5c5c;">Monthly fee:</p>
								</td>
								<td style="width:40%">
									<p style="font-size: 16px;color: #5c5c5c;"><cfoutput>#content.PlanName_vch#</cfoutput></p>
									<p style="font-size: 16px;color: #5c5c5c;"><cfoutput>#content.PlanId_int#</cfoutput></p>
									<p style="font-size: 16px;color: #5c5c5c;"><cfoutput>#content.BuyKeywordNumber_int#</cfoutput></p>
									<!---<p style="font-size: 16px;color: #5c5c5c;"><cfoutput>#content.cardNumber#</cfoutput></p>--->
									<p style="font-size: 16px;color: #5c5c5c;"><b><cfoutput>#DateFormat(content.StartDate_dt, 'mm/dd/yyyy')#</cfoutput></b></p>
									<p style="font-size: 16px;color: #5c5c5c;"><cfoutput>#content.BuyKeywordNumber_int# * #content.PriceKeywordAfter_dec# + #content.Amount_dec#</cfoutput>USD</p>

								</td>
								<td style="width:10%"></td>
							</tr>
							
						</table>
						
						<p style="font-size: 16px;color: #5c5c5c;"><cfoutput>Please click on this link <a href="#content.EXTENDLINK#">link</a> to pay this monthly free. Your account will be inactive if you don't pay before: #DateFormat(content.EndDate_dt, 'mm/dd/yyyy')#.</cfoutput></p>

						<p style="font-size: 16px;color: #5c5c5c;">Thanks,<br>
						SIRE Assistance Team
						</p>
		  			</td>
		  		</tr>	
		  	</tbody>
		  	<cfinclude template="footer.cfm">
		</table>
	</body>

</html>