<cfset var content	= {} />
<cfset content = DeserializeJSON(arguments.data)>
<cfset content.ROOTURL_HTTP = "https://#CGI.SERVER_NAME#">

<!--- SET DOMAIN FOR CRON --->
<cfif LCase(CGI.SERVER_NAME) EQ 'cron.siremobile.com'>
	<cfset content.ROOTURL_HTTP = 'https://siremobile.com'>	
</cfif>
<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfoById" returnvariable="userInfo">
	<cfinvokeargument name="inpUserId" value="#content.UserID#">
</cfinvoke>
<cfset userName = userInfo.USERNAME>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Your Master Coupon was assigned</title>
	</head>

	<body style="font-family: Arial; font-size: 12px;">
		<table style="width: 600px; margin: 0 auto;" cellpadding="0" cellspacing="0" id="payment-detail">
			<cfinclude template="header.cfm">
		  	<tbody style="margin: 0; padding: 0;">
		  		<tr>
		  			<td colspan="2" style="margin: 0; padding: 0;">
		  				<div style="text-align: center;">
		  					<img src="https://www.siremobile.com/public/sire/images/emailtemplate/monthly-billing-email.jpg" />
		  				</div>
		  			</td>
		  		</tr>
		  		<tr>
		  			<td colspan="2" style="padding: 0 30px;">

						<p style="font-size: 16px;color: #5c5c5c;">Dear <cfoutput >#userName#</cfoutput>,</p>
						
						<br>

						<p style="font-size: 18px;color: #74c37f; text-align:center; font-weight:bold">Your Master Coupon Code was assigned successfully!</p>
						<hr style="border: 0;border-bottom: 1px solid #74c37f;">

						<table style="width:100%">
							<tr>
								<td style="width:10%"></td>
								<td style="width:80%">
									<p style="font-size: 16px;color: #5c5c5c;">			
										Please follow below link for create your sub account:<br>							
										<cfoutput>
											<a href="#content.ROOTURL_HTTP#/signup?MasterCouponCode=#content.CouponCode#">Create Sub Account</a>
										</cfoutput>
									</p>
								</td>								
							</tr>
							
						</table>						
						
						<p style="font-size: 16px;color: #5c5c5c;">Thanks,<br>
						SIRE Assistance Team
						</p>
		  			</td>
		  		</tr>	
		  	</tbody>
		  	<cfinclude template="footer.cfm">
		</table>
	</body>

</html>

<style type="text/css">
	#payment-detail p {
		margin-top: 10px;
		margin-top: 10px;
	}
</style>