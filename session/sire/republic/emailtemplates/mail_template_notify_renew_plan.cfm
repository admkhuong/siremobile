<cfset var content	= {} />
<cfset content = DeserializeJSON(arguments.data)>
<cfset content.ROOTURL_HTTP = "https://#CGI.SERVER_NAME#">

<!--- SET DOMAIN FOR CRON --->
<cfif LCase(CGI.SERVER_NAME) EQ 'cron.siremobile.com'>
	<cfset content.ROOTURL_HTTP = 'https://siremobile.com'>	
</cfif>

<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfoById" returnvariable="userInfo">
	<cfinvokeargument name="inpUserId" value="#content.USERID#">
</cfinvoke>

<cfset userName = userInfo.USERNAME>
<cfset fullName = "#userInfo.USERNAME#" & " #userInfo.LASTNAME#">
<cfset userPhone = userInfo.FULLMFAPHONE>

<cfinvoke component="session.sire.models.cfc.billing"  method="GetBalance"  returnvariable="RetValBillingData">
	<cfinvokeargument name="inpUserId" value="#content.USERID#">
</cfinvoke>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>SIRE - Renew Plan Notification</title>
	</head>

	<body style="font-family: Arial; font-size: 12px;">
		<table style="width: 600px; margin: 0 auto;" cellpadding="0" cellspacing="0">
			<cfinclude template="header.cfm">
		  	<tbody style="margin: 0; padding: 0;">
		  		<tr>
		  			<td colspan="2" style="margin: 0; padding: 0;">
		  				<!---
		  				<div style="background: url('<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/authen-img.jpg'); height: 318px;"></div>
		  				--->
		  				<div style="text-align: center;">
		  					<img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/monthly-billing-email.jpg" />
		  				</div>
		  			</td>
		  		</tr>
		  		<tr>
		  			<td colspan="2" style="padding: 0 30px;">

						<p style="font-size: 16px;color: #5c5c5c;">Hi <cfoutput>#userName#</cfoutput>,</p>

						<p style="font-size: 16px;color: #5c5c5c;">
							Thank you for being with Sire and being part of our Beta program!<br><br>
							We are writting this email to inform you that your account plan will be renewed on <cfoutput>#content.ENDDATE#</cfoutput>.<br><br>
							Here is some information for your records:<br>
							Name: <cfoutput>#fullName#</cfoutput><br>
							Mobile Number: <cfoutput>#userPhone#</cfoutput><br>
							Plan Type: <cfoutput>#content.PLANTYPE#</cfoutput><br>
						</p>
						<p style="font-size: 16px; color: #5c5c5c;">Please contact us to <a href="mailto:support@siremobile.com">support@siremobile.com</a> if you have any question.</p>
						
						<p style="font-size: 16px;color: #5c5c5c;">Thanks,<br>
						SIRE Assistance Team
						</p>
		  			</td>
		  		</tr>	
		  	</tbody>
		  	<cfinclude template="footer.cfm">
		</table>
	</body>

</html>