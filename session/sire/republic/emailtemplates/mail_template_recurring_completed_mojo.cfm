<cfset var content	= {} />
<cfset content = DeserializeJSON(arguments.data)>
<cfset content.ROOTURL_HTTP = "https://#CGI.SERVER_NAME#">

<!--- SET DOMAIN FOR CRON --->
<cfif LCase(CGI.SERVER_NAME) EQ 'cron.siremobile.com'>
	<cfset content.ROOTURL_HTTP = 'https://siremobile.com'>	
</cfif>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Recurring Completed</title>
	</head>

	<body style="font-family: Arial; font-size: 12px;">
		<table style="width: 600px; margin: 0 auto;" cellpadding="0" cellspacing="0" id="payment-detail">
			<cfinclude template="header.cfm">
		  	<tbody style="margin: 0; padding: 0;">
		  		<tr>
		  			<td colspan="2" style="margin: 0; padding: 0;">
		  				<!---
		  				<div style="background: url('<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/authen-img.jpg'); height: 318px;"></div>
		  				--->
		  				<div style="text-align: center;">
		  					<img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/monthly-billing-email.jpg" />
		  				</div>
		  			</td>
		  		</tr>
		  		<tr>
		  			<td colspan="2" style="padding: 0 30px;">

						<p style="font-size: 16px;color: #5c5c5c;">Dear <cfoutput >#content.FullName#</cfoutput>,</p>
						
						<br>

						<p style="font-size: 18px;color: #74c37f; text-align:center; font-weight:bold">Payment Information</p>
						<hr style="border: 0;border-bottom: 1px solid #74c37f;">

						<table style="width:100%">
							<tr>
								<td style="width:10%"></td>
								<td style="width:30%">
									<p style="font-size: 16px;color: #5c5c5c;">Plan: </p>
								</td>
								<td style="width:50%">
									<p style="font-size: 16px;color: #5c5c5c;"><cfoutput>#content.PlanName#</cfoutput></p>
								</td>
							</tr>

							<tr>
								<td style="width:10%"></td>
								<td style="width:30%">
									<p style="font-size: 16px;color: #5c5c5c;">Number of keywords: </p>
								</td>
								<td style="width:50%">
									<p style="font-size: 16px;color: #5c5c5c;"><cfoutput>#content.numberOfKeyword#</cfoutput></p>
								</td>
							</tr>

							<tr>
								<td style="width:10%"></td>
								<td style="width:30%">
									<p style="font-size: 16px;color: #5c5c5c;">Number of credits:</p>
								</td>
								<td style="width:50%">
									<p style="font-size: 16px;color: #5c5c5c;"><cfoutput>#content.numberOfCredit#</cfoutput></p>

								</td>
							</tr>

							<tr>
								<td style="width:10%"></td>
								<td style="width:30%">
									<p style="font-size: 16px;color: #5c5c5c;">Amount:</p>
								</td>
								<td style="width:50%">
									<p style="font-size: 16px;color: #5c5c5c;"><cfoutput>$ #content.authorizedAmount#</cfoutput></p>
								</td>
							</tr>
							
						</table>
						
						<p style="font-size: 16px;color: #5c5c5c;">Thanks,<br>
						SIRE Assistance Team
						</p>
		  			</td>
		  		</tr>	
		  	</tbody>
		  	<cfinclude template="footer.cfm">
		</table>
	</body>

</html>

<style type="text/css">
	#payment-detail p {
		margin-top: 10px;
		margin-top: 10px;
	}
</style>