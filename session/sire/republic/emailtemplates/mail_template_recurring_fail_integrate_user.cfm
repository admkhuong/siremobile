<cfset var content	= {} />
<cfset content = deserializeJSON(arguments.data)>
<cfset content.ROOTURL_HTTP = "https://#CGI.SERVER_NAME#">

<!--- SET DOMAIN FOR CRON --->
<cfif LCase(CGI.SERVER_NAME) EQ 'cron.siremobile.com'>
	<cfset content.ROOTURL_HTTP = 'https://siremobile.com'>	
</cfif>
<cfif content.MailToMaster EQ 1>
	<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfoById" returnvariable="userInfo">
		<cfinvokeargument name="inpUserId" value="#content.HIGHERUSERID#">
	</cfinvoke>
<cfelse>
	<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfoById" returnvariable="userInfo">
		<cfinvokeargument name="inpUserId" value="#content.SubAccId#">
	</cfinvoke>
</cfif>
<!---
<cfset userName = userInfo.USERNAME&' '&userInfo.LASTNAME>
--->
<cfset userName = userInfo.USERNAME>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Recurring fail</title>
	</head>

	<body style="font-family: Arial; font-size: 12px;">
		<table style="width: 600px; margin: 0 auto;" cellpadding="0" cellspacing="0">
			<cfinclude template="header.cfm">
		  	<tbody style="margin: 0; padding: 0;">
		  		<tr>
		  			<td colspan="2" style="margin: 0; padding: 0;">
		  				<!---
		  				<div style="background: url('<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/authen-img.jpg'); height: 318px;"></div>
		  				--->
		  				<div style="text-align: center;">
		  					<img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/monthly-billing-email.jpg" />
		  				</div>
		  			</td>
		  		</tr>
		  		<tr>
		  			<td colspan="2" style="padding: 0 30px;">

						<p style="font-size: 16px;color: #5c5c5c;">Dear <cfoutput>#userName#</cfoutput>!</p>

						<p style="font-size: 16px;color: #5c5c5c;">
							<cfif content.MailToMaster EQ 0>
								<cfoutput>Did you receive our email on #DateFormat(content.EndDate_dt, 'mm/dd/yyyy')# regarding an issue with your account? Can you please log onto <a href="https://www.siremobile.com">www.siremobile.com</a> and update your account with a valid credit card to avoid any further interruption to your account or service.  If we do not obtain payment by #DateFormat(content.DownGradeDate_dt, 'mm/dd/yyyy')#, your account will be reset credit to 0.</cfoutput>
							<cfelse>
								<cfoutput>Did you receive our email on #DateFormat(content.EndDate_dt, 'mm/dd/yyyy')# regarding an issue with your sub account? Can you please log onto <a href="https://www.siremobile.com">www.siremobile.com</a> and update your account with a valid credit card to avoid any further interruption to your account or service.  If we do not obtain payment by #DateFormat(content.DownGradeDate_dt, 'mm/dd/yyyy')#, your sub account will be reset credit to 0.</cfoutput>
							</cfif>						
						</p>

						<p style="font-size: 16px;color: #5c5c5c;">
							<cfif content.MailToMaster EQ 0>
								Information of your account:
							<cfelse>
								Information of your sub account:
							</cfif>
						</p>


						<table style="width:100%">
							<tr>
								<td style="width:10%"></td>
								<td style="width:40%">
									<p style="font-size: 16px;color: #5c5c5c;">
										
										<cfif content.MailToMaster EQ 0>
											Your Account ID:
										<cfelse>
											Your Sub Account ID:
										</cfif>
									</p>
									<p style="font-size: 16px;color: #5c5c5c;">Plan:</p>
									<p style="font-size: 16px;color: #5c5c5c;"><b>Start Date:</b></p>
									<p style="font-size: 16px;color: #5c5c5c;"><b>End Date:</b></p>
								</td>
								<td style="width:40%">
									<p style="font-size: 16px;color: #5c5c5c;"><cfoutput>#content.SubAccId#</cfoutput></p>
									<p style="font-size: 16px;color: #5c5c5c;"><cfoutput>#content.PlanName_vch#</cfoutput></p>
									<p style="font-size: 16px;color: #5c5c5c;"><b><cfoutput>#DateFormat(content.StartDate_dt, 'mm/dd/yyyy')#</cfoutput></b></p>
									<p style="font-size: 16px;color: #5c5c5c;"><b><cfoutput>#DateFormat(content.EndDate_dt, 'mm/dd/yyyy')#</cfoutput></b></p>
								</td>
								<td style="width:10%"></td>
							</tr>
							
						</table>
						
						<p style="font-size: 16px;color: #5c5c5c;">Please contact us to support@siremobile.com if you have any question.</p>

						<p style="font-size: 16px;color: #5c5c5c;">Thanks,<br>
						SIRE Assistance Team
						</p>
		  			</td>
		  		</tr>	
		  	</tbody>
		  	<cfinclude template="footer.cfm">
		</table>
	</body>

</html>