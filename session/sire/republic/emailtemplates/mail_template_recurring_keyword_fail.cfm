<cfset var content	= {} />
<cfset content = DeserializeJSON(arguments.data)>
<cfset content.ROOTURL_HTTP = "https://#CGI.SERVER_NAME#">

<!--- SET DOMAIN FOR CRON --->
<cfif LCase(CGI.SERVER_NAME) EQ 'cron.siremobile.com'>
	<cfset content.ROOTURL_HTTP = 'https://siremobile.com'>	
</cfif>

<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfoById" returnvariable="userInfo">
	<cfinvokeargument name="inpUserId" value="#content.USERID#">
</cfinvoke>
<!---
<cfset userName = userInfo.USERNAME&' '&userInfo.LASTNAME>
--->
<cfset userName = userInfo.USERNAME>

<cfinvoke component="session.sire.models.cfc.billing"  method="GetBalance"  returnvariable="RetValBillingData">
	<cfinvokeargument name="inpUserId" value="#content.USERID#">
</cfinvoke>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Recurring Completed</title>
	</head>

	<body style="font-family: Arial; font-size: 12px;">
		<table style="width: 600px; margin: 0 auto;" cellpadding="0" cellspacing="0">
			<cfinclude template="header.cfm">
		  	<tbody style="margin: 0; padding: 0;">
		  		<tr>
		  			<td colspan="2" style="margin: 0; padding: 0;">
		  				<!---
		  				<div style="background: url('<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/authen-img.jpg'); height: 318px;"></div>
		  				--->
		  				<div style="text-align: center;">
		  					<img src="https://siremobile.com/public/sire/images/emailtemplate/monthly-billing-email.jpg" />
		  				</div>
		  			</td>
		  		</tr>
		  		<tr>
		  			<td colspan="2" style="padding: 0 30px;">

						<p style="font-size: 16px;color: #5c5c5c;">Dear <cfoutput>#userName#</cfoutput>,</p>

						<p style="font-size: 16px;text-align:left;color: #5c5c5c;">Did you receive our email on <cfoutput>#content.SENDDATE#</cfoutput> ragarding an issue with your account? Can you please log onto www.siremobile.com and update your account with a valid credit card to avoid any further interuption to your account or service. If we do not obtain payment by <cfoutput>#content.DISABLEDATE#</cfoutput>, we will disable your purchased keywords.</p>

						<p style="font-size: 16px;text-align: left;color: #5c5c5c;">Information of your keywords:</p>

						<p style="font-size: 16px;text-align: left;color: #5c5c5c;">
							Plan's limit: <cfoutput>#content.PLANKEYWORD#</cfoutput><br>
							Promotion: <cfoutput>#content.PROMOKEYWORD#</cfoutput><br>
							Purchased: <cfoutput>#content.PURCHASEDKEYWORD#</cfoutput><br>
							Active: <cfoutput>#content.ACTIVEKEYWORD#</cfoutput><br>
							Available: <cfoutput>#content.AVAILABLEKEYWORD#</cfoutput>
						</p>
						<p style="font-size: 16px; color: #5c5c5c;">Please contact us to <a href="mailto:support@siremobile.com">support@siremobile.com</a> if you have any question.</p>
						
						<p style="font-size: 16px;color: #5c5c5c;">Thanks,<br>
						SIRE Assistance Team
						</p>
						<br><br><br>
		  			</td>
		  		</tr>	
		  	</tbody>
		  	<cfinclude template="footer.cfm">
		</table>
	</body>

</html>