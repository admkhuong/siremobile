<!--- <cfset var content	= {} />
<cfset content = deserializeJSON(arguments.data)>
<cfset content.ROOTURL_HTTP = "https://#CGI.SERVER_NAME#">

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Welcome</title>
	</head>

	<body style="font-family: Arial; font-size: 12px;">
		<table style="width: 600px; margin: 0 auto;" cellpadding="0" cellspacing="0">
			<cfinclude template="header.cfm">
		  	<tbody style="margin: 0; padding: 0;">
		  		<tr>
		  			<td width="50%" style="margin: 0; padding: 0;">
		  				<div style="text-align: center;">
		  					<img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/welcome.jpg" alt="">
		  				</div>
		  				<br>
		  				<p style="font-size: 16px;">Hello <cfoutput>#content.USERNAME#</cfoutput>!</p>

						<p style="font-size: 16px;">Congratulations! You have successfully registered at SIRE.</p>

						<p style="font-size: 16px;">If you have any problems, please reply to this email to get assistance.</p>

						<p style="font-size: 16px;">Thanks,<br>
						SIRE Assistance Team
						</p>
		  			</td>
		  			<td style="text-align: right; margin: 0; padding: 0;">
		  				<img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/welcome-img.jpg" alt="">
		  			</td>
		  		</tr>
		  	</tbody>
		  	<cfinclude template="footer.cfm">
		</table>
	</body>

</html> --->

<cfset var content	= {} />
<cfset content = deserializeJSON(arguments.data)>
<cfset content.ROOTURL_HTTP = "https://#CGI.SERVER_NAME#">

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Welcome</title>
	</head>

	<body style="font-family: Arial; font-size: 16px">
		<table style="width: 570px; margin: 0 auto; color:#5c5c5c;" cellpadding="0" cellspacing="0" align="center">			
				<tr>
					<td>
						<table width="100%">
							<tr>
								<td width="100%" style="margin: 0; padding-left: 2%; font-size: 16px">
									<br><br>
									Hi <cfoutput>#content.USERNAME#</cfoutput>,<br>

									<p>Thanks for joining Sire Mobile! We’re excited to have you on board.</p>

									<p>You're probably aware of how our user-friendly templates can help your business grow.  What you may not know is that we offer the best customer support in the industry.  I know you have a lot going on so we set aside time for all our new clients to:</p>
									<ul style="list-style-type: square;">
											<li>Build your first campaign</li>
											<li>Create your marketing list</li>
											<li>Walk you through the interface</li>
											<li>Answer any questions you have</li>
										</ul>							
									<p>
										All you need to do is <a href="https://sire-mobile.appointlet.com/s/new-client-free-account-setup/sire-support">click here</a> and schedule a time to get started.  Together, we'll set up and customize your campaign, discuss time-saving tips, and answer any questions you have to ensure your campaign is a success. I promise it'll be <a href="https://sire-mobile.appointlet.com/s/new-client-free-account-setup/sire-support">time well spent</a>.
									</p>									
									<p>If you're feeling ambitious, you can <a href="https://www.siremobile.com/">log in here</a> to get started on your own.  I'm only an email away if you have any questions, <a href="mailto:hello@siremobile.com">hello@siremobile.com</a>.  Happy texting and I look forward to speaking with you soon!</p>								
									<br>
									<p>Sincerely,</p>
									<p>Mindy<br><br>
									Customer Experience Manager
									</p>
								</td>
							</tr>
						</table>
					</td>
				</tr>					
		</table>
	</body>

</html>

