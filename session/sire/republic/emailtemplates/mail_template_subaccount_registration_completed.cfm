<cfset var content	= {} />
<cfset content = DeserializeJSON(arguments.data)>
<cfset content.ROOTURL_HTTP = "https://#CGI.SERVER_NAME#">

<!--- SET DOMAIN FOR CRON --->
<cfif LCase(CGI.SERVER_NAME) EQ 'cron.siremobile.com'>
	<cfset content.ROOTURL_HTTP = 'https://siremobile.com'>	
</cfif>
<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfoById" returnvariable="userInfo">
	<cfinvokeargument name="inpUserId" value="#content.MasterUserID#">
</cfinvoke>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>[SIRE] New account just signed up!</title>
	</head>

	<body style="font-family: Arial; font-size: 12px;">
		<table style="width: 600px; margin: 0 auto;" cellpadding="0" cellspacing="0" id="payment-detail">
			<cfinclude template="header.cfm">
		  	<tbody style="margin: 0; padding: 0;">
		  		<tr>
		  			<td colspan="2" style="margin: 0; padding: 0;">
		  				<div style="text-align: center;">
		  					<img src="https://www.siremobile.com/public/sire/images/emailtemplate/bot-banner.jpg" />
		  				</div>
		  			</td>
		  		</tr>
		  		<tr>
		  			<td colspan="2" style="padding: 0 30px;">

						<p style="font-size: 16px;color: #5c5c5c;">Hi <cfoutput >#content.SubUserName# ,</cfoutput></p>
						
						<br>						

						<table style="width:100%">
							<tr>								
								<td style="width:99%">
									<p style="font-size: 16px;color: #5c5c5c;">			
										<cfoutput>
										Good news.  #content.SubFullName# just signed up for a Sire account using your <a href="#content.ROOTURL_HTTP#/signup?MasterCouponCode=#content.MasterCouponCode#">#content.MasterCouponCode#</a> code. 
										High five! 
										You can click <a href="#content.ROOTURL_HTTP#/session/sire/pages/dashboard">here</a> to manage your list of sub accounts.  <br>							
																					
										</cfoutput>
									</p>
								</td>								
							</tr>
							
						</table>						
						
						<p style="font-size: 16px;color: #5c5c5c;">Thanks,<br>
						Sire Support.
						</p>
		  			</td>
		  		</tr>	
		  	</tbody>
		  	<cfinclude template="footer.cfm">
		</table>
	</body>

</html>

<style type="text/css">
	#payment-detail p {
		margin-top: 10px;
		margin-top: 10px;
	}
</style>