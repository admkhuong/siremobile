<cfset var content	= {} />
<cfset content = deserializeJSON(arguments.data)>

<cfset content.ROOTURL_HTTP = "https://#CGI.SERVER_NAME#">
<!DOCTYPE html>
<html>	
	<body style="font-family: Arial; font-size: 12px;">
		<table style="width: 600px; margin: 0 auto;" cellpadding="0" cellspacing="0">			
		  	<tbody style="margin: 0; padding: 0;">		  		
		  		<tr>
		  			<td colspan="2" style="padding: 0 30px;">
						<p style="font-size: 16px; color: #5c5c5c;">Hi <cfoutput>support team,</cfoutput></p>

						<p style="font-size: 16px; color: #5c5c5c;"><cfoutput>#content.FIRSTNAME#</cfoutput> has been active White Glove. The Sire account information as below:</p>
						<p>
							<ul style="list-style-type: square;">
								<li>User Name: <cfoutput>#content.USERNAME#</cfoutput></li>
								<li>UserId: <cfoutput>#content.USERID#</cfoutput></li>								
								<li>Phone Number: <cfoutput>#content.MFAPHONE#</cfoutput></li>
								<li>Email: <cfoutput>#content.FULLEMAIL#</cfoutput></li>								
							</ul>							
						<p>
						<p style="font-size: 16px; color: #5c5c5c;">
							Thanks,<br>
							SIRE Assistance Team
						</p>
		  			</td>
		  		</tr>	
		  	</tbody>		  	
		</table>
	</body>

</html>