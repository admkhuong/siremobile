<cfset variables._title = 'Device not Recognized - Sire'>
<cfset variables.body_class = 'device_reg_page'>
<cfset hideFixedSignupForm = '1'>

<cfinvoke component="session.sire.republic.cfc.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/republic/js/verify_code.js">
</cfinvoke>

<cfinvoke component="session.sire.republic.cfc.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/republic/js/fastclick.js">
</cfinvoke>

<cfinvoke component="session.sire.republic.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke>

<cfinvoke component="session.sire.republic.cfc.userstools" method="checkMFACodeExits" returnvariable="checkMFA"></cfinvoke>

<cfinvoke component="session.sire.republic.cfc.userstools" method="checkLogin" returnvariable="checkLogin"></cfinvoke>

<!--- <cfset userInfo.MFACONTACTTYPE = 0 /> --->

<!---<cfif checkMFA.RESULT EQ 'SUCCESS' OR userInfo.RESULT NEQ 'SUCCESS' OR checkLogin EQ True>--->
<cfif checkMFA.RESULT EQ 'SUCCESS' AND userInfo.RESULT EQ 'SUCCESS' AND checkLogin EQ True>

<cflocation url='/session/sire/pages/campaign-manage' addtoken="false">
</cfif>

<cfset check_user_security_question = false>

<cfif userInfo.SQUESTIONENABLE EQ 1>

	<cfinvoke component="session.sire.models.cfc.myaccountinfo" method="GetUserListQuestion" returnvariable="userListQuestion">
		<cfinvokeargument name="detail_question" value=1>
	</cfinvoke>

	<cfif isArray(userListQuestion.LISTQUESTION) >
		<cfset check_user_security_question = true>
	</cfif>

</cfif>

<section class="main-wrapper device-register">
	<!--- DEVICE NOT REG --->
	<div class="container-fluid" id="device_not_reg_wrapper" <cfif userInfo.MFACONTACTTYPE GT 1>style="display:none;"</cfif>>
		<div class="row">
			<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-offset-2 signup_title">
				<p class="row device_reg_title">
					<span class="text_menu"> SIGN IN  > </span> <span class="current-title"><strong> DEVICE NOT RECOGNIZED </strong> </span>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-10 col-sm-10 col-lg-8 col-xs-offset-1 col-sm-offset-1 col-lg-offset-2 wraper_signup_form">
				<div class="row">
					<div class="signup_form">
						<h4><strong>This device/computer has not been authorized yet. Please validate the device by selecting an authentication method below.</strong></h4>
						<form method="post" action="##" name="device_not_reg_form" id="device_not_reg_form" autocomplete="off">
							<br/>
							<p class="send_code_txt">Send a verification code to my:</p>
							<div class="form_device_not_reg_control">
								<div class="form-group form-group-select-opt">
                                             <cfif StructKeyExists(userInfo, "MFATYPE")>
     									<cfif userInfo.MFATYPE EQ 2>
     									  	<div class="radio">
     										  <label>
     										    <input type="radio" class="radio_device_reg" name="sendType" id="optionsRadios1" value="3" checked>
     										    Mobile phone ending in ****<cfoutput>#right(userInfo.MFAPHONE, 4)#</cfoutput>
     										  </label>
     										</div>
     										<div class="radio">
     										  <label>
     										    <input type="radio" class="radio_device_reg" name="sendType" id="optionsRadios2" value="2">
     										    E-mail beginning in <cfoutput>#left(userInfo.FULLEMAIL,4)#</cfoutput>****
     										  </label>
     										</div>
     									<cfelse>
     										<div class="radio">
     										  <label>
     										    <input type="radio" class="radio_device_reg" name="sendType" id="optionsRadios1" value="3" checked>
     										    Mobile phone ending in ****<cfoutput>#right(userInfo.MFAPHONE, 4)#</cfoutput>
     										  </label>
     										</div>
     									</cfif>
                                             <cfelse>
                                                  <cflocation url='/public/sire/pages/index.cfm' addtoken="false">
                                             </cfif>
								</div>
								<div class="form-group">
									<div class="checkbox">
										<!--- No longer need a "default" option --->
									  	<input type="hidden" class="checkbox_device_reg" name="makeDefault" value="1">
									</div>
								</div>
							</div>
							<div class="form-group">
								<button type="button" class="btn btn-success btn-success-custom btn-custom_size triggerfocus" id="btn_device_not_reg">Continue</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--- VERIFICATION CODE --->
	<div class="container-fluid" id="verification_code" style="display:none">
		<div class="row">
			<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-lg-offset-2 signup_title">
				<p class="row device_reg_title">
					<span class="text_menu"> SIGN IN  > </span> <span class="text_menu"> DEVICE NOT RECOGNIZED > </span> <span class="current-title"> VERIFICATION CODE SENT</span>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-10 col-sm-10 col-lg-8 col-xs-offset-1 col-sm-offset-1 col-lg-offset-2 wraper_signup_form">
				<div class="row">
					<div class="signup_form" style="margin: 20px 0;">
						<h4><strong>
							A verification code has been sent to your
							<span class="hidden your-device">device ending in ****<cfoutput>#Right(userInfo.MFAPHONE,4)#</cfoutput>.</span>
							<span class="hidden your-email">E-mail beginning in <cfoutput>#left(userInfo.FULLEMAIL,4)#</cfoutput>****.</span>
						</strong></h4>
						<br><br>
						<form class="col-sm-10 col-xs-12" method="post" action="##" name="verification_code_form" id="verification_code_form" autocomplete="off">
							<div class="form-group">
								<label for="fname">Please enter the verification code you received below:</label>
							    <input type="text" class="form-control validate[required,custom[integer]]" id="inpMFACode" name="inpMFACode" maxlength="6" placeholder="" data-prompt-position="topLeft:100" autofocus />
							</div>
							<br><br>
							<div class="form-group btn-group-verification_code">
								<button type="button" class="btn btn-primary btn-back-custom btn-custom_size" id="btn_back_to_dev_not_reg">Back</button>
								<a href="/signout" class="btn btn-primary btn-back-custom btn-custom_size" id="btn_verification_code_cancel" style="display:none">Cancel</a>
								<button type="button" class="btn btn-success btn-success-custom btn-custom_size" id="btn_verification_code">Continue</button>

								<a data-toggle="modal" href="##" data-target="#InfoModalMFAFail" class="hidden your-device" style=""><h4>Why can't I get my Code via SMS?</h4></a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!---INVALID CODE --->
	<div class="container-fluid" id="invalid_verification_code" style="display:none">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1 col-lg-offset-2 signup_title">
				<p class="device_reg_title">
					<span class="text_menu"> SIGN IN  > </span> <span class="text_menu"> DEVICE NOT RECOGNIZED > </span> <span class="current-title"> INVALID VERIFICATION CODE </span>
				</p>
			</div>
		</div>
		<div class="row" id="invalid-body">
			<div class="col-xs-12 col-sm-10 col-lg-7 col-sm-offset-1 col-lg-offset-2 wraper_signup_form">
				<div class="row">
					<div class="signup_form">
						<h4 class="verification_title">Sorry, the verification code you entered was not confirmed.</h4>
						<cfif check_user_security_question>
							<p class="verification_title">Please click Back to try again or select Security Questions to Continue.</p>
						<cfelse>
							<p class="verification_title">Please click Back to try again </p>
						</cfif>

						<br><br>
						<div class="form-group">
							<button type="button" class="btn btn-primary btn-back-custom btn-custom_size" id="btn_back_to_verification_code">Back</button>
							<cfif check_user_security_question>
								<button type="button" class="btn btn-success btn-success-custom btn-custom_size" id="btn_answer_question">Answer security questions</button>
							<cfelse>
								<a href="/signout?DALD=1" class="btn btn-success btn-success-custom btn-custom_size" id="btn_device_reg_cancel">Cancel</a>
							</cfif>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

    <!--- Verify Code Success --->
    <div class="container-fluid" id="device_registered" style="display:none" >
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 col-lg-offset-2 signup_title">
                <p class="device_reg_title">
                    <span class="text_menu"> SIGN IN  > </span> <span> DEVICE RECOGNIZED </span>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-10 col-lg-7 col-sm-offset-1 col-lg-offset-2 wraper_signup_form">
                <div class="row">
                    <div class="col-sm-11 col-xs-11 signup_form">
                       <h4><strong>Thank you! The code you entered has been verified and the device has been authenticated.</strong></h4>
                        <br><br>
                        <div class="form-group">
                        	<cfif structKeyExists(url, "redirect")>
                        			<cfset redirectUrl = url.redirect>
								<cfelseif structKeyExists(url, "invite")>
									<cfset redirectUrl = '/session/sire/pages/invite-friend'>
								<cfelse>
									<cfset redirectUrl = '/session/sire/pages/dashboard.cfm?action=checkUserCampaign'>
                        	</cfif>

                            <a href="<cfoutput>#redirectUrl#</cfoutput>" class="btn btn-success btn-success-custom btn-custom_size">Continue</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--- Security Question --->
    <cfif check_user_security_question>
    	<div class="container-fluid" id="answer_security_question" style="display:none">
		    <div class="row">
		        <div class="col-sm-10 col-sm-offset-1 col-lg-offset-2 signup_title">
		            <p class="device_reg_title">
		                <span class="text_menu"> SIGN IN  > </span> <span class="text-uppercase"> Answer Security Questions </span>
		            </p>
		        </div>
		    </div>
		    <div class="row">
		        <div class="col-xs-12 col-sm-10 col-lg-7 col-sm-offset-1 col-lg-offset-2 wraper_signup_form">
		            <div class="row">
		                <div class="col-sm-11 col-xs-11 signup_form">
		                   <h4><strong>We value your security. Please answer the following 3 security questions to access your account.</strong></h4>
		                    <br><br>
		                    <form class="form-horizontal" method="post" action="##" name="answer_security_question_form" id="answer_security_question_form" autocomplete="off">
		                    	<div class="question_list">
		                			<cfset num = 1>
		                			<cfloop array="#userListQuestion.LISTQUESTION#" index="question_item">
										 <div class="form-group">
										    <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Question <cfoutput>#num#</cfoutput> </label>
										    <div class="col-xs-8 col-sm-9 col-lg-10">
										     	<h4><strong><em> <cfoutput>#question_item.QUESTION#</cfoutput></strong></em></h4>
										    </div>
										</div>
										<div class="form-group">
										    <label for="input" class="col-xs-4 col-sm-3 col-lg-2 control-label">Your Answer</label>
										    <div class="col-xs-8 col-sm-9 col-lg-10">
										     	<input type="text" class="form-control validate[required] input_answer" maxlength="255" name="answer_<cfoutput>#question_item.QUESTIONID#</cfoutput>">
										    </div>
										</div>
										<cfset num++ >
									</cfloop>
								</div>

		                        <div class="form-group btn-group-answer-security-question">
		                        	<div class="col-xs-12">
			                        	<a href="/signout?DALD=1" class="btn btn-primary btn-back-custom btn-custom_size" >Cancel</a>
			                            <button class="btn btn-success btn-success-custom btn-custom_size" id="btn_verify_security_question">Continue</button>
			                        </div>
		                        </div>
		                    </form>
		                </div>
		            </div>
		        </div>
		    </div>
    	</div>
    </cfif>

</section>


<!-- Modal -->
<div id="InfoModalMFAFail" data-backdrop="false" tabindex="-1" class="modal fade" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="hidden" id="ErrorSendSMSMFA">Unable to send SMS to provided Mobile phone ending in ****<cfoutput>#right(userInfo.MFAPHONE, 4)#</cfoutput></h4>
      </div>
      <div class="modal-body">

			<h2 class="modal-title">Why can't I get my Code via SMS?</h2>


	   		<h4></h4>
	   		<p>Did you provide an mobile phone number where you can receive SMS? Stone-age land lines won't work here.</p>

	   		<p>How long has it been? Most carriers will deliver in a couple of seconds. Some of the smaller carriers take up to a minute to deliver the SMS.</p>

		    <p>Is your phone able to send and receive to Short Codes? A small percentage (less than 3% in the US) of phone numbers have Short Codes blocked on their phone plans. To test your phone send the <b>Keyword</b> "HELP" to <b>Short Code</b> 39492 - You should get back Sire's default help message.</p>

		    <p><i>If your phone can still <b>NOT</b> send and receive SMS messages to and from Short Codes, then you need to call your phone service provider and tell them you need Short codes turned on.</i></p>

			<h5 style="margin-top:1.5rem;">Notes:</h5>

			<p>We're sorry but Google Voice phone numbers are not supported for MFA validation. Please contact us at support@siremobile.com for alternative verification options.</p>
			<p>The MFA codes are only good in the US. If you are using this outside the US then you need to talk to our <a href="contact-us">support</a> about localized versions for your country.</p>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- Modal Block MFA -->
<div id="InfoModalMFABlock" data-backdrop-limit="1" tabindex="-1" class="modal fade" role="dialog" aria-hidden="true" data-focus-on="input:first" style="display: none;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 id="BlockSendSMSMFA">WARNING ACCOUNT</h4>
      </div>
      <div class="modal-body">
	  	Your IP address has been blocked.<br>Please contact support if need help accessing this account.
	 </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>

  </div>
</div>



<cfoutput>
<script type="text/javascript">
	var check_default = #userInfo.MFACONTACTTYPE#;
</script>
</cfoutput>

<cfinclude template="/session/sire/views/layouts/device-register-layout.cfm">
