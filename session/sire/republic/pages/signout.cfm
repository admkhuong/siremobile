<!---
If also don't want the user to be automatically logged
in again, so remove the client cookies.
--->

<!--- De-Authorize Local Device--->
<cfparam name="DALD" default="0">
<cfparam name="userId" default="0">
<cfparam name="redirectUrl" default="">

<cfif structKeyExists(Session, "USERID") AND Session.USERID GT 0 >
	<cfset userId = Session.USERID>
</cfif>

<cfif DALD GT 0 >
    <cfcookie name="MFAEBM#userId#" value="" expires="now" />
</cfif>

<cfset session.setMaxInactiveInterval( javaCast( "long", 1 ) ) />

<cfcookie name="RememberMe" value="" expires="now" />
<cfcookie name="CFID" value="" expires="NOW" />
<cfcookie name="CFTOKEN" value="" expires="NOW" />

<!--- <cfset OnSessionEnd(session) > --->
<cfset StructClear(session)>

<cfset Session.USERID = "">
<cfset Session.loggedIn = 0>

<cflogout />
<cfif redirectUrl NEQ "">
    <cflocation url="#redirectUrl#" addtoken="false">
<cfelse>
    <cflocation url="/" addtoken="false">
</cfif>
