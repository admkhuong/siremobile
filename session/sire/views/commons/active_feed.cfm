<cfparam name="url.id" default="0">
<cfparam name="url.af_start_date" default="">
<cfparam name="url.af_end_date" default="">
<cfparam name="url.af_phone_number" default="">
<cfparam name="url.af_batch_id" default="0">
<cfparam name="url.page" default="1">

<cfif !isNumeric(url.af_batch_id) OR url.af_batch_id LT 1>
	<cfset url.af_batch_id = 0>
</cfif>
<cfif !isNumeric(url.id) OR url.id LT 1>
	<cfif url.af_batch_id GT 0>
		<cfset url.id = url.af_batch_id>
	<cfelse>
		<cfset url.id = 0>
	</cfif>
</cfif>

<cfset url.af_phone_number = trim(url.af_phone_number)>
<cfset af_sd_valid = IsDate(url.af_start_date)>
<cfset af_ed_valid = IsDate(url.af_end_date)>
<cfquery name="campaignBatchIds" datasource="#Session.DBSourceREAD#">
	SELECT BatchId_bi FROM simpleobjects.batch WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">; 
</cfquery>

<cfif campaignBatchIds.RecordCount GT 0>
	<cfset batchIds = valueList(campaignBatchIds.BatchId_bi,',')>
<cfelse>
	<cfset batchIds = '-1'>
</cfif>

<!---<cfquery name="activeFeeds" datasource="#Session.DBSourceREAD#">
	SELECT 
    	BatchId_bi, 
        ContactString_vch, 
		IF(ISNULL(OptOut_dt) OR OptIn_dt < OptOut_dt, OptIn_dt, OptOut_dt) AS dt, 
		IF(ISNULL(OptOut_dt) OR OptIn_dt < OptOut_dt, 1, 2) AS log_type 
	FROM 
    	simplelists.optinout 
	<cfif url.id GT 0>
		WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.id#">
	<cfelse>
		WHERE BatchId_bi IN (#batchIds#)
	</cfif>
		AND LENGTH(ContactString_vch) < 14
	ORDER BY 
    	dt DESC, 
        OptId_int DESC
	LIMIT 10
</cfquery>--->
<cfquery name="allactiveFeeds" datasource="#Session.DBSourceREAD#">
 SELECT SUM(TOTAL) AS TOTAL FROM (
  (
  SELECT
   COUNT(OT.OptId_int) AS TOTAL
  FROM 
      simplelists.optinout OT
     INNER JOIN
      simplelists.contactstring CS
  ON
   OT.ContactString_vch = CS.ContactString_vch
  INNER JOIN
   simplelists.contactlist CL
  ON
   CL.ContactId_bi = CS.ContactId_bi
  AND
   CL.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
      
  <cfif url.id GT 0>
   WHERE OT.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.id#">
  <cfelse>
   WHERE (OT.BatchId_bi IN (#batchIds#) OR OT.OptOutSource_vch = 'MO Stop Request')
  </cfif>
  AND LENGTH(OT.ContactString_vch) < 14
  <cfif url.af_phone_number NEQ "">
   AND OT.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#url.af_phone_number#%">
  </cfif>

  <cfif af_sd_valid OR af_ed_valid>
   <cfif af_sd_valid>
    AND DATE(IF(ISNULL(OT.OptOut_dt) OR OT.OptIn_dt < OT.OptOut_dt, OT.OptIn_dt, OT.OptOut_dt)) >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#url.af_start_date#"> 
   </cfif>
   <cfif af_ed_valid>
    <cfif af_sd_valid>AND</cfif>
    DATE(IF(ISNULL(OT.OptOut_dt) OR OT.OptIn_dt < OT.OptOut_dt, OT.OptIn_dt, OT.OptOut_dt)) <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#url.af_end_date#"> 
   </cfif>
  </cfif>
  )
  
  UNION
  
  (
  SELECT
   COUNT(IREResultsId_bi) AS TOTAL
  FROM 
      simplexresults.ireresults
  <cfif url.id GT 0>
   WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.id#">
  <cfelse>
   WHERE BatchId_bi IN (#batchIds#)
  </cfif>
  AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
  AND IREType_int IN (1,2)
  AND LENGTH(ContactString_vch) < 14
  <cfif url.af_phone_number NEQ "">
   AND ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#url.af_phone_number#%">
  </cfif>
  <cfif af_sd_valid OR af_ed_valid>
   <cfif af_sd_valid>
    AND DATE(Created_dt) >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#url.af_start_date#"> 
   </cfif>
   <cfif af_ed_valid>
    <cfif af_sd_valid>AND</cfif>
    DATE(Created_dt) <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#url.af_end_date#"> 
   </cfif>
  </cfif>
  )
  <!---
  <cfif url.af_batch_id EQ 0>
  UNION 
  (
   SELECT CT.CPPX_trackingId_int AS Id_bi, 
    0 AS BatchId_bi, 
    CT.ContactString_vch, 
    CT.Created_dt AS dt, 
    999 AS sms_type, 
    CD.cppxName_vch AS sms, 
    3 AS log_type 
   FROM simplexresults.cppx_tracking CT INNER JOIN simplelists.cppx_data CD ON CT.CPPId_int = CD.ccpxDataId_int
   WHERE CT.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
   AND LENGTH(CT.ContactString_vch) < 14
   <cfif url.af_phone_number NEQ "">
    AND CT.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#url.af_phone_number#%">
   </cfif>
  )
  </cfif>
  --->
 ) AS ActiveFeed
</cfquery>

<!--- Limit records in one activity feed page --->
<cfset limit = 10>
<!--- <cfdump var="#allactiveFeeds#"> --->
<cfif allactiveFeeds.TOTAL % limit EQ 0>
	<cfset allpages = allactiveFeeds.TOTAL \ limit>
<cfelse>
	<cfset allpages = allactiveFeeds.TOTAL \ limit + 1>
</cfif>

<cfif url.page LTE 0>
	<cfset url.page = 1>

<cfelseif url.page GT allpages>
	<cfset url.page = allpages>

<cfelseif url.page EQ 1>
	<cfset start = 0>

<cfelseif url.page NEQ 1>
	<cfset start = (url.page-1) * limit>
</cfif>


<cfquery name="activeFeeds" datasource="#Session.DBSourceREAD#">
 SELECT Id_bi,BatchId_bi, ContactString_vch, dt, sms_type, sms, log_type FROM (
  (
  SELECT
   OT.OptId_int AS Id_bi,
      OT.BatchId_bi, 
         OT.ContactString_vch, 
   IF(ISNULL(OT.OptOut_dt) OR OT.OptIn_dt < OT.OptOut_dt, OT.OptIn_dt, OT.OptOut_dt) AS dt,
   0 AS sms_type,
   '' AS sms,
   IF(ISNULL(OT.OptOut_dt) OR OT.OptIn_dt < OT.OptOut_dt, 1, 2) AS log_type 
  FROM 
      simplelists.optinout OT INNER JOIN simplelists.contactstring CS ON OT.ContactString_vch = CS.ContactString_vch
   INNER JOIN simplelists.contactlist CL ON CL.ContactId_bi = CS.ContactId_bi AND CL.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
      
  <cfif url.id GT 0>
   WHERE OT.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.id#">
  <cfelse>
   WHERE (OT.BatchId_bi IN (#batchIds#) OR OT.OptOutSource_vch = 'MO Stop Request')
  </cfif>
   AND LENGTH(OT.ContactString_vch) < 14
   <cfif url.af_phone_number NEQ "">
    AND OT.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#url.af_phone_number#%">
   </cfif>
  <cfif af_sd_valid OR af_ed_valid>
   <cfif af_sd_valid>
    AND DATE(IF(ISNULL(OT.OptOut_dt) OR OT.OptIn_dt < OT.OptOut_dt, OT.OptIn_dt, OT.OptOut_dt)) >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#url.af_start_date#"> 
   </cfif>
   <cfif af_ed_valid>
    <cfif af_sd_valid>AND</cfif>
    DATE(IF(ISNULL(OT.OptOut_dt) OR OT.OptIn_dt < OT.OptOut_dt, OT.OptIn_dt, OT.OptOut_dt)) <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#url.af_end_date#"> 
   </cfif>
  </cfif>
   ORDER BY 
       dt DESC, 
          Id_bi DESC
      <cfif allactiveFeeds.TOTAL GT 0>
       LIMIT
        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.page*limit#">
      </cfif>
  )
  
  UNION
  
  (
  SELECT
   IREResultsId_bi AS Id_bi,
      BatchId_bi, 
         ContactString_vch, 
   Created_dt AS dt,
   IREType_int AS sms_type,
   Response_vch AS sms,
   0 AS log_type 
  FROM 
      simplexresults.ireresults
  <cfif url.id GT 0>
   WHERE BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.id#">
  <cfelse>
   WHERE BatchId_bi IN (#batchIds#)
  </cfif>
   AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
   AND IREType_int IN (1,2)
   AND LENGTH(ContactString_vch) < 14
   <cfif url.af_phone_number NEQ "">
    AND ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#url.af_phone_number#%">
   </cfif>
  <cfif af_sd_valid OR af_ed_valid>
   <cfif af_sd_valid>
    AND DATE(Created_dt) >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#url.af_start_date#"> 
   </cfif>
   <cfif af_ed_valid>
    <cfif af_sd_valid>AND</cfif>
    DATE(Created_dt) <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#url.af_end_date#"> 
   </cfif>
  </cfif>
  ORDER BY 
      dt DESC, 
         Id_bi DESC
     <cfif allactiveFeeds.TOTAL GT 0>
      LIMIT
       <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.page*limit#">
     </cfif>
  )
  <!---
  <cfif url.af_batch_id EQ 0>
  UNION 
  (
   SELECT CT.CPPX_trackingId_int AS Id_bi, 
    0 AS BatchId_bi, 
    CT.ContactString_vch, 
    CT.Created_dt AS dt, 
    999 AS sms_type, 
    CD.cppxName_vch AS sms, 
    3 AS log_type 
   FROM simplexresults.cppx_tracking CT INNER JOIN simplelists.cppx_data CD ON CT.CPPId_int = CD.ccpxDataId_int
   WHERE CT.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
   AND LENGTH(CT.ContactString_vch) < 14
   <cfif url.af_phone_number NEQ "">
    AND CT.ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#url.af_phone_number#%">
   </cfif>
  )
  </cfif>
  --->
 ) AS ActiveFeed
 <cfset af_sd_valid = IsDate(url.af_start_date)>
 <cfset af_ed_valid = IsDate(url.af_end_date)>
 <cfif af_sd_valid OR af_ed_valid>
 WHERE
  <cfif af_sd_valid>
   DATE(dt) >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#url.af_start_date#"> 
  </cfif>
  <cfif af_ed_valid>
   <cfif af_sd_valid>AND</cfif>
   DATE(dt) <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#url.af_end_date#"> 
  </cfif>
 </cfif>
 
 ORDER BY 
     dt DESC, 
        Id_bi DESC
    <cfif allactiveFeeds.TOTAL GT 0>
     LIMIT
      <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#start#">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#limit#">
    </cfif>
</cfquery>

<!--- <cfdump var="#allactiveFeeds.TOTAL#">
<cfdump var="#activeFeeds.RecordCount#">
<cfdump var="#url.af_batch_id#"> --->

<cfif activeFeeds.RecordCount GT 0>

	<cfset campaignNames = []>
	
	<cfif isDefined('campaignDetail')>
		<cfset campaignNames[url.id] = campaignDetail.BatchObject.Desc_vch>
	<cfelse>
		<cfset campaignIds = []>
		<cfloop query="activeFeeds">
			<cfset arrayAppend(campaignIds, activeFeeds.BatchId_bi)>
		</cfloop>
		
		<cfquery name="campaignList" datasource="#Session.DBSourceREAD#">
			SELECT BatchId_bi, Desc_vch 
			FROM simpleobjects.batch
			WHERE BatchId_bi IN (#arrayToList(campaignIds, ',')#);
		</cfquery>
		
		<cfif campaignList.RecordCount GT 0>
			<cfloop query="campaignList">
				<cfset campaignNames[campaignList.BatchId_bi] = campaignList.Desc_vch>
			</cfloop>
		</cfif>
	</cfif>

</cfif>

<table id="AFTable" class="table table-striped table-bordered table-hover">
	<input type="hidden" id="totalFeed" value="<cfoutput>#activeFeeds.RecordCount#</cfoutput>">
    <thead>
        <tr>
            <th colspan="2">
            	Activity Feed
            	<a title="Filter Activity Feed" href="#ActFeeFilModal" data-toggle="modal" data-target="#ActFeeFilModal" class="pull-right active-feed-filter">
            		<span aria-hidden="true" class="glyphicon glyphicon-filter"></span>
        		</a>
            </th>
        </tr>
    </thead>
    <cfif activeFeeds.RecordCount GT 0> 
    <tbody>
    	<cfoutput query="activeFeeds">
		<tr>
		<cfswitch expression="#log_type#">
		<cfdefaultcase>
			<td><span class="icon-newsfeed-message"></span></td>
			<td>
				<p>#ContactString_vch#
				<cfswitch expression="#sms_type#">
					<cfcase value="1">
						received:
					</cfcase>
					<cfcase value="2">
						sent:
					</cfcase>
				</cfswitch>
				</p>
				
				<!--- Preserve possible newlines in display but format all other tags --->
				<cfset ScrubbedSMS = ReplaceNoCase(sms, "<br>", "newlinex", "ALL")>
				<cfset ScrubbedSMS = ReplaceNoCase(ScrubbedSMS, "\n", "newlinex", "ALL")>
				<cfset ScrubbedSMS = Replace(ScrubbedSMS, chr(10), "newlinex", "ALL")>
				
				<cfset ScrubbedSMS = HTMLEditFormat(ScrubbedSMS) />
				
				<!--- Allow newline in text messages - reformat for display --->
				<cfset ScrubbedSMS = ReplaceNoCase(ScrubbedSMS, "newlinex", "<br>", "ALL")>
				<cfset ScrubbedSMS = Replace(ScrubbedSMS, "\n", "<br>", "ALL")>
				<!--- Display ' but store as &apos; in the XML --->
				<cfset ScrubbedSMS = Replace(ScrubbedSMS, "&apos;", "'", "ALL")>
														
				<pre class="dont-break-out">#ScrubbedSMS#</pre>
				<p class="time">#DateFormat(activeFeeds.dt, 'long')# #TimeFormat(activeFeeds.dt, 'short')# PST</p>
			</td>
		</cfdefaultcase>
		<cfcase value="1">
			<td><span class="icon-newsfeed-plus"></span></td>
			<td>#activeFeeds.ContactString_vch# joined 
				<cfif arrayIsDefined(campaignNames, activeFeeds.BatchId_bi)>
					#htmleditformat(campaignNames[activeFeeds.BatchId_bi])#
				</cfif>
				Campaign 
				<p class="time">#DateFormat(activeFeeds.dt, 'long')# #TimeFormat(activeFeeds.dt, 'short')# PST</p></td>
		</cfcase>
		<cfcase value="2">
			<td><span class="icon-newsfeed-minus"></span></td>
			<td>#activeFeeds.ContactString_vch# left your Campaign 
				<p class="time">#DateFormat(activeFeeds.dt, 'long')# #TimeFormat(activeFeeds.dt, 'short')# PST</p></td>
		</cfcase>
		<cfcase value="3">
			<td><span class="icon-newsfeed-plus"></span></td>
			<td>#activeFeeds.ContactString_vch# joined 
				<b>#sms#</b>
				CPP 
				<p class="time">#DateFormat(activeFeeds.dt, 'long')# #TimeFormat(activeFeeds.dt, 'short')# PST</p></td>
		</cfcase>
		</cfswitch>
		</tr>
    	</cfoutput>
        <!---<tr>
            <td><span class="icon-newsfeed-plus"></span></td>
            <td>(111) 000-0001 joined BOND Campaign <p class="time">October 19, 2015 09:15 PM PST</p></td>
        </tr>
        <tr>
            <td><span class="icon-newsfeed-minus"></span></td>
            <td>(111) 000-0002 left your Campaign <p class="time">October 18, 2015 09:15 PM PST</p></td>
        </tr>
        <tr>
            <td><span class="icon-newsfeed-message"></td>
            <td>Message of BOND campaign sent to  10 subscribers <p class="time"> October 17, 2015 09:15 PM PST</p></td>
        </tr>--->
    </tbody>

    </cfif>
</table>
<cfif activeFeeds.RecordCount GT 0>

<div class="active_feed_table" id="active_feed_table" style="padding-bottom: 5px">

	<span class="paginate_button_af_first">
		<img class="paginate-arrow" src="/session/sire/images/double-left.png">
	</span>

	<span class="paginate_button_af_previous">
		<img class="paginate-arrow" src="/session/sire/images/left.png">
	</span>

	<span class="paginate_page_af"><strong  style="padding-right: 10px">Page </strong></span>
		<input type="text" class="paginate_text_active" style="display: inline; width: 44px" value="<cfoutput>#url.page#</cfoutput>">
		<span class="paginate_of"><strong> of </strong><a id="allPages" style="text-decoration: none; color: #5c5c5c; font-weight:bold"><cfoutput>#allPages#</cfoutput></a>
	</span>

	<span class="paginate_button_af_next">
		<img class="paginate-arrow" src="/session/sire/images/right.png">
	</span>

	<span class="paginate_button_af_last">
		<img class="paginate-arrow" src="/session/sire/images/double-right.png">
	</span>

	&nbsp;<span class="glyphicon glyphicon-refresh glyphicon-spin activity-spinner" style="display: none;"></span>
</div>

</cfif>
<cfif !isAjaxRequest()>

</cfif>

<cffunction name="isAjaxRequest" output="false" returntype="boolean" access="public">
    <cfset var headers = getHttpRequestData().headers />
    <cfreturn structKeyExists(headers, "X-Requested-With") and (headers["X-Requested-With"] eq "XMLHttpRequest") />
</cffunction>

<style type="text/css">
	.paginate_text_active{
		width: 44px;
		text-align: center;
	}

	.active_feed_table{
		text-align: center;
	}
</style>
