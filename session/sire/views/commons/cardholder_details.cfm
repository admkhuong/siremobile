<div class="clearfix">
    <h3 class="form-heading">Cardholder Details</h3>
</div>

<cfoutput>
    <div class="update_cardholder_info">
        <cfif hidden_card_form NEQ ''>
            <ul class="nav list-newinfo">
                <li>Card Number: #maskedNumber#</li>
                <li>Expiration Date: #expirationDate#</li>
                <li>Cardholder Name: #firstName# #lastName#</li>
                <li>Address: #line1#</li>
                <li>City: #city#</li>
                <li>State: #state#</li>
                <li>Zip code: #zip#</li>
                <li>Country: #country#</li>
                <li>Email: #emailAddress#</li>
            </ul>

<!---             <div class="form-group">
                <button type="button" id="remove-payment-btn" title="Remove Card Infomation" class="btn newbtn green-cancel">Remove</button>
            </div> --->

            <div class="form-group">
                <div class="radio-inline" id="rad-currentcard">
                    <label>
                        <input type="radio" name="<cfif variables.urlpage EQ 'myplan'><cfoutput>optionsRadios</cfoutput><cfelseif variables.urlpage EQ 'upgradeplan' OR variables.urlpage EQ 'buysms' OR variables.urlpage EQ 'buykeyword'><cfoutput>select_used_card</cfoutput></cfif>" class="select_used_card" id="optionsRadios1" value="1" checked>
                        Current Card
                    </label>
                </div>
                <div class="radio-inline" id="rad-newcard" style="padding-left: 22px !important;">
                    <label>
                        <input type="radio" name="<cfif variables.urlpage EQ 'myplan'><cfoutput>optionsRadios</cfoutput><cfelseif variables.urlpage EQ 'upgradeplan' OR variables.urlpage EQ 'buysms' OR variables.urlpage EQ 'buykeyword'><cfoutput>select_used_card</cfoutput></cfif>" class="select_used_card" id="optionsRadios2" value="2">
                        New Card
                    </label>
                </div>
            </div>
        </cfif>
        <div id="fset_cardholder_info" style="<cfif hidden_card_form NEQ ''>display: none</cfif>">
        <input type="radio" class="hidden" name="payment_method" class="check_payment_method" id="payment_method_0" value="0" checked/>
        <div class="form-group">
            <div class="card-support-type">
                <img class="img-responsive" src="../assets/layouts/layout4/img/supported-payment-types.png" alt="">
            </div>
        </div>

        <div class="row row-small">
            <div class="col-lg-8 col-md-6">
                <div class="form-group">
                    <label>Card Number <span>*</span></label>
                    <input type="text" class="form-control validate[required,custom[creditCardFunc]]" maxlength="32" id="card_number" name="number" value='' placeholder="---- ---- ---- 1234">
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="form-group">
                    <label>Security Code <span>*</span></label>
                    <input type="text" class="form-control validate[required,custom[onlyNumber]]" maxlength="4" id="security_code" name="cvv" data-errormessage-custom-error="* Invalid security code">
                </div>
            </div>
        </div>

        <div class="row row-small">
            <div class="col-lg-8 col-md-6">
                <div class="form-group">
                    <label>Expiration Date <span>*</span></label>
                    <input type="hidden" id="expiration_date" name="expirationDate" value="">
                    <select class="form-control validate[required]" id="expiration_date_month">
                        <option value="" disabled selected>Month</option>
                        <option value="01">January</option>
                        <option value="02">February</option>
                        <option value="03">March</option>
                        <option value="04">April</option>
                        <option value="05">May</option>
                        <option value="06">June</option>
                        <option value="07">July</option>
                        <option value="08">August</option>
                        <option value="09">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                    </select>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="form-group">
                    <label>&nbsp;</label>
                    <select class="form-control validate[required]" id="expiration_date_year">
                        <option value="" disabled selected>Year</option>
                        <cfloop from="#year(now())#" to="#year(now())+50#" index="i">
                            <option value="#i#">#i#</option>
                        </cfloop>
                    </select>
                </div>
            </div>
        </div>

        <div class="row row-small">
            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    <label>Cardholder Name <span>*</span></label>
                    <input type="text" class="form-control validate[required,custom[onlyLetterNumberSp]]" maxlength="50" id="first_name" name="firstName" placeholder="First Name" value="">
                </div>
            </div>

            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    <label>&nbsp;</label>
                    <input type="text" class="form-control validate[required,custom[onlyLetterNumberSp]]" maxlength="50" id="last_name" name="lastName" placeholder="Last Name" value="">
                </div>
            </div>
        </div>

        <div class="form-group">
            <label>Address:<span>*</span></label>
            <input type="text" class="form-control validate[required,custom[noHTML]] cardholder" maxlength="60" id="line1" name="line1" data-value='#line1#'>
        </div>

        <div class="form-group">
            <label>City:<span>*</span></label>
            <input type="text" class="form-control validate[required,custom[noHTML]] cardholder" maxlength="40" id="city" name="city" data-value='#city#'>
        </div>


        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>State:<span>*</span></label>
                    <input type="text" class="form-control validate[required,custom[noHTML]] cardholder" maxlength="40" id="state" name="state" data-value="#state#">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Zip Code:<span>*</span></label>
                    <input type="text" class="form-control validate[required,custom[onlyNumber]] cardholder" maxlength="5" id="zip" name="zip" data-value="#zip#" data-errormessage-custom-error="* Invalid zip code">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Country:<span>*</span></label>
            <select class="form-control cardholder validate[required]" id="country" name="country" data-value="#country#" >
                <cfinclude template="../commons/payment/country.cfm">
            </select>
        </div>

        <input type="hidden" class="form-control validate[custom[phone]] cardholder" maxlength="30" id="phone" name="phone" data-value="#phone#">

        <div class="form-group">
            <label>Email:</label>
            <input type="text" class="form-control validate[custom[email]] cardholder" maxlength="128" id="email" name="email" data-value="#emailAddress#">
        </div>
        <hr>
    </div>
</div>
</cfoutput>
<div class="form-group">
    <img class="img-responsive img-simon" src="../assets/layouts/layout4/img/worldpay.png" alt="">  
     <!--- 
    <span class="worldpay-help simon-help">For help with your payment, visit <a target="blank" href="http://www.worldpay.com/uk/support">WorldPay Help</a></span> --->
</div> 