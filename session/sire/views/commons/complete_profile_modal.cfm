<!--- Complete Profile Modal --->
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="" id="modal-complete-profile">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
            </div>
            <div class="modal-body">
                <h3 class="message">You're in the Home Stretch</h3>
                <div class="home-stretch-body">
                    <p>There's just one last step before you build your awesome campaigns. It won't take long, I promise.<br><br>Click on the button below to fill out your company profile. Many of the SMS templates will pull info from your company profile so this one step will save you a ton of time in the future.</p>
                </div>
                <div class="button-confirm-signout text-center">
                    <a href="profile" class="btn btn-re btn-modal-complete-profile">Complete My Profile</a>
                </div>  
            </div>
        </div>
    </div>
</div>