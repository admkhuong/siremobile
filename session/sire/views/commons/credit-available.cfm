<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUserPlan" returnvariable="RetUserPlan"></cfinvoke> 
<cfinvoke component="session.sire.models.cfc.order_plan" method="checkUserPlanExpried" returnvariable="checkUserPlan">
	<cfinvokeargument name="userId" value="#SESSION.USERID#">
</cfinvoke>

<cfif RetUserPlan.RXRESULTCODE EQ 0>
	<cflocation url="/user-plan-error" addtoken="false">
</cfif>

<cfset exntendPlanEndDate = DateAdd("m", 1, RetUserPlan.ENDDATE)>
<cfset exntendPlanTK = ToBase64(DateFormat(exntendPlanEndDate, 'yyyy-mm-dd-') & Session.USERID)>
<!--- <cfset extendPlanUrl = '/extend-plan?tk=#exntendPlanTK#'>	 --->
<!--- <cfset extendPlanUrl = '##'> --->

<cfif isDate(RetUserPlan.USERDOWNGRADEDATE)>
	<div class="text-center no-print">
		<p class="notice-title text-danger">
			<cfoutput> Your plan will be downgraded on #Dateformat(RetUserPlan.ENDDATE,"yyyy-mm-dd")#. Please click <a href="/session/sire/pages/my-plan?action=downgradeDetail">here</a> for more detail.</cfoutput>
		</p>
	</div>	
</cfif>

<cfif RetUserPlan.PLANEXPIRED EQ 1>
	<div class="text-center no-print">
		<p class="notice-title text-danger">
			<cfoutput> Your plan <cfif RetUserPlan.KEYWORDPURCHASED GT 0> and keywords </cfif> has expired on #Dateformat(RetUserPlan.ENDDATE,"yyyy-mm-dd")#.<!--- <a class="active" href="#extendPlanUrl#"> Click here </a> to extend your plan ! ---></cfoutput>
		</p>
	</div>	
</cfif>

<!--- <cfif RetUserPlan.PLANEXPIRED GTE 2 AND RetUserPlan.KEYWORDBUYAFTEREXPRIED GT 0>
	<div class="row text-center no-print">
		<p class="notice-title text-danger">
			<cfoutput> Your plan had downgraded to Free Plan. <cfif RetUserPlan.KEYWORDBUYAFTEREXPRIED GT 0> <a class="active" href="#extendPlanUrl#"> Click here </a> to pay for #RetUserPlan.KEYWORDBUYAFTEREXPRIED# <cfif RetUserPlan.KEYWORDBUYAFTEREXPRIED GT 1> keywords <cfelse> keyword </cfif>  ! </cfif> </cfoutput>
		</p>
	</div>	
</cfif> --->