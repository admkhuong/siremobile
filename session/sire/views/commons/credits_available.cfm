<!--- UNUSED FILE --->
<cfinvoke component="session.sire.models.cfc.billing"  method="GetBalanceCurrentUser"  returnvariable="RetValBillingData"></cfinvoke>

<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUserPlan" returnvariable="RetUserPlan"></cfinvoke> 
<cfparam name="isCampaignManage" default="">
<cfset userPlanName = 'Free'>
<cfif RetUserPlan.RXRESULTCODE EQ 1>
	<cfset userPlanName = 	RetUserPlan.PLANNAME>
</cfif>

<style>
	
	.page
	{
		
		
	}
	
</style>	

<div class="row no-print">
	<div class="col-sm-6 col-md-6" id="xmy_plan_title"><span id="xCurrentPlan"><h4>My Plan: <a href="/session/sire/pages/my-plan"><cfoutput>#userPlanName#</cfoutput></a></h4></span></div>
	<div class="col-sm-6 col-md-6 credits_available"><span id="XCurrentCreditsAvailable"><h4>Credits Available: <a href="/session/sire/pages/my-plan"><cfoutput >#NumberFormat(RetValBillingData.Balance,',')#</cfoutput></a></h4></span></div>
</div>


<cfinvoke component="session.sire.models.cfc.order_plan" method="checkUserPlanExpried" returnvariable="checkUserPlan">
	<cfinvokeargument name="userId" value="#SESSION.USERID#">
</cfinvoke>

<cfif RetUserPlan.RXRESULTCODE EQ 0>
	<cflocation url="/user-plan-error" addtoken="false">
</cfif>

<cfset exntendPlanEndDate = DateAdd("m", 1, RetUserPlan.ENDDATE)>
<cfset exntendPlanTK = ToBase64(DateFormat(exntendPlanEndDate, 'yyyy-mm-dd-') & Session.USERID)>
<cfset extendPlanUrl = '/extend-plan?tk=#exntendPlanTK#'>	

<cfif RetUserPlan.PLANEXPIRED EQ 1>
	<div class="row text-center no-print">
		<p class="notice-title text-danger">
			<cfoutput> Your plan <cfif RetUserPlan.KEYWORDPURCHASED GT 0> and keywords </cfif> has expired on #Dateformat(RetUserPlan.ENDDATE,"yyyy-mm-dd")#.<a class="active" href="#extendPlanUrl#"> Click here </a> to extend your plan !</cfoutput>
		</p>
	</div>	
</cfif>

<cfif RetUserPlan.PLANEXPIRED GTE 2 AND RetUserPlan.KEYWORDBUYAFTEREXPRIED GT 0>
	<div class="row text-center no-print">
		<p class="notice-title text-danger">
			<cfoutput> Your plan had downgraded to Free Plan. <cfif RetUserPlan.KEYWORDBUYAFTEREXPRIED GT 0> <a class="active" href="#extendPlanUrl#"> Click here </a> to pay for #RetUserPlan.KEYWORDBUYAFTEREXPRIED# <cfif RetUserPlan.KEYWORDBUYAFTEREXPRIED GT 1> keywords <cfelse> keyword </cfif>  ! </cfif> </cfoutput>
		</p>
	</div>	
</cfif>


