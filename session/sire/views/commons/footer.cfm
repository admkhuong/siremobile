<footer class="no-print">
   <div id="footer">
      <div class="container-fluid">
         <div class="">
            <div class="social">
               <a href="https://www.facebook.com/siremobile/" target="_blank" title="Facebook"> <img src="/public/sire/images/facebook.png?v=1.0" alt="Facebook"/> </a>
               <a href="https://twitter.com/sire_mobile" target="_blank" title="Twitter"><img src="/public/sire/images/twitter.png?v=1.0" alt="Twitter"/></a>
               <!--- <a href="https://www.pinterest.com/siremobile/" target="_blank" title="Pinterest"><img src="../images/pinterest.png" alt="Twitter"/></a> --->
               <!--- <a href="https://www.instagram.com/siremobile/" target="_blank" title="Instagram"><img src="../images/instagram.png" alt="Twitter"/></a> --->
               <a href="https://www.youtube.com/channel/UCYqFX_Uy0MIY5i7gjgr_LXg" target="_blank" title="Youtube"><img src="/public/sire/images/youtube.png?v=1.0" alt="Youtube"/></a>
                <a href="https://www.linkedin.com/company/12952559" target="_blank" title="Linkedin"><img src="/public/sire/images/linkedin.png?v=1.0" alt="LinkedIn"/></a>
               <a href="https://plus.google.com/116619152084831668774" target="_blank" title="Google+"><img src="/public/sire/images/google_plus.png?v=1.0" alt="Google+"/></a>
            </div>
            <div class="credits">
               <ul>
                  <li>
                     <a href="/about-us">About Us</a>
                  </li>
                  <li>
                     <a href="/term-of-use">Terms of Use</a>
                  </li>
                  <li>
                     <a href="/privacy-policy">Privacy Policy</a>
                  </li>
                  <li>
                     <a href="/anti-spam">Anti-Spam Policy</a>
                  </li>
                  <li>
                     <a href="/">&copy; Sire, Inc. 2016</a>
                  </li>
               </ul>
            </div>

         </div>
      </div>
   </div>
</footer>

<!--- Alert popup --->
<div class="modal fade" id="bootstrapAlert" tabindex="-1" role="dialog" aria-labelledby="alertLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-body-inner row">
        	<h4 class="col-sm-10 modal-title"></h4>
        	<p class="col-sm-12 alert-message">       		
        	</p>
        </div>
      </div>
    </div>
  </div>
</div>


<!--- Sign out popup --->
<div class="modal fade" id="signout" tabindex="-1" role="dialog" aria-labelledby="signOutLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-body-inner row">
        	<form name="signout" class="row signout-form" autocomplete="off">
          <input type="hidden" name="SESuserId" id="SESuserId" value="<cfoutput>#SESSION.USERID#</cfoutput>">
		    	<h4 class="col-sm-10 modal-title">Sign Out of SIRE</h4>
		    	<div class="col-sm-12">
					<div class="checkbox">
					    <label>
					      <input type="checkbox" id='DALD' name='DALD'> Deauthorize this device? May require MFA code at next log in.
					    </label>
					 </div>
				</div>	 
			    <div class="col-sm-12">
				    <button type="submit" class="btn btn-larger btn-success-custom btn-modal-signout">Yes, Sign Out now</button>
				    <button type="submit" class="btn btn-larger btn-back-custom" data-dismiss="modal">Cancel</button>
			    </div>
		    </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="processingPayment">
    <img src="/public/sire/images/loading.gif" class="ajax-loader"/>
</div>
