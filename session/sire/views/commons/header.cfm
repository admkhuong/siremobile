<!---Verify whether to show dev features in production see - session.sire.models.cfc.admin method GetDevPermission to add your account user id for dev and testing --->
<cfinvoke method="GetDevPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetDevPermission" />
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission"><cfinvokeargument name="inpSkipRedirect" value="1"></cfinvoke>
<!--- Start rewrite menu canvas --->



<div class="overlay-canvas no-print"></div>
<div id="myCanvasNav" class="canvasNav no-print">
  <a href="#" class="closeNav"><span class="glyphicon glyphicon-arrow-left"></span></a>
  <ul class="nav offcanvasNav" id="accordion" role="tablist" aria-multiselectable="true">
    <li>
      <span>Menu</span>
    </li>
    <li>
      <a href="/session/sire/pages/dashboard">Dashboard</a>
    </li>
    <li class="panel">
        <a href="#collapseOne" class="dropdown-toggle" data-toggle="collapse" data-parent="#accordion" aria-expanded="true" aria-controls="collapseOne">Campaigns<span class="caret"></span></a>
        <ul class="nav collapse" id="collapseOne">
          <li><a href="/session/sire/pages/campaign-template-picker">Create Campaign</a></li>
		  <li><a href="/session/sire/pages/campaign-manage">Manage Campaigns</a></li>
		  <li><a href="/session/sire/pages/subscribers">Subscribers</a></li>
	    </ul>
    </li>
   
    <li class="panel">
      <a href="#collapseTwo" class="dropdown-toggle" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="collapseTwo">Tools<span class="caret"></span></a>
      <ul class="nav collapse" id="collapseTwo">
        <cfif RetVarGetDevPermission.ISDEVOK EQ  1>
        
        </cfif>
        <li class="dropdown-header">Advanced Features</li>
        <li class=""><a href="/session/sire/pages/mlp-list" style="padding-left:2em;">Marketing Landing Pages</a></li>
        <li class=""><a href="/session/sire/pages/short-url-manage" style="padding-left:2em;">Manage Short URLs</a></li>
        <li class=""><a href="/session/sire/pages/send-one" style="padding-left:2em;">Single Campaign Trigger</a></li>
        <li><a href="/session/sire/pages/qa-tool" style="padding-left:2em;">QA Tool (With Time Machine)</a></li>
        <li><a href="/session/sire/pages/reports-dashboard" style="padding-left:2em;">Account Level Reporting and Analytics</a></li>

        <cfif FINDNOCASE(CGI.SERVER_NAME,'awsqa.siremobile.com') GT 0>
          <li class="divider"></li>
          <li class="dropdown-header">Waitlist</li>
          <li><a href="/session/sire/pages/wait-list-manage" style="padding-left:2em;">Waitlist Manage</a></li>
          <li><a href="/session/sire/pages/wait-list-app" style="padding-left:2em;">Waitlist App</a></li>
        </cfif>
        
        <li class="divider"></li>

        <li class="dropdown-header">Advanced Campaigns</li>
        <li><a href="/session/sire/pages/campaign-edit?templateid=116&adv=1" style="padding-left:2em;">Build new custom campaign</a></li>
		  
        <!--- Requires session.sire.models.cfc.admin method GetAdminPermission to be loaded on this page first --->
        <cfif RetVarGetAdminPermission.ISADMINOK EQ  1>
	      <li class="divider"></li>
	      <li class="dropdown-header">Administrator Only Tools</li>
          <li class=""><a href="/session/sire/pages/admin-home" style="padding-left:2em;">Admin</a></li>
          <cfif FIND('campaign-edit', CGI.request_url) GT 0>
            <li class=""><a href="" style="padding-left:2em;" id="save-as-new-template">Save as new template</a></li>
          </cfif>
        </cfif>
      </ul>
    </li>
    <li class="panel">
      <a href="#collapseThree" class="dropdown-toggle" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="collapseThree">Support<span class="caret"></span></a>
      <ul class="nav collapse" id="collapseThree">
        <li><a href="/session/sire/pages/welcome">Welcome Overview</a></li>
		<li class="divider"></li>
        <li class="dropdown-header">Video Tutorial</li>
          <li><a href="/how-it-works?vplay=1" style="padding-left:2em;">Overview</a></li>
          <li><a href="/how-it-works?vplay=2" style="padding-left:2em;">Create Campaign</a></li>
          <li><a href="/how-it-works?vplay=3" style="padding-left:2em;">Subscriber List</a></li>
        <li class="divider"></li>
        <li class="dropdown-header">Guided Tours</li>
        <li><a href="/session/sire/pages/my-account?tour=gettingstarted&tourrs=true" style="padding-left:2em;">System Guide</a></li>
        <li class="divider"></li> 
        <li class="dropdown-header">Tutorials</li>
        <li><a href="/session/sire/pages/support-transactional-api.cfm" style="padding-left:2em;">Transactional API</a></li>
        <li><a href="/public/sire/pages/cs-faqs.cfm" style="padding-left:2em;">FAQs</a></li> 
      </ul>
    </li>
    <li class="panel">
      <a href="#collapsefour" class="dropdown-toggle" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="collapsefour">My account <span class="caret"></span></a>
      <ul class="nav collapse" id="collapsefour">
	  	<li><a href="/session/sire/pages/my-account">My Profile</a></li>
        <li><a href="/session/sire/pages/change-password">Change Password</a></li>
        <li><a href="/session/sire/pages/my-plan">My Plan</a></li>
        <li><a href="/session/sire/pages/security-credentials">Security credentials</a></li>
        <li><a href="javascript:void(0)" data-toggle="modal" data-target="#signout">Sign out</a></li>
      </ul>
    </li>
  </ul>
</div>
<!--- End rewrite menu canvas --->


<header id="header">
  <div class="navbar navbar-inverse navbar-fixed-top no-print navbar-site" role="navigation" id="slide-nav">
    <div class="container-fluid h-100">
      <div class="navbar-header clearfix h-100">
        <a class="navbar-toggle" id="trigger-canvasNav">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        <!---          <a class="navbar-brand logo" href="/session/sire/pages"><img src="../images/SIRE-logo-white.png" alt=""></a> --->
        <a class="navbar-brand logo" href="/session/sire/pages/landing">
          <img class="hidden-xs" src="/public/sire/images/logo-v14.png" alt="">
          <img class="visible-xs" src="/public/sire/images/logo2.png" alt="">
        </a>
      </div>
      <div id="slidemenu">
        <form class="navbar-form navbar-right fwhite visible-xs" role="form">
          <div>
            SIRE
          </div>
        </form>
        <ul class="nav navbar-nav navbar-right">
	         <li>
			 	<a href="/session/sire/pages/dashboard">Dashboard</a>
	  		</li>
    
          <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Campaigns<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="/session/sire/pages/campaign-template-picker">Create Campaign</a></li>
                <li><a href="/session/sire/pages/campaign-manage">Manage Campaigns</a></li>
                <li><a href="/session/sire/pages/subscribers">Subscribers</a></li>
              </ul>
          </li>
        
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tools<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <!--- Requires session.sire.models.cfc.admin method GetDevPermission to be loaded on this page first - remove this check when ready for full production release --->
              <cfif RetVarGetDevPermission.ISDEVOK EQ  1>
              
              </cfif>
				<li class="dropdown-header">Advanced Features</li>
				<li class=""><a href="/session/sire/pages/mlp-list" style="padding-left:2em;">Marketing Landing Pages</a></li>
				<li class=""><a href="/session/sire/pages/short-url-manage" style="padding-left:2em;">Manage Short URLs</a></li>
				<li class=""><a href="/session/sire/pages/send-one" style="padding-left:2em;">Single Campaign Trigger</a></li>
				<li><a href="/session/sire/pages/qa-tool" style="padding-left:2em;">QA Tool (With Time Machine)</a></li>
				<li><a href="/session/sire/pages/reports-dashboard" style="padding-left:2em;">Account Level Reporting and Analytics</a></li>
        
        <cfif FINDNOCASE(CGI.SERVER_NAME,'awsqa.siremobile.com') GT 0>
          <li class="divider"></li>
          <li class="dropdown-header">Waitlist</li>
          <li><a href="/session/sire/pages/wait-list-manage" style="padding-left:2em;">Waitlist Manage</a></li>
          <li><a href="/session/sire/pages/wait-list-app" style="padding-left:2em;">Waitlist App</a></li>
        </cfif>  
        
        		<li class="divider"></li>
				<li class="dropdown-header">Advanced Campaigns</li>
				<li><a href="/session/sire/pages/campaign-edit?templateid=116&adv=1" style="padding-left:2em;">Build new custom campaign</a></li>
					
              <!--- Requires session.sire.models.cfc.admin method GetAdminPermission to be loaded on this page first --->
              <cfif RetVarGetAdminPermission.ISADMINOK EQ  1>
			  	<li class="divider"></li>
				<li class="dropdown-header">Administrator Only Tools</li>
                <li class=""><a href="/session/sire/pages/admin-home" style="padding-left:2em;">Admin</a></li>
                <cfif FIND('campaign-edit', CGI.request_url) GT 0>
                  <li class=""><a href="" style="padding-left:2em;" id="save-as-new-template">Save as new template</a></li>
                </cfif>
              </cfif>
              
            </ul>
          </li>
                    
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Support<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="/session/sire/pages/welcome">Welcome Overview</a></li>
              <li class="divider"></li>
              <li class="dropdown-header">Video Tutorial</li>
                <li><a href="/how-it-works?vplay=1" style="padding-left:2em;">Overview</a></li>
                <li><a href="/how-it-works?vplay=2" style="padding-left:2em;">Create Campaign</a></li>
                <li><a href="/how-it-works?vplay=3" style="padding-left:2em;">Subscriber List</a></li>
              <li class="divider"></li>
              <li class="dropdown-header">Guided Tours</li>
              <li><a href="/session/sire/pages/my-account?tour=gettingstarted&tourrs=true" style="padding-left:2em;">System Guide</a></li>
              <li class="divider"></li>
              <li class="dropdown-header">Tutorials</li>
              <li><a href="/session/sire/pages/support-transactional-api.cfm" style="padding-left:2em;">Transactional API</a></li>
              <li><a href="/public/sire/pages/cs-faqs.cfm" style="padding-left:2em;">FAQs</a></li> 
            </ul>
          </li>
          
          
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My account <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="/session/sire/pages/my-account">My Profile</a></li>
              <li><a href="/session/sire/pages/change-password">Change Password</a></li>
              <li><a href="/session/sire/pages/my-plan">My Plan</a></li>
              <li><a href="/session/sire/pages/security-credentials">Security credentials</a></li>
              <li><a href="javascript:void(0)" data-toggle="modal" data-target="#signout">Sign out</a></li>
            </ul>
          </li>
        </ul>

      </div>
    </div>
  </div>
</header>
