<header id="header" class="or-plans-wrap">
   <div class="navbar navbar-inverse navbar-fixed-top no-print navbar-site" role="navigation">
      <div class="container-fluid">
         <div class="navbar-header">
            <a class="navbar-brand logo" href="/session/sire/pages">
               <img class="hidden-xs" src="/public/sire/images/logo-v14.png" alt="">
               <img class="visible-xs" src="/public/sire/images/logo2.png" alt="">
            </a>
         </div>

         <div id="order-plan-spec" class="clearfix dropdown">
            <button class="navbar-toggle" data-toggle="dropdown">
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
            </button>
            <ul class="nav navbar-nav navbar-right">
                     <li><a tabindex="-1" href="/features">Features</a></li>
              	     <li><a href="/who-uses-sire">Who</a></li>
	             <li><a tabindex="-1" href="/why-sire">Why</a></li>
                     <li><a tabindex="-1" href="/plans-pricing">Plans &amp; Pricing</a></li>
               	     <li><a href="/how-it-works">How It Works</a></li>
                     <li><a tabindex="-1" href="/contact-us">Contact Us</a></li>
            </ul>
         </div>         
      </div>
   </div>
</header> 
