<!--- BEGIN: BUILD YOUR FIRST CAMPAIGN --->
<div class="modal be-modal fade carousel-popup" id="CompleteModalBlast1-0" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">            
            <div class="modal-header">                                            
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="my-text-center head-intro">
                    <h4><b>High Five! You're done!</b></h4>
                    <p>Here are some recommended next steps for you.</p>
                </div>
                <div class="wrap-set-introduce" data-my-slideset="{default: 1, small: 2}">
                    <div class="my-slidenav-position">
                        <ul class="my-grid my-slideset">
                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-phone.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>Test your Campaign  </b></h4>
                                        <p>
                                            Test it out.  Grab your phone and text your keyword to the short phone number 39492.  If it looks good, you're ready to go.
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-edit.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>Edit your Campaign  </b></h4>
                                        <p>
                                        Make changes to the campaign by navigating to <a target="_blank" href="/session/sire/pages/campaign-manage"> Manage Campaigns </a> in the left menu and clicking on the <span class="btn-re-edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span> "edit" button
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-talk.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>We're here to help </b></h4>
                                        <p>
                                            Our Client Support team is here to help you succeed.  If you have any questions or need help, email us at <a href="mailto:hello@siremobile.com">hello@siremobile.com</a> or book an appointment by clicking <a href="https://sire-mobile.appointlet.com/b/sire-support" target="_blank" > here </a>.
                                        </p>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <a href="" class="my-slidenav my-slidenav-previous" data-my-slideset-item="previous"></a>
                        <a href="" class="my-slidenav my-slidenav-next" data-my-slideset-item="next"></a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div> 
<!--- END: BUILD YOUR FIRST CAMPAIGN --->

<!--- BEGIN: SURVEY GREEN --->
<div class="modal be-modal fade carousel-popup" id="CompleteModalBlast11-0" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">            
            <div class="modal-header">                                            
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="my-text-center head-intro">
                    <h4><b>High Five! You're done!</b></h4>
                    <p>Here are some recommended next steps for you.</p>
                </div>
                <div class="wrap-set-introduce" data-my-slideset="{default: 1, small: 2}">
                    <div class="my-slidenav-position">
                        <ul class="my-grid my-slideset">
                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-phone.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>Test your Campaign  </b></h4>
                                        <p>
                                            If you want to test it out, grab your phone and text your keyword to the short phone number 39492.  If it looks good, you're ready to go.
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-forum.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>Spread the Word </b></h4>
                                        <p>
                                            Promote your keyword & short code by adding them to signs, flyers, mailers, social media and your website to get the feedback you want.
                                        </p>
                                    </div>
                                </div>
                            </li>    

                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-phone.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>Get your Results</b></h4>
                                        <p>
                                            Now the fun begins.  You can watch the survey results in real-time.
                                            Go to  <a target="_blank" href="/session/sire/pages/campaign-manage"> Manage Campaigns </a> and click on the <span class="btn-re-xs">REPORT</span> button in the appropriate campaign.
                                        </p>
                                    </div>
                                </div>
                            </li>

                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-talk.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>We're here to help </b></h4>
                                        <p>
                                            Our Client Support team is here to help you succeed.  If you have any questions or need help, email us at <a href="mailto:hello@siremobile.com">hello@siremobile.com</a> or book an appointment by clicking <a href="https://sire-mobile.appointlet.com/b/sire-support" target="_blank" > here </a>.
                                        </p>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <a href="" class="my-slidenav my-slidenav-previous" data-my-slideset-item="previous"></a>
                        <a href="" class="my-slidenav my-slidenav-next" data-my-slideset-item="next"></a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div> 
<!--- END: SURVEY GREEN --->

<!--- BEGIN: SURVEY BLUE --->
<div class="modal be-modal fade carousel-popup" id="CompleteModalBlast11-1" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">            
            <div class="modal-header">                                            
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="my-text-center head-intro">
                    <h4><b>High Five! You're done!</b></h4>
                    <p>Here are some recommended next steps for you.</p>
                </div>
                <div class="wrap-set-introduce" data-my-slideset="{default: 1, small: 2}">
                    <div class="my-slidenav-position">
                        <ul class="my-grid my-slideset">
                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-phone.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>Get your Results</b></h4>
                                        <p>
                                            Now the fun begins.  You can watch the survey results in real-time.
                                            Go to  <a target="_blank" href="/session/sire/pages/campaign-manage"> Manage Campaigns </a> and click on the <span class="btn-re-xs">REPORT</span> button in the appropriate campaign.
                                        </p>
                                    </div>
                                </div>
                            </li>

                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-talk.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>We're here to help </b></h4>
                                        <p>
                                            Our Client Support team is here to help you succeed.  If you have any questions or need help, email us at <a href="mailto:hello@siremobile.com">hello@siremobile.com</a> or book an appointment by clicking <a href="https://sire-mobile.appointlet.com/b/sire-support" target="_blank" > here </a>.
                                        </p>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <a href="" class="my-slidenav my-slidenav-previous visible-xs" data-my-slideset-item="previous"></a>
                        <a href="" class="my-slidenav my-slidenav-next visible-xs" data-my-slideset-item="next"></a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div> 
<div class="modal be-modal fade carousel-popup" id="CompleteModalBlast11-2" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">            
            <div class="modal-header">                                            
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="my-text-center head-intro">
                    <h4><b>High Five! You're done!</b></h4>
                    <p>Here are some recommended next steps for you.</p>
                </div>
                <div class="wrap-set-introduce" data-my-slideset="{default: 1, small: 2}">
                    <div class="my-slidenav-position">
                        <ul class="my-grid my-slideset">
                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-phone.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>Get your Results</b></h4>
                                        <p>
                                            Now the fun begins.  You can watch the survey results in real-time.
                                            Go to  <a target="_blank" href="/session/sire/pages/campaign-manage"> Manage Campaigns </a> and click on the <span class="btn-re-xs">REPORT</span> button in the appropriate campaign.
                                        </p>
                                    </div>
                                </div>
                            </li>

                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-talk.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>We're here to help </b></h4>
                                        <p>
                                            Our Client Support team is here to help you succeed.  If you have any questions or need help, email us at <a href="mailto:hello@siremobile.com">hello@siremobile.com</a> or book an appointment by clicking <a href="https://sire-mobile.appointlet.com/b/sire-support" target="_blank" > here </a>.
                                        </p>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <a href="" class="my-slidenav my-slidenav-previous visible-xs" data-my-slideset-item="previous"></a>
                        <a href="" class="my-slidenav my-slidenav-next visible-xs" data-my-slideset-item="next"></a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div> 
<div class="modal be-modal fade carousel-popup" id="CompleteModalBlast11-3" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">            
            <div class="modal-header">                                            
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="my-text-center head-intro">
                    <h4><b>High Five! You're done!</b></h4>
                    <p>Here are some recommended next steps for you.</p>
                </div>
                <div class="wrap-set-introduce" data-my-slideset="{default: 1, small: 2}">
                    <div class="my-slidenav-position">
                        <ul class="my-grid my-slideset">
                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-phone.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>Get your Results</b></h4>
                                        <p>
                                            Now the fun begins.  You can watch the survey results in real-time.
                                            Go to  <a target="_blank" href="/session/sire/pages/campaign-manage"> Manage Campaigns </a> and click on the <span class="btn-re-xs">REPORT</span> button in the appropriate campaign.
                                        </p>
                                    </div>
                                </div>
                            </li>

                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-talk.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>We're here to help </b></h4>
                                        <p>
                                            Our Client Support team is here to help you succeed.  If you have any questions or need help, email us at <a href="mailto:hello@siremobile.com">hello@siremobile.com</a> or book an appointment by clicking <a href="https://sire-mobile.appointlet.com/b/sire-support" target="_blank" > here </a>.
                                        </p>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <a href="" class="my-slidenav my-slidenav-previous visible-xs" data-my-slideset-item="previous"></a>
                        <a href="" class="my-slidenav my-slidenav-next visible-xs" data-my-slideset-item="next"></a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div> 
<!--- END: SURVEY BLUE --->

<!--- BEGIN: DRIP GREEN --->
<div class="modal be-modal fade carousel-popup" id="CompleteModalBlast8-0" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">            
            <div class="modal-header">                                            
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="my-text-center head-intro">
                    <h4><b>High Five! You're done!</b></h4>
                    <p>Here are some recommended next steps for you.</p>
                </div>
                <div class="wrap-set-introduce" data-my-slideset="{default: 1, small: 2}">
                    <div class="my-slidenav-position">
                        <ul class="my-grid my-slideset">
                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-phone.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>Test your Campaign  </b></h4>
                                        <p>
                                           If you want to test it out, grab your phone and text your keyword to the short phone number 39492.  If it looks good, you're ready to go
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-forum.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>Spread the Word </b></h4>
                                        <p>
                                            Promote your keyword & short code by adding them to signs, flyers, social media and your website to get people to chat with you.
                                        </p>
                                    </div>
                                </div>
                            </li>    

                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-phone.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>Get your Results</b></h4>
                                        <p>
                                            Once your campaign is sending, it’s always fun watching the progress of the drip.  Just go to <a target="_blank" href="/session/sire/pages/campaign-manage"> Manage Campaigns </a> and click on the <span class="btn-re-xs">REPORT</span> button.
                                        </p>
                                    </div>
                                </div>
                            </li>

                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-talk.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>We're here to help </b></h4>
                                        <p>
                                            Our Client Support team is here to help you succeed.  If you have any questions or need help, email us at <a href="mailto:hello@siremobile.com">hello@siremobile.com</a> or book an appointment by clicking <a href="https://sire-mobile.appointlet.com/b/sire-support" target="_blank" > here </a>.
                                        </p>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <a href="" class="my-slidenav my-slidenav-previous" data-my-slideset-item="previous"></a>
                        <a href="" class="my-slidenav my-slidenav-next" data-my-slideset-item="next"></a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div> 
<!--- END: DRIP GREEN --->

<!--- BEGIN: DRIP BLUE --->
<div class="modal be-modal fade carousel-popup" id="CompleteModalBlast8-1" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">            
            <div class="modal-header">                                            
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="my-text-center head-intro">
                    <h4><b>High Five! You're done!</b></h4>
                    <p>Here are some recommended next steps for you.</p>
                </div>
                <div class="wrap-set-introduce" data-my-slideset="{default: 1, small: 2}">
                    <div class="my-slidenav-position">
                        <ul class="my-grid my-slideset">
                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-phone.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>Get your Results</b></h4>
                                        <p>
                                            Once your campaign is sending, it’s always fun watching the progress of the drip.  Just go to <a target="_blank" href="/session/sire/pages/campaign-manage"> Manage Campaigns </a> and click on the <span class="btn-re-xs">REPORT</span> button.
                                        </p>
                                    </div>
                                </div>
                            </li>

                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-talk.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>We're here to help </b></h4>
                                        <p>
                                            Our Client Support team is here to help you succeed.  If you have any questions or need help, email us at <a href="mailto:hello@siremobile.com">hello@siremobile.com</a> or book an appointment by clicking <a href="https://sire-mobile.appointlet.com/b/sire-support" target="_blank" > here </a>.
                                        </p>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <a href="" class="my-slidenav my-slidenav-previous visible-xs" data-my-slideset-item="previous"></a>
                        <a href="" class="my-slidenav my-slidenav-next visible-xs" data-my-slideset-item="next"></a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div> 
<div class="modal be-modal fade carousel-popup" id="CompleteModalBlast8-2" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">            
            <div class="modal-header">                                            
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="my-text-center head-intro">
                    <h4><b>High Five! You're done!</b></h4>
                    <p>Here are some recommended next steps for you.</p>
                </div>
                <div class="wrap-set-introduce" data-my-slideset="{default: 1, small: 2}">
                    <div class="my-slidenav-position">
                        <ul class="my-grid my-slideset">
                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-phone.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>Get your Results</b></h4>
                                        <p>
                                            Once your campaign is sending, it’s always fun watching the progress of the drip.  Just go to <a target="_blank" href="/session/sire/pages/campaign-manage"> Manage Campaigns </a> and click on the <span class="btn-re-xs">REPORT</span> button.
                                        </p>
                                    </div>
                                </div>
                            </li>

                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-talk.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>We're here to help </b></h4>
                                        <p>
                                            Our Client Support team is here to help you succeed.  If you have any questions or need help, email us at <a href="mailto:hello@siremobile.com">hello@siremobile.com</a> or book an appointment by clicking <a href="https://sire-mobile.appointlet.com/b/sire-support" target="_blank" > here </a>.
                                        </p>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <a href="" class="my-slidenav my-slidenav-previous visible-xs" data-my-slideset-item="previous"></a>
                        <a href="" class="my-slidenav my-slidenav-next visible-xs" data-my-slideset-item="next"></a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div> 
<div class="modal be-modal fade carousel-popup" id="CompleteModalBlast8-3" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">            
            <div class="modal-header">                                            
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="my-text-center head-intro">
                    <h4><b>High Five! You're done!</b></h4>
                    <p>Here are some recommended next steps for you.</p>
                </div>
                <div class="wrap-set-introduce" data-my-slideset="{default: 1, small: 2}">
                    <div class="my-slidenav-position">
                        <ul class="my-grid my-slideset">
                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-phone.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>Get your Results</b></h4>
                                        <p>
                                            Once your campaign is sending, it’s always fun watching the progress of the drip.  Just go to <a target="_blank" href="/session/sire/pages/campaign-manage"> Manage Campaigns </a> and click on the <span class="btn-re-xs">REPORT</span> button.
                                        </p>
                                    </div>
                                </div>
                            </li>

                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-talk.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>We're here to help </b></h4>
                                        <p>
                                            Our Client Support team is here to help you succeed.  If you have any questions or need help, email us at <a href="mailto:hello@siremobile.com">hello@siremobile.com</a> or book an appointment by clicking <a href="https://sire-mobile.appointlet.com/b/sire-support" target="_blank" > here </a>.
                                        </p>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <a href="" class="my-slidenav my-slidenav-previous visible-xs" data-my-slideset-item="previous"></a>
                        <a href="" class="my-slidenav my-slidenav-next visible-xs" data-my-slideset-item="next"></a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div> 
<!--- END: DRIP BLUE --->

<!--- BEGIN: CHAT --->
<div class="modal be-modal fade carousel-popup" id="CompleteModalBlast9-0" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">            
            <div class="modal-header">                                            
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="my-text-center head-intro">
                    <h4><b>High Five! You're done!</b></h4>
                    <p>Here are some recommended next steps for you.</p>
                </div>
                <div class="wrap-set-introduce" data-my-slideset="{default: 1, small: 2}">
                    <div class="my-slidenav-position">
                        <ul class="my-grid my-slideset">
                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-phone.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>Test your Campaign  </b></h4>
                                        <p>
                                            If you want to test it out, grab your phone and text your keyword to the short phone number 39492.  If it looks good, you're ready to go.
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-forum.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>Start your Chat</b></h4>
                                        <p>
                                            Start chatting with your customers by going to to <a target="_blank" href="/session/sire/pages/campaign-manage"> Manage Campaigns </a>and clicking on the Chat <br/> <span class="btn-chat-xs"><i class="fa fa-commenting-o" aria-hidden="true"></i><span>1<span></span></span></span>button.
                                        </p>
                                    </div>
                                </div>
                            </li>    

                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-phone.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>Get your Results</b></h4>
                                        <p>
                                            Once your campaign is sending, it’s always fun watching the progress of the drip.  Just go to <a target="_blank" href="/session/sire/pages/campaign-manage"> Manage Campaigns </a> and click on the <span class="btn-re-xs">REPORT</span> button.
                                        </p>
                                    </div>
                                </div>
                            </li>

                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-phone.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>Don’t miss opportunities </b></h4>
                                        <p>
                                            Make sure you’ve entered a mobile number or email to recieve a chat session alert.  Go to <a target="_blank" href="/session/sire/pages/campaign-manage"> Manage Campaigns </a> and click on the edit <span class="btn-re-edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span> button.
                                        </p>
                                        <p>
                                            <img class="" src="/session/sire/images/introduce-img/chat-noti.png">
                                        </p>    
                                    </div>
                                </div>
                            </li>

                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-talk.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>We're here to help </b></h4>
                                        <p>
                                            Our Client Support team is here to help you succeed.  If you have any questions or need help, email us at <a href="mailto:hello@siremobile.com">hello@siremobile.com</a> or book an appointment by clicking <a href="https://sire-mobile.appointlet.com/b/sire-support" target="_blank" > here </a>.
                                        </p>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <a href="" class="my-slidenav my-slidenav-previous" data-my-slideset-item="previous"></a>
                        <a href="" class="my-slidenav my-slidenav-next" data-my-slideset-item="next"></a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div> 
<!--- END: CHAT --->



<!--- BEGIN: SEND COUPON --->

<!-- Blast Now -->
<div class="modal be-modal fade carousel-popup" id="CompleteModalBlast3-1" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">            
            <div class="modal-header">                                            
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="my-text-center head-intro">
                    <h4><b>High Five! You're done!</b></h4>
                    <p>Here are some recommended next steps for you.</p>
                </div>
                <div class="wrap-set-introduce" data-my-slideset="{default: 1, small: 2}">
                    <div class="my-slidenav-position">
                        <ul class="my-grid my-slideset">
                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-phone.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>Watch the magic happen </b></h4>
                                        <p>
                                            It’s always fun watching the progress of the blast.  You can do this by going to <a target="_blank" href="/session/sire/pages/campaign-manage"> Manage Campaigns </a> and clicking on the <span class="btn-re-xs">REPORT</span>  button
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-talk.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>We're here to help </b></h4>
                                        <p>
                                            Our Client Support team is here to help you succeed.  If you have any questions or need help, email us at <a href="mailto:hello@siremobile.com">hello@siremobile.com</a> or book an appointment by clicking <a href="https://sire-mobile.appointlet.com/b/sire-support" target="_blank" > here </a>.
                                        </p>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <a href="" class="my-slidenav my-slidenav-previous visible-xs" data-my-slideset-item="previous"></a>
                        <a href="" class="my-slidenav my-slidenav-next visible-xs" data-my-slideset-item="next"></a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>

<!-- Blast Save  -->
<div class="modal be-modal fade carousel-popup" id="CompleteModalBlast3-2" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">            
            <div class="modal-header">                                            
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="my-text-center head-intro">
                    <h4><b>High Five! You're done!</b></h4>
                    <p>Here are some recommended next steps for you.</p>
                </div>
                <div class="wrap-set-introduce" data-my-slideset="{default: 1, small: 2}">
                    <div class="my-slidenav-position">
                        <ul class="my-grid my-slideset">
                            <!--- <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-edit.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>Second thoughts? </b></h4>
                                        <p>
                                            You can always return to this campaign and make edits as long as it’s before the designated send date & time.  Go to <a target="_blank" href="/session/sire/pages/campaign-manage"> Manage Campaigns </a> and click this button to edit <span class="btn-re-edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span>.
                                        </p>
                                    </div>
                                </div>
                            </li> --->
                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-phone.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>Watch the magic happen </b></h4>
                                        <p>
                                        Once your campaign is sending, it’s always fun watching the progress of the blast.  You can do this by going to <a target="_blank" href="/session/sire/pages/campaign-manage"> Manage Campaigns </a> and clicking on the <span class="btn-re-xs">REPORT</span> button
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-talk.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>We're here to help </b></h4>
                                        <p>
                                            Our Client Support team is here to help you succeed.  If you have any questions or need help, email us at <a href="mailto:hello@siremobile.com">hello@siremobile.com</a> or book an appointment by clicking <a href="https://sire-mobile.appointlet.com/b/sire-support" target="_blank" > here </a>.
                                        </p>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <a href="" class="my-slidenav my-slidenav-previous visible-xs" data-my-slideset-item="previous"></a>
                        <a href="" class="my-slidenav my-slidenav-next visible-xs" data-my-slideset-item="next"></a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div> 

<!-- Modal Carousel -->
<div class="modal be-modal fade carousel-popup" id="CompleteModalBlast3-3" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">            
            <div class="modal-header">                                            
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="my-text-center head-intro">
                    <h4><b>High Five! You're done!</b></h4>
                    <p>Here are some recommended next steps for you.</p>
                </div>
                <div class="wrap-set-introduce" data-my-slideset="{default: 1, small: 2}">
                    <div class="my-slidenav-position">
                        <ul class="my-grid my-slideset">
                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-edit.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>Don’t forget about me </b></h4>
                                        <p>
                                            Come back to finish this blast by going to <a target="_blank" href="/session/sire/pages/campaign-manage"> Manage Campaigns </a> 
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-phone.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>Watch the magic happen </b></h4>
                                        <p>
                                            Make changes to the campaign by navigating to <a target="_blank" href="/session/sire/pages/campaign-manage"> Manage Campaigns </a> in the left menu and clicking on the "edit" <span class="btn-re-edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span> button
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <li class="intro-item my-text-center">
                                <div class="inner-intro-item">
                                    <div class="img uk-flex uk-flex-middle uk-flex-center">
                                        <img src="\session\sire\images\introduce-img\intro-talk.png" alt="">
                                    </div>
                                    <div class="content">
                                        <h4><b>We're here to help </b></h4>
                                        <p>
                                            Our Client Support team is here to help you succeed.  If you have any questions or need help, email us at <a href="mailto:hello@siremobile.com">hello@siremobile.com</a> or book an appointment by clicking <a href="https://sire-mobile.appointlet.com/b/sire-support" target="_blank" > here </a>.
                                        </p>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <a href="" class="my-slidenav my-slidenav-previous" data-my-slideset-item="previous"></a>
                        <a href="" class="my-slidenav my-slidenav-next" data-my-slideset-item="next"></a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div> 


<!--- END: SEND COUPON --->
