<div class="row heading">
	<div class="col-xs-12 heading-title">Customer Billing Information<hr></div>
</div>
<div class="row secure-payment-plan">
	<div class="col-xs-4">First Name:</div>
	<div class="col-xs-8"><div id="rsFirstName" class="pull-right active"></div></div>
</div>
<div class="row secure-payment-plan">
	<div class="col-xs-4">Last Name:</div>
	<div class="col-xs-8"><div id="rsLastName" class="pull-right active"></div></div>
</div>
<div class="row secure-payment-plan">
	<div class="col-xs-4">Address:</div>
	<div class="col-xs-8"><div id="rsAddress" class="pull-right active"></div></div>
</div>
<div class="row secure-payment-plan">
	<div class="col-xs-4">City:</div>
	<div class="col-xs-8"><div id="rsCity" class="pull-right active"></div></div>
</div>
<div class="row secure-payment-plan">
	<div class="col-xs-4">State:</div>
	<div class="col-xs-8"><div id="rsState" class="pull-right active"></div></div>
</div>
<div class="row secure-payment-plan">
	<div class="col-xs-4">Zip Code:</div>
	<div class="col-xs-8"><div id="rsZipCode" class="pull-right active"></div></div>
</div>
<div class="row secure-payment-plan">
	<div class="col-xs-4">Country:</div>
	<div class="col-xs-8"><div id="rsCountry" class="pull-right active"></div></div>
</div>
<div class="row secure-payment-plan">
	<div class="col-xs-4">Email:</div>
	<div class="col-xs-8"><div id="rsEmail" class="pull-right active"></div></div>
</div>