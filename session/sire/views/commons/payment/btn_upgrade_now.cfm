<div class="row">
    <div class="col-sm-12 small-info form-footer" style="color:red;padding-bottom:10px;">
        By upgrading now you are signing up for a new monthly subscription
    </div>
	<div class="col-sm-12 text-center">
	   	<a type="button" class="btn btn-medium btn-back-custom pull-right"  href="javascript:history.go(-1)">Cancel</a>
    	<button type="submit" class="btn btn-medium btn-success-custom btn-payment-credits pull-right margin-right-5">Upgrade Now</button>
        <a type="button" id="btnStep3_back" class="btn btn-medium btn-primary-custom pull-right margin-right-5">Back</a>
    </div>
</div>