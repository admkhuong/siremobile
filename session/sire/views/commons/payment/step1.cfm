<cfoutput >
<cfif hidden_card_form NEQ ''>
		<hr>
		<div class="row heading">
			<cfif paymentType EQ 'card'>
			<div class="col-sm-12 heading-title">Card Information<hr></div>
			<div class="col-sm-11 form-horizontal">
				<div class="form-group">
					<label for="card_number" class="col-sm-11 control-label">Card Number: #maskedNumber#</label>
				</div>		
				<div class="form-group">
					<label for="card_number" class="col-sm-11 control-label">Expiration Date: #expirationDate#</label>
				</div>		
				<div class="form-group">
					<label for="card_number" class="col-sm-11 control-label">Cardholder Name: #firstName# #lastName# </label>
				</div>
				
				<input type="hidden" id="h_firstName" value="#firstName#">
				<input type="hidden" id="h_lastName" value="#lastName#">
			<cfelseif paymentType EQ 'check'>
				<div class="col-sm-12 heading-title">ECheck Information<hr></div>
    			<div class="col-sm-11 form-horizontal">
    				<div class="form-group">
    					<label for="card_number" class="col-sm-11 control-label">Routing Number: ****#Right(maskedNumber, 4)#</label>
    				</div>		
    				<div class="form-group">
    					<label for="card_number" class="col-sm-11 control-label">Account Number: ****#Right(cvv, 4)#</label>
    				</div>		
    				<div class="form-group">
    					<label for="card_number" class="col-sm-11 control-label">Customer Name: #firstName# #lastName# </label>
    				</div>
			</cfif>	
				<div class="radio-inline">
				 <label>
				    <input type="radio" name="select_used_card" class="select_used_card" id="optionsRadios1" value="1" checked>
				    	Current Card
				  </label>
				</div>
				<div class="radio-inline">
				  	<label>
				    	<input type="radio" name="select_used_card" class="select_used_card" id="optionsRadios2" value="2">
				    	New Card
				  	</label>
				</div>			
			</div>	
		</div>
	</cfif>

    <hr>
    <div class="update_card_info" style="#hidden_card_form#">
    	<div class="row heading">
    		<!---
			<div class="col-sm-12 heading-title">
    			Select Your Payment Method:
			</div>
			--->
			<div class="col-sm-12">
				<input type="radio" name="payment_method" class="check_payment_method" id="payment_method_0" value="0" checked="checked"/>
    			<label for="payment_method_0" class="payment_method_card payment_method_card_0">
    				&nbsp;
    			</label>
				<!---<input type="radio" name="payment_method" class="check_payment_method" id="payment_method_0_1" value="0"/>
    			<label for="payment_method_0_1" class="payment_method_card payment_method_card_1">
    				&nbsp;
    			</label>
				<input type="radio" name="payment_method" class="check_payment_method" id="payment_method_0_2" value="0"/>
    			<label for="payment_method_0_2" class="payment_method_card payment_method_card_2">
    				&nbsp;
    			</label>
				<input type="radio" name="payment_method" class="check_payment_method" id="payment_method_0_3" value="0"/>
    			<label for="payment_method_0_3" class="payment_method_card payment_method_card_3">
    				&nbsp;
    			</label>--->
    			<!---
				<input type="radio" name="payment_method" class="check_payment_method" id="payment_method_1" value="1"/>
    			<label for="payment_method_1" class="payment_method_echeck">
    				&nbsp;
    			</label>
    			--->
			</div>
			<div class="col-sm-12">
				<hr style="margin-top: 10px;"> 
			</div>
    	</div>
        <!---<div class="row heading">
        	<div class="col-sm-12 heading-title">Card Information<hr></div>
        </div>--->
        <div class="row">
        	<div class="col-sm-12 form-horizontal">
			  <div class="form-group">
			    <label for="card_number" class="col-sm-4 control-label">Card Number:<span class="text-danger">*</span></label>
			    <div class="col-sm-8">
			      <input type="text" class="form-control validate[required,custom[creditCardFunc]]" maxlength="32" id="card_number" name="number">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="security_code" class="col-sm-4 control-label">Security Code:<span class="text-danger">*</span>
			    	<a href="##" data-toggle="modal" data-target="##scModal"><img src="/session/sire/images/help-small.png"/></a>
			    </label>
			    <div class="col-sm-8">
			      <input type="text" class="form-control validate[required,custom[onlyNumber]]" maxlength="4" id="security_code" name="cvv" data-errormessage-custom-error="* Invalid security code">
			    </div>
			  </div>
			  <div class="form-group" id="ExpirationDate">
			    <label for="" class="col-sm-4 control-label">Expiration Date:<span class="text-danger">*</span></label>
		        <input type="hidden" id="expiration_date" name="expirationDate">
			    <div class="col-sm-4">
			      <select class="form-control validate[required]" id="expiration_date_month">
			      	<option value="01">January</option>
			      	<option value="02">February</option>
			      	<option value="03">March</option>
			      	<option value="04">April</option>
			      	<option value="05">May</option>
			      	<option value="06">June</option>
			      	<option value="07">July</option>
			      	<option value="08">August</option>
			      	<option value="09">September</option>
			      	<option value="10">October</option>
			      	<option value="11">November</option>
			      	<option value="12">December</option>
			      </select>
			    </div>
			    <div class="col-sm-4">
			    	<select class="form-control validate[required]" id="expiration_date_year">
			    		<cfloop from="#year(now())#" to="#year(now())+50#" index="i">
			    			<option value="#i#">#i#</option>
			    		</cfloop>
			    	</select>
			    </div>
			  </div>

			<div class="form-group">
			    <label for="first_name" class="col-sm-4 control-label" id="card_name_label">Cardholder Name:<span class="text-danger">*</span></label>
			    <div class="col-sm-4">
			      <input type="text" class="form-control validate[required,custom[onlyLetterNumberSp]]" maxlength="50" id="first_name" name="firstName" placeholder="First Name">
			    </div>
			    <div class="col-sm-4">
			      <input type="text" class="form-control validate[required,custom[onlyLetterNumberSp]]" maxlength="50" id="last_name" name="lastName" placeholder="Last Name">
			    </div>
			</div>
			<cfif hidden_card_form NEQ ''>
				<div class="checkbox">
				    <label>
				      <input type="checkbox" name='save_cc_info' value='1'>Save Credit Card information for recurring and future payments.
				    </label>
				</div>
			</cfif>	
			
			</div>
    	</div>

    	
	</div>
	<div class="col-sm-12 form-horizontal form-footer text-center">
		<a type="button" class="btn btn-medium btn-back-custom pull-right"  href="javascript:history.go(-1)">Cancel</a>
		<a type="button" id="btnStep1" class="btn btn-medium btn-success-custom pull-right margin-right-5">Next</a>
	 </div>
</div>
</div>
</cfoutput>