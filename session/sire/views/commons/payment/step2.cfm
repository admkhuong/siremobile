<cfoutput >
<input type="hidden" name="h_email_save" value="#emailAddress#">
	
<div id="step2" style="display:none">
<div class="row">
	<div class="col-md-6">
    <div class="row heading">
        <div class="col-sm-12 heading-title">Cardholder Details<hr></div>
    </div>

    <div class="update_cardholder_info">
        <fieldset name='fset_cardholder_info' #disabled_field#>
        <div class="row">
        	<div class="col-sm-12 form-horizontal">
			  <div class="form-group">
			    <label for="line1" class="col-sm-4 control-label">Address:<span class="text-danger">*</span></label>
			    <div class="col-sm-8">
			      <input type="text" class="form-control validate[required,custom[noHTML]] cardholder" maxlength="60" id="line1" name="line1" value="#line1#" data-value='#line1#'>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="city" class="col-sm-4 control-label">City:<span class="text-danger">*</span></label>
			    <div class="col-sm-8">
			      <input type="text" class="form-control validate[required,custom[noHTML]] cardholder" maxlength="40" id="city" name="city" value="#city#" data-value='#city#'>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="state" class="col-sm-4 control-label">State:<span class="text-danger">*</span></label>
			    <div class="col-sm-8">
			      <input type="text" class="form-control validate[required,custom[noHTML]] cardholder" maxlength="40" id="state" name="state" value="#state#" data-value="#state#">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="zip" class="col-sm-4 control-label">Zip Code:<span class="text-danger">*</span></label>
			    <div class="col-sm-8">
			      <input type="text" class="form-control validate[required,custom[onlyNumber]] cardholder" maxlength="5" id="zip" name="zip" data-errormessage-custom-error="* Invalid zip code" value="#zip#" data-value="#zip#">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="country" class="col-sm-4 control-label">Country:<span class="text-danger">*</span></label>
			    <div class="col-sm-8">
					<select class="form-control cardholder validate[required]" id="country" name="country" data-value="#country#">
						<cfinclude template="country.cfm">
					</select>
			    </div>
			  </div>
			  <!---
			  <div class="form-group" >
			    <label for="phone" class="col-sm-4 control-label">Telephone:</label>
			    <div class="col-sm-8">
			      <input type="text" class="form-control validate[custom[phone]] cardholder" maxlength="30" id="phone" name="phone" value="#phone#" data-value="#phone#">
			    </div>
			  </div>--->
			  <input type="hidden" class="form-control validate[custom[phone]] cardholder" maxlength="30" id="phone" name="phone" value="#phone#" data-value="#phone#">
			  <div class="form-group">
			    <label for="email" class="col-sm-4 control-label">Email:</label>
			    <div class="col-sm-8">
			      <input type="text" class="form-control [required,custom[email]] cardholder" maxlength="128" id="email" name="email" value="#emailAddress#" data-value="#emailAddress#">
			    </div>
			  </div>
			  <br>
            	<hr>
            	
            	
            	
		    </div>
        </div>
        </fieldset>
        <div class="row">
            <div class="col-sm-12 form-horizontal form-footer text-center">
	        		<!---<button type="submit" class="btn btn-medium btn-success-custom btn-payment-credits pull-left">Make Payment</button>--->
	        		<a type="button" class="btn btn-medium green-gd pull-right"  href="javascript:history.go(-1)">Cancel</a>
	        		<a type="button" id="btnStep2" class="btn btn-medium green-gd  pull-right margin-right-5">Next</a>
	            	<a type="button" id="btnStep2_back" class="btn btn-medium green-cancel pull-right margin-right-5">Back</a>
	            	<div class="clearfix"></div>
	    	</div>
    	</div>
    </div>
</div>
<!---<div class="col-sm-11 col-md-6 col-md-offset-6 form-footer text-center">
	<button type="submit" class="btn btn-medium btn-success-custom btn-payment-credits pull-left">Make Payment</button>
	<a type="button" class="btn btn-medium btn-primary-custom pull-right"  href="javascript:history.go(-1)">Cancel</a>
</div>--->
</div>
</div>
</cfoutput >