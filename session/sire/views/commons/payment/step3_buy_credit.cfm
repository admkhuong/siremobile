<cfoutput >
<div id="step3" style="display:none">
	<div class="row">
		<div class="col-md-6">
			<div class="row heading">
        		<div class="col-sm-12 heading-title">Payment Information<hr></div>
    		</div>
			<div class="row secure-payment-plan">
				<div class="col-xs-4">Currency:</div>
				<div class="col-xs-8"><div class="pull-right active">$ US Dollars</div></div>
			</div>
			<div class="row secure-payment-plan">
				<div class="col-xs-4">Number of Credit:</div>
				<div class="col-xs-8"><div id="rsNumberofCredit" class="pull-right active">0</div></div>
			</div>
			<div class="row secure-payment-plan">
				<div class="col-xs-4">Amount:</div>
				<div class="col-xs-8"><div id="rsAmount" class="pull-right active">0</div></div>
			</div>
			<cfinclude template="billing_info.cfm" >
			<cfinclude template="btn_makepayment.cfm">
		</div>
		
		
	</div>
	
</div>
</cfoutput>