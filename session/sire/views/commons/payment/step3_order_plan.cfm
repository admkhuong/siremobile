<cfoutput >
<div id="step3" style="display:none">
	<div class="row">
		<div class="col-md-6">
			<div class="row heading">
        		<div class="col-sm-12 heading-title">Payment Information<hr></div>
    		</div>
			<div class="row secure-payment-plan">
				<div class="col-xs-4">Currency:</div>
				<div class="col-xs-8"><div class="pull-right active">$ US Dollars</div></div>
			</div>
			<div class="row secure-payment-plan">
				<div class="col-xs-4">Select Your Plan:</div>
				<div class="col-xs-8"><div id="rsSelectYourPlan" class="pull-right active">0</div></div>
			</div>
			<div class="row secure-payment-plan">
				<div class="col-xs-4">First Credits Include:</div>
				<div class="col-xs-8"><div id="rsFirstCreditsInclude" class="pull-right active">0</div></div>
			</div>
			<div class="row secure-payment-plan">
				<div class="col-xs-4">Number of Plan Keyword:</div>
				<div class="col-xs-8"><div id="rsNumberofPlanKeyword" class="pull-right active">0</div></div>
			</div>
			<div class="row secure-payment-plan">
				<div class="col-xs-4">Monthly Keyword Fee:</div>
				<div class="col-xs-8"><div id="rsMonthlyKeywordFee" class="pull-right active">0</div></div>
			</div>
			<div class="row secure-payment-plan">
				<div class="col-xs-4">Monthly Plan Price:</div>
				<div class="col-xs-8"><div id="rsMonthlyPlanPrice" class="pull-right active">0</div></div>
			</div>
			<div class="row secure-payment-plan">
				<div class="col-xs-4">Monthly Amount:</div>
				<div class="col-xs-8"><div id="rsAmount" class="pull-right active">0</div></div>
			</div>
	  		<cfinclude template="billing_info.cfm" >
			
			<cfinclude template="btn_upgrade_now.cfm">
		</div>
		
		
	</div>
	
</div>
</cfoutput>