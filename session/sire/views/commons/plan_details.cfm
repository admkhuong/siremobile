<ul class="nav list-plan-statistical">
<cfoutput>
    <li>Plan Type: <span>#userPlanName#</span></li>
    <li>Monthly Amount:  <span>$#NumberFormat(monthlyAmount,',')#</span></li>
    <li>Credits Available: 
        <span>
            <cfif RetValBillingData.Balance GT 0>
                #NumberFormat(RetValBillingData.Balance,',')#
            <cfelse>    
                0
            </cfif>
        </span>
    </li>
    <li>Keywords Available: 
        <span>
            <cfif RetUserPlan.KEYWORDAVAILABLE GT 0>
                #RetUserPlan.KEYWORDAVAILABLE#
            <cfelse>    
                0
            </cfif>
        </span>
    </li>
    <li>Keywords Purchased:
        <cfif UnsubscribeNumberMax GT 0>
            <a href="##" data-toggle="modal" data-target="##UnscriberKeywordModal" id="unsubscribe-keyword"><i class="glyphicon glyphicon-arrow-down" title="Unsubscribe keyword"></i></a>
        <cfelse>
            <a class="unsubscribe_disable"><i class="glyphicon glyphicon-arrow-down" title="Unsubscribe keyword"></i></a>
        </cfif>

        <span>
            <cfset keywordsPurchased = RetUserPlan.KEYWORDPURCHASED + RetUserPlan.KEYWORDBUYAFTEREXPRIED>
            <cfif keywordsPurchased GT 0>
                #keywordsPurchased#
            <cfelse>    
                0
            </cfif>
        </span>
    </li>
    <li>MLPs Available: <span>#((IsNumeric(RetUserPlan.MLPSLIMITNUMBER) AND RetUserPlan.MLPSLIMITNUMBER GT 0) ? NumberFormat(RetUserPlan.MLPSLIMITNUMBER-RetUsedMLP.usedMLP,',') : "Unlimited")#</span></li>
    <li>Short URLs Available: <span>#((IsNumeric(RetUserPlan.SHORTURLSLIMITNUMBER) AND RetUserPlan.SHORTURLSLIMITNUMBER GT 0) ? NumberFormat(RetUserPlan.SHORTURLSLIMITNUMBER-RetUsedShortURL.usedURL,',') : "Unlimited")#</span></li>
    <li>Friends Referred: 
        <span>
            <cfif RetValUserReferInfo.FriendsReferred GT 0>
                #RetValUserReferInfo.FriendsReferred#
            <cfelse>
                0
            </cfif>
        </span>
    </li>
    <li>Referral Credits Received: 
        <span>
            <cfif RetValUserReferInfo.PromoCreditsReceived GT 0>
                #RetValUserReferInfo.PromoCreditsReceived#
            <cfelse>
                0
            </cfif>
        </span>
    </li>
</cfoutput>
</ul>