<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUserPlan" returnvariable="RetUserPlan"></cfinvoke>

<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetOrderPlan"  returnvariable="RetPlan">
    <cfinvokeargument name="plan" value="#RetUserPlan.PLANID#">
</cfinvoke>

<!--- <cfsavecontent variable="variables.portleft">
    <div class="widget-free">
        <div class="widget-free-header">
            <h4><cfoutput>#userPlanName#</cfoutput></h4>
        </div>
        <div class="widget-free-body">
            <ul class="nav list-widget-free">
                <cfoutput>
                    <li><span>$#NumberFormat(planMonthlyAmount,',')#</span> / month</li>
                    <li>#RetPlan.USERACCOUNTNUMBER# User Account</li>
                    <li>#RetPlan.KEYWORDSLIMITNUMBER# User Reserved <span><a data-toggle="modal" id="what-is-keyword" title="What is a Keyword?" href="##" data-target="##InfoModalKeyword">Keywords</a></span></li>
                    <li>First #NumberFormat(RetPlan.FIRSTSMSINCLUDED,',')# SMS Included</li>
                    <li>#NumberFormat(RetPlan.PRICEMSGAFTER,'._')# Cents per Msg after</li>
                    <li>#((IsNumeric(RetPlan.MLPSLIMITNUMBER) AND RetPlan.MLPSLIMITNUMBER GT 0) ? NumberFormat(RetPlan.MLPSLIMITNUMBER,',') : "Unlimited")#<span><a data-toggle="modal" id="what-is-mlp" title="What is a MLP?"  href="##" data-target="##InfoModalMLP"> MLPs</a></span></li>
                    <li>#((IsNumeric(RetPlan.SHORTURLSLIMITNUMBER) AND RetPlan.SHORTURLSLIMITNUMBER GT 0) ? NumberFormat(RetPlan.SHORTURLSLIMITNUMBER,',') : "Unlimited")# Short URLs</li>
                </cfoutput>
            </ul>
        </div>
    </div>
</cfsavecontent> --->

<cfsavecontent variable="variables.portleft">
    <div class="portlet light bordered box-widget-sidebar ">
        <div class="portlet-body">
            <div class="ads-pro">
                <cfoutput>
                    <div class="head">
                        <b>#UCase(userPlanName)#</b>
                        <cfif IsDefined("userInfo")>
                            <cfif  userInfo.USERLEVEL EQ 3 AND userInfo.USERTYPE EQ 2>
                                <h4>Pay As You Go</h4>
                            </cfif>
                        <cfelse>
                            <h4><span>$#NumberFormat(planMonthlyAmount,',')#</span> / month</h4>
                        </cfif>
                    </div>
                    <div class="body">
                        <ul class="list-ads-pro">
                            <!--- <li>#RetPlan.USERACCOUNTNUMBER# User Account</li> --->
                            <li>First #NumberFormat(RetPlan.FIRSTSMSINCLUDED,',')# SMS Included</li>
                            <li>#RetPlan.KEYWORDSLIMITNUMBER# User Reserved <a href="##" data-toggle="modal" title="What is a Keyword?" data-target="##InfoModalKeyword">Keywords</a></li>
                            <li>#NumberFormat(RetPlan.PRICEMSGAFTER,'._')# Cents per Msg after</li>
                            <li>#((IsNumeric(RetPlan.MLPSLIMITNUMBER) AND RetPlan.MLPSLIMITNUMBER GT 0) ? NumberFormat(RetPlan.MLPSLIMITNUMBER,',') : "Unlimited")# <a href="##" data-toggle="modal" title="What is a MLP?" data-target="##InfoModalMLP">MLPs</a></li>
                            <li>#((IsNumeric(RetPlan.SHORTURLSLIMITNUMBER) AND RetPlan.SHORTURLSLIMITNUMBER GT 0) ? NumberFormat(RetPlan.SHORTURLSLIMITNUMBER,',') : "Unlimited")# Short URLs</li>
                        </ul>
                    </div>
                </cfoutput>
            </div>
        </div>
    </div>
</cfsavecontent>
