<cfparam name="Session.FIRSTNAME" default="">
<p class="welcome_title">Welcome <cfoutput>#Session.FIRSTNAME#</cfoutput>!</p>

<!--- remove old method: 
<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>								
--->
<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>

<cfif shortCode.SHORTCODE NEQ ''>
	<p class="sub-title">Your Short Code is <span class="active"><cfoutput>#shortCode.SHORTCODE#</cfoutput></span></p>	
<cfelse>
	<p class="sub-title"><a class="active" href="##">Set Keywords</a></p>
</cfif>
