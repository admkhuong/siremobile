<!-- What is a keyword Modal -->
<div id="InfoModalKeyword" class="modal be-modal fade" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>        
      </div>
      <div class="modal-body">
        <h4 class="be-modal-title">What is a Keyword?</h4>
        <p class="message-non-footer">
            Imagine if Joe’s Coffee Shop offers this promotion: Text “FreeCoffee” to 39492 and receive a free coffee on your next visit.  “FreeCoffee” is the Keyword.  It’s the unique word or code you create so your customers can respond to your campaign.  
        </p>
      </div>
    </div>

  </div>
</div>
<!--- end modal --->

<!-- what is a MLP Modal -->
<div id="InfoModalMLP" class="modal be-modal fade" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>        
      </div>
      <div class="modal-body">
        <h4 class="be-modal-title">What is a MLP?</h4>
        <p class="message-non-footer">
            Let’s say Pete’s Gym offered a promotion: Buy 5 group fitness classes and get 3 free! Click here to redeem coupon.  Once customers clicks on that link they will be sent to your own Marketing Landing Page (MLP) displaying the coupon.  It’s a standalone web page promoting your offer and the best part is it’s included for free!
        </p>
      </div>
    </div>
  </div>
</div>
<!--- end modal --->


<!--- Unsubscribe keyword Modal --->
<div class="bootbox modal fade" id="UnscriberKeywordModal" tabindex="-1" role="dialog" aria-labelledby="signOutLabel">
  
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title title">Unsubscribe Keyword</h4>            
            </div>
      <div class="modal-body">
                <form id="unscriber_keyword_form" class="row">
                    <label for="subscriber_list_name" class="col-sm-4">
                        Number Keyword:*
                    </label>
                    <div class="col-sm-8">
                        <input type="hidden" id="unsubscribe_number_max" name="unsubscribe_number_max" value="<cfoutput>#UnsubscribeNumberMax#</cfoutput>">
                        <input id="numberKeywordUnscriber" class="form-control validate[required,custom[onlyNumber],min[1],max[<cfoutput>#UnsubscribeNumberMax#</cfoutput>]]" maxlength="255">
                        <div style="color:red">You can Unsubscribe Max <cfoutput>#UnsubscribeNumberMax#</cfoutput> keywords</div>
                    </div>
                </form>
       </div>
        <div class="modal-footer">
                <button type="submit" class="btn btn btn-medium btn-success-custom" id="Unsubscribe"> Unsubscribe </button>
                <button type="button" class="btn btn btn-medium btn-back-custom" data-dismiss="modal">Cancel</button>
            </div>
    </div>
  </div>
</div>
<!--- End --->