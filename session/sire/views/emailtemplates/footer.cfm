<!--- <tfoot>
<tr style="text-align: center; color: #ffffff; background: #274e60; font-size: 12px;">
	        <td colspan="2" style="padding-top: 15px;  padding-bottom: 20px;">
	            Your privacy is very important to us. View our <a href="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>/public/sire/pages/privacy-policy" style="color:white"> privacy policy </a>
	            <br>
	            <address style="font-style:normal; color:white">4533 MacArthur Blvd, Suite 1090 Newport Beach, CA 92660 | (888) 747-4411 | <a href="mailto:support@siremobile.com" style="color:white">support@siremobile.com</a></address>
	        </td>
	    </tr>

	    <tr style="text-align: center; background-color: #274E60; ">
	        <td colspan="2" style="padding-bottom: 15px;">
	            <a href="https://www.facebook.com/siremobile/"><img src="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>/public/sire/images/emailtemplate/facebook.png?v=1.0" alt="fb"/></a>
	            <a href="https://twitter.com/sire_mobile"><img src="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>/public/sire/images/emailtemplate/twitter.png?v=1.0" alt="twt"></a>
	            <!--- <a href="https://www.pinterest.com/siremobile/"><img src="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>/public/sire/images/emailtemplate/pinterest.png" alt="ptr"></a>
	            <a href="https://www.instagram.com/siremobile/"><img src="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>/public/sire/images/emailtemplate/instagram.png" alt=""></a> --->
	            <a href="https://www.youtube.com/channel/UCYqFX_Uy0MIY5i7gjgr_LXg" target="_blank" title="Youtube"><img src="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>/public/sire/images/youtube.png?v=1.0" alt="Youtube"/></a>
                <a href="https://www.linkedin.com/company/12952559" target="_blank" title="Linkedin"><img src="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>/public/sire/images/linkedin.png?v=1.0" alt="LinkedIn"/></a>
	            <a href="https://plus.google.com/116619152084831668774"><img src="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>/public/sire/images/emailtemplate/g+.png?v=1.0" alt="g+"></a>
	        </td>
	    </tr>
</tfoot> --->

<tfoot>
	<tr style="text-align: center" bgcolor="#274e60">
		<td style="font-family: Arial; font-size: 12px; padding-left: 15px; color: white" colspan="2">
		<p>For support requests, please call or email us at:<br>
		(888) 747-4411 | <a href="mailto:support@siremobile.com" style="text-decoration: none; color: white">support@siremobile.com</a> | Text "<a style="color: white; text-decoration: none;"><strong>support</strong></a>" to <a style="color: white; text-decoration: none;"><strong>39492</strong></a></p>
		</td>
	</tr>

	<tr style="text-align: center" bgcolor="#274e60">
		<td width="100%" style="padding-bottom: 15px" colspan="2">
	    	<a href="https://www.facebook.com/siremobile/"><img src="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>/public/sire/images/emailtemplate/facebook.png?v=1.0" alt="fb"/></a>
	            <a href="https://twitter.com/sire_mobile"><img src="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>/public/sire/images/emailtemplate/twitter.png?v=1.0" alt="twt"></a>
	            <!--- <a href="https://www.pinterest.com/siremobile/"><img src="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>/public/sire/images/emailtemplate/pinterest.png" alt="ptr"></a>
	            <a href="https://www.instagram.com/siremobile/"><img src="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>/public/sire/images/emailtemplate/instagram.png" alt=""></a> --->
	            <a href="https://www.youtube.com/channel/UCYqFX_Uy0MIY5i7gjgr_LXg" target="_blank" title="Youtube"><img src="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>/public/sire/images/youtube.png?v=1.0" alt="Youtube"/></a>
                <a href="https://www.linkedin.com/company/12952559" target="_blank" title="Linkedin"><img src="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>/public/sire/images/linkedin.png?v=1.0" alt="LinkedIn"/></a>
	            <a href="https://plus.google.com/116619152084831668774"><img src="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>/public/sire/images/emailtemplate/g+.png?v=1.0" alt="g+"></a>
	    </td>
	</tr>
</tfoot>