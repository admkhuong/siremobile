<cfset var data	= '' />
<cfset data = deserializeJSON(arguments.data)>

<cfoutput >

<body style="font-family:sans-serif;font-size:12pt;">
<p>Hi #data.REPORT.INPFIRSTNAME#,</p>
<p>Great news.  Upon review, your transaction has been approved and your account<br>has been updated!  Feel free to log in and continue building your awesome<br>campaigns.  If you have any additional questions, we’re only an email away: <br><a href="mailto:support@siremobile.com"> support@siremobile.com </a></p>

<p><b>_________ PAYMENT INFORMATION ___________</b></p>
<cfif data.REPORT.numberSMS GT 0>
	<p>Number of Credit: #data.REPORT.numberSMS#</p>
</cfif>
<cfif data.REPORT.numberKeyword GT 0>
	<p>Number of Keyword: #data.REPORT.numberKeyword#</p>
</cfif>

<p>Amount : $#data.REPORT.AMT#</p>
<p>Account (last 4): #data.REPORT.INPNUMBER#</p>
<p>Transaction ID : #data.REPORT.TRANSACTIONID#</p>

<p><b>_____ CUSTOMER BILLING INFORMATION ____</b></p>

<p>First Name : #data.REPORT.INPFIRSTNAME# </p>
<p>Last Name : #data.REPORT.INPLASTNAME#</p>
<p>Address :  #data.REPORT.INPLINE1#</p>
<p>City : #data.REPORT.INPCITY#</p>
<p>State/Province : #data.REPORT.INPSTATE#</p>
<p>Zip/Postal Code : #data.REPORT.INPPOSTALCODE#</p>
<!--- <p>Country : #data.report.INPSTATE#</p> --->
<!---<p>Phone : #data.transaction.billAddress.phone#</p>--->
<p>E-Mail : #data.REPORT.EMAIL#</p> 
<br/>
<p>Thanks,</p>
<p>SIRE Customer Support</p>
</body>                        
                            
</cfoutput>