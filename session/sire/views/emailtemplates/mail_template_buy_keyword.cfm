<cfset var data	= '' />
<cfset data = deserializeJSON(arguments.data)>

<cfoutput >

<body style="font-family:sans-serif;font-size:12pt;">
<p>Dear #data.transaction.cardHolder_FirstName# #data.transaction.cardHolder_LastName#,</p>

<p><b>_________ PAYMENT INFORMATION ___________</b></p>
<p>Number of Keywords: #form.numberKeyword#</p>
<p>Amount : $#data.transaction.authorizedAmount#</p>
<p>Account: #data.transaction.cardNumber#</p>
<p>Transaction ID : #data.transaction.transactionId#</p>

<p><b>_____ CUSTOMER BILLING INFORMATION ____</b></p>

<p>First Name : #data.transaction.cardHolder_FirstName# </p>
<p>Last Name : #data.transaction.cardHolder_LastName#</p>
<p>Address : #data.transaction.billAddress.line1#</p>
<p>City : #data.transaction.billAddress.city#</p>
<p>State/Province : #data.transaction.billAddress.state#</p>
<p>Zip/Postal Code : #data.transaction.billAddress.zip#</p>
<p>Country : #data.transaction.billAddress.country#</p>
<!---<p>Phone : #data.transaction.billAddress.phone#</p>--->
<p>E-Mail : #data.transaction.email#</p> 
<br/>
<p>Thanks,</p>
<p>SIRE Assistance Team!</p>
<p> Website: <a href="https://siremobile.com"> https://siremobile.com </a> </p>
<p> Phone: #_SIREPhoneNumber# </p>
<p> Email: <a href="mailto:support@siremobile.com"> support@siremobile.com </a> </p>
</body>                        
                            
</cfoutput>