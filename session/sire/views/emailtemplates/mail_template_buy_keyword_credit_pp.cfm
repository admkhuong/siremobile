<cfset var data	= '' />
<cfset data = deserializeJSON(arguments.data)>

<cfoutput >

<body style="font-family:sans-serif;font-size:12pt;">
<p>Dear #data.FullName#,</p>

<p><b>_________ PAYMENT INFORMATION ___________</b></p>

<cfif data.numberKeywords GT 0>
	<p>Number of Keywords: #data.numberKeywords#</p>	
</cfif>

<cfif data.numberSMS GT 0>
	<p>Number of Credits: #data.numberSMS#</p>
</cfif>

<p>Amount : $#data.authorizedAmount#</p>
<p>Transaction ID : #data.transactionId#</p>
<br/>
<p>Thanks,</p>
<p>SIRE Assistance Team!</p>
<p> Website: <a href="https://siremobile.com"> https://siremobile.com </a> </p>
<p> Phone: #_SIREPhoneNumber# </p>
<p> Email: <a href="mailto:support@siremobile.com"> support@siremobile.com </a> </p>
</body>                        
                            
</cfoutput>