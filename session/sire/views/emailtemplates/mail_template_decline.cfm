<cfset var data	= '' />
<cfset data = deserializeJSON(arguments.data)>

<cfoutput >

<body style="font-family:sans-serif;font-size:12pt;">
<p>Hi #data.inpFirstName#,</p>
<p>Upon reviewing your order, we are unable to process your transaction.  Some<br>common issues that may have caused this include:</p>
<ul>
	<li>Account address and credit card address do not match</li>
	<li>Invalid credit card numbers, expiration or CVV</li>
	<li>Name on the card does not match the name on the account</li>
</ul>
<br/>
<p>If you have any questions about this or feel this decision was made in error, please<br>reach out to our Customer Support team at <a href="mailto:support@siremobile.com"> support@siremobile.com </a>.You may<br>also try to place an order using a different valid card.</p>
<br/>
<p>Sincerely,</p>
<br/>
<p>Sire Customer Support</p>
</body>                        
                            
</cfoutput>