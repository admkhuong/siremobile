<cfset var content	= '' />
<cfset content = deserializeJSON(arguments.data)/>
<cfset content.ROOTURL_HTTP = "https://#CGI.SERVER_NAME#"/>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Notify Trouble Ticket Change Status</title>
	</head>

	<body style="font-family: Arial; font-size: 12px;">
        <table style="width: 600px; margin: 0 auto;" cellpadding="0" cellspacing="0">
        <cfinclude template="/public/sire/views/emailtemplates/header.cfm">
            <tbody style="margin: 0; padding: 0;">
                <tr>
                    <td colspan="2" style="margin: 0; padding: 0;">
                        <div style="text-align: center;">
                            <img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/monthly-billing-email.jpg" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0 30px;">
                        <p style="font-size: 16px;">The ticket ID: <b><cfoutput>#content.TROUBLETICKETID#</cfoutput>.</b></p>
                        <p style="font-size: 14px;">Assign to: <cfoutput>#content.FULLNAME#</cfoutput>.</p>
                        <p style="font-size: 14px;">Just has changed status to <cfoutput>#content.STATUS#</cfoutput>.</p>
                        <p style="font-size: 16px;">
                            Thanks,<br>
                            SIRE Support
                        </p>
                    </td>
                </tr>	
            </tbody>
        <cfinclude template="/public/sire/views/emailtemplates/footer.cfm">
        </table>
    </body>

</html>