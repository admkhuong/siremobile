
<cfif arguments.data NEQ ''>
	<cfset data = deserializeJSON(arguments.data)>
</cfif>
<cfoutput >

<body style="font-family:sans-serif;font-size:12pt;">
<p>Dear #data.FullName#,</p>
<p>Great news.  Upon review, your transaction has been approved and your account<br>has been updated!  Feel free to log in and continue building your awesome<br>campaigns.  If you have any additional questions, we’re only an email away: <br><a href="mailto:support@siremobile.com"> support@siremobile.com </a></p>

<p><b>_________ PAYMENT INFORMATION ___________</b></p>
<p>Select Your Plan: #data.planName#</p>

<p>Amount : $#data.authorizedAmount#</p>
<p>Transaction ID : #data.transactionId#</p>


<p>Thanks,</p>
<p>SIRE Assistance Team!</p>
<p> Website: <a href="https://siremobile.com"> https://siremobile.com </a> </p>
<p> Phone: #_SIREPhoneNumber# </p>
<p> Email: <a href="mailto:support@siremobile.com"> support@siremobile.com </a> </p>
</body>                        
                            
</cfoutput>