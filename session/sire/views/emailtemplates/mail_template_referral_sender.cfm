<cfset var envServer = CGI />

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Invite Email</title>
</head>
<body style="margin: 0; padding: 0; font-family: Arial">
	<table cellpadding="0" cellspacing="0" style="width: 600px; margin: 0 auto;">

	    <cfinclude template="header.cfm">

	    <tr>
	        <td colspan="2">
	        	<img src="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>/public/sire/images/emailtemplate/banner-2.jpg"  alt="banner">
	        </td>
	    </tr>
	    <tr>
	        <td colspan="2" style="font-size: 16px; padding: 0 30px; color:#5c5c5c">
	            <br>
	            Dear <cfoutput>#dataEmail.senderFistName#</cfoutput>,
	            <br><br>
	            Congratulations <cfoutput>#dataEmail.senderName#</cfoutput>! You will receive 1,000 credits next month for inviting your friend, who signed up with Sire!
	            <br><br>
	            Refer more friends and earn more credits for every friend who signs up with Sire! It's that easy! <a href="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>" style="color: #578da6; text-decoration: none;">Sign in</a> to your Sire account now and invite more friends!
	            <br><br>
	            <p class="about-sire" style="font-size:13px;">
	            	<strong>About Sire</strong>: Sire is a mobile CRM platform that is used to communicate information through sms/text to customers and group members. Please browse through our <a href="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>" style="color: #578da6; text-decoration: none;">website</a> for more information.
	            <br><br><br>
	            </p>
	        </td>
	    </tr>
		<cfinclude template="footer.cfm">
	</table>
</body>
</html>

