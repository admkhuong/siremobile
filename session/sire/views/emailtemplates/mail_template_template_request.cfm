<cfset var content	= {} />
<cfset content = deserializeJSON(arguments.data)>
<cfset content.ROOTURL_HTTP = "https://#CGI.SERVER_NAME#">

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>SIRE template request</title>
	</head>

	<body style="font-family: Arial; font-size: 16px">
		<table style="width: 570px; margin: 0 auto; color:#5c5c5c;" cellpadding="0" cellspacing="0" align="center">
			    <tr>
			    	<td>
			    		<table width="100%">
			    			<tr>
			    				<td width="40%">
									<a href="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>"><img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/logo.jpg" alt=""></a>
								</td>
						     	<td style="text-align: right; color: #5c5c5c; font-size: 12px;" align="right">
									<p>Add <a href="mailto:support@siremobile.com">support@siremobile.com</a> to your contacts</p>
								</td>
			    			</tr>
			    		</table>
			    	</td>
			    </tr>

				<tr>
					<td>
						<table width="100%">
							<tr>
								<td width="100%" style="margin: 0; padding-left: 2%;font-size: 16px">
									<br><br>
									Hi <cfoutput>#content.USERFIRSTNAME#</cfoutput>,<br><br>

									Thank you for requesting a new template with Sire. We will be responding to you shortly so we can review your requirements in more details. In the meantime, your request ticket # is <cfoutput>#content.Ticket#</cfoutput>.

									<br><br><br><br>
									Best Regards,<br>
									Andy<br>
									Customer Success Manager
									<br><br><br>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr bgcolor="#274e60">
					<td>
						<table width="100%">
							<tr style="text-align: center">
								<td style="font-family: Arial; font-size: 12px; padding-left: 15px; padding-top: 15px; padding-bottom: 15px; color: white" width="70%">
								<p>For support requests, please call or email us at:<br>
								(888) 747-4411 | <a href="mailto:support@siremobile.com" style="text-decoration: none; color: white">support@siremobile.com</a> | Text "<a style="color: white; text-decoration: none;"><strong>support</strong></a>" to <a style="color: white; text-decoration: none;"><strong>39492</strong></a></p>
								</td>
							</tr>

							<tr style="text-align: center">
								<td width="100%" style="padding-bottom: 15px">
							    	<a style="padding-right: 7px" href="https://www.facebook.com/siremobile/"><img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/facebook.png?v=1.0" alt="fb"/></a>
						            <a style="padding-right: 7px" href="https://twitter.com/sire_mobile"><img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/twitter.png?v=1.0" alt="twt"></a>
						            <a style="padding-right: 7px" href="https://www.youtube.com/channel/UCYqFX_Uy0MIY5i7gjgr_LXg" target="_blank" title="Youtube"><img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/youtube.png?v=1.0" alt="Youtube"/></a>
			    					<a style="padding-right: 7px" href="https://www.linkedin.com/company/12952559" target="_blank" title="Linkedin"><img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/linkedin.png?v=1.0" alt="LinkedIn"/></a>
						            <a href="https://plus.google.com/116619152084831668774"><img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/g+.png?v=1.0" alt="g+"></a>
							    </td>
							</tr>

						</table>
					</td>
				</tr>
		</table>
	</body>

</html>

