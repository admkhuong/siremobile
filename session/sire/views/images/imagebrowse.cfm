<cfinvoke method="BrowseImage" component="session.sire.models.cfc.image" returnvariable="retValBrowseImage">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/css/image-browse.css">
</cfinvoke>

<div class="upload-container" id="imageListContainer">
	<div class="row heading">
		<div class="col-sm-6 page-title" style="padding-left: 25px">Uploaded Images</div>
	</div>
	<div class="row image-list" id="image-list">
		<cfif retValBrowseImage.RXRESULTCODE GT 0>
			<cfif arrayLen(retValBrowseImage.USERIMAGES) LT 1>
				<cfoutput>
					You haven't upload any image yet
				</cfoutput>
			<cfelse>
				<cfloop array="#retValBrowseImage.USERIMAGES#" index="image">
					<cfoutput>
						<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 image-list-item">
							<img class="image" data-url="#image.IMAGEURL#" src="#image.IMAGETHUMB#" alt="#image.IMAGENAME#">
							<button class="btn btn-small btn-danger image-delete" data-imageid="#image.IMAGEID#">Delete</button>
						</div>
					</cfoutput>
				</cfloop>
			</cfif>
		</cfif>
	</div>
</div>