<cfif !isDefined('variables._Response')>
	<cfset variables._PageContext = GetPageContext()>
	<!--- Coldfusion --->
	<!---<cfset variables._Response = variables._PageContext.getCFOutput()>--->
	<cfset variables._Response = variables._PageContext.getOut()>
</cfif>
<cfset variables._page = variables._Response.getString()>
<cfset variables._Response.clear()>
<cfparam name="variables._page" default="">
<cfparam name="variables._title" default="">
<cfparam name="variables.meta_description" default="">
<cfparam name="Request._css" default="[]">
<cfparam name="Request._js" default="[]">
<cfoutput>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>#variables._title#</title>
	<meta name="robots" content="index, follow">
	<meta name="description" content="#replaceList(variables.meta_description, '<,>,"', '&lt;,&gt;,&quot;')#">
	<meta name="viewport" id="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=10.0,initial-scale=1.0">
	<link href="/public/sire/css/bootstrap.css" rel="stylesheet">
	<link href="/public/sire/css/validationEngine.jquery.css" rel="stylesheet">

	<link href="/session/sire/css/style.css" rel="stylesheet">
	<link href="/session/sire/css/screen.css" rel="stylesheet">

	<cfif (isDefined("Request._css") && isArray(Request._css))>
		<cfloop array="#Request._css#" index="cssPath">
    		<link href="#cssPath#" rel="stylesheet">
		</cfloop>
		<cfset Request._css=[]>
	</cfif>

    <!--- http://realfavicongenerator.net/favicon?file_id=p1a4qpcvhn1jltrkukhpvas1f206#.VlNZzHunXvA  --->
    <link rel="apple-touch-icon" sizes="57x57" href="/public/sire/images/sireicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/public/sire/images/sireicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/public/sire/images/sireicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/public/sire/images/sireicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/public/sire/images/sireicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/public/sire/images/sireicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/public/sire/images/sireicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/public/sire/images/sireicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/public/sire/images/sireicons/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/public/sire/images/sireicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/public/sire/images/sireicons/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/public/sire/images/sireicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/public/sire/images/sireicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/public/sire/images/manifest.json">
    <link rel="mask-icon" href="/public/sire/images/sireicons/safari-pinned-tab.svg" color="##5bbad5">
    <meta name="msapplication-TileColor" content="##da532c">
    <meta name="msapplication-TileImage" content="/public/sire/images/sireicons/mstile-144x144.png">
    <meta name="theme-color" content="##ffffff">

</head>	
<body>
	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5LLR72"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push(
	{'gtm.start': new Date().getTime(),event:'gtm.js'}
	);var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5LLR72');</script>
	<!-- End Google Tag Manager -->
	
	<!-- HEADER SECTION
	=============================================================-->
	<cfinclude template="../commons/header_public.cfm" >

	<!-- BODY SECTION
	=============================================================-->
	<div id="page-content" class="" style="left: 0px;">
	#variables._page#


	<!-- FOOTER SECTION
	=============================================================-->
	<cfinclude template="../commons/footer.cfm">
	</div>

	<script src="/public/sire/js/jquery-2.1.4.js"></script>
	<script src="/public/sire/js/bootstrap.js"></script>
	<!---
	<script src="/public/sire/js/retina.js"></script>
	--->
	<script src="/public/sire/js/jquery.validationEngine.en.js"></script>
	<script src="/public/sire/js/jquery.validationEngine.js"></script>
	<script src="/public/sire/js/bootbox.min.js"></script>
	<script src="/public/js/datatables/js/jquery.dataTables.min.js"></script>

	<script src="/session/sire/js/site/menu.js"></script>
	<script src="/session/sire/js/site/sign_in.js"></script>
	<script src="/session/sire/js/dataTables.inputPagination.js"></script>
	<script src="/session/sire/js/site/common.js"></script>

	<cfif (isDefined("Request._js") && isArray(Request._js))>
		<cfloop array="#Request._js#" index="jsPath">
			<script src="#jsPath#"></script>
		</cfloop>
		<cfset Request._js=[]>
	</cfif>
	<!-- Integrate Live Chat -->
	<cfinclude template="../../../../public/sire/views/commons/tawkto_script.cfm">
</body>
</html>
</cfoutput>