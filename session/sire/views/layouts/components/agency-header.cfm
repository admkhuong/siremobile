<!--- fixed header overlay for Agency info --->


<!--- 0 Current session user is not an agency GT 0 means this user an Agency --->
<cfparam name="Session.AgencyId" default="0">
<cfparam name="Session.AgencyManagerId" default="0">
<cfparam name="Session.AgencyShortDesc" default="">

<!---
     0 is regular Sire User - GT 0 Client is under specified agancy.
     Client can only be member of one agency
--->
<cfparam name="Session.ClientOfAgencyId" default="0">

<!---
     0 is not a impersonation right now
     is this session an impersonation of a client by an agency - if so store that agency Id here
--->
<cfparam name="Session.ImpersonateClientAsAgencyId" default="0">

<!---
     0 is not a impersonation right now
     is this session an impersonation of a client by an agency - if so store the Sire UserId of the Manager for agency Id here
--->
<cfparam name="Session.ImpersonateClientAsAgencyManagerUserId" default="0">



<cfif Session.AgencyId GT 0 AND Session.AgencyManagerId GT 0>
     <div  class="navbar-fixed-top no-print" style="text-align:center; color: #5c5c5c; z-index:10001; font-size:.8em; margin-top:-1px;">

          <span style="background-color:#fff; padding: 0px .5em .2em .5em; border-radius: 0 0 8px 8px; box-shadow: 0px 0px 1px 1px rgba(255, 255, 255, 0.4);">
               <cfoutput><b>Agency</b> <i>#Session.AgencyShortDesc#</i></cfoutput>
          </span>
     </div>

<cfelseif Session.ImpersonateClientAsAgencyId GT 0 AND Session.ImpersonateClientAsAgencyManagerUserId GT 0  >

     <div  class="navbar-fixed-top no-print" style="text-align:center; color: #5c5c5c; z-index:10001; font-size:.8em; margin-top:-1px;">

          <span style="background-color:#fff; padding: 0px .5em .2em .5em; border-radius: 0 0 8px 8px; box-shadow: 0px 0px 1px 1px rgba(255, 255, 255, 0.4);">
               <cfoutput><a href="/session/sire/pages/agency-unwind-impersonate" id="UnwindImpersonateLink"><b>Impersonating from</b> <i>#Session.ImpersonateClientAsAgencyId#</i> <i>#Session.ImpersonateClientAsAgencyManagerUserId#</i></a></cfoutput>
          </span>
     </div>

</cfif>

<!---

<cfset Session.AgencyId = "0">
<cfset Session.AgencyManagerId = "0">
<cfset Session.AgencyShortDesc = "">

Agency Id (#Session.AgencyId#)  Agency Manger Id (#Session.AgencyManagerId#)

 --->


<!--- background-color: #dedede; --->
