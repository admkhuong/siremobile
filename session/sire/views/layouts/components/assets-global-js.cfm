<!--- <script src="/session/sire/assets/global/plugins/jquery.min.js" type="text/javascript"></script> --->
<script src="/session/sire/assets/global/plugins/jquery-3.3.1.min.js" type="text/javascript"></script>
<script src="/session/sire/assets/global/plugins/bootstrap/js/bootstrap.min-v3.3.7.js" type="text/javascript"></script>
<script src="/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>
<script src="/session/sire/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="/session/sire/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="/session/sire/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/session/sire/assets/global/plugins/jquery-mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
<script src="/session/sire/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<!--- <script src="/session/sire/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script> --->
<script src="/session/sire/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="/session/sire/assets/global/plugins/bootstrap-select/js/bootstrap-select.js" type="text/javascript"></script>
<script src="/public/sire/js/bootbox.min.js"></script>
<script src="/public/sire/js/jquery.mask.min.js" type="text/javascript"></script>

<!--- BEGIN SYSTEM CUSTOMIZED JS  --->
<script src="/session/sire/assets/pages/scripts/signout.js" type="text/javascript"></script>
<!--- END --->
<script src="/session/sire/js/site/menu.js"></script>
<script src="/session/sire/js/site/sign_in.js"></script>
<script src="/session/sire/js/site/common.js"></script>

<!--- BEGIN VALIDATION --->
<script src="/public/sire/js/jquery.validationEngine.en.js"></script>
<script src="/public/sire/js/jquery.validationEngine.js"></script>
<!--- END VALIDATION --->

<!-- BEGIN DATATABLE -->
<script src="/public/js/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="/session/sire/js/dataTables.inputPagination.js" type="text/javascript"></script>
<!-- END DATATABLE -->

<script src="/session/sire/assets/global/scripts/functions.js" type="text/javascript"></script>
<script src="/session/sire/assets/global/scripts/top-menu.js" type="text/javascript"></script>


