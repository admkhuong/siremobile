<!--- <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> --->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i,900,900i" rel="stylesheet"  media="screen" >
<!---<link href="/session/sire/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"  media="screen"  />--->
<link href="/session/sire/assets/global/plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"  media="screen"  />
<link href="/session/sire/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"  media="screen"  />
<link href="/session/sire/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"   media="screen" />
<!--- <link href="/session/sire/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" /> --->
<link href="/session/sire/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"  media="screen" />
<link href="/public/sire/css/validationEngine.jquery.css" rel="stylesheet" media="screen">
<link href="/session/sire/assets/global/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" media="screen">
<link href="/session/sire/assets/global/plugins/jquery-mcustomscrollbar/jquery.mCustomScrollbar.css" rel="stylesheet" media="screen">
<link href="/public/js/jquery-ui-1.11.4.custom/jquery-ui.css" rel="stylesheet" media="screen">
<link href="/session/sire/css/print.css?_=2.5.1.5" rel="stylesheet" media="print">
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN LEVEL PLUGINS -->
<cfif (isDefined("Request._css_plugin") && isArray(Request._css_plugin))>
    <cfloop array="#Request._css_plugin#" index="cssPath">
        <cfoutput><link href="#cssPath#" rel="stylesheet"></cfoutput>
    </cfloop>
    <cfset Request._css_plugin=[]>
</cfif>
<!-- END LEVEL PLUGINS -->

<!-- BEGIN THEME GLOBAL STYLES -->
<link href="/session/sire/assets/global/css/components-rounded.min.css" rel="stylesheet" id="style_components" type="text/css"  media="screen" />
<link href="/session/sire/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css"  media="screen" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="/session/sire/assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css"  media="screen" />
<link href="/session/sire/assets/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color"  media="screen"  />
<link href="/session/sire/assets/layouts/layout4/css/custom.css" rel="stylesheet" type="text/css"  media="screen" />
<link href="/session/sire/assets/layouts/layout4/css/custom1.css" rel="stylesheet" type="text/css"  media="screen" />
<link href="/session/sire/assets/layouts/layout4/css/custom2.css" rel="stylesheet" type="text/css"  media="screen" />