<cfparam name="Session.FIRSTNAME" default="">
<cfparam name="currentUserCredits" default="0">
<cfparam name="currentUserPlanName" default="Free">
<cfparam name="TriggerBillingWarning" default="0"> <!--- Allow URL trigger of balance low warning - for QA --->


<!--- remove old method: 
<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>								
--->
<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>
<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUserPlan" returnvariable="RetUserPlan"></cfinvoke> 
<cfinvoke component="session.sire.models.cfc.billing"  method="GetBalanceCurrentUser"  returnvariable="RetValBillingData"></cfinvoke>
<cfinvoke component="session.sire.models.cfc.users" method="getListShortCodes" returnvariable="resultSetListShortCodes"></cfinvoke>

<cfif RetValBillingData.RXRESULTCODE GT 0>
    <cfif RetValBillingData.Balance LT 1000000 >
        <cfset currentUserCredits = NumberFormat(RetValBillingData.Balance,',')>
     <cfelseif RetValBillingData.Balance LT 1000000000>
        <cfset currentUserCredits = NumberFormat(RetValBillingData.Balance \ 1000000,',') & " M">
     <cfelse>   
        <cfset currentUserCredits = NumberFormat(RetValBillingData.Balance \ 1000000000,',') & " B">
    </cfif>    
</cfif>

<cfif RetUserPlan.RXRESULTCODE GT 0>
    <cfset currentUserPlanName = RetUserPlan.PLANNAME />
</cfif>

    
<!--- Better user feedback on plan credits --->

<!--- Javscript is at bottom of master.cfm page after jquery has been added --->
        
<!--- Set default value --->
<cfset BillingPercentageOfPlan = 0 />

<!--- Make sure no divide by 0 errors --->
<cfif RetUserPlan.FIRSTSMSINCLUDED GT 0>
    <cfset BillingPercentageOfPlan = ( LSParseNumber(RetValBillingData.Balance) / RetUserPlan.FIRSTSMSINCLUDED) * 100 /> 
</cfif>    

<cfif BillingPercentageOfPlan LT 50 AND TriggerBillingWarning NEQ 1>
    
    <cfif !StructKeyExists(Cookie,"BillingLowWarning")>
        
        <cfset TriggerBillingWarning = 1 />
        
    </cfif>
    
    <!--- Dont annoy users with pop-under more than once per day expires=1 --->
    <cfcookie name="BillingLowWarning" value=1 expires=1 />
</cfif>
