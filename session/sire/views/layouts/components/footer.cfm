 <div class="page-footer-inner">
    <div class="social-link">
        <ul>

            <li> <a href="https://www.facebook.com/siremobile/" target="_blank" title="Facebook"><img src="/session/sire/assets/layouts/layout4/img/social/facebook.png?v=1.0" alt="Facebook"></a></li>
            <li> <a href="https://twitter.com/sire_mobile" target="_blank" title="Twitter"><img src="/session/sire/assets/layouts/layout4/img/social/twitter.png?v=1.0" alt="Twitter"></a></li>
            <li> <a href="https://www.youtube.com/channel/UCYqFX_Uy0MIY5i7gjgr_LXg" target="_blank" title="Youtube"><img src="/session/sire/assets/layouts/layout4/img/social/youtube.png?v=1.0" alt="Youtube"></a></li>
            <li> <a href="https://www.linkedin.com/company/12952559" target="_blank" title="Linkedin"><img src="/session/sire/assets/layouts/layout4/img/social/linkedin.png?v=1.0" alt="LinkedIn"></a></li>
            <li> <a href="https://plus.google.com/116619152084831668774" target="_blank" title="Google+"><img src="/session/sire/assets/layouts/layout4/img/social/google_plus.png?v=1.0" alt="Twitter"></a></li>
        </ul>
    </div>
    <div class="foot-link">
        <ul>
            <li><a href="/about-us">About Us</a></li>
            <li><a href="/term-of-use">Terms of Use</a></li>
            <li><a href="/privacy-policy">Privacy Policy</a></li>
            <li><a href="/anti-spam">Anti-Spam Policy</a></li>
        </ul>
    </div>
    <div class="sire-coppy">
        <span>&copy; Sire, Inc. 2000-2018</span>
    </div>
</div>
<div class="scroll-to-top left-scroll-to-top">
    <i class="icon-arrow-up"></i>
</div>

<div id="processingPayment">
    <img src="/public/sire/images/loading.gif" class="ajax-loader"/>
</div>
