<!---Verify whether to show dev features in production see - session.sire.models.cfc.admin method GetDevPermission to add your account user id for dev and testing --->
<cfinvoke method="GetDevPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetDevPermission" />
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission"><cfinvokeargument name="inpSkipRedirect" value="1"></cfinvoke>
<!--- Start rewrite menu canvas --->
<cfparam name="inpTemplateFlag" default="0"/>

 <ul class="page-sidebar-menu no-print" data-keep-expanded="false" data-auto-scroll="false" data-slide-speed="200">

	<li class="top-menu top-menu-small">

		<div class="page-top">

	   		 <cfinclude template="top-menu-2.cfm">
	    </div>
	</li>

	<!--- <li class="nav-item user-dashboard">
	    <a href="/session/sire/pages/dashboard" class="nav-link nav-toggle" style="border-top:none;">
	   <span class="title"><i class="fa fa-arrow-left" aria-hidden="true"></i>Dashboard</span>
	    </a>
	</li> --->

	<li class="nav-item account-li hide-collapsed">
        <a href="##" class="nav-link nav-toggle">
            <span class="title"><cfoutput>#Session.FIRSTNAME#</cfoutput><cfif LEN(TRIM(Session.FIRSTNAME)) EQ 0 >MY ACCOUNT</cfif></span>
            <i class="fa fa-caret-down" aria-hidden="true"></i>
        </a>
        <ul class="sub-menu">
	        <li class="nav-item account-li">
		        <a href="/session/sire/pages/account" class="nav-link nav-toggle">
		            <span class="title">Account</span>
		        </a>
		    </li>
            <cfif userInfo.USERTYPE EQ 2 AND userInfo.USERLEVEL EQ 2>
                <li class="nav-item sub-account-li">
                    <a href="/session/sire/pages/sub-account" class="nav-link nav-toggle">
                        <span class="title">Sub Account</span>
                    </a>
                </li>
            </cfif>
            <li class="nav-item profile-li">
		        <a href="/session/sire/pages/profile" class="nav-link nav-toggle">
		            <span class="title">Profile</span>
		        </a>
		    </li>
		    <li class="nav-item my-plan-li order-plan-li buy-sms-li buy-keyword-li invite-friend-li">
		        <a href="/session/sire/pages/my-plan" class="nav-link nav-toggle">
		            <span class="title">My Plan: <i><cfoutput>#currentUserPlanName#</cfoutput></i></span>
		        </a>
		    </li>
		    <li class="nav-item billing-li">
		        <a href="/session/sire/pages/billing" class="nav-link nav-toggle">
		            <span class="title">Billing</span>
		        </a>
		    </li>

		    <li class="nav-item support support-li">
		        <a href="/session/sire/pages/support" class="nav-link nav-toggle">
		            <span class="title">Support</span>
		        </a>
		    </li>
            <li class="nav-item message-li">
                <a href="/session/sire/pages/message" class="nav-link nav-toggle">
                    <span class="title">Messages</span>
                </a>
            </li>
            <li class="nav-item affiliates-dashboard-li">
                <a href="/session/sire/pages/affiliates-dashboard" class="nav-link nav-toggle">
                    <span class="title">Affiliate</span>
                </a>
            </li>
		    <li class="nav-item user-signout">
		        <a href="javascript:void(0)" class="nav-link nav-toggle">
		            <span class="title">Sign Out</span>
		        </a>
		    </li>

        </ul>
    </li>
</ul>

	<cfif hfe eq 0>

		<div style="text-align: center; color: #5c5c5c; margin: 0em; font-size: .8em;">
	    	 	<p class="hidden-sm hidden-xs">Drag a Step or Action to insert into conversation flow</p>
	     </div>

		 <!--- Left Menu for CP Objects --->
		 <div id="CPObjMenu">

			 <div class="row">
				 <div class="col-xs-6 col-sm-6">
					 <div class="cpobj-item draggable cpobj-statement"> <i class="fa fa-comments cpobj-item-icon"></i> <div class="cpobj-item-label">Statement</div> </div>
				 </div>

				 <div class="col-xs-6 col-sm-6">
					 <div class="cpobj-item draggable cpobj-question"> <i class="fa fa-question cpobj-item-icon"></i> <div class="cpobj-item-label">Question</div> </div>
				 </div>
			 </div>

			 <div class="row">
				 <div class="col-xs-6 col-sm-6">
					 <div class="cpobj-item draggable cpobj-rules-engine"> <i class="fa fa-cogs cpobj-item-icon"></i> <div class="cpobj-item-label">Rules Engine</div> </div>
				 </div>

				 <div class="col-xs-6 col-sm-6 ">
					 <div class="cpobj-item draggable cpobj-api"> <i class="fa fa-link cpobj-item-icon"></i> <div class="cpobj-item-label">API</div> </div>
				 </div>
			 </div>

			 <div class="row">
				 <div class="col-xs-6 col-sm-6">
					 <div class="cpobj-item draggable cpobj-trailer"> <i class="fa fa-columns cpobj-item-icon"></i> <div class="cpobj-item-label">Trailer</div> </div>
				 </div>

				 <div class="col-xs-6 col-sm-6">
					 <div class="cpobj-item draggable cpobj-cdf"> <i class="fa fa-user cpobj-item-icon"></i> <div class="cpobj-item-label">CDF</div> </div>
				 </div>
			 </div>

			 <div class="row">
				 <div class="col-xs-6 col-sm-6">
					 <div class="cpobj-item draggable cpobj-interval"> <i class="far fa-hourglass cpobj-item-icon"></i> <div class="cpobj-item-label">Interval</div> </div>
				 </div>

				 <div class="col-xs-6 col-sm-6">
					 <div class="cpobj-item draggable cpobj-opt-in"> <i class="fa fa-check-square cpobj-item-icon"></i> <div class="cpobj-item-label">Opt In</div> </div>
				 </div>
			 </div>

			 <cfif afe eq 1>
				 <div class="row">
					 <div class="col-xs-6 col-sm-6">
						 <div class="cpobj-item draggable cpobj-batch-cp"> <i class="fa fa-stream cpobj-item-icon"></i> <div class="cpobj-item-label">Context</div> </div>
					 </div>

					 <div class="col-xs-6 col-sm-6">
						 <div class="cpobj-item draggable cpobj-wfhh"> <i class="fa fa-brain cpobj-item-icon"></i> <div class="cpobj-item-label">HAU</div> </div>
					 </div>
				 </div>

				 <div class="row">
					<div class="col-xs-6 col-sm-6">
						<div class="cpobj-item draggable cpobj-reset"> <i class="fa fa-sync-alt cpobj-item-icon"></i> <div class="cpobj-item-label">Loop Reset</div> </div>
					</div>


				</div>

			 </cfif>

		 </div>

	</cfif>
