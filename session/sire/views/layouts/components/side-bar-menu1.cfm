<!---Verify whether to show dev features in production see - session.sire.models.cfc.admin method GetDevPermission to add your account user id for dev and testing --->
<cfinvoke method="GetDevPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetDevPermission" />
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission"><cfinvokeargument name="inpSkipRedirect" value="1"></cfinvoke>
<!--- Start rewrite menu canvas --->
<cfparam name="inpTemplateFlag" default="0"/>

<ul class="page-sidebar-menu no-print" data-keep-expanded="false" data-auto-scroll="false" data-slide-speed="200">

	<li class="top-menu top-menu-small">

		<div class="page-top">

	   		 <cfinclude template="top-menu-2.cfm">
	    </div>
	</li>

	<li class="nav-item account-li hide-collapsed">
        <a href="##" class="nav-link nav-toggle">
            <span class="title"><cfoutput>#Session.FIRSTNAME#</cfoutput><cfif LEN(TRIM(Session.FIRSTNAME)) EQ 0 >MY ACCOUNT</cfif></span>
            <i class="fa fa-caret-down" aria-hidden="true"></i>
        </a>
        <ul class="sub-menu">
	        <li class="nav-item account-li">
		        <a href="/session/sire/pages/account" class="nav-link nav-toggle">
		            <span class="title">Account</span>
		        </a>
		    </li>
            <cfif userInfo.USERTYPE EQ 2 AND userInfo.USERLEVEL EQ 2>
                <li class="nav-item sub-account-li">
                    <a href="/session/sire/pages/sub-account" class="nav-link nav-toggle">
                        <span class="title">Sub Account</span>
                    </a>
                </li>
            </cfif>
            <cfif Session.AGENCYID GT 0 >
     		  <li class="nav-item ">
     		        <a href="/session/sire/pages/agency" class="nav-link nav-toggle">
     		            <span class="title">Agencies</span>
     		        </a>
     		  </li>
            </cfif>
            <li class="nav-item profile-li">
		        <a href="/session/sire/pages/profile" class="nav-link nav-toggle">
		            <span class="title">Profile</span>
		        </a>
		    </li>
		    <li class="nav-item my-plan-li order-plan-li buy-sms-li buy-keyword-li invite-friend-li">
		        <a href="/session/sire/pages/my-plan" class="nav-link nav-toggle">
		            <span class="title">My Plan: <i><cfoutput>#currentUserPlanName#</cfoutput></i></span>
		        </a>
		    </li>
		    <li class="nav-item billing-li">
		        <a href="/session/sire/pages/billing" class="nav-link nav-toggle">
		            <span class="title">Billing</span>
		        </a>
		    </li>

		    <li class="nav-item support support-li">
		        <a href="/session/sire/pages/support" class="nav-link nav-toggle">
		            <span class="title">Support</span>
		        </a>
		    </li>
            <li class="nav-item message-li">
                <a href="/session/sire/pages/message" class="nav-link nav-toggle">
                    <span class="title">Messages</span>
                </a>
            </li>
            <li class="nav-item affiliates-dashboard-li">
                <a href="/session/sire/pages/affiliates-dashboard" class="nav-link nav-toggle">
                    <span class="title">Affiliate</span>
                </a>
            </li>
		    <li class="nav-item user-signout">
		        <a href="javascript:void(0)" class="nav-link nav-toggle">
		            <span class="title">Sign Out</span>
		        </a>
		    </li>

        </ul>
    </li>


    <li class="nav-item dashboard-li">
        <a  href="/session/sire/pages/dashboard" class="nav-link nav-toggle dashboard-a">
            <span class="title">Dashboard</span>
        </a>
    </li>
    <li class="nav-item campaign-li">
        <a href="##" class="nav-link nav-toggle">
            <span class="title">Campaigns</span>
            <i class="fa fa-caret-down" aria-hidden="true"></i>
        </a>

        <ul class="sub-menu">
            <li class="nav-item start campaign-template-choice-li">
                <a href="/session/sire/pages/campaign-template-new?templateId=1&selectedTemplateType=0" class="nav-link ">
                    <span class="title">Create campaign</span>
                </a>
            </li>
            <li class="nav-item start campaign-manage-li campaign-reports-li">
                <a href="/session/sire/pages/campaign-manage" class="nav-link ">
                    <span class="title">Manage campaigns</span>
                </a>
            </li>
            <li class="nav-item start keyword-li">
                <a href="/session/sire/pages/keyword" class="nav-link nav-toggle">
                    <span class="title">Keywords</span>
                </a>
            </li>

            <li class="nav-item start subscriber-li">
                <a href="/session/sire/pages/subscriber" class="nav-link nav-toggle">
                    <span class="title">Subscribers</span>
                </a>
            </li>
        </ul>
    </li>

    <!---REMOVE IT FIRST
    <li class="nav-item sms-chat-li">
        <a href="/session/sire/pages/sms-chat" class="nav-link nav-toggle">
            <span class="title">SMS Chat</span>
        </a>
    </li>
    --->
    <li class="nav-item tools-li">
        <a href="##" class="nav-link nav-toggle">
            <span class="title">Tools</span>
            <i class="fa fa-caret-down" aria-hidden="true"></i>
        </a>
        <ul class="sub-menu">
            <!---
            <li class="nav-item user-chat-tools-li">
                <a href="/session/sire/pages/user-chat-tools" class="nav-link nav-toggle">
                    <span class="title">Let's Chat</span>
                </a>
            </li>
            --->

		  <li class="nav-item user-import-contact-file-li">
		      <a href="/session/sire/pages/calendar/appointments" class="nav-link nav-toggle">
		  	   <span class="title">Appointment Reminders</span>
		      </a>
		  </li>

		  <li class="nav-item user-import-contact-file-li">
                <a href="/session/sire/pages/user-import-contact-file" class="nav-link nav-toggle">
                    <span class="title">Import Contacts</span>
                </a>
            </li>

            <li class="nav-item mlp-list-li mlp-edit-li mlp-template-picker-li">
                <a href="/session/sire/pages/mlp-list" class="nav-link nav-toggle">
                    <span class="title">Mlp</span>
                </a>
            </li>

            <li class="nav-item short-url-manage-li">
                <a href="/session/sire/pages/short-url-manage" class="nav-link nav-toggle">
                    <span class="title">Short Url</span>
                </a>

            </li>

        </ul>
    </li>

   <!---  <li class="nav-item trouble-ticket-app-li">
        <a href="##" class="nav-link nav-toggle">
            <span class="title">Trouble Ticket App</span>
            <i class="fa fa-caret-down" aria-hidden="true"></i>
        </a>
        <ul class="sub-menu">
            <li class="nav-item trouble-ticket-landing-li">
                <a href="/session/sire/pages/trouble-ticket-landing" class="nav-link nav-toggle">
                    <span class="title">List tickets</span>
                </a>
            </li>
            <li class="nav-item ticket-type-management-li">
                <a href="/session/sire/pages/ticket-type-management" class="nav-link nav-toggle">
                    <span class="title">Manage Ticket Type</span>
                </a>
            </li>
            <li class="nav-item campaign-template-troubleticket-li">
                <a href="/session/sire/pages/campaign-template-troubleticket" class="nav-link nav-toggle">
                    <span class="title">Edit Campaign</span>
                </a>
            </li>

        </ul>
    </li> --->

    <li class="nav-item advanced-li  advanced-campaigns-li admin-tools-li simon-parent-li">
        <a href="##" class="nav-link nav-toggle">
            <span class="title">Advanced</span>
            <i class="fa fa-caret-down" aria-hidden="true"></i>
        </a>
        <ul class="sub-menu">
            <li class="nav-item start send-one-li">
                <a href="/session/sire/pages/send-one" class="nav-link ">
                    <span class="title">Single Campaign Trigger</span>
                </a>
            </li>
            <li class="nav-item start qa-tool-li">
                <a href="/session/sire/pages/qa-tool" class="nav-link ">
                    <span class="title">QA Tool (With Time Machine)</span>
                </a>
            </li>
            <li class="nav-item start reports-dashboard-li">
                <a href="/session/sire/pages/reports-dashboard" class="nav-link ">
                    <span class="title">Reporting and Analytics Dashboard</span>
                </a>
            </li>

             <li class="nav-item start campaign-edit-li simon-menu-li">
                <a href="/session/sire/pages/campaign-edit?templateid=116&adv=1&customCampaign=1" class="nav-link ">
                    <span class="title">Build new custom campaign</span>
                </a>
            </li>

			<li class="nav-item start image-manage-li">
                <a href="/session/sire/pages/image-manage" class="nav-link ">
                    <span class="title">MLP Image Manager</span>
                </a>
            </li>
            <cfif userInfo.USERTYPE EQ 2 AND userInfo.USERLEVEL LT 3 OR RetVarGetAdminPermission.ISADMINOK EQ  1>
                <li class="nav-item start integrate-management-dashboard-li">
                    <a href="/session/sire/pages/integrate-management-dashboard" class="nav-link ">
                        <span class="title">Enterprise Admin</span>
                    </a>
                </li>
            </cfif>
            <!--- Calendar APP --->
<!---             <li class="nav-item start image-manage-li">
                <a href="/session/sire/pages/calendar" class="nav-link ">
                    <span class="title">Sire Calendar</span>
                </a>
            </li> --->
            <!---
            <li class="nav-item start image-manage-li">
                <a href="/session/sire/pages/calendar-event-report" class="nav-link ">
                    <span class="title">Calendar Report</span>
                </a>
            </li> --->

            <li class="nav-item start image-manage-li">
                <a href="/session/sire/pages/phone_number_lookup" class="nav-link ">
                    <span class="title">Contact String History Report</span>
                </a>
            </li>

            <cfif RetVarGetAdminPermission.ISADMINOK EQ  1>
                <li class="nav-item start admin-home-li admin-user-support-li admin-system-usage-report-today-li admin-system-usage-report-li admin-site-errors-li admin-mlp-template-li mlp-edit-li admin-mlp-list-li admin-recurring-report-li live-chat-setting-li admin-account-update-li admin-template-category-management-li admin-copy-batch-li trouble-ticket-li admin-contactstring-history-report-li">
                    <a href="/session/sire/pages/admin-home" class="nav-link nav-toggle">
                        <span class="title">Admin</span>
                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                    </a>
                    <ul class="sub-menu" id="admSub">
                        <li class="nav-item start">
                            <!---<a href="/session/sire/pages/admin-home?currTab=section-crm" class="nav-link ">--->
                            <a href="/session/sire/pages/admin-crm-reports" class="nav-link ">

                                <span class="title">CRM</span>
                            </a>
                        </li>
                        <li class="nav-item start">
                            <a href="/session/sire/pages/admin-home?currTab=section-support" class="nav-link ">
                                <span class="title">Support</span>
                            </a>
                        </li>
                        <li class="nav-item start">
                            <a href="/session/sire/pages/admin-home?currTab=section-it" class="nav-link ">
                                <span class="title">IT</span>
                            </a>
                        </li>
                        <li class="nav-item start">
                            <a href="/session/sire/pages/admin-home?currTab=section-campign" class="nav-link ">
                                <span class="title">Campaign</span>
                            </a>
                        </li>
                        <li class="nav-item start">
                            <a href="/session/sire/pages/affiliate" class="nav-link ">
                                <span class="title">Affiliate</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <cfif FIND('campaign-edit', CGI.request_url) GT 0 AND inpTemplateFlag EQ 0>
                <li class="nav-item start save-template-li">
                    <a href="#" class="nav-link " id="save-as-new-template">
                        <span class="title">Save as new template</span>
                    </a>
                </li>
                </cfif>
            </cfif>
        </ul>
    </li>
</ul>
