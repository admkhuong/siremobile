<cfoutput>
<ul class="page-sidebar-menu  no-print ul-simon" data-keep-expanded="false" data-auto-scroll="false" data-slide-speed="200">
     <li class="nav-item user-dashboard">
         <a href="/session/sire/pages/dashboard" class="nav-link nav-toggle">
            <span class="title"><i class="fa fa-arrow-left" aria-hidden="true"></i>Sire</span>
         </a>
     </li>
    <li class="nav-item account-li">
        <a href="/session/sire/pages/account" class="nav-link nav-toggle">
            <span class="title">Account</span>
        </a>
    </li>
    <cfif userInfo.USERTYPE EQ 2 AND userInfo.USERLEVEL EQ 2>
    <li class="nav-item sub-account-li">
        <a href="/session/sire/pages/sub-account" class="nav-link nav-toggle">
            <span class="title">Sub Account</span>
        </a>
    </li>
    </cfif>
    <li class="nav-item profile-li">
        <a href="/session/sire/pages/profile" class="nav-link nav-toggle">
            <span class="title">Profile</span>
        </a>
    </li>

    <li class="nav-item my-plan-li order-plan-li buy-sms-li buy-keyword-li invite-friend-li">
        <a href="/session/sire/pages/my-plan" class="nav-link nav-toggle">
            <span class="title">My Plan: <i>#currentUserPlanName#</i></span>
        </a>
    </li>
    <cfif userInfo.USERTYPE EQ 1 OR ( userInfo.USERLEVEL NEQ 3 AND userInfo.USERTYPE EQ 2) >
    <li class="nav-item billing-li">
        <a href="/session/sire/pages/billing" class="nav-link nav-toggle">
            <span class="title">Billing</span>
        </a>
    </li>
    </cfif>
    <!--- <li class="nav-item security-credentials-li">
        <a href="/session/sire/pages/security-credentials" class="nav-link nav-toggle">
            <span class="title">Security</span>
        </a>
    </li>
    --->
    <li class="nav-item support support-li">
        <a href="/session/sire/pages/support" class="nav-link nav-toggle">
            <span class="title">Support</span>
        </a>
    </li>
    <li class="nav-item message-li">
        <a href="/session/sire/pages/message" class="nav-link nav-toggle">
            <span class="title">Messages</span>
        </a>
    </li>
    <li class="nav-item affiliates-dashboard-li">
        <a href="/session/sire/pages/affiliates-dashboard" class="nav-link nav-toggle ">
            <span class="title">Affiliate</span>
        </a>
    </li>

    <li class="nav-item user-signout">
        <a href="javascript:void(0)" class="nav-link nav-toggle">
            <span class="title">Sign Out</span>
        </a>
    </li>
</ul>
</cfoutput>
