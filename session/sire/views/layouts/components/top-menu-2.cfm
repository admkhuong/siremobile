
<cfoutput>
    
    <ul class="nav navbar-nav" style="margin-top: 10px;">
        <li class="dropdown dropdown-user disabled shortcode-li-2">
            <a class="dropdown-toggle cursor-disabled">
                <cfif arrayLen(resultSetListShortCodes.LIST_SHORT_CODES) GT 1>
                    <div class="row">
                        <div class="col-xs-6 col-md-6 col-sm-6 hide-on-mobile" style="">
                            <span class="">Short code: </span>
                        </div>
                        <div class="col-xs-6 col-md-6 col-sm-6 shortcode" style=";bottom: 6px">
                            <span>
                                <select class="form-control short-code-list-select">
                                </select>
                            </span>
                        </div>
                    </div>
                <cfelseif arrayLen(resultSetListShortCodes.LIST_SHORT_CODES) EQ 1>
                    <cfif resultSetListShortCodes.LIST_SHORT_CODES[1].SHORT_CODE NEQ ''>
                        <span class="hide-on-mobile">Short code: </span> <span><cfif shortCode.SHORTCODE NEQ ''>#shortCode.SHORTCODE#</cfif> </span>
                    </cfif>
                <cfelse>
                </cfif>
            </a>
        </li>
        <li class="separator"> </li>
        <li class="dropdown dropdown-user" >
            <!--- /session/sire/pages/buy-sms --->
            <a id="BillingBalanceSection" href="/session/sire/pages/my-plan?active=addon" class="dropdown-toggle BillingBalanceSection" style="overflow: visible;" <cfif TriggerBillingWarning EQ 1> data-trigger="manual" data-placement="bottom" title="Credits Low Alert" data-content="<p>Your credits are getting low.</p><a href='/public/sire/pages/cs-ooc' style='margin-top:1em;'>What Happens when I run out of credits?</a>" data-html="true" </cfif>>
                <span class="text-green" id="span-current-user-credits">#currentUserCredits# <cfif currentUserCredits GT 1> credits <cfelse> credit </cfif> </span>
                                              
                <!--- https://codepen.io/pankajparashar/pen/GnFpA or https://css-tricks.com/html5-meter-element/  without IE fallback - just text OK for old crappy browsers --->
                <!--- <meter value="#BillingPercentageOfPlan#" min="0" max="100" low="50" high="75" optimum="#RetUserPlan.FIRSTSMSINCLUDED#" title="#currentUserCredits#"></meter>  --->

                <cfif BillingPercentageOfPlan GT 50>
                    <cfset dotColor = "##74c37f"/>
                    <cfset dotTooltip = "More than 50% of your monthly credits remaining"/>
                <cfelseif BillingPercentageOfPlan GT 25 AND BillingPercentageOfPlan LTE 50>
                    <cfset dotColor = "##f6df17"/>
                    <cfset dotTooltip = "Less than 50% of your monthly credits remaining"/>
                <cfelse>
                    <cfset dotColor = "##ef3d3d"/>
                    <cfset dotTooltip = "Less than 25% of your monthly credits remaining"/>
                </cfif>
                
                <i class="fa fa-circle" id="credit-status-dot" data-placement="bottom" data-trigger="hover" data-toggle="popover" data-content="#dotTooltip#" style="color: #dotColor#; font-size: 17px"></i>

            </a>
                   
        </li>
        <li class="separator"> </li>     

                                
    </ul>
     
</cfoutput>

<style type="text/css">
    @media screen and (max-width: 480px) {
        .shortcode {
            width: 100%;
        }
    }

    @media screen and (min-width: 481px) {
        .shortcode-li-2 {
            width: 280px;
        }
    }
</style>