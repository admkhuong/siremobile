
<!---
<div>
     <a href="/session/sire/pages/dashboard" class="" style="border-top:none;">
          <span class="title"><i class="fa fa-arrow-left" aria-hidden="true"></i>Dashboard</span>
     </a>

</div> --->
<!--- dropbar: true; dropbar-mode: push; mode: click; --->


    <nav class="uk-navbar-container uk-navbar-transparent" uk-navbar="dropbar: true; dropbar-mode: push;">

        <!--- <div class="uk-navbar-left"> --->

            <ul class="uk-navbar-nav">

                <li>
                    <a href="#">Triggers</a>
                    <div class="uk-navbar-dropdown" style="width:100%">


                                    <!--- Hide while loading so whole section does not show FOUC --->
								<div id="CampaignTriggerMenu" style="">

									<ul uk-tab="connect: .campaign-options-menu" class="cp-report-tab">
							            <li class="uk-active">
							                <a href="##"><cfoutput>#UIL_Keyword#</cfoutput></a>
							            </li>
							            <li>
							                <a href="##">Blast</a>
							            </li>
							            <li>
							                <a href="##">API</a>
							            </li>
							        </ul>

									<ul class="uk-switcher uk-margin campaign-options-menu">
							            <li>

											<h3><cfoutput>#UIL_Keyword#</cfoutput></h3>
											<!--- Keyword Section --->
											<cfinclude template="/session/sire/pages/advanced-templates/inc-keyword.cfm" />
							            </li>

							           	<li>
											<h3>Blast</h3>

											<!--- This section is for specifying "Blast" values --->
	 										<cfinclude template="/session/sire/pages/advanced-templates/inc-blast.cfm" />
							           	</li>

							           	<li>
											<h3>API</h3>

											<!--- This section is for specifying "API" values --->
	 										<cfinclude template="/session/sire/pages/advanced-templates/inc-api.cfm" />
							           	</li>
									</ul>

								</div>

                    </div>

                </li>

                <li>
                <a href="#">Options</a>
                <div class="uk-navbar-dropdown" uk-drop="boundary: !nav; boundary-align: true; pos: bottom-justify;" style="margin-top: 0px;">
                    <ul class="uk-nav uk-navbar-dropdown-nav">
                         <cfoutput>
                              <!--- <li><a href="/session/sire/pages/campaign-reports?campaignid=#campaignid#">Report</a></li> --->
                              <li><a href="/session/sire/pages/advanced-templates/campaign-report?campaignid=#campaignid#" target="_blank">Response Report</a></li>
                              <li><a href="/session/sire/pages/advanced-templates/campaign-report-subscriber?campaignid=#campaignid#" target="_blank">Subscriber Report</a></li>

                              <li><a data-toggle="modal" href="##" data-target="##cloneCampaignModal">Clone</a></li>

                              <!-- toggle hide flow editor functionality - Flag to enabel or disable flow editor options-->
                              <cfif hfe EQ 1>
                              	<li><a href="/session/sire/pages/campaign-edit?campaignid=#campaignid#&hfe=0&afe=#afe#">Show Flow Editor</a></li>
                              <cfelse>
                              	<li><a href="/session/sire/pages/campaign-edit?campaignid=#campaignid#&hfe=1&afe=#afe#">Hide Flow Editor</a></li>
                              </cfif>

                              <cfif GetAdminPermission.ISADMINOK EQ  1>
                                   <cfif XMLCSEditable EQ true>
                              	     <li>
                              			<a href="##" data-toggle="modal" data-target="##EditControlString">Edit XMLControlString</a>
                              		</li>
                                   </cfif>
                              <cfelse>
                              	<cfif XMLCSEditable EQ true>
                              	     <li>
                              		     <a href="##" data-toggle="modal" data-target="##EditControlString">Edit XMLControlString</a>
                              	     </li>
                                   </cfif>
                              </cfif>

                              <li><a href="##" data-toggle="modal" data-target="##CustomMessageModal" style="">Custom Help / Stop</a></li>

                              <cfif hfe EQ 0 AND afe EQ 1 >
                              	<li><a id="MainPasteCP" href="##">Paste</a></li>
                              </cfif>

                         </cfoutput>
                    </ul>
                </div>
            </li>

            </ul>

        <!--- </div> --->

    </nav>

    <div class="uk-navbar-dropbar"></div>
