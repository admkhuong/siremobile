<cfoutput>
    <ul class="nav navbar-nav pull-right">
                <cfif arrayLen(resultSetListShortCodes.LIST_SHORT_CODES) GT 1>
                    <li class="dropdown dropdown-user disabled shortcode-li">
                        <a class="dropdown-toggle cursor-disabled">
                            <div class="row">
                                <div class="col-xs-6 col-md-6 col-sm-6" style="padding-right: 0px">
                                    <span class="hide-on-mobile">Short code: </span>
                                </div>
                                <div class="col-xs-6 col-md-6 col-sm-6" style="padding-left: 0px;bottom: 6px">
                                    <span>
                                        <select class="form-control short-code-list-select">
                                        </select>
                                    </span>
                                </div>
                            </div>
                <cfelseif arrayLen(resultSetListShortCodes.LIST_SHORT_CODES) EQ 1>
                    <cfif resultSetListShortCodes.LIST_SHORT_CODES[1].SHORT_CODE NEQ ''>
                        <li class="dropdown dropdown-user disabled">
                            <a class="dropdown-toggle cursor-disabled">
                            <span class="hide-on-mobile">Short code: </span> <span><cfif shortCode.SHORTCODE NEQ ''>#shortCode.SHORTCODE#</cfif> </span>
                    </cfif>
                <cfelse>
                </cfif>
            </a>
        </li>
        <li class="separator"> </li>
        <li class="dropdown dropdown-user" >
            <!--- /session/sire/pages/my-plan?active=addon --->
            <!--- /session/sire/pages/buy-sms --->
            <a id="BillingBalanceSection" href="/session/sire/pages/my-plan?active=addon" class="dropdown-toggle BillingBalanceSection" <cfif TriggerBillingWarning EQ 1> data-trigger="manual" data-placement="bottom" title="Credits Low Alert" data-content="<p>Your credits are getting low.</p><a href='https://www.siremobile.com/blog/support/out-of-credits/' target='_blank' style='margin-top:1em;'>What Happens when I run out of credits?</a>" data-html="true" </cfif>>
                <span class="text-green" id="span-current-user-credits">#currentUserCredits# <cfif currentUserCredits GT 1> credits <cfelse> credit </cfif> </span>

                <!--- https://codepen.io/pankajparashar/pen/GnFpA or https://css-tricks.com/html5-meter-element/  without IE fallback - just text OK for old crappy browsers --->
                <!--- <meter value="#BillingPercentageOfPlan#" min="0" max="100" low="50" high="75" optimum="100" title="#currentUserCredits#"></meter>   --->
                <cfif BillingPercentageOfPlan GT 50>
                    <cfset dotColor = "##74c37f"/>
                    <cfset dotTooltip = "More than 50% of your monthly credits remaining"/>
                <cfelseif BillingPercentageOfPlan GT 25 AND BillingPercentageOfPlan LTE 50>
                    <cfset dotColor = "##f6df17"/>
                    <cfset dotTooltip = "Less than 50% of your monthly credits remaining"/>
                <cfelse>
                    <cfset dotColor = "##ef3d3d"/>
                    <cfset dotTooltip = "Less than 25% of your monthly credits remaining"/>
                </cfif>

                <i class="fa fa-circle" id="credit-status-dot" data-placement="bottom" data-trigger="hover" data-toggle="popover" data-content="#dotTooltip#" style="color: #dotColor#"></i>
            </a>

        </li>
        <li class="separator"> </li>


        <!-- BEGIN USER LOGIN DROPDOWN -->
        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
        <li class="dropdown dropdown-user">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <span class="hide-on-mobile">#Session.FIRSTNAME#</span>
                <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                <i class="fa fa-caret-down name-profile-mobile" aria-hidden="true"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-default list-upper-link">
                <li>
                    <a href="/session/sire/pages/account"> Account</a>
                </li>
                <cfif userInfo.USERTYPE EQ 2 AND userInfo.USERLEVEL EQ 2>
                    <li>
                        <a href="/session/sire/pages/sub-account">Sub Account</a>
                    </li>
                </cfif>
                <cfif Session.AGENCYID GT 0>
                <li>
                   <a href="/session/sire/pages/agency"> Agencies</a>
                </li>
                </cfif>
                <li>
                    <a href="/session/sire/pages/profile"> Profile </a>
                </li>

                <li>
                    <a href="/session/sire/pages/my-plan"> My Plan: #currentUserPlanName# </a>
                </li>
                <cfif userInfo.USERTYPE EQ 1 OR ( userInfo.USERLEVEL NEQ 3 AND userInfo.USERTYPE EQ 2) >
                <li>
                    <a href="/session/sire/pages/billing"> Billing </a>
                </li>
                </cfif>
                <!--- <li>
                    <a href="/session/sire/pages/security-credentials"> Security </a>
                </li> --->
                <li>
                    <a href="/session/sire/pages/support"> Support </a>
                </li>
                <li>
                    <a href="/session/sire/pages/message">
                        Messages
                        <cfif structKeyExists(Session, "webAlertMessages") AND isStruct(Session.webAlertMessages) AND StructCount(Session.webAlertMessages) GT 0>
                            <span class="badge">#StructCount(Session.webAlertMessages)#</span>
                        </cfif>
                    </a>
                </li>
                <li>
                    <a href="/session/sire/pages/affiliates-dashboard">Affiliate</a>
                </li>
                <li>
                    <a class="user-signout" href="javascript:void(0)"> Sign Out </a>
                </li>
            </ul>
        </li>
        <!-- END USER LOGIN DROPDOWN -->
    </ul>
</cfoutput>

<style type="text/css">
    @media screen and (min-width: 1600px) {
        .shortcode-li {
            width: 300px;
        }
    }
    @media screen and (max-width: 1599px) {
        .shortcode-li {
            width: 250px;
        }
    }
</style>
