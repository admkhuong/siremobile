<cfif !isDefined('variables._Response')>

<cfset variables._PageContext = GetPageContext()>

<cfset variables._Response = variables._PageContext.getOut()>
</cfif>
<cfset variables._page = variables._Response.getString()>
<cfset variables._Response.clear()>
<cfparam name="variables._page" default="">
<cfparam name="variables._title" default="">
<cfparam name="variables.meta_description" default="">
<cfparam name="Request._css" default="[]">
<cfparam name="Request._js" default="[]">
<cfparam name="variables.menuToUse" default="1">
<cfparam name="variables.portleft" default="">
<cfset ContentBuffVar = replaceList(variables.meta_description, '<,>,"', '&lt;,&gt;,&quot;') />

<cfoutput>
<!DOCTYPE html>
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>#variables._title#</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" id="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0,user-scalable=no" />
        <meta name="description" content="#ContentBuffVar#">
        <meta content="" name="author" />
        <meta name="robots" content="index, follow">

     <link href='https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400,200,300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
     <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i,900,900i" rel="stylesheet">

     <!-- BEGIN GLOBAL MANDATORY STYLES -->
     <cfinclude template="components\assets-global-3.cfm">
     <!-- END GLOBAL MANDATORY STYLES -->

     <cfif (isDefined("Request._css") && isArray(Request._css))>
          <cfloop array="#Request._css#" index="cssPath">
               <link href="#cssPath#" rel="stylesheet">
          </cfloop>
          <cfset Request._css=[]>
     </cfif>
     <!-- END THEME LAYOUT STYLES -->

     <cfinclude template="/public/sire/views/commons/google_task_script_head.cfm">
     </head>

     <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">

          <cfinclude template="/public/sire/views/commons/google_task_script_body.cfm">

          <!-- BEGIN HEADER -->
          <div class="page-header navbar navbar-fixed-top no-print">
               <!-- BEGIN HEADER INNER -->
               <div class="page-header-inner ">

                <!-- BEGIN LOGO -->
                    <div class="page-logo">
                         <a href="/public/sire/pages/index.cfm">
                              <img src="/session/sire/assets/layouts/layout4/img/logo-be.png" alt="logo" class="logo-default" />
                         </a>
                    </div>
                <!-- END LOGO -->
               </div>
               <!-- END HEADER INNER -->
               <br/>
          </div>

        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper sidebar-fixed no-print">

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE BASE CONTENT -->
                    #variables._page#
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer no-print">
            <cfinclude template="components\footer.cfm">
        </div>
        <!-- END FOOTER -->

        <!-- BEGIN CORE PLUGINS -->
        <cfinclude template="components\assets-global-3-js.cfm">
        <!-- END CORE PLUGINS -->

          <!-- BEGIN PAGE LEVEL PLUGINS -->
          <cfif (isDefined("Request._js_plugin") && isArray(Request._js_plugin))>
               <cfloop array="#Request._js_plugin#" index="jsPath">
                    <script src="#jsPath#"></script>
               </cfloop>
               <cfset Request._js_plugin=[]>
          </cfif>
          <!-- END PAGE LEVEL PLUGINS -->

          <!-- BEGIN THEME GLOBAL SCRIPTS -->
          <script src="/session/sire/assets/global/scripts/app.3.js" type="text/javascript"></script>
          <script src="/session/sire/assets/pages/scripts/sidebar-config.js" type="text/javascript"></script>
          <!-- END THEME GLOBAL SCRIPTS -->

          <!-- BEGIN PAGE LEVEL SCRIPTS -->
               <cfif (isDefined("Request._js") && isArray(Request._js))>
                    <cfloop array="#Request._js#" index="jsPath">
                         <script src="#jsPath#"></script>
                    </cfloop>
                 <cfset Request._js=[]>
               </cfif>
          <!-- END PAGE LEVEL SCRIPTS -->
        <script type="text/javascript">
             (function($){
               $('body').on('select2:open', '.select2', function(event){
                    var ele = $(this);
                    if(ele.length > 0){
                        var modal = ele.parents('div.modal');
                        if(modal.length > 0){
                            $('body > span.select2-container').css('z-index', modal.css('z-index'));
                        }
                    }
               });
            })(jQuery);
        </script>
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="/session/sire/assets/layouts/layout4/scripts/layout.3.js" type="text/javascript"></script>
        <script src="/session/sire/assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->


    </body>

</html>

</cfoutput>

<!--- Device Reg originally from /public/sire/css/common.css  --->
<style>
.page-container
{
     background: url('/session/sire/republic/images/device_register_bg.jpg') no-repeat top;
     background-size: cover;
     font-family: 'Source Sans Pro', sans-serif;
     height: 90vh;
     opacity: 0.9;
     background-position-y: -10px;
}

.page-content-wrapper
{
     margin-left: -8.5%;
}

.page-content
{
     width: 100%;
}

/* the verification input box positioning */
#verification_code,
#device_registered,
#invalid_verification_code
{
     margin-left: 15%;
     width: 100%;
}

#invalid-body
{
     width: 112%;
}

.modal-content
{
     top: 80px;
}

.main-wrapper
{
     color: #505050;
}

/* for the text body */
.wraper_signup_form .row
{
     background-color: rgba(230,229,229,.9);
}

.signup_form
{
     padding: 25px;
}

.your-device h4
{
     padding-bottom: 20px;
}

.page-footer
{
     position: absolute;
     /* position: fixed;
     margin-top: 36.5%; */
     margin-left: -1.7%;
     /* padding-bottom: 10%; */
     padding-bottom: 3.5%;
     padding-top: 2%;
     width: 102%;
     top: 91%;
}

.device_reg_title
{
     font-size: 38px;
     font-family: 'Yanone Kaffeesatz', sans-serif;
     font-weight: bold;
     margin-bottom: 15px;
     padding-top: 60px;
     text-transform: uppercase;
}

.device_reg_title .text_menu
{
     color: #8C9899;
}

.device_reg_title .current-title
{
     color: #ECECEC;
}

#device_not_reg_form
{
     font-size: 20px;
     font-family: 'Source Sans Pro', sans-serif;
}

#device_not_reg_form label
{
     font-size: 18px;
}

.radio_device_reg,
.checkbox_device_reg
{
     width: 16px;
     height: 16px;
}

.btn-custom_size
{
     width: 145px;
     text-transform: uppercase;
}

.form_device_not_reg_control
{
     margin-left: 20px;
     margin-bottom: 20px;
}

.form_device_not_reg_control .form-group-select-opt
{
     margin-bottom: 25px;
}

#btn_answer_question
{
     width: 200px;
}

.brand-success
{
     color: #5cb85c;
}

.btn-group-verification_code .btn
{
     margin: 0 10px 15px 0;
}

@media only screen and (max-width: 1100px)
{
     .page-footer
     {
          top: 85vh;
     }
}

@media only screen and (min-width: 768px) and (max-width: 1024px)
{
     .main-wrapper
     {
          padding-bottom: 125px;
     }
}

@media (max-width: 767px)
{
     .main-wrapper
     {
          background: none;
     }
}

@media (max-width: 667px)
{
     .device_reg_title
     {
          font-size: 30px;
     }

     .main-wrapper
     {
          padding-bottom: 0;
     }

     .device_reg_title
     {
          font-size: 32px;
          padding-top: 0;
     }
}

@media (max-width: 480px)
{
     .device_reg_title
     {
          font-size: 19px;
     }

     label,
     #device_not_reg_form p
     {
          font-size: 16px;
     }

     .btn-custom_size
     {
          width: 100px;
     }

     .main-wrapper
     {
          padding-bottom: 0;
     }

     .btn-custom_size
     {
          width: calc(50% - 12px);
     }

     .device_reg_title
     {
          font-size: 32px;
          padding-top: 0;
     }
}
</style>
