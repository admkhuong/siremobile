<cfif !isDefined('variables._Response')>

<cfset variables._PageContext = GetPageContext()>

<cfset variables._Response = variables._PageContext.getOut()>
</cfif>
<cfset variables._page = variables._Response.getString()>
<cfset variables._Response.clear()>
<cfparam name="variables._page" default="">
<cfparam name="variables._title" default="">
<cfparam name="variables.meta_description" default="">
<cfparam name="Request._css" default="[]">
<cfparam name="Request._js" default="[]">
<cfparam name="variables.menuToUse" default="1">
<cfparam name="variables.portleft" default="">
<cfset ContentBuffVar = replaceList(variables.meta_description, '<,>,"', '&lt;,&gt;,&quot;') />

<cfoutput>
<!DOCTYPE html>
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>#variables._title#</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" id="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0,user-scalable=no" />
        <meta name="description" content="#ContentBuffVar#">
        <meta content="" name="author" />
        <meta name="robots" content="index, follow">
        

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <cfinclude template="components\assets-global.cfm">        
        <!-- END GLOBAL MANDATORY STYLES -->

        <!--- <link href="/public/sire/css/bootstrap.css" rel="stylesheet" media="screen"> --->
        <!--- <link rel="stylesheet" href="/public/sire/css/bootstrap.min_no_print.css" media="print"/> --->
        <!--- <link href="/public/sire/css/validationEngine.jquery.css" rel="stylesheet" media="screen"> --->
        <!--- <link href="/public/sire/css/bootstrap-tour.min.css" rel="stylesheet" media="screen"> --->

        <!--- <link href="/session/sire/css/style.css" rel="stylesheet" media="screen"> --->
        <!--- <link href="/session/sire/css/screen.css" rel="stylesheet" media="screen"> --->
        
        <!--- <link href="/session/sire/css/print.css" rel="stylesheet" media="print"> --->

        <cfif (isDefined("Request._css") && isArray(Request._css))>
            <cfloop array="#Request._css#" index="cssPath">
                <link href="#cssPath#" rel="stylesheet">
            </cfloop>
            <cfset Request._css=[]>
        </cfif>
        <!-- END THEME LAYOUT STYLES -->
        
        <cfinclude template="/public/sire/views/commons/google_task_script_head.cfm">
        <!--- Cleaned up style changes - move to .css files and make sure there are not any conflicts or duplicates --->
    </head>
    
    <!-- END HEAD -->
    
    <!--- Common billing information and checks --->
    <cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke>
    <cfinclude template="components\billing-data.cfm" />    

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">

        <cfinclude template="/public/sire/views/commons/google_task_script_body.cfm">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top no-print">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="/session/sire/pages/dashboard">
                        <img src="/session/sire/assets/layouts/layout4/img/logo-be.png" alt="logo" class="logo-default" />
                    </a>
                    <div class="menu-toggler sidebar-toggler hide">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>
                </div>
                <!-- END LOGO -->

                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                
                <!-- BEGIN PAGE TOP -->
                <div class="page-top top-menu-large">
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <cfinclude template="components\top-menu.cfm">
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END PAGE TOP -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <cfinclude template="\session\sire\views\commons\credit-available.cfm">
            
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper sidebar-fixed no-print">
                      
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                
                <!--- variable TriggerBillingWarning is defined in inculde file components\billing-data.cfm--->
				<cfif TriggerBillingWarning EQ 1>
					<!--- Display the collapse menu as open (IN) by default so warning popover can be seen --->
					<div class="page-sidebar navbar-collapse collapse in">
				<cfelse>
					<div class="page-sidebar navbar-collapse collapse">
				</cfif>
                    
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <div class="handle-scroll">
	                                      
                        <cfif variables.menuToUse EQ 1>
                            <cfinclude template="components\side-bar-menu1.cfm">
                        <cfelseif variables.menuToUse EQ 2>
                            <cfinclude template="components\side-bar-menu2.cfm">
                        </cfif>    

                        <!-- END SIDEBAR MENU -->

                        <!-- START BOX WIDGET -->
                            #variables.portleft#
                        <!-- END BOX WIDGET -->
                    </div>
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">                    
                    <!-- BEGIN PAGE BASE CONTENT -->
                    #variables._page#
                    <!-- END PAGE BASE CONTENT -->                    
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer no-print">
            <cfinclude template="components\footer.cfm">
        </div>
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
        <script src="assets/global/plugins/respond.min.js"></script>
        <script src="assets/global/plugins/excanvas.min.js"></script> 
        <![endif]-->

        <!-- Sign out modal -->
        <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="" id="modal-signout">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">x</button>
                    </div>
                    <div class="modal-body">
                        <form name="signout" class="row signout-form" autocomplete="off">
                        <h3 class="message">Are You Sure You Want to Sign Out?</h3>
                            <input type="hidden" name="SESuserId" id="SESuserId" value="<cfoutput>#SESSION.USERID#</cfoutput>">
                            <div class="button-confirm-signout text-center">
                                <button type="button" class="btn green-gd btn-re btn-modal-signout">YES, SIGN OUT NOW</button>
                                <button type="button" class="btn blue-gd btn-re" data-dismiss="modal">CANCEL</button>
                            </div>
                        </form>    
                    </div>
                </div>
            </div>
        </div>

        <!-- BEGIN CORE PLUGINS -->
        <cfinclude template="components\assets-global-js.cfm">
        <!-- END CORE PLUGINS -->

        <!-- BEGIN PAGE LEVEL PLUGINS -->     
        <cfif (isDefined("Request._js_plugin") && isArray(Request._js_plugin))>
            <cfloop array="#Request._js_plugin#" index="jsPath">
                <script src="#jsPath#"></script>
            </cfloop>
            <cfset Request._js_plugin=[]>
        </cfif>   
        <!-- END PAGE LEVEL PLUGINS -->

        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="/session/sire/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <script src="/session/sire/assets/pages/scripts/sidebar-config.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->

        <!-- BEGIN PAGE LEVEL SCRIPTS -->
         <cfif (isDefined("Request._js") && isArray(Request._js))>
            <cfloop array="#Request._js#" index="jsPath">
                <script src="#jsPath#"></script>
            </cfloop>
            <cfset Request._js=[]>
        </cfif>   
        <!-- END PAGE LEVEL SCRIPTS -->
        <script type="text/javascript">
             (function($){
               $('body').on('select2:open', '.select2', function(event){
                    var ele = $(this);
                    if(ele.length > 0){
                        var modal = ele.parents('div.modal');
                        if(modal.length > 0){
                            $('body > span.select2-container').css('z-index', modal.css('z-index'));
                        }
                    }
               });
            })(jQuery);
        </script>
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="/session/sire/assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="/session/sire/assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        <cfinclude template="../../../../public/sire/views/commons/livezilla_script.cfm">
        <script> 
            <cfoutput>var userid="#SESSION.USERID#";</cfoutput>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','//ssl.google-analytics.com/analytics.js','ga'); 
            ga('create', 'UA-82724101-1', 'auto',{
                            "userId": userid
                    }); 
            ga('set', 'dimension1', userid); 
            ga(function(tracker) { 
            tracker.set('dimension2', tracker.get('clientId'));
            });
             ga('send', 'pageview');
        </script> 
       
    </body>

</html>

</cfoutput>


<!--- variable TriggerBillingWarning is defined in inculde file  components\billing-data.cfm--->
<cfif TriggerBillingWarning EQ 1>

	<!--- Scripts to support billing credits low alerts - move to .js once stable --->
	<script type="text/javascript">
		
		$( function() {
			
			
			// $('.page-sidebar').trigger('click.bs.dropdown');
			$('.menu-toggler responsive-toggler').click();
							
			<!--- Select all elements with data-toggle="billing-alert-popover-manual" in the document --->
			$('.BillingBalanceSection').popover(<!--- {trigger:'manual', placement:'bottom', template:'<div class="popover pop2it" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'} ---> );
			
			<!--- Hide manual pop-overs on any click --->
			$(document).on('click touchstart', function (e) {		    
			     (($('.BillingBalanceSection').popover('hide').data('bs.popover')||{}).inState||{}).click = false  // fix for BS 3.3.6		    
			});
	
		    $('.BillingBalanceSection').popover('show');
			$('.BillingBalanceSection').on('shown.bs.popover', function () {                
                var topPopover=$("#BillingBalanceSection").outerHeight()/2 +20;                
                $('.popover').css({"top":topPopover});                                
            })
					
		});
			
	</script>	

</cfif>



