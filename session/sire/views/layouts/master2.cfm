<cfif !isDefined('variables._Response')>

<cfset variables._PageContext = GetPageContext()>

<cfset variables._Response = variables._PageContext.getOut()>
</cfif>
<cfset variables._page = variables._Response.getString()>
<cfset variables._Response.clear()>
<cfparam name="variables._page" default="">
<cfparam name="variables._title" default="">
<cfparam name="variables.meta_description" default="">
<cfparam name="Request._css" default="[]">
<cfparam name="Request._js" default="[]">
<cfparam name="variables.menuToUse" default="1">
<cfparam name="variables.portleft" default="">
<cfset ContentBuffVar = replaceList(variables.meta_description, '<,>,"', '&lt;,&gt;,&quot;') />

<cfoutput>
<!DOCTYPE html>
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>#variables._title#</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" id="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0,user-scalable=no" />
        <meta name="description" content="#ContentBuffVar#">
        <meta content="" name="author" />
        <meta name="robots" content="index, follow">
        

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <cfinclude template="components\assets-global.cfm">
        <!-- END GLOBAL MANDATORY STYLES -->

        <cfif (isDefined("Request._css") && isArray(Request._css))>
            <cfloop array="#Request._css#" index="cssPath">
                <link href="#cssPath#" rel="stylesheet">
            </cfloop>
            <cfset Request._css=[]>
        </cfif>
        <!-- END THEME LAYOUT STYLES -->

        <cfinclude template="/public/sire/views/commons/google_task_script_head.cfm">
    </head>
    <!-- END HEAD -->

    <!--- Common billing information and checks --->
    <cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke>
    <cfinclude template="components\billing-data.cfm" />    
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo none-sidebar">
         <cfinclude template="/public/sire/views/commons/google_task_script_body.cfm">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top no-print">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="/session/sire/pages/dashboard">
                        <img src="/session/sire/assets/layouts/layout4/img/logo-be.png" alt="logo" class="logo-default" />
                    </a>
                    <div class="menu-toggler sidebar-toggler hide">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>
                </div>
                <!-- END LOGO -->

                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                
                <!-- BEGIN PAGE TOP -->
                <div class="page-top">
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <cfinclude template="components\top-menu.cfm">
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END PAGE TOP -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <cfinclude template="\session\sire\views\commons\credit-available.cfm">

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">                    
                    <!-- BEGIN PAGE BASE CONTENT -->
                    #variables._page#
                    <!-- END PAGE BASE CONTENT -->                    
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer no-print">
            <cfinclude template="components\footer.cfm">
        </div>
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
        <script src="assets/global/plugins/respond.min.js"></script>
        <script src="assets/global/plugins/excanvas.min.js"></script> 
        <![endif]-->

        <!-- Sign out modal -->
        <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="" id="modal-signout">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">x</button>
                    </div>
                    <div class="modal-body">
                        <form name="signout" class="row signout-form" autocomplete="off">
                        <h3 class="message">Are You Sure You Want to Sign Out?</h3>
                            <input type="hidden" name="SESuserId" id="SESuserId" value="<cfoutput>#SESSION.USERID#</cfoutput>">
                            <div class="button-confirm-signout text-center">
                                <button type="button" class="btn green-gd btn-re btn-modal-signout">YES, SIGN OUT NOW</button>
                                <button type="button" class="btn blue-gd btn-re" data-dismiss="modal">CANCEL</button>
                            </div>
                        </form>    
                    </div>
                </div>
            </div>
        </div>

        <!-- BEGIN CORE PLUGINS -->
        <cfinclude template="components\assets-global-js.cfm">
        <!-- END CORE PLUGINS -->

        <!-- BEGIN PAGE LEVEL PLUGINS -->     
        <cfif (isDefined("Request._js_plugin") && isArray(Request._js_plugin))>
            <cfloop array="#Request._js_plugin#" index="jsPath">
                <script src="#jsPath#"></script>
            </cfloop>
            <cfset Request._js_plugin=[]>
        </cfif>   
        <!-- END PAGE LEVEL PLUGINS -->

        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="/session/sire/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->

        <!-- BEGIN PAGE LEVEL SCRIPTS -->
         <cfif (isDefined("Request._js") && isArray(Request._js))>
            <cfloop array="#Request._js#" index="jsPath">
                <script src="#jsPath#"></script>
            </cfloop>
            <cfset Request._js=[]>
        </cfif>   
        <!-- END PAGE LEVEL SCRIPTS -->

        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="/session/sire/assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="/session/sire/assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        <cfinclude template="../../../../public/sire/views/commons/livezilla_script.cfm">
    </body>

</html>
</cfoutput>