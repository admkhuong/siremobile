<div class="row" id="contacts">
	<div class="container-fluid">
		<div class="row">
			<div class="container-fluid">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 invite-friend-input no-padding">
					<div class="clearfix">
						<div class="col-lg-6 col-xs-8 col-md-6 no-padding">
							<form id="email_form">
								<input type="text" class="form-control input-medium validate[required,custom[email]]" id="emailadd" placeholder="Email Address">
							</form>
						</div>
						<div class="col-lg-3 col-xs-4 col-md-3 no-padding-right">
							<button class="btn btn-medium btn-success-custom" id="add-email-btn" type="button" style="width: 100%;">Add email</button>
						</div>
					</div>
				</div>
				<div class="col-lg-5 col-md-4 col-sm-12 col-xs-12 pull-right no-padding invite-friend-input">
					<div class="col-lg-9 col-xs-8 no-padding">
						<input type="text" class="form-control input-medium search" id="input-search" placeholder="Search">
					</div>
					<div class="col-lg-3 col-xs-4 no-padding-right">
						<button class="btn btn-medium btn-success-custom" id="search-btn" style="width: 100%" type="button">Search</button>
					</div>
			    </div>
		    </div>
	    </div>
	    <!-- /input-group -->
		<div class="mail-pills col-lg-3 col-md-3 col-sm-4 col-xs-12 no-padding">
			<ul>
				<li class="active">
					<a href="#gmail" class="gmail-contacts-btn" id="gmail-contacts-btn" data-toggle="pill">
						<div class="media">
						  	<div class="media-left">
						      	<img class="media-object" src="../images/icon/icon-gmail.png" alt="...">
					  		</div>
					  		<div class="media-body">
				    			<h4 class="media-heading">Import Gmail Contacts</h4>
						  	</div>
						</div>
					</a>
				</li>
				<li>
					<a href="#outlook" class="outlook-contacts-btn" id="outlook-contacts-btn" data-toggle="pill">
						<div class="media">
						  <div class="media-left">
						      <img class="media-object" src="../images/icon/icon-outlook.png" alt="...">
						  </div>
						  <div class="media-body">
						    <h4 class="media-heading">Import Outlook Contacts</h4>
						  </div>
						</div>
					</a>
				</li>
				<li>
					<a href="#yahoo" class="yahoo-contacts-btn" id="yahoo-contacts-btn" data-toggle="pill">
						<div class="media">
						  <div class="media-left">
						      <img class="media-object" src="../images/icon/icon-yahoo.png" alt="...">
						  </div>
						  <div class="media-body">
						    <h4 class="media-heading">Import Yahoo Mail Contacts</h4>
						  </div>
						</div>
					</a>
				</li>
	    	</ul>
    	</div>
    	<div class="clearfix visible-xs"></div>
		<div class="tab-content col-lg-9 col-sm-8 col-md-9 no-padding-right">
			<div class="friends-list tab-pane fade in active" id="gmail">
				<div class="row list">
				</div>
			</div>
			<div class="friends-list tab-pane fade" id="outlook">
				<div class="row list">
				</div>
			</div>
			<div class="friends-list tab-pane fade" id="yahoo">
				<div class="row list">
				</div>
			</div>
			<h4><strong>Selected Emails</strong></h4>
			<div class="selected-email clearfix" id="selected-email">
			</div>
    	</div>

	</div>

</div>
<div class="col-lg-1 col-md-2 col-sm-2 col-xs-6 pull-right no-padding">
	<button class="btn btn-medium btn-success-custom" id="send-email-invitation-btn" data-type="email" type="button" style="width: 100%;margin-top:20px">Send invite</button>
</div>