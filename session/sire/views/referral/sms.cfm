<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.validationEngine.Functions.js">
</cfinvoke>
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js">
</cfinvoke>

<div class="row">
	<div class="col-lg-4 col-md-5 col-sm-6">
	<!--- Validated Add new phone number form --->
		<form name="add_new_phonenumber" id="add_new_phonenumber">
			<div class="input-group input-group-custom">
		        <input type="text" id="phonenumber" class="form-control input-medium usNumber validate[required,custom[usPhoneNumber]]" placeholder="Phone Number" <cfif smsQuota.SMSQUOTA EQ 0>disabled</cfif>>
		        <span class="input-group-btn input-group-btn">
		            <button  class="btn btn-medium btn-success-custom" id="add-new-phone-btn" <cfif smsQuota.SMSQUOTA EQ 0>disabled</cfif>>Add</button>
		        </span>
	    	</div><!-- /input-group -->
		</form>
	</div>
	<div class="col-lg-4 col-md-5 col-sm-6">
		<cfif smsQuota.SMSQUOTA GT 0>
			<p style="padding-top:2%"><strong id="sms-quota-status">You can invite <span id="sms-quota-index"><cfoutput>#smsQuota.SMSQUOTA#</cfoutput></span> more friends today</strong></p>
		<cfelse>
			<p style="padding-top:2%;color:red"><strong>Sorry, You used all your sms invite quota</strong></p>
		</cfif>
	</div>
</div>

<div class="row">
	<div class="container-fluid">
		<h4><strong>Added Phone Number(s)</strong></h4>
		<div class="selected-email clearfix sms-list height-auto" id="selected-sms">
			<!--- Added Phone Number --->
		</div>
	</div>
</div>
<div class="text-right margin-top-10">
	<button id="btn-send-sms-invitation" type="submit" data-type="sms" class="btn btn-medium btn-success-custom" <cfif smsQuota.SMSQUOTA EQ 0>disabled</cfif>>Send Invite</button>
</div>

<style type="text/css">
	div.height-auto {
		min-height: 91px;
		height: auto!important;
	}
</style>