<div class="row">
	<div class="col-lg-8 col-md-7 col-sm-6">
		<div class="social share-url">
			<a class="social-auth"><i class="fa fa-facebook-square"></i></a>
			<a class="social-auth-tw"><i class="fa fa-twitter-square"></i></a>
			<a class="social-auth"><i class="fa fa-google-plus-square"></i></a>
			<!--- <a class="social-auth"><i class="fa fa-linkedin-square"></i></a> --->
			<!--- <a class="social-auth"><i class="fa fa-pinterest-square"></i></a> --->
		</div>
	</div>
	<div class="col-lg-4 col-md-5 col-sm-6">
		<div class="input-group input-group-custom">
	      <input type="text" class="form-control input-medium" id="search-inp" placeholder="Search for twitter...">
	      <span class="input-group-btn input-group-btn">
	        <button class="btn btn-medium btn-success-custom search-btn" id="twitter-search-btn" type="button">Search</button>
	      </span>
	    </div><!-- /input-group -->
	</div>
</div>



<div role="tabpanel" class="tab-pane" id="refer_via_social">
	<div class="friends-list" id="twitter">
		<div id="twitter-friend-list" class="row list">
			
		</div>
	</div>
	<h4><strong>Selected Friends</strong></h4>
	<div class="friends-list selected-email">
		<div id="twitter-friend-list-selected" class="row">

		</div>
	</div>
	<div class="text-right margin-top-10">
		<button disabled="disabled" type="button" class="btn btn-medium btn-success-custom send-invitation">Send Invite</button>
	</div>
</div>

<style type="text/css">
div.friends-list{
	height: auto!important;
	max-height: 250px;
	overflow-y: auto;
}
div.custom-checkbox label span.name {
	padding-left: 10px;
	vertical-align: top;
	font-weight: 600;
}

div.friend-list-item {
	cursor: pointer;
}
div.alert {
	padding: 20px;
}
</style>