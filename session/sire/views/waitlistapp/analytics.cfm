<div class="row">
	<style type="text/css">
		div.morris-label-list
		{
			padding: 0px!important;
			margin-top: 20px;
		}

		p.morris-label {
			font-size: 14px;
		}

		span.square {
			margin-top: 2px;
			margin-right: 5px;
			width: 14px;
			height: 14px;
		}
		div.summary-item {
			margin: 30px 0px;
		}

		span.line {
		
			width: 30px;
			height: 4px;
			margin-top: 10px;
			margin-right: 5px;
		}

		h2.box-title {
			color: #085f7a;
			font-size: 20px;
			margin: 0px;
		}

		span.morris-label {
			float: left;
			display: block;
		}

		p.summary-label {
			font-size: 40px;
		}

		span.waitlisted {
			background-color: #085f7a;
		}

		span.served {
			background-color: #7ccbe4;
		}
		span.no-show {
			background-color: #71c27a;
		}
		div.daterangepicker-box{
			position: relative;
		}
		div.daterangepicker-box > i.glyphicon-calendar {
			position: absolute;
			top: 35px;
			right: 10px;
		}

		.cursor-pointer {
			cursor: pointer;
		}
	</style>

	<div class="container-fluid">
		<div class="col-lg-12 col-sm-12 col-md-12">
			<div class="row">
				<div class="col-xs-12"><h2 style="margin: 0px;" class="text-center"><strong>Analytics</strong></h2></div>		
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-10">
					<div class="row">
						<div class="col-md-4">
							<div class="daterangepicker-box">
								<label style="display: block;">Start Date:</label>
								<input type="text" data-type="daterange" data-control="start-date" class="form-control">
								<i class="glyphicon glyphicon-calendar fa fa-calendar cursor-pointer"></i>
							</div>
						</div>
						<div class="col-md-4">
							<div class="daterangepicker-box">
								<label style="display: block;">End Date:</label>
								<input type="text" data-type="daterange" data-control="end-date" class="form-control">
								<i class="glyphicon glyphicon-calendar fa fa-calendar cursor-pointer"></i>
							</div>
							<cfoutput>
								<input type="hidden" name="inpStartDate" id="inpStartDate" data-control="start-date" value="#DateFormat(NOW(), "yyyy-mm-dd")#" data-value="#DateFormat(NOW(), "mm/dd/yyyy")#">
								<input type="hidden" name="inpEndDate" id="inpEndDate" data-control="end-date" value="#DateFormat(NOW(), "yyyy-mm-dd")#" data-value="#DateFormat(NOW(), "mm/dd/yyyy")#">
							</cfoutput>
						</div>
						<div class="col-md-4">
							<div class="daterangepicker-box">
								<label style="display: block;">Waitlist:</label>
								<select class="form-control" id="waitlist-select-analytic">
									<cfif  URL.com EQ 'analytics'>
										<option value="0">--All waitlist--</option>
									</cfif>
									<cfoutput>
										<cfloop array="#WaitLists.RESULT#" index="list">
											<option data-listid="#list.ID#" value="#list.ID#" #iIf(list.ID EQ currentWaitListId, DE("selected"), DE(""))#>#list.NAME#</option>
										</cfloop>
									</cfoutput>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-2">
					<label style="display: block;">&nbsp;</label>
					<button id="view-analytic" style="line-height: 1.25;" class="btn btn-success-custom">View</button>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-12"><h2 class="text-center box-title">Summary</h2></div>
			</div>
			<div class="row">
				<cfoutput>
				<div class="col-md-4 col-sm-4 col-sx-12"><div class="summary-item text-center"><p class="summary-label"><strong class="summary-value" data-prop="waitlisted"></strong></p><p><span >Total Waitlisted</span></p></div></div>
				<div class="col-md-4 col-sm-4 col-sx-12"><div class="summary-item text-center"><p class="summary-label"><strong class="summary-value" data-prop="served"></strong></p><p><span >Total Served</span></p></div></div>
				<div class="col-md-4 col-sm-4 col-sx-12"><div class="summary-item text-center"><p class="summary-label"><strong class="summary-value" data-prop="noshow"></strong></p><p><span>Total No Shows</span></p></div></div>
				<!--- <div class="col-md-3 col-sm-6 col-sx-12"><div class="summary-item text-center"><p class="summary-label"><strong  class="summary-value" data-prop="sms"></strong></p><p><span>SMS Sent</span></p></div></div> --->
				</cfoutput>
			</div>
			<hr>
			<br><br><br>
			<div class="row">
				<div class="col-md-6 col-sm-12">
					<div class="row">
						<div class="col-md-12"><h2 class="text-center box-title">Timeline</h2></div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-9">
									<div id="graph"></div>
								</div>
								<div class="col-md-3 morris-label-list">
									<p class="morris-label"><span class="line morris-label waitlisted"></span>Waitlisted Guests</p>
									<p class="morris-label"><span class="line morris-label served"></span>Served Guests</p>
									<p class="morris-label"><span class="line morris-label no-show"></span>No Show Guests</p>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-12">
					<div class="row">
						<div class="col-md-12"><h2 class="text-center box-title">By Week</h2></div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-9">
									<div id="graph-bar-by-weekdays"></div>
								</div>
								<div class="col-md-3 morris-label-list">
									<p class="morris-label"><span class="square morris-label waitlisted"></span>Waitlisted Guests</p>
									<p class="morris-label"><span class="square morris-label served"></span>Served Guests</p>
									<p class="morris-label"><span class="square morris-label no-show"></span>No Show Guests</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
			<br>
			<br>
			<div class="row">
				<div class="col-md-12"><h2 class="text-center box-title">By Hour</h2></div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div id="graph-bar-by-hours"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<div class="row morris-label-list">
						<div class="col-md-4 col-xs-12"><p class="morris-label"><span class="square morris-label waitlisted"></span>Waitlisted Guests</p></div>
						<div class="col-md-4 col-xs-12"><p class="morris-label"><span class="square morris-label served"></span>Served Guests</p></div>
						<div class="col-md-4 col-xs-12"><p class="morris-label"><span class="square morris-label no-show"></span>No Show Guests</p></div>
					</div>
				</div>
				<div class="col-md-3"></div>
			</div>
    	</div>

	</div>
</div>


<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/vendors/morris/raphael-min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/site/waitlistapp-analytic.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/js/vendors/morris/morris.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/public/sire/css/daterangepicker.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/moment.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/daterangepicker.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/vendors/morris/morris.min.js">
</cfinvoke>






