<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12"><h2 style="margin: 0px;" class="text-center"><strong>Customers</strong></h2></div>		
	</div>
	<div class="row">
		<div class="col-xs-12"><p style="margin-bottom: 5px;">Search</p></div>		
	</div>
	<div class="row">
		<div class="col-md-2 col-sm-4 col-xs-12">
			<div class="form-group">
				<input type="text" placeholder="Name, Phone or Email" class="form-control filter-control" data-operator="LIKE" id="txtKeyword" name="">
			</div>
		</div>
		<div class="col-md-2 col-sm-4 col-xs-12">
			<div class="form-group">
				<select class="form-control" id="waitlist-select-customer">
					<option value="0">--All waitlist--</option>
					<cfoutput>
						<cfloop array="#WaitLists.RESULT#" index="list">
							<option value="#list.ID#" #iIf(list.ID EQ currentWaitListId, DE("selected"), DE(""))#>#list.NAME#</option>
						</cfloop>
					</cfoutput>
				</select>
			</div>
		</div>
		<div class="col-md-8 col-sm-4 col-xs-12">
			<div class="form-group">
				<button class="btn btn-success-custom" style="line-height: 1.25;" id="search">Search</button>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 table-responsive">
			<table id="tblListEMS" class="table table-bordered table-responsive table-custom">
				<thead>
					<tr>
						<th>Name</th><th>Phone</th><th>Email</th><th>Update Date</th><th>Notes</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>

<style type="text/css">
#tblListEMS_wrapper {
	overflow-x: auto;
}
</style>
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/site/waitlistapp-customer.js">
</cfinvoke>


