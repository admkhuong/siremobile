<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/site/waitlistapp/history.js">
</cfinvoke>
<cfinclude template="todaystatistic.cfm" />
<div class="col-md-9">
	<div class="col-right">
		<div class="row">
			<div class="container-fluid">
				<div class="col-lg-12 col-sm-12 col-md-12 no-padding-right">
					<div class="row">
						<div class="col-md-3 col-sm-4 col-xs-12">
							<div class="form-group">
								<input type="text" placeholder="Name, Phone or Email" class="form-control filter-control" data-operator="LIKE" id="txtKeyword-history" name="">
							</div>
						</div>
						<div class="col-md-9 col-sm-8 col-xs-12">
							<div class="form-group">
								<button class="btn btn-success-custom" style="line-height: 1.25;" id="search-history">Search</button>
							</div>
						</div>
					</div>
					<div class="row list">
						<div class="col-md-12 col-sm-12 col-lg-12">
							<div class="table-responsive">
								<table id="waitlisthistory" class="table table-striped table-bordered table-hover table-custom">
									<thead>
										<tr>
											<th nowrap style="width: 10%">Number</th><th>Information</th><th>Action</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
		    	</div>
			</div>
		</div>
	</div>
</div>