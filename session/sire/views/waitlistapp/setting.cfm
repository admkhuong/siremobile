<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/site/waitlistapp/setting.js">
</cfinvoke>

<a class="hidden" id="dafault-template-id"><cfoutput>#DefaultWaitlistTemplate#</cfoutput></a>
<div class="col-xs-12" id="create-edit-campaign">
	<a id="create-campaign-link" class="btn btn-success-custom">Create Campaign</a>
	<!--- <a id="edit-campaign-link" href="/session/sire/pages/campaign-edit?campaignid=<cfoutput>#DefaultWaitlistTemplate#</cfoutput>" class="btn btn-success-custom">Edit Campaign</a> --->
</div>
<div class="row">
	<div class="col-lg-12 col-xs-12 col-sm-12">
		<input type="text" name="UserId" id="user-id" class="hidden" value="<cfoutput>#Session.USERID#</cfoutput>">
		<div class="row content waitlistapp-content">
			<h3><strong>Waitlist Settings</strong></h3>
			<div class="row col-lg-3 col-md-3 col-sm-3 waitlist-setting-sec">
				<h5 class="waitlist-setting-title">Type of customers</h5>
				<p>Choose maximum size you want to display in your waitlist</p>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9">
				<div class="row">
					<div class="row col-lg-4 col-md-4 col-sm-4 col-xs-10">
						<label for="maxpartysize">Max size</label>
						<select id="max-party-size" class="form-control">
						<cfloop from="1" to="50" index="i">
							<cfoutput>
								<option value="#i#">#i#</option>
							</cfoutput>
						</cfloop>
						</select>
					</div>
				</div>
			</div>
		</div>

		<hr>

		<div class="row content waitlistapp-content">
			<h3><strong>Customize SMS</strong></h3>
			<div class="row col-lg-3 col-md-3 col-sm-3 waitlist-setting-sec">
				<h5 class="waitlist-setting-title">Confirmation text</h5>
				<p>Sent automatically to new customers added to your waitlist</p>
			</div>

			<div class="col-lg-9 col-md-9 col-sm-9">
				<div class="row">
					<div class="row col-lg-2 col-md-2 col-sm-2">
						<label class="switch">
							<input type="checkbox" id="confirm-text-status">
							<div class="slider round"></div>
						</label>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<form class="row" name="update-confirmation-text-form" id="update-confirmation-text-form">
							<div class="form-group" id="confirm-text-area">
								<label for="updateconfirmationtext">Update text</label>
								<label class="pull-right" id="confirm-text-countdown-label"><a id="confirm-text-countdown">160</a> left</label>
								<textarea class="form-control no-resize validate[required,maxSize[160]]" id="confirmation-text-content" name="confirmation-text-content" placeholder="Update text (Limit)" rows="6" data-prompt-position="topLeft:100"></textarea>
							</div>
							<div class="">
								<div class="col-xs-6 col-sm-8 col-lg-9 bottom-quote">
									<label class="placehoders-quote">Placehoders</label>
									<a class="label label-help-buble" data-toggle="popover" data-trigger="hover" data-content="Simply add these variables in your SMS texts to personalize them. Hover over each variable to learn about it. Click to add in." data-placement="bottom">?</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Your Company/Organization Name" data-placement="top">[company]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Your customer's Name" data-placement="top">[name]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Size of customer party" data-placement="top">[size]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Customer's order in line (number)" data-placement="top">[order]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="The date you add your customer to waitlist" data-placement="top">[date]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="URL to your public waitlist for customers to view current wait and cancel their spot. Recommended to include." data-placement="top">[link]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Time you quoted customer" data-placement="top">[quote]</a>
								</div>
								<div class="col-xs-6 col-sm-4 col-lg-3 waitlist-setting-btn">
									<button class="btn btn-default btn-sm pull-right" id="reset-confirmation-text">Reset</button>
									<button class="btn btn-save-waitlist-setting btn-sm pull-right" id="save-confirmation-text" type="submit">Save</button>
								</div>
							</div>
						</form>
					</div>

				</div>
			</div>
		</div>

		<hr>

		<div class="row content waitlistapp-content">
			<div class="row col-lg-3 col-md-3 col-sm-3 waitlist-setting-sec">
				<h5 class="waitlist-setting-title">Alert text</h5>
				<p>When you click on “Alert” button on Waitlist screen, this alert text will be sent to customer. Your customer can reply to your SMS directly and it will be shown on your waitlist SMS conversation</p>
			</div>

			<div class="col-lg-9 col-md-9 col-sm-9">
				<div class="row">
					<div class="row col-lg-2 col-md-2 col-sm-2">
						<label class="switch">
							<input type="checkbox" id="alert-text-status">
							<div class="slider round"></div>
						</label>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<form class="row" name="update-alert-text-form" id="update-alert-text-form">
							<div class="form-group" id="alert-text-area">
								<label for="updatealerttext">Update text</label>
								<label class="pull-right" id="alert-text-countdown-label"><a id="alert-text-countdown">160</a> left</label>
								<textarea class="form-control no-resize validate[required,maxSize[160]]" id="alert-text-content" name="alert-text-content" placeholder="Update text (Limit)" rows="6" data-prompt-position="topLeft:100"></textarea>
							</div>
							<div class="">
								<div class="col-xs-6 col-sm-8 col-lg-9 bottom-quote">
									<label class="placehoders-quote">Placehoders</label>
									<a class="label label-help-buble" data-toggle="popover" data-trigger="hover" data-content="Simply add these variables in your SMS texts to personalize them. Hover over each variable to learn about it. Click to add in." data-placement="bottom">?</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Your Company/Organization Name" data-placement="top">[company]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Your customer's Name" data-placement="top">[name]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Size of customer party" data-placement="top">[size]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Customer's order in line (number)" data-placement="top">[order]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="The date you add your customer to waitlist" data-placement="top">[date]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="URL to your public waitlist for customers to view current wait and cancel their spot. Recommended to include." data-placement="top">[link]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Time you quoted customer" data-placement="top">[quote]</a>
									
								</div>
								<div class="col-xs-6 col-sm-4 col-lg-3 waitlist-setting-btn">
									<button class="btn btn-default btn-sm pull-right" id="reset-alert-text">Reset</button>
									<button class="btn btn-save-waitlist-setting btn-sm pull-right" id="save-alert-text">Save</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<hr>

		<div class="row content waitlistapp-content">
			<div class="row col-lg-3 col-md-3 col-sm-3 waitlist-setting-sec">
				<h5 class="waitlist-setting-title">Notice text</h5>
				<p>When you change cusomter quote time on Waitlist screen, this notice text will be sent to customer. Your customer can reply to your SMS directly and it will be shown on your waitlist SMS conversation</p>
			</div>

			<div class="col-lg-9 col-md-9 col-sm-9">
				<div class="row">
					<div class="row col-lg-2 col-md-2 col-sm-2">
						<label class="switch">
							<input type="checkbox" id="notice-text-status">
							<div class="slider round"></div>
						</label>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<form class="row" name="update-notice-text-form" id="update-notice-text-form">
							<div class="form-group" id="notice-text-area">
								<label for="updatenoticetext">Update text</label>
								<label class="pull-right" id="notice-text-countdown-label"><a id="notice-text-countdown">160</a> left</label>
								<textarea class="form-control no-resize validate[required,maxSize[160]]" id="notice-text-content" name="notice-text-content" placeholder="Update text (Limit)" rows="6" data-prompt-position="topLeft:100"></textarea>
							</div>
							<div class="">
								<div class="col-xs-6 col-sm-8 col-lg-9 bottom-quote">
									<label class="placehoders-quote">Placehoders</label>
									<a class="label label-help-buble" data-toggle="popover" data-trigger="hover" data-content="Simply add these variables in your SMS texts to personalize them. Hover over each variable to learn about it. Click to add in." data-placement="bottom">?</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Your Company/Organization Name" data-placement="top">[company]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Your customer's Name" data-placement="top">[name]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Size of customer party" data-placement="top">[size]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Customer's order in line (number)" data-placement="top">[order]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="The date you add your customer to waitlist" data-placement="top">[date]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="URL to your public waitlist for customers to view current wait and cancel their spot. Recommended to include." data-placement="top">[link]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Time you quoted customer" data-placement="top">[quote]</a>
									
								</div>
								<div class="col-xs-6 col-sm-4 col-lg-3 waitlist-setting-btn">
									<button class="btn btn-default btn-sm pull-right" id="reset-notice-text">Reset</button>
									<button class="btn btn-save-waitlist-setting btn-sm pull-right" id="save-notice-text">Save</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<hr>

		<div class="row content waitlistapp-content">
			<h3><strong>Customize emails</strong></h3>
			<div class="row col-lg-3 col-md-3 col-sm-3 waitlist-setting-sec">
				<h5 class="waitlist-setting-title">Confirmation email</h5>
				<p>Sent automatically to new customers added to your waitlist</p>
			</div>

			<div class="col-lg-9 col-md-9 col-sm-9">
				<div class="row">
					<div class="row col-lg-2 col-md-2 col-sm-2">
						<label class="switch">
							<input type="checkbox" id="confirm-email-status">
							<div class="slider round"></div>
						</label>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<form class="row" name="update-confirmation-email-form" id="update-confirmation-email-form">
							<div class="form-group">
								<label for="confirmationemailsubject">Subject</label>
								<label class="pull-right" id="confirm-email-sub-countdown-label"><a id="confirm-email-sub-countdown">255</a> left</label>
								<input type="text" class="form-control validate[required,maxSize[255]]" id="confirmation-email-subject" name="confirmation-email-subject" placeholder="You have been waitlisted to [name] on [date]." data-prompt-position="topLeft:100"/>
							</div>
							<div class="form-group">
								<label for="updateconfirmationemail">Message</label>
								<label class="pull-right" id="confirm-email-mess-countdown-label"><a id="confirm-email-mess-countdown">1000</a> left</label>
								<textarea class="form-control no-resize validate[required,maxSize[1000]]" id="confirmation-email-message" name="confirmation-email-message" placeholder="Update text (Limit)" rows="6" data-prompt-position="topLeft:100"></textarea>
							</div>
							<div class="">
								<div class="col-xs-6 col-sm-8 col-lg-9 bottom-quote">
									<label class="placehoders-quote">Placehoders</label>
									<a class="label label-help-buble" data-toggle="popover" data-trigger="hover" data-content="Simply add these variables in your SMS texts to personalize them. Hover over each variable to learn about it. Click to add in." data-placement="bottom">?</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Your Company/Organization Name" data-placement="top">[company]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Your customer's Name" data-placement="top">[name]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Size of customer party" data-placement="top">[size]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Customer's order in line (number)" data-placement="top">[order]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="The date you add your customer to waitlist" data-placement="top">[date]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="URL to your public waitlist for customers to view current wait and cancel their spot. Recommended to include." data-placement="top">[link]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Time you quoted customer" data-placement="top">[quote]</a>
									
								</div>
								<div class="col-xs-6 col-sm-4 col-lg-3 waitlist-setting-btn">
									<button class="btn btn-default btn-sm pull-right" id="reset-confirmation-email">Reset</button>
									<button class="btn btn-save-waitlist-setting btn-sm pull-right" id="save-confirmation-email">Save</button>
								</div>
							</div>
							 
						</form>
					</div>
				</div>
			</div>
		</div>

		<hr>

		<div class="row content waitlistapp-content">
			<div class="row col-lg-3 col-md-3 col-sm-3 waitlist-setting-sec">
				<h5 class="waitlist-setting-title">Alert email</h5>
				<p>This alert email is to send email</p>
			</div>

			<div class="col-lg-9 col-md-9 col-sm-9">
				<div class="row">
					<div class="row col-lg-2 col-md-2 col-sm-2">
						<label class="switch">
							<input type="checkbox" id="alert-email-status">
							<div class="slider round"></div>
						</label>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<form class="row" name="update-alert-email-form" id="update-alert-email-form">
							<div class="form-group">
								<label for="confirmationemailsubject">Subject</label>
								<label class="pull-right" id="alert-email-sub-countdown-label"><a id="alert-email-sub-countdown">255</a> left</label>
								<input type="text" class="form-control validate[required,maxSize[255]]" id="alert-email-subject" name="alert-email-subject" placeholder="Great! It’s your turn at [company]." data-prompt-position="topLeft:100"/>
							</div>

							<div class="form-group">
								<label for="updatealertemail">Message</label>
								<label class="pull-right" id="alert-email-mess-countdown-label"><a id="alert-email-mess-countdown">1000</a> left</label>
								<textarea class="form-control no-resize validate[required,maxSize[1000]]" id="alert-email-message" name="alert-email-message" placeholder="Update text (Limit)" rows="6" data-prompt-position="topLeft:100"></textarea>
							</div>
							<div class="">
								<div class="col-xs-6 col-sm-8 col-lg-9 bottom-quote">
									<label class="placehoders-quote">Placehoders</label>
									<a class="label label-help-buble" data-toggle="popover" data-trigger="hover" data-content="Simply add these variables in your SMS texts to personalize them. Hover over each variable to learn about it. Click to add in." data-placement="bottom">?</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Your Company/Organization Name" data-placement="top">[company]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Your customer's Name" data-placement="top">[name]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Size of customer party" data-placement="top">[size]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Customer's order in line (number)" data-placement="top">[order]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="The date you add your customer to waitlist" data-placement="top">[date]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="URL to your public waitlist for customers to view current wait and cancel their spot. Recommended to include." data-placement="top">[link]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Time you quoted customer" data-placement="top">[quote]</a>
									
								</div>
								<div class="col-xs-6 col-sm-4 col-lg-3 waitlist-setting-btn">
									<button class="btn btn-default btn-sm pull-right" id="reset-alert-email">Reset</button>
									<button class="btn btn-save-waitlist-setting btn-sm pull-right" id="save-alert-email">Save</button>
								</div>
							</div>
							
						</form>
					</div>
				</div>
			</div>
		</div>

		<hr>

		<div class="row content waitlistapp-content">
			<div class="row col-lg-3 col-md-3 col-sm-3 waitlist-setting-sec">
				<h5 class="waitlist-setting-title">Notice email</h5>
				<p>This notice email is to send email when ever you change quote time of your customer</p>
			</div>

			<div class="col-lg-9 col-md-9 col-sm-9">
				<div class="row">
					<div class="row col-lg-2 col-md-2 col-sm-2">
						<label class="switch">
							<input type="checkbox" id="notice-email-status">
							<div class="slider round"></div>
						</label>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<form class="row" name="update-notice-email-form" id="update-notice-email-form">
							<div class="form-group">
								<label for="confirmationemailsubject">Subject</label>
								<label class="pull-right" id="notice-email-sub-countdown-label"><a id="notice-email-sub-countdown">255</a> left</label>
								<input type="text" class="form-control validate[required,maxSize[255]]" id="notice-email-subject" name="notice-email-subject" placeholder="Great! It’s your turn at [company]." data-prompt-position="topLeft:100"/>
							</div>

							<div class="form-group">
								<label for="updatenoticeemail">Message</label>
								<label class="pull-right" id="notice-email-mess-countdown-label"><a id="notice-email-mess-countdown">1000</a> left</label>
								<textarea class="form-control no-resize validate[required,maxSize[1000]]" id="notice-email-message" name="notice-email-message" placeholder="Update text (Limit)" rows="6" data-prompt-position="topLeft:100"></textarea>
							</div>
							<div class="">
								<div class="col-xs-6 col-sm-8 col-lg-9 bottom-quote">
									<label class="placehoders-quote">Placehoders</label>
									<a class="label label-help-buble" data-toggle="popover" data-trigger="hover" data-content="Simply add these variables in your SMS texts to personalize them. Hover over each variable to learn about it. Click to add in." data-placement="bottom">?</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Your Company/Organization Name" data-placement="top">[company]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Your customer's Name" data-placement="top">[name]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Size of customer party" data-placement="top">[size]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Customer's order in line (number)" data-placement="top">[order]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="The date you add your customer to waitlist" data-placement="top">[date]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="URL to your public waitlist for customers to view current wait and cancel their spot. Recommended to include." data-placement="top">[link]</a>
									<a class="label label-as-quote" data-toggle="popover" data-trigger="hover" data-content="Time you quoted customer" data-placement="top">[quote]</a>
									
								</div>
								<div class="col-xs-6 col-sm-4 col-lg-3 waitlist-setting-btn">
									<button class="btn btn-default btn-sm pull-right" id="reset-notice-email">Reset</button>
									<button class="btn btn-save-waitlist-setting btn-sm pull-right" id="save-notice-email">Save</button>
								</div>
							</div>
							
						</form>
					</div>
				</div>
			</div>
		</div>

		<hr>

		<div class="row content waitlistapp-content">
			<div class="row col-lg-3 col-md-3 col-sm-3 waitlist-setting-sec">
				<h5 class="waitlist-setting-title">Add your services</h5>
				<p>If you have more than one service, you can add here to manage waitlist by services</p>
			</div>

			<div class="col-lg-9 col-md-9 col-sm-9">
				<div class="row">
					<div class="row col-lg-2 col-md-2 col-sm-2">
						<label class="switch">
							<input type="checkbox" id="add-service-status">
							<div class="slider round"></div>
						</label>
					</div>
				</div>

				<div class="row" id="add-service">
					
				</div>
				<div class="row ">
					<div type="button" class="btn btn-success-custom" data-toggle="modal" data-target="#add-new-service" id="add-service-btn">Add Service</div>
				</div>

			</div>
		</div>

		<hr>

		<div class="row content waitlistapp-content">
			<div class="row col-lg-3 col-md-3 col-sm-3 waitlist-setting-sec">
				<h5 class="waitlist-setting-title">Estimated Wait</h5>
				<p>Add field to the waitlist to estimate your customers the expected wait time</p>
			</div>

			<div class="col-lg-9 col-md-9 col-sm-9">
				<div class="row">
					<div class="row col-lg-2 col-md-2 col-sm-2">
						<label class="switch">
							<input type="checkbox" id="quote-wait-status">
							<div class="slider round"></div>
						</label>
					</div>
				</div>
				<div class="row">
					<div class="row col-lg-4 col-md-4 col-sm-4 col-xs-10">
						<label for="quotetimeintervals" id="quotetime-label">Estimated time intervals</label>
						<select id="quote-time-intervals" class="form-control">
							<cfloop from="1" to="6" index="i">
								<cfoutput>
									<option value="#i*5#">#i*5# minutes</option>
								</cfoutput>
							</cfloop>

							<option value="60">1 hour</option>

							<cfloop from="2" to="5" index="j">
								<cfoutput>
									<option value="#j*60#">#j# hours</option>
								</cfoutput>
							</cfloop>
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!--- Add new waitlist service modal --->
<div class="modal fade" id="add-new-service" tabindex="-1" role="dialog" aria-labelledby="addNewServiceLabel">
    <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      	<div class="modal-body">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <div class="modal-body-inner row">
		        	<h4 class="modal-title col-sm-8">Add new Waitlist Service</h4>
		        	<form name="add-waitlist-service" class="col-sm-12" autocomplete="off" id="add-waitlist-service-form">
					  <div class="form-group">
					  	<label for="service" id="service-label">Service</label>
					  	<input type="text" class="form-control validate[required,maxSize[255], custom[noHTML]]" id="new-service-name" placeholder="" name="new-service-name">
					  </div>
					  <div class="form-group">
					  	<label for="service-description" id="service-description-label">Description</label>
					  	<textarea class="form-control validate[maxSize[1000], custom[noHTML]]" id="new-service-description" name="new-service-description" placeholder="" rows="4"></textarea>
					  </div>
					</form>
					<div class="col-sm-4 pull-right row">
					  	<button class="btn btn-success-custom" id="save-service-btn" type="submit">Save</button>
					  	<button class="btn btn-back-custom" id="cancel-service-btn" data-dismiss="modal">Cancel</button>
					</div>
		        </div>
	        </div>
	    </div>
    </div>
</div>