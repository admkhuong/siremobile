<cfoutput>
	<div class="col-md-3 pr10">
		<div class="col-left">
			<div class="item">
				<p class="name">Served Today</p>
				<p class="number span-served-today">#todayStatistic.STATUS['served']#</p>
			</div>
			<div class="item">
				<p class="name">No-shows Today</p>
				<p class="number span-noshow-today">#todayStatistic.STATUS['noshow']#</p>
			</div>
			<div class="item">
				<p class="name">Texted Today</p>
				<p class="number span-texted-today">#todayStatistic.STATUS['texted']#</p>
			</div>
		</div>
	</div>
</cfoutput>