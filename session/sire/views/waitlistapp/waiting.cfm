<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js">
</cfinvoke>


<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/session/sire/js/site/waitlistapp/waiting.js" />
</cfinvoke>

<div>
<cfinclude template="todaystatistic.cfm" />
<div class="col-md-9">
	<div class="col-right">
		<div class="row">
			<div class="container-fluid">
				<div type="button" class="btn btn-success-custom" data-toggle="modal" data-target="#add-new-waitlist" id="add-to-waitlist-btn">Add To Waitlist</div>
				<div class="col-lg-12 col-sm-12 col-md-12 no-padding-right">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-lg-12">
							<div id="waiting-filter">
								<cfoutput>
									<!---set up column --->
									<cfset waitlist_datatable_ColumnModel = arrayNew(1) />
									<cfset arrayappend(waitlist_datatable_ColumnModel,{DISPLAY_NAME = 'Customer Name', CF_SQL_TYPE = 'CF_SQL_VARCHAR', TYPE='TEXT', SQL_FIELD_NAME = ' w.CustomerName_vch '}) />
									<cfset arrayappend(waitlist_datatable_ColumnModel,{DISPLAY_NAME = 'Waitlist Size', CF_SQL_TYPE = 'CF_SQL_INTEGER', TYPE='INTEGER', SQL_FIELD_NAME = ' w.WaitListSize_int '}) />
									<cfset waitlist_datatable_jsCallback = "GetWaitList" />
									<div class="col-lg-12">
										<cfinclude template="/session/sire/pages/waitlist-datatable-filter.cfm" />
									</div>
								</cfoutput>
							</div>
						</div>
					</div>
					<div class="row list">
						<div class="col-md-12 col-sm-12 col-lg-12">
							<div class="table-responsive">
								<table id="waitlist" class="table table-striped table-bordered table-hover table-custom">
									<thead>
										<tr>
											<th style="width: 10%;">Number</th><th>Information</th><th>Action</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="sms">
	<div class="modal-dialog">
		<div class="chat_window">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
			<div class="top_menu">
				<div class="title" id="chat-dialog-title"></div>
			</div>
			<ul class="messages"></ul>
			<div class="bottom_wrapper clearfix">
				<label class="pull-right text-info"><span id="chat-text-countdown">160</span> left</label>
				<div class="message_input_wrapper">
					<textarea class="form-control no-resize message_input" id="chat-content" placeholder="Type your message here..." rows="4" data-prompt-position="topLeft:100"></textarea>
				</div>
				<div class="send_message">
					<button type="button" class="btn btn-back-custom" data-dismiss="modal">Cancel</button> <button type="button" class="btn btn-success-custom btn-send">Send</button>
				</div>
			</div>
		</div>
		<div class="message_template">
			<ul>
				<li class="message">
					<div class="text_wrapper">
						<div class="text">
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>

<!--- Add new waitlist modal --->
<div class="modal fade" id="add-new-waitlist" tabindex="-1" role="dialog" aria-labelledby="addNewWaitlistLabel">
    <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      	<div class="modal-body">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <div class="modal-body-inner row">
		        	<h4 class="col-sm-8 modal-title">Add to Waitlist</h4>
		        	<form name="add-waitlist" class="col-sm-12" autocomplete="off" id="add-waitlist-form">
		        		<input type="text" name="UserId" id="user-id" class="hidden" value="<cfoutput>#Session.USERID#</cfoutput>">
		        		<div class="row col-sm-6">
		        			<div class="form-group has-feedback">
							  	<label for="customername" id="customer-name-label">Name</label>
							  	
							  	<input type="text" class="form-control add-waitlist-style validate[custom[noHTML],  maxSize[50]]" id="customer-name" placeholder="John Doe" name="customer-name">
							  	<i class="form-control-feedback inline-icon-waitlist-form fa fa-user"></i>
							  	
							</div>

							<div class="form-group has-feedback">
							  	<label for="customername" id="customer-email-label">Email</label>
							  	<input type="text" class="form-control add-waitlist-style validate[custom[email], custom[noHTML]]" id="customer-email" placeholder="johndoe@gmail.com" name="customer-email">
							  	<i class="form-control-feedback inline-icon-waitlist-form fa fa-envelope"></i>
							</div>

							<div class="form-group has-feedback">
								<label for="selectservice" id="select-service-label">Service Name</label>
								<select id="select-service" class="form-control">
									<cfloop from="1" to="#ArrayLen(userWaitlistAppService['services'])#" index="i">
										<cfoutput>
											<option value="#userWaitlistAppService['services'][#i#][1]#">#userWaitlistAppService['services'][#i#][2]#</option>
										</cfoutput>
									</cfloop>
								</select>
								<button type="button" class="btn btn-success-custom" data-toggle="modal" id="add-new-service-btn" data-toggle="modal" data-target="#add-new-service-waiting" style="margin-top: 5px">ADD SERVICE</button>
							</div>

							<div class="form-group has-feedback">
								<label for="customerorder" id="customer-order-label">Customer Order</label>
								<select id="customer-order" class="form-control">
									<!--- Order Number --->
								</select>
							</div>
		        		</div>

		        		<div class="row col-sm-6" id="right-input-waitlist">
		        			<div class="form-group has-feedback">
							  	<label for="customerphone" id="customer-phone-label">Phone</label>
							  	<input type="text" class="form-control add-waitlist-style" id="customer-phone" placeholder="(###)###-####" name="customer-phone">
							  	<i class="form-control-feedback inline-icon-waitlist-form fa fa-phone"></i>
							</div>
							<div class="form-group has-feedback">
								<label for="partysize" id="party-size-label">Size</label>
								<i class="form-control-feedback inline-icon-waitlist-form fa fa-users"></i>
								<select id="party-size" class="form-control add-waitlist-style">
								
									<cfloop from="1" to="#userWaitlistAppSetting.MAXSIZE#" index="j">
										<cfoutput>
											<option value="#j#">#j#</option>
										</cfoutput>
									</cfloop>
								</select>
							</div>

							<div class="form-group has-feedback">
								<label for="waittime" id="wait-time-label">Wait time</label>
								<i class="form-control-feedback inline-icon-waitlist-form fa fa-clock-o"></i>
								<select id="wait-time" class="form-control add-waitlist-style">
									<cfif userWaitlistAppSetting.QUOTETIME LT 60>
										<cfloop from="1" to="#userWaitlistAppSetting.QUOTETIME/5#" index="k">
											<cfoutput>
												<option value="#k*5#">#k*5# minutes</option>
											</cfoutput>
										</cfloop>
									<cfelse>
										<cfloop from="1" to="6" index="k">
											<cfoutput>
												<option value="#k*5#">#k*5# minutes</option>
											</cfoutput>
										</cfloop>
										<option value="60">1 hour</option>
										<cfloop from="2" to="#userWaitlistAppSetting.QUOTETIME/60#" index="l">
											<cfoutput>
												<option value="#l*60#">#l# hours</option>
											</cfoutput>
										</cfloop>
									</cfif>
								</select>
							</div>

							<div class="form-group has-feedback">
							  	<label for="addnotes" id="add-notes-label">Notes</label>
							  	<i class="form-control-feedback inline-icon-waitlist-form fa fa-pencil"></i>
							  	<input type="text" class="form-control add-waitlist-style validate[ maxSize[500]]" id="add-notes" placeholder="Additional notes" name="add-notes" />
							</div>
		        		</div>
					</form>
					<div class="confirm-waitlist-btn-group">
					  	<button class="btn btn-success-custom" id="confirm-add-waitlist-btn" type="submit">Add</button>
					  	<button class="btn btn-back-custom" id="close-add-waitlist-btn" data-dismiss="modal" style="margin-left: 10px">Close</button>
					</div>
		        </div>
	        </div>
	    </div>
    </div>
</div>
<!--- Modal end --->

<!--- Edit waitlist --->
<div class="modal fade" id="edit-waitlist" tabindex="-1" role="dialog" aria-labelledby="editWaitlistLabel">
    <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      	<div class="modal-body">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <div class="modal-body-inner row">
		        	<h4 class="col-sm-8 modal-title">Edit Waitlist</h4>
		        	<form name="edit-waitlist" class="col-sm-12" autocomplete="off" id="edit-waitlist-form">
		        		<div class="row col-sm-6">
		        			<div class="form-group has-feedback">
		        				<input type="hidden" name="waitlist-id" id="waitlist-id" value="">
							  	<label for="customername-edit" id="customer-name-label-edit">Name</label>
							  	<input type="text" class="form-control add-waitlist-style validate[custom[noHTML],  maxSize[50]]" id="customer-name-edit" name="customer-name-edit">
							  	<i class="form-control-feedback inline-icon-waitlist-form fa fa-user"></i>
							</div>

							<div class="form-group has-feedback">
							  	<label for="customeremail-edit" id="customer-email-label-edit">Email</label>
							  	<input type="text" class="form-control add-waitlist-style validate[custom[email], custom[noHTML]]" id="customer-email-edit" name="customer-email-edit">
							  	<i class="form-control-feedback inline-icon-waitlist-form fa fa-envelope"></i>
							</div>

							<div class="form-group has-feedback">
								<label for="selectservice-edit" id="select-service-label-edit">Service Name</label>
								<select id="select-service-edit" class="form-control add-waitlist-style">
									<!--- <cfloop from="1" to="#ArrayLen(userWaitlistAppService['services'])#" index="i">
										<cfoutput>
											<option value="#userWaitlistAppService['services'][#i#][1]#">#userWaitlistAppService['services'][#i#][2]#</option>
										</cfoutput>
									</cfloop> --->
								</select>
								<button type="button" class="btn btn-success-custom" data-toggle="modal" id="add-new-service-btn-edit" data-toggle="modal" data-target="#add-new-service-waiting" style="margin-top: 5px">ADD SERVICE</button>
							</div>

							<div class="form-group has-feedback">
								<label for="customerorderedit" id="customer-order-edit-label">Customer Order</label>
								<select id="customer-order-edit" class="form-control add-waitlist-style">
									<!--- Order Number --->
								</select>
							</div>
		        		</div>

		        		<div class="row col-sm-6" id="right-input-waitlist">
		        			<div class="form-group has-feedback">
							  	<label for="customerphone-edit" id="customer-phone-label-edit">Phone</label>
							  	<input type="text" class="form-control add-waitlist-style validate[custom[noHTML]]" id="customer-phone-edit" name="customer-phone-edit">
							  	<i class="form-control-feedback inline-icon-waitlist-form fa fa-phone"></i>
							</div>
							<div class="form-group has-feedback">
								<label for="partysize-edit" id="party-size-label-edit">Size</label>
								<i class="form-control-feedback inline-icon-waitlist-form fa fa-users"></i>
								<select id="party-size-edit" class="form-control add-waitlist-style">
								
									<!--- <cfloop from="1" to="#userWaitlistAppSetting.MAXSIZE#" index="j">
										<cfoutput>
											<option value="#j#">#j#</option>
										</cfoutput>
									</cfloop> --->
								</select>
							</div>

							<div class="form-group has-feedback">
								<label for="waittime-edit" id="wait-time-label-edit">Wait time</label>
								<i class="form-control-feedback inline-icon-waitlist-form fa fa-clock-o"></i>
								<select id="wait-time-edit" class="form-control add-waitlist-style">
									<!--- <cfif userWaitlistAppSetting.QUOTETIME LT 60>
										<cfloop from="1" to="#userWaitlistAppSetting.QUOTETIME/5#" index="k">
											<cfoutput>
												<option value="#k*5#">#k*5# minutes</option>
											</cfoutput>
										</cfloop>
									<cfelse>
										<cfloop from="1" to="6" index="k">
											<cfoutput>
												<option value="#k*5#">#k*5# minutes</option>
											</cfoutput>
										</cfloop>
										<option value="60">1 hour</option>
										<cfloop from="2" to="#userWaitlistAppSetting.QUOTETIME/60#" index="l">
											<cfoutput>
												<option value="#l*60#">#l# hours</option>
											</cfoutput>
										</cfloop>
									</cfif> --->
								</select>
							</div>
							<div class="form-group has-feedback">
							  	<label for="addnotes-edit" id="add-notes-label-edit">Notes</label>
							  	<i class="form-control-feedback inline-icon-waitlist-form fa fa-pencil"></i>
							  	<input type="text" class="form-control add-waitlist-style validate[ maxSize[500]]" id="add-notes-edit" name="add-notes-edit" />
							</div>
		        		</div>
					</form>
					<div class="confirm-edit-waitlist-btn-group">
						<button class="btn btn-success-custom" id="confirm-edit-waitlist-btn" type="submit">Save</button>
					  	<button class="btn btn-back-custom" id="close-edit-waitlist-btn" data-dismiss="modal" style="margin-left: 10px">Close</button>
					</div>
		        </div>
	        </div>
	    </div>
    </div>
</div>
<!--- Modal end --->

<!--- Add new waitlist service modal --->
<div class="modal fade" id="add-new-service-waiting" tabindex="-1" role="dialog" aria-labelledby="addNewServiceLabel">
    <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      	<div class="modal-body">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <div class="modal-body-inner row">
		        	<h4 class="modal-title col-sm-8">Add new Waitlist Service</h4>
		        	<form name="add-waitlist-service" class="col-sm-12" autocomplete="off" id="add-waitlist-service-form">
					  <div class="form-group">
					  	<label for="service" id="service-label">Service</label>
					  	<input type="text" class="form-control validate[required,maxSize[255], custom[noHTML]]" id="new-service-name" placeholder="" name="new-service-name">
					  </div>
					  <div class="form-group">
					  	<label for="service-description" id="service-description-label">Description</label>
					  	<textarea class="form-control validate[maxSize[1000], custom[noHTML]]" id="new-service-description" name="new-service-description" placeholder="" rows="4"></textarea>
					  </div>
					</form>
					<div class="col-sm-4 pull-right row">
					  	<button class="btn btn-success-custom" id="save-service-btn" type="button">Save</button>
					  	<button class="btn btn-back-custom" id="cancel-service-btn" data-dismiss="modal">Cancel</button>
					</div>
		        </div>
	        </div>
	    </div>
    </div>
</div>
<!--- Modal end --->
