<cfinclude template="../cfc/csc/constants.cfm">
<cfinvoke component="#LocalSessionDotPath#.cfc.csc.CSC" method="getShortCodeByUser" returnvariable="getShortCodeByUser">
<cfset SCMB =getShortCodeByUser.GETSCMBArr>
<cfparam name="ShowCancel" default="false" >

<!---<script language="javascript" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/js/sms/ui.spinner.js"></script>
<script language="javascript" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/js/sms/ui.spinner.js"></script>--->
<!---<link rel="stylesheet" type="text/css" href="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/css/sms/ui.spinner.css"/>--->
<cfinvoke component="#LocalSessionDotPath#.cfc.csc.CSC" method="getShortCodeByUser" returnvariable="getShortCodeByUser">
<cfif getShortCodeByUser.COUNTSCPENDING GT 0>
<div style="padding-top:5px;">
	<center>
		Request a new short code
	</center>
</div>
<cfelse>
<div class="SCRow">
	You have no short codes associcated with your account. Please choose one of following:
</div>
</cfif>
<!---begin option 1--->
<div class="SCRow">
	<input type="radio" name="SCType" id="SCNotExist" checked ="checked" value="1"> 
	<label for = "SCNotExist" >Add an existing short code that you own</label>
</div>
<div class="SCRow" id="SCNotExistContent">
	<div class ="SCEXitRow">
		<label for = "SCText" >Your short code</label>
		<input type="text" name="SCText" id="SCText">
	</div> 
</div>
<!---end option 1--->
<!---begin option 2--->
<div class="SCRow">
	<input type="radio" name="SCType" id="SCExist" value="2"> 
	<label for = "SCExist" >Use one owned and operated by <cfoutput>#BrandShort#</cfoutput></label>
</div>
<div class="SCRow" id="SCExistContent" style="display: none;">
	<div class ="SCEXitRow">
		<label>Available short codes</label>
		<select id="shotCodeList" onChange ="shotCodeListChange()">
			<cfoutput>
				<cfset iSCMB = 0>
				<cfloop array="#SCMB#" index="index">
					<cfset iSCMB ++>
					<option <cfif iSCMB eq 1> selected ="selected" </cfif> value="#SCMB[iSCMB].ShortCodeId_int#" alt="#SCMB[iSCMB].CLASSIFICATION_INT#" >
						#SCMB[iSCMB].shortcode_vch# ( 
							<cfif SCMB[iSCMB].CLASSIFICATION_INT EQ CLASSIFICATION_PUBLIC_NOTSHARED_VALUE >
								Not Shared
							<cfelseif SCMB[iSCMB].CLASSIFICATION_INT EQ CLASSIFICATION_PUBLIC_SHARED_VALUE>
								Shared
							</cfif>
						)
					</option>
				</cfloop>
			</cfoutput>
		</select>
	</div>
	<div id="SCKeyword" >
		<div class ="SCEXitRow" >
			This short code is shared with other users. Limitation on keywords is not uncommon. Unshared short codes have unlimited usage. 
		</div>
		<div class ="SCEXitRow">
			<label>Total number of keywords requested</label>
			<input type="text" id="numberKeyword" value="1" style="width: 40px;">
		</div>
	</div>
	<div class ="SCEXitRow">
		All keywords must be approved before being activated for a campaign.
	</div>
	<div class ="SCEXitRow">
		<label for = "SCVolume" >What kind of volume are you expecting accross keywords?</label>
		<input type="text" name="SCVolume" id="SCVolume"> 
	</div>
</div>
<!---end option 2--->
<!---begin option 3--->
<div class="SCRow">
	<input type="radio" name="SCType" id="SCByKeyword" value="<cfoutput>#ADD_SC_OPTION_HAVE_KEYWORD#</cfoutput>"> 
	<label for = "SCByKeyword" >Find a short code  that matches a keyword</label>
</div>
<div class="SCRow" id="SCKeywordContent" style="display: none; margin-left:30px;">
	<div class ="SCEXitRow">
		<label>Enter a keyword and we'll tel you which short code it's available on!</label><br>
	</div>
	<div class ="SCEXitRow">
		<input type="text" name="SCkeyword" id="SCkeyword" maxlength="160">
		<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading.gif" id="imgLoading" style="display:none;">
		<a id="btnLocate" class="button filterButton small" onclick="LocateSCByKeyword();" href="javascript:void(0)">Locate</a><br>
		<div id="ErrValidKeyword" style="color:red; display:none;">Your must enter a valid keyword!</div>
		<div id="NotSCAvailable" style="color:red; display:none;">There are no short codes available for that keyword. Please try another keyword.</div>
	</div>
	<div class ="SCEXitRow" id ="sltShortCode" style="height:90px; overflow-x:auto; display:none; border:1px solid #939292; padding:10px;">
	</div>
	<div id ="VolumeSC" style="display:none;">
		<label>What kind of volume are you expecting for your keyword <span id="txtKeyword"></span> using short code <span id="txtShortCode"></span>?</label>
		<input type="text" name="txtVolumeSC" id="txtVolumeSC" style="width: 30px; text-align:right;"> msgs/day <br>
		<label id="ErrVolumeMustBeNumber" style="color:red; display:none;">Volume must be number</label>
	</div>
</div>
<!---end option 3--->
<div class="SCRow" style="text-align:right">
	<!---<cfif ShowCancel EQ true>
		<a class="button filterButton small" onclick="closeDialog(); return false;" href="javasrcipt:void(0)">Cancel</a>
	</cfif>
	<a class="button filterButton small" onclick="saveSC(); return false;" href="javasrcipt:void(0)">Submit</a>--->
    
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" onclick ="saveSC(); return false;" data-dismiss="modal">Submit Request</button>
        
</div>
<div class="SCRow SCDetail" >
	All requests must be approved and may take up to 24 hours
</div>

<script type="text/javascript">
	
	$(document).ready(function(){
		$('SCNotExist').attr('checked', true);
		
		$('#SCNotExist').click(function(){
			$('#SCNotExistContent').show();
			$('#SCExistContent').hide();
			$('#SCKeywordContent').hide();
		});
		
		$('#SCExist').click(function(){
			$('#SCExistContent').show();
			$('#SCNotExistContent').hide();
			$('#SCKeywordContent').hide();
			<!---$('#numberKeyword').spinner({min:0});--->
		});
		
		$('#SCByKeyword').click(function(){			
			$('#SCExistContent').hide();
			$('#SCNotExistContent').hide();
			$('#SCKeywordContent').show();
			<!---//$('#numberKeyword').spinner({min:0});--->
		});

		$("#txtVolumeSC").on('keyup', function() {
		    var VolumeVal = $(this).val();
		    if (isNaN(VolumeVal)) {
		        $("#ErrVolumeMustBeNumber").show();
		    }
			else{
				$("#ErrVolumeMustBeNumber").hide();
			}
		});
		shotCodeListChange();
	});
	
	function shotCodeListChange(){
		var shotCodeListVal = $('#shotCodeList option:selected').attr('alt');
		if(typeof(shotCodeListVal) == 'undefined'){
			shotCodeListVal = '<cfoutput><cfif iSCMB GTE 1># SCMB[1].CLASSIFICATION_INT# </cfif></cfoutput>';
		}
		
		if(shotCodeListVal == '<cfoutput>#CLASSIFICATION_PUBLIC_NOTSHARED_VALUE#</cfoutput>' ){
			$('#SCKeyword').hide();
		}else{
			$('#SCKeyword').show();
		}
	}
	
	function LocateSCByKeyword(){
		var keyword=$("#SCkeyword").val();
		if(keyword==""){
			$("#ErrValidKeyword").show();
			$('#sltShortCode').html('');
			$('#sltShortCode').hide();
			$("#VolumeSC").hide();
		}
		else{
			$('#sltShortCode').hide();
			$("#btnLocate").hide();
			$("#imgLoading").show();
			$("#ErrValidKeyword").hide();
			$("#VolumeSC").hide();
			//call ajax search SC available
			<cfoutput>
			var data = { 
			     	'keyword':keyword
			    };	
			ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'GetShortCodeAvailableByKeyword', data, "Get Short Code fail!", function(d ) {
				var shortCodeArr = d.SHORTCODEARR;
				$('##sltShortCode').html('');				 
				if(shortCodeArr.length > 0){
					var htmlInput = '';
					for(i=0; i< shortCodeArr.length; i++){
						var shortCodeObj = shortCodeArr[i];						
						if (shortCodeObj.CLASSIFICATION_INT == '<cfoutput>#CLASSIFICATION_PUBLIC_NOTSHARED_VALUE#</cfoutput>') {
							htmlInput = htmlInput + '<input type="radio" id="shortCode_'+i+'" onclick="ShortCodeChange(this)" data-name="'+ shortCodeObj.SHORTCODE_VCH +'" name="rdoShortCode" value="' + shortCodeObj.SHORTCODEID_INT + '"><label for="shortCode_'+i+'">' + shortCodeObj.SHORTCODE_VCH +' (<cfoutput>#NOT_SHARED_DISPLAY#</cfoutput>)'+ '</label><br>';
						}
						else if (shortCodeObj.CLASSIFICATION_INT == '<cfoutput>#CLASSIFICATION_PUBLIC_SHARED_VALUE#</cfoutput>') {
							htmlInput = htmlInput + '<input type="radio" id="shortCode_'+i+'" onclick="ShortCodeChange(this)" data-name="'+ shortCodeObj.SHORTCODE_VCH +'" name="rdoShortCode" value="' + shortCodeObj.SHORTCODEID_INT + '"><label for="shortCode_'+i+'">' + shortCodeObj.SHORTCODE_VCH +' (<cfoutput>#SHARED_DISPLAY#</cfoutput>)'+ '</label><br>';
						}
					}
					$('##NotSCAvailable').hide();
					$('##sltShortCode').html(htmlInput);
					setTimeout(function(){
						$('##imgLoading').hide();
						$('##btnLocate').show();
						$('##sltShortCode').show();
					},300);
					
				}
				else{
					$('##NotSCAvailable').show();
					$('##sltShortCode').hide();
					$('##imgLoading').hide();
					$('##btnLocate').show();
				}
				
			});
		</cfoutput>
		}
	}
	
	function ShortCodeChange(shortcode){
		$("#txtKeyword").html($("#SCkeyword").val());
		$("#txtShortCode").html($(shortcode).attr("data-name"));
		$("#VolumeSC").show();
	}
	function saveSC(){
		var SCType = $("input[type='radio'][name='SCType']:checked").val();		
		var inpSCId = $("input[type='radio'][name='rdoShortCode']:checked").val();
		var keyword = $("#SCkeyword").val();
		if(SCType == '<cfoutput>#ADD_SC_OPTION_HAVE_KEYWORD#</cfoutput>'){	
			if(keyword == ""){
				bootbox.alert("Please enter keyword.");
			}
			else if (typeof inpSCId == "undefined") {
				bootbox.alert("Please select a short code.");
			}
			else{
				var data = { 
						'inpKeyword': keyword,				
						'inpSCId': inpSCId,
						'inpVolumeValue': $('#txtVolumeSC').val()
				    };	
				ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'InsertShortCodeByUserAndKeyword', data, "No Response from the remote server. Check your connection and try again.", function(d ) {
					location.reload();
				});
			}	
		}
		else{
			$.ajax({
				type: "POST", <!--- Update short code if exist else alert error --->
				url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=InsertShortCodeByUser&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				data:  { 
					inpScType: $('input[name=SCType]:checked').val(),
					inpSCText: $('#SCText').val(),
					inpSCId: $('#shotCodeList').val(),
					inpNumberKeyword: $('#numberKeyword').val(),
					inpVolumeKeyword: $('#SCVolume').val()
				},					  
				success:function(d){
					<!--- Get row 1 of results if exisits, Alert if failure--->
					if (d.ROWCOUNT > 0){						
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined"){							
							if(d.DATA.RXRESULTCODE[0] > 0){
								location.reload();
							}else{
								bootbox.alert(d.DATA.MESSAGE[0]);							
							}
						}
					}else{
						<!--- No result returned --->		
						bootbox.alert("No Response from the remote server. Check your connection and try again.");
					}
				} 			
			});
		}
	}
</script>