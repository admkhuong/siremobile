<cfparam name="keywordId" default="">
<cfparam name="inpBatchId" default="">
<cfparam name="shortCodeRequestId" default="">
<cfparam name="shortCode" default="">
<cfparam name="shortCodeId" default="">

<style>

.KeywordNotAvailable {
    font-weight: bold;
    color: black;
}

.KeywordNotAvailable {
    font-weight: bold;
    color: red;
}


</style>


<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    
    <cfif keywordId NEQ "" AND shortCodeRequestId NEQ "">
    	<h4 class="modal-title">Edit Campaign</h4>    
    <cfelse>
    	<h4 class="modal-title">Add New Keyword/Campaign</h4>        
	</cfif>
</div>

<div style="margin:20px;" id="KeyWordDetailsPopup">

	

        <div style="width: 100%; text-align: left; margin-bottom:2em;">
            Please enter the following information for short code <cfoutput>#shortCode#</cfoutput>
        </div>
	
    	<input type="hidden" name="shortCodeRequestId" id="shortCodeRequestId" value=" <cfoutput>#shortCodeRequestId#</cfoutput>" />
    	<input type="hidden" name="shortCode" value="#shortCode#" />
    
    
		<div class="" title='<cfoutput>#inpBatchId#</cfoutput>'>
			Keyword <span id='KeywordAvailabilityMessage'><span id='KeywordAvailabilityText'></span></span>
		</div>
		
        <div class="" style="margin-bottom:1em;">
			<input type="text" class="" id="txtKeyword" maxlength='160' style="width:100%;">
		</div>	
	
    	<div class="">
			Response
		</div>
        
		<div class="" style="margin-bottom:2em;">
			<textarea type="text" class="" id="txtResponse" row="5" maxlength='612' style="min-height:60px; width:100%;"></textarea> <br>
			<label class="sms_remaining_chars_keyword" id="lblRemainChars">160 of first 160 characters remaining</label>
		</div>	
	
   
	    <div class="">
			Interactive
		</div>
        
      <!---  
	  	<div id="SurveySlider" class="btn-group btn-toggle" cb-status="0"> 
            <button class="btn btn-xs btn-default btn_on">ON</button>
            <button class="btn btn-xs btn-primary btn_off">OFF</button>
        </div>
		--->
        
	   	 <input type="hidden" name="GoInteractiveFlag" id="GoInteractiveFlag" value="0" />
        
         <button type="button" class="btn btn-default" onclick ="GoInteractive();return false" style="margin-bottom:1em;">Go Interactive</button>
         
         <button type="button" class="btn btn-default" onclick ="LoadFromTemplate();return false" style="margin-bottom:1em;">Load Template</button>
         
        	
        
    <div class="sms_popup_row">
        <div class="clear" id="JSONOptions" style="padding-top:20px; padding-bottom:0px;">
            <label class="qe-label-a">Advanced - <span style="text-decoration:underline; cursor:pointer;" id="CustomHelpStopLink">Custom Help and Stop</span></label>
        </div> 
    </div>
    
    <div id="CustomHelpStopContainer" style="display:none;">
            
        <div class="sms_popup_row">
            <div class="sms_popup_row_label">
                Custom<BR />HELP
            </div>
            <div class="sms_popup_row_control_keyword">
                <textarea type="text" class="sms_text_width" id="txtCustomHELP" row="5" maxlength='612' style="min-height:60px;"></textarea> <br>
                <label class="sms_remaining_chars_keyword" id="lblRemainCharsCH">160 of first 160 characters remaining</label>
            </div>	
        </div>
        
        <div class="sms_popup_row">
            <div class="sms_popup_row_label">
                Custom<BR />STOP
            </div>
            <div class="sms_popup_row_control_keyword">
                <textarea type="text" class="sms_text_width" id="txtCustomSTOP" row="5" maxlength='612' style="min-height:60px;"></textarea> <br>
                <label class="sms_remaining_chars_keyword" id="lblRemainCharsCS">160 of first 160 characters remaining</label>
            </div>	
        </div>
    
    </div>
    
	<div class="sms_popup_action">

		<button type="button" class="btn btn-default" data-dismiss="modal" style="margin-bottom:1em;">Close</button>
        <button type="button" class="btn btn-primary" onclick ="Save();return false" style="margin-bottom:1em;">Save changes</button>

	</div>	
</div>

<script type="text/javascript">

		
	$(function() {
	
		$('.btn-toggle').click(function() {
			$(this).find('.btn').toggleClass('active');  
			
			if ($(this).find('.btn-primary').size()>0) {
				$(this).find('.btn').toggleClass('btn-primary');
			}
			
			$(this).find('.btn').toggleClass('btn-default');			
									   
		});
		 												
		LoadKeywordDetails();
			
		$('textarea[maxlength]').keyup(function(){  
	        //get the limit from maxlength attribute  
	        var limit = parseInt($(this).attr('maxlength'));  
	        //get the current text inside the textarea  
	        var text = $(this).val();  
	        //count the number of characters in the text  
	        var chars = text.length;  
	  <!--- no more limits - just warn increments charged per 153 character anthing over 160 characters to begin with  --->
	  <!---
	        //check if there are more characters then allowed  
	        if(chars > limit){  
	            //and if there are use substr to get the text before the limit  
	            var new_text = text.substr(0, limit);  
	  
	            //and change the current text with the new text  
	            $(this).val(new_text);  
	        }
	  --->
			
	    });  
	    
		$('#txtResponse').bind('cut copy paste', function (e) {
			ViewRemainChars($('#lblRemainChars'),$('#txtResponse'));
		});
		
	    $('#txtResponse').keyup(function(){  
	        ViewRemainChars($('#lblRemainChars'),$('#txtResponse'));
	    });  
		
		$('#txtCustomHELP').bind('cut copy paste', function (e) {
			ViewRemainChars($('#lblRemainCharsCH'),$('#txtCustomHELP'));
		});
		
	    $('#txtCustomHELP').keyup(function(){  
	        ViewRemainChars($('#lblRemainCharsCH'),$('#txtCustomHELP'));
	    });  
		
		$('#txtCustomSTOP').bind('cut copy paste', function (e) {
			ViewRemainChars($('#lblRemainCharsCS'),$('#txtCustomSTOP'));
		});
		
	    $('#txtCustomSTOP').keyup(function(){  
	        ViewRemainChars($('#lblRemainCharsCS'),$('#txtCustomSTOP'));
	    });  
		
		$("#CustomHelpStopLink").click(function(){
			
			$('#CustomHelpStopContainer').toggle('slow', function() {
				<!--- Animation complete. --->
			  });
  
		});		
		
		
		$('#txtKeyword').change(function(){
			checkKeywordAvailability();
		});
		
		$('#txtKeyword').keyup(function(){
			checkKeywordAvailability();
		});
		
		
	});
	
			 
	function GoInteractive() 
	{	
		<!--- Set hidden input to interactive --->
		$("#GoInteractiveFlag").val(1);	
		 Save();
		
	}
		
	function LoadFromTemplate() 
	{	
	
	
	}
			
	function Save() {
		if (Validate()) {
													
			if ('' == '<cfoutput>#keywordId#</cfoutput>') {
				var data = { 
					ShortCodeRequestId: $('#shortCodeRequestId').val(), 
					ShortCode: '<cfoutput>#ShortCode#</cfoutput>',
			     	Keyword: $('#txtKeyword').val(),
			     	Response: $('#txtResponse').val(),
					CustomHELP: $('#txtCustomHELP').val(),
					CustomSTOP: $('#txtCustomSTOP').val(),
					IsSurvey: $("#GoInteractiveFlag").val()
			    };
			    		
				ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'AddKeyword', data, "Add Keyword fail!", function(d ) {
					
					if(parseInt(d.BATCHID) > 0 && $("#GoInteractiveFlag").val() > 0)
					{	
						location.href = '<cfoutput >#rootUrl#/#SessionPath#/</cfoutput>'+'ire/builder/index?inpBatchId='+ d.BATCHID + '&INPCOMTYPE=SMS';
					}
					else
					{						
						<!--- Just reload the current datatable with the current filters and paginging information --->
						$('#tbl<cfoutput >#shortCodeId#</cfoutput>').DataTable().ajax.reload( function ( json ) {								
							<!--- Dont kill the modal until the table update is complete --->
							$('#KeywordEditModal').modal('hide');
						}, false );
	
					}
					
				});
			}
			else {
				var data = { 
					keywordId: '<cfoutput>#keywordId#</cfoutput>',
					ShortCodeRequestId: $('#shortCodeRequestId').val(), 
					ShortCode: '<cfoutput>#ShortCode#</cfoutput>',
			     	Keyword: $('#txtKeyword').val(),
			     	Response: $('#txtResponse').val(),
					CustomHELP: $('#txtCustomHELP').val(),
					CustomSTOP: $('#txtCustomSTOP').val(),
					IsSurvey:$("#GoInteractiveFlag").val()
			    };
			    		
				ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'UpdateKeyword', data, "Update Keyword fail!", function(d ) {
																
					if($("#GoInteractiveFlag").val() > 0)
					{	
						location.href = '<cfoutput >#rootUrl#/#SessionPath#/</cfoutput>'+'ire/builder/index?inpBatchId=<cfoutput>#inpBatchId#</cfoutput>&INPCOMTYPE=SMS';
					}
					else
					{							
						<!--- Just reload the current datatable with the current filters and paginging information --->
						$('#tbl<cfoutput >#shortCodeId#</cfoutput>').DataTable().ajax.reload( function ( json ) {								
							<!--- Dont kill the modal until the table update is complete --->
							$('#KeywordEditModal').modal('hide');
						}, false );					
					}
				});		
			}
		}
	}
		
	function Validate() {
		var keyword = $.trim($('#txtKeyword').val());
		var response = $.trim($('#txtResponse').val());
		if (keyword == '') {
			bootbox.alert("Keyword cannot be empty! - Failure!", function(result) { } );	
			return false;
		}
		
		if (response == '' && $("#GoInteractiveFlag").val() < 1) {
			bootbox.alert("Response cannot be empty! Failure!", function(result) { } );	
			return false;
		}
		
		var letters = /^[0-9aA-zZ]+$/;
		keyword = keyword.replace(' ', '');
		var result = letters.test(keyword);
		
		if (!result) {
			bootbox.alert("Keyword must only be alphanumeric and space! - Failure!", function(result) { } );	
			return false;
		}
		return true;
	}
	
	function LoadKeywordDetails() {
		<cfif KeywordId NEQ "">
			var data = { 
			     	KeywordId : '<cfoutput>#KeywordId#</cfoutput>'
			    };
			    		
				ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'GetKeyword', data, "Get keyword fail!", function(d ) {
					$('#txtKeyword').val(d.KEYWORD);
			     	$('#txtResponse').val(d.RESPONSE);	
					$('#shortCodeRequestId').val(d.SHORTCODEREQUESTID);					
					$("#EditSurveyLink").attr("BatchId", d.BATCHID);
					$('#txtCustomHELP').val(d.CUSTOMHELP);	
					$('#txtCustomSTOP').val(d.CUSTOMSTOP);						
					
					switch(parseInt(d.ISSURVEY))
					{
						
					}
					
				 	
			     	ViewRemainChars($('#lblRemainChars'),$('#txtResponse'));
					ViewRemainChars($('#lblRemainCharsCH'),$('#txtCustomHELP'));
					ViewRemainChars($('#lblRemainCharsCS'),$('#txtCustomSTOP'));
				});		
		</cfif>	
	}
	
	function ViewRemainChars(inpObj, inpObj2) {
		var value = (160 -  countSymbols(inpObj2.val()) <!---inpObj2.val().length--->) + '/160 characters remaining';
		inpObj.text(value)	
	}
	
	<!--- Cheap unicode counter logic --->
	function countSymbols(string) {
		var regexAstralSymbols = /[\uD800-\uDBFF][\uDC00-\uDFFF]/g;
	
		return string
		// replace every surrogate pair with a BMP symbol
		.replace(regexAstralSymbols, '_')
		// then get the length
		.length;
	}
	
	function checkKeywordAvailability(){
		if($.trim($('#txtKeyword').val()) != '')
			{				
				$("#KeywordAvailabilityMessage").show();
       			$("#KeywordAvailabilityText").removeClass();
       			$("#KeywordAvailabilityText").addClass("vanityAvailable");	
       			
       			
				$.ajax({
		            type: "POST",
		            url: "<cfoutput>#rootUrl#/#sessionPath#/</cfoutput>cfc/csc/csc.cfc?method=CheckKeywordAvailability&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
		    		data:
					{
		    			inpKeyword: $('#txtKeyword').val(),
						inpShortCode: '<cfoutput>#shortCode#</cfoutput>'
					},
		            dataType: "json", 
		            success: function(d) {
		            	
		            	$("#KeywordAvailabilityMessage").show();
		            	if(d.RXRESULTCODE > 0){
		            		
		            		if(parseInt(d.EXISTSFLAG) == 0 )
							{		            			
			            		$("#KeywordAvailabilityText").html('Available');
								$("#KeywordAvailabilityText").removeClass();
		            			$("#KeywordAvailabilityText").addClass("KeywordAvailable");
								
								$("#boxVanity").css("border", "1px solid #CCCCCC");
								$("#LeftVanity").css("border-right", "1px solid #CCCCCC");
								$("#txtKeyword").css("background-color", "");
		            			
		            		}
							else
							{
		            			$("#KeywordAvailabilityText").html('In Use');
		            			$("#KeywordAvailabilityText").removeClass();
		            			$("#KeywordAvailabilityText").addClass("KeywordNotAvailable");
								$("#boxVanity").css("border", "1px solid red");
								$("#LeftVanity").css("border-right", "1px solid red");
								$("#txtKeyword").css("background-color", "#F6E1E1");
		            		}
		         		}
			     	}
			     });		
			}
			else
			{
								
				$('#KeywordAvailabilityText').html('');
			}
					
	}
	
		
</script>
