<cfparam name="INPBATCHID" default="0">

<!---check permission--->
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkBatchPermissionByBatchId">
	<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfif NOT checkBatchPermissionByBatchId.havePermission >
	<cfoutput>#checkBatchPermissionByBatchId.message#</cfoutput>
	<cfexit>
</cfif>
        
<div id='SMSStructureMainx' class="RXForm">

	<div id="LeftMenu">

		<div width="100%" align="center" style="margin-bottom:5px;"><h1>SMS Content Editor</h1></div>

		<div width="100%" align="center" style="margin-bottom:20px;"><img class = "SMSIconWebII" src="../../public/images/SMSIconWebII.png" /></div>

		<div style="margin: 10px 5px 10px 20px;">
            <ul>         		
                <li>Press "Save" button when finished.</li>
                <li>Press "Cancel" button to exit without saving changes.</li>               
            </ul>                                              
        </div>
        
                 
            <div style="width 250px; margin:0px 0 5px 0;" class="RXForm">
             
               <span class="small300">Optional</span>            
               
               <BR />
               <BR />    
                   
                <label>Templates</label>
                <div style="margin: 2px 5px 3px 20px;">
                    <ul>
                        <li>Get a head start - choose from an array of pre built templates</li>                          
                    </ul>   
                </div>       
                <a id="SMSTemplates">Templates</a>
                
                <BR />
                
                <label>Dynamic Content </label>
                <div style="margin: 2px 5px 3px 20px;">
                    <ul>
                        <li>Advanced Users Only - Personalize the content based on data feed.</li>                          
                    </ul>   
                </div>       
                <a id="InsertDynamicContent">Insert Dynamic Content</a>
                
                <BR />
                
                <label>Description</label>       
                <div style="margin: 2px 5px 5px 20px;">
                    <ul>
                        <li>Give your <i>SMS</i> a title you can use to uniquely distiguish it.</li>                          
                    </ul>   
                </div>
            
        		<input TYPE="text" name="inpDesc" id="inpDesc" style="width:230px;" class="ui-corner-all" /> 
                
        
                <BR />
                
            <!---    <div id="eMailAdvancedContentHTML">  
                     <h2>Program Notes</h2>
                     <textarea id="inpDesc" class="editableAreaeMailProgramNotes ui-IPE-Display"></textarea>
        	   </div>--->
                
                  
				<!---         <!-- Some integration calls -->
        
                        <a href="javascript:;" onclick="tinyMCE.execCommand('mceInsertContent', false, 'testing'); return false;">[Insert HTML]</a>
                        <a href="javascript:;" onclick="tinyMCE.execCommand('mceInsertContent', false, '[$FirstName]'); return false;">[Insert &lt;$FirstName&gt;]</a>
                        <a href="javascript:;" onclick="tinyMCE.execCommand('mceInsertContent', false, '<a>Crap</a>'); return false;">[Insert Crap Link]</a>
          
                 
                         tinyMCE.execCommand('mceInsertContent', false, $(this).attr('rel'));	
                 
                            <a href="javascript:;" onclick="$('#TAcontent_EmailEditor').tinymce().show();return false;">[Show]</a>
                            <a href="javascript:;" onclick="$('#TAcontent_EmailEditor').tinymce().hide();return false;">[Hide]</a>
                            <a href="javascript:;" onclick="$('#TAcontent_EmailEditor').tinymce().execCommand('Bold');return false;">[Bold]</a>
                            <a href="javascript:;" onclick="alert(tinyMCE.activeEditor.getContent()); return false;">[Get contents]</a>
                            <a href="javascript:;" onclick="alert($('#TAcontent_EmailEditor').tinymce().selection.getContent());return false;">[Get selected HTML]</a>
                            <a href="javascript:;" onclick="alert($('#TAcontent_EmailEditor').tinymce().selection.getContent({format : 'text'}));return false;">[Get selected text]</a>
                            <a href="javascript:;" onclick="alert($('#TAcontent_EmailEditor').tinymce().selection.getNode().nodeName);return false;">[Get selected element]</a>
                            <a href="javascript:;" onclick="$('#TAcontent_EmailEditor').tinymce().execCommand('mceInsertContent',false,'<b>Hello world!!</b>');return false;">[Insert HTML]</a>
                            <a href="javascript:;" onclick="$('#TAcontent_EmailEditor').tinymce().execCommand('mceReplaceContent',false,'<b>{$selection}</b>');return false;">[Replace selection]</a>
                    
                            <a href="javascript:;" onclick="tinyMCE.execCommand('mceAddControl', false, 'TAcontent_EmailEditor');">[Test]</a>
                             
                            <a href="javascript:;" onclick="ReadDM_MT3XMLString();">[Test Read]</a>
                            <a href="javascript:;" onclick="GetDM_MT3XMLString();">[Test Write]</a>
                            
                            <a href="javascript:;" onclick="GetDM_MT3XMLString();">[Test e-Mail]</a>
                --->
             
            </div>	    
            
        
	</div>
    
    <div id="RightStage">
                        
            <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>">
            <input TYPE="hidden" name="INPQID" id="INPQID" value="1">
            <input TYPE="hidden" name="inprxt" id="inprxt" value="20">
            <input TYPE="hidden" name="inpBS" id="inpBS" value="0">
            <input TYPE="hidden" name="inpXML" id="inpXML" value="">
            <input TYPE="hidden" name="inpX" id="inpX" value="0">
            <input TYPE="hidden" name="inpY" id="inpY" value="0">
            <input TYPE="hidden" name="inpLINKTo" id="inpLINKTo" value="0">            
            <input TYPE="hidden" name="inpCK8" id="inpCK8" value="">
            <input TYPE="hidden" name="inpCK9" id="inpCK9" value="">
            <input TYPE="hidden" name="inpCK10" id="inpCK10" value="">
            <input TYPE="hidden" name="inpCK11" id="inpCK11" value="">
            <input TYPE="hidden" name="inpCK12" id="inpCK12" value="">
            <input TYPE="hidden" name="inpCK13" id="inpCK13" value="">
            <input TYPE="hidden" name="inpCK14" id="inpCK14" value="">
            <input TYPE="hidden" name="inpCK15" id="inpCK15" value="">
            
            
            <button id="SaveSMSStructure" class="SaveSMSStructure">Save<BR><img src="../../public/images/SendIcon40x32Web.png"/></button>
            <button id="CancelSMSStructure" class="CancelSMSStructure">Cancel</button>
        
            <button class="FromSMSStructure">Short Code ...</button> <input type="text" name="inpCK7" id="inpCK7" class="inpCK7SMS" >
                		                         
            <div id="SMSContentHTML">
                                          
                <div id="bhtabs_SMSStructure" class="tabs-bottom ui-corner-top">
                
                    <ul style="height:20px; border:0;">                            
                        <li><a href="#MCM_SMStabs-0">Main Message Mobile Terminated (MT)</a></li>
                        <li><a href="#MCM_SMStabs-1">HELP (MO)</a></li>
                        <li><a href="#MCM_SMStabs-2">STOP (MO)</a></li>  
                        <li><a href="#MCM_SMStabs-3">JOIN (MO)</a></li>  
                        <li><a href="#MCM_SMStabs-4">KEYWORD (MO)</a></li>                                
                    </ul>
                      
                    <div id="MCM_SMStabs-0" class="SMSStructure_Tab">
                        <h2>Mobile Terminated (MT) Message</h2>
                        <textarea id="inpCK1" name="inpCK1" class="editableAreaSMSStructure ui-IPE-Display"></textarea>
                        <span style="float:right; margin-right:12px;">Characters Left <input readonly type="text" id="remLen1" name="remLen1" size="3" maxlength="3" value="155"></span>
                    </div>
                    <div id="MCM_SMStabs-1" class="SMSStructure_Tab">
                        <h2>Mobile Originated (MO) HELP Response Message</h2>
                        <textarea id="inpCK2" name="inpCK2" class="editableAreaSMSStructure ui-IPE-Display"></textarea>
                        <span style="float:right; margin-right:12px;">Characters Left <input readonly type="text" id="remLen2" name="remLen2" size="3" maxlength="3" value="155"></span>
                    </div>
                    <div id="MCM_SMStabs-2" class="SMSStructure_Tab">
                        <h2>Mobile Originated (MO) STOP Response Message</h2>
                        <textarea id="inpCK3" name="inpCK3" class="editableAreaSMSStructure ui-IPE-Display"></textarea>
                        <span style="float:right; margin-right:12px;">Characters Left <input readonly type="text" id="remLen3" name="remLen3" size="3" maxlength="3" value="155"></span>
                    </div>
                    <div id="MCM_SMStabs-3" class="SMSStructure_Tab">
                        <h2>Mobile Originated (MO) JOIN Response Message</h2>
                        <textarea id="inpCK4" name="inpCK4" class="editableAreaSMSStructure ui-IPE-Display"></textarea>
                        <span style="float:right; margin-right:12px;">Characters Left <input readonly type="text" id="remLen4" name="remLen4" size="3" maxlength="3" value="155"></span>
                    </div>                   
                    <div id="MCM_SMStabs-4" class="SMSStructure_Tab">
                        <h2>Mobile Originated (MO) generic Keyword Response Message</h2>
                        <textarea id="inpCK5" name="inpCK5" class="editableAreaSMSStructure ui-IPE-Display"></textarea>
                        <input TYPE="text" name="inpCK6" id="inpCK6" value="">
                        <span style="float:right; margin-right:12px;">Characters Left <input readonly type="text" id="remLen5" name="remLen5" size="3" maxlength="3" value="155"></span>
                    </div>
                    
                  
                </div>
			
            </div>
                            
    
    </div>

</div>

<script type="text/javascript"> 

$(function() 
{
	
	var LastSavedDataResultsSMS = 0;
	
	<cfinclude template="..\account\HorizontalTabs_Bottom\dsp_SMSStructureSetup.cfm">	
	
	 ReadDM_MT4XMLString();
		 
	 $("#SMSStructureMainx #SaveSMSStructure").click(  function() { GetDM_MT4XMLString(); });
	
	
	$("#SMSStructureMainx #CancelSMSStructure").click( function() { 		
																		CompareDM_MT4XMLString();
																		
																	});
								
	$("#SMSStructureMainx #inpCK1").change(function() 
    { 
		textCounter($("#SMSStructureMainx #inpCK1"),$("#SMSStructureMainx #remLen1"),155)
	});
	
	$("#SMSStructureMainx #inpCK1").keyup(function(){  
		textCounter($("#SMSStructureMainx #inpCK1"),$("#SMSStructureMainx #remLen1"),155)
    });
	
									
	$("#SMSStructureMainx #inpCK2").change(function() 
    { 
		textCounter($("#SMSStructureMainx #inpCK2"),$("#SMSStructureMainx #remLen2"),155)
	});
	
	$("#SMSStructureMainx #inpCK2").keyup(function(){  
		textCounter($("#SMSStructureMainx #inpCK2"),$("#SMSStructureMainx #remLen2"),155)
    });

								
	$("#SMSStructureMainx #inpCK3").change(function() 
    { 
		textCounter($("#SMSStructureMainx #inpCK3"),$("#SMSStructureMainx #remLen3"),155)
	});
	
	$("#SMSStructureMainx #inpCK3").keyup(function(){  
		textCounter($("#SMSStructureMainx #inpCK3"),$("#SMSStructureMainx #remLen3"),155)
    });
	
	$("#SMSStructureMainx #inpCK4").change(function() 
    { 
		textCounter($("#SMSStructureMainx #inpCK4"),$("#SMSStructureMainx #remLen4"),155)
	});
	
	$("#SMSStructureMainx #inpCK4").keyup(function(){  
		textCounter($("#SMSStructureMainx #inpCK4"),$("#SMSStructureMainx #remLen4"),155)
    });
	
		$("#SMSStructureMainx #inpCK5").change(function() 
    { 
		textCounter($("#SMSStructureMainx #inpCK5"),$("#SMSStructureMainx #remLen5"),155)
	});
	
	$("#SMSStructureMainx #inpCK5").keyup(function(){  
		textCounter($("#SMSStructureMainx #inpCK5"),$("#SMSStructureMainx #remLen5"),155)
    });
	
	
																									
//	ReadDM_MT4XMLString();


});
   
<!--- Output an XML CCD String based on form values --->
	function ReadDM_MT4XMLString(inpCancel)
	{				
		<!--- AJAX/JSON Do CFTE Demo --->	
//		$('#TAcontent_SMSEditor').html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');
//		tinyMCE.activeEditor.setContent('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');				
//		tinyMCE.get('TAcontent_SMSEditor').setContent('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">'); 

//		<!--- Let the user know somthing is processing --->
//		$("#CurrentCCDXMLString").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');	
		
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=ReadDM_MT4XML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			data: { INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>}, 			  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:	
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
														
						LastSavedDataResultsSMS = d;
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{																									
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{
									<!---
										
										
									--->
									
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.INPQID[0]) != "undefined")
										{									
											$("#SMSStructureMainx #INPQID").val(d.DATA.INPQID[0]);																							
										}
										else
										{
											$("#SMSStructureMainx #INPQID").val('1');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRRXT[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inprxt").val(d.DATA.CURRRXT[0]);																							
										}
										else
										{
											$("#SMSStructureMainx #inprxt").val('13');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRDESC[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpDesc").val(RXdecodeXML(d.DATA.CURRDESC[0]));																							
										}
										else
										{
											$("#SMSStructureMainx #inpDesc").val('empty');		
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRBS[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpBS").val(d.DATA.CURRBS[0]);																							
										}
										else
										{
											$("#SMSStructureMainx #inpBS").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRUID[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpUID").val(d.DATA.CURRUID[0]);																							
										}
										else
										{
											$("#SMSStructureMainx #inpUID").val('0');
										}
																				
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK1[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpCK1").val(d.DATA.CURRCK1[0]);	
											$("#SMSStructureMainx #inpCK1").trigger('change');
																																	
										}
										else
										{
											$("#SMSStructureMainx #inpCK1").val('');
											$("#SMSStructureMainx #inpCK1").trigger('change');		
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK2[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpCK2").val(d.DATA.CURRCK2[0]);																							
											$("#SMSStructureMainx #inpCK2").trigger('change');
										}
										else
										{
											$("#SMSStructureMainx #inpCK2").val('');
											$("#SMSStructureMainx #inpCK2").trigger('change');
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK3[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpCK3").val(d.DATA.CURRCK3[0]);																							
											$("#SMSStructureMainx #inpCK3").trigger('change');
										}
										else
										{
											$("#SMSStructureMainx #inpCK3").val('');		
											$("#SMSStructureMainx #inpCK3").trigger('change');
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK4[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpCK4").val(d.DATA.CURRCK4[0]);			
										}
										else
										{
											$("#SMSStructureMainx #inpCK4").val('');			
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK5[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpCK5").val(d.DATA.CURRCK5[0]);																							
										}
										else
										{
											$("#SMSStructureMainx #inpCK5").val('-1');	
										}
																															
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK6[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpCK6").val(d.DATA.CURRCK6[0]);																							
										}
										else
										{
											$("#SMSStructureMainx #inpCK6").val('');	
										}
									  
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK7[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpCK7").val(d.DATA.CURRCK7[0]);																							
										}
										else
										{
											$("#SMSStructureMainx #inpCK7").val('');			
										}
										  
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK8[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpCK8").val(d.DATA.CURRCK8[0]);																							
										}
										else
										{
											$("#SMSStructureMainx #inpCK8").val(' ');	
										}
										
										 <!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK9[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpCK9").val(d.DATA.CURRCK9[0]);																							
										}
										else
										{
											$("#SMSStructureMainx #inpCK9").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK10[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpCK10").val(d.DATA.CURRCK10[0]);																							
										}
										else
										{
											$("#SMSStructureMainx #inpCK10").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK11[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpCK11").val(d.DATA.CURRCK11[0]);																							
										}
										else
										{
											$("#SMSStructureMainx #inpCK11").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK12[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpCK12").val(d.DATA.CURRCK12[0]);																							
										}
										else
										{
											$("#SMSStructureMainx #inpCK12").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK13[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpCK13").val(d.DATA.CURRCK13[0]);																							
										}
										else
										{
											$("#SMSStructureMainx #inpCK13").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK14[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpCK14").val(d.DATA.CURRCK14[0]);																							
										}
										else
										{
											$("#SMSStructureMainx #inpCK14").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK15[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpCK15").val(d.DATA.CURRCK15[0]);																							
										}
										else
										{
											$("#SMSStructureMainx #inpCK15").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRX[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpX").val(d.DATA.CURRX[0]);																							
										}
										else
										{
											$("#SMSStructureMainx #inpX").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRY[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpY").val(d.DATA.CURRY[0]);																							
										}
										else
										{
											$("#SMSStructureMainx #inpY").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRLINK[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpLINK").val(d.DATA.CURRLINK[0]);																							
										}
										else
										{
											$("#SMSStructureMainx #inpLINK").val('-1');	
										}
		
		
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRRQ[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpRQ").val(d.DATA.CURRRQ[0]);																							
										}
										else
										{
											$("#SMSStructureMainx #inpRQ").val('');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCP[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpCP").val(d.DATA.CURRCP[0]);																							
										}
										else
										{
											$("#SMSStructureMainx #inpCP").val('');	
										}
															   
								}
								else
								{
									$("#CurrentCCDXMLString").html("Returned" + d.DATA.RXRESULTCODE[0]);											
								}
							}
							else
							{<!--- Invalid structure returned --->	
								$("#CurrentCCDXMLString").html("Write Error - Invalid structure");										
							}
						}
						else
						{<!--- No result returned --->
							$("#CurrentCCDXMLString").html("Write Error - No result returned");									
						}
						if(inpCancel){
							CreateNewSMSDetailsDialog.remove();		
						}
				}
		});	

	}
	
	<!--- Output an XML CCD String based on form values --->
	function WriteDM_MT4XMLString()
	{				
		<!--- AJAX/JSON  --->	
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=WriteDM_MT4XML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			data:  { INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, inpXML : $("#SMSStructureMainx #inpXML").val(), inpPassBackdisplayxml : 1}, 		  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:	
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d) 
					{
						<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CurrRXResultCode > 0)
									{
										ReadDM_MT4XMLString(0);
										
										<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLString[0]  --->								
										if(typeof(d.DATA.DISPLAYXML_VCH[0]) != "undefined")
										{									
											$("#SMSStructureMainx #CurrentCCDXMLString").html(d.DATA.DISPLAYXML_VCH[0]);																					
										}				
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#SMSStructureMainx #CurrentCCDXMLString").html("Write Error - Invalid structure");										
								}
							}
							else
							{<!--- No result returned --->
								$("#SMSStructureMainx #CurrentCCDXMLString").html("Write Error - No result returned");									
							}
							CreateNewSMSDetailsDialog.remove();					
					} 
					
		});	
					
		
	}   
	
	function GetDM_MT4XMLString()	
	{
	
		<!--- AJAX/JSON  --->	
		
		<!--- Let the user know somthing is processing --->
		$("#CurrentCCDXMLString").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');	
	
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=gen_DM_MT4&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { INPQID : $("#SMSStructureMainx #INPQID").val(), inpBS : $("#SMSStructureMainx #inpBS").val(), inpCK1 : $("#SMSStructureMainx #inpCK1").val(), inpCK2 : $("#SMSStructureMainx #inpCK2").val(), inpCK3 : $("#SMSStructureMainx #inpCK3").val(), inpCK4 : $("#SMSStructureMainx #inpCK4").val(), inpCK5 : $("#SMSStructureMainx #inpCK5").val(), inpCK6 : $("#SMSStructureMainx #inpCK6").val(), inpCK7 : $("#SMSStructureMainx #inpCK7").val(), inpCK8 : $("#SMSStructureMainx #inpCK8").val(), inpCK9 : $("#SMSStructureMainx #inpCK9").val(), inpCK10 : $("#SMSStructureMainx #inpCK10").val(), inpCK11 : $("#SMSStructureMainx #inpCK11").val(), inpCK12 : $("#SMSStructureMainx #inpCK12").val(), inpCK13 : $("#SMSStructureMainx #inpCK13").val(), inpCK14 : $("#SMSStructureMainx #inpCK14").val(), inpCK15 : $("#SMSStructureMainx #inpCK15").val(), inpDesc : $("#SMSStructureMainx #inpDesc").val(), inpX : $("#SMSStructureMainx #inpX").val(), inpY : $("#SMSStructureMainx #inpY").val(), inpLINKTo : $("#SMSStructureMainx #inpLINKTo").val(), inpRQ : $("#SMSStructureMainx #inpRQ").val(), inpCP : $("#SMSStructureMainx #inpCP").val()}, 					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:		
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d) 
					{
						<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CURRRXResultCode > 0)
									{
										<!--- Check if variable is part of JSON result string   d.DATA.RXTXMLSTRING[0]  --->								
										if(typeof(d.DATA.RXTXMLSTRING[0]) != "undefined")
										{									
                                        
                                        	<!--- Display Value --->
											$("#SMSStructureMainx #CURRentRXTXMLSTRING").html(d.DATA.RXTXMLSTRING[0]);	
										}		
                                        
                                        <!--- Check if variable is part of JSON result string   d.DATA.inpXML[0]  --->								
										if(typeof(d.DATA.RAWXML[0]) != "undefined")
										{						
                                        	<!--- Update hidden Value --->
											$("#SMSStructureMainx #inpXML").val(d.DATA.RAWXML[0]);	
									                                                  
                                            <!--- Save changes to DB --->
                                            WriteDM_MT4XMLString();
										}	
									}else{
										alert(d.DATA.MESSAGE[0]);
										return false;
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#SMSStructureMainx #CURRentRXTXMLSTRING").html("Write Error - Invalid structure");										
								}
							}
							else
							{<!--- No result returned --->
								$("#SMSStructureMainx #CURRentRXTXMLSTRING").html("Write Error - No result returned");									
							}
					} 								
		});
	
	}
	
	function CompareDM_MT4XMLString()
	{				
		<!--- AJAX/JSON Do CFTE Demo --->	
		<!--- Let the user know somthing is processing --->
				
						<!--- Save trips to server--->
						var d = LastSavedDataResultsSMS;
						var RetVal = 1;
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{																									
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{
									<!---
										
										
									--->
																											
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.INPQID[0]) != "undefined")
										{									
											if($("#SMSStructureMainx #INPQID").val() != d.DATA.INPQID[0])
												if(RetVal>0) RetVal = -1;																							
										}
										else
										{
											if($("#SMSStructureMainx #INPQID").val() != '1')
												if(RetVal>0) RetVal = -1;	
										}
									
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRRXT[0]) != "undefined")
										{									
											if($("#SMSStructureMainx #inprxt").val() != d.DATA.CURRRXT[0]) 
												if(RetVal>0) RetVal = -2;																							
										}	
										else
										{
											if($("#SMSStructureMainx #inprxt").val() != '13')
												if(RetVal>0) RetVal = -2;											
										}							
									
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRDESC[0]) != "undefined")
										{									
											if($("#SMSStructureMainx #inpDesc").val() != d.DATA.CURRDESC[0])	
												if(RetVal>0) RetVal = -3;																						
										}
										else
										{
											if($("#SMSStructureMainx #inpDesc").val() != 'empty')		
												if(RetVal>0) RetVal = -3;
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRBS[0]) != "undefined")
										{									
											if($("#SMSStructureMainx #inpBS").val() != d.DATA.CURRBS[0])																							
												if(RetVal>0) RetVal = -4;
										}
										else
										{												
											if($("#SMSStructureMainx #inpBS").val() != '0')	
												if(RetVal>0) RetVal = -4;
										}							
									
								<!---			
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRUID[0]) != "undefined")
										{					
														
											if($("#SMSStructureMainx #inpUID").val() != d.DATA.CURRUID[0])	
												if(RetVal>0) RetVal = -5;																						
										}
										else
										{
											if($("#SMSStructureMainx #inpUID").val() != '0')
												if(RetVal>0) RetVal = -5;
										}--->
																					
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK1[0]) != "undefined")
										{									
											if($("#SMSStructureMainx #inpCK1").val() != d.DATA.CURRCK1[0])																							
												if(RetVal>0) RetVal = -6;
										}
										else
										{
											if($("#SMSStructureMainx #inpCK1").val() != '')	
												if(RetVal>0) RetVal = -6;
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK2[0]) != "undefined")
										{									
											if($("#SMSStructureMainx #inpCK2").val() != d.DATA.CURRCK2[0])																							
												if(RetVal>0) RetVal = -7;
										}
										else
										{
											if($("#SMSStructureMainx #inpCK2").val() != '')
												if(RetVal>0) RetVal = -7;
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK3[0]) != "undefined")
										{									
											if($("#SMSStructureMainx #inpCK3").val() != d.DATA.CURRCK3[0])																							
												if(RetVal>0) RetVal = -8;
										}
										else
										{
											if($("#SMSStructureMainx #inpCK3").val() != '')		
												if(RetVal>0) RetVal = -8;
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK4[0]) != "undefined")
										{									
											if($("#SMSStructureMainx #inpCK4").val() != d.DATA.CURRCK4[0])																							
												if(RetVal>0) RetVal = -9;
										}
										else
										{
											if($("#SMSStructureMainx #inpCK4").val() != '')		
												if(RetVal>0) RetVal = -9;
										}
																												
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK5[0]) != "undefined")
										{									
											if($("#SMSStructureMainx #inpCK5").val() != d.DATA.CURRCK5[0])																							
												if(RetVal>0) RetVal = -10;
										}
										else
										{
											if($("#SMSStructureMainx #inpCK5").val() != 'noreply@MBCamapaign.com')	
												if(RetVal>0) RetVal = -10;
										}
																				
																															
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK6[0]) != "undefined")
										{									
											if($("#SMSStructureMainx #inpCK6").val() != d.DATA.CURRCK6[0])																							
												if(RetVal>0) RetVal = -11;
										}
										else
										{
											if($("#SMSStructureMainx #inpCK6").val() != '')	
												if(RetVal>0) RetVal = -11;
										}
									  
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK7[0]) != "undefined")
										{									
											if($("#SMSStructureMainx #inpCK7").val() != d.DATA.CURRCK7[0])																							
												if(RetVal>0) RetVal = -12;
										}
										else
										{
											if($("#SMSStructureMainx #inpCK7").val() != '')				
												if(RetVal>0) RetVal = -12;
										}
										  
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK8[0]) != "undefined")
										{									
											if($("#SMSStructureMainx #inpCK8").val() != d.DATA.CURRCK8[0])																							
												if(RetVal>0) RetVal = -13;
										}
										else
										{
											if($("#SMSStructureMainx #inpCK8").val() != '')	
												if(RetVal>0) RetVal = -13;
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK9[0]) != "undefined")
										{									
											if($("#SMSStructureMainx #inpCK9").val() != d.DATA.CURRCK9[0])																							
												if(RetVal>0) RetVal = -14;
										}
										else
										{
											if($("#SMSStructureMainx #inpCK9").val() != '0')		
												if(RetVal>0) RetVal = -14;
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK10[0]) != "undefined")
										{									
											if($("#SMSStructureMainx #inpCK10").val() != d.DATA.CURRCK10[0])																							
												if(RetVal>0) RetVal = -15;
										}
										else
										{
											if($("#SMSStructureMainx #inpCK10").val() != '0')	
												if(RetVal>0) RetVal = -15;
										}
										
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK11[0]) != "undefined")
										{									
											if($("#SMSStructureMainx #inpCK11").val() != d.DATA.CURRCK11[0])																							
												if(RetVal>0) RetVal = -16;
										}
										else
										{
											if($("#SMSStructureMainx #inpCK11").val() != '0')	
												if(RetVal>0) RetVal = -16;
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK12[0]) != "undefined")
										{									
											if($("#SMSStructureMainx #inpCK12").val() != d.DATA.CURRCK12[0])																							
												if(RetVal>0) RetVal = -17;
										}
										else
										{
											if($("#SMSStructureMainx #inpCK12").val() != '')	
												if(RetVal>0) RetVal = -17;
										}
																				
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK13[0]) != "undefined")
										{									
											if($("#SMSStructureMainx #inpCK13").val() != d.DATA.CURRCK13[0])																						
												if(RetVal>0) RetVal = -18;
										}
										else
										{
											if($("#SMSStructureMainx #inpCK13").val() != '0')	
												if(RetVal>0) RetVal = -18;
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK14[0]) != "undefined")
										{									
											if($("#SMSStructureMainx #inpCK14").val() != d.DATA.CURRCK14[0])																							
												if(RetVal>0) RetVal = -19;
										}
										else
										{
											if($("#SMSStructureMainx #inpCK14").val() != '0')		
												if(RetVal>0) RetVal = -19;
										}
																				
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCK15[0]) != "undefined")
										{									
											if($("#SMSStructureMainx #inpCK15").val() != d.DATA.CURRCK15[0])																							
												if(RetVal>0) RetVal = -20;
										}
										else
										{
											if($("#SMSStructureMainx #inpCK15").val() != '0')	
												if(RetVal>0) RetVal = -20;
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRX[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpX").val(d.DATA.CURRX[0]);																							
										}
										else
										{
											$("#SMSStructureMainx #inpX").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRY[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpY").val(d.DATA.CURRY[0]);																							
										}
										else
										{
											$("#SMSStructureMainx #inpY").val('0');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRLINK[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpLINK").val(d.DATA.CURRLINK[0]);																							
										}
										else
										{
											$("#SMSStructureMainx #inpLINK").val('-1');	
										}
													
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRRQ[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpRQ").val(d.DATA.CURRRQ[0]);																							
										}
										else
										{
											$("#SMSStructureMainx #inpRQ").val('');	
										}
										
										<!--- Check if variable is part of JSON result string     --->								
										if(typeof(d.DATA.CURRCP[0]) != "undefined")
										{									
											$("#SMSStructureMainx #inpCP").val(d.DATA.CURRCP[0]);																							
										}
										else
										{
											$("#SMSStructureMainx #inpCP").val('');	
										}
										
										
															   
								}
								else
								{
									$("#CurrentCCDXMLString").html("Returned" + d.DATA.RXRESULTCODE[0]);											
								}
							}
							else
							{<!--- Invalid structure returned --->	
								$("#CurrentCCDXMLString").html("Write Error - Invalid structure");										
							}
						}
						else
						{<!--- No result returned --->
							$("#CurrentCCDXMLString").html("Write Error - No result returned");									
						}	
						
						if(RetVal < 0)
						{				
							$.alerts.okButton = '&nbsp;Confirm Delete!&nbsp;';
							$.alerts.cancelButton = '&nbsp;No&nbsp;';		
																																	
							jConfirm( "Are you sure you want to revert/lose all changes to this SMS content since last saved??", "About to undo/delete recent changes.", function(result) { 
								if(result)
								{	
									ReadDM_MT4XMLString(1); 
								}
								return false;																	
							});
							
						}
						else
						{
							CreateNewSMSDetailsDialog.remove();
						}																											
			

	}
	

		
	<!--- Global so popup can refernece it to close it--->
	var CreateFieldChooserDialogSMSTools = 0;
	
	function FieldChooserDialogSMSTools()
	{				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
							
		var ParamStr = '';
	
		ParamStr = '?inpbatchid=' + encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>);
		
		<!--- Erase any existing dialog data --->
		if(CreateFieldChooserDialogSMSTools != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			CreateFieldChooserDialogSMSTools.remove();
			CreateFieldChooserDialogSMSTools = 0;
		}
						
		CreateFieldChooserDialogSMSTools = $('<div></div>').append($loading.clone());
		
		CreateFieldChooserDialogSMSTools
			.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/sms/dsp_FieldChooser' + ParamStr)
			.dialog({
				modal : true,
				title: 'Field Chooser - Dynamic Content SMS Tool',
				width: 600,
				minHeight: 200,
				close : function() { }, 
				height: 'auto',
				position: 'top' 
			});
	
		CreateFieldChooserDialogSMSTools.dialog('open');
	
		return false;		
	}


	$("#SMSStructureMainx #AdvancedOptionsToggle").click( function() 
	
		{
			if($("#SMSStructureMainx #AdvancedOptionsToggle").data("CurrState") )
			{			
				$("#AdvancedSMSOptions").hide();
				$("#SMSStructureMainx #AdvancedOptionsToggle").html("Show Advanced SMS Options");
				$("#SMSStructureMainx #AdvancedOptionsToggle").data("CurrState", false);
			}
			else
			{
				$("#AdvancedSMSOptions").show();	
				$("#SMSStructureMainx #AdvancedOptionsToggle").html("Hide Advanced SMS Options");
				$("#SMSStructureMainx #AdvancedOptionsToggle").data("CurrState", true);
			}			
		}
	
	); 
	
	<cfset Session.AdvancedSMSOptions = 0> 
	<cfif Session.AdvancedSMSOptions EQ 0>
		$("#AdvancedSMSOptions").hide();
		$("#SMSStructureMainx #AdvancedOptionsToggle").html("Show Advanced SMS Options");
		$("#SMSStructureMainx #AdvancedOptionsToggle").data("CurrState", false);	
	<cfelse>
		$("#AdvancedSMSOptions").show();	
		$("#SMSStructureMainx #AdvancedOptionsToggle").html("Hide Advanced SMS  Options");
		$("#SMSStructureMainx #AdvancedOptionsToggle").data("CurrState", true);
	</cfif>
	
	$("#SMSStructureMainx #AdvancedOptionsToggle").data("CurrState", false);
	
	
	$("#SMSStructureMainx #InsertDynamicContent").click( function() 
	{
		FieldChooserDialogSMSTools();
	
		// .execCommand('mceInsertContent',false,'<b>Hello world!!</b>');
	}); 
	
	$("#SMSStructureMainx #SMSTemplates").click( function() 
	{
		TemplatePickerDialogSMSTools();
	
		// .execCommand('mceInsertContent',false,'<b>Hello world!!</b>');
	}); 
	  	
	<!--- Global so popup can refernece it to close it--->
	var CreateTemplatePickerDialogSMSTools = 0;
	
	function TemplatePickerDialogSMSTools()
	{				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
							
		var ParamStr = '';
	
		ParamStr = '?inpbatchid=' + encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>);
		
		<!--- Erase any existing dialog data --->
		if(CreateTemplatePickerDialogSMSTools != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			CreateTemplatePickerDialogSMSTools.remove();
			CreateTemplatePickerDialogSMSTools = 0;
			
		}
						
		CreateTemplatePickerDialogSMSTools = $('<div></div>').append($loading.clone());
		
		CreateTemplatePickerDialogSMSTools
			.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/sms/dsp_TemplatePickerSMS' + ParamStr)
			.dialog({
				modal : true,
				title: 'SMS Template Tool',
				width: 920,
				minHeight: 200,
				height: 'auto',
				position: 'top',
				close : function() {$(this).dialog('destroy'); $(this).remove();  }, 
				beforeClose: function(event, ui) {
					$("#MC_PreviewPanel_SMS_Templates").attr("class", "MC_PreviewPanel_SMS");
					$("#mbtemplatessms").attr("class", "MC_SMS_Templates");
				}
			});
	
		CreateTemplatePickerDialogSMSTools.dialog('open');
	
		return false;		
	}
	  
	<!--- Global so popup can refernece it to close it--->
	var CreateFieldChooserDialogSMSTools = 0;
	
	function FieldChooserDialogSMSTools()
	{				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
							
		var ParamStr = '';
	
		ParamStr = '?inpbatchid=' + encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>);
		
		<!--- Erase any existing dialog data --->
		if(CreateFieldChooserDialogSMSTools != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			CreateFieldChooserDialogSMSTools.remove();
			CreateFieldChooserDialogSMSTools = 0;
		}
						
		CreateFieldChooserDialogSMSTools = $('<div></div>').append($loading.clone());
		
		CreateFieldChooserDialogSMSTools
			.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/sms/dsp_FieldChooser' + ParamStr)
			.dialog({
				modal : true,
				title: 'Field Chooser - Dynamic Content SMS Tool',
				width: 600,
				minHeight: 200,
				close : function() {$(this).dialog('destroy'); $(this).remove();}, 
				height: 'auto',
				position: 'top' 
			});
	
		CreateFieldChooserDialogSMSTools.dialog('open');
	
		return false;		
	}
		
		
		   
</script> 

<style>
	<cfoutput>
		@import url('#rootUrl#/#PublicPath#/css/mcid/sms.css');
	</cfoutput>	
</style>