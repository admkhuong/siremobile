<cfinclude template="../MCID/cx_mcid_tree.cfm">
<div id="EBMSMSDynamicContent_PickerDiv"></div>

<script type="text/javascript">
var SERVICE_DATA = {
	title: "",
	items: [
	/* Common Templates */
		{
			title: "Common Templates",
			items: [
				{
					title: "Account Activity",
					rel: "Hello. This is a Security Alert from CustomText1]. A new computer has been verified for access to your online accounts as of {date/time triggered}.",
					rel2: "If you did not recently verify a new computer online, please call 1-877-MYBANK-PC.",
					rel3: "You will no longer receive alerts of this type. To manage alert setting please log in to your account."
				},
				{
					title: "Digital Coupon",
					rel: "Hello, Show this message to our staff and receive a free appetizer with your next meal purchase.",
					rel2: "You are receiving this message as part of the digital coupon program.",
					rel3: "You have been removed from the digital coupon program. To re-join simply text JOIN to 55555555"
				},
				{
					title: "Change of Terms",
					rel: "Change of Terms",
					rel2: "HELP",
					rel3: "STOP"
				},
				{
					title: "Expiration Notice",
					rel: "Expiration Notice",
					rel2: "HELP",
					rel3: "STOP"
				},
				{
					title: "Renewal Notice",
					rel: "Renewal Notice",
					rel2: "HELP",
					rel3: "STOP"
				},
				{
					title: "Reminder",
					rel: "Reminder",
					rel2: "HELP",
					rel3: "STOP"
				}
			]
		},
	/* Custom Presets */
		{
			title: "Custom Presets",
			items: [
				{
					title: "Custom Preset 1",
					rel: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Custom Preset 2",
					rel: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Custom Preset 3",
					rel: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Custom Preset 4",
					rel: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Custom Preset 5",
					rel: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Custom Preset 6",
					rel: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Custom Preset 7",
					rel: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Custom Preset 8",
					rel: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Custom Preset 9",
					rel: "",
					rel2: "",
					rel3: ""
				},
				{
					title: "Custom Preset 10",
					rel: "",
					rel2: "",
					rel3: ""
				}
			]
		}
	]
};
</script>


<script TYPE="text/javascript">
/* Process tool */
function eventHandle(item) {
	$.alerts.okButton = '&nbsp;Confirm Template!&nbsp;';
	$.alerts.cancelButton = '&nbsp;No&nbsp;';	

	var LocalSel1= item.rel;
	var LocalSel2 = item.rel2;
	var LocalSel3 = item.rel3;


	<!---jConfirm( "Are you sure you want to replace your existing content with this template?", "About to apply template.", function(result) {
		if(result)
		{--->
			$("#SMSStructureMain #inpCK1").val(LocalSel1);
			$("#SMSStructureMain #inpCK1").trigger('change');
			$("#SMSStructureMain #inpCK2").val(LocalSel2);
			$("#SMSStructureMain #inpCK2").trigger('change');
			$("#SMSStructureMain #inpCK3").val(LocalSel3);
			$("#SMSStructureMain #inpCK3").trigger('change');
			CreateTemplatePickerDialogSMSTools.remove();
<!---		}
		return false;																	
	});--->
	
	
	
<!---

	$.alerts.okButton = '&nbsp;Confirm Template!&nbsp;';
	$.alerts.cancelButton = '&nbsp;No&nbsp;';
	

	jConfirm( "Are you sure you want to replace your existing content with this template?", "About to apply template.", function(result) {
		if(result)
		{
			$.get('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/emailTemplates/' + LocalSel + '/html/full_width.html', function(data) {
				tinyMCE.execCommand('mceSetContent', false, data);
			});
			CreateTemplatePickerDialogeMailTools.remove();
		}
		return false;																	
	});--->
}

///////////////////////////////////////////////////////////
// Sample
$(function(){
	$.cx_mcid_tree({
		data: SERVICE_DATA,
		clazz: "cx_mcid_tree",
		itemWidth: 80,
		itemHeight: "auto",
		itemLength: 24,
		onClick: function(item){
			$.alerts.okButton = '&nbsp;Confirm Template!&nbsp;';
			$.alerts.cancelButton = '&nbsp;No&nbsp;';
			jConfirm(
				"Are you sure you want to replace your existing content with this template?",
				"About to apply template.",
				function(result){
					if(result){
						eventHandle(item);
						
						//$("#SMSStructureMain #inpCK1").val(item.xml);
						//$("#SMSStructureMain #inpCK2").val(item.rel2);
						//$("#SMSStructureMain #inpCK3").val(item.rel3);
						//	CreateTemplatePickerDialogSMSTools.remove();
					}
					return false;
				}
			);
		},
		onDraw: function(div){
			$("._cx_mcid_tree_container_").remove();
			var container = $('#EBMSMSDynamicContent_PickerDiv');
			container.html(div).attr("class","_cx_mcid_tree_container_");
			container = null;
		}
	});
});
</script>
