<cfprocessingdirective pageencoding = "utf-8">

<h3>PLAIN TEXT</h3>

<p>
If the format of the message is set to text, the character set supported by the EBM platform is a subset of ISO-8859-1 (Latin-1), as defined in the following table. The table shows each character along with its hexadecimal code point (the ISO-8859-1 code point).
</p>


<table>
   <tbody>
        <tr>
            <th>00</th>
            <th>01</th>
            <th>02</th>
            <th>03</th>
            <th>04</th>
            <th>05</th>
            <th>06</th>
            <th>07</th>
            <th>08</th>
            <th>09</th>
            <th>0A</th>
            <th>0B</th>
            <th>0C</th>
            <th>0D</th>
            <th>0E</th>
            <th>0F</th>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>LF</td>
            <td></td>
            <td></td>
            <td>CR</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <th>20</th>
            <th>21</th>
            <th>22</th>
            <th>23</th>
            <th>24</th>
            <th>25</th>
            <th>26</th>
            <th>27</th>
            <th>28</th>
            <th>29</th>
            <th>2A</th>
            <th>2B</th>
            <th>2C</th>
            <th>2D</th>
            <th>2E</th>
            <th>2F</th>
        </tr>
        <tr>
            <td>SP</td>
            <td>!</td>
            <td>“</td>
            <td>#</td>
            <td>$</td>
            <td>%</td>
            <td>&amp;</td>
            <td>‘</td>
            <td>(</td>
            <td>)</td>
            <td>*</td>
            <td>+</td>
            <td>,</td>
            <td>-</td>
            <td>.</td>
            <td>/</td>
        </tr>
        <tr>
            <th>30</th>
            <th>31</th>
            <th>32</th>
            <th>33</th>
            <th>34</th>
            <th>35</th>
            <th>36</th>
            <th>37</th>
            <th>38</th>
            <th>39</th>
            <th>3A</th>
            <th>3B</th>
            <th>3C</th>
            <th>3D</th>
            <th>3E</th>
            <th>3F</th>
        </tr>
        <tr>
            <td>0</td>
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>4</td>
            <td>5</td>
            <td>6</td>
            <td>7</td>
            <td>8</td>
            <td>9</td>
            <td>:</td>
            <td>;</td>
            <td>&lt;</td>
            <td>=</td>
            <td>&gt;</td>
            <td>?</td>
        </tr>
        <tr>
            <th>40</th>
            <th>41</th>
            <th>42</th>
            <th>43</th>
            <th>44</th>
            <th>45</th>
            <th>46</th>
            <th>47</th>
            <th>48</th>
            <th>49</th>
            <th>4A</th>
            <th>4B</th>
            <th>4C</th>
            <th>4D</th>
            <th>4E</th>
            <th>4F</th>
        </tr>
        <tr>
            <td>@</td>
            <td>A</td>
            <td>B</td>
            <td>C</td>
            <td>D</td>
            <td>E</td>
            <td>F</td>
            <td>G</td>
            <td>H</td>
            <td>I</td>
            <td>J</td>
            <td>K</td>
            <td>L</td>
            <td>M</td>
            <td>N</td>
            <td>O</td>
        </tr>
        <tr>
            <th>50</th>
            <th>51</th>
            <th>52</th>
            <th>53</th>
            <th>54</th>
            <th>55</th>
            <th>56</th>
            <th>57</th>
            <th>58</th>
            <th>59</th>
            <th>5A</th>
            <th>5B</th>
            <th>5C</th>
            <th>5D</th>
            <th>5E</th>
            <th>5F</th>
        </tr>
        <tr>
            <td>P</td>
            <td>Q</td>
            <td>R</td>
            <td>S</td>
            <td>T</td>
            <td>U</td>
            <td>V</td>
            <td>W</td>
            <td>X</td>
            <td>Y</td>
            <td>Z</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>_</td>
        </tr>
        <tr>
            <th>60</th>
            <th>61</th>
            <th>62</th>
            <th>63</th>
            <th>64</th>
            <th>65</th>
            <th>66</th>
            <th>67</th>
            <th>68</th>
            <th>69</th>
            <th>6A</th>
            <th>6B</th>
            <th>6C</th>
            <th>6D</th>
            <th>6E</th>
            <th>6F</th>
        </tr>
        <tr>
            <td></td>
            <td>a</td>
            <td>b</td>
            <td>c</td>
            <td>d</td>
            <td>e</td>
            <td>f</td>
            <td>g</td>
            <td>h</td>
            <td>i</td>
            <td>j</td>
            <td>k</td>
            <td>l</td>
            <td>m</td>
            <td>n</td>
            <td>o</td>
        </tr>
        <tr>
            <th>70</th>
            <th>71</th>
            <th>72</th>
            <th>73</th>
            <th>74</th>
            <th>75</th>
            <th>76</th>
            <th>77</th>
            <th>78</th>
            <th>79</th>
            <th>7A</th>
            <th>7B</th>
            <th>7C</th>
            <th>7D</th>
            <th>7E</th>
            <th>7F</th>
        </tr>
        <tr>
            <td>p</td>
            <td>q</td>
            <td>r</td>
            <td>s</td>
            <td>t</td>
            <td>u</td>
            <td>v</td>
            <td>w</td>
            <td>x</td>
            <td>y</td>
            <td>z</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <th>80</th>
            <th>81</th>
            <th>82</th>
            <th>83</th>
            <th>84</th>
            <th>85</th>
            <th>86</th>
            <th>87</th>
            <th>88</th>
            <th>89</th>
            <th>8A</th>
            <th>8B</th>
            <th>8C</th>
            <th>8D</th>
            <th>8E</th>
            <th>8F</th>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <th>90</th>
            <th>91</th>
            <th>92</th>
            <th>93</th>
            <th>94</th>
            <th>95</th>
            <th>96</th>
            <th>97</th>
            <th>98</th>
            <th>99</th>
            <th>9A</th>
            <th>9B</th>
            <th>9C</th>
            <th>9D</th>
            <th>9E</th>
            <th>9F</th>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <th>A0</th>
            <th>A1</th>
            <th>A2</th>
            <th>A3</th>
            <th>A4</th>
            <th>A5</th>
            <th>A6</th>
            <th>A7</th>
            <th>A8</th>
            <th>A9</th>
            <th>AA</th>
            <th>AB</th>
            <th>AC</th>
            <th>AD</th>
            <th>AE</th>
            <th>AF</th>
        </tr>
        <tr>
            <td></td>
            <td>i</td>
            <td></td>
            <td>£</td>
            <td>¤</td>
            <td>¥</td>
            <td></td>
            <td>§</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <th>B0</th>
            <th>B1</th>
            <th>B2</th>
            <th>B3</th>
            <th>B4</th>
            <th>B5</th>
            <th>B6</th>
            <th>B7</th>
            <th>B8</th>
            <th>B9</th>
            <th>BA</th>
            <th>BB</th>
            <th>BC</th>
            <th>BD</th>
            <th>BE</th>
            <th>BF</th>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>¿</td>
        </tr>
        <tr>
            <th>C0</th>
            <th>C1</th>
            <th>C2</th>
            <th>C3</th>
            <th>C4</th>
            <th>C5</th>
            <th>C6</th>
            <th>C7</th>
            <th>C8</th>
            <th>C9</th>
            <th>CA</th>
            <th>CB</th>
            <th>CC</th>
            <th>CD</th>
            <th>CE</th>
            <th>CF</th>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Ä</td>
            <td>Å</td>
            <td>Æ</td>
            <td>Ç</td>
            <td></td>
            <td>É</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <th>D0</th>
            <th>D1</th>
            <th>D2</th>
            <th>D3</th>
            <th>D4</th>
            <th>D5</th>
            <th>D6</th>
            <th>D7</th>
            <th>D8</th>
            <th>D9</th>
            <th>DA</th>
            <th>DB</th>
            <th>DC</th>
            <th>DD</th>
            <th>DE</th>
            <th>DF</th>
        </tr>
        <tr>
            <td></td>
            <td>Ñ</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Ö</td>
            <td>Ø</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Ü</td>
            <td></td>
            <td></td>
            <td>ß</td>
        </tr>
        <tr>
            <th>E0</th>
            <th>E1</th>
            <th>E2</th>
            <th>E3</th>
            <th>E4</th>
            <th>E5</th>
            <th>E6</th>
            <th>E7</th>
            <th>E8</th>
            <th>E9</th>
            <th>EA</th>
            <th>EB</th>
            <th>EC</th>
            <th>ED</th>
            <th>EE</th>
            <th>EF</th>
        </tr>
        <tr>
            <td>á</td>
            <td></td>
            <td></td>
            <td></td>
            <td>ä</td>
            <td>å</td>
            <td>æ</td>
            <td></td>
            <td>è</td>
            <td>é</td>
            <td></td>
            <td></td>
            <td>ì</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <th>F0</th>
            <th>F1</th>
            <th>F2</th>
            <th>F3</th>
            <th>F4</th>
            <th>F5</th>
            <th>F6</th>
            <th>F7</th>
            <th>F8</th>
            <th>F9</th>
            <th>FA</th>
            <th>FB</th>
            <th>FC</th>
            <th>FD</th>
            <th>FE</th>
            <th>FF</th>
        </tr>
        <tr>
            <td></td>
            <td>ñ</td>
            <td>ò</td>
            <td></td>
            <td></td>
            <td></td>
            <td>ö</td>
            <td></td>
            <td>ø</td>
            <td>ù</td>
            <td></td>
            <td></td>
            <td>ü</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </tbody>

</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<a href="http://matadornetwork.com/abroad/global-netspeak-20-words-and-phrases-to-get-started-texting-in-spanish/">http://matadornetwork.com/abroad/global-netspeak-20-words-and-phrases-to-get-started-texting-in-spanish/</a>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>
The maximum size of an SMS is 140 bytes. This equates to 160 plain text 7-bit characters (Latin-1 or GSM) or 70 Unicode (16-bit) characters. If you wish to send an SMS longer than this, it must be split into multiple parts. Each of these parts are sent as individual SMS messages to the handset, so in order for the handset to piece them together, you must configure a UDH (User Data Header) for each part of the long message.
</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Kill Word Copy and Paste Common Unicode &quot;special&quot; characters</p>
<p>‘ (U+2018) LEFT SINGLE QUOTATION MARK - chr(8216)<br>
’ (U+2019) RIGHT SINGLE QUOTATION MARK - chr(8217)<br>
“ (U+201C) LEFT DOUBLE QUOTATION MARK  - chr(8220)<br>
” (U+201D) RIGHT DOUBLE QUOTATION MARK  - chr(8221)<br>
<br>
Dashes - en and em<br>
U+2012 - chr(8210)<br>
U+2013 - chr(8211)<br>
U+2014 - chr(8212)<br>
U+2015 - chr(8213)<br>
<br>
Unicode Elipses will be replaces with .
U+2015 - chr(8230)<br>

</p>


<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>


<p>Sample Unicode for testing:<br></p>
<BR>

<cfset SampleText = "What is your favorite color?" />

<cfset Pattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]" ) ) />
                       
           
    <!---
        Create the pattern matcher for our target text. The
        matcher will be able to loop through all the high
        ascii values found in the target string.
    --->
    <cfset Matcher = Pattern.Matcher(
        JavaCast( "string", SampleText )
        ) />


	<cfset RetVal = Matcher.Find() />

<cfoutput>#SampleText#</cfoutput><BR>    
<cfdump var="#RetVal#" />

<BR>


<BR>

<cfset SampleText = "What is your favorite color? " & chr(8217) />

<cfset Pattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]" ) ) />
                       
           
    <!---
        Create the pattern matcher for our target text. The
        matcher will be able to loop through all the high
        ascii values found in the target string.
    --->
    <cfset Matcher = Pattern.Matcher(
        JavaCast( "string", SampleText )
        ) />


	<cfset RetVal = Matcher.Find() />

<cfoutput>#SampleText#</cfoutput><BR>    
<cfdump var="#RetVal#" /> but will auto scrub with ASCII "

<BR>


<BR>

<cfset SampleText = "What is your favorite color? " & chr(8260) />

<cfset Pattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]" ) ) />
                       
           
    <!---
        Create the pattern matcher for our target text. The
        matcher will be able to loop through all the high
        ascii values found in the target string.
    --->
    <cfset Matcher = Pattern.Matcher(
        JavaCast( "string", SampleText )
        ) />


	<cfset RetVal = Matcher.Find() />

<cfoutput>#SampleText#</cfoutput><BR>    
<cfdump var="#RetVal#" />


<BR>

<BR>

<cfset SampleText = "Bonjour. Vous êtes très mignon, et je voudrais vraiment votre prise en mains (ou, à s'emparer de vos fesses, même si je pense que peut-être trop en avant à ce moment)." & chr(8217)  />


<cfset Pattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]" ) ) />
          
 
    <!---
        Create the pattern matcher for our target text. The
        matcher will be able to loop through all the high
        ascii values found in the target string.
    --->
    <cfset Matcher = Pattern.Matcher(
        JavaCast( "string", SampleText )
        ) />


	<cfset RetVal = Matcher.Find() />
    
<cfoutput>#SampleText#</cfoutput><BR>    
<cfdump var="#RetVal#" />
<cfdump var="#Matcher#" />

<cfset CSVInvalidChar = '' />
<!--- loop through the matches --->
<cfloop condition="Matcher.find()">
	<cfdump var="#Matcher.group(0)#" />
	<cfset CSVInvalidChar = ListAppend(CSVInvalidChar, Matcher.group(0)) />
   
</cfloop>

<cfdump var="#CSVInvalidChar#" />

				

<!---


public String removeBoldTags(CharSequence htmlString) {
  Pattern patt = Pattern.compile("<b>([^<]*)</b>");
  Matcher m = patt.matcher(htmlString);
  StringBuffer sb = new StringBuffer(htmlString.length());
  while (m.find()) {
    String text = m.group(1);
    // ... possibly process 'text' ...
    m.appendReplacement(sb, Matcher.quoteReplacement(text));
  }
  m.appendTail(sb);
  return sb.toString();
}
--->



<BR>


<BR>

<cfset SampleText = "Bonjour. Vous êtes très mignon, et je voudrais vraiment votre prise en mains (ou, à s'emparer de vos fesses, même si je pense que peut-être trop en avant à ce moment)." />


<cfset Pattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]" ) ) />
          
 
    <!---
        Create the pattern matcher for our target text. The
        matcher will be able to loop through all the high
        ascii values found in the target string.
    --->
    <cfset Matcher = Pattern.Matcher(
        JavaCast( "string", SampleText )
        ) />


	<cfset RetVal = Matcher.Find() />
    
<cfoutput>#SampleText#</cfoutput><BR>    
<cfdump var="#RetVal#" />

<BR>


<BR>

<cfset SampleText = "Interrup. en 11840 SW 177TH TER causada por un equipo de FPL dañado. Su servicio debería estar restablecido. Si no, pruebe FPL.com/resetbreakers" />
    
<!--- Replace high unicode characters with equivalent low ASCII values --->
<!--- http://www.fileformat.info/info/unicode/char/00ed/index.htm --->
<cfset SampleText = replaceNoCase(SampleText, chr(195) & chr(177), chr(241), "ALL")>
<cfset SampleText = replaceNoCase(SampleText, chr(195) & chr(173), chr(236), "ALL")>
						
<!--- MBLox (All Carriers?) does not support all accent ASCII characters --->
<!--- á with à --->
<cfset SampleText = replaceNoCase(SampleText, chr(341), chr(340), "ALL")>

<!---  	è 	é both work OK - Why ?!?!  --->
<!---<cfset SampleText = replaceNoCase(SampleText, chr(233), chr(232), "ALL")>--->

<!--- Replace í with ì --->
<cfset SampleText = replaceNoCase(SampleText, chr(237), chr(236), "ALL")>

<!--- ó with ò  --->
<cfset SampleText = replaceNoCase(SampleText, chr(243), chr(242), "ALL")>

<!--- Replace ú with ù --->
<cfset SampleText = replaceNoCase(SampleText, chr(250), chr(249), "ALL")>


<cfset Pattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]" ) ) />
 
    <!---
        Create the pattern matcher for our target text. The
        matcher will be able to loop through all the high
        ascii values found in the target string.
    --->
    <cfset Matcher = Pattern.Matcher(
        JavaCast( "string", SampleText )
        ) />


	<cfset RetVal = Matcher.Find() />
    
<cfoutput>#SampleText#</cfoutput><BR>    
<cfdump var="#RetVal#" />



<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]" ) ) />
                        
<!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
<cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", SampleText ) ) />


<!--- Look for character higher than 255 ASCII --->   
<cfif UniSearchMatcher.Find() >
	
	<cfset SendAsUnicode = 1 />

<cfelse>

	<cfset SendAsUnicode = 0 />

</cfif>
                     
                         
<cfoutput>SendAsUnicode = #SendAsUnicode#</cfoutput><BR>
   
                        

<cfset SampleText = "Alerta FPL: No hay servicio en 2391 KEMPS BAY. Usted es el ùnico cliente afectado. Tiempo de restablecimiento estimado bajo investigaciòn. FPL.com/outage" />

<cfset Pattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]" ) ) />
          
 
    <!---
        Create the pattern matcher for our target text. The
        matcher will be able to loop through all the high
        ascii values found in the target string.
    --->
    <cfset Matcher = Pattern.Matcher(
        JavaCast( "string", SampleText )
        ) />


	<cfset RetVal = Matcher.Find() />
    
<cfoutput>#SampleText#</cfoutput><BR>    
<cfdump var="#RetVal#" />

<BR>
   
                                                
                        
                        
<BR>










            