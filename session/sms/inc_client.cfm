﻿﻿<script type="text/javascript">
/**
 * @author SETACINQ
 */
!function(window, $, client) {
	// build client object
	client.data = {};
	client.socket = io.connect('<cfoutput>#serverSocket#:#serverSocketPort#</cfoutput>');
	
	client.on = function(event, element, callback) {
		/* Used jQuery 1.7 event handler */
		$(element).live(event, function(e) {
			e.preventDefault();
			callback.apply(this, arguments); // Used arguments to fixed error in IE
		});
	};

	client.off = function(event, element) {
		/* Used jQuery 1.7 event handler */
		$(element).die(event);
	};
	
	client.appendMessage = function(result, type){
		// type (Agent=1,User=2)
		if(type == 1){
			$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble you">' + result.message + '</div>');
			$("#SMSHistoryScreenArea").stop(true, true).animate({ scrollTop: $('#SMSHistoryScreenArea')[0].scrollHeight}, 1000);	
		}
		else{
			$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble me">' + result.message + '</div>');	
		}
	};

	// DOM ready
	$(function() {
		
		client.socket.on('connect', function () {
			client.socket.emit('connect');
		});
				
		client.socket.on('addnewcomment', function(result) {
			if(result.shortcode == $("#inpSMSToAddress").val() 
				&& result.contactstring == $("#inpContactString").val())
			{
				client.appendMessage(result, 1);
				$("#SMSTextInputArea").val("");
			}
		});
		
//		client.socket.on('notification', function(result) {
//			if(result.shortcode == client.data.shortcode 
//				&& result.contactstring == client.data.contactstring
//				&& result.userid == client.data.userid)
//			{
//				client.appendMessage(result, 2); // append new message from user mobile
//			}
//		});
		
		client.socket.on('disconnect', function (msg) {
			// TODO: call when time out session
		});
		
		
		window.onbeforeunload = function() {
        	client.socket.disconnect();
    	};
		
	}); // End DOM ready
}(window, window.jQuery, window.client);

</script>