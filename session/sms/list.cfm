<!--- Set title for page here --->
<cfhtmlhead text="<title>SMS CMS</title>">



<cfoutput>
	
    <cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
	<cfinclude template="/#sessionPath#/cfc/csc/constants.cfm">
	
	<script type="text/javascript" src="#rootUrl#/#publicPath#/home7assets/js/datatables1.10.7/js/jquery.datatables.min.js"></script>
	<script type="text/javascript" src="#rootUrl#/#publicPath#/home7assets/js/datatables1.10.7/extensions/Responsive/js/dataTables.responsive.min.js"></script>

 	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/home7assets/js/datatables1.10.7/extensions/Responsive/css/dataTables.responsive.css">
  	<link rel ="styleSheet" href="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/css/ire/sms.css">
	<link rel ="styleSheet" href="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/css/administrator/smscampaigns/shortcoderequest.css">

</cfoutput>

<cfparam name="goto" default="" >

<cfsetting showdebugoutput="false">

<!---check permission--->
<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>
<cfset smsPermission = permissionObject.havePermission(SMS_Campaigns_Management_Title)>
<cfset smsAddSCPermission = permissionObject.havePermission(Short_Code_Add_Title)>
<cfset smsRemoveSCPermission = permissionObject.havePermission(Short_Code_Remove_Title)>
<cfset smsActionKeywordPermission = permissionObject.havePermission(Keyword_Action_Title)>

<!--- Super user cannot access to short code request page --->
<cfif Session.USERROLE EQ 'SuperUser' OR NOT smsPermission.havePermission>
	<cfset session.permissionError = surveyPermission.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<style>

.sc-name
{
	width:150px !important;		
}

table.dataTable
{
	width:100%;	
}

</style>


<cfinvoke component="#LocalSessionDotPath#.cfc.csc.CSC" method="getShortCodeByUser" returnvariable="getShortCodeByUser">

<!---<cfdump var="#getShortCodeByUser#" />--->

<cfset SCMB =getShortCodeByUser.GETSCMB>
<cfif getShortCodeByUser.RXRESULTCODE LT 0>
	<cflocation url="#rootUrl#/#sessionPath#/account/home"> 
</cfif>
<!--- include js --->
<cfinclude template="smsJs.cfm">



<!---wrap the page content do not style this--->
<div id="page-content">
   
  <div class="container" >
    <h2 class="no-margin-top">SMS Content Management System</h2>


	<div style="font-size: 25px; margin-bottom:30px">
		
	</div>
	
	<!--- 
	if number of short code request GT 0 show list of short code request 
	if number of approved and reject short code request GT 0 show list of approved and reject short code request
	if number of pending short code request GT 0 show list of pending short code request
	Normal user and company admin can see approved, reject and pending short code request, delete pending short code request, clear approved and reject short code request
	company user can see approved short code request only, cannot delete pending short code request, cannot clear approved and reject short code request
	--->
	<cfif getShortCodeByUser.COUNTSHORTCODE GT 0 >
		
		<div style ="clear:both"></div>
		
		<!--- 
			get keyword by short code and current user 
			if exist keyword for shortcode, show keyword follow grid
		 --->
		<cfinvoke component="#LocalSessionDotPath#.cfc.csc.CSC" method="getShortCodeRequestByUser" returnvariable="getShortCodeRequestByUser">
		<cfset SCMBbyUser =getShortCodeRequestByUser.getSCMB />
		
       <!--- <cfdump var="#getShortCodeRequestByUser#" />--->
       
		<cfif getShortCodeRequestByUser.RXRESULTCODE GT 0>
			<div class="sc-bar">
				<div class="left sc-name">Request</div>		
			</div>
			<div class="rq-content">
				<div id="tabs">
					<ul>
						<li><a href="#tabs-1">Updates</a></li>
						<li><a href="#tabs-2">Pending Requests</a></li>
					</ul>
					<div id="tabs-1">
						<cfif getShortCodeByUser.countSCApprovedAndReject GT 0>
							<div class="note-request">You have <cfoutput>#getShortCodeByUser.countSCApprovedAndReject#</cfoutput> update<cfif getShortCodeByUser.countSCApprovedAndReject GT 0>s</cfif> regarding your requests</div>
							<div class="RequestGrid">
								<table id="SCApprovedAndRejected" width="100%">
									<tfoot id="tfSCApprovedAndRejected" width="100%">
										<tr align="left">
											<th style="padding-left: 0px;"></th>
											<th></th>
											<th></th>
											<th></th>
										</tr>
									</tfoot>
								</table>
								<script type="text/javascript">
									/* Initialise the DataTable */
									InitDatatableForGettingRequestNotPending();
								</script>
							</div>
							<div style="height:30px;clear:both"></div>
						<cfelseif session.USERROLE EQ 'CompanyUser'>
							There is no approved short code request
						</cfif>
						<div style="clear:both"></div>
					</div>
					<div id="tabs-2">
                    	<cfif session.userRole EQ 'User' OR session.userRole EQ 'CompanyAdmin'>
							<div class="note-request">You have <cfoutput>#getShortCodeByUser.COUNTSCPENDING#</cfoutput> pending<cfif getShortCodeByUser.COUNTSCPENDING GT 0>s</cfif> short code request. You may submit additional requests any time</div>
							<cfif smsAddSCPermission.havePermission>
								<div id="SCRequestPanel" style="float: right;">
									<!---<a class="button filterButton small" onclick="AddSC(); return false;" href="#">Add</a>--->
                                    <cfoutput>
                                    	<a data-toggle='modal' href='dsp_insertSCRequest?ShowCancel=true' data-target='##KeywordEditModal'>(Add)</a>
                                    </cfoutput>
								</div>
							</cfif>
							<div style="clear:both;"></div>
							<cfif getShortCodeByUser.COUNTSCPENDING GT 0 >
								<div class="RequestGrid" >
									<table id="SCPending" width="100%">
										<tfoot id="tfSCPending" width="100%">
											<tr align="left">
												<th style="padding-left: 0px;"></th>
												<th></th>
												<th></th>
												<th></th>
											</tr>
										</tfoot>
									</table>
									<script type="text/javascript">
										InitDatatableForGettingRequestPending();
									</script>
								</div>
							<cfelse>
									<div style="clear:both;height:40px;"></div>					
							</cfif>
						</cfif>
					</div>
				</div>			
			</div>
			<div style="clear:both;"></div>
			<cfloop query="SCMBbyUser">
				<cfoutput >
					<cfinvoke component="#LocalSessionDotPath#.cfc.csc.CSC" method="getKeywordByShortCodeAndUser" returnvariable="getKeywordByShortCodeAndUser" >
						<cfinvokeargument name="ShortCodeRequestId" value="#SCMBbyUser.ShortCodeRequestId_int#">
					</cfinvoke>
                    
                    <div class="sc-bar row" style="margin-left: 0;">
						<div class="" style="float:left; margin-left:10px;">CSC: #SCMBbyUser.ShortCode_vch#  </div>						
										
						<cfinvoke component="#LocalSessionDotPath#.cfc.csc.CSC" method="GetClassification" returnvariable="GetClassification" >
							<cfinvokeargument name="ClassificationId" value="#SCMBbyUser.Classification_int#">
						</cfinvoke>						
						<div class="" style="float:right; margin-right:10px;">#GetClassification#</div>
						
                        <cfif smsRemoveSCPermission.havePermission>
                            <div class="" style="float:right; margin-right:10px;">
                                <img class='del_CPPContactRow ListIconLinks' title='Delete this shortcode' 
                                src='<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons16x16/delshorcode.png' width='10' height='10' onclick="RemoveSC('#SCMBbyUser.ShortCodeId_int#', '#SCMBbyUser.ShortCode_vch#')">
                            </div>
						</cfif>		
                        
						<!---<a href="javascript:void(0);" style="padding-left:1em" onclick="ShowHideIndividual('<cfoutput>#SCMBbyUser.ShortCodeId_int#</cfoutput>',this)"><img src="<cfoutput>#rootUrl#/#publicPath#/css/mcid/images/group-collapse.gif</cfoutput>"></img></a>--->
                        
                        <!---<cfdump var="#SCMBbyUser#" />--->
					</div>
					<div class="sc-content">
                    
                   			<!---<cfif <!---getKeywordByShortCodeAndUser.records GT 0 OR ---> SCMBbyUser.CLASSIFICATION_INT EQ CLASSIFICATION_PUBLIC_SHARED_VALUE>--->
							<div class="KeywordGrid" id="div#SCMBbyUser.ShortCodeId_int#">
								
                                                             
                                <cfif NOT(SCMBbyUser.KEYWORDQUANTITY_INT LTE getKeywordByShortCodeAndUser.countSCNotDefault AND SCMBbyUser.CLASSIFICATION_INT EQ CLASSIFICATION_PUBLIC_SHARED_VALUE) AND smsActionKeywordPermission.havePermission  >                                    
                                        <div style="text-align:right; float:right;">                            
                                            <a class="<!---sc-addkeyword--->" data-toggle='modal' href='dsp_keywordDetails?ShortCodeRequestId=#SCMBbyUser.SHORTCODEREQUESTID_INT#&shortCode=#SCMBbyUser.ShortCode_vch#&shortcodeId=#SCMBbyUser.ShortCodeId_int#' data-target='##KeywordEditModal'>Add Keyword</a>
                                            <!---<a  onclick="AddKeyword(#SCMBbyUser.SHORTCODEREQUESTID_INT#, '#SCMBbyUser.ShortCode_vch#', $(this)); return false;" href="##">Add Keyword</a>--->
                                        </div>                                                            
                                </cfif>
                                
                                <table id="tbl#SCMBbyUser.ShortCodeId_int#" width="100%"></table>
								<div style="clear:both;"></div>
								<!---use datatable plugin to generate data here --->
								<script type="text/javascript">
									InitDatatableByShortCodeAndUser("<cfoutput>#SCMBbyUser.ShortCodeId_int#</cfoutput>","#SCMBbyUser.ShortCodeRequestId_int#");
								</script>
								<span class="numberOfUse">
										<cfif SCMBbyUser.CLASSIFICATION_INT EQ CLASSIFICATION_PUBLIC_SHARED_VALUE OR SCMBbyUser.CLASSIFICATION_INT EQ CLASSIFICATION_SIRE_SHARED_VALUE>
											#getKeywordByShortCodeAndUser.CountSCNotDefault# of #SCMBbyUser.KEYWORDQUANTITY_INT# 
										<cfelse>
											 #getKeywordByShortCodeAndUser.records# of unlimited
										</cfif>
									keywords of used
								</span>
							</div>
						<!---</cfif>--->
					</div>
				</cfoutput>				
			</cfloop>
		</cfif>
		<div style="clear:both;"></div>
	<cfelseif session.USERROLE EQ 'CompanyAdmin' OR session.USERROLE EQ 'User'>
		<cfinclude template="dsp_insertSCRequest.cfm">
	<cfelseif session.USERROLE EQ 'CompanyUser'>
		There is no approved short code request
	<cfelse>
		<cflocation url="#rootUrl#/#sessionPath#/account/home">
	</cfif> 
    
        <p class="lead"></p>

    	
        <row>
        	
            <div class="">
            
            
            </div>
        
        </row>
    
   </div>
  <!--- /.container --->
  
</div>
<!--- /#page-content --->


<!-- Modal -->
<div class="modal fade" id="KeywordEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Modal title</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

