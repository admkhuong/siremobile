<style type="text/css">

.AlignLeft
{
	text-align:left;	
	padding-left:1em;
}

.AlignRight
{
	text-align:right;
	padding-right:1em;	
}

select {
    font-size: .8em;
    width: 100%;
}

	.KeywordGrid{
		float:left;
		<!---width:670px;--->
		width:100%;
	 	background: none repeat scroll 0 0 #fff;
	}
	.RequestGrid{
		float:left;
		<!---width:670px;--->
		width:100%;
	 	background: none repeat scroll 0 0 #fff;
	}
	.note-request{
		color:#666666;
		float:left;
		font-size:13px;
		margin-bottom:10px;
	}
	table.dataTable thead{
		border: 1px solid #0888d1;
	}
	table.dataTable tbody{
		border: 1px solid #ddd;
		color:#666;
	}
	table.dataTable a.paginate_button{ 
		padding: 0px 5px 0px;
	}
	table.dataTable tr.odd  {
	    background-color: #E5E5E5;
	}
	table.dataTable tr.odd td.sorting_1 {
	    background-color: #E5E5E5;
	}
	table.dataTable tr.even {
	    background-color: #FFFFFF;
	}
	table.dataTable tr.even td.sorting_1{
	    background-color: #FFFFFF;
	}
	.paging_full_numbers{
		line-height: 28px;
	}
	.datatables_wrapper{
	    background: none repeat scroll 0 0 #FCFFF4;
	    clear: both;
	    max-height: 100%;
	    position: relative;
	}
	.numberOfUse{
		float: right; 
		margin-top: 4px;
		color:#666666;
	}
	.paging_full_numbers {
    	line-height: 27px;
	    height: 27px;
	}
	.del_CPPContactRow{		
		float:left;
		cursor:pointer;
		margin-top:10px;
		margin-left:180px;
	}


	.ui-tabs-active {
    background-color: #FFFFFF !important;
    color: #FFFFFF !important;
	}
	.ui-tabs-active a {
	    color: #0888D1 !important;
	}
	.ui-widget-content {
	    border: 0 none;
	}
	.ui-tabs .ui-tabs-nav {
	    background: none repeat scroll 0 0 #DDDDDD;
	}
	.ui-tabs .ui-tabs-panel {
	    border: 0 none;
	    height: auto;
	    min-height: 236px;
		<!---width:670px;--->
		width:100%;
	    padding: 10px 20px 20px 20px;
	}
	.ui-tabs-nav {
	    border-left-width: 0;
	    border-right-width: 0;
	    height: 38px;
	}
	.ui-tabs .ui-tabs-nav li {
	    background: none repeat scroll 0 0 #F0F0F0;
	    bottom: 0;
	    height: 30px;
	    margin-bottom: 0;
	    margin-left: 5px;
	    margin-top: 4px;
	    top: 0;
	    width: 161px;
	}
	.ui-tabs .ui-tabs-nav li.ui-state-default {
	    border-bottom: 0 none;
	}
	.ui-tabs .ui-tabs-nav li a {
	    color: #666666;
	    
	    font-size: 14px;
	    font-weight: bold;
	    line-height: 30px;
	    padding: 0;
	    text-align: center;
	    width: 159px;
		padding-bottom:4px;
	}
	.ui-tabs-nav a, ui-tabs-nav a:link {
	   <!--- border-color: #AAAAAA;
	    border-radius: 5px 5px 0 0;
	    border-style: solid;
	    border-width: 1px;--->
		border:none;
	    color: #666666;
	    height: 34px;
		margin-top: -5px;
	}
	.ui-tabs-nav .ui-tabs-active a {
	    border-bottom: 0 none !important;
	}
	#innertube a, #innertube a:link, #innertube a:hover, #innertube a:visited, #innertube a:active {
	    color: #666666;
	}
	




.sc-bar{
	background-color:#0888d1;
	width:100%;
	height:30px;
	line-height:30px;	
	border:1px solid #0c6599;
	border-radius: 3px 3px 0 0;
	color:#fff;
	font-size:13px;	
	float:left;	
	box-shadow: inset 0 1px 0 #2aa3e9;
}
.sc-name{
	width:100px;
	margin-left:14px;
}
.sc-content{
	padding: 10px 20px 10px 20px;
	border: 1px solid #DDD;
	border-top:0;
	float:left;
	<!---width:670px;--->
	width:100%;
	margin-bottom:30px;
	box-shadow: 0px 3px 4px -2px rgba(0,0,0,0.5);	
}
.rq-content{
	
	border: 1px solid #DDD;
	border-top:0;
	float:left;
	<!---width:710px;--->
	width:100%;
	margin-bottom:30px;
	box-shadow: 0px 3px 4px -2px rgba(0,0,0,0.5);	
}

table.dataTable thead tr{
	background-color:#0888d1;
	height:23px;
	line-height:23px;
	border-bottom:2px solid #fa7d29;
	color:#fff;
}



.dataTable
{
	
	width:100%;	
}


.datatables_paginate {
	float: right;
	text-align: right;
	background-color: #EEEEEE;
    border-radius: 0 0 3px 3px;
    border-top: 2px solid #0888D1;
    height: 35px;
    padding-top: 10px;
    width: 100%;
	height:33px !important;
	box-shadow: 0px 3px 4px -2px rgba(0,0,0,0.5);
}

.datatables_info
{
	position:relative;
	top:32px;
	left:12px;
	color:#666666;
	font-size:14px;
}

.datatables_filter {
	float: left  !important;
	text-align: left !important;
	border:1px solid #ddd;
	border-radius:4px;
	<!---width:654px;--->
	width:90%;
	padding-left:14px;
	margin-bottom:10px;
	height:26px;
}
.datatables_filter input{
	height:25px;
	border-left:1px solid #ddd;
	border-top:0;
	border-right:0;
	border-bottom:0;
	padding:0 0 0 10px;
	<!---width:507px;--->	
	width:80%;
	margin:0;
	border-radius:0 3px 3px 0;
	color:#cccccc;
	font-size:12px;
	border-shadow:0;
	
}


.datatables_filter label{
	color:#666;
	font-size:12px;
}
.sc-addkeyword{
	color:#fff !important;
	text-decoration:underline !important;
	font-size:12px;
	margin-right:10px;
}
.paginate_button{
	background-color:#46a6dd !important;
	border:1px solid #0888d1 !important;
	box-shadow: 0 1px 0 #2AA3E9 inset;
	padding:3px 8px !important;
	color:#fff !important;
}
.paging_full_numbers a.paginate_button:hover {
    background-color:#0888d1 !important;
    text-decoration: none !important;
}
a.paginate_active
{
	background-color:#0888d1 !important;
	border:1px solid #0888d1 !important;
	box-shadow: 0 1px 0 #2AA3E9 inset;
    border-radius: 5px;
	padding:3px 8px !important;
    color: #fff !important;
    cursor: pointer;
}

</style>
<script type="text/javascript">
	$(document).ready(function(){
		$('#subTitleText').text('SMS Campaign Management');
		$('#mainTitleText').html('<cfoutput>Campaigns <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> SMS </cfoutput>');
		<cfif goto NEQ ''>
			location.href = '<cfoutput>#rootUrl#/#sessionPath#/ire/smscampaign/sms</cfoutput>'+"#<cfoutput>#goto#</cfoutput>";
		</cfif>
		
		<!--- Force modal to reload with new parameters each tine or it will just re-SHOW the old data --->
		$('body').on('hidden.bs.modal', '.modal', function () {			
  			$(this).removeData('bs.modal');			
		});

	});
	$(function() {
		$( "#tabs" ).tabs();
	});
	
	<!---
	<!--- Program Active Message Editor  --->
	function EditKeyword(ShortCodeRequestId, keywordId, shortCode, inpBatchId, shortCodeId)
	{		
		
		var ParamStr = '';
	
		ParamStr = '?inpbatchid=' + encodeURIComponent(inpBatchId) + '&ShortCodeRequestId=' + ShortCodeRequestId + '&keywordId=' + keywordId + '&shortCode=' + encodeURIComponent(shortCode) + '&shortCodeId=' + shortCodeId;
									
		var options = 
		{
			show: true,
			"remote" : 'dsp_keywordDetails' + ParamStr
		}

		$('#KeywordEditModal').modal(options);
					
	}
		--->
			
	function RemoveSC(scId, scText){
		bootbox.confirm( "Would you  like to remove Short code " + scText + "? Hit OK to Remove Short Code", function(result){ 
			if(result){
				$.ajax({
					type: "POST", <!--- Update short code if exist else alert error --->
					url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=DeleteSCRequest&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					data:  { 
						inpSCId: scId
					},					  
					success:function(d){
						<!--- Get row 1 of results if exisits, Alert if failure--->
						if (d.ROWCOUNT > 0){						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined"){							
								if(d.DATA.RXRESULTCODE[0] > 0){
									location.href = '<cfoutput>#rootUrl#/#sessionPath#/ire/smscampaign/sms</cfoutput>';
								}else{
									bootbox.alert(d.DATA.MESSAGE[0] + ' - Failure');							
								}
							}
						}else{
							<!--- No result returned --->		
							bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");
						}
					} 			
				});	
			}
		});
	}
	
	function DeleteSCRequest(scId, scText){
		bootbox.confirm( "Are you sure you'd like to delete the request for Short code " + scText + "? Hit OK to Delete Request", function(result){ 
			if(result){
				$.ajax({
					type: "POST", <!--- Update short code if exist else alert error --->
					url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=DeleteSCRequest&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					data:  { 
						inpSCId: scId,
					},					  
					success:function(d){
						<!--- Get row 1 of results if exisits, Alert if failure--->
						if (d.ROWCOUNT > 0){						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined"){							
								if(d.DATA.RXRESULTCODE[0] > 0){
									//reload data in datatable
									var oTable = $('#SCPending').dataTable();
								  	// Re-draw the table - you wouldn't want to do it here, but it's an example :-)
								  	oTable.fnDraw();									
								}else{
									jAlert(d.DATA.MESSAGE[0] + ' Failure');							
								}
							}
						}else{
							<!--- No result returned --->		
							bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");
						}
					} 			
				});	
			}
		});
	}
	
	function ClearSCRequest(scId, scText){
		bootbox.confirm( "Are you sure you'd like to clear the request for Short code " + scText + "? Hit OK to Clear Request", function(result){ 
			if(result){
				$.ajax({
					type: "POST", <!--- Update short code if exist else alert error --->
					url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=ClearSCRequest&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					data:  { 
						inpSCId: scId
					},					  
					success:function(d){
						<!--- Get row 1 of results if exisits, Alert if failure--->
						if (d.ROWCOUNT > 0){						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined"){							
								if(d.DATA.RXRESULTCODE[0] > 0){
									//reload data in datatable here
									var oTable = $('#SCApprovedAndRejected').dataTable();
									oTable.fnDraw();
								}else{
									bootbox.alert(d.DATA.MESSAGE[0] + ' Failure');							
								}
							}
						}else{
							<!--- No result returned --->		
							bootbox.alert("Error. No Response from the remote server. Check your connection and try again.");
						}
					} 			
				});	
			}
		});
	}

	function DeleteKeyword(keywordId,tableId, inpKeyword) {
		
		bootbox.confirm( "Are you sure you want to delete selected keyword - {" + inpKeyword + " }? Hit OK to Delete Keyword", function(result) { 
			if (result)
			{	
				var data = { 
					     	keywordId : keywordId
					    };
					    		
				ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'DeleteKeyword', data, "Delete keyword fail!", function(d ) {
					bootbox.alert("Keyword deleted Success!", function(result) { 
						var oTable = $('#tbl'+tableId).dataTable();
					  	
					  	oTable.fnDraw();
					});
				});			
				
			}
		});	
	}
	
	function smsReporting(keywordId,shortCode,keyword){
		location.href = '<cfoutput >#rootUrl#/#SessionPath#/</cfoutput>'+'ire/smscampaign/smsreporting/smsreporting?keywordId='+keywordId+'&shortCode='+shortCode+'&keyword='+keyword;
	}
	
	function EditSurvey(inpObj) 
	{
		if( parseInt($(inpObj).attr("BatchId")) > 0 )
		{
			location.href = '<cfoutput >#rootUrl#/#SessionPath#/</cfoutput>'+'ire/builder/index?inpBatchId='+ $(inpObj).attr("BatchId") + '&INPCOMTYPE=SMS';
		}
	}
	
	function htmlEncode(value){
	    if (value) {
	        return jQuery('<div />').text(value).html();
	    } else {
	        return '';
	    }
	}
 
	function htmlDecode(value) {
	    if (value) {
	        return $('<div />').html(value).text();
	    } else {
	        return '';
	    }
	}
	
	function InitDatatableByShortCodeAndUser(shortCodeId,shortCodeRequestId){
		$('#tbl'+shortCodeId).dataTable( {
		    "bProcessing": true,
		    "bStateSave": true,
		    "bServerSide": true,
			<!---"sPaginationType": "full_numbers",--->
			"pagingType": "full",
		 	"oLanguage": {
            	"sSearch": "Search for keyword:"
        	},
			"sAjaxDataProp": "aaData",
		    "bLengthChange": false,
//			"bFilter": false,
		    "sAjaxSource": "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=GetKeywordByShortCodeAndUserForDatatable&returnformat=plain&queryformat=column",
		    "aoColumns": [
				{"sName": 'Keyword', "sTitle": 'Keyword', "sWidth": '60%',"bSortable": false, "sClass": "AlignLeft"},
				<!---{"sName": 'Triggered', "sTitle": 'Triggered', "sWidth": '20%',"bSortable": false, "sClass": "AlignLeft"},--->
				{"sName": 'Options', "sTitle": 'Options', "sWidth": '40%',"bSortable": false, "sClass": "AlignRight"}
			],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
		        aoData.push({"name":"ShortCodeRequestId", "value":  shortCodeRequestId});
			 	aoData.push({"name":"ShortCodeId", "value": shortCodeId});
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
				 });
	        },
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				//decode html that passing from server
				$('td:eq(0)', nRow).html(htmlDecode(aData[0]));
				$('td:eq(1)', nRow).html(htmlDecode(aData[1]));
	            $('td', nRow).attr('nowrap','nowrap');
	            return nRow;
	       	},
			"fnInitComplete": function(oSettings, json){
//				$("#tbl"+shortCodeId+"_wrapper").prepend("<input type='button' value='+' onclick='ShowHideIndividual("+shortCodeId+");'/>");
				<!---$(".next.paginate_button").html("&raquo;");
			    $(".previous.paginate_button").html("&laquo");
			    $(".first.paginate_button").css("display","none");
			    $(".last.paginate_button").css("display","none");--->
			}
	    });
	}
	
	function InitDatatableForGettingRequestNotPending(){
		$('#SCApprovedAndRejected').dataTable( {
		    "bProcessing": true,
//		    "bStateSave": true,
		    "bServerSide": true,
			"oLanguage": {
            	"sSearch": "Search for shortcode:"
        	},
			"pagingType": "full",
			"sAjaxDataProp": "aaData",
		    "bLengthChange": false,
		    "sAjaxSource": "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=GetRequestNotPendingForDatatable&returnformat=plain&queryformat=column",
		    "aoColumns": [
				{"sName": 'ShortCode', "sTitle": 'Short Code', "sWidth": '25%',"bSortable": false},
				{"sName": 'Type', "sTitle": 'Type', "sWidth": '30%',"bSortable": false},
				{"sName": 'Status', "sTitle": 'Status', "sWidth": '25%',"bSortable": false},
				{"sName": 'Options', "sTitle": 'Options', "sWidth": '20%',"bSortable": false, "sClass": "AlignRight"}
			],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
				 });
	        },
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				//decode html that passing from server
				$('td:eq(3)', nRow).html(htmlDecode(aData[3]));
	            $('td', nRow).attr('nowrap','nowrap');
	            return nRow;
	       	},
		 	"fnInitComplete": function(oSettings, json){
				$("#SCApprovedAndRejected_filter").remove();
				var oTable =  $('#SCApprovedAndRejected').dataTable();
				$("#tfSCApprovedAndRejected th").each( function ( i ) {
					if (i == 2||i==1) {
						this.innerHTML = fnCreateSelect( oTable.fnGetColumnData(i) );
						$('select', this).change( function () {
			            	oTable.fnFilter( $(this).val(), i );
			        	} );
					}
					<!---if(i==0){
						this.innerHTML = '<input type="text" value="" id="txtSCPending" style="margin-bottom: 0px;"></input>';
						$("#txtSCApprovedAndRejected").keyup( function () {
					        /* Filter on the column (the index) of this element */
					        oTable.fnFilter( this.value, 0 );
					    } );
					}--->
			    } );
			}
	    });
	}
	
	
	
	
	function InitDatatableForGettingRequestPending(){
		var result ;
		result = $('#SCPending').dataTable( {
		    "bProcessing": true,
		    "bStateSave": true,
		    "bServerSide": true,
			"oLanguage": {
            	"sSearch": "Search for shortcode:"
        	},
			"pagingType": "full",
			"sAjaxDataProp": "aaData",
		    "bLengthChange": false,
			<!---"bFilter": false,--->
		    "sAjaxSource": "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=GetRequestPendingForDatatable&returnformat=plain&queryformat=column",
		    "aoColumns": [
				{"sName": 'ShortCode', "sTitle": 'Short Code', "sWidth": '25%',"bSortable": false,"sSearchName":"sSearch_ShortCode"},
				{"sName": 'Type', "sTitle": 'Type', "sWidth": '30%',"bSortable": false},
				{"sName": 'Status', "sTitle": 'Status', "sWidth": '25%',"bSortable": false},
				{"sName": 'Options', "sTitle": 'Options', "sWidth": '20%',"bSortable": false, "sClass": "AlignRight"}
			],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
				 });
	        },
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				//decode html that passing from server
				$('td:eq(3)', nRow).html(htmlDecode(aData[3]));
	            $('td', nRow).attr('nowrap','nowrap');
	            return nRow;
	       	},
			"fnInitComplete": function(oSettings, json){
				$("#SCPending_filter").remove();
				var oTable =  $('#SCPending').dataTable();
				$("#tfSCPending th").each( function ( i ) {
					if (i==1) {
						this.innerHTML = fnCreateSelect( oTable.fnGetColumnData(i) );
						$('select', this).change( function () {
			            	oTable.fnFilter( $(this).val(), i );
			        	} );
					}
					<!---if(i==0){
						this.innerHTML = '<input type="text" value="" id="txtSCPending" style="margin-bottom: 0px;"></input>';
						$("#txtSCPending").keyup( function () {
					        /* Filter on the column (the index) of this element */
					        oTable.fnFilter( this.value, 0 );
					    } );
					}--->
			    } );
			}
	    });
		return result;
	}
</script>	


<script type="text/javascript">
		(function($) {
		/*
		 * Function: fnGetColumnData
		 * Purpose:  Return an array of table values from a particular column.
		 * Returns:  array string: 1d data array 
		 * Inputs:   object:oSettings - dataTable settings object. This is always the last argument past to the function
		 *           int:iColumn - the id of the column to extract the data from
		 *           bool:bUnique - optional - if set to false duplicated values are not filtered out
		 *           bool:bFiltered - optional - if set to false all the table data is used (not only the filtered)
		 *           bool:bIgnoreEmpty - optional - if set to false empty values are not filtered from the result array
		 * Author:   Benedikt Forchhammer <b.forchhammer /AT\ mind2.de>
		 */
		$.fn.dataTableExt.oApi.fnGetColumnData = function ( oSettings, iColumn, bUnique, bFiltered, bIgnoreEmpty ) {
		    // check that we have a column id
		    if ( typeof iColumn == "undefined" ) return new Array();
		     
		    // by default we only want unique data
		    if ( typeof bUnique == "undefined" ) bUnique = true;
		     
		    // by default we do want to only look at filtered data
		    if ( typeof bFiltered == "undefined" ) bFiltered = true;
		     
		    // by default we do not want to include empty values
		    if ( typeof bIgnoreEmpty == "undefined" ) bIgnoreEmpty = true;
		     
		    // list of rows which we're going to loop through
		    var aiRows;
		    // use only filtered rows
		    if (bFiltered == true) aiRows = oSettings.aiDisplay; 
		    // use all rows
		    else aiRows = oSettings.aiDisplayMaster; // all row numbers
		    // set up data array    
		    var asResultData = new Array();
		    for (var i=0,c=aiRows.length; i<c; i++) {
		        iRow = aiRows[i];
		        var aData = this.fnGetData(iRow);
		        var sValue = aData[iColumn];
		         
		        // ignore empty values?
		        if (bIgnoreEmpty == true && sValue.length == 0) continue;
		 
		        // ignore unique values?
		        else if (bUnique == true && jQuery.inArray(sValue, asResultData) > -1) continue;
		         
		        // else push the value onto the result data array
		        else asResultData.push(sValue);
		    }
	     
		    return asResultData;
		}}(jQuery));
	 
	function fnCreateSelect( aData )
	{
	    var r='<select><option value=""></option>', i, iLen=aData.length;
	    for ( i=0 ; i<iLen ; i++ )
	    {
	        r += '<option value="'+aData[i]+'">'+aData[i]+'</option>';
	    }
	    return r+'</select>';
	}
			
	function ShowHideIndividual(shortcodeId, btnObject){
		var displayStatus = $("#tbl"+shortcodeId+"_filter label").css("display");
		
		if(displayStatus == 'none'){
			$("#tbl"+shortcodeId+"_filter label").show();
			btnObject.innerHTML = "<img src='<cfoutput>#rootUrl#/#publicPath#/css/mcid/images/group-collapse.gif</cfoutput>'></img>";
		}
		else{
			$("#tbl"+shortcodeId+"_filter label").hide();
			btnObject.innerHTML = "<img src='<cfoutput>#rootUrl#/#publicPath#/css/mcid/images/group-expand.gif</cfoutput>'></img>";
		}
	}
</script>