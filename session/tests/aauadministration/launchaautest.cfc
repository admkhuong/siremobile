﻿<cfcomponent displayname="mxunit.framework.MyComponentTest"  extends="mxunit.framework.TestCase" >
	<cfinclude template="../../../public/paths.cfm" >
	<cffunction name="TestInviteAgent" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfquery name="GetBatchIdByUser" datasource="#Session.DBSourceEBM#">
            select BatchId_bi as id from simpleobjects.batch where UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.userid#"> 
			limit 1
        </cfquery>
		<cfset relVal = distributionObject.InviteAgent("test@gmail.com",#GetBatchIdByUser.id#)>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,#relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
		<cfquery name="DeleteHauInvitationCode" datasource="#Session.DBSourceEBM#" result="DeleteResult">
            DELETE FROM simpleobjects.hauinvitationcode
                WHERE HauInvitationCode_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#relVal.invatationCode#">
        </cfquery>
        <cfscript>
			assertequals(1, DeleteResult.RecordCount,"Happens error!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetHAUSetting" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.GetHAUSetting()>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestInsertHAUSetting" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.InsertHAUSetting(true,true,true,3,true,true,true)>
		<cfscript>
			assertequals(1,relVal.RXRESULTCODE, "Happens error!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestUpdateHAUSetting" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.UpdateHAUSetting(1,true,true,true,3,true,true,true)>
		<cfscript>
			assertequals(1,relVal.RXRESULTCODE, "Happens error!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetAgentList" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.GetAgentList(1,20)>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE, "Happens error!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetSessionStatistic" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.GetSessionStatistic("today")>
		<cfscript>
			assertequals(1,relVal.RXRESULTCODE, "Happens error!");
		</cfscript>
		<cfset relVal = distributionObject.GetSessionStatistic("yesterday")>
		<cfscript>
			assertequals(1,relVal.RXRESULTCODE, "Happens error!");
		</cfscript>
		<cfset relVal = distributionObject.GetSessionStatistic("7days")>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE, "Happens error!");
		</cfscript>
		<cfset relVal = distributionObject.GetSessionStatistic("30days")>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetActiveAgentList" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.GetActiveAgentList(1,20)>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetActiveSessionList" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.GetActiveSessionList(1,20,282)>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetAvailableCapaignCode" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.GetAvailableCapaignCode()>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetQuestionList" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.GetQuestionList(1,20)>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestAddQuestion" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.AddQuestion(1,"test question","test answer")>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
		<cfquery name="GetRecentInsertedId" datasource="#Session.DBSourceEBM#">
			select last_insert_id() as lastId
		</cfquery>
		<cfquery name="DeleteQuestion" datasource="#Session.DBSourceEBM#" result="DeleteResult">
            DELETE FROM simpleobjects.question
                WHERE QuestionId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#GetRecentInsertedId.lastId#">
        </cfquery>
        <cfscript>
			assertequals(1, DeleteResult.RecordCount,"Happens error!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestDeleteQuestion" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.AddQuestion(1,"test question","test answer")>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
		<cfquery name="GetRecentInsertedId" datasource="#Session.DBSourceEBM#">
			select last_insert_id() as lastId
		</cfquery>
		<cfset relVal = distributionObject.DeleteQuestion(GetRecentInsertedId.lastId)>
        <cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
	</cffunction>
	
	<cffunction name="TestEditQuestion" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.AddQuestion(1,"test question","test answer")>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
		<cfquery name="GetRecentInsertedId" datasource="#Session.DBSourceEBM#">
			select last_insert_id() as lastId
		</cfquery>
		<cfset relVal = distributionObject.EditQuestion(#GetRecentInsertedId.lastId#,1,"test edit question","test edit answer")>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
		<cfset relVal = distributionObject.DeleteQuestion(GetRecentInsertedId.lastId)>
        <cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
	</cffunction>
	
	<cffunction name="TestSelectQuestionById" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.SelectQuestionById(1)>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetCategoryList" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.GetCategoryList(1,20)>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
	</cffunction>
	
	<cffunction name="TestAddCategory" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.AddCategory("test category","test category description")>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
		<cfquery name="GetRecentInsertedId" datasource="#Session.DBSourceEBM#">
			select last_insert_id() as lastId
		</cfquery>
		<cfquery name="DeleteCategory" datasource="#Session.DBSourceEBM#" result="DeleteResult">
            DELETE FROM simpleobjects.haucategory
                WHERE haucategoryId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#GetRecentInsertedId.lastId#">
        </cfquery>
        <cfscript>
			assertequals(1, DeleteResult.RecordCount,"Happens error!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetCategoryById" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.GetCategoryById(1)>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
	</cffunction>
	
	<cffunction name="TestEditCategory" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.AddCategory("test category","test category description")>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
		<cfquery name="GetRecentInsertedId" datasource="#Session.DBSourceEBM#">
			select last_insert_id() as lastId
		</cfquery>
		<cfset relVal = distributionObject.EditCategory(#GetRecentInsertedId.lastId#,"test category","test category description")>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
		<cfquery name="DeleteCategory" datasource="#Session.DBSourceEBM#" result="DeleteResult">
            DELETE FROM simpleobjects.haucategory
                WHERE haucategoryId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#GetRecentInsertedId.lastId#">
        </cfquery>
        <cfscript>
			assertequals(1, DeleteResult.RecordCount,"Happens error!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestDeleteCategory" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.AddCategory("test category","test category description")>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
		<cfquery name="GetRecentInsertedId" datasource="#Session.DBSourceEBM#">
			select last_insert_id() as lastId
		</cfquery>
		<cfset relVal = distributionObject.DeleteCategory(GetRecentInsertedId.lastId)>
        <cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetCategoryListForAutoComplete" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.GetCategoryListForAutoComplete()>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
	</cffunction>
	
	<cffunction name="TestSearchQuestionByKeyword" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.SearchQuestionByKeyword(1,20,' ')>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetCannedList" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.GetCannedList(1,20)>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
	</cffunction>
	
	<cffunction name="TestAddCanned" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.AddCanned(1144, "test canned","test canned description")>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
		<cfquery name="GetRecentInsertedId" datasource="#Session.DBSourceEBM#">
			select last_insert_id() as lastId
		</cfquery>
		<cfquery name="DeleteCanned" datasource="#Session.DBSourceEBM#" result="DeleteResult">
            DELETE FROM simpleobjects.cannedresponse
                WHERE CANNEDRESPONSEID_INT = <cfqueryparam cfsqltype="cf_sql_integer" value="#GetRecentInsertedId.lastId#">
        </cfquery>
        <cfscript>
			assertequals(1, DeleteResult.RecordCount,"Happens error!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestDeleteCanned" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.AddCanned(1144, "test canned","test canned description")>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
		<cfquery name="GetRecentInsertedId" datasource="#Session.DBSourceEBM#">
			select last_insert_id() as lastId
		</cfquery>
		<cfset relVal = distributionObject.DeleteCanned(#GetRecentInsertedId.lastId#)>
        <cfscript>
			assertequals(1, DeleteResult.RecordCount,"Happens error!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetCannedById" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.GetCannedById(1)>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
	</cffunction>
	
	<cffunction name="TestEditCanned" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.AddCanned(1144, "test canned","test canned description")>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
		<cfquery name="GetRecentInsertedId" datasource="#Session.DBSourceEBM#">
			select last_insert_id() as lastId
		</cfquery>
		<cfset relVal = distributionObject.EditCanned(1144,#GetRecentInsertedId.lastId#,"test edit canned","test edit canned description")>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
		<cfquery name="DeleteCanned" datasource="#Session.DBSourceEBM#" result="DeleteResult">
            DELETE FROM simpleobjects.cannedresponse
                WHERE cannedresponseId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#GetRecentInsertedId.lastId#">
        </cfquery>
        <cfscript>
			assertequals(1, DeleteResult.RecordCount,"Happens error!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestMoveCategory" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.MoveCategory(1)>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetSessionById" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.GetSessionById(1)>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
	</cffunction>
	
	<cffunction name="TestUpdateSesionLock" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.UpdateSesionLock(1,1)>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
		<cfset relVal = distributionObject.UpdateSesionLock(1,0)>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetChatContent" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.GetChatContent("1","1","1","1")>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
	</cffunction>
	
	<cffunction name="TestCloseSession" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.CloseSession(1)>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetCannedListForSessionDetail" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.GetCannedListForSessionDetail(1)>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
	</cffunction>
	
	<cffunction name="TestUpdateOwnerSession" access="public" returntype="void">
		<cfquery name="AddDummyDataForSession" datasource="#Session.DBSourceEBM#">
			Insert into simplexresults.conversationmessage
			(
				ShortCode_vch,
				OwnerId_int,
				ContactString_vch,
				StartTime_dt,
				EndTime_dt,
				SessionCode_vch,
				SessionLocked_ti,
				BatchId_bi
			)
			values
			(	
				'1244', 
				1,
				'447900000000' ,
				'2013-07-12 11:48:02' ,
				'2013-10-22 09:14:52' , 
				'123', 
				0, 
				1
			);		
		</cfquery>
		<cfquery name="GetRecentInsertedId" datasource="#Session.DBSourceEBM#">
			select last_insert_id() as lastId
		</cfquery>
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.UpdateOwnerSession(GetRecentInsertedId.lastId,session.userid)>
		<cfscript>
			assertequals(1, relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
		<cfquery name="delete" datasource="#Session.DBSourceEBM#" result="DeleteResult">
			delete from simplexresults.conversationmessage
			where SessionId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#GetRecentInsertedId.lastId#">			
		</cfquery>
		<cfscript>
			assertequals(1, DeleteResult.recordCount ,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
	</cffunction>
	
	<cffunction name="TestRevokeUser" access="public" >
		<cfset startTime = DateFormat(Now(),"yyyy-MM-dd")&" "&TimeFormat(Now(),"HH:mm:ss")>
		<cfquery name="InsertHauInviteCode" datasource="#Session.DBSourceEBM#">
			Insert into simpleobjects.hauinvitationcode
			(
				HauInvitationCode_vch,
				Created_dt,
				InviteUserId_int,
				EmailAddress_vch,
				Status_ti,
				BatchId_bi,
				UserTypeId_ti
			) values
			(
				'test hau invite code',
				<cfqueryparam cfsqltype="cf_sql_timestamp" value="#startTime#"/>,
				1,
				"test@gmail.com",
				1,
				1,
				1
			)
		</cfquery>
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.RevokeUser(1,'test@gmail.com')>
		<cfset result = -1>
		<cfif relVal.RXRESULTCODE EQ 0 || relVal.RXRESULTCODE EQ 1>
			<cfset result = 1>
		</cfif>
		<cfscript>
			assertequals(1,result,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
		<cfquery name="delete" datasource="#Session.DBSourceEBM#" result="DeleteResult">
			delete from simpleobjects.hauinvitationcode
			where HauInvitationCode_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="test hau invite code">	
			and Created_dt = <cfqueryparam cfsqltype="cf_sql_timestamp" value="#startTime#">		
		</cfquery>
		<cfscript>
			assertequals(1, DeleteResult.recordCount ,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
	</cffunction>
	
	<cffunction name="TestgetKeywordAndShortCodeByUser" access="public" >
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.getKeywordAndShortCodeByUser('namdvpro@gmail.com',"0GFM-R3KW-Q22Q-VSYT")>
		<cfscript>
			assertequals(1,relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
	</cffunction>
	
	<cffunction name="TestgetBatchByHauinvitationCode" access="public" >
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.getBatchByHauinvitationCode("0GFM-R3KW-Q22Q-VSYT")>
		<cfscript>
			assertequals(1,relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
	</cffunction>

	<cffunction name="TestEditCampaignCodeOfAgent" access="public" >
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.aau")>
		<cfset relVal = distributionObject.EditCampaignCodeOfAgent("SQVP-885E-NBCT-YKRA","[1174]")>
		<cfscript>
			assertequals(1,relVal.RXRESULTCODE,"Happens error! "& #relVal.MESSAGE# & #relVal.ERRMESSAGE#);
		</cfscript>
	</cffunction>
</cfcomponent>