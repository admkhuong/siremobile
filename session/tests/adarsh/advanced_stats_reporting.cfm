
<!--- Nameing convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Display_open_results" access="remote" output="false" returntype="any" hint="Display total Open Count Percentage">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">

	<!--- <cfset var GetEMSResults = ''/> --->
    
	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">

 	<cfquery name="GetOpenResults" datasource="#Session.DBSourceEBM#">
       SELECT  
	     Count(TableId_bi) AS Total_Events
            ,Sum(CASE WHEN 'OPEN'       =	event_vch THEN 100 END) / Count(*) AS OPENS
	        ,Sum(CASE WHEN 'BOUNCE' 	= 	event_vch THEN 100 END) / Count(*) AS BOUNCE
            ,Sum(CASE WHEN 'DEFERRED'   = 	event_vch THEN 100 END) / Count(*) AS DEFERRED_EVENT
            ,Sum(CASE WHEN 'DROPPED' 	= 	event_vch THEN 100 END) / Count(*) AS DROPPED
            ,Sum(CASE WHEN 'DELIVERED'  = 	event_vch THEN 100 END) / Count(*) AS DELIVERED
            ,Sum(CASE WHEN 'PROCESSED'  = 	event_vch THEN 100 END) / Count(*) AS PROCESSED
            ,Sum(CASE WHEN 'SPAMREPORT' = 	event_vch THEN 100 END) / Count(*) AS SPAMREPORT 
    FROM    
	        simplexresults.contactresults_email                             
        WHERE        
             batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
          
        AND
            Scheduled_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
        AND
            Scheduled_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">  
    </cfquery>
        
	<cfsavecontent variable="outPutToDisplay">
       	<cfoutput>

			<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
			<div style="width:100%; height:100%; position:relative;" class="EBMDashCounterWrapper content-EMS">
              
               <div class="head-EMS"><div class="DashObjHeaderText">Open Percentage</div></div>
          
                <!--- Keep table auto-scroll in managed area--->
               <!---  style="height:100px; line-height:100px;"  <div class="EBMDashCounterWrapper">--->
                    <div class="Absolute-Center">            
                        <h1>#GetOpenResults.OPENS#</h1>
                        <h2>Opens</h2>
                    </div>             
                <!---</div>--->
              
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
 
    <cfreturn outPutToDisplay />
</cffunction> 




<!--- Nameing convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Display_spam_results" access="remote" output="false" returntype="any" hint="Display total Spam Report Percentage">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">

	<!--- <cfset var GetEMSResults = ''/> --->
    
	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">

 	<cfquery name="GetSpamResults" datasource="#Session.DBSourceEBM#">
       SELECT  
	     Count(TableId_bi) AS Total_Events
            ,Sum(CASE WHEN 'OPEN'       =	event_vch THEN 100 END) / Count(*) AS OPENS
	        ,Sum(CASE WHEN 'BOUNCE' 	= 	event_vch THEN 100 END) / Count(*) AS BOUNCE
            ,Sum(CASE WHEN 'DEFERRED'   = 	event_vch THEN 100 END) / Count(*) AS DEFERRED_EVENT
            ,Sum(CASE WHEN 'DROPPED' 	= 	event_vch THEN 100 END) / Count(*) AS DROPPED
            ,Sum(CASE WHEN 'DELIVERED'  = 	event_vch THEN 100 END) / Count(*) AS DELIVERED
            ,Sum(CASE WHEN 'PROCESSED'  = 	event_vch THEN 100 END) / Count(*) AS PROCESSED
            ,Sum(CASE WHEN 'SPAMREPORT' = 	event_vch THEN 100 END) / Count(*) AS SPAMREPORT 
    FROM    
	        simplexresults.contactresults_email                             
        WHERE        
             batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
          
        AND
            Scheduled_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
        AND
            Scheduled_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">  
    </cfquery>
        
	<cfsavecontent variable="outPutToDisplay">
       	<cfoutput>

			<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
			<div style="width:100%; height:100%; position:relative;" class="EBMDashCounterWrapper content-EMS">
              
               <div class="head-EMS"><div class="DashObjHeaderText">Open Percentage</div></div>
          
                <!--- Keep table auto-scroll in managed area--->
               <!---  style="height:100px; line-height:100px;"  <div class="EBMDashCounterWrapper">--->
                    <div class="Absolute-Center">            
                        <h1>#GetSpamResults.OPENS#</h1>
                        <h2>Opens</h2>
                    </div>             
                <!---</div>--->
              
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
 
    <cfreturn outPutToDisplay />
</cffunction> 


<!--- Nameing convention for data table "TABLE" reports Display_NNNNN where NNNNN is the name of the Report --->
<cffunction name="Display_delivered_results" access="remote" output="false" returntype="any" hint="Display Delivered Percentage">
	<cfargument name="inpBatchIdList" required="yes" type="any">
	<cfargument name="inpStart" required="yes" type="string" >
	<cfargument name="inpEnd" required="yes" type="string">

	<!--- <cfset var GetEMSResults = ''/> --->
    
	<cfset arguments.inpStart = "#dateformat(inpStart,'yyyy-mm-dd')#" & " " & "00:00:00">
    <cfset arguments.inpEnd = "#dateformat(inpEnd,'yyyy-mm-dd')#" & " " & "23:59:59">

 	<cfquery name="GetDeliveredResults" datasource="#Session.DBSourceEBM#">
       SELECT  
	     Count(TableId_bi) AS Total_Events
            ,Sum(CASE WHEN 'OPEN'       =	event_vch THEN 100 END) / Count(*) AS OPENS
	        ,Sum(CASE WHEN 'BOUNCE' 	= 	event_vch THEN 100 END) / Count(*) AS BOUNCE
            ,Sum(CASE WHEN 'DEFERRED'   = 	event_vch THEN 100 END) / Count(*) AS DEFERRED_EVENT
            ,Sum(CASE WHEN 'DROPPED' 	= 	event_vch THEN 100 END) / Count(*) AS DROPPED
            ,Sum(CASE WHEN 'DELIVERED'  = 	event_vch THEN 100 END) / Count(*) AS DELIVERED
            ,Sum(CASE WHEN 'PROCESSED'  = 	event_vch THEN 100 END) / Count(*) AS PROCESSED
            ,Sum(CASE WHEN 'SPAMREPORT' = 	event_vch THEN 100 END) / Count(*) AS SPAMREPORT 
    FROM    
	        simplexresults.contactresults_email                             
        WHERE        
             batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />)
          
        AND
            Scheduled_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#">
        AND
            Scheduled_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">  
    </cfquery>
        
	<cfsavecontent variable="outPutToDisplay">
       	<cfoutput>

			<!--- This outer wrapper is two parents up from ReportDataTable - this is important for daownload links to be initialized--->
			<div style="width:100%; height:100%; position:relative;" class="EBMDashCounterWrapper content-EMS">
              
               <div class="head-EMS"><div class="DashObjHeaderText">Delivered Percentage</div></div>
          
                <!--- Keep table auto-scroll in managed area--->
               <!---  style="height:100px; line-height:100px;"  <div class="EBMDashCounterWrapper">--->
                    <div class="Absolute-Center">            
                        <h1>#GetDeliveredResults.Delivered#</h1>
                        <h2>Opens</h2>
                    </div>             
                <!---</div>--->
              
            </div>
                       
		</cfoutput>                 
 	</cfsavecontent>
 
    <cfreturn outPutToDisplay />
</cffunction> 





