<cfparam name="INPBATCHID" default="0">

<!---check permission--->
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkBatchPermissionByBatchId">
	<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfif NOT checkBatchPermissionByBatchId.havePermission >
	<cfset session.permissionError = checkBatchPermissionByBatchId.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#edit_Campaign_Title# #INPBATCHID#">
</cfinvoke>
<div id="campaignStyle">
	<style>
		<cfoutput>
			@import url('#rootUrl#/#PublicPath#/css/mb/jquery-ui-1.8.7.custom.css'); 
			@import url('#rootUrl#/#PublicPath#/css/mcid/rxt/edit_rxt.css'); 
			@import url('#rootUrl#/#PublicPath#/css/mcid/ruler.css'); 
			@import url('#rootUrl#/#PublicPath#/css/rxdsmenu.css');
			@import url('#rootUrl#/#PublicPath#/css/socialmedia.css');
			@import url('#rootUrl#/#PublicPath#/js/help/tipsy.css');
			<!--- @import url('#rootUrl#/#PublicPath#/css/rxform2.css'); --->
			<!--- @import url('#rootUrl#/#PublicPath#/js/jquery/css/jquery.multiselect.css'); --->
			@import url('#rootUrl#/#PublicPath#/js/jquery/css/jquery.jqgrid.css');
			@import url('#rootUrl#/#PublicPath#/css/mcid/css/print-preview.css');
		</cfoutput>
	</style>
</div>

<cfinvoke 
	component="#LocalSessionDotPath#.cfc.distribution"
	method="GetRunningCampaignStatus"
	returnvariable="runningStatusResult">     
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#"/>  
</cfinvoke> 

<script type="text/javascript">
	
	var isRunning =  '<cfoutput>#runningStatusResult.ISRUNNING#</cfoutput>';

	$(function() {
		$('#subTitleText').text('<cfoutput>#edit_Campaign_Title#</cfoutput>');
		$('#mainTitleText').text('<cfoutput>#Campaign_Title#</cfoutput>');
		
		var $items = $('#vtab>ul>li.MultiChannelVTab');
		$items.addClass('selected');
		if(isRunning === 'true'){
			$("#MC_Stage_MCIDgen").load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/dsp_statisticCampaign.cfm?inpbatchid=<cfoutput>#INPBATCHID#</cfoutput>', function() { 
				return false; 
			});
		} else{
			$("#MC_Stage_MCIDgen").load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/dsp_scriptbuilder?inpbatchid=<cfoutput>#INPBATCHID#</cfoutput>', function() { 
				return false; 
			});
		}	
	});
	
</script> 

<cfoutput>
	<div id="MC_Voice_Container" style="position:relative;">
	    <div id="MC_Stage_MCIDgen" class="MC_Content_Stage_Voice  ui-corner-all">
	    </div>
	</div>
	<div id="overlay" style="text-align: center">
		<img src="#rootUrl#/#PublicPath#/images/loading.gif" width="100" height="100" style="padding-top: 200px;">
	</div>
</cfoutput>