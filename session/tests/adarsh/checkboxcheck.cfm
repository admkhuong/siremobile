<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>

<ul class="list_permisstion">
    <span class="label_permission">
       <input type="checkbox" id="Reporting" class="grouping" />
        <label for="Reporting">Reporting</label>
    </span>
    <ul class="list_permisstion">
        <li>
            <input id="RC"type="checkbox"/>
            <label for="RC">Reporting Campaigns</label>
        </li>
        <li>
            <input id="RS"type="checkbox"/>
            <label for="RS">Reporting Surveys</label>
        </li>
        <li>
            <input id="RSMs"type="checkbox"/>
            <label for="RSMs">Reporting SmsMessages</label>
        </li>
    </ul>
    
    <ul class="list_permisstion">
        <span class="label_permission">
                  <input type="checkbox" id="Reporting1" class="grouping" />
            <label for="Reporting1">Email Group</label>
        </span>
        <ul class="list_permisstion">
            <li>
                <input id="R11"type="checkbox"/>
                <label for="R11">Reporting Email Pie Chart</label>
            </li>
            <li>
                <input id="R11"type="checkbox"/>
                <label for="R11">Reporting Events Bar Chart</label>
            </li>
            <li>
                <input id="R11"type="checkbox"/>
                <label for="R11">Reporting Events table</label>
            </li>
        </ul>
    </ul>
    
    
    <ul class="list_permisstion">
        <span class="label_permission">
                  <input type="checkbox" id="Reporting2" class="grouping" />
            <label for="Reporting2">SMS Group</label>
        </span>
        <ul class="list_permisstion">
            <li>
                <input id="R21"type="checkbox"/>
                <label for="R21">Reporting SMS MO Log</label>
            </li>
            <li>
                <input id="R21"type="checkbox"/>
                <label for="R21">Reporting SMS Session States</label>
            </li>
            <li>
                <input id="R21"type="checkbox"/>
                <label for="R21">Reporting Active IC By Keyword</label>
            </li>
        </ul>
    </ul>
</ul>
    
<script>

$(function() {
    $('.grouping').on('click', function() {
        parentCheckboxCheck(this)
    });
});


function parentCheckboxCheck(obj, childrenClass, parentId ){
    var el = $(obj);
    el.closest('.list_permisstion')
        .find(':checkbox')
        .not(this).each(function(){
            $(this).prop('checked', el.is(':checked'));
        });
};


</script>



</body>
</html>