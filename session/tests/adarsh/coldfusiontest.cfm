<cfscript>

	Permission_Fail = "You do not have permission to access this page";
	
	// time
	Today_Time = "Today";
	Last_7_Days = 'Last 7 days';
	Last_30_Days = 'Last 30 days';
	All_Time = 'All Time';
	
	//Modules 
	Campaign_Title = 'Campaigns';
	ABCampaign_Title = 'Campaigns';
	Survey_Title = 'Interactive Campaign';
	Contact_Title = 'Contacts';
	Marketing_Title = 'Marketings';
	UnitTest_Title = 'Unit Test';
	EMS = 'Emergency Messaging Messages';
	
	//Operators Campaign
	Create_Campaign_Title = 'Create Campaign';
	edit_Campaign_Title = 'Edit Campaign';
	Campaign_Schedule_Title = 'Campaign Schedule';
	Run_Campaign_Title = 'Run Campaign';
	Stop_Campaign_Title = 'Stop Campaign';
	Cancel_Campaign_Title = 'Cancel Campaign';
	delete_Campaign_Title = 'Delete Campaign';
	Caller_ID_Title = 'Caller ID';
	
	//Operators AB Campaign
	Create_ABCampaign_Title = 'Create AB Campaign';
	Manage_ABCampaign_Title = 'Manage AB Campaign';
	Run_ABCampaign_Title = 'Run ABCampaign';
	Stop_ABCampaign_Title = 'Stop ABCampaign';
	Cancel_ABCampaign_Title = 'Cancel ABCampaign';
	delete_ABCampaign_Title = 'Delete ABCampaign';
	ABCampaign_Text = 'AB Campaign';
	ABCampaign_Title = 'AB Campaign';
	
	//Operators Survey
//	Create_Survey_Title = 'Create Survey';
//	edit_Survey_Title = 'Edit Survey or Interactive Campaign';
//	Survey_Question_Title = 'Survey Question';
//	Launch_Survey_Title = 'Launch Survey';
//	View_Survey_Title = 'View Survey';
//	delete_Survey_Title = 'Delete Survey';
//	Add_Question_Title = 'Add Question';
//	edit_Question_Title = 'Edit Question';
//	delete_Question_Title = 'Delete Question';
//	Survey_Builder_Title = 'Survey or Interactive Campaign Builder';
//	Survey_Builder_Sub_Title = 'Build Your Survey or Interactive Campiagn Flow';
		
	Create_Survey_Title = 'Create Interaction Campaign';
	edit_Survey_Title = 'Edit Interactive Campaign';
	Survey_Question_Title = 'Interaction';
	Launch_Survey_Title = 'Launch Interactive Campaign';
	View_Survey_Title = 'View Ineractive Campaign';
	delete_Survey_Title = 'Delete Interactive Campaign';
	Add_Question_Title = 'Add Interaction';
	edit_Question_Title = 'Edit Interaction';
	delete_Question_Title = 'Delete Interaction';
	Survey_Builder_Title = 'Interactive Campaign Builder';
	Survey_Builder_Sub_Title = 'Build Your Interactive Campaign Flow';
	
	
	//Operators Contacts
	Contact_List_Title = 'Contact List';
	Add_Contact_Title = 'Add Contact';
	Add_Bulk_Contact_Title = 'Add Bulk Contact';
	View_Contact_Details_Title = 'View Contact Details';
	edit_Contact_Details_Title = 'Edit Contact Details';
	delete_Contact_Title = 'Delete Contact';
	MangecustomdataFields = 'Manage Custom Data Fields';
	OptionMangecustomdataFields = 'View, Rename, Add, or Delete Custom Data Fields';
	
	//Operators Contact Group
	Contact_Group_Title = 'Contact Group';
	Add_Contact_Group_Title = 'Add Contact Group';
	Add_Bulk_Groups_Title = 'Add Bulk Groups';
	View_Group_Details_Title = 'View Groups Details';
	edit_Group_Details_Title = 'Edit Groups Details';
	delete_Group_Title = 'Delete Group';
	Add_Contacts_To_Group_Title = 'Add Contacts to Group';
	delete_Contacts_From_Group_Title = 'Delete Contacts from Group';
	
	Contact_Group_Text = 'Contact Groups';
	
	Bulk_Upload_Text = 'Bulk Upload';
	Create_Definition_Text = 'Create Definition';
	edit_Definition_Text = 'Edit Definition';
	
	//Operator reporting
	Reporting_Title = 'Reporting';
	Reporting_Campaigns_Title = 'Reporting Campaigns';
	Reporting_Surveys_Title = 'Reporting Surveys';
	Reporting_CPP_Title = 'Reporting Customer Preference Portal';
	Reporting_SmsMessagings_Title = 'Reporting SmsMessagings';
	Reporting_Dial_Title = 'Reporting Dials';
	Reporting_CallResults_Title = 'Reporting Call Results';
	Reporting_States_Title = 'Reporting States Results';
	Reporting_SMS_Title = 'Reporting SMS' ;
	Reporting_Email_Title = 'Reporting Email' ;
	Reporting_Test_Title = 'Reporting TestNew';
	
	// Operators Reporting Email Added By AK
	Reporting_Group_Title = 'Email Group';
	Reporting_Email_Pie_Chart_Title = 'Reporting Email Pie Chart';
	Reporting_Events_Bar_Graph_Title = 'Reporting Events Bar Graph';
	Reporting_Events_Results_Table_Title = 'Reporting Events Table';
	Reporting_Email_History_Lookup_Title = 'Reporting Email History Lookup';
	Reporting_Client_Stats_Title = 'Reporting Client Stats';
	Reporting_Unique_Client_Stats_Title = 'Reporting Unique Client Stats';
	Reporting_Open_Devices_Title = 'Reporting Open Devices';
	Reporting_Unique_Devices_Title = 'Reporting Unique Devices';
	Reporting_ISPS_Results_Title = 'Reporting ISPS Results';
	
	//Operators Reporting SMS Added By AK
	
	Reporting_SMS_Group_Title = 'SMS Group';
	Reporting_SMSMO_Log_Title = 'Reporting SMS MO Log';
	Reporting_SMSSession_States_Title = 'Reporting SMS Session States';
	Reporting_ActiveICBy_Keyword_Title = 'Reporting Active IC By Keyword';
	Reporting_ActiveIC_Details_Title = 'Reporting Active IC Details';
	Reporting_ActiveIC_Counts_Title = 'Reporting Active IC Counts';
	Reporting_MTDelivery_Results_Title = 'Reporting MC Delivery Results';
	Reporting_MO_Responses_Title = 'Reporting MO Responses';
	
	// Operators Reporting States Added By AK
	Reporting_States_Group_Title = 'States Group';
	Reporting_CallResults_ByState_Title = 'Reporting Call Results By State';
	Reporting_AvgCallTime_ByState_Title = 'Reporting Avg Call Time By State';
	Reporting_TotalCalls_ByState_Title = 'Reporting Total Calls By State';
	
	//Operators Call Results Added By AK
	
	Reporting_CallResults_Group_Title = 'Call Results Group';
	Reporting_CallResults_Title = 'Reporting Call Results';
	Reporting_AvgCallTime_Title = 'Reporting Avg Call Time';
	Reporting_CallResultsPerHour_Title = 'Reporting Call Results Per Hour';
	Reporting_LiveCallsPerHour_Title = 'Reporting Live Calls Per Hour';
	Reporting_MachineCallsPerHour_Title = 'Reporting Machine Calls Per Hour';
	Reporting_AvgCallTimeBy_CallResult_Title = 'Reporting Avg Call Time By Call Result';
	Reporting_FinalCallResults_Basic_Title = 'Reporting Call Results Basic';
	Reporting_DialHistoryLookup_Title = 'Reporting Dial History Lookup';
	
	
	
	
	//Operator CPP
	Cpp_Title = 'Customer Preference Portal';
	Cpp_Create_Title = 'Create Customer Preference Portal';
	Cpp_Preview_Title = 'Preview Customer Preference Portal';
	Cpp_edit_Title = 'Edit Customer Preference Portal';
	Cpp_delete_Title = 'Delete Customer Preference Portal';
	Cpp_Api_Access_Text = 'API Access';
	Cpp_IFrame_Text = 'IFrame';
	Cpp_Agent_Title = 'Agent Access';
	
	Create_Contact_Title = 'Create Contact';
	Create_Group_Title = 'Create Group';	
	Assign_Contacts_To_Group_Title = 'Assign Contacts to Group';
	Remove_Contacts_From_Group_Title = 'Remove Contacts from Group';
	Upload_File_Contact_Title = 'Upload Contacts From File';
	
	
	//Operators SMS Campaigns 
	SMS_Campaigns_Management_Title = 'SMS Campaigns Mangement'; 
	Short_Code_Add_Title = 'Add Short Code';
	Short_Code_Remove_Title = 'Remove Short Code';
	Keyword_Action_Title = 'Action Keyword';
		
	
	// Developers
	Developers_Title = 'Developers';
	Developers_Help_Title = 'Developers Help';
	DEVELOPERS_SOMTHING_ELSE_TITLE = 'Somthing Else';
	
	//EMS
	EMS_Title = "Emergency Messaging Messages";
	EMS_Voice_Title  = 'EMS Voice';
	EMS_SMS_Title  = "EMS SMS";
	EMS_Email_Title  = "EMS Email";
	
	Home_Title ='Home';
	Contact_Feeds_Title ='Data Feeds';
	Survey_Web_Link_Title = 'Survey Web Link';
	Survey_Email_Title = 'Survey Email';
	Survey_Website_Title = 'Survey Website';
	Survey_Facebook_Title = 'Survey Facebook';
	
	Cpp_Build_Portal_Title = 'Build Portal';
	Cpp_Launch_Title = 'Customer Preference Portal Launch';
	
	Cpp_Api_Access_Title = 'Api Access';
	LifeCycle_Title ='Life Cycle Events';
	LifeCycle_View_Title = 'View Life Cycle';
	LifeCycle_edit_Title = 'Edit Life Cycle';
	LifeCycle_Create_Title = 'Create Life Cycle';
	Reporting_Campaign_Detail_Title = 'Reporting Campaign Details';
	Reporting_Campaign_Response_Title = 'Reporting Campaign Responses';
	Reporting_Campaign_Error_Title = 'Reporting Campaign Errors';
	Reporting_Survey_Response_Title = 'Reporting Survey Responses';
	Reporting_CPP_Title = 'Reporting Customer Preference Portal';
	Reporting_CPP_Detail_Title = 'Reporting Customer Preference Portal Details';
	Reporting_CPP_Responses_Title = 'Reporting Customer Preference Portal Responses';
	Reporting_CPP_Errors_Title = 'Reporting Customer Preference Portal Errors';
	
	Reporting_CPP_Text = 'Customer Preference Portal';
	Reporting_Campaigns_Text = 'Campaigns';
	Reporting_Surveys_Text = 'Surveys';
	Reporting_Dial_Text = 'Phone Calls';
	
	Reporting_Detail_Text = 'Details';
	Reporting_Response_Text = 'Responses';
	Reporting_Error_Text = 'Errors';
	
	Administrator_Title = 'Administration';
	Admin_Profile_Title = 'Account Details';
	Admin_Buy_Credits_Title = 'Buy Credits';
	Admin_Transaction_Title = 'Transactions';
	Admin_Reports_Views_Title = 'Report Viewer';
	Admin_Social_media_Title = 'Social media';
	Admin_Users_Management_Title = 'User Management';
	Admin_User_Logs_Title = 'Show Logs';
	Admin_User_Permission_Title = 'Permission';
	Admin_Companies_Management_Title = 'Company Management';
	Admin_Company_User_Title = 'User Management';
	Admin_Company_Logs_Title ='Show Logs';
	Admin_Security_Credentials_Title = 'Security Credentials';
	Admin_Companies_Information_Title = 'Company Information';
	Admin_Companies_Information_Text = 'Information';
	
	Help_Home_Title = 'Help Center';
	
	delete_Text ='Delete';
	edit_Text ='Edit';
	
	ENABLE_PERMISSION = true;
	HAVE_NOT_PERMISSION = 'You do not have permission to access this functionality';
	
</cfscript>

<cfset Campaigns_List = [
	Create_Campaign_Title,
	edit_Campaign_Title, 
	Campaign_Schedule_Title,
	Run_Campaign_Title,
	Stop_Campaign_Title,
	Cancel_Campaign_Title,
	Caller_ID_Title,
	delete_Campaign_Title
]>

<cfset ABCampaigns_List = [
	Create_ABCampaign_Title,
	Manage_ABCampaign_Title, 
	Run_ABCampaign_Title,
	Stop_ABCampaign_Title,
	Cancel_ABCampaign_Title,
	delete_ABCampaign_Title
]>

<cfset Surveys_list = [
	Create_Survey_Title,
	edit_Survey_Title,
	Survey_Question_Title,
	Launch_Survey_Title,
	View_Survey_Title,
	delete_Survey_Title,
	Add_Question_Title,
	edit_Question_Title,
	edit_Survey_Title,
	delete_Question_Title,
	Survey_Builder_Title,
	Survey_Builder_Sub_Title
]>

<cfset CPP_list =[
	Cpp_Create_Title,
	Cpp_edit_Title,
	Cpp_delete_Title,
	Cpp_Agent_Title
] >

<cfset SMS_Campaigns_list =[
	Short_Code_Add_Title,
	Short_Code_Remove_Title,
	Keyword_Action_Title
] >

<cfset Contacts_Parent_List =[
	Contact_List_Title,
	Contact_Group_Title
]>

<cfset Contacts_List = [
	Contact_List_Title,
	Add_Contact_Title,
	Add_Bulk_Contact_Title,
	View_Contact_Details_Title,
	edit_Contact_Details_Title,
	delete_Contact_Title,
	MangecustomdataFields,
	OptionMangecustomdataFields
]>

<cfset Contact_Group_List =[
	Add_Contact_Group_Title,
	Add_Bulk_Groups_Title,
	View_Group_Details_Title,
	edit_Group_Details_Title,
	delete_Group_Title,
	Add_Contacts_To_Group_Title,
	delete_Contacts_From_Group_Title	
]>

<cfset Reporting_Parent_List =[
	Reporting_Campaigns_Title,
	Reporting_Group_Title,
	Reporting_SMS_Group_Title,
	Reporting_States_Group_Title,
	Reporting_CallResults_Group_Title
]> 

<cfset Reporting_List = [
	Reporting_Campaigns_Title,
	Reporting_Surveys_Title,
	Reporting_SmsMessagings_Title,
	Reporting_Dial_Title,
	Reporting_CallResults_Title,
	Reporting_States_Title,
	Reporting_SMS_Title,
	Reporting_Email_Title,
	Reporting_Test_Title
		
]>

<cfset Reporting_Group_List =[
    <!--- Reporting_Group_Title, --->
	Reporting_Email_Pie_Chart_Title,
	Reporting_Events_Bar_Graph_Title,
	Reporting_Events_Results_Table_Title,
	Reporting_Email_History_Lookup_Title,
	Reporting_Client_Stats_Title,
	Reporting_Unique_Client_Stats_Title,
	Reporting_Open_Devices_Title,
	Reporting_Unique_Devices_Title,
	Reporting_ISPS_Results_Title	
]>


<cfset Reporting_SMSGroup_List =[
    
	Reporting_SMSMO_Log_Title,
	Reporting_SMSSession_States_Title,
	Reporting_ActiveICBy_Keyword_Title,
	Reporting_ActiveIC_Details_Title,
	Reporting_ActiveIC_Counts_Title,
	Reporting_MTDelivery_Results_Title,
	Reporting_MO_Responses_Title
		
]>


<cfset Reporting_StatesGroup_List = [
    Reporting_CallResults_ByState_Title,
	Reporting_AvgCallTime_ByState_Title,
	Reporting_TotalCalls_ByState_Title
		
]>


<cfset Reporting_CallResultsGroup_List = [
    Reporting_CallResults_Title,
	Reporting_AvgCallTime_Title,
	Reporting_CallResultsPerHour_Title,
	Reporting_LiveCallsPerHour_Title,
	Reporting_MachineCallsPerHour_Title,
	Reporting_AvgCallTimeBy_CallResult_Title,
	Reporting_FinalCallResults_Basic_Title,
	Reporting_DialHistoryLookup_Title
		
]>



<cfset Developers_List = [
	Developers_Help_Title,
	Developers_Somthing_Else_Title	
]>

<cfset EMS_List = [
	EMS_Voice_Title,
	EMS_SMS_Title,
	EMS_Email_Title
]>

<cfset AllPermission = [
	Campaign_Title,
	ABCampaign_Title,
	Marketing_Title,
	Survey_Title,
	Cpp_Title,
	SMS_Campaigns_Management_Title,
	Contact_Title,
	Reporting_Title,
	Developers_Title,
	EMS_Title,
	UnitTest_Title
]>
	
<cfloop array="#Campaigns_List#" index="ListLoop" >
	<cfset arrayAppend(AllPermission,ListLoop )>
</cfloop>

<cfloop array="#ABCampaigns_List#" index="ListLoop" >
	<cfset arrayAppend(AllPermission,ListLoop )>
</cfloop>

<cfloop array="#Surveys_list#" index="ListLoop" >
	<cfset arrayAppend(AllPermission,ListLoop )>
</cfloop>

<cfloop array="#CPP_list#" index="ListLoop" >
	<cfset arrayAppend(AllPermission,ListLoop )>
</cfloop>

<cfloop array="#SMS_Campaigns_list#" index="ListLoop" >
	<cfset arrayAppend(AllPermission,ListLoop )>
</cfloop>

<cfloop array="#Contacts_Parent_List#" index="ListLoop" >
	<cfset arrayAppend(AllPermission,ListLoop )>
</cfloop>

<cfloop array="#Contacts_List#" index="ListLoop" >
	<cfset arrayAppend(AllPermission,ListLoop )>
</cfloop>

<cfloop array="#Contact_Group_List#" index="ListLoop" >
	<cfset arrayAppend(AllPermission,ListLoop )>
</cfloop>

<cfloop array="#Reporting_Parent_List#" index="ListLoop" >
	<cfset arrayAppend(AllPermission,ListLoop )>
</cfloop>

<cfloop array="#Reporting_List#" index="ListLoop" >
	<cfset arrayAppend(AllPermission,ListLoop )>
</cfloop>

<cfloop array="#Reporting_Group_List#" index="ListLoop" >
	<cfset arrayAppend(AllPermission,ListLoop )>
</cfloop>

<cfloop array="#Reporting_SMSGroup_List#" index="ListLoop" >
	<cfset arrayAppend(AllPermission,ListLoop )>
</cfloop>

<cfloop array="#Reporting_StatesGroup_List#" index="ListLoop" >
	<cfset arrayAppend(AllPermission,ListLoop )>
</cfloop>


<cfloop array="#Reporting_CallResultsGroup_List#" index="ListLoop" >
	<cfset arrayAppend(AllPermission,ListLoop )>
</cfloop>



<cfloop array="#Developers_List#" index="ListLoop" >
	<cfset arrayAppend(AllPermission,ListLoop )>
</cfloop>

<cfloop array="#EMS_List#" index="ListLoop" >
	<cfset arrayAppend(AllPermission,ListLoop )>
</cfloop>



<cfdump var="#AllPermission#">