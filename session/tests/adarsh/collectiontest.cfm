<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>



<!--- Create a structure and loop through its contents. --->
<cfset Departments = StructNew()>
<cfset val = StructInsert(Departments, "John ", "Sales ")>
<cfset val = StructInsert(Departments, "Tom ", "Finance ")>
<cfset val = StructInsert(Departments, "Mike ", "Education ")>
<!--- Build a table to display the contents --->
<cfoutput>

<cfdump var="#Departments#">

<table cellpadding = "2 " cellspacing = "2 ">
    <tr>
        <td><b>Employee</b></td>
        <td><b>Dept.</b></td>
    </tr>
<!--- Use item to create the variable person to hold value of key as loop runs. --->
<cfloop collection = #Departments# item = "person ">
    <tr>
        <td>#person#</td>
        <td>#StructFind(Departments, person)#</td></tr>
</cfloop>
</table>
</cfoutput>

</body>
</html>