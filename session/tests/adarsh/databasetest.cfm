<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Database Test</title>
</head>

<body>


<cfquery name="ISPBarchartQuery" datasource="#Session.DBSourceEBM#">
         SELECT  
                SUM(blocked_int) AS Blocked_isps,
                SUM(bounce_int) AS Bounce_isps,
                SUM(deferred_int) AS Deferred_isps,
                SUM(delivered_int) AS Delivered_isps,
                SUM(drop_int) AS Drop_isps,
                SUM(open_int) AS Open_isps,
                SUM(processed_int) AS Processed_isps,
                SUM(request_int) AS Request_isps,
                SUM(spamreport_int) AS Spamreport_isps,
                SUM(uniqueopen_int) AS Uniqueopen_isps
                
 
       FROM    simplexresults.contactresults_email_account_summary_isps  ;
       
    </cfquery>   
    
    
    <cfdump var="#ISPBarchartQuery#">    
    
<cfquery name = "ispbarchartdisplay" dbtype = "query">

    SELECT SUM(Blocked_isps) as Ecount, 'Blocked ISP' as type
    from ISPBarchartQuery
    
    UNION
    
    SELECT SUM(Bounce_isps) as Ecount, 'Bounced ISP' as type
    from ISPBarchartQuery
    
    UNION
    
    SELECT SUM(Deferred_isps) as Ecount, 'Deferred ISP' as type
    from ISPBarchartQuery
    
    UNION
    
    SELECT SUM(Delivered_isps) as Ecount, 'Delivered ISP' as type
    from ISPBarchartQuery
    
    UNION
    
    SELECT SUM(Drop_isps) as Ecount, 'Dropped ISP' as type
    from ISPBarchartQuery
    
    UNION
    
    SELECT SUM(Open_isps) as Ecount, 'Open ISP' as type
    from ISPBarchartQuery
    
    UNION
    
    
    SELECT SUM(Processed_isps) as Ecount, 'Processed ISP' as type
    from ISPBarchartQuery
    
    UNION
    
    SELECT SUM(Request_isps) as Ecount, 'Request ISP' as type
    from ISPBarchartQuery
    
    UNION
    
    SELECT SUM(Spamreport_isps) as Ecount, 'Spamreport ISP' as type
    from ISPBarchartQuery
    
    UNION
    
    SELECT SUM(Uniqueopen_isps) as Ecount, 'Unique Open ISP' as type
    from ISPBarchartQuery
    
    

   </cfquery>
    
    
     <cfdump var="#ISPBarchartDisplay#">
                   
</body>
</html>