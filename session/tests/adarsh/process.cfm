<cfscript>
api = new smtpApiHeader();
 
// Convert our lists into arrays
addresses = listToArray( form.emails );
names = listToArray( form.subs );
 
// Add our e-mail addresses to the JSON string
api.addTo( addresses );
 
// Add our substitutions to the JSON string
// SendGrid will look for %name% in our e-mail
// and replace it with the appropriate value
api.addSubVal( '%name%', names );
 
// Create our HTTP service so that we can send
// this to the Web API
httpSvc = new http();
httpSvc.setUrl( 'https://sendgrid.com/api/mail.send.json' );
 
// Add params containg the information we need to send
 
// This is the JSON string that smtpApiHeader built for us
httpSvc.addParam( name = 'x-smtpapi', value = api.asJSON(), type = 'url' );
 
// The "to" address appears to be ignored if there is a "to"
// array in x-smtpapi. I just set it to the same value as the
// "from address"
httpSvc.addParam( name = 'to', value = 'your@email.com', type = 'url' );
httpSvc.addParam( name = 'from', value = 'your@email.com', type = 'url' );
httpSvc.addParam( name = 'subject', value = form.subject, type = 'url' );
 
// We could also specify HTML content here by adding another
// param named "html"
httpSvc.addParam( name = 'text', value = form.body, type = 'url' );
 
// Your api_user and api_key are your SendGrid username and password
httpSvc.addParam( name = 'api_user', value = 'YOUR_API_USER', type = 'url' );
httpSvc.addParam( name = 'api_key', value = 'YOUR_API_KEY', type = 'url' );
 
// Send our request!
httpResult = httpSvc.send().getPrefix();
</cfscript>