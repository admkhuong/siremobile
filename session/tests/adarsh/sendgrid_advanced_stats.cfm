<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Advanced ISPS Statistics</title>
</head>

<body>

<cfset urladdress = "https://api.sendgrid.com/api/stats.getAdvanced.json?api_user=mbroadcast&api_key=L9v8kKrN&start_date=2014-01-27&end_date=2014-01-31&data_type=isps">

<cfhttp url="#urladdress#" method="GET" resolveurl="Yes" throwonerror="Yes">

<!--- <cfoutput>
  This is just a string:<br />
  #CFHTTP.FileContent#<br />
</cfoutput> --->

<cfset cfData=DeserializeJSON(CFHTTP.FileContent)> 

<!--- This is object:<br />
<cfdump var="#cfData#"> --->   

<cfset arraylen = ArrayLen(cfdata)>

ArrayLength is : <cfdump var="#arraylen#"> <br/>


<cfset totals = {}> 





<cfloop index="struct" array="#cfData#">
    <cfset totals[struct.date] = {}>
     <cfloop item="key" collection="#struct#"> 
        <cfif isStruct(struct[key])>
            <cfset total = 0>
            <cfloop item="subkey" collection="#struct[key]#">
                <cfset total += struct[key][subkey]>
            </cfloop> 
            <cfset totals[struct.date][key] = total>
        </cfif>
    </cfloop>
</cfloop> 

<cfdump var="#totals#"><br/> 

<cfset dateArray = structKeyArray(totals)>


<!--- <cfdump var="#theDate#"> --->

<cfloop from = "1" to="#arrayLen(dateArray)#" index="i">

<cfset theDate = dateArray[i]>

Blocked : <cfoutput>#totals[theDate].blocked#</cfoutput> <br/>


Date : <cfoutput>#theDate#</cfoutput> <br/>

</cfloop>

<!--- <cfloop collection="#totals#" item="theDate">


Blocked:<cfoutput>#totals[theDate].blocked#</cfoutput>
Bounce: <cfoutput>#totals[theDate].bounce#</cfoutput>


</cfloop>



 

<cfdump var="#StructCount(totals)#"><br/>  --->

<!--- Testing Individually --->

 <!--- <cfloop from="1" to="#StructCount(totals)#" index="i">
 
 <cfoutput>#totals#</cfoutput>
 
 </cfloop> --->



<!--- <cfdump var="#totals["2014-01-27"]["blocked"]#"><br/>  --->



<!--- Structure Elements:
<cfdump var="#struct#"><br/> 
 --->

















<!--- set column names in a list --->
<!--- <cfset columnList = "delivered_int,
                    unique_open_int,
					spamreport_int,
					drop_int,
					request_int,
					bounce_int,
					deferred_int,
					processed_int,
					date_dt,
					startdate_dt,
					enddate_dt,
					open_int,
					blocked_int"> --->
                    
                    
                    
 <!---  <cfdump var="#columnList#">              
 --->
 
 <!--- <cfquery datasource="10.11.0.130" name="qCoulmnInsert">
INSERT INTO 
           simplexresults.contactresults_email_account_summary_devices (open_webMail_int,
           																open_phone_int,
                                                                        open_tablet_int,
                                                                        open_desktop_int,
																		open_other_int,
                                                                        open_desktop_int,
                                                                        startdate_dt,
                                                                        enddate_dt,
                                                                        unique_webMail_int,
                                                                        unique_tablet_int,
																		unique_other_int,
																		date_dt)
           
  VALUES
              <!--- loop through your array --->
             <cfloop from="1" to="#arrayLen(cfData)#" index="i">
             (
              <cfif structKeyExists(cfData[i], "delivered")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i].delivered#">
              <cfelse>
                 NULL
              
              </cfif> ,
              
              <cfif structKeyExists(cfData[i], "unique_open")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i].unique_open#">
              <cfelse>
                 NULL
              </cfif> ,
              
              
              <cfif structKeyExists(cfData[i], "spamreport")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i].unique_open#">
              <cfelse>
                 NULL
              </cfif> ,
              
              <cfif structKeyExists(cfData[i], "drop")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i].drop#">
               <cfelse>
                 NULL
              </cfif> ,
              
              
              <cfif structKeyExists(cfData[i], "request")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i].request#">
              <cfelse>
                 NULL
              </cfif> ,
              
              
              <cfif structKeyExists(cfData[i], "bounce")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i].bounce#">
               <cfelse>
                 NULL
              </cfif> ,
              
              <cfif structKeyExists(cfData[i], "deferred")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i].deferred#">
               <cfelse>
                 NULL
              </cfif> ,
              
              <cfif structKeyExists(cfData[i], "processed")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i].processed#">
              <cfelse>
                 NULL
              </cfif> ,
              
              <cfif structKeyExists(cfData[i], "date")>
                <cfqueryparam CFSQLTYPE="CF_SQL_DATE" value="#cfData[i].date#">
              <cfelse>
                 NULL
              </cfif> ,
              
              <cfif structKeyExists(cfData[i], "startdate_dt")>
                <cfqueryparam CFSQLTYPE="CF_SQL_DATE" value="#cfData[i].startdate_dt#">
              <cfelse>
                 NULL
              </cfif> ,
              
              
              <cfif structKeyExists(cfData[i], "enddate_dt")>
                <cfqueryparam CFSQLTYPE="CF_SQL_DATE" value="#cfData[i].enddate_dt#">
              <cfelse>
                 NULL
              </cfif>, 
              
              
              
              <cfif structKeyExists(cfData[i], "open")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i].open#">
              <cfelse>
                 NULL
              </cfif>,
               
              
              
              <cfif structKeyExists(cfData[i], "blocked")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i].blocked#">
              <cfelse>
                 NULL
              </cfif> 
            
              )
              
              <cfif i neq arrayLen(cfData)>,</cfif>
          </cfloop>
</cfquery> 
 --->




<!--- START: WORKING TESTING CODE --->
<!--- ArrayLength is : <cfoutput>#arraylen#</cfoutput> --->
<!--- <cfset KeyList   = "delivered,
                    unique_open,
					spamreport,
					drop,
					request,
					bounce,
					deferred,
					processed,
					date,
					startdate,
					enddate,
					open,
					blocked"> --->

<!--- <cfloop from="1" to="#arraylen#" index="i">
     <!--- <cfoutput>#StructKeyList(cfData[i].open)#</cfoutput><cfabort> --->
        <!--- <cfloop list="#KeyList#" index="colItem"> --->    
        <cfif structKeyExists(cfData[i], "open") AND structKeyExists(cfdata[i]["open"],"Lotus Notes")>
        <cfoutput>#cfData[i]["open"]["Lotus Notes"]#</cfoutput>
        
      <cfelse>
         NULL
      </cfif>
        <!--- <cfif colItem neq listLast(KeyList)>,</cfif>  --->
    <!--- </cfloop> --->
    </cfloop>
    
    <!--- END:WORKING TESTING CODE ---> 
    
     --->
    
    
    
    
    
    

<!--- 
  <!--- ACTUAL CODE --->
 
 
 <!--- set column names in a list --->
<cfset columnList = "delivered_int,
                    unique_open_int,
					spamreport_int,
					drop_int,
					request_int,
					bounce_int,
					deferred_int,
					processed_int,
					date_dt,
					startdate_dt,
					enddate_dt,
					open_int,
					blocked_int">
                    
                    
                    
  <cfdump var="#columnList#">              

<cfquery datasource="10.11.0.130" name="qCoulmnInsert">
INSERT INTO 
           simplexresults.contactresults_email_account_summary_global (#columnList#)
VALUES
      <!--- loop through your array --->
     <cfloop from="1" to="#arrayLen(cfData)#" index="i">
     (
      <!--- loop through the list of columns and see if they exists in the struct --->
      <cfloop list="#columnList#" index="colItem">
      <cfif structKeyExists(cfData[i], colItem)>
        <cfqueryparam value="#cfData[i][colItem]#">
      <cfelse>
         NULL
      </cfif>
        <cfif colItem neq listLast(columnList)>,</cfif>
    </cfloop>
    )<cfif i neq arrayLen(cfData)>,</cfif>
  </cfloop>
</cfquery>
 
 
 <!--- ACTUAL CODE --->
  --->


<!--- Blocked : <cfoutput>#arraylen[1].blocked#</cfoutput> ---> 
 <!--- <cfset blockedtotal = 0 />
<cfset bouncetotal = 0 />
<!--- <cfset blocked = 0/> --->
<cfset datetotal = 0 /> --->

<!--- <cfloop array = #cfData# index = "i">
<cfset blockedtotal += i.blocked />
<cfset bouncetotal += i.bounce />
</cfloop>

 Blocked Total : <cfdump var="#blockedtotal#"/>
 
  Bounce Total : <cfdump var="#bouncetotal#"/>
 --->
 
 
<!---  <cfloop array = #cfData# index = "i">
<!--- <cfset blockedtotal = i.blocked /> --->
<!---  <cfset bouncetotal = i.bounce />  --->
<cfset datetotal = i.date />

</cfloop>
<cfif structKeyExists(cfData,"bounce")>
<cfoutput>Bounce:#cfData.bounce#"/></cfoutput>
<cfelse>
<cfoutput> Bounce : [none]<br/></cfoutput>
</cfif>
Blocked  : <cfdump var="#blockedtotal#"/>
 
   Date  : <cfdump var="#datetotal#"/> --->
 
 <!---  Bounce Total : <cfdump var="#bouncetotal#"/> --->       

</body>
</html>