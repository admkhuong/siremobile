<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EBM:Select a Template</title>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
 <script type="text/javascript">

	$(function(){
		$('#subTitleText').hide();
		
		var $items = $('#vtab>ul>li.MultiChannelVTab');
		$items.addClass('selected');		
		$("#campaign").click(function() {
			window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/listcampaigns.cfm';
		});
		$("#ire").click(function() {
			window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/ire';
		});
		$("#contact").click(function() {
			window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/contacts/Contacts.cfm';
		});
		$("#reporting").click(function() {
			window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/reporting/reporting.cfm';
		});
		$("#administrator").click(function() {
			window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/administration/administration.cfm';
		});
		
		
		<!---$("#testLoad").load("http://cppseta.com/home/3953024561");  ---> 
	});

</script>  



<cfoutput>



<cfoutput> 
			 <script type="text/javascript" src="#rootUrl#/#SessionPath#/tests/adarsh/select_template/jquery-ui.js"></script>
             <script type="text/javascript" src="#rootUrl#/#SessionPath#/tests/adarsh/select_template/jquery.js"></script>
             <script type="text/javascript" src="#rootUrl#/#SessionPath#/tests/adarsh/select_template/jquery_002.js"></script>
 </cfoutput>  


<style>		
	            
				@import url('#rootUrl#/#publicPath#/css/email/combine.css');
				@import url('#rootUrl#/#publicPath#/css/email/elements-reset-icmin.css');
				@import url('#rootUrl#/#publicPath#/css/email/shared-icmin.css');
				@import url('#rootUrl#/#publicPath#/css/email/templateSelection-icmin.css');
                
</style>	


</cfoutput>


 <cfparam name="INPBATCHID" default="0">

<!---check permission--->
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">


<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkBatchPermissionByBatchId">
	<cfinvokeargument name="operator" value="#Create_Campaign_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfif NOT checkBatchPermissionByBatchId.havePermission >
	<cfoutput>#checkBatchPermissionByBatchId.message#</cfoutput>
	<cfexit>
</cfif> 







</head>

<body class="templateSelection">

		<div id="wideWrapper">

		     <div style="z-index: 2;" id="accountMenu" class=""></div>
			
  					<div id="divContentFrame">
    					<div class="ContentContainer">
      	 					<h2>Select a Template</h2>
								<div style="display: none;" id="filterDescription" class="error"></div>
									<div id="templatesWrap">
										<div class="template" data-templatelocation="AboveBoard" data-templatename="Above Board" data-templatecategory="general">
                                       		<div class="templateContainer">
												<span>
													<a href="" title="Use Template">
													<img class="thumbnail" src="select_template/blueprint_01.jpg" alt="AboveBoard">
													</a>
												</span>
													<div class="templateName">Template #1</div>
															<p>
																<a href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/tests/adarsh/ebm_email_create.cfm?button=temp1" title="Use Template" class="button secondary" id = "temp1">Use Template</a>
															</p>
													</div>
											</div>
                                            
                                            
                                            
										<div class="template" data-templatelocation="AboveBoardRight" data-templatename="Above Board Right"  data-templatecategory="general">
											<div class="templateContainer">
												<span>
													<a href="" title="Use Template">
													<img class="thumbnail" src="select_template/fancy_1_2_3.jpg" alt="AboveBoardRight">
													</a>
												</span>
													<div class="templateName">Fancy 1-2-3</div>
															<p>
															<a href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/tests/adarsh/ebm_email_create.cfm?button=temp1" title="Use Template" class="button secondary">Use Template</a>
															</p>
													</div>
											</div>
                                            
                                            
										<div class="template" data-templatelocation="AirMailOneColumn" data-templatename="AirMailOneColumn" data-templatecategory="general">
											<div class="templateContainer">
												<span>
													<a href="" title="Use Template">
													<img class="thumbnail" src="select_template/simple_basic.jpg" alt="AirMailOneColumn">
													</a>
												</span>
													<div class="templateName">Simple Basic</div>
														 <p>
														<a href="" title="Use Template" class="button secondary">Use Template</a>
														</p>
													</div>
											</div>
                                            
                                            
										<div class="template" data-templatelocation="AirMailPostcard" data-templatename="Air Mail Postcard"  data-templatecategory="general">
											<div class="templateContainer">
												<span>
													<a href="" title="Use Template">
													<img class="thumbnail" src="select_template/transactional_basic.jpg" alt="AirMailPostcard">
													</a>
												</span>
													<div class="templateName">Transactional Basic</div>
														<p>
														<a href="" title="Use Template" class="button secondary">Use Template</a>
														</p>
													</div>
											</div>
            
            
            							<div class="template" data-templatelocation="AirMailPostcard" data-templatename="Air Mail Postcard"  data-templatecategory="general">
											<div class="templateContainer">
												<span>
													<a href="" title="Use Template">
													<img class="thumbnail" src="select_template/transactional_basic.jpg" alt="AirMailPostcard">
													</a>
												</span>
											<div class="templateName">Transactional Basic</div>
													<p>
													<a href="" title="Use Template" class="button secondary">Use Template</a>
													</p>
											</div>
			</div>
								
		</ul>
		
		
		
	</div>
</div><!-- #footer -->
</div><!-- #wideWrapper -->

	

</div>


 
		





</body>
</html>