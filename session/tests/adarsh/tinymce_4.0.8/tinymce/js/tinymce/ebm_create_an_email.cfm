<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta name="robots" content="noindex, nofollow">
	
	<title>EBM : Create an Email Message</title>
	<link rel="stylesheet" type="text/css" href="ebm_message_files/combine.css">
    <link rel="stylesheet" type="text/css" href="ebm_message_files/elements-reset-icmin.css">
    <link rel="stylesheet" type="text/css" href="ebm_message_files/shared-icmin.css">


<!--My CODE -->
    <script type="text/javascript" src="tinymce.min.js"></script>
    <script type="text/javascript">

	tinymce.init({
    selector: "textarea",
	convert_urls: false,
    theme: "modern",
    plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor"
    ],
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    toolbar2: "print preview media | forecolor backcolor emoticons",
    image_advtab: true,
    templates: [
        {title: 'Test template 1', content: 'Test 1'},
        {title: 'Test template 2', content: 'Test 2'}
    ]
   });
  </script>
<!-- /TinyMCE -->


<!--My CODE -->

	
		



</head>


<body>
<!--<body onpageshow='event.persisted &amp;&amp; (function(){var allInstances = CKEDITOR.instances, editor, doc;for ( var i in allInstances ){	editor = allInstances[ i ];	doc = editor.document;	if ( doc )	{		doc.$.designMode = "off";		doc.$.designMode = "on";	}}})();' class="messageCoderBody">
 -->


<div id="wideWrapper"> 

			
		<!--<div id="header">
				<div id="mainNavWrapper">
			    <div id="mainNav"> 
				<ul>
					
				<li></li>
				<li>
				<div class="mainNavSubNav">
						
				</div>
			    </li>
				</ul>
			    </div>
		        </div>

			    <span class="decoration"></span>
	            </div>
 -->
  <div id="divContentFrame">
    <div class="ContentContainer">
      
   
      <h2>Create an Email Message</h2>


					<form id="messageCoderForm" action="http://ebmdevii.messagebroadcast.com/public/home" method="post">

							<input class="noDisplay" name="token" id="sCsrfToken" value="" type="hidden">
								<div id="messageInfoWrap">
           											 <div class="messageInfo">
															<div class="messageInfoFields ">
			    												<div class="field required">
																	<label for="subject">From Address:<span class="indicator">*</span></label>
																	<input name="sAddress" id="fromaddress" value="Address" type="text">
			   													 </div>
                                 
		   			   										 </div>
															<div class="messageInfoFields ">
																<div class="field required">
																	<label for="subject">Email Subject:<span class="indicator">*</span></label>
																	<input name="sSubject" id="subject" value="New Message" type="text">
						        								</div>
		                									</div>
           													<div class="messageInfoFields ">
																<div class="field required">
																	<label for="subject">Description Field:<span class="indicator">*</span></label>
																	<input name="sDescription" id="description" value="Description Not Specified for XML Control String" type="text">
															</div>
		   												</div>
								</div>




															<div class="messageInfoFields referenceName">
																<div class="field required">
																	<label for="refname">Message name:</label>
																	<input name="sRefName" id="refname" type="text">
																	<span class="moreInfo right tooltipHelp"></span>
			
				    												<span class="border"></span>
				    												<span class="arrow right"></span>
																</div>
															</div>
	</div>
</div>	
		
					<div class="messageCoder" style="display: none;">
												<div id="messageCover"></div>
													<div style="position: fixed; top: 0px; left: 0px;" id="toolbarWrapper">
														<div id="controlGroupWrapper">
															<div id="controlGroup">
																<div id="messageControls" class="controlGroup active">
																	<h6 id="messageButtonHeading">Message</h6>
																		<div class="overlay"></div>
																</div>
															</div>
														</div>
														<div id="rightEndCap" class="controlGroup">
														</div>
												</div>
		
														<div id="messageWorkAreaWrapper">
																<div id="messageWorkArea">
				
												         		</div>
														</div>
					</div> <!-- /messageCoder -->

	
					<div id="textVersion">
								<div class="field">
			
								<textarea id="sTextBody" name = "content" style="width:948px; max-width:948px;"></textarea>
								</div>	
		
								</div> <!-- /textVersion -->
								<!--DO NO DELETE -->	
									<div class="actions hubActions">
										<input id="iMessageId" name="iMessageId" value="38610" type="hidden">
										<input id="iMessageMode" name="iMessageMode" value="0" type="hidden">
										<input id="sMessageMode" name="sMessageMode" value="" type="hidden">
		
										<button type="submit" id="buttonSaveAsDraft" class="primary preview next button buttonAction" title="Save and exit MessageCoder" value="1">Save</button>				
        							</div>
        
        <!--DO NOT DELETE -->
</form>


</body>

</html>