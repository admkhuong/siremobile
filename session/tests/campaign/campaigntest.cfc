<!---
 MXUnit TestCase Template
 @author
 @description
 @history
 --->

<cfcomponent displayname="mxunit.framework.MyComponentTest"  extends="mxunit.framework.TestCase" >
	<cfinclude template="../../../public/paths.cfm" >
<!--- Begin Specific Test Cases --->
	<cffunction name="TestGetListElligableGroupCount_ByType_Success" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.distribution")>
		<cfset RetValGroupCount = distributionObject.GetListElligableGroupCount_ByType("862", "5", "1,2,3,", "", "1" )>
		<cfscript>
		assertequals(1, RetValGroupCount.RXRESULTCODE, "Happens error!");
		assertnotequals(0, RetValGroupCount.TOTALCOUNT, "Cannot get contacts correctly!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetListElligableGroupCount_ByType_NoResult" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.distribution")>
		<cfset RetValGroupCount = distributionObject.GetListElligableGroupCount_ByType("862", "-1", "", "", "1" )>
		<cfscript>
		assertequals(1, RetValGroupCount.RXRESULTCODE, "happens Error!");
		assertequals(0, RetValGroupCount.TOTALCOUNT, "Cannot get contacts correctly!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestValidateBilling_Success" access="public" returntype="void">
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.billing"
			method="ValidateBilling"
			returnvariable="RetValBillingData">                         
				<cfinvokeargument name="inpCount" value="4"/>
				<cfinvokeargument name="inpUserId" value="#Session.USERID#"/>
		</cfinvoke>
		
		<cfscript>
		assertequals(1, RetValBillingData.RXRESULTCODE, "happens Error!");
		assertequals(1, RetValBillingData.BALANCEOK, "happens Error!");
		assertNotequals(0, RetValBillingData.ESTIMATEDCOSTPERUNIT, "happens Error!");
		
		</cfscript>
		
	</cffunction>	
	
	<cffunction name="TestValidateBilling_SessionTimeOut" access="public" returntype="void">
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.billing"
			method="ValidateBilling"
			returnvariable="RetValBillingData">                         
				<cfinvokeargument name="inpCount" value="4"/>
				<cfinvokeargument name="inpUserId" value="0"/>
		</cfinvoke>
		
		<cfscript>
		assertequals(-2, RetValBillingData.RXRESULTCODE, "happens Error!");
		</cfscript>
	</cffunction>					
	
	<cffunction name="TestValidateBilling_Failed" access="public" returntype="void">
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.billing"
			method="ValidateBilling"
			returnvariable="RetValBillingData">                         
				<cfinvokeargument name="inpCount" value="400000"/>
				<cfinvokeargument name="inpUserId" value="#Session.USERID#"/>
		</cfinvoke>
		
		<cfscript>
		assertequals(-1, RetValBillingData.RXRESULTCODE, "expect Error happens!");
		assertequals("Billing Error - Balance Too Low", RetValBillingData.MESSAGE, "expect Error happens!");
		
		</cfscript>
	</cffunction>	
	
	<cffunction name="TestGetXMLControlString_Success" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.distribution")>
		<cfset xmlControlString = distributionObject.GetXMLControlString(880)>
		
		<cfscript>
		assertequals(1, xmlControlString.RXRESULTCODE, "Error happens!");
		assertnotequals("", xmlControlString.XMLCONTROLSTRING_VCH, "Error happens!");		
		</cfscript>
	</cffunction>	
	
	<cffunction name="TestGetXMLControlString_Failed" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.distribution")>
		<cfset xmlControlString = distributionObject.GetXMLControlString(-1)>
		
		<cfscript>
		assertequals(1, xmlControlString.RXRESULTCODE, "Error happens!");
		assertequals("", xmlControlString.XMLCONTROLSTRING_VCH, "Error happens!");		
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetSimpleXMLControlString_Success" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.distribution")>
		<cfset xmlControlString = distributionObject.GetSimpleXMLControlString(1, 1, 1)>
		
		<cfscript>
		assertequals(1, xmlControlString.RXRESULTCODE, "Error happens!");
		assertnotequals("", xmlControlString.XMLCONTROLSTRING_VCH, "Error happens!");		
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetSimpleXMLControlString_InvalidScriptId" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.distribution")>
		<cfset xmlControlString = distributionObject.GetSimpleXMLControlString("abc", 1, 1)>
		
		<cfscript>
		assertequals(-1, xmlControlString.RXRESULTCODE, "Error happens!");
		assertequals("", xmlControlString.XMLCONTROLSTRING_VCH, "Error happens!");		
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetSimpleXMLControlString_InvalidLidId" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.distribution")>
		<cfset xmlControlString = distributionObject.GetSimpleXMLControlString(1, "abc", 1)>
		
		<cfscript>
		assertequals(-1, xmlControlString.RXRESULTCODE, "Error happens!");
		assertequals("", xmlControlString.XMLCONTROLSTRING_VCH, "Error happens!");		
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetSimpleXMLControlString_InvalidEleId" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.distribution")>
		<cfset xmlControlString = distributionObject.GetSimpleXMLControlString(1, 1, "abc")>
		
		<cfscript>
		assertequals(-1, xmlControlString.RXRESULTCODE, "Error happens!");
		assertequals("", xmlControlString.XMLCONTROLSTRING_VCH, "Error happens!");		
		</cfscript>
	</cffunction>
	
	<cffunction name="TestAddNewBatch_Success" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.distribution")>
		<cfset xmlControlString = '<XMLControlStringDoc/><STAGE h="900" s="1" w="1200">0</STAGE>'>
		<cfset newBatchInfor = distributionObject.AddNewBatch("#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#", "#xmlControlString#", "1", "1", "1", "1", "1")>
		
		<cfscript>
		assertequals(1, newBatchInfor.RXRESULTCODE, "Error happens!");
		asserttrue(newBatchInfor.NEXTBATCHID GT 0, "Error happens!#newBatchInfor.INPXMLCONTROLSTRING# #newBatchInfor.NEXTBATCHID#");
		</cfscript>
		
		<cfquery name="DeleteBatch" datasource="#Session.DBSourceEBM#" result="DeleteBatchResult">
            DELETE FROM SimpleObjects.batch
                WHERE BatchId_bi = #newBatchInfor.NEXTBATCHID#
        </cfquery>  
		<cfscript>
			assertequals(1, DeleteBatchResult.RECORDCOUNT, "Error happens");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestAddNewBatch_InvalidLibId" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.distribution")>
		<cfset xmlControlString = '<XMLControlStringDoc/><STAGE h="900" s="1" w="1200">0</STAGE>'>
		<cfset newBatchInfor = distributionObject.AddNewBatch("#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#", "#xmlControlString#", "abc", "1", "1", "1", "1")>
		
		<cfscript>
		assertequals(-1, newBatchInfor.RXRESULTCODE, "Expected Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestAddNewBatch_InvalidEleId" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.distribution")>
		<cfset xmlControlString = '<XMLControlStringDoc/><STAGE h="900" s="1" w="1200">0</STAGE>'>
		<cfset newBatchInfor = distributionObject.AddNewBatch("#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#", "#xmlControlString#", "1", "abc", "1", "1", "1")>
		
		<cfscript>
		assertequals(-1, newBatchInfor.RXRESULTCODE, "Expected Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestAddNewBatch_InvalidScriptId" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.distribution")>
		<cfset xmlControlString = '<XMLControlStringDoc/><STAGE h="900" s="1" w="1200">0</STAGE>'>
		<cfset newBatchInfor = distributionObject.AddNewBatch("#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#", "#xmlControlString#", "1", "1", "abc", "1", "1")>
		
		<cfscript>
		assertequals(-1, newBatchInfor.RXRESULTCODE, "Expected Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestAddNewBatch_InvalidGroupId" access="public" returntype="void">
		<cfset distributionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.distribution")>
		<cfset xmlControlString = '<XMLControlStringDoc/><STAGE h="900" s="1" w="1200">0</STAGE>'>
		<cfset newBatchInfor = distributionObject.AddNewBatch("#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#", "#xmlControlString#", "1", "1", "1", "abc", "1")>
		
		<cfscript>
		assertequals(-1, newBatchInfor.RXRESULTCODE, "Expected Error happens!");
		</cfscript>
	</cffunction>
	
<!--- End Specific Test Cases --->



</cfcomponent>