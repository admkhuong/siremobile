﻿<cfcomponent displayname="mxunit.framework.MyComponentTest"  extends="mxunit.framework.TestCase" >
	<!--- Begin Specific Test Cases --->
	<cfinclude template="../../../public/paths.cfm" >
	
	<cffunction name="TestAdd_EditDescription_EditAndDeleteNewCPP_Success" access="public" returntype="void" output="true">
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="addNewCpp"
			returnvariable="RetValCreateCpp">                         
				<cfinvokeargument name="inpType" value="1"/>
				<cfinvokeargument name="inpCppDescription" value="Test mxunit"/>
				<cfinvokeargument name="inpTermService" value="1"/>
				<cfinvokeargument name="inpHtmlTemplate" value=""/>
				<cfinvokeargument name="inpVanity" value=""/>	
		</cfinvoke>
		<cfset jSonUtil = DeserializeJSON(RetValCreateCpp, true) />
		<cfscript>
			assertequals(1, jSonUtil.RXRESULTCODE, "Error happens!");
		</cfscript>
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="EditDescCPP"
			returnvariable="RetValEditDescCPP">                         
				<cfinvokeargument name="id" value="#jSonUtil.CPPUUID#"/>
				<cfinvokeargument name="Desc_vch" value="test edit Desc"/>
				<cfinvokeargument name="Expire_Dt" value="10"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValEditDescCPP.RXRESULTCODE, "Error happens!");
		</cfscript>
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="UpdateCpp"
			returnvariable="RetValUpdateCpp">
				<cfinvokeargument name="inpCPPUUID" value="#jSonUtil.CPPUUID#"/>                         
				<cfinvokeargument name="inpType" value="1"/>
				<cfinvokeargument name="inpCppDescription" value="Test mxunit"/>
				<cfinvokeargument name="inpTermService" value="1"/>
				<cfinvokeargument name="inpHtmlTemplate" value=""/>
				<cfinvokeargument name="inpVanity" value=""/>	
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValUpdateCpp.RXRESULTCODE, "Error happens!");
		</cfscript>
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="deleteCPP"
			returnvariable="RetValDeleteCPP">                         
				<cfinvokeargument name="CPPUUID" value="#jSonUtil.CPPUUID#" />
		</cfinvoke>
		
		<cfscript>
			assertequals(1, RetValDeleteCPP.RXRESULTCODE, "Error happens!");
		</cfscript>
		
	</cffunction>
	
	<cffunction name="TestGetCPPList_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="getCPPList"
			returnvariable="RetValGetCPPList">                         
				<cfinvokeargument name="q" value="1"/>
				<cfinvokeargument name="page" value="1"/>
				<cfinvokeargument name="rows" value="10"/>
				<cfinvokeargument name="sord" value="DESC"/>	
				<cfinvokeargument name="dialstring_mask" value="" />
				<cfinvokeargument name="inpSocialmediaFlag" value="0" />
				<cfinvokeargument name="inpSocialmediaRequests" value="0" />	
				<cfinvokeargument name="CPP_UUID_vch" value="aaa" />
				<cfinvokeargument name="Desc_vch" value="aaa" />		
				<cfinvokeargument name="Created_dt" value="" />
				<cfinvokeargument name="INPSTARTDATE" value="0" />			
				<cfinvokeargument name="INPENDDATE" value="0" />				
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetCPPList.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetCPPListReport_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="GetCPPListReport"
			returnvariable="RetValGetCPPListReport">                         
				<cfinvokeargument name="q" value="1"/>
				<cfinvokeargument name="page" value="1"/>
				<cfinvokeargument name="rows" value="10"/>
				<cfinvokeargument name="sord" value="DESC"/>	
				<cfinvokeargument name="dialstring_mask" value="" />
				<cfinvokeargument name="inpSocialmediaFlag" value="0" />
				<cfinvokeargument name="inpSocialmediaRequests" value="0" />	
				<cfinvokeargument name="CPP_UUID_vch" value="aaa" />
				<cfinvokeargument name="Desc_vch" value="aaa" />		
				<cfinvokeargument name="Created_dt" value="" />
				<cfinvokeargument name="INPSTARTDATE" value="0" />			
				<cfinvokeargument name="INPENDDATE" value="0" />				
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetCPPListReport.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<!---<cffunction name="TestEditDescCPP_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="EditDescCPP"
			returnvariable="RetValEditDescCPP">                         
				<cfinvokeargument name="id" value="1"/>
				<cfinvokeargument name="Desc_vch" value="test edit Desc"/>
				<cfinvokeargument name="Expire_Dt" value="10"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValEditDescCPP.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>--->
	
	<!---<cffunction name="TestDeleteCPP_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="deleteCPP"
			returnvariable="RetValDeleteCPP">                         
				<cfinvokeargument name="CPPUUID" value="1"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValDeleteCPP.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>--->
	
	<cffunction name="TestGetCPPData_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="GetCPPData"
			returnvariable="RetValGetCPPData">                         
				<cfinvokeargument name="CPPUUID" value="0052422069"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetCPPData.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<!---<cffunction name="TestUpdateCPPList_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="UpdateCPPList"
			returnvariable="RetValUpdateCPPList">                         
				<cfinvokeargument name="CPP_UUID_vch" value="0052422069"/>
				<cfinvokeargument name="UserId_int" value="10"/>
				<cfinvokeargument name="Desc_vch" value="test Update CPP"/>
				<cfinvokeargument name="PrimaryLink_vch" value="test Update CPP"/>
				<cfinvokeargument name="SystemPassword_vch" value="abc@123"/>
				<cfinvokeargument name="CPP_TEMPLATE_VCH" value="test Update CPP"/>
				<cfinvokeargument name="CPPSTYLETEMPLATE_VCH" value="2"/>
				<cfinvokeargument name="FACEBOOKMSG_VCH" value="0052422069"/>
				<cfinvokeargument name="TWITTERMSG_VCH" value="0052422069"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValUpdateCPPList.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>--->
	
	<cffunction name="TestGetReportCPP_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="GetReportCPP"
			returnvariable="RetValGetReportCPP">                         
				<cfinvokeargument name="UUID" value="56"/>
				<cfinvokeargument name="query" value="1"/>
				<cfinvokeargument name="Start_date" value=""/>
				<cfinvokeargument name="End_date" value=""/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetReportCPP.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetCPPTemplate_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="getCPPTemplate"
			returnvariable="RetValGetCPPTemplate">                         
				<cfinvokeargument name="CPP_UUID_vch" value="Test Add New CPP List"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetCPPTemplate.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestUpdateCPPTemplate_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="UpdateCPPTemplate"
			returnvariable="RetValUpdateCPPTemplate">                         
				<cfinvokeargument name="CPP_UUID_vch" value="Test Add New CPP List"/>
				<cfinvokeargument name="CPP_Template_vch" value="Test Add New CPP List"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValUpdateCPPTemplate.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetContactData_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="GetContactData"
			returnvariable="RetValGetContactData">                         
				<cfinvokeargument name="page" value="1"/>
				<cfinvokeargument name="rows" value="10"/>
				<cfinvokeargument name="sidx" value="UniqueCustomer_UUID_vch"/>
				<cfinvokeargument name="sord" value="ASC"/>
				<cfinvokeargument name="_search" value="false"/>
				<cfinvokeargument name="searchField" value="1"/>
				<cfinvokeargument name="searchString" value="1"/>
				<cfinvokeargument name="searchOper" value="1"/>
				<cfinvokeargument name="INPCPPUUID" value="1"/>
				<cfinvokeargument name="grouplist_vch" value="1"/>
				<cfinvokeargument name="UniqueCustomer_UUID_vch" value="1"/>
				<cfinvokeargument name="ContactTypeId_int" value="1"/>
				<cfinvokeargument name="ContactString_vch" value="1"/>
				<cfinvokeargument name="FirstName_vch" value="1"/>
				<cfinvokeargument name="LastName_vch" value="1"/>
				<cfinvokeargument name="CPPID_vch" value="1"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetContactData.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestCountLoginMadeChange_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="CountLoginMadeChange"
			returnvariable="RetValCountLoginMadeChange">                         
				<cfinvokeargument name="INPCPPUUID" value="1"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValCountLoginMadeChange.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestAverageNumberContact_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="AverageNumberContact"
			returnvariable="RetValAverageNumberContact">                         
				<cfinvokeargument name="INPCPPUUID" value="1"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValAverageNumberContact.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<!---<cffunction name="TestCppEditorDialog_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="CppEditorDialog"
			returnvariable="RetValCppEditorDialog">                         
				<cfinvokeargument name="inpShowSystemGroups" value="1"/>
				<cfinvokeargument name="INPCPPUUID" value="1"/>
				<cfinvokeargument name="page" value="1"/>
				<cfinvokeargument name="rows" value="10"/>
				<cfinvokeargument name="sidx" value="UniqueCustomer_UUID_vch"/>
				<cfinvokeargument name="sord" value="ASC"/>
				<cfinvokeargument name="_search" value="false"/>
				<cfinvokeargument name="searchField" value="1"/>
				<cfinvokeargument name="searchString" value="1"/>
				<cfinvokeargument name="searchOper" value="1"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValCppEditorDialog.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>--->
	
	<!---<cffunction name="TestUpdateDefaultMessage_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="updateDefaultMessage"
			returnvariable="RetValUpdateDefaultMessage">                         
				<cfinvokeargument name="inpCppId" value="0052422069"/>
				<cfinvokeargument name="inpFbMsg" value="0052422069"/>
				<cfinvokeargument name="inpTwitterMsg" value="0052422069"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValUpdateDefaultMessage.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>--->
	
	<cffunction name="TestGetDefaultMessage_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="getDefaultMessage"
			returnvariable="RetValGetDefaultMessage">                         
				<cfinvokeargument name="inpCppId" value="1436627781"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetDefaultMessage.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetDefaultMessage_Failed" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="getDefaultMessage"
			returnvariable="RetValGetDefaultMessage_Failed">                         
				<cfinvokeargument name="inpCppId" value="empty string"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetDefaultMessage_Failed.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestUpdateIPFilter_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="updateIPFilter"
			returnvariable="RetValUpdateIPFilter">                         
				<cfinvokeargument name="inpCppId" value="1436627781"/>
				<cfinvokeargument name="inpStartIp" value="192.168.1.1"/>
				<cfinvokeargument name="inpEndIp" value="192.168.1.225"/>
				<cfinvokeargument name="inpIpRange" value="1436627781"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValUpdateIPFilter.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>

	<cffunction name="TestRemoveIPRange_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="removeIPRange"
			returnvariable="RetValRemoveIPRange">                         
				<cfinvokeargument name="inpCppId" value="1436627781"/>
				<cfinvokeargument name="inpIpRange" value="1436627781"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValRemoveIPRange.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetIPFilterRanges_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="getIPFilterRanges"
			returnvariable="RetValGetIPFilterRanges">                         
				<cfinvokeargument name="inpCppId" value="1436627781"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetIPFilterRanges.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestActiveDeactiveFrame_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="activeDeactiveFrame"
			returnvariable="RetValActiveDeactiveFrame">                         
				<cfinvokeargument name="inpCppId" value="1436627781"/>
				<cfinvokeargument name="inpCurrentType" value="1"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValActiveDeactiveFrame.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestActiveDeactiveApiAccess_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="ActiveDeactiveApiAccess"
			returnvariable="RetValActiveDeactiveApiAccess">                         
				<cfinvokeargument name="inpCppId" value="1436627781"/>
				<cfinvokeargument name="inpCurrentType" value="1"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValActiveDeactiveApiAccess.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<!---<cffunction name="TestUpdateCpp_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="UpdateCpp"
			returnvariable="RetValUpdateCpp">
				<cfinvokeargument name="inpCPPUUID" value="1436627781"/>                         
				<cfinvokeargument name="inpType" value="1"/>
				<cfinvokeargument name="inpCppDescription" value="Test mxunit"/>
				<cfinvokeargument name="inpTermService" value="1"/>
				<cfinvokeargument name="inpHtmlTemplate" value=""/>
				<cfinvokeargument name="inpVanity" value=""/>	
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValUpdateCpp.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>--->
	
	<cffunction name="TestCheckVanity_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="CheckVanity"
			returnvariable="RetValCheckVanity">
				<cfinvokeargument name="inpVanity" value=""/>	
				<cfinvokeargument name="cppId" value="1"/>	
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValCheckVanity.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestAdd_UpdateAndDeleteCppPreference_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="AddCppPreference"
			returnvariable="RetValAddCppPreference">
				<cfinvokeargument name="inpCPPUUID" value="0546425241"/>	
				<cfinvokeargument name="inpPreferenceDescription" value="my list b"/>
				<cfinvokeargument name="inpPreferenceValue" value="my list a"/>	
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValAddCppPreference.RXRESULTCODE, "Error happens!");
		</cfscript>
		
		<cfquery name="test" datasource="#Session.DBSourceEBM#">
    		select last_insert_id() as Last_ID
     	</cfquery>
     	
     	<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="updatePreferenceContact"
			returnvariable="RetValUpdatePreferenceContact">
				<cfinvokeargument name="inpCPPUUID" value="#test.Last_ID#"/>	
				<cfinvokeargument name="preferenceId" value="1"/>
				<cfinvokeargument name="contactGroupId" value="1"/>	
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValUpdatePreferenceContact.RXRESULTCODE, "Error happens!");
		</cfscript>
		 
	  	<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="deletePreference"
			returnvariable="RetValDeletePreference">
				<cfinvokeargument name="preferenceId" value="#test.Last_ID#"/>	
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValDeletePreference.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<!---<cffunction name="TestUpdatePreferenceContact_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="updatePreferenceContact"
			returnvariable="RetValUpdatePreferenceContact">
				<cfinvokeargument name="inpCPPUUID" value="1436627781"/>	
				<cfinvokeargument name="preferenceId" value="1"/>
				<cfinvokeargument name="contactGroupId" value="1"/>	
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValUpdatePreferenceContact.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>--->
	
	<cffunction name="TestGetPreference_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="GetPreference"
			returnvariable="RetValGetPreference">
				<cfinvokeargument name="inpCPPUUID" value="1436627781"/>	
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetPreference.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetIdentifyGroup_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="GetIdentifyGroup"
			returnvariable="RetValGetIdentifyGroup">
				<cfinvokeargument name="inpCPPUUID" value="1436627781"/>	
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetIdentifyGroup.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestRemoveIdentifyGroup_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="RemoveIdentifyGroup"
			returnvariable="RetValRemoveIdentifyGroup">
				<cfinvokeargument name="inpIdentifyGroupId" value="2"/>	
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValRemoveIdentifyGroup.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetCPPSETUP_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="GetCPPSETUP"
			returnvariable="RetValGetCPPSETUP">
				<cfinvokeargument name="inpCPPUUID" value="1436627781"/>	
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetCPPSETUP.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestUpdateCPPSETUP_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="UpdateCPPSETUP"
			returnvariable="RetValUpdateCPPSETUP">
				<cfinvokeargument name="inpCPPUUID" value="3856463416"/>	
				<cfinvokeargument name="inpMultiplePreference" value="0"/>	
				<cfinvokeargument name="inpSetCustomValue" value="0"/>	
				<cfinvokeargument name="inpUpdatePreference" value="0"/>	
				<cfinvokeargument name="inpVoiceMethod" value="1"/>	
				<cfinvokeargument name="inpSMSMethod" value="1"/>	
				<cfinvokeargument name="inpEmailMethod" value="1"/>	
				<cfinvokeargument name="inpIncludeLanguage" value="0"/>	
				<cfinvokeargument name="inpIncludeIdentity" value="0"/>	
				<cfinvokeargument name="inpStepSetup" value="1"/>	
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValUpdateCPPSETUP.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestChangeCPPTempateData_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="ChangeCPPTempateData"
			returnvariable="RetValChangeCPPTempateData">				
				<cfinvokeargument name="templateName_vch" value="1436627781">
				<cfinvokeargument name="CPP_UUID_VCH" value="0">
				<cfinvokeargument name="fontFamily_vch" value="None">
				<cfinvokeargument name="fontSize_int" value="14">
				<cfinvokeargument name="fontSizeUnit_vch" value="px">
				<cfinvokeargument name="fontWeight_vch" value="Normal">
				<cfinvokeargument name="fontStyle_vch" value="Normal">
				<cfinvokeargument name="fontVariant_vch" value="Normal">
				<cfinvokeargument name="fontColor_vch" value="">
				<cfinvokeargument name="customCss_txt" value="">
				<cfinvokeargument name="backgroundColor_vch" value="">
				<cfinvokeargument name="backgroundImage_vch" value="">
				<cfinvokeargument name="imageRepeat_vch" value="repeat">
				<cfinvokeargument name="borderColor_vch" value="">
				<cfinvokeargument name="borderWidth_int" value="2">
				<cfinvokeargument name="borderRadius_int" value="2">
				<cfinvokeargument name="dataType" value="0">
				<cfinvokeargument name="partType" value="0">
				<cfinvokeargument name="templateId_int" value="0">	
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValChangeCPPTempateData.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetTemplate_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="GetTemplate"
			returnvariable="RetValGetTemplate">				
				<cfinvokeargument name="inpCPPUUID" value="1436627781">
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetTemplate.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetTemplateData_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="GetTemplateData"
			returnvariable="RetValGetTemplateData">				
				<cfinvokeargument name="inpCPPUUID" value="1436627781">
				<cfinvokeargument name="inpTemplateId" value="17">
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetTemplateData.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestSaveCustomHtml_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="SaveCustomHtml"
			returnvariable="RetValSaveCustomHtml">				
				<cfinvokeargument name="inpCPPUUID" value="1436627781">
				<cfinvokeargument name="inpTemplateId" value="17">
				<cfinvokeargument name="inpCustomHTML" value="17">				
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValSaveCustomHtml.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestChangeTemplate_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="ChangeTemplate"
			returnvariable="RetValChangeTemplate">				
				<cfinvokeargument name="inpCPPUUID" value="1436627781">
				<cfinvokeargument name="inpTemplateId" value="17">
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValChangeTemplate.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestDeleteTemplate_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="DeleteTemplate"
			returnvariable="RetValDeleteTemplate">				
				<cfinvokeargument name="inpCPPUUID" value="1436627781">
				<cfinvokeargument name="inpTemplateId" value="17">
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValDeleteTemplate.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestUpdateTemplate_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="UpdateTemplate"
			returnvariable="RetValUpdateTemplate">				
				<cfinvokeargument name="templateId" value="0">
				<cfinvokeargument name="imgServerFile" value="">
				<cfinvokeargument name="title" value="">
				<cfinvokeargument name="description" value="">
				
				<cfinvokeargument name="preTitleFont" value="Arial,Helvetica,sans-serif">
				<cfinvokeargument name="preTitleSize" value="25px">
				<cfinvokeargument name="preTitleColor" value="##000000">
				
				<cfinvokeargument name="preDescFont" value="Arial,Helvetica,sans-serif">
				<cfinvokeargument name="preDescSize" value="15px">
				<cfinvokeargument name="preDescColor" value="##000000">
				
				<cfinvokeargument name="preContentFont" value="Arial,Helvetica,sans-serif">
				<cfinvokeargument name="preContentSize" value="15px">
				<cfinvokeargument name="preContentColor" value="##000000">
				
				<cfinvokeargument name="contactTitleFont" value="Arial,Helvetica,sans-serif">
				<cfinvokeargument name="contactTitleSize" value="25px">
				<cfinvokeargument name="contactTitleColor" value="##000000">
				
				<cfinvokeargument name="contactDescFont" value="Arial,Helvetica,sans-serif">
				<cfinvokeargument name="contactDescSize" value="15px">
				<cfinvokeargument name="contactDescColor" value="##000000">
				
				<cfinvokeargument name="contactLabelFont" value="Arial,Helvetica,sans-serif">
				<cfinvokeargument name="contactLabelSize" value="15px">
				<cfinvokeargument name="contactLabelColor" value="##000000">
				
				<cfinvokeargument name="langTitleFont" value="Arial,Helvetica,sans-serif">
				<cfinvokeargument name="langTitleSize" value="25px">
				<cfinvokeargument name="langTitleColor" value="##000000">
				
				<cfinvokeargument name="langDescFont" value="Arial,Helvetica,sans-serif">
				<cfinvokeargument name="langDescSize" value="15px">
				<cfinvokeargument name="langDescColor" value="##000000">
				
				<cfinvokeargument name="langLabelFont" value="Arial,Helvetica,sans-serif">
				<cfinvokeargument name="langLabelSize" value="15px">
				<cfinvokeargument name="langLabelColor" value="##000000">
				
				<cfinvokeargument name="langContentFont" value="Arial,Helvetica,sans-serif">
				<cfinvokeargument name="langContentSize" value="15px">
				<cfinvokeargument name="langContentColor" value="##000000">
			</cfinvoke>
		<cfscript>
			assertequals(1, RetValUpdateTemplate.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>

	<cffunction name="TestUpdateNewTemplate_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="UpdateNewTemplate"
			returnvariable="RetValUpdateNewTemplate">				
				<cfinvokeargument name="cppUUID" value="1436627781">
				<cfinvokeargument name="sizeType_int" value="0">
				<cfinvokeargument name="width_int" value="0">
				<cfinvokeargument name="height_int" value="0">
				<cfinvokeargument name="displayType_ti" value="0">
				<cfinvokeargument name="templateId_int" value="0">
				<cfinvokeargument name="customHtml_txt" value="0">
			</cfinvoke>
		<cfscript>
			assertequals(1, RetValUpdateNewTemplate.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>

	<cffunction name="TestSelectCppVanity_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingcpp"
			method="SelectCppVanity"
			returnvariable="RetValSelectCppVanity">				
				<cfinvokeargument name="inpCPPUUID" value="1436627781">
			</cfinvoke>
		<cfscript>
			assertequals(1, RetValSelectCppVanity.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
</cfcomponent>