<!---
NOTE: these tests take a long time to run. A lot of the time is in a subset of tests that use CF's built-in component explorer, which is a pig. In addition, the EXT output is slower than the HTML output.

--->
<cfsetting requesttimeout="300">
<cfinclude template="../../public/paths.cfm">
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">

<cfparam name="url.output" default="extjs">
<cfparam name="url.debug" default="false">
<cfparam name="url.quiet" default="false">

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
	<cfinvokeargument name="operator" value="#UnitTest_Title#">
</cfinvoke>
<cfif NOT permissionStr.havePermission>
	<cfset session.permissionError = permissionStr.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfset dir = getDirectoryFromPath(getCurrentTemplatePath()) />
<cfset DTS = createObject("component","#LocalServerDirPath#.mxunit.runner.DirectoryTestSuite")>

<cfset excludes = "fixture,samples,install">

<cfinvoke 
	component="#DTS#" 
	method="run"
	directory="#dir#"
	componentpath="#LocalServerDirPath#.Session.tests" 
	recurse="true" 
	excludes="#excludes#"
	returnvariable="Results">
<cfif not url.quiet>
	
	<cfif NOT StructIsEmpty(DTS.getCatastrophicErrors())>
		<cfdump var="#DTS.getCatastrophicErrors()#" expand="false" label="#StructCount(DTS.getCatastrophicErrors())# Catastrophic Errors">
	</cfif>
	
	<cfsetting showdebugoutput="true">
	<cfoutput>#results.getResultsOutput(url.output)#</cfoutput>
	
	<cfif isBoolean(url.debug) AND url.debug>
		<div class="bodypad">
			<cfdump var="#results.getResults()#" label="Debug">
		</div>
	</cfif>
</cfif>

<!--- 
<cfdump var="#results.getDebug()#"> --->