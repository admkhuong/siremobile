﻿<cfcomponent displayname="mxunit.framework.MyComponentTest"  extends="mxunit.framework.TestCase" >
	<!--- Begin Specific Test Cases --->
	<cfinclude template="../../../public/paths.cfm" >
	
	<cffunction name="TestReadXMLQuestions_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="ReadXMLQuestions"
			returnvariable="RetValReadXMLQuestions">                         
				<cfinvokeargument name="INPBATCHID" value="265"/>
				<cfinvokeargument name="GROUPID" value="Test mxunit"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValReadXMLQuestions.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestReadXMLQuestionsOptimized_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="ReadXMLQuestionsOptimized"
			returnvariable="RetValReadXMLQuestionsOptimized">                         
				<cfinvokeargument name="INPBATCHID" value="265"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValReadXMLQuestionsOptimized.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestReadSurveyAnswers_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="ReadSurveyAnswers"
			returnvariable="RetValReadSurveyAnswers">                         
				<cfinvokeargument name="INPBATCHID" value="265"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValReadSurveyAnswers.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetStartQ_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="GetStartQ"
			returnvariable="RetValGetStartQ">                         
				<cfinvokeargument name="INPBATCHID" value="265"/>
				<cfinvokeargument name="GROUPID" value="1"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetStartQ.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetQAggregateData_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="GetQAggregateData"
			returnvariable="RetValGetQAggregateData">                         
				<cfinvokeargument name="INPBATCHID" value="265"/>
				<cfinvokeargument name="GROUPID" value="1"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetQAggregateData.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestReadXMLQuestions1_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="ReadXMLQuestions1"
			returnvariable="RetValReadXMLQuestions1">                         
				<cfinvokeargument name="INPBATCHID" value="265"/>
				<cfinvokeargument name="GROUPID" value="1"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValReadXMLQuestions1.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestReadSurveyVoiceResponseCount_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="ReadSurveyVoiceResponseCount"
			returnvariable="RetValReadSurveyVoiceResponseCount">                         
				<cfinvokeargument name="INPBATCHID" value="265"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValReadSurveyVoiceResponseCount.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestReadSurveyVoiceAnswers_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="ReadSurveyVoiceAnswers"
			returnvariable="RetValReadSurveyVoiceAnswers">                         
				<cfinvokeargument name="INPBATCHID" value="265"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValReadSurveyVoiceAnswers.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestReadXMLEMAIL_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="ReadXMLEMAIL"
			returnvariable="RetValReadXMLEMAIL">                         
				<cfinvokeargument name="BATCHID" value="265"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValReadXMLEMAIL.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestReadXMLQs_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="ReadXMLQs"
			returnvariable="RetValReadXMLQs">                         
				<cfinvokeargument name="BATCHID" value="265"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValReadXMLQs.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetListBatchID_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="getListBatchID"
			returnvariable="RetValGetListBatchID">                         
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetListBatchID.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestAddAndDeleteNewSurvey_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="AddNewSurvey"
			returnvariable="RetValAddNewSurvey">                         
				<cfinvokeargument name="INPBATCHID" value="999999"/>
				<cfinvokeargument name="INPBATCHDESC" value="abc"/>
				<cfinvokeargument name="inpXML" value="abc"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValAddNewSurvey.RXRESULTCODE, "Error happens!");
		</cfscript>
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.distribution"
			method="UpdateBatchStatus"
			returnvariable="RetValUpdateBatchStatus">                         
				<cfinvokeargument name="INPBATCHID" value="999999"/>
				<cfinvokeargument name="TYPEBATCH" value="1"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValAddNewSurvey.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestSaveQuestionsSurvey_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="SaveQuestionsSurvey"
			returnvariable="RetValSaveQuestionsSurvey">                         
				<cfinvokeargument name="INPBATCHID" value="264"/>
				<cfinvokeargument name="INPTYPE" value="1"/>
				<cfinvokeargument name="inpXML" value="abc"/>
				<cfinvokeargument name="POSITION" value="1"/>
				<cfinvokeargument name="STARTNUMBERQ" value="1"/>
				<cfinvokeargument name="INPQuestionID" value="abc"/>
				<cfinvokeargument name="SquashUnicode" value="abc"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValSaveQuestionsSurvey.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetSurveyMCContent_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="GetSurveyMCContent"
			returnvariable="RetValGetSurveyMCContent"> 
				<cfinvokeargument name="q" value="1"/>                        
				<cfinvokeargument name="page" value="1"/>
				<cfinvokeargument name="rows" value="10"/>
				<cfinvokeargument name="sidx" value="UniqueCustomer_UUID_vch"/>
				<cfinvokeargument name="sord" value="ASC"/>
				<cfinvokeargument name="INPGROUPID" value="3" />
				<cfinvokeargument name="dialstring_mask" value="" />
				<cfinvokeargument name="notes_mask" value="" />
				<cfinvokeargument name="buddy_mask" value="" />
				<cfinvokeargument name="out_mask" value="" />
				<cfinvokeargument name="in_mask" value="" />
				<cfinvokeargument name="fresh_mask" value="" />
				<cfinvokeargument name="inpSocialMediaFlag" value="0" />
				<cfinvokeargument name="inpSocialMediaRequests" value="0" />
				<cfinvokeargument name="filterData" value="">
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetSurveyMCContent.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetReportingSurveys_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="GetReportingSurveys"
			returnvariable="RetValGetReportingSurveys"> 
				<cfinvokeargument name="q" value="1"/>                        
				<cfinvokeargument name="page" value="1"/>
				<cfinvokeargument name="rows" value="10"/>
				<cfinvokeargument name="sidx" value="UniqueCustomer_UUID_vch"/>
				<cfinvokeargument name="sord" value="ASC"/>
				<cfinvokeargument name="INPGROUPID" value="3" />
				<cfinvokeargument name="dialstring_mask" value="" />
				<cfinvokeargument name="notes_mask" value="" />
				<cfinvokeargument name="buddy_mask" value="" />
				<cfinvokeargument name="out_mask" value="" />
				<cfinvokeargument name="in_mask" value="" />
				<cfinvokeargument name="fresh_mask" value="" />
				<cfinvokeargument name="inpSocialMediaFlag" value="0" />
				<cfinvokeargument name="inpSocialMediaRequests" value="0" />
				<cfinvokeargument name="filterData" value="">
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetReportingSurveys.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestDeleteAllSurveyQuestions_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="DeleteAllSurveyQuestions"
			returnvariable="RetValDeleteAllSurveyQuestions">                         
				<cfinvokeargument name="INPBATCHID" value="265"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValDeleteAllSurveyQuestions.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestUpdateSurveyListDescExpDate_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="UpdateSurveyListDescExpDate"
			returnvariable="RetValUpdateSurveyListDescExpDate">                         
				<cfinvokeargument name="INPBATCHID" value="265"/>
				<cfinvokeargument name="id"  value="0" />
				<cfinvokeargument name="Desc_vch" value="" />
				<cfinvokeargument name="Expire_Dt" value="" />
				<cfinvokeargument name="communicationType" value="" />
				<cfinvokeargument name="IsIncludeWelcomePage" value="0" />
				<cfinvokeargument name="IsIncludeThankYouPage" value="0" />
				<cfinvokeargument name="IsEnableBackButton" value="0" />
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValUpdateSurveyListDescExpDate.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetBasicSurveyInfo_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="GetBasicSurveyInfo"
			returnvariable="RetValGetBasicSurveyInfo">                         
				<cfinvokeargument name="INPBATCHID" value="265"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetBasicSurveyInfo.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetAnswerList_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="GetAnswerList"
			returnvariable="RetValGetAnswerList">                         
				<cfinvokeargument name="INPVOICEDESC" value="264"/>
		</cfinvoke>
		<cfscript>
			assertIsArray(RetValGetAnswerList);
		</cfscript>
	</cffunction>
	
	<cffunction name="TestUpdateQuestion_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="UpdateQuestion"
			returnvariable="RetValUpdateQuestion">                         
				<cfinvokeargument name="INPBATCHID" value="264"/>
				<cfinvokeargument name="INPQUESTIONID" value="90"/>
				<cfinvokeargument name="INPXMLEmail" value=""/>
				<cfinvokeargument name="INPXMLSMS" value=""/>
				<cfinvokeargument name="INPXMLVoice" value=""/>
				<cfinvokeargument name="INPTYPE" value="1"/>
				<cfinvokeargument name="SquashUnicode" value="1"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValUpdateQuestion.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestDeleteQuestion_Success" access="public" returntype="void" >
		
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="DeleteQuestion"
			returnvariable="RetValDeleteQuestion">                         
				<cfinvokeargument name="INPBATCHID" value="264"/>
				<cfinvokeargument name="INPQUESTIONID" value="90"/>
				<cfinvokeargument name="INPTYPE" value="1"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValDeleteQuestion.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestSaveAnswerSurvey_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="SaveAnswerSurvey"
			returnvariable="RetValSaveAnswerSurvey">                         
				<cfinvokeargument name="BATCHID" value="264"/>
				<cfinvokeargument name="UUID" value="90"/>
				<cfinvokeargument name="inpXML" value="1"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValSaveAnswerSurvey.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestNextQID_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="NextQID"
			returnvariable="RetValNextQID">                         
				<cfinvokeargument name="RXTType" value="3"/>
		</cfinvoke>
		<cfscript>
			assertnotequals("", RetValNextQID, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetUUID_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="getUUID"
			returnvariable="RetValGetUUID">                         
				<cfinvokeargument name="INPBATCHID" value="264"/>
		</cfinvoke>
		<cfscript>
			assertnotequals("" ,RetValGetUUID, "Error happens!");
		</cfscript>
	</cffunction>
	
	<!---<cffunction name="TestUpdateSurveyTemplate_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="UpdateSurveyTemplate"
			returnvariable="RetValUpdateSurveyTemplate">                         
				<cfinvokeargument name="INPBATCHID" value="264"/>
				<cfinvokeargument name="RURL" value="abc"/>
				<cfinvokeargument name="RTEXT" value="acb"/>
				<cfinvokeargument name="TEMPLATE" value="abc"/>
		</cfinvoke>
		<cfscript>
			assertequals(1,RetValUpdateSurveyTemplate.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>--->
	
	<cffunction name="TestGetSurveyPage_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="getSurveyPage"
			returnvariable="RetValGetSurveyPage">                         
				<cfinvokeargument name="INPBATCHID" value="264"/>
		</cfinvoke>
		<cfscript>
			assertequals(1,RetValGetSurveyPage.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestUpdateSurveyPage_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="UpdateSurveyPage"
			returnvariable="RetValUpdateSurveyPage">                         
				<cfinvokeargument name="INPBATCHID" value="1534"/>
				<cfinvokeargument name="INPSTARTPROMPT" value="200"/>
				<cfinvokeargument name="INPENDPROMPT" value="264"/>
				<cfinvokeargument name="INPSTARTPROMPTSCRIPTID" value="264"/>
				<cfinvokeargument name="INPENDPROMPTSCRIPTID" value="264"/>
				<cfinvokeargument name="INPWELCOMETEXT" value="264"/>
				<cfinvokeargument name="INPTHANKYOUTEXT" value="264"/>
				<cfinvokeargument name="INPREDIRECTTEXT" value="264"/>
				<cfinvokeargument name="INPREDIRECTURL" value="264"/>
		</cfinvoke>
		<cfscript>
			assertequals(1,RetValUpdateSurveyPage.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetSurveyCustomize_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="GetSurveyCustomize"
			returnvariable="RetValGetSurveyCustomize">                         
				<cfinvokeargument name="INPBATCHID" value="264"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetSurveyCustomize.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestUpdateSurveyCustomize_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="GetSurveyCustomize"
			returnvariable="RetValGetSurveyCustomize">                         
				<cfinvokeargument name="INPBATCHID" value="264"/>
				<cfinvokeargument name="INPSURVEYNAME" value="264"/>
				<cfinvokeargument name="INPFOOTERTEXT" value="264"/>
				<cfinvokeargument name="INPIMGUPLOADLOGO" value="264"/>
				<cfinvokeargument name="INPDISABLEBACK" value="264"/>
				<cfinvokeargument name="INPISONLYUPDATEDISABLEBACK" value="264"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetSurveyCustomize.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetCallResult_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="getCallResult"
			returnvariable="RetValGetCallResult">                         
				<cfinvokeargument name="INPBATCHID" value="264"/>
				<cfinvokeargument name="INPUUID" value="264"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetCallResult.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetDataAverageSurvey_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="GetDataAverageSurvey"
			returnvariable="RetValGetDataAverageSurvey">                         
				<cfinvokeargument name="INPBATCHID" value="264"/>
				<cfinvokeargument name="INPUUID" value="264"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetDataAverageSurvey.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>

	<cffunction name="TestSendMail_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="SendMail"
			returnvariable="RetValSendMail">                         
				<cfinvokeargument name="TYPE" value="5"/>
				<cfinvokeargument name="SUBJECT" value="264"/>
				<cfinvokeargument name="SENDER" value="264"/>
				<cfinvokeargument name="EMAILLIST" value="264"/>
				<cfinvokeargument name="GROUPID" value="264"/>
				<cfinvokeargument name="CONTENT" value="264"/>
				<cfinvokeargument name="INPBATCHID" value="264"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValSendMail.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestSendVoice_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="SendVoice"
			returnvariable="RetValSendVoice">                         
				<cfinvokeargument name="INPBATCHID" value="264"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValSendVoice.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<!---<cffunction name="TestRXdecodeXML_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="RXdecodeXML"
			returnvariable="RetValRXdecodeXML">                         
				<cfinvokeargument name="string" value="0"/>
		</cfinvoke>
		<cfscript>
			assertnotequals("0" , RetValRXdecodeXML, "Error happens!");
		</cfscript>
	</cffunction>--->
	
	<cffunction name="TestGetConfigPage_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="GetConfigPage"
			returnvariable="RetValGetConfigPage">                         
				<cfinvokeargument name="INPBATCHID" value="86"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetConfigPage.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestAddAndDeleteNewPage_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="AddPage"
			returnvariable="RetValAddPage">                         
				<cfinvokeargument name="INPBATCHID" value="1198"/>
				<cfinvokeargument name="GID" value="2"/>
				<cfinvokeargument name="INPTYPE" value="SMS"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValAddPage.RXRESULTCODE, "Error happens!");
		</cfscript>
		
		<cfquery name="test" datasource="#Session.DBSourceEBM#">
    		select last_insert_id() as Last_ID
     	</cfquery>
     	
     	<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="DeletePage"
			returnvariable="RetValDeletePage">                         
				<cfinvokeargument name="INPBATCHID" value="1198"/>
				<cfinvokeargument name="INPPAGEID" value="#test.Last_ID#"/>
				<cfinvokeargument name="INPTYPE" value="SMS"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValDeletePage.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetGroupNumber_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="GetGroupNumber"
			returnvariable="RetValGetGroupNumber">                         
				<cfinvokeargument name="INPBATCHID" value="264"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetGroupNumber.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestGetSurveyResponseStatistic_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="GetSurveyResponseStatistic"
			returnvariable="RetValGetSurveyResponseStatistic">                         
				<cfinvokeargument name="BATCHID" value="264"/>
				<cfinvokeargument name="colMethod" value="3"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetSurveyResponseStatistic.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestDownloadAudio_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="GetSurveyResponseStatistic"
			returnvariable="RetValGetSurveyResponseStatistic">                         
				<cfinvokeargument name="Text" value="abc"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValGetSurveyResponseStatistic.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestCreateSurveyAudioData_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="CreateSurveyAudioData"
			returnvariable="RetValCreateSurveyAudioData">                         
				<cfinvokeargument name="INPBATCHID" value="264"/>
				<cfinvokeargument name="INPAUDIONAME" value="abc"/>
				<cfinvokeargument name="INPSURVEYNAME" value="abc"/>
				<cfinvokeargument name="INPSECONDLEVELNAME" value="abc"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValCreateSurveyAudioData.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestCreateSurveyElementAudioData_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="CreateSurveyElementAudioData"
			returnvariable="RetValCreateSurveyElementAudioData">                         
				<cfinvokeargument name="INPBATCHID" value="264"/>
				<cfinvokeargument name="INPSURVEYNAME" value="abc"/>
				<cfinvokeargument name="INPSECONDLEVELNAME" value="abc"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValCreateSurveyElementAudioData.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>

	<cffunction name="TestClearBranchVoiceConditions_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="ClearBranchVoiceConditions"
			returnvariable="RetValClearBranchVoiceConditions">                         
				<cfinvokeargument name="INPBATCHID" value="80"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValClearBranchVoiceConditions.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestAddAndRemoveNewControlPointGrouping_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="AddNewControlPointGrouping"
			returnvariable="RetValAddNewControlPointGrouping">                         
				<cfinvokeargument name="INPBATCHID" value="264"/>
				<cfinvokeargument name="inpCPGXML" value="264"/>
				<cfinvokeargument name="INPTYPE" value="264"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValAddNewControlPointGrouping.RXRESULTCODE, "Error happens!");
		</cfscript>
		
		<cfquery name = "test" datasource="#Session.DBSourceEBM#">
		    select last_insert_id() as Last_ID
	     </cfquery>
	     
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="RemoveControlPointGrouping"
			returnvariable="RetValRemoveControlPointGrouping">                         
				<cfinvokeargument name="INPBATCHID" value="264"/>
				<cfinvokeargument name="inpCPGID" value="#test.Last_ID#"/>
				<cfinvokeargument name="INPTYPE" value="264"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValRemoveControlPointGrouping.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestRemoveAllControlPointGrouping_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="RemoveAllControlPointGrouping"
			returnvariable="RetValRemoveAllControlPointGrouping">                         
				<cfinvokeargument name="INPBATCHID" value="264"/>
				<cfinvokeargument name="INPTYPE" value="264"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValRemoveAllControlPointGrouping.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestUpdateControlPointGroup_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="UpdateControlPointGroup"
			returnvariable="RetValUpdateControlPointGroup">                         
				<cfinvokeargument name="INPBATCHID" value="264"/>
				<cfinvokeargument name="INPTYPE" value="264"/>
				<cfinvokeargument name="inpCPGID" value="264"/>
				<cfinvokeargument name="inpMin" value="0"/>
				<cfinvokeargument name="inpMax" value="264"/>
				<cfinvokeargument name="inpDesc" value="264"/>
				<cfinvokeargument name="inpDBSourceEBM" value="264"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValUpdateControlPointGroup.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestCopyControlPointGroup_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="CopyControlPointGroup"
			returnvariable="RetValCopyControlPointGroup">                         
				<cfinvokeargument name="INPBATCHID" value="264"/>
				<cfinvokeargument name="INPTYPE" value="264"/>
				<cfinvokeargument name="inpCPGID" value="264"/>
				<cfinvokeargument name="inpInsertAfterQID" value="0"/>
				<cfinvokeargument name="inpDBSourceEBM" value="264"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValCopyControlPointGroup.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
	
	<cffunction name="TestReadGroupData_Success" access="public" returntype="void" >
		<cfinvoke 
			component="#LocalSessionDotPath#.cfc.ire.marketingSurveyTools"
			method="ReadGroupData"
			returnvariable="RetValReadGroupData">                         
				<cfinvokeargument name="INPBATCHID" value="264"/>
		</cfinvoke>
		<cfscript>
			assertequals(1, RetValReadGroupData.RXRESULTCODE, "Error happens!");
		</cfscript>
	</cffunction>
</cfcomponent>