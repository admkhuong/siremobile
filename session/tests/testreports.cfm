<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>


<cfquery name="findpercentage" datasource="#Session.DBSourceEBM#">
    
    SELECT  
	     Count(TableId_bi) AS Total_Events
            ,Sum(CASE WHEN 'OPEN'       =	Event_vch THEN 100 END) / Count(*) AS OPENS
	        ,Sum(CASE WHEN 'BOUNCE' 	= 	Event_vch THEN 100 END) / Count(*) AS BOUNCE
            ,Sum(CASE WHEN 'DEFERRED'   = 	Event_vch THEN 100 END) / Count(*) AS DEFERRED_Event
            ,Sum(CASE WHEN 'DROPPED' 	= 	Event_vch THEN 100 END) / Count(*) AS DROPPED
            ,Sum(CASE WHEN 'DELIVERED'  = 	Event_vch THEN 100 END) / Count(*) AS DELIVERED
            ,Sum(CASE WHEN 'PROCESSED'  = 	Event_vch THEN 100 END) / Count(*) AS PROCESSED
            ,Sum(CASE WHEN 'SPAMREPORT' = 	Event_vch THEN 100 END) / Count(*) AS SPAMREPORT 
    FROM    
	        simplexresults.contactresults_email;
    
   <!---  WHERE
	       mbTimestamp_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpStart#"> 
     AND  
	       mbTimestamp_dt <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEnd#">
     AND  
           batchid_bi IN (<cfqueryparam value="#inpBatchIdList#" cfsqltype="CF_SQL_BIGINT" list="yes" />); --->
    
    </cfquery>


<cfdump var="#findpercentage#">



<cfquery name = "piechartdisplay" dbtype = "query">

SELECT OPENS as Ecount, 'Open' as type
from findpercentage

UNION

SELECT BOUNCE as Ecount, 'Bounce' as type
from findpercentage

UNION

SELECT DEFERRED_Event as Ecount, 'Deferred' as type
from findpercentage

UNION

SELECT DROPPED as Ecount, 'Dropped' as type
from findpercentage

UNION

SELECT DELIVERED as Ecount, 'Delivered' as type
from findpercentage

UNION

SELECT PROCESSED as Ecount, 'Processed' as type
from findpercentage

UNION


SELECT SPAMREPORT as Ecount, 'Spamreport' as type
from findpercentage



</cfquery>




<cfdump var="#piechartdisplay#">













</body>
</html>