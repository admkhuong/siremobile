﻿<cfcomponent>
	
	<cffunction name="generateSignature" returnformat="json" access="remote" output="false">
		<cfargument name="verb" required="true" type="string" default="GET" hint="verb" />
		<cfargument name="secretKey" required="true" type="string" default="" />
		
		<cfset arguments.verb = UCase(arguments.verb)>
		
		<cfset dateTimeString = GetHTTPTimeString(Now())>
		
		<!--- Create a canonical string to send --->
		<cfset cs = "#arguments.verb#\n\n\n#dateTimeString#\n\n\n">
		<cfset signature = createSignature(cs, secretKey)>
		<cfset retval.SIGNATURE = signature>
		<cfset retval.DATE = dateTimeString>
		
		<cfreturn retval>
	</cffunction>


	<cffunction name="HMAC_SHA1" returntype="binary" access="private" output="false" hint="NSA SHA-1 Algorithm">
		<cfargument name="signKey" type="string" required="true" />
		<cfargument name="signMessage" type="string" required="true" />
		
		<cfset var jMsg = JavaCast("string",arguments.signMessage).getBytes("iso-8859-1") />
		<cfset var jKey = JavaCast("string",arguments.signKey).getBytes("iso-8859-1") />
		<cfset var key = createObject("java","javax.crypto.spec.SecretKeySpec") />
		<cfset var mac = createObject("java","javax.crypto.Mac") />
		<cfset key = key.init(jKey,"HmacSHA1") />
		<cfset mac = mac.getInstance(key.getAlgorithm()) />
		<cfset mac.init(key) />
		<cfset mac.update(jMsg) />
		<cfreturn mac.doFinal() />
	</cffunction>


	<cffunction name="createSignature" returntype="string" access="public" output="false">
		<cfargument name="stringIn" type="string" required="true" />
		<cfargument name="scretKey" type="string" required="true" />
		
		<!--- Replace "\n" with "chr(10) to get a correct digest --->
		<cfset var fixedData = replace(arguments.stringIn,"\n","#chr(10)#","all")>
		<!--- Calculate the hash of the information --->
		<cfset var digest = HMAC_SHA1(scretKey,fixedData)>
		<!--- fix the returned data to be a proper signature --->
		<cfset var signature = ToBase64("#digest#")>
		<cfreturn signature>
	</cffunction>

	<cffunction name="ReadFileContent" returnformat="json" access="remote" output="false">
		<cfargument name="path" required="yes" type="string"/>
		<cfset filePath = expandPath(path) />
		<cffile action = "read" 
			file = "#filePath#"
			variable = "Message">
		<cfreturn Message>
	</cffunction>

</cfcomponent>