<html>
  <head>
    <script src="/session/sire/assets/global/plugins/jquery.min.js?_=3.44.5" type="text/javascript"></script>
    <script src="/session/sire/assets/global/plugins/bootstrap/js/bootstrap.min.js?_=3.44.5" type="text/javascript"></script>
    <script src="/public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js?_=3.44.5" type="text/javascript"></script>
    <script src="/session/sire/assets/global/plugins/js.cookie.min.js?_=3.44.5" type="text/javascript"></script>
    <script src="/session/sire/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js?_=3.44.5" type="text/javascript"></script>
    <script src="/session/sire/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js?_=3.44.5" type="text/javascript"></script>
    <script src="/session/sire/assets/global/plugins/jquery-mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js?_=3.44.5" type="text/javascript"></script>
    <script src="/session/sire/assets/global/plugins/jquery.blockui.min.js?_=3.44.5" type="text/javascript"></script>
    <script src="/session/sire/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js?_=3.44.5" type="text/javascript"></script>
    <script src="/session/sire/assets/global/plugins/bootstrap-select/js/bootstrap-select.js?_=3.44.5" type="text/javascript"></script>
    <script src="/public/sire/js/bootbox.min.js?_=3.44.5"></script>
    <script src="/public/sire/js/jquery.mask.min.js?_=3.44.5" type="text/javascript"></script>
    <script src="/session/sire/assets/pages/scripts/signout.js?_=3.44.5" type="text/javascript"></script>
    <script src="/session/sire/js/site/menu.js?_=3.44.5"></script>
    <script src="/session/sire/js/site/sign_in.js?_=3.44.5"></script>
    <script src="/session/sire/js/site/common.js?_=3.44.5"></script>
    <script src="/public/sire/js/jquery.validationEngine.en.js?_=3.44.5"></script>
    <script src="/public/sire/js/jquery.validationEngine.js?_=3.44.5"></script>
    <script async defer src="https://apis.google.com/js/api.js"></script>


  </head>
  <body>


       <script type="text/javascript">
    // https://developers.google.com/google-apps/calendar/v3/reference/events
    // status  string  Status of the event. Optional. Possible values are:
    // "confirmed" - The event is confirmed. This is the default status.
    // "tentative" - The event is tentatively confirmed.
    // "cancelled" - The event is cancelled.


    //https://developers.google.com/google-apps/calendar/v3/reference/events/list

    var clientId = '20348249418-cpq9052uguml07j3evagt0f2mv7fejb6.apps.googleusercontent.com';
    var apiKey = 'AIzaSyC4Hel1V9G57UDYm7068M2xlRkcPwlPb4c';
    var eventObj =  { 'eventName':"", 'startDate':"", 'endDate':"", 'status':""}
    var listEvent = new Array();
    var nextPageToken = '';
    var listGoogleId = [];

    var options = {
      'calendarId': 'primary',
      'singleEvents': true,
      'maxResults':100,
      //'pageToken': 'CigKGjMzNzFyZGwwNGEyNGt1cDY4bG9jdjgybnN0GAEggICAit_724QWGg0IABIAGMDZw_3cqdgC',
      //'timeMin': today.toISOString(),
    }

  $(function() {
      function handleClientLoad() {
        // Loads the client library and the auth2 library together for efficiency.
        // Loading the auth2 library is optional here since `gapi.client.init` function will load
        // it if not already loaded. Loading it upfront can save one network request.
        gapi.load('client:auth2', initClient);
      }

      function initClient() {
        // Initialize the client with API key and People API, and initialize OAuth with an
        // OAuth 2.0 client ID and scopes (space delimited string) to request access.
        gapi.client.init({
            apiKey: apiKey,
            discoveryDocs: ["https://people.googleapis.com/$discovery/rest?version=v1"],
            clientId: clientId,
            scope: 'https://www.googleapis.com/auth/calendar'
        }).then(function () {
          // Listen for sign-in state changes.
          gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

          // Handle the initial sign-in state.
          updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
        });
      }

      function updateSigninStatus(isSignedIn) {
        // When signin status changes, this function is called.
        // If the signin status is changed to signedIn, we make an API call.
        if (isSignedIn) {
          gapi.client.load('calendar', 'v3',makeApiCall);
          // gapi.client.load('calendar', 'v3').then(function () {

          //   //displayInbox();
          //   makeApiCall();
          // });

        }
        else{
          handleSignInClick();
        }
      }

      function handleSignInClick(event) {
        // Ideally the button should only show up after gapi.client.init finishes, so that this
        // handler won't be called before OAuth is initialized.
        gapi.auth2.getAuthInstance().signIn();
      }

      function handleSignOutClick(event) {
        gapi.auth2.getAuthInstance().signOut();
      }

      function getEventsList(){

      }


      function makeApiCall(nextPageToken) {
          var listEvent = [];
          //return new Promise(function(re){
            if (nextPageToken!='') {
              options.pageToken = nextPageToken;
            }

            console.log(1);

            var request = gapi.client.calendar.events.list(options);
            request.then(function(response){
                var resp = response.result;
                nextPageToken = resp.nextPageToken;
                if(resp.items.length > 0) 
                {
                  for (var i = 0; i < resp.items.length; i++) 
                  {
                      eventObj = {};

                      var checkexist = listGoogleId.replace(resp.items[i].id, "@@@@@@");
                      var findGoogleId = checkexist.split("@@@@@@");
                      
                      eventObj['id'] = resp.items[i].id;
                      eventObj['eventName'] = resp.items[i].summary;
                      eventObj['AllDayFlag'] = 0;
                      if(resp.items[i].start.dateTime){
                        var startdate = resp.items[i].start.dateTime;
                        startdate = startdate.substr(0,19);
                        eventObj['startDate'] = startdate;
                      }
                      if(resp.items[i].start.dateTime){
                        var enddate = resp.items[i].end.dateTime;
                        enddate = enddate.substr(0,19);
                        eventObj['endDate'] =  enddate;
                      }
                      eventObj['ConfirmationFlag'] = 1;
                      eventObj['status'] = resp.items[i].status;
                      if(resp.items[i].start.dateTime){
                        console.log(resp.items[i].id +" Check"+ findGoogleId.length);
                        if(findGoogleId.length < 2){
                           listEvent.push(eventObj);
                        }
                      }
                  }
                }
                console.log(listEvent);
                $("#nextPageToken").val(nextPageToken);

                
                if($("#nextPageToken").val()!=''){
                  makeApiCall($("#nextPageToken").val());
                }

                console.log('call API to save data'+JSON.stringify(listEvent));
                if(listEvent.length > 0){
                  $.ajax({
                    type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
                    url: '/session/sire/models/cfc/calendar.cfc?method=AddEventSync&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true' ,   
                    dataType: 'json',
                    async: true,
                    data:  
                    { 			
                      inpEvent : JSON.stringify(listEvent)			
                    },					  
                    error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
                    success:		
                    <!--- Default return function for call back --->
                    function(d) 
                    {																																								
                      <!--- RXRESULTCODE is 1 if everything is OK --->
                      if (parseInt(d.RXRESULTCODE) == 1) 
                      {								
                          alert("Dong bo thanh cong : "+listEvent.length+" events.");
                      }										
                    } 		
                      
                  });
                }
            });
          // console.log(nextPageToken);  
          $("#data").text(JSON.stringify(listEvent));
         
      }

      $("#signin-button").on('click', function(){
        handleClientLoad()
        <!--- Get list Google calendar Id --->
        $.ajax({
          type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
          url: '/session/sire/models/cfc/calendar.cfc?method=GetCalendarEventInfoSync&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true' ,   
          dataType: 'json',
          async: true,
          data: {},					  
          error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },					  
          success:		
          <!--- Default return function for call back --->
          function(d) 
          {																																								
            <!--- RXRESULTCODE is 1 if everything is OK --->
            if (parseInt(d.RXRESULTCODE) == 1) 
            {								
                  listGoogleId = d.GGIDLIST;
            }										
          } 		
            
        });
      })
  });

    </script>


    <input type="text" id="nextPageToken" value=''>

    <button id="signin-button">Sync</button>
    <div id="data"></div>
  </body>
</html>