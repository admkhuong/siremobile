
<cfparam name="url.secretKey">
<cfparam name="url.accessKey">

<!--- generate signature string --->
      <cffunction name="generateSignature" returnformat="json" access="remote" output="true">
        <cfargument name="method" required="true" type="string" default="GET" hint="Method" />
        <cfset dateTimeString = GetHTTPTimeString(Now()) />
        <cfset dateTimeString = 'Mon 01 Aug 2016 08:25:46 GMT'>
        <!--- Create a canonical string to send --->
        <cfset cs = "#arguments.method#\n\n\n#dateTimeString#\n\n\n" />
        <cfset signature = createSignature(cs,variables.secretKey) />
        <cfset retval.SIGNATURE = signature />
        <cfset retval.DATE = dateTimeString />
        <cfreturn retval>
      </cffunction>

      <!--- Encrypt with HMAC SHA1 algorithm--->

      <cffunction name="HMAC_SHA1" returntype="binary" access="private" output="true" hint="NSA SHA-1 Algorithm">
        <cfargument name="signKey" type="string" required="true" />
        <cfargument name="signMessage" type="string" required="true" />
        <cfset var jMsg = JavaCast("string",arguments.signMessage).getBytes("iso-8859-1") />
        <cfset var jKey = JavaCast("string",arguments.signKey).getBytes("iso-8859-1") />
        <cfset var key = createObject("java","javax.crypto.spec.SecretKeySpec") />
        <cfset var mac = createObject("java","javax.crypto.Mac") />
        <cfset key = key.init(jKey,"HmacSHA1") />
        <cfset mac = mac.getInstance(key.getAlgorithm()) />
        <cfset mac.init(key) />
        <cfset mac.update(jMsg) />
        <cfreturn mac.doFinal() />
      </cffunction>

      <cffunction name="hmacEncrypt" returntype="binary" access="public" output="false">
         <cfargument name="signKey" type="string" required="true" />
         <cfargument name="signMessage" type="string" required="true" />


         <cfset var jMsg = JavaCast("string",arguments.signMessage).getBytes("iso-8859-1") />
         <cfset var jKey = JavaCast("string",arguments.signKey).getBytes("iso-8859-1") />

         <cfset var key = createObject("java","javax.crypto.spec.SecretKeySpec") />
         <cfset var mac = createObject("java","javax.crypto.Mac") />

         <cfset key = key.init(jKey,"HmacSHA1") />

         <cfset mac = mac.getInstance(key.getAlgorithm()) />
         <cfset mac.init(key) />
         <cfset mac.update(jMsg) />
         <cfreturn mac.doFinal() />
      </cffunction>

      <cffunction name="createSignature" returntype="string" access="public" output="true">
        <cfargument name="stringIn" type="string" required="true" />
        <cfargument name="scretKey" type="string" required="true" />
        <!--- Replace "\n" with "chr(10) to get a correct digest --->
        <cfset var fixedData = replace(arguments.stringIn,"\n","#chr(10)#","all") />
     
        <!--- Calculate the hash of the information --->
        <cfoutput>#scretKey# <br/></cfoutput>
        <cfdump var="#fixedData#"/>


        <cfset var digest = HMAC_SHA1("1091c2F+b59dD076Ea52EF531a245f0Cb9E46d11",fixedData) />
       
        <!--- fix the returned data to be a proper signature --->
        <cfset var signature = toString(ToBase64(digest)) />
        
        <cfdump var="#signature#">
        <cfreturn signature/>
      </cffunction>


<cfset accessKey = '8A8C2A51BC345F301417' />
<cfset secretKey = '1091c2F+b59dD076Ea52EF531a245f0Cb9E46d11' />

<cfset sign = generateSignature("POST", secretKey) /> <!---Verb must match that of request type--->


<cfset datetime = sign.DATE>
<cfset signature = sign.SIGNATURE>
<cfset authorization = accessKey & ":" & signature>
<cfdump var="#sign#">

<cfsavecontent variable="fixedData">POST


Mon 01 Aug 2016 08:25:46 GMT


</cfsavecontent>



<cfset result = hmacEncrypt("1091c2F+b59dD076Ea52EF531a245f0Cb9E46d11", fixedData)>
<cfset result2 = HMAC_SHA1("1091c2F+b59dD076Ea52EF531a245f0Cb9E46d11", fixedData)>

<cfset x1 = toString(tobase64(result))>
 <cfset x2 =  toString(tobase64(result2))>

<cfoutput>#x1# <br/></cfoutput>
<cfoutput>#x2#</cfoutput>






