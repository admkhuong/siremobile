<!---
  --- fpldip
  --- ------
  ---
  --- author: ahasan
  --- date:   4/7/15
  --- component for dipping for FPL
  --->
<cfcomponent accessors="true" output="false" persistent="true">


	<!--- MB Account info for voxtelesys --->
    <cfproperty name="ServiceLogin" type="string" default="90020">
    <cfproperty name="ServicePassword" type="string" default="81E4EAE6-48B7-4267-B787-C04E8BF2C63B">

    <!---API urls at voxtelesys--->
    <cfproperty name="URL1" type="string" default="https://ops-srvphp01.voxtelesys.net/numberLookup.php">
    <cfproperty name="URL2" type="string" default="https://ops-srvphp02.voxtelesys.net/numberLookup.php">

    <!---local error var--->
    <cfproperty name="error" type="string" default="">

	<!---initialize component and return a instance--->
	<cffunction name="init" returntype="fpldip">
    	<cfreturn this>
    </cffunction>

    <!--- get information about cell phone --->
    <cffunction name="getPhoneInfo" access="remote" output="true" returnformat="json">
	    <cfargument name="phoneNumber" type="string" required="true">

        <!---initialize the return struct--->
        <cfset var sReturn = structNew()>
        <cfset var strURL = "">

        <cfset structInsert(sReturn,'error','')>
        <cfset structInsert(sReturn,'origPhoneNumber',arguments.phoneNumber)>
        <cfset structInsert(sReturn,'numberType','UNKNOWN')>

        <cfset setError('')>

       <cftry>

       		<!---alternate b/w server 1 & 2--->
       		<cfif second(now()) mod 2 EQ 1>
            	<cfset strURL = this.getURL1()>
            <cfelse>
            	<cfset strURL = this.getURL2()>
            </cfif>

        	<!---construct soap request--->
            <cfsavecontent variable="soapBody">
            <cfoutput>
            <?xml version="1.0" encoding="UTF-8"?>
                <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                xmlns:urn="urn:vtsPhoneNumberLookup">
                <soapenv:Header/>
                    <soapenv:Body>
                        <urn:numberLookup soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                            <phoneNumber xsi:type="xsd:string">#arguments.phoneNumber#</phoneNumber>
                            <userName xsi:type="xsd:string">#this.getServiceLogin()#</userName>
                            <password xsi:type="xsd:string">#this.getServicePassword()#</password>
                        </urn:numberLookup>
                    </soapenv:Body>
                </soapenv:Envelope>
            </cfoutput>
            </cfsavecontent>

			<!---make https call to voxtelesys--->
			<cfhttp url="#strURL#" method="post" result="httpResponse">
				<cfhttpparam type="header"	name="Content-Type" value="text/xml" />
             	<cfhttpparam type="header"	name="SOAPAction" value="urn:vtsPhoneNumberLookup##phoneNumber" />
             	<cfhttpparam type="xml" value="#trim(soapBody)#"/>
            </cfhttp>

			<cfdump var="#httpResponse#">
			<cfabort>


			<cfif find( "200", httpResponse.statusCode )>
                <cfset soapResponse = xmlParse( httpResponse.fileContent ) />
                <cfset responseNodes = xmlSearch(soapResponse,"//*[ local-name() = 'numberType' ]") />
                <cfif arrayLen(responseNodes) GT 0>
                	<cfset sReturn.numberType = responseNodes[1].XmlText>
                </cfif>
			<cfelse>
            	<cfset setError('Error occured while dipping: ' & httpResponse.statusCode)>
            </cfif>

            <cfcatch type="any">
            	<cfset setError('Error occured while dipping: ' & cfcatch.Message)>
            </cfcatch>
		</cftry>

		<cfset sReturn.error = getError()>

        <cfcontent type="application/json" reset="yes">
        <cfreturn serializeJSON(sReturn)>
    </cffunction>

	<!---function to return true or false for cell--->
	<cffunction name="isCell" access="remote" output="false" returnformat="json">
		<cfargument name="phoneNumber" type="string" required="true">

        <cfset var sResult = structNew()>

        <cfset var sReturn = structNew()>
        <cfset structInsert(sReturn,'error','')>
        <cfset structInsert(sReturn,'origPhoneNumber',arguments.phoneNumber)>
        <cfset structInsert(sReturn,'isCell','')>

        <cfset setError('')>

        <cfset sResult = deSerializeJSON(this.getPhoneInfo(arguments.phoneNumber))>

        <cfif getError() EQ "" AND structKeyExists(sResult,'numberType')>

        	<cfswitch expression="#UCase(sResult.numberType)#">
	            <cfcase value="CELLULAR">
                	<cfset sReturn.isCell = True>
                </cfcase>
	            <cfcase value="LANDLINE">
                	<cfset sReturn.isCell = False>
                </cfcase>
                <cfdefaultcase>

                </cfdefaultcase>
            </cfswitch>

		<cfelse>
        	<cfset sReturn.error = getError()>
        </cfif>


		<!---Lot it into table--->
		<!--- <cfset logServiceCall(serviceType="DIP", serviceData = serializeJSON(sReturn))> --->

		<cfcontent type="application/json" reset="yes">
		<cfreturn serializeJSON(sReturn)>
    </cffunction>


</cfcomponent>