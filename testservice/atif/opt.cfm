<h1>Atif Test</h1>

<cfinclude template="../../cfc/csc/constants.cfm" >

    <cfset OptOut(inpContactString='9492786295',inpShortCode='69375',inpBatchId='1374')>

	<!---get all the opt activity--->
	<cffunction name="GetOpts" access="remote" output="true" hint="Get all the opt activity for multiple filters">
		<cfargument name="inpShortCode" TYPE="numeric" default=0 required="no"/>
        <cfargument name="inpContractString" TYPE="string" default="" required="no"/>
        <cfargument name="inpBatchId" TYPE="numeric" default=0 required="no"/>
        <cfargument name="inpUserId" TYPE="numeric" default=0 required="no"/>

		<!---get opts--->
		<cfquery name = "getOpts" dataSource = "#Session.DBSourceEBM#"> 
		    SELECT 	*
		    FROM	simplelists.optinout AS oi 
		    WHERE	1=1 
            		<cfif inpShortcode NEQ 0>AND oi.ShortCode_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpShortCode#"></cfif>
            		<cfif inpContractString NEQ "">AND oi.ContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContractString#"></cfif>
            		<cfif inpBatchId NEQ 0>AND oi.BatchId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.inpBatchId#"></cfif>
                    <cfif inpUserId NEQ 0>AND oi.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#"></cfif>
			ORDER BY  oi.optId_int desc
		</cfquery>

		<cfreturn getOpts />
	</cffunction>
    
        <!---gets a contact string and other optional parameters and return a opt structures with following possible values 
    opt.OptStatus"] = U, O, I  (U=unknown, O=out, I=in)--->
	<cffunction name="GetOptStatus" access="remote" output="true" hint="Get latest status of opt-in or opt-out" returntype="struct">
        <cfargument name="inpContractString" TYPE="string" required="yes"/>    
		<cfargument name="inpShortCode" TYPE="numeric" required="yes"/>
        <cfargument name="inpBatchId" TYPE="numeric" default=0 required="no"/>
        <cfargument name="inpUserId" TYPE="numeric" default=0 required="no"/>
        
        <cfset var opt = {}>
        <cfset opt.OptStatus = "U"> <!--- initialize with status U i.e. unknowin--->
        <cfset opt.OptDate =  now()>
        <cfset opt.OptSoruce = "">
        
        <!---get opts--->
        <cfset qOpts = GetOpts(argumentCollection=arguments)>
        
        
        <cfif qOpts.recordcount GT 0> 
        	<!---opt in if date is avaialble--->
        	<cfif isDate(qOpts.OptIn_dt)>
		        <cfset opt.OptStatus = "I">
				<cfset opt.OptDate = qOpts.OptIn_dt>
                <cfset opt.OptSoruce = qOpts.OptInSource_vch>
			<cfelseif isDate(qOpts.Optout_dt)> <!---opt out if opt out date is avaialble--->
		        <cfset opt.OptStatus = "O">
				<cfset opt.OptDate = qOpts.Optout_dt>		
                <cfset opt.OptSoruce = qOpts.OptOutSource_vch>                	
            </cfif>
        
        </cfif>

		<cfreturn opt />
	</cffunction>
    
    <!---method to optIn. This method make use of AddOp too--->
    <cffunction name="OptIn" access="remote" output="true" hint="OptIn a phone number or email">
        <cfargument name="inpContactString" type="string" required="yes"/>
        <cfargument name="inpShortCode" type="string" required="yes"/>        
        <cfargument name="inpUserId" type="numeric" default=0 required="no"/>
        <cfargument name="inpOptIn" type="string" default="#now()#" required="no"/>        
        <cfargument name="inpOptInSource" type="string" default="MO Confirm Opt In" required="no"/>
        <cfargument name="inpFromAddress" type="string" default="" required="no"/>
        <cfargument name="inpCallerId" type="string" default="" required="no"/>
        <cfargument name="inpBatchId" type="numeric" default=0 required="no"/>    

        
        <cftransaction action="begin">
        
        	<cftry>
        
				<!---Update old opt out if any--->
                <cfquery name="UpdateOldOptOut" datasource="#Session.DBSourceEBM#" >                            
                    UPDATE
                        simplelists.optinout
                    SET
                        OptIn_dt = <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.inpOptIn#">
                    WHERE
                        ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpShortCode#">
                    AND 
                        OptIn_dt IS NULL
                    AND
                        (
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(arguments.inpContactString)#">
                            OR
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(arguments.inpContactString)#">
                        )                                                         	            	    	
                </cfquery>
        
        		<!---add new opt-in record in table--->
                <cfset dataout =  AddOpt(argumentCollection=arguments)>
                
                <!---commit or roll back depending on result from add record function--->
                <cfif dataout.RXRESULTCODE EQ "1">
                    <cftransaction action="commit">
                <cfelse>
                    <cftransaction action="rollback">
                </cfif>
                
                <cfcatch type="any">

					<cfset dataout.RXRESULTCODE = "-1" />
                    <cfset dataout.NEWID =  "" />               
                    <cfset dataout.TYPE = cfcatch.TYPE />
                    <cfset dataout.MESSAGE = cfcatch.MESSAGE />
                    <cfset dataout.ERRMESSAGE = cfcatch.detail />                  
                
                </cfcatch>
			</cftry>
	            
		</cftransaction>            

		<cfreturn dataout>      
    
    </cffunction>
    
    
    
    <!---method to optIn. This method make use of AddOp too--->
    <cffunction name="OptOut" access="remote" output="true" hint="OptIn a phone number or email">
        <cfargument name="inpContactString" type="string" required="yes"/>
        <cfargument name="inpShortCode" type="string" required="yes"/>        
        <cfargument name="inpOptOut" type="string" default="#now()#" required="no"/>        
        <cfargument name="inpOptOutSource" type="string" default="MO Confirm Opt In" required="no"/>
        <cfargument name="inpBatchId" type="numeric" default=0 required="no"/>    

        
        <cftransaction action="begin">
        
        	<cftry>
            
            	<!---If no batchId is provided, try to get the last batch ID--->
				<cfif arguments.inpBatchId EQ 0>
                
                    <cfquery name="GetLastBatchSent" datasource="#Session.DBSourceEBM#" >
                        SELECT 
                            BatchId_bi
                        FROM
                            simplexresults.contactresults 
                        WHERE 
                            SMSCSC_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpShortCode#">
                        AND
                            (
                                ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(arguments.inpContactString)#">
                            OR
                                ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(arguments.inpContactString)#">
                            )   
                        AND
                            BatchId_bi <> 0
                        ORDER BY
                            MasterRXCallDetailId_int DESC 
                        LIMIT 1                  	            	    	
                    </cfquery>   
                
					<cfif GetLastBatchSent.RecordCount GT 0>                	
                        <cfset arguments.inpBatchId = "#GetLastBatchSent.BatchId_bi#" /> 
                    </cfif>
                    
				</cfif>
                
                
                <!--- Terminate any intervals queued up --->                          
                <cfquery name="UpdateSMSQueue" datasource="#Session.DBSourceEBM#" >
                    UPDATE
                        simplequeue.moinboundqueue
                    SET
                        Status_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SMSQCODE_STOP#">                
                    WHERE
                       Status_ti IN (#SMSQCODE_QA_TOOL_READYTOPROCESS#, #SMSQCODE_READYTOPROCESS#)
                    AND
                       (
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(arguments.inpContactString)#">
                        OR
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(arguments.inpContactString)#">
                        )  
                    AND
                        ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpShortCode#">                   
                </cfquery>
                            
                <!--- Terminate any in Queue MT from this short code to --->
                <cfquery name="UpdateOutboundQueue" datasource="#Session.DBSourceEBM#" >
                    UPDATE
                        simplequeue.contactqueue
                    SET
                        DTSStatusType_ti = 100                
                    WHERE
                        DTSStatusType_ti IN (#SMSOUT_PAUSED#, #SMSOUT_QUEUED#)
                    AND
                       (
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(arguments.inpContactString)#">
                        OR
                            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(arguments.inpContactString)#">
                        )  
                    AND
                        ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpShortCode#">                   
                </cfquery>                 
                    
			  	<!--- Update contact as opt out on this short code --->
                 <cfquery name="InsertSMSStopRequest" datasource="#Session.DBSourceEBM#">
                    INSERT INTO sms.SMSStopRequests
					( 
                    	ShortCode_vch, 
                       	ContactString_vch, 
                        Created_dt 
					)
                    VALUES
					( 
                    	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpShortCode#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpContactString#">, 
                        <cFQUERYPARAM cfsqltype="cf_sql_timestamp" VALUE="#arguments.inpOptOut#">
					)
                </cfquery>                                    
            
        		<!---add new opt-in record in table--->
                <cfset dataout =  AddOpt(argumentCollection=arguments)>
                
                <!---commit or roll back depending on result from add record function--->
                <cfif dataout.RXRESULTCODE EQ "1">
                    <cftransaction action="commit">
                <cfelse>
                    <cftransaction action="rollback">
                </cfif>
                
                <cfcatch type="any">

					<cfset dataout.RXRESULTCODE = "-1" />
                    <cfset dataout.NEWID =  "" />               
                    <cfset dataout.TYPE = cfcatch.TYPE />
                    <cfset dataout.MESSAGE = cfcatch.MESSAGE />
                    <cfset dataout.ERRMESSAGE = cfcatch.detail />                  
                
                </cfcatch>
			</cftry>
	            
		</cftransaction>            

		<cfdump var="#dataout#">

		<cfreturn dataout>      
    
    </cffunction>    
    

	<!--- method to add record in opt table. This could be OptIn or Optout record --->    
	<cffunction name="AddOpt" access="remote" output="true" hint="Add a new opt record">
        <cfargument name="inpContactString" type="string" required="yes"/>
        <cfargument name="inpUserId" type="numeric" default=0 required="no"/>
        <cfargument name="inpOptOut" type="string" default="" required="no"/>
        <cfargument name="inpOptIn" type="string" default="" required="no"/>
        <cfargument name="inpOptOutSource" type="string" default="" required="no"/>
        <cfargument name="inpOptInSource" type="string" default="" required="no"/>
        <cfargument name="inpShortCode" type="string" default="" required="no"/>
        <cfargument name="inpFromAddress" type="string" default="" required="no"/>
        <cfargument name="inpCallerId" type="string" default="" required="no"/>
        <cfargument name="inpBatchId" type="numeric" default=0 required="no"/>    
        

		<!--- Set default to error in case later processing goes bad --->        
		<cfset var dataout = {} />         
		<cfset NextOptId = -1>                
        
        <cftry>

			<!--- Validate session still in play - handle gracefully if not --->
            <cfif Session.USERID GT 0>
            
            	<!---Make sure that at least one of optin or optout date is provided else generate error---> 
                <cfif NOT isDate(arguments.inpOptOut) AND NOT isDate(arguments.inpOptIn)>
                	<cfthrow type="data" message="Invalid Opt-In/Opt-out dates" detail="You must provide at least one valid Opt-In or Opt-Out dates">
                </cfif>
        
        		<!---everything is good execute insert query--->
                <cfquery name = "AddOpt" dataSource = "#Session.DBSourceEBM#" result="AddOptResult">
                    INSERT INTO simplelists.optinout
                    (
                        UserId_int,
                        ContactString_vch,
                        OptOut_dt,
                        OptIn_dt,
                        OptOutSource_vch,
                        OptInSource_vch,
                        ShortCode_vch,
                        FromAddress_vch,
                        CallerId_vch,
                        BatchId_bi
                    )
                    VALUES
                    (
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#">,
                        <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.inpOptOut#" null="#IIF(not isdate(arguments.inpOptOut),DE('Yes'),DE('No'))#">,
                        <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.inpOptIn#" null="#IIF(not isdate(arguments.inpOptIn),DE('Yes'),DE('No'))#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpOptOutSource#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpOptInSource#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpShortCode#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpFromAddress#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpCallerId#">,
                        <cfqueryparam cfsqltype="cf_sql_bigint" value="#arguments.inpBatchId#">
                    );
                </cfquery>
                
               	<!--- insert successful. get the next id and populate out data query --->

				<cfset NextOptId = AddOptResult.GENERATEDKEY>
                
				<cfset dataout.RXRESULTCODE = "1" />
				<cfset dataout.NEWID =  NextOptId />               
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "" />
                <cfset dataout.ERRMESSAGE = "" />                
                
			<cfelse>
            
				<cfset dataout.RXRESULTCODE = "-2" />
				<cfset dataout.NEWID =  "" />               
                <cfset dataout.TYPE = "-2" />
                <cfset dataout.MESSAGE = "Session Expired! Refresh page after logging back in." />
                <cfset dataout.ERRMESSAGE = "" />                            
            
            </cfif>                
        
        
        	<cfcatch type="any">
            
				<cfset dataout.RXRESULTCODE = "-1" />
				<cfset dataout.NEWID =  "" />               
                <cfset dataout.TYPE = cfcatch.TYPE />
                <cfset dataout.MESSAGE = cfcatch.MESSAGE />
                <cfset dataout.ERRMESSAGE = cfcatch.detail />                                        
            
            </cfcatch>
        
        </cftry>     
        
		<cfreturn dataout>
            
    </cffunction>