<cfparam name="smsnumber" default="">
<cfparam name="smsmessage" default="">

<cfdump var="#form#">

	<!---FPL company admin--->
    <cfset variables.accessKey = '04CE566E85D5E697BB49' />
    <cfset variables.secretKey = '090617/A35EEa8BBE2EDd34544010605CEF2862e' />
    
    <!---FPL qa--->
    <cfset variables.accessKey = '66D8F53DE9B97F11E689' />
    <cfset variables.secretKey = '57166/51960116+DFCc5B444F518C571c9/C1cdD' />
    
    
    
    <CFFORM name="frmmain" action="https://ebmui.com/session/tests/atif/sendsingle.cfm" method="get" format="html">
    
    	<table cellpadding="20" cellspacing="10">
        <tr>
        	<td align="right">Short Code:</td>
			<td>69375</td>
        </tr>
    	<tr>
    		<td align="right">SMS Number:</td>
            <td><cfinput type="text" name="smsnumber" required="yes" message="- SMS Number is missing" value="#smsnumber#" label="SMS Number" ></td>
		</tr>            
        <tr>
    		<td align="right">SMS Message:</td>
            <td><cfinput type="text" name="smsmessage" required="yes" message="- SMS Message is missing" value="#smsmessage#" label="SMS Message" size="100"></td>
        </tr>
        <tr>
        	<td></td>
            <td><cfinput type="submit" value="Send SMS" name="cmdsubmit"></td>
        </tr>
        </table>
    
    </CFFORM>
    
    
    <cfif smsnumber NEQ "">
		<cfset variables.sign = generateSignature("POST") /> <!---Verb must match that of request type--->
        <cfset datetime = variables.sign.DATE>
        <cfset signature = variables.sign.SIGNATURE>
        
        <cfset authorization = variables.accessKey & ":" & signature>
    
   
        <cfhttp url="http://ebmui.com/webservice/ebm/pdc/addtorealtime" method="POST" result="returnStruct" >
        
         
           <!--- Required components --->
        
           <!--- By default EBM API will return json or XML --->
           <cfhttpparam name="Accept" type="header" value="application/json" />    	
           <cfhttpparam type="header" name="datetime" value="#datetime#" />
           <cfhttpparam type="header" name="authorization" value="#authorization#" /> 
                         
           <!--- Batch Id controls which pre-defined campaign to run --->
           <cfhttpparam type="formfield" name="inpBatchId" value="1404" />
           
           <!--- Contact string--->
           <cfhttpparam type="formfield" name="inpContactString" value="#smsnumber#" />
           
           <!--- 1=voice 2=email 3=SMS--->
           <cfhttpparam type="formfield" name="inpContactTypeId" value="3" />
           
           
           
           <!--- Optional Components --->
           
           <!--- Custom data element for PDC Batch Id 1135 --->
           <cfhttpparam type="formfield" name="inpCustomSMS" value="#smsmessage#" />
          
          
          
        </cfhttp>
        
        <cfdump var="#DeserializeJSON(returnStruct.Filecontent)#">        
        
        <!---<cfdump var="#returnStruct#"> --->     
        
        <cfoutput>
        authorization= #authorization#<hr>
        signature= #signature#<hr>
        accessKey= #accessKey#<hr />
        secretKey= #secretKey#<hr />
        datetime= #datetime#<hr />
        </cfoutput>
        
        
        
	</cfif>        





	<!--- generate signature string --->
	<cffunction name="generateSignature" returnformat="json" access="remote" output="false">
		<cfargument name="method" required="true" type="string" default="GET" hint="Method" />
                 
		<cfset dateTimeString = GetHTTPTimeString(Now())>
		<!--- Create a canonical string to send --->
		<cfset cs = "#arguments.method#\n\n\n#dateTimeString#\n\n\n">
		<cfset signature = createSignature(cs,variables.secretKey)>
		
		<cfset retval.SIGNATURE = signature>
		<cfset retval.DATE = dateTimeString>
		
		<cfreturn retval>
	</cffunction>
	
	<!--- Encrypt with HMAC SHA1 algorithm--->
	<cffunction name="HMAC_SHA1" returntype="binary" access="private" output="false" hint="NSA SHA-1 Algorithm">
	   <cfargument name="signKey" type="string" required="true" />
	   <cfargument name="signMessage" type="string" required="true" />
	
	   <cfset var jMsg = JavaCast("string",arguments.signMessage).getBytes("iso-8859-1") />
	   <cfset var jKey = JavaCast("string",arguments.signKey).getBytes("iso-8859-1") />
	   <cfset var key = createObject("java","javax.crypto.spec.SecretKeySpec") />
	   <cfset var mac = createObject("java","javax.crypto.Mac") />
	
	   <cfset key = key.init(jKey,"HmacSHA1") />
	   <cfset mac = mac.getInstance(key.getAlgorithm()) />
	   <cfset mac.init(key) />
	   <cfset mac.update(jMsg) />
	
	   <cfreturn mac.doFinal() />
	</cffunction>
	
	
	<cffunction name="createSignature" returntype="string" access="public" output="false">
	    <cfargument name="stringIn" type="string" required="true" />
		<cfargument name="scretKey" type="string" required="true" />
		<!--- Replace "\n" with "chr(10) to get a correct digest --->
		<cfset var fixedData = replace(arguments.stringIn,"\n","#chr(10)#","all")>
		<!--- Calculate the hash of the information --->
		<cfset var digest = HMAC_SHA1(scretKey,fixedData)>
		<!--- fix the returned data to be a proper signature --->
		<cfset var signature = ToBase64("#digest#")>
		
		<cfreturn signature>
	</cffunction>