<cfparam name="fa" default="">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Unica File Processing</title>
</head>

<h1><a href="index.cfm">UNICA FILE PROCESSING</a></h1>

<ul>
	<li><a href="index.cfm?fa=getfiles">1) Get files from Unica Server</a></li>
	<li><a href="index.cfm?fa=processfiles">2) Process New Files (transform from CSV to DB)</a></li>
	<li><a href="index.cfm?fa=sendsms">3) Send SMS to newly imported data</a></li>        
    <li><a href="index.cfm?fa=exportresponse">4) Export Response to files</a></li>        
</ul>

<body>
</body>
</html>


<cfoutput><h1>Action: #fa#</h1></cfoutput>

<cfswitch expression="#fa#">

    <cfcase value="getfiles">
        <cfinclude template="unica_get_files_ebm.cfm">
    </cfcase>
    
    <cfcase value="processfiles">
        <cfinclude template="unica_process_files_ebm.cfm">
    </cfcase>

    <cfcase value="sendsms">
        <cfinclude template="unica_send_msg_ebm.cfm">
    </cfcase>
    
    <cfcase value="exportresponse">
        <cfinclude template="unica_export_response_ebm.cfm">    
    </cfcase>
    

</cfswitch>