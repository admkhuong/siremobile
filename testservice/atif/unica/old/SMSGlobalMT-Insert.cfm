<!--- Place This File Prior To Sending SMS Message --->

<!--- Insert SMS Message Into Database --->
	<!---
	CompanyName
	Profile
	ShortCode
	ServiceID
	SentDate
	SentMessage
	SMSNumber
	--->
	
   <!--- <cfset CompanyName = "FPLQA_700800">--->
    <!---<cfset Profile = >
    <cfset ShortCode = >--->
 <!---   <cfset ServiceID = "56484">
    <cfset SentMessage = #SentMessage#>
    <cfset SMSNumber = right(OriginatingNumber,10)>--->
    <cfset bnDate = Now()>
    <cfset SentDate = DateConvert("Local2UTC", bnDate)>
    
    
    <cfquery name="InsertGlobalSMS" datasource="MBASPSQL2K" result="getMaxId">
    	INSERT INTO
        	SMS..SMSGlobalDetails
        (
        CompanyName,
        Profile,
        ShortCode,
        ServiceID,
        SentDate,
        SentMessage,
        SMSNumber
        )
        values
        (
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#CompanyName#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#Profile#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#ShortCode#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#ServiceID#">,
        #SentDate#,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#SentMessage#">,
        <cfqueryparam cfsqltype="cf_sql_varchar" value="#SMSNumber#">
        )
    </cfquery>
    
<!--- Retrieve the Primaray key _ BigInt (SMSGlobalMessageID) --->
	
	<cfset MaxSMSGlobalIDNumber = getMaxId.IDENTITYCOL>
    
    <!---update unica table--->
    <cfquery name="InsertUnicaFileLink" datasource="MBASPSQL2K">
    	UPDATE FPL..UNICAFiles
        SET		[SMSGlobalMessageID] =  <cfqueryparam cfsqltype="cf_sql_bigint" value="#MaxSMSGlobalIDNumber#">
        WHERE	PKId = #qFileData.PKId#
    </cfquery>    
    
    

<!--- Parse Primary Key For BatchID and SequenceNumber --->
	
	<!--- SequenceNumber Max Value: 99,999 --->
    <cfif len(MaxSMSGlobalIDNumber) gt 5>
    	<cfset SequenceNumber = numberFormat(right(MaxSMSGlobalIDNumber,5),"00000")>
    <cfelse>
    	<cfset SequenceNumber = numberFormat(MaxSMSGlobalIDNumber,"00000")>
    </cfif>
	
	
	<!--- BatchID Max Value: 99,999,999 --->
	
	<cfif len(MaxSMSGlobalIDNumber) gt 5>
    	<cfset BatchCount = len(MaxSMSGlobalIDNumber)>
    	<cfset BatchID = left(MaxSMSGlobalIDNumber,(BatchCount - 5))>
    <cfelse>
    	<cfset BatchID = '0'>
    </cfif>
	
	
    <!---9,999,999,999,999 total numbers --->