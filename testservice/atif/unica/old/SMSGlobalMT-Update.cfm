<!--- Place This File After Sending SMS Message For Response --->
<cftry>
	
	<cfif IsXML(smsreturn.FileContent)>
    
		<cfset XMLData = XmlParse(smsreturn.FileContent)>  
        <cfset XMLHead = XMLData.NotificationRequestResult >
        
        <cfset RequestResultCode = trim(XMLHead.NotificationResultHeader.RequestResultCode.xmltext)>
        <cfset RequestResultText = trim(XMLHead.NotificationResultHeader.RequestResultText.xmltext)>
        <cfset NotificationResultCode = trim(XMLHead.NotificationResultList.NotificationResult.NotificationResultCode.xmltext)>
        <cfset NotificationResultText = trim(XMLHead.NotificationResultList.NotificationResult.NotificationResultText.xmltext)>
        <cfset SubscriberResultCode = trim(XMLHead.NotificationResultList.NotificationResult.SubscriberResult.SubscriberResultCode.xmltext)>
        <cfset SubscriberResultText = trim(XMLHead.NotificationResultList.NotificationResult.SubscriberResult.SubscriberResultText.xmltext)>
        
        
        <cfquery name="UpdateGlobalSMS" datasource="MBASPSQL2K">
            UPDATE
                SMS..SMSGlobalDetails
            SET
                BatchID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#BatchID#">,
                SequenceNumber = <cfqueryparam cfsqltype="cf_sql_varchar" value="#numberFormat(SequenceNumber,'00000')#">,
                RequestResultCode = <cfqueryparam cfsqltype="cf_sql_varchar" value="#RequestResultCode#">,
                RequestResultText = <cfqueryparam cfsqltype="cf_sql_varchar" value="#RequestResultText#">,
                NotificationResultCode = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NotificationResultCode#">,
                NotificationResultText = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NotificationResultText#">,
                SubscriberResultCode = <cfqueryparam cfsqltype="cf_sql_varchar" value="#SubscriberResultCode#">,
                SubscriberResultText = <cfqueryparam cfsqltype="cf_sql_varchar" value="#SubscriberResultText#">
            WHERE
                SMSGlobalMessageID = <cfqueryparam cfsqltype="cf_sql_bigint" value="#MaxSMSGlobalIDNumber#">
        </cfquery>
    
    </cfif>

	<cfcatch type="any">
    	<cfmail to="ahasan@messagebroadcast.com" from="mbreports@messagebroadcast.com" subject="FPL UNICA - #SMSNumber# Mblox inserts" type="html" > 			<cfdump var="#XMLHead#">
        	<cfdump var="#cfcatch#">
            <cfdump var="#smsreturn#">
        </cfmail>
    </cfcatch>

</cftry>