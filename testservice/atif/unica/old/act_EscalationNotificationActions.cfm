<cfparam name="ENA_Message" default="No Message Specified">
<cfparam name="SubjectLine" default="AT&T Error Notification">
<cfparam name="Developers" default="mbreports@messagebroadcast.com">
<cfparam name="PurgeCountBuff" default="0">
<cfparam name="AlertType" default="1">
<cfparam name="SendEmail" default="0">

<cfscript> 
/** 
* Performs a reverse DNS lookup. 
* 
* @param host      The host name to lookup. (Required) 
* @return Returns an IP address. 
* @author Ben Forta (ben@forta.com) 
* @version 1, November 11, 2002 
*/ 

// Variables
iaclass=""; 
addr=""; 
  
// Init class 
iaclass=CreateObject("java", "java.net.InetAddress"); 
  
// Get address 
addr = iaclass.getByName(host); 
  
// Return the address     
addr = addr.getHostAddress(); 
</cfscript>

<cfif RIGHT(SubjectLine,6) NEQ "DEL408">
	<cfset SubjectLine = SubjectLine & " - DEL408">
</cfif>

<cfoutput>
    <cfsavecontent variable="CGIDump">
        <cfif IsDefined("CGIDump")>#CGIDump#</cfif>
        
		<cfif IsDefined("cfcatch.type") AND Len(Trim(cfcatch.type)) GT 0 >
        cfcatch.type
        -------------------------
        #cfcatch.type#
        -------------------------
        </cfif>
        
        <cfif IsDefined("cfcatch.message") AND Len(Trim(cfcatch.message)) GT 0 >
        cfcatch.message
        -------------------------
        #cfcatch.message#
        -------------------------
        </cfif>
        
        <cfif IsDefined("cfcatch.detail") AND Len(Trim(cfcatch.detail)) GT 0 >
        cfcatch.detail
        -------------------------
        #cfcatch.detail#
        -------------------------
        </cfif>
        
        <cfif IsDefined("CGI.HTTP_HOST") AND Len(Trim(CGI.HTTP_HOST)) GT 0 >
        CGI.HTTP_HOST
        -------------------------
        #CGI.HTTP_HOST#
        -------------------------
        </cfif>
        
        <cfif IsDefined("CGI.HTTP_REFERER") AND Len(Trim(CGI.HTTP_REFERER)) GT 0 >
        CGI.HTTP_REFERER
        -------------------------
        #CGI.HTTP_REFERER#
        -------------------------
        </cfif>
        
        <cfif IsDefined("CGI.HTTP_USER_AGENT") AND Len(Trim(CGI.HTTP_USER_AGENT)) GT 0 >
        CGI.HTTP_USER_AGENT
        -------------------------
        #CGI.HTTP_USER_AGENT#
        -------------------------
        </cfif>
        
        <cfif IsDefined("CGI.PATH_TRANSLATED") AND Len(Trim(CGI.PATH_TRANSLATED)) GT 0 >
        CGI.PATH_TRANSLATED
        -------------------------
        #CGI.PATH_TRANSLATED#
        -------------------------
        </cfif>
        
        <cfif IsDefined("CGI.QUERY_STRING") AND Len(Trim(CGI.QUERY_STRING)) GT 0 >
        CGI.QUERY_STRING
        -------------------------
        #CGI.QUERY_STRING#
        -------------------------
        </cfif>
    </cfsavecontent>
    
    <cfif SendEmail GT 0>
        <cfmail to="#DEVELOPERS#" from="mbreports@messagebroadcast.com" subject="#SubjectLine#">
            #ENA_Message#
            #CGIDump#
        </cfmail>
	</cfif>

    <cftry>
        <cfquery datasource="GlobalAlertsDB">
            INSERT INTO [Alerts].[dbo].[Daily_Alerts]
               (ServerIP_vch
               ,ENASubject_vch
               ,ENAMessage_vch
               ,CGIDump_txt
               ,HighPriority_bit
               ,AlertType_si
               ,To_vch)
         VALUES
               ('#addr#'
               ,'#SubjectLine#'
               ,'#ENA_Message#'
               ,'#CGIDump#'
               ,0
               ,#AlertType#
               ,'#DEVELOPERS#')
        </cfquery>
	<cfcatch>
    	<cfmail to="#DEVELOPERS#" from="mbreports@messagebroadcast.com" subject="#SubjectLine#">
            Failed to post alert to database:
            cfcatch.Message: <cfif IsDefined("cfcatch.message") AND Len(Trim(cfcatch.message)) GT 0>#cfcatch.Message#<cfelse>Unknown</cfif>
            cfcatch.Detail: <cfif IsDefined("cfcatch.detail") AND Len(Trim(cfcatch.detail)) GT 0 >#cfcatch.Detail#<cfelse>Unknown</cfif>
            ---
            #ENA_Message#
            #CGIDump#
        </cfmail>
	</cfcatch>
    </cftry>
</cfoutput>