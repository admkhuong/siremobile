

<cfparam name="inpListId" default="0"> 
<cfparam name="inpDialString" default="0">
<cfparam name="UserSpecifiedData_vch" default="">
<cfparam name="FileSeqNumber_int" default="0">
<cfparam name="XMLControlString_vch" default="">
<cfparam name="LocalityId_int" default="0">
<cfparam name="TimeZone_ti" default="0">

<!--- -1 Invalid Locality/Time Zone --->
<!--- -2 Exists On List --->
<!--- -3 Severe Error --->
<!--- 0 Default Nada --->
<!--- 1 Added To List --->
<cfparam name="CurrResult" default=0><!--- Include on any page that uses this method --->

<!--- Add number to list --->
<cftry>	

		<!--- Select Locality ID --->
		<cfquery name="SelectLocalityIds" datasource="MBASPSQL2K">
			SELECT 
			  lr.LocalityId_int
			FROM				  
			  MasterPhoneData..locality loc (NOLOCK) INNER JOIN
			  MasterPhoneData..locality_reference lr (NOLOCK) ON (loc.Alt1_vch = lr.Alt1_vch)
			WHERE
			  loc.AECombo_int = LEFT('#inpDialString#', 6)
		</cfquery>

		<cfquery name="SelectTimeZoneIds" datasource="MBASPSQL2K">
			SELECT
			  tzd.TimeZone_ti
			FROM  			  
			  MasterPhoneData..time_zone_data tzd (NOLOCK)
			WHERE
			  tzd.AECombo_int = LEFT('#inpDialString#', 6)			 
		</cfquery>
		

		<cfif SelectLocalityIds.RecordCount gt 0 AND SelectTimeZoneIds.RecordCount gt 0>

			<!--- For use in further queries... --->
			<cfset LocalityId_int = SelectLocalityIds.LocalityId_int>	
			<cfset TimeZone_ti = SelectTimeZoneIds.TimeZone_ti>		
			
			<cfset CurrResult= -2>
			
			<!--- ,XMLControlString_vch	 --->		
			<!--- Need to specially added XML String --->	
			<!---<cfquery name="InsertIntoList" datasource="MBASPSQL2K">										
				INSERT INTO RXList..RX_Phone_List_#inpListId#
					(
						DialString_vch,
						LocalityId_int,
						TimeZone_ti,
						UserSpecifiedData_vch,
						FileSeqNumber_int 						  
					)
				  VALUES
					(	
						'#inpDialString#', 
						#LocalityId_int#, 
						#TimeZone_ti#,
						'#UserSpecifiedData_vch#',
						#FileSeqNumber_int#  						
					)								
			</cfquery>--->
		
			<cfset CurrResult= 1>
		<cfelse>
		
			<cfset CurrResult= -2>
		
			<cfset LocalityId_int = "0">	
			<cfset TimeZone_ti = "0">	
		
			<!--- Allow unknown and international numbers to dial --->
			<!---<cfquery name="InsertIntoList" datasource="MBASPSQL2K">										
				INSERT INTO RXList..RX_Phone_List_#inpListId#
					(
						DialString_vch,
						LocalityId_int,
						TimeZone_ti,
						UserSpecifiedData_vch,
						FileSeqNumber_int 						  
					)
				  VALUES
					(	
						'#inpDialString#', 
						#LocalityId_int#, 
						#TimeZone_ti#,
						'#UserSpecifiedData_vch#',
						#FileSeqNumber_int#  						
					)								
			</cfquery>--->
			
			<cfset CurrResult=1>
		</cfif>
									
		<cfcatch type="any"><!--- Add number to list --->

			<cfif CurrResult neq -2> 
			
				<cfset CurrResult= -3>	
			
			</cfif>
											
		
		</cfcatch><!--- Add number to list --->

	</cftry><!--- Add number to list --->










