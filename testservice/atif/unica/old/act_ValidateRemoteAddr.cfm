<!--- Secures access to web services --->
<!--- Validate CGI.REMOTE_ADDR is in valid range or single unique address--->

<!--- Use subnet calculator to determine valid ranges --->

<cftry>
    
    <!--- 64.57.32.0/20 ----  64.57.32.1 to 64.57.47.254 --->
    <cfif ListGetAt(#CGI.REMOTE_ADDR#,1,'.') EQ 64 AND  ListGetAt(#CGI.REMOTE_ADDR#,2,'.') EQ 57 AND ListGetAt(#CGI.REMOTE_ADDR#,3,'.') GT 31 AND ListGetAt(#CGI.REMOTE_ADDR#,3,'.') LT 48 >
    
    <!--- 216.230.48.0/20 ----  216.230.48.1 to 216.230.63.254--->
    <cfelseif ListGetAt(#CGI.REMOTE_ADDR#,1,'.') EQ 216 AND ListGetAt(#CGI.REMOTE_ADDR#,2,'.') EQ 230 AND ListGetAt(#CGI.REMOTE_ADDR#,3,'.') GT 47 AND ListGetAt(#CGI.REMOTE_ADDR#,3,'.') LT 64 >
    
    <!--- MB testing ranges Intelenet I --->
    <cfelseif ListGetAt(#CGI.REMOTE_ADDR#,1,'.') EQ 207 AND ListGetAt(#CGI.REMOTE_ADDR#,2,'.') EQ 38 AND ListGetAt(#CGI.REMOTE_ADDR#,3,'.') EQ 123 AND ListGetAt(#CGI.REMOTE_ADDR#,4,'.') GT 19 AND ListGetAt(#CGI.REMOTE_ADDR#,4,'.') LT 23 >
 
    <!--- MB testing ranges Intelenet II--->
    <cfelseif ListGetAt(#CGI.REMOTE_ADDR#,1,'.') EQ 216 AND ListGetAt(#CGI.REMOTE_ADDR#,2,'.') EQ 23 AND ListGetAt(#CGI.REMOTE_ADDR#,3,'.') EQ 173 AND ListGetAt(#CGI.REMOTE_ADDR#,4,'.') GT 147 AND ListGetAt(#CGI.REMOTE_ADDR#,4,'.') LT 159 >
 
 
    <!--- MB testing ranges Intelenet III--->
    <cfelseif ListGetAt(#CGI.REMOTE_ADDR#,1,'.') EQ 209 AND ListGetAt(#CGI.REMOTE_ADDR#,2,'.') EQ 80 AND ListGetAt(#CGI.REMOTE_ADDR#,3,'.') EQ 11 AND ListGetAt(#CGI.REMOTE_ADDR#,4,'.') GT 114 AND ListGetAt(#CGI.REMOTE_ADDR#,4,'.') LT 127 >
    
    
    <!--- MB testing ranges Pajo/Tier 0 Newport Office--->
    <!--- 64.239.154.193/27 ---- 64.239.153.193- 64.239.153.222 --->
    <cfelseif ListGetAt(#CGI.REMOTE_ADDR#,1,'.') EQ 64 AND ListGetAt(#CGI.REMOTE_ADDR#,2,'.') EQ 239 AND ListGetAt(#CGI.REMOTE_ADDR#,3,'.') EQ 153 AND ListGetAt(#CGI.REMOTE_ADDR#,4,'.') GT 192 AND ListGetAt(#CGI.REMOTE_ADDR#,4,'.') LT 223 >
    
    <cfelse>
    
        <!--- Post warning message email --->
        <cfoutput><ack inserted="0" read="0" errcode="101"/></cfoutput>
       	<cfset ENA_Message = "Invalid Host #CGI.REMOTE_ADDR# Chase RPS MFA dial">		
        <cfset SubjectLine = "Error Chase Alerts - 34">		
		<cfinclude template="act_EscalationNotificationActions.cfm">	        
        <cfabort>
    
    </cfif>

<cfcatch type="any">
	
	<!--- Post warning message email --->
	<cfoutput><ack inserted="0" read="0" errcode="102"/></cfoutput>
    <cfset ENA_Message = "Invalid Host Specified #CGI.REMOTE_ADDR# Chase RPS MFA dial">		
    <cfset SubjectLine = "Error Chase Alerts - 34">		
    <cfinclude template="act_EscalationNotificationActions.cfm">	    
    <cfabort>    
    
</cfcatch>

</cftry>

