
<cfquery name="qExport" datasource="#EBMUI_DSN#">
SELECT transid_bi, 
       pfpid_bi, 
       CASE 
         WHEN dtsid_bi > 0 THEN 3 
         ELSE 5 
       END                         AS ReasonCode, 
       CASE 
         WHEN dtsid_bi > 0 THEN 'Delivered' 
         ELSE 'Failed' 
       END                         AS ReasonDescription, 
       timestamp1_dt, 
       responsemessagetimestamp_dt AS timestamp2_dt, 
       responsemessagetimestamp_dt AS timestamp3_dt, 
       responseexporttimestamp_dt, 
       accountno_vch, 
       premise_vch, 
       primaryphonenumber_vch, 
       secondaryphonenumber_vch, 
       campaigncode_vch, 
       emailaddress_vch, 
       custom1_vch, 
       custom2_vch, 
       custom3_vch, 
       custom4_vch, 
       custom5_vch,
	   InFileName_vch,
	   PKID_bi
FROM   simplexprogramdata.portal_link_c10_p1_unicafiles U
ORDER BY U.InFileName_vch asc, U.PKID_bi asc
</cfquery>


<cfdump var="#qExport#">    


<!---    <cfquery name="qCampaigns" datasource="MBASPSQL2K">
		SELECT 		DISTINCT UNICAFILENAME , [MsgSentTimeStamp]
        FROM 		FPL..UNICAFileLinks
		WHERE		ResponseExporetedTimeStamp is NULL
        ORDER BY	[MsgSentTimeStamp] DESC 
    </cfquery>   
    
    
   <!--- ge tall the messages sent for this campaign--->
    <cfquery name="qResponses" datasource="MBASPSQL2K">   
        SELECT	d.SMSGlobalMessageID, 
                d.SMSNumber, 
                d.SubscriberResultCode, 
                d.SubscriberResultText, 
                ISNULL(r.Reason,d.SubscriberResultCode) as Reason, 
                ISNULL(r.Status,d.SubscriberResultText) as Status,
                r.TimeStamp as ResponseTimeStamp,
                d.SentDate as SentTimeStamp,
				l.transid,
				l.pfpid                
        FROM	FPL.dbo.UNICAFileLinks L 
                inner join SMS.dbo.SMSGlobalDetails d  on l.SMSGlobalMessageID = d.SMSGlobalMessageID AND l.UNICAFileName = <cfqueryparam cfsqltype="cf_sql_varchar" value="#qCampaigns.UNICAFILENAME#">
                left join	SMS.dbo.SMSGlobalResponse r on l.SMSGlobalMessageID = r.SMSGlobalMessageID 
		ORDER BY d.SMSGlobalMessageID, Reason                  
	</cfquery>                
    
    <cfoutput query="qResponses" group="SMSGlobalMessageID">
    	#SMSGlobalMessageID#<br />
       <cfset responseCode = "">
        <cfset responseText = "">
        <cfoutput>
        </cfoutput>
    </cfoutput>
    
    
    <cfdump var="#qCampaigns#">
    <cfdump var="#qResponses#">    --->
	
<!---<cfquery name="qResponseData" datasource="MBASPSQL2K">    
SELECT 
u.infilename,
		d.smsglobalmessageid,
		u.trans_id, 
       u.pfp_id, 
	   r.TimeStamp,
       CASE r.reason 
         WHEN 3 THEN 3 
         WHEN 4 THEN 4 
		 WHEN 6 THEN 4
         ELSE 5 
       END        AS ReasonCode, 
       CASE r.reason 
         WHEN 3 THEN 'Acknowledged' 
         WHEN 4 THEN 'Delivered' 
		 WHEN 6 THEN 'Delivered'
         ELSE 'Failed' 
       END        AS ReasonDescription, 
       u.timestamp1, 
       d.sentdate AS timestamp2, 
       CONVERT(datetime,substring(r.TimeStamp,1,8) + ' ' + substring(r.TimeStamp,9,2) + ':' + substring(r.TimeStamp,11,2),112) AS timestamp3, 
       u.account_no, 
       u.premise, 
       u.primary_phone_number, 
       u.secondary_phone_number, 
       u.campaigncode, 
       u.email_address, 
       u.custom1, 
       u.custom2, 
       u.custom3, 
       u.custom4, 
       u.custom5 
FROM   fpl.dbo.unicafiles u 
       INNER JOIN sms.dbo.smsglobaldetails d 
               ON u.smsglobalmessageid = d.smsglobalmessageid 
       LEFT JOIN [SMS].[dbo].[smsglobalresponse] r 
              ON d.smsglobalmessageid = r.smsglobalmessageid 
WHERE  U.smsglobalmessageid IS NOT NULL 
       AND U.responseexported IS NULL 
       AND U.InFileName = 'C000000039_sms_20141215_234245.csv'
ORDER  BY U.infilename, 
          U.pkid,
		  r.TimeStamp desc
</cfquery>   
--->

   