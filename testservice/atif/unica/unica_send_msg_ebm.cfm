<cfsetting showdebugoutput="yes">


<cfparam name="AllowDuplicates" default="0">
<cfparam name="CurrResult" default="1">
<cfparam name="CurrResultDist" default="10">

<cfset ENA_Message = "Debug Info --- FPL Test dial<BR>" >	
<cfset SubjectLine = "UNICA Send SMS">			

<cftry>

	<cfquery name="qFileData" datasource="MBASPSQL2K">
    	SELECT 	*
        FROM	[FPL].[dbo].[UNICAFiles]
        WHERE	[SMSGlobalMessageID] is NULL
        	AND inFilename = 'C000000100_20141215_234245.csv'
        ORDER BY [InFileName]
	</cfquery>  
    
    <cfdump var="#qFileData#">
    
	<cfset variables.accessKey = '66D8F53DE9B97F11E689' />
    <cfset variables.secretKey = '57166/51960116+DFCc5B444F518C571c9/C1cdD' />    
    
 	<cfset variables.sign = generateSignature("POST") /> <!---Verb must match that of request type--->
	<cfset datetime = variables.sign.DATE>
    <cfset signature = variables.sign.SIGNATURE>
    
    <cfset authorization = variables.accessKey & ":" & signature>
    


 <cfhttp url="http://ebmui.com/webservice/ebm/pdc/addtorealtime" method="POST" result="returnStruct" >
	
     
       <!--- Required components --->
    
	   <!--- By default EBM API will return json or XML --->
	   <cfhttpparam name="Accept" type="header" value="application/json" />    	
	   <cfhttpparam type="header" name="datetime" value="#datetime#" />
       <cfhttpparam type="header" name="authorization" value="#authorization#" /> 
  	                 
       <!--- Batch Id controls which pre-defined campaign to run --->
       <cfhttpparam type="formfield" name="inpBatchId" value="1404" />
       
       <!--- Contact string--->
       <cfhttpparam type="formfield" name="inpContactString" value="9492786295" />
       
       <!--- 1=voice 2=email 3=SMS--->
       <cfhttpparam type="formfield" name="inpContactTypeId" value="3" />
       
       
       
       <!--- Optional Components --->
       
       <!--- Custom data element for PDC Batch Id 1135 --->
       <cfhttpparam type="formfield" name="inpCustomSMS" value="Atif test from dev server" />
      
      
      
    </cfhttp>
    
    
    <cfdump var="#returnStruct#">
    
    <cfdump var="#deserializeJSON(returnStruct.Filecontent)#">
    
   
    
    <cfabort>  
    
    <cfset actualCount = 0>

    <cfloop query="qFileData">
    
			<cfset smsnumber = trim(qFileData.primary_phone_number)>
            
            <cfif len(smsnumber) EQ 10 and isnumeric(smsnumber)>
        
                <cfset sentmessage =  qFileData.text_message>
                <cfset text1 = sentMessage>
        
                <cfinclude template="SMSGlobalMT-Insert.cfm">
                
                <cfset inpMFASMSDoc = "<?xml version=""1.0"" ?> 
                <NotificationRequest Version=""3.4""> 
                 <NotificationHeader> 
                   <PartnerName>MsgBroadcastMT</PartnerName> 
                   <PartnerPassword>Qa3PEwe8</PartnerPassword> 
                 </NotificationHeader> 
                    <NotificationList BatchID=""#BatchID#"">
                    <Notification SequenceNumber=""#SequenceNumber#"" MessageType=""SMS"">
                    <Message>#xmlformat(text1)#</Message> 
                    <Profile>32258</Profile> 
                    <SenderID Type=""Shortcode"">700800</SenderID> 
                    <Tariff>0</Tariff> 
                    <Subscriber> 
                        <SubscriberNumber>1#smsnumber#</SubscriberNumber> 
                    </Subscriber>
                    <ServiceId>60363</ServiceId> 
                    </Notification>
                 </NotificationList>
                </NotificationRequest>">
        
                <cfhttp url="http://xml3.us.mblox.com:8180/send" method="post" result="smsreturn" resolveurl="no" throwonerror="yes">
                    <cfhttpparam type="XML" name="XMLDoc" value="#inpMFASMSDoc#">	
                </cfhttp>
    
                <cfinclude template="SMSGlobalMT-update.cfm">
                
                <cfset actualCount = actualCount +1 >
            
			</cfif>
	</cfloop>

	<cfcatch type="any"> 
    	<cfdump var="#cfcatch#">
        <cfabort>
        <cfset ENA_Message = "Error while tryin to auto-distribute FPL SMS Opt In">		
        <cfset SubjectLine = "FPL UNICA SMS Send Error">		
		<cfinclude template="act_EscalationNotificationActions_old.cfm">	
	</cfcatch>

</cftry>

<cfoutput><h1>Messages processed: #actualCount# out of #qFileData.recordcount#</h1></cfoutput>





<cffunction
    name="CleanHighAscii"
    access="public"
    returntype="string"
    output="false"
    hint="Cleans extended ascii values to make the as web safe as possible.">
 
    <!--- Define arguments. --->
    <cfargument
        name="Text"
        type="string"
        required="true"
        hint="The string that we are going to be cleaning."
        />
 
    <!--- Set up local scope. --->
    <cfset var LOCAL = {} />
 
    <!---
        When cleaning the string, there are going to be ascii
        values that we want to target, but there are also going
        to be high ascii values that we don't expect. Therefore,
        we have to create a pattern that simply matches all non
        low-ASCII characters. This will find all characters that
        are NOT in the first 127 ascii values. To do this, we
        are using the 2-digit hex encoding of values.
    --->
    <cfset LOCAL.Pattern = CreateObject(
        "java",
        "java.util.regex.Pattern"
        ).Compile(
            JavaCast( "string", "[^\x00-\x7F]" )
            )
        />
 
    <!---
        Create the pattern matcher for our target text. The
        matcher will be able to loop through all the high
        ascii values found in the target string.
    --->
    <cfset LOCAL.Matcher = LOCAL.Pattern.Matcher(
        JavaCast( "string", ARGUMENTS.Text )
        ) />
 
 
    <!---
        As we clean the string, we are going to need to build
        a results string buffer into which the Matcher will
        be able to store the clean values.
    --->
    <cfset LOCAL.Buffer = CreateObject(
        "java",
        "java.lang.StringBuffer"
        ).Init() />
 
 
    <!--- Keep looping over high ascii values. --->
    <cfloop condition="LOCAL.Matcher.Find()">
 
        <!--- Get the matched high ascii value. --->
        <cfset LOCAL.Value = LOCAL.Matcher.Group() />
 
        <!--- Get the ascii value of our character. --->
        <cfset LOCAL.AsciiValue = Asc( LOCAL.Value ) />
 
        <!---
            Now that we have the high ascii value, we need to
            figure out what to do with it. There are explicit
            tests we can perform for our replacements. However,
            if we don't have a match, we need a default
            strategy and that will be to just store it as an
            escaped value.
        --->
 
        <!--- Check for Microsoft double smart quotes. --->
        <cfif (
            (LOCAL.AsciiValue EQ 8220) OR
            (LOCAL.AsciiValue EQ 8221)
            )>
 
            <!--- Use standard quote. --->
            <cfset LOCAL.Value = """" />
 
        <!--- Check for Microsoft single smart quotes. --->
        <cfelseif (
            (LOCAL.AsciiValue EQ 8216) OR
            (LOCAL.AsciiValue EQ 8217)
            )>
 
            <!--- Use standard quote. --->
            <cfset LOCAL.Value = "'" />
 
        <!--- Check for Microsoft elipse. --->
        <cfelseif (LOCAL.AsciiValue EQ 8230)>
 
            <!--- Use several periods. --->
            <cfset LOCAL.Value = "..." />
 
        <cfelse>
 
            <!---
                We didn't get any explicit matches on our
                character, so just store the escaped value.
            --->
            <cfset LOCAL.Value = "&###LOCAL.AsciiValue#;" />
 
        </cfif>
 
 
        <!---
            Add the cleaned high ascii character into the
            results buffer. Since we know we will only be
            working with extended values, we know that we don't
            have to worry about escaping any special characters
            in our target string.
        --->
        <cfset LOCAL.Matcher.AppendReplacement(
            LOCAL.Buffer,
            JavaCast( "string", LOCAL.Value )
            ) />
 
    </cfloop>
 
    <!---
        At this point there are no further high ascii values
        in the string. Add the rest of the target text to the
        results buffer.
    --->
    <cfset LOCAL.Matcher.AppendTail(
        LOCAL.Buffer
        ) />
 
 
    <!--- Return the resultant string. --->
    <cfreturn LOCAL.Buffer.ToString() />
</cffunction>




	<!--- generate signature string --->
	<cffunction name="generateSignature" returnformat="json" access="remote" output="false">
		<cfargument name="method" required="true" type="string" default="GET" hint="Method" />
                 
		<cfset dateTimeString = GetHTTPTimeString(Now())>
		<!--- Create a canonical string to send --->
		<cfset cs = "#arguments.method#\n\n\n#dateTimeString#\n\n\n">
		<cfset signature = createSignature(cs,variables.secretKey)>
		
		<cfset retval.SIGNATURE = signature>
		<cfset retval.DATE = dateTimeString>
		
		<cfreturn retval>
	</cffunction>
	
	<!--- Encrypt with HMAC SHA1 algorithm--->
	<cffunction name="HMAC_SHA1" returntype="binary" access="private" output="false" hint="NSA SHA-1 Algorithm">
	   <cfargument name="signKey" type="string" required="true" />
	   <cfargument name="signMessage" type="string" required="true" />
	
	   <cfset var jMsg = JavaCast("string",arguments.signMessage).getBytes("iso-8859-1") />
	   <cfset var jKey = JavaCast("string",arguments.signKey).getBytes("iso-8859-1") />
	   <cfset var key = createObject("java","javax.crypto.spec.SecretKeySpec") />
	   <cfset var mac = createObject("java","javax.crypto.Mac") />
	
	   <cfset key = key.init(jKey,"HmacSHA1") />
	   <cfset mac = mac.getInstance(key.getAlgorithm()) />
	   <cfset mac.init(key) />
	   <cfset mac.update(jMsg) />
	
	   <cfreturn mac.doFinal() />
	</cffunction>
	
	
	<cffunction name="createSignature" returntype="string" access="public" output="false">
	    <cfargument name="stringIn" type="string" required="true" />
		<cfargument name="scretKey" type="string" required="true" />
		<!--- Replace "\n" with "chr(10) to get a correct digest --->
		<cfset var fixedData = replace(arguments.stringIn,"\n","#chr(10)#","all")>
		<!--- Calculate the hash of the information --->
		<cfset var digest = HMAC_SHA1(scretKey,fixedData)>
		<!--- fix the returned data to be a proper signature --->
		<cfset var signature = ToBase64("#digest#")>
		
		<cfreturn signature>
	</cffunction>
    
   
