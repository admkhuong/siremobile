<cfinclude template="../paths.cfm" >
<cfscript>
	if(isDefined("form.btnSubmit")){
		objServiceObject = createObject('component', 'testservice.cfc.ebmService').init(); 
		// set your access key and secret key here
		// Two line code is no need if you configure your access key and secret key in paths.cfm
		objServiceObject.setAccessKey("1553B678833359F7ECAC");
		objServiceObject.setSecretKey("28BA2D6e5E4D55BB2905245B7f1B385Bd9b/0Cb6");
		
		// rename batch
		returnData = objServiceObject.renameBatch(
			inpBatchId = form.inpBatchId,
			inpDescription = form.inpBatchDesc
		);
		// dump return data
		writedump(returnData);

	}
</cfscript>
<html>
	<head>
		<title>Rename batch</title>
	</head>
	<body>
		
	<cfoutput>
    <div id="RightStage">
		<cfform method="post" name="">
			<span class="small">Batch id</span>
			<cfinput type="text" id="inpBatchId" name="inpBatchId" required="true" message="Batch id is required!"/><br />
			<span class="small">Description</span>
			<cfinput type="text" id="inpBatchDesc" name="inpBatchDesc" required="true" message="Batch's description is required!"/><br />
			<cfinput type="submit" name="btnSubmit" value="add"/>
		</cfform>
    </div>
	</cfoutput>
	</body>
</html>