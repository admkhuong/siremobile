﻿<cfinclude template="../paths.cfm" >
<cfscript>
	
	if(isDefined("form.btnSubmit")){
		scheduleTime = [{
			"blackoutEndHour"=0,
			"blackoutStartHour"=0,
			"dayId"=0,
			"enabledBlackout"=false,
			"endHour"=19,
			"endMinute"=0,
			"scheduleType"="2",
			"startDate"= LSDateFormat(NOW(), 'yyyy/mm/dd'),
			"startHour"=10,
			"startMinute"=0,
			"stopDate"=  LSDateFormat(DateAdd('m', 1, now()), 'yyyy/mm/dd')
		}];
		
		objServiceObject = createObject('component', 'testservice.cfc.ebmService').init(); 
		
		// set your access key and secret key here
		objServiceObject.setAccessKey("10022EE3B634934442AE");
		objServiceObject.setSecretKey("E5c190bF14ECdF73/6154CD7F0807Dae46ead27f");
		
		returnData = objServiceObject.updateBatchSchedule(
			inpBatchId = form.inpBatchId,
			scheduleTime = serializeJSON(scheduleTime)
			);
		// dump return data
		writedump(returnData);

	}
</cfscript>
<html>
	<head>
		<title>Update schedule for a batch</title>
	</head>
	<body>
		
	<cfoutput>
    <div id="RightStage">
		<form method="post" name="">
			<span class="small">Update schedule</span>
			<input type="text" id="inpBatchId" name="inpBatchId" />
			<input type="submit" name="btnSubmit" value="Update"/>
		</form>
    </div>
	</cfoutput>
	</body>
</html>