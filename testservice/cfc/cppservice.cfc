﻿<cfcomponent extends="ebmbase">
	
	<cffunction name="addCppContact" returnformat="jSON" access="remote" output="true">
		
		<cfargument name="api_method" required="false" type="string" default="GET" />
		<cfargument name="access_key" required="false" type="string" default="" />
		<cfargument name="secret_key" required="false" type="string" default="" />
		<cfargument name="format" required="false" type="string" default="json" />
		
		<cfargument name="INPCONTACTSTRING" required="false" type="string" default="" />
		<cfargument name="INPCONTACTTYPEID" required="false" type="string" default="" />
		<cfargument name="INPGROUPID" required="false" type="string" default="" />
		<cfargument name="INPUSERSPECIFIEDDATA" required="false" type="string" default="" />
		<cfargument name="INP_SOURCEKEY" required="false" type="string" default="" />
		<cfargument name="INPACCOUNTID" required="false" type="string" default="" />
		<cfargument name="INPAPPENDFLAG" required="false" type="string" default="" />
		<cfargument name="INPALLOWDUPLICATES" required="false" type="string" default="" />
		
		<cfset var retData = StructNew() >		
		<cftry>
			<!--- Generate signature --->
			<cfset init()>
			<cfset variables.accessKey = access_key />
			<cfset variables.secretKey = secret_key />
			<cfset variables.sign = generateSignatureBySecretKey(api_method,secret_key) />
			
			<cfset datetime = variables.sign.DATE>
			<cfset signature = variables.sign.SIGNATURE>
			<cfset authorization = access_key & ":" & signature>
			<cfhttp url="#variables.cppUrl#/AddContact" method="post" result="returnStruct" >
				<cfhttpparam type="header" name="accept" value="json" />
				<cfhttpparam type="header" name="datetime" value="#datetime#" />
				<cfhttpparam type="header" name="authorization" value="#authorization#" />
				
				<cfhttpparam type="FormField" name="INPCONTACTSTRING" value="#INPCONTACTSTRING#">
				<cfhttpparam type="FormField" name="INPCONTACTTYPEID" value="#INPCONTACTTYPEID#">
				<cfhttpparam type="FormField" name="INPGROUPID" value="#INPGROUPID#">
				<cfhttpparam type="FormField" name="INPUSERSPECIFIEDDATA" value="#INPUSERSPECIFIEDDATA#">
				<cfhttpparam type="FormField" name="INP_SOURCEKEY" value="#INP_SOURCEKEY#">
				<cfhttpparam type="FormField" name="INPACCOUNTID" value="#INPACCOUNTID#">
				<cfhttpparam type="FormField" name="INPAPPENDFLAG" value="#INPAPPENDFLAG#">
				<cfhttpparam type="FormField" name="INPALLOWDUPLICATES" value="#INPALLOWDUPLICATES#">
			</cfhttp>
			
			<cfif returnStruct.Responseheader.Status_code EQ 200>
				<cfset retData = serializeJSON(returnStruct.fileContent) >
				<cfset retData.SUCCESS = true>
				<cfset retData.MESSAGE = "Get list cpp successfully.">
			<cfelse>
				<cfset retData.SUCCESS = false>
				<cfset retData.MESSAGE = "#returnStruct.Statuscode#">
			</cfif>
			
        <cfcatch type="Any" >
        </cfcatch>
        </cftry>

				
		<cfreturn retData>
	</cffunction>
	
	<!--- List contact by CPP --->
	<cffunction name="listContactByCpp" returnformat="jSON" access="remote" output="true">
		
		<cfargument name="access_key" required="false" type="string" default="" />
		<cfargument name="secret_key" required="false" type="string" default="" />
		
		<cfargument name="api_method" required="false" type="string" default="GET" />
		<cfargument name="inpCPPUUID" required="false" type="string" default="" />
		
		<cfset var retData = StructNew() >		
		<cftry>
			<!--- Generate signature --->
			<cfset init()>
			<cfset variables.accessKey = access_key />
			<cfset variables.secretKey = secret_key />
			<cfset variables.sign = generateSignatureBySecretKey("#api_method#",secret_key) />
			
			<cfset datetime = variables.sign.DATE>
			<cfset signature = variables.sign.SIGNATURE>
			<cfset authorization = access_key & ":" & signature>
			
			<cfhttp url="#variables.contactsUrl#/listcontactbycpp?inpCPPUUID=#inpCPPUUID#" method="#api_method#" result="returnStruct" >
				<cfhttpparam type="header" name="accept" value="json" />
				<cfhttpparam type="header" name="datetime" value="#datetime#" />
				<cfhttpparam type="header" name="authorization" value="#authorization#" />
				
			</cfhttp>
			
			<cfif returnStruct.Responseheader.Status_code EQ 200>
				<cfset retData = returnStruct.fileContent >
				<cfset retData.SUCCESS = true>
				<cfset retData.MESSAGE = "Get list contact by cpp successfully.">
			<cfelse>
				<cfset retData.SUCCESS = false>
				<cfset retData.MESSAGE = "#returnStruct.Statuscode#">
			</cfif>
			
        <cfcatch type="Any" >
        </cfcatch>
        </cftry>

				
		<cfreturn retData>
	</cffunction>
	
	<!--- List contact by CDF --->
	<cffunction name="listContactByCdf" returnformat="jSON" access="remote" output="true">
		
		<cfargument name="access_key" required="false" type="string" default="" />
		<cfargument name="secret_key" required="false" type="string" default="" />
		
		<cfargument name="api_method" required="false" type="string" default="GET" />
		
		<cfargument name="inpCdfName" required="false" type="string" default="" />
		<cfargument name="inpCdfValue" required="false" type="string" default="" />
		<cfargument name="inpGroupId" required="false" type="string" default="" />
		
		<cfset var retData = StructNew() >		
		<cftry>
			<!--- Generate signature --->
			<cfset init()>
			<cfset variables.accessKey = access_key />
			<cfset variables.secretKey = secret_key />
			<cfset variables.sign = generateSignatureBySecretKey("#api_method#",secret_key) />
			
			<cfset datetime = variables.sign.DATE>
			<cfset signature = variables.sign.SIGNATURE>
			<cfset authorization = access_key & ":" & signature>
			
			<cfhttp url="#variables.contactsUrl#/listcontactbycdf?inpCdfName=#inpCdfName#&inpCdfValue=#inpCdfValue#&inpGroupId=#inpGroupId#" method="#api_method#" result="returnStruct" >
				<cfhttpparam type="header" name="accept" value="json" />
				<cfhttpparam type="header" name="datetime" value="#datetime#" />
				<cfhttpparam type="header" name="authorization" value="#authorization#" />				
			</cfhttp>
			
			<cfif returnStruct.Responseheader.Status_code EQ 200>
				<cfset retData = returnStruct.fileContent >
				<cfset retData.SUCCESS = true>
				<cfset retData.MESSAGE = "Get list contact by cdf successfully.">
			<cfelse>
				<cfset retData.SUCCESS = false>
				<cfset retData.MESSAGE = "#returnStruct.Statuscode#">
			</cfif>
			
        <cfcatch type="Any" >
        </cfcatch>
        </cftry>

				
		<cfreturn retData>
	</cffunction>
	
	<!--- Delete contact --->
	<cffunction name="delcontact" returnformat="jSON" access="remote" output="true">
		
		<cfargument name="access_key" required="false" type="string" default="" />
		<cfargument name="secret_key" required="false" type="string" default="" />
		
		<cfargument name="api_method" required="false" type="string" default="GET" />
		
		<cfargument name="INPGROUPID" required="false" type="string" default="" />
		<cfargument name="INPCONTACTID" required="false" type="string" default="" />
		<cfargument name="INPCONTACTSTRING" required="false" type="string" default="" />
		<cfargument name="INPCONTACTTYPE" required="false" type="string" default="" />
		<cfargument name="INP_CPPID" required="false" type="string" default="" />
		
		<cfset var retData = StructNew() >		
		<cftry>
			<!--- Generate signature --->
			<cfset init()>
			<cfset variables.accessKey = access_key />
			<cfset variables.secretKey = secret_key />
			<cfset variables.sign = generateSignatureBySecretKey("#api_method#",secret_key) />
			
			<cfset datetime = variables.sign.DATE>
			<cfset signature = variables.sign.SIGNATURE>
			<cfset authorization = access_key & ":" & signature>
			
			<cfhttp url="#variables.contactsUrl#/delcontact?INPGROUPID=#INPGROUPID#&INPCONTACTID=#INPCONTACTID#&INPCONTACTSTRING=#INPCONTACTSTRING#&INPCONTACTTYPE=#INPCONTACTTYPE#&INP_CPPID=#INP_CPPID#" method="#api_method#" result="returnStruct" >
				<cfhttpparam type="header" name="accept" value="json" />
				<cfhttpparam type="header" name="datetime" value="#datetime#" />
				<cfhttpparam type="header" name="authorization" value="#authorization#" />				
			</cfhttp>
			
			<cfif returnStruct.Responseheader.Status_code EQ 200>
				<cfset retData = returnStruct.fileContent >
				<cfset retData.SUCCESS = true>
				<cfset retData.MESSAGE = "Delete contact successfully.">
			<cfelse>
				<cfset retData.SUCCESS = false>
				<cfset retData.MESSAGE = "#returnStruct.Statuscode#">
			</cfif>
			
        <cfcatch type="Any" >
        </cfcatch>
        </cftry>

				
		<cfreturn retData>
	</cffunction>
	
	
	<!--- Edit contact --->
	<cffunction name="editcontact" returnformat="jSON" access="remote" output="true">
		
		<cfargument name="access_key" required="false" type="string" default="" />
		<cfargument name="secret_key" required="false" type="string" default="" />
		
		<cfargument name="api_method" required="false" type="string" default="GET" />
		
		<cfargument name="Company_vch" TYPE="string" required="no">		
		<cfargument name="FirstName_vch" TYPE="string" required="no">        
		<cfargument name="LastName_vch" TYPE="string" required="no">        
		<cfargument name="Address_vch" TYPE="string" required="no">        
		<cfargument name="Address1_vch" TYPE="string" required="no">        
		<cfargument name="City_vch" TYPE="string" required="no">        
		<cfargument name="State_vch" TYPE="string" required="no">        
		<cfargument name="ZipCode_vch" TYPE="string" required="no">        
		<cfargument name="Country_vch" TYPE="string" required="no">        
		<cfargument name="UserDefinedKey_vch" TYPE="string" required="no">        
		<cfargument name="ContactId_bi" TYPE="string" required="no">
		
		<cfset var retData = StructNew() >		
		<cftry>
			<!--- Generate signature --->
			<cfset init()>
			<cfset variables.accessKey = access_key />
			<cfset variables.secretKey = secret_key />
			<cfset variables.sign = generateSignatureBySecretKey("#api_method#",secret_key) />
			
			<cfset datetime = variables.sign.DATE>
			<cfset signature = variables.sign.SIGNATURE>
			<cfset authorization = access_key & ":" & signature>
			
			<cfhttp url="#variables.contactsUrl#/editcontact?Company_vch=#Company_vch#&FirstName_vch=#FirstName_vch#&LastName_vch=#LastName_vch#&Address_vch=#Address_vch#&Address1_vch=#Address1_vch#&City_vch=#City_vch#&State_vch=#State_vch#&ZipCode_vch=#ZipCode_vch#&Country_vch=#Country_vch#&UserDefinedKey_vch=#UserDefinedKey_vch#&ContactId_bi=#ContactId_bi#" method="#api_method#" result="returnStruct" >
				<cfhttpparam type="header" name="accept" value="json" />
				<cfhttpparam type="header" name="datetime" value="#datetime#" />
				<cfhttpparam type="header" name="authorization" value="#authorization#" />				
			</cfhttp>
			
			<cfif returnStruct.Responseheader.Status_code EQ 200>
				<cfset retData = returnStruct.fileContent >
				<cfset retData.SUCCESS = true>
				<cfset retData.MESSAGE = "Edit contact successfully.">
			<cfelse>
				<cfset retData.SUCCESS = false>
				<cfset retData.MESSAGE = "#returnStruct.Statuscode#">
			</cfif>
			
        <cfcatch type="Any" >
        </cfcatch>
        </cftry>

				
		<cfreturn retData>
	</cffunction>
	
</cfcomponent>