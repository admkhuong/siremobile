﻿<cfcomponent output="false" >
	<!--- <cfinclude template="../paths.cfm" >
	<cffunction name="init" access="public" output="false" returntype="any">
		
		<cfset variables.baseServiceUrl = "http://#siteDomain#/webservice/ebm" />
		<cfset variables.cppUrl = "#variables.baseServiceUrl#/index.cfm/cpp" />
		<cfset variables.batchUrl = "#variables.baseServiceUrl#/batch/index.cfm/batch" />
		<cfset variables.scheduleUrl = "#variables.baseServiceUrl#/index.cfm/Schedule" />
		<cfset variables.contactsUrl = "#variables.baseServiceUrl#/index.cfm/contacts" />
		<cfreturn this />
	</cffunction>
	
	<cffunction name="generateSignature" returnformat="json" access="remote" output="false">
		<cfargument name="method" required="true" type="string" default="GET" hint="Method" />
		<cfset dateTimeString = GetHTTPTimeString(Now())>
		<!--- Create a canonical string to send --->
		<cfset cs = "#arguments.method#\n\n\n#dateTimeString#\n\n\n">
		<cfset signature = createSignature(cs,variables.secretKey)>
		<cfset retval.SIGNATURE = signature>
		<cfset retval.DATE = dateTimeString>
		<cfreturn retval>
	</cffunction>


	<cffunction name="HMAC_SHA1" returntype="binary" access="private" output="false" hint="NSA SHA-1 Algorithm">
		<cfargument name="signKey" type="string" required="true" />
		<cfargument name="signMessage" type="string" required="true" />
		<cfset var jMsg = JavaCast("string",arguments.signMessage).getBytes("iso-8859-1") />
		<cfset var jKey = JavaCast("string",arguments.signKey).getBytes("iso-8859-1") />
		<cfset var key = createObject("java","javax.crypto.spec.SecretKeySpec") />
		<cfset var mac = createObject("java","javax.crypto.Mac") />
		<cfset key = key.init(jKey,"HmacSHA1") />
		<cfset mac = mac.getInstance(key.getAlgorithm()) />
		<cfset mac.init(key) />
		<cfset mac.update(jMsg) />
		<cfreturn mac.doFinal() />
	</cffunction>


	<cffunction name="createSignature" returntype="string" access="public" output="false">
		<cfargument name="stringIn" type="string" required="true" />
		<cfargument name="scretKey" type="string" required="true" />
		<!--- Replace "\n" with "chr(10) to get a correct digest --->
		<cfset var fixedData = replace(arguments.stringIn,"\n","#chr(10)#","all")>
		<!--- Calculate the hash of the information --->
		<cfset var digest = HMAC_SHA1(scretKey,fixedData)>
		<!--- fix the returned data to be a proper signature --->
		<cfset var signature = ToBase64("#digest#")>
		<cfreturn signature>
	</cffunction>


	<cffunction name="setAccessKey" returntype="string" output="false" access="public" hint="">
		<cfargument name="inpAccessKey" type="string" required="true" />
		<cfset variables.accessKey = inpAccessKey/>
	</cffunction>


	<cffunction name="setSecretKey" returntype="string" output="false" access="public" hint="">
		<cfargument name="inpSecretKey" type="string" required="true" />
		<cfset variables.secretKey = inpSecretKey/>
	</cffunction>


	<cffunction name="ReadFileContent" returnformat="json" access="remote" output="false">
		<cfargument name="path" required="yes" type="string"/>
		<cfset filePath = expandPath(path) />
		<cffile action = "read" 
			file = "#filePath#"
			variable = "Message">
		<cfreturn Message>
	</cffunction> --->

</cfcomponent>