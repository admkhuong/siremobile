<cfcomponent output="false" extends="ebmbase" >
	<cfinclude template="../paths.cfm" >
	
	<cffunction name="getCppList" returnformat="json" access="remote" output="true">
		<cfargument name="format" required="false" type="string" default="xml" hint="return format type" />
		<!--- Generate signature --->
		<cfset variables.sign = generateSignature("GET") />
		<cfset datetime = variables.sign.DATE>
		<cfset signature = variables.sign.SIGNATURE>
		
		<cfset authorization = variables.accessKey & ":" & signature>
		
		<cfhttp url="#variables.cppUrl#/data" method="get" result="returnStruct" >
			<cfhttpparam type="header" name="accept" value="#format#" />
			
			<cfhttpparam type="header" name="datetime" value="#datetime#" />
			<cfhttpparam type="header" name="authorization" value="#authorization#" />
		</cfhttp>
		<cfif returnStruct.Responseheader.Status_code EQ 200>
			<cfif format EQ 'json'>
				<cfset retData = deserializeJSON(returnStruct.fileContent)>
			<cfelse>
				<cfset retData = returnStruct.fileContent>
			</cfif>
			<cfset retData.SUCCESS = true>
			<cfset retData.MESSAGE = "Get list cpp successfully.">
		<cfelse>
			<cfset retData.SUCCESS = false>
			<cfset retData.MESSAGE = "#returnStruct.Statuscode#">
		</cfif>
				
		<cfreturn retData>
	</cffunction>
	
	<cffunction name="createCPP" returnformat="json" access="remote" output="false">
		<cfargument name="inpDescription" required="true" type="string" default="" hint="cpp decription" />
		<cfset variables.sign = generateSignature("POST") />		
		<cfset datetime = variables.sign.DATE>
		<cfset signature = variables.sign.SIGNATURE>
		
		<cfset authorization = variables.accessKey & ":" & signature>
		
		<cfhttp url="#variables.cppUrl#" method="post" result="returnStruct" >
			<cfhttpparam type="formfield" name="cppListDesc" value="#arguments.inpDescription#" />
			
			<cfhttpparam type="header" name="datetime" value="#datetime#" />
			<cfhttpparam type="header" name="authorization" value="#authorization#" />
		</cfhttp>
		
		<cfif returnStruct.Responseheader.Status_code EQ 200>
			<cfset retData.SUCCESS = true>
			<cfset retData.MESSAGE = "Get list cpp successfully.">
		<cfelse>
			<cfset retData.SUCCESS = false>
			<cfset retData.MESSAGE = "#returnStruct.Statuscode#">
		</cfif>
		<cfreturn retData>
	</cffunction>
	
	<cffunction name="updateCpp" returnformat="json" access="remote" output="false">
		
		<cfargument name="cppUUID" required="false" type="string" default="" hint="cpp UUID" />
		<cfargument name="desc" required="false" type="string" default="" hint="cpp decription" />
		<cfargument name="cppstyletemplate" required="false" type="string" default="" hint="style template" />
		<cfargument name="facebookmsg" required="false" type="string" default="" hint="facebook message" />
		<cfargument name="twittermsg" required="false" type="string" default="" hint="twitter message" />
		
		<cfset variables.sign = generateSignature("PUT") />
		<cfset datetime = variables.sign.DATE>
		<cfset signature = variables.sign.SIGNATURE>
		
		<cfset authorization = variables.accessKey & ":" & signature>
		<cfhttp url="#variables.cppUrl#/#arguments.cppUUID#?desc=#arguments.desc#&cppstyletemplate=#arguments.cppstyletemplate#&facebookmsg=#arguments.facebookmsg#&twittermsg=#arguments.twittermsg#" method="put" result="returnStruct" >
			<cfhttpparam type="header" name="datetime" value="#datetime#" />
			<cfhttpparam type="header" name="authorization" value="#authorization#" />
		</cfhttp>
		
		<cfif returnStruct.Responseheader.Status_code EQ 200>
			<cfset retData.SUCCESS = true>
			<cfset retData.MESSAGE = "#variables.cppUrl#/#arguments.cppUUID#?desc=#arguments.desc#&cppstyletemplate=#arguments.cppstyletemplate#&facebookmsg=#arguments.facebookmsg#&twittermsg=#arguments.twittermsg#">
		<cfelse>
			<cfset retData.SUCCESS = false>
			<cfset retData.MESSAGE = "#returnStruct.Statuscode#">
		</cfif>
		<cfreturn retData>
	</cffunction>
	
	<cffunction name="deleteCpp" returnformat="json" access="remote" output="false">
		<cfargument name="cppUUID" required="false" type="string" default="" hint="cpp UUID" />
		<cfset variables.sign = generateSignature("DELETE") />
		<cfset datetime = variables.sign.DATE>
		<cfset signature = variables.sign.SIGNATURE>
		
		<cfset authorization = variables.accessKey & ":" & signature>
		<cfhttp url="#variables.cppUrl#/#arguments.cppUUID#" method="delete" result="returnStruct" >
			<cfhttpparam type="header" name="datetime" value="#datetime#" />
			<cfhttpparam type="header" name="authorization" value="#authorization#" />
		</cfhttp>
		
		<cfif returnStruct.Responseheader.Status_code EQ 200>
			<cfset retData.SUCCESS = true>
			<cfset retData.MESSAGE = "delete cpp successfully.">
		<cfelse>
			<cfset retData.SUCCESS = false>
			<cfset retData.MESSAGE = "#returnStruct.Statuscode#">
		</cfif>
		<cfreturn retData>
	</cffunction>
	
	
	<cffunction name="getCpp" returnformat="json" access="remote" output="true">
		<cfargument name="inpCppUUID" required="true" type="string" default="" hint="cpp uuid" />
		<cfset variables.sign = generateSignature("GET") />
		<cfset datetime = variables.sign.DATE>
		<cfset signature = variables.sign.SIGNATURE>
		
		<cfset authorization = variables.accessKey & ":" & signature>
		
		<cfhttp url="#variables.cppUrl#/#arguments.inpCppUUID#" method="get" result="returnStruct" >
			<cfhttpparam type="formfield" name="format" value="json" />
			
			<cfhttpparam type="header" name="datetime" value="#datetime#" />
			<cfhttpparam type="header" name="authorization" value="#authorization#" />
		</cfhttp>
		
		<cfif returnStruct.Responseheader.Status_code EQ 200>
			<cfset retData.SUCCESS = true>
			<cfset retData.MESSAGE = "Get list cpp successfully.">
			<cfset retData = deserializeJSON(returnStruct.fileContent)>
		<cfelse>
			<cfset retData.SUCCESS = false>
			<cfset retData.MESSAGE = "#returnStruct.Statuscode#">
		</cfif>
		<cfreturn retData>
	</cffunction>
	
	<cffunction name="createBatch" returnformat="json" access="remote" output="true">
		<cfargument name="inpDescription" required="true" type="string" default="" hint="cpp decription" />
		<!----- Generate signature for calling api--------->
		<!--- Please pay attention to our verb. In case we use POST to create batch --->
		<!--- This verb must match with the verb in our API --->
		<cfset variables.sign = generateSignature("POST") />		
		<cfset datetime = variables.sign.DATE>
		<cfset signature = variables.sign.SIGNATURE>
		
		<cfset authorization = variables.accessKey & ":" & signature>
		
		<cfhttp url="#variables.batchUrl#" method="post" result="returnStruct" >
			<cfhttpparam type="formfield" name="inpBatchDesc" value="#arguments.inpDescription#" />
			
			<cfhttpparam type="header" name="datetime" value="#datetime#" />
			<cfhttpparam type="header" name="authorization" value="#authorization#" />
		</cfhttp>
		
		<cfif returnStruct.Responseheader.Status_code EQ 200>
			<cfset retData.SUCCESS = true>
			<cfset retData.MESSAGE = "Create new batch successfully.">
		<cfelse>
			<cfset retData.SUCCESS = false>
			<cfset retData.MESSAGE = "#returnStruct.Statuscode#">
		</cfif>
		<cfreturn retData>
	</cffunction>
	
	<cffunction name="renameBatch" returnformat="json" access="remote" output="true">
		<cfargument name="inpBatchId" required="true" type="string" default="" hint="Batch id" />
		<cfargument name="inpDescription" required="true" type="string" default="" hint="Batch description" />
		
		<!----- Generate signature for calling api--------->
		<!--- Please pay attention to our verb. In case we use PUT to rename the batch --->
		<!--- This verb must match with the verb in our API --->
		<cfset variables.sign = generateSignature("PUT") />		
		<cfset datetime = variables.sign.DATE>
		<cfset signature = variables.sign.SIGNATURE>
		
		<cfset authorization = variables.accessKey & ":" & signature>
		
		<!--- this url matches with taffy uri "batch/{batchId}" --->
		<!--- method matches with verb PUT --->
		<cfhttp url="#variables.batchUrl#/#arguments.inpBatchId#?inpBatchDesc=#arguments.inpDescription#" method="put" result="returnStruct" >
			<cfhttpparam type="header" name="datetime" value="#datetime#" />
			<cfhttpparam type="header" name="authorization" value="#authorization#" />
			
		</cfhttp>

		<cfif returnStruct.Responseheader.Status_code EQ 200>
			<cfset retData.SUCCESS = true>
			<cfset retData.MESSAGE = "Rename batch successfully.">
		<cfelse>
			<cfset retData.SUCCESS = false>
			<cfset retData.MESSAGE = "#returnStruct.Statuscode#">
		</cfif>
		<cfreturn retData>
	</cffunction>
	
	<cffunction name="updateBatchSchedule" returnformat="json" access="remote" output="true">
		<cfargument name="inpBatchId" required="true" type="string" default="" hint="Batch id" />
		<cfargument name="scheduleTime" required="true" type="any" default="[]" hint="Batch description" />
		
		<!----- Generate signature for calling api--------->
		<!--- Please pay attention to our verb. In case we use PUT to rename the batch --->
		<!--- This verb must match with the verb in our API --->
		<cfset variables.sign = generateSignature("POST") />		
		<cfset datetime = variables.sign.DATE>
		<cfset signature = variables.sign.SIGNATURE>
		
		<cfset authorization = variables.accessKey & ":" & signature>
		<!--- this url matches with taffy uri "/Schedule/update/{BatchId}" --->
		<cfhttp url="#variables.scheduleUrl#/update/#arguments.inpBatchId#" method="POST" result="returnStruct" >
			<cfhttpparam type="formfield" name="loopLimit_int" value="100" />
			<cfhttpparam type="formfield" name="scheduleTime" value="#scheduleTime#" />
			
			<cfhttpparam type="header" name="datetime" value="#datetime#" />
			<cfhttpparam type="header" name="authorization" value="#authorization#" />
		</cfhttp>
		<cfdump var="#deserializeJSON(returnStruct.fileContent)#">
		<cfif returnStruct.Responseheader.Status_code EQ 200>
			<cfset retData.SUCCESS = true>
			<cfset retData.MESSAGE = "Update schedule successfully.">
		<cfelse>
			<cfset retData.SUCCESS = false>
			<cfset retData.MESSAGE = "#returnStruct.Statuscode#">
		</cfif>
		<cfreturn retData>
	</cffunction>
	
	<cffunction name="generateSignature" returnformat="json" access="remote" output="false">
		<cfargument name="method" required="true" type="string" default="GET" hint="Method" />
		<cfset dateTimeString = GetHTTPTimeString(Now())>
		<!--- Create a canonical string to send --->
		<cfset cs = "#arguments.method#\n\n\n#dateTimeString#\n\n\n">
		<cfset signature = createSignature(cs,variables.secretKey)>
		
		<cfset retval.SIGNATURE = signature>
		<cfset retval.DATE = dateTimeString>
		
		<cfreturn retval>
	</cffunction>
	
	<cffunction name="HMAC_SHA1" returntype="binary" access="private" output="false" hint="NSA SHA-1 Algorithm">
	   <cfargument name="signKey" type="string" required="true" />
	   <cfargument name="signMessage" type="string" required="true" />
	
	   <cfset var jMsg = JavaCast("string",arguments.signMessage).getBytes("iso-8859-1") />
	   <cfset var jKey = JavaCast("string",arguments.signKey).getBytes("iso-8859-1") />
	   <cfset var key = createObject("java","javax.crypto.spec.SecretKeySpec") />
	   <cfset var mac = createObject("java","javax.crypto.Mac") />
	
	   <cfset key = key.init(jKey,"HmacSHA1") />
	   <cfset mac = mac.getInstance(key.getAlgorithm()) />
	   <cfset mac.init(key) />
	   <cfset mac.update(jMsg) />
	
	   <cfreturn mac.doFinal() />
	</cffunction>
	
	<cffunction name="createSignature" returntype="string" access="public" output="false">
	   <cfargument name="stringIn" type="string" required="true" />
		<cfargument name="scretKey" type="string" required="true" />
		<!--- Replace "\n" with "chr(10) to get a correct digest --->
		<cfset var fixedData = replace(arguments.stringIn,"\n","#chr(10)#","all")>
		<!--- Calculate the hash of the information --->
		<cfset var digest = HMAC_SHA1(scretKey,fixedData)>
		<!--- fix the returned data to be a proper signature --->
		<cfset var signature = ToBase64("#digest#")>
		
		<cfreturn signature>
	</cffunction>
	
	<cffunction name="setAccessKey" returntype="string" output="false" access="public" hint="">
		<cfargument name="inpAccessKey" type="string" required="true" />
		
		<cfset variables.accessKey = inpAccessKey/>
	</cffunction>
	
	<cffunction name="setSecretKey" returntype="string" output="false" access="public" hint="">
		<cfargument name="inpSecretKey" type="string" required="true" />
		
		<cfset variables.secretKey = inpSecretKey/>
	</cffunction>
	
	<cffunction name="ReadFileContent" returnformat="json" access="remote" output="false">
		<cfargument name="path" required="yes" type="string"/>
		
		<cfset filePath = expandPath(path) />
		
		<cffile action = "read" 
	    file = "#filePath#"
	    variable = "Message">
		<cfreturn Message>
<!--- 		<cfset dataFile = fileOpen(filePath, "read")/>
		<cfset fileContent = "">
		<cfloop condition="!fileIsEOF( dataFile )">
			<!--- Read the next line. --->
			<cfset line = fileReadLine( dataFile ) />
			
			<cfset fileContent = fileContent & #line# & "<br/>"> 
		</cfloop>
		<cfset fileClose( dataFile ) />
		
		<cfreturn "<br/>" & fileContent> --->
	</cffunction>	

	<cffunction name="generateSignatureBySecretKey" returnformat="json" access="remote" output="false">
		<cfargument name="method" required="true" type="string" default="GET" />
		<cfargument name="secretKey" required="true" type="string" default="" />
		<cfset dateTimeString = GetHTTPTimeString(Now())>
		<!--- Create a canonical string to send --->
		<cfset cs = "#arguments.method#\n\n\n#dateTimeString#\n\n\n">
		<cfset signature = createSignature(cs,secretKey)>
		
		<cfset retval.SIGNATURE = signature>
		<cfset retval.DATE = dateTimeString>
		
		<cfreturn retval>
	</cffunction>
	
</cfcomponent>