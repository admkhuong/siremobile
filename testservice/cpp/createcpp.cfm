<cfinclude template="../paths.cfm" >
<cfscript>
	if(isDefined("form.btnSubmitCpp")){
		 objServiceObject = createObject('component', 'testservice.cfc.ebmService').init(); 
		objServiceObject.createCPP(form.inpDescription);
		location("#rootUrl#/cpp/listCpp.cfm");
	}
</cfscript>
<html>
	<head>
		<title>Create new cpp</title>
	</head>
	<body>
		
<cfoutput>
    <div id="RightStage">
		<cfform method="post" name="">
			Desc <span class="small">Description for CPP</span>
			<cfinput type="text" id="inpDescription" name="inpDescription" required="true" message="Cpp's description is required!"/>
			<cfinput type="submit" name="btnSubmitCpp" value="add"/>
			<cfinput type="button" name="btnCancelCpp" onclick="location.href='listCpp.cfm'" value="Cancel"/>
		</cfform>
    </div>

</cfoutput>

	</body>
</html>