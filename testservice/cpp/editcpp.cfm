<cfparam name="CPPUUID" default="0">
<cfinclude template="../paths.cfm" >
<cfscript>
	// init service object
	 objServiceObject = createObject('component', 'testservice.cfc.ebmService').init(); 
	 if(isDefined("form.btnSubmitUpdateCpp")){
	 	 objServiceObject.updateCpp(
	 		cppUUID = form.inpCPPListCPPUUID,
	 		desc = form.inpCPPListDesc,
	 		cppstyletemplate = form.inpCppStyleTemplate,
	 		facebookMsg = form.inpFacebookMsg,
	 		twittermsg = form.inpTwitterMsg
	 	);
	 }
</cfscript>
<html>
	<head>
		<title>Edit cpp</title>
		<script type="text/javascript" src="<cfoutput>#rootUrl#</cfoutput>/js/jquery-1.7.2.min.js"></script>
	</head>
	<body>
		<cfoutput>
			<!---- Get cpp ----->
			<cfscript>
				 data =  objServiceObject.getCpp(CPPUUID);
			</cfscript>
		    <div>
		                       
			    <cfform name="EditCPPListForm" method="post">
			        <label style="float:none">CPP UUID: #data.CPPUUID#</label>
					<cfinput type="hidden" name="inpCPPListCPPUUID" id="inpCPPListCPPUUID" value="#data.CPPUUID#">
					<br />
		            <label style="float:none">Desc
	                <span class="small">Description for CPP</span>
		            </label>
		              <cfinput TYPE="text" name="inpCPPListDesc" id="inpCPPListDesc" value="#data.Desc#"  /> 
					<fieldset class="ui-corner-all">
						<legend>Style sheet</legend>	
			            <label style="float:none">Optional
			            <span class="small">Style Sheet for CPP</span>
			            </label>
			            <cftextarea id="inpCppStyleTemplate" name="inpCppStyleTemplate" style="min-width:385px; min-height:100px;margin-left:20px" >
				            #data.CPPSTYLETEMPLATE#
						</cftextarea>
						<br /> 
						<br />
					</fieldset>
					
					<br /> 
					<fieldset class="ui-corner-all">
						<legend>Social network default message</legend>
						<br />
						<label style="float:none">
							Facebook default message <span class="small">(less than 400 characters)</span>
				        </label>
						<br />
				        <cftextarea name="inpFacebookMsg" id="inpFacebookMsg" width="400px"  style="width : 385px;height:88px;margin-left:20px" maxlength="400">
					        #data.FACEBOOKMSG#
				        </cftextarea>
				        <br />
				        <br /> 
						<br /> 
				        <label style="float:none">
					        Twitter default message <span class="small">(less than 140 characters)</span>
				        </label>
						<br />
			        	<cftextarea name="inpTwitterMsg" id="inpTwitterMsg"  title="Twitter Message" style="width : 385px;height:88px;margin-left:20px" maxlength="140">
				        	#data.TWITTERMSG#
						</cftextarea>
				        <br /> 
				        <br />
					</fieldset>
					<br />
					<cfinput type="submit" name="btnSubmitUpdateCpp" id="btnSubmitUpdateCpp" value="Update"/>
					<cfinput type="button" name="btnCancelUpdateCpp" id="btnCancelUpdateCpp" value="Cancel" onclick="location.href='listCpp.cfm';"/>
				</cfform>
		    </div>
		</cfoutput>
	
	</body>
</html>
