<cfinclude template="../paths.cfm" ><script language="javascript" src="../../public/js/jquery-1.4.2.js"></script>
<cfdump var="#GetHTTPTimeString(Now())#">
<cfscript>
	 objServiceObject = createObject('component', 'testservice.cfc.ebmService').init(); 
     data =  objServiceObject.getCppList('json');
     
</cfscript>

<html>
	<head>
		<title>List CPP</title>
		<cfoutput>
		<style>
			@import url('#rootUrl#/css/cf_table.css');
			@import url('#rootUrl#/css/iconslist.css');
		</style>
		</cfoutput>	
	</head>
	<body>
		
		<cfif data.SUCCESS EQ false>
			<cfoutput>
				#data.MESSAGE#
			</cfoutput>
		<cfelse>
		
		
		<cfoutput>
			<table class="cf_table">
				<tr>
					<th> CPP UUID</th>
					<th> Desc</th>
					<th>Create</th>
					<th>Option</th>
				</tr>
			<cfloop array="#data.ROWS#" index="index">
				<tr>
						<td>#index.CPP_UUID#</td>
						<td>#index.DESCRIPTION#</td>
						<td>#index.CREATED#</td>
						<td>
							<a href="#rootUrl#/cpp/editCpp.cfm?CPPUUID=#index.CPP_UUID#">
								<img class="ListIconLinks img18_18 edit_18_18" src="#rootUrl#/css/images/blank.gif" title="Edit CPP">
							</a>
							<a href="#rootUrl#/cpp/deleteCpp.cfm?CPPUUID=#index.CPP_UUID#" >
								<img class="ListIconLinks img18_18 delete_18_18" src="#rootUrl#/css/images/blank.gif" title="Edit CPP">
							</a>
						</td>
				</tr>
			</cfloop>
			</table>
		<input type="button" value="Create CPP" onclick="location.href='createCpp.cfm';">
		</cfoutput>
		</cfif>
			<script type="text/javascript">
				 var xmlHTTP ;
			     if (window.XMLHttpRequest) {
				    xmlHTTP= new window.XMLHttpRequest;//For browsers other than ie
				}
				else {
				    try {
				        xmlHTTP= new ActiveXObject("MSXML2.XMLHTTP.3.0"); //for ie
				    }
				    catch(ex) {
				         
				    }
				}
				
				$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: 'http://webservice.seta.com/webservice/ebm/cpp/index.cfm/cpp',   
				data:  { 
							format : 'json',
							datetime: 'Fri, 22 Mar 2013 06:58:40 GMT',
							authorization: '1553B678833359F7ECAC:BuGiWhCt3niSlY/gpLty/E20z/M='},
				error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
				success:		
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d) 
					{
						
												
					} 		
					
				});
		</script>
	</body>
</html>