﻿<cfinclude template="../paths.cfm" >
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Test API</title>
<script src="http://code.jquery.com/jquery-1.11.0.min.js" type="text/javascript"></script>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

</head>
<style>
.formContainer {
    border: 1px solid;
    display: block;
    float: left;
    <!---height: 280px;--->
    margin: 5px;
    padding: 10px;
    width: 300px;
}
.container-fluid {
	padding: 0;
}
.container-fluid .row{
	margin-bottom: 10px;
}
h2 {
	margin-top: 0;
}
h4 {
	font-size: 14px;
}
</style>
<script type="text/javascript" language="javascript">
	//var urlAPI = "<cfoutput>#rootUrl#</cfoutput>"+"/cfc/cppservice.cfc?method=addCppContact";
	$(document).ready(function(){
		$(".btn-submit").click(function(){
			var self = $(this);
			var form = self.parents("form").first();
			$.ajax({
				url: form.attr("action"),
				method: "POST",
				data: form.serialize(),
				success: function(data) {
					console.log(data);
				}
			});
		});
	});
</script>
<body>
	<div id="result" style="width: 100%; background-color:#EFEFEB; padding: 10px; color:Red; display:block;">
		Test API CPP integration to another companies website.
	</div>
	<!---Add Contact --->
 	<div class="formContainer">
    	<form id="cppaddcontact" action="<cfoutput>#rootUrl#</cfoutput>/cfc/cppservice.cfc?method=addCppContact" class="form-inline">
            <h2>cpp add contact</h2>
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
			            <select name="api_method" class="form-control input-sm reqMethod">
							<option value="POST">POST</option>
						</select>
						<code class="resourceUri">/contacts/addcontact</code>
					</div>
					<div class="col-xs-12 col-sm-12">
						<h4>Access Key ID:</h4>
						<input type="text" name="access_key" class="form-control" style="width:100%">
						<h4>Secret Access Key:</h4>
						<input type="text" name="secret_key" class="form-control" style="width:100%">
						<h4>INPCONTACTSTRING:</h4>
						<input type="text" name="INPCONTACTSTRING" class="form-control" style="width:100%">
						<h4>INPCONTACTTYPEID:</h4>
						<input type="text" name="INPCONTACTTYPEID" class="form-control" style="width:100%">
						<h4>INPGROUPID:</h4>
						<input type="text" name="INPGROUPID" class="form-control" style="width:100%">

						<h4>INPUSERSPECIFIEDDATA:</h4>
						<input type="text" name="INPUSERSPECIFIEDDATA" class="form-control" style="width:100%">
						<h4>INP_SOURCEKEY:</h4>
						<input type="text" name="INP_SOURCEKEY" class="form-control" style="width:100%">
						<h4>INPACCOUNTID:</h4>
						<input type="text" name="INPACCOUNTID" class="form-control" style="width:100%">
						<h4>INPAPPENDFLAG:</h4>
						<input type="text" name="INPAPPENDFLAG" class="form-control" style="width:100%">
						<h4>INPALLOWDUPLICATES:</h4>
						<input type="text" name="INPALLOWDUPLICATES" class="form-control" style="width:100%">
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<h4>Accept:</h4>
						<select name="format" class="form-control input-sm reqFormat">
							<option selected="selected" value="json">json</option>
							<option value="xml">xml</option>
						</select>
						<input type="button" class="btn btn-default btn-submit" value="Send" name="Send" />
					</div>
				</div>
			</div>
		</form>
    </div>
	<!---Edit Contact --->
 	<div class="formContainer">
    	<form id="cppaddcontact" action="<cfoutput>#rootUrl#</cfoutput>/cfc/cppservice.cfc?method=editcontact" class="form-inline">
            <h2>edit contact</h2>
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
			            <select name="api_method" class="form-control input-sm reqMethod">
							<option value="POST">POST</option>
						</select>
						<code class="resourceUri">/contacts/editcontact</code>
					</div>
					<div class="col-xs-12 col-sm-12">
						<h4>Access Key ID:</h4>
						<input type="text" name="access_key" class="form-control" style="width:100%">
						<h4>Secret Access Key:</h4>
						<input type="text" name="secret_key" class="form-control" style="width:100%">
						
						<h4>Company_vch:</h4>
						<input type="text" name="Company_vch" class="form-control" style="width:100%">
						<h4>FirstName_vch:</h4>
						<input type="text" name="FirstName_vch" class="form-control" style="width:100%">
						<h4>LastName_vch:</h4>
						<input type="text" name="LastName_vch" class="form-control" style="width:100%">
						<h4>Address_vch:</h4>
						<input type="text" name="Address_vch" class="form-control" style="width:100%">
						<h4>Address1_vch:</h4>
						<input type="text" name="Address1_vch" class="form-control" style="width:100%">
						<h4>City_vch:</h4>
						<input type="text" name="City_vch" class="form-control" style="width:100%">
						<h4>State_vch:</h4>
						<input type="text" name="State_vch" class="form-control" style="width:100%">
						<h4>ZipCode_vch:</h4>
						<input type="text" name="ZipCode_vch" class="form-control" style="width:100%">
						<h4>Country_vch:</h4>
						<input type="text" name="Country_vch" class="form-control" style="width:100%">
						<h4>UserDefinedKey_vch:</h4>
						<input type="text" name="UserDefinedKey_vch" class="form-control" style="width:100%">
						<h4>ContactId_bi:</h4>
						<input type="text" name="ContactId_bi" class="form-control" style="width:100%">
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<h4>Accept:</h4>
						<select name="format" class="form-control input-sm reqFormat">
							<option selected="selected" value="json">json</option>
							<option value="xml">xml</option>
						</select>
						<input type="button" class="btn btn-default btn-submit" value="Send" name="Send" />
					</div>
				</div>
			</div>
		</form>
    </div>
	
	<!---Delete Contact--->
	<div class="formContainer">
    	<form id="cppeditcontact" action="<cfoutput>#rootUrl#</cfoutput>/cfc/cppservice.cfc?method=delcontact" class="form-inline">
            <h2>delete contact</h2>
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
			            <select name="api_method" class="form-control input-sm reqMethod">
							<option value="POST">POST</option>
							<option value="GET">GET</option>
						</select>
						<code class="resourceUri">/contacts/delcontact</code>
					</div>
					<div class="col-xs-12 col-sm-12">
						<h4>Access Key ID:</h4>
						<input type="text" name="access_key" class="form-control" style="width:100%">
						<h4>Secret Access Key:</h4>
						<input type="text" name="secret_key" class="form-control" style="width:100%">
						<h4>INPGROUPID:</h4>
						<input type="text" name="INPGROUPID" class="form-control" style="width:100%">
						<h4>INPCONTACTID:</h4>
						<input type="text" name="INPCONTACTID" class="form-control" style="width:100%">
						<h4>INPCONTACTSTRING:</h4>
						<input type="text" name="INPCONTACTSTRING" class="form-control" style="width:100%">
						<h4>INPCONTACTTYPE:</h4>
						<input type="text" name="INPCONTACTTYPE" class="form-control" style="width:100%">
						<h4>INP_CPPID:</h4>
						<input type="text" name="INP_CPPID" class="form-control" style="width:100%">
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<h4>Accept:</h4>
						<select name="format" class="form-control input-sm reqFormat">
							<option selected="selected" value="json">json</option>
							<option value="xml">xml</option>
						</select>
						<input type="button" class="btn btn-default btn-submit" value="Send" name="Send" />
					</div>
				</div>
			</div>
		</form>
    </div>
		
	<!--- list all contact by cdf --->
	<div class="formContainer">
    	<form id="cppeditcontact" action="<cfoutput>#rootUrl#</cfoutput>/cfc/cppservice.cfc?method=listcontactbycdf" class="form-inline">
            <h2>list all contact by cdf</h2>
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
			            <select name="api_method" class="form-control input-sm reqMethod">
							<option value="POST">POST</option>
							<option value="GET">GET</option>
						</select>
						<code class="resourceUri">/contacts/listcontactbycdf</code>
					</div>
					<div class="col-xs-12 col-sm-12">
						<h4>Access Key ID:</h4>
						<input type="text" name="access_key" class="form-control" style="width:100%">
						<h4>Secret Access Key:</h4>
						<input type="text" name="secret_key" class="form-control" style="width:100%">
						<h4>inpCdfName:</h4>
						<input type="text" name="inpCdfName" class="form-control" style="width:100%">
						<h4>inpCdfValue:</h4>
						<input type="text" name="inpCdfValue" class="form-control" style="width:100%">
						<h4>inpGroupId:</h4>
						<input type="text" name="inpGroupId" class="form-control" style="width:100%">
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<h4>Accept:</h4>
						<select name="format" class="form-control input-sm reqFormat">
							<option selected="selected" value="json">json</option>
							<option value="xml">xml</option>
						</select>
						<input type="button" class="btn btn-default btn-submit" value="Send" name="Send" />
					</div>
				</div>
			</div>
		</form>
    </div>
	<!---list all cpp contact --->
	<div class="formContainer">
    	<form id="cppaddcontact" action="<cfoutput>#rootUrl#</cfoutput>/cfc/cppservice.cfc?method=listContactByCpp" class="form-inline">
            <h2>list all cpp contact</h2>
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
			            <select name="api_method" class="form-control input-sm reqMethod">
							<option value="POST">POST</option>
							<option value="GET">GET</option>
						</select>
						<code class="resourceUri">/contacts/listcontactbycpp</code>
					</div>
					<div class="col-xs-12 col-sm-12">
						<h4>Access Key ID:</h4>
						<input type="text" name="access_key" class="form-control" style="width:100%">
						<h4>Secret Access Key:</h4>
						<input type="text" name="secret_key" class="form-control" style="width:100%">
						<h4>inpCPPUUID:</h4>
						<input type="text" name="inpCPPUUID" class="form-control" style="width:100%">

					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<h4>Accept:</h4>
						<select name="format" class="form-control input-sm reqFormat">
							<option selected="selected" value="json">json</option>
							<option value="xml">xml</option>
						</select>
						<input type="button" class="btn btn-default btn-submit" value="Send" name="Send" />
					</div>
				</div>
			</div>
		</form>
    </div>	
</body>
</html>