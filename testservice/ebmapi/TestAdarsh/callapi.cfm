<!---
  Created by neovinhtru on 20/08/2014.
--->


<cfset inpCPPUUID = "9304052224" >
<cfset access_key = "FDE3568B165B1C510591">
<cfset scret_key = "CAd9fbd60f81e66EAFd79455607e89631eAA41/0">
<cfset inpAccountId = 0>

<cfset sign = generateSignature("POST") />
<cfset datetime = sign.DATE>
<cfset signature = sign.SIGNATURE>
<cfset authorization = access_key & ":" & signature>

<cfset inpEmailCheck = 0/>
<cfset inpSMSCheck = 0/>
<cfset inpVoiceCheck = 0/>
<cfset inpPreferenceCheck = SerializeJSON(["118","150"])/>
<cfset inpEmailObject = SerializeJSON(["email1@gmail.com","email2@gmail.com"])/>
<cfset inpSMSNumberObject = SerializeJSON(["4342342342","2342342342"])/>
<cfset inpVoiceNumberObject = SerializeJSON(["4234234234","5345456456"])/>


<cfhttp url="http://ebmdevii.messagebroadcast.com/webservice/ebm/cpps/9304052224/contacts/multiple" method="POST" result="returnStruct" >
    <cfhttpparam type="header" name="accept" value="json" />
    <cfhttpparam type="header" name="datetime" value="#datetime#" />
    <cfhttpparam type="header" name="authorization" value="#authorization#" />

    <cfhttpparam type="FormField" name="inpCPPUUID" value="#inpCPPUUID#">
    <cfhttpparam type="FormField" name="inpEmailObject" value="#inpEmailObject#">

    <cfhttpparam type="FormField" name="inpPreferenceCheck" value="#inpPreferenceCheck#">
    <cfhttpparam type="FormField" name="inpCDFs" value="{}">
    <cfhttpparam type="FormField" name="inpSMSNumberObject" value="#inpSMSNumberObject#">
    <cfhttpparam type="FormField" name="inpVoiceNumberObject" value="#inpVoiceNumberObject#">
    <cfhttpparam type="FormField" name="inpEmailCheck" value="#inpEmailCheck#">
    <cfhttpparam type="FormField" name="inpSMSCheck" value="#inpSMSCheck#">
    <cfhttpparam type="FormField" name="inpVoiceCheck" value="#inpVoiceCheck#">
    <cfhttpparam type="FormField" name="inpAccountId" value="#inpAccountId#">

</cfhttp>

<cfdump var="#returnStruct#"/>


<cffunction name="generateSignature" returnformat="json" access="remote" output="true">
    <cfargument name="method" required="true" type="string" default="GET" hint="Method" />
    <cfset arguments.method = UCase(arguments.method)>
    <cfset dateTimeString = GetHTTPTimeString(Now())>
    <!--- Create a canonical string to send --->
    <cfset cs = "#arguments.method#\n\n\n#dateTimeString#\n\n\n">
    <cfset signature = createSignature(cs,scret_key)>
    <cfset retval.SIGNATURE = signature>
    <cfset retval.DATE = dateTimeString>
    <cfreturn retval>
</cffunction>

<cffunction name="createSignature" returntype="string" access="public" output="false">
    <cfargument name="stringIn" type="string" required="true" />
    <cfargument name="scretKey" type="string" required="true" />

<!--- Replace "\n" with "chr(10) to get a correct digest --->
    <cfset var fixedData = replace(arguments.stringIn,"\n","#chr(10)#","all")>
<!--- Calculate the hash of the information --->
    <cfset var digest = HMAC_SHA1(scretKey,fixedData)>
<!--- fix the returned data to be a proper signature --->
    <cfset var signature = ToBase64("#digest#")>
    <cfreturn signature>
</cffunction>

<cffunction name="HMAC_SHA1" returntype="binary" access="private" output="false" hint="NSA SHA-1 Algorithm">
    <cfargument name="signKey" type="string" required="true" />
    <cfargument name="signMessage" type="string" required="true" />

    <cfset var jMsg = JavaCast("string",arguments.signMessage).getBytes("iso-8859-1") />
    <cfset var jKey = JavaCast("string",arguments.signKey).getBytes("iso-8859-1") />
    <cfset var key = createObject("java","javax.crypto.spec.SecretKeySpec") />
    <cfset var mac = createObject("java","javax.crypto.Mac") />
    <cfset key = key.init(jKey,"HmacSHA1") />
    <cfset mac = mac.getInstance(key.getAlgorithm()) />
    <cfset mac.init(key) />
    <cfset mac.update(jMsg) />
    <cfreturn mac.doFinal() />
</cffunction>