<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Test JSON</title>
</head>

<body>


<cfset convertjson = SerializeJSON([ 
                                     { 
									 
									 preferenceId="118",
								     preferenceValue="1",
								     preferenceDesc="Checking Description for 118",
								     preferenceGroup="1"
									 
								     }
								   ])/>  
                                        
                                        
<!--- <!--- <cfset convertjson = SerializeJSON([
 {
  Id = "123",
  value = "1",
  Desc = "Checking Description ",
  Group = "1"
 }
])/>        ---> --->                                  
 
Normal Output : <cfoutput>#convertjson#</cfoutput> 

<br/>

Dump Output: <cfdump var="#convertjson#">

</body>
</html>