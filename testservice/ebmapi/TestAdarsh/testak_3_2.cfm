<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Test CPP Contact Multiple</title>
</head>

<body>					

	<!--- generate signature string --->
	<cffunction name="generateSignature" returnformat="json" access="remote" output="false">
		<cfargument name="method" required="true" type="string" default="GET" hint="Method" />
                 
		<cfset dateTimeString = GetHTTPTimeString(Now())>
		<!--- Create a canonical string to send --->
		<cfset cs = "#arguments.method#\n\n\n#dateTimeString#\n\n\n">
		<cfset signature = createSignature(cs,variables.secretKey)>
		
		<cfset retval.SIGNATURE = signature>
		<cfset retval.DATE = dateTimeString>
		
		<cfreturn retval>
	</cffunction>
	
	<!--- Encrypt with HMAC SHA1 algorithm--->
	<cffunction name="HMAC_SHA1" returntype="binary" access="private" output="false" hint="NSA SHA-1 Algorithm">
	   <cfargument name="signKey" type="string" required="true" />
	   <cfargument name="signMessage" type="string" required="true" />
	
	   <cfset var jMsg = JavaCast("string",arguments.signMessage).getBytes("iso-8859-1") />
	   <cfset var jKey = JavaCast("string",arguments.signKey).getBytes("iso-8859-1") />
	   <cfset var key = createObject("java","javax.crypto.spec.SecretKeySpec") />
	   <cfset var mac = createObject("java","javax.crypto.Mac") />
	
	   <cfset key = key.init(jKey,"HmacSHA1") />
	   <cfset mac = mac.getInstance(key.getAlgorithm()) />
	   <cfset mac.init(key) />
	   <cfset mac.update(jMsg) />
	
	   <cfreturn mac.doFinal() />
	</cffunction>
	
	
	<cffunction name="createSignature" returntype="string" access="public" output="false">
	    <cfargument name="stringIn" type="string" required="true" />
		<cfargument name="scretKey" type="string" required="true" />
		<!--- Replace "\n" with "chr(10) to get a correct digest --->
		<cfset var fixedData = replace(arguments.stringIn,"\n","#chr(10)#","all")>
		<!--- Calculate the hash of the information --->
		<cfset var digest = HMAC_SHA1(scretKey,fixedData)>
		<!--- fix the returned data to be a proper signature --->
		<cfset var signature = ToBase64("#digest#")>
		
		<cfreturn signature>
	</cffunction>
    
    
    <cfset inpCPPUUID = "9304052224" >   
    <cfset inpAccountId = 0>
	<cfset variables.accessKey = 'FDE3568B165B1C510591' />
    <cfset variables.secretKey = 'CAd9fbd60f81e66EAFd79455607e89631eAA41/0' />
    <cfset inpEmailCheck = 1/>
    <cfset inpSMSCheck = 1/>
    <cfset inpVoiceCheck = 1/>
    <cfset inpPreferenceCheck = SerializeJSON(["118","150"])/>
    <cfset inpEmailObject = SerializeJSON(["email1@gmail.com","email2@gmail.com","email3@gmail.com"])/>
    <cfset inpSMSNumberObject = SerializeJSON(["4342342342","2342342342"])/>
    <cfset inpVoiceNumberObject = SerializeJSON(["4234234234","5345456456"])/>
   
 	<cfset variables.sign = generateSignature("POST") /> <!---Verb must match that of request type--->
	<cfset datetime = variables.sign.DATE>
    <cfset signature = variables.sign.SIGNATURE>
    
    <cfset authorization = variables.accessKey & ":" & signature>
    
    <cfdump var="#datetime#"> 
    <BR />
    <cfdump var="#signature#"> 
    <BR />
    <cfdump var="#authorization#"> 
    <BR />
    <cfoutput>
        variables.accessKey = #variables.accessKey#
        <BR />
        variables.secretKey = #variables.secretKey#
        <BR />
    </cfoutput>
 
   


<!--- The method="XXX" used in the http request must match the method used to generate key in the generateSignature() function--->
<cfhttp url="https://ebmdevii.messagebroadcast.com/webservice/ebm/cpps/9304052224/contacts/multiple" method="POST" result="returnStruct" >
	
    <cfhttpparam type="header" name="accept" value="json" />
    <cfhttpparam type="header" name="datetime" value="#datetime#" />
    <cfhttpparam type="header" name="authorization" value="#authorization#" />
    <cfhttpparam type="FormField" name="inpCPPUUID" value="#inpCPPUUID#">
    <cfhttpparam type="FormField" name="inpEmailObject" value="#inpEmailObject#">
    <cfhttpparam type="FormField" name="inpPreferenceCheck" value="#inpPreferenceCheck#">
    <cfhttpparam type="FormField" name="inpCDFs" value="{}">
    <cfhttpparam type="FormField" name="inpSMSNumberObject" value="#inpSMSNumberObject#">
    <cfhttpparam type="FormField" name="inpVoiceNumberObject" value="#inpVoiceNumberObject#">
    <cfhttpparam type="FormField" name="inpEmailCheck" value="#inpEmailCheck#">
    <cfhttpparam type="FormField" name="inpSMSCheck" value="#inpSMSCheck#">
    <cfhttpparam type="FormField" name="inpVoiceCheck" value="#inpVoiceCheck#">
    <cfhttpparam type="FormField" name="inpAccountId" value="#inpAccountId#">
          
</cfhttp>
    
    returnStruct<BR />                        
    <cfdump var="#returnStruct#">  
    
    <BR />
    
    <cfoutput>
    	#returnStruct.Filecontent#                      
    </cfoutput>
 
</body>
</html>