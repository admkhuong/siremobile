<cfparam name="inpContactString" default="">
<cfparam name="inpContactId" default="0">
<cfparam name="inpContactTypeId" default="1">
<cfparam name="INPUSERSPECIFIEDDATA" default="">
<cfparam name="INPGROUPID" default="0">
<cfparam name="IsEditSubmit" default="0">
<cfparam name="INPACCOUNTID" default="">
<cfparam name="INPAPPENDFLAG" default="0">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Test Add Contact To Group</title>
</head>

<body>

<cfoutput>
    <div>
    
        <form>
                <input type="hidden" id="IsEditSubmit" name="IsEditSubmit" value="1"/>
                inpContactString <BR />
                <input type="text" id="inpContactString" name="inpContactString" value="#inpContactString#"/><BR />
                
                
                inpContactType <BR />               
                <input type="text" id="inpContactTypeId" name="inpContactTypeId" value="#inpContactTypeId#"/><BR />
                
                INPGROUPID <BR />
                <input type="text" id="INPGROUPID" name="INPGROUPID" value="#INPGROUPID#"/><BR />
                
                
                INPUSERSPECIFIEDDATA <BR />
                <input type="text" id="INPUSERSPECIFIEDDATA" name="INPUSERSPECIFIEDDATA" value="#INPUSERSPECIFIEDDATA#"/><BR />
                
                INPACCOUNTID <BR />
                <input type="text" id="INPACCOUNTID" name="INPACCOUNTID" value="#INPACCOUNTID#"/><BR />                
               
 				Append to Existing Contact <BR />               
                <input type="checkbox" name="INPAPPENDFLAG" id="INPAPPENDFLAG" value="1" <cfif INPAPPENDFLAG GT 0>checked="checked"</cfif>  /> <BR />
               
                <BR />
                <BR />
                                 
                <button id="UpdateContact" name="submit" TYPE="Submit" class="">Update</button>
                
                <BR />
                <BR />
                <BR />
       </form>
                        
    </div>                    

</cfoutput>

	<!--- generate signature string --->
	<cffunction name="generateSignature" returnformat="json" access="remote" output="false">
		<cfargument name="method" required="true" type="string" default="GET" hint="Method" />
                 
		<cfset dateTimeString = GetHTTPTimeString(Now())>
		<!--- Create a canonical string to send --->
		<cfset cs = "#arguments.method#\n\n\n#dateTimeString#\n\n\n">
		<cfset signature = createSignature(cs,variables.secretKey)>
		
		<cfset retval.SIGNATURE = signature>
		<cfset retval.DATE = dateTimeString>
		
		<cfreturn retval>
	</cffunction>
	
	<!--- Encrypt with HMAC SHA1 algorithm--->
	<cffunction name="HMAC_SHA1" returntype="binary" access="private" output="false" hint="NSA SHA-1 Algorithm">
	   <cfargument name="signKey" type="string" required="true" />
	   <cfargument name="signMessage" type="string" required="true" />
	
	   <cfset var jMsg = JavaCast("string",arguments.signMessage).getBytes("iso-8859-1") />
	   <cfset var jKey = JavaCast("string",arguments.signKey).getBytes("iso-8859-1") />
	   <cfset var key = createObject("java","javax.crypto.spec.SecretKeySpec") />
	   <cfset var mac = createObject("java","javax.crypto.Mac") />
	
	   <cfset key = key.init(jKey,"HmacSHA1") />
	   <cfset mac = mac.getInstance(key.getAlgorithm()) />
	   <cfset mac.init(key) />
	   <cfset mac.update(jMsg) />
	
	   <cfreturn mac.doFinal() />
	</cffunction>
	
	
	<cffunction name="createSignature" returntype="string" access="public" output="false">
	    <cfargument name="stringIn" type="string" required="true" />
		<cfargument name="scretKey" type="string" required="true" />
		<!--- Replace "\n" with "chr(10) to get a correct digest --->
		<cfset var fixedData = replace(arguments.stringIn,"\n","#chr(10)#","all")>
		<!--- Calculate the hash of the information --->
		<cfset var digest = HMAC_SHA1(scretKey,fixedData)>
		<!--- fix the returned data to be a proper signature --->
		<cfset var signature = ToBase64("#digest#")>
		
		<cfreturn signature>
	</cffunction>
    
	<!---<cfset variables.accessKey = 'C18F18237081E2437596' />
    <cfset variables.secretKey = 'aD82295273e7025ddD4eCaE4b+b79182A83FE8dF' />--->
    
    <cfset variables.accessKey = '6445F641658C373E7733' />
    <cfset variables.secretKey = '838c47e7DF9937300be34F8A5+b153263e89+4Dc' />
   
 	<cfset variables.sign = generateSignature("POST") /> <!---Verb must match that of request type--->
	<cfset datetime = variables.sign.DATE>
    <cfset signature = variables.sign.SIGNATURE>
    
    <cfset authorization = variables.accessKey & ":" & signature>
    
    <cfdump var="#datetime#"> 
    <BR />
    <cfdump var="#signature#"> 
    <BR />
    <cfdump var="#authorization#"> 
    <BR />
    <cfoutput>
        variables.accessKey = #variables.accessKey#
        <BR />
        
        variables.secretKey = #variables.secretKey#
        <BR />
        
        INPGROUPID = #INPGROUPID#
        <BR />

        inpContactString = #inpContactString#
        <BR />
        
        inpContactTypeId = #inpContactTypeId#
        <BR />
        
        INPACCOUNTID = #INPACCOUNTID#
        <BR />
                
        INPAPPENDFLAG = #INPAPPENDFLAG#
        <BR />
    </cfoutput>
    
   
       
    <cfif TRIM(IsEditSubmit) EQ "1">
    
		<!--- The method="XXX" used in the http request must match the method used to generate key in the generateSignature() function--->
        <cfhttp url="http://ebmdevii.messagebroadcast.com/webservice/ebm/cpp/addcontact" method="POST" result="returnStruct" >
        
           <!--- By default EBM API will return json or XML --->
           <cfhttpparam name="Accept" type="header" value="application/json" />    	
           <cfhttpparam type="header" name="datetime" value="#datetime#" />
           <cfhttpparam type="header" name="authorization" value="#authorization#" /> 
           
                  
           <!--- If DebugAPI is greater than 0 and is specidfied then use Dev DB for testing--->
           <!--- If in debug mode make sure you are using the correct credentials--->
           <cfhttpparam type="formfield" name="DebugAPI" value="0" />
           
           <cfhttpparam type="formfield" name="INPGROUPID" value="#INPGROUPID#" />
           <cfhttpparam type="formfield" name="inpContactString" value="#inpContactString#" />
           <cfhttpparam type="formfield" name="inpContactTypeId" value="#inpContactTypeId#" />
           <cfhttpparam type="formfield" name="INPUSERSPECIFIEDDATA" value="#INPUSERSPECIFIEDDATA#" />
           <cfhttpparam type="formfield" name="INPACCOUNTID" value="#INPACCOUNTID#" />  
           <cfhttpparam type="formfield" name="INPAPPENDFLAG" value="#INPAPPENDFLAG#" />   
              
        </cfhttp>
        
        returnStruct<BR />                        
        <cfdump var="#returnStruct#">  
        
        <BR />
          
        
        <cfoutput>
            #returnStruct.Filecontent#                      
        </cfoutput>
    
</cfif>
 
    
    
 
</body>
</html>