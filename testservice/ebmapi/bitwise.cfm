


<cfset uuid = createobject("java", "java.util.UUID") />

<cfset buffa = uuid.randomUUID().toString() />

<cfset newUUID = uCase(removeChars(buffa, 24, 1)) />

<cfdump var="#uuid#">
<BR />
<cfdump var="#ucase(buffa)#">
<BR />
<cfdump var="#newUUID#">
<BR />

<HR />

<cfset BinaryBuff = 64>

<cfset BitResult52 = BitMaskRead(BinaryBuff, 2, 4) />
<cfset BitResult6 = BitMaskRead(BinaryBuff, 6, 1) />


<BR>
<cfdump var="#BinaryBuff#">
<BR>
<cfdump var="#BitResult52#">
<BR>
<cfdump var="#BitResult6#">
<BR>



<cfset BinaryBuff = 63>

<cfset BitResult52 = BitMaskRead(BinaryBuff, 2, 4) />
<cfset BitResult6 = BitMaskRead(BinaryBuff, 6, 1) />


<BR>
<cfdump var="#BinaryBuff#">
<BR>
<cfdump var="#BitResult52#">
<BR>
<cfdump var="#BitResult6#">
<BR>


<h3>BitMaskRead Example</h3>
<p>Returns integer created from <em>length</em> bits of <em>number</em>, beginning
    with <em>start</em>.</p>

<p>BitMaskRead(255, 4, 4): <cfoutput>#BitMaskRead(255, 4, 4)#</cfoutput></p>
<p>BitMaskRead(255, 0, 4): <cfoutput>#BitMaskRead(255, 0, 4)#</cfoutput></p>
<p>BitMaskRead(128, 0, 7): <cfoutput>#BitMaskRead(128, 0, 7)#</cfoutput></p>
