
<cfparam name="MLPDelayLoad" default="1"> <!--- 1= show, it 0 = ignore it Save certain scripts and code from only loading until on actual MLP - certain js libraries can alter the DOM in ways that get saved into MLP storage and can hose final display - Make available to javascript --->




<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css?_=3.10.29">

<!--- Get more icons here https://www.freefavicon.com/freefavicons/objects/iconinfo/rocket-152-190970.html --->
<link href="data:image/x-icon;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAABILAAASCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAjwAAAAgAAAAAAAAAAAAAABQAAAAUAAAAAAAAAAAAAAAIAAAAjgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAM0AAAC3AAAAFgAAAGkAAAD8AAAA/AAAAGgAAAAWAAAAtwAAAM0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACnAAAA/wAAAMUAAADpAAAA/wAAAP8AAADoAAAAxQAAAP8AAACnAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARgAAAP8AAAD0AAAA/wAAAP8AAAD/AAAA/wAAAPQAAAD/AAAARgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACUAAAA/wAAAP8AAAD/AAAA/wAAAP8AAAD/AAAAlAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAALkAAAD/AAAA/wAAAP8AAAD/AAAAuAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC6AAAA/wAAAP8AAAD/AAAA/wAAALgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwAAAAP8AAAD/AAAA/wAAAP8AAAC+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALcAAAD/AAAAxQAAAMUAAAD/AAAAtAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACaAAAA7wAAAAYAAAAGAAAA7wAAAJgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAZgAAAPwAAABdAAAAXQAAAPwAAABkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABkAAAD6AAAA/wAAAP8AAAD5AAAAGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAmAAAAP8AAAD/AAAAlgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA0AAADJAAAAxwAAAA0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACQAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA7/cAAOZnAADgBwAA4AcAAOAHAADgBwAA8A8AAPgfAAD4HwAA+B8AAPgfAAD4HwAA+B8AAPw/AAD8PwAA/n8AAA==?_=3.18.29" rel="icon" type="image/x-icon">

<!---
OR link to a remote site
<link rel="shortcut icon" type="image/x-icon" href="https://www.bbc.co.uk/favicon.ico?_=3.18.29" />
--->



<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css?_=3.18.29" rel="stylesheet">

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js?_=3.18.29"></script>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>


<!--- BXSlider --->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.12/jquery.bxslider.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.12/vendor/jquery.fitvids.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.12/jquery.bxslider.min.js"></script>


<!--- 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.12/vendor/jquery.easing.1.3.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.12/vendor/jquery.fitvids.js"></script>



<link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.css?_=3.18.29" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js?_=3.18.29"></script> 
	
 --->


<!--- for Google --->
<meta name="description" content="" />
<meta name="keywords" content="" />

<meta name="author" content="" />
<meta name="copyright" content="" />
<meta name="application-name" content="" />

<!--- for Facebook --->          
<meta property="og:title" content="Affordable Mobile Marketing & SMS Marketing" />
<meta property="og:type" content="website" />
<meta property="og:image" content="	https://www.siremobile.com/public/sire/images/home/homeupdate/promotions.jpg" />
<meta property="og:url" content="https://www.siremobile.com/" />
<meta property="og:description" content="You're better than your competition. You deserve a text marketing interface that is better than the rest. Artificial Intelligence is a concept that large enterprises have had the luxury of being able to afford. At Sire, we don't think the big boys should have all the fun. We built our ICI so that SM..." />

<!--- for Twitter --->          
<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="" />
<meta name="twitter:description" content="" />
<meta name="twitter:image" content="" />


<style>
         #MLP-Master-Section
          { font-family: 'Roboto', sans-serif;  word-wrap: break-word; overflow-x: hidden; }
</style>


	<!--- Styles that need to also be on main MLP-X page --->
	<style>
		
		<!--- https://www.w3schools.com/cssref/tryit.asp?filename=trycss_float_clear_overflow --->
		.clearfix::after {
		    content: "";
		    clear: both;
		    display: table;
		}

		.img-responsive {
		    display: block;
		    height: auto;
		    width: 100%;
		    padding: 0;
		}
	
		<!--- https://zellwk.com/blog/responsive-typography/ --->
		<!--- https://css-tricks.com/viewport-sized-typography/ --->
		#MLPMain h1, #main-stage-content h1 {
			font-size: 5.7vw;
		}
		
		#MLPMain h2, #main-stage-content h2 {
			font-size: 4.0vw;
		}
		
		#MLPMain h3, #main-stage-content h3 {
			font-size: 2.8vw;
		}
		
		#MLPMain h4, #main-stage-content h4 {
			font-size: 2.0vw;
		}
		
		#MLPMain h5, #main-stage-content h5 {
			font-size: 1.6vw;
		}
		
		#MLPMain p, #main-stage-content p {
			font-size: 2.5vw;
		}
	
		@media all and (min-width: 800px) {
		  #MLPMain h1, #main-stage-content h1 {
				font-size: 5.7vw;
			}
			
			#MLPMain h2, #main-stage-content h2 {
				font-size: 4.0vw;
			}
			
			#MLPMain h3, #main-stage-content h3 {
				font-size: 2.8vw;
			}
			
			#MLPMain h4, #main-stage-content h4 {
				font-size: 2.0vw;
			}
			
			#MLPMain h5, #main-stage-content h5 {
				font-size: 1.6vw;
			}
			
			#MLPMain p, #main-stage-content p {
				font-size: 2.5vw;
			}
		}
		
		@media all and (min-width: 1200px) {
		  #MLPMain h1, #main-stage-content h1 {
				font-size: 2.65vw;
			}
			
			#MLPMain h2, #main-stage-content h2 {
				font-size: 2.0vw;
			}
			
			#MLPMain h3, #main-stage-content h3 {
				font-size: 1.4vw;
			}
			
			#MLPMain h4, #main-stage-content h4 {
				font-size: 1.0vw;
			}
			
			#MLPMain h5, #main-stage-content h5 {
				font-size: 0.8vw;
			}
			
			#MLPMain p, #main-stage-content p {
				font-size: 1.2vw;
			}
		}
		
		<!--- An article titled “12 Little-Known CSS Facts”, published recently by SitePoint, mentions that <hr> can set its border-color to its parent's color if you specify  --->
		hr { border-color: inherit }

		.MLPMasterSectionContainer {
		    position: relative;
		}
			
		.section-arrow-top:before {
		    left: 50%;
		    -ms-transform: translateX(-50%);
		    transform: translateX(-50%);
		    border: solid transparent;
		    border-color: transparent;
		    content: "";
		    height: 0;
		    pointer-events: none;
		    position: absolute;
		    width: 0;
		    z-index: 1;
		    border-bottom-color: inherit !important;
		    border-width: 0 30px 20px !important;
		    bottom: 100%;
		}
		
		.section-arrow-bottom:before {
		    left: 50%;
		    -ms-transform: translateX(-50%);
		    transform: translateX(-50%);
		    border: solid transparent;
		    border-color: transparent;
		    content: "";
		    height: 0;
		    pointer-events: none;
		    position: absolute;
		    width: 0;
		    z-index: 1;
		    border-top-color: inherit !important;
		    border-width: 20px 30px 0 !important;
		    top: 100%;
		}
		
		.MLPEditable {
		    position: relative;
		    padding: 2em;
		}
	
	</style>	



<style>
	.bx-wrapper, .bx-viewport {
	    height: 250px !important; 
        margin-bottom: 0px;
	}
	
	.ad-image
	{
		max-width: 160px;
		height: 90px;		
		display: block;
		margin: 0 auto;	
	}
	
	.ad-slide
	{
		text-align: center;
	}
	
	.ad-slide p
	{
		
		width: 60%;
		margin-left: 20% !important;
		margin-right: 20% !important;
		margin-top: 2em !important;
	}
	
	
	
	.agency-ad
	{
		position: relative;
		width: 170px;
		height: 230px;
		overflow: hidden;
		margin: .5em 0 0 .5em;		
		background-color: #fff;
		display: inline-block;
		padding: .5em;

	}
	.ad-image
	{
		max-width: 130px;
		max-height: 130px;		
		display: block;
		margin: 0 auto;	
	}
	
	.ad-slide
	{
		text-align: center;
	}
	
	.ad-slide p
	{
		
		width: 60%;
		margin-left: 20% !important;
		margin-right: 20% !important;
		margin-top: 2em !important;
	}

	.ad-expires
	{
		position: absolute;
		bottom: 1em;
		right: 1em;
		text-align: right;
		color: efefef;
		font-size: .7em;		
	}


</style>

<cftry>

 <!--- Only load slider if tag is found - load in mlp index page --->
	          
	          <!--- Dynamically load based on variables --->
	          <!--- 
		          
		          New Table of Ads - try to force MLP, but that might not work
		          New Table of Agents
		          
		          
		          From to add agent
		          	auto add keywords
		          	
		          	
		          Form to add keyword
		          	auto add for each agent
		          	
		          
		          Agent form to build ads to display
		          	Track what agents choose
		          
		          Reporting
		          	Active agents
		          	Ads displayed
		          	Keywords sent
		          	Keywords opted in
		          			
		          
		          New Business Rule CDF Code - Is on List
		          
		          
		          
		          
		          List all ads with filters
		          Set base keyword
		          Set expiration
		          Set Image
		                    
		          
		          Each ad is a panel - only load if not expired
		          Each panel transition will also check expiration
		          
		          Which ads
		          	each ad has a base keyword + widget code
		          	each ad has expiration date
		          
		          
		          
		          
		          Bullet proof: If no influncer id is specified - dont show ad
				  Validate influencer id? Active?
				  
		        
		        
		        ToDo:
		        	Remove/Hide FOUC
		        	
		        
		        
		          
		        --->

	
	<div style="float: right; margin: 3em 3em 0 0;">
	       <div id="Widget" class="MLPMasterSection MLPMasterSectionEmpty MLPEditable droppable draggable sortable sortable-item connectedSortable ui-sortable ui-droppable" data-auto-margin="0" contenteditable="false" style="text-align: center; margin: 0px auto; min-height: 6em; max-width: 225px; padding: 0px;">     
	          
	                   
	           <div id="Header" class="" data-auto-margin="1em auto" contenteditable="false" style="text-align: center; margin: 0px 0px; max-width: 960px; background-color: rgb(0, 140, 186);">
	               <div class="MLPEditable MLPImageContainer draggable sortable-item" contenteditable="false" id="New Image Section" data-helper-text="Image Container" style="color: #fff; float: none; position: static; top: 0px; right: 0px; padding: .5em;" width-units="px" min-width-units="px" max-width-units="px" height-units="px" min-height-units="px" max-height-units="px">
	<!---                     <img src="http://shopsharenetwork.com/wp-content/uploads/2015/11/Screen-Shot-2015-11-16-at-2.05.20-AM.png" style="max-width: 100%; height: 20px;" class="" id="New Image 2" width-units="px" min-width-units="px" max-width-units="px" height-units="20px" min-height-units="px" max-height-units="px"> --->
	
						<h3>Text2Save</h3>
						
						<h5> Enter To Receive Offer On Mobile phone </h5>
						
	                    <div class="MLPEditMenu" style="display: none;">
	                         <span style="min-width:120px;" contenteditable="false"><i class="fa fa-gear mlp-icon mlp-icon-settings"></i> <i class="fa fa-remove mlp-icon-alert" data-tooltip="Are you sure?" style="cursor: pointer;"></i> <i class="fa fa-arrows mlp-icon mlp-icon-drag ui-sortable-handle"></i> <i class="fa fa-image mlp-icon ImageChooserModal" data-toggle="modal" href="#" data-target="#ImageChooserModal" data-image-target="img"></i> <i class=
	                         "fa fa-gears mlp-icon mlp-icon-img-settings"></i></span>
	                    </div>
	               </div>
	
	               <div class="MLPEditMenu" style="display: none;">
	                    <span style="min-width:120px; font-size: 1em;" contenteditable="false"><i class="fa fa-gear mlp-icon mlp-icon-settings"></i> <i class="fa fa-arrows mlp-icon mlp-icon-drag"></i> <i class="fa fa-remove mlp-icon-alert" data-tooltip="Are you sure?" style="cursor: pointer;"></i> <i class="fa fa-columns mlp-icon mlp-icon-split-section"></i></span>
	               </div>
	          </div>
	
	          
	        
	        <div style="text-align: center;">
	          
	          	<ul class="bxslider">
		          	
		        	<cfinclude template="/session/sire/portals/shopsharenetwork/inc_ads.cfm" />
									
				</ul>
			
	        </div>		
	     	     
	        <div contenteditable="true" style="padding:1em; font-size:.5em !important; background-color: rgb(0, 140, 186); color: #fff;">Msg&amp;Data Rates May Apply. Max 3 Msgs Per Week. Consent not required to purchase property, goods or services. To Opt Out, text STOP to {%ShortCode%} For complete terms and conditions visit {%TermsURL%}</div>
	     
			<div contenteditable="true" style="font-size:.5em !important; color: rgb(0, 0, 0); padding: 2px; background-color: rgb(255, 255, 255); border: #ccc solid 1px;"><span style="color: #7fd8dc">SHOPSHARE</span> NETWORK</div>
	     
	     </div>
	     
	</div>     
               
</div>

<!--- <cfdump var="#RetVarReadAgencyAds#" />	 --->

<script TYPE="text/javascript">

	$(document).ready(function(){
						
		var MLPDelayLoad = "<cfoutput>#MLPDelayLoad#</cfoutput>";
		
		if(parseInt(MLPDelayLoad) == 1)
		{
			$('.bxslider').bxSlider({
			  video: true,
			  useCSS: false,
			  pager: false
			});
		}	
		
		
				
		
		<!--- Load Slides --->
		
	
	});
	
	

</script>


 <cfcatch TYPE="any">
                
	<cfdump var="#cfcatch#" />
                
</cfcatch>

</cftry>     



