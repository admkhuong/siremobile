
<cfset ATT_PartnerName = "Legacy" />
<cfset ATT_PartnerPassword = "Legacy" />

<cfset inpContactString = "9494000553" >
<cfset inpShortCode = "4892">
<cfset RetValGetResponse = StructNew() />
<cfset RetValGetResponse.RESPONSE = "EBM to SMPP AT&T SMS Test Message" />

<cfset SendSMPPJSON = StructNew() />
<cfset SendSMPPJSON.shortMessage = "" />
<cfset SendSMPPJSON.sourceAddress = "" />
<cfset SendSMPPJSON.destAddress = "" />
<cfset SendSMPPJSON.registeredDelivery = "" />
<cfset SendSMPPJSON.ApplicationRequestId = "" />
   
   
   
<cfset SendSMPPJSON.shortMessage = "#RetValGetResponse.RESPONSE#" />
<cfset SendSMPPJSON.sourceAddress = "#inpShortCode#" />
<cfset SendSMPPJSON.destAddress = "#inpContactString#" />
<cfset SendSMPPJSON.registeredDelivery = "1" />
<cfset SendSMPPJSON.ApplicationRequestId = "#999999999#" />
            
    
<!--- Track each requests processing tiume to watch for errors on remote end --->
<cfset intHTTPStartTime = GetTickCount() />  
	
<!--- It appears the URL is http://xmlX.us.mblox.com:8180/send where X is the major version number of the format --->
<!--- OLD DNS for outbound - pre LB - http://spx03.mbdt01.com/prod/serviceforwardrequest.cfm--->
<!---Load Balanced to http://spx03.ebm.internal/prod/serviceforwardrequest.cfm as of 2014-09-18 - verify DNS before implementing --->
<cfhttp url="http://10.18.1.44:8083/smpp/sendsmpp.cfm" method="post" resolveurl="no" throwonerror="yes" result="smsreturn" timeout="35">
	<cfhttpparam type="header" name="content-type" value="application/json" />
    <cfhttpparam type="body" value="#SerializeJSON(SendSMPPJSON)#" />                        
</cfhttp>  

<cfset intHTTPTotalTime = (GetTickCount() - intHTTPStartTime) /> 
            

<cfdump var="#intHTTPTotalTime#" />      

<cfdump var="#smsreturn#" />   

<cfdump var="#smsreturn.fileContent#" />  

         
         
         
         <!--- 
		 
		   <cfhttpparam type="header" name="Accept" value="application/json" />
    <cfhttpparam type="header" name="accept-encoding" value="no-compression" />
		 
		 Service forward request SMPP Section - code bu 
		 
		 
		 <!--- This page will need more security if it is to be public - should be on private internal network only --->


<!--- This page will take standar SMPP params and output JSON result data --->
<cfparam name="sourceAddress" default="" />
<cfparam name="shortMessage" default="" />
<cfparam name="destAddress" default="" />
<cfparam name="registeredDelivery" default="1" />
<cfparam name="ApplicationIRequestId" default="" />


<!--- Store result data as structure - return as JSON --->
<cfset ATTSMPPDataOut = StructNew() />
<cfset ATTSMPPDataOut.MESSAGE = "Error - Default Data" />
<cfset ATTSMPPDataOut.BINARYRESULTCODE = 0 />
<cfset ATTSMPPDataOut.PROCTIME = 0 />

<!--- 1 if success, 0 if failure - check ATTSMPPDataOut.MESSAGE for failure resons--->
<cfset ATTSMPPDataOut.RESULTCODE = 0 />
<cfset ATTSMPPDataOut.ERRORCODE = 0 />


<!--- Build the array of possible SMPP Binds --->
<cfset BindArray = ArrayNew(1) />

<!--- Add all of your active binds here. --->
<!--- Is there a way to do this programmactically ???? --->
<cfset ArrayAppend(BindArray, "ATT-SMPP-209.183.36.100-TX1") />
<cfset ArrayAppend(BindArray, "ATT-SMPP-209.183.36.100-TX2") />

<!--- Calculate numnber of binds --->
<cfset NumberOfActiveBinds = ArrayLen(BindArray) />

<!--- Get the next bind randomly --->
<cfset CurrRndBind = RandRange(1, NumberOfActiveBinds, "SHA1PRNG") />

<!--- By default will return same values as service call would have --->
<cfsetting enablecfoutputonly="true">

       	        
<cftry>

    <!--- Start time of request tracking --->
    <cfset intHTTPStartTime = GetTickCount() /> 

               
    <cfif LEN(destAddress) GT 0>
        
        <!--- To send static message to multiple recipeients - submitMulti use http://help.adobe.com/en_US/ColdFusion/9.0/CFMLRef/WSc3ff6d0ea77859461172e0811cbec22c24-77e3.html  --->
        <!--- http://help.adobe.com/en_US/ColdFusion/9.0/CFMLRef/WSc3ff6d0ea77859461172e0811cbec22c24-77e5.html --->
        <cfset msg = structNew() />
        <cfset msg.command = "submit" />
        <cfset msg.shortMessage = shortMessage>
        <cfset msg.destAddress = destAddress>
        <cfset msg.sourceAddress = sourceAddress>
        <cfset msg.registeredDelivery = registeredDelivery />
                       
        <cfset param = structNew() />
        <cfset param.sequence_number = 4 />
        
        <cfset param.optionalParameters = param />
        
        <cfset ATTSMPPDataOut.BINARYRESULTCODE = sendGatewayMessage(BindArray[CurrRndBind], msg) /> 
        
        <!--- Validate success or look for error codes (in logs?) --->
        <cfif LEN(ATTSMPPDataOut.BINARYRESULTCODE) GT 0 >
        
            <cfset ATTSMPPDataOut.RESULTCODE = 1 />
            <cfset ATTSMPPDataOut.MESSAGE = "OK" />
        
        <cfelse>
        
            <!--- Failed to send request --->
            <cfset ATTSMPPDataOut.RESULTCODE = 0 />                
            <cfset ATTSMPPDataOut.MESSAGE = "Error - Destination Address not specified" />
        
        </cfif>
       
        <!--- Add time to send to result --->                 
        <cfset ATTSMPPDataOut.PROCTIME = (GetTickCount() - intHTTPStartTime) /> 
       
     <cfelse>
        
        <!--- Failed to send request --->
        <cfset ATTSMPPDataOut.RESULTCODE = 0 />                
        <cfset ATTSMPPDataOut.MESSAGE = "Error - Destination Address not specified" />
                 
    </cfif>
                       
<cfcatch type="any">

       
    <!--- Failed to send request --->
	<cfset ATTSMPPDataOut.RESULTCODE = 0 />                
    <cfset ATTSMPPDataOut.MESSAGE = "#cfcatch.ErrNumber# #cfcatch.Type# #cfcatch.Message# #cfcatch.Detail#" />
        

</cfcatch>

</cftry>
    

<!--- Output results as JSON --->
<cfoutput>#SerializeJSON(ATTSMPPDataOut)#</cfoutput>
		 
		 
		 
		 
		 
		 
		 <!--- This page will need more security if it is to be public - should be on private internal network only --->
<cfsetting enablecfoutputonly="yes">
<cfsetting showdebugoutput="no">

<!--- This page will take standard SMPP params in a post JSON string as content and output JSON result data --->


<!--- Parse JSON for structured data --->
<cfif cgi.content_type EQ "application/json">                      
	<cfset SendSMPPJSON = DeserializeJSON(ToString(getHTTPRequestData().content)) />  
<cfelse>
	<cfset SendSMPPJSON = StructNew() />
	<cfset SendSMPPJSON.shortMessage = "" />
    <cfset SendSMPPJSON.sourceAddress = "" />
    <cfset SendSMPPJSON.destAddress = "" />
    <cfset SendSMPPJSON.registeredDelivery = "" />
    <cfset SendSMPPJSON.ApplicationRequestId = "" />                              
</cfif>     
                 
<!--- Store result data as structure - return as JSON --->
<cfset ATTSMPPDataOut = StructNew() />
<cfset ATTSMPPDataOut.MESSAGE = "Error - Default Data" />
<cfset ATTSMPPDataOut.BINARYRESULTCODE = 0 />
<cfset ATTSMPPDataOut.PROCTIME = 0 />
<cfset ATTSMPPDataOut.BINDADDR = "Default" />

<!--- 1 if success, 0 if failure - check ATTSMPPDataOut.MESSAGE for failure resons--->
<cfset ATTSMPPDataOut.RESULTCODE = 0 />
<cfset ATTSMPPDataOut.ERRORCODE = 0 />
       	        
<cftry>

	<!--- Start time of request tracking --->
    <cfset intHTTPStartTime = GetTickCount() /> 
    
    <!--- Load bind list from file - every server is optionally different--->
	<cfinclude template="conf_bindlist.cfm" />
    
    <!--- Calculate numnber of binds --->
    <cfset NumberOfActiveBinds = ArrayLen(BindArray) />
    
    <!--- Get the next bind randomly --->
    <cfset CurrRndBind = RandRange(1, NumberOfActiveBinds, "SHA1PRNG") />
    
    <cfset ATTSMPPDataOut.BINDADDR = "#BindArray[CurrRndBind]#" />
               
    <cfif LEN(SendSMPPJSON.destAddress) GT 0>
        
        <!--- To send static message to multiple recipeients - submitMulti use http://help.adobe.com/en_US/ColdFusion/9.0/CFMLRef/WSc3ff6d0ea77859461172e0811cbec22c24-77e3.html  --->
        <!--- http://help.adobe.com/en_US/ColdFusion/9.0/CFMLRef/WSc3ff6d0ea77859461172e0811cbec22c24-77e5.html --->
        <cfset msg = structNew() />
        <cfset msg.command = "submit" />
        <cfset msg.shortMessage = SendSMPPJSON.shortMessage>
        <cfset msg.destAddress = SendSMPPJSON.destAddress>
        <cfset msg.sourceAddress = SendSMPPJSON.sourceAddress>
        <cfset msg.registeredDelivery = SendSMPPJSON.registeredDelivery />
                       
        <cfset param = structNew() />
        <cfset param.sequence_number = 4 />
        
        <cfset param.optionalParameters = param />
        
        <!--- Docs say sendGatewayMessage is ASYN?!?? see http://help.adobe.com/en_US/ColdFusion/9.0/CFMLRef/WSc3ff6d0ea77859461172e0811cbec22c24-61ff.html --->	
        
        <cfset ATTSMPPDataOut.BINARYRESULTCODE = sendGatewayMessage(BindArray[CurrRndBind], msg) /> 
        
        <!--- Validate success or look for error codes (in logs?) --->
        <cfif LEN(ATTSMPPDataOut.BINARYRESULTCODE) GT 0 >
        
            <cfset ATTSMPPDataOut.RESULTCODE = 1 />
            <cfset ATTSMPPDataOut.MESSAGE = "OK" />
        
        <cfelse>
        
            <!--- Failed to send request --->
            <cfset ATTSMPPDataOut.RESULTCODE = 0 />                
            <cfset ATTSMPPDataOut.MESSAGE = "Error - Gateway did not respond." />
        
        </cfif>
       
        <!--- Add time to send to result --->                 
        <cfset ATTSMPPDataOut.PROCTIME = (GetTickCount() - intHTTPStartTime) /> 
       
     <cfelse>
        
        <!--- Failed to send request --->
        <cfset ATTSMPPDataOut.RESULTCODE = 0 />                
        <cfset ATTSMPPDataOut.MESSAGE = "Error - Destination Address not specified" />
                 
    </cfif>
                       
<cfcatch type="any">
       
    <!--- Failed to send request --->
	<cfset ATTSMPPDataOut.RESULTCODE = 0 />                
    <cfset ATTSMPPDataOut.MESSAGE = "#cfcatch.ErrNumber# #cfcatch.Type# #cfcatch.Message# #cfcatch.Detail#" />

</cfcatch>

</cftry>

<!--- Output results as JSON --->
<cfoutput>#SerializeJSON(ATTSMPPDataOut)#</cfoutput>



		 
		 
		 
		 
		 --->