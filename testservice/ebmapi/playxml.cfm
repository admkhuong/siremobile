<cffunction name="encodeStringXml" access="public" returntype="string" output="false" hint="Encode several special characters that make xml invalid">
		<cfargument	name="string" type="string" required="true" hint="" />
		
		<cfset string = Replace(string, '&', '&amp;', 'ALL')/>
		<cfset string = Replace(string, '"', '&quot;', 'ALL')/>
		<cfset string = Replace(string, '<', '&lt;', 'ALL')/>
		<cfset string = Replace(string, '>', '&gt;', 'ALL')/>
		<cfset string = Replace(string, "'", '&apos;', 'ALL')/>		
		
		<cfreturn string>	
	</cffunction>
	
	<cffunction name="decodeStringXml" access="public" returntype="string" 	output="false" hint="Decode xml string">
		<cfargument name="string" type="any" required="true" hint="" />
		
		<cfset string = Replace(string, '&amp;', '&', 'ALL')/>
		<cfset string = Replace(string, '&quot;', '"', 'ALL')/>
		<cfset string = Replace(string, '&lt;', '<', 'ALL')/>
		<cfset string = Replace(string, '&gt;', '>', 'ALL')/>
		<cfset string = Replace(string, '&apos;', "'", 'ALL')/>
		
		<cfreturn string>
	</cffunction>
	
	<cffunction	name="XmlDeleteNodes" access="public" returntype="void"	output="false" hint="I remove a node or an array of nodes from the given XML document.">
		<!--- Define arugments. --->
		<cfargument	name="XmlDocument" type="any" required="true" hint="I am a ColdFusion XML document object."/>
	 
		<cfargument	name="Nodes" type="any"	required="false" hint="I am the node or an array of nodes being removed from the given document."/>
	 
		<!--- Define the local scope. --->
		<cfset var LOCAL = StructNew() />
	 
		<!---
			Check to see if we have a node or array of nodes. If we
			only have one node passed in, let's create an array of
			it so we can assume an array going forward.
		--->
		<cfif NOT IsArray( ARGUMENTS.Nodes )>
	 
			<!--- Get a reference to the single node. --->
			<cfset LOCAL.Node = ARGUMENTS.Nodes />
	 
			<!--- Convert single node to array. --->
			<cfset ARGUMENTS.Nodes = [ LOCAL.Node ] />
	 
		</cfif>
	 
	 
		<!---
			Flag nodes for deletion. We are going to need to delete
			these via the XmlChildren array of the parent, so we
			need to be able to differentiate them from siblings.
			Also, we only want to work with actual ELEMENT nodes,
			not attributes or anything, so let's remove any nodes
			that are not element nodes.
		--->
		<cfloop	index="LOCAL.NodeIndex"	from="#ArrayLen( ARGUMENTS.Nodes )#" to="1"	step="-1">
	 
			<!--- Get a node short-hand. --->
			<cfset LOCAL.Node = ARGUMENTS.Nodes[ LOCAL.NodeIndex ] />
	 
			<!---
				Check to make sure that this node has an XmlChildren
				element. If it does, then it is an element node. If
				not, then we want to get rid of it.
			--->
			<cfif StructKeyExists( LOCAL.Node, "XmlChildren" )>
	 
				<!--- Set delet flag. --->
				<cfset LOCAL.Node.XmlAttributes[ "delete-me-flag" ] = "true" />
	 
			<cfelse>
	 
				<!---
					This is not an element node. Delete it from out
					list of nodes to delete.
				--->
				<cfset ArrayDeleteAt(ARGUMENTS.Nodes, LOCAL.NodeIndex) />
	 
			</cfif>
	 
		</cfloop>
	 
	 
		<!---
			Now that we have flagged the nodes that need to be
			deleted, we can loop over them to find their parents.
			All nodes should have a parent, except for the root
			node, which we cannot delete.
		--->
		<cfloop	index="LOCAL.Node" array="#ARGUMENTS.Nodes#"> 
			<!--- Get the parent node. --->
			<cfset LOCAL.ParentNodes = XmlSearch( LOCAL.Node, "../" ) />
	 
			<!---
				Check to see if we have a parent node. We can't
				delete the root node, and we also be deleting other
				elements as well - make sure it is all playing
				nicely together. As a final check, make sure that
				out parent has children (only happens if we are
				dealing with the root document element).
			--->
			<cfif (
				ArrayLen( LOCAL.ParentNodes ) AND
				StructKeyExists( LOCAL.ParentNodes[ 1 ], "XmlChildren" )
				)>
	 
				<!--- Get the parent node short-hand. --->
				<cfset LOCAL.ParentNode = LOCAL.ParentNodes[ 1 ] />
	 
				<!---
					Now that we have a parent node, we want to loop
					over it's children to one the nodes flagged as
					deleted (and delete them). As we do this, we
					want to loop over the children backwards so that
					we don't go out of bounds as we start to remove
					child nodes.
				--->
				<cfloop
					index="LOCAL.NodeIndex"
					from="#ArrayLen( LOCAL.ParentNode.XmlChildren )#"
					to="1"
					step="-1">
	 
					<!--- Get the current node shorthand. --->
					<cfset LOCAL.Node = LOCAL.ParentNode.XmlChildren[ LOCAL.NodeIndex ] />
	 
					<!---
						Check to see if this node has been flagged
						for deletion.
					--->
					<cfif StructKeyExists( LOCAL.Node.XmlAttributes, "delete-me-flag" )>
	 
						<!--- Delete this node from parent. --->
						<cfset ArrayDeleteAt(
							LOCAL.ParentNode.XmlChildren,
							LOCAL.NodeIndex
							) />
	 
						<!---
							Clean up the node by removing the
							deletion flag. This node might still be
							used by another part of the program.
						--->
						<cfset StructDelete(
							LOCAL.Node.XmlAttributes,
							"delete-me-flag"
							) />
	 
					</cfif>
	 
				</cfloop>
	 
			</cfif>
	 
		</cfloop>
		<!--- Return out. --->
		<cfreturn />
	</cffunction>
	
	<cffunction	name="XmlAppend" access="public" returntype="any" output="false" hint="Copies the children of one node to the node of another document.">
	 
		<!--- Define arguments. --->
		<cfargument	name="NodeA" type="any"	required="true"	hint="The node whose children will be added to."/>
	 
		<cfargument	name="NodeB" type="any"	required="true"	hint="The node whose children will be copied to another document."/>
		<!--- Set up local scope. --->
		<cfset var LOCAL = StructNew() />
		<!---
			Get the child nodes of the originating XML node.
			This will return both tag nodes and text nodes.
			We only want the tag nodes.
		--->
		<cfset LOCAL.Node = ARGUMENTS.NodeB.CloneNode(true) />
		<cfset LOCAL.Node = ARGUMENTS.NodeA.GetOwnerDocument().ImportNode(
				LOCAL.Node,
				JavaCast( "boolean", true )
				) />
	 	<cfset ARGUMENTS.NodeA.AppendChild(
					LOCAL.Node
				) />
	 
		<!--- Return the target node. --->
		<cfreturn ARGUMENTS.NodeA />
	</cffunction>
	
	<cffunction	name="XmlPrepend" access="public" returntype="any" output="false" hint="Copies the children of one node to the node of another document.">
	 
		<!--- Define arguments. --->
		<cfargument	name="NodeA" type="any"	required="true"	/>
	 
		<cfargument	name="NodeB" type="any"	required="true"	hint="The node whose children will be copied to another document."/>
		<!--- Set up local scope. --->
		<cfset var LOCAL = StructNew() />
		<!---
			Get the child nodes of the originating XML node.
			This will return both tag nodes and text nodes.
			We only want the tag nodes.
		--->
		<cfset LOCAL.Node = ARGUMENTS.NodeB.CloneNode(true) />
		<cfset LOCAL.Node = ARGUMENTS.NodeA.GetOwnerDocument().ImportNode(LOCAL.Node,JavaCast( "boolean", true )) />
		<cfset ARGUMENTS.NodeA.InsertBefore(LOCAL.Node, ARGUMENTS.NodeA.GetFirstChild()) /> 
		<!--- Return the target node. --->
		<cfreturn ARGUMENTS.NodeA />
	</cffunction>
	
	
	<cffunction	name="XmlInsertAfter" access="public" returntype="any" output="false" hint="Copies the children of one node to the node of another document.">
	 
		<!--- Define arguments. --->
		<cfargument	name="NodeA" type="any"	required="true"	hint="The node whose children will be added to."/>
	 	<cfargument	name="Pos"   type="any"	required="true"	hint="Position of element B will be insert at Node A"/>
		<cfargument	name="NodeB" type="any"	required="true"	hint="The node whose children will be copied to another document."/>
	
		<!--- Set up local scope. --->
		<cfset var LOCAL = StructNew() />
			<!--- Get the child nodes of the originating XML node.
			This will return both tag nodes and text nodes.
			We only want the tag nodes. --->
	
			<cfset LOCAL.Node = ARGUMENTS.NodeB.CloneNode(true) />
			
	     	<cfset LOCAL.Node = ARGUMENTS.NodeA.GetOwnerDocument().ImportNode(
				LOCAL.Node,
				JavaCast( "boolean", true )
				) />
		    <cfset ARGUMENTS.NodeA.InsertBefore(LOCAL.Node, ARGUMENTS.NodeA.GetChildNodes().Item(Pos))/>
		<!--- Return the target node. --->
		<cfreturn ARGUMENTS.NodeA />
	</cffunction> 
	
	<!--- Find one pos after tag  --->
	<cffunction	name="XmlInsertAfterTag" access="public" returntype="any" output="true" hint="Copies the children of one node to the node of another document.">
	 
		<!--- Define arguments. --->
		<cfargument	name="NodeA" type="any"	required="true"	hint="The node whose children will be added to."/>
	 	<cfargument	name="Tag"   type="any"	required="true"	hint="Position of element B will be insert at Node A"/>
		<cfargument	name="NodeB" type="any"	required="true"	hint="The node whose children will be copied to another document."/>
	
		<!--- Set up local scope. --->
		<cfset var LOCAL = StructNew() />
			<!--- Get the child nodes of the originating XML node.
			This will return both tag nodes and text nodes.
			We only want the tag nodes. --->
	
			<cfset LOCAL.Node = ARGUMENTS.NodeB.CloneNode(true) />
			
	     	<cfset LOCAL.Node = ARGUMENTS.NodeA.GetOwnerDocument().ImportNode(
				LOCAL.Node,
				JavaCast( "boolean", true )
				) />
			<cfdump var="#LOCAL.Node#">
		    <cfset ARGUMENTS.NodeA.InsertBefore(LOCAL.Node, ARGUMENTS.NodeA.GetChildNodes().Item(Pos))/>
		<!--- Return the target node. --->
		<cfreturn ARGUMENTS.NodeA />
	</cffunction> 



<cfsavecontent variable="inpXML">
	<HEADER></HEADER>
<BODY></BODY>
<FOOTER></FOOTER>
<RXSSCSC DESC='' GN='1' RQ='1' X='200' Y='200'>
    <Q AF='NOFORMAT' GID='1' ID='8' IENQID='0' IHOUR='0' IMIN='0' IMRNR='2' INOON='0' INRMO='END' ITYPE='MINUTES' IVALUE='0' MDF='0' OIG='0' REQANS='undefined' RQ='1' TDESC='' TEXT='questions' TYPE='ONESELECTION' errMsgTxt='undefined'>
        <OPTION AVAL='1' ID='1' TEXT='red'>0</OPTION>
        <OPTION AVAL='2' ID='2' TEXT='green'>0</OPTION>
    </Q>
    <Q AF='NOFORMAT' GID='1' ID='2' IENQID='0' IHOUR='0' IMIN='0' IMRNR='2' INOON='0' INRMO='END' ITYPE='MINUTES' IVALUE='0' MDF='0' OIG='0' REQANS='undefined' RQ='2' TDESC='The below message will be sent in response to your choosen keyword.' TEXT='Hello! This is my first keyword response...' TYPE='TRAILER' errMsgTxt='undefined'></Q>
    <Q AF='NOFORMAT' GID='1' ID='4' IENQID='0' IHOUR='0' IMIN='0' IMRNR='2' INOON='0' INRMO='END' ITYPE='MINUTES' IVALUE='0' MDF='0' OIG='0' REQANS='undefined' RQ='3' TDESC='Desc' TEXT='new end' TYPE='STATEMENT' errMsgTxt='undefined'></Q>
    <Q AF='NOFORMAT' GID='1' ID='5' IENQID='0' IHOUR='0' IMIN='0' IMRNR='2' INOON='0' INRMO='END' ITYPE='MINUTES' IVALUE='0' MDF='0' OIG='0' REQANS='undefined' RQ='4' TDESC='Desc' TEXT='4' TYPE='STATEMENT' errMsgTxt='undefined'></Q>
    <Q AF='NOFORMAT' GID='1' ID='10' IENQID='0' IHOUR='0' IMIN='0' IMRNR='3' INOON='0' INRMO='NEXT' ITYPE='SECONDS' IVALUE='0' MDF='0' OIG='0' REQANS='undefined' RQ='5' TDESC='' TEXT='6' TYPE='SHORTANSWER' errMsgTxt='undefined'></Q>
    <Q AF='NOFORMAT' GID='1' ID='12' IENQID='0' IHOUR='0' IMIN='0' IMRNR='3' INOON='0' INRMO='NEXT' ITYPE='SECONDS' IVALUE='0' MDF='0' OIG='0' REQANS='undefined' RQ='6' TDESC='' TEXT='7' TYPE='SHORTANSWER' errMsgTxt='undefined'></Q>
    <Q AF='NOFORMAT' GID='1' ID='6' IENQID='0' IHOUR='0' IMIN='0' IMRNR='2' INOON='0' INRMO='END' ITYPE='MINUTES' IVALUE='0' MDF='0' OIG='0' REQANS='undefined' RQ='7' TDESC='Desc' TEXT='5' TYPE='STATEMENT' errMsgTxt='undefined'></Q>
</RXSSCSC>
<CONFIG BA='1' CT='SMS' FT='' GN='1' LOGO='' RT='' RU='' SN='' THK='' VOICE='0' WC='1' WT=''>0</CONFIG>
<EXP DATE=''>0</EXP>
<DM BS='0' DSUID='338' Desc='Description Not Specified' LIB='0' MT='1' PT='12'>
    <ELE ID='0'>0</ELE>
</DM>
<RXSS></RXSS>
<RULES></RULES>
<CCD CID='9499999998' ESI='-14' SSVMD='20'>0</CCD>
<STAGE h='950' s='1' w='950'>0</STAGE>	
</cfsavecontent>
            
            
<cfoutput>  

Go <BR>

<!--- Parse for data --->
<cftry>
	<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpXML# & "</XMLControlStringDoc>") />
	
	
	
	
<!---
	
	<cfdump var="#myxmldocResultDoc.XMLControlStringDoc.RXSSCSC#" /> 

	<cfdump var="#myxmldocResultDoc.XMLControlStringDoc.RXSSCSC.Q#" /> 
	
		<cfdump var="#myxmldocResultDoc.XMLControlStringDoc.RXSSCSC.XMLChildren#" /> 
	<cfdump var="#myxmldocResultDoc#" /> 
	<cfdump var="#myxmldocResultDoc.xmlroot.xmlchildren#" />
	<cfdump var="#myxmldocResultDoc.XMLControlStringDoc.RXSSCSC#" /> 
--->
	
	
	<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
	
	<BR>
	#htmlcodeformat(OutToDBXMLBuff)#
	<BR>
	
	
	
	
	
	<cfset RXSSCSC = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSCSC") />
	
	
	<cfset OutToDBXMLBuff = ToString(RXSSCSC[1]) />
	
	<BR>
	#htmlcodeformat(OutToDBXMLBuff)#
	<BR>


	                       

<!---
	<cfset xmlElement = myxmldocResultDoc.xmlroot.xmlchildren />
	<cfset nodeName = "RXSSCSC" />

	<cfscript>
	
		nodesFound = 0; 
	    for (i = 1; i LTE ArrayLen(xmlElement.XmlChildren); i = i+1) 
	    { 
	        if (xmlElement.XmlChildren[i].XmlName IS nodeName) 
	            nodesFound = nodesFound + 1; 
	    } 
	
	</cfscript>
	
    <BR>nodesFound = #nodesFound#
	<BR>
--->
	
	<cfset Qs = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSSCSC/Q") />
	
	
	<cfdump var="#Qs#">


	<cfsavecontent variable="inpXML2"><cfoutput><Q ID='101' TYPE='STATEMENT' TEXT='New Text' TDESC='New Des' GID='0' MDF='0' AF='NOFORMAT' OIG='0' ITYPE='MINUTES' IVALUE='0' IHOUR='0' IMIN='0' INOON='0' IENQID='0' IMRNR='2' INRMO='END' REQANS='undefined' errMsgTxt='undefined'></Q></cfoutput></cfsavecontent>
         
	<cfset genEleArr = XmlSearch(inpXML2, "/*") />
	<cfset generatedElement = genEleArr[1] />
         
	<cfdump var="#genEleArr#">
	<cfdump var="#generatedElement#">
	
	

	<cfset PAGE_POSITION = 3 />
	<cfset Counter = 1 />	

	<cfset NewRXSSCSC = "" />
	<cfset NewRXSSCSC &= "<RXSSCSC>#Chr(13)##Chr(10)#" />
	
	
	<!--- Insert First if first count --->
	<cfif PAGE_POSITION LTE Counter>
		<cfset NewRXSSCSC &= "#inpXML2##Chr(13)##Chr(10)#"/> 		
	</cfif>	
	
	<cfloop array="#Qs#" index="item">
		
		<cfset Counter = Counter + 1 />
		
		<cfif PAGE_POSITION EQ Counter>
			<cfset NewRXSSCSC &= "#inpXML2##Chr(13)##Chr(10)#"/>
		</cfif>
		
	    <!--- Clean up XML for DB storage --->                             	
		<cfset OutToDBXMLBuff = ToString(item) />
		<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
		<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
		<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
		<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "'", '&apos;',  'ALL')>
		<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
		<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />
		<cfset NewRXSSCSC &= "#OutToDBXMLBuff##Chr(13)##Chr(10)#"/> 
	
	</cfloop>
	
	<!--- Insert last if beyond current count --->
	<cfif PAGE_POSITION GT Counter>
		<cfset NewRXSSCSC &= "#inpXML2##Chr(13)##Chr(10)#"/> 		
	</cfif>	
	
	
	<cfset NewRXSSCSC &= "</RXSSCSC>"/>

	NewRXSSCSC
	<BR>	
	#htmlcodeformat(NewRXSSCSC)#
	<BR/>

	
	<!--- Parse new XML --->
	<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #NewRXSSCSC# & "</XMLControlStringDoc>") />	
	
	 <!--- make sure to update all RQ PAGE_POSITIONs --->
    <cfset rxssDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc") />

	<!--- Order all questions by RQ --->
    <cfif ArrayLen(rxssDoc) GT 0>
        <!---<cfset XmlAppend(rxssDoc[1],generatedElement) />--->                            
        <cfset rXSSEMElements = XmlSearch(rxssDoc[1], "//Q") />
        <cfset i = 0>
        <cfloop array="#rXSSEMElements#" index="item">
            <cfset i = i + 1>
            <cfset item.XmlAttributes["RQ"] ="#i#"/>            
        </cfloop>
    </cfif>
	
<!---
	
	
	<cfset ArrayInsertAt(Qs, 3, genEleArr) />
	
	<cfdump var="#Qs#">
	
	
	<cfset ArrayInsertAt(myxmldocResultDoc.XMLControlStringDoc.RXSSCSC.XMLChildren, 3, XmlElemNew(myxmldocResultDoc, "Q" ) ) /> 
	
	<cfdump var="#myxmldocResultDoc.XMLControlStringDoc.RXSSCSC.XMLChildren#">
--->
		
	<!--- Clean up XML for DB storage --->                             	
	<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
	<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "") />
	<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL") />
	<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL") />
	<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "'", '&apos;',  'ALL')>
	<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL") />
	<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff) />


	<BR>
	#htmlcodeformat(OutToDBXMLBuff)#
	<BR>
	
	        
       
					
					
<!---
	
	
	<cfset ArrayInsertAt(myxmldocResultDoc., 3, genEleArr) />
	
	<cfdump var="#Qs#">
	
	
	<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) />
	
	<BR>
	#htmlcodeformat(OutToDBXMLBuff)#
	<BR>
	
--->




	
	<cfcatch TYPE="any">
		<!--- Squash bad data  --->
		<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>") />
		
		<cfdump var="#cfcatch#">
		
	</cfcatch>
</cftry>
			
			
</cfoutput>