<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Send multi thread</title>
</head>
<body>

	<cfsetting RequestTimeout="86400"/>
	<cfparam name="inpBatchId" default=""/>
	<cfparam name="inpGoNow" default="0"/>
	<cfparam name="accessKey" default=""/>
	<cfparam name="secretKey" default=""/>
	<cfparam name="inpContactStringStart" default="8888880001"/>
	<cfparam name="inpContactStringEnd" default="8888880100"/>
	<cfparam name="UrlService" default="sire.lc"/>
	<cfparam name="numThread" default="5">
	
	<cfoutput>
		<form method="post">
			<input type="hidden" id="inpGoNow" name="inpGoNow" value="1" />
			<br/>
			<label>Enter Batch Id</label>
			<input type="text" value="#inpBatchId#" id="inpBatchId" name="inpBatchId" />
			<br/><br/>
			<label>Enter Rank Contact String</label>
			<input type="text" value="#inpContactStringStart#" id="inpContactStringStart" name="inpContactStringStart" />
			-
			<input type="text" value="#inpContactStringEnd#" id="inpContactStringEnd" name="inpContactStringEnd" />
			<br/><br/>
			<label>Access Key</label>
			<input type="text" value="#accessKey#" id="accessKey" name="accessKey" style="width:450px" />
			<br/><br/>
			<label>Secret Key</label>
			<input type="text" value="#secretKey#" id="secretKey" name="secretKey" style="width:450px" />
			<br/><br/>
			<label>Domain</label>
			<input type="text" value="#UrlService#" id="UrlService" name="UrlService" style="width:450px" />
			<br/><br/>
			<label>Thread</label>
			<input type="text" value="#numThread#" id="numThread" name="numThread"/>
			<input type="submit"  />
		</form>
		
		<cfif inpGoNow EQ 1>
			
			<!--- generate signature string --->
			<cffunction name="generateSignature" returnformat="json" access="remote" output="false">
				<cfargument name="method" required="true" type="string" default="GET" hint="Method" />
		                 
				<cfset dateTimeString = GetHTTPTimeString(Now())>
				<!--- Create a canonical string to send --->
				<cfset cs = "#arguments.method#\n\n\n#dateTimeString#\n\n\n">
				<cfset signature = createSignature(cs,variables.secretKey)>
				
				<cfset retval.SIGNATURE = signature>
				<cfset retval.DATE = dateTimeString>
				
				<cfreturn retval>
			</cffunction>
			
			<!--- Encrypt with HMAC SHA1 algorithm--->
			<cffunction name="HMAC_SHA1" returntype="binary" access="private" output="false" hint="NSA SHA-1 Algorithm">
			   <cfargument name="signKey" type="string" required="true" />
			   <cfargument name="signMessage" type="string" required="true" />
			
			   <cfset var jMsg = JavaCast("string",arguments.signMessage).getBytes("iso-8859-1") />
			   <cfset var jKey = JavaCast("string",arguments.signKey).getBytes("iso-8859-1") />
			   <cfset var key = createObject("java","javax.crypto.spec.SecretKeySpec") />
			   <cfset var mac = createObject("java","javax.crypto.Mac") />
			
			   <cfset key = key.init(jKey,"HmacSHA1") />
			   <cfset mac = mac.getInstance(key.getAlgorithm()) />
			   <cfset mac.init(key) />
			   <cfset mac.update(jMsg) />
			
			   <cfreturn mac.doFinal() />
			</cffunction>
			
			
			<cffunction name="createSignature" returntype="string" access="public" output="false">
			    <cfargument name="stringIn" type="string" required="true" />
				<cfargument name="scretKey" type="string" required="true" />
				<!--- Replace "\n" with "chr(10) to get a correct digest --->
				<cfset var fixedData = replace(arguments.stringIn,"\n","#chr(10)#","all")>
				<!--- Calculate the hash of the information --->
				<cfset var digest = HMAC_SHA1(scretKey,fixedData)>
				<!--- fix the returned data to be a proper signature --->
				<cfset var signature = ToBase64("#digest#")>
				
				<cfreturn signature>
			</cffunction>

			<cfset variables.accessKey = #accessKey# />
			<cfset variables.secretKey = #secretKey# />
			<cfset variables.sign = generateSignature("POST") />
			<!---Verb must match that of request type--->
			<cfset datetime = variables.sign.DATE />
			<cfset signature = variables.sign.SIGNATURE />
			<cfset authorization = variables.accessKey & ":" & signature />

			<cfset numberContactString = (inpContactStringEnd - inpContactStringStart +1) />
			<cfset numContactOfThred = Ceiling(numberContactString / numThread) />			

			<cfset _logfile = "sendprodqamultithread1" & DateFormat(now(), '-yyyy-mm-dd') & TimeFormat(now(), '-HH-mm-ss') & '.txt'/>
			<cfset _logFilePath = "#ExpandPath('/')#testservice/ebmapi/sendmultithread/#_logfile#" />
			
			<cfset j=1 />
			<cfset _rqn=0 />
			<cfset _rqt=0 />
			<cfset _rqts=[] />
			<cfset _rqtMin = 0 />
			<cfset _rqtMax = 0 />
			<cfset _rqtAvg = 0 />
			<cfset _rqtNum = 0 />
			<cfset _n_m = 0 />
			<cfset _rqtStr = '' />
			<cfset _nas = now() />
			<cfset threadList = '' />
			<cfset i=inpContactStringStart />

			<cfloop index="pageIndex" from="1" to="#numThread#">
				<!---<cfdump var="#i#">--->
				<cfthread name="thr#pageIndex#" intContactStringStart="#i#" thrIndex="#j#" action="run">
					
					<cfloop condition="(intContactStringStart LT (thrIndex * numContactOfThred) + inpContactStringStart) AND intContactStringStart LE inpContactStringEnd">
						
						<!--- --->
						<cftry>
						<!--- --->

						<cfset _ns = GetTickCount() />

					    <!--- The method="XXX" used in the http request must match the method used to generate key in the generateSignature() function --->
					    <cfhttp url="http://#UrlService#/ire/secure/triggerSMS" method="POST" result="returnStruct" >
						
						   <!--- By default EBM API will return json or XML --->
						   <cfhttpparam name="Accept" type="header" value="application/json" />    	
						   <cfhttpparam type="header" name="datetime" value="#datetime#" />
					       <cfhttpparam type="header" name="authorization" value="#authorization#" /> 
					  	   
					      
					       <cfhttpparam type="formfield" name="inpBatchId" value="#inpBatchId#" />
					       <cfhttpparam type="formfield" name="inpContactString" value="#intContactStringStart#" />
					       <cfhttpparam type="formfield" name="inpContactTypeId" value="3" />
					       <!---<cfhttpparam type="formfield" name="inpCompany" value="Tester" />--->
					       <!---<cfhttpparam type="formfield" name="inpCity" value="Corona" />--->
					       <!---<cfhttpparam type="formfield" name="inpState" value="CA" />--->
					       <!---<cfhttpparam type="formfield" name="FirstName_vch" value="Jeffffff" />--->
					       <!---<cfhttpparam type="formfield" name="REPNAME" value="Chester" />--->
					       <cfhttpparam type="formfield" name="inpSkipLocalUserDNCCheck" value="1" />
					       <cfhttpparam type="formfield" name="inpSkipNumberValidations" value="1" />
					     
					       
					       <!---<cfhttpparam type="formfield" name="inpTimeZone" value="PST" />--->
					       
					    </cfhttp>
			
						<cfset _nt = (GetTickCount() - _ns) />
						<!---<cfset arrayAppend(_rqts,_nt) />--->

						<cfset Variables._rqtNum++ />
						<cfset Variables._rqtAvg += (_nt / 1000)/>
						<cfif Variables._rqtMin EQ 0>
							<cfset Variables._rqtMin = _nt />
						<cfelse>
							<cfset Variables._rqtMin = min(Variables._rqtMin,_nt) />
						</cfif>
						<cfif Variables._rqtMax EQ 0>
							<cfset Variables._rqtMax = _nt />
						<cfelse>
							<cfset Variables._rqtMax = max(Variables._rqtMax,_nt) />
						</cfif>

						<!---<cffile action = "append"
								file = "#_logFilePath#"
								output = "#intContactStringStart#: #_nt# milliseconds"
								addNewLine = "yes"
								charset = "utf-8">--->
								<!---mode = "777">--->
						<cfset Variables._rqtStr &= "#intContactStringStart#: #_nt# milliseconds#chr(13)#" />
						
						<!--- --->
						<cfcatch type="Any" >
							<!---<cffile action = "append"
									file = "#_logFilePath#"
									output = "#intContactStringStart#: #SerializeJSON(cfcatch)#"
									addNewLine = "yes"
									charset = "utf-8">--->
									<!---mode = "777">--->
							<cfset Variables._rqtStr &= "#intContactStringStart#: #SerializeJSON(cfcatch)##chr(13)#" />
                        </cfcatch>
                        </cftry>
                        <!--- --->

						<cfset intContactStringStart++ />

						<cfset Variables._n_m++ />
						<cfif Variables._n_m GT 10000>
							<cffile action = "append"
									file = "#Variables._logFilePath#"
									output = "#Variables._rqtStr#"
									addNewLine = "yes"
									charset = "utf-8">
							<cfset Variables._rqtStr = '' />
							<cfset Variables._n_m = 0 />
						</cfif>

					</cfloop>

				</cfthread>

				<cfset listAppend(threadList,"thr#pageIndex#")>
				<cfset i += numContactOfThred />
				<cfset j++ />
			</cfloop>
			
			<cfthread action="join" name="#threadList#"></cfthread>
			
			<cfset _nat = DateDiff("s", _nas, now()) />
						
			<cffile action = "append"
					file = "#_logFilePath#"
					output = "#_rqtStr#"
					addNewLine = "yes"
					charset = "utf-8">

			<cffile action = "append"
					file = "#_logFilePath#"
					output = "Duration time to test: #_nat# seconds"
					addNewLine = "yes"
					charset = "utf-8">
					<!---mode = "777">--->

			<cffile action = "append"
					file = "#_logFilePath#"
					output = "Min of response time: #_rqtMin# milliseconds"
					addNewLine = "yes"
					charset = "utf-8">
					<!---mode = "777">--->

			<cffile action = "append"
					file = "#_logFilePath#"
					output = "Max of response time: #_rqtMax# milliseconds"
					addNewLine = "yes"
					charset = "utf-8">
					<!---mode = "777">--->

			<cffile action = "append"
					file = "#_logFilePath#"
					output = "Avg of response time: #(_rqtAvg/_rqtNum)*1000# milliseconds"
					addNewLine = "yes"
					charset = "utf-8">
					<!---mode = "777">--->

		<br/><br/>
		#_logfile#
		</cfif>
	</cfoutput>

</body>
</html>