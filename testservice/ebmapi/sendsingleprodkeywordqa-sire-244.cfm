<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sample Trigger an Interactive Campaign via API call - Keyword</title>
</head>

<body>

<cfsetting RequestTimeout = "7200">

<cfparam name="inpBatchId" default="">
<cfparam name="inpShortCode" default="QA2Nowhere">
<cfparam name="inpContactString" default="9494000553">
<cfparam name="inpKeyword" default="QA1000Join">
<cfparam name="inpGoNow" default="0">
<cfparam name="accessKey" default="">
<cfparam name="secretKey" default="">
<cfparam name="inpContactStringStart" default="8888880001">
<cfparam name="inpContactStringEnd" default="8888881000">

<cfparam name="UrlService" default="http://ebmdev.seta-international.com.vn/webservice/ebm/pdc/getnextresponse" >

<cfoutput>
    <form>
    
    	<BR />
	    <BR />
        <input type="hidden" id="inpGoNow" name="inpGoNow" value="1"  />
       
        <label>Enter Short Code</label>
        <input type="text" value="#inpShortCode#" id="inpShortCode" name="inpShortCode"  />
        <BR />
        <BR />
        <label>Enter Rank Contact String</label>
        <!---<input type="text" value="#inpContactString#" id="inpContactString" name="inpContactString"  />--->
        <input type="text" value="#inpContactStringStart#" id="inpContactStringStart" name="inpContactStringStart"  /> -  <input type="text" value="#inpContactStringEnd#" id="inpContactStringEnd" name="inpContactStringEnd"  /> 
        <BR />
        <BR />
          <label>Enter keyword to send</label>
        <input type="text" value="#inpKeyword#" id="inpKeyword" name="inpKeyword"  />
        <BR />
        <BR />
        <label>Access Key</label>
		<input type="text" value="#accessKey#" id="accessKey" name="accessKey"  />
        <BR />
        <BR />
		<label>Secret Key</label>
        <input type="text" value="#secretKey#" id="secretKey" name="secretKey"  />
        <BR />
        <BR />
        <label>Domain</label>
        <input type="text" value="#UrlService#" id="UrlService" name="UrlService"  style="width:450px"/>
        <BR />
        <BR />
        
        <input type="submit"  />
    
    </form>
</cfoutput>


<cfif inpGoNow EQ 1>
	
<cfset startTime = Now()>
StartTime: <cfoutput>#startTime# </cfoutput><br>


	<!--- generate signature string --->
	<cffunction name="generateSignature" returnformat="json" access="remote" output="false">
		<cfargument name="method" required="true" type="string" default="GET" hint="Method" />
                 
		<cfset dateTimeString = GetHTTPTimeString(Now())>
		<!--- Create a canonical string to send --->
		<cfset cs = "#arguments.method#\n\n\n#dateTimeString#\n\n\n">
		<cfset signature = createSignature(cs,variables.secretKey)>
		
		<cfset retval.SIGNATURE = signature>
		<cfset retval.DATE = dateTimeString>
		
		<cfreturn retval>
	</cffunction>
	
	<!--- Encrypt with HMAC SHA1 algorithm--->
	<cffunction name="HMAC_SHA1" returntype="binary" access="private" output="false" hint="NSA SHA-1 Algorithm">
	   <cfargument name="signKey" type="string" required="true" />
	   <cfargument name="signMessage" type="string" required="true" />
	
	   <cfset var jMsg = JavaCast("string",arguments.signMessage).getBytes("iso-8859-1") />
	   <cfset var jKey = JavaCast("string",arguments.signKey).getBytes("iso-8859-1") />
	   <cfset var key = createObject("java","javax.crypto.spec.SecretKeySpec") />
	   <cfset var mac = createObject("java","javax.crypto.Mac") />
	
	   <cfset key = key.init(jKey,"HmacSHA1") />
	   <cfset mac = mac.getInstance(key.getAlgorithm()) />
	   <cfset mac.init(key) />
	   <cfset mac.update(jMsg) />
	
	   <cfreturn mac.doFinal() />
	</cffunction>
	
	
	<cffunction name="createSignature" returntype="string" access="public" output="false">
	    <cfargument name="stringIn" type="string" required="true" />
		<cfargument name="scretKey" type="string" required="true" />
		<!--- Replace "\n" with "chr(10) to get a correct digest --->
		<cfset var fixedData = replace(arguments.stringIn,"\n","#chr(10)#","all")>
		<!--- Calculate the hash of the information --->
		<cfset var digest = HMAC_SHA1(scretKey,fixedData)>
		<!--- fix the returned data to be a proper signature --->
		<cfset var signature = ToBase64("#digest#")>
		
		<cfreturn signature>
	</cffunction>
    
	<cfset variables.accessKey = #accessKey# />
    <cfset variables.secretKey = #secretKey# />
   
 	<cfset variables.sign = generateSignature("POST") /> <!---Verb must match that of request type--->
	<cfset datetime = variables.sign.DATE>
    <cfset signature = variables.sign.SIGNATURE>
    
    <cfset authorization = variables.accessKey & ":" & signature>
    
    <cfdump var="#datetime#"> 
    <BR />
    <cfdump var="#signature#"> 
    <BR />
    <cfdump var="#authorization#"> 
    <BR />
    <cfoutput>
        variables.accessKey = #variables.accessKey#
        <BR />
        variables.secretKey = #variables.secretKey#
        <BR />
    </cfoutput>
    
    <cfset inpFormDataJSON = StructNew() />
    
    <cfset inpFormDataJSON.FirstName = "Lee" />
    <cfset inpFormDataJSON.LastName = "Peterson" />
    
   
    <cfloop index="index" from="#inpContactStringStart#" to="#inpContactStringEnd#">
    	    
    <!--- The method="XXX" used in the http request must match the method used to generate key in the generateSignature() function  --->
    <cfhttp url= #UrlService# method="POST" result="returnStruct" >
	
	   <!--- By default EBM API will return json or XML --->
	   <cfhttpparam name="Accept" type="header" value="application/json" />    	
	   <cfhttpparam type="header" name="datetime" value="#datetime#" />
       <cfhttpparam type="header" name="authorization" value="#authorization#" /> 
  	                          
       <cfhttpparam type="formfield" name="inpShortCode" value="#inpShortCode#" />
       <cfhttpparam type="formfield" name="inpContactString" value="#index#" /> <!--- jpeterson@messagebroadcast.com (2)  9494000553 {"INPFIRSTNAME":"LEE"} --->
       <cfhttpparam type="formfield" name="inpContactTypeId" value="2" />
       <cfhttpparam type="formfield" name="inpNewLine" value="\n\r" />
       <cfhttpparam type="formfield" name="inpKeyword" value="#inpKeyword#" />
       <cfhttpparam type="formfield" name="inpQATool" value="0" />
       <cfhttpparam type="formfield" name="inpFormDataJSON" value="#SerializeJSON(inpFormDataJSON)#" />
       <cfhttpparam type="formfield" name="inpSkipLocalUserDNCCheck" value="1" />
       <cfhttpparam type="formfield" name="inpSkipNumberValidations" value="1" />
       <cfhttpparam type="formfield" name="inpQATool" value="0" />
              
    </cfhttp>
    
    <cfhttp url= #UrlService# method="POST" result="returnStruct1" >
	
	   <!--- By default EBM API will return json or XML --->
	   <cfhttpparam name="Accept" type="header" value="application/json" />    	
	   <cfhttpparam type="header" name="datetime" value="#datetime#" />
       <cfhttpparam type="header" name="authorization" value="#authorization#" /> 
  	                          
       <cfhttpparam type="formfield" name="inpShortCode" value="#inpShortCode#" />
       <cfhttpparam type="formfield" name="inpContactString" value="#index#" /> <!--- jpeterson@messagebroadcast.com (2)  9494000553 {"INPFIRSTNAME":"LEE"} --->
       <cfhttpparam type="formfield" name="inpContactTypeId" value="2" />
       <cfhttpparam type="formfield" name="inpNewLine" value="\n\r" />
       <cfhttpparam type="formfield" name="inpKeyword" value="yes" />
       <cfhttpparam type="formfield" name="inpQATool" value="0" />
       <cfhttpparam type="formfield" name="inpFormDataJSON" value="#SerializeJSON(inpFormDataJSON)#" />
       <cfhttpparam type="formfield" name="inpSkipLocalUserDNCCheck" value="1" />
       <cfhttpparam type="formfield" name="inpSkipNumberValidations" value="1" />
       <cfhttpparam type="formfield" name="inpQATool" value="0" />
              
    </cfhttp>
    </cfloop>
    <!---
    returnStruct1<BR />                        
    <cfdump var="#returnStruct1#"> ---> 
    
    
    <cfoutput>
    	
    	#returnStruct1.Filecontent#                      
    </cfoutput>
	<BR />
	<cfset endTime = Now()>
	<cfoutput>#endTime#</cfoutput>

</cfif>


 
</body>
</html>