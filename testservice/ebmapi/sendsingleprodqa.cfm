<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sample Trigger an Interactive Campaign via API call - Batch Id</title>
</head>

<body>


<cfparam name="inpBatchId" default="">
<cfparam name="inpGoNow" default="0">

<cfoutput>
    <form>
    
    	<BR />
	    <BR />
        <input type="hidden" id="inpGoNow" name="inpGoNow" value="1"  />
        <label>Enter Batch Id</label>
        <BR />
        <input type="text" value="#inpBatchId#" id="inpBatchId" name="inpBatchId"  />
        <BR />
        <BR />
        <BR />
        <BR />
        <input type="submit"  />
    
    
    </form>
</cfoutput>


<cfif inpGoNow EQ 1>


	<!--- generate signature string --->
	<cffunction name="generateSignature" returnformat="json" access="remote" output="false">
		<cfargument name="method" required="true" type="string" default="GET" hint="Method" />
                 
		<cfset dateTimeString = GetHTTPTimeString(Now())>
		<!--- Create a canonical string to send --->
		<cfset cs = "#arguments.method#\n\n\n#dateTimeString#\n\n\n">
		<cfset signature = createSignature(cs,variables.secretKey)>
		
		<cfset retval.SIGNATURE = signature>
		<cfset retval.DATE = dateTimeString>
		
		<cfreturn retval>
	</cffunction>
	
	<!--- Encrypt with HMAC SHA1 algorithm--->
	<cffunction name="HMAC_SHA1" returntype="binary" access="private" output="false" hint="NSA SHA-1 Algorithm">
	   <cfargument name="signKey" type="string" required="true" />
	   <cfargument name="signMessage" type="string" required="true" />
	
	   <cfset var jMsg = JavaCast("string",arguments.signMessage).getBytes("iso-8859-1") />
	   <cfset var jKey = JavaCast("string",arguments.signKey).getBytes("iso-8859-1") />
	   <cfset var key = createObject("java","javax.crypto.spec.SecretKeySpec") />
	   <cfset var mac = createObject("java","javax.crypto.Mac") />
	
	   <cfset key = key.init(jKey,"HmacSHA1") />
	   <cfset mac = mac.getInstance(key.getAlgorithm()) />
	   <cfset mac.init(key) />
	   <cfset mac.update(jMsg) />
	
	   <cfreturn mac.doFinal() />
	</cffunction>
	
	
	<cffunction name="createSignature" returntype="string" access="public" output="false">
	    <cfargument name="stringIn" type="string" required="true" />
		<cfargument name="scretKey" type="string" required="true" />
		<!--- Replace "\n" with "chr(10) to get a correct digest --->
		<cfset var fixedData = replace(arguments.stringIn,"\n","#chr(10)#","all")>
		<!--- Calculate the hash of the information --->
		<cfset var digest = HMAC_SHA1(scretKey,fixedData)>
		<!--- fix the returned data to be a proper signature --->
		<cfset var signature = ToBase64("#digest#")>
		
		<cfreturn signature>
	</cffunction>
    
	<!---<cfset variables.accessKey = 'B0B4CD1919F4A43C81E4' />
    <cfset variables.secretKey = '99916CCf54+bb3bDC7b4D20Fb73bc/3eA852212e' />--->
        <cfset variables.accessKey = '0615228793BA36B80276' />
    <cfset variables.secretKey = '3bB64cB793Ba03241623/e7714A40162769A1/89' />

   
 	<cfset variables.sign = generateSignature("POST") /> <!---Verb must match that of request type--->
	<cfset datetime = variables.sign.DATE>
    <cfset signature = variables.sign.SIGNATURE>
    
    <cfset authorization = variables.accessKey & ":" & signature>
    
    <cfdump var="#datetime#"> 
    <BR />
    <cfdump var="#signature#"> 
    <BR />
    <cfdump var="#authorization#"> 
    <BR />
    <cfoutput>
        variables.accessKey = #variables.accessKey#
        <BR />
        variables.secretKey = #variables.secretKey#
        <BR />
    </cfoutput>
    
    <!--- The method="XXX" used in the http request must match the method used to generate key in the generateSignature() function --->
    <cfhttp url="http://dev.ebmui.com/webservice/ebm/pdc/AddToRealTime" method="POST" result="returnStruct" >
	
	   <!--- By default EBM API will return json or XML --->
	   <cfhttpparam name="Accept" type="header" value="application/json" />    	
	   <cfhttpparam type="header" name="datetime" value="#datetime#" />
       <cfhttpparam type="header" name="authorization" value="#authorization#" /> 
  	   
      
       <cfhttpparam type="formfield" name="inpBatchId" value="#inpBatchId#" />
       <cfhttpparam type="formfield" name="inpContactString" value="9494000553" /> <!--- jpeterson@messagebroadcast.com (email is type 2)  9494000553 --->
       <cfhttpparam type="formfield" name="inpContactTypeId" value="3" />
       <cfhttpparam type="formfield" name="inpCompany" value="Tester" />
       <cfhttpparam type="formfield" name="inpCity" value="Corona" />
       <cfhttpparam type="formfield" name="inpState" value="CA" />       
       <cfhttpparam type="formfield" name="FirstName_vch" value="Jeffffff" />       
       <cfhttpparam type="formfield" name="REPNAME" value="Chester" />
       <cfhttpparam type="formfield" name="inpSkipLocalUserDNCCheck" value="1" />
       <cfhttpparam type="formfield" name="inpSkipNumberValidations" value="1" />
     
       
       <!---<cfhttpparam type="formfield" name="inpTimeZone" value="PST" />--->
       
    </cfhttp>
    
    returnStruct<BR />                        
    <cfdump var="#returnStruct#">  
    
    <BR />
    
    <cfoutput>
    	#returnStruct.Filecontent#                      
    </cfoutput>

</cfif>


 
</body>
</html>
