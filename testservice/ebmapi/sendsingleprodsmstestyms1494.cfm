<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Test Add To Real Time - YMS</title>
</head>

<body>
		    
    <cfset authorization = "511C2175BBC5564E2CF0:yms.mc">
       
    <cfdump var="#authorization#"> 
    <BR />
       
    <!--- The method="XXX" used in the http request must match the method used to generate key in the generateSignature() function--->
 	<cfhttp url="http://ebmapi.messagebroadcast.com/webservice/ebm/pdc/addtorealtime" method="POST" result="returnStruct" >
	   
       <!--- Required components --->
    
	   <!--- By default EBM API will return json or XML --->
	   <cfhttpparam name="Accept" type="header" value="application/json" /> 	
       <cfhttpparam type="header" name="datetime" value="#GetHTTPTimeString(Now())#" />  
       <cfhttpparam type="header" name="authorization" value="#authorization#" /> 
  	                 
       <!--- Batch Id controls which pre-defined campaign to run --->
       <cfhttpparam type="formfield" name="inpBatchId" value="1494" />
       
       <!--- Contact string Canada Test number 6138601757 --->
       <cfhttpparam type="formfield" name="inpContactString" value="9494000553" />
       
       <!--- 1=voice 2=email 3=SMS--->
       <cfhttpparam type="formfield" name="inpContactTypeId" value="3" />      
       
       <!--- Allows for douple opt in from form --->
       <cfhttpparam type="formfield" name="inpSkipLocalUserDNCCheck" value="1" />
              
      
    </cfhttp>
    
    returnStruct<BR />                        
    <cfdump var="#returnStruct#">  
    
    <BR />
    
    <cfoutput>
    	#returnStruct.Filecontent#                      
    </cfoutput>
 
</body>
</html>