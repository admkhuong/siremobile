<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Test Add To Queue</title>
</head>

<body>

	<!--- generate signature string --->
	<cffunction name="generateSignature" returnformat="json" access="remote" output="false">
		<cfargument name="method" required="true" type="string" default="GET" hint="Method" />
                 
		<cfset dateTimeString = GetHTTPTimeString(Now())>
		<!--- Create a canonical string to send --->
		<cfset cs = "#arguments.method#\n\n\n#dateTimeString#\n\n\n">
		<cfset signature = createSignature(cs,variables.secretKey)>
		
		<cfset retval.SIGNATURE = signature>
		<cfset retval.DATE = dateTimeString>
		
		<cfreturn retval>
	</cffunction>
	
	<!--- Encrypt with HMAC SHA1 algorithm--->
	<cffunction name="HMAC_SHA1" returntype="binary" access="private" output="false" hint="NSA SHA-1 Algorithm">
	   <cfargument name="signKey" type="string" required="true" />
	   <cfargument name="signMessage" type="string" required="true" />
	
	   <cfset var jMsg = JavaCast("string",arguments.signMessage).getBytes("iso-8859-1") />
	   <cfset var jKey = JavaCast("string",arguments.signKey).getBytes("iso-8859-1") />
	   <cfset var key = createObject("java","javax.crypto.spec.SecretKeySpec") />
	   <cfset var mac = createObject("java","javax.crypto.Mac") />
	
	   <cfset key = key.init(jKey,"HmacSHA1") />
	   <cfset mac = mac.getInstance(key.getAlgorithm()) />
	   <cfset mac.init(key) />
	   <cfset mac.update(jMsg) />
	
	   <cfreturn mac.doFinal() />
	</cffunction>
	
	
	<cffunction name="createSignature" returntype="string" access="public" output="false">
	    <cfargument name="stringIn" type="string" required="true" />
		<cfargument name="scretKey" type="string" required="true" />
		<!--- Replace "\n" with "chr(10) to get a correct digest --->
		<cfset var fixedData = replace(arguments.stringIn,"\n","#chr(10)#","all")>
		<!--- Calculate the hash of the information --->
		<cfset var digest = HMAC_SHA1(scretKey,fixedData)>
		<!--- fix the returned data to be a proper signature --->
		<cfset var signature = ToBase64("#digest#")>
		
		<cfreturn signature>
	</cffunction>
    
	<!--- <cfset variables.accessKey = '6445F641658C373E7733' />
    <cfset variables.secretKey = '838c47e7DF9937300be34F8A5+b153263e89+4Dc' /> --->
	
	<!--- joeblow@aol3.com Dev --->
	<!---<cfset variables.accessKey = 'C18F18237081E2437596' />
    <cfset variables.secretKey = 'aD82295273e7025ddD4eCaE4b+b79182A83FE8dF' />--->
    
    <!--- Prod --->
    <cfset variables.accessKey = 'E05BE8685A8F38033D38' />
    <cfset variables.secretKey = 'C60DDdc4C8453bD0e33315D0aE3+c8E4e/4e5a+0' />
   
 	<cfset variables.sign = generateSignature("POST") /> <!---Verb must match that of request type--->
	<cfset datetime = variables.sign.DATE>
    <cfset signature = variables.sign.SIGNATURE>
    
    <!--- Test using Secret Name instead --->
   <!--- <cfset signature = "AT&T">--->
    
    
    <cfset authorization = variables.accessKey & ":" & signature>
    
    <cfdump var="#datetime#"> 
    <BR />
    <cfdump var="#signature#"> 
    <BR />
    <cfdump var="#authorization#"> 
    <BR />
    <cfoutput>
        variables.accessKey = #variables.accessKey#
        <BR />
        variables.secretKey = #variables.secretKey#
        <BR />
    </cfoutput>
    
    <!--- ebmapiint.ebm.internal The method="XXX" used in the http request must match the method used to generate key in the generateSignature() function--->
 	<!---
		<cfhttp url="http://#CGI.SERVER_NAME#/webservice/ebm/pdc/AddToRealTime" method="POST" result="returnStruct" >
		<cfhttp url="http://ebmui.com/webservice/ebm/pdc/AddToRealTime" method="POST" result="returnStruct" >
	--->
    
    	<cfhttp url="http://ebmapiint.ebm.internal/webservice/ebm/pdc/AddToRealTime" method="POST" result="returnStruct" >
	
	    <!--- By default EBM API will return json or XML --->
	    <cfhttpparam name="Accept" type="header" value="application/json" />    	
	    <cfhttpparam type="header" name="datetime" value="#datetime#" />
        <cfhttpparam type="header" name="authorization" value="#authorization#" /> 
  	   
      <!---  <cfargument name="inpCampaignId" required="yes" default="0" hint="The pre-defined campaign Id you created under your account.">
        <cfargument name="inpContactString" required="yes" default="" hint="The contact string - Phone number ,eMail, or SMS number">
        <cfargument name="inpContactTypeId" required="no" default="" hint="1=Phone number, 2=SMS, 3=eMail">
        <cfargument name="inpInternational" required="no" default="0" hint=" 1 will flag this number as international">
        <cfargument name="inpValidateScheduleActive" required="no" default="0" hint="1 will halt message if time is currently outside of the pre-defined campaigns's scheduled range">
        <cfargument name="inpTimeZone" required="no" default="">
        <cfargument name="inpCompany" required="no" default="">
        <cfargument name="inpFirstName" required="no" default="">
        <cfargument name="inpLastName" required="no" default="">
        <cfargument name="inpAddress" required="no" default="">
        <cfargument name="inpAddress1" required="no" default="">
        <cfargument name="inpCity" required="no" default="">
        <cfargument name="inpState" required="no" default="">
        <cfargument name="inpZipCode" required="no" default="">
        <cfargument name="inpCountry" required="no" default="">
        <cfargument name="inpUserDefinedKey" required="no" default="">       
        <cfargument name="inpMMLS" required="no" default="1">
        <cfargument name="inpScheduleOffsetSeconds" required="no" default="0">--->
       
       
       <!--- If DebugAPI is greater than 0 and is specidfied then use Dev DB for testing--->
       <!--- If in debug mode make sure you are using the correct credentials--->
       <cfhttpparam type="formfield" name="DebugAPI" value="0" />
       
       <cfhttpparam type="formfield" name="inpBatchId" value="1255" />
       <cfhttpparam type="formfield" name="inpContactString" value="9494000553" />
       <cfhttpparam type="formfield" name="inpContactTypeId" value="3" />
       <cfhttpparam type="formfield" name="inpTimeZone" value="PST" />
       <cfhttpparam type="formfield" name="inpSkipNumberValidations" value="1" />
       
       
       
       <cfhttpparam type="formfield" name="inpCompany" value="Tester" />
       <cfhttpparam type="formfield" name="inpCity" value="Corona" />
       <cfhttpparam type="formfield" name="inpState" value="CA" />       
       <cfhttpparam type="formfield" name="FirstName_vch" value="Jeffffff" />       
          
    </cfhttp>
    
    returnStruct<BR />                        
    <cfdump var="#returnStruct#">  
    
    <cfmail server="smtp.gmail.com" 
                    username="noreply@contactpreferenceportal.com" 
                    password="ghj##$@##Tz09" port="465" 
                    useSSL="true" 
                    to="jpeterson@messagebroadcast.com" 
                    from="noreply@contactpreferenceportal.com" 
                    subject="SMS PDC Realtime Testing"
                    type="html"
                >
                    <cfdump var="#returnStruct#"/>
                
                </cfmail>
                
    
    <BR />
    
    <cfoutput>
    	#returnStruct.Filecontent#                      
    </cfoutput>
 
</body>
</html>


<cfsetting showdebugoutput="true">