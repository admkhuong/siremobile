<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Test Send Syniverse</title>
</head>

<body>


<!--- 


Method [POST] : Sending an SMS
This method is used to send an SMS message from your application to the Aerialink Gateway which will be delivered to the end users mobile phone. The request is initiated by your application.

Request
The following request parameters are available for this resource and method

Required Parameters
source [string(15)]
The source code for the message. This must match to one of your codes associated with your connection.

destination [string(15)]
The destination number that you want to send the SMS message to. Aerialink accepts almost any format while in “friendly format” mode (see the parameter destinationFormat below). The recommendation for US based numbers is the 11 digit format: “13105551212”.

messageText [string(160)]
The SMS message text that you are sending to the end user handset device. This is limited to the general standard of 160 characters unless concatenation is enabled on your account. In some cases your character limit may be less depending on your specific use case and configuration or a carrier limitation.

Optional Parameters
destinationFormat [integer]
Defaults to 0 for “friendly format” which is the default. If this parameter is passed with a value of 1 then Aerialink will expect the destination number in “international format” with the country code (ex. “xXXXxxxXXXX”). This is required for any international messaging.

registeredDelivery [integer]
Defaults to 0. If set to 1, this will request a delivery receipt from the carrier which will be delivered to the URL that you specify in your account configuration.

destinationPort [string(10)]
Use this parameter when you need your MT (mobile terminate) SMS to be directed to a specific port on the mobile handset device. This parameter should not be passed when sending standard SMS Text messages that are intended for the end user to read.

programID [string(25)]
Use this parameter to specify a program ID that is directly related to an approved and certified program or campaign with the carrier/operators. This value is typically only required for Dedicated Short Codes and will be provided as part of your carrier certification.

dcs [integer]
Use this to specify the character set that should be used for the message. Defaults to 0 for the SMSC alphabet. Refer to the Data Coding Scheme page for supported DCS values.

Response (Synchronous)
Response Properties
The parameters or elements returned in the response to an API request.

version : The requested version of the resource
resource : The requested resource name
method : The method for the request. This will always be one of the following; GET, POST, UPDATE, DELETE
type : The request type; Syncronous or Asyncronous
transactionGUID : The globally unique transaction ID for the message
destination : Operating company directory number
Response Schema
JSON Format(default schema)

{
  "aerialink": {
    "version": "v4",
    "resource": "messages",
    "method": "post",
    "type": "synchronous",
    "transactions": [
      {
        "transaction": {
          "transactionGUID": "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
          "destination": "12223334444"
        }
      }
    ]
  }
}
	
	
	
 --->
	
	<cfset MosaicUserName = "96ebeca83061707d830e8c31999db21c" />
	<cfset MosaicPassword = "a536d756c494036f57efa68b331065f2" />
	
	
	<cfset MosacicKeys = MosaicUserName & ':' & MosaicPassword />
	<cfset MosacicKeysBase64 = ToBase64(MosacicKeys)/>
	<cfset Authorization = "Basic #ToBase64( MosaicUserName & ':' & MosaicPassword)#"/>
	
	
	<cfdump var="#MosacicKeys#" />
	<cfdump var="#MosacicKeysBase64#" />
	<cfdump var="#Authorization#" />
    
   
    <!--- 
	    https://apix.aerialink.net/v4/messages.json
	    
	    https://api.aerialink.net/20100101/sms/submit/
	
		<cfhttpparam name="Accept" type="header" value="application/json" />  
	
		 <cfhttpparam type="header" name="Authorization" value="Basic #ToBase64( MosaicUserName & ':' & MosaicPassword)#" />
	
	--->
    
    <!--- The method="XXX" used in the http request must match the method used to generate key in the generateSignature() function--->
 	<cfhttp url="https://text.mes.syniverse.com/SMSSend" method="POST" result="returnStruct" timeout="30"  >
	
	    <!--- By default EBM API will return json or XML --->
	      	
		<cfhttpparam type="formfield" name="user" value="4798" />
		<cfhttpparam type="formfield" name="pass" value="Tl1@Vw2!" />
		<cfhttpparam type="formfield" name="smsfrom" value="28776" />
		<cfhttpparam type="formfield" name="smsto" value="19494000553" />
		<cfhttpparam type="formfield" name="smsmsg" value="Hello World POST Testing!" />
		<cfhttpparam type="formfield" name="split" value="4" />
                
    </cfhttp>
    
    returnStruct<BR />                        
    <cfdump var="#returnStruct#">  
    
    <BR />
    
    <cfoutput>
    	#returnStruct.Filecontent#                      
    </cfoutput>
    
<!---
    <cfif IsJSON(returnStruct.Filecontent)>
    	<cfset PostResultJSON = deSerializeJSON(returnStruct.Filecontent) />
    
    
		<cfdump var="#PostResultJSON#" />

    
		<cfif StructKeyExists(PostResultJSON, "aerialink")>
			
			<BR>aerialink Found!
			
			<cfif StructKeyExists(PostResultJSON.aerialink, "transactions")>
				
				<BR>transactions Found!
				
				<cfdump var="#PostResultJSON.aerialink.transactions[1]#" /> 
				
				<cfif StructKeyExists(PostResultJSON.aerialink.transactions[1], "transaction")>
				
					<BR>transaction Found!
	
					<cfdump var="#PostResultJSON.aerialink.transactions[1].transaction#" /> 
				
					transactionGUID = <cfoutput>#PostResultJSON.aerialink.transactions[1].transaction.transactionGUID#</cfoutput>
					
				</cfif>	
				
			</cfif>	
	                        	
			
			
		<cfelse>
		
			<BR>aerialink NOT Found!           	
	                        	
       	</cfif>	
       	
    <cfelse>
    	
    	<cfset PostResultJSON = returnStruct.Filecontent />
    	<cfdump var="#PostResultJSON#" />

    </cfif>
--->
    
        
    	

 
</body>
</html>