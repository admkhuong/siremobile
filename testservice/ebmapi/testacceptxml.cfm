<cfdump 
    var="#GetHttpRequestData()#" 
    label="Get Http Request Data"
    />

<!---<!---
    All data that is posted to this page will now
    be part of the HTTP Request data structure. Get
    a reference to this structure.
--->
<cfset objRequest = GetHttpRequestData() />

<!---
    Our XML data will be the content of the request.
    Let's grab it out of the request structure. When
    we do this, trim the value to help create a
    valid XML string.
--->
<cfset strXML = objRequest.Content.Trim() />

<!---
    At this point, we may or may not have a valid XML
    string. Before we try to do any parsing, let's
    validate it.
--->
<cfif IsXml( strXML )>

    <!---
        Now that we know the request was of valid
        format, we can parse the XML document.
    --->
    <cfset xmlData = XmlParse( strXML ) />

    <!---
        For this demo, we are going to validate the
        request ONLY for Credit / Debit.
    --->
    <cfset xmlType = XmlSearch(
        xmlData,
        "/ResponseService/Header"
        ) />

    <!---
        Check to see if we found a Type node as a child
        to the transaction root node.
    --->
    <cfif ArrayLen( xmlType )>

        <!---
            We found the type - this request has been
            validated. Now, we just need to return a
            confirmation message.
        --->
        <cfset strResponse = ("#xmlType[ 1 ].XmlText# approved") />

    <cfelse>

        <!--- We did not find a valid node. --->
        <cfset strResponse = "ERROR2: Missing type node" />

    </cfif>

<cfelse>

    <!---
        The passed in data was not a valid XML string.
        Store our error into our response message.
    --->
    <cfset strResponse = "ERROR1: Invalid request (Bad XML)" />

</cfif>


<!---
    ASSERT: At this point, no matter what happend (if the
    request was valid or invalid) we will have a message
    in our response string.
--->


<!--- Return response string. --->
<cfcontent
    type="text/plain"
    variable="#ToBinary( ToBase64( strResponse ))#"
    />--->