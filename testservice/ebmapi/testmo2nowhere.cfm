<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sample Trigger an Interactive Campaign via MO call - Keyword</title>


</head>


<body>

<cfinclude template="../../session/cfc/csc/constants.cfm" > <!--- Use one SMS constants in EBM so as not to confuse or miss any --->
<cfinclude template="../../session/cfc/csc/inc_smsdelivery.cfm">
<cfinclude template="../../session/cfc/csc/inc_ext_csc.cfm">
    

<cfparam name="inpBatchId" default="">
<cfparam name="inpShortCode" default="QA2Nowhere">
<cfparam name="inpContactString" default="9494000553">
<cfparam name="inpKeyword" default="QA1000Join">
<cfparam name="inpGoNow" default="0">
<cfparam name="inpVerboseDebug" default="0">


<cfset DBSourceEBM = "Bishop" />
<cfset Session.DBSourceEBM = "Bishop" />


<cfoutput>

	<h1>MO Testing and QA Page</h1>
    <form>
    
    	<BR />
	    <BR />
        <input type="hidden" id="inpGoNow" name="inpGoNow" value="1"  />
       
        <label>Enter Short Code</label>
        <BR />
        <input type="text" value="#inpShortCode#" id="inpShortCode" name="inpShortCode"  />
        <BR />
        <BR />
        <label>Enter Contact String</label>
        <BR />
        <input type="text" value="#inpContactString#" id="inpContactString" name="inpContactString"  />
        <BR />
        <BR />
          <label>Enter keyword to send</label>
        <BR />
        <input type="text" value="#inpKeyword#" id="inpKeyword" name="inpKeyword"  />
        <BR />
        <BR />
        <label>Verbose Debug Info</label>
        <BR />
        <input type="text" value="#inpVerboseDebug#" id="inpVerboseDebug" name="inpVerboseDebug"  />
        <BR />
        <BR />
        <BR />
        <BR />
        <input type="submit"  />
    
    </form>
</cfoutput>


<cfif inpGoNow EQ 1>


	
	
    <cfset transactionId = "#LSDateFormat(NOW(), 'yyyymmdd')##LSTimeFormat(NOW(), 'HHmmss')#">
    <cfset carrier = "10">
    <cfset shortCode = inpShortCode>
    <cfset keyword = inpKeyword>
    <cfset time = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" >            
    <cfset ServiceId = 0>	
    <!--- Need to get UDH string from future MBlox --->
    <cfset UDHString = "">	
    <cfset XMLDATA = "#inpShortCode# #inpKeyword# #inpContactString#">	        
	
	<cfset inpContactStringBuff = inpContactString />
	<!--- MBLOX uses 1's but EBM does not - messes with time zone and api calls - scrub leading ones here --->            
    <cfloop condition="LEFT(inpContactStringBuff,1) EQ '1' AND LEN(inpContactStringBuff) GT 1">
        <cfset inpContactStringBuff = RIGHT(inpContactStringBuff, LEN(inpContactStringBuff) - 1) />
    </cfloop>
                   	
	                   
				<!--- Check for instant process for now - still need processer externally SMSQCODE_READYTOPROCESS OR SMSQCODE_PROCESSED --->
                <!--- Check if TYPE is INTERVAL or API or .... and force into queue --->
            
				<!--- Process here if allowed by BR - this will slow down rate of messages accepted but can be offset with load balancers and DB mirrors --->
                <cfif 1 EQ 1>
                
                	<!--- Log the fact that we processed to the DB --->    
                    <cfinvoke                    
                         method="QueueNextResponseSMS"
                         returnvariable="RetValQueueNextResponseSMS">                         
                            <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>
                            <cfinvokeargument name="inpCarrier" value="#carrier#"/>
                            <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
                            <cfinvokeargument name="inpKeyword" value="#keyword#"/>
                            <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>
                            <cfinvokeargument name="inpPreferredAggregator" value="10"/>
                            <cfinvokeargument name="inpTime" value="#time#"/>
                            <cfinvokeargument name="inpScheduled" value=""/>
                            <cfinvokeargument name="inpQueueState" value="#SMSQCODE_PROCESSED_ON_MO#"/>
                            <cfinvokeargument name="inpServiceId" value="#ServiceId#"/>
                            <cfinvokeargument name="inpXMLDATA" value="#XMLDATA#"/>
                            <cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>
                    </cfinvoke>                      
                            
                    <cfif inpVerboseDebug gt 0>
	                    <h4>RetValQueueNextResponseSMS</h4>        	
                    	<cfdump var="#RetValQueueNextResponseSMS#"/>                
                    </cfif>
                                                
                	<!--- Validate this is not a duplicate ... --->
                    <cfinvoke                    
                         method="CheckForDuplicateMO"
                         returnvariable="RetValCheckForDuplicateMO">                         
                            <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>                            
                            <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
                            <cfinvokeargument name="inpKeyword" value="#keyword#"/> 
                            <cfinvokeargument name="inpLastQueuedId" value="#RetValQueueNextResponseSMS.LASTQUEUEDUPID#"/> 
                            <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>                         
                    </cfinvoke>  
                    
                    <cfif inpVerboseDebug gt 0>
                    	<h4>RetValCheckForDuplicateMO</h4>    	
                    	<cfdump var="#RetValCheckForDuplicateMO#"/>                
                    </cfif>
                    
                    <!--- Check for duplicate --->
                    <cfif RetValCheckForDuplicateMO.RXRESULTCODE EQ 2 OR RetValCheckForDuplicateMO.RXRESULTCODE EQ 3 >
                    	
                        <!--- Flag as duplicate --->
                    
                    	<!--- Log Duplicates ignored--->
                        <cfinvoke                    
                             method="LogDuplicateResponseSMS"
                             returnvariable="RetValLogDuplicateResponseSMS">                         
                                <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>
                                <cfinvokeargument name="inpCarrier" value="#carrier#"/>
                                <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
                                <cfinvokeargument name="inpKeyword" value="#keyword#"/>
                                <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>
                                <cfinvokeargument name="inpPreferredAggregator" value="10"/>
                                <cfinvokeargument name="inpTime" value="#time#"/>
                                <cfinvokeargument name="inpScheduled" value=""/>
                                <cfinvokeargument name="inpQueueState" value="#SMSQCODE_PROCESSED_ON_MO#"/>
                                <cfinvokeargument name="inpServiceId" value="#ServiceId#"/>
                                <cfinvokeargument name="inpXMLDATA" value="#XMLDATA#"/>
                                <cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>
                        </cfinvoke>  
                        
                        <cfif inpVerboseDebug gt 0>
                    		<h4>RetValLogDuplicateResponseSMS</h4>    	
                    		<cfdump var="#RetValLogDuplicateResponseSMS#"/>                
                    	</cfif>
                        
                        <!--- Store result ???  BATCHID ??? LOOKUP LAST BATCH???--->
                        
                    <cfelse> <!--- Not a duplicate --->
                    
						<!--- Validate not a concatenated message --->
                        <cfif LEN(UDHString) EQ 12>
                         	                                                    
                            <!--- Only process for next response on first part of message --->
                            <cfif RIGHT(UDHString, 2 ) EQ "01" >
                            
                                <!--- Process next response --->
                                <cfinvoke                    
                                     method="ProcessNextResponseSMS"
                                     returnvariable="RetValProcessNextResponseSMS">                         
                                        <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>
                                        <cfinvokeargument name="inpCarrier" value="#carrier#"/>
                                        <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
                                        <cfinvokeargument name="inpKeyword" value="#keyword#"/>
                                        <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>
                                        <cfinvokeargument name="inpPreferredAggregator" value="10"/>
                                        <cfinvokeargument name="inpTime" value="#time#"/>
                                        <cfinvokeargument name="inpServiceId" value="#ServiceId#"/> <!--- MO SERVICEID NOT SAME AS MT ONE USED TO SEND ??? --->
                                        <cfinvokeargument name="inpXMLDATA" value="#XMLDATA#"/>
                                        <cfinvokeargument name="inpOverrideAggregator" value="1"/>
                                        <cfinvokeargument name="inpLastQueuedId" value="#RetValQueueNextResponseSMS.LASTQUEUEDUPID#"/>
                                        <cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>
                                </cfinvoke>
                                                        
                                <cfinvoke                    
                                     method="InsertMessagePart"
                                     returnvariable="RetValInsertMessagePart">                         
                                        <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>                                        
                                        <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
                                        <cfinvokeargument name="inpKeyword" value="#keyword#"/>
                                        <cfinvokeargument name="inpSurveyResultsId" value="#RetValProcessNextResponseSMS.LASTSURVEYRESULTID#"/>  
                                        <cfinvokeargument name="inpReferenceNumber" value="#MID(UDHString, 7, 2 )#"/>
                                        <cfinvokeargument name="inpTotalParts" value="#MID(UDHString, 9, 2 )#"/>
                                        <cfinvokeargument name="inpPartNumber" value="#RIGHT(UDHString, 2 )#"/>                                         
                                </cfinvoke>
                                                            
                                <!--- Check if all parts of the message have arrived yet inside of InsertMessagePart function --->
                                
                            <cfelse> <!--- Alternate message part - not part 1 --->
                            
                                <!--- Log message MO just dont respond to it --->
                                
                                <cfinvoke                    
                                     method="InsertMessagePart"
                                     returnvariable="RetValInsertMessagePart">                         
                                        <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>                                        
                                        <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
                                        <cfinvokeargument name="inpKeyword" value="#keyword#"/>
                                        <cfinvokeargument name="inpReferenceNumber" value="#MID(UDHString, 7, 2 )#"/>
                                        <cfinvokeargument name="inpTotalParts" value="#MID(UDHString, 9, 2 )#"/>
                                        <cfinvokeargument name="inpPartNumber" value="#RIGHT(UDHString, 2 )#"/>  
                                </cfinvoke>                              
                                
                                <!--- Check if all parts of the message have arrived yet inside of InsertMessagePart function --->
                            
                            </cfif> <!--- Only process for next response on first part of message --->
                        
                        <cfelse> <!--- not a concatenated message --->
                       
                            <!--- Process next response --->
                            <cfinvoke                    
                                 method="ProcessNextResponseSMS"
                                 returnvariable="RetValProcessNextResponseSMS">                         
                                    <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>
                                    <cfinvokeargument name="inpCarrier" value="#carrier#"/>
                                    <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
                                    <cfinvokeargument name="inpKeyword" value="#keyword#"/>
                                    <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>
                                    <cfinvokeargument name="inpPreferredAggregator" value="10"/>
                                    <cfinvokeargument name="inpTime" value="#time#"/>
                                    <cfinvokeargument name="inpServiceId" value="#ServiceId#"/> <!--- MO SERVICEID NOT SAME AS MT ONE USED TO SEND ??? --->
                                    <cfinvokeargument name="inpXMLDATA" value="#XMLDATA#"/>
                                    <cfinvokeargument name="inpOverrideAggregator" value="1"/>
                                    <cfinvokeargument name="inpLastQueuedId" value="#RetValQueueNextResponseSMS.LASTQUEUEDUPID#"/>
                                    <cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>                                    
                            </cfinvoke>
                            
                            <cfif inpVerboseDebug gt 0>
                                <h4>RetValProcessNextResponseSMS</h4>    	
                                <cfdump var="#RetValProcessNextResponseSMS#"/>                
                            </cfif>
                        
                        </cfif> <!--- Validate not a concatenated message --->
                	
                    </cfif> <!--- Check for duplicate --->
                                                
                <cfelse>

					<!--- Queue up future response tracking in DB--->                    
                    <cfinvoke                    
                         method="QueueNextResponseSMS"
                         returnvariable="RetValQueueNextResponseSMS">                         
                            <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>
                            <cfinvokeargument name="inpCarrier" value="#carrier#"/>
                            <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
                            <cfinvokeargument name="inpKeyword" value="#keyword#"/>
                            <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>
                            <cfinvokeargument name="inpPreferredAggregator" value="10"/>
                            <cfinvokeargument name="inpTime" value="#time#"/>
                            <cfinvokeargument name="inpScheduled" value=""/>
                            <cfinvokeargument name="inpQueueState" value="#SMSQCODE_SECONDARYQUEUE#"/>
                            <cfinvokeargument name="inpServiceId" value="#ServiceId#"/>
                            <cfinvokeargument name="inpXMLDATA" value="#XMLDATA#"/>
                            <cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>
                    </cfinvoke>                                
                
            	</cfif>            
            
    
	
	
	
	
	

</cfif>


 
</body>
</html>