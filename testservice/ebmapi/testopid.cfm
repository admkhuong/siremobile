<cfset MBLOX_PartnerName_With_Carrier = "SireMobileUS">
<cfset MBLOX_PartnerPassword_With_Carrier = "fR8Bjx3V">



<!---

Requests to the Operator ID Service itself should be directed to the following URL:
https://soap.mblox.com/operatoridservice/lookup



<?xml version="1.0" encoding="UTF-8"?>
<wsdl:definitions name="SubscriberLookup" targetNamespace="urn:subscriberlookup.soap.mblox.com/1/0" xmlns:tns="urn:subscriberlookup.soap.mblox.com/1/0" xmlns:types="urn:types.soap.mblox.com/1/1" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/">
        <wsdl:types>
                <xsd:schema elementFormDefault="qualified" xmlns:xsd="http://www.w3.org/2001/XMLSchema" targetNamespace="urn:subscriberlookup.soap.mblox.com/1/0">
                        <xsd:import namespace="urn:types.soap.mblox.com/1/1" schemaLocation="soapframework.xsd"/>
                        <xsd:include schemaLocation="subscriberlookup.xsd"/>
                        <xsd:element name="GetSubscriberDetailsArgs" type="tns:SubscriberLookupRequestType"/>
                        <xsd:element name="GetSubscriberDetailsResult" type="types:ResponseType"/>
                </xsd:schema>
        </wsdl:types>
        <wsdl:message name="GetSubscriberDetailsResponse">
                <wsdl:part name="GetSubscriberDetailsResult" element="tns:GetSubscriberDetailsResult"/>
        </wsdl:message>
        <wsdl:message name="GetSubscriberDetailsRequest">
                <wsdl:part name="GetSubscriberDetailsArgs" element="tns:GetSubscriberDetailsArgs"/>
        </wsdl:message>
        <wsdl:portType name="subscriberLookup">
                <wsdl:operation name="GetSubscriberDetails">
                        <wsdl:input message="tns:GetSubscriberDetailsRequest"/>
                        <wsdl:output message="tns:GetSubscriberDetailsResponse"/>
                </wsdl:operation>
        </wsdl:portType>
        <wsdl:binding name="subscriberLookupSOAP" type="tns:subscriberLookup">
                <soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http"/>
                <wsdl:operation name="GetSubscriberDetails">
                        <soap:operation soapAction="GetSubscriberDetails"/>
                        <wsdl:input>
                                <soap:body use="literal"/>
                        </wsdl:input>
                        <wsdl:output>
                                <soap:body use="literal"/>
                        </wsdl:output>
                </wsdl:operation>
        </wsdl:binding>
        <wsdl:service name="subscriberLookup">
                <wsdl:port name="subscriberLookupSOAP" binding="tns:subscriberLookupSOAP">
                        <soap:address location="http://soap.mblox.com/operatoridservice/lookup"/>
                </wsdl:port>
        </wsdl:service>
</wsdl:definitions>



Request:
 <soapenv:Envelope
 xmlns:q0="urn:subscriberlookup.soap.mblox.com/1/0"
 xmlns:q1="urn:types.soap.mblox.com/1/1"
 xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
 xmlns:xsd="http://www.w3.org/2001/XMLSchema"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
 <soapenv:Body>
 <q0:GetSubscriberDetailsArgs>
 <q1:ClientDetails>
 <q1:ClientUserName>Bolt</q1:ClientUserName>
 <q1:ClientPassword>Sm0keYha1R</q1:ClientPassword>
 <q1:ProductId>400</q1:ProductId>
 </q1:ClientDetails>
 <q1:ClientRequestReference>abc-def-ghi-jkl
</q1:ClientRequestReference>
 <q0:TelephoneNumber>14084808856</q0:TelephoneNumber>
 <q0:LookupOperation>OperatorIDService
</q0:LookupOperation>
 </q0:GetSubscriberDetailsArgs>
 </soapenv:Body>
 </soapenv:Envelope> 


<?xml version="1.0" encoding="utf-8"?>


Sample Result

<?xml version='1.0' encoding='UTF-8'?>
	<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
		<soapenv:Body>
			<ns2:GetSubscriberDetailsResult xmlns:ns2="urn:subscriberlookup.soap.mblox.com/1/0">
			
				<Success xmlns="urn:types.soap.mblox.com/1/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns2:SubscriberLookupSuccessResponseType"><ClientRequestReference>123456</ClientRequestReference><MBloxTransactionReference>56254f73-26e2-4700-9d84-3c6f3234e7f1</MBloxTransactionReference><ns2:SubscriberInfo><ns2:TelephoneNumber>19494000553</ns2:TelephoneNumber><ns2:NetworkDetails><ns2:DetailName>OperatorIDService</ns2:DetailName><ns2:DetailStatus>FOUND</ns2:DetailStatus>
			
				<ns2:OperatorId>31002</ns2:OperatorId><ns2:PSMSEnabled>true</ns2:PSMSEnabled></ns2:NetworkDetails></ns2:SubscriberInfo></Success>
			
			</ns2:GetSubscriberDetailsResult>
		</soapenv:Body>
	</soapenv:Envelope>

17148691767




--->



<!---
    We are going to subscribe to Campaing Monitor using the
    AddAndResubscribe actions. This is a SOAP-based method that
    requires the following XML body.
--->
<cfsavecontent variable="soapBody">
    <cfoutput>
        <soapenv:Envelope
             xmlns:q0="urn:subscriberlookup.soap.mblox.com/1/0"
             xmlns:q1="urn:types.soap.mblox.com/1/1"
             xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
             xmlns:xsd="http://www.w3.org/2001/XMLSchema"
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         
             <soapenv:Body>
             
                 <q0:GetSubscriberDetailsArgs>
                    <q1:ClientDetails>
                         <q1:ClientUserName>#MBLOX_PartnerName_With_Carrier#</q1:ClientUserName>
                         <q1:ClientPassword>#MBLOX_PartnerPassword_With_Carrier#</q1:ClientPassword>
                         <q1:ProductId>600</q1:ProductId>
                    </q1:ClientDetails>                      
                    <q1:ClientRequestReference>123456</q1:ClientRequestReference>
                    <q0:TelephoneNumber>17148691767</q0:TelephoneNumber>
                 	<q0:LookupOperation>OperatorIDService</q0:LookupOperation>
                 </q0:GetSubscriberDetailsArgs>
                 
             </soapenv:Body>
             
         </soapenv:Envelope> 
    </cfoutput>
</cfsavecontent>


<!---
    Now that we have our SOAP body defined, we need to post it as
    a SOAP request to the Campaign Monitor website. Notice that
    when I POST the SOAP request, I am NOT required to append the
    "WSDL" flag to the target URL (this is only required when you
    actually want to get the web service definition).
--->
<cfhttp
    url="https://soap.mblox.com/operatoridservice/lookup"
    method="post"
    result="httpResponse"
    port="443">

    <!---
        Most SOAP action require some sort of SOAP Action header
        to be used.
    --->
    <cfhttpparam
        type="header"
        name="SOAPAction"
        value="https://soap.mblox.com/operatoridservice/lookup.GetSubscriberDetails"
        />

    <!---
        I typically use this header because CHTTP cannot handle
        GZIP encoding. This "no-compression" directive tells the
        server not to pass back GZIPed content.
    --->
    <cfhttpparam
        type="header"
        name="accept-encoding"
        value="no-compression"
        />

    <!---
        When posting the SOAP body, I use the CFHTTPParam type of
        XML. This does two things: it posts the XML as a the BODY
        and sets the mime-type to be XML.
        NOTE: Be sure to Trim() your XML since XML data cannot be
        parsed with leading whitespace.
    --->
    <cfhttpparam
        type="xml"
        value="#trim( soapBody )#"
        />

</cfhttp>


<cfdump var="#httpResponse#" />

<cfdump var="#httpResponse.fileContent#" />



<cfif find( "200", httpResponse.statusCode )>

    <!--- Parse the XML SOAP response. --->
    <cfset soapResponse = xmlParse( httpResponse.fileContent ) />


<cfdump var="#soapResponse#" />

    <!---
        Query for the response nodes using XPath. Because the
        SOAP XML document has name spaces, querying the document
        becomes a little funky. Rather than accessing the node
        name directly, we have to use its local-name().
    --->
    
    
    
    <cfset responseNodes = xmlSearch(
        soapResponse,
        "//*[ local-name() = 'OperatorId' ]"
        ) />


	
    

<cfdump var="#responseNodes#" />
<!---<cfdump var="#responseNodes[ 1 ].NetworkDetails.OperatorId#" />--->




    <!---
	
        Once we have the response node, we can use our typical
        ColdFusion struct-style XML node access.
    --->
    <cfoutput>
    
    <cfif ArrayLen(responseNodes) GT 0 >
    	<BR />
    	OperatorId = #responseNodes[ 1 ].xmlText#
    </cfif>
    
    
<!---
        Code: #responseNodes[ 1 ].OperatorId.xmlText#
        <br />
        Success: #responseNodes[ 1 ].Message.xmlText#--->

    </cfoutput>

</cfif>




<!---


<!---
    When the HTTP response comes back, our SOAP response will be
    in the FileContent atribute. SOAP always returns valid XML,
    even if there was an error (assuming the error was NOT in the
    communication, but rather in the data).
--->
<cfif find( "200", httpResponse.statusCode )>

    <!--- Parse the XML SOAP response. --->
    <cfset soapResponse = xmlParse( httpResponse.fileContent ) />

    <!---
        Query for the response nodes using XPath. Because the
        SOAP XML document has name spaces, querying the document
        becomes a little funky. Rather than accessing the node
        name directly, we have to use its local-name().
    --->
    <cfset responseNodes = xmlSearch(
        soapResponse,
        "//*[ local-name() = 'Subscriber.AddAndResubscribeResult' ]"
        ) />

    <!---
        Once we have the response node, we can use our typical
        ColdFusion struct-style XML node access.
    --->
    <cfoutput>

        Code: #responseNodes[ 1 ].Code.xmlText#
        <br />
        Success: #responseNodes[ 1 ].Message.xmlText#

    </cfoutput>

</cfif>


--->