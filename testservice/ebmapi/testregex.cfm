<cfsetting showdebugoutput="yes" >

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Test Reg Exp</title>
</head>

<body>

<cfoutput>

<!---	ReFind("^$", "") = #ReFind("^$", "")#
    <BR />
    ReFind("", "") = #ReFind("", "")#
    <BR />
    ReFind(" ", " ") = #ReFind(" ", " ")#--->

	<cfdump var="#CGI#" />

	<h3>
	^détente$|^end$|^fin$|^go away$|^opt out$|^please stop$|^quit$|^remove$|^spam$|^stop$|cancela la subscripcion|dame de baja|do not send|do not survey|do not text|don't send|dont send|don't survey|dont survey|don't text|dont text|elimina la subscripcion|encuestas no|leave me alone|no envies|no envies mas sms|no mandes mas mensajes|no mandes mas preguntas|no mas encuestas|no mas mensajes|no mas preguntes|no mas sms|no more messages|no more texts|no survey|para con los mensajes|para de enviar mensajes|quit texting|remove me|stop sending|stop text|stop texting|unsubscribe
    </h3>
    <BR />

	<cfset TestString = "stop" />
	
	<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "^détente$|^end$|^fin$|^go away$|^opt out$|^please stop$|^quit$|^remove$|^spam$|^stop$|cancela la subscripcion|dame de baja|do not send|do not survey|do not text|don't send|dont send|don't survey|dont survey|don't text|dont text|elimina la subscripcion|encuestas no|leave me alone|no envies|no envies mas sms|no mandes mas mensajes|no mandes mas preguntas|no mas encuestas|no mas mensajes|no mas preguntes|no mas sms|no more messages|no more texts|no survey|para con los mensajes|para de enviar mensajes|quit texting|remove me|stop sending|stop text|stop texting|unsubscribe" ) ) />
                        
	<cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", TestString ) ) />
                    
    TestString = #TestString#
    <BR />          
    UniSearchMatcher.Find() = #UniSearchMatcher.Find()#           
	
    <hr />
    
    
    <cfset TestString = "help" />
	
	<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "^détente$|^end$|^fin$|^go away$|^opt out$|^please stop$|^quit$|^remove$|^spam$|^stop$|cancela la subscripcion|dame de baja|do not send|do not survey|do not text|don't send|dont send|don't survey|dont survey|don't text|dont text|elimina la subscripcion|encuestas no|leave me alone|no envies|no envies mas sms|no mandes mas mensajes|no mandes mas preguntas|no mas encuestas|no mas mensajes|no mas preguntes|no mas sms|no more messages|no more texts|no survey|para con los mensajes|para de enviar mensajes|quit texting|remove me|stop sending|stop text|stop texting|unsubscribe" ) ) />
                        
	<cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", TestString ) ) />
                    
    TestString = #TestString#
    <BR />          
    UniSearchMatcher.Find() = #UniSearchMatcher.Find()#           
	
    <hr />


	<cfset TestString = "please stop" />
	
	<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "^détente$|^end$|^fin$|^go away$|^opt out$|^please stop$|^quit$|^remove$|^spam$|^stop$|cancela la subscripcion|dame de baja|do not send|do not survey|do not text|don't send|dont send|don't survey|dont survey|don't text|dont text|elimina la subscripcion|encuestas no|leave me alone|no envies|no envies mas sms|no mandes mas mensajes|no mandes mas preguntas|no mas encuestas|no mas mensajes|no mas preguntes|no mas sms|no more messages|no more texts|no survey|para con los mensajes|para de enviar mensajes|quit texting|remove me|stop sending|stop text|stop texting|unsubscribe" ) ) />
                        
	<cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", TestString ) ) />
                    
    TestString = #TestString#
    <BR />          
    UniSearchMatcher.Find() = #UniSearchMatcher.Find()#           
	
    <hr />
    
    <cfset TestString = "Please stop" />
	
	<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "(?i)^détente$|^end$|^fin$|^go away$|^opt out$|^please stop$|^quit$|^remove$|^spam$|^stop$|cancela la subscripcion|dame de baja|do not send|do not survey|do not text|don't send|dont send|don't survey|dont survey|don't text|dont text|elimina la subscripcion|encuestas no|leave me alone|no envies|no envies mas sms|no mandes mas mensajes|no mandes mas preguntas|no mas encuestas|no mas mensajes|no mas preguntes|no mas sms|no more messages|no more texts|no survey|para con los mensajes|para de enviar mensajes|quit texting|remove me|stop sending|stop text|stop texting|unsubscribe" ) ) />
                        
	<cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", TestString ) ) />
                    
    TestString = #TestString#
    <BR />          
    UniSearchMatcher.Find() = #UniSearchMatcher.Find()#           
	
    <hr />
    
    
    
    
	<cfset TestString = "please do not text me - Stop" />
	
	<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "^détente$|^end$|^fin$|^go away$|^opt out$|^please stop$|^quit$|^remove$|^spam$|^stop$|cancela la subscripcion|dame de baja|do not send|do not survey|do not text|don't send|dont send|don't survey|dont survey|don't text|dont text|elimina la subscripcion|encuestas no|leave me alone|no envies|no envies mas sms|no mandes mas mensajes|no mandes mas preguntas|no mas encuestas|no mas mensajes|no mas preguntes|no mas sms|no more messages|no more texts|no survey|para con los mensajes|para de enviar mensajes|quit texting|remove me|stop sending|stop text|stop texting|unsubscribe" ) ) />
                        
	<cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", TestString ) ) />
                    
    TestString = #TestString#
    <BR />          
    UniSearchMatcher.Find() = #UniSearchMatcher.Find()#           
	
    <hr />
    
    
   	<cfset TestString = "please don't text me - Stop" />
	
	<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "^détente$|^end$|^fin$|^go away$|^opt out$|^please stop$|^quit$|^remove$|^spam$|^stop$|cancela la subscripcion|dame de baja|do not send|do not survey|do not text|don't send|dont send|don't survey|dont survey|don't text|dont text|elimina la subscripcion|encuestas no|leave me alone|no envies|no envies mas sms|no mandes mas mensajes|no mandes mas preguntas|no mas encuestas|no mas mensajes|no mas preguntes|no mas sms|no more messages|no more texts|no survey|para con los mensajes|para de enviar mensajes|quit texting|remove me|stop sending|stop text|stop texting|unsubscribe" ) ) />
                        
	<cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", TestString ) ) />
          
    TestString = #TestString#
    <BR />          
    UniSearchMatcher.Find() = #UniSearchMatcher.Find()#           
	
    <hr />
    
    
    
   	<cfset TestString = "please stop text me - Stop" />
	
	<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "^détente$|^end$|^fin$|^go away$|^opt out$|^please stop$|^quit$|^remove$|^spam$|^stop$|cancela la subscripcion|dame de baja|do not send|do not survey|do not text|don't send|dont send|don't survey|dont survey|don't text|dont text|elimina la subscripcion|encuestas no|leave me alone|no envies|no envies mas sms|no mandes mas mensajes|no mandes mas preguntas|no mas encuestas|no mas mensajes|no mas preguntes|no mas sms|no more messages|no more texts|no survey|para con los mensajes|para de enviar mensajes|quit texting|remove me|stop sending|stop text|stop texting|unsubscribe" ) ) />
                        
	<cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", TestString ) ) />
          
    TestString = #TestString#
    <BR />          
    UniSearchMatcher.Find() = #UniSearchMatcher.Find()#           
	
    <hr />
    
    
   	<cfset TestString = "please stop send text me - Stop" />
	
	<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "^détente$|^end$|^fin$|^go away$|^opt out$|^please stop$|^quit$|^remove$|^spam$|^stop$|cancela la subscripcion|dame de baja|do not send|do not survey|do not text|don't send|dont send|don't survey|dont survey|don't text|dont text|elimina la subscripcion|encuestas no|leave me alone|no envies|no envies mas sms|no mandes mas mensajes|no mandes mas preguntas|no mas encuestas|no mas mensajes|no mas preguntes|no mas sms|no more messages|no more texts|no survey|para con los mensajes|para de enviar mensajes|quit texting|remove me|stop sending|stop text|stop texting|unsubscribe" ) ) />
                        
	<cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", TestString ) ) />
          
    TestString = #TestString#
    <BR />          
    UniSearchMatcher.Find() = #UniSearchMatcher.Find()#           
	
    <hr />


  <!---
					^détente$|^end$|^fin$|^go away$|^opt out$|^please stop$|^quit$|^remove$|^spam$|^stop$|cancela la subscripcion|dame de baja|do not send|do not survey|do not text|don't send|dont send|don't survey|dont survey|don't text|dont text|elimina la subscripcion|encuestas no|leave me alone|no envies|no envies mas sms|no mandes mas mensajes|no mandes mas preguntas|no mas encuestas|no mas mensajes|no mas preguntes|no mas sms|no more messages|no more texts|no survey|para con los mensajes|para de enviar mensajes|quit texting|remove me|stop sending|stop text|stop texting|unsubscribe
			
			
			
			  			<cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]" ) ) />
                        
						<!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
                        <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", RetValGetResponse.RESPONSE ) ) />
						
						<!--- Look for character higher than 255 ASCII --->   
                      	<cfif UniSearchMatcher.Find() >
                        	
                            <cfset SendAsUnicode = 1 />
                        
                        <cfelse>
                        
	                        <cfset SendAsUnicode = 0 />
                        
                        </cfif>
                        
									
			
				--->                        	
                          
                          
                          
</cfoutput>


</body>
</html>


<cfsetting showdebugoutput="yes" >
