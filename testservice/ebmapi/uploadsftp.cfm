<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Upload File - Send to SFTP - Then Bulk insert to mySQL DB</title>
</head>


 	<cfparam
        name="FORM.name"
        type="string"
        default=""
        />
 
    
    <cfparam
        name="FORM.inpFile"
        type="string"
        default=""
        />


 <cfparam
            name="FORM.submitted"
            type="numeric"
            default="0"
            />


<body>
 
    <cfoutput>
 
    
        <form
            action="#CGI.script_name#"
            method="post"
            enctype="multipart/form-data">
 
            <!--- Our form submission flag. --->
            <input type="hidden" name="submitted" value="1" />
              
            <label for="inpFile">
                inpFile:
 
                <input type="file" name="inpFile" id="inpFile" value="#FORM.inpFile#" />
                
            </label>
            <br />
 
            <input type="submit" value="Upload File" />
 
        </form>
 
  
    
    
        <BR />
        <HR />
        
        
        
        <cfif FORM.submitted GT 0>
        
        <!--- Write file to temp server directory --->
        <cffile
            action="UPLOAD"
            filefield="inpFile"
            destination="#GetTempDirectory()#"
            nameconflict="MAKEUNIQUE"
            result="UploadFileResult"
            />
        
        
        GetTempDirectory() = #GetTempDirectory()# 
        <BR/>
        
                
        
        <!--- push file to SFTP site --->
        <cfset SFTPHostName = "10.25.0.150">
		<cfset SFTPUserName = "ebmsshremote">
        <cfset SFTPPassword = "mbW1110!S"> 
        <cfset SFTPInDir = "/">
        <cfset SFTPOutDir = "/output">  

        
		<!--- Then execute bulk insert query --->
        
        
        
        <cftry>
        
			<!--- You need to connect with some other tool at least once to get the remote server's finger print and put it here in call to open--->
            <!--- from a mc/linux box you just go to command terminal and type: ssh SFTPHostName --->
            
            <cfftp action = "open"
              username = "#SFTPUserName#"
              connection = "CurrSFTPConn"
              password = "#SFTPPassword#"
              fingerprint = "06:84:59:cd:8e:77:b8:87:16:e3:d3:cb:90:e1:a0:f3"
              server = "#SFTPHostName#"
              secure = "yes"
              stoponerror="Yes">
          
            <cfdump var="#CurrSFTPConn#">
            	
                
                        
            File Name on Server (Name Conflict Make Unique) = #UploadFileResult.serverDirectory#/#UploadFileResult.serverFile# <BR />
                    
                    
          	<!---         You don't need to first open a connection if all you are going to do is upload or download a single file using the getfile or putfile actions. e.g.--->
            <cfftp action = "putFile"
                username = "#SFTPUserName#"
                password = "#SFTPPassword#"
                fingerprint = "06:84:59:cd:8e:77:b8:87:16:e3:d3:cb:90:e1:a0:f3"
                server = "#SFTPHostName#"
                transferMode = "ASCII" 
                localFile = "#UploadFileResult.serverDirectory#/#UploadFileResult.serverFile#" 
                remoteFile = "folder_1/#UploadFileResult.serverFile#"
				secure = "yes"
              	stoponerror="Yes">

                   
            
            <cfftp action="close" connection="CurrSFTPConn"/>
        
        
        <cfcatch type="any">
	        <cfdump var="#cfcatch#">
        	<cfftp action="close" connection="CurrSFTPConn"/>
        </cfcatch>
        
        </cftry>
        
        
        </cfif>



  </cfoutput>



    
 
</body>
</html>