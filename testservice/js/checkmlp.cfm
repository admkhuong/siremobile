<cfsetting enableCFoutputOnly="Yes"/>

<cfparam name="inpMLPURLID" default="">

<cfinvoke method="ReadMLPData" component="mlp-x.lz.cfc.mlp" returnvariable="RetVarReadMLPData">
	<cfinvokeargument name="MLPURL" value="#inpMLPURLID#">
	
</cfinvoke> 

<cfoutput>#SerializeJSON(RetVarReadMLPData)#</cfoutput>