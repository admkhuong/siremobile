<cfcomponent output="true">
	<cfinclude template="../../paths.cfm" >
	<cffunction name="init" access="public" output="true" returntype="any">
		
		<cfset variables.accessKey = APPLICATION.accessKeyId />
		<cfset variables.secretKey = APPLICATION.secretKey />
		
		<cfset variables.baseServiceUrl = "http://#siteDomain#/webservice/ebm/soap" />
		<cfset variables.cppUrl = "#variables.baseServiceUrl#/cpp.cfc?WSDL" />
		<cfset variables.contactUrl = "#variables.baseServiceUrl#/contact.cfc?WSDL" />
		<cfreturn this />
	</cffunction>
	
		
	
	<cffunction name="getCppList" returnformat="plain" access="remote" output="true">
		<!--- generate signature --->
		<cfset variables.sign = generateSignature("ListCpp") />
		<cfset datetime = variables.sign.DATE>
		<cfset signature = variables.sign.SIGNATURE>
		
		<cfxml variable="requestSoapXml">
			<ListCpp xmlns="http://www.eventbasedmessaging.com">
				<page>1</page>
				<AccessKeyId>#variables.accessKey#</AccessKeyId>
				<Timestamp>#datetime#</Timestamp>
				<Signature>#signature#</Signature>
			</ListCpp>
		</cfxml>
		<cfscript>
			ws = CreateObject("webservice", "#variables.cppUrl#");  
			addSOAPRequestHeader(ws, "ignoredNameSpace", "ignoredName", requestSoapXml);  
			ws.listCpp();
			headerResponse = GetSOAPResponseHeader(ws, "http://www.eventbasedmessaging.com", "ListCpp_response", true);  
			// convert xml to json
			
			successXml = xmlSearch(headerResponse,"//:Success");
			success = successXml[1].xmlText;
			
			messageXml = xmlSearch(headerResponse, "//:Message");
			message = messageXml[1].xmlText;
			
			retData = structNew();
			retData.Success = success;
			retData.Message = message;
			
			if(success == true){
				pageXml = xmlSearch(headerResponse, "//:Page");
				retData.Page = pageXml[1].xmlText;
				
				totalXml = xmlSearch(headerResponse, "//:Total");
				retData.Total = totalXml[1].xmlText;
				
				recordsXml = xmlSearch(headerResponse, "//:Records");
				retData.Records = recordsXml[1].xmlText;
				
				rowsXml = xmlSearch(headerResponse, "//:Rows/:Cpp");
				
				rows = arrayNew(1);
				if(arraylen(rowsXml) > 0){
					for(i=1; i <= arraylen(rowsXml); i++){
						cppXml = rowsXml[i];
						cpp = structNew();
						for(z=1; z <= arrayLen(cppXml.XmlChildren); z++){
							cppField = cppXml.XmlChildren[z];
							if(LCase(cppField.XmlName) == "cppuuid"){
								cpp.CppUUID = cppField.xmlText;
							}
							
							if(LCase(cppField.XmlName) == "description"){
								cpp.Description = cppField.xmlText;
							}
							
							if(LCase(cppField.XmlName) == "created"){
								cpp.Created = cppField.xmlText;
							}
						}
						rows[i] = cpp;
					}
				}
				retData.Rows = rows;	
			}
		</cfscript>
		
		<cfreturn retData>
	</cffunction>
	
	<cffunction name="createCPP" returnformat="json" access="remote" output="true">
		<cfargument name="inpDescription" required="true" type="string" default="" hint="cpp decription" />
		<cfset variables.sign = generateSignature("CreateCpp") />		
		<cfset datetime = variables.sign.DATE>
		<cfset signature = variables.sign.SIGNATURE>
		
		<cfxml variable="requestSoapXml">
			<CreateCpp xmlns="http://www.eventbasedmessaging.com">
				<AccessKeyId>#variables.accessKey#</AccessKeyId>
				<Timestamp>#datetime#</Timestamp>
				<Signature>#signature#</Signature>
				<Description>#arguments.inpDescription#</Description>
				<CustomField1_vch>#arguments.inpCustomField1_vch#</CustomField1_vch>
			</CreateCpp>
		</cfxml>
		
		<cfscript>
			ws = CreateObject("webservice", "#variables.cppUrl#");  
			addSOAPRequestHeader(ws, "ignoredNameSpace", "ignoredName", requestSoapXml);  
			ws.createCpp();
			headerResponse = GetSOAPResponseHeader(ws, "http://www.eventbasedmessaging.com", "CreateCpp_response", true);  
			writeDump(headerResponse);
			retData = structNew();
			successXml = xmlSearch(headerResponse,"//:Success");
			retData.Success = successXml[1].xmlText;
			
			messageXml = xmlSearch(headerResponse, "//:Message");
			retData.Message = messageXml[1].xmlText;
		</cfscript>
		
		<cfreturn retData>
	</cffunction>
	
	<cffunction name="addNewContact" returnformat="json" access="remote" output="true">
		
		<cfargument name="inpContactString" required="yes" hint="The actual contact string. Phone, email address, or SMS."/>       
        <cfargument name="inpContactType" required="yes" hint="The actual contact string type. 1=Phone, 2=email address, or 3=SMS."/>
        <cfargument name="cppUUID" required="yes" default="1507183569" hint="The UUID of the CPP this contact is being added to." />
        <cfargument name="CPPLOGINID" required="no" default="" hint="User Id used to Authenticate Changes if required. Used to link multiple contact strings and types to one account. Usually the end user's verified email address">
        <cfargument name="CPPPWD" required="no" default="" hint="Password used to Authenticate Changes if required. Used to link multiple contact strings and types to one account. Usually matched with the end user's verified email address">
        <cfargument name="inpFirstName" required="no" default="" hint="Will store first name if provided."/>
		<cfargument name="inpLastName" required="no" default="" hint="Will store last name if provided."/>		
        <cfargument name="inpBatchId" required="no" default="0" hint="If provided, system will also write a contact result entry for each request" />
        <cfargument name="INPUSERSPECIFIEDDATA" required="no" default="" hint="Up to 2048 characters of user specified data." />
        <cfargument name="inpCustomField1_vch" required="no"default=""  hint="Up to 2048 characters of additional user specified data." />
        <cfargument name="inpCustomField2_vch" required="no" default="" hint="Up to 2048 characters of additional user specified data." />
        <cfargument name="inpCustomField3_vch" required="no" default="" hint="Up to 2048 characters of additional user specified data." />
        <cfargument name="inpCustomField4_vch" required="no" default="" hint="Up to 2048 characters of additional user specified data." />
        <cfargument name="inpCustomField5_vch" required="no" default="" hint="Up to 2048 characters of additional user specified data." />
        <cfargument name="inpCustomField6_vch" required="no" default="" hint="Up to 2048 characters of additional user specified data." />
        <cfargument name="inpCustomField7_vch" required="no" default="" hint="Up to 2048 characters of additional user specified data." />
        <cfargument name="inpCustomField8_vch" required="no" default="" hint="Up to 2048 characters of additional user specified data." />
        <cfargument name="inpCustomField9_vch" required="no" default="" hint="Up to 2048 characters of additional user specified data." />
        <cfargument name="inpCustomField10_vch" required="no" default="" hint="Up to 2048 characters of additional user specified data." />
        <cfargument name="inpCustomField1_int" required="no" default="" hint="Floating point of up to 4 decimal places of user specified numeric data." />
        <cfargument name="inpCustomField2_int" required="no" default="" hint="Floating point of up to 4 decimal places of user specified numeric data." />
        <cfargument name="inpCustomField3_int" required="no" default="" hint="Floating point of up to 4 decimal places of user specified numeric data." />
        <cfargument name="inpCustomField4_int" required="no" default="" hint="Floating point of up to 4 decimal places of user specified numeric data." />
        <cfargument name="inpCustomField5_int" required="no" default="" hint="Floating point of up to 4 decimal places of user specified numeric data." />
        <cfargument name="inpCustomField6_int" required="no" default="" hint="Floating point of up to 4 decimal places of user specified numeric data." />
        <cfargument name="inpCustomField7_int" required="no" default="" hint="Floating point of up to 4 decimal places of user specified numeric data." />
        <cfargument name="inpCustomField8_int" required="no" default="" hint="Floating point of up to 4 decimal places of user specified numeric data." />
        <cfargument name="inpCustomField9_int" required="no" default="" hint="Floating point of up to 4 decimal places of user specified numeric data." />
        <cfargument name="inpCustomField10_int" required="no" default="" hint="Floating point of up to 4 decimal places of user specified numeric data." />
        <cfargument name="inpLocationKey1_vch" required="no" default="" hint="Up to 255 characters. Normally used to store the City of the contact string by convention - can be overridden with any custom value. If Location fields 1 and 2 are blank and the contact string is a phone number or SMS number this will be auto filled if found in lookup tables."/>
        <cfargument name="inpLocationKey2_vch" required="no" default="" hint="Up to 255 characters. Normally used to store the State of the contact string by convention - can be overridden with any custom value. If Location fields 1 and 2 are blank and the contact string is a phone number or SMS number this will be auto filled if found in lookup tables."/>
        <cfargument name="inpLocationKey3_vch" required="no" default="" hint="Up to 255 characters. Normally used to store the Zip Code of the contact string by convention - can be overridden with any custom value."/>
        <cfargument name="inpLocationKey4_vch" required="no" default="" hint="Up to 255 characters of location data." />
        <cfargument name="inpLocationKey5_vch" required="no" default="" hint="Up to 255 characters of location data." />
        <cfargument name="inpLocationKey6_vch" required="no" default="" hint="Up to 255 characters of location data." />
        <cfargument name="inpLocationKey7_vch" required="no" default="" hint="Up to 255 characters of location data." />
        <cfargument name="inpLocationKey8_vch" required="no" default="" hint="Up to 255 characters of location data." />
        <cfargument name="inpLocationKey9_vch" required="no" default="" hint="Up to 255 characters of location data." />
        <cfargument name="inpLocationKey10_vch" required="no" default="" hint="Up to 255 characters of location data." />
		
		<cfset variables.sign = generateSignature("AddNewContact") />		
		<cfset datetime = variables.sign.DATE>
		<cfset signature = variables.sign.SIGNATURE>
		
		<cfxml variable="requestSoapXml">
			<AddNewContact xmlns="http://www.eventbasedmessaging.com">
				<AccessKeyId>#variables.accessKey#</AccessKeyId>
				<Timestamp>#datetime#</Timestamp>
				<Signature>#signature#</Signature>
				
				<ContactString>#inpContactString#</ContactString>
				<ContactType>#inpContactType#</ContactType>
				<CppUUID>#cppUUID#</CppUUID>
				<CppLoginId>#CPPLOGINID#</CppLoginId>
				<CppPwd>#CPPPWD#</CppPwd>
				<FirstName>#inpFirstName#</FirstName>
				<LastName>#inpLastName#</LastName>
				<BatchId>#inpBatchId#</BatchId>
				<UserSpecifiedData>#INPUSERSPECIFIEDDATA#</UserSpecifiedData>
				
				<CustomField1_vch>#inpCustomField1_vch#</CustomField1_vch>
				<CustomField2_vch>#inpCustomField2_vch#</CustomField2_vch>
				<CustomField3_vch>#inpCustomField3_vch#</CustomField3_vch>
				<CustomField4_vch>#inpCustomField4_vch#</CustomField4_vch>
				<CustomField5_vch>#inpCustomField5_vch#</CustomField5_vch>
				<CustomField6_vch>#inpCustomField6_vch#</CustomField6_vch>
				<CustomField7_vch>#inpCustomField7_vch#</CustomField7_vch>
				<CustomField8_vch>#inpCustomField8_vch#</CustomField8_vch>
				<CustomField9_vch>#inpCustomField9_vch#</CustomField9_vch>
				<CustomField10_vch>#inpCustomField10_vch#</CustomField10_vch>
				
				<CustomField1_int>#inpCustomField1_int#</CustomField1_int>
				<CustomField2_int>#inpCustomField2_int#</CustomField2_int>
				<CustomField3_int>#inpCustomField3_int#</CustomField3_int>
				<CustomField4_int>#inpCustomField4_int#</CustomField4_int>
				<CustomField5_int>#inpCustomField5_int#</CustomField5_int>
				<CustomField6_int>#inpCustomField6_int#</CustomField6_int>
				<CustomField7_int>#inpCustomField7_int#</CustomField7_int>
				<CustomField8_int>#inpCustomField8_int#</CustomField8_int>
				<CustomField9_int>#inpCustomField9_int#</CustomField9_int>
				<CustomField10_int>#inpCustomField10_int#</CustomField10_int>
				
				<LocationKey1_vch>#inpLocationKey1_vch#</LocationKey1_vch>
				<LocationKey2_vch>#inpLocationKey2_vch#</LocationKey2_vch>
				<LocationKey3_vch>#inpLocationKey3_vch#</LocationKey3_vch>
				<LocationKey4_vch>#inpLocationKey4_vch#</LocationKey4_vch>
				<LocationKey5_vch>#inpLocationKey5_vch#</LocationKey5_vch>
				<LocationKey6_vch>#inpLocationKey6_vch#</LocationKey6_vch>
				<LocationKey7_vch>#inpLocationKey7_vch#</LocationKey7_vch>
				<LocationKey8_vch>#inpLocationKey8_vch#</LocationKey8_vch>
				<LocationKey9_vch>#inpLocationKey9_vch#</LocationKey9_vch>
				<LocationKey10_vch>#inpLocationKey10_vch#</LocationKey10_vch>
			</AddNewContact>
		</cfxml>
		
		
		<cfscript>
			writedump(requestSoapXml);
			ws = CreateObject("webservice", "#variables.contactUrl#");  
			addSOAPRequestHeader(ws, "ignoredNameSpace", "ignoredName", requestSoapXml);  
			ws.addNewContact();
			headerResponse = GetSOAPResponseHeader(ws, "http://www.eventbasedmessaging.com", "AddNewContact_response", true);  
			writeDump(headerResponse);
			retData = structNew();
			successXml = xmlSearch(headerResponse,"//:Success");
			retData.Success = successXml[1].xmlText;
			
			messageXml = xmlSearch(headerResponse, "//:Message");
			retData.Message = messageXml[1].xmlText;
		</cfscript>
		
		<cfreturn retData>
	</cffunction>
	
	<cffunction name="editContact" returnformat="json" access="remote" output="true">
		
		<cfargument name="inpContactString" required="no" default="9362244544" hint="The actual contact string. Phone, email address, or SMS."/>       
        <cfargument name="inpContactType" required="no" default="1" hint="The actual contact string type. 1=Phone, 2=email address, or 3=SMS."/>
        <cfargument name="cppUUID" required="no" default="7711479065" hint="The UUID of the CPP this contact is being added to." />
        <cfargument name="CPPLOGINID" required="no" default="" hint="User Id used to Authenticate Changes if required. Used to link multiple contact strings and types to one account. Usually the end user's verified email address">
        <cfargument name="CPPPWD" required="no" default="" hint="Password used to Authenticate Changes if required. Used to link multiple contact strings and types to one account. Usually matched with the end user's verified email address">
        <cfargument name="inpFirstName" required="no" default="Manh" hint="Will store first name if provided."/>
		<cfargument name="inpLastName" required="no" default="" hint="Will store last name if provided."/>		
        <cfargument name="inpBatchId" required="no" default="0" hint="If provided, system will also write a contact result entry for each request" />
        <cfargument name="INPUSERSPECIFIEDDATA" required="no" default="" hint="Up to 2048 characters of user specified data." />
        <cfargument name="inpCustomField1_vch" required="no"default="vvv"  hint="Up to 2048 characters of additional user specified data." />
        <cfargument name="inpCustomField2_vch" required="no" default="" hint="Up to 2048 characters of additional user specified data." />
        <cfargument name="inpCustomField3_vch" required="no" default="" hint="Up to 2048 characters of additional user specified data." />
        <cfargument name="inpCustomField4_vch" required="no" default="" hint="Up to 2048 characters of additional user specified data." />
        <cfargument name="inpCustomField5_vch" required="no" default="" hint="Up to 2048 characters of additional user specified data." />
        <cfargument name="inpCustomField6_vch" required="no" default="" hint="Up to 2048 characters of additional user specified data." />
        <cfargument name="inpCustomField7_vch" required="no" default="" hint="Up to 2048 characters of additional user specified data." />
        <cfargument name="inpCustomField8_vch" required="no" default="" hint="Up to 2048 characters of additional user specified data." />
        <cfargument name="inpCustomField9_vch" required="no" default="" hint="Up to 2048 characters of additional user specified data." />
        <cfargument name="inpCustomField10_vch" required="no" default="" hint="Up to 2048 characters of additional user specified data." />
        <cfargument name="inpCustomField1_int" required="no" default="" hint="Floating point of up to 4 decimal places of user specified numeric data." />
        <cfargument name="inpCustomField2_int" required="no" default="" hint="Floating point of up to 4 decimal places of user specified numeric data." />
        <cfargument name="inpCustomField3_int" required="no" default="" hint="Floating point of up to 4 decimal places of user specified numeric data." />
        <cfargument name="inpCustomField4_int" required="no" default="" hint="Floating point of up to 4 decimal places of user specified numeric data." />
        <cfargument name="inpCustomField5_int" required="no" default="" hint="Floating point of up to 4 decimal places of user specified numeric data." />
        <cfargument name="inpCustomField6_int" required="no" default="" hint="Floating point of up to 4 decimal places of user specified numeric data." />
        <cfargument name="inpCustomField7_int" required="no" default="" hint="Floating point of up to 4 decimal places of user specified numeric data." />
        <cfargument name="inpCustomField8_int" required="no" default="" hint="Floating point of up to 4 decimal places of user specified numeric data." />
        <cfargument name="inpCustomField9_int" required="no" default="" hint="Floating point of up to 4 decimal places of user specified numeric data." />
        <cfargument name="inpCustomField10_int" required="no" default="" hint="Floating point of up to 4 decimal places of user specified numeric data." />
        <cfargument name="inpLocationKey1_vch" required="no" default="" hint="Up to 255 characters. Normally used to store the City of the contact string by convention - can be overridden with any custom value. If Location fields 1 and 2 are blank and the contact string is a phone number or SMS number this will be auto filled if found in lookup tables."/>
        <cfargument name="inpLocationKey2_vch" required="no" default="" hint="Up to 255 characters. Normally used to store the State of the contact string by convention - can be overridden with any custom value. If Location fields 1 and 2 are blank and the contact string is a phone number or SMS number this will be auto filled if found in lookup tables."/>
        <cfargument name="inpLocationKey3_vch" required="no" default="" hint="Up to 255 characters. Normally used to store the Zip Code of the contact string by convention - can be overridden with any custom value."/>
        <cfargument name="inpLocationKey4_vch" required="no" default="" hint="Up to 255 characters of location data." />
        <cfargument name="inpLocationKey5_vch" required="no" default="" hint="Up to 255 characters of location data." />
        <cfargument name="inpLocationKey6_vch" required="no" default="" hint="Up to 255 characters of location data." />
        <cfargument name="inpLocationKey7_vch" required="no" default="" hint="Up to 255 characters of location data." />
        <cfargument name="inpLocationKey8_vch" required="no" default="" hint="Up to 255 characters of location data." />
        <cfargument name="inpLocationKey9_vch" required="no" default="" hint="Up to 255 characters of location data." />
        <cfargument name="inpLocationKey10_vch" required="no" default="" hint="Up to 255 characters of location data." />
		
		<cfset variables.sign = generateSignature("EditContact") />		
		<cfset datetime = variables.sign.DATE>
		<cfset signature = variables.sign.SIGNATURE>
		
		<cfxml variable="requestSoapXml">
			<EditContact xmlns="http://www.eventbasedmessaging.com">
				<AccessKeyId>#variables.accessKey#</AccessKeyId>
				<Timestamp>#datetime#</Timestamp>
				<Signature>#signature#</Signature>
				
				<ContactString>#inpContactString#</ContactString>
				<ContactType>#inpContactType#</ContactType>
				<CppUUID>#cppUUID#</CppUUID>
				<CppLoginId>#CPPLOGINID#</CppLoginId>
				<CppPwd>#CPPPWD#</CppPwd>
				<FirstName>#inpFirstName#</FirstName>
				<LastName>#inpLastName#</LastName>
				<BatchId>#inpBatchId#</BatchId>
				<UserSpecifiedData>#INPUSERSPECIFIEDDATA#</UserSpecifiedData>
				
				<CustomField1_vch>#inpCustomField1_vch#</CustomField1_vch>
				<CustomField2_vch>#inpCustomField2_vch#</CustomField2_vch>
				<CustomField3_vch>#inpCustomField3_vch#</CustomField3_vch>
				<CustomField4_vch>#inpCustomField4_vch#</CustomField4_vch>
				<CustomField5_vch>#inpCustomField5_vch#</CustomField5_vch>
				<CustomField6_vch>#inpCustomField6_vch#</CustomField6_vch>
				<CustomField7_vch>#inpCustomField7_vch#</CustomField7_vch>
				<CustomField8_vch>#inpCustomField8_vch#</CustomField8_vch>
				<CustomField9_vch>#inpCustomField9_vch#</CustomField9_vch>
				<CustomField10_vch>#inpCustomField10_vch#</CustomField10_vch>
				
				<CustomField1_int>#inpCustomField1_int#</CustomField1_int>
				<CustomField2_int>#inpCustomField2_int#</CustomField2_int>
				<CustomField3_int>#inpCustomField3_int#</CustomField3_int>
				<CustomField4_int>#inpCustomField4_int#</CustomField4_int>
				<CustomField5_int>#inpCustomField5_int#</CustomField5_int>
				<CustomField6_int>#inpCustomField6_int#</CustomField6_int>
				<CustomField7_int>#inpCustomField7_int#</CustomField7_int>
				<CustomField8_int>#inpCustomField8_int#</CustomField8_int>
				<CustomField9_int>#inpCustomField9_int#</CustomField9_int>
				<CustomField10_int>#inpCustomField10_int#</CustomField10_int>
				
				<LocationKey1_vch>#inpLocationKey1_vch#</LocationKey1_vch>
				<LocationKey2_vch>#inpLocationKey2_vch#</LocationKey2_vch>
				<LocationKey3_vch>#inpLocationKey3_vch#</LocationKey3_vch>
				<LocationKey4_vch>#inpLocationKey4_vch#</LocationKey4_vch>
				<LocationKey5_vch>#inpLocationKey5_vch#</LocationKey5_vch>
				<LocationKey6_vch>#inpLocationKey6_vch#</LocationKey6_vch>
				<LocationKey7_vch>#inpLocationKey7_vch#</LocationKey7_vch>
				<LocationKey8_vch>#inpLocationKey8_vch#</LocationKey8_vch>
				<LocationKey9_vch>#inpLocationKey9_vch#</LocationKey9_vch>
				<LocationKey10_vch>#inpLocationKey10_vch#</LocationKey10_vch>
			</EditContact>
		</cfxml>
		
		
		<cfscript>
			writedump(requestSoapXml);
			ws = CreateObject("webservice", "#variables.contactUrl#");  
			addSOAPRequestHeader(ws, "ignoredNameSpace", "ignoredName", requestSoapXml);  
			ws.editContact();
			headerResponse = GetSOAPResponseHeader(ws, "http://www.eventbasedmessaging.com", "EditContact_response", true);  
			writeDump(headerResponse);
			retData = structNew();
			successXml = xmlSearch(headerResponse,"//:Success");
			retData.Success = successXml[1].xmlText;
			
			messageXml = xmlSearch(headerResponse, "//:Message");
			retData.Message = messageXml[1].xmlText;
		</cfscript>
		
		<cfreturn retData>
	</cffunction>
	
	<cffunction name="verifyContact" returnformat="json" access="remote" output="true">
		
		<cfargument name="inpContactString" required="yes" hint="The actual contact string. Phone, email address, or SMS."/>       
        <cfargument name="inpContactType" required="yes" hint="The actual contact string type. 1=Phone, 2=email address, or 3=SMS."/>
        <cfargument name="cppUUID" required="yes" default="1507183569" hint="The UUID of the CPP this contact is being added to." />
        <cfargument name="cppId" required="no" default="" hint="User Id used to Authenticate Changes if required. Used to link multiple contact strings and types to one account. Usually the end user's verified email address">
        
		
		<cfset variables.sign = generateSignature("VerifyContact") />		
		<cfset datetime = variables.sign.DATE>
		<cfset signature = variables.sign.SIGNATURE>
		
		<cfxml variable="requestSoapXml">
			<VerifyContact xmlns="http://www.eventbasedmessaging.com">
				<AccessKeyId>#variables.accessKey#</AccessKeyId>
				<Timestamp>#datetime#</Timestamp>
				<Signature>#signature#</Signature>
				
				<ContactString>#inpContactString#</ContactString>
				<ContactType>#inpContactType#</ContactType>
				<CppUUID>#cppUUID#</CppUUID>
				<CppId>#cppId#</CppId>

			</VerifyContact>
		</cfxml>
		
		
		<cfscript>
			writedump(requestSoapXml);
			ws = CreateObject("webservice", "#variables.contactUrl#");  
			addSOAPRequestHeader(ws, "ignoredNameSpace", "ignoredName", requestSoapXml);  
			ws.verifyContact();
			headerResponse = GetSOAPResponseHeader(ws, "http://www.eventbasedmessaging.com", "VerifyContact_response", true);  
			writeDump(headerResponse);
			retData = structNew();
			successXml = xmlSearch(headerResponse,"//:Success");
			retData.Success = successXml[1].xmlText;
			
			messageXml = xmlSearch(headerResponse, "//:Message");
			retData.Message = messageXml[1].xmlText;
		</cfscript>
		
		<cfreturn retData>
	</cffunction>
	
	<cffunction name="deleteContact" returnformat="json" access="remote" output="true">
		
		<cfargument name="inpContactString" required="yes" hint="The actual contact string. Phone, email address, or SMS."/>       
        <cfargument name="inpContactType" required="yes" hint="The actual contact string type. 1=Phone, 2=email address, or 3=SMS."/>
        <cfargument name="cppUUID" required="yes" default="1507183569" hint="The UUID of the CPP this contact is being added to." />
        <cfargument name="cppId" required="no" default="" hint="User Id used to Authenticate Changes if required. Used to link multiple contact strings and types to one account. Usually the end user's verified email address">
        
		
		<cfset variables.sign = generateSignature("DeleteContact") />		
		<cfset datetime = variables.sign.DATE>
		<cfset signature = variables.sign.SIGNATURE>
		
		<cfxml variable="requestSoapXml">
			<DeleteContact xmlns="http://www.eventbasedmessaging.com">
				<AccessKeyId>#variables.accessKey#</AccessKeyId>
				<Timestamp>#datetime#</Timestamp>
				<Signature>#signature#</Signature>
				
				<ContactString>#inpContactString#</ContactString>
				<ContactType>#inpContactType#</ContactType>
				<CppUUID>#cppUUID#</CppUUID>
				<CppId>#cppId#</CppId>

			</DeleteContact>
		</cfxml>
		
		
		<cfscript>
			writedump(requestSoapXml);
			ws = CreateObject("webservice", "#variables.contactUrl#");  
			addSOAPRequestHeader(ws, "ignoredNameSpace", "ignoredName", requestSoapXml);  
			ws.deleteContact();
			headerResponse = GetSOAPResponseHeader(ws, "http://www.eventbasedmessaging.com", "DeleteContact_response", true);  
			writeDump(headerResponse);
			retData = structNew();
			successXml = xmlSearch(headerResponse,"//:Success");
			retData.Success = successXml[1].xmlText;
			
			messageXml = xmlSearch(headerResponse, "//:Message");
			retData.Message = messageXml[1].xmlText;
		</cfscript>
		
		<cfreturn retData>
	</cffunction>
	
	<cffunction name="listContact" returnformat="json" access="remote" output="true">
		
		<cfargument name="inpContactString" required="false" hint="The actual contact string. Phone, email address, or SMS."/>       
        <cfargument name="inpContactType" required="false" hint="The actual contact string type. 1=Phone, 2=email address, or 3=SMS."/>
        <cfargument name="cppUUID" required="yes" default="1507183569" hint="The UUID of the CPP this contact is being added to." />
        <cfargument name="cppId" required="no" default="" hint="User Id used to Authenticate Changes if required. Used to link multiple contact strings and types to one account. Usually the end user's verified email address">
        
		
		<cfset variables.sign = generateSignature("ListContact") />		
		<cfset datetime = variables.sign.DATE>
		<cfset signature = variables.sign.SIGNATURE>
		
		<cfxml variable="requestSoapXml">
			<ListContact xmlns="http://www.eventbasedmessaging.com">
				<AccessKeyId>#variables.accessKey#</AccessKeyId>
				<Timestamp>#datetime#</Timestamp>
				<Signature>#signature#</Signature>
				
				<CppUUID>#cppUUID#</CppUUID>
			</ListContact>
		</cfxml>
		
		
		<cfscript>
			ws = CreateObject("webservice", "#variables.contactUrl#");  
			addSOAPRequestHeader(ws, "ignoredNameSpace", "ignoredName", requestSoapXml);  
			ws.listContact();
			headerResponse = GetSOAPResponseHeader(ws, "http://www.eventbasedmessaging.com", "ListContact_response", true);  
			writeDump(headerResponse);
			retData = structNew();
			successXml = xmlSearch(headerResponse,"//:Success");
			retData.Success = successXml[1].xmlText;
			
			messageXml = xmlSearch(headerResponse, "//:Message");
			retData.Message = messageXml[1].xmlText;
		</cfscript>
		
		<cfreturn retData>
	</cffunction>
	
	<cffunction name="generateSignature" returnformat="json" access="remote" output="true">
		<cfargument name="operation" required="true" type="string" default="" hint="Operation" />
		
		<cfset dateTimeString = GetHTTPTimeString(Now())>
		<!--- Create a canonical string to send --->
		<cfset cs = "#arguments.operation#\n\n\n#dateTimeString#\n\n\n">
		<cfset signature = createSignature(cs,variables.secretKey)>
		
		<cfset retval.SIGNATURE = signature>
		<cfset retval.DATE = dateTimeString>
		
		<cfreturn retval>
	</cffunction>
	
	<cffunction name="HMAC_SHA1" returntype="binary" access="private" output="true" hint="NSA SHA-1 Algorithm">
	   <cfargument name="signKey" type="string" required="true" />
	   <cfargument name="signMessage" type="string" required="true" />
	
	   <cfset var jMsg = JavaCast("string",arguments.signMessage).getBytes("iso-8859-1") />
	   <cfset var jKey = JavaCast("string",arguments.signKey).getBytes("iso-8859-1") />
	   <cfset var key = createObject("java","javax.crypto.spec.SecretKeySpec") />
	   <cfset var mac = createObject("java","javax.crypto.Mac") />
	
	   <cfset key = key.init(jKey,"HmacSHA1") />
	   <cfset mac = mac.getInstance(key.getAlgorithm()) />
	   <cfset mac.init(key) />
	   <cfset mac.update(jMsg) />
	
	   <cfreturn mac.doFinal() />
	</cffunction>
	
	<cffunction name="createSignature" returntype="string" access="public" output="true">
	   <cfargument name="stringIn" type="string" required="true" />
		<cfargument name="scretKey" type="string" required="true" />
		<!--- Replace "\n" with "chr(10) to get a correct digest --->
		<cfset var fixedData = replace(arguments.stringIn,"\n","#chr(10)#","all")>
		<!--- Calculate the hash of the information --->
		<cfset var digest = HMAC_SHA1(scretKey,fixedData)>
		<!--- fix the returned data to be a proper signature --->
		<cfset var signature = ToBase64("#digest#")>
		
		<cfreturn signature>
	</cffunction>
</cfcomponent>