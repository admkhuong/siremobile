<cfscript>
	if(isDefined("form.submit")){
		objServiceObject = createObject('component', 'testservice.soap.cfc.ebmService').init(); 
		result = objServiceObject.addNewContact(
			inpContactString = form.inpContactString,
			inpContactType = form.inpContactType,
			inpFirstName = form.inpFirstName,
			inpLastName	= form.inpLastName,
			INPUSERSPECIFIEDDATA = form.INPUSERSPECIFIEDDATA,
			inpCustomField1_vch = form.inpCustomField1_vch,
			cppUUID = form.cppUUID
			);
			
		writedump(result);	
	}
</cfscript>

<cfform id="cx_rxmultilist_form" name="cx_rxmultilist_form" method="post">
	<table cellpadding="3" cellspacing="6" width="100%" class="cx_rxmultilist">
		<tr>
			<th width="20%" class="cx_rxmultilist_rtxt">UserId</th>
			<td width="30%" class="cx_rxmultilist_val">135
				<input type="hidden" id="UserId_int" name="UserId_int" value="135"/>
			</td>
			<th width="20%" class="cx_rxmultilist_rtxt">ContactString</th>
			<td width="30%" class="cx_rxmultilist_val">
				<input type="text" id="inpContactString" name="inpContactString" value=""/>
			</td>
		</tr>
		
		<tr>
			<th width="20%" class="cx_rxmultilist_rtxt">First name</th>
			<td width="30%" class="cx_rxmultilist_val"><input type="text" id="inpFirstName" name="inpFirstName" value=""/>
			</td>
			<th width="20%" class="cx_rxmultilist_rtxt">LastName</th>
			<td width="30%" class="cx_rxmultilist_val">
				<input type="text" id="inpLastName" name="inpLastName" value=""/>
			</td>
		</tr>
		
		<tr>
			<th class="cx_rxmultilist_rtxt">ContactTypeId</th>
			<td class="cx_rxmultilist_val">
				<input type="text" id="inpContactType" name="inpContactType" value=""/>
			</td>
			<td class="cx_rxmultilist_rtxt">CustomField1</td>
			<td><input type="text" id="CustomField1_int" name="inpCustomField1_int" class="validate[funcCall[checkPosInt]] ui-corner-all" size="10" value=""/></td>
		</tr>
		<tr>
			<td class="cx_rxmultilist_rtxt">CustomField2</td>
			<td><input type="text" id="CustomField2_int" name="inpCustomField2_int" class="validate[funcCall[checkPosInt]] ui-corner-all" size="10" value=""/></td>
			<td class="cx_rxmultilist_rtxt">CustomField3</td>
			<td><input type="text" id="CustomField3_int" name="inpCustomField3_int" class="validate[funcCall[checkPosInt]] ui-corner-all" size="10" value=""/></td>
		</tr>
		<tr>
			<td class="cx_rxmultilist_rtxt">CustomField4</td>
			<td><input type="text" id="CustomField4_int" name="inpCustomField4_int" class="validate[funcCall[checkPosInt]] ui-corner-all" size="10" value=""/></td>
			<td class="cx_rxmultilist_rtxt">CustomField5</td>
			<td><input type="text" id="CustomField5_int" name="inpCustomField5_int" class="validate[funcCall[checkPosInt]] ui-corner-all" size="10" value=""/></td>
		</tr>
		<tr>
			<td class="cx_rxmultilist_rtxt">CustomField6</td>
			<td><input type="text" id="CustomField6_int" name="inpCustomField6_int" class="validate[funcCall[checkPosInt]] ui-corner-all" size="10" value=""/></td>
			<td class="cx_rxmultilist_rtxt">CustomField7</td>
			<td><input type="text" id="CustomField7_int" name="inpCustomField7_int" class="validate[funcCall[checkPosInt]] ui-corner-all" size="10" value=""/></td>
		</tr>
		<tr>
			<td class="cx_rxmultilist_rtxt">CustomField8</td>
			<td><input type="text" id="CustomField8_int" name="inpCustomField8_int" class="validate[funcCall[checkPosInt]] ui-corner-all" size="10" value=""/></td>
			<td class="cx_rxmultilist_rtxt">CustomField9</td>
			<td><input type="text" id="CustomField9_int" name="inpCustomField9_int" class="validate[funcCall[checkPosInt]] ui-corner-all" size="10" value=""/></td>
		</tr>
		<tr>
			<td class="cx_rxmultilist_rtxt">CustomField10</td>
			<td><input type="text" id="CustomField10_int" name="inpCustomField10_int" class="validate[funcCall[checkPosInt]] ui-corner-all" size="10" value=""/></td>
			<td class="cx_rxmultilist_rtxt">UniqueCustomer_UUID</td>
			<td><input type="text" id="UniqueCustomer_UUID_vch" name="INPUSERSPECIFIEDDATA" class="validate[maxSize[36]] ui-corner-all" size="30" maxlength="36" value=""/></td>
		</tr>
		<tr>
			<td class="cx_rxmultilist_rtxt">LocationKey1</td>
			<td><input type="text" id="LocationKey1_vch" name="inpLocationKey1_vch" class="validate[maxSize[255]] ui-corner-all" size="30" maxlength="255" value=""/></td>
			<td class="cx_rxmultilist_rtxt">LocationKey2</td>
			<td><input type="text" id="LocationKey2_vch" name="inpLocationKey2_vch" class="validate[maxSize[255]] ui-corner-all" size="30" maxlength="255" value=""/></td>
		</tr>
		<tr>
			<td class="cx_rxmultilist_rtxt">LocationKey3</td>
			<td><input type="text" id="LocationKey3_vch" name="inpLocationKey3_vch" class="validate[maxSize[255]] ui-corner-all" size="30" maxlength="255" value=""/></td>
			<td class="cx_rxmultilist_rtxt">LocationKey4</td>
			<td><input type="text" id="LocationKey4_vch" name="inpLocationKey4_vch" class="validate[maxSize[255]] ui-corner-all" size="30" maxlength="255" value=""/></td>
		</tr>
		<tr>
			<td class="cx_rxmultilist_rtxt">LocationKey5</td>
			<td><input type="text" id="LocationKey5_vch" name="inpLocationKey5_vch" class="validate[maxSize[255]] ui-corner-all" size="30" maxlength="255" value=""/></td>
			<td class="cx_rxmultilist_rtxt">LocationKey6</td>
			<td><input type="text" id="LocationKey6_vch" name="inpLocationKey6_vch" class="validate[maxSize[255]] ui-corner-all" size="30" maxlength="255" value=""/></td>
		</tr>
		<tr>
			<td class="cx_rxmultilist_rtxt">LocationKey7</td>
			<td><input type="text" id="LocationKey7_vch" name="inpLocationKey7_vch" class="validate[maxSize[255]] ui-corner-all" size="30" maxlength="255" value=""/></td>
			<td class="cx_rxmultilist_rtxt">LocationKey8</td>
			<td><input type="text" id="LocationKey8_vch" name="inpLocationKey8_vch" class="validate[maxSize[255]] ui-corner-all" size="30" maxlength="255" value=""/></td>
		</tr>
		<tr>
			<td class="cx_rxmultilist_rtxt">LocationKey9</td>
			<td><input type="text" id="LocationKey9_vch" name="inpLocationKey9_vch" class="validate[maxSize[255]] ui-corner-all" size="30" maxlength="255" value=""/></td>
			<td class="cx_rxmultilist_rtxt">LocationKey10</td>
			<td><input type="text" id="LocationKey10_vch" name="inpLocationKey10_vch" class="validate[maxSize[255]] ui-corner-all" size="30" maxlength="255" value=""/></td>
		</tr>
		<tr>
			<td class="cx_rxmultilist_rtxt">GroupList</td>
			<td><input type="text" id="GroupList_vch" name="GroupList_vch" class="validate[maxSize[512]] ui-corner-all" size="30" maxlength="512" value=""/></td>
			<td class="cx_rxmultilist_rtxt">CustomField1</td>
			<td><input type="text" id="CustomField1_vch" name="inpCustomField1_vch" class="validate[maxSize[2048]] ui-corner-all" size="30" maxlength="2048" value=""/></td>
		</tr>
		<tr>
			<td class="cx_rxmultilist_rtxt">CustomField2</td>
			<td><input type="text" id="CustomField2_vch" name="inpCustomField2_vch" class="validate[maxSize[2048]] ui-corner-all" size="30" maxlength="2048" value=""/></td>
			<td class="cx_rxmultilist_rtxt">CustomField3</td>
			<td><input type="text" id="CustomField3_vch" name="inpCustomField3_vch" class="validate[maxSize[2048]] ui-corner-all" size="30" maxlength="2048" value=""/></td>
		</tr>
		<tr>
			<td class="cx_rxmultilist_rtxt">CustomField4</td>
			<td><input type="text" id="CustomField4_vch" name="inpCustomField4_vch" class="validate[maxSize[2048]] ui-corner-all" size="30" maxlength="2048" value=""/></td>
			<td class="cx_rxmultilist_rtxt">CustomField5</td>
			<td><input type="text" id="CustomField5_vch" name="inpCustomField5_vch" class="validate[maxSize[2048]] ui-corner-all" size="30" maxlength="2048" value=""/></td>
		</tr>
		<tr>
			<td class="cx_rxmultilist_rtxt">CustomField6</td>
			<td><input type="text" id="CustomField6_vch" name="inpCustomField6_vch" class="validate[maxSize[2048]] ui-corner-all" size="30" maxlength="2048" value=""/></td>
			<td class="cx_rxmultilist_rtxt">CustomField7</td>
			<td><input type="text" id="CustomField7_vch" name="inpCustomField7_vch" class="validate[maxSize[2048]] ui-corner-all" size="30" maxlength="2048" value=""/></td>
		</tr>
		<tr>
			<td class="cx_rxmultilist_rtxt">CustomField8</td>
			<td><input type="text" id="CustomField8_vch" name="inpCustomField8_vch" class="validate[maxSize[2048]] ui-corner-all" size="30" maxlength="2048" value=""/></td>
			<td class="cx_rxmultilist_rtxt">CustomField9</td>
			<td><input type="text" id="CustomField9_vch" name="inpCustomField9_vch" class="validate[maxSize[2048]] ui-corner-all" size="30" maxlength="2048" value=""/></td>
		</tr>
		<tr>
			<td class="cx_rxmultilist_rtxt">CustomField10</td>
			<td><input type="text" id="CustomField10_vch" name="inpCustomField10_vch" class="validate[maxSize[2048]] ui-corner-all" size="30" maxlength="2048" value=""/></td>
			<td class="cx_rxmultilist_rtxt">UserSpecifiedData</td>
			<td><input type="text" id="UserSpecifiedData_vch" name="UserSpecifiedData_vch" class="validate[maxSize[2048]] ui-corner-all" size="30" maxlength="2048" value=""/></td>
		</tr>
		<tr>
			<td class="cx_rxmultilist_rtxt">SourceString</td>
			<td><input type="text" id="SourceString_vch" name="SourceString_vch" class="ui-corner-all" size="30" value=""/></td>
			<td class="cx_rxmultilist_rtxt">Cpp uuid</td>
			<td><input type="text" id="cppUUID" name="cppUUID" class="ui-corner-all" size="30" value=""/></td>
		</tr>
	</table>
	<p class="cx_rxmultilist_btn">
		
		<button id="UpdateContact" name="submit" TYPE="Submit" class="CancelRXT">Update</button>
	</p>
</cfform>