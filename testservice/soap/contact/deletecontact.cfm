<cfscript>
	if(isDefined("form.submit")){
		objServiceObject = createObject('component', 'testservice.soap.cfc.ebmService').init(); 
		result = objServiceObject.deleteContact(
			inpContactString = form.inpContactString,
			inpContactType = form.inpContactType,
			cppId = form.cppId
			);
			
		writedump(result);	
	}
</cfscript>

<cfform id="cx_rxmultilist_form" name="cx_rxmultilist_form" method="post">
	<table cellpadding="3" cellspacing="6" width="100%" class="cx_rxmultilist">
		<tr>
			<th width="20%" class="cx_rxmultilist_rtxt">UserId</th>
			<td width="30%" class="cx_rxmultilist_val">135
				<input type="hidden" id="UserId_int" name="UserId_int" value="135"/>
			</td>
			<th width="20%" class="cx_rxmultilist_rtxt">ContactString</th>
			<td width="30%" class="cx_rxmultilist_val">
				<input type="text" id="inpContactString" name="inpContactString" value=""/>
			</td>
		</tr>
		
		<tr>
			<th class="cx_rxmultilist_rtxt">ContactTypeId</th>
			<td class="cx_rxmultilist_val">
				<input type="text" id="inpContactType" name="inpContactType" value=""/>
			</td>
			<th class="cx_rxmultilist_rtxt">Cpp id</th>
			<td><input type="text" id="cppId" name="cppId" class="validate[funcCall[checkPosInt]] ui-corner-all" size="10" value=""/></td>
		</tr>
		
	</table>
	<p class="cx_rxmultilist_btn">
		
		<button id="UpdateContact" name="submit" TYPE="Submit" class="CancelRXT">Delete</button>
	</p>
</cfform>