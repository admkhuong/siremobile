<cfinclude template="../../paths.cfm" >
<cfscript>
	 objServiceObject = createObject('component', 'testservice.soap.cfc.ebmService').init(); 
     data =  objServiceObject.getCppList();
</cfscript>
<html>
	<head>
		<title>List CPP</title>
		<cfoutput>
		<style>
			@import url('#rootUrl#/css/cf_table.css');
			@import url('#rootUrl#/css/iconslist.css');
		</style>
		</cfoutput>	
	</head>
	<body>
		<cfif data.SUCCESS EQ false>
			<cfoutput>
				#data.MESSAGE#
			</cfoutput>
		<cfelse>
		
		
		
		<cfoutput>
			<table class="cf_table">
				<tr>
					<th> CPP UUID</th>
					<th> Desc</th>
					<th>Create</th>
					<th>Option</th>
				</tr>
			<cfloop array="#data.ROWS#" index="index">
				<tr>
						<td>#index.CPPUUID#</td>
						<td>#index.DESCRIPTION#</td>
						<td>#index.CREATED#</td>
						<td>
							<a href="#rootUrl#/soap/cpp/editCpp.cfm?CPPUUID=#index.CPPUUID#">
								<img class="ListIconLinks img18_18 edit_18_18" src="#rootUrl#/css/images/blank.gif" title="Edit CPP">
							</a>
							<a href="#rootUrl#/soap/cpp/deleteCpp.cfm?CPPUUID=#index.CPPUUID#" >
								<img class="ListIconLinks img18_18 delete_18_18" src="#rootUrl#/css/images/blank.gif" title="Edit CPP">
							</a>
						</td>
				</tr>
			</cfloop>
			</table>
		</cfoutput>
		</cfif>
		<input type="button" value="Create CPP" onclick="location.href='createCpp.cfm';">
	</body>
</html>

