<!--- File assumes that referenced variables are already set. --->

<!--- Set variables based on UserSpecifiedData. Most of the string is unneccesary; we only care about
	fields 4, 6-8, 12, and 14 for this survey. --->
<cfset dataset			= SELECTATTSMSData.UserSpecifiedData_vch>
<cfloop condition		="FINDNOCASE('||',dataset) GT 0">
	<cfset dataset 		= REPLACENOCASE(dataset,'||','|0|','all')>
</cfloop>
<cfset dataRray			= ListToArray(dataset,'|')>

<cfset inpRepName		= dataRray[5]>
<cfset Lang				= dataRray[9]>
<cfset AltLang			= dataRray[10]>
<cfset AltScript		= dataRray[11]>
<cfset Code				= dataRray[12]>
<cfset EnableCallback	= dataRray[14]>

<!--- Number to call --->
<cfset inpDialString	= trim(SELECTATTSMSData.DialString_vch)>

<!--- Survey Specific Dialer Settings --->
<cfset inpBatchId		= Evaluate("inpBatchId_#SELECTATTSMSData.SurveyId_vch#_#Code#")>		<!--- inpBatchId set in application.cfm --->