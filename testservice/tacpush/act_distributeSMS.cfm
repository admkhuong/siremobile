<cfparam name="inpBatchId" default="">
<cfparam name="inpRepName" default="">
<cfparam name="inpCustName" default="">
<cfparam name="inpDateTime" default="">
<cfparam name="inpDialString" default="">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Test Add To Queue</title>
</head>

<body>
	<!--- The method="XXX" used in the http request must match the method used to generate key in the generateSignature() function--->
 	<cfhttp url="http://ebmdevii.messagebroadcast.com/webservice/ebm/PDC/AddToQueue" method="POST" result="returnStruct" >
	<!---<cfhttp url="http://v3.messagebroadcast.com/webservice/ebm/PDC/AddToQueue" method="POST" result="returnStruct" >--->
    
    
	   <!--- By default EBM API will return json or XML --->
	   <cfhttpparam name="Accept" type="header" value="application/json" />    	
	   <cfhttpparam type="header" name="datetime" value="#datetime#" />
       <cfhttpparam type="header" name="authorization" value="#authorization#" /> 
  	  
      <!---  <cfargument name="inpCampaignId" required="yes" default="0" hint="The pre-defined campaign Id you created under your account.">
        <cfargument name="inpContactString" required="yes" default="" hint="The contact string - Phone number ,eMail, or SMS number">
        <cfargument name="inpContactTypeId" required="no" default="" hint="1=Phone number, 2=SMS, 3=eMail">
        <cfargument name="inpInternational" required="no" default="0" hint=" 1 will flag this number as international">
        <cfargument name="inpValidateScheduleActive" required="no" default="0" hint="1 will halt message if time is currently outside of the pre-defined campaigns's scheduled range">
        <cfargument name="inpTimeZone" required="no" default="">
        <cfargument name="inpCompany" required="no" default="">
        <cfargument name="inpFirstName" required="no" default="">
        <cfargument name="inpLastName" required="no" default="">
        <cfargument name="inpAddress" required="no" default="">
        <cfargument name="inpAddress1" required="no" default="">
        <cfargument name="inpCity" required="no" default="">
        <cfargument name="inpState" required="no" default="">
        <cfargument name="inpZipCode" required="no" default="">
        <cfargument name="inpCountry" required="no" default="">
        <cfargument name="inpUserDefinedKey" required="no" default="">       
        <cfargument name="inpMMLS" required="no" default="1">
        <cfargument name="inpScheduleOffsetSeconds" required="no" default="0">--->
       
       
       <!--- If DebugAPI is greater than 0 and is specidfied then use Dev DB for testing--->
       <!--- If in debug mode make sure you are using the correct credentials--->
       <cfhttpparam type="formfield" name="DebugAPI" value="1" />
       
       <cfhttpparam type="formfield" name="inpBatchId" value="#inpBatchID#" />
       <cfhttpparam type="formfield" name="inpContactString" value="1#inpDialString#" />
       <cfhttpparam type="formfield" name="inpContactTypeId" value="3" />
       <cfhttpparam type="formfield" name="REPNAME" value="#inpRepName#" />
       <cfhttpparam type="formfield" name="CUSTNAME" value="#inpCustName#" />
       <cfhttpparam type="formfield" name="DATETIME" value="#inpDateTime#" />
       <cfhttpparam type="formfield" name="inpTimeZone" value="EST" />
    </cfhttp>
    </body>
</html>