<!--- Target Database --->
<cfset TargetATTTACRFTDB = "MBTesting">

<!--- Active Projects --->
<cfset CurrProjectIDList = "235,281,282,284,285,301,302,303,304,320,435,721,722,2872">

<!--- Batch ID List --->
<cfset inpBatchId_235_A = 816>	<!--- Survey #235 A --->
<cfset inpBatchId_235_E = 817>	<!--- Survey #235 E --->
<cfset inpBatchId_235_I = 818>	<!--- Survey #235 I --->
<cfset inpBatchId_235_B = 838>	<!--- Survey #235 B --->

<cfset inpBatchId_281_A = 906>	<!--- Survey #281 A --->
<cfset inpBatchId_281_B = 920>	<!--- Survey #281 B --->
<cfset inpBatchId_281_C = 921>	<!--- Survey #281 C --->
<cfset inpBatchId_281_D = 922>	<!--- Survey #281 D --->
<cfset inpBatchId_281_E = 923>	<!--- Survey #281 E --->

<cfset inpBatchId_282_A = 925>	<!--- Survey #282 A --->
<cfset inpBatchId_282_B = 926>	<!--- Survey #282 B --->
<cfset inpBatchId_282_C = 927>	<!--- Survey #282 C --->
<cfset inpBatchId_282_D = 928>	<!--- Survey #282 D --->
<cfset inpBatchId_282_E = 929>	<!--- Survey #282 E --->
<cfset inpBatchId_282_F = 930>	<!--- Survey #282 F --->

<cfset inpBatchId_284_A = 931>	<!--- Survey #284 A --->
<cfset inpBatchId_284_B = 935>	<!--- Survey #284 B --->
<cfset inpBatchId_284_C = 936>	<!--- Survey #284 C --->
<cfset inpBatchId_284_D = 937>	<!--- Survey #284 D --->
<cfset inpBatchId_284_E = 938>	<!--- Survey #284 E --->

<cfset inpBatchId_285_A = 950>	<!--- Survey #285 A --->

<cfset inpBatchId_301_A = 892>	<!--- AiO Survey #301 A --->
<cfset inpBatchId_302_A = 893>	<!--- AiO Survey #302 A --->
<cfset inpBatchId_302_B = 894>	<!--- AiO Survey #302 B --->
<cfset inpBatchId_303_A = 895>	<!--- AiO Survey #303 A --->

<cfset inpBatchId_304_A = 912>	<!--- Alltel Survey #304 A --->
<cfset inpBatchId_304_B = 913>	<!--- Alltel Survey #304 B --->

<cfset inpBatchId_305_A = 914>	<!--- Alltel Survey #305 A --->
<cfset inpBatchId_305_A = 917>	<!--- Alltel Test --->

<cfset inpBatchId_320_A = 898>	<!--- TACRFT Survey #320 A --->
<cfset inpBatchId_320_B = 899>	<!--- TACRFT Survey #320 B --->

<cfset inpBatchId_435_A = 819>	<!--- Survey #435 A --->
<cfset inpBatchId_435_E = 820>	<!--- Survey #435 A --->
<cfset inpBatchId_435_I = 821>	<!--- Survey #435 A --->
<cfset inpBatchId_435_M = 822>	<!--- Survey #435 A --->
<cfset inpBatchId_435_Q = 823>	<!--- Survey #435 A --->
<cfset inpBatchId_435_U = 824>	<!--- Survey #435 A --->

<cfset inpBatchId_721_A = 999>	<!--- Survey #721 A --->
<cfset inpBatchId_721_B = 1000>	<!--- Survey #721 B --->
<cfset inpBatchId_721_C = 1001>	<!--- Survey #721 C --->
<cfset inpBatchId_721_D = 1002>	<!--- Survey #721 D --->
<cfset inpBatchId_721_E = 1003>	<!--- Survey #721 E --->
<cfset inpBatchId_721_F = 1004>	<!--- Survey #721 F --->

<cfset inpBatchId_721_A_ES = 1005>	<!--- Survey #721 A (Span) --->
<cfset inpBatchId_721_B_ES = 1006>	<!--- Survey #721 B (Span) --->
<cfset inpBatchId_721_C_ES = 1007>	<!--- Survey #721 C (Span) --->
<cfset inpBatchId_721_D_ES = 1008>	<!--- Survey #721 D (Span) --->
<cfset inpBatchId_721_E_ES = 1009>	<!--- Survey #721 E (Span) --->
<cfset inpBatchId_721_F_ES = 1010>	<!--- Survey #721 F (Span) --->

<cfset inpBatchId_722_A = 1020>	<!--- Survey #722 A --->
<cfset inpBatchId_722_B = 1028>	<!--- Survey #722 B --->
<cfset inpBatchId_722_C = 1029>	<!--- Survey #722 C --->
<cfset inpBatchId_722_D = 1030>	<!--- Survey #722 D --->
<cfset inpBatchId_722_E = 1031>	<!--- Survey #722 E --->
<cfset inpBatchId_722_F = 1032>	<!--- Survey #722 F --->
<cfset inpBatchId_722_G = 1033>	<!--- Survey #722 G --->
<cfset inpBatchId_722_H = 1034>	<!--- Survey #722 H --->
<cfset inpBatchId_722_I = 1035>	<!--- Survey #722 I --->

<cfset inpBatchId_722_A_ES = 1036>	<!--- Survey #722 A (Span) --->
<cfset inpBatchId_722_B_ES = 1038>	<!--- Survey #722 B (Span) --->
<cfset inpBatchId_722_C_ES = 1039>	<!--- Survey #722 C (Span) --->
<cfset inpBatchId_722_D_ES = 1040>	<!--- Survey #722 D (Span) --->
<cfset inpBatchId_722_E_ES = 1041>	<!--- Survey #722 E (Span) --->
<cfset inpBatchId_722_F_ES = 1042>	<!--- Survey #722 F (Span) --->
<cfset inpBatchId_722_G_ES = 1043>	<!--- Survey #722 G (Span) --->
<cfset inpBatchId_722_H_ES = 1044>	<!--- Survey #722 H (Span) --->
<cfset inpBatchId_722_I_ES = 1045>	<!--- Survey #722 I (Span) --->

<cfset inpBatchId_723_A = 1047>	<!--- Survey #723 A --->
<cfset inpBatchId_723_B = 1052>	<!--- Survey #723 B --->
<cfset inpBatchId_723_C = 1053>	<!--- Survey #723 C --->
<cfset inpBatchId_723_D = 1054>	<!--- Survey #723 D --->
<cfset inpBatchId_723_E = 1055>	<!--- Survey #723 E --->
<cfset inpBatchId_723_F = 1056>	<!--- Survey #723 F --->
<cfset inpBatchId_723_G = 1057>	<!--- Survey #723 G --->
<cfset inpBatchId_723_I = 1059>	<!--- Survey #723 I --->
<cfset inpBatchId_723_J = 1060>	<!--- Survey #723 J --->
<cfset inpBatchId_723_K = 1061>	<!--- Survey #723 K --->
<cfset inpBatchId_723_L = 1062>	<!--- Survey #723 L --->
<cfset inpBatchId_723_M = 1063>	<!--- Survey #723 M --->

<cfset inpBatchId_2872_A = 811>	<!--- Survey #2872 A --->
<cfset inpBatchId_2872_E = 812>	<!--- Survey #2872 E --->
<cfset inpBatchId_2872_I = 813>	<!--- Survey #2872 I --->

<!--- generate signature string for EBM --->
<cffunction name="generateSignature" returnformat="json" access="remote" output="false">
    <cfargument name="method" required="true" type="string" default="GET" hint="Method" />
             
    <cfset dateTimeString = GetHTTPTimeString(Now())>
    <!--- Create a canonical string to send --->
    <cfset cs = "#arguments.method#\n\n\n#dateTimeString#\n\n\n">
    <cfset signature = createSignature(cs,variables.secretKey)>
    
    <cfset retval.SIGNATURE = signature>
    <cfset retval.DATE = dateTimeString>
    
    <cfreturn retval>
</cffunction>

<!--- Encrypt with HMAC SHA1 algorithm--->
<cffunction name="HMAC_SHA1" returntype="binary" access="private" output="false" hint="NSA SHA-1 Algorithm">
  <cfargument name="signKey" type="string" required="true" />
  <cfargument name="signMessage" type="string" required="true" />

  <cfset var jMsg = JavaCast("string",arguments.signMessage).getBytes("iso-8859-1") />
  <cfset var jKey = JavaCast("string",arguments.signKey).getBytes("iso-8859-1") />
  <cfset var key = createObject("java","javax.crypto.spec.SecretKeySpec") />
  <cfset var mac = createObject("java","javax.crypto.Mac") />

  <cfset key = key.init(jKey,"HmacSHA1") />
  <cfset mac = mac.getInstance(key.getAlgorithm()) />
  <cfset mac.init(key) />
  <cfset mac.update(jMsg) />

  <cfreturn mac.doFinal() />
</cffunction>


<cffunction name="createSignature" returntype="string" access="public" output="false">
   <cfargument name="stringIn" type="string" required="true" />
    <cfargument name="scretKey" type="string" required="true" />
    <!--- Replace "\n" with "chr(10) to get a correct digest --->
    <cfset var fixedData = replace(arguments.stringIn,"\n","#chr(10)#","all")>
    <!--- Calculate the hash of the information --->
    <cfset var digest = HMAC_SHA1(scretKey,fixedData)>
    <!--- fix the returned data to be a proper signature --->
    <cfset var signature = ToBase64("#digest#")>
    
    <cfreturn signature>
</cffunction>

<cfset variables.accessKey = '4358024801C166045644' />
<cfset variables.secretKey = 'DEfcA4547393+724933808dd96133b840470e780' />

<cfset variables.sign = generateSignature("POST") /> <!---Verb must match that of request type--->
<cfset datetime = variables.sign.DATE>
<cfset signature = variables.sign.SIGNATURE>

<cfset authorization = variables.accessKey & ":" & signature>