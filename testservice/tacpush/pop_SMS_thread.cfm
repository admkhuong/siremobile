<!--- Set up page specific processing variables here --->
<cfset TargetATTTACRFTDB = "MBTesting" />
<cfset CurrProjectIDList = "0" />

<cfparam name="VerboseDebug" default="0">
<cfparam name="InvalidRecordCount" default="0">
<cfparam name="inpNumberOfDialsToRead" default = "1000">
<cfparam name="FileSeqNumber_int" default="0">
<cfparam name="inpMod" default="1">
<cfparam name="DoModValue" default="0">

<!--- Platform Defining Parameters. --->
<cfparam name="DoQA" default="1">
<cfparam name="Platform" default="">

<cfset inpBatchId="0">
<cfset inpDialString ="">

<cfset inpBadRecord = 0>

<cfset CountDistributed_int = 0>

<!--- This is the number of threads to run--->
<cfset NumThreads = 5 />
<cfset inpMod = NumThreads>

<!--- Start timing test --->
<cfset tickBegin = GetTickCount()>

<!--- Loop to create threads--->
<cfloop index="pageIndex" from="0" to="#(NumThreads - 1)#"> 
	<!--- Main thread processing here. You must manually define all variables to pass in here. --->
    <cfthread name="thr#pageIndex#" threadIndex="#pageIndex#" action="run">
        <cfset RecordCounted = 0 />
        <cfset DoModValue = threadIndex>
        
      	<cfset Variables.theOutput[threadIndex]="Thread index attribute:" & threadIndex & "&nbsp;&nbsp; Page index value: " & pageIndex> 
   	
		<!--- Generic AT&T AO Query Loop --->
		<cfquery name="SELECTATTSMSData" datasource="MBATTSQL2K">
			SELECT TOP #inpNumberOfDialsToRead#
				BatchQueue_int,
				FileSeqNumber_int,
				SurveyId_vch,
				DialString_vch,
				LocalityId_int,
				TimeZone_ti,
				UserSpecifiedData_vch
			FROM
				#TargetATTTACRFTDB#..Batch_Queue (NOLOCK)
			WHERE
				Status_int = 0
			<cfif TargetATTTACRFTDB EQ "TACRFT">
				AND SurveyId_vch IN (#CurrProjectIDList#)
			</cfif>
			AND
				SMS_bit = 1
			AND
				FileSeqNumber_int <cfif DoQA GT 0> < <cfelse> > </cfif> 0
			AND
				InitialDate_dt > '#DateFormat(NOW(),"yyyy-mm-dd")#'
            AND
				BatchQueue_int%#inpMod# = #DoModValue#
			ORDER BY
				InitialDate_dt,
				SUBSTRING(DialString_vch,5,3)
		</cfquery>
        <cfset thread.DoModValue = DoModvalue>
		<!--- Do something else useful here ....--->        
	    <cfset RecordCounted = SELECTATTSMSData.RecordCount>
         
         <cfif SELECTATTSMSData.RecordCount GT 0>
            <cfquery name="UpdateATTDialData" datasource="MBATTSQL2K">
                UPDATE 
                    #TargetATTTACRFTDB#..Batch_Queue
                SET
                    Status_int = 1  <!--- 1 = Queued --->
                WHERE
                    BatchQueue_int IN (#ValueList(SELECTATTSMSData.BatchQueue_int,',')#)
            </cfquery>
        </cfif>
        
        <cfloop query="SELECTATTSMSData">
        	<cfset inpDialString ="">
			<cfset inpBatchId ="">
            <cfset inpRepName ="">
            
            <cfset inpBadRecord = 0>
            
            <cftry>
				<cfinclude template="LoadLogic/#SELECTATTSMSData.SurveyId_vch#.cfm">
			<cfcatch type="any">
			</cfcatch>
			</cftry>
            
            <cfif inpBadRecord EQ 0>
				<cfinclude template="act_distributeSMS.cfm">
			</cfif>
            
            <cfstoredproc datasource="MBATTSQL2K" procedure="tacrft_update_finished_qa">
            	<cfprocparam cfsqltype="cf_sql_integer" value="#BatchQueue_int#">
            </cfstoredproc>
        </cfloop>
    </cfthread> 
</cfloop> 
 
<!--- Join all processing threads to main page and wait for them to finish - Careful - timeout 0 is infinite ---> 
<cfthread action="join" name="thr0,thr1,thr2,thr3,thr4" timeout=0/> 
 
<!--- Out put any page results here--->
<cfloop index="j" from="0" to="#(NumThreads-1)#"> 
    <cftry> 
        <cfset outBuff = cfthread["thr#j#"].Elapsedtime />
        <cfoutput>Elapsed Time: #outBuff#<BR /></cfoutput>
        <cfset outBuff2 = Evaluate("thr#j#")>
    <cfcatch type="any">
        <cfdump var="#cfcatch#" />
    </cfcatch>
    </cftry>
</cfloop>

<!--- Calculate final result --->
<cfset tickEnd = GetTickCount()>
<cfset testTime = tickEnd - tickBegin>

<cfoutput>
	testTime = #testTime#
</cfoutput>