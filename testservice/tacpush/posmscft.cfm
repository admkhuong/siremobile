
<!--- Set up page specific processing variables here --->
<cfset TargetATTTACRFTDB = "MBTesting" />
<cfset CurrProjectIDList = "0" />


<cfparam name="VerboseDebug" default="0">
<cfparam name="InvalidRecordCount" default="0">
<cfparam name="inpNumberOfDialsToRead" default = "250">
<cfparam name="FileSeqNumber_int" default="0">
<cfparam name="inpMod" default="1">
<cfparam name="DoModValue" default="0">

<!--- Platform Defining Parameters. --->
<cfparam name="DoQA" default="0">
<cfparam name="Platform" default="">

<cfset inpBatchId="0">
<cfset inpDialString ="">

<cfset inpBadRecord = 0>

<cfset CountDistributed_int = 0>



<!--- This is the number of threads to run--->
<cfset NumThreads = 5 />


<!--- Start timing test --->
<cfset tickBegin = GetTickCount()>


<!--- Loop to create threads--->
<cfloop index="pageIndex" from="0" to="#(NumThreads - 1)#"> 

	<!--- Main thread processing here. You must manually define all variables to pass in here. --->
    <cfthread name="thr#pageIndex#" threadIndex="#pageIndex#" action="run" 
        inpMod="#NumThreads#" 
        DoModValue="#pageIndex#"
        inpNumberOfDialsToRead= "#inpNumberOfDialsToRead#"
        TargetATTTACRFTDB= "#TargetATTTACRFTDB#"
        CurrProjectIDList= "#CurrProjectIDList#"
        DoQA = "#DoQA#"
    > 
        
        <cfset thread.RecordCounted = 0 />
        
      	<cfset Variables.theOutput[threadIndex]="Thread index attribute:" & threadIndex & "&nbsp;&nbsp; Page index value: " & pageIndex> 
   	
		<!--- Generic AT&T AO Query Loop --->
		<cfquery name="thread.SELECTATTSMSData" datasource="MBATTSQL2K">
			SELECT TOP #inpNumberOfDialsToRead#
				BatchQueue_int,
				FileSeqNumber_int,
				SurveyId_vch,
				DialString_vch,
				LocalityId_int,
				TimeZone_ti,
				UserSpecifiedData_vch
			FROM
				#TargetATTTACRFTDB#..Batch_Queue (NOLOCK)
			WHERE
		<!---		Status_int <> 2
			<cfif TargetATTTACRFTDB EQ "TACRFT">
				AND SurveyId_vch IN (#CurrProjectIDList#)
			</cfif>
			AND
				SMS_bit = 1
			AND--->
				FileSeqNumber_int <cfif DoQA GT 0> < <cfelse> > </cfif> 0
		<!---	AND
				InitialDate_dt > '#DateFormat(NOW(),"yyyy-mm-dd")#'--->
            AND
				BatchQueue_int%#inpMod# = #DoModValue#
			ORDER BY
				InitialDate_dt,
				SUBSTRING(DialString_vch,5,3)
		</cfquery>
		
        
		<!--- Do something else useful here ....--->        
        
	     
    </cfthread> 
</cfloop> 
 
 
<!--- Join all processing threads to main page and wait for them to finish - Careful - timeout 0 is infinite ---> 
<cfthread action="join" name="thr0,thr1,thr2,thr3,thr4" timeout=0/> 
 
<!--- Out put any page results here--->
<cfloop index="j" from="0" to="#(NumThreads-1)#"> 

    <cftry> 
        <cfoutput>#theOutput[j]# <br /></cfoutput> 
        
        <cfset outBuff = cfthread["thr#j#"].Elapsedtime />
        <cfset outBuff2 = thr0 />
        
        <cfoutput>#outBuff#<BR /></cfoutput>
        <cfoutput><cfdump var="#outBuff2#" /><BR /></cfoutput>
        
    <cfcatch type="any">
    
        <cfdump var="#cfcatch#" />
    
    </cfcatch>
    
    </cftry>

</cfloop>

<!--- Calculate final result --->
<cfset tickEnd = GetTickCount()>
<cfset testTime = tickEnd - tickBegin>

<cfoutput>
	testTime = #testTime#
</cfoutput>

                                
                                
















