Begin....<br/>

<cfset inpUserID = 634 />

<cfinvoke method="GetBalanceSire" returnvariable="RetGetBalance" component="session.cfc.csc.csc">
    <cfinvokeargument name="inpUserId" value="634"/>
</cfinvoke>

<cfdump var="#RetGetBalance#" />


<cfinvoke method="DeductCreditsSire" returnvariable="RetDeductCreditsSire" component="session.cfc.csc.csc">
    <cfinvokeargument name="inpUserId" value="634"/>
    <cfinvokeargument name="inpCredits" value="1"/>
</cfinvoke>
                
<cfdump var="#RetDeductCreditsSire#" />  
                
<cfinvoke method="GetBalanceSire" returnvariable="RetGetBalance2" component="session.cfc.csc.csc">
    <cfinvokeargument name="inpUserId" value="634"/>
</cfinvoke>

<cfdump var="#RetGetBalance2#" />            
                
               
               
<cfset inpCredits = 1 />               
               
               <cfif inpCredits LTE (RetGetBalance.BALANCE + RetGetBalance.PROMOTIONCREDITBALANCE + RetGetBalance.BUYCREDITBALANCE) >
                    
                    	Level 1 <br/>
                    
                        <cfif inpCredits LTE RetGetBalance.BALANCE>
                            
                            
                            Level 1A <br/>
                            
                            <!--- Subtraction Credits And update balance--->
<!---
                            <cfquery name="updateUsers" datasource="#Session.DBSourceEBM#">
                                  UPDATE
                                    simplebilling.billing
                                  SET
                                    Balance_int = Balance_int - #inpCredits#
                                  WHERE
                                    userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.inpUserId#">
                              </cfquery>
--->
                        <cfelseif inpCredits LTE (RetGetBalance.BALANCE + RetGetBalance.PROMOTIONCREDITBALANCE) >

                            <!--- Check for plan and promotion credit --->

							Level 1B <br/>

                            <cfif RetGetBalance.BALANCE GT 0>
                                <cfset DifferenceBalanceBufferPlan = inpCredits - RetGetBalance.BALANCE />
                                <cfset BalDiffBufferPlan = inpCredits - DifferenceBalanceBufferPlan />
                            <cfelse>
                                <cfset DifferenceBalanceBufferPlan = inpCredits />
                                <cfset BalDiffBufferPlan = 0 />
                            </cfif>

							<cfoutput>
							BalDiffBufferPlan = #BalDiffBufferPlan# <br/>
							DifferenceBalanceBufferPlan = #DifferenceBalanceBufferPlan# <br/>
							</cfoutput>	

                            <cfquery name="updateUsers" datasource="Bishop">
                                  UPDATE  
                                    simplebilling.billing
                                  SET   
                                    Balance_int = Balance_int - #BalDiffBufferPlan#,
                                    PromotionCreditBalance_int = PromotionCreditBalance_int - #DifferenceBalanceBufferPlan#
                                  WHERE 
                                    userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#inpUserID#">
                             </cfquery>

                        <cfelse>
                        
                        	Level 1C <br/>
                        
                            <!--- How much less? Dont just set to 0 in case this transaction happens while somthing else is updating balance like monthly plan updates --->
                            <!--- Take Balance to 0 first then subtract difference from buy credits--->
                            <!--- It is OK for balance to go a little negative do to simultaneous transactions --->

                            <cfif RetGetBalance.BALANCE GT 0>
                                <cfset DifferenceBalanceBufferPlan = inpCredits - RetGetBalance.BALANCE />
                                <cfset BalDiffBufferPlan = inpCredits - DifferenceBalanceBufferPlan />
                            <cfelse>
                                <cfset DifferenceBalanceBufferPlan = inpCredits />
                                <cfset BalDiffBufferPlan = 0 />
                            </cfif>


                            <!--- Check promotion credit available --->
                            <cfif RetGetBalance.PROMOTIONCREDITBALANCE GT 0>
                                <cfset DifferenceBalanceBufferPromo = DifferenceBalanceBufferPlan - RetGetBalance.PROMOTIONCREDITBALANCE />
                                <cfset BalDiffBufferPromo = DifferenceBalanceBufferPlan - DifferenceBalanceBufferPromo />
                            <cfelse>
                                <cfset DifferenceBalanceBufferPromo = DifferenceBalanceBufferPlan />
                                <cfset BalDiffBufferPromo = 0 />
                            </cfif>

<!---
                            <cfquery name="updateUsers" datasource="#Session.DBSourceEBM#">
                                  UPDATE
                                    simplebilling.billing
                                  SET
                                    Balance_int = Balance_int - #BalDiffBufferPlan#,
                                    PromotionCreditBalance_int = PromotionCreditBalance_int - #BalDiffBufferPromo#,
                                    BuyCreditBalance_int = BuyCreditBalance_int - #DifferenceBalanceBufferPromo#
                                  WHERE
                                    userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.inpUserId#">
                             </cfquery>
--->
                         
                        </cfif>
                        
               </cfif>	
                      
                      
  <cfinvoke method="GetBalanceSire" returnvariable="RetGetBalance3" component="session.cfc.csc.csc">
    <cfinvokeargument name="inpUserId" value="634"/>
</cfinvoke>

<cfdump var="#RetGetBalance3#" />            
                                  
                
<br/>...end
<br/>
<br/>
<br/>