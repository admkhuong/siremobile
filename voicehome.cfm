test 3		
         <cfoutput>




                <div style="display:inline; text-align:left;" >
                	<img src="#rootUrl#/#publicPath#/help/images/icons/doc-help-icon32x32.png" width="32" height="32" style="float:left; padding-right: 10px;"/>

                    <h2 style="line-height:32px; color: ##f8853b;">Voice</h2>

               		<div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">

		            	<p style="font-weight:300;">
                        Interactive voice broadcasting (also referred to as interactive voice messaging) programs allow the call recipient to listen to the recorded message and interact with the system by pressing keys on the phone keypad. The system can detect which key is pressed and be programmed to interact and play various messages accordingly. This is a form of Interactive voice response (IVR).
						</p>

                        <p style="font-weight:300;">The actions that can be programmed may include surveys, information confirmation, contact preference confirmation, or navigation through a phone menu. An example of the use of this technology is automated phone surveys, where professional polling organizations place automatic calls to conduct surveys. Respondents are provided survey questions that are answered using DTMF-tone keypad responses.
						</p>

						<p style="font-weight:300;">More qualitative results can be captured by allowing call recipients to leave their own voice message instead of just button presses. This function can not only be used for "grassroots" lobbying, but can be used to allow loved ones to leave voice messages for each other when separated after a disaster.
						</p>

               		</div>

                	<div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">

	        	        <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    		            <h3 style="margin-left: 20px;">Answer Detection Logic</h3>

                  		<cfinclude template="detectionlogic.cfm">

                	</div>





                </div>






 		</cfoutput>
